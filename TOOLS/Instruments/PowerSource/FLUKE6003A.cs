﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Instruments.Utility;
using Ivi.Visa.Interop;
using log4net;
using System;
using System.ComponentModel;
using System.Globalization;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.08)]
    public class FLUKE6003A : PowerSourceACIII, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(FLUKE6003A));
        private CultureInfo enUS = new CultureInfo("en-US");

        private FormattedIO488 _Gpib;
        private ResourceManager _mgr;
        private byte _adrr = 6;
        private string version;
        private bool _IsOpen = false;
        private OutputFrequencySynchronizationModes synchronismMode;
        private bool syncLocked = false;
        private const double impedanciaFluke = 0.018;

        public enum PrivateCommands
        {
            SET_VOLTAGE_GROUND_CONFIGURATION = Commands.USER_DEFINED_COMMANDS + 1,
            SET_CURRENT_GROUND_CONFIGURATION,
            SET_COMPLETE_REMOTE_MODE,
            SET_OUTPUT_CURRENT_MODE,
            SET_OUTPUT_VOLTAGE_FROM_CURRENT_FACTOR,
            SET_OUTPUT_FREQUENCY_SYNCHRONIZATION,
            GET_OUTPUT_FREQUENCY_SYNCHRONIZATION,
            GET_OUTPUT_FREQUENCY_SYNCHRONIZATION_LOCK,
            SET_POLARITY_CURRENT
        };

        // Comandos comunes o de control
        private const string READ_IDENTIFICATION_INFORMATION_COMMAND = "*IDN?";
        private const string READ_OPERATION_COMPLETE_COMMAND = "*OPC?";
        private const string READ_EVENT_STATUS_REGISTER_COMMAND = "*ESR?";
        private const string READ_EVENT_STATUS_ENEBLED_COMMAND = "*ESE?";
        private const string RESET_COMMAND = "*RST";
        private const string CLEAR_STATUS_COMMAND = "*CLS";
        private const string SET_COMPLETE_REMOTE_MODE= "SYST:RWL";

        // Comandos de las salidas
        private const string SET_OUTPUT = "OUTP {0}";
        private const string GET_OUTPUT = "OUTP?";

        private const string SET_VOLTAGE_OUTPUT_LOW_CONNECTION = "OUTP:LOWV {0}";
        private const string SET_CURRENT_OUTPUT_LOW_CONNECTION = "OUTP:LOWC {0}";

        private const string SET_OUTPUT_SYNCHRONIZATION = "OUTP:SYNC {0}";
        private const string GET_OUTPUT_SYNCHRONIZATION = "OUTP:SYNC?";
        private const string GET_OUTPUT_SYNCHRONIZATION_LOCKING = "OUTP:SYNC:LOCK?";

        private const string SET_VOLTAGE_FROM_CURRENT = "OUTP:VFC {0}";
        private const string GET_VOLTAGE_FROM_CURRENT = "OUTP:VFC?";
        private const string SET_VOLTAGE_FROM_CURRENT_FACTOR = "OUTP:EQU:FACT {0}";
        private const string GET_VOLTAGE_FROM_CURRENT_FACTOR = "OUTP:EQU:FACT?";

        private const string SET_PACE_POWER = "P{0}E:POW {1}";
        private const string GET_PACE_POWER = "P{0}E:POW?";

        private const string SET_PACE_VOLTAGE = "P{0}E:VOLT{1} {2}";
        private const string GET_PACE_VOLTAGE = "P{0}E:VOLT{1}?";
        private const string SET_PACE_VOLTAGE_ENABLE = "P{0}E:VOLT{1}:ENAB {2}";
        private const string GET_PACE_VOLTAGE_ENABLE = "P{0}E:VOLT{1}:ENAB?";

        private const string SET_PACE_CURRENT = "P{0}E:CURR{1} {2}";
        private const string GET_PACE_CURRENT = "P{0}E:CURR{1}?";
        private const string SET_PACE_CURRENT_ENABLE = "P{0}E:CURR{1}:ENAB {2}";
        private const string GET_PACE_CURRENT_ENABLE = "P{0}E:CURR{1}:ENAB?";

        private const string SET_POLARITY_CURRENT = "PAC:POL {0}";

        private const string SET_PACE_VOLTAGE_PHASE_ANGLE = "PACE:VOLT{0}:PHAS {1}";
        private const string GET_PACE_VOLTAGE_PHASE_ANGLE = "PACE:VOLT{0}:PHAS?";

        private const string SET_PACE_CURRENT_PHASE_ANGLE = "PACE:CURR{0}:PHAS {1}";
        private const string GET_PACE_CURRENT_PHASE_ANGLE = "PACE:CURR{0}:PHAS?";

        private const string SET_PACE_FREQUENCY = "PACE:FREQ {0}";
        private const string GET_PACE_FREQUENCY = "PACE:FREQ ?";

        public AC_DC AC_DC { get; set; }

        public bool Voltage_from_Current { get; set; }

        public double DeviceInputImpedance { get; set; }

        public byte PortAddress
        {
            get { return _adrr; }
            set
            {
                Close();
                _adrr = value;
            }
        }

        private void Close()
        {
            if (_IsOpen && _Gpib.IO != null)
                _Gpib.IO.Close();

            _IsOpen = false;
        }

        private void Open()
        {
            Close();

            _Gpib.IO = (IMessage)_mgr.Open("GPIB0::" + _adrr.ToString(), AccessMode.NO_LOCK, 2000, "");
            _Gpib.IO.TerminationCharacterEnabled = true;
            _IsOpen = true;
        }

        public void SetPortAdress(string portAdress)
        {
            var portTmp = portAdress.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortAddress = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento portAdress a byte");
        }

        private bool IsOpen()
        {
            return _Gpib != null && _Gpib.IO != null;
        }

        public FLUKE6003A()
            : base(logger)
        {
            if (_Gpib == null)
                _Gpib = new FormattedIO488();

            protocol = new StringProtocol();
            protocol.CaracterFinRx = "\n";
            protocol.CaracterFinTx = "\n";
            transport = new MessageTransport(protocol, new GpibPortAdapter(_Gpib), logger);
            _mgr = new ResourceManager();

            AC_DC = Utility.AC_DC.AC;
        }     

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));
            return version;
        }

        //****************************************************************************************
        public void GetOutputSynchronism()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.GET_OUTPUT_FREQUENCY_SYNCHRONIZATION, true));
        }

        public void SetRemoteMode()
        {
            SendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_COMPLETE_REMOTE_MODE, false));
        }

        public void SetCurrent()
        {
            SendCommand(new Command<Commands>(Commands.SET_CURRENT, true));
        }
       
        public void SetOutputCurrentMode(OutputCurrentModes currentMode)
        {
            SetGndCurrent(false);

            switch (currentMode)
            {
                case OutputCurrentModes.CURRENT:
                    Voltage_from_Current = false;
                   SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_CURRENT_MODE, true, "OFF"));
                break;
                case OutputCurrentModes.VOLTAGE_FROM_CURRENT:
                    Voltage_from_Current = true;
                   SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_CURRENT_MODE, true, "ON"));
                break;
                default:
                    throw new NotSupportedException("El modo de trabajo seleccionado para la corriente de salida es inválido");
            }
        }

        public enum typePolarity
        {
            LEAD,
            LAG
        }

        public void Set_AC_DC(AC_DC type)
        {
            AC_DC = type;
        }

        public void SetPolarityCurrent(typePolarity type)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_POLARITY_CURRENT, true, type));
        }

        public void SetGndVoltage(bool active = false)
        {
           SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE_GROUND_CONFIGURATION, true, active ? "GRO" : "FLO"));           
        }

        public void SetGndCurrent(bool active = false)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CURRENT_GROUND_CONFIGURATION, true, active ? "GRO" : "FLO"));
        }

        public void SetVoltageFromCurrentFactor(double factor)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_VOLTAGE_FROM_CURRENT_FACTOR, true, factor));
        }

        public void SetOuputSynchronism(OutputFrequencySynchronizationModes mode)
        {
            GetOutputSynchronism();
            if (synchronismMode != mode)
            {
                SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_FREQUENCY_SYNCHRONIZATION, true, mode.GetDescription()));
                if(mode == OutputFrequencySynchronizationModes.LINE)
                    internalWaitSincronismLock();            
            }
        }

        private bool internalWaitSincronismLock()
        {
            var sampler = Sampler.Run(
            () => { return new SamplerConfig { Interval = 2000, InitialDelay = 2000, NumIterations = 60, CancelOnException = true, ThrowException = true }; },
            (step) =>
            {
                SendCommand(new Command<PrivateCommands>(PrivateCommands.GET_OUTPUT_FREQUENCY_SYNCHRONIZATION_LOCK, true));
                step.Cancel = syncLocked;
            });

            if (sampler.Canceled)
                return true;

            TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE("FLUKE6003 ESPERA BLOQUEO SYNCRONISMO").Throw();
            return false;
        }

        protected override bool ValidateEstabilization( double tolerance)
        {
            ReadVoltage();
            ReadCurrent();
            return DeviceData.Voltage.InToleranceFrom(DevicePreset.Voltage, tolerance) &&
               DeviceData.Current.InToleranceFrom(DevicePreset.Current, tolerance);
        }

        public void Dispose()
        {
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            if (!_IsOpen)
                Open();

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:
                    message = Presets.Voltage.L1 != 0 ? StringMessage.Create(enUS, SET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 1, "ON") : StringMessage.Create(enUS, SET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 1, "OFF");
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);

                    if (!Presets.Voltage.L1.Equals(0))
                    {
                        message = StringMessage.Create(enUS, SET_PACE_VOLTAGE, AC_DC.ToString(), 1, Presets.Voltage.L1);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }

                    message = Presets.Voltage.L2 != 0 ? StringMessage.Create(enUS, SET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 2, "ON") : StringMessage.Create(enUS, SET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 2, "OFF");
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    if (!Presets.Voltage.L2.Equals(0))
                    {
                        message = StringMessage.Create(enUS, SET_PACE_VOLTAGE, AC_DC.ToString(), 2, Presets.Voltage.L2);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }

                    message = Presets.Voltage.L3 != 0 ? StringMessage.Create(enUS, SET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 3, "ON") : StringMessage.Create(enUS, SET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 3, "OFF");
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    if (!Presets.Voltage.L3.Equals(0))
                    {
                        message = StringMessage.Create(enUS, SET_PACE_VOLTAGE, AC_DC.ToString(), 3, Presets.Voltage.L3);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }

                    DevicePreset.Voltage = Presets.Voltage;
                    break;

                case (int)Commands.SET_CURRENT:

                    if (Voltage_from_Current)
                    {
                        var correctionVoltage = (1 - (DeviceInputImpedance / (DeviceInputImpedance + impedanciaFluke))) * 100;
                        var volategeCorrectionL1 = 0D;
                        var volategeCorrectionL2 = 0D;
                        var volategeCorrectionL3 = 0D;

                        if (Presets.Current.L1 >= 0.333)
                            volategeCorrectionL1 = (Presets.Current.L1 * correctionVoltage) / 100;

                        if (Presets.Current.L2 >= 0.333)
                            volategeCorrectionL2 = (Presets.Current.L1 * correctionVoltage) / 100;

                        if (Presets.Current.L3 >= 0.333)
                            volategeCorrectionL3 = (Presets.Current.L1 * correctionVoltage) / 100;

                        Presets.Current += TriLineValue.Create(volategeCorrectionL1, volategeCorrectionL2, volategeCorrectionL3);
                    }

                    message = Presets.Current.L1 != 0 ? StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 1, "ON") : StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(),1, "OFF");
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    if (!Presets.Current.L1.Equals(0))
                    {
                        message = StringMessage.Create(enUS, SET_PACE_CURRENT, AC_DC.ToString(), 1, Presets.Current.L1);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }

                    message = Presets.Current.L2 != 0 ? StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 2, "ON") : StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 2, "OFF");
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    if (!Presets.Current.L2.Equals(0))
                    {
                        message = StringMessage.Create(enUS, SET_PACE_CURRENT, AC_DC.ToString(), 2, Presets.Current.L2);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }

                    message = Presets.Current.L3 != 0 ? StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 3, "ON") : StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 3, "OFF");
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    if (!Presets.Current.L3.Equals(0))
                    {
                        message = StringMessage.Create(enUS, SET_PACE_CURRENT, AC_DC.ToString(), 3, Presets.Current.L3);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }

                    DevicePreset.Current = Presets.Current;
                    break;
                case (int)Commands.SET_FREQ:

                    if (AC_DC == AC_DC.DC)
                        break;

                    if (Presets.Freq == 0)
                        SetOuputSynchronism(FLUKE6003A.OutputFrequencySynchronizationModes.LINE);
                    else
                        SetOuputSynchronism(FLUKE6003A.OutputFrequencySynchronizationModes.INT);

                    GetOutputSynchronism();
                    if (synchronismMode != OutputFrequencySynchronizationModes.LINE)
                    {
                        message = StringMessage.Create(enUS, SET_PACE_FREQUENCY, Presets.Freq);
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                        DevicePreset.Freq = Presets.Freq;
                    }
                    break;
                case (int)Commands.SET_PF:

                    if (AC_DC == Utility.AC_DC.DC)
                        break;

                    message = StringMessage.Create(enUS, SET_PACE_CURRENT_PHASE_ANGLE, 1, Presets.Desfase.L1 + Presets.PF.L1);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    message = StringMessage.Create(enUS, SET_PACE_CURRENT_PHASE_ANGLE, 2, Presets.Desfase.L2 + Presets.PF.L2);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    message = StringMessage.Create(enUS, SET_PACE_CURRENT_PHASE_ANGLE, 3, Presets.Desfase.L3 + Presets.PF.L3);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.PF = Presets.PF;
                    break;

                case (int)Commands.SET_DESFASE:

                    if (AC_DC == Utility.AC_DC.DC)
                        break;

                    message = StringMessage.Create(enUS, SET_PACE_VOLTAGE_PHASE_ANGLE, 1, Presets.Desfase.L1);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    message = StringMessage.Create(enUS, SET_PACE_VOLTAGE_PHASE_ANGLE, 2, Presets.Desfase.L2);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    message = StringMessage.Create(enUS, SET_PACE_VOLTAGE_PHASE_ANGLE, 3, Presets.Desfase.L3);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Desfase = Presets.Desfase;
                    break;

                case (int)PrivateCommands.SET_POLARITY_CURRENT:
                    message = StringMessage.Create(enUS, SET_POLARITY_CURRENT, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)Commands.APPLY_CONFIG:
                    if (Presets.Current.L1 == 0)
                    {
                        message = StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 1, "OFF");
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }
                    if (Presets.Current.L2 == 0)
                    {
                        message = StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 2, "OFF");
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }
                    if (Presets.Current.L3 == 0)
                    {
                        message = StringMessage.Create(enUS, SET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 3, "OFF");
                        message.HasResponse = false;
                        transport.SendMessage<StringMessage>(message);
                    }
                    message = new StringMessage { Text = string.Format(SET_OUTPUT, "ON") };
                    message.HasResponse = false;
                    message.Validator = (m) => { return m.Text == "SET=1"; };
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(500);
                    break;

                case (int)Commands.APPLY_OFF:
                    message = new StringMessage { Text = string.Format(SET_OUTPUT, "OFF") };
                    message.HasResponse = false;
                    message.Validator = (m) => { return m.Text == "OFF=O"; };
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(500);
                    //ResetPresets();
                    break;

                case (int)Commands.RESET:
                    message = new StringMessage { Text = RESET_COMMAND };
                    message.HasResponse = false;
                    message.Validator = (m) => { return m.Text == "R=O"; };
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(500);
                    //ResetPresets();
                    break;

                case (int)Commands.READ_VOLTAGE:

                    var volatege1 = 0D;
                    message = StringMessage.Create(enUS, GET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 1);
                    var enabledVL1 =  transport.SendMessage<StringMessage>(message);
                    if (enabledVL1.Text== "ON")
                    {
                        message = new StringMessage { Text = string.Format(GET_PACE_VOLTAGE, AC_DC.ToString(), 1) };
                        response = transport.SendMessage<StringMessage>(message);
                        volatege1 = response.QueryNumber();
                    }
                    var volatege2 = 0D;
                    message = StringMessage.Create(enUS, GET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 2);
                    var enabledVL2 = transport.SendMessage<StringMessage>(message);
                    if (enabledVL2.Text == "ON")
                    {
                        message = new StringMessage { Text = string.Format(GET_PACE_VOLTAGE, AC_DC.ToString(), 2) };
                        response = transport.SendMessage<StringMessage>(message);
                        volatege2 = response.QueryNumber();
                    }

                    var volatege3 = 0D;
                    message = StringMessage.Create(enUS, GET_PACE_VOLTAGE_ENABLE, AC_DC.ToString(), 3);
                    var enabledVL3 = transport.SendMessage<StringMessage>(message);
                    if (enabledVL3.Text == "ON")
                    {
                        message = new StringMessage { Text = string.Format(GET_PACE_VOLTAGE, AC_DC.ToString(), 3) };
                        response = transport.SendMessage<StringMessage>(message);
                        volatege3 = response.QueryNumber();
                    }

                    DeviceData.Voltage = TriLineValue.Create(volatege1, volatege2, volatege3);
                    break;

                case (int)Commands.READ_CURRENT:

                    var current1 = 0D;
                    message = StringMessage.Create(enUS, GET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 1);
                    var enabledIL1 = transport.SendMessage<StringMessage>(message);
                    if (enabledIL1.Text == "ON")
                    {
                        message = new StringMessage { Text = string.Format(GET_PACE_CURRENT, AC_DC.ToString(), 1) };
                        response = transport.SendMessage<StringMessage>(message);
                        current1 = response.QueryNumber();
                    }

                    var current2 = 0D;
                    message = StringMessage.Create(enUS, GET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 2);
                    var enabledIL2 = transport.SendMessage<StringMessage>(message);
                    if (enabledIL2.Text == "ON")
                    {
                        message = new StringMessage { Text = string.Format(GET_PACE_CURRENT, AC_DC.ToString(), 2) };
                        response = transport.SendMessage<StringMessage>(message);
                        current2 = response.QueryNumber();
                    }

                    var current3 = 0D;
                    message = StringMessage.Create(enUS, GET_PACE_CURRENT_ENABLE, AC_DC.ToString(), 3);
                    var enabledIL3 = transport.SendMessage<StringMessage>(message);
                    if (enabledIL3.Text == "ON")
                    {
                        message = new StringMessage { Text = string.Format(GET_PACE_CURRENT, AC_DC.ToString(), 3) };
                        response = transport.SendMessage<StringMessage>(message);
                        current3 = response.QueryNumber();
                    }

                    DeviceData.Current = TriLineValue.Create(current1, current2, current3);
                    break;

                case (int)Commands.READ_DESFASE:

                    if (AC_DC == Utility.AC_DC.DC)
                        break;

                    message = new StringMessage { Text = string.Format(GET_PACE_VOLTAGE_PHASE_ANGLE, 1) };
                    response = transport.SendMessage<StringMessage>(message);
                    var desdase1 = response.QueryNumber();
                    message = new StringMessage { Text = string.Format(GET_PACE_VOLTAGE_PHASE_ANGLE, 2) };
                    response = transport.SendMessage<StringMessage>(message);
                    var desdase2 = response.QueryNumber();
                    message = new StringMessage { Text = string.Format(GET_PACE_VOLTAGE_PHASE_ANGLE, 3) };
                    response = transport.SendMessage<StringMessage>(message);
                    var desdase3 = response.QueryNumber();
                    DeviceData.Desfase = TriLineValue.Create(desdase1, desdase2, desdase3);
                    break;

                case (int)Commands.READ_POWERFACTOR:

                    if (AC_DC == Utility.AC_DC.DC)
                        break;

                    message = new StringMessage { Text = string.Format(GET_PACE_CURRENT_PHASE_ANGLE, 1) };
                    response = transport.SendMessage<StringMessage>(message);
                    var pf1 = response.QueryNumber();
                    message = new StringMessage { Text = string.Format(GET_PACE_CURRENT_PHASE_ANGLE, 2) };
                    response = transport.SendMessage<StringMessage>(message);
                    var pf2 = response.QueryNumber();
                    message = new StringMessage { Text = string.Format(GET_PACE_CURRENT_PHASE_ANGLE, 3) };
                    response = transport.SendMessage<StringMessage>(message);
                    var pf3 = response.QueryNumber();
                    DeviceData.PF = TriLineValue.Create(pf1, pf2, pf3);
                    break;

                case (int)Commands.READ_STATUS:
                    message = new StringMessage { Text = READ_OPERATION_COMPLETE_COMMAND };
                    var responseOpc = transport.SendMessage<StringMessage>(message);
                    logger.DebugFormat("OPC: {0}", responseOpc);
                    Status.IsBusy = !Convert.ToBoolean(Convert.ToInt32(responseOpc.Text));

                    message = new StringMessage { Text = READ_EVENT_STATUS_ENEBLED_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);
                    logger.DebugFormat("ERE: {0}", response);

                    message = new StringMessage { Text = READ_EVENT_STATUS_REGISTER_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);
                    logger.DebugFormat("ERS: {0}", response);

                    //TODO a veces devuelve un ; 

                    EventStatusRegister eventStatus = (EventStatusRegister)Convert.ToInt16(response.Text.Replace(";",""));
                    Status.HasError = eventStatus.HasFlag(EventStatusRegister.EXECUTION_ERROR)
                        || eventStatus.HasFlag(EventStatusRegister.DEVICE_DEPENDANT_ERROR)
                        || eventStatus.HasFlag(EventStatusRegister.COMMAND_ERROR)
                        || eventStatus.HasFlag(EventStatusRegister.QUERY_ERROR);


                    break;

                case (int)Commands.READ_IDN:
                    message = StringMessage.Create(enUS, READ_IDENTIFICATION_INFORMATION_COMMAND, "");
                    message.Validator = (m) => { return m.Text.StartsWith("FLUKE"); };
                    response = transport.SendMessage<StringMessage>(message);
                    version = response.Text.Replace("FLUKE,", "");
                    break;

                case (int)PrivateCommands.SET_CURRENT_GROUND_CONFIGURATION:
                    message = StringMessage.Create(enUS, SET_CURRENT_OUTPUT_LOW_CONNECTION, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)PrivateCommands.SET_VOLTAGE_GROUND_CONFIGURATION:
                    message = StringMessage.Create(enUS, SET_VOLTAGE_OUTPUT_LOW_CONNECTION, command.Args[0]);
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)PrivateCommands.SET_COMPLETE_REMOTE_MODE:
                    message = StringMessage.Create(enUS, SET_COMPLETE_REMOTE_MODE, string.Empty);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)PrivateCommands.SET_OUTPUT_CURRENT_MODE:
                    message = StringMessage.Create(enUS, SET_VOLTAGE_FROM_CURRENT, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)PrivateCommands.SET_OUTPUT_VOLTAGE_FROM_CURRENT_FACTOR:
                    message = StringMessage.Create(enUS, SET_VOLTAGE_FROM_CURRENT_FACTOR, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)PrivateCommands.SET_OUTPUT_FREQUENCY_SYNCHRONIZATION:
                    message = StringMessage.Create(enUS, SET_OUTPUT_SYNCHRONIZATION, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;

                case (int)PrivateCommands.GET_OUTPUT_FREQUENCY_SYNCHRONIZATION:
                    message = StringMessage.Create(enUS, GET_OUTPUT_SYNCHRONIZATION);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    synchronismMode = OutputFrequencySynchronizationModes.INT;
                    Enum.TryParse<OutputFrequencySynchronizationModes>(response.Text, out synchronismMode);
                    break;

                case (int)PrivateCommands.GET_OUTPUT_FREQUENCY_SYNCHRONIZATION_LOCK:
                    message = StringMessage.Create(enUS, GET_OUTPUT_SYNCHRONIZATION_LOCKING);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    logger.DebugFormat("LOCKED SYNC: {0}", response);
                    syncLocked = response.Text.Trim() == "1" ? true : false;
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        [Flags]
        public enum EventStatusRegister
        {
            POWER_ON = 128,
            USER_REQUEST = 64,
            COMMAND_ERROR = 32,
            EXECUTION_ERROR = 16,
            DEVICE_DEPENDANT_ERROR = 8,
            QUERY_ERROR = 4,
        }

        public enum OutputFrequencySynchronizationModes
        {
            [Description("INTERNAL")]
            INT,
            [Description("LINE")]
            LINE,
            [Description("IN2_EXTERNAL")]
            IN2
        }
    }
}