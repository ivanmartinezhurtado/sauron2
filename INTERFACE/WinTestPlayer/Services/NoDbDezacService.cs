﻿using Dezac.Data;
using Dezac.Data.ViewModels;
using Dezac.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using WinTestPlayer.Utils;

namespace WinTestPlayer.Services
{
    public class NoDbDezacService : IDezacService
    {
        private PRODUCTO producto;
        private List<T_GRUPOPARAMETRIZACION> parametros;

        public static string DataFilesPath { get; set; }

        public NoDbDezacService()
        {
            if (DataFilesPath == null)
                DataFilesPath = ConfigurationManager.AppSettings["PathLocalProductData"] ?? AppProfile.ProgramPath;
        }

        public VW_EMPLEADO Login(string userName, string password)
        {
            return new VW_EMPLEADO { IDEMPLEADO = userName, IDOPERARIO = userName };
        }

        public VW_EMPLEADO GetOperarioByIdEmpleado(string empleadoId)
        {
            return new VW_EMPLEADO { IDEMPLEADO = empleadoId, IDOPERARIO = "230" };
        }

        public PRODUCTO GetProducto(int numProducto, int version)
        {
            return producto;
        }

        public PRODUCTO GetProductoByIOrdenFab(int numProducto, int version)
        {
            string productData = Path.Combine(DataFilesPath, string.Format("PRODUCT_{0}_{1}.json", numProducto, version));

            producto = AppProfile.DeserializeFile<PRODUCTO>(productData);

            return producto;
        }

        public List<T_GRUPOPARAMETRIZACION> GetGruposParametrizacion(PRODUCTO producto, string idfase, int? numTipoGrupo = null)
        {
            string paramsData = Path.Combine(DataFilesPath, string.Format("PARAMS_{0}_{1}.json", producto.NUMPRODUCTO, producto.VERSION));

            parametros = AppProfile.DeserializeFile<List<T_GRUPOPARAMETRIZACION>>(paramsData);

            return parametros;
        }

        public void Dispose()
        {
        }

        public List<TestView> GetTestHistory(DateTime? desde, DateTime? hasta, int? numOrden, int? numProducto, int? version)
        {
            throw new NotImplementedException();
        }

        public PRODUCTOFASE GetProductoFase(PRODUCTO producto, string idFase)
        {
            return new PRODUCTOFASE { NUMPRODUCTO = producto.NUMPRODUCTO, VERSION = producto.VERSION, FASETEST = idFase, TIEMPOSTANDARD = 10, FASE = new FASE { IDFASE = idFase, DESCRIPCION = "VERIFICAR" } };
        }

        public List<ORDENPRODUCTO> GetOrdenProductoByOrden(int Orden)
        {
            return new List<ORDENPRODUCTO>();
        }

        public string GetCodigoCircutor(int numProducto)
        {
            return "OK";
        }

        public string GetCostumerCode2(int numProducto)
        {
            return "OK";
        }

        public ORDEN GetOrden(int idOrden)
        {
            return new ORDEN { NUMORDEN = idOrden, NUMPRODUCTO = producto.NUMPRODUCTO, VERSION = producto.VERSION };
        }

        public DIARIOMARCAJEFASE GetLastDiarioMarcaje(string operarioId)
        {
            return new DIARIOMARCAJEFASE { IDFASE = "120", HOJAMARCAJEFASE = new HOJAMARCAJEFASE { NUMORDEN = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")) } };
        }

        public TestIndicators GetTestIndicators(int orden, string idFase)
        {
            return new TestIndicators { };
        }

        public List<TestOrdenProductoResult> GetTestResults(int orden, string idFase, bool ok)
        {
            return new List<TestOrdenProductoResult>();
        }

        public List<T_GRUPOPARAMETRIZACION> GetGruposParametrizacion(int numProducto, int version, string idfase, int? numTipoGrupo = null)
        {
            return GetGruposParametrizacion(new PRODUCTO { NUMPRODUCTO = numProducto, VERSION = version }, idfase, numTipoGrupo);
        }

        public void SetParametroTestReportProducto(PRODUCTO producto, string idFase, int numParam, bool selFamilia, bool? selProducto)
        {
            throw new NotImplementedException();
        }

        public void AddTestReport(int numTest, byte[] pdf)
        {
            throw new NotImplementedException();
        }

        public bool GetValidateProductByIDP(int numArticulo, int version)
        {
            return true;
        }

        public List<ORDENPRODUCTO> GetOrdenProductoByOrden(int Orden, int cajaActual)
        {
            return new List<ORDENPRODUCTO>();
        }

        public int RetirarMatriculaOrdenProducto(int numMatricula, string username)
        {
            throw new NotImplementedException();
        }

        public List<LaserItemViewModel> GetDatosLaser(int producto, int version, string idfase)
        {
            throw new NotImplementedException();
        }

        public List<T_TESTFASE> GetLastTestFaseOKByOrden(int numOrden, string idFase)
        {
            throw new NotImplementedException();
        }

        public ASISTENCIATECNICA GetAsistenciaTecnica(int numAsistencia, int numLinea)
        {
            throw new NotImplementedException();
        }

        public PRODUCTO GetProductoAutoTest(string devicePC)
        {
            throw new NotImplementedException();
        }

        public DateTime? GetLastAutoTestDate(string devicePC)
        {
            return null;
        }

        public void AddTestFaseFile(int numTestFase, string name, byte[] data, string tipoDocId)
        {
            throw new NotImplementedException();
        }

        public Tuple<int, byte[]> GetSequenceFileData(PRODUCTO producto, string idFase)
        {
            return null;
        }

        public Tuple<int, byte[]> GetSequenceFileData(PRODUCTO producto, string idFase, ref int tipoSecuencia, ref string nombreFihcero)
        {
            return null;
        }

        public Tuple<int, byte[]> GetConfigFile(PRODUCTO producto, string nombreFichero)
        {
            throw new NotImplementedException();
        }

        public bool IsSerialNumberValidInOF(string serialNumber, int orden)
        {
            throw new NotImplementedException();
        }

        public int? GetLastNumTestFase(int numBastidor, int numOrden, string idFase)
        {
            throw new NotImplementedException();
        }

        public ORDENPRODUCTO GetOrdenProductoByBastidor(int bastidor)
        {
            throw new NotImplementedException();
        }

        public T_TESTFASE GetLastTestFaseOK(int numBastidor, int numOrden)
        {
            throw new NotImplementedException();
        }

        public BIEN GetBienAndParams(int numBien)
        {
            throw new NotImplementedException();
        }

        public T_TESTFASE GetLastTestFase(int numBastidor, string idFase, int numOrden = 0, bool? OK = true, bool includeParams = false)
        {
            throw new NotImplementedException();
        }

        public string GetUserParam(string application, string userName, string paramName)
        {
            throw new NotImplementedException();
        }


        public List<TestView> GetTesNumSerie(long NumSerie)
        {
            throw new NotImplementedException();
        }


        public List<TestView> GetTestHistory(int numOrden)
        {
            throw new NotImplementedException();
        }

        public List<LabelItemViewModel> GetLabels(int numProducto, int version, string idFase, string tipo = null)
        {
            throw new NotImplementedException();
        }

        public byte[] GetBinario(int numArticulo, string tipoDoc)
        {
            throw new NotImplementedException();
        }

        public List<string> GetDireccionesByNumProducto(int numProducto, int version)
        {
            throw new NotImplementedException();
        }

        public List<string> GetDirecciones(int numDireccion)
        {
            throw new NotImplementedException();
        }

        public int GetCajaActual()
        {
            throw new NotImplementedException();
        }

        public string GetCurrentSerialNumber()
        {
            throw new NotImplementedException();
        }

        public int GetNumEquiposCajaActual()
        {
            throw new NotImplementedException();
        }

        public bool ResetLineaCajas()
        {
            throw new NotImplementedException();
        }

        public ORDENPRODUCTO GetOrdenProductoByFabricacion(int bastidor, int nummatricula)
        {
            throw new NotImplementedException();
        }

        public int GetCajaActual(int orden)
        {
            throw new NotImplementedException();
        }

        public int GetNumEquiposCajaActual(int cajaActual)
        {
            throw new NotImplementedException();
        }

        public int GetIDCajaActual(int orden)
        {
            throw new NotImplementedException();
        }

        Tuple<int, int> IDezacService.GetCajaActual(int orden)
        {
            throw new NotImplementedException();
        }

        public int? GenerarOrdenEnsayo(int numProducto, int version, int cantidad, string notas)
        {
            throw new NotImplementedException();
        }

        public List<PRODUCTOFASE> GetProductosFasesHijasTest(int numProducto, int version, string idFase)
        {
            throw new NotImplementedException();
        }

        public string GetCodigoGrupo(string idFase)
        {
            throw new NotImplementedException();
        }

        public Tuple<DateTime?, string> GetLastAutoTest(string devicePC)
        {
            throw new NotImplementedException();
        }

        public int? NuevaOrdenAsistencia(int numeroAssistencia, int version = 0)
        {
            throw new NotImplementedException();
        }

        public T_FAMILIA AddFamiliaToProducto(PRODUCTO producto, int familia)
        {
            throw new NotImplementedException();
        }

        public ErrorsIrisDescription GetIrisErrorsDescriptions(string errorcode, int numfamilia, int producto, string fase)
        {
            throw new NotImplementedException();
        }

        public List<BIEN> GetBienAndParams(int numProducto, int version, string idfase)
        {
            throw new NotImplementedException();
        }

        public List<BIEN> GetBienByProductoFase(int numProducto, int version, string idfase)
        {
            throw new NotImplementedException();
        }

        public BIEN GetBienParamsAssitencia(int numBien)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetDescripcionLenguagesCliente(int numProducto, string codCircutor)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAtributosCliente(int numProducto, string codCircutor)
        {
            throw new NotImplementedException();
        }

        Tuple<DateTime?, string, string> IDezacService.GetLastAutoTest(string devicePC)
        {
            throw new NotImplementedException();
        }

        public int? GetVersionTestReport(int numFamilia)
        {
            throw new NotImplementedException();
        }

        public List<PRODUCTOFASE> GetProductoFases(PRODUCTO producto)
        {
            throw new NotImplementedException();
        }

        public int GetLaseredPieces(int orden)
        {
            throw new NotImplementedException();
        }

        public string GetMinNumSerieCaja(int numCaja)
        {
            throw new NotImplementedException();
        }


        public string GetMaxNumSerieCaja(int numCaja)
        {
            throw new NotImplementedException();
        }

        public bool GetBoxHasFinished(int numCaja, string numSerie)
        {
            throw new NotImplementedException();
        }

        public string GetRefClientLine(int numOrden)
        {
            throw new NotImplementedException();
        }

        public string GetReferenciaOrigen(int numOrden)
        {
            throw new NotImplementedException();
        }

        public string GetEANCode(int numProducto, string codCircutor)
        {
            throw new NotImplementedException();
        }

        public string GetEANMarcaCode(int numProducto, string codCircutor)
        {
            throw new NotImplementedException();
        }

        public List<T_TESTFASEFILE> GetTestFaseFiles(int bastidor, string tipoDocId)
        {
            throw new NotImplementedException();
        }

        public string[] GetNewMatricula(int numMatriculas)
        {
            throw new NotImplementedException();
        }

        public int? GetIDCajaByBastidor(int bastidor)
        {
            throw new NotImplementedException();
        }

        public string GetReferenciaVenta(int numProducto)
        {
            throw new NotImplementedException();
        }

        public string GetDescripcionComercial(int numProducto)
        {
            throw new NotImplementedException();
        }

        public string GetCodigoBcn(int numProducto)
        {
            throw new NotFiniteNumberException();
        }

        public List<T_TESTFASEFILE> GetTestFaseFiles(string name, string tipoDocId)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllSerialNumbersBox(int numCaja)
        {
            throw new NotImplementedException();
        }

        public List<DeviceDataView> GetDevicesInBoxesByOrden(int numOrden)
        {
            throw new NotImplementedException();
        }

        public T_TESTFASE GetLastTestFaseOKByOrden(int numOrden)
        {
            throw new NotImplementedException();
        }

        public List<DeviceDataView> GetDeviceBox(int numOrden)
        {
            throw new NotImplementedException();
        }

        public bool GetValidateLineaHost(string host)
        {
            throw new NotImplementedException();
        }

        public BIEN GetTorreByPC(string devicePC)
        {
            throw new NotImplementedException();
        }

        public string GetLoteCaja(int numCaja)
        {
            throw new NotImplementedException();
        }

        public void SetLoteCaja(int numCaja, string lote)
        {
            throw new NotImplementedException();
        }

        public bool GetBoxHasClosed(int numCaja)
        {
            throw new NotImplementedException();
        }

        public List<BoxesView> GetBoxes(int numOrden)
        {
            throw new NotImplementedException();
        }

        public int AddTestConfigFile(int numTCF, string userName, byte[] data, string description)
        {
            throw new NotImplementedException();
        }

        public byte[] GetTestConfigFile(PRODUCTO producto, string description)
        {
            throw new NotImplementedException();
        }

        public byte[] GetTestConfigFile(int numTCFV)
        {
            throw new NotImplementedException();
        }

        public List<DeviceDataView> GetTestHistory(int numOrden, string idFase)
        {
            throw new NotImplementedException();
        }

        public List<DeviceDataView> GetAllDeviceTestOK(int numOrden, string idFase)
        {
            throw new NotImplementedException();
        }
    }
}
