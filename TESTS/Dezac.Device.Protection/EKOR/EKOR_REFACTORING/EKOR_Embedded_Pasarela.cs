﻿using System;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class EKOR_Embedded_Pasarela : DeviceBase
    {
        private ModbusDeviceTCP Modbus { get; set; }

        public EKOR_Embedded_Pasarela()
        {
        }

        public EKOR_Embedded_Pasarela(string host)
        {
            SetPort(host);
        }

        public void SetPort(string host, int port = 502, ModbusTCPProtocol protocol = ModbusTCPProtocol.TCP)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceTCP(host, port, protocol, _logger);
            else
                Modbus.HostName = host;

            Modbus.TimeOut = 5000;
            Modbus.PerifericNumber = 1;
        }

        public Status ReadStatus()
        {
            LogMethod();
            var status = (StatusEnum)Modbus.ReadHoldingRegister((ushort)Registers.STATUS);           
            _logger.InfoFormat("STATUS : {0}", status);
            return new Status(status);
        }

        public EthernetStatus ReadEth0Status()
        {
            LogMethod();
            var status = (EthernetStatus)Modbus.ReadHoldingRegister((ushort)Registers.ETH0_STATUS);
            _logger.InfoFormat("STATUS : {0}", status);
            return status;
        }

        public EthernetStatus ReadEth1Status()
        {
            LogMethod();
            var status = (EthernetStatus)Modbus.ReadHoldingRegister((ushort)Registers.ETH1_STATUS);
            _logger.InfoFormat("STATUS : {0}", status);
            return status;
        }

        public string ReadMac0()
        {
            LogMethod();
            var mac = Modbus.ReadHexStringHoldingRegister((ushort)Registers.MAC0, 3);
            return mac;
        }

        public string ReadMac1()
        {
            LogMethod();
            var mac = Modbus.ReadHexStringHoldingRegister((ushort)Registers.MAC1, 3);
            return mac;
        }

        public int ReadBastidor()
        {
            LogMethod();
            return Modbus.ReadInt32HoldingRegisters((ushort)Registers.BASTIDOR);
        }

        public string ReadFlashStatus()
        {
            LogMethod();
            var status = (FlashStatus)Modbus.ReadHoldingRegister((ushort)Registers.FLASH_STATUS);
            _logger.InfoFormat("STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public string ReadHwOcotpCustcap()
        {
            LogMethod();
            var status = Modbus.ReadInt32HoldingRegisters((ushort)Registers.HW_OCOTP_CUSTCAP);
            _logger.InfoFormat("OcotpCustcap : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public string ReadHwOcotpLock()
        {
            LogMethod();
            var status = Modbus.ReadInt32HoldingRegisters((ushort)Registers.HW_OCOTP_LOCK);
            _logger.InfoFormat("OcotpLock : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        //*************************************************

        public void ReadResultTest(Tests test)
        {
            LogMethod();
            Modbus.ReadHoldingRegister((ushort)test);
        }

        public void WriteStatus()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.STATUS, (ushort)1);
        }

        public void ExecuteTest(Tests test)
        {
            LogMethod();
            Modbus.Write((ushort)test, (ushort)1);
        }

        #region TEST PLACAS

        [Flags]
        public enum PlacasTtySPStatus
        {
            DETECTED_INTERFACE = 0x01,
            INTERFACE_OPEN = 0x02,
            RX_OK = 0x04,
            RX_KO = 0x08,
            TX_OK = 0x10,
            TX_KO = 0x20
        }

        public string ReadTestPlacaTtySP0Status()
        {
            LogMethod();
            var status = (PlacasTtySPStatus)Modbus.ReadHoldingRegister((ushort)Registers.TTY_SP0__STATUS);
            _logger.InfoFormat("SP0 STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public string ReadTestPlacaTtySP1Status()
        {
            LogMethod();
            var status = (PlacasTtySPStatus)Modbus.ReadHoldingRegister((ushort)Registers.TTY_SP1__STATUS);
            _logger.InfoFormat("SP1 STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public string ReadTestPlacaTtySP2Status()
        {
            LogMethod();
            var status = (PlacasTtySPStatus)Modbus.ReadHoldingRegister((ushort)Registers.TTY_SP2__STATUS);
            _logger.InfoFormat("SP2 STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public string ReadTestPlacaTtySP3Status()
        {
            LogMethod();
            var status = (PlacasTtySPStatus)Modbus.ReadHoldingRegister((ushort)Registers.TTY_SP3__STATUS);
            _logger.InfoFormat("SP3 STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public void WriteMac0(string mac)
        {
            LogMethod();
            Modbus.WriteHexString((ushort)Registers.MAC0, mac);
        }

        public void WriteMac1(string mac)
        {
            LogMethod();
            Modbus.WriteHexString((ushort)Registers.MAC1, mac);
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.BASTIDOR, bastidor);
        }

        public void WriteFlashRecord()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FLASH_STATUS, (ushort)1);
        }

        public void WriteHwOcotpCustcap()
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.HW_OCOTP_CUSTCAP, (ushort)1);
        }

        public void WriteHwOcotpLock()
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.HW_OCOTP_LOCK, (int)1);
        }

        #endregion

        #region TEST EQUIPO FINAL

        [Flags]
        public enum EquipoTtySP_TX_Status
        {
            Sp_OPEN_OK = 0x00,
            TX_OK = 0x01,
            Sp_CLOSE_OK = 0x02,
            Sp_OPEN_ERROR = 0x0E,
            Sp_IS_OPEN_ERROR = 0x0F,
        }

        public string ReadEquipo_TTY_SP0_TX_Status()
        {
            LogMethod();
            var status = (EquipoTtySP_TX_Status)Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_TTY_SP0_TX_STATUS);
            _logger.InfoFormat("SP0 TX STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public void WriteEquipo_TTY_SP0_TX_Status()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_TTY_SP0_TX_STATUS, (ushort)1);
        }

        public string ReadEquipo_TTY_SP3_TX_Status()
        {
            LogMethod();
            var status = (EquipoTtySP_TX_Status)Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_TTY_SP3_TX_STATUS);
            _logger.InfoFormat("SP3 TX STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public string ReadVersionTest()
        {
            LogMethod();
            var mac = Modbus.ReadHoldingRegister((ushort)Registers.VERSION_TEST);
            return mac.ToString("X4").Insert(2,".");
        }
        public void WriteEquipo_TTY_SP3_TX_Status()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_TTY_SP3_TX_STATUS, (ushort)1);
        }

        [Flags]
        public enum EquipoTtySP_RX_Status
        {
            Sp_OPEN_OK = 0x00,
            RX_OK = 0x01,
            DATOS_OK = 0x02,
            Sp_CLOSE_OK = 0x03,

            Sp_RutinaTest_ERROR = 0x0A,
            Sp_Rx_RutinaTest_ERROR = 0x0C,
            Sp_Rx_TimeOut_ERROR = 0x0B,
            Sp_OPEN_ERROR = 0x0E,
            Sp_IS_OPEN_ERROR = 0x0F,

            OK = Sp_OPEN_OK | RX_OK | DATOS_OK,
        }

        public string ReadEquipo_TTY_SP0_RX_Status()
        {
            LogMethod();
            var status = (EquipoTtySP_RX_Status)Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_TTY_SP0_RX_STATUS);
            _logger.InfoFormat("SP0 RX STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public void WriteEquipo_TTY_SP0_RX_Status()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_TTY_SP0_RX_STATUS, (ushort)1);
        }

        public string ReadEquipo_TTY_SP3_RX_Status()
        {
            LogMethod();
            var status = (EquipoTtySP_RX_Status)Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_TTY_SP3_RX_STATUS);
            _logger.InfoFormat("SP3 RX STATUS : {0}", status);
            var value = (int)status;
            return value.ToString("X4");
        }

        public void WriteEquipo_TTY_SP3_RX_Status()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_TTY_SP3_RX_STATUS, (ushort)1);
        }

        public ushort ReadLED_A1_Red()
        {
            LogMethod();
            var status = Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_LED_A1_RED);
            _logger.InfoFormat("LED A1 RED STATUS : {0}", status);
            return status;
        }

        public void WriteLED_A1_Red(bool enabled)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_LED_A1_RED, enabled ? (ushort)1 :(ushort) 0);
        }

        public ushort ReadLED_A1_Green()
        {
            LogMethod();
            var status = Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_LED_A1_GREEN);
            _logger.InfoFormat("LED A1 GREEN STATUS : {0}", status);
            return status;
        }

        public void WriteLED_A1_Green(bool enabled)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_LED_A1_GREEN, enabled ? (ushort)1 : (ushort)0);
        }

        public ushort ReadLED_A2_Red()
        {
            LogMethod();
            var status = Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_LED_A2_RED);
            _logger.InfoFormat("LED A2 RED STATUS : {0}", status);
            return status;
        }

        public ushort ReadLED_A2_Green()
        {
            LogMethod();
            var status = Modbus.ReadHoldingRegister((ushort)Registers.EQUIPO_LED_A2_GREEN);
            _logger.InfoFormat("LED A2 GREEN STATUS : {0}", status);
            return status;
        }

        public void WriteLED_A2_Red(bool enabled)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_LED_A2_RED, enabled ? (ushort)1 : (ushort)0);
        }

        public void WriteLED_A2_Green(bool enabled)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EQUIPO_LED_A2_GREEN, enabled ? (ushort)1 : (ushort)0);
        }

        public void WriteFinalizarEquipo()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RENAME_TEST, (ushort)1);
        }

        #endregion

        public override void Dispose()
        {
            if (Modbus != null)
                Modbus.Dispose();

            base.Dispose();
        }

        [Flags]
        public enum StatusEnum
        {
            TEST_COMUNICATIONS_OK = 0x01,
            ETH0_BUS_MII_OK = 0x02,
            ETH0_LINK_OK = 0x04,
            ETH1_BUS_MII_OK = 0x08,
            ETH1_LINK_OK = 0x10,
            TTY_SP0_TX_OK = 0x20,
            TTY_SP1_TX_OK = 0x40,
            TTY_SP2_TX_OK = 0x80,
            TTY_SP3_TX_OK = 0x100,
            TTY_SP0_RX_OK = 0x200,
            TTY_SP1_RX_OK = 0x400,
            TTY_SP2_RX_OK = 0x800,
            TTY_SP3_RX_OK = 0x1000
        }

        public struct Status
        {
            public bool CommunicationsOK { get; set; }
            public bool Eth0BusMiiOK { get; set; }
            public bool Eth0LinkOK { get; set; }
            public bool Eth1BusMiiOK { get; set; }
            public bool Eth1LinkOK { get; set; }
            public bool TtySP0TxOK { get; set; }
            public bool TtySP1TxOK { get; set; }
            public bool TtySP2TxOK { get; set; }
            public bool TtySP3TxOK { get; set; }
            public bool TtySP0RxOK { get; set; }
            public bool TtySP1RxOK { get; set; }
            public bool TtySP2RxOK { get; set; }
            public bool TtySP3RxOK { get; set; }
            public Status(StatusEnum state)
            {
                CommunicationsOK = state.HasFlag(StatusEnum.TEST_COMUNICATIONS_OK);
                Eth0BusMiiOK = state.HasFlag(StatusEnum.ETH0_BUS_MII_OK);
                Eth0LinkOK = state.HasFlag(StatusEnum.ETH0_LINK_OK);
                Eth1BusMiiOK = state.HasFlag(StatusEnum.ETH1_BUS_MII_OK);
                Eth1LinkOK = state.HasFlag(StatusEnum.ETH1_LINK_OK);
                TtySP0TxOK = state.HasFlag(StatusEnum.TTY_SP0_TX_OK);
                TtySP1TxOK = state.HasFlag(StatusEnum.TTY_SP1_TX_OK);
                TtySP2TxOK = state.HasFlag(StatusEnum.TTY_SP2_TX_OK);
                TtySP3TxOK = state.HasFlag(StatusEnum.TTY_SP3_TX_OK);
                TtySP0RxOK = state.HasFlag(StatusEnum.TTY_SP0_RX_OK);
                TtySP1RxOK = state.HasFlag(StatusEnum.TTY_SP1_RX_OK);
                TtySP2RxOK = state.HasFlag(StatusEnum.TTY_SP2_RX_OK);
                TtySP3RxOK = state.HasFlag(StatusEnum.TTY_SP3_RX_OK);
            }
        }

        [Flags]
        public enum EthernetStatus
        {
            DETECTED_INTERFACE = 0x01,
            DETECTED_IP = 0x02,
            DETECTED_GATEWAY = 0x04,
            COMUNICATION_ACTIVE = 0x08
        }

       

        [Flags]
        public enum FlashStatus
        {
            NODE_FLASH_MTD0 = 0x01,
            NODE_FLASH_MTD1 = 0x02,
            FORMATED_MTD0 = 0x04,
            FORMATED_MTD1 = 0x08,
            RECORD_BLCK0_IMG_LINUX = 0x10,
            NODE_FILE_SYSTEM_UBIFS = 0x20,
            UBIATTACH_DEV_UBI_CTRL = 0x40,
            NODE_FILE_SYSTEM_UBI0 = 0x80,
            NODE_FILE_SYSTEM_UBI0_0 = 0x100,
            VOL_UBIFS = 0x200,
            RECORD_BLCK1_IMG_ROOTFS = 0x400,
            CREATED_DIRECTORY_MNT_UBIFS = 0x800,
            MOUNTED_PARTITION_UBIFS = 0x1000,
            VERIFIED_NAND_FLASH_VERSION = 0x2000,
            UPDATED_FLASH_FILES = 0x4000,
            BUSY = 0x8000
        }

        public enum Tests
        {
            TEST_ETHERNETS = 0X0201,
            TEST_SP0_SP3 = 0X0202,
            TEST_SP0_SP1 = 0X0203,
            TEST_SP3_SP0 = 0X0204,
            TEST_SP3_SP1 = 0X0205,
            TEST_SP1_SP0 = 0X0206,
            TEST_SP1_SP3 = 0X0207,
        }

        public enum Registers
        {
            STATUS = 0X0100,
            ETH0_STATUS = 0X0101,
            ETH1_STATUS = 0X0102,
            TTY_SP0__STATUS = 0X0103,
            TTY_SP1__STATUS = 0X0104,
            TTY_SP2__STATUS = 0X0105,
            TTY_SP3__STATUS = 0X0106,
            MAC0 = 0X0110,
            MAC1 = 0X0111,
            BASTIDOR = 0X0112,
            FLASH_STATUS = 0X0113,
            RENAME_TEST = 0X114,
            HW_OCOTP_CUSTCAP = 0X0115,
            HW_OCOTP_LOCK = 0X0116,

            EQUIPO_TTY_SP0_TX_STATUS = 0X0120,
            EQUIPO_TTY_SP3_TX_STATUS = 0X0122,
            EQUIPO_TTY_SP0_RX_STATUS = 0X0121,
            EQUIPO_TTY_SP3_RX_STATUS = 0X0123,

            EQUIPO_LED_A1_RED = 0X0130,
            EQUIPO_LED_A1_GREEN = 0X0131,
            EQUIPO_LED_A2_RED = 0X0132,
            EQUIPO_LED_A2_GREEN = 0X0133,

            EXIT_TEST = 0X0200,

            VERSION_TEST = 0x1000 
        }
    }
}
