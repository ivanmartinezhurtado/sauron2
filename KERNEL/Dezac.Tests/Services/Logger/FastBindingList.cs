﻿using System.ComponentModel;
using System.Reflection;

namespace Dezac.Tests.Services.Logger
{
    public class FastBindingList<T> : BindingList<T>
    {
        public FastBindingList()
        {
            FieldInfo fi = typeof(BindingList<T>).GetField("raiseItemChangedEvents", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fi != null)
                fi.SetValue(this, false);
        }
    }
}
