namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ORDEN")]
    public partial class ORDEN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDEN()
        {
            HOJAMARCAJE = new HashSet<HOJAMARCAJE>();
            HOJAMARCAJEFASE = new HashSet<HOJAMARCAJEFASE>();
            ORDENCAJA = new HashSet<ORDENCAJA>();
            ORDENPRODUCTO = new HashSet<ORDENPRODUCTO>();
            ORDENPROCESOFASE = new HashSet<ORDENPROCESOFASE>();
            REPROCESOFAB = new HashSet<REPROCESOFAB>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMORDEN { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        public int NUMPRODUCTO { get; set; }

        public int VERSION { get; set; }

        [Required]
        [StringLength(1)]
        public string ABIERTA { get; set; }

        public decimal CANTIDAD { get; set; }

        public decimal? PENDIENTE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDALMACEN { get; set; }

        [StringLength(20)]
        public string UBICACION { get; set; }

        public DateTime FECHAALTA { get; set; }

        public short PRIORIDAD { get; set; }

        public int? NUMPROPUESTA { get; set; }

        [StringLength(10)]
        public string PROC_DBMS { get; set; }

        [StringLength(1)]
        public string PROC_TIPO { get; set; }

        [StringLength(3)]
        public string PROC_SERIE { get; set; }

        public decimal? PROC_NUMERO { get; set; }

        public decimal? PROC_CANTIDAD { get; set; }

        [StringLength(1)]
        public string CONMODIFICACION { get; set; }

        [StringLength(1)]
        public string DETRANSFORMACION { get; set; }

        [StringLength(800)]
        public string NOTASMODIFICACION { get; set; }

        public int? TRAPRODORG { get; set; }

        public int? TRAVERORG { get; set; }

        [StringLength(1)]
        public string TRAPARTESINO { get; set; }

        [StringLength(30)]
        public string USERNAMEALTA { get; set; }

        [StringLength(30)]
        public string USERNAMEMODIFICACION { get; set; }

        public DateTime? FECHACIERRE { get; set; }

        [StringLength(1)]
        public string CONFIRMACIONENTREGA { get; set; }

        [StringLength(1)]
        public string CONFIRMACIONENTREGAIE { get; set; }

        [Required]
        [StringLength(1)]
        public string FABRICARPORFASES { get; set; }

        [Required]
        [StringLength(1)]
        public string REFERENCIATIEMPO { get; set; }

        [StringLength(500)]
        public string NOTASIDP { get; set; }

        public int? NROSERIE { get; set; }

        [Required]
        [StringLength(1)]
        public string PRODUCCIONSINO { get; set; }

        [StringLength(1)]
        public string PROTOTIPOSINO { get; set; }

        [Required]
        [StringLength(1)]
        public string BLOQUEOSINO { get; set; }

        [StringLength(500)]
        public string NOTASBLOQUEO { get; set; }

        [Required]
        [StringLength(1)]
        public string PRESERIESINO { get; set; }

        public int? NUMPROYECTO { get; set; }

        public int? NUMFASE { get; set; }

        public int? NUMTAREA { get; set; }

        [Required]
        [StringLength(1)]
        public string REPROCESO { get; set; }

        [StringLength(10)]
        public string IDTIPO { get; set; }

        [Required]
        [StringLength(1)]
        public string STOCKSINO { get; set; }

        [StringLength(10)]
        public string IDLINEAENTREGA { get; set; }

        public int? NUMASISTENCIA { get; set; }

        [StringLength(1)]
        public string MRP_CODI { get; set; }

        [StringLength(1)]
        public string BLOQUEOMARCAJE { get; set; }

        [StringLength(1)]
        public string NIVELCONTROLTEST { get; set; }

        [StringLength(1)]
        public string EXTERNOSINO { get; set; }

        [StringLength(1)]
        public string CALCULARINDICADOR { get; set; }

        [StringLength(20)]
        public string UBICACION2 { get; set; }

        [StringLength(1)]
        public string ENSAYO { get; set; }

        [StringLength(1)]
        public string PROSERIESINO { get; set; }

        [StringLength(1)]
        public string MANTENERNROSERIE { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HOJAMARCAJE> HOJAMARCAJE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HOJAMARCAJEFASE> HOJAMARCAJEFASE { get; set; }

        public virtual PROYECTO PROYECTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENCAJA> ORDENCAJA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENPRODUCTO> ORDENPRODUCTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENPROCESOFASE> ORDENPROCESOFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPROCESOFAB> REPROCESOFAB { get; set; }
    }
}
