﻿using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.04)]
    public class TBMDispenserTestPlacas : TestBase
    {
        private TowerBoard tower;
        private CirwattDlms tbm;
        private const int PILOTO_ERROR = 20;

        internal CirwattDlms Meter
        {
            get
            {
                if (tbm == null)
                {
                    tbm = AddInstanceVar(new CirwattDlms(), "TBMDispenserCirwattDlms");
                    Resultado.Set("VERSION_DEVICE2", string.Format("{0}: {1}", Meter.GetType().Name, Meter.GetDeviceVersion()), ParamUnidad.SinUnidad);
                }

                return tbm;
            }
        }

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }              
            tower.Active24VDC();
          
            tower.IO.DO.Off(PILOTO_ERROR);
        }

        public void TestCommunications(int comPortTTL = 0, int comPortRS485 = 0)
        {
            if (comPortTTL == 0)
                comPortTTL = Comunicaciones.SerialPort;

            tower.Chroma.SetVoltageRange(Chroma.VoltageRangeEnum.HIGH);

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            Meter.SetPort(comPortTTL, 115200);

            logger.InfoFormat("New TBMDispenserTestPlacas by  COM PORT:{0}", comPortTTL);

            Meter.Retries = 3;

            SamplerWithCancel((p) => { Meter.IniciarSesion(); return true; }, "Error en las comunicaciones DLMS con el equipo por el puerto OPTICO (TTL)", 2, 500);

            var firmwareVersion = Meter.LeerVersionSW();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de firmware incorrecta"), ParamUnidad.SinUnidad);

            Meter.CerrarSesion();

            if (comPortRS485 == 0)
                comPortRS485 = Comunicaciones.SerialPort2;

            Meter.SetPort(comPortRS485, 115200);

            logger.InfoFormat("TBMDispenserTestPlacas by  COM PORT:{0}", comPortRS485);

            SamplerWithCancel((p) => { Meter.IniciarSesion(); return true; }, "Error en las comunicaciones DLMS con el equipo por el puerto OPTICO (TTL)", 2, 500);

            Resultado.Set("COM.RS485", "OK", ParamUnidad.SinUnidad);

            Meter.EscribirEstadoFabricacion(799);

            Meter.EscribirNumeroFabricacion((uint)TestInfo.NumBastidor.Value);

            Meter.EscribirModelo(Identificacion.MODELO);

            var modelo = Meter.LeerModelo();

            Assert.AreEqual(ConstantsParameters.Identification.MODELO, Identificacion.MODELO, modelo, Error().SOFTWARE.BBDD.VERSION_INCORRECTA("Error el modelo del equipo no corresponde con el de la BBDD"), ParamUnidad.SinUnidad);
        }

        public void TestMedida()
        {
            var defsTension = new AdjustValueDef(Params.V.Null.Verificacion.Name, 0, Params.V.Null.Verificacion.Min(), Params.V.Null.Verificacion.Max(), 0, 0, ParamUnidad.V);

            TestMeasureBase(defsTension,
              (step) =>
              {
                  var measure = Meter.ReadVoltagePoints(CirwattDlms.enumFases.MONOFASICO);
                  return measure[0];
              }, 2, 10, 200);

            var defsPotenciaActiva = new AdjustValueDef(Params.POINTS.Other("KW").Verificacion.Name, 0, Params.POINTS.Other("KW").Verificacion.Min(), Params.POINTS.Other("KW").Verificacion.Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(defsPotenciaActiva,
              (step) =>
              {
                  var  puntosActiva = tbm.ReadPowerPoints(CirwattDlms.enumFases.MONOFASICO, TipoPotencia.Activa);
                  return puntosActiva[0];
              
              }, 2, 10, 200);
        }

        public void TestCustomization()
        {
            Meter.EscribirEstadoFabricacion(700);

            TestInfo.NumSerie= TestInfo.NumBastidor.Value.ToString();

            Meter.EscribirNumeroSerie(Convert.ToUInt32(TestInfo.NumSerie));

            tbm.CerrarSesion();
        }

        public void TestFinish()
        {
            if (tbm != null)
                tbm.CerrarSesion();

            if (tower != null)
            {
                tower.ShutdownSources();

                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(21, 7);

                    if (String.IsNullOrEmpty(TestInfo.NumSerie))
                        tower.IO.DO.On(PILOTO_ERROR);

                    Delay(1000, "Espera de equipo malo");             
                }
                tower.Dispose();
            }
        }
    }

}
