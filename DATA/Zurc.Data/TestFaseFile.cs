namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TestFaseFile")]
    public partial class TestFaseFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TestFase")]
        public int TestFaseId { get; set; }

        [StringLength(10)]
        public string TipoDocId { get; set; }
        public byte[] Fichero { get; set; }
        [StringLength(100)]
        public string Nombre { get; set; }

        public virtual TestFase TestFase { get; set; }
    }
}
