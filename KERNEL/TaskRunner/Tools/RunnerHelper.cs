﻿using System;
using System.Threading;
using TaskRunner.Model;

namespace TaskRunner.Tools
{
    public static class RunnerHelper
    {
        private static readonly object _syncObject = new Object();
        private static object _waitLastTestAndRunOnlyOnceObj = new object();

        private static SharedVariablesList SharedVariables => SequenceContext.Current.SharedVariables;

        public static bool IsInstanceRunning(int numInstance)
        {
            return SequenceContext.Current.SharedVariables.Get<bool>("_INSTANCE_RUNNING_" + numInstance, false);
        }

        #region Task
        private static void WaitAllTasks(int numTasksToWait, string signal, int timeOut = 60000)
        {
            string key = "_signal_" + signal;

            int numSignals = 0;

            long finalValue = Environment.TickCount + timeOut;

            lock (_syncObject)
            {
                SharedVariables.AddOrUpdate(key, SharedVariables.Get<int>(key, 0) + 1);
            }
            do
            {
                numSignals = SharedVariables.Get<int>(key, 0);

                // Detect for a timeout			
                if (numSignals < numTasksToWait && Environment.TickCount > finalValue)
                    throw new Exception("PARALELISMO CANCELADO");
            } while (numSignals < numTasksToWait);
        }
        public static bool WaitAllTasksAndRunOnlyOnce(int numTasksToWait, string signal, Action action)
        {
            string key = "_signal_" + signal;
            int result = 0;

            WaitAllTasks(numTasksToWait, signal + "_BLOCKER_START_");

            lock (_syncObject)
            {
                result = SharedVariables.Get<int>(key, 0) + 1;

                SharedVariables.AddOrUpdate(key, result);

                if (result == 1)
                    action();
            }

            WaitAllTasks(numTasksToWait, signal + "_BLOCKER_END_");

            return result == 1;
        }
        public static T WaitAllTasksAndRunOnlyOnce<T>(int numTasksToWait, string signal, Func<T> action, T varType)
        {
            string key = "_signal_" + signal;
            int result = 0;

            WaitAllTasks(numTasksToWait, signal + "_BLOCKER_START_");

            lock (_syncObject)
            {
                result = SharedVariables.Get<int>(key, 0) + 1;

                SharedVariables.AddOrUpdate(key, result);

                if (result == 1)
                {
                    T value = action();
                    SharedVariables.AddOrUpdate(key + "_value", value);
                }
            }

            WaitAllTasks(numTasksToWait, signal + "_BLOCKER_END_");

            T resultValor = SharedVariables.Get<T>(key + "_value", varType);

            return ChangeType<T>(resultValor);
        }
        #endregion

        #region Test
        public static AggregateException WaitAllTestsAndRunOnlyOnce(string signal, Action action)
        {
            WaitAllTests(signal + "_BLOCKER_START_");
            return RunOnlyOnce(signal, action);
        }
        public static void WaitAllTests(string signal)
        {
            string key = "_signal_" + signal;

            int runningInstances = 0;
            int numSignals = 0;

            lock (_syncObject)
            {
                SharedVariables.AddOrUpdate(key, SharedVariables.Get<int>(key, 0) + 1);
            }
            do
            {
                if (numSignals < runningInstances)
                    Thread.Sleep(200);

                runningInstances = SequenceContext.Current.RunningInstances;

                numSignals = SharedVariables.Get<int>(key, 0);

            } while (numSignals < runningInstances);
        }
        public static AggregateException RunOnlyOnce(string signal, Action action)
        {
            string key = "_signal_" + signal;

            lock (_syncObject)
            {
                int result = SharedVariables.Get<int>(key, 0) + 1;

                SharedVariables.AddOrUpdate(key, result);

                if (result == 1)
                    try
                    {
                        action();
                    }
                    catch (AggregateException ex)
                    {
                        SequenceContext.Current.SharedVariables.AddOrUpdate(key + "_STEP_WAIT_ALL_END_EXCEPTION_", ex);
                    }
            }

            return SharedVariables.Get<AggregateException>(key + "_STEP_WAIT_ALL_END_EXCEPTION_", (AggregateException)null);
        }
        public static bool RunOnlyLastTest(string signal, Action action)
        {
            bool result = false;

            string key = "_signal_" + signal;

            lock (_waitLastTestAndRunOnlyOnceObj)
            {
                int count = SharedVariables.Get<int>(key, 0) + 1;

                SharedVariables.AddOrUpdate(key, count);

                if (count == SequenceContext.Current.TotalNumInstances)
                {
                    action();
                    result = true;
                }
            }

            return result;
        }
        public static void RunTestSequentiallyOrdered(string signal, Action action)
        {
            SequenceContext context = SequenceContext.Current;

            string key = "_signal_" + signal;
            int numInstance = context.NumInstance;
            int maxInstances = context.TotalNumInstances;
            bool done = false;

            WaitAllTests(signal + "_BLOCKER_START_");

            int firstInstance = 1;
            while (firstInstance <= maxInstances && !IsInstanceRunning(firstInstance))
                firstInstance++;

            while (!done)
            {
                lock (_syncObject)
                {
                    int next = SharedVariables.Get<int>(key, firstInstance);
                    if (next == numInstance)
                        try
                        {
                            action();
                        }

                        finally
                        {
                            next++;
                            while (next <= maxInstances && !IsInstanceRunning(next))
                                next++;

                            context.SharedVariables.AddOrUpdate(key, next);
                            done = true;
                        }
                }
                if (!done)
                    System.Threading.Thread.Sleep(1000);
            }
        }
        #endregion

        public static T ChangeType<T>(object value)
        {
            Type t = typeof(T);
            Type type = Nullable.GetUnderlyingType(t) ?? t;

            return (value == null || DBNull.Value.Equals(value)) ? default(T) : (T)Convert.ChangeType(value, type);
        }

        public static object ChangeType(object value, Type t)
        {
            Type type = Nullable.GetUnderlyingType(t) ?? t;

            return (value == null || DBNull.Value.Equals(value)) ? null : Convert.ChangeType(value, type);
        }

    }
}
