namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PRODUCTOFASE")]
    public partial class PRODUCTOFASE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPRODUCTO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VERSION { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SECUENCIA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDFASE { get; set; }

        public decimal? TIEMPOTEORICO { get; set; }

        public decimal? TIEMPOSTANDARD { get; set; }

        [StringLength(1600)]
        public string NOTAS { get; set; }

        public decimal? TIEMPOPREPARACION { get; set; }

        public int? OPERARIOS { get; set; }

        [Required]
        [StringLength(1)]
        public string FASETEST { get; set; }

        public decimal? TACKTIME { get; set; }

        [StringLength(3)]
        public string CODIGOGRUPO { get; set; }

        public int? SECUENCIAPADRE { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }
    }
}
