﻿using Autofac;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Services;
using System.Configuration;
using TaskRunner.Model;
using WinTestPlayer.Views;

namespace WinTestPlayer.Services
{
    public class DI
    {
        public static IContainer Container { get; private set; }

        public static IContainer Build()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<DezacContext>();
            builder.RegisterType<SequenceContext>();

            string offline = ConfigurationManager.AppSettings["Offline"];
            if (offline != "1")
                builder.RegisterType<DezacService>().As<IDezacService>();
            else
                builder.RegisterType<NoDbDezacService>().As<IDezacService>();

            builder.RegisterType<ReprocesosService>().As<IReprocesosService>();

            builder.RegisterType<RunnerService>().SingleInstance();
            builder.RegisterType<ReportsService>().SingleInstance();
            builder.RegisterType<ShellService>().SingleInstance();
            builder.RegisterType<AppService>().SingleInstance();

            builder.RegisterType<Header>().SingleInstance();
            builder.RegisterType<TestView>().SingleInstance();
            builder.RegisterType<ParamsView>();
            builder.RegisterType<HistorialView>();
            builder.RegisterType<ProductsView>();
            builder.RegisterType<AsistenciaTecnicaView>();
            //builder.RegisterType<TestReportParamsView>();
            builder.RegisterType<MainForm>().SingleInstance();
            builder.RegisterType<CacheService>().As<ICacheService>().SingleInstance();

            Container = builder.Build();

            return Container;
        }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
