﻿using System;
using System.Globalization;

namespace Comunications.Message
{
    public class PTEMessage : StringMessage
    {
        private readonly char COMMAND_SEPARATOR = ';';
        private static CultureInfo enUS = new CultureInfo("en-US");

        public static PTEMessage Create(string format, params object[] args)
        {
            PTEMessage message = new PTEMessage { Format = format };

            message.SetArgs(enUS, args);

            return message;
        }

        public int NumCommands
        {
            get { return this.Text.Split(COMMAND_SEPARATOR).Length; }
        }

        public string[] Commands
        {
            get
            {
                return this.Text.Split(COMMAND_SEPARATOR);
            }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            if (Validator != null)
            {
                if (!Validator((StringMessage)response))
                    throw new FormatException("Error, formato de la trama de respuesta del PTE Messsage incorrecta");

                return;
            }

            string[] commands = Commands;
            string[] results = ((PTEMessage)response).Commands;

            if (commands.Length != results.Length)
                throw new FormatException("Error, longitud de la trama de respuesta del MTE Messsage incorrecta");

            for (int i = 0; i < results.Length; i++)
            {
                string cmd = commands[i];
                bool valid = false;

                valid = cmd + "=O" == results[i];

                if (!valid)
                    throw new Exception(string.Format("Error ejecutando el comando {0} (Resp.: {1})", cmd, results[i]));
            }
        }
    }
}
