namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ARTICULO
    {
        public ARTICULO()
        {
            this.ARTICULOBLOQUEO = new HashSet<ARTICULOBLOQUEO>();
            this.ARTICULOCLIENTE = new HashSet<ARTICULOCLIENTE>();
            this.ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            this.ORDEN = new HashSet<ORDEN>();
            this.PRODUCTO = new HashSet<PRODUCTO>();
            this.PRODUCTOCOMPONENTE = new HashSet<PRODUCTOCOMPONENTE>();
            this.PROYECTO1 = new HashSet<PROYECTO>();
        }
    
        public int NUMARTICULO { get; set; }
        public string CLACODIGO { get; set; }
        public string CLADESC { get; set; }
        public string DESCRIPCION { get; set; }
        public string CODIGOBCN { get; set; }
        public string DESCBCN { get; set; }
        public string CODIGOTERRASSA { get; set; }
        public string DESCTERRASSA { get; set; }
        public string IDTIPOARTICULO { get; set; }
        public Nullable<System.DateTime> FECHAALTA { get; set; }
        public string IDIVA { get; set; }
        public string IDPROVEEDOR { get; set; }
        public string IDMARCA { get; set; }
        public Nullable<int> NUMXREF { get; set; }
        public string IDALMACENCALIDAD { get; set; }
        public string CUENTACOMPRA { get; set; }
        public string CUENTAVENTA { get; set; }
        public string CUENTAFABRICACION { get; set; }
        public string EXTXREF { get; set; }
        public string INVENTARIABLE { get; set; }
        public decimal EXISTENCIA { get; set; }
        public decimal ORDENADO { get; set; }
        public decimal RESERVADO { get; set; }
        public decimal DISPONIBLE { get; set; }
        public string CONTROLCALIDAD { get; set; }
        public string CARGABLE { get; set; }
        public Nullable<decimal> PRECIOSTANDARD { get; set; }
        public Nullable<decimal> PRECIOMEDIO { get; set; }
        public Nullable<decimal> PRECIOULTIMO { get; set; }
        public Nullable<decimal> PRECIOTEORICO { get; set; }
        public Nullable<decimal> PRECIOVENTA { get; set; }
        public string ACTIVO { get; set; }
        public string OBSERVING { get; set; }
        public string OBSERVCOMPRA { get; set; }
        public string OBSERVVENTA { get; set; }
        public string OBSERVALMACEN { get; set; }
        public string CODCLAPROV { get; set; }
        public string UDSPEDMODREC_SN { get; set; }
        public string DOCUMENTABLE { get; set; }
        public string PREVISIONABLEVTA { get; set; }
        public string DEPOSITOCLIENTE { get; set; }
        public decimal EXISTENCIADEPOSITOCLIENTE { get; set; }
        public Nullable<int> NUMPROYECTO { get; set; }
        public string IDFAMILIAPRODUCCION { get; set; }
        public string ROHS { get; set; }
        public Nullable<decimal> PESO { get; set; }
        public Nullable<int> LEADTIME { get; set; }
        public Nullable<int> UDSEMBALAJE { get; set; }
        public Nullable<int> LOTEOPTIMO { get; set; }
        public string CRITICO { get; set; }
        public string TIPOABC_MA { get; set; }
        public Nullable<int> DESTINOSFINALACTIVOS { get; set; }
        public string FICHEROIMAGEN { get; set; }
        public Nullable<int> IMAGEWIDTH { get; set; }
        public Nullable<int> IMAGEHEIGHT { get; set; }
        public string AVISOPPC { get; set; }
        public string RETIRASTOCKBULTO { get; set; }
        public string NOTACMC { get; set; }
    
        public virtual ICollection<ARTICULOBLOQUEO> ARTICULOBLOQUEO { get; set; }
        public virtual ICollection<ARTICULOCLIENTE> ARTICULOCLIENTE { get; set; }
        public virtual PROYECTO PROYECTO { get; set; }
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }
        public virtual ICollection<ORDEN> ORDEN { get; set; }
        public virtual ICollection<PRODUCTO> PRODUCTO { get; set; }
        public virtual ICollection<PRODUCTOCOMPONENTE> PRODUCTOCOMPONENTE { get; set; }
        public virtual ICollection<PROYECTO> PROYECTO1 { get; set; }
    }
}
