﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    /// <summary>
    /// Scp client to download file to the server Scp. 
    /// </summary>
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(ScpClientDownLoadFileDescription))]
    public class ScpClientDownloadFile : ActionBase
    {    
        /// <summary>
        /// Path of file to download.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Ruta en el servidor del fichero a descargar")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable DownloadFile { get; set; } = new ActionVariable();
        /// <summary>
        /// Destination path of downloaded file.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Ruta donde se descargará el fichero")]
        public ActionVariable DestinationFile { get; set; } = new ActionVariable();

        /// <summary>
        /// Instance of object SCPClient.
        /// </summary>
        [DefaultValue("SCPClient")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SCPClient")]
        public string DeviceName { get; set; }

        /// <summary>
        /// Execute the action.
        /// </summary>
        public override void Execute()
        {
            var scp = GetVariable(DeviceName) as SCPClient;
            if (scp == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("ScpClient").Throw();

            var downloadFile = DownloadFile.Value.ToString();
            var destinationFile = DestinationFile.Value.ToString();

            try
            {
                scp.DownLoad(downloadFile, destinationFile);
            }
            catch (Exception e)
            {
                Error().UUT.COMUNICACIONES.ERROR_DOWNLOAD(e.Message).Throw();
            }
        }
    }

    class ScpClientDownLoadFileDescription : ActionDescription
    {
        public override string Name { get { return "SCPClientDownloadFile"; } }
        public override string Category { get { return "Tools/Ethernet/SCP"; } }
        public override string Description { get { return "Descarga un fichero de un servidor mediante el protocolo SCP"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
