﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(CalculateAverageDescription))]
    
    public class CalculateAverage : ActionBase
    {

        [Description("Nombres de las variables a calcular la media")]
        [Category("Configuración")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string VariablesList { get; set; }

        [Description("Nombre de la variable resultado")]
        [Category("Resultados")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string VariableResult { get; set; }

        [Description("Unidad de la variable resultado")]
        [Category("Resultados")]
        public ParamUnidad Unidad { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            List<double> valuesList = new List<double>();

            var variables = VariablesList.Replace(" ","\n").Replace("\r", "").TrimEnd('\n').Split('\n');

            foreach (var variable in variables)
            {
                double value = 0;

                try
                {
                    value = Convert.ToDouble(VariablesTools.ResolveValue(variable));
                }
                catch (FormatException)
                {
                    Error().PROCESO.ACTION_EXECUTE.VALOR_INCORRECTO(string.Format("VARIABLE {0} NO NUMERICA", variable)).Throw();
                }

                valuesList.Add(value);
            }
            var average = valuesList.Average();

            if (!string.IsNullOrEmpty(VariableResult))
            {
                Logger.InfoFormat("{0} = {2}", VariableResult, average.ToString());
                Context.ResultList.Add(string.Format("{0} -> {1}", VariableResult, average));

                testBase.Resultado.Set(VariableResult, average, Unidad);

                SetVariable(VariableResult, average);
            }
        }
    }

    public class CalculateAverageDescription : ActionDescription
    {
        public override string Name { get { return "CalculateAverage"; } }
        public override string Description { get { return "Calcula la media aritmética de la lista de variables de entrada"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Math; } }
    }
}
