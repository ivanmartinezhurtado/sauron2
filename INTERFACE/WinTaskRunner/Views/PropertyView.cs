﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WinTaskRunner.Services;
using TaskRunner.Model;
using Dezac.Tests.Actions;

namespace WinTaskRunner.Views
{
    public partial class PropertyView : UserControl
    {
        private Dictionary<Type, ActionDescription> cache = new Dictionary<Type, ActionDescription>();

        protected readonly EventBus eventBus;

        public PropertyView()
        {
            InitializeComponent();
        }

        public PropertyView(EventBus eventBus)
            : this()
        {
            this.eventBus = eventBus;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            eventBus.Register("ShowObjectProperties", OnShowObject);
        }

        private void OnShowObject(object sender, string e, object a)
        {
            TaskRunner.Tools.IActionBase item = a != null && a is Step ? ((Step)a).Action : null;

            actionProps.SelectedObject = item;

            if (item != null)
            {
                Type type = item.GetType();

                if (!cache.TryGetValue(type, out ActionDescription help))
                {
                    DesignerActionAttribute designType = type.GetCustomAttributes(typeof(DesignerActionAttribute), true)
                        .Cast<DesignerActionAttribute>()
                        .FirstOrDefault();

                    help = Activator.CreateInstance(designType.Type) as ActionDescription;
                    cache.Add(type, help);
                }

                lblTitle.Text = help.Name;
                lblDescription.Text = help.Description;
                picture.Image = help.Icon;

                splitContainer1.Panel1Collapsed = false;
                splitContainer1.Panel1.Show();
            }
            else
            {
                splitContainer1.Panel1Collapsed = true;
                splitContainer1.Panel1.Hide();
            }
        }
    }
}
