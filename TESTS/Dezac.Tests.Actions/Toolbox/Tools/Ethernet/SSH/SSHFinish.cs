﻿using Comunications.Ethernet;
using log4net;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(SSHFinishActionDescription))]
    public class SSHFinish : ActionBase
    {
        [DefaultValue("SSH_Device")]
        [Category("3. Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            var ssh = GetVariable(DeviceName.Replace('-', '_')) as SSHClient;

            if (ssh != null)
                ssh.Dispose();
        }

        public class SSHFinishActionDescription : ActionDescription
        {
            public override string Name { get { return "SSHFinish"; } }
            public override string Description { get { return "Desconecta y cierra una instancia de la sesión SSH"; } }
            public override string Category { get { return "Tools/Ethernet/SSH"; } }
            public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        }
    }
}
