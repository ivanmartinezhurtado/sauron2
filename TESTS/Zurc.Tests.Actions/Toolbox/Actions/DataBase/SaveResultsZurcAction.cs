﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Zurc.Data;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.26)]
    [DesignerAction(Type = typeof(SaveResultsZurcActionDescription))]
    //[Browsable(true)]
    public class SaveResultsZurcAction : ActionBase
    {
        public override void Execute()
        {
            internalExcute(null);
        }

        public void Execute(string fromStep = null)
        { 
            internalExcute(fromStep);       
        }

        public void internalExcute(string fromStep = null)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            try
            {
                Logger.Debug("Iniciamos Action -> SaveResultsAction");

                if (data == null)
                    throw new Exception("No se pueden guardar datos de un test sin datos de contexto");

                if (!data.TestInfo.NumBastidor.HasValue)
                    throw new Exception("TEST SIN BASTIDOR");

                if (context.ExecutionResult == TestExecutionResult.Aborted)
                    return;

                if (context.GetSteps(StepExecutionResult.Failed).Count() == 1)
                {
                    var messageEror = context.GetSteps(StepExecutionResult.Failed).FirstOrDefault().GetErrorMessage();

                    Logger.WarnFormat("Tenemos un messageError {0}", messageEror);

                    if (messageEror.Contains("PARALELISMO CANCELADO"))
                        return;

                    if (messageEror.Contains("sin ninguna reparación!"))
                        return;
                }

                int numBastidor = (int)data.TestInfo.NumBastidor.Value;
                string resultado = context.ExecutionResult == TestExecutionResult.Completed ? "O" : "E";

                SaveParameters(data);

                data.TestInfo.SaveBBDD = true;

                using (var svc = new ZurcService())
                {
                    if (!data.TestInfo.NumFabricacion.HasValue)
                        data.TestInfo.NumFabricacion = null;

                    Logger.DebugFormat("Buscamos si existe un Test para {0}", numBastidor);
                    Test test = svc.GetTest(data.NumOrden, numBastidor);

                    if (test == null || data.modoPlayer == ModoTest.SAT)
                    {
                        Logger.DebugFormat("Creamos un nuevo Test para {0}", numBastidor);
                        test = new Test
                        {
                            OrdenId = data.NumOrden,
                            Lote = data.Lote,
                            MatriculaId = numBastidor,
                            FechaAlta = data.TestInfo.StartTime,
                            ArticuloId = data.NumProducto,
                            Version = data.Version,
                            Resultado = resultado,
                            FamiliaId = data.NumFamilia,
                            ReportVersion = data.TestInfo.ReportVersion
                        };
                    }

                    if (resultado == "O")
                    {
                        test.Resultado = "O";

                        if (data.TestInfo.NumSerie != null)
                            test.NumSerie = data.TestInfo.NumSerie;

                        test.MatriculaId = numBastidor;
                    }
                    else
                    {
                        test.Resultado = "E";

                        Logger.DebugFormat("Test ERROR con NUMMATRICULA {0} y NROSERIE {1}", numBastidor, test.NumSerie);

                        if (data.modoPlayer != ModoTest.TIC && data.TestInfo.NumSerieAssigned && !data.MantenerNumSerie)
                        {
                            Logger.DebugFormat("Test ERROR se borral el numero de serie");
                            test.NumSerie = null;
                        }
                    }

                    var fase = new TestFase
                    {
                        Test = test,
                        FaseId = data.IdFase,
                        EmpleadoId = data.IdOperario,
                        Fecha = data.TestInfo.StartTime,
                        Duracion = Convert.ToInt32(data.TestInfo.Elapsed.TotalSeconds),
                        Resultado = resultado,
                        NumCaja = data.NumCajaActual,
                        EquipoId = data.PC,
                        Valores = new List<TestValor>(),
                        Errores = new List<TestError>(),
                        Steps = new List<TestStep>()
                    };

                    var steps = fromStep == null ?
                        // Seleccionamos todos los pasos sin hijos y que no sean hijos de una nueva fase
                        context.GetSteps(p => p.Step.Steps.Count == 0 || p.Step.Action is NewTestFaseContextAction, p => !(p.Step.Action is NewTestFaseContextAction)) :
                        // Seleccionamos todos los pasos sin hijos del paso pasado por parámetro sin incluir pasos de nueva fase
                        context.GetSteps(fromStep, p => p.Step.Steps.Count == 0);

                    var faultedSteps = steps.Where(p => p.Exception != null);

                    foreach (var faultedStep in faultedSteps)
                    {
                        List<Exception> innerExceptions;

                        if (faultedStep.Exception is AggregateException)
                            innerExceptions = (faultedStep.Exception as AggregateException).InnerExceptions.ToList();
                        else
                            innerExceptions = new List<Exception>() { faultedStep.Exception };

                        foreach (Exception e in innerExceptions)
                        {
                            var sauronExc = e as SauronException;

                            var errMsg = e.Message.Left(4000);

                            Logger.WarnFormat("Creamos nuevos T_TESTERROR con msg: {0}", errMsg);

                            if (!errMsg.Contains("PARALELISMO CANCELADO"))
                                fase.Errores.Add(new TestError
                                {
                                    PuntoError = faultedStep.Step.Name,
                                    Extension = errMsg,
                                    NumError = 160,
                                    ErrorCode = sauronExc != null ? sauronExc.CodeError : null
                                });
                        }
                    }

                    data.Resultados.ForEach(p =>
                    {
                        if (p.Modified != null)
                            fase.Valores.Add(new TestValor { ParametroId = p.IdParam, Valor = p.Valor, ValMax = p.Max.ToDecimal(), ValMin = p.Min.ToDecimal(), ValorEsperado = p.ValorEsperado, StepName = p.StepName, UnidadId = (int)p.IdUnidad });
                    });

                    foreach (var step in steps)
                        fase.Steps.Add(new TestStep
                        {
                            Nombre = step.Name,
                            FechaInicio = step.StartTime,
                            FechaFin = step.EndTime == DateTime.MinValue ? DateTime.Now : step.EndTime,
                            Resultado = step.ExecutionResult != StepExecutionResult.Failed ? "O" : "E",
                            Excepcion = step.Exception == null ? null : step.GetErrorMessage().Left(500),
                            Grupo = step.Step.ReportGroupName
                        });

                    svc.GrabarDatos(fase);

                    Logger.InfoFormat("SaveResult OK => Nº TEST: {0}, Nº FASE: {1}, Nº ORDEN: {2}, Nº RESULTS: {3}",
                        test.Id, fase.Id, test.OrdenId, fase.Valores.Count);

                    data.TestInfo.NumTest = test.Id;
                    data.TestInfo.NumTestFase = fase.Id;
                }
            }
            catch (Exception ex)
            {
                var InnerException = ex.InnerException;

                Logger.Error(string.Format("Error in SaveResults: {0}", ex.Message), ex);

                if (ex.Message.Contains("TEST SIN BASTIDOR"))
                    return;

                if (data != null)
                {
                    string errorMessage = "";
                    string errorStep = "";
                    if (context != null)
                    {
                        var steps = fromStep == null ?
                        context.GetSteps(p => p.Step.Steps.Count == 0 || p.Step.Action is NewTestFaseContextAction, p => !(p.Step.Action is NewTestFaseContextAction)) :
                        context.GetSteps(fromStep, p => p.Step.Steps.Count == 0);

                        var faultedStep = steps.Where(p => p.Exception != null).FirstOrDefault();
                        errorMessage = faultedStep != null ? faultedStep.GetErrorMessage().Left(200) : "";
                        errorStep = faultedStep != null ? faultedStep.Step.Name.Left(50) : "";

                        Logger.WarnFormat("Excepción con Context  errorMessage: {0}  errorStep:{1}", errorMessage, errorStep);
                    }

                    var body = new StringBuilder();
                    body.AppendLine("SaveResultsAction Failed");
                    body.AppendLine("----------------------------");
                    body.AppendLine(" ");
                    body.AppendLine(string.Format("TEST FAMILIA: {0}", data.DescFamilia));
                    body.AppendLine(" ");
                    body.AppendLine(string.Format("PC: {0} TORRE:{1}", data.PC, data.LocationTest));
                    body.AppendLine(" ");
                    body.AppendLine(string.Format("ORDEN:{0} PRODUCTO:{1} VERSION:{2}", data.NumOrden, data.NumProducto, data.Version));
                    body.AppendLine(" ");
                    body.AppendLine(string.Format("EXCEPCION: {0} ", ex.Message));

                    if (InnerException != null)
                    {
                        do
                        {
                            var innerTemp = InnerException.InnerException;
                            if (innerTemp != null)
                                InnerException = innerTemp;
                            else
                                break;
                        } while (true);

                        body.AppendLine(" ");
                        body.AppendLine(string.Format("INNER EXCEPCION: {0} ", InnerException.GetInnerMessage()));
                    }
                    else
                        InnerException = ex;

                    body.AppendLine(" ");
                    if (!string.IsNullOrEmpty(errorMessage))
                        body.AppendLine(string.Format("TEST ERROR: {0}. EN STEP: {1}", errorMessage, errorStep));

                    body.AppendLine(" ");
                    body.AppendLine(" ");

                    Logger.WarnFormat("Enviamos correo con Body {0}", body.ToString());


                    LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, true, null, data, false, Path.Combine(Path.GetTempPath(), string.Format("Log{0}.html", context.NumInstance)));

                    SendMail.Send("Error --->  SaveResultsAction", body.ToString(), false, false, true, false, Path.Combine(Path.GetTempPath(), string.Format("Log{0}.html", context.NumInstance)));

                }

                Logger.WarnFormat("Lanzamos la excepción");

                throw InnerException;
            }
        }

        private void SaveParameters(ITestContext data)
        {
            if (!data.NumFamilia.HasValue || string.IsNullOrEmpty(data.IdFase))
                return;

            using (var svc = new ZurcService())
            {
                SaveParameters(svc, data, data.Comunicaciones, "Com", 6, 1);
                SaveParameters(svc, data, data.Consignas, "Consig", 7, 18);
                SaveParameters(svc, data, data.Configuracion, "Config", 1, 6);
                SaveParameters(svc, data, data.Identificacion, "Ident", 5, 7);
                SaveParameters(svc, data, data.Margenes, "Marg", 2, 4);
                SaveParameters(svc, data, data.Resultados, "Result", 8, 19);
                SaveParameters(svc, data, data.Parametrizacion, "Param", 16, 23);
                SaveParameters(svc, data, data.VectorHardware, "VectorHardw", 17, 25);
                SaveParameters(svc, data, data.Vision, "Vision", 18, 26);
            }
        }

        private void SaveParameters(ZurcService svc, ITestContext data, ParamValueCollection list, string alias, int numTipoGrupo, int numTipoParam)
        {
            if (list == null)
                return;

            Logger.DebugFormat("Entramos en SaveParameters tipoGrupo: {0}", numTipoGrupo);

            list.ForEach(p =>
            {
                if (p.IdParam <= 0)
                {
                    Logger.InfoFormat("Creando parámetro: {0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8}",
                        data.NumFamilia.Value, data.IdFase, numTipoGrupo, p.Name, (int)p.IdUnidad, p.GetTipoValorFromUnidad(), numTipoParam, alias, p.Valor);

                    p.IdParam = svc.CrearParametro(data.NumFamilia.Value, data.IdFase, numTipoGrupo, p.Name, (int)p.IdUnidad, p.GetTipoValorFromUnidad(), numTipoParam, alias, p.Valor);
                    data.ReloadParametersOnNextTest = true;
                }
            });
        }
    }

    public class SaveResultsZurcActionDescription : ActionDescription
    {
        public override string Name { get { return "SaveResultsZurc"; } }
        public override string Category { get { return "ZurcTest/Database"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }
}
