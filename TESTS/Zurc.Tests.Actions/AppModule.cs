﻿using Autofac;
using Dezac.Tests.Extensions;
using Dezac.Tests.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurc.Tests.Actions.Extensions;

namespace Zurc.Tests.Actions
{
    public class AppModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RunnerExtensions>().As<IRunnerExtensions>();

            LogService.RunnerExtensions = new RunnerExtensions();
        }
    }
}
