namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PROYECTOTAREA")]
    public partial class PROYECTOTAREA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPROYECTO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMFASE { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTAREA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTAREA { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [StringLength(800)]
        public string DESCRIPCIONDETALLADA { get; set; }

        [StringLength(10)]
        public string IDEMPLEADO { get; set; }
    }
}
