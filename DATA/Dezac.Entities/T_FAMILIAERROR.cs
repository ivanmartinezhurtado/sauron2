namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_FAMILIAERROR
    {
        public T_FAMILIAERROR()
        {
            this.OPE_ERROR = new HashSet<OPE_ERROR>();
        }
    
        public int NUMFAMILIA { get; set; }
        public string DESCRIPCION { get; set; }
    
        public virtual ICollection<OPE_ERROR> OPE_ERROR { get; set; }
    }
}
