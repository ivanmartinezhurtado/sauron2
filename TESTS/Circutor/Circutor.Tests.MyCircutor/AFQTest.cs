﻿using Circutor.Tests.MyCircutor.Client;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Circutor.Tests.MyCircutor
{
    /// <summary>
    /// 1. Login
    /// 2. Check if already registered in MyCircutor by item code and serial number
    /// 3. Register in MyCircutor
    /// 4. AFQm configuration
    /// 5. Initiate check function
    /// 6. Check status on MyCircutor
    /// </summary>
    public class AFQTest
    {
        public string token;

        public string BaseUrl { get; set; }

        public AFQTest()
        {
            ServicePointManager.ServerCertificateValidationCallback = (s, c, ch, p) => true;
        }

        public string Login(string userName, string password)
        {
            var result = Task.Run(() => LoginAsync(userName, password)).Result;

            return result;
        }

        public async Task<string> LoginAsync(string userName, string password)
        {
            var client = new AuthClient();

            if (BaseUrl != null)
                client.BaseUrl = BaseUrl;

            var result = await client.LoginAsync(new Credentials
            {
                Username = userName,
                Password = password
            });

            if (result == null)
                throw new Exception("Invalid login!");

            token = result.Token;

            return token;
        }

        public AFQRegistrationResponse GetRegisteredInfo(string itemCode, string serialNumber)
        {
            var result = Task.Run(() => GetRegisteredInfoAsync(itemCode, serialNumber)).Result;

            return result;
        }

        public async Task<AFQRegistrationResponse> GetRegisteredInfoAsync(string itemCode, string serialNumber)
        {
            var client = new GetClient { Token = token };

            if (BaseUrl != null)
                client.BaseUrl = BaseUrl;

            try
            {
                var result = await client.AfqmAsync(serialNumber);
                if (result == null)
                    return null;

                var model = new AFQRegistrationResponse
                {
                    ItemCode = itemCode,
                    SerialNumber = serialNumber,
                    DeviceToken = result.DeviceToken,
                    RegistrationCode = result.RegistrationCode
                };

                return model;
            } catch
            {
                return null;
            }
        }

        public AFQRegistrationResponse RegisterAFQ(string itemCode, string serialNumber)
        {
            var result = Task.Run(() => RegisterAFQAsync(itemCode, serialNumber)).Result;

            return result;
        }

        public async Task<AFQRegistrationResponse> RegisterAFQAsync(string itemCode, string serialNumber)
        {
            var client = new RegisterClient { Token = token };

            if (BaseUrl != null)
                client.BaseUrl = BaseUrl;

            var item = new AfqmRegistrationQuery
            {
                ItemCode = itemCode,
                SerialNumber = serialNumber
            };

            var result = await client.AfqmAsync(item);

            var model = new AFQRegistrationResponse
            {
                ItemCode = itemCode,
                SerialNumber = serialNumber,
                DeviceToken = result.DeviceToken,
                RegistrationCode = result.RegistrationCode
            };

            return model;
        }

        public string GetCheck(string serialNumber)
        {
            var result = Task.Run(() => GetCheckAsync(serialNumber)).Result;

            return result;
        }

        public async Task<string> GetCheckAsync(string serialNumber)
        {
            var client = new AfqmClient { Token = token };

            if (BaseUrl != null)
                client.BaseUrl = BaseUrl;

            var result = await client.StatusAsync(serialNumber);
           
            return result.Result;
        }
    }

    public class AFQRegistrationResponse
    {
        public string SerialNumber { get; set; }
        public string ItemCode { get; set; }
        public string DeviceToken { get; set; }
        public string RegistrationCode { get; set; }
    }
}
