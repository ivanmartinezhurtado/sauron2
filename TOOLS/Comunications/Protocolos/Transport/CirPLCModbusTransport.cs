using Comunications.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Comunications.Message
{
    public class CirPLCModbusTransport : ModbusTransport
	{
        protected static byte codeFunctionCirPLC;
        protected static int serialNumberHost;

        public enum enumCodeFunctionCirPLC
        {
            CirPLCSendRequest = 0x01,
            CirPLCSendConfirm = 0x71,
            CirPLCGetSimpleRequest = 0x03,
            CirPLCGetSimpleConfirm = 0x73,
            CirPLCGetArrayRequest = 0x04,
            CirPLCGetArrayConfirm = 0x74,
            CirPLCSetSimpleRequest = 0x05,
            CirPLCSetSimpleConfirm = 0x75,
            CirPLCLeaveRequest = 0x07,
            CirPLCLeaveConfirm = 0x77,
            CirPLCResetRequest = 0x08,
            CirPLCResetConfirm = 0x78,
            CirPLCFwUpVersionRangeCheck = 0x09,
            CirPLCFwUpVerisonRangeConfirm = 0x79,
            CirPLCFwUpStart = 0x0A,
            CirPLCFwUpSendRequest = 0x0B,
            CirPLCFwUpSendConfirm = 0x7B,
            CirPLCStatusRequest = 0x0D,
            CirPLCStatusResponse = 0x7D,
            CirPLCGetRemoteRequest = 0x0E,
            CirPLCGetRemoteConfirm = 0x7E,
            CirPLCSetRemoteRequest = 0x0F,
            CirPLCSetRemoteConfirm = 0x7F,
        }

        public enumCodeFunctionCirPLC CodeFunctionCirPLC
        {
            set { codeFunctionCirPLC = (byte)value; }

            get { return (enumCodeFunctionCirPLC)codeFunctionCirPLC; }
        }

        public int SerialNumberHost
        {
            set { serialNumberHost = value; }

            get { return serialNumberHost; }
        }

        public CirPLCModbusTransport(IStreamResource streamResource)
            : base(streamResource)
		{
		}

        public CirPLCModbusTransport(IStreamResource streamResource, ILog logger)
            : base(streamResource, logger)
        {
        }

        protected override byte[] BuildRequestMessageFrame(ITransportMessage message)
        {
            ModbusMessage msg = message as ModbusMessage;     

            byte[] CirPLC = message.MessageFrame;

            List<byte> messageModbusBody = new List<byte>();
            messageModbusBody.AddRange(CirPLC);
            messageModbusBody.AddRange(ModbusUtility.CalculateCrc(messageModbusBody.ToArray()));

            List<byte> messageBody = new List<byte>();
            messageBody.AddRange(GetCirHeader(msg, messageModbusBody.ToArray().Length));
            messageBody.AddRange(messageModbusBody.ToArray());
            messageBody.AddRange(ComunicationUtility.RunCrc_crc16Calc(messageBody.ToArray()));

            return messageBody.ToArray();
        }

     
        internal static byte[] GetCirHeader(ModbusMessage message, int lengthCirPLC)
        {
            List<byte> header = new List<byte>();

            byte[] SerialNumber = BitConverter.GetBytes((int)IPAddress.HostToNetworkOrder(serialNumberHost));
            byte[] length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)(lengthCirPLC)));

            header.Add(codeFunctionCirPLC);
            header.AddRange(SerialNumber);
            header.AddRange(length);

            return header.ToArray();
        }

        protected override byte[] ReadRequestResponse(ITransportMessage message)
        {
            byte[] mbapHeader = new byte[7];
            int numBytesRead = 0;
            int i = 0;
            byte first = 0x00;

            while (first != 0xE1 && i < 10)
            {
                numBytesRead = _streamResource.Read(mbapHeader, 0, 1);

                if (numBytesRead == 0)
                    throw new FormatException("Read resulted in 0 bytes returned.");

                first = mbapHeader[0];
                int skipBytes = 0;

                if (first == 0xED || first == 0x71)
                    skipBytes = 3;
                else if (first == 0xEE || first == 0xE1)
                    skipBytes = 6;

                if (skipBytes > 0)
                {
                    numBytesRead += _streamResource.Read(mbapHeader, 1, skipBytes);
                    if (numBytesRead != skipBytes + 1)
                        throw new FormatException("Evento 0xE1 incompleto");

                    if (first == 0x71 && mbapHeader[1] != 0x01)
                        throw new FormatException(string.Format("Error CirPLC Send, recibido: {0:X}", mbapHeader[1]));

                    i ++;
                }
            }

            if (i > 10)
                throw new Exception("Respuesta no recibida");

            ushort frameLength = (ushort)IPAddress.HostToNetworkOrder(BitConverter.ToInt16(mbapHeader, 5));

            // read message
            byte[] messageFrame = new byte[frameLength];
            numBytesRead = 0;

            while (numBytesRead != frameLength)
            {
                numBytesRead += _streamResource.Read(messageFrame, numBytesRead, frameLength - numBytesRead);

                if (numBytesRead == 0)
                    throw new FormatException("Read resulted in 0 bytes returned.");

                if (numBytesRead == 3 && messageFrame[1] > Modbus.ExceptionOffset)
                    break;
            }
            
            byte[] crc = new byte[2];
            _streamResource.Read(crc, 2, 0);

            byte[] frameCRC = messageFrame.ToArray();

            byte[] frame = messageFrame.Take(messageFrame.Length - 2).ToArray();

            _logger.InfoFormat("RX: {0}", GetLogFrameString(frame));

            return frame;
        }

     
	}
}
