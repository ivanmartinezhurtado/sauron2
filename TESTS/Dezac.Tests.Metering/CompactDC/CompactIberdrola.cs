﻿
using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Metering
{
    [TestVersion(2.06)]
    public class CompactIberdrola : CDCTestBase
    {
        private const int PILOTO_ERROR = 48;
        private const int PISTON_GENERAL = 4;
        private const int OFF_ALIMENTACION_CONTADORES =56;
        private const int SALTAR_TRAFOS = 55;

        private string Camera_12345;

        public void TestInitialization()
        {
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            //******************************************
            tower.IO.DO.OffWait(1000, PILOTO_ERROR, OFF_ALIMENTACION_CONTADORES, SALTAR_TRAFOS);

            has3G = Configuracion.GetString("HAS_MODULE_MODEM", "NO").Trim().ToUpper() == "SI" ? "3G_" : "";
            onlySBT = true;
            DoubleSecondary = !onlySBT;
            InstanceNumber = 1;
            SBT_NUMBER = 1;

            Camera_12345 = GetVariable<string>("Camera", Camera_12345);

            PositionCameraDictionary = new Dictionary<int, string>();
            PositionCameraDictionary.Add(1, Camera_12345);
            PositionCameraDictionary.Add(2, Camera_12345);
            PositionCameraDictionary.Add(3, Camera_12345);
            PositionCameraDictionary.Add(4, Camera_12345);
            PositionCameraDictionary.Add(5, Camera_12345);
        }

        public void TestConsumption()
        {
            List<TriAdjustValueDef> consumptionValues = new List<TriAdjustValueDef>();

            consumptionValues.Add(new TriAdjustValueDef(Params.W.L1.EnVacio.Name, Params.W.Null.EnVacio.Min(), Params.W.Null.EnVacio.Max(), 0, 0, ParamUnidad.W));
            consumptionValues.Add(new TriAdjustValueDef(Params.VAR.L1.EnVacio.Name, Params.VAR.Null.EnVacio.Min(), Params.VAR.Null.EnVacio.Max(), 0, 0, ParamUnidad.Var));

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(230, 0, 50, 0, TriLineValue.Create(0, 120, 240));

            TestMeasureBase(consumptionValues, (step) =>
            {
                var cvmMteReadings = tower.CvmminiMTE.ReadAllVariables().Phases;

                return new double[] { cvmMteReadings.L1.PotenciaActiva, cvmMteReadings.L2.PotenciaActiva, cvmMteReadings.L3.PotenciaActiva,
                                      cvmMteReadings.L1.PotenciaReactiva,  cvmMteReadings.L2.PotenciaReactiva, cvmMteReadings.L3.PotenciaReactiva };
            }, 2, 5, 1000, null , true);
        }

        public override void TestVerificationSBT(int initCount = 3, int samples = 8, int timeInterval = 1200)
        {
            base.TestVerificationSBT(initCount, samples, timeInterval);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            WaitAllTasksAndRunOnlyOnce(InstanceNumber, "MTE_TEST_POWER_VERIFICATION_OFF_SBT", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0, 50, 0, new TriLineValue() { L1 = 0, L2 = 120, L3 = 240 }); });
        }
 
        public void TestPLCfromMeter()
        {
            internalTestPLCfromMeter("L1", 15, "CIR0000000001");
            internalTestPLCfromMeter("L2", 1, 15, 30, true, "CIR0000000002");
            internalTestPLCfromMeter("L3", 1, 15, 30, true, "CIR0000000003");
        }

        public override void DeleteNodesRestart()
        {
            base.DeleteNodesRestart();

            tower.IO.DO.On(OFF_ALIMENTACION_CONTADORES);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

           //this.ShowDialogWithImage("VERIFICACION FINAL", "INTERCAMBIAR LA TAPA DE TEST A LA FINAL", @"\\sfserver01\Idp\Idp Público\IMAGESOURCE\COMPACT_DC\COMPACT_DC_CCT.png", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.DialogResult.OK);

            tower.PowerSourceIII.ApplyAndWaitStabilisation();

            if (ZonaHoraria == "NO")
                dateTimeLocalRestart = DateTime.Now;
            else
            {
                var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
                dateTimeLocalRestart = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            }
        }

        public override void TestFinish()
        {
            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    if (string.IsNullOrEmpty(TestInfo.NumSerie))
                        tower.IO.DO.On(PILOTO_ERROR);

                    Delay(1500, "Espera de equipo malo");

                    tower.IO.DO.Off(PISTON_GENERAL);
                }
            }
            base.TestFinish();
        }
    }
}
