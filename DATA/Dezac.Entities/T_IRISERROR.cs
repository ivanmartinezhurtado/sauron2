namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_IRISERROR
    {
        public T_IRISERROR()
        {
            this.T_TESTERROR = new HashSet<T_TESTERROR>();
        }
    
        public string ERRORCODE { get; set; }
        public string DESCRIPCION { get; set; }
        public string DESCRIPCIONML { get; set; }
        public string DIAGNOSTICOML { get; set; }
    
        public virtual ICollection<T_TESTERROR> T_TESTERROR { get; set; }
    }
}
