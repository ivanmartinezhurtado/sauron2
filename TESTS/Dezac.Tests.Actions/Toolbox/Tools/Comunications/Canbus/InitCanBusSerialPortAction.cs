﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(InitCanBusSerialPortActionDescription))]
    public class InitCanBusSerialPortAction : ActionBase
    {

        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("3. Configuration")]
        [Description("Puerto en el que se quiere trabajar")]
        public string Port { get; set; }

        [Category("3. Configuration")]
        [Description("Velocidad de transmsión del puerto")]
        [DefaultValue(BaudRateEnum.Bps9600)]
        public BaudRateEnum Bps { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_CAN")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de espera a recibir la trama amtes de dar error")]
        [DefaultValue(1000)]
        public int TimeOut { get; set; }

        public override void Execute()
        {
            var portDevice = Convert.ToByte(GetVariable<string>(Port, Port).Replace("COM", ""));

            var sp = new SerialPort("COM" + portDevice, (int)Bps, Parity.None, 8, StopBits.One);
            sp.ReadTimeout = TimeOut;
            sp.WriteTimeout = TimeOut;
          
            var protocol = new CanBusProtocol();

            var spa = new SerialPortAdapter(sp);
            spa.WaitTimeOpenPort = 1000;

            var transport = new MessageTransport(protocol, spa, Logger);

            SetVariable(DeviceName ?? ("COM" + portDevice), transport);
        }
    }

    public class InitCanBusSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "InitCanBusSerialPort"; } }
        public override string Description { get { return "Crea una instancia de un puerto de comunicacion Serie utilizando el protocol CanBus"; } }
        public override string Category { get { return "Tools/Comunications/Canbus"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
