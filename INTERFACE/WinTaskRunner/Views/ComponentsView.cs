﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Tests;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using WinTaskRunner.Design.Services;
using WinTaskRunner.Models;
using WinTaskRunner.Services;

namespace WinTaskRunner.Views
{
    public partial class ComponentsView : UserControl
    {
        public List<ComponentsViewItem> items;
        protected readonly AppService app;
        protected readonly AssemblyResolverService assemblyResolver;
        protected readonly IInstanceResolveService instanceResolveService;
        protected readonly IDocumentService documentService;
        protected readonly IShell shell;
        protected readonly EventBus eventBus;

        private ComponentsView()
        {
            InitializeComponent();

            treeView.NodeExpandedChanging += List_NodeExpandedChanging;
        }

        public ComponentsView(AppService appService
            , AssemblyResolverService assemblyResolver
            , InstanceResolveService instanceResolveService
            , IDocumentService documentService
            , IShell shell
            , EventBus eventBus)
            : this()
        {
            app = appService;
            this.assemblyResolver = assemblyResolver;
            this.instanceResolveService = instanceResolveService;
            this.documentService = documentService;
            this.shell = shell;
            this.eventBus = eventBus;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            AddNodeIconActions();

            if (!DesignMode)
                Reload();
        }

        private void AddNodeIconActions()
        {
            foreach (Utils.AppConfig.IconTreeNode icons in app.Config.ImageActions)
            {
                string imagePath = string.Format(@"{0}\Images\{1}.png", Application.StartupPath, icons.Icon);
                if (!File.Exists(imagePath))
                    imagePath = string.Format(@"{0}\Images\Action.png", Application.StartupPath);

                var imageIcon = Image.FromFile(imagePath);
                imageList.Images.Add(icons.Key, imageIcon);
            }
        }
        public void Reload()
        {
            if (items == null)
                items = new List<ComponentsViewItem>();

            treeView.Nodes.Clear();

            imageList.Images.Add("Default", Properties.Resources.Action);

            AddItem(null, new ComponentsViewItem { Name = "Devices", IsFolder = true });
            //AddItem(null, new ComponentItem { Name = "Tests", IsFolder = true });

            LoadAssemblies();

            AddItem(null, new ComponentsViewItem { Name = "Device Generated", IsFolder = true });

            LoadDeviceLibrary();

            LoadSnippets();
        }
        private void LoadSnippets()
        {
            RadTreeNode node = AddItem(null, new ComponentsViewItem
            {
                Name = "Sequence Snippets",
                IsFolder = true,
                IsDirectory = true,
                IsLazy = true,
                FileName = app.GetUserSettings("PathSequenceSnippets"),
                Filter = (p) => (p.EndsWith(".seq") || p.EndsWith(".json"))
            });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }
        private RadTreeNode AddItem(RadTreeNode parent, ComponentsViewItem item)
        {
            var node = new RadTreeNode(item.Name);

            node.Tag = item;
            node.ImageKey = item.ImageKey ?? (imageList.Images.ContainsKey(item.Name) ? item.Name : "Default");

            if (!item.IsFolder || item.IsLazy)
            {
                var lazy = new RadTreeNode("Loading...");
                lazy.Tag = null;
                node.Nodes.Add(lazy);
            }

            if (parent == null)
                treeView.Nodes.Add(node);
            else
                parent.Nodes.Add(node);

            return node;
        }
        private void LoadAssemblies()
        {
            try
            {
                foreach (string name in app.Config.ComponentsView.LoadAssemblies)
                {
                    Assembly assembly = assemblyResolver.Resolve(app.Config.PathCurrentAsssembly, name);
                    if (assembly != null)
                        try
                        {
                            var item = new ComponentsViewItem { FileName = assembly.ManifestModule.Name, IsDirectory = false, IsDll = true };
                            items.Add(item);

                            if (item.FileName.IndexOf(".Device.", StringComparison.InvariantCultureIgnoreCase) >= 0)
                            {
                                item.Name = item.FileName.Replace("Dezac.Device.", "").Replace(".dll", "");
                                AddItem(treeView.Nodes[0], item);
                            }
                            else if (item.FileName.IndexOf(".Instruments.", StringComparison.InvariantCultureIgnoreCase) >= 0)
                            {
                                item.Name = item.FileName.Replace("Dezac.", "").Replace(".dll", "");
                                AddItem(null, item);
                            }
                            //else if (item.FileName.IndexOf(".Tests.", StringComparison.InvariantCultureIgnoreCase) >= 0)
                            //{
                            //    item.Name = item.FileName.Replace("Dezac.Tests.", "").Replace(".dll", "");
                            //    AddItem(list.Nodes[1], item);
                            //}
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Ensamblado inválido!");
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void AddNodeAssembly(string parentNode, string fileName, string replace = "Device.Generated.")
        {
            try
            {
                RadTreeNode node = treeView.Nodes.Where(p => p.Name == parentNode).FirstOrDefault();
                string name = Path.GetFileName(fileName).Replace(replace, "").Replace(".dll", "");
                if (!node.Nodes.Where(p => p.Name == name).Any())
                {
                    var item = new ComponentsViewItem { Name = name, FileName = fileName, IsDirectory = false, IsDll = true };
                    items.Add(item);
                    AddItem(node, item);
                }
                else
                    MessageBox.Show("Device Library ya existente !");
            }
            catch (Exception)
            {
                MessageBox.Show("Ensamblado inválido!");
            }
        }
        private void List_NodeExpandedChanging(object sender, Telerik.WinControls.UI.RadTreeViewCancelEventArgs e)
        {

        }
        private void LoadItem(ComponentsViewItem item, RadTreeNode node)
        {
            if (item.IsDirectory)
                LoadDirectory(item, node);
            else if (item.IsDll)
                LoadAssembly(item, node);
        }
        private void LoadDirectory(ComponentsViewItem item, RadTreeNode node)
        {
            node.Nodes.Clear();

            if (!Directory.Exists(item.FileName))
                return;

            new DirectoryInfo(item.FileName)
                .GetDirectories()
                .ToList()
                .ForEach(p =>
                {
                    var citem = new ComponentsViewItem { FileName = p.FullName, IsDirectory = true, Filter = item.Filter, IsLazy = item.Filter != null };
                    if (item.Filter != null)
                    {
                        citem.Name = Path.GetFileNameWithoutExtension(p.FullName);
                        citem.ImageKey = "folder";
                    }
                    AddItem(node, citem);
                });

            Directory.EnumerateFiles(item.FileName)
                .Where(p => (item.Filter != null && item.Filter(p)) || (p.EndsWith(".dll") || p.EndsWith(".exe")))
                .ToList()
                .ForEach(p =>
                {
                    var citem = new ComponentsViewItem { FileName = p, IsDirectory = false };

                    if (item.Filter != null)
                    {
                        citem.IsFolder = true;
                        citem.Name = Path.GetFileNameWithoutExtension(p);
                        citem.ImageKey = "file";
                    }

                    AddItem(node, citem);
                });
        }
        private void LoadAssembly(ComponentsViewItem item, RadTreeNode node)
        {
            node.Nodes.Clear();

            try
            {
                Assembly assembly = assemblyResolver.Resolve(app.Config.PathCurrentAsssembly, item.FileName);
                if (assembly == null)
                    assembly = Assembly.LoadFrom(item.FileName);

                if (assembly != null)
                {
                    IEnumerable<Type> types = from t in assembly.GetTypes()
                                              where t.IsClass && t.IsPublic && !t.IsAbstract
                                              select t;

                    Type[] types2 = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        bool isToolboxItem = type.GetCustomAttributes<ToolboxItemAttribute>().Any(p => p.ToolboxItemType != null);
                        if (isToolboxItem ||
                            type.IsSubclassOf(typeof(TestBase)) ||
                            type.FullName.StartsWith("Dezac.Device."))
                        {
                            if (isToolboxItem)
                            {
                                IEnumerable<Attribute> descriptionClass = type.GetCustomAttributes(typeof(DesignerControlAttribute));
                                var att = new { Type = type, Design = descriptionClass.Cast<DesignerControlAttribute>().FirstOrDefault() };
                                var instance = Activator.CreateInstance(att.Design.Type) as ControlDescription;
                                string category = instance.Category ?? "Others";

                                var child = new RadTreeNode(instance.Name);
                                child.Tag = type;
                                child.ToolTipText = item.FileName;
                                child.Image = instance.Icon;

                                if (instance.SubcomponentType != null)
                                    LoadMethods(instance.SubcomponentType, child);

                                RadTreeNode nodeCategory = node.Nodes.Where(p => p.Name.Contains(category)).FirstOrDefault();
                                if (nodeCategory == null)
                                {
                                    RadTreeNode radTreeCategory = node.Nodes.Add(category);
                                    radTreeCategory.ImageKey = imageList.Images.ContainsKey(category) ? category : "Insruments";
                                    radTreeCategory.Nodes.Add(child);
                                }
                                else
                                    nodeCategory.Nodes.Add(child);
                            }
                            else if (type.IsSubclassOf(typeof(TestBase)) || type.IsSubclassOf(typeof(DeviceBase)))
                            {
                                var child = new RadTreeNode(type.Name);
                                child.Tag = type;
                                child.ToolTipText = item.FileName;
                                child.ImageKey = imageList.Images.ContainsKey(type.Name) ? type.Name : "type";

                                LoadMethods(type, child);

                                node.Nodes.Add(child);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadMethods(Type type, RadTreeNode node)
        {
            IOrderedEnumerable<MethodInfo> methods = type
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => !m.IsSpecialName && !m.DeclaringType.FullName.StartsWith("System.") && m.DeclaringType.Name != "TestBase" && m.DeclaringType.Name != "ThreadBase")
                .OrderBy(p => p.Name);

            foreach (MethodInfo method in methods)
            {
                var visible = method.GetCustomAttributes(typeof(BrowsableAttribute), true).FirstOrDefault() as BrowsableAttribute;
                if (visible != null && !visible.Browsable)
                    continue;

                var mchild = new RadTreeNode(method.Name);
                mchild.Tag = method;
                mchild.ImageKey = "method";

                node.Nodes.Add(mchild);
            }
        }
        private void cmdInspect_Click(object sender, EventArgs e)
        {
            var inspector = new InspectorViewer();
            inspector.LoadType(mnuNode.Tag as Type);

            AddToolWindow(inspector, DockPosition.Right, "Inspector", 1100, 850);
        }
        private void tsOpenInstrument_Click(object sender, EventArgs e)
        {
            var type = mnuInstruments.Tag as Type;
            if (type == null)
                return;

            IEnumerable<Attribute> descriptionClass = type.GetCustomAttributes(typeof(DesignerControlAttribute));
            var att = new { Type = type, Design = descriptionClass.Cast<DesignerControlAttribute>().FirstOrDefault() };
            var instance = Activator.CreateInstance(att.Design.Type) as ControlDescription;

            if (instance.Visible && instance.IconBig != null)
            {
                var ctrl = Activator.CreateInstance(type) as Control;
                if (ctrl != null)
                    if (instanceResolveService != null)
                    {
                        Type ctrlType = ctrl.GetType();
                        PropertyInfo prop = ctrlType.GetProperty("InstrumentType");
                        var typeInstr = (Type)prop.GetValue(ctrl);

                        object target = instanceResolveService.Resolve(typeInstr);
                        if (target != null)
                        {
                            PropertyInfo prop2 = ctrlType.GetProperty("Instrument");
                            prop2.SetValue(ctrl, target);
                        }
                    }

                AddToolWindow(ctrl, DockPosition.Right, ctrl.Name, ctrl.Width + 10, ctrl.Height + 25);
            }
            else
                MessageBox.Show("Este instrumento no tiene Interface de Usuario Habiltada desde este menu conetextual", "Avios usuario", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void cmdSnippetsPath_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();

            ofd.ValidateNames = false;
            ofd.CheckFileExists = false;
            ofd.FileName = "Select Folder";
            ofd.Filter = "Sequence Files (*.seq)|*.seq|JSON files (*.json)|*.json";

            string currentPath = app.GetUserSettings("PathSequenceSnippets");

            if (!string.IsNullOrEmpty(currentPath))
                ofd.InitialDirectory = currentPath;

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            string pathName = Path.GetDirectoryName(ofd.FileName);

            app.SetUserSettings("PathSequenceSnippets", pathName);

            RadTreeNode node = treeView.Nodes.Where(p => p.Name == "Sequence Snippets").FirstOrDefault();

            var item = node.Tag as ComponentsViewItem;
            item.IsLoaded = false;
            item.FileName = pathName;

            node.Nodes.Clear();
            LoadDirectory(item, node);
        }
        private void AddToolWindow(Control control, DockPosition position, string title = null, int width = 0, int height = 0)
        {
            var win = new ToolWindow(title);
            win.CloseAction = DockWindowCloseAction.Close;
            win.ToolCaptionButtons = ToolStripCaptionButtons.Close;
            win.DockState = DockState.Floating;
            win.Controls.Add(control);
            var size = new Size(width + 10, height + 25);
            win.DefaultFloatingSize = size;
            ((DocumentService)documentService).Manager.DockWindow(win, position);
            win.DockManager.DockWindowClosing += DockManager_DockWindowClosing;
            win.TabStrip.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
            win.TabStrip.SizeInfo.AbsoluteSize = size;
            win.Float(new Point(400, 100), size);
        }
        private void DockManager_DockWindowClosing(object sender, DockWindowCancelEventArgs e)
        {
            DockWindow rd = e.NewWindow;

            if (rd.CloseAction == DockWindowCloseAction.Close)
                rd.Dispose();
        }
        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();

            ofd.ValidateNames = false;
            ofd.CheckFileExists = false;
            ofd.Filter = "Device.Generated.* (*.dll)|*.dll";
            ofd.FilterIndex = 1;
            ofd.DefaultExt = ".dll";
            ofd.FileName = "Device.Generated.*";
            string currentPath = ConfigurationManager.AppSettings["PathDeviceLibrary"];

            if (!string.IsNullOrEmpty(currentPath))
                ofd.InitialDirectory = currentPath;

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                string pathName = Path.GetFullPath(ofd.FileName);
                string destFileName = string.Format(@"{0}\{1}", Application.StartupPath, ofd.SafeFileName);
                File.Copy(pathName, destFileName, true);
                Thread.Sleep(500);
                AddNodeAssembly("Device Generated", destFileName);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                if (ex.InnerException != null)
                    message = ex.InnerException.Message;

                MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void LoadDeviceLibrary()
        {
            IEnumerable<string> files = Directory.EnumerateFiles(Application.StartupPath, "Device.Generated.*", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                string pathName = Path.GetFullPath(file);
                AddNodeAssembly("Device Generated", pathName);
            }
        }
        private void DeleteDeviceLibraryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();

            ofd.ValidateNames = false;
            ofd.CheckFileExists = false;
            ofd.Filter = "Device.Generated.* (*.dll)|*.dll";
            ofd.FilterIndex = 1;
            ofd.DefaultExt = ".dll";
            ofd.FileName = "Device.Generated.*";
            string currentPath = Application.StartupPath;

            if (!string.IsNullOrEmpty(currentPath))
                ofd.InitialDirectory = currentPath;

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            string pathName = Path.GetFullPath(ofd.FileName);

            try
            {
                RadTreeNode node = treeView.Nodes.Where(p => p.Name == "Device Generated").FirstOrDefault();
                string name = Path.GetFileName(pathName).Replace("Device.Generated.", "").Replace(".dll", "");
                if (node.Nodes.Where(p => p.Name == name).Any())
                {
                    RadTreeNode nodeDevice = node.FindNodes((p) => p.Name == name).FirstOrDefault();
                    RadTreeNode nodeChils = nodeDevice.FirstNode;
                    nodeChils.Tag = null;
                    nodeChils.Remove();
                    nodeDevice.Tag = null;
                    nodeDevice.Remove();
                    treeView.Refresh();
                    ComponentsViewItem listDelete = items.Where(p => p.FileName == name).FirstOrDefault();
                    items.Remove(listDelete);
                    Thread.Sleep(200);
                    File.Delete(pathName);
                }
                else
                    File.Delete(pathName);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                if (ex.InnerException != null)
                    message = ex.InnerException.Message;

                MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Treeview
        private void treeView_DragEnding(object sender, RadTreeViewDragCancelEventArgs e) => e.Cancel = true;
        private void treeView_DragEnter(object sender, DragEventArgs e) => e.Effect = DragDropEffects.Copy;
        private void treeView_NodeMouseDown(object sender, RadTreeViewMouseEventArgs e)
        {
            if (treeView.Tag != null && treeView.Tag == e.Node)
                return;

            if (e.OriginalEventArgs.Button == MouseButtons.Right)
            {
                ContextMenuStrip menu = null;

                if (e.Node.Name == "Sequence Snippets")
                    menu = mnuSnippets;
                else if (e.Node.Name == "Device Generated")
                    menu = mnuDeviceGenerator;
                else if (e.Node.Tag is Type)
                    menu = e.Node.RootNode.Name != "Instruments" ? mnuNode : mnuInstruments;

                if (menu != null)
                {
                    menu.Tag = e.Node.Tag;
                    menu.Show(PointToScreen(e.OriginalEventArgs.Location));
                }

                return;
            }

            RadTreeNode node = e.Node;
            if (node == null || node.Tag == null)
                return;

            object tag = node.Tag;
            var m = tag as MethodInfo;
            if (m == null)
            {
                var c = tag as ComponentsViewItem;
                if (c == null || c.IsDirectory || c.ImageKey != "file")
                    return;
            }

            treeView.Tag = e.Node;
            treeView.DoDragDrop(tag, DragDropEffects.Copy);
        }
        private void treeView_MouseUp(object sender, MouseEventArgs e) => treeView.Tag = null;
        private void treeView_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.None)
                treeView.Tag = null;
        }
        private void treeView_NodeExpandedChanging(object sender, RadTreeViewCancelEventArgs e)
        {
            var item = e.Node.Tag as ComponentsViewItem;

            var tree = sender as RadTreeViewElement;

            bool hasNode = e.Node.Nodes.Any();

            if (item != null && !item.IsLoaded && !e.Node.Expanded && hasNode)
                LoadItem(item, e.Node);
        }
        #endregion

        #region Searchbox
        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            treeView.Filter = searchBox.Text.Length > 0 ? "Custom" : null;
            treeView.TreeViewElement.FilterPredicate = FilterNode;

            if (!string.IsNullOrEmpty(searchBox.Text))
                treeView.ExpandAll();
            else
                treeView.CollapseAll();
        }
        private bool FilterNode(RadTreeNode node)
        {
            string text = searchBox.Text;
            if (string.IsNullOrEmpty(text) || node.Text == null)
                return true;

            if (NodeContainsText(node, text) ||
                (node.ImageKey == "method" && node.Parent.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0))
                return true;

            if (ParentsNodeContainsText(node, text))
                return true;

            return false;
        }
        private bool NodeContainsText(RadTreeNode node, string text)
        {
            if (node.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            foreach (RadTreeNode child in node.Nodes)
                if (NodeContainsText(child, text))
                    return true;

            return false;
        }
        private bool ParentsNodeContainsText(RadTreeNode node, string text)
        {
            if (node.Parent?.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            if (node.Parent != null && ParentsNodeContainsText(node.Parent, text))
                return true;

            return false;
        }
        #endregion


    }
}
