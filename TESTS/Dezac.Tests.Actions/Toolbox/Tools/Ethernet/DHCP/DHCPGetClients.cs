﻿using Dezac.Core.Utility;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using WinDHCP.Library;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DHCPGetClientsDescription))]
    public class DHCPGetClients : ActionBase
    {
        [DefaultValue("DHCPServer")]
        [Category("3. Instance")]
        [Description("Nombre de la instancia del objeto DHCP Server")]
        public string DHCPServerName { get; set; }

        [DefaultValue("DHCPClients")]
        [Category("4. Results")]
        [Description("Nombre de la varabler donde se guardara la lista de clientes DHCP")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            var DHCPserver = GetSharedVariable(DHCPServerName) as DhcpServer;
            if (DHCPserver != null)
            {
               var clients = DHCPserver.ClientsActive;
               var resultClients = new List<ClientEntry>();
                var i = 1;
                foreach (var cli in clients)
                {
                    resultClients.Add(new ClientEntry() { Name = cli.Value.HostName, MAC = cli.Value.Owner.MAC, IP = cli.Value.Address.ToString(), Expire = cli.Value.Expiration.ToShortTimeString() });
                    Logger.InfoFormat(string.Format("--> CLIENT  -- NAME: {0}  -- MAC: {1}  -- IP: {2}  -- Expired: {3}", cli.Value.HostName, cli.Value.Owner.MAC, cli.Value.Address.ToString(), cli.Value.Expiration));
                    var nameHost = string.IsNullOrEmpty(cli.Value.HostName) ?  "UNKNOW": cli.Value.HostName;
                    nameHost = TaskRunner.Tools.VariablesResolver.ReplaceForbiddenCharacters(nameHost);
                    SetVariable(string.Format("{0}_MAC_{1}_{2}", DataOutput, i.ToString(), nameHost), cli.Value.Owner.MAC);
                    SetVariable(string.Format("{0}_IP_{1}_{2}", DataOutput, i.ToString(), nameHost), cli.Value.Address.ToString());
                    i++;
                }
            }
            else
                Error().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE("DHCPServerName no encontrado").Throw();          
        }
    }

    public class DHCPGetClientsDescription : ActionDescription
    {
        public override string Name { get { return "DHCPGetClients"; } }
        public override string Description { get { return "Obtiene los clientes conectados al Servidor DHCP"; } }
        public override string Category { get { return "Tools/Ethernet/DHCP"; } }
        public override Image Icon { get { return Properties.Resources.DHCP; } }
    }
}
