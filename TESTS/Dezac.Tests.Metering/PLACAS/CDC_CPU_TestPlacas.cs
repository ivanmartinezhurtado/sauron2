﻿using Dezac.Device.Metering;
using Dezac.Device.Metering.SOAP;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Metering.Placas
{
    [TestVersion(1.06)]
    public class CDC_CPU_TestPlacas : TestBase
    {
        private TowerBoard tower;
        private COMPACT_DC compacdc;

        private const int PILOTO_ERROR = 43;
        private const int ALIMENTACION_12V = 19;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            compacdc = AddInstanceVar(new COMPACT_DC(Identificacion.VERSION_STG), "UUT");
            var keyFileSSH = Configuracion.GetString("HAS_KEY_FILE_SSH", "NO", ParamUnidad.SinUnidad);
            if (keyFileSSH.ToUpper().Trim() == "SI")
            {
                var dirPath = ConfigurationManager.AppSettings["PathCustomizationFile"];
                var keyFile = Path.Combine(dirPath, "CDC", "id_rsa");
                compacdc.PrivateKeyFile = keyFile;
            }

            tower.Active24VDC();

            tower.IO.DO.Off(PILOTO_ERROR);

            Resultado.Set("PRUEBAS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestConfigurationPorts()
        {

            var processo = Identificacion.MODELO;

            SamplerWithCancel((p) => { compacdc.KillConcentratorAplication(processo); return true; }, "", 3, 2000, 10000, false, false);

            compacdc.Uart.SerialPortConfigure();

            compacdc.Puertos.ConfigurationAllPorts();
        }

        public void TestLeds()
        {
            var ledsValue = new Dictionary<string, COMPACT_DC.Ports.DefinePortsOutputs>();

            ledsValue.Add("OUT_LED_POWER", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_POWER);
            ledsValue.Add("OUT_LED_PLC_LINK", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_PLC_LINK);
            ledsValue.Add("OUT_LED_SBT_LINK", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_SBT_LINK);
            ledsValue.Add("OUT_LED_MODEM_LINK", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_MODEM_LINK);

            foreach (KeyValuePair<string, COMPACT_DC.Ports.DefinePortsOutputs> led in ledsValue)
                compacdc.Puertos.On(led.Value);

            var msg = Shell.MsgBox("Comprobar que los 5 leds estan encendidos","Test de LEDS", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            Assert.AreEqual(msg, DialogResult.Yes, Error().PROCESO.MATERIAL.LED("Error. mal funcionamiento de los LEDS"));

            Resultado.Set("LEDS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestOutputs()
        {
            var outputsValue = new Dictionary<COMPACT_DC.Ports.DefinePortsOutputs, byte>();

            outputsValue.Add(COMPACT_DC.Ports.DefinePortsOutputs.OUT_GLOBAL_RESET, 13);
            outputsValue.Add(COMPACT_DC.Ports.DefinePortsOutputs.OUT_GLOBAL_SUPPLY, 18);
            outputsValue.Add(COMPACT_DC.Ports.DefinePortsOutputs.OUT_SBT_SWITCH, 19);

            foreach (KeyValuePair<COMPACT_DC.Ports.DefinePortsOutputs, byte> output in outputsValue)
            {
                compacdc.Puertos.Off(output.Key);

                Delay(500, "Espera para apagar la salida a testear");

                compacdc.Puertos.On(output.Key);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Value];
                }, string.Format("Error. no se ha activado la {0}", output.Key.ToString(), 10, 500));

                compacdc.Puertos.Off(output.Key);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Value];
                }, string.Format("Error. no se ha desactivado la {0}", output.Key.ToString(), 10, 500));

                Resultado.Set(output.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestInputs()
        {
            var inputsValue = new Dictionary<COMPACT_DC.Ports.DefinePortsInputs, byte>();

            inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_SLOT_ID0, 20);
            inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_SLOT_ID1, 20);
            inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_SLOT_ID2, 20);
          //  inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_SLOT_ID3, 20);
            inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_INTERRUPT, 20);
            inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_PG, 20);
            inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_PLC_SECONDARY, 20);
           // inputsValue.Add(COMPACT_DC.Ports.DefinePortsInputs.IN_PLC_MONITOR, 20);

            foreach (KeyValuePair<COMPACT_DC.Ports.DefinePortsInputs, byte> input in inputsValue)
            {
                tower.IO.DO.Off(input.Value);

                Delay(500, "espera para apagar entrada por seguridad");

                tower.IO.DO.On(input.Value);

                Assert.IsTrue(compacdc.Puertos.VerificationInputsStatus(COMPACT_DC.statusPort.On, input.Key), Error().UUT.COMUNICACIONES.SETUP(string.Format("Error. mal funcionamiento de la entrada {0} ", input.Key)));

                tower.IO.DO.Off(input.Value);

                Assert.IsTrue(compacdc.Puertos.VerificationInputsStatus(COMPACT_DC.statusPort.Off, input.Key), Error().UUT.COMUNICACIONES.SETUP(string.Format("Error. mal funcionamiento de la entrada {0} ", input.Key)));

                Resultado.Set(input.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestWatchDog()
        {
            tower.HP53131A
             .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
             .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1)
             .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            var adjustValueTestPoint = new AdjustValueDef(Params.FREQ.Null.TestPoint("WATCHDOG").Name, 0, Params.FREQ.Null.TestPoint("WATCHDOG").Min(), Params.FREQ.Null.TestPoint("WATCHDOG").Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                return tower.MeasureWithFrecuency(InputMuxEnum.IN5, 5000, HP53131A.Variables.FREQ, Instruments.Measure.HP53131A.Channels.CH1);

            }, 0, 3, 500);


            Assert.IsTrue(compacdc.Puertos.VerificationInputsStatus(COMPACT_DC.statusPort.On, COMPACT_DC.Ports.DefinePortsInputs.IN_SOFT_RESET), Error().UUT.COMUNICACIONES.SETUP(string.Format("Error. mal funcionamiento de la entrada {0} ", "IN_SOFT_RESET")));

            compacdc.Puertos.Off(COMPACT_DC.Ports.DefinePortsOutputs.OUT_WATCHDOG);

            Assert.IsTrue(compacdc.Puertos.VerificationOutputStatus(COMPACT_DC.statusPort.Off, COMPACT_DC.Ports.DefinePortsOutputs.OUT_WATCHDOG), Error().UUT.COMUNICACIONES.SETUP(string.Format("Error. mal funcionamiento de la salida {0} ", "OUT_WATCHDOG")));

            compacdc.Puertos.On(COMPACT_DC.Ports.DefinePortsOutputs.OUT_WATCHDOG);

            tower.IO.DO.On(17);
            tower.IO.DO.On(18);

            tower.HP53131A
             .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
             .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1)
             .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            adjustValueTestPoint = new AdjustValueDef(Params.FREQ.Other("RESET").TestPoint("WATCHDOG").Name, 0, Params.FREQ.Other("RESET").TestPoint("WATCHDOG").Min(), Params.FREQ.Other("RESET").TestPoint("WATCHDOG").Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                return tower.MeasureWithFrecuency(InputMuxEnum.IN5, 5000, HP53131A.Variables.FREQ, Instruments.Measure.HP53131A.Channels.CH1);

            }, 0, 3, 500);

            tower.HP53131A
             .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
             .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1)
             .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.TINT, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            adjustValueTestPoint = new AdjustValueDef(Params.DCYCLE.Other("RESET").TestPoint("WATCHDOG").Name, 0, Params.DCYCLE.Other("RESET").TestPoint("WATCHDOG").Min(), Params.DCYCLE.Other("RESET").TestPoint("WATCHDOG").Max(), 0, 0, ParamUnidad.ms);

            TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                return tower.MeasureWithFrecuency(InputMuxEnum.IN5, 5000, HP53131A.Variables.DCYCLE, Instruments.Measure.HP53131A.Channels.CH1);

            }, 0, 3, 500);


            tower.IO.DO.Off(17);
            tower.IO.DO.Off(18);

            Resultado.Set("WATCHDOG", "OK", ParamUnidad.SinUnidad);

        }

        public void TestUART(byte UART1_SBT = 5, byte UART2_PLC = 6, byte UART4_RS485 = 7)
        {
            var com_UART1_SBT = UART1_SBT; // Comunicaciones.SerialPort; //COM5 UART1 TTL
            var com_UART2_PLC = UART2_PLC; // Comunicaciones.SerialPort2; //COM6 UART2 TTL
            var com_UART4_RS485 = UART4_RS485; // Comunicaciones.SerialPort; //COM7 UART4 RS485 

            var comValue = new Dictionary<COMPACT_DC.UART.defineUART, byte>();

            //comValue.Add(COMPACT_DC.UART.defineUART.UART1_SBT, com_UART1_SBT);
            //comValue.Add(COMPACT_DC.UART.defineUART.UART2_PLC, com_UART2_PLC);
            comValue.Add(COMPACT_DC.UART.defineUART.UART4_RS485, com_UART4_RS485);

            foreach (KeyValuePair<COMPACT_DC.UART.defineUART, byte> com in comValue)
            {
                compacdc.Uart.Active_UART(com.Key);

                Delay(500, "Espera iniciar puerto a testear");

                SamplerWithCancel((p) => { compacdc.Uart.ExecuteCommand("Hola Mundo", 2); return true; }, string.Format("Error. mal funcionamiento del Puerto {0} ", com.Key), 5, 500);

                Resultado.Set(com.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestWebService()
        {
            CompactDctSoapService.configuration config = null;

            SamplerWithCancel((p) => { config = compacdc.Soap.ReadConfiguration(); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 10000, false, false);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, config.info.Vf, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            internalCheckTestPartitionMemory(compacdc.StatusInternalMemory());
        }

        public void TestFinish()
        {
            if (compacdc != null)
                compacdc.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(21, 17, 7);

                    Delay(1000, "Espera de equipo malo");
                }
                tower.Dispose();
            }
        }


        //----------------------------------------

        internal void internalCheckTestPartitionMemory(List<COMPACT_DC.Memory> memory)
        {
            if (memory.Count() < 4)
                throw new Exception("Error. no se han detectado el número de particiones correctamente");

            var ArrayPartitions = new List<string> { "ubi0", "ubi1", "ubi2" };

            foreach (string partition in ArrayPartitions)
            {
                var mod = memory.Where((p) => p.Filesystem.Contains(partition));
                if (mod.Count() == 0)
                    throw new Exception(string.Format("Error. no se ha encontrado la partición {0} ", partition));

                CultureInfo enUS = new CultureInfo("en-US");

                var size = Convert.ToDouble(mod.FirstOrDefault().Size.Replace("M", ""), enUS);

                var partitionName = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Name;
                var partitionSizeMin = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Min();
                var partitionSizeMax = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Max();

                Assert.AreBetween(partitionName, size, partitionSizeMin, partitionSizeMax, Error().UUT.EEPROM.MARGENES(string.Format("Error. tamaño de la partición{0} de la memoria interna fuera de margenes", partition)), ParamUnidad.MByte);
            }
        }
    }
}
