﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace Dezac.Device.Metering.SOAP
{
    public class CirlampSoapService
    {
        protected static readonly ILog _logger = LogManager.GetLogger("CirlampSoapService");

        private CompactDcSoapTransport _soap;
        public string VersionXml;

        public CirlampSoapService()
        {
        }

        protected CompactDcSoapTransport Soap
        {
            get
            {
                if (_soap == null)
                    _soap = new CompactDcSoapTransport();

                return _soap;
            }
        }
        
        public string ReadFile(string file)
        {
            LogMethod();

            var response = Soap.SynchReport(file);

            XElement xelement = XElement.Parse(response);

            return xelement.ToString();
        }

        public void WriteFile(string file)
        {
            LogMethod();

            var xmlFile = XElement.Load(file);

            Soap.Order(xmlFile.ToString());
        }

        public void WriteOutputs(byte output, bool state)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);
            var frame = new Dictionary<string, string>();
            frame.Add("Dir", output.ToString());
            frame.Add("On", state ? "Y" : "N");
            frame.Add("Off", state ? "N" : "Y");
            Soap.Order(message.BuildMessageXml("E13", "Output", frame));
        }
        

        protected void LogMethod([CallerMemberName] string name = "")
        {
            _logger.InfoFormat(name);
        }
    }
}
