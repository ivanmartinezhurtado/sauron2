﻿using Comunications;
using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using TaskRunner;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.08)]
    public class MTEPPS : PowerSourceACIII, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(MTEPPS));


        public enum PrivateCommands
        {
            SET_ACTIVE_RELE_120A = Commands.USER_DEFINED_COMMANDS + 1,
            SET_AMPLI_ON = Commands.USER_DEFINED_COMMANDS + 2,
            SET_AMPLI_OFF = Commands.USER_DEFINED_COMMANDS + 3,
            SET_HARMONICS_VOLTAGE = Commands.USER_DEFINED_COMMANDS + 4,
            SET_HARMONICS_CURRENT = Commands.USER_DEFINED_COMMANDS + 5,
        };

        public enum OnOffEnum
        {
            UI = 1,
            U = 2,
            I = 3
        }

        private SerialPort sp;
        private byte _port = 1;
        private bool _output120A = false;
        private string version;
        private typeSourceEnum typeSource;

        private const string PRESET_CONFIG_VOLTAGE_COMMAND = "U1,{0:00.000};U2,{1:00.000};U3,{2:00.000}";
        private const string PRESET_CONFIG_CURRENT_COMMAND = "I1,{0:00.000};I2,{1:00.000};I3,{2:00.000}";
        private const string PRESET_CONFIG_PF_COMMAND = "W1,{0:00.000};W2,{1:00.000};W3,{2:00.000}";
        private const string PRESET_CONFIG_DESFASE_COMMAND = "PH1,{0:0};PH2,{1:0};PH3,{2:0}";
        private const string PRESET_CONFIG_FRECUENCY_COMMAND = "FRQ,{0:0.00}";
        private const string APPLY_CONFIG_COMMAND = "SET";
        private const string APPLY_OFF_COMMAND = "OFF";
        private const string RESET_COMMAND = "R";
        private const string READ_VOLTAGE_COMMAND = "?2";
        private const string READ_CURRENT_COMMAND = "?1";
        private const string READ_DESFASE_VOLTAGE_COMMAND = "?13";
        private const string READ_DESFASE_CURRENT_COMMAND = "?12";
        private const string READ_STATUS_COMMAND = "SE";
        private const string READ_POWERFACTOR_COMMAND = READ_DESFASE_CURRENT_COMMAND + ";" + READ_DESFASE_VOLTAGE_COMMAND;
        private const string READ_VERSION = "VER{0}";

        private const string APPLY_ACTIVE_RELE_120A_COMMAND = "SKLI0,{0};SKLI1,{0};SKLI2,{0}";
        private const string APPLY_AMPLI_ON_COMMAND = "ON{0}";
        private const string APPLY_AMPLI_OFF_COMMAND = "OFF{0}";
        private const string APPLY_HARMONICS_VOLTAGE = "OWU{0},{1},{2},{3}";
        private const string APPLY_HARMONICS_CURRENT = "OWI{0},{1},{2},{3}";

        public MTEPPS()
            : base(logger)
        {
            InitializeComponent();

            protocol = new MTEProtocol();
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), logger);
            transport.Retries = 1;
            transport.OnMessageException += OnMessageException;
        }

        public enum typeSourceEnum
        {
            PPS400,
            SPE120
        }

        public typeSourceEnum TypeSource
        {
            get { return typeSource; }
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public void SetPort(string port)
        {         
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public bool Output120A
        {
            get { return _output120A; }
            set
            {
                _output120A = value;

                if(string.IsNullOrEmpty(version))
                    GetIdentification();

                if (typeSource== typeSourceEnum.PPS400)             
                    SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_ACTIVE_RELE_120A, true, _output120A ? 1 : 0));            
            }
        }

        public void SetOutput120A(bool output120A = false)
        {
            Output120A = output120A;  
        }

        public void ActivateGenerator(OnOffEnum method = OnOffEnum.UI)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_AMPLI_ON, true, (int)method));
        }

        public void DeactivateGenerator(OnOffEnum method = OnOffEnum.UI)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_AMPLI_OFF, true, (int)method));
        }

        public void ApplyHarmonicsVoltage(Phase phase, byte harmonic, int amplitud)
        {
            if (harmonic > 30)
                throw new NotSupportedException("No se puede configurar un armonico superior al numero 31");

            if (amplitud >= 100)
                throw new NotSupportedException("No se puede configurar una amplitud superior o igual al 100% de la fundamental");

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_HARMONICS_VOLTAGE, true, (byte)phase, harmonic, amplitud, 0));
        }

        public void ApplyHarmonicsCurrent(Phase phase, byte harmonic, int amplitud)
        {
            if (harmonic > 30)
                throw new NotSupportedException("No se puede configurar un armonico superior al numero 31");

            if (amplitud >= 100)
                throw new NotSupportedException("No se puede configurar una amplitud superior o igual al 100% de la fundamental");

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_HARMONICS_CURRENT, true, (byte)phase, harmonic, amplitud, 0));
        }

        public void ApplyConfig()
        {
            SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
        }

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));

            if (version.Contains("SPE120.3"))
                typeSource = typeSourceEnum.SPE120;
            else
                typeSource = typeSourceEnum.PPS400;

            return version; 
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = 1000;
            sp.ReadTimeout = 3000;
            sp.BaudRate = 19200;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
        }

        public enum Phase
        {
            L1 = 1,
            L2 = 2,
            L3 = 3
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            MTEMessage message, response;

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:
                    message = MTEMessage.Create(PRESET_CONFIG_VOLTAGE_COMMAND, Presets.Voltage.L1, Presets.Voltage.L2, Presets.Voltage.L3);
                    transport.SendMessage<MTEMessage>(message);
                    DevicePreset.Voltage = Presets.Voltage;
                    break;
                case (int)Commands.SET_CURRENT:
                    message = MTEMessage.Create(PRESET_CONFIG_CURRENT_COMMAND, Presets.Current.L1, Presets.Current.L2, Presets.Current.L3);
                    transport.SendMessage<MTEMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    break;
                case (int)Commands.SET_FREQ:
                    message = MTEMessage.Create(PRESET_CONFIG_FRECUENCY_COMMAND, Presets.Freq);
                    transport.SendMessage<MTEMessage>(message);
                    DevicePreset.Freq = Presets.Freq;
                    break;
                case (int)Commands.SET_PF:
                    message = MTEMessage.Create(PRESET_CONFIG_PF_COMMAND, Presets.PF.L1, Presets.PF.L2, Presets.PF.L3);
                    transport.SendMessage<MTEMessage>(message);
                    DevicePreset.PF = Presets.PF;
                    break;
                case (int)Commands.SET_DESFASE:
                    message = MTEMessage.Create(PRESET_CONFIG_DESFASE_COMMAND, Presets.Desfase.L1, Presets.Desfase.L2, Presets.Desfase.L3);
                    transport.SendMessage<MTEMessage>(message);
                    DevicePreset.Desfase = Presets.Desfase;
                    break;
                case (int)Commands.APPLY_CONFIG:
                    message = new MTEMessage { Text = APPLY_CONFIG_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("SET"); };
                    var reint = 0;
                    do
                    {
                        var respSet = transport.SendMessage<MTEMessage>(message);
                        Status.HasError = respSet.Text == "SET=0";
                        if (Status.HasError)
                        {
                            System.Threading.Thread.Sleep(1500);
                            reint++;
                        }

                    } while (Status.HasError && reint < 3);

                    if (Status.HasError)
                        Status.ErrorDescription = "Comando SET = 0 no se ha podido aplicar la consigna";

                    break;
                case (int)Commands.APPLY_OFF:
                    message = new MTEMessage { Text = APPLY_OFF_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("OFF"); };
                    var respOff = transport.SendMessage<MTEMessage>(message);
                    Status.HasError = respOff.Text == "OFF=E";
                    if (Status.HasError)
                        Status.ErrorDescription = "Comando OFF = E no se ha podido realizar";

                    ResetPresets();
                    break;
                case (int)Commands.RESET:
                    message = new MTEMessage { Text = RESET_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("R="); };
                    var respRst = transport.SendMessage<MTEMessage>(message);
                    Status.HasError = respRst.Text == "R=E";
                    if (Status.HasError)
                        Status.ErrorDescription = "Comando RESET = E no se ha podido realizar";
                    ResetPresets();
                    break;
                case (int)Commands.READ_VOLTAGE:
                    message = new MTEMessage { Text = READ_VOLTAGE_COMMAND };
                    response = transport.SendMessage<MTEMessage>(message);

                    DeviceData.Voltage = TriLineValue.CreateFromString(response.Text.Substring(3));
                    break;
                case (int)Commands.READ_CURRENT:
                    message = new MTEMessage { Text = READ_CURRENT_COMMAND };
                    response = transport.SendMessage<MTEMessage>(message);

                    DeviceData.Current = TriLineValue.CreateFromString(response.Text.Substring(3));
                    break;
                case (int)Commands.READ_DESFASE:
                    message = new MTEMessage { Text = READ_DESFASE_VOLTAGE_COMMAND };
                    response = transport.SendMessage<MTEMessage>(message);
                    DeviceData.Desfase = TriLineValue.CreateFromString(response.Text.Substring(3));
                    break;
                case (int)Commands.READ_POWERFACTOR:
                    message = new MTEMessage { Text = READ_POWERFACTOR_COMMAND };
                    response = transport.SendMessage<MTEMessage>(message);
                    string[] values = response.Text.Split(';');

                    DeviceData.Desfase = TriLineValue.CreateFromString(values[0].Substring(3));
                    DeviceData.PF = CalculaPF(DeviceData.Desfase, TriLineValue.CreateFromString(values[1].Substring(3)));
                    break;
                case (int)Commands.READ_STATUS:
                    message = new MTEMessage { Text = READ_STATUS_COMMAND };
                    message.Validator = (m) => { return m.Text.StartsWith("SE="); };
                    response = transport.SendMessage<MTEMessage>(message);

                    string text = response.Text.Substring(3);

                    int p = text.IndexOf('S');
                    int pe = text.IndexOf('E');

                    Status.IsBusy = p >= 0 && text[p + 3] == '1';
                    Status.HasError = pe >= 0 && text[pe + 1] == '1';
                    Status.HasWarning = pe >= 0 && text[pe + 2] == '1';
                    break;
                case (int)PrivateCommands.SET_ACTIVE_RELE_120A:
                    message = MTEMessage.Create(APPLY_ACTIVE_RELE_120A_COMMAND, command.Args[0]);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_AMPLI_ON:
                    message = MTEMessage.Create(APPLY_AMPLI_ON_COMMAND, command.Args[0]);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_AMPLI_OFF:
                    message = MTEMessage.Create(APPLY_AMPLI_OFF_COMMAND, command.Args[0]);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                case (int)Commands.READ_IDN:
                    message = MTEMessage.Create(READ_VERSION, "1");
                    message.Validator = (m) => { return m.Text.StartsWith("VER"); };
                    response = transport.SendMessage<MTEMessage>(message);
                    var versionTmp = response.Text.Replace("VER1=", "").Split('#');
                    version = string.Format("{0}, SN={1}", versionTmp[0], versionTmp[1]);
                    break;
                case (int)PrivateCommands.SET_HARMONICS_VOLTAGE:
                    message = MTEMessage.Create(APPLY_HARMONICS_VOLTAGE, command.Args[0], command.Args[1], command.Args[2], command.Args[3]);
                    transport.SendMessage<MTEMessage>(message);
                    break;
                case (int)PrivateCommands.SET_HARMONICS_CURRENT:
                    message = MTEMessage.Create(APPLY_HARMONICS_CURRENT, command.Args[0], command.Args[1], command.Args[2], command.Args[3]);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
  
        protected TriLineValue CalculaPF(TriLineValue desfaseI, TriLineValue desfaseV)
        {
            double Factor_Rad = (2 * Math.PI) / 360;

            TriLineValue PF_rad = 0;

            //TODO falta acabar de mejorar ..
            if (Presets.PF == 0) return PF_rad;

            TriLineValue DesfaseV_rad = Factor_Rad * desfaseV;
            TriLineValue DesfaseI_rad = Factor_Rad * desfaseI;
            PF_rad = DesfaseV_rad - DesfaseI_rad;

            double Factor_Grados = 360 / (2 * Math.PI);

            return (Factor_Grados * PF_rad);
        }

        protected override bool ValidateEstabilization(double tolerance)
        {
            var list = new StatisticalList("VoltageL1", "VoltageL2", "VoltageL3", "CurrentL1", "CurrentL2", "CurrentL3"); // ,"PF_L1L2", "PF_L2L3", "PF_L3L1");
            var NumeroMuestras = 2;
            var maxRepeticiones = NumeroMuestras * 3;

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = 1200, InitialDelay = 0, NumIterations = maxRepeticiones, CancelOnException = true, ThrowException = true},
               (step) =>
               {
                   var voltage = ReadVoltage();
                   var current = ReadCurrent();
                   //var pf = ReadPowerFactor();

                   if (voltage.InToleranceFrom(DevicePreset.Voltage, tolerance) && current.InToleranceFrom(DevicePreset.Current, tolerance))
                      // && pf.InToleranceFrom(DevicePreset.PF, tolerance))
                   {
                       var valores = new List<double>();
                       valores.AddRange(voltage.ToArray());
                       valores.AddRange(current.ToArray());
                      // valores.AddRange(pf.ToArray());
                       list.Add(valores.ToArray());
                   }
                   else
                   {
                       list.Clear();
                       return;
                   }

                   if (list.Count(0) >= NumeroMuestras)
                       step.Cancel = true;
               });

            return sampler.Canceled;
        }

        protected virtual void OnMessageException(object sender, MessageExceptionHandler e)
        {
            logger.WarnFormat("PowerSource MTE OnMessageExceptionr by {0}", e.Exception.Message);
            if (e.Exception is ApplicationException)
                if (!e.Throw)
                {
                    SendCommand(new Command<Commands>(Commands.RESET, true));
                    System.Threading.Thread.Sleep(1000);
                }
        }
    }
}
