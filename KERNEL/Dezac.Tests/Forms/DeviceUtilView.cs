﻿using Dezac.Core.Utility;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Forms
{
    public partial class DeviceUtilView : DialogViewBase
    {
        private List<BIEN> ListBien { get; set; }
        private TestContextModel Model { get; set; }

        private bool HasUtil { get; set; }
        private bool HasManguera { get; set; }

        public int? Utillaje { get; set; }
        public int? Manguera { get; set; }

        public bool IsDebug { get; set; }

        private bool UtilForced { get; set; }
        private bool MangeraForced { get; set; }

        private bool? resultUtil { get; set; }
        private bool? resultManguera { get; set; }

        public DeviceUtilView(TestContextModel model, bool isDebug = false)
        {
            InitializeComponent();

            HasManguera = true;
            HasUtil = true;

            Model = model;
            IsDebug = isDebug;

            using (var svc = new DezacService())
            {
                ListBien = svc.GetBienByProductoFase(Model.NumProducto, Model.Version, Model.IdFase);
            }

            var listBienesGruop = ListBien.GroupBy(p => p.IDTIPO).ToList();

            HasUtil = ListBien.Where((p) => p.IDTIPO == "1D1" || p.IDTIPO == "1D2").Any();
            HasManguera = ListBien.Where((p) => p.IDTIPO == "1D3").Any();
        }

        private void txtUtil_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUtil.Text))
                return;

            if ((txtUtil.Text.Length < 4 || txtUtil.Text.Length > 7) && !IsDebug)
            {
                lblUtilDesc.Text = "";
                pbUtil.Image = Properties.Resources.empty_set;
                return;
            }

            int intParsed;
            if (!int.TryParse(txtUtil.Text.Trim(), out intParsed))
                return;

            Utillaje = intParsed;

            SelectionBien("UTIL");
        }

        private void txtManguera_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtManguera.Text))
                return;

            if ((txtManguera.Text.Length < 4 || txtManguera.Text.Length > 7) && !IsDebug)
            {
                lblMangueraDesc.Text = "";
                pbManquera.Image = Properties.Resources.empty_set;
                return;
            }

            int intParsed;
            if (!int.TryParse(txtManguera.Text.Trim(), out intParsed))
                return;

            Manguera = intParsed;

            SelectionBien("MANGUERA");
        }

        private void SelectionBien(string tipo)
        {
            BIEN Bien;
            int numBien;
            Image imagen;

            if (tipo == "UTIL")
            {
                lblUtilDesc.Text = "";
                pbUtil.Image = Properties.Resources.empty_set;
                UtilForced = false;
                numBien = Utillaje.Value;
                Bien = ListBien.Where((p) => p.NUMBIEN == numBien && (p.IDTIPO == "1D1" || p.IDTIPO == "1D2")).FirstOrDefault();
            }
            else
            {
                lblMangueraDesc.Text = "";
                pbManquera.Image = Properties.Resources.empty_set;
                MangeraForced = false;
                numBien = Manguera.Value;
                Bien = ListBien.Where((p) => p.NUMBIEN == numBien && (p.IDTIPO == "1D3")).FirstOrDefault();
            }

            if (Bien == null)
                using (var svc = new DezacService())
                {
                    Bien = svc.GetBienParamsAssitencia(numBien);
                }

            if (Bien != null)
            {
                if (Bien.IMAGEN == null)
                    imagen = Properties.Resources.Imagen_no_disponible_svg;
                else
                    using (MemoryStream mStream = new MemoryStream(Bien.IMAGEN))
                    {
                        imagen = Image.FromStream(mStream);
                    }

                if (tipo == "UTIL")
                {
                    lblUtilDesc.Text = Bien.DESCRIPCION;
                    pbUtil.Image = imagen;
                }
                else
                {
                    lblMangueraDesc.Text = Bien.DESCRIPCION;
                    pbManquera.Image = imagen;
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Enter))
            {
                if (txtUtil.Focused)
                    resultUtil = ValidateBien(HasUtil, "UTIL");

                if (txtManguera.Focused)
                    resultManguera = ValidateBien(HasManguera, "MANGUERA");

                if (txtUtil.Focused && resultUtil.Value)
                    txtManguera.Focus();

                if (!resultManguera.HasValue || !resultUtil.HasValue || !resultManguera.Value || !resultUtil.Value)
                    return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private bool ValidateBien(bool HasBien, string tipo)
        {
            bool search = false;
            bool searchType = false;

            var textValue = tipo == "UTIL" ? txtUtil.Text : txtManguera.Text;

            if (HasBien)
            {
                if (string.IsNullOrEmpty(textValue))
                {
                    MessageBox.Show("NO SE PUEDE DEJAR EN BLANCO ESTE BIEN, PORQUE EXISTE UNO PARA ESTE PRODUCTO FASE", "AVISO BIEN NO PUEDE DEJARSE EN BLANCO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(textValue))
                    return true;
            }

            int numBien;
            if (!int.TryParse(textValue, out numBien))
                return false;

            if (SequenceContext.Current != null && SequenceContext.Current.RunMode == RunMode.Debug && numBien == 0)
                return true;

            if (txtUtil.Text.Trim() == txtManguera.Text.Trim())
            {
                MessageBox.Show("NO SE PUEDE REPETIR EL MISMO BIEN", "AVISO BIEN REPETIDO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            BIEN Bien = null;

            Bien = ListBien.Where((p) => p.NUMBIEN == numBien).FirstOrDefault();
            if (Bien != null)
            {
                search = true;

                if (tipo == "UTIL")
                {
                    if (Bien.IDTIPO == "1D1" || Bien.IDTIPO == "1D2")
                        searchType = true;
                } else
                {
                    if (Bien.IDTIPO == "1D3")
                        searchType = true;
                }
            } else
                using (var svc = new DezacService())
                {
                    Bien = svc.GetBienParamsAssitencia(numBien);
                }

            if (Bien == null)
            {
                MessageBox.Show("BIEN INEXISTENTE O NO ENCONTRADO EN LA BBDD", "BIEN NO ENCONTRADO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            var assitencia = Bien.BIENASISTENCIA.Where((p) => p.ABIERTA_SN == "S").Any();
            if (assitencia)
            {
                MessageBox.Show("NO SE PUEDE REALIZAR UN TEST CON UN BIEN QUE TENGA UNA ASISTENCIA ABIERTA", "BIEN CON ASISTENCIA ABIERTA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SendMailManteniment(numBien, 1);
                //return false;
            }

            var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S").OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
            if (progrmacion == null || !progrmacion.FECHAULTIMAEJECUCION.HasValue || !progrmacion.PERIODICIDAD.HasValue)
                SendMailManteniment(numBien, 2);
            else
            {
                var periodicidad = new TimeSpan(progrmacion.PERIODICIDAD.Value, 0, 0, 0);
                var dateTimeNextTask = progrmacion.FECHAULTIMAEJECUCION.Value.Add(periodicidad);
                var DiffTime = dateTimeNextTask.Subtract(DateTime.Now);
                if (DiffTime.TotalDays < 0)
                {
                    MessageBox.Show("BIEN CON UNA PROGRAMACION DE MANTENIMIENTO NO REALIZADA", "BIEN FUERA DE TIEMPO DE MANTENIMIENTO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    SendMailManteniment(numBien, 3);
                    //return false;
                }
                else if (DiffTime.TotalDays < 14)
                {
                    MessageBox.Show("BIEN CON UNA PROGRAMACION DE MANTENIMIENTO APUNTO DE CADUCARSE", "AVISO BIEN FUERA TIEMPO DE MANTENIMIENTO EN DOS SEMANAS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    SendMailManteniment(numBien, 4);
                }
            }

            if (!search || !searchType)
            {
                string message = string.Format("BIEN {0} NO ENCONTRADO EN SU FASE DE TEST, DESEA CONTINUAR?", tipo);
                if (!searchType)
                    message = string.Format("BIEN ENCONTRADO PERO NO ES DEL TIPO ESPERADO EN SU FASE DE TEST, TIPO {0}, DESEA CONTINUAR?", tipo);

                if (MessageBox.Show(message, string.Format("{0} NO ENCONTRADO O NO ES DEL TIPO CORRECTO", tipo), MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    if (tipo == "UTIL")
                        UtilForced = true;
                    else
                        MangeraForced = true;
                }
                else
                    return false;
            }

            return true;
        }
 
   

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            if (!resultUtil.HasValue)
                resultUtil = ValidateBien(HasUtil, "UTIL");

            if (!resultManguera.HasValue)
                resultManguera = ValidateBien(HasManguera, "MANGUERA");

            SendMailToWarning(UtilForced, MangeraForced);

            return resultUtil.Value & resultManguera.Value;
        }


        private void SendMailToWarning(bool UtilForced, bool MangeraForced)
        {
            if (UtilForced || MangeraForced)
            {
                StringBuilder message = new StringBuilder();
                message.AppendFormat("PRODUCTO {0} / {1}   FAMILIA: {2} ({3})  FASE: {4}   MODEL_LEADER: {5}", Model.NumProducto, Model.Version, Model.DescFamilia, Model.IdFamilia, Model.IdFase, Model.ModelLeader);
                message.AppendLine();

                if (UtilForced)
                {
                    message.AppendFormat("SE HA INTRODUICIDO EL BIEN {0} COMO UTIL PARA REALIZAR EL TEST CUANDO NO EXISTE O NO ES DEL TIPO ESPECIFICADO", Utillaje);
                    message.AppendLine();
                }

                if (MangeraForced)
                {
                    message.AppendFormat("SE HA INTRODUICIDO EL BIEN {0} COMO MANGUERA PARA REALIZAR EL TEST CUANDO NO EXISTE O NO ES DEL TIPO ESPECIFICADO", Manguera);
                    message.AppendLine();
                }

                SendMail.Send("AVISO BIENES INCORRECTOS EN LA ESTRUCTURA DEL PRODUCTO FASE", message + "\r\n", false, false, false, true);
            }
        }

        private void SendMailManteniment(int bien, int tipo)
        {
            StringBuilder message = new StringBuilder();
            message.AppendFormat("PRODUCTO {0} / {1}   FAMILIA: {2} ({3})  FASE: {4}   MODEL_LEADER: {5}", Model.NumProducto, Model.Version, Model.DescFamilia, Model.IdFamilia, Model.IdFase, Model.ModelLeader);
            message.AppendLine();
            message.AppendFormat("EL BIEN {0} ", bien);
            message.AppendLine();

            var dist = new List<string>();
            dist.Add("arodriguez@dezac.com");
            var para = "";

            if (tipo == 1)
            {
                message.AppendFormat("TIENE UNA ASISTENICIA ABIERTA");
                para = "TIENE UNA ASISTENICIA ABIERTA";
            }

            if (tipo == 2)
            {
                message.AppendFormat("NO TIENE UNA PROGRAMACION DE MANTENIMIENTO CREADA");
                para = "NO TIENE UNA PROGRAMACION DE MANTENIMIENTO CREADA";
            }

            if (tipo == 3)
            {
                message.AppendFormat("TIENE UNA PROGRAMACION DE MANTENIMIENTO CADUCADA");
                para = "TIENE UNA PROGRAMACION DE MANTENIMIENTO CADUCADA";
            }

            if (tipo == 4)
            {
                message.AppendFormat("TIENE UNA PROGRAMACION DE MANTENIMIENTO QUE EN 14 DIAS CADUCARÁ");
                para = "TIENE UNA PROGRAMACION DE MANTENIMIENTO QUE EN 14 DIAS CADUCARÁ";
            }

           // SendMail.Send(string.Format("AVISO BIEN {0} PARA {1}", bien, para), message + "\r\n", dist, null);
        }
    }
}
