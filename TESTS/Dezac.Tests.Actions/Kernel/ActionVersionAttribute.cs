﻿using System;

namespace Dezac.Tests
{
    public class ActionVersionAttribute : Attribute
    {
        public double Version { get; set; }

        public ActionVersionAttribute()
        {

        }

        public ActionVersionAttribute(double version)
        {
            Version = version;
        }
    }
}
