﻿using System.Collections.Generic;

namespace Dezac.Core.Enumerate
{
    public static class LabelTamplete
    {
        // Todas las plantillas de etiquetas el CODIGOBCN del artículo empieza por 00119 y luego vienen dos dígitos que identifican el tipo de etiqueta
        // que corresponden con los valores del enumerado
        public enum LabelTemplate
        {
            Caracteristicas = 0x01,
            EmbalajeIndividual = 0x02,
            EmbalajeConjunto = 0x03,
            NumSerie = 0x04,
            EtiquetasEsquema = 0x05,
            EtiquetaPromocional = 0x06
        }

        // Cada Plantilla tiene su grupo de parametrización
        // En este diccionario se establece el mapeo
        public static Dictionary<LabelTemplate, int> mapTipoGrupos = new Dictionary<LabelTemplate, int> {
            { LabelTemplate.Caracteristicas, 19 },
            { LabelTemplate.EmbalajeIndividual, 21 },
            { LabelTemplate.EmbalajeConjunto, 20 },
            { LabelTemplate.NumSerie, 22 },
            { LabelTemplate.EtiquetasEsquema, 23 },
            { LabelTemplate.EtiquetaPromocional, 25 },
        };
    }

    public static class EnumPrinters
    {
        public enum Printers
        {
            NINGUNA,
            IMPRESORA_1,
            IMPRESORA_2,
            IMPRESORA_3
        }

        public static Dictionary<Printers, string> RelationPrinterAndPrinterName = new Dictionary<Printers, string>()
        {
            { Printers.NINGUNA, "" },
            { Printers.IMPRESORA_1, "#1" },
            { Printers.IMPRESORA_2, "#2" },
            { Printers.IMPRESORA_3, "#3" }
        };


    }
}
