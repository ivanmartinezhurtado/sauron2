﻿namespace Comunications.Message
{
    public interface ITransportMessage
    {
        bool HasResponse { get; set; }
        byte[] MessageFrame { get; }
        int TransactionId { get; set; }
        void Initialize(byte[] frame);
        int SearchLastEndLine { get; set; }
        int? AnswerLen { get; set; }
    }
}
