﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using Dezac.Tests.Services;
using Instruments.PowerSource;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.42)]
    public class RGRN_LCTest : RGRNTestBase
    {
        new RG_RN_LC Rgrn;

        public override void TestInitialization()
        {    
            Rgrn = new RG_RN_LC(Comunicaciones.SerialPort);
            Rgrn.Modbus.BaudRate = Comunicaciones.BaudRate;
            Rgrn.Modbus.PerifericNumber = Comunicaciones.Periferico;
            Rgrn.Modbus.UseFunction06 = true;

            base.TestInitialization();
        }

        public override void TestInitLAC25Macros(bool rewrite = false)
        {
            bool Macros;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("LAC25_MACROS", out Macros) || rewrite)
            {
                Macros = AddOrGetSharedVar<bool>("LAC25_MACROS", () => { return true; });

                Tower.Actuator_LAC25.InitLAC25();

                List<double> ListPositions = new List<double>();
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_1", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_2", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_3", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_4", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_5", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_6", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_CORRIENTE_7", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_1", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_2", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_3", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_4", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_5", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_6", 0, ParamUnidad.Numero));
                ListPositions.Add(Configuracion.GetDouble("POSICION_SELECTOR_TIEMPO_7", 0, ParamUnidad.Numero));

                var pushTorque = (int)Configuracion.GetDouble("FUERZA_LINEAL", 12000, ParamUnidad.Numero);
                var rotationTorque = (int)Configuracion.GetDouble("FUERZA_ROTACION", 12000, ParamUnidad.Numero);

                Tower.Actuator_LAC25.WritePositions(ListPositions.ToArray(), 240000, 5000, rotationTorque, 40);

                Tower.Actuator_LAC25.WriteAndRunFindSelector(false, true, 30, 231072, 5243, 32767, 9243, 5243, pushTorque, pushTorque, 60000, 4500, rotationTorque);
                Tower.Actuator_LAC25.WriteGoFinalPosition(60000, 4500, rotationTorque);

                if (cacheSvc != null)
                    cacheSvc.Add("LAC25_MACROS", Macros);
            }
        }

        public override void TestCommunications()
        {
            string firmwareVersion = "0";

            SamplerWithCancel((p) => 
            {
                firmwareVersion = Rgrn.ReadFirmwareVersion(); return true;
            }, "", 1, 500, 0,  false, false,
            (s) =>
                {
                Error().UUT.COMUNICACIONES.INICIALIZACION(s).Throw();
            });

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE.ToString(), Error().PROCESO.PARAMETROS_ERROR.VERSION_FIRMWARE(), ParamUnidad.SinUnidad);
        }

        public override void TestToroidal()
        {
            Rgrn.WriteFlagTest();

            Tower.IO.DO.On(OUT_ABRE_TOROIDAL);

            Delay(50, "");

            SamplerWithCancel((p) =>
            {
                Rgrn.WriteTestToroidal();

                Delay(50, "");

                var state = Rgrn.ReadInputsState();

                return state == RG_RN_LC.Inputs.TORO_ERROR;
            }, "Error: El equipo no detecta la desconexion del toroidal", 5, 100, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.TOROIDAL(s).Throw();
            });

            Resultado.Set("TEST_TOROIDAL_DESCONECTADO", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.Off(OUT_ABRE_TOROIDAL);

            Delay(50, "");

            SamplerWithCancel((p) =>
            {
                Rgrn.WriteTestToroidal();

                Delay(50, "");

                var state = Rgrn.ReadInputsState();

                return state != RG_RN_LC.Inputs.TORO_ERROR;
            }, "Error: El equipo no detecta el toroidal conectado", 5, 100, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.TOROIDAL(s).Throw();
            });

            Resultado.Set("TEST_TOROIDAL_CONECTADO", "OK", ParamUnidad.SinUnidad);
        }

        public override void TestAdjust()
        {
            InitAdjust(false);

            Rgrn.WriteFlagTest();

            AdjustValueDef adjustValueGain = new AdjustValueDef(Params.GAIN_I.Null.Ajuste.Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(adjustValueGain, (step) =>
            {
                Rgrn.WriteWorkMode(RG_RN_LC.WorkModes.ADJUST);

                Delay(1500, "Espera a la calibración");

                var calibrationResultInfo = Rgrn.ReadCurrentGain();
                return calibrationResultInfo;
            }, 0, 1, 500);

            AdjustValueDef adjustValueCurrent = new AdjustValueDef(Params.I.Null.Ajuste.Name, 0, Params.I.Null.Ajuste.Min(), Params.I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(adjustValueCurrent, (step) =>
            {
                var currentRead = Rgrn.ReadCurrent();
                return currentRead;
            }, 0, 3, 1200);

            Tower.Actuator_LAC25.RunGoPosition(0);

            Tower.PTE.SetSource(PTE.SourceType.PTE_50_CE);
            Tower.PTE.ApplyOff();
        }

        public void TestCurrentSelector()
        {
            //base.TestCurrentSelector(() =>
            //{
            //    return Rgrn.ReadCurrentSelector();
            //}, 200, false);

            Dictionary<RG_RN_LC.CurrentSelectorPosition, AdjustValueDef> dictionaryPositionEscale = new Dictionary<RG_RN_LC.CurrentSelectorPosition, AdjustValueDef>()
            {
                { RG_RN_LC.CurrentSelectorPosition.ESC_7, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_7").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_7").Min(), Params.I.Null.Verificacion.TestPoint("ESC_7").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_1, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_1").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_1").Min(), Params.I.Null.Verificacion.TestPoint("ESC_1").Max(), 0, 0, ParamUnidad.Puntos)},
            };

            foreach (KeyValuePair<RG_RN_LC.CurrentSelectorPosition, AdjustValueDef> escala in dictionaryPositionEscale)
                SamplerWithCancel((s) =>
                {
                    try
                    {
                        if (escala.Key == RG_RN_LC.CurrentSelectorPosition.ESC_1)
                            Tower.Actuator_LAC25.GoInitialPosition();
                        else
                            Tower.Actuator_LAC25.GoFinalPosition();
                        
                        Delay(200, "Estabilizacion medida");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message + " en " + escala.Key.GetDescription());
                    }
                    finally
                    {
                        uint currentRead = 0;
                        TestMeasureBase(escala.Value,
                            (step) =>
                            {
                                currentRead = Rgrn.ReadCurrentSelector();
                                return currentRead;
                            }, 0, 3, 500);
                        Resultado.Set(String.Format("CORRIENTE_{0}", escala.Key.ToString()), "OK", ParamUnidad.SinUnidad);
                    }
                    return true;
                }, "", 2, 300, 0, false, false, null);

            this.InParellel(() =>
            {
                Tower.PTE.SetSource(PTE.SourceType.PTE_50_CE);
                Tower.PTE.ApplyOff();
            }, () =>
            {
                Tower.Actuator_LAC25.Home(false, true);
            });
        }

        public void TestTimeSelector()
        {
            //base.TestTimeSelector(() =>
            //{
            //    return Rgrn.ReadTimeSelector();
            //}, 200, false);

            Tower.IO.DO.Off(OUT_EV_ACTUADOR);
            SamplerWithCancel((p) => { return (!Tower.IO.DI[IN_POSICION_ACTUADOR_CORRIENTE] && Tower.IO.DI[IN_POSICION_ACTUADOR_TIEMPO]); }, "Error: No se ha detectado el sensor del actuador en posicion del selector de tiempo", 20, 100);

            Tower.Actuator_LAC25.WriteAndRunFindSelector();

            Delay(200, "Estabilizacion medida");

            var defRef = new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_REF").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_1").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_1").Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(defRef, (step) =>
            {
                return Rgrn.ReadTimeSelector();
            }, 0, 4, 500);

            Dictionary<RG_RN_LC.TimeSelectorPosition, AdjustValueDef> dictionaryPositionEscale = new Dictionary<RG_RN_LC.TimeSelectorPosition, AdjustValueDef>()
            {
                { RG_RN_LC.TimeSelectorPosition.ESC_7, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_7").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_7").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_7").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_1, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_1").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_1").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_1").Max(), 0, 0, ParamUnidad.Puntos)},
            };

            foreach (KeyValuePair<RG_RN_LC.TimeSelectorPosition, AdjustValueDef> escala in dictionaryPositionEscale)
                SamplerWithCancel((s) =>
                {
                    try
                    {
                        if (escala.Key == RG_RN_LC.TimeSelectorPosition.ESC_1)
                            Tower.Actuator_LAC25.GoInitialPosition();
                        else
                            Tower.Actuator_LAC25.GoFinalPosition();

                        Delay(200, "Estabilizacion medida");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message + " en " + escala.Key.GetDescription());
                    }
                    finally
                    {
                        ushort timeRead = 0;
                        TestMeasureBase(escala.Value, (step) =>
                        {
                            timeRead = Rgrn.ReadTimeSelector();
                            return timeRead;
                        }, 0, 4, 500);
                        Resultado.Set(String.Format("TIEMPO_{0}", escala.Key.ToString()), "OK", ParamUnidad.SinUnidad);
                    }
                    return true;
                }, "", 2, 300, 0, false, false, null);

            Tower.Actuator_LAC25.Home(false, true);
        }

        public void TestKeyboard()
        {
            Rgrn.WriteFlagTest();

            var keyboardTowerOutputsDictionary = new Dictionary<RG_RN_LC.Inputs, int>()
            {
                {RG_RN_LC.Inputs.RESET_KEY, OUT_EV_TECLA_RESET},
                {RG_RN_LC.Inputs.TEST_KEY, OUT_EV_TECLA_TEST},
            };

            foreach (KeyValuePair<RG_RN_LC.Inputs, int> key in keyboardTowerOutputsDictionary)
            {
                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OnWait(150, key.Value);
                    var input = Rgrn.ReadInputsState();
                    return input == key.Key;
                }, string.Format("Error: Tecla {0} no detectada al pulsarla", key.Key.GetDescription()), 3, 200, 0, false, false,
                (s) =>
                {
                    Error().UUT.HARDWARE.KEYBOARD(s).Throw();
                });

                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OffWait(150, key.Value);
                    var input = Rgrn.ReadInputsState();
                    return !keyboardTowerOutputsDictionary.ContainsKey(input);
                }, string.Format("Error: Tecla {0} encallada al desactivarla", key.Key.GetDescription()), 3, 200, 0, false, false,
                (s) =>
                {
                    Error().UUT.HARDWARE.KEYBOARD(s).Throw();
                });

                Resultado.Set(String.Format("KEYBOARD_{0}", key.Key.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestLeds()
        {
            CAMERA_IDS = GetVariable<string>("CAMARA_LEDS", CAMERA_IDS);

            Rgrn.WriteFlagTest();

            Rgrn.WriteLeds(RG_RN_LC.Leds.VERDE_POWER);

            Delay(100, "Espera encendido del LED");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "RG_RN_VERDE", "VERDE", "RG_RN", 1);

            Rgrn.WriteLeds(RG_RN_LC.Leds.ROJO_TRIP);

            Delay(100, "Espera encendido del LED");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "RG_RN_ROJO", "ROJO", "RG_RN", 1);
        }

        public void TestRelays()
        {
            Rgrn.WriteFlagTest();

            SamplerWithCancel((p) =>
            {
                Rgrn.WriteRelay(RG_RN_LC.Relay.RELAY_ON);
                Delay(50, "");
                return (Tower.IO.DI.Read(IN_RELAY_NO).Value && !Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error no se detecta la activacion del rele de disparo", 3, 500, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.RELE(s).Throw();
            });

            SamplerWithCancel((p) =>
            {
                Rgrn.WriteRelay(RG_RN_LC.Relay.RELAY_OFF);
                Delay(50, "");
                return (!Tower.IO.DI.Read(IN_RELAY_NO).Value && Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error no se detecta la desactivacion del rele de disparo", 3, 500, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.RELE(s).Throw();
            });

            Resultado.Set("TEST_RELE", "OK", ParamUnidad.SinUnidad);
        }

        public void TestVerification()
        {
            Rgrn.WriteWorkMode(RG_RN_LC.WorkModes.NORMAL);

            base.TestVerification(() =>
            {
                return Rgrn.ReadTripCurrent();
            });
        }

        public override void TestCostumization()
        {
            Rgrn.WriteFlagTest();

            var modelBBDD = new RG_RN_LC.ModelConfig();

            modelBBDD.ModelString = VectorHardware.GetString("MODELO", "RGE_R", ParamUnidad.SinUnidad);

            Rgrn.WriteModelConfiguration(modelBBDD);
            Delay(200, "");
            Rgrn.WriteNumSerie(Convert.ToUInt64(TestInfo.NumSerie));
            Delay(200, "");

            Rgrn.WriteNumBastidor((uint)TestInfo.NumBastidor);

            PowerSourceResetAndApplyPrsetes(() =>
            {
                SamplerWithCancel((p) =>
                {
                    var firmwareVersion = Rgrn.ReadFirmwareVersion(); return true;
                }, "Error: el equipo no arranca o no comunica despues del reset", 2, 500, 0, false, false, (s) =>
                {
                    Error().UUT.COMUNICACIONES.INICIALIZACION(s).Throw();
                });
                return true;
            });

            var model = Rgrn.ReadModelConfiguration();

            Assert.AreEqual("MODELO", modelBBDD.ModelString, model.ModelString, Error().UUT.FIRMWARE.SETUP("Error: el modelo leido no coincide con el de BBDD"), ParamUnidad.SinUnidad);

            Assert.AreEqual("NUMERO_SERIE", Rgrn.ReadNumSerie(), Convert.ToUInt64(TestInfo.NumSerie), Error().UUT.FIRMWARE.SETUP("Error: El numero de serie no se ha grabado correctamente"), ParamUnidad.Numero);

            Assert.AreEqual("NUMERO_BASTIDOR", (int)Rgrn.ReadNumBastidor(), (int)TestInfo.NumBastidor, Error().UUT.FIRMWARE.SETUP("Error: El numero de bastidor no se ha grabado correctamente"), ParamUnidad.Numero);
        }

        public override void TestFinish()
        {
            base.TestFinish();

            if (Rgrn != null)
                Rgrn.Dispose();
        }

    }
}