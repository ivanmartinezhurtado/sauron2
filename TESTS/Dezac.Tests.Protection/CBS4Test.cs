﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.27)]
    public class CBS4Test : TestBase
    {
        #region Entradas y salidas
        // Salidas de la manguera
        internal const byte OUT_SWITCH_COM5_TO_COM4 = 15;//
        internal const byte OUT_EXTERNAL_TRIGGER = 18; //
        internal const byte OUT_CIRCUITO_DC = 23;
        internal const byte OUT_CORRIENTE = 30;
        internal const byte OUT_ILUMINACION = 59;//

        // Salidas teclado
        internal const byte OUT_TECLA_TD = 6; //
        internal const byte OUT_TECLA_ID = 7;//
        internal const byte OUT_TECLA_STD = 8;//
        internal const byte OUT_TECLA_SET = 9;//
        internal const byte OUT_TECLA_PROG = 10;//
        internal const byte OUT_TECLA_TEST = 11;//
        internal const byte OUT_TECLA_RESET = 12;//

        // Salidas conexión de puntas al equipo 
        internal const byte OUT_ACTIVACION_ELECTROVALVULAS = 5;//
        internal const byte OUT_BORNERA_IZQUIERDA = 48;//
        internal const byte OUT_BORNERA_DERECHA = 49;//
        internal const byte OUT_PUNTAS_COM_SERIE = 50;//
        internal const byte OUT_PUNTAS_TTL = 51;//
        internal const byte OUT_PUNTAS_TTL_BORNES_17 = 52;//
        internal const byte OUT_PUNTAS_TTL_BORNES_18 = 53;//
        internal const byte OUT_PUNTAS_TTL_BORNES_19 = 54;//
        internal const byte OUT_PUNTAS_TTL_BORNES_20 = 55;//
        internal const byte OUT_PUNTAS_TTL_BORNES_21 = 56;//
        internal const byte OUT_FIJACION_CARRO = 57;//

        // Salidas USB Advantech
        internal const byte OUT_ADVANTECH_MODELO_CBS4 = 1;//
        internal const byte OUT_ADVANTECH_TORO_500 = 3;//
        internal const byte OUT_ADVANTECH_TORO_ABIERTO = 5;//
        internal const byte OUT_ADVANTECH_TORO_CRUZADO = 6;//
        internal const byte OUT_ADVANTECH_ACTIVA_TORO_CERCANOS = 7;//


        // Entradas de la manguera
        internal const byte IN_RELAY_1 = 11; //
        internal const byte IN_RELAY_2 = 12; //
        internal const byte IN_RELAY_3 = 9;//
        internal const byte IN_RELAY_4 = 10;//

        //Entradas presencia equipo 
        internal const byte BLOQ_SECURITY = 23;//
        internal const byte IN_UTIL_CERRADO = 24;//
        internal const byte IN_DETECT_UUT = 26;//
        internal const byte IN_DETECT_COVER = 25;//

        //Entradas Estado Pisto
        internal const byte IN_ESTADO_PUNTAS_TTL = 30;//
        internal const byte IN_ESTADO_PUNTAS_COM_SERIE = 31;//
        internal const byte IN_ESTADO_PUNTAS_DERECHA = 32;//
        internal const byte IN_ESTADO_PUNTAS_IZQUIERDA = 33;//

        // Entradas Mux
        internal const InputMuxEnum TOROIDAL_COMMON = InputMuxEnum.IN11; // Salida corriente
        internal const InputMuxEnum TOROIDAL_CHANNEL_1 = InputMuxEnum.IN2; //
        internal const InputMuxEnum TOROIDAL_CHANNEL_2 = InputMuxEnum.IN3;//
        internal const InputMuxEnum TOROIDAL_CHANNEL_3 = InputMuxEnum.IN4;//
        internal const InputMuxEnum TOROIDAL_CHANNEL_4 = InputMuxEnum.IN5;//
        internal const InputMuxEnum PREALARM_RELAY_INPUT = InputMuxEnum.IN14;//
        #endregion

        private SerialPort sp;

        private Tower3 Tower;
        private CBS4 Cbs;
        private string CAMERA_IDS;
        private USB4761 usbIO;
        public USB4761 UsbIO
        {
            get
            {
                if (usbIO == null)
                    usbIO = new USB4761();

                return usbIO;
            }
        }

        public void TestInitialization()
        {
            Cbs = new CBS4(Comunicaciones.SerialPort);
            Cbs.Modbus.BaudRate = Comunicaciones.BaudRate;
            Cbs.Modbus.PerifericNumber = Comunicaciones.Periferico;
            Cbs.Modbus.UseFunction06 = true;
           
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out Tower))
            {
                Tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", Tower);
            }

            Tower.MUX.Reset();
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);
            Tower.IO.DO.On(OUT_ACTIVACION_ELECTROVALVULAS);

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[IN_UTIL_CERRADO])
                    return false;
                return true;
            }, "Error: No se ha cerrado el utillaje.", 40, 500, 2500, false, false);

            Tower.IO.DO.On(OUT_FIJACION_CARRO);

            this.InParellel(() =>
            {
                Tower.PowerSourceIII.ApplyOff();
            },
            () =>
            {
                Tower.Chroma.ApplyOff();
            },
            () =>
            {
                Tower.LAMBDA.ApplyOff();
            });

            UsbIO.DO.OffRange(0, 6);
            UsbIO.DO.On(OUT_ADVANTECH_MODELO_CBS4);

            Tower.IO.DO.On(OUT_BORNERA_IZQUIERDA, OUT_BORNERA_DERECHA);
            Tower.IO.DI.WaitAllOn(2000, IN_ESTADO_PUNTAS_DERECHA, IN_ESTADO_PUNTAS_IZQUIERDA);

            bool ComBoard = Comunicaciones.SerialPort == 7 ? true : false; 

            if (ComBoard)
            {
                Tower.IO.DO.On(OUT_PUNTAS_COM_SERIE);
                Tower.IO.DI.WaitOn(IN_ESTADO_PUNTAS_COM_SERIE, 2000);
           }
            else
            {
                Tower.IO.DO.On(OUT_PUNTAS_TTL_BORNES_17, OUT_PUNTAS_TTL_BORNES_19, OUT_PUNTAS_TTL_BORNES_21);
                Tower.IO.DO.On(OUT_PUNTAS_TTL);
                Tower.IO.DI.WaitAllOn(2000, IN_ESTADO_PUNTAS_TTL);
                if (Comunicaciones.SerialPort == 5)
                    Tower.IO.DO.Off(OUT_SWITCH_COM5_TO_COM4);
                else
                    Tower.IO.DO.On(OUT_SWITCH_COM5_TO_COM4);
            }

            Assert.IsTrue(Tower.IO.DI[IN_DETECT_UUT], Error().HARDWARE.INSTRUMENTOS.DETECCION_EQUIPO("Error No se ha detectado el equipo o el equipo no lleva el ancla puesta"));

            Assert.IsTrue(Tower.IO.DI[IN_DETECT_COVER], Error().PROCESO.MATERIAL.FALTA_TAPA("Error El equipo no lleva la tapa puesta"));

            Tower.PowerSourceIII.Tolerance = 1;
        }

        public void TestConsumption([Description("Tiempo espera estabilización consumo")]int timeWait = 1000)
        {
            var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);
            if (typeSupply == "DC")
                Tower.IO.DO.On(OUT_CIRCUITO_DC);

            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 5,
                ResultName = "",
                TiempoIntervalo = 2000,
                typePowerSource = typeSupply == "AC" ? TypePowerSource.CHROMA : TypePowerSource.LAMBDA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = 0,
                WaitTimeReadMeasure = timeWait
            };
            this.TestConsumo(Tower, configuration);
        }

        public void TestCommunications()
        {

            if (Comunicaciones.Protocolo.ToUpper().Trim() == "JOHNSON")
                PowerSourceResetAndApplyPrsetes(); 

            SamplerWithCancel((p) => { Cbs.WriteWorkMode(CBS4.WorkModes.TEST); return true; }, "Error de comunicaciones, el equipo no arranca o no comunica", 3, 500, 500, false, false);

            CultureInfo enUS = new CultureInfo("en-US");

            double firmwareVersion = Cbs.ReadFirmwareVersion();
            TestInfo.FirmwareVersion = (firmwareVersion / 10).ToString("0.0", enUS);
            TestInfo.ProductoVersion = string.Format("{0}/{1}", Model.NumProducto, Model.Version);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, TestInfo.FirmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware incorrecta"), ParamUnidad.SinUnidad);

            if (Comunicaciones.SerialPort == 7)
            {
                double firmwareVersionPlacaComunicaciones = Cbs.ReadCommunicationsBoardVersion(Comunicaciones.Protocolo.ToUpper().Trim());
                var stringfirmwareVersionPlacaComunicaciones = (firmwareVersionPlacaComunicaciones / 10).ToString("0.0", enUS);
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_PLACA_COM, stringfirmwareVersionPlacaComunicaciones, Identificacion.VERSION_FIRMWARE_PLACA_COM, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware placa comunicaciones incorrecta"), ParamUnidad.SinUnidad);
            }
        }

        public void TestLeds()
        {
            var timeWait = 1000;

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

            Cbs.WriteDisplay(CBS4.DisplayOperations.ALL_SEGMENTS);

            Delay(timeWait, "Espera condicionamiento de los LEDS");

            Cbs.WriteLeds(CBS4.Leds.TURN_OFF_BACKLIGHT_POWER_PREALARM_LEDS);

            Delay(timeWait, "Espera condicionamiento de los LEDS");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "CBS4_LED_VERDE", "LED_VERDE", "CBS4");

            Cbs.WriteLeds(CBS4.Leds.PREALARM_LED_RED_POWER_LED_RED_BACKLIGHT);

            Delay(timeWait, "Espera condicionamiento de los LEDS");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "CBS4_LED_ROJO_NARANJA", "LED_ROJO_NARANJA", "CBS4");

            Cbs.WriteDisplay(CBS4.DisplayOperations.NONE_SEGMENTS);
        }

        public void TestDisplay()
        {
            var timeWait = 1500;

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

            Cbs.WriteLeds(CBS4.Leds.TURN_OFF_BACKLIGHT_POWER_PREALARM_LEDS);

            Delay(200, "");

            Cbs.WriteDisplay(CBS4.DisplayOperations.NONE_SEGMENTS);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CBS4_OFF_SEGMENTS", "OFF_SEGMENTS", "CBS4");

            Cbs.WriteDisplay(CBS4.DisplayOperations.EVEN_SEGMENTS);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CBS4_EVEN_SEGMENTS", "EVEN_SEGMENTS", "CBS4");

            Cbs.WriteDisplay(CBS4.DisplayOperations.ODD_SEGMENTS);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CBS4_ODD_SEGMENTS", "ODD_SEGMENTS", "CBS4");

            Cbs.WriteDisplay(CBS4.DisplayOperations.ALL_SEGMENTS);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CBS4_ALL_SEGMENTS", "ALL_SEGMENTS", "CBS4");
        }


        public void TestToroidal()
        {
            Dictionary<CBS4.Channel, CBS4.Inputs> deviceChannelsDeviceInputsValues = new Dictionary<CBS4.Channel, CBS4.Inputs>()
            {
                {CBS4.Channel.CHANNEL_4, CBS4.Inputs.TOROIDAL_ERROR_INPUT_4},
                {CBS4.Channel.CHANNEL_3, CBS4.Inputs.TOROIDAL_ERROR_INPUT_3},
                {CBS4.Channel.CHANNEL_2, CBS4.Inputs.TOROIDAL_ERROR_INPUT_2},
                {CBS4.Channel.CHANNEL_1, CBS4.Inputs.TOROIDAL_ERROR_INPUT_1}
            };

            Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);
            var posicionI = (ushort)Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 0, ParamUnidad.SinUnidad);
            var posicionT = (ushort)Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 0, ParamUnidad.SinUnidad);

            foreach (CBS4.Channel channel in Enum.GetValues(typeof(CBS4.Channel)))
            {
                Cbs.WriteTriggerCurrentSelectionChannel(channel, posicionI);
                Cbs.WriteTriggerTimeSelectionChannel(channel, posicionT);
            }

            Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);

            UsbIO.DO.On(OUT_ADVANTECH_ACTIVA_TORO_CERCANOS);

            Cbs.WriteWorkMode(CBS4.WorkModes.NORMAL);

            CBS4.Inputs inputsState = CBS4.Inputs.NO_INPUT;

            UsbIO.DO.Off(OUT_ADVANTECH_TORO_ABIERTO, OUT_ADVANTECH_TORO_CRUZADO);

            var toroidalOk_Aux = new List<CBS4.Inputs>()
                {
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_1,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_2,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_3,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_4,
                };

            SamplerWithCancel((p) =>
            {
                Cbs.WriteChannelManagement(CBS4.ChannelManagement.TRIGGER_ALL);
                Delay(500, "");
                Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);
                Delay(2000, "");
                inputsState = Cbs.ReadInputsState();
                toroidalOk_Aux.RemoveAll(x => !inputsState.HasFlag(x));
                return toroidalOk_Aux.Count.Equals(0);
            }, "Error: El equipo no ha podido hacer un reset al error de toroidal ", 3, 1500, 0);

            var toroidalOk = new List<CBS4.Inputs>()
                {
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_1,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_2,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_3,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_4,
                };
            var toroidalKO = new List<CBS4.Inputs>()
                {
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_1,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_2,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_3,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_4,
                };
            
            SamplerWithCancel((p) =>
            {
                inputsState = Cbs.ReadInputsState();
                toroidalOk.RemoveAll(x => inputsState.HasFlag(x));
                toroidalKO.RemoveAll(x => !inputsState.HasFlag(x));
                if (toroidalOk.Count() == 4)
                    return true;
                else
                    throw new Exception(string.Format("Error, no se ha detectado el/los {0} cuando estan conectados", string.Join<string>(", ", toroidalKO.ConvertAll<string>(x => x.GetDescription()))));
            },"", 3, 5000, 20000, false, false);

            Resultado.Set("TOROIDAL_CHANNEL_1_PRESENCE_DETECTION", "OK", ParamUnidad.SinUnidad);
            Resultado.Set("TOROIDAL_CHANNEL_2_PRESENCE_DETECTION", "OK", ParamUnidad.SinUnidad);
            Resultado.Set("TOROIDAL_CHANNEL_3_PRESENCE_DETECTION", "OK", ParamUnidad.SinUnidad);
            Resultado.Set("TOROIDAL_CHANNEL_4_PRESENCE_DETECTION", "OK", ParamUnidad.SinUnidad);

            //*********************************************************************************       

            UsbIO.DO.Off(OUT_ADVANTECH_ACTIVA_TORO_CERCANOS);
            UsbIO.DO.On(OUT_ADVANTECH_TORO_500);

            if (Identificacion.MODELO != "CC")
            {
                Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);

                Delay(500, "Espera desconexion");

                UsbIO.DO.On(OUT_ADVANTECH_TORO_ABIERTO);

                toroidalOk = new List<CBS4.Inputs>()
                {
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_1,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_2,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_3,
                    CBS4.Inputs.TOROIDAL_ERROR_INPUT_4,
                };

                SamplerWithCancel((s) =>
                {
                    if (s % 5 == 4)
                    {
                        Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_CHANNEL_1);
                        Delay(500, "Espera el reset de toro");
                    }

                    inputsState = Cbs.ReadInputsState();

                    toroidalOk.RemoveAll(x => inputsState.HasFlag(x));

                    if (toroidalOk.Count().Equals(0))
                        return true;
                    else
                        throw new Exception(string.Format("Error, no responde error de toroidal en el/los {0} cuando estan desconectados", string.Join<string>(", ", toroidalOk.ConvertAll<string>(x => x.GetDescription()))));

                }, "", 40, 1500, 2000, false, false);

                Resultado.Set("TOROIDAL_ALL_CHANNELS_NO_PRESENCE_DETECTION", "OK", ParamUnidad.SinUnidad);

                UsbIO.DO.Off(OUT_ADVANTECH_TORO_ABIERTO);             
            }

            // Se inyecta corriente linea 1, que pasa solo por el toro 1 y 3
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(Consignas.GetDouble(Params.I.Null.TestPoint("TOROIDAL").Name, 0, ParamUnidad.A), 0, 0), 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);

            var adjustValueCurrentList = new List<AdjustValueDef>();
            foreach (var deviceChannel in deviceChannelsDeviceInputsValues)
                adjustValueCurrentList.Add(new AdjustValueDef(Params.I.Other(string.Format("TOROIDAL_{0}", deviceChannel.Key.ToString())).Verificacion.TestPoint("TOROIDAL").Name, 0, Params.I.Other(string.Format("TOROIDAL_{0}", deviceChannel.Key.ToString())).Verificacion.TestPoint("TOROIDAL").Min(), Params.I.Other(string.Format("TOROIDAL_{0}", deviceChannel.Key.ToString())).Verificacion.TestPoint("TOROIDAL").Max(), 0, 0, ParamUnidad.A));

            var adjustResult = TestMeasureBase(adjustValueCurrentList, (s) =>
            {
                Delay(400, "");
                Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);
                Delay(1500, "");

                var resultList = new List<double>();
                foreach (var deviceChannel in deviceChannelsDeviceInputsValues)
                    resultList.Add(Cbs.ReadTriggerCurrentRMSValueChannel(deviceChannel.Key));

                return resultList.ToArray();
            }, 0, 8, 200);
          

            UsbIO.DO.Off(OUT_ADVANTECH_TORO_500);
        }

        public void TestKeyboard()
        {
            var keyboardTowerOutputsDictionary = new Dictionary<CBS4.Keyboard, int?>()
            {
                {CBS4.Keyboard.SET_KEY, OUT_TECLA_SET},
                {CBS4.Keyboard.CURRENT_SELECTION_KEY, OUT_TECLA_ID},
                {CBS4.Keyboard.POLARITY_SELECTION_KEY, OUT_TECLA_STD},
                {CBS4.Keyboard.PROG_KEY, OUT_TECLA_PROG},
                {CBS4.Keyboard.RESET_KEY, OUT_TECLA_RESET},
                {CBS4.Keyboard.TEST_KEY, OUT_TECLA_TEST},
                {CBS4.Keyboard.TIME_SELECTION_KEY, OUT_TECLA_TD},
            };

            Tower.IO.DO.Off(OUT_TECLA_SET, OUT_TECLA_ID, OUT_TECLA_PROG, OUT_TECLA_RESET, OUT_TECLA_STD, OUT_TECLA_TD, OUT_TECLA_TEST);

            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

            Cbs.WriteRelays(CBS4.Relays.TURN_OFF_RELAYS);

            Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);

            foreach (KeyValuePair<CBS4.Keyboard, int?> keyboard in keyboardTowerOutputsDictionary)
                SamplerWithCancel((p) =>
                {
                    if (keyboard.Value.HasValue)
                        Tower.IO.DO.OnWait(150, keyboard.Value.Value);

                    var result = keyboard.Key == Cbs.ReadKeyboard();

                    if (keyboard.Value.HasValue)
                        Tower.IO.DO.OffWait(150, keyboard.Value.Value);

                    var result2 = Cbs.ReadKeyboard() == CBS4.Keyboard.NO_INPUT;

                    Resultado.Set(keyboard.Key.ToString(), result == true ? "OK" : "KO", ParamUnidad.SinUnidad);

                    return result && result2;

                }, string.Format("Error tecla {0} no detectada al activarla o encallada al desactivarla", keyboard.Key.GetDescription()));
        }

        public void TestExternalInput()
        {
            SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OffWait(200, OUT_EXTERNAL_TRIGGER);

                    var externalInput = Cbs.ReadInputsState();

                    return !externalInput.HasFlag(CBS4.Inputs.EXTERNAL_INPUT_ERROR);
                }, "Error la entrada externa se encuentra activada cuando deberia estar desactivada", cancelOnException: false, throwException: false);

            SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OnWait(200, OUT_EXTERNAL_TRIGGER);

                    var externalInput = Cbs.ReadInputsState();

                    return externalInput.HasFlag(CBS4.Inputs.EXTERNAL_INPUT_ERROR);
                }, "Error no se ha activado la entrada externa", cancelOnException: false, throwException: false);

            Tower.IO.DO.OffWait(200, OUT_EXTERNAL_TRIGGER);

            Resultado.Set("EXTERNAL_INPUT", "OK", ParamUnidad.SinUnidad);
        }

        public void TestRelays()
        {
            Dictionary<CBS4.Relays, int> deviceOutputsTowerInputsRelationship = new Dictionary<CBS4.Relays, int>
            {
                {CBS4.Relays.TRIGGER_RELAY_CHANNEL_1, IN_RELAY_1},
                {CBS4.Relays.TRIGGER_RELAY_CHANNEL_2, IN_RELAY_2},
                {CBS4.Relays.TRIGGER_RELAY_CHANNEL_3, IN_RELAY_3},
                {CBS4.Relays.TRIGGER_RELAY_CHANNEL_4, IN_RELAY_4},
            };

            List<int> listInputsRelays = new List<int>();
            listInputsRelays.Add(IN_RELAY_1);
            listInputsRelays.Add(IN_RELAY_2);
            listInputsRelays.Add(IN_RELAY_3);
            listInputsRelays.Add(IN_RELAY_4);

            var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);
            MagnitudsMultimeter typeMultimeter = typeSupply == "AC" ? MagnitudsMultimeter.VoltAC : MagnitudsMultimeter.VoltDC;

            var result = Parametrizacion.GetString("SEGURIDAD_RELE_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR"; 

            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

            Cbs.WriteRelays(CBS4.Relays.TRIGGER_ALL_RELAY);
            Delay(500, "");
            Cbs.WriteRelays(CBS4.Relays.TURN_OFF_RELAYS);
            Delay(500, "Espera accionamiento relés");

            foreach (var deviceOutputTowerInput in deviceOutputsTowerInputsRelationship)
            {
                Cbs.WriteRelays(deviceOutputTowerInput.Key);

                Delay(400, "");

                listInputsRelays.Remove(deviceOutputTowerInput.Value);

                foreach(int input in listInputsRelays)
                    SamplerWithCancel((p) =>
                    {
                        return Tower.IO.DI.Read(input).Value != result;
                    }, string.Format("Error al comprobar el {0} porque tambien se ha activado el {1}", deviceOutputTowerInput.Key.GetDescription(), deviceOutputsTowerInputsRelationship.FirstOrDefault(x => x.Value == input).Key.GetDescription() ), 5, 200);

                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI.Read(deviceOutputTowerInput.Value).Value == result;
                }, string.Format("Error al comprobar el {0}, no se detecta su activación", deviceOutputTowerInput.Key.GetDescription()),5,200);

                Cbs.WriteRelays(CBS4.Relays.TURN_OFF_RELAYS);

                SamplerWithCancel((p) =>
                {
                    return !Tower.IO.DI.Read(deviceOutputTowerInput.Value).Value == result;
                }, string.Format("Error al comprobar el {0}, no se detecta su desactivación", deviceOutputTowerInput.Key.GetDescription()),5,500);

                listInputsRelays.Add(deviceOutputTowerInput.Value);

                Resultado.Set(deviceOutputTowerInput.Key.ToString().Replace("TRIGGER_", string.Empty).Replace("CHANNEL_", string.Empty), "OK", ParamUnidad.SinUnidad);
            }

            Cbs.WriteRelays(CBS4.Relays.PREALARM_RELAY);

            var preAlarmRelayCheckInformation = new AdjustValueDef(Params.V.Null.TestPoint("PREALARM_RELAY_ON").Name, 0, 0, 0, Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V), 15, ParamUnidad.V);

            TestMeasureBase(preAlarmRelayCheckInformation, (step) =>
                {
                    return Tower.MeasureMultimeter(PREALARM_RELAY_INPUT, typeMultimeter);
                }, 0, 4, 500);

            Cbs.WriteRelays(CBS4.Relays.TURN_OFF_RELAYS);

            preAlarmRelayCheckInformation = new AdjustValueDef(Params.V.Null.TestPoint("PREALARM_RELAY_OFF").Name, 0, 0, 100, 0, 0, ParamUnidad.V);

            TestMeasureBase(preAlarmRelayCheckInformation, (step) =>
            {
                return Tower.MeasureMultimeter(PREALARM_RELAY_INPUT, typeMultimeter);
            }, 0, 4, 500);

            Resultado.Set("PREALARM_RELAY", "OK", ParamUnidad.SinUnidad);
        }

        public void TestSetupDefault()
        {
            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

            var currentScaleTableWriting = GetCurrentScaleTable(Parametrizacion.GetString("TABLA_I_DISPARO", "0;30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad));

            var timeScaleTableWriting = GetTimeScaleTable(Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad));

            var serialNumber = Cbs.ReadSerialNumber();

            if ((serialNumber != 0xFFFFFFFF) || (Parametrizacion.GetString("FORZAR_TABLAS","NO",ParamUnidad.SinUnidad).ToUpper().Trim()=="SI"))
            {
                Cbs.WriteTableClear(CBS4.TablesClearOrders.DELETE_CURRENT_TIME_TABLES);

                Cbs.WriteTableClear(CBS4.TablesClearOrders.DELETE_RECONNECTION_TABLES);

                Delay(2000, "Espera para el borrado de las tablas");

                Cbs.WriteScaleTable(currentScaleTableWriting, CBS4.TableKind.CURRENT);
                Cbs.WriteScaleTable(timeScaleTableWriting, CBS4.TableKind.TIME);
            }

            if (Identificacion.MODELO == "CC")
            {
                Cbs.WriteErrorCodeCC(0);
                Cbs.WriteBastidorNumber((uint)TestInfo.NumBastidor.Value);
            }
            else
                Cbs.WriteErrorCode(0xFFFF);

            var currentDeviceTable = Cbs.ReadScaleTable(CBS4.TableKind.CURRENT);

            Assert.AreEqual("TABLA_CORRIENTES", currentScaleTableWriting.listElementsString.Trim(';'), currentDeviceTable.Trim(';'), Error().UUT.CONSUMO.SETUP("Error al comprobar la lectura de la tabla de corrientes"), ParamUnidad.SinUnidad);

            var timeDeviceTable = Cbs.ReadScaleTable(CBS4.TableKind.TIME);

            Assert.AreEqual("TABLA_TIEMPOS", timeScaleTableWriting.listElementsString.Trim(';'), timeDeviceTable.Trim(';'), Error().UUT.CONSUMO.SETUP("Error al comprobar la lectura de la tabla de tiempos"), ParamUnidad.SinUnidad);

            Cbs.WriteWorkMode(CBS4.WorkModes.NORMAL);

            Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);

            var rele2 = Parametrizacion.GetString("SEGURIDAD_RELES", "ESTANDAR", ParamUnidad.SinUnidad);

            var releSecurity = CBS4.RelayOutputsPolarity.ESTANDAR;

            Enum.TryParse<CBS4.RelayOutputsPolarity>(rele2, out releSecurity);

            Cbs.WriteRelayOutputsPolarity(releSecurity);

            Resultado.Set("SEGURIDAD_RELES", rele2, ParamUnidad.SinUnidad);

            Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);

        }

        public void TestAdjust()
        {
            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            UsbIO.DO.Off(OUT_ADVANTECH_TORO_ABIERTO, OUT_ADVANTECH_TORO_CRUZADO, OUT_ADVANTECH_TORO_500);

            Dictionary<CBS4.ScaleCalibrations, TriLineValue> scaleCalibrationInfo = new Dictionary<CBS4.ScaleCalibrations, TriLineValue>()
            {
                {CBS4.ScaleCalibrations.SCALE_1, TriLineValue.Create(Consignas.GetDouble(Params.I.L1.TestPoint("ESCALA_1").Name, 0, ParamUnidad.A),  0,  0)},
                {CBS4.ScaleCalibrations.SCALE_2, TriLineValue.Create(Consignas.GetDouble(Params.I.L1.TestPoint("ESCALA_2").Name, 0, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.TestPoint("ESCALA_2").Name, 0, ParamUnidad.A), 0)},
                {CBS4.ScaleCalibrations.SCALE_3, TriLineValue.Create(Consignas.GetDouble(Params.I.L1.TestPoint("ESCALA_3").Name, 0, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.TestPoint("ESCALA_3").Name, 0, ParamUnidad.A), 0)},
            };

            if (Identificacion.MODELO != "CC")
                scaleCalibrationInfo.Add(CBS4.ScaleCalibrations.SCALE_4, TriLineValue.Create(0, Consignas.GetDouble(Params.I.L2.TestPoint("ESCALA_4").Name, 0, ParamUnidad.A), 0));

            foreach (var scale in scaleCalibrationInfo)
            {
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, scale.Value, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

                CBS4.ScaleCalibrationResults calibrationResultInfo = new CBS4.ScaleCalibrationResults();
                var minimo1 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_1",scale.Key.ToString())).Min();
                var maximo1 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_1",scale.Key.ToString())).Max();

                var minimo2 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_2", scale.Key.ToString())).Min();
                var maximo2 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_2", scale.Key.ToString())).Max();

                var minimo3 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_3", scale.Key.ToString())).Min();
                var maximo3 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_3", scale.Key.ToString())).Max();

                var minimo4 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_4", scale.Key.ToString())).Min();
                var maximo4 = Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_4", scale.Key.ToString())).Max();

                var scaleCalibrationMarginsInformation = new List<AdjustValueDef>()
                {
                   new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_1",scale.Key.ToString())).Name, 
                    0, minimo1, maximo1, 0, 0, ParamUnidad.Puntos),
                   new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_2",scale.Key.ToString())).Name, 
                    0, minimo2, maximo2, 0, 0, ParamUnidad.Puntos),
                   new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_3",scale.Key.ToString())).Name, 
                    0, minimo3, maximo3, 0, 0, ParamUnidad.Puntos),
                   new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(string.Format("{0}_{1}", "CANAL_4",scale.Key.ToString())).Name, 
                    0, minimo4, maximo4, 0, 0, ParamUnidad.Puntos),
                };

                TestMeasureBase(scaleCalibrationMarginsInformation, (step) =>
                {
                    Cbs.WriteStartCalibration(scale.Key);

                    Delay(3000, "Espera fin de calibración");

                    calibrationResultInfo = Cbs.ReadCalibrationResultsAllChannels();

                    calibrationResultInfo = Identificacion.MODELO != "CC" ? (calibrationResultInfo / (uint)1600) : calibrationResultInfo.Raiz();

                    return calibrationResultInfo.ToDoubleArray();

                }, 0, 4, 500);

                Cbs.WriteCalibrationConstantAllChannels(calibrationResultInfo, scale.Key);
            }
        }

        public void TestVerification()
        {
            UsbIO.DO.Off(OUT_ADVANTECH_TORO_ABIERTO, OUT_ADVANTECH_TORO_CRUZADO, OUT_ADVANTECH_TORO_500);

            Delay(500, "esperando apagado reles");

            Dictionary<CBS4.Channel, InputMuxEnum> dictionaryChannelsMultiplexer = new Dictionary<CBS4.Channel, InputMuxEnum>()
            {
                {CBS4.Channel.CHANNEL_1, TOROIDAL_CHANNEL_1},
                {CBS4.Channel.CHANNEL_2, TOROIDAL_CHANNEL_2},
                {CBS4.Channel.CHANNEL_3, TOROIDAL_CHANNEL_3},
                {CBS4.Channel.CHANNEL_4, TOROIDAL_CHANNEL_4}
            };

            this.InParellel(() =>
            {
                PowerSourceResetAndApplyPrsetes();
                Cbs.WriteWorkMode(CBS4.WorkModes.NORMAL);
                
                Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);
                var posicionI = (ushort)Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 0, ParamUnidad.SinUnidad);
                var posicionT = (ushort)Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 0, ParamUnidad.SinUnidad);

                foreach (CBS4.Channel channel in Enum.GetValues(typeof(CBS4.Channel)))
                {
                    Cbs.WriteTriggerCurrentSelectionChannel(channel, posicionI);
                    Cbs.WriteTriggerTimeSelectionChannel(channel, posicionT);
                }

                Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);

                Delay(500, "");
            },
            () =>
            {
                Tower.IO.DO.On(OUT_CORRIENTE);
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(0, 0, Consignas.GetDouble(Params.I.Null.TestPoint("TRIGGER").Name, 0, ParamUnidad.A)), 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
                UsbIO.DO.On(OUT_ADVANTECH_TORO_500);
            },
            () =>
            {
                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.2);
                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                if (Parametrizacion.GetString("SEGURIDAD_RELES", "ESTANDAR", ParamUnidad.SinUnidad) == Parametrizacion.GetString("SEGURIDAD_RELE_HW", "ESTANDAR", ParamUnidad.SinUnidad))
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
                else
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
            }, 
            () =>
            {
                Tower.MUX.Connect2((int)TOROIDAL_COMMON, Outputs.FreCH1, (int)TOROIDAL_CHANNEL_1, Outputs.FreCH2);
            });

            foreach (var deviceChannelTowerOutput in dictionaryChannelsMultiplexer)
            {
                Tower.MUX.Connect2((int)TOROIDAL_COMMON, Outputs.FreCH1, (int)dictionaryChannelsMultiplexer[deviceChannelTowerOutput.Key], Outputs.FreCH2);

                SamplerWithCancel((p) =>
                {
                    Cbs.WriteChannelManagement(CBS4.ChannelManagement.TRIGGER_ALL);
                    Delay(500, "");
                    Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);
                    return Parametrizacion.GetString("SEGURIDAD_RELES", "ESTANDAR", ParamUnidad.SinUnidad) == Parametrizacion.GetString("SEGURIDAD_RELE_HW", "ESTANDAR", ParamUnidad.SinUnidad) ?
                        !Tower.IO.DI.Read(IN_RELAY_1).Value && !Tower.IO.DI.Read(IN_RELAY_2).Value && !Tower.IO.DI.Read(IN_RELAY_3).Value && !Tower.IO.DI.Read(IN_RELAY_4).Value : 
                        Tower.IO.DI.Read(IN_RELAY_1).Value && Tower.IO.DI.Read(IN_RELAY_2).Value && Tower.IO.DI.Read(IN_RELAY_3).Value && Tower.IO.DI.Read(IN_RELAY_4).Value;
                }, "Error no se han desactivado los reles de disparo", 5, 200);
              
                var adjustValue = new AdjustValueDef(Params.TIME.Other(string.Format("TRIGGER_{0}", deviceChannelTowerOutput.Key.ToString())).Verificacion.esc_1.Name, 0, Params.TIME.Other("TRIGGER").Verificacion.esc_1.Min(), Params.TIME.Other("TRIGGER").Verificacion.esc_1.Max(), 0, 0, ParamUnidad.ms);

                Delay(500, "Espera");

                var adjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    try
                    {
                        var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 5000, HP53131A.Channels.CH1,() =>
                        {
                            Delay(500, "Espera disparo");

                            logger.InfoFormat("Rele1:{0}", Tower.IO.DI.Read(IN_RELAY_1).Value);
                            logger.InfoFormat("Rele2:{0}", Tower.IO.DI.Read(IN_RELAY_2).Value);
                            logger.InfoFormat("Rele3:{0}", Tower.IO.DI.Read(IN_RELAY_3).Value);
                            logger.InfoFormat("Rele4:{0}", Tower.IO.DI.Read(IN_RELAY_4).Value);

                            Tower.IO.DO.Off(OUT_CORRIENTE);
                        });
                        
                        return (timeResult.Value * 1000); ;
                    }
                    catch (Exception)
                    {
                        var status = Cbs.ReadOutputStatus();
                        logger.InfoFormat("Satatus de disparo:{0}", status);

                        logger.InfoFormat("Rele1:{0}", Tower.IO.DI.Read(IN_RELAY_1).Value);
                        logger.InfoFormat("Rele2:{0}", Tower.IO.DI.Read(IN_RELAY_2).Value);
                        logger.InfoFormat("Rele3:{0}", Tower.IO.DI.Read(IN_RELAY_3).Value);
                        logger.InfoFormat("Rele4:{0}", Tower.IO.DI.Read(IN_RELAY_4).Value);

                        if (status == CBS4.OutputsStatus.COIL_CHANNEL_ALL)
                        {
                            var current = Cbs.ReadTriggerCurrentRMSValueChannel(deviceChannelTowerOutput.Key);
                            logger.InfoFormat("Corriente de disparo:{0}", current);

                            if (current > Params.I.Other("TRIGGER").Verificacion.esc_1.Min())
                                throw new Exception(string.Format("Error, el equipo ha disparado por el {0} pero el frecuencimetro no lo ha detectado", deviceChannelTowerOutput.Key.GetDescription()));
                            else
                                throw new Exception(string.Format("Error, el equipo ha disparado por el {0} pero no se ha detectado corriente", deviceChannelTowerOutput.Key.GetDescription()));
                        }
                        else
                        {
                            var current = Cbs.ReadTriggerCurrentRMSValueChannel(deviceChannelTowerOutput.Key);
                            logger.InfoFormat("Corriente de disparo:{0}", current);
                            if (current > Params.I.Other("TRIGGER").Verificacion.esc_1.Min())
                                throw new Exception(string.Format("Error, el equipo no ha disparado por el {0} pero hay corriente de disparo ", deviceChannelTowerOutput.Key.GetDescription()));
                            else
                                throw new Exception(string.Format("Error, el equipo no ha disparado por el {0}", deviceChannelTowerOutput.Key.GetDescription()));
                        }
                    }
                }, 0, 1, 1000);


                Delay(1000, "Espera");

                var adjustValueCurrent = new AdjustValueDef(Params.I.Other(string.Format("TRIGGER_{0}", deviceChannelTowerOutput.Key.ToString())).Verificacion.esc_1.Name, 0, Params.I.Other("TRIGGER").Verificacion.esc_1.Min(), Params.I.Other("TRIGGER").Verificacion.esc_1.Max(), 0, 0, ParamUnidad.mA);

                adjustResult = TestMeasureBase(adjustValueCurrent, (s) =>
                {
                    Delay(400, "");
                    Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);
                    Delay(1500, "");
                    return Cbs.ReadTriggerCurrentRMSValueChannel(deviceChannelTowerOutput.Key);
                }, 0, 8, 200);

                Tower.IO.DO.On(OUT_CORRIENTE);
            }
        }

        public void TestWriteReconectionTable()
        {          
            if (Identificacion.MODELO == "RA")
            {
                Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

                Cbs.WriteTableClear(CBS4.TablesClearOrders.DELETE_RECONNECTION_TABLES);

                WriteCustomizeReconectionTable();               
            }

            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

            var model = Identificacion.MODELO == "RA" ? CBS4.ModelType.RA : CBS4.ModelType.STD;

            Cbs.WriteModel(model);

            PowerSourceResetAndApplyPrsetes();
        }

        public void TestVerificationReconectionTable()
        {
            if (Identificacion.MODELO == "RA")
            {
                Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

                VerificationCustomizeReconectionTable();

                foreach (CBS4.Channel channel in Enum.GetValues(typeof(CBS4.Channel)))
                    SamplerWithCancel((p) =>
                    {
                        return Cbs.ReadReconnectionSequenceSelectionChannel(channel) == 1;
                    }, string.Format("Error, no se ha grabado bien la tabla de reconecxion del canal {0}", channel), 2);
            }
            
        }

        public void TestCodigoEmbalageCliente()
        {
            var labelBox = Parametrizacion.GetString("ETIQUETA_EMABALJE", "NO", ParamUnidad.SinUnidad);
            if (labelBox != "NO")
            {
                string barcode = "";
                SamplerWithCancel((p) =>
                {
                    var barcodeInput = new InputKeyBoard("Leer código de barras de la etiqueta del embalaje");
                    Shell.ShowDialog("INICIO TEST", () => barcodeInput, MessageBoxButtons.OKCancel);

                    if (barcodeInput.TextValue.ToUpper().Trim() != labelBox.ToUpper().Trim())
                    {
                        Shell.MsgBox("Error. código de barras del embalaje leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DE EMBALAJE INCORRECTO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }

                    barcode = barcodeInput.TextValue;
                    return true;

                }, "Error no se ha leido el código de barras del embalaje", 2);

                if (barcode.ToUpper().Trim() != labelBox.ToUpper().Trim())
                    throw new Exception("Error. código de barras del embalaje leido no coincide con el de la BBDD");
            }
        }

        public void TestCodigoLaser()
        {
            string barcode = "";
            var barcodeInputSemana = 99;
            SamplerWithCancel((p) =>
            {
                var barcodeInput = new InputKeyBoard("Leer código de barras del laser del equipo");
                Shell.ShowDialog("COMPROBACION LASER", () => barcodeInput, MessageBoxButtons.OKCancel);

                if (barcodeInput.TextValue.Length != 6)
                {
                    Shell.MsgBox("Error. código de barras del laser leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DEL MARCADO LASER", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                barcodeInputSemana = Int32.Parse( string.Format("{0}{1}",barcodeInput.TextValue[2], barcodeInput.TextValue[3]));

                if (!barcodeInput.TextValue.ToUpper().Trim().StartsWith("ZR") || barcodeInputSemana > 53)
                {
                    Shell.MsgBox("Error. código de barras del laser leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DEL MARCADO LASER", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                barcode = barcodeInput.TextValue;
                return true;

            }, "Error no se ha leido el código de barras del laser", 2);

            Resultado.Set("CODIGO_ZR", barcode, ParamUnidad.SinUnidad);

            if (!barcode.ToUpper().Trim().StartsWith("ZR") || barcode.Length != 6 || barcodeInputSemana > 53)
                throw new Exception("Error. código de barras del laser leido no coincide con el de la BBDD");
        }

        public void TestCustomization()
        {
            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);
            Delay(1000, "");

            Cbs.WriteTablesVersion((ushort)Configuracion.GetDouble("VERSION_TABLAS", 0, ParamUnidad.SinUnidad));
            Delay(1000, "");
            Cbs.WriteHardwareVersion((ushort)Configuracion.GetDouble("VERSION_HARDWARE", 0, ParamUnidad.SinUnidad));
            Delay(1000, "");
            Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);
            Delay(1000, "");
            ushort prealarmRelayLevel = (ushort)Configuracion.GetDouble("PREALARM_RELAY_LEVEL", 0, ParamUnidad.SinUnidad);
            
            if (prealarmRelayLevel != 0)
            {
                Cbs.WritePrealarmRelayLevel(prealarmRelayLevel);

                Cbs.WriteReconectionStateSCE();
            }

            Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);
            Delay(1000, "");
            foreach (CBS4.Channel channel in Enum.GetValues(typeof(CBS4.Channel)))
                WriteAndVerifyDefaultTriggerPosition(channel);

            var posicionI = (ushort)Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 0, ParamUnidad.SinUnidad);
            Resultado.Set("POSICION_I_DISPARO_DEFECTO", posicionI.ToString(), ParamUnidad.SinUnidad);

            var posicionT = (ushort)Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 0, ParamUnidad.SinUnidad);
            Resultado.Set("POSICION_T_RETARDO_DEFECTO", posicionT.ToString(), ParamUnidad.SinUnidad);

            Cbs.WriteWorkMode(CBS4.WorkModes.TEST);
            Delay(1000, "");
            Cbs.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));
            Delay(1000, "");
            
            Cbs.WriteWorkMode(CBS4.WorkModes.NORMAL);
            Delay(1000, "");
            Cbs.WriteWorkMode(CBS4.WorkModes.RESET);
            
            Delay(10000, "Espera al reset del equipo");

            PowerSourceResetAndApplyPrsetes();

            uint serialNumber = 0;

            SamplerWithCancel((p) => { serialNumber = Cbs.ReadSerialNumber(); return true; }, "Error de comunicaciones, al leer numero de serie después de reiniciar el equipo", 3, 1500, 1000);

            Assert.IsTrue(serialNumber.ToString().Trim() == TestInfo.NumSerie.Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error en la grabación del número de serie, el número grabado no coincide con el original"));

            Assert.IsTrue(Cbs.ReadHardwareVersion() == (ushort)Configuracion.GetDouble("VERSION_HARDWARE", 0, ParamUnidad.SinUnidad), Error().UUT.HARDWARE.NO_COINCIDE("La version de Hardware Leida no coincide con la de BBDD."));
            Resultado.Set("VERSION_HARDWARE", Configuracion.GetDouble("VERSION_HARDWARE", 0, ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);

            Assert.IsTrue(Cbs.ReadTablesVersion() == (ushort)Configuracion.GetDouble("VERSION_TABLAS", 0, ParamUnidad.SinUnidad), Error().UUT.MEDIDA.NO_COINCIDE("La version de Tablas Leida no coincide con la de BBDD."));
            Resultado.Set("VERSION_TABLAS", Configuracion.GetDouble("VERSION_TABLAS", 0, ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            
            var resultModelo = Cbs.ReadModelVersion();

            Resultado.Set("MODELO_LEIDO", resultModelo.ToString(), ParamUnidad.SinUnidad );
            Assert.IsTrue(Identificacion.MODELO == "RA" ? resultModelo == CBS4.ModelType.RA : Identificacion.MODELO == "STD" ? resultModelo == CBS4.ModelType.STD : resultModelo == CBS4.ModelType.CC, Error().UUT.CONFIGURACION.SETUP("Error el equipo no tiene el modelo grabado correctamente"));

            ushort deviceErrorCode = 0;

            if (prealarmRelayLevel != 0)
            {
                Assert.IsTrue(Cbs.ReadPrealarmRelayLevel() == prealarmRelayLevel, Error().UUT.CONFIGURACION.ALARMA("Error el nivel de prealarma no se ha grabado correctamente"));

                Assert.IsTrue(Cbs.ReadReconectionState() == (ushort)0x000F, Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("Error el estado de reconexiones no se ha grabado correctamente"));
            }

            if (Identificacion.MODELO == "CC")
            {
                deviceErrorCode = Cbs.ReadCodeErrorCC();

                Resultado.Set("CODE_ERROR", deviceErrorCode, ParamUnidad.SinUnidad);

                Assert.AreEqual(deviceErrorCode, (ushort)0, Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("Error al comprobar el codigo de error del equipo, es distinto de 0"));

                var bastidorNumber = Cbs.ReadBastidorNumber();

                Assert.IsTrue(bastidorNumber == (uint)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error en la grabación del número de bastidor, el número leido no coincide con el grabado"));
            }
            else
            {
                if (VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad) == "DC")
                    Delay(5000, "Refresco del valor del registro");

                deviceErrorCode = Cbs.ReadCodeError();

                Resultado.Set("CODE_ERROR", deviceErrorCode, ParamUnidad.SinUnidad);

                Assert.AreEqual(deviceErrorCode, (ushort)0xFFFF, Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("Error al comprobar el codigo de error del equipo, es distinto de FFFF"));
            }
        }

        public void TestFinish()
        {
            if (Cbs != null)
                Cbs.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();

                if (Tower.IO != null)
                {
                    Tower.IO.DO.Off(OUT_TECLA_ID, OUT_TECLA_PROG, OUT_TECLA_RESET, OUT_TECLA_SET, OUT_TECLA_STD, OUT_TECLA_TD, OUT_TECLA_TEST);

                    Tower.IO.DO.OffWait(300, OUT_PUNTAS_COM_SERIE, OUT_PUNTAS_TTL, OUT_PUNTAS_TTL_BORNES_17, OUT_PUNTAS_TTL_BORNES_18, OUT_PUNTAS_TTL_BORNES_19, OUT_PUNTAS_TTL_BORNES_20, OUT_PUNTAS_TTL_BORNES_21);

                    Tower.IO.DO.OffWait(300, OUT_BORNERA_IZQUIERDA, OUT_BORNERA_DERECHA);
              
                    Tower.IO.DO.Off(OUT_SWITCH_COM5_TO_COM4);

                    Tower.IO.DO.OffWait(300, OUT_EXTERNAL_TRIGGER);

                    Tower.IO.DO.OffWait(300, OUT_FIJACION_CARRO);
                }

                Tower.Dispose();
            }
        }

        // Esta funcion se creo para determinar el tiempo que tarda el equipo en dar error de toro. 
        //public void TestToroidalErrorTime()
        //{
        //    Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);
        //    foreach (CBS4.Channel channel in Enum.GetValues(typeof(CBS4.Channel)))
        //    {
        //        var posicionI = (ushort)Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 0, ParamUnidad.SinUnidad);
        //        Cbs.WriteTriggerCurrentSelectionChannel(channel, posicionI);
        //        var posicionT = (ushort)Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 0, ParamUnidad.SinUnidad);
        //        Cbs.WriteTriggerTimeSelectionChannel(channel, posicionT);
        //    }

        //    Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);

        //    Delay(500, "");

        //    Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);

        //    Delay(500, "");

        //    Tower.IO.DO.Off(OUTPUT_CONNECT_TOROIDAL_500_CHANNEL_1, OUTPUT_CONNECT_TOROIDAL_500_CHANNEL_2, OUTPUT_CONNECT_TOROIDAL_500_CHANNEL_3, OUTPUT_CONNECT_TOROIDAL_500_CHANNEL_4, OUTPUT_SWITCH_TOROIDAL_1000_TOROIDAL_500);

        //    Delay(500, "esperando apagado reles");

        //    Dictionary<CBS4.Channel, int> deviceChannelsTowerOutputsRelationship = new Dictionary<CBS4.Channel, int>()
        //    {
        //        {CBS4.Channel.CHANNEL_2, OUTPUT_CONNECT_TOROIDAL_500_CHANNEL_2}
        //    };


        //    Dictionary<CBS4.Channel, CBS4.Inputs> deviceChannelsDeviceInputsValues = new Dictionary<CBS4.Channel, CBS4.Inputs>()
        //    {
        //        {CBS4.Channel.CHANNEL_2, CBS4.Inputs.TOROIDAL_ERROR_INPUT_2}
        //    };

        //    Tower.IO.DO.OnWait(500, OUTPUT_SWITCH_TOROIDAL_1000_TOROIDAL_500);

        //    Cbs.WriteWorkMode(CBS4.WorkModes.NORMAL);

        //    CBS4.Inputs inputsState = CBS4.Inputs.NO_INPUT;

        //    foreach (KeyValuePair<CBS4.Channel, int> deviceChannelTowerOutput in deviceChannelsTowerOutputsRelationship)
        //    {
        //        Stopwatch stopwatch = new Stopwatch();

        //        Tower.IO.DO.OnWait(500, OUTPUT_CONNECT_TOROIDAL_500_CHANNEL_1);

        //        stopwatch.Start();

        //        Tower.IO.DO.OnWait(500, deviceChannelTowerOutput.Value);

        //        Cbs.WriteChannelManagement(CBS4.ChannelManagement.RESET_ALL);

        //        SamplerWithCancel((p) =>
        //        {
        //            inputsState = Cbs.ReadInputsState();
        //            return inputsState.HasFlag(deviceChannelsDeviceInputsValues[deviceChannelTowerOutput.Key]);
        //        }, "Error: No ha saltado error de Toro", 12, 5000, 5000);

        //        Resultado.Set("TIEMPO_FALLO_TOROIDAL", stopwatch.ElapsedMilliseconds, ParamUnidad.ms);
        //    }
        //}

        #region Auxiliar functions

        protected CBS4.ScaleTableConfiguration GetCurrentScaleTable(string currentScaleElements)
        {
            var scaleElementsList = currentScaleElements.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return new CBS4.ScaleTableConfiguration(scaleElementsList, "corriente");
        }

        protected CBS4.ScaleTableConfiguration GetTimeScaleTable(string timeScaleElements)
        {
            var scaleElementsArray = timeScaleElements.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            for (int i = 0; i < scaleElementsArray.Count(); i++)
            {
                if (scaleElementsArray[i].Trim().ToUpper() == "INS")
                    scaleElementsArray[i] = "1";

                if (scaleElementsArray[i].Trim().ToUpper() == "SEL")
                    scaleElementsArray[i] = "2";
            };

            return new CBS4.ScaleTableConfiguration(scaleElementsArray, "tiempo");
        }

        protected void WriteCustomizeReconectionTable()
        {
            var srdcTable = GetSRDCTable();

            if (srdcTable.HasReconnections)
            {
                Cbs.WriteWorkMode(CBS4.WorkModes.TEST);
                Cbs.WriteTableClear(CBS4.TablesClearOrders.DELETE_RECONNECTION_TABLES);

                if (srdcTable.HasReconnections)
                    Cbs.WriteReconectionTable(srdcTable);

                var srdcPosition = Parametrizacion.GetDouble("SECUENCIA_RECONEXION_DEFECTO", 10, ParamUnidad.SinUnidad);

                Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);

                Cbs.WriteReconectionSelectionChannel((byte)srdcPosition, CBS4.Channel.CHANNEL_1);
                Cbs.WriteReconectionSelectionChannel((byte)srdcPosition, CBS4.Channel.CHANNEL_2);
                Cbs.WriteReconectionSelectionChannel((byte)srdcPosition, CBS4.Channel.CHANNEL_3);
                Cbs.WriteReconectionSelectionChannel((byte)srdcPosition, CBS4.Channel.CHANNEL_4);

                Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);
            }
            else
                throw new Exception("Error de parametrización, no hay elementos de secuencias de reconexion");

            WriteResultsReconnectionTables();
        }

        protected void WriteResultsReconnectionTables()
        {
            Resultado.Set("TABLA_RECONEXION_SEC_1", Parametrizacion.GetString("TABLA_RECONEXION_SEC_1", "6,900,8,16,30,59,115,224,0,0", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_2", Parametrizacion.GetString("TABLA_RECONEXION_SEC_2", "30,900,20,40,300,300,300,300,300,300", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_3", Parametrizacion.GetString("TABLA_RECONEXION_SEC_3", "8,900,30,60,120,180,240,300,360,420", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_4", Parametrizacion.GetString("TABLA_RECONEXION_SEC_4", "6,300,10,20,30,60,130,600,0,0", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_5", Parametrizacion.GetString("TABLA_RECONEXION_SEC_5", "6,900,120,240,480,480,480,480,0,0", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_6", Parametrizacion.GetString("TABLA_RECONEXION_SEC_6", "7,900,30,60,120,180,240,480,960,0", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_7", Parametrizacion.GetString("TABLA_RECONEXION_SEC_7", "10,1800,60,60,60,60,60,60,60,60", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_8", Parametrizacion.GetString("TABLA_RECONEXION_SEC_8", "10,1800,90,90,90,90,90,90,90,90", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_9", Parametrizacion.GetString("TABLA_RECONEXION_SEC_9", "6,900,120,240,360,360,360,360,0,0", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_10", Parametrizacion.GetString("TABLA_RECONEXION_SEC_10", "10,1800,180,180,180,180,180,180,180,180", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_11", Parametrizacion.GetString("TABLA_RECONEXION_SEC_11", "10,3600,60,60,60,60,60,60,60,60", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
            Resultado.Set("TABLA_RECONEXION_SEC_12", Parametrizacion.GetString("TABLA_RECONEXION_SEC_12", "10,3600,90,90,90,90,90,90,90,90", ParamUnidad.SinUnidad), ParamUnidad.SinUnidad);
        }

        protected void VerificationCustomizeReconectionTable()
        {
            var srdcTable = GetSRDCTable();

            if (srdcTable.HasReconnections)
            {
                Cbs.WriteWorkMode(CBS4.WorkModes.TEST);

                if (srdcTable.HasReconnections)
                    Cbs.ReadReconectionTable(srdcTable);
            }
            else
                throw new Exception("Error de parametrización, no hay elementos de secuencias de reconexion");
        }

        protected CBS4.ReconnectionTableConfiguration GetSRDCTable()
        {
            CBS4.ReconnectionTableConfiguration srdcTable = new CBS4.ReconnectionTableConfiguration(12);

            var secuencias = new Dictionary<int, string>()
            {
              {1,Parametrizacion.GetString("TABLA_RECONEXION_SEC_1", "6,900,8,16,30,59,115,224,0,0", ParamUnidad.SinUnidad)},
              {2,Parametrizacion.GetString("TABLA_RECONEXION_SEC_2", "30,900,20,40,300,300,300,300,300,300", ParamUnidad.SinUnidad)},
              {3,Parametrizacion.GetString("TABLA_RECONEXION_SEC_3", "8,900,30,60,120,180,240,300,360,420", ParamUnidad.SinUnidad)},
              {4,Parametrizacion.GetString("TABLA_RECONEXION_SEC_4", "6,300,10,20,30,60,130,600,0,0", ParamUnidad.SinUnidad)},
              {5,Parametrizacion.GetString("TABLA_RECONEXION_SEC_5", "6,900,120,240,480,480,480,480,0,0", ParamUnidad.SinUnidad)},
              {6,Parametrizacion.GetString("TABLA_RECONEXION_SEC_6", "7,900,30,60,120,180,240,480,960,0", ParamUnidad.SinUnidad)},
              {7,Parametrizacion.GetString("TABLA_RECONEXION_SEC_7", "10,1800,60,60,60,60,60,60,60,60", ParamUnidad.SinUnidad)},
              {8,Parametrizacion.GetString("TABLA_RECONEXION_SEC_8", "10,1800,90,90,90,90,90,90,90,90", ParamUnidad.SinUnidad)},
              {9,Parametrizacion.GetString("TABLA_RECONEXION_SEC_9", "6,900,120,240,360,360,360,360,0,0", ParamUnidad.SinUnidad)},
              {10,Parametrizacion.GetString("TABLA_RECONEXION_SEC_10", "10,1800,180,180,180,180,180,180,180,180", ParamUnidad.SinUnidad)},
              {11,Parametrizacion.GetString("TABLA_RECONEXION_SEC_11", "10,3600,60,60,60,60,60,60,60,60", ParamUnidad.SinUnidad)},
              {12,Parametrizacion.GetString("TABLA_RECONEXION_SEC_12", "10,3600,90,90,90,90,90,90,90,90", ParamUnidad.SinUnidad)}
            };

            for (byte i = 1; i < 12; i++) //12 maximo de secuencias por memoria del equipo que puede tener
            {
                var param = Parametrizacion.GetString(string.Format("TABLA_RECONEXION_SEC_{0}", i.ToString()), "NO", ParamUnidad.SinUnidad);
                if (param.ToUpper().Trim() == "NO")
                    break;

                srdcTable.ReconectionSequenceTable.Add(new CBS4.ReconnectionTableConfiguration.ReconnectionStage(param));
            }

            return srdcTable;
        }

        protected void WriteAndVerifyDefaultTriggerPosition(CBS4.Channel channel)
        {
            var posicionI = (ushort)Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 0, ParamUnidad.SinUnidad);
            var posicionT = (ushort)Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 0, ParamUnidad.SinUnidad);

            var currentScaleTableWriting = GetCurrentScaleTable(Parametrizacion.GetString("TABLA_I_DISPARO", "0;30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad));
            var timeScaleTableWriting = GetTimeScaleTable(Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad));

            var valorPosicionI = (ushort)currentScaleTableWriting.ListElemnts[posicionI].ElementValue;
            var valorPosicionT = (ushort)timeScaleTableWriting.ListElemnts[posicionT].ElementValue;

            SamplerWithCancel((p) =>
            {
                Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.ENABLE);

                Cbs.WriteTriggerCurrentSelectionChannel(channel, posicionI);

                Cbs.WriteTriggerTimeSelectionChannel(channel, posicionT);

                Cbs.WriteConfigurationManagement(CBS4.ConfigurationWritting.SAVE);
  
                bool result = (Cbs.ReadTriggerCurrentSelectionChannel(channel) == posicionI) || (Cbs.ReadTriggerCurrentSelectionChannel(channel) == valorPosicionI);
            
                bool result2 = (Cbs.ReadTriggerTimeSelectionChannel(channel) == posicionT) || (Cbs.ReadTriggerTimeSelectionChannel(channel) == valorPosicionT);

                return result && result2;
            }, "Error no se han podido grabar bien las selecciones de la tabla");   
        }

        protected void PowerSourceResetAndApplyPrsetes()
        {
            var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);
            var paramV = typeSupply == "AC" ? Params.V.Null.EnVacio.Vnom.Name : Params.V_DC.Null.EnVacio.Vmin.Name;
            var paramI = Params.I.Null.EnVacio.Vnom.Name;
            var voltage = Consignas.GetDouble(paramV, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(paramI, 0, ParamUnidad.A);

            this.InParellel(() =>
            {

                if (typeSupply == "AC")
                {
                    Tower.Chroma.ApplyOff();
                    Delay(1500, "Esperando apagado del equipo");

                    if (voltage > 300)
                    {
                        voltage /= 2;
                        Tower.IO.DO.On(13);
                    }
                    Tower.Chroma.ApplyPresetsAndWaitStabilisation(voltage, 0, 50);
                }
                else
                {
                    Tower.LAMBDA.ApplyOff();
                    Delay(1500, "Esperando apagado del equipo");

                    Tower.IO.DO.On(23);
                    Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltage, current);
                }

                Delay(2500, "Esperando encendido del equipo");
            }, () =>
            {
                if (Comunicaciones.Protocolo.ToUpper().Trim() == "JOHNSON")
                    JonshonToModbus();
            });
        }

        protected void JonshonToModbus()
        {
            Cbs.Modbus.ClosePort();

            sp = new SerialPort("COM" + Comunicaciones.SerialPort.ToString(), Comunicaciones.BaudRate, Parity.None, 8, StopBits.One);

            try
            {
                sp.Open();
                sp.NewLine = "\r";
                sp.ReadTimeout = 500;

                SamplerWithCancel((p) =>
                {
                    sp.WriteLine(">01ABCD6BE3");
                    var result = sp.ReadLine();
                    return (result == "A");
                }, "No se ha podido Cambiar a protocolo Modbus desde Jonshon", 20, 500, 500);
            }
            finally
            {
                sp.Close();
                sp.Dispose();
            }

            Cbs.Modbus.OpenPort();
        }

        #endregion

    }
}