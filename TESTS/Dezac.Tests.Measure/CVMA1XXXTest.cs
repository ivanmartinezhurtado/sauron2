﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.19)]
    public class CVMA1XXXTest : CVMB1XXTest
    {
        private CVMB1XXTestModuloDataLogger datalogger = new CVMB1XXTestModuloDataLogger();
        private CVMB1XXDataloggerTestPlacas dataloggerHTTP = new CVMB1XXDataloggerTestPlacas();

        private const byte OUT_SHIELD = 22;
        private const byte OUT_CC_IL2 = 20;
        private const byte OUT_CC_IL3 = 21;
        public override void TestInitialization(int portCVM = 0)
        {
            base.TestInitialization(portCVM);

            Tower.PowerSourceIII = Tower.FLUKE;
            Tower.PowerSourceIII.Tolerance = 0.1;
            Tower.FLUKE.DeviceInputImpedance = 8;
        }

        public override void TestConector()
        {
            Cvm.FlagTest();

            SamplerWithCancel((p) =>
            {
                return Cvm.Modbus.Read<CVMA1XXX.Conector>().IsValid;
            }, "Error conector posterior no detectado", 15, 1000, 500);

            var param = new AdjustValueDef(Params.V_DC.Null.TestPoint("VN212V"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
            }, 0, 3, 2000);

            param = new AdjustValueDef(Params.V_DC.Null.TestPoint("P3V3"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.IN3, MagnitudsMultimeter.VoltDC);
            }, 0, 3, 2000);           
        }

        public void FlagTest()
        {
            Cvm.FlagTest();
        }

        public void TestDetectionModule()
        {
            datalogger.CVMDummy = Cvm;
            datalogger.TestDetectionModule();
        }

        public void TestFlash()
        {
            datalogger.TestFlashAndWriteSerielNumber();
        }

        public void TestDataLogger()
        {
            datalogger.TestDataLogger();
        }

        public void TestIncrementDate()
        {
            datalogger.TestIncrementDate();
            
        }

        public void TestHttpSendHttpLockSession()
        {
            dataloggerHTTP.Cvm = new CVMB1XXModuloDatalogger();
            dataloggerHTTP.HttpSendHttpLockSession();
        }

        public void TestHttpSendHttpXmlConfiguration()
        {
            dataloggerHTTP.HttpSendHttpXmlConfiguration("CVMA1000CONTAINER", "CVM-A1000");
        }

        public void TestSendHttpSynchronize()
        {
            dataloggerHTTP.HttpSendHttpSynchronize();
        }

        public void TestSendHttpUnLockSession()
        {
            dataloggerHTTP.HttpSendHttpUnLockSession();
        }

        public void TestShortCircuitFlex(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            Cvm.FlagTest();           

            var offsetsV = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = Cvm.ReadVariablesInstantaneas();
                    return new double[] { vars.L1.Tension, vars.L2.Tension, vars.L3.Tension,
                        vars.L1.Corriente, vars.L2.Corriente, vars.L3.Corriente };

                }, delFirts, samples, timeInterval);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 151, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 0.05, ParamUnidad.V);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 0.075, ParamUnidad.V);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 0.1, ParamUnidad.V);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            Cvm.FlagTest();

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = Cvm.ReadVariablesInstantaneas();
                    return new double[] { vars.L1.Tension, vars.L2.Tension, vars.L3.Tension};

                }, delFirts, samples, timeInterval);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();
        }
        /******************************* ADJUST FLEX *******************************/


        public void TestAdjustCurrentFlex(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Tower.IO.DO.On(OUT_SHIELD);
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);


            var parameter = Params.GAIN_I.L1.Ajuste.esc_5A;
            var defs = new TriAdjustValueDef(new AdjustValueDef(parameter, ParamUnidad.Puntos));

            var result = Cvm.AjusteCorrienteFlex(delFirts, initCount, samples, timeInterval, adjustCurrent,
                (value) =>
                {
                    defs.L1.Value = value.L1;
                    defs.L2.Value = value.L2;
                    defs.L3.Value = value.L3;

                    return !defs.HasMinMaxError();
                },
                (corrientes) => (corrientes.CorrienteL1 > 0 && corrientes.CorrienteL2 > 0 && corrientes.CorrienteL3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, Error().UUT.MEDIDA.MARGENES(string.Format("Error valores del ajuste de ganacia de corriente fuera de margenes")));
        }

        public void TestAdjustNeutralCurrentFlex(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Tower.IO.DO.On(OUT_CC_IL2, OUT_CC_IL3);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            var parameter = Params.GAIN_I.LN_CALC.Ajuste.esc_5A;
            var defs = new AdjustValueDef(parameter.Name, 0, parameter.Min(), parameter.Max(), 0, 0, ParamUnidad.Puntos);

            var result = Cvm.AjusteCorrienteNeutroFlex(delFirts, initCount, samples, timeInterval, adjustCurrent,
             (value) =>
             {
                 defs.Value = value;
                 return defs.IsValid();
             });

            var currents = Tower.FLUKE.ReadCurrent();

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, Error().UUT.MEDIDA.MARGENES("Error valores del ajuste de ganacia de la corriente de Neutro calculada fuera de margenes"));

            Tower.IO.DO.Off(OUT_CC_IL2, OUT_CC_IL3);

        }

        public void TestAdjustPhaseFlex(int delFirts = 10, int initCount = 10, int samples = 20, int timeInterval = 1100)
        {
             Cvm.FlagTest();

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            var parameter = Params.GAIN_DESFASE.L1.Ajuste.esc_5A;
            var defs = new TriAdjustValueDef(new AdjustValueDef(parameter, ParamUnidad.Puntos));

            var result = Cvm.AjusteDesfaseFlex(delFirts, initCount, samples, timeInterval, powerFactor,
                (value) =>
                {
                    defs.L1.Value = value.L1;
                    defs.L2.Value = value.L2;
                    defs.L3.Value = value.L3;

                    return !defs.HasMinMaxError();
                },
                (potencias) => (potencias.L1.Activa > 0 && potencias.L2.Activa > 0 && potencias.L3.Activa > 0 && potencias.L1.Reactiva > 0 && potencias.L2.Reactiva > 0 && potencias.L3.Reactiva > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, Error().UUT.MEDIDA.MARGENES(string.Format("Error valores del ajuste de ganacia del desfase a fuera de margenes")));
        }

        public void TestVerificationFlex(int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var Escala = CVMB1XX.Escalas.E5A;

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.esc_5A.Name, 5, ParamUnidad.A);
            adjustCurrent *= 10000;
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

          
            var toleranciaV = Params.V.Null.Verificacion.TestPoint(Escala.ToString()).Tol();
            var toleranciaI = Params.I.Null.Verificacion.TestPoint(Escala.ToString()).Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.TestPoint(Escala.ToString()).Tol();
            var toleranciaKvar = Params.KVAR.Null.Verificacion.TestPoint(Escala.ToString()).Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.KVAR.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaKvar, ParamUnidad.Var));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaI, ParamUnidad.A));

            Delay(2000, "Esperando relacion transformacion se aplique");

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadVariablesInstantaneas();
                    var powersFlex = Cvm.ReadPowersFlex();
                    return new double[] {
                    powersFlex.L1.Activa,  powersFlex.L2.Activa, powersFlex.L3.Activa,
                    powersFlex.L1.Reactiva, powersFlex.L2.Reactiva, powersFlex.L3.Reactiva,
                    vars.L1.Tension, vars.L2.Tension,vars.L3.Tension,
                    vars.L1.Corriente * 100d , vars.L2.Corriente * 100d  , vars.L3.Corriente * 100d };
                },
                () =>
                {
                    var varsList = new List<double>();
                    var activePower = adjustVoltage * adjustCurrent * Math.Cos(powerFactor.ToRadians());
                    var reactivePower = adjustVoltage * adjustCurrent * Math.Sin(powerFactor.ToRadians());

                    varsList.Add(activePower); varsList.Add(activePower); varsList.Add(activePower);
                    varsList.Add(reactivePower); varsList.Add(reactivePower); varsList.Add(reactivePower);
                    varsList.Add(adjustVoltage); varsList.Add(adjustVoltage); varsList.Add(adjustVoltage);
                    varsList.Add(adjustCurrent); varsList.Add(adjustCurrent); varsList.Add(adjustCurrent);

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();
        }

        public void TestVerificationNeutralCurrentFlex(int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Tower.IO.DO.On(OUT_CC_IL2, OUT_CC_IL3);

            var Escala = CVMB1XX.Escalas.E5A;

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.esc_5A.Name, 5, ParamUnidad.A);
            adjustCurrent *= 10000;
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            var toleranciaV = Params.V.Null.Verificacion.TestPoint(Escala.ToString()).Tol();
            var toleranciaI = Params.I.Null.Verificacion.TestPoint(Escala.ToString()).Tol();

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.V.L1.Verificacion.TestPoint("NEUTRO").Name, 0, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.L1.Verificacion.TestPoint("NEUTRO").Name, 0, 0, 0, 0, toleranciaI, ParamUnidad.A));

            Delay(2000, "Esperando relacion transformacion se aplique");

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.Modbus.Read<CVMB1XX.TensionesCorrientes>();
                    return new double[] {
                    vars.TensionNeutro,vars.CorrienteNeutro * 100d };
                },
                () =>
                {
                    var varsList = new List<double>();
                    varsList.Add(adjustVoltage);
                    varsList.Add(adjustCurrent);

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();

            Tower.IO.DO.OffWait(100, OUT_CC_IL2, OUT_CC_IL3);
        }
    }
}
