﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Instruments.Cameras.DTO
{
    public enum ExposureAutoEnum
    {
        Off = 0,
        Continuous = 2,
    }
}
