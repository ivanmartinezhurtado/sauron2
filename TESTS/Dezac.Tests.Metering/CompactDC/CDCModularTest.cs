﻿using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.37)]
    public class CDCModularTest : CDCTestBase
    {
        private const int PILOTO_ERROR = 48;
        private const int POWER_SUPPLY_TTLRS232 = 15;
        private const int OUT_PLC_NEUTRO = 22;
        private const int OUT_PLC2_NEUTRO = 43;
        private const int PULSADOR_RESET_7M = 9;
        private const int PISTON_GENERAL = 4;
        private const int OUT_OPEN_I_SBT1_SBT2 = 53;
        private const int OUT_ALIMENTACION_CONTADORES = 56;
        private const int OUT_CONTADORES_TO_PLS = 54;

        public enum TestLine
        {
            PLC1_L1 = 17,
            PLC1_L2 = 18,
            PLC1_L3 = 19,
            PLC2_L1 = 49,
            PLC2_L2 = 50,
            PLC2_L3 = 52,
        }
      
        public void TestInitialization()
        {
            CAMERA_CPU_PSU = GetVariable<string>("CAMARA_IDS_CPU", CAMERA_CPU_PSU);
            CAMERA_SBT = GetVariable<string>("CAMARA_IDS_SBT", CAMERA_SBT);
            CAMERA_AUX = GetVariable<string>("CAMARA_IDS_AUX", CAMERA_AUX);

            tower.Active24VDC();
            tower.IO.DO.On(PISTON_GENERAL, POWER_SUPPLY_TTLRS232);
            tower.IO.DO.Off(PILOTO_ERROR, OUT_ALIMENTACION_CONTADORES, OUT_CONTADORES_TO_PLS);
            //tower.IO.DO.On(OUT_OPEN_I_SBT1_SBT2);
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            InstanceNumber = (int)Configuracion.GetDouble("NUMERO_EQUIPOS_SBT", 2, ParamUnidad.SinUnidad);
            has3G = Configuracion.GetString("HAS_MODULE_MODEM", "NO").Trim().ToUpper() == "SI" ? "3G_" : "";
            onlySBT = InstanceNumber == 1 ? true : false;
            DoubleSecondary = !onlySBT;
            SBT_NUMBER = 1;
            PULSADOR_RESET = PULSADOR_RESET_7M;

            PositionCameraDictionary = new Dictionary<int, string>();
            PositionCameraDictionary.Add(1, CAMERA_CPU_PSU);
            PositionCameraDictionary.Add(2, CAMERA_CPU_PSU);
            PositionCameraDictionary.Add(3, CAMERA_CPU_PSU);
            PositionCameraDictionary.Add(4, CAMERA_SBT);
            PositionCameraDictionary.Add(5, CAMERA_SBT);
            PositionCameraDictionary.Add(6, CAMERA_SBT);
        }

         // AJUSTE SBT //
        //**************************************
        public void TestLeds_PLC2_SBT1_SBT2()
        {
            this.TestHalconFindLedsProcedure(CAMERA_SBT, "CDC_Modular_PLC2_SBT1_SBT2", "PLC2_SBT1_SBT2", "CDC_Modular", 3);
        }

        public void TestLeds_PSU_PLC1()
        {
            this.TestHalconFindLedsProcedure(CAMERA_CPU_PSU, "CDC_Modular_PSU_PLC1", "PSU_PLC1", "CDC_Modular", 3);
        }

        public void TestLeds_PLC()
        {
            this.TestHalconFindLedsProcedure(CAMERA_CPU_PSU, "CDC_Modular_PLC", "PLC", "CDC_Modular", 1);
        }

        public void TestLeds_SBT()
        {
            this.TestHalconFindLedsProcedure(CAMERA_SBT, "CDC_Modular_SBT", "SBT", "CDC_Modular", 3);
        }

        public override void TestLedsCPU()
        {
            ActivateLedsCPU();
            this.TestHalconFindLedsProcedure(CAMERA_CPU_PSU, "CDC_Modular_CPU", "CPU", "CDC_Modular", 3,
            (step) =>
                {
                    ActivateLedsCPU();
                    Delay(1000, "");
                });
        }

        public void TestLeds_PSU_CPU()
        {
            ActivateLedsCPU();
            this.TestHalconFindLedsProcedure(CAMERA_CPU_PSU, "CDC_Modular_PSU_CPU", "PSU_CPU", "CDC_Modular", 3);
        }

        public void TestLeds3G_PLCS()
        {
            Delay(2500, "Espera leds");

            this.TestHalconFindLedsProcedure(CAMERA_SBT, "CDC_Modular_3G_PLCS", "3G_PLCS", "CDC_Modular", 3);
        }

        public override void TestLeds3G()
        {
            Delay(2500, "Espera leds");

            this.TestHalconFindLedsProcedure(CAMERA_AUX, "CDC_Modular_3G", "3G", "CDC_Modular", 3);
        }

        public void VerificationParametersSBT2()
        {
            VerificationParametersSBT(2);
        }

        public void WriteFactorsDefaultSBT2()
        {
            WriteFactorsDefaultSBT(2);
        }

        public void TestVoltageAdjustSBT2()
        {
            TestVoltageAdjustSBT(2);
        }

        public void TestGainPhaseOffsetAdjustSBT2()
        {
            TestGainPhaseOffsetAdjustSBT(2);
        }

        public void TestGainPowerAdjustSBT2()
        {
            TestGainPowerAdjustSBT(2);
        }

        public void TestCRCIntegritySBT2()
        {
            TestCRCIntegritySBT(2);
        }

        // PLC  //
        //**************************************
        public void TestPLC1fromMeter()
        {
            internalTestPLCfromMeter("L1", 15, "CIR0000000001");
            internalTestPLCfromMeter("L2", 15, "CIR0000000002");
            internalTestPLCfromMeter("L3", 15, "CIR0000000003");
        }

        public void TestPLC2fromMeter2()
        {
            tower.IO.DO.On(OUT_CONTADORES_TO_PLS);

            internalTestPLCfromMeter("L1", 35, "CIR0000000001", true);
            internalTestPLCfromMeter("L2", 15, "CIR0000000002", true);
            internalTestPLCfromMeter("L3", 15, "CIR0000000003", true);
        }

        public void TestPLC2fromSBT2()
        {
            TestPLCfromSBT(2);
        }
      
        public void TestVerificationSBT2(int initCount = 4, int samples = 6, int timeInterval = 1200)
        {
            TestVerificationSBT(initCount, samples, timeInterval, 2);
        }

        public override void DeleteNodesRestart()
        {
            base.DeleteNodesRestart();

            tower.IO.DO.On(OUT_ALIMENTACION_CONTADORES);

            tower.Chroma.ApplyOffAndWaitStabilisation();         

            if (Configuracion.GetString("HAS_MODULE_BATTERY", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
                Delay(20000, "Esperamos a que se apague el equipo");
            else
                Delay(2000, "Esperamos se apague el equipo");

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0.1, 50);

            if (ZonaHoraria == "NO")
                dateTimeLocalRestart = DateTime.Now;
            else
            {
                var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
                dateTimeLocalRestart = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            }
        }

        public override void TestFinish()
        {
            if (tower != null)
            {
                tower.ShutdownSources();

                if (tower.IO != null)
                {
                    tower.IO.DO.On(OUT_PLC_NEUTRO, (int)TestLine.PLC1_L1, (int)TestLine.PLC1_L2, (int)TestLine.PLC1_L3, OUT_PLC2_NEUTRO, (int)TestLine.PLC2_L1, (int)TestLine.PLC2_L2, (int)TestLine.PLC2_L3); //Para descargar los Condensadores del SBT y PLC
                    tower.IO.DO.Off(POWER_SUPPLY_TTLRS232, OUT_OPEN_I_SBT1_SBT2);

                    if (string.IsNullOrEmpty(TestInfo.NumSerie))
                        tower.IO.DO.On(PILOTO_ERROR);

                    Delay(1500, "Espera de equipo malo");

                    tower.IO.DO.Off(OUT_PLC_NEUTRO, (int)TestLine.PLC1_L1, (int)TestLine.PLC1_L2, (int)TestLine.PLC1_L3, OUT_PLC2_NEUTRO, (int)TestLine.PLC2_L1, (int)TestLine.PLC2_L2, (int)TestLine.PLC2_L3); 
                }
            }

            base.TestFinish();
        }
    }
}
