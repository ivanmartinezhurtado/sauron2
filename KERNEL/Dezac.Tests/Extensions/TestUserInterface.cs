﻿using Dezac.Tests.Forms;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Visio.DTO;
using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Extensions
{
    public static class TestUserInterface
    {
        public static int GetIntroBastiorByUser(this TestBase testBase, string device)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            if (string.IsNullOrEmpty(device))
                device = " a testear";

            var isMultiple = context.RunningInstances > 1;

            string msg = context.NumInstance > 1 || isMultiple ?
                string.Format("ENTRADA DEL NUMERO DE BASTIDOR DEL TEST {0}", context.NumInstance) :
                "ENTRADA DEL NUMERO DE BASTIDOR";

            if (isMultiple)
                device += string.Format("\n(TEST {0})", context.NumInstance);

            var bastidor = iShell.ShowDialog<string>(msg, () =>
            {
                return new InputKeyBoard(string.Format("Leer código de barras del bastidor del equipo {0}", device));
            }
           , MessageBoxButtons.OKCancel, (c, d) =>
           {
               if (d == DialogResult.OK)
                   return ((InputKeyBoard)c).TextValue;

               return string.Empty;
           });

            bastidor = bastidor.Trim();

            if (string.IsNullOrEmpty(bastidor))
                throw new Exception("El usuario ha cancelado la entrada del bastidor!");

            var intBastidor = Convert.ToInt32(bastidor);

            if (bastidor.Length < 5)
                throw new Exception("La longitud del bastidor introducido es mas pequeña de la esperada");

            if (bastidor.Length > 7)
                throw new Exception("La longitud del bastidor introducido es mas grande de la esperada");

            return intBastidor;
        }

        public static string GetIntroNumSerieByUser(this TestBase testBase, string description="")
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var isMultiple = context.TotalNumInstances > 1;

            string msg = isMultiple ?
                string.Format("ENTRADA DEL NUMERO DE SERIE DEL EQUIPO DEL TEST {0}", context.NumInstance) :
                "ENTRADA DEL NUMERO DE SERIE DEL EQUIPO";

            if (isMultiple)
                description += string.Format("\n(TEST {0})", context.NumInstance);

            var numserie = iShell.ShowDialog<string>(msg, () =>
            {
                return new InputKeyBoard(string.Format("Leer código de barras del numero de serie del equipo {0}", description));
            }
           , MessageBoxButtons.OKCancel, (c, d) =>
           {
               if (d == DialogResult.OK)
                   return ((InputKeyBoard)c).TextValue;

               return string.Empty;
           });

            if (string.IsNullOrEmpty(numserie))
                throw new Exception("El usuario ha cancelado la entrada del numero de serie!");

            if (numserie.Length > 14)
                throw new Exception("La longitud del numero de serie introducido es mayor de 14");

            if (numserie.Length < 8)
                throw new Exception("La longitud del numero de serie introducido es menor de 8");

            return numserie;
        }

        public static DialogResult CompareImageFromPattern(this TestBase testBase, ImageStruct imageToCompare, string descriptionImage)
        {
            Image patternImage;
            string imageNameWithoutNumber = Regex.Replace(imageToCompare.ImageName, @"_[0-9]$", "");
            var patternImageBBDD = testBase.ConfigurationFiles.GetData(imageNameWithoutNumber);
            if (patternImageBBDD != null)
                using (MemoryStream ms = new MemoryStream(patternImageBBDD))
                {
                    patternImage = Image.FromStream(ms);
                }
            else
                patternImage = Properties.Resources.Imagen_no_disponible_svg;

            var imageViewPatron = new ImageViewPatron(imageToCompare.Image, patternImage);

            var camaraResult = testBase.Shell.ShowDialog("Test de imagen", () =>
            {
                return imageViewPatron;
            }, MessageBoxButtons.YesNo, !string.IsNullOrEmpty(descriptionImage) ? descriptionImage : "Comprobar la imagen de la Cámara como en la imagen patrón", 2000);

            testBase.Resultado.Set($"{imageToCompare.ImageName}_USER", camaraResult == DialogResult.Yes ? "OK" : "FAIL", ParamUnidad.SinUnidad);
            patternImage.Dispose();

            return camaraResult;
        }

        public static void ShowDialogMessage(this TestBase testBase, string caption, string message, int height, int width)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            if (iShell.ShowDialog(caption, () => new Label() { Width = width + 200, Height = height, Text = message, Font= font, TextAlign = ContentAlignment.MiddleCenter, Dock= DockStyle.Fill }, MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                throw new Exception("Test cancelado por el usuario");
        }

        public static void ShowDialogTaskParalel(this TestBase testBase, string title, string pathImage, Func<bool> funcLoad, Action<bool> funcParalel, string errorMessage)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var closed = false;

            var progress = iShell.ShowDialog(title,
              () =>
              {
                  var c = new ImageView(title, pathImage);

                  iShell.RunSync(() =>
                  {
                      funcParalel(!closed);
                  });

                  c.Load += (s, ev) => { funcLoad(); };

                  return c;
              },
              MessageBoxButtons.OKCancel);

            closed = true;

            if (progress != DialogResult.OK)
                throw new Exception(errorMessage);
        }

        public static void ShowDialogSamplerCanceled(this TestBase testBase, string title, string pathImage, Func<bool> funcLoad, Func<bool> funcTerminated, string errorMessage)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var progress = iShell.ShowDialog(title,
              () =>
              {
                  var c = new ImageView(title, pathImage);

                  c.Load += (s, ev) => {

                      var ctrl = s as ImageView;

                      if (funcLoad != null)
                        funcLoad();

                      System.Threading.Timer timer = null;
                      timer = new System.Threading.Timer((obj) =>
                      {
                          ctrl.CloseView(funcTerminated() ? DialogResult.OK : DialogResult.Abort);
                          timer.Dispose();
                      }, null, 250, System.Threading.Timeout.Infinite);
                  };

                  return c;
              },
              MessageBoxButtons.RetryCancel);

            if (progress != DialogResult.OK)
                throw new Exception(errorMessage);
        }

        public static Form ShowForm(this TestBase testBase, Func<Control> control)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            return iShell.ShowForm(control);
        }

        public static Form ShowForm(this TestBase testBase, Func<Control> control, MessageBoxButtons? buttons)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            return iShell.ShowForm(() =>
            {
                var ctrl = control();

                var form = new MsgBox(ctrl.Text, ctrl, buttons);
                form.ActiveControl = ctrl;

                form.ShowInTaskbar = false;
                //form.TopMost = true;

                return form;
            });
        }

        public static Form ShowForm(this TestBase testBase, Func<Control> control, MessageBoxButtons? buttons, string message)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            return iShell.ShowForm(() =>
            {
                var ctrl = control();

                var form = new MsgBox(ctrl.Text, ctrl, buttons, message);
                form.ActiveControl = ctrl;

                form.ShowInTaskbar = false;
                form.TopMost = true;

                return form;
            });
        }

        public static void CloseForm(this TestBase testBase, Form form)
        {
            if (form == null)
                return;

            var iShell = SequenceContext.Current.Services.Get<IShell>();

            iShell.Run(() => form.Dispose());
        }

    }
}
