﻿using Dezac.Device.Renewable;
using Dezac.Tests.Model;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace Dezac.Tests.Renewable
{
    [TestVersion(1.00)]
    public class InversorBeagleBoneControlTest : TestBase
    {
        InversorBeagleBoneControl bb;

        private string PathImage = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\STG_PLACA_CONTROL\";

        public void TestInitialization(int port = 3)
        {
            bb = new InversorBeagleBoneControl(port);
        }
        
        public void TestVerifyAPairAnalogInputs()
        {
            var analogInputsDictionary = new Dictionary<InversorBeagleBoneControl.AnalogInputs, AdjustValueDef>();

            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A0, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_0").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A1, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_1_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A2, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_2").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A3, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_3_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A4, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_4").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A5, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_5_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A6, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_6").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A7, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_7_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));

            Shell.ShowDialog("Verificación entradas analógicas", new ImageView(string.Empty, PathImage + "ADC_A_PARES.png"), MessageBoxButtons.OK);

            // Min: 0.93, Max: 1.03
            VerifyAnalogInputReading(analogInputsDictionary);

        }
        
        public void TestVerifyAImpairAnalogInputs()
        {
            var analogInputsDictionary = new Dictionary<InversorBeagleBoneControl.AnalogInputs, AdjustValueDef>();

            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A0, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_0_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A1, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_1").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A2, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_2_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A3, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_3").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A4, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_4_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A5, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_5").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A6, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_6_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_A7, new AdjustValueDef(Params.V_DC.Null.TestPoint("A_INPUT_7").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("A_INPUTS").Avg(), Params.V_DC.Null.TestPoint("A_INPUTS").Tol(), ParamUnidad.V));

            Shell.ShowDialog("Verificación entradas analógicas", new ImageView(string.Empty, PathImage + "ADC_A_IMPARES.png"), MessageBoxButtons.OK);

            // Min: 0.93, Max: 1.03
            VerifyAnalogInputReading(analogInputsDictionary);

        }
        
        public void TestVerifyBImpairAnalogInputs()
        {
            var analogInputsDictionary = new Dictionary<InversorBeagleBoneControl.AnalogInputs, AdjustValueDef>();

            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B0, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_0_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B1, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_1").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B2, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_2_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B3, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_3").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B4, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_4_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_4_6_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_4_6_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B5, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_5").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_4_6").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_4_6").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B6, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_6_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_4_6_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_4_6_INACTIVE").Tol(), ParamUnidad.V));

            Shell.ShowDialog("Verificación entradas analógicas", new ImageView(string.Empty, PathImage + "ADC_B_IMPARES.png"), MessageBoxButtons.OK);

            // Min: 1.6, Max: 1.7
            VerifyAnalogInputReading(analogInputsDictionary);
        }

        public void TestVerifyBPairAnalogInputs()
        {
            var analogInputsDictionary = new Dictionary<InversorBeagleBoneControl.AnalogInputs, AdjustValueDef>();

            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B0, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_0").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B1, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_1_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B2, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_2").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B3, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_3_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_0_3_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B4, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_4").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_4_6").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_4_6").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B5, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_5_INACTIVE").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_4_6_INACTIVE").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_4_6_INACTIVE").Tol(), ParamUnidad.V));
            analogInputsDictionary.Add(InversorBeagleBoneControl.AnalogInputs.ADC_B6, new AdjustValueDef(Params.V_DC.Null.TestPoint("B_INPUTS_6").Name, 0, 0, 0, Params.V_DC.Null.TestPoint("B_INPUTS_4_6").Avg(), Params.V_DC.Null.TestPoint("B_INPUTS_4_6").Tol(), ParamUnidad.V));

            Shell.ShowDialog("Verificación entradas analógicas", new ImageView(string.Empty, PathImage + "ADC_B_PARES.png"), MessageBoxButtons.OK);

            // Min: 1.9, Max: 2
            VerifyAnalogInputReading(analogInputsDictionary);
        }

        public void TestVerifyDigitalOutputs()
        {
            var outputs = new InversorBeagleBoneControl.REOutputs();

            outputs.Re1 = true;
            outputs.Re2 = false;
            outputs.Re3 = true;
            outputs.Re4 = false;
            outputs.Re5 = true;
            outputs.Re6 = false;
            outputs.Re7 = true;
            outputs.Re8 = false;
            outputs.Re9 = true;
            outputs.Re10 = false;
            outputs.Re11 = true;
            outputs.Re12 = false;

            bb.WriteOutputs(outputs);

            if (Shell.ShowDialog("Comprobación salidas digitales", new ImageView("Se encienden los leds destacados en la imagen?", PathImage + "LEDS_RE_IMPARES.png"), MessageBoxButtons.YesNo) != DialogResult.Yes)
                throw new Exception("Error en la activación de las salidas RE impares");

            Resultado.Set("RE_IMPAIR_OUTPUTS", "OK", ParamUnidad.SinUnidad);
            
            outputs.Re1 = false;
            outputs.Re2 = true;
            outputs.Re3 = false;
            outputs.Re4 = true;
            outputs.Re5 = false;
            outputs.Re6 = true;
            outputs.Re7 = false;
            outputs.Re8 = true;
            outputs.Re9 = false;
            outputs.Re10 = true;
            outputs.Re11 = false;
            outputs.Re12 = true;

            bb.WriteOutputs(outputs);

            if (Shell.ShowDialog("Comprobación salidas digitales", new ImageView("Se encienden los leds destacados en la imagen?", PathImage + "LEDS_RE_PARES.png"), MessageBoxButtons.YesNo) != DialogResult.Yes)
                throw new Exception("Error en la activación de las salidas RE impares");

            Resultado.Set("RE_PAIR_OUTPUTS", "OK", ParamUnidad.SinUnidad);

            outputs.Re1 = false;
            outputs.Re2 = false;
            outputs.Re3 = false;
            outputs.Re4 = false;
            outputs.Re5 = false;
            outputs.Re6 = false;
            outputs.Re7 = false;
            outputs.Re8 = false;
            outputs.Re9 = false;
            outputs.Re10 = false;
            outputs.Re11 = false;
            outputs.Re12 = false;


            bb.WriteOutputs(outputs);
        }

        public void TestVerifyDigitalInputs()
        {
            Shell.ShowDialog("Comprobación salidas digitales", new ImageView("Configuración de los interruptores", PathImage + "DIGITAL_INPUT_1.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_1, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_2, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_3, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_4, false);

            Resultado.Set("DIGITAL_INPUT_1", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView("Configuración de los interruptores", PathImage + "DIGITAL_INPUT_2.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_1, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_2, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_3, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_4, false);

            Resultado.Set("DIGITAL_INPUT_2", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "DIGITAL_INPUT_3.png"), MessageBoxButtons.OK);
            
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_1, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_2, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_3, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_4, false);

            Resultado.Set("DIGITAL_INPUT_3", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "DIGITAL_INPUT_4.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_1, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_2, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_3, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.INPUT_4, true);

            Resultado.Set("DIGITAL_INPUT_4", "OK", ParamUnidad.SinUnidad);
        }

        public void TestVerifyPWMAndResetOutputs()
        {
            var outputs = new InversorBeagleBoneControl.RESETAndPWMOutputs();

            outputs.Reset1 = true;
            outputs.Reset2 = false;
            outputs.Reset3 = true;
            outputs.Reset4 = false;
            outputs.Pwm1_top = true;
            outputs.Pwm1_bot = false;
            outputs.Pwm2_top = true;
            outputs.Pwm2_bot = false;
            outputs.Pwm3_top = true;
            outputs.Pwm3_bot = false;
            outputs.Pwm4_top = true;
            outputs.Pwm4_bot = false;
            outputs.Pwm5_top = true;
            outputs.Pwm5_bot = false;
            outputs.Pwm6_top = true;
            outputs.Pwm6_bot = false;

            bb.WriteOutputs(outputs);

            if (Shell.ShowDialog("Comprobación salidas digitales", new ImageView("Se encienden los leds destacados en la imagen?", PathImage + "LEDS_RESET_Y_PWM_IMPARES.png"), MessageBoxButtons.YesNo) != DialogResult.Yes)
                throw new Exception("Error en la activación de las salidas RESET y PWM impares");

            Resultado.Set("RESET_AND_PWM_IMPAIR_OUTPUTS", "OK", ParamUnidad.SinUnidad);

            outputs.Reset1 = false;
            outputs.Reset2 = true;
            outputs.Reset3 = false;
            outputs.Reset4 = true;
            outputs.Pwm1_top = false;
            outputs.Pwm1_bot = true;
            outputs.Pwm2_top = false;
            outputs.Pwm2_bot = true;
            outputs.Pwm3_top = false;
            outputs.Pwm3_bot = true;
            outputs.Pwm4_top = false;
            outputs.Pwm4_bot = true;
            outputs.Pwm5_top = false;
            outputs.Pwm5_bot = true;
            outputs.Pwm6_top = false;
            outputs.Pwm6_bot = true;

            bb.WriteOutputs(outputs);

            if (Shell.ShowDialog("Comprobación salidas digitales", new ImageView("Se encienden los leds destacados en la imagen?", PathImage + "LEDS_RESET_Y_PWM_PARES.png"), MessageBoxButtons.YesNo) != DialogResult.Yes)
                throw new Exception("Error en la activación de las salidas RESET y PWM pares");

            Resultado.Set("RESET_AND_PWM_PAIR_OUTPUTS", "OK", ParamUnidad.SinUnidad);

            outputs.Reset1 = false;
            outputs.Reset2 = false;
            outputs.Reset3 = false;
            outputs.Reset4 = false;
            outputs.Pwm1_top = false;
            outputs.Pwm1_bot = false;
            outputs.Pwm2_top = false;
            outputs.Pwm2_bot = false;
            outputs.Pwm3_top = false;
            outputs.Pwm3_bot = false;
            outputs.Pwm4_top = false;
            outputs.Pwm4_bot = false;
            outputs.Pwm5_top = false;
            outputs.Pwm5_bot = false;
            outputs.Pwm6_top = false;
            outputs.Pwm6_bot = false;

            bb.WriteOutputs(outputs);
        }

        public void TestVerifyErrorInputReading()
        {
            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "ERROR_INPUT_1.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR1, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR2, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR3, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR4, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR5, true);

            Resultado.Set("ERROR_INPUT_1", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "ERROR_INPUT_2.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR1, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR2, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR3, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR4, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR5, true);

            Resultado.Set("ERROR_INPUT_2", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "ERROR_INPUT_3.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR1, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR2, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR3, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR4, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR5, true);

            Resultado.Set("ERROR_INPUT_3", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "ERROR_INPUT_4.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR1, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR2, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR3, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR4, false);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR5, true);

            Resultado.Set("ERROR_INPUT_4", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "ERROR_INPUT_5.png"), MessageBoxButtons.OK);

            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR1, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR2, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR3, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR4, true);
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR5, false);

            Resultado.Set("ERROR_INPUT_5", "OK", ParamUnidad.SinUnidad);

            Shell.ShowDialog("Comprobación salidas digitales", new ImageView(string.Empty, PathImage + "ERROR_INPUT_ALL.png"), MessageBoxButtons.OK);
            
            VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs.ERROR, false);

            Resultado.Set("ERROR_INPUTS_ALL", "OK", ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (bb != null)
                bb.Dispose();
        }

        #region Metodos auxiliares

        private void VerifyAnalogInputReading(Dictionary<InversorBeagleBoneControl.AnalogInputs, AdjustValueDef> analogInputs)
        {
            foreach (KeyValuePair<InversorBeagleBoneControl.AnalogInputs, AdjustValueDef> analogInput in analogInputs)
                TestMeasureBase(analogInput.Value, (step) =>
                {
                    var reading = bb.ReadAnalogInput(analogInput.Key);
                    return reading;
                }, 0, 5, 200);
        }

        private void VerifyDigitalInputReading(InversorBeagleBoneControl.DigitalInputs input, bool expectedValue)
        {
            SamplerWithCancel((p) => 
            {
                Thread.Sleep(100);

                var reading = bb.ReadDigitalInput(input);
                if (reading != (ushort)(expectedValue ? 1 : 0))
                    if (Shell.MsgBox("No se ha detectado la entrada digital " + input.ToString() + " volver a verificar la entrada?", "Verificación de las entradas digitales", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        return false;
                    else
                        throw new Exception("El usuario ha cancelado la comprobación de la entrada digitale");

                return true;
            }, "Error no se detecta la entrada digital " + input.ToString(), cancelOnException:true, throwException:true);
        } 

        #endregion
    }
}
