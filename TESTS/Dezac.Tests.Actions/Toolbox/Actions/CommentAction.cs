﻿using log4net;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(CommentActionDescription))]
    public class CommentAction : ActionBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("CommentAction");

        [Description("Comentarios del usuario editable para remarcar cualquier consideración o función de la secuencia global")]
        [Category("2. Comments")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string CommentsAction { get; set; }

        public override void Execute()
        {
            logger.Info(CommentsAction);
        }
    }

    public class CommentActionDescription : ActionDescription
    {
        public override string Name { get { return "Comment"; }}
        public override string Description { get { return "Comentarios sobre la secuencia de Test"; } }
        public override string Category { get { return "Actions"; } }
        public override Image Icon { get { return Properties.Resources.note; } }
    }
}
