﻿namespace Dezac.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.ORDENPROCESOFASEFILE")]
    public partial class ORDENPROCESOFASEFILE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMOPFF { get; set; }

        public int NUMREGISTRO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTIPODOC { get; set; }

        public byte[] FICHERO { get; set; }        

        public virtual ORDENPROCESOFASE ORDENPROCESOFASE { get; set; }
    }
}
