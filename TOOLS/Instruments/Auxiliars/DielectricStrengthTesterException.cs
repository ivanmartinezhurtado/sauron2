﻿using Instruments.Utility;
using System;
using System.Collections.Generic;

namespace Instruments.Auxiliars
{
    public class DielectricStrengthTesterException : InstrumentException
    {
        public enum Codes : int
        {
            NO_ERROR = 0,
            SYNTAX_ERROR = 102,
            INVALID_SEPARATOR = 103,
            PARAMETER_NOT_ALLOWED = 108,
            MISSING_PARAMETER = 109,
            PROGRAM_MNEMONIC_TOO_LONG = 112,
            UNDEFINED_HEADER = 113,
            HEADER_SUFFIX_OUT_OF_RANGE = 114,
            NUMERIC_DATA_ERROR = 120,
            CHARACTER_DATA_ERROR = 140,
            INVALID_CHARACTER_DATA = 141,
            INVALID_STRING_DATA = 151,
            STRING_DATA_NOT_ALLOWED = 158,
            EXPRESSION_ERROR = 170,
            EXECUTION_ERROR = 200,
            COMMAND_PROTECTED = 203,
            SETTINGS_CONFLICT = 221,
            DATA_OUT_OF_RANGE = 222,
            TOO_MUCH_DATA = 223,
            MEMORY_USE_ERROR = 290,
            OUT_OF_MEMORY = 291,
            REFERENCED_NAME_DOES_NOT_EXIST = 292,
            REFERENCED_NAME_DOES_EXIST = 293,
            QUEUE_OVERFLOW = 350,
            PARITY_ERROR = 361,
            TIMEOUT_ERROR = 365,
            INPUT_BUFFER_OVERRUN = 363,
            QUEUE_ERROR = 400,
            QUERY_INTERRUPTED = 410,
            QUERY_TERMINATED = 420
        };

        private static readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            {(int)Codes.NO_ERROR, "No hay errores"},
            {(int)Codes.SYNTAX_ERROR, "Error de sintaxis del comando"},
            {(int)Codes.INVALID_SEPARATOR, "Caracter separador de comandos inválido"},
            {(int)Codes.PARAMETER_NOT_ALLOWED, "El parametro de entrada no ha sido aceptado"},
            {(int)Codes.MISSING_PARAMETER, "A la función le falta el parametro"},
            {(int)Codes.PROGRAM_MNEMONIC_TOO_LONG, "Longitud de la cabecera del comando mayor de la esperada"},
            {(int)Codes.UNDEFINED_HEADER, "No se identifica la cabecera del comando introducido"},
            {(int)Codes.HEADER_SUFFIX_OUT_OF_RANGE, "Sufijo de la cabecera del comando fuera de rango"},
            {(int)Codes.NUMERIC_DATA_ERROR, "Error de parametro numerico"},
            {(int)Codes.CHARACTER_DATA_ERROR, "Error de parametro de caracter"},
            {(int)Codes.INVALID_CHARACTER_DATA, "Parametro de caracter invalido"},
            {(int)Codes.INVALID_STRING_DATA, "Parametro de cadena de texto invalida"},
            {(int)Codes.STRING_DATA_NOT_ALLOWED, "El comando no acepta una cadena de texto como parametro"},
            {(int)Codes.EXPRESSION_ERROR, "Error en la expresión del comando debido a parametros incompletos"},
            {(int)Codes.EXECUTION_ERROR, "Error en la ejecución del comando"},
            {(int)Codes.COMMAND_PROTECTED, "El dispositivo rechaza este comando"},
            {(int)Codes.SETTINGS_CONFLICT, "El dispositivo rechaza esta configuración"},
            {(int)Codes.DATA_OUT_OF_RANGE, "Datos fuera de rango"},
            {(int)Codes.TOO_MUCH_DATA, "Longitud de la cadena de texto entrada demasiado grande"},
            {(int)Codes.MEMORY_USE_ERROR, "Error en la lectura o escritura de la memoria"},
            {(int)Codes.OUT_OF_MEMORY, "El valor de memoria pedido no existe"},
            {(int)Codes.REFERENCED_NAME_DOES_NOT_EXIST, "El nombre referenciado de la memoria no existe"},
            {(int)Codes.REFERENCED_NAME_DOES_EXIST, "El nombre a asignar en la memoria ya existe"},
            {(int)Codes.QUEUE_OVERFLOW, "Cantidad de errores almacenados mayor de 30"},
            {(int)Codes.PARITY_ERROR, "Error de paridad en el mensaje"},
            {(int)Codes.TIMEOUT_ERROR, "No se ha recibido el caracter de fin de comando dentro del tiempo de espera"}, 
            {(int)Codes.INPUT_BUFFER_OVERRUN, "El buffer de entrada contiene mas de 1024 caracteres"},
            {(int)Codes.QUEUE_ERROR, "El buffer de salida contiene mas de 256 caracteres"},
            {(int)Codes.QUERY_INTERRUPTED, "La petición de información se ha visto interrumpida inesperadamente"},
            {(int)Codes.QUERY_TERMINATED, "Aun con la salida de datos vacía, se le ha pedido al equipo datos"},
        };

        public DielectricStrengthTesterException(Codes code)
            : base((int)code, GetMessage((int)code))
        {
        }

        public DielectricStrengthTesterException(Codes code, Exception innerException)
            : base((int)code, GetMessage((int)code), innerException)
        {
        }

        private static string GetMessage(int code)
        {
            string message = null;

            messages.TryGetValue(code, out message);

            return message ?? "Error desconocido!";
        }
    }
}
