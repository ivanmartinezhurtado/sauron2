﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(SSHReadLineActionDescription))]
    public class SSHReadLine : ActionBase
    {
        [DefaultValue("SSH_Device")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Texto esperado")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable TextToRead { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("TimeOut en milisegundos")]
        public int TimeOut { get; set; }

        [DefaultValue((byte)5)]
        [Category("3. Configuration")]
        [Description("Reintentos")]
        public byte Retries { get; set; }

        [DefaultValue(100)]
        [Category("3. Configuration")]
        [Description("Tiempo de intervalo entre lecturas")]
        public int TimeInterval { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            Retries = (byte)(Retries == 0 ? 5 : Retries);
            TimeInterval = TimeInterval == 0 ? 100 : TimeInterval;

            var ssh = GetVariable(DeviceName.Replace('-', '_')) as SSHClient;

            if (ssh == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el SSHClient").Throw();

            var textToRead = VariablesTools.ResolveText(TextToRead.Value.ToString());
            testBase.SamplerWithCancel((p) =>
            {              
                var response = ssh.Shell.ReadLine(TimeSpan.FromMilliseconds(TimeOut));
                Context.ResultList.Add(response);
                testBase.Logger.DebugFormat(response);
                return response.ToLower().Trim() == textToRead.ToLower().Trim();

            }, "No se ha recibido la respuesta esperada por SSH", Retries, TimeInterval, 500, false, false);
        }
    }

    public class SSHReadLineActionDescription : ActionDescription
    {
        public override string Name { get { return "SSHShellReadLine"; } }
        public override string Description { get { return "Lee una linea por una tty de la sesión SSH y comprueba que contenga el texto a leer especificado"; } }
        public override string Category { get { return "Tools/Ethernet/SSH"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "SSHInitialization;SSHFinish:2"; } }
    }
}
