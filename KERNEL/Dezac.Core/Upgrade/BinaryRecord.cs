﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dezac.Core.Upgrade
{
    public class BinaryRecord
    {
        public byte[] Data { get; set; }
    }

    public class IntelHexReader : IBinaryReader
    {
        const uint APP_BUF_SIZE = 128;

        private List<BinaryRecord> records;

        public byte[] ReadRecord(int recordNumber)
        {
            return records[recordNumber].Data;
        }

        public int RecordsCount
        {
            get { return records.Count; }
        }

        public string FileName { get; protected set; }

        public void Load(string fileName)
        {
            FileName = fileName;

            if (!File.Exists(FileName))
                throw new Exception(string.Format("Binary file {0} not found!", FileName));

            try
            {
                ReadRecords(ReadFile());
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Invalid binary file {0} by {1}", FileName, ex.Message));
            }
        }

        protected byte[] ReadFile()
        {
            byte aux;
            byte fadd;
            byte fini;
            byte fnext;
            byte fsalir;
            byte fbloc;
            byte fresto;
            byte nbytes;
            byte nbytes2;
            byte resto;
            byte[] aux_2 = new byte[2];
            byte[] aux_3 = new byte[8];
            byte[] auxresto = new byte[15];
            byte[] HNbloc = new byte[4];
            byte[] HNWord = new byte[4];

            UInt16 dif;
            UInt16 i;
            UInt16 NWord;

            short Nbloc_old;
            short Nbloc;

            UInt32 suma_datos;
            UInt32 address;
            UInt32 address_base;
            UInt32 address_next;
            UInt32 addoffset;
            UInt32 nbytes_next;
            UInt32 ncar;
            UInt32 address_old;

            using (var objFile = new FileStream(FileName, FileMode.Open, FileAccess.Read))
            {
                using (MemoryStream objFileBoot = new MemoryStream())
                {

                    fini = 1;
                    ncar = 0;
                    fsalir = 0;
                    fnext = 0;
                    fadd = 0;
                    fresto = 0;
                    resto = 0;
                    //address_old = -1;
                    address_old = 0xFFFFFFFF;
                    addoffset = 0;
                    NWord = 64;
                    // Alberto
                    address_base = 0;
                    // End Alberto
                    nbytes_next = 0;
                    suma_datos = 0;
                    Nbloc_old = 0;
                    address_next = 0;
                    dif = 0;

                    while (ncar < 9000000 && fsalir == 0)
                    {
                        fbloc = 0;
                        aux = (byte)objFile.ReadByte();
                        ncar++;

                        if (aux == ':')
                        {
                            for (i = 0; i < 2; i++)
                            {
                                aux_2[i] = (byte)objFile.ReadByte();
                                ncar++;
                            }// end for

                            nbytes = (byte)Tools.WhichInt((char)aux_2[0]);
                            nbytes = (byte)((nbytes << 4) | Tools.WhichInt((char)aux_2[1]));

                            if (nbytes == 0)
                            {
                                if (suma_datos < 256)				// omplim am FF fins fins a completar l'ultim bloc
                                {
                                    for (i = (UInt16)suma_datos; i < 256; i++)
                                        objFileBoot.WriteByte((byte)'F');
                                    objFileBoot.WriteByte((byte)'\r');
                                    objFileBoot.WriteByte((byte)'\n');

                                }
                                fsalir = 1;
                            }
                            else
                            {
                                nbytes2 = (byte)(nbytes * 2);
                                aux_3[0] = (byte)'0';
                                aux_3[1] = (byte)'0';
                                aux_3[2] = (byte)'0';
                                aux_3[3] = (byte)'0';
                                for (i = 4; i < 8; i++)
                                {
                                    aux_3[i] = (byte)objFile.ReadByte();
                                    ncar++;
                                }
                                address = 0;
                                for (int iX = 0; iX < 8; iX++)
                                {
                                    address = address << 4;
                                    address = address | Tools.WhichInt((char)aux_3[iX]);
                                }
                                address = address + addoffset;
                                aux = (byte)objFile.ReadByte();
                                aux = (byte)objFile.ReadByte();

                                if (aux == '2')
                                    addoffset = 65536;
                                else if (aux == '4')
                                {
                                    for (i = 0; i < 4; i++)
                                    {
                                        aux_3[i] = (byte)objFile.ReadByte();
                                        ncar++;
                                    }
                                    addoffset = 0;
                                    for (int iX = 0; iX < 8; iX++)
                                    {
                                        addoffset = addoffset << 4;
                                        addoffset = addoffset | Tools.WhichInt((char)aux_3[iX]);
                                    }
                                    // AAA
                                }
                                else if (aux == '0')
                                {
                                    if (address_old < 0)
                                    {
                                        address_base = address;
                                        address_old = (UInt32)address;
                                        address_next = address;
                                    }
                                    Nbloc = (short)((address - address_base) / 128);   // /64;      					
                                    if (Nbloc != Nbloc_old && suma_datos < 256 && (fini == 0))
                                    {
                                        for (i = (UInt16)suma_datos; i < 256; i++)
                                            objFileBoot.WriteByte((byte)'F');
                                        objFileBoot.WriteByte((byte)'\r');
                                        objFileBoot.WriteByte((byte)'\n');
                                    }// end if

                                    if (address != address_next && Nbloc == Nbloc_old)
                                    {
                                        fnext = 1;
                                        nbytes_next = (address - address_next);  //*2;
                                    }// end if

                                    if (Nbloc != Nbloc_old && (address - (Nbloc * 128)) != 0)		//per si l'adreça no és inici de bloc
                                    {
                                        fadd = 1;
                                        dif = (UInt16)(address - (Nbloc * 128));
                                    }// end if
                                    fbloc = 1;

                                    while (fbloc != 0)
                                    {
                                        if ((Nbloc != Nbloc_old) || (fini != 0))
                                        {
                                            HNbloc[0] = (byte)Tools.WhichHex((Nbloc >> 12) & 0x000F);
                                            HNbloc[1] = (byte)Tools.WhichHex((Nbloc >> 8) & 0x000F);
                                            HNbloc[2] = (byte)Tools.WhichHex((Nbloc >> 4) & 0x000F);
                                            HNbloc[3] = (byte)Tools.WhichHex(Nbloc & 0x000F);

                                            HNWord[0] = (byte)Tools.WhichHex((NWord >> 12) & 0x000F);
                                            HNWord[1] = (byte)Tools.WhichHex((NWord >> 8) & 0x000F);
                                            HNWord[2] = (byte)Tools.WhichHex((NWord >> 4) & 0x000F);
                                            HNWord[3] = (byte)Tools.WhichHex(NWord & 0x000F);

                                            for (i = 0; i < 4; i++)
                                                objFileBoot.WriteByte(HNbloc[i]);
                                            for (i = 0; i < 4; i++)
                                                objFileBoot.WriteByte(HNWord[i]);

                                            Nbloc_old = (short)Nbloc;
                                            suma_datos = 0;
                                            fini = 0;
                                        }// end if

                                        if (fnext != 0)
                                        {
                                            for (i = 0; i < nbytes_next; i++)	// omplim amb FF els espais entre dues adreces no consecutives
                                            // dins el mateix bloc.
                                                if (suma_datos < 256)
                                                {
                                                    objFileBoot.WriteByte((byte)'F');
                                                    objFileBoot.WriteByte((byte)'F');
                                                    suma_datos = suma_datos + 2;
                                                }//end if
                                            fnext = 0;
                                        }// end if

                                        if (fadd != 0)
                                        {
                                            for (i = (UInt16)suma_datos; i < (dif * 2); i++)      // especial adreces que no 
                                            // són inci de bloc
                                                if (suma_datos < 256)
                                                {
                                                    objFileBoot.WriteByte((byte)'F');
                                                    suma_datos++;
                                                }
                                                else
                                                    for (i = 0; i < nbytes2; i++)		// guarda el bytes restants per 
                                                    {								// afegir-los al següent bloc
                                                        auxresto[i] = (byte)objFile.ReadByte();
                                                        resto++;
                                                        fresto = 1;
                                                        fbloc = 0;
                                                    }//end for
                                            fadd = 0;
                                        }//end if

                                        if (fresto != 0)
                                        {
                                            for (i = 0; i == resto; i++)
                                            {
                                                objFileBoot.WriteByte((byte)auxresto[i]);
                                                suma_datos++;
                                            }//end for
                                            resto = 0;
                                            fresto = 0;
                                        }//end if
                                        address_next = address + nbytes; //+numwords;

                                        while ((suma_datos < 256) && (nbytes2 != 0))
                                        {
                                            aux = (byte)objFile.ReadByte();
                                            objFileBoot.WriteByte((byte)aux);
                                            ncar++;
                                            suma_datos++;
                                            nbytes2--;
                                        }//end while

                                        if (suma_datos == 256)
                                        {
                                            Nbloc++;
                                            objFileBoot.WriteByte((byte)'\r');	//salt de linea
                                            objFileBoot.WriteByte((byte)'\n');
                                            suma_datos = 0;
                                        }
                                        else
                                            fbloc = 0;
                                    }// end while
                                }// end if
                            }// end if
                        }// end if		
                    }// end while	

                    return objFileBoot.ToArray();
                }
            }
        }

        protected void ReadRecords(byte[] data)
        {
            byte aux;
            var m_sci_s1_buf = new byte[APP_BUF_SIZE + 4];
            var m_buf_M = new byte[(APP_BUF_SIZE + 4) * 2];

            int maxRecords = data.Length / 266;

            records = new List<BinaryRecord>(maxRecords);

            using (var ms = new MemoryStream(data))
            {
                while (records.Count < maxRecords)
                {
                    if (records.Any())
                    {
                        aux = (byte)ms.ReadByte();	// saltamos el '\r'\n del fichero
                        aux = (byte)ms.ReadByte();	// saltamos el '\r'\n del fichero
                    }

                    for (int i = 0; i < 264; i++)
                    {
                        // La conversion a byte da error para los 4 primeros bytes
                        aux = Convert.ToByte(ms.ReadByte());   // lee una linea (bloque) y lo mete en un buffer 
                        if (aux < 10)
                        {
                            aux = (byte)(aux % 0x30);
                            aux = (byte)(aux + 0x30);
                        }
                        else
                        {
                            if (aux < 16)
                                aux = (byte)(aux + 0x37);
                        }
                        m_buf_M[i] = aux;     // lee una linea (bloque) y lo mete en un buffer 
                    }

                    Tools.Hex2Bin(m_buf_M, ref m_sci_s1_buf, 264);
                    records.Add(new BinaryRecord { Data = m_sci_s1_buf.ToArray() });
                }
            }
        }
    }

    public class BinReader : IBinaryReader
    {
        private List<BinaryRecord> records;

        public byte[] ReadRecord(int recordNumber)
        {
            return records[recordNumber].Data;
        }

        public int RecordsCount
        {
            get { return records.Count; }
        }

        public string FileName { get; protected set; }

        public int LenBlock { get; protected set; }

        public void Load(string fileName, int lenBlock)
        {
            FileName = fileName;
            LenBlock = lenBlock;

            if (!File.Exists(FileName))
                throw new Exception(string.Format("Binary file {0} not found!", FileName));

            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                // Get the file size
                var imageSize = (UInt32)fs.Length;
                var numBlock = (imageSize + LenBlock - 1) / LenBlock;
                ReadRecords(fs, (int)numBlock);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Invalid binary file {0} by {1}", FileName, ex.Message));
            }
        }

        protected void ReadRecords(FileStream fs, int numBlock)
        {
            records = new List<BinaryRecord>((int)numBlock);
            using (var br = new BinaryReader(fs))
            {
                while (records.Count < numBlock)
                {
                    var record = new BinaryRecord();
                    var listData = br.ReadBytes((int)LenBlock).ToList();
                    if (listData.Count < LenBlock)
                        listData.AddRange(new byte[LenBlock]);

                    record.Data = listData.ToArray();
                    records.Add(record);
                }
            }
        }

        public void Load(string fileName)
        {
            throw new NotImplementedException();
        }
    }

}
