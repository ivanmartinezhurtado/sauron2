﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskRunner;
using TaskRunner.Model;

namespace WinTaskRunner.Reports
{
    public interface ITestReportAdapter
    {
        object GetReport(TestResult testResult);
    }

    public class TestReportAdapter : ITestReportAdapter
    {
        public object GetReport(TestResult testResult)
        {
            var ds = new Reports();

            var test = ds.TestReport.NewTestReportRow();

            test.Id = 1;
            test.StartTime = testResult.StartTime;
            test.EndTime = testResult.EndTime;
            test.Duration = string.Format("{0} seg.", testResult.Duration.TotalMilliseconds / 1000);
            test.Result = (int)testResult.ExecutionResult;
            test.Description = "TEST REPORT";

            var context = testResult.Context.Services.Get<ITestContext>();
            if (context != null)
            {
                test.IdUser = context.IdEmpleado;
                test.ProductId = string.Format("{0}/{1}", context.NumProducto, context.Version);
                //test.ProductDescription = 
                test.OperationId = context.IdFase;
                test.SystemId = context.PC;
                test.SerialNumber = context.TestInfo != null ? context.TestInfo.NumSerie ?? string.Empty : string.Empty;
                test.ManufacturingId = context.TestInfo != null && context.TestInfo.NumBastidor.HasValue ? context.TestInfo.NumBastidor.ToString() ?? string.Empty : string.Empty;
            }

            ds.TestReport.AddTestReportRow(test);

            foreach (var step in testResult.StepResults)
            {
                if (step.Step.Action != null)
                {
                    var item = ds.TestStep.NewTestStepRow();

                    item.ReportId = 1;
                    item.Id = ds.TestStep.Count + 1;
                    item.Step = step.Step.Name;
                    item.StartTime = step.StartTime;
                    item.EndTime = step.EndTime;
                    item.Duration = string.Format("{0} seg.", step.Duration.TotalMilliseconds / 1000);
                    item.Result = step.Succeed ? "OK" : "ERROR";
                    item.Comments = step.Step.Comments;

                    ds.TestStep.AddTestStepRow(item);
                }
            }

            return ds;
        }
    }
}
