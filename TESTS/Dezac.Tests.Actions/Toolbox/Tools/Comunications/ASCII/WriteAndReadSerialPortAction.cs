﻿using Dezac.Core.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Threading;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(WriteAndReadSerialPortActionDescription))]
    public class WriteAndReadSerialPortAction : ActionBase
    {
        [Description("Texto a enviar")]
        [Category("3. Configuration")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToSend { get; set; }

        [Description("Valor de texto que si se encuentra sale del action")]
        [Category("3. Configuration")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToRead { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(200)]
        [Description("Tiempo de espera a recibir la trama amtes de dar error")]
        public int TimeOut { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(500)]
        [Description("Tiempo de espera antes de realizar otra comunicación")]
        public int TimeInterval { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(0)]
        [Description("Tiempo de espera antes de realizar la primera comunicación")]
        public int InitialDelay { get; set; }

        [Category("3.2 Retries")]
        [DefaultValue(1)]
        [Description("Número de respeticiones de lectura")]
        public int RetryReads { get; set; }

        [Category("3.2 Retries")]
        [DefaultValue(3)]
        [Description("Número de respeticiones de excepciones")]
        public int RetryException { get; set; }

        [Category("3.3 Exception")]
        [DefaultValue(false)]
        [Description("Si se habilita la cancelación o Si se encuenrta una excepción")]
        public bool CancelOnException { get; set; }

        [Category("3.3 Exception")]
        [DefaultValue(false)]
        [Description("Si se habilita la propagación de la excepción")]
        public bool ThrowException { get; set; }

        public override void Execute()
        {
            var sender = GetVariable(DeviceName) as SerialPort;
            if (sender == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el serial port").Throw();

            string[] stringSeparators = new string[] { "\r\n" };

            string[] linesTx = TextToSend.Split(stringSeparators, StringSplitOptions.None);

            string[] linesRx = TextToRead.Split(stringSeparators, StringSplitOptions.None);

            sender.ReadTimeout = TimeOut;

            int retryExceptionTmp = 0;

            for (int i = 0; i < linesTx.Length; i++)
            {
                Thread.Sleep(InitialDelay);

                var Tx = VariablesTools.ResolveText(linesTx[i]);

                sender.WriteLine(Tx.ToString());

                Context.ResultList.Add(string.Format("{0} - TX: {1}", DeviceName, Tx));

                Logger.InfoFormat("TX: {0}", this.GetType().Name, Tx);

                var sampler = Sampler.Run(
                () => { return new SamplerConfig { Interval = TimeInterval, InitialDelay = 0, NumIterations = RetryReads, CancelOnException = CancelOnException, ThrowException = ThrowException, CancellationToken = SequenceContext.Current.CancellationToken }; },
                (step) =>
                {
                    try
                    {
                        var tramaRx = sender.ReadLine();

                        retryExceptionTmp = 0;
                        sender.ReadTimeout = TimeOut;

                        tramaRx = tramaRx.Replace("\r", "");
                        tramaRx = tramaRx.Replace("\n", "");

                        Logger.DebugFormat("PORT: {0} RX: {1}", DeviceName, tramaRx);

                        Context.ResultList.Add(string.Format("{0} - RX: {1}", DeviceName, tramaRx));

                        var rxContext = VariablesTools.ResolveText(linesRx[i]);

                        Logger.DebugFormat("RX leido: {0} == RX Esperado: {1}", tramaRx, rxContext);

                        step.Cancel = tramaRx.Contains((string)rxContext);
                    }
                    catch (TimeoutException e)
                    {
                        Logger.WarnFormat("{0}, ", e.GetType().Name, e);
                        retryExceptionTmp += 1;

                        if (retryExceptionTmp > RetryException)
                            Error().UUT.COMUNICACIONES.NO_COMUNICA("No se recibe respuesta");
                    }
                });

                Assert.IsTrue(sampler.Canceled, Error().UUT.COMUNICACIONES.NO_CORRESPONDE_CON_ENVIO("No se ha recibido la trama RX esperada"));
            }
        }
    }

    public class WriteAndReadSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "Write&ReadASCIISerialPort"; } }
        public override string Description { get { return "Envía y espera a recibir la cadena de de texto especificada"; } }
        public override string Category { get { return "Tools/Comunications/ASCII"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
