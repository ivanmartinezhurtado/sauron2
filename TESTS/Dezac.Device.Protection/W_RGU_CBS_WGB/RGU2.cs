﻿using System;
using System.ComponentModel;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class RGU2 : DeviceBase
    {
        public RGU2()
        {
        }

        public RGU2(int port = 5)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public void FlagTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONTROL, (ushort)ControlOperations.TEST);
        }

        public void Reset()
        {
            LogMethod();

            var retries = Modbus.Retries;
            Modbus.Retries = 0;

            try
            {
                Modbus.Write((ushort)Registers.CONTROL, (ushort)ControlOperations.RESET);
            }
            catch(TimeoutException)
            {
                _logger.Debug("RESETEANDO EQUIPO");
                Thread.Sleep(3000);
            }

            Modbus.Retries = retries;
            
        }

        public ushort ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)ProductionParametersRegisters.FIRMWARE_VERSION);
        }

        public InputReadings ReadKeyboard()
        {
            LogMethod();
            return (InputReadings)Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public int ReadAdjustConstant(AdjustType adjustType)
        {
            LogMethod();
            switch (adjustType)
            {
                case AdjustType.ADJUST_VOLTAGE:
                    return Modbus.ReadRegister((ushort)AdjustRegisters.REFERENCE_VOLTAGE);
                case AdjustType.ADJUST_CURRENT_1:
                    return (Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_1_H) << 16) + Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_1_L);
                case AdjustType.ADJUST_CURRENT_2:
                    return ((Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_2_H) << 16) + Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_2_L));
                case AdjustType.ADJUST_CURRENT_3:
                    return ((Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_3_H) << 16) + Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_3_L));
                case AdjustType.ADJUST_CURRENT_4:
                    return ((Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_4_H) << 16) + Modbus.ReadRegister((ushort)AdjustRegisters.SCALE_4_L));
                default:
                    throw new Exception("Error. El registro pedido no es valido.");
            }
        }

        public ushort ReadToroidalState()
        {
            LogMethod();
            return  Modbus.ReadRegister((ushort)Registers.LECTURA_ESTADO_TOROIDAL);
        }

        public double ReadTriggerCurrent()
        {
            LogMethod();
            var currenttrigger= Modbus.ReadRegister((ushort)ConfigurationParametersRegisters.TRIGGER_CURRENT);
            return (double)currenttrigger / 1000;
        }

        public ushort ReadModel()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)ProductionParametersRegisters.DEVICE_MODEL);
        }

        public ushort ReadErrorCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)ProductionParametersRegisters.ERROR_CODE);
        }

        public bool ReadTriggerRelayLogic()
        {
            LogMethod();
            return Convert.ToBoolean(Modbus.ReadRegister((ushort)ConfigurationParametersRegisters.TRIGGER_RELE_LOGIC));
        }

        public bool ReadAuxiliarRelayLogic()
        {
            LogMethod();
            return Convert.ToBoolean(Modbus.ReadRegister((ushort)ConfigurationParametersRegisters.AUXILIAR_RELAY_LOGIC));
        }

        public uint ReadSerialNumber()
        {
            LogMethod();
            return (uint)((Modbus.ReadRegister((ushort)ProductionParametersRegisters.SERIAL_NUMBER_H) << 16) + Modbus.ReadRegister((ushort)ProductionParametersRegisters.SERIAL_NUMBER_L));
        }

        public string ReadInitialMessage()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.INITIAL_MESSAGE, 1) + Modbus.ReadString((ushort)Registers.INITIAL_MESSAGE + 1, 1);
        }

        #region Write operations

        public void WriteErrorCode(ushort errorCode = 0xFFFF)
        {
            LogMethod();
            Modbus.Write((ushort)ProductionParametersRegisters.ERROR_CODE, errorCode);
        }

        public void WriteDisplaySegments(DisplayOperations operation)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DISPLAY, (ushort)operation);
        }

        public void WriteOutputs(OutputOperations operation)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)operation);
        }

        public void WriteAdjustOperation(AdjustOperation operation)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONTROL, (ushort)operation);
        }

        public void WriteTriggerCurrent(TriggerCurrents triggerCurrent)
        {
            LogMethod();
            Modbus.Write((ushort)ConfigurationParametersRegisters.TRIGGER_CURRENT_RANGE, (ushort)triggerCurrent);
        }

        public void WriteTriggerTime(TriggerTimes triggerTime)
        {
            LogMethod();
            Modbus.Write((ushort)ConfigurationParametersRegisters.TRIGGER_TIME_RANGE, (ushort)triggerTime);
        }

        public void WriteModel(ushort modelNumber)
        {
            LogMethod();
            Modbus.Write((ushort)ProductionParametersRegisters.DEVICE_MODEL, modelNumber);
        }

        public void WriteTransformer(TransfomerOperations operation)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)operation);
        }

        public void WriteClearKeyboard()
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.INPUTS, 0);
        }

        public void WriteFrameNumber(int frameNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)ProductionParametersRegisters.FRAME_NUMBER_L, frameNumber);
        }

        public void WriteInitialMessage(string screenMessage)
        {
            LogMethod();
            screenMessage = screenMessage.PadLeft(4, '0');
            Modbus.WriteString((ushort)Registers.INITIAL_MESSAGE, screenMessage);
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)ProductionParametersRegisters.SERIAL_NUMBER_H, (int)serialNumber);
        }

        #endregion

        #region  Enums Parameters Value

        public enum AdjustType
        {
            ADJUST_VOLTAGE,
            ADJUST_CURRENT_1,
            ADJUST_CURRENT_2,
            ADJUST_CURRENT_3,
            ADJUST_CURRENT_4,
        }

        public enum ControlOperations
        {
            TEST = 2,
            RESET = 0,
        }

        public enum TransfomerOperations
        {
            ENFORCE_TRANSFORMER_TEST = 0x0800,
            CLEAR_TRANSFORMER_TEST_RESULT = 0x1000,
        }

        public enum TransformerResults
        {
            CORRECT = 0x0000,
            INCORRECT = 0x0001,
        }

        public enum OutputOperations
        {
            OFF_ALL_LEDS_OUTPUTS = 0x0000,
            ACTIVATE_20_PERCENT_LED = 0x0001,
            ACTIVATE_40_PERCENT_LED = 0x0002,
            ACTIVATE_60_PERCENT_LED = 0x0004,
            ACTIVATE_80_PERCENT_LED = 0x0008,
            ACTIVATE_100_PERCENT_LED = 0x0010,
            ACTIVATE_LED_POWER = 0x0020,
            ACTIVATE_TRIGGER_LED = 0x0040,
            ACTIVATE_GREEN_BACKLIGHT = 0x0080,
            ACTIVATE_RED_BACKLIGHT = 0x0100,
            ACTIVATE_TRIGGER_RELAY = 0x0200,
            ACTIVATE_AUXILIAR_RELAY = 0x0400,
        }

        public enum DisplayOperations
        {
            NO_ACTION = 0x0000,
            ACTIVATE_IMPAIR_SEGMENTS_IMPAIR_COMMONS = 0x0001,
            ACTIVATE_PAIR_SEGMENTS_PAIR_COMMONS = 0x0002,
            ACTIVATE_IMPAIR_SEGMENTS_PAIR_COMMONS = 0x0003,
            ACTIVATE_PAIR_SEGMENTS_IMPAIR_COMMONS = 0x0004,
            DEACTIVATE_ALL_SEGMENTS = 0x0005,
            ACTIVATE_ALL_SEGMENTS = 0x0006,
        }

        [Flags]
        public enum InputReadings
        {
            NO_BUTTON_OR_INPUT = 0x0000,
            [Description("entrada_externa")]
            EXTERNAL_INPUT = 0x0001,
            [Description("tecla_de_reset")]
            RESET_BUTTON = 0x0002,
            [Description("tecla_de_test")]
            TEST_BUTTON = 0x0004,
            [Description("tecla_de_prog")]
            PROG_BUTTON = 0x0008,
        }

        public enum AdjustOperation
        {
            REFERENCE_VOLTAGE = 0x0003,
            SCALE_1 = 0x0103,
            SCALE_2 = 0x0203,
            SCALE_3 = 0x0303,
            SCALE_4 = 0x0403,
        }

        public enum TriggerCurrents
        {
            CURRENT_A03 = 0,
            CURRENT_A1 = 1,
            CURRENT_A3 = 2,
            CURRENT_A5 = 3,
            CURRENT_1A = 4,
            CURRENT_2A = 5,
            CURRENT_3A = 6,
            CURRENT_5A = 7
        };

        public enum TriggerTimes
        {
            TIME_INS = 0,
            TIME_SEL = 1,
            TIME_S1 = 2,
            TIME_S2 = 3,
            TIME_S3 = 4,
            TIME_S5 = 5,
            TIME_S8 = 6,
            TIME_1S = 7,
            TIME_2S = 8,
            TIME_3S = 9,
            TIME_5S = 10
        };

        public enum OperatingFrequencies
        {
            FREQUENCY_50HZ = 0,
            FREQUENCY_60HZ = 1,
        };

        public enum RelayLogics
        {
            STANDARD = 0,
            POSITIVE = 1,
        };

        public enum AuxiliarRelayFunction
        {
            FAILURE_SINGALING = 0,
            PREALARM = 1,
        };

        #endregion

        #region  Enums Adress Registers Value

        public enum AdjustRegisters
        {
            REFERENCE_VOLTAGE = 0x0030,
            SCALE_1_H = 0x0028, 
            SCALE_1_L = 0x0029,
            SCALE_2_H = 0x002A,
            SCALE_2_L = 0x002B,
            SCALE_3_H = 0x002C,
            SCALE_3_L = 0x002D, 
            SCALE_4_H = 0x002E,
            SCALE_4_L = 0x002F,
        }

        public enum ProductionParametersRegisters
        {
            FIRMWARE_VERSION = 0x0000,
            SERIAL_NUMBER_H = 0x0023,
            SERIAL_NUMBER_L = 0x0024,
            DEVICE_MODEL = 0x0020,
            FRAME_NUMBER_H = 0x0025,
            FRAME_NUMBER_L = 0x0026,
            ERROR_CODE = 0x0027,
        }

        public enum ConfigurationParametersRegisters
        {
            TRIGGER_CURRENT_RANGE = 0x0001,
            TRIGGER_TIME_RANGE = 0x0002,
            TRIGGER_CURRENT = 0x0008,
            OPERATING_FREQUENCY = 0x0003,
            TRIGGER_RELE_LOGIC = 0x0004,
            AUXILIAR_RELAY_LOGIC = 0x0005,
            AUXILIAR_RELAY_FUNCTION = 0x0006,
        }

        public enum Registers
        {
            CONTROL = 0x0010,
            LECTURA_ESTADO_TOROIDAL = 0x0012,
            OUTPUTS = 0x0011,
            DISPLAY = 0X0014,
            NUMERO_DISPLAY = 0X0014,
            INPUTS = 0X0013,
            LEAKAGE_CURRENT = 0x0008,
            INITIAL_MESSAGE = 0x0021,
        }

        #endregion
    }
}
