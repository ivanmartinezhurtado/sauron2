﻿namespace Circutor.Tests.MyCircutor
{
    partial class ItemsInputView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLVCBItemCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRegulatorItemCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nudStages = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudStages)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLVCBItemCode
            // 
            this.txtLVCBItemCode.Location = new System.Drawing.Point(229, 61);
            this.txtLVCBItemCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLVCBItemCode.Name = "txtLVCBItemCode";
            this.txtLVCBItemCode.Size = new System.Drawing.Size(213, 26);
            this.txtLVCBItemCode.TabIndex = 1;
            this.txtLVCBItemCode.Text = "R3L310";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 67);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Código Batería:";
            // 
            // txtRegulatorItemCode
            // 
            this.txtRegulatorItemCode.Location = new System.Drawing.Point(229, 183);
            this.txtRegulatorItemCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRegulatorItemCode.Name = "txtRegulatorItemCode";
            this.txtRegulatorItemCode.Size = new System.Drawing.Size(213, 26);
            this.txtRegulatorItemCode.TabIndex = 5;
            this.txtRegulatorItemCode.Text = "R13862";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(65, 189);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 20);
            this.label9.TabIndex = 4;
            this.label9.Text = "Código Regulador:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(65, 126);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nº Pasos:";
            // 
            // nudStages
            // 
            this.nudStages.Location = new System.Drawing.Point(229, 120);
            this.nudStages.Name = "nudStages";
            this.nudStages.Size = new System.Drawing.Size(120, 26);
            this.nudStages.TabIndex = 3;
            this.nudStages.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // ItemsInputView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nudStages);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtRegulatorItemCode);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtLVCBItemCode);
            this.Controls.Add(this.label7);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ItemsInputView";
            this.Size = new System.Drawing.Size(551, 270);
            ((System.ComponentModel.ISupportInitialize)(this.nudStages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLVCBItemCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRegulatorItemCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudStages;
    }
}
