﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dezac.Tests.Utils
{
    public class SamplerList
    {
        public List<SamplerListItem> series;

        public SamplerList(int numSeries)
        {
            series = new List<SamplerListItem>(numSeries);

            for (int i = 0; i < numSeries; i++)
                series.Add(new SamplerListItem(i.ToString()));
        }

        public SamplerList(params string[] namedSeries)
        {
            series = new List<SamplerListItem>(namedSeries.Length);

            for (int i = 0; i < namedSeries.Length; i++)
                series[i] = new SamplerListItem(namedSeries[i]);
        }

        public void Clear()
        {
            series.ForEach(p => p.Clear());
        }

        public void Add(params AdjustValueDef[] values)
        {
            if (values.Length != series.Count)
                throw new Exception("Error");

            for (int i = 0; i < series.Count; i++)
                series[i].Data.Add(values[i]);
        }

        public double Average(int serie)
        {
            return series[serie].Data.Average(p => Convert.ToDouble(p));
        }

        public AdjustValueDef Max(int serie)
        {
            return series[serie].Data.Max();
        }

        public AdjustValueDef Min(int serie)
        {
            return series[serie].Data.Min();
        }

        public AdjustValueDef Last(int serie)
        {
            return series[serie].Data.Last();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            series.ForEach(p => sb.AppendLine(p.ToString()));

            return sb.ToString();
        }

        public class SamplerListItem
        {
            public string Name { get; set; }
            public List<AdjustValueDef> Data { get; set; }

            public SamplerListItem()
            {
                Data = new List<AdjustValueDef>();
            }

            public SamplerListItem(string name)
                : this()
            {
                Name = name;
            }

            public void Clear()
            {
                Data.Clear();
            }

            public override string ToString()
            {
                return string.Format("First: {0} - Last: {1} - Avg: {2} - Max: {3} - Min: {4} - Count: {5}",
                    Data.First(),
                    Data.Last(),
                    Data.Average(p => Convert.ToDouble(p.Value)),
                    Data.Max(),
                    Data.Min(),
                    Data.Count);
            }
        }
    }
}
