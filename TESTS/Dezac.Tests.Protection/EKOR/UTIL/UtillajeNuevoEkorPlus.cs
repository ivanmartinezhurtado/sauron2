﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Instruments.IO;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Protection
{
    public class UtillajeNuevoEkorPlus : IUtillajeEkor
    {
        private Tower3 tower { get; set; }
        private TestBase testBase { get; set; }
        private USB5856 usb { get; set; }

        public UtillajeNuevoEkorPlus(TestBase testBase, ITower tower)
        {
            this.tower = (Tower3)tower;
            this.testBase = testBase;
            usb = new USB5856();
        }

        private const int VALVULA_GENERAL_UTIL = 9;
        private const int PISTON_DOMO = 5;
        private const int OUT_DI_1 = 17;
        private const int OUT_DI_2 = 18;
        private const int OUT_DI_3 = 19;
        private const int OUT_DI_4 = 41;
        private const int OUT_DI_5 = 42;
        private const int OUT_DI_6 = 40;
        private const int OUT_DI_7 = 0;
        private const int OUT_DI_8 = 1;
        private const int OUT_DI_9 = 2;
        private const int OUT_DI_10 = 3;

        private const int DETECTION_DOMO_DOWN = 11;
        private const int DETECTION_DOMO_UP = 14;

        private const int OUT_ACTIVE_VL = 20;
        private const int OUT_ACTIVE_VF = 21;

        private const byte IN_OUT1 = 9;
        private const byte IN_OUT2 = 15;
        private const byte IN_OUT3 = 16;
        private const byte IN_OUT4 = 17;

        private const int OUT_KEY_SIDE = 54;
        private const int OUT_KEY_SET = 7;
        private const int OUT_KEY_ESC = 8;
        private const int OUT_KEY_PLUS = 9; //ADVANTECH
        private const int OUT_KEY_MIN = 12;
        private const int OUT_KEY_LEFT = 10;
        private const int OUT_KEY_RIGHT = 11;

        private const int OUT_SUPPLY_USB = 7;
        private int INPUT_DETECTION_TRIGGER = 10;

        private int OUTPUT_VL1_PASATAPAS = 4;
        private int OUTPUT_VL2_PASATAPAS = 3;
        private int OUTPUT_VL3_PASATAPAS = 2;

        private int OUT_CLK = 15;

        private enum Pistones
        {
            PISTON_PUNTAS = 6,
            PISTON_DB9 = 50,
            PISTON_USB_RPA = 51,
            PISTON_ETH1_RTU = 52,
            PISTON_ETH2_RTU = 53,
            PISTON_SP0_SP3_RS485 = 54,
            PISTON_TRIGGER_PASARELA = 55,
            PISTON_ETH_DTC2 = 56,
            PISTON_RS485_DTC2_EKORPLUS = 57,
            PISTON_PUNTAS_DISPARO_DTC2_EKORPLUS = 58,
            PISTON_FIJACION_EQUIPO = 59,
            PISTON_GENERAL_COM_FRONTAL = 48,
            PISTON_GENERAL_POSTERIOR = 49
        }



        List<KeyValuePair<byte, byte>> inputsValue = new List<KeyValuePair<byte, byte>>
        {
            new KeyValuePair<byte, byte>(OUT_DI_1, 0),
            new KeyValuePair<byte, byte>(OUT_DI_2, 1),
            new KeyValuePair<byte, byte>(OUT_DI_3, 2),
            new KeyValuePair<byte, byte>(OUT_DI_4, 3),
            new KeyValuePair<byte, byte>(OUT_DI_5, 4),
            new KeyValuePair<byte, byte>(OUT_DI_6, 5),
            new KeyValuePair<byte, byte>(OUT_DI_7, 6),
            new KeyValuePair<byte, byte>(OUT_DI_8, 7),
            new KeyValuePair<byte, byte>(OUT_DI_9, 8),
            new KeyValuePair<byte, byte>(OUT_DI_10, 9)
        };



        List<KeyValuePair<byte, byte>> outputsValue = new List<KeyValuePair<byte, byte>>
        {
            new KeyValuePair<byte, byte>(IN_OUT1, 0),
            new KeyValuePair<byte, byte>(IN_OUT2, 1),
            new KeyValuePair<byte, byte>(IN_OUT3, 2),
            new KeyValuePair<byte, byte>(IN_OUT4, 3)
        };

        List<KeyValuePair<byte, byte>> keyBoardValue = new List<KeyValuePair<byte, byte>>
        {    
                new KeyValuePair<byte, byte>(OUT_KEY_SET, 0),
                new KeyValuePair<byte, byte>(OUT_KEY_ESC, 2),
                new KeyValuePair<byte, byte>(OUT_KEY_PLUS, 1),
                new KeyValuePair<byte, byte>(OUT_KEY_MIN, 3),
                new KeyValuePair<byte, byte>(OUT_KEY_LEFT, 5),
                new KeyValuePair<byte, byte>(OUT_KEY_RIGHT, 4)
        };

        public void ActivatePistonRS232()
        {
            tower.IO.DO.OnWait(250, (byte)Pistones.PISTON_DB9);
            testBase.SamplerWithCancel((p) => { return tower.IO.DI[28]; }, "Error, no se ha detectado el piston DB9 entrar al equipo correctamente", 3, 500, 1000);
        }

        public void DesactivatePistonRS232()
        {
            if (testBase.Identificacion.MODELO.Contains("RPA100"))
            {
                tower.IO.DO.OffWait(250, (byte)Pistones.PISTON_USB_RPA);
                testBase.SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[27]; }, "Error, no se ha detectado el piston DB9 salir del equipo correctamente", 3, 500, 1000);
            }
            else
            {
                tower.IO.DO.OffWait(250, (byte)Pistones.PISTON_DB9);
                testBase.SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[28]; }, "Error, no se ha detectado el piston DB9 salir del equipo correctamente", 3, 500, 1000);
            }
        }

        public void TestDeviceConnection()
        {
            tower.Active24VDC();
            tower.ActiveElectroValvule();

            var timeWait = 1000;
            tower.IO.DO.OnWait(timeWait, VALVULA_GENERAL_UTIL);

            tower.IO.DO.Off(PISTON_DOMO);
            testBase.SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => TestException.Create().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            var inputsOuputsEkor = new Dictionary<Pistones, int[]>()
            {
                {Pistones.PISTON_FIJACION_EQUIPO, new int[]{21, 22}},    // deteccion piston carro
                {Pistones.PISTON_PUNTAS, new int[]{25,26}},              // deteccion pistones puntas test
            };

            string modelo = testBase.Identificacion.MODELO;
            bool hasPasarela = testBase.Identificacion.PASARELA == "NO" ? false : true;

            if (hasPasarela)
            {
                tower.IO.DO.OffWait(1000, (int)Pistones.PISTON_GENERAL_POSTERIOR);
                inputsOuputsEkor.Add(Pistones.PISTON_ETH1_RTU, new int[] { 24 });    // Detección pistones ETH1 RTU
                inputsOuputsEkor.Add(Pistones.PISTON_ETH2_RTU, new int[] { 29 });    // Detección pistones ETH2 RTU
                inputsOuputsEkor.Add(Pistones.PISTON_SP0_SP3_RS485, new int[] { 30, 31 });   // Detección pistones SP0/SP3/RS485 
                inputsOuputsEkor.Add(Pistones.PISTON_TRIGGER_PASARELA, new int[] { 32 });
            }
            else
            {
                tower.IO.DO.OnWait(2000, (int)Pistones.PISTON_GENERAL_POSTERIOR);
                inputsOuputsEkor.Add(Pistones.PISTON_RS485_DTC2_EKORPLUS, new int[] { 34 });
                inputsOuputsEkor.Add(Pistones.PISTON_PUNTAS_DISPARO_DTC2_EKORPLUS, new int[] { 35 });
            }

            if (modelo.Contains("RPA"))
            {
                tower.IO.DO.OnWait(2000, (int)Pistones.PISTON_GENERAL_COM_FRONTAL);
                inputsOuputsEkor.Add(Pistones.PISTON_USB_RPA, new int[] { 27 });
            }
            else if (modelo.Contains("PLUS"))
            {
                tower.IO.DO.OffWait(2000, (int)Pistones.PISTON_GENERAL_COM_FRONTAL);
                inputsOuputsEkor.Add(Pistones.PISTON_DB9, new int[] { 28 });
            }
            else if(!modelo.Contains("RPG_CI_RTU"))
                testBase.Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE().Throw();

            foreach (KeyValuePair<Pistones, int[]> inOut in inputsOuputsEkor)
                testBase.SamplerWithCancel((p) =>
                {
                    tower.IO.DO.OnWait(timeWait, (int)inOut.Key);

                    foreach (int input in inOut.Value)
                        if (!tower.IO.DI[input])
                            throw new Exception(string.Format("Error no se ha activado el piston {0} de la IN{1} ", inOut.Key.ToString(), input));

                    return true;
                }, "", cancelOnException: false, throwException: false);
        }

        public void TestDeviceDisconnection()
        {
            var timeWait = 800;
            var inputsOuputs = new Dictionary<Pistones, int[]>()
            {
                {Pistones.PISTON_PUNTAS, new int[]{25,26}},   // Detección pistones puntas
                {Pistones.PISTON_DB9, new int[]{28}},   // Detección pistones DB9
                {Pistones.PISTON_USB_RPA, new int[]{27}},    // Detección pistones USB RPA
                {Pistones.PISTON_ETH1_RTU, new int[]{24}},     // Detección pistones ETH1 RTU
                {Pistones.PISTON_ETH2_RTU, new int[]{29}},     // Detección pistones ETH2 RTU
                {Pistones.PISTON_SP0_SP3_RS485, new int[]{30,31}},     // Detección pistones SP0/SP3/RS485
                {Pistones.PISTON_TRIGGER_PASARELA, new int[]{32}},      // Detección pistones TRIGGER DTC"
                {Pistones.PISTON_ETH_DTC2, new int[]{33}},         // Detección pistones ethernet DTC2
                {Pistones.PISTON_RS485_DTC2_EKORPLUS, new int[]{34}},       // Detección pistones RS485 DTC2/EKORPLUS
                {Pistones.PISTON_PUNTAS_DISPARO_DTC2_EKORPLUS, new int[]{35}},     // Detección puntas de disparo DTC2 / EKORPLUS
                {Pistones.PISTON_FIJACION_EQUIPO, new int[]{21}},     // Detección puntas de disparo DTC2 / EKORPLUS
            };

            tower.IO.DO.Off(PISTON_DOMO);
            testBase.SamplerWithCancelWhitOutCancelToken(
            () =>
            {
                return tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => TestException.Create().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            foreach (KeyValuePair<Pistones, int[]> inOut in inputsOuputs)
                testBase.SamplerWithCancelWhitOutCancelToken(
                    () =>
                    {
                        tower.IO.DO.OffWait(timeWait, (int)inOut.Key);

                        foreach (int input in inOut.Value)
                            if (tower.IO.DI[input])
                                throw new Exception(string.Format("Error no se han desactivado el piston {0} de la IN{1} ", inOut.Key.ToString(), input));

                        return true;
                    }, "", initialDelay: 200, cancelOnException: false, throwException: false);

            tower.IO.DO.OffWait(timeWait, (int)Pistones.PISTON_GENERAL_COM_FRONTAL, (int)Pistones.PISTON_GENERAL_POSTERIOR);
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairInputs()
        {
            return inputsValue;
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairOutputs()
        {
            return outputsValue;
        }

        public byte GetOutActivateVL()
        {
            return OUT_ACTIVE_VL;
        }

        public byte GetOutActivateVF()
        {
            return OUT_ACTIVE_VF;
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairKeyBoard()
        {
            return keyBoardValue;
        }

        public void SupplyUSB(bool state = true)
        {          
            if (state)
                usb.DO.On(OUT_SUPPLY_USB);
            else
                usb.DO.Off(OUT_SUPPLY_USB);
        }

        public int GetInputDetectionTrigger()
        {
            return INPUT_DETECTION_TRIGGER;
        }

        public int GetOutputMeasurePasatapasL1()
        {
            return OUTPUT_VL1_PASATAPAS;
        }

        public int GetOutputMeasurePasatapasL2()
        {
            return OUTPUT_VL2_PASATAPAS;
        }

        public int GetOutputMeasurePasatapasL3()
        {
            return OUTPUT_VL3_PASATAPAS;
        }

        public int GetOutCLK()
        {
            return OUT_CLK;
        }

        public void ResetUSB()
        {
            SupplyUSB(false);
            testBase.Delay(2500, "Reseteando..");
            SupplyUSB(true);
            testBase.Delay(4000, "Esperando COM virtual");
        }

        public void ActivateMeasurePasatapasL1()
        {
            usb.DO.OnWait(1000, OUTPUT_VL1_PASATAPAS);
        }

        public void ActivateMeasurePasatapasL2()
        {         
            usb.DO.OnWait(1000, OUTPUT_VL2_PASATAPAS);
        }

        public void ActivateMeasurePasatapasL3()
        {
            usb.DO.OnWait(1000, OUTPUT_VL3_PASATAPAS);
        }

        public void DesactivateMeasurePasatapasL1()
        {
            usb.DO.Off(OUTPUT_VL1_PASATAPAS);
        }

        public void DesactivateMeasurePasatapasL2()
        {
            usb.DO.Off(OUTPUT_VL2_PASATAPAS);
        }

        public void DesactivateMeasurePasatapasL3()
        {
            usb.DO.Off(OUTPUT_VL3_PASATAPAS);
        }

        public void ActivatePistonTestKeyboard()
        {
            tower.IO.DO.On(PISTON_DOMO);
            testBase.SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_DOMO_UP];
            }, string.Empty, 10, 500, 1000, true, true, (e) => TestException.Create().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

        }

        public void DesactivatePistonTestKeyboard()
        {
            tower.IO.DO.Off(PISTON_DOMO);
            testBase.SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => TestException.Create().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());
        }
    }
}
