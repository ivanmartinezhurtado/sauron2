﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.02)]
    public class NRG96 : DeviceBase
    {
        public NRG96()
        {
        }

        public NRG96(int port)
        {
            SetPort(port, 1);
        }

        public void SetPort(int port, int periferico, int baudRate = 9600)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)periferico;
        }
       

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Modbus read operations

        public byte[] ReadOutputState()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Registers.OUTPUT);
        }

        public KeyboardKeysRegisters ReadKey()
        {
            LogMethod();
            var keyboardReading = (byte)(Modbus.ReadMultipleCoil((ushort)Registers.OUTPUT, 5)[0] & 0x1E);
            return (KeyboardKeysRegisters)keyboardReading;
        }

        public byte[] ReadBacklightState()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Registers.BACKLIGHT, 2);
        }

        public ScreenConfiguration ReadScreenSelection()
        {
            LogMethod();
            var screenConfiguration = new ScreenConfiguration(Modbus.ReadMultipleCoil((ushort)Registers.SCREEN_CONFIGURATION, 32));
            return screenConfiguration;
        }

        public byte[] ReadBacklightShutdownTime()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Registers.DISPLAY_SHUTDOWN_TIME, 16);
        }

        public HardwareVector ReadHardwareConfiguration()
        {
            LogMethod();
            var hardwareConfig = new HardwareVector(Modbus.ReadMultipleCoil((ushort)Registers.HARDWARE_CONFIG, 96));
            return hardwareConfig;
        }

        public AllVariables ReadAllElectricalVariables()
        {
            LogMethod();

            var Fases = Modbus.Read<VariablesInstantaneas>();
            Fases.L1.Tension /= 10;
            Fases.L1.FactorPotencia /= 100;
            Fases.L2.Tension /= 10;
            Fases.L2.FactorPotencia /= 100;
            Fases.L3.Tension /= 10;
            Fases.L3.FactorPotencia /= 100;

            // var All = Read<AllVariables>();
            var Trifasicas = Modbus.Read<VariablesTrifasicas>();
            Trifasicas.CosenoPhiIII /= 100;
            Trifasicas.FactorPotenciaIII /= 100;
            Trifasicas.Frecuencia /= 10;
            Trifasicas.TensionLineaL1L2 /= 10;
            Trifasicas.TensionLineaL2L3 /= 10;
            Trifasicas.TensionLineaL3L1 /= 10;

            AllVariables All = new AllVariables();
            All.Phases = Fases;
            All.Trifasicas = Trifasicas;

            return All;
        }

        public VariablesInstantaneas ReadPhaseVariables()
        {
            LogMethod();
            return Modbus.Read<VariablesInstantaneas>();
        }

        public VariablesTrifasicas ReadThreePhaseVariables()
        {
            LogMethod();
            return Modbus.Read<VariablesTrifasicas>();
        }

        public THDReadings ReadTHD()
        {
            LogMethod();
            return Modbus.Read<THDReadings>();
        }

        public PasswordConfiguration ReadPasswordConfiguration()
        {
            LogMethod();
            return Modbus.Read<PasswordConfiguration>();
        }

        public SetupConfiguration ReadSetupConfiguration()
        {
            LogMethod();
            return Modbus.Read<SetupConfiguration>();
        }

        public AlarmConfiguration ReadAlarmConfiguration()
        {
            LogMethod();
            return Modbus.Read<AlarmConfiguration>();
        }

        public MaximumDemandConfiguration ReadMaximumDemandConfiguration()
        {
            LogMethod();
            return Modbus.Read<MaximumDemandConfiguration>();
        }

        public string ReadInitialScreenMessage()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.START_SCREEN_MESSAGE, 4);
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.FIRMWARE_VERSION, 3).Replace("\0", string.Empty).Trim();
        }

        public EEPROMErrorCodes ReadEEPROMErrorCodes()
        {
            LogMethod();
            return (EEPROMErrorCodes)Modbus.ReadRegister((ushort)Registers.EEPROM_ERROR_CODE);
        }

        public ushort ReadROMCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ROM_CODE);
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public CalibrationFactors ReadCalibrationFactors()
        {
            LogMethod();
            return Modbus.Read<CalibrationFactors>();
        }

        public TriCalibrationFactors ReadITFCalibrationFactors()
        {
            LogMethod();
            return Modbus.Read<TriCalibrationFactors>((ushort)Registers.ITF_CALIBRATION_FACTORS);
        }

        public TriCalibrationFactors ReadCompoundVoltageCalibrationFactors()
        {
            LogMethod();
            return Modbus.Read<TriCalibrationFactors>((ushort)Registers.COMPOUND_VOLTAGE_CALIBRATION_FACTORS);
        }

        public ConverterPoints ReadConverterPoints()
        {
            LogMethod();
            return Modbus.Read<ConverterPoints>();
        }

        public ChannelOffsets ReadChannelOffsets()
        {
            LogMethod();
            return Modbus.Read<ChannelOffsets>();
        }
        
        public EnergyConsumptionInformation ReadEnergyInformation()
        {
            LogMethod();
            return Modbus.Read<EnergyConsumptionInformation>();
        }

        #endregion

        #region Modbus write operations

        public void WriteDigitalOutput(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.OUTPUT, state);
        }

        public void WriteBacklightsState(bool backLight1State, bool backLight2State)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)0x0008, new bool[2] { !backLight1State, !backLight2State });
        }

        public void WriteLedState(bool ledState)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.LEDS, ledState);
        }

        public void WriteReset()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public void WriteEnergyClear()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_ENERGIES, true);
        }

        public void WriteMaximumDemandInitialziation()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.MAXIMUM_DEMAND_INITIALIZATION, true);
        }

        public void WriteMaximumMinimumClear()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_MAXIMUM_MINIMUM, true);
        }

        public void WriteAllInformationClear()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_ALL, true);
        }

        public void WriteMaximumDemandClear()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_MAXIMUM_DEMAND, true);
        }

        public void WriteDisplaySegments(params bool[] segmentsToTurnOn)
        {
            LogMethod();
            if (segmentsToTurnOn.Count() > 160)
                throw new Exception(string.Format("Error al escribir en el display, el maximo numero de segmentos es 128 mientras que se introduce información para {0}", segmentsToTurnOn.Count()));
            Modbus.WriteMultipleCoil((ushort)Registers.DISPLAY_SEGMENTS, segmentsToTurnOn);
        }

        public void WriteScreenConfiguration(ScreenConfiguration screenConfigurationInfo)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.SCREEN_CONFIGURATION, screenConfigurationInfo.ToBoolArray());
        }

        public void WriteHardwareVector(HardwareVector hardwareVector)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_CONFIG, hardwareVector.ToSingleCoil());
        }

        public void WriteFlagTest(bool activate = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, activate);
        }

        public void WriteRecoverCalibrationValuesFromEEPROM()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.APPLY_CALIBRATION, true);
        }

        public void WritePasswordInformation(PasswordConfiguration passwordConfiguration)
        {
            LogMethod();
            Modbus.Write<PasswordConfiguration>(passwordConfiguration);
        }

        public void WriteSetupConfiguration(SetupConfiguration setupConfiguration)
        {
            LogMethod();
            Modbus.Write<SetupConfiguration>(setupConfiguration);
        }

        public void WriteAlarmConfiguration(AlarmConfiguration alarmConfiguration)
        {
            LogMethod();
            Modbus.Write<AlarmConfiguration>(alarmConfiguration);
        }

        public void WriteMaximumDemandConfiguration(MaximumDemandConfiguration maximumDemandConfiguration)
        {
            LogMethod();
            Modbus.Write<MaximumDemandConfiguration>(maximumDemandConfiguration);
        }

        public void WriteInitialScreenMessage(string message)
        {
            LogMethod();
            if (message.Length > 8)
                throw new Exception("Error al grabar el mensaje inicial, se ha excedido el limite de 8 caracteres para el mensaje inicial");
            message.PadRight(8, ' ');
            Modbus.WriteString((ushort)Registers.START_SCREEN_MESSAGE, message);
        }

        public void WriteCalibrationFactors(CalibrationFactors calibrationFactors)
        {
            Modbus.Write<CalibrationFactors>(calibrationFactors);
        }

        public void WriteITFCalibrationFactors(TriCalibrationFactors itfCalibrationFactors)
        {
            LogMethod();
            Modbus.Write<TriCalibrationFactors>((ushort)Registers.ITF_CALIBRATION_FACTORS, itfCalibrationFactors);
        }

        public void WriteCompoundvoltageFactors(TriCalibrationFactors compoundCalibrationFactors)
        {
            Modbus.Write<TriCalibrationFactors>((ushort)Registers.COMPOUND_VOLTAGE_CALIBRATION_FACTORS, compoundCalibrationFactors);
        }

        public void WriteEnergyInformation(EnergyConsumptionInformation energyInformation)
        {
            LogMethod();
            Modbus.Write<EnergyConsumptionInformation>(energyInformation);
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            LogMethod();
            Modbus.Write<uint>((ushort)Registers.SERIAL_NUMBER, serialNumber);
        }

        public void WriteDefaultCommunicationConfiguration()
        {
            var commConfig = new CommunicationsConfiguration(1);
            Modbus.Write<CommunicationsConfiguration>(commConfig);
        }

        #endregion

        #region Methods

        public void SendDisplayInfo(DisplayState state)
        {
            WriteDisplaySegments(displayDictionary[state]);
        }

        #endregion

        #region Adjust methods

        public Tuple<bool, CalibrationFactors> CalculateVoltageCurrentCalibrationFactors(int delFirst, int initCount, int samples, int interval, TriLineValue voltage, TriLineValue current, double transformationRelatio, Func<CalibrationFactors, bool> adjustValidation)
        {
            WriteFlagTest(); // se escribe el flag de test

            var gains = ReadCalibrationFactors();

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, ushort.MaxValue,
                () =>
                {
                    var allVariables = ReadConverterPoints();

                    var varlist = new List<double>(){ 
                       allVariables.VoltageL1 / allVariables.AmountOfSamples, 
                       allVariables.VoltageL2 / allVariables.AmountOfSamples, 
                       allVariables.VoltageL3 / allVariables.AmountOfSamples,
                       allVariables.CurrentL1 / allVariables.AmountOfSamples, 
                       allVariables.CurrentL2 / allVariables.AmountOfSamples, 
                       allVariables.CurrentL3 / allVariables.AmountOfSamples};

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.VoltageL1 = Convert.ToUInt16(10000 * Math.Pow(voltage.L1, 2) / listValues.Average(0));
                    gains.VoltageL2 = Convert.ToUInt16(10000 * Math.Pow(voltage.L2, 2) / listValues.Average(1));
                    gains.VoltageL3 = Convert.ToUInt16(10000 * Math.Pow(voltage.L3, 2) / listValues.Average(2));
                    gains.CurrentL1 = Convert.ToUInt16(100 * Math.Pow(current.L1 * 1000, 2) / listValues.Average(3));
                    gains.CurrentL2 = Convert.ToUInt16(100 * Math.Pow(current.L2 * 1000, 2) / listValues.Average(4));
                    gains.CurrentL3 = Convert.ToUInt16(100 * Math.Pow(current.L3 * 1000, 2) / listValues.Average(5));

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, TriCalibrationFactors> CalculateCompoundMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue voltage, double transformationRelatio, Func<TriCalibrationFactors, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            WriteFlagTest();

            var gains = ReadCompoundVoltageCalibrationFactors();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, ushort.MaxValue,
                () =>
                {
                    var allVariables = ReadConverterPoints();

                    var varlist = new List<double>(){ 
                       allVariables.VoltageL12 / allVariables.AmountOfSamples, 
                       allVariables.VoltageL23 / allVariables.AmountOfSamples, 
                       allVariables.VoltageL31 / allVariables.AmountOfSamples};

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.L1 = Convert.ToUInt16(10000 * Math.Pow(voltage.L1 * Math.Sqrt(3), 2) / (listValues.Average(0)));
                    gains.L2 = Convert.ToUInt16(10000 * Math.Pow(voltage.L2 * Math.Sqrt(3), 2) / (listValues.Average(1)));
                    gains.L3 = Convert.ToUInt16(10000 * Math.Pow(voltage.L3 * Math.Sqrt(3), 2) / (listValues.Average(2)));

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, TriCalibrationFactors> CalculateGapAdjustFactors(int delFirst, int initCount, int samples, int interval, TriLineValue powerReference, double offsetReference, double transformationRelation, Func<TriCalibrationFactors, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            double angle = 0.0D;
            double angleError = 0.0D;
            double angleErrorCorrected = 0.0D;
            var result = new TriCalibrationFactors();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, ushort.MaxValue,
                () =>
                {
                    var electricalVariables = ReadPhaseVariables();

                    return new double[] { (electricalVariables.L1.PotenciaActiva - powerReference.L1) / powerReference.L1, 
                    (electricalVariables.L2.PotenciaActiva- powerReference.L2) / powerReference.L2, 
                    (electricalVariables.L3.PotenciaActiva- powerReference.L3) / powerReference.L3};
                },
                (listValues) =>
                {
                    angle = (1 - listValues.Average(0)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (760E-9);
                    result.L1 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    angle = (1 - listValues.Average(1)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (760E-9);
                    result.L2 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    angle = (1 - listValues.Average(2)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (760E-9);
                    result.L3 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }


        #endregion

        #region Structures

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Phases;
            public VariablesTrifasicas Trifasicas;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            public Int32 Tension;
            public Int32 Corriente;
            public Int32 PotenciaActiva;
            public Int32 PotenciaReactiva;
            public Int32 FactorPotencia;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x001E)]
        public struct VariablesTrifasicas
        {
            public Int32 PotenciaActivaIII;
            public Int32 PotenciaInductivaIII;
            public Int32 PotenciaCapacitivaIII;
            public Int32 CosenoPhiIII;
            public Int32 FactorPotenciaIII;
            public Int32 Frecuencia;
            public Int32 TensionLineaL1L2;
            public Int32 TensionLineaL2L3;
            public Int32 TensionLineaL3L1;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0030)]
        public struct THDReadings
        {
            public int VoltageL1;
            public int VoltageL2;
            public int VoltageL3;
            public int CurrentL1;
            public int CurrentL2;
            public int CurrentL3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.PASSWORD_CONFIGURATION)]
        public struct PasswordConfiguration
        {
            public ushort Password;
            public byte DUMMY_BYTE;
            public byte SetupLocking;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COMMUNICATIONS_CONFIGURATION)]
        public struct CommunicationsConfiguration
        {
            public CommunicationsConfiguration(byte periferic = 1)
            {
                PerifericNumberAndProtocol = (ushort)(0x0000 + periferic);
                BaudsAndParity = 0x0300;
                StopBytesAndDataLength = 0x0800;
            }

            public ushort PerifericNumberAndProtocol;
            public ushort BaudsAndParity;
            public ushort StopBytesAndDataLength;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP_CONFIGURATION)]
        public struct SetupConfiguration
        {
            public SetupConfiguration(int voltagePrimary, ushort voltageSecondary, ushort currentPrimary, string voltageType, MainDefaultScreens mainDefaultPage, EnergyDefaultScreens energyDefaultScreen, bool calculateHarmonics, byte backlightShutdownTime)
            {
                if (voltagePrimary > 100000)
                    throw new ArgumentOutOfRangeException("La tensión de primario introducida esta fuera de márgenes");
                if (voltageSecondary > 999)
                    throw new ArgumentOutOfRangeException("La tensión de secundario introducida esta fuera de márgenes");
                if (currentPrimary > 10000)
                    throw new ArgumentOutOfRangeException("La corriente de primario introducida esta fuera de márgenes");
                if (backlightShutdownTime > 99)
                    throw new ArgumentOutOfRangeException("El tiempo de apagado del backlight introducido esta fuera de márgenes");
                
                VoltagePrimary = voltagePrimary;
                VoltageSecondary = voltageSecondary;
                CurrentPrimary = currentPrimary;
                if (Enum.IsDefined(typeof(VoltageTypes), voltageType))
                {
                    VoltageTypes voltageTypeConversion = VoltageTypes.SIMPLE;

                    var conversionSuccesful = Enum.TryParse<VoltageTypes>(voltageType, true, out voltageTypeConversion);
                    VoltageType = voltageTypeConversion == VoltageTypes.SIMPLE ? (byte)0 : (byte)1;
                }
                else
                    throw new ArgumentException("El tipo de tensión que se intenta introducir no es válido");

                DefaultPage = (byte)((int)energyDefaultScreen * 16 + mainDefaultPage);
                CalculateHarmonics = calculateHarmonics ? (byte)1 : (byte)0;
                BacklightShutdownTime = backlightShutdownTime;
            }

            public int VoltagePrimary;
            public ushort VoltageSecondary;
            public ushort CurrentPrimary;
            public byte VoltageType;
            public byte DefaultPage;
            public byte CalculateHarmonics;
            public byte BacklightShutdownTime;

            public enum VoltageTypes
            {
                SIMPLE,
                COMPOUND
            }

            public enum MainDefaultScreens
            {
                VOLTAGE,
                CURRENT,
                ACTIVE_POWER,
                REACTIVE_POWER,
                POWER_FACTOR,
                VOLTAGE_THD,
                CURRENT_THD,
                ACTIVE_INDUCTIVE_CAPACITIVE_POWER_THREE_PHASE,
                ACTIVE_INDUCTIVE_APPARENT_POWER_THREE_PHASE,
                MAXIMUM_DEMAND_FRECUENCY_POWER_FACTOR_THREE_PHASE,
                MAXIMUM_DEMAND_FRECUENCY_PHI_COSINE_THREE_PHASE,
            }

            public enum EnergyDefaultScreens
            {
                ACTIVE_CONSUMED,
                INDUCTIVE_CONSUMED,
                CAPACITIVE_CONSUMED,
                APPARENT_CONSUMED,
                ACTIVE_GENERATED,
                INDUCTIVE_GENERATED,
                CAPACITIVE_GENERATED,
                APPARENT_GENERATED
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ALARM_CONFIGURATION)]
        public struct AlarmConfiguration
        {
            public int MaximumValue;
            public int MinimumValue;
            public ushort Delay;
            private byte dummyByte;
            public byte VariableNumber;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAXIMUM_DEMAND_CONFIGURATION)]
        public struct MaximumDemandConfiguration
        {
            public ushort MaximumDemandVariable;
            public ushort RegisterTime;

            public string VariabletoCalculate
            {
                get
                {
                    return ((MaximumDemandVariables)MaximumDemandVariable).ToString();
                }
                set
                {
                    switch (value)
                    {
                        case "NO_PD":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.NO_PD;
                            break;
                        case "ACTIVE":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.ÀCTIVE;
                            break;
                        case "REACTIVE":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.REACTIVE;
                            break;
                        case "CURRENT_AVERAGE":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.CURRENT_AVERAGE;
                            break;
                        default:
                            throw new NotImplementedException("Error de parametrización de máxima demanda, la variable a monitorizar selccionada no existe");
                    }
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CALIBRATION_FACTORS)]
        public struct CalibrationFactors
        {
            public CalibrationFactors(ushort currentFactor, ushort voltageFactor)
            {
                CurrentL1 = CurrentL2 = CurrentL3 = currentFactor;
                VoltageL1 = VoltageL2 = VoltageL3 = voltageFactor;
            }

            public ushort CurrentL1;
            public ushort VoltageL1;
            public ushort CurrentL2;
            public ushort VoltageL2;
            public ushort CurrentL3;
            public ushort VoltageL3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriCalibrationFactors
        {
            public TriCalibrationFactors(ushort factor)
            {
                L1 = L2 = L3 = factor;
            }

            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ENERGY_INFORMATION)]
        public struct EnergyConsumptionInformation
        {
            public uint Active;
            public uint Inductive;
            public uint Capacitive;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONVERTER_POINTS)]
        public struct ConverterPoints
        {
            public int CurrentL1;
            public int VoltageL1;
            public int CurrentL2;
            public int VoltageL2;
            public int CurrentL3;
            public int VoltageL3;
            public int VoltageL12;
            public int VoltageL23;
            public int VoltageL31;
            public int ActivePowerL1;
            public int ActivePowerL2;
            public int ActivePowerL3;
            public ushort AmountOfSamples;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CHANNEL_OFFSETS)]
        public struct ChannelOffsets
        {
            public ushort CurrentL1;
            public ushort VoltageL1;
            public ushort CurrentL2;
            public ushort VoltageL2;
            public ushort CurrentL3;
            public ushort VoltageL3;
            public ushort VoltageL2Dummy;
            public ushort VoltageL1Dummy;
        }

        public struct ScreenConfiguration
        {
            public bool VoltagePhaseNeuter;
            public bool VoltagePhasePhase;
            public bool Current;
            public bool ActivePower;
            public bool ReactivePower;
            public bool PowerFactor;
            public bool VoltageTHD;
            public bool CurrentTHD;
            public bool ThreePhaseActiveInductiveCapacitivePower;
            public bool ThreePhaseActiveReactiveApparentPower;
            public bool PdHzPF;
            public bool PdHzCos;
            public bool PhasesPd;

            public ScreenConfiguration(byte[] screenConfiguration)
            {
                VoltagePhaseNeuter = (screenConfiguration[3] & 0x01) == 0x01;
                VoltagePhasePhase = (screenConfiguration[3] & 0x02) == 0x02;
                Current = (screenConfiguration[3] & 0x04) == 0x04;
                ActivePower = (screenConfiguration[3] & 0x08) == 0x08;
                ReactivePower = (screenConfiguration[3] & 0x10) == 0x10;
                PowerFactor = (screenConfiguration[3] & 0x20) == 0x20;
                VoltageTHD = (screenConfiguration[3] & 0x40) == 0x40;
                CurrentTHD = (screenConfiguration[3] & 0x80) == 0x80;
                ThreePhaseActiveInductiveCapacitivePower = (screenConfiguration[2] & 0x01) == 0x01;
                ThreePhaseActiveReactiveApparentPower = (screenConfiguration[2] & 0x02) == 0x02;
                PdHzPF = (screenConfiguration[2] & 0x04) == 0x04;
                PdHzCos = (screenConfiguration[2] & 0x08) == 0x08;
                PhasesPd = (screenConfiguration[2] & 0x10) == 0x10;
            }

            public bool[] ToBoolArray()
            {
                return new bool[32] { VoltagePhaseNeuter, VoltagePhasePhase, Current, ActivePower, ReactivePower, PowerFactor, VoltageTHD, CurrentTHD, 
                    ThreePhaseActiveInductiveCapacitivePower, ThreePhaseActiveReactiveApparentPower, PdHzPF, PdHzCos, PhasesPd, false, false, false, 
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false
                };
            }
        }

        #region Hardware vector

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Shunt { get; set; }
            public bool CalculoHarmonicos { get; set; }
            public bool PaginasDePotencia { get; set; }

            public VectorHardwareCurrentInputs InputCurrent { get; private set; }
            public VectorHardwareVoltageInputs InputVoltage { get; private set; }
            public VectorHardwareSupply PowerSupply { get; private set; }
            public VectorHardwareModelo Modelo { get; private set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value;
                    if (powerSupplyString.ToUpper().Contains("AC"))
                    {
                        if (powerSupplyString.Contains("230"))
                        {
                            PowerSupply = VectorHardwareSupply._230Vac;
                            return;
                        }
                        else
                        {
                            if (powerSupplyString.Contains("400"))
                            {
                                PowerSupply = VectorHardwareSupply._400Vac;
                                return;
                            }
                            else
                            {
                                if (powerSupplyString.Contains("480"))
                                {
                                    PowerSupply = VectorHardwareSupply._480Vac;
                                    return;
                                }
                                else
                                {
                                    if (powerSupplyString.Contains("85") || powerSupplyString.Contains("265"))
                                    {
                                        PowerSupply = VectorHardwareSupply._85_265Vac;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (powerSupplyString.Contains("24") || powerSupplyString.Contains("120"))
                        {
                            PowerSupply = VectorHardwareSupply._24_120Vdc;
                            return;
                        }
                    }
                    throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else
                    {
                        if (inputVoltageString.Contains("110"))
                        {
                            InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                            return;
                        }
                        else
                        {
                            if (inputVoltageString.Contains("500"))
                            {
                                InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                                return;
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("5"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                        return;
                    }
                    else if (inputCurrentString.Contains("2"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                        return;
                    }
                    else if (inputCurrentString.Contains("1"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                        return;
                    }
                    throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString.Contains("POWER_NET"))
                    {
                        Modelo = VectorHardwareModelo.POWER_NET;
                        return;
                    }
                    else if (modeloString.Contains("CVM_NRG_96"))
                    {
                        Modelo = VectorHardwareModelo.CVM_NRG_96;
                        return;
                    }
                    throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele = (vectorHardware[0] & 0x10) == 0x10;
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                CalculoHarmonicos = (vectorHardware[10] & 0x01) == 0x01;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[81] = PaginasDePotencia;
                vector[80] = CalculoHarmonicos;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
            Primary275V = 3,
            Primary250V = 4,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _24_120Vdc = 3,
            _85_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            POWER_NET = 1,
            CVM_NRG_96 = 2

        }

    	#endregion

        #endregion

        #region Dictionarys

        public Dictionary<DisplayState, bool[]> displayDictionary = new Dictionary<DisplayState, bool[]>
        {
            {DisplayState.NO_SEGMENTS, noSegments},
            {DisplayState.HORIZONTAL_SEGMENTS, horizontalDisplaySegments},
            {DisplayState.VERTICAL_SEGMENTS, verticalDisplaySegments},
            {DisplayState.SYMBOL_SEGMENTS, symbolDisplaySegments},
            {DisplayState.ALL_SEGMENTS, allSegments},
        };
        
        #endregion

        #region Information Arrays

        public static readonly bool[] noSegments = new bool[160] 
        {
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] horizontalDisplaySegments = new bool[160] 
        {
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,  
            true,   false,  false,  true,   false,  false,  true,   false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] verticalDisplaySegments = new bool[160] 
        {
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  true,   true,   false,  false,  true,   false,  true, 
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] symbolDisplaySegments = new bool[160] 
        {
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            false,  false,  false,  false,  true,   false,  false,  false, 
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
        };

        public static readonly bool[] allSegments = new bool[160] 
        {
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
        };
        
        #endregion

        public enum DisplayState
        {
            NO_SEGMENTS,
            HORIZONTAL_SEGMENTS,
            VERTICAL_SEGMENTS,
            SYMBOL_SEGMENTS,
            ALL_SEGMENTS
        }
        public enum MaximumDemandVariables
        {
            NO_PD = 0,
            ÀCTIVE = 16,
            REACTIVE = 34,
            CURRENT_AVERAGE = 36
        }

        [Flags]
        public enum EEPROMErrorCodes
        {
            NO_ERROR = 0x0000,
            HARDWARE_VECTOR_NOT_WRITTEN = 0x0001,
            COMMUNICATIONS_INCORRECT_OR_NOT_WRITTEN = 0x0002,
            PASSWORD_NOT_WRITTEN = 0x0004,
            MAIN_SETUP_PAGE_SELECTION = 0x0008,
            INITIAL_SCREEN_MESSAGE_NOT_WRITTEN = 0x0010,
            ALARM_NOT_INITIALIZED = 0x0020,
            ENERGIES_NOT_WRITTEN_OR_NEGATIVE_VALUES = 0x0080,
            CALIBRATION_FACTORS_NOT_WRITTEN =0x0100,
            SERIAL_NUMBER_NOT_WRITTEN = 0x0200,
            MAXIMUM_DEMAND_CONFIGURATION_ERROR = 0x0400
        }

        [Flags]
        public enum KeyboardKeysRegisters
        {
            RESET_KEY = 0x0002,
            DISPLAY_KEY = 0x0004,
            MAX_KEY = 0x0008,
            MIN_KEY = 0x0010,
            RESET_KEY_MAX_KEY = RESET_KEY | MAX_KEY,
            DISPLAY_KEY_MIN_KEY = DISPLAY_KEY | MIN_KEY
        }

        public enum Registers
        {
            OUTPUT = 0x0000,
            BACKLIGHT = 0x0000,
            LEDS = 0x0010,
            DISPLAY_SEGMENTS = 0x03E8,
            RESET = 0x07D0,
            COMMUNICATIONS_CONFIGURATION = 0x03E8,
            CLEAR_ENERGIES = 0x0834,
            MAXIMUM_DEMAND_INITIALIZATION = 0x0835,
            CLEAR_MAXIMUM_MINIMUM = 0x0836,
            CLEAR_ALL = 0x0837,
            CLEAR_MAXIMUM_DEMAND = 0x0838,
            SCREEN_CONFIGURATION = 0x0898,
            DISPLAY_SHUTDOWN_TIME = 0x08FC,
            HARDWARE_CONFIG = 0x2710,
            FLAG_TEST = 0x2AF8,
            APPLY_CALIBRATION = 0x2B5C,
            ELECTRICAL_VARIABLES = 0x0000,
            PASSWORD_CONFIGURATION = 0x041A,
            SETUP_CONFIGURATION = 0x044C,
            ALARM_CONFIGURATION = 0x047E,
            MAXIMUM_DEMAND_CONFIGURATION = 0x04E2,
            START_SCREEN_MESSAGE = 0x0514,
            FIRMWARE_VERSION = 0x0578,
            EEPROM_ERROR_CODE = 0x05DC,
            SERIAL_NUMBER = 0x2710,
            CALIBRATION_FACTORS = 0x2AF8,
            ITF_CALIBRATION_FACTORS = 0x2B2A,
            COMPOUND_VOLTAGE_CALIBRATION_FACTORS = 0x2B34,
            CONVERTER_POINTS = 0x2B5C,
            CHANNEL_OFFSETS = 0x2BC0,
            ENERGY_INFORMATION = 0x4E20,
            ROM_CODE = 0x526C,
        }
    }
}
