﻿using Dezac.Core.Enumerate;
using Dezac.Data;
using Dezac.Data.ViewModels;
using Dezac.Labels;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace WinTestPlayer.Services
{
    public class ReportsService
    {
        private const int NUMPARAM_MAC = 14205;
        private const int NUMPARAM_MAC_PLC = 15411;
        private const int NUMPARAM_IMEI = 15677;
        private const int NUMPARAM_NUMSERIESBT = 15767;
        private const int NUMPARAM_NUMSERIESBT1 = 16926;
        private const int NUMPARAM_NUMSERIESBT2 = 16927;
        private const int NUMPARAM_NUMSETIE_INTERN = 18434;
        private const int NUMPARAM_SSID = 17207;
        private const int NUMPARAM_MAC_WIFI = 17209;
        private const int NUMPARAM_MAC_3G = 17210;
        private const int NUMPARAM_REGISTER_CODE = 16365;
        private const int NUMPARAM_FIMWARE_VERSION = 14273;
        private const int NUMPARAM_HARDWARE_VERSION = 15345;
        private const int NUMPARAM_FECHA_FABRICACION = 20565;
        private const int NUMPARAM_FACTOR_IP = 20966;

        private string pathReport = ConfigurationManager.AppSettings["PathReports"];

        protected IShell Shell { get { return DI.Resolve<ShellService>(); } }

        //***************************************************************************************
        //              PRINTLABELNUMSERIE
        //***************************************************************************************

        public Dictionary<string, string> PrintBoxLabel(ITestContext testContext)
        {
            string labelName = GetLabelPath(testContext);

            if (string.IsNullOrEmpty(labelName))
                return null;

            using (var btr = new BarTenderReportService(labelName))
            {
                btr.PrinterName = testContext.TestInfo.PrinterPackingPackage;

                if (string.IsNullOrEmpty(btr.PrinterName) || btr.PrinterName == "NINGUNA")
                    return null;

                return btr.Print(testContext, "03");
            }
        }

        public void PrintLabelFromData(AppService app, Dictionary<string, string> data, string labelName, LabelTamplete.LabelTemplate tipoEtiqueta)
        {
            using (var btr = new BarTenderReportService(labelName))
            {
                btr.PrinterName = tipoEtiqueta == LabelTamplete.LabelTemplate.NumSerie
                    ? app.Model.PrinterLabel : tipoEtiqueta == LabelTamplete.LabelTemplate.Caracteristicas
                    ? app.Model.PrinterCharacteristics : tipoEtiqueta == LabelTamplete.LabelTemplate.EmbalajeIndividual
                    ? app.Model.PrinterIndividualPackage : tipoEtiqueta == LabelTamplete.LabelTemplate.EmbalajeConjunto
                    ? app.Model.PrinterPackingPackage : app.Model.PrinterLabel;

                if (string.IsNullOrEmpty(btr.PrinterName) || btr.PrinterName == "NINGUNA")
                    Shell.MsgBox(string.Format("No se ha seleccionado una impresora para imprimir la etiqueta de {0}", tipoEtiqueta.ToString()), "IMPRESORA NO SELECCIONADA");
                else
                    btr.PrintData(data);
            }
        }

        public string GetLabelPath(ITestContext modelContext, LabelTamplete.LabelTemplate template = LabelTamplete.LabelTemplate.EmbalajeConjunto)
        {
            if (modelContext == null)
                throw new Exception("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            string key = string.Format("Label_{0}_{1}_{2}.btw", template.ToString(), modelContext.NumProducto, modelContext.Version);
            string fileName = null;

            LabelItemViewModel item = GetEtiqueta(modelContext.NumProducto, modelContext.Version, modelContext.IdFase, template);

            if (item == null)
                return null;

            if (item.Data == null)
                throw new Exception("No se ha podido obtener ninguna etiqueta asociada para este producto");

            fileName = SaveBinaryFile(key, item.Data);

            return fileName;
        }

        public string GetLabelPath(int NumProducto, int Version, string IdFase, LabelTamplete.LabelTemplate template = LabelTamplete.LabelTemplate.EmbalajeConjunto)
        {
            string key = string.Format("Label_{0}_{1}_{2}.btw", template.ToString(), NumProducto, Version);
            string fileName = null;

            LabelItemViewModel item = GetEtiqueta(NumProducto, Version, IdFase, template);

            if (item == null)
                return null;

            if (item.Data == null)
                throw new Exception("No se ha podido obtener ninguna etiqueta asociada para este producto");

            fileName = SaveBinaryFile(key, item.Data);

            return fileName;
        }

        private LabelItemViewModel GetEtiqueta(int numProducto, int version, string idFase, LabelTamplete.LabelTemplate template)
        {
            LabelItemViewModel item = null;
            bool NewLabel = false;

            using (var svc = new DezacService())
            {
                item = svc.GetLabels(numProducto, version, idFase, ((int)template).ToString()).FirstOrDefault();

                if (item != null)
                {
                    item.Data = svc.GetBinario(item.NumArticulo, "950");
                    idFase = item.IdFase;
                }

                List<T_GRUPOPARAMETRIZACION> grupos = svc.GetGruposParametrizacion(numProducto, version, idFase);
                NewLabel = grupos.Where(p => p.NUMTIPOGRUPO == LabelTamplete.mapTipoGrupos[template]).Any();

                if (NewLabel)
                {
                    var valueLabesl = grupos.Where(p => p.NUMTIPOGRUPO == LabelTamplete.mapTipoGrupos[template]).FirstOrDefault().T_VALORPARAMETRO.ToList();
                    string validate = valueLabesl.Where((p) => p.T_PARAMETRO.PARAM.Contains("IDP_VALIDATED")).Select(p => p.VALOR).FirstOrDefault();
                    if (validate == null)
                        NewLabel = false;
                    else
                        NewLabel = validate.Trim().ToUpper() == "SI" || validate.Trim().ToUpper() == "PRUEBA";
                }
            }

            return NewLabel ? item : null;
        }

        private string PathFileLabelTemplate
        {
            get { return ConfigurationManager.AppSettings["PathFileLabelTemplate"]; }
        }

        private string SaveBinaryFile(string name, byte[] data)
        {
            if (!Directory.Exists(PathFileLabelTemplate))
                Directory.CreateDirectory(PathFileLabelTemplate);

            string fileName = System.IO.Path.Combine(PathFileLabelTemplate, name);

            if (!File.Exists(fileName))
                File.WriteAllBytes(fileName, data);

            return fileName;
        }

        public DatosEquipoToPrint GetdatosByBastidor(bool POSTVENTA = false, int? Bastidor = null)
        {
            ShellService view = DI.Resolve<ShellService>();
            int bastidor = Bastidor.HasValue ? Bastidor.Value
                : view.ShowInputKeyboard<int>("LECTURA DEL NUMERO DE BASTIDOR", "Lee con el lector de codigo de barras el nº de bastidor del equipo a repetir etiqueta de numero de serie", 0);

            if (bastidor == 0)
                throw new Exception("Error el Bastidor no puede ser 0");

            int Numorden = 0;
            int NumProducto = 0;
            int version = 0;
            string description = "";

            using (IDezacService db = DI.Resolve<IDezacService>())
            {
                if (!POSTVENTA)
                {
                    ORDENPRODUCTO orden = db.GetOrdenProductoByBastidor(bastidor);
                    if (orden == null)
                        throw new Exception("Error, este bastidor no esta registrado en ninguna Orden de fabricación");

                    ORDEN ordenSubconjunto = db.GetOrden(orden.NUMORDEN);
                    if (ordenSubconjunto == null)
                        throw new Exception("Error, este bastidor no esta registrado en ninguna Orden de fabricación");

                    Numorden = orden.NUMORDEN;
                    NumProducto = ordenSubconjunto.NUMPRODUCTO;
                    version = ordenSubconjunto.VERSION;
                    description = ordenSubconjunto.DESCRIPCION;
                }


                T_TESTFASE testFase = db.GetLastTestFase(bastidor, "0", Numorden);
                if (testFase == null)
                    throw new Exception("Error, este bastidor no esta registrado en ningun test anterior");

                if (POSTVENTA)
                {
                    NumProducto = testFase.T_TEST.NUMPRODUCTOTEST.Value;
                    version = testFase.T_TEST.VERSIONPRODUCTOTEST.Value;
                    // description = testFase.T_TEST.T_FAMILIA
                }
                var data = new DatosEquipoToPrint()
                {
                    NUMORDEN = Numorden,
                    NUMTEST = testFase.T_TEST.NUMTEST,
                    NUMTESTFASE = testFase.NUMTESTFASE,
                    NUMPRODUCTO = NumProducto,
                    VERSION = version,
                    IDFASE = Bastidor.HasValue ? null : testFase.IDFASE,
                    NUMMATRICULA = bastidor,
                    RESULTADO = testFase.RESULTADO,
                    NROSERIE = testFase.T_TEST.NROSERIE,
                    DESCRIPCION = description
                };
                return data;
            }
        }

        public DatosEquipoToPrint GetDatosByDeviceData(DeviceDataView DeviceData)
        {
            var data = new DatosEquipoToPrint()
            {
                NUMORDEN = DeviceData.NUMORDEN,
                NUMMATRICULA = DeviceData.NUMMATRICULA,
                NROSERIE = DeviceData.NROSERIE,
                NUMPRODUCTO = DeviceData.NUMPRODUCTO,
                VERSION = DeviceData.VERSION,
                DESCRIPCION = DeviceData.DESCRIPCION,
                NUMCAJA = DeviceData.NUMCAJA,
                IDCAJA = DeviceData.IDCAJA,
                RESULTADO = "O"
            };
            return data;
        }

        public DatosEquipoToPrint GetDatosByBoxData(BoxesView BoxData, int numOrden = 0)
        {
            T_TESTFASE lastTestFaseOk;
            using (var ds = new DezacService())
            {
                lastTestFaseOk = ds.GetLastTestFaseOKBySerialNumber(numOrden, BoxData.HASTANROSERIE);
            }
            var data = new DatosEquipoToPrint()
            {
                NUMORDEN = numOrden,
                NROSERIE = BoxData.HASTANROSERIE,
                NUMCAJA = BoxData.NUMCAJA,
                IDCAJA = BoxData.NUMEROCAJA,
                RESULTADO = "O",
                NUMMATRICULA = lastTestFaseOk.T_TEST.NUMMATRICULA,
                NUMPRODUCTO = lastTestFaseOk.T_TEST.NUMPRODUCTOTEST,
                VERSION = lastTestFaseOk.T_TEST.VERSIONPRODUCTOTEST,
                IDFASE = lastTestFaseOk.IDFASE
            };
            return data;
        }

        public PackagingLabels SelectPackagingLabel()
        {
            var list = new List<PackagingLabels>();
            list.Add(new PackagingLabels { EMBALAJE = "ETIQUETA EMBALAJE INDIVIDUAL", PLANTILLA = LabelTamplete.LabelTemplate.EmbalajeIndividual });
            list.Add(new PackagingLabels { EMBALAJE = "ETIQUETA EMBALAJE CONJUNTO", PLANTILLA = LabelTamplete.LabelTemplate.EmbalajeConjunto });

            PackagingLabels boxTables = Shell.ShowTableDialog<PackagingLabels>("TIPO ETIQUETA EMBALAJE", new TableGridViewConfig
            {
                Data = list,
                Fields = "EMBALAJE",
            });

            if (boxTables == null)
            {
                Shell.MsgBox("NO SE HA SELECCIONADO NINGÚN TIPO DE ETIQUETA, NO SE HARÁ NADA", "SELECCIÓN INCORRECTA");
                return null;
            }
            return boxTables;
        }

        public GetDatosBBDDToPrinLabel ConfigPrintLabels(AppService app, DatosEquipoToPrint Current, string TipoEtiqueta = null)
        {
            if (string.IsNullOrEmpty(app.Model.PrinterLabel) || (app.Model.PrinterLabel == "NINGUNA"))
                throw new Exception("Debe selecionar una impresora para poder imprimir una etiqueta");

            if (Current == null)
                throw new Exception("Debe selecionar un bastidor para poder imprimir su etiqueta de numero de serie");

            if (Current.RESULTADO != "O")
                throw new Exception("Debe selecionar un bastidor con resultado OK para poder imprimir su etiqueta");

            LabelItemViewModel labelTemplate = null;

            ITestContext testContext = null;
            string tipoEtiqueta = TipoEtiqueta;
            string fileName = null;
            T_TESTFASE testFase = null;
            bool NewLabel = false;
            int grupoLabel = 0;
            var NumCodelabelBBDD = new List<string>();
            int? numeroCopiesBBDD = null;

            List<T_VALORPARAMETRO> identificacion = null;
            List<T_GRUPOPARAMETRIZACION> grupos = null;
            using (var ds = new DezacService())
            {
                testFase = ds.GetLastTestFase(Current.NUMMATRICULA.Value, Current.IDFASE ?? "0", Current.NUMORDEN, true, true);
                if (testFase == null)
                    throw new Exception("Error al imprimir la etiqueta porque no se encuentra un TestFase en la BBDD");

                List<LabelItemViewModel> labelTemplates = ds.GetLabels(Current.NUMPRODUCTO.Value, Current.VERSION.Value, Current.IDFASE);

                if (labelTemplates != null && labelTemplates.Any())
                {
                    if (tipoEtiqueta == null)
                    {
                        labelTemplates = labelTemplates.ToList();
                        if (labelTemplates.Count() > 1)
                            labelTemplate = Shell.ShowTableDialog<LabelItemViewModel>("ETIQUETAS", new TableGridViewConfig
                            {
                                Data = labelTemplates,
                                Fields = "Descripcion"
                            });
                        else
                            labelTemplate = labelTemplates.FirstOrDefault();

                        tipoEtiqueta = labelTemplate.Tipo;
                    }
                    else
                        labelTemplate = labelTemplates.Where(l => l.Tipo == tipoEtiqueta).FirstOrDefault();

                    grupos = ds.GetGruposParametrizacion(Current.NUMPRODUCTO.Value, Current.VERSION.Value, Current.IDFASE ?? labelTemplate.IdFase);

                    grupoLabel = LabelTamplete.mapTipoGrupos[(LabelTamplete.LabelTemplate)Convert.ToByte(tipoEtiqueta)];

                    NewLabel = grupos.Where(p => p.NUMTIPOGRUPO == grupoLabel).Any();
                    if (NewLabel)
                    {
                        var labelsNewMethod = grupos.Where(p => p.NUMTIPOGRUPO == grupoLabel).FirstOrDefault().T_VALORPARAMETRO.ToList();
                        if (labelsNewMethod != null)
                        {
                            string validate = labelsNewMethod.Where((p) => p.T_PARAMETRO.PARAM.Contains("IDP_VALIDATED")).Select(p => p.VALOR).FirstOrDefault();
                            if (validate == null)
                                NewLabel = false;
                            else
                                NewLabel = validate.Trim().ToUpper() == "SI" || validate.Trim().ToUpper() == "PRUEBA";

                            if (NewLabel)
                            {
                                string numCopies = labelsNewMethod.Where((p) => p.T_PARAMETRO.PARAM.Contains("LABEL_NUMBER_COPIES")).Select(p => p.VALOR).FirstOrDefault();
                                numeroCopiesBBDD = numCopies != null ? Convert.ToInt32(numCopies) : 1;

                                string key = string.Format("Label_{0}_{1}_{2}.btw", tipoEtiqueta, Current.NUMPRODUCTO, Current.VERSION);
                                byte[] data = ds.GetBinario(labelTemplate.NumArticulo, "950");

                                if (data == null)
                                    throw new Exception("Etiqueta no encontrada en OT doc!");

                                fileName = SaveBinaryFile(key, data);

                                testContext = new TestContextModel
                                {
                                    NumProducto = Current.NUMPRODUCTO.Value,
                                    Version = Current.VERSION.Value,
                                    IdFase = Current.IDFASE,
                                    NumOrden = Current.NUMORDEN,
                                    Direcciones = ds.GetDireccionesByNumProducto(Current.NUMPRODUCTO.Value, Current.VERSION.Value),
                                };

                                RellenarTestContext(testContext, testFase);
                                grupos.ForEach(p =>
                                {
                                    ParamValueCollection list = testContext.GetGroupList(p.NUMTIPOGRUPO);
                                    if (list != null)
                                    {
                                        if (p.NUMTIPOGRUPO == 8) //Grupo 8 = RESULTADOS
                                            foreach (T_TESTVALOR vp in testFase.T_TESTVALOR)
                                            {
                                                T_VALORPARAMETRO param = p.T_VALORPARAMETRO.Where(t => { return t.NUMPARAM == vp.NUMPARAM; }).FirstOrDefault();

                                                var pv = new ParamValue
                                                {
                                                    IdGrupo = param.NUMGRUPO,
                                                    IdTipoGrupo = (TipoGrupo)p.NUMTIPOGRUPO,
                                                    Grupo = list.Name,
                                                    IdParam = vp.NUMPARAM,
                                                    ValorInicio = param.VALORINICIO,
                                                    Valor = vp.VALOR,
                                                    IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                                                    IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                                                    Unidad = param.T_PARAMETRO.T_UNIDAD.UNIDAD,
                                                    Name = vp.T_PARAMETRO.PARAM,
                                                    IdCategoria = param.IDCATEGORIA
                                                };

                                                list.Add(pv);
                                            }
                                        else
                                            foreach (T_VALORPARAMETRO vp in p.T_VALORPARAMETRO)
                                            {
                                                var pv = new ParamValue
                                                {
                                                    IdGrupo = vp.NUMGRUPO,
                                                    IdTipoGrupo = (TipoGrupo)p.NUMTIPOGRUPO,
                                                    Grupo = list.Name,
                                                    IdParam = vp.NUMPARAM,
                                                    ValorInicio = vp.VALORINICIO,
                                                    Valor = vp.VALOR,
                                                    IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                                                    IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                                                    Unidad = vp.T_PARAMETRO.T_UNIDAD.UNIDAD,
                                                    Name = vp.T_PARAMETRO.PARAM,
                                                    IdCategoria = vp.IDCATEGORIA
                                                };

                                                list.Add(pv);
                                            }
                                    }
                                });

                                testContext.TestInfo.PrinterLabel = app.Model.PrinterLabel;
                                testContext.TestInfo.PrinterCharacteristics = app.Model.PrinterCharacteristics;
                                testContext.TestInfo.PrinterIndividualPackage = app.Model.PrinterIndividualPackage;
                                testContext.TestInfo.PrinterPackingPackage = app.Model.PrinterPackingPackage;
                            }
                        }
                    }
                }

                grupos = grupos == null ? ds.GetGruposParametrizacion(Current.NUMPRODUCTO.Value, Current.VERSION.Value, Current.IDFASE) : grupos;
                IEnumerable<T_GRUPOPARAMETRIZACION> identGroup = grupos.Where(p => p.NUMTIPOGRUPO == 5);
                if (identGroup.Count() != 0)
                    identificacion = identGroup.FirstOrDefault().T_VALORPARAMETRO.ToList();
                else
                    identificacion = null;
            }

            if (identificacion != null && !NewLabel)
            {
                IEnumerable<T_VALORPARAMETRO> labels = identificacion.Where((p) => p.T_PARAMETRO.PARAM.Contains("LABEL"));
                if (labels == null && !NewLabel)
                    throw new Exception("Error al imprimir la etiqueta porque no se encuentrado ninguna Etiqueta configurada en la BBDD");

                string numCopies = identificacion.Where((p) => p.T_PARAMETRO.PARAM.Contains("LABEL_NUMBER_COPIES")).Select(p => p.VALOR).FirstOrDefault();
                numeroCopiesBBDD = numCopies != null ? Convert.ToInt32(numCopies) : 1;

                foreach (T_VALORPARAMETRO label in labels)
                    if (!label.T_PARAMETRO.PARAM.Contains("LABEL_NUMBER_COPIES"))
                        NumCodelabelBBDD.Add(label.VALOR);
            }

            return new GetDatosBBDDToPrinLabel()
            {
                Context = testContext,
                NumCodeLabel = NumCodelabelBBDD,
                NumCopiesLabel = !numeroCopiesBBDD.HasValue ? 1 : Convert.ToInt32(numeroCopiesBBDD),
                TipoEtiqueta = tipoEtiqueta,
                FileName = fileName,
                TestFase = testFase
            };
        }

        public void PrintLabelFromDataFile(ITestContext context, string tipoEtiqueta, string labelDataFile, string fileName, int numcopies)
        {
            Dictionary<string, string> labelData = Utils.AppProfile.DeserializeFile<Dictionary<string, string>>(labelDataFile);

            using (var btr = new BarTenderReportService(fileName))
            {
                btr.Print(context, tipoEtiqueta, labelData, numcopies);
            }
        }

        public void GenerateLabelAndPrint(ITestContext testContext, T_TESTFASE testFase, string tipoEtiqueta, string fileName, int numcopies)
        {
            if (testContext == null)
                throw new Exception("No se ha podido obtener un testContext para obtener la información de la etiqueta");

            if (tipoEtiqueta == "03")
            {
                bool boxIsFinishedOrClosed = false;
                using (IDezacService db = DI.Resolve<IDezacService>())
                {
                    bool boxFinished, boxClosed = false;
                    boxFinished = db.GetBoxHasFinished(testContext.IdCajaActual, testContext.MaxSerialNumberBox);
                    if (!boxFinished)
                        boxClosed = db.GetBoxHasClosed(testContext.IdCajaActual);

                    boxIsFinishedOrClosed = boxFinished | boxClosed;
                }

                if (!boxIsFinishedOrClosed)
                    throw new Exception(string.Format("La caja {0} no está cerrada", testContext.IdCajaActual));
            }

            RellenarTestContext(testContext, testFase);

            using (var btr = new BarTenderReportService(fileName))
            {
                btr.Print(testContext, tipoEtiqueta);
            }
        }

        public void PrintLabelCurrentNumSerie(T_TESTFASE testFase, string NumparamLabel, int numcopies, string PrinterSelected)
        {
            string pathLabels = ConfigurationManager.AppSettings["PathBarTenderLabels"];

            TestInfo testInfo = RellenarTestInfo(testFase);

            using (var btr = new BarTenderReport())
            {
                btr.LabelFileName = Path.Combine(pathLabels, NumparamLabel + ".btw");
                btr.PrinterName = PrinterSelected;
                btr.IdenticalCopiesOfLabel = numcopies;
                btr.PrintTest(testInfo);
            }
        }

        public string GetFileLabelData(int numBastidor, string name, string tipoDoc = "501")
        {
            List<T_TESTFASEFILE> testFaseFiles;

            string fileName = Path.Combine(PathFileLabelTemplate, name + "_" + numBastidor);
            if (File.Exists(fileName))
                return fileName;

            using (IDezacService db = DI.Resolve<IDezacService>())
            {
                if (name.Contains("EmbalajeConjunto"))
                {
                    int? idCaja = db.GetIDCajaByBastidor(numBastidor);
                    if (idCaja == null)
                        throw new Exception(string.Format("Error repitiendo etiqueta, el bastidor {0} no se encuentra registrado en ninguna caja", numBastidor));

                    name += string.Format("_{0}", idCaja);
                    testFaseFiles = db.GetTestFaseFiles(name, tipoDoc);
                }
                else
                    testFaseFiles = db.GetTestFaseFiles(numBastidor, tipoDoc);
            }

            if (testFaseFiles == null)
                return null;

            T_TESTFASEFILE testFaseFile = testFaseFiles.Where(p => p.NOMBRE == name).FirstOrDefault();
            if (testFaseFile == null || testFaseFile.FICHERO == null)
                return null;

            string pathFile = SaveBinaryFile(testFaseFile.NOMBRE + "_" + numBastidor, testFaseFile.FICHERO);
            return pathFile;
        }

        public string GetFileLabelBoxData(int idCaja, string tipoDoc = "501")
        {
            List<T_TESTFASEFILE> testFaseFiles;
            string name = string.Format("PRINT_LABEL_EmbalajeConjunto_{0}", idCaja);

            string fileName = Path.Combine(PathFileLabelTemplate, name);
            if (File.Exists(fileName))
                return fileName;

            using (IDezacService db = DI.Resolve<IDezacService>())
            {
                testFaseFiles = db.GetTestFaseFiles(name, tipoDoc);
            }

            if (testFaseFiles == null)
                return null;

            T_TESTFASEFILE testFaseFile = testFaseFiles.Where(p => p.NOMBRE == name).FirstOrDefault();
            if (testFaseFile == null || testFaseFile.FICHERO == null)
                return null;

            string pathFile = SaveBinaryFile(testFaseFile.NOMBRE, testFaseFile.FICHERO);
            return pathFile;
        }

        public void RellenarTestContext(ITestContext testContext, T_TESTFASE testFase)
        {
            var valores = testFase.T_TESTVALOR.ToList();

            using (IDezacService db = DI.Resolve<IDezacService>())
            {
                testContext.IdCajaActual = db.GetIDCajaByBastidor(testFase.T_TEST.NUMMATRICULA.Value).Value;
                testContext.MinSerialNumberBox = db.GetMinNumSerieCaja(testContext.IdCajaActual);
                testContext.MaxSerialNumberBox = db.GetMaxNumSerieCaja(testContext.IdCajaActual);
                testContext.SerialNumberBoxRange = db.GetAllSerialNumbersBox(testContext.IdCajaActual);
                testContext.ReferenciaLineaCliente = db.GetRefClientLine(testContext.NumOrden.Value);
                testContext.ReferenciaOrigen = db.GetReferenciaOrigen(testContext.NumOrden.Value);
                testContext.TestInfo.CodCircutor = db.GetCodigoCircutor(testFase.T_TEST.NUMPRODUCTOTEST.Value);
                testContext.TestInfo.CostumerCode2 = db.GetCostumerCode2(testFase.T_TEST.NUMPRODUCTOTEST.Value);
                testContext.Atributos = db.GetAtributosCliente(testFase.T_TEST.PRODUCTO.NUMPRODUCTO, testContext.TestInfo.CodCircutor);
                testContext.DescripcionLenguagesCliente = db.GetDescripcionLenguagesCliente(testFase.T_TEST.PRODUCTO.NUMPRODUCTO, testContext.TestInfo.CodCircutor);
                testContext.EAN = db.GetEANCode(testFase.T_TEST.PRODUCTO.NUMPRODUCTO, testContext.TestInfo.CodCircutor);
                testContext.EAN_MARCA = db.GetEANMarcaCode(testFase.T_TEST.PRODUCTO.NUMPRODUCTO, testContext.TestInfo.CodCircutor);
                testContext.UdsCajaActual = testFase.T_TEST.PRODUCTO.ARTICULO.UDSEMBALAJE.Value;
                testContext.NumEquiposCaja = db.GetNumEquiposCajaActual(testContext.IdCajaActual);
                testContext.Direcciones = db.GetDireccionesByNumProducto(testFase.T_TEST.NUMPRODUCTOTEST.Value, testFase.T_TEST.VERSIONPRODUCTOTEST.Value);
                testContext.Lote = db.GetLoteCaja(testContext.IdCajaActual);
                testContext.ConfigurationFiles.LoadAllFilesFromBBDD(testFase.T_TEST.PRODUCTO);
            }

            RellenarTestInfo(testFase, testContext);
        }

        public TestInfo RellenarTestInfo(T_TESTFASE testFase, ITestContext testContext = null)
        {
            var valores = testFase.T_TESTVALOR.ToList();

            TestInfo testInfo;

            if (testContext != null)
                testInfo = testContext.TestInfo;
            else
                testInfo = new TestInfo();

            using (IDezacService db = DI.Resolve<IDezacService>())
            {
                testInfo.CodCircutor = db.GetCodigoCircutor(testFase.T_TEST.NUMPRODUCTOTEST.Value);
                testInfo.CostumerCode2 = db.GetCostumerCode2(testFase.T_TEST.NUMPRODUCTOTEST.Value);
            }

            testInfo.NumBastidor = Convert.ToUInt64(testFase.T_TEST.NUMMATRICULATEST);
            testInfo.NumSerie = testFase.T_TEST.NROSERIE;
            testInfo.NumSerieInterno = testFase.T_TEST.NROSERIEORIGINAL;
            testInfo.ProductoVersion = string.Format("{0}/{1}", testFase.T_TEST.NUMPRODUCTOTEST, testFase.T_TEST.VERSIONPRODUCTOTEST);
            testInfo.Producto = testFase.T_TEST.NUMPRODUCTOTEST.ToString();
            testInfo.FirmwareVersion = valores.Where(p => p.NUMPARAM == NUMPARAM_FIMWARE_VERSION).Select(p => p.VALOR).FirstOrDefault();
            testInfo.HardwareVersion = valores.Where(p => p.NUMPARAM == NUMPARAM_HARDWARE_VERSION).Select(p => p.VALOR).FirstOrDefault();

            testInfo.RefVenta = testFase.T_TEST.PRODUCTO.ARTICULO.REFVENTA;
            testInfo.DescripcionComercial = testFase.T_TEST.PRODUCTO.ARTICULO.DESCRIPCIONCOMERCIAL;
            testInfo.CodigoBcn = testFase.T_TEST.PRODUCTO.ARTICULO.CODIGOBCN;

            testInfo.NumMAC = valores.Where(p => p.NUMPARAM == NUMPARAM_MAC).Select(p => p.VALOR).FirstOrDefault();
            testInfo.NumMAC_PLC = valores.Where(p => p.NUMPARAM == NUMPARAM_MAC_PLC).Select(p => p.VALOR).FirstOrDefault();
            testInfo.NumSerieInterno = valores.Where(p => p.NUMPARAM == NUMPARAM_NUMSETIE_INTERN).Select(p => p.VALOR).FirstOrDefault();
            testInfo.IMEI = valores.Where(p => p.NUMPARAM == NUMPARAM_IMEI).Select(p => p.VALOR).FirstOrDefault();
            testInfo.SSID = valores.Where(p => p.NUMPARAM == NUMPARAM_SSID).Select(p => p.VALOR).FirstOrDefault();
            testInfo.NumMACWifi = valores.Where(p => p.NUMPARAM == NUMPARAM_MAC_WIFI).Select(p => p.VALOR).FirstOrDefault();
            testInfo.NumMAC3G = valores.Where(p => p.NUMPARAM == NUMPARAM_MAC_3G).Select(p => p.VALOR).FirstOrDefault();
            testInfo.PSW = valores.Where(p => p.NUMPARAM == NUMPARAM_REGISTER_CODE).Select(p => p.VALOR).FirstOrDefault();
            testInfo.FechaFabricacion = valores.Where(p => p.NUMPARAM == NUMPARAM_FECHA_FABRICACION).Select(p => p.VALOR).FirstOrDefault();
            testInfo.FactoryIP = valores.Where(p => p.NUMPARAM == NUMPARAM_FACTOR_IP).Select(p => p.VALOR).FirstOrDefault();

            var numserieSubconjuntos = new Dictionary<string, string>();
            string valor = valores.Where(p => p.NUMPARAM == NUMPARAM_NUMSERIESBT).Select(p => p.VALOR).FirstOrDefault();

            numserieSubconjuntos.Add("NS1", valor);

            valor = valores.Where(p => p.NUMPARAM == NUMPARAM_NUMSERIESBT1).Select(p => p.VALOR).FirstOrDefault();
            numserieSubconjuntos.Add("NS2", valor);

            valor = valores.Where(p => p.NUMPARAM == NUMPARAM_NUMSERIESBT2).Select(p => p.VALOR).FirstOrDefault();
            numserieSubconjuntos.Add("NS3", valor);

            testInfo.NumSerieSubconjunto = numserieSubconjuntos;

            return testInfo;
        }

        public struct GetDatosBBDDToPrinLabel
        {
            public ITestContext Context;
            public List<string> NumCodeLabel;
            public int NumCopiesLabel;
            public string TipoEtiqueta;
            public string FileName;
            public T_TESTFASE TestFase;
        }

        public class DatosEquipoToPrint
        {
            public int? NUMTEST { get; set; }
            public int? NUMTESTFASE { get; set; }
            public int NUMORDEN { get; set; }
            public int? NUMMATRICULA { get; set; }
            public int? NUMPRODUCTO { get; set; }
            public int? VERSION { get; set; }
            public string DESCRIPCION { get; set; }
            public string NROSERIE { get; set; }
            public int? NUMCAJA { get; set; }
            public int? IDCAJA { get; set; }
            public string IDFASE { get; set; }
            public string RESULTADO { get; set; }
            public bool ETIQUETACAJA { get; set; }
        }

        //***************************************************************************************
        //              SHOWPDFINSTRUCTIONS
        //***************************************************************************************
        public int ShowPdfInstructions(int numProducto, int version, string idFase)
        {
            byte[] data = null;

            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                string sql = string.Format(@"
                    SELECT it.DocPdf as Data
                    FROM app.productofase pf
                    INNER JOIN app.productofaseinstec pfi ON pfi.numproducto = pf.numproducto AND pfi.version = pf.version AND pfi.secuencia = pf.secuencia
                    INNER JOIN app.instrucciontecnica it ON pfi.refinstruccion = it.refinstruccion
                    WHERE pf.NumProducto={0} AND pf.Version={1} AND pf.idfase = {2}
                    ORDER BY pf.secuencia",
                    numProducto, version, idFase);

                data = db.SqlQuery<BlobModel>(sql).Select(p => p.Data).FirstOrDefault();

                if (data == null)
                {

                    string sql2 = string.Format(@"
                    SELECT pf.idfase
                    FROM app.productofase pf
                    WHERE pf.NumProducto= {0} AND pf.Version={1}
					and pf.secuencia = 
					(select pf2.secuenciapadre FROM app.productofase pf2
                    WHERE pf2.NumProducto= {0} AND pf2.Version={1} AND pf2.idfase = '{2}')
                    ORDER BY pf.secuencia",
                    numProducto, version, idFase);

                    string fasePadre = db.SqlQuery<string>(sql2).FirstOrDefault();

                    string sql3 = string.Format(@"
                    SELECT it.DocPdf as Data
                    FROM app.productofase pf
                    INNER JOIN app.productofaseinstec pfi ON pfi.numproducto = pf.numproducto AND pfi.version = pf.version AND pfi.secuencia = pf.secuencia
                    INNER JOIN app.instrucciontecnica it ON pfi.refinstruccion = it.refinstruccion
                    WHERE pf.NumProducto={0} AND pf.Version={1} AND pf.idfase ='{2}'
                    ORDER BY pf.secuencia",
                   numProducto, version, fasePadre);

                    data = db.SqlQuery<BlobModel>(sql3).Select(p => p.Data).FirstOrDefault();
                }
            }

            if (data != null)
                return System.Diagnostics.Process.Start(Utils.Tools.CrearUrlFileInstruccion(data)).Id;

            return -1;
        }

        public class BlobModel
        {
            public byte[] Data { get; set; }
        }

        //***************************************************************************************
        //              LABELTEMPLATE
        //***************************************************************************************
        public string ExportBarTenderFile(string fileName)
        {
            using (var bt = new BarTenderService(fileName))
            {
                string path = bt.ExportImage(PathFileLabelTemplate);
                return path;
            }
        }

        public string SetFieldsFileAndExportImage(string fileName, List<TemplateView> fields)
        {
            var templateView = new List<BarTenderNamedItem>();

            using (var bt = new BarTenderService(fileName))
            {
                foreach (TemplateView field in fields)
                    templateView.Add(new BarTenderNamedItem { Name = field.KEY, Value = field.VALUE });

                bt.SetFields(templateView);
                string ImagePath = bt.ExportImage(PathFileLabelTemplate);

                return ImagePath;
            }
        }

        public List<TemplateView> GetFieldsFile(string fileName)
        {
            var templateView = new List<TemplateView>();
            using (var bt = new BarTenderService(fileName))
            {
                List<BarTenderNamedItem> fields = bt.GetFields();
                foreach (BarTenderNamedItem field in fields)
                    templateView.Add(new TemplateView { KEY = field.Name, VALUE = field.Value });

                return templateView;
            }
        }

    }
}