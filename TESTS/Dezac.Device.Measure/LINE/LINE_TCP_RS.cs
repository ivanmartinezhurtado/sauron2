﻿using Dezac.Device.Bluetooth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Device.Measure
{
    public class LINE_TCP_RS : ApiDevice
    {
        public LINE_TCP_RS()
        {
        }

        public void SetPort(string host, string url)
        {
            LogMethod();
            SetApiDevice(string.Format("http://{0}/{1}", host, url));
        }

        public TestParams GetTestParams()
        {
            LogMethod();
            return Task.Run(async () => await Get<TestParams>("/test-params")).GetAwaiter().GetResult();
        }
        public DeviceInfo GetDeviceInfo()
        {
            LogMethod();
            return Task.Run(async () => await Get<DeviceInfo>("/device-info")).GetAwaiter().GetResult();
        }
        public Ethernet GetEthernetConfig()
        {
            LogMethod();
            return Task.Run(async () => await Get<Ethernet>("/net/eth")).GetAwaiter().GetResult();
        }
        public WiFi GetWifiConfig()
        {
            LogMethod();
            return Task.Run(async () => await Get<WiFi>("/net/wifi")).GetAwaiter().GetResult();
        }
        public Bluetooth GetBluetoothConfig()
        {
            LogMethod();
            return Task.Run(async () => await Get<Bluetooth>("/bluetooth")).GetAwaiter().GetResult();
        }
        public Serial_Port GetSerialPortConfig()
        {
            LogMethod();
            return Task.Run(async () => await Get<Serial_Port>("/serial-port")).GetAwaiter().GetResult();
        }
        public Protocol GetProtocolConfig()
        {
            LogMethod();
            return Task.Run(async () => await Get<Protocol>("/protocol")).GetAwaiter().GetResult();
        }
        public Status GetStatus()
        {
            LogMethod();
            return Task.Run(async () => await Get<Status>("/status")).GetAwaiter().GetResult();
        }

        public void SetCustomizeParams(CustomizeParams body)
        {
            LogMethod();
            Task.Run(async () => await Put<CustomizeParams>("/test-params", body)).GetAwaiter().GetResult();
        }
        public void SetEthernetConfig(Ethernet body)
        {
            LogMethod();
            Task.Run(async () => await Put<Ethernet>("/net/eth", body)).GetAwaiter().GetResult();
        }
        public void SetWifiConfig(WiFi body)
        {
            LogMethod();
            Task.Run(async () => await Put<WiFi>("/net/wifi", body)).GetAwaiter().GetResult();
        }
        public void SetSerialPortConfig(Serial_Port body)
        {
            LogMethod();
            Task.Run(async () => await Put<Serial_Port>("/serial-port", body)).GetAwaiter().GetResult();
        }

        public void SetProtocolConfig(Protocol body)
        {
            LogMethod();
            Task.Run(async () => await Put<Protocol>("/protocol", body)).GetAwaiter().GetResult();
        }
        public void OnLed(Leds led)
        {
            LogMethod();
            var ledsOn = new LedsOn(led);
            Task.Run(async () => await Put<LedsOn>("/test-params", ledsOn)).GetAwaiter().GetResult();
        }
        public void OffLed(Leds led)
        {
            LogMethod();
            var ledsOff = new LedsOff(led);
            Task.Run(async () => await Put<LedsOff>("/test-params", ledsOff)).GetAwaiter().GetResult();
        }
        public void ResetLeds()
        {
            LogMethod();
            Task.Run(async () => await Put("/test-params", new ResetLedsTest { resetLedsTest = true })).GetAwaiter().GetResult();
        }

        public void UpdateFirmware(System.IO.Stream body)
        {
            LogMethod();
            Task.Run(async () => await Post("/update", body)).GetAwaiter().GetResult();
        }


        #region Bluetooth

        private TCP_RSDevice tcpBLE;

        public void BLEFindDevice(string serialNumber)
        {
            _logger.Info("Scanning TCP_RS bluetooth LE Device...");

            tcpBLE = null;

            try
            {
                tcpBLE = TCP_RSDevice.Find(serialNumber, 5000);

                if (tcpBLE != null)
                    _logger.Info($"TCP-RS Device Found at Address {tcpBLE.BluetoothAddress}");
                else
                {
                    _logger.Info("No device found!");
                    throw new Exception("Error comunications Bluetooth, no device found");
                }
            }
            catch (Exception ex)
            {
                _logger.Info(ex.Message);
                if (ex.InnerException != null)
                    _logger.Info(ex.InnerException.Message);

                if (tcpBLE != null)
                    tcpBLE.Dispose();

                throw new Exception(ex.Message);
            }
        }

        public struct BLEDeviceInfo
        {
            public string ManufacturerName;
            public string ModelNumber;
            public string SerialNumber;
            public string HardwareRevision;
            public string FirmwareRevision;
        }

        public BLEDeviceInfo BLEGetInfo()
        {
            var value = new BLEDeviceInfo();

            value.ModelNumber = tcpBLE.GetModelNumberAsync();
            _logger.Info($"Device Model: {value.ModelNumber}");

            value.ManufacturerName = tcpBLE.GetManufacturerNameAsync();
            _logger.Info($"Manufacturer Name: { value.ManufacturerName}");

            value.SerialNumber = tcpBLE.GetSerialNumberAsync();
            _logger.Info($"Serial Number: {value.SerialNumber}");

            value.HardwareRevision = tcpBLE.GetHardwareRevisionAsync();
            _logger.Info($"HW Revision: {value.HardwareRevision}");

            value.FirmwareRevision = tcpBLE.GetFirmwareRevisionAsync();
            _logger.Info($"Firmware Revision: {value.FirmwareRevision}");

            return value;
        }

        public bool PairAsync()
        {
            _logger.Info("Starting pairing...");
            var result = tcpBLE.PairAsync(CVM_E3Device.DEFAULT_PIN);
            _logger.InfoFormat("Pairing {0}", result);
            return result;
        }

        public bool UnpairAsync()
        {
            _logger.Info("Starting UnpairAsync...");
            var result = tcpBLE.UnpairAsync();
            _logger.InfoFormat("Unpairing {0}", result);
            return result;
        }


        #endregion

        public class LedsOn
        {
            public string ledsRegOn { get; set; }

            public LedsOn(Leds led)
            {
                ledsRegOn = "0x" + ((ushort)led).ToString("X2");
            }
        }

        public class LedsOff
        {
            public string ledsRegOff { get; set; }

            public LedsOff(Leds led)
            {
                ledsRegOff = "0x" + ((ushort)led).ToString("X2");
            }
        }

        public class ResetLedsTest
        {
            public bool resetLedsTest { get; set; } 
        }

        [Flags]
        public enum Leds
        {
            POWER_LED = 0x01,
            LAN_LED = 0x02,
            WLAN_LED = 0x04,
            TX_LED = 0x08,
            RX_LED = 0x10,
            BT_LED = 0x20,
            ALARM_LED = 0x40,
            ALL_LEDS = POWER_LED | LAN_LED | WLAN_LED | TX_LED | RX_LED | BT_LED | ALARM_LED,
            ODD_LEDS = POWER_LED | LAN_LED | RX_LED,
            PAIR_LEDS = WLAN_LED | TX_LED | BT_LED | ALARM_LED               
        }
        public class TestParams
        {
            [Newtonsoft.Json.JsonProperty("serialNum", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string serialNum { get; set; }

            [Newtonsoft.Json.JsonProperty("frameNum", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string frameNum { get; set; }

            [Newtonsoft.Json.JsonProperty("prodStatus", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string prodStatus { get; set; }

            [Newtonsoft.Json.JsonProperty("firmVer", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string firmVer { get; set; }

            [Newtonsoft.Json.JsonProperty("rssi", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string rssi { get; set; }

            [Newtonsoft.Json.JsonProperty("ledStatus", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string ledStatus { get; set; }
        }

        public class CustomizeParams
        {
            [Newtonsoft.Json.JsonProperty("serialNum", Required = Newtonsoft.Json.Required.Default)]
            public string serialNum { get; set; }

            [Newtonsoft.Json.JsonProperty("frameNum", Required = Newtonsoft.Json.Required.Default)]
            public string frameNum { get; set; }

            [Newtonsoft.Json.JsonProperty("prodStatus", Required = Newtonsoft.Json.Required.Default)]
            public string prodStatus { get; set; }

            [Newtonsoft.Json.JsonProperty("wifiDefault", Required = Newtonsoft.Json.Required.Default)]
            public string wifiDefault { get; set; }
            [Newtonsoft.Json.JsonProperty("serialPortDefault", Required = Newtonsoft.Json.Required.Default)]
            public string serialPortDefault { get; set; }
        }
        public class DeviceInfo
        {
            [Newtonsoft.Json.JsonProperty("SerialNumer", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Include)]
            public string SerialNumer { get; set; }

            [Newtonsoft.Json.JsonProperty("SerialId", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Include)]
            public double SerialId { get; set; }

            [Newtonsoft.Json.JsonProperty("Date", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Include)]
            public string Date { get; set; }

            [Newtonsoft.Json.JsonProperty("Model", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Include)]
            public string Model { get; set; }

            /// <summary>Firmware versions</summary>
            [Newtonsoft.Json.JsonProperty("Firmware", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Include)]
            public Firmware Firmware { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }


        }      

        public class Ethernet
        {
            [Newtonsoft.Json.JsonProperty("Connected", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public bool Connected { get; set; }

            [Newtonsoft.Json.JsonProperty("DHCP", Required = Newtonsoft.Json.Required.Always)]
            public bool DHCP { get; set; }

            [Newtonsoft.Json.JsonProperty("IP", Required = Newtonsoft.Json.Required.Default)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string IP { get; set; }

            [Newtonsoft.Json.JsonProperty("Mask", Required = Newtonsoft.Json.Required.Default)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Mask { get; set; }

            [Newtonsoft.Json.JsonProperty("Gateway", Required = Newtonsoft.Json.Required.Default)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Gateway { get; set; }

            [Newtonsoft.Json.JsonProperty("MAC", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string MAC { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }


        }
       
        public class WiFi
        {
            [Newtonsoft.Json.JsonProperty("Enabled", Required = Newtonsoft.Json.Required.Always)]
            public bool Enabled { get; set; }

            [Newtonsoft.Json.JsonProperty("Connected", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public bool Connected { get; set; }

            [System.ComponentModel.DataAnnotations.Required]
            [System.ComponentModel.DataAnnotations.StringLength(32, MinimumLength = 0)]
            public string SSID { get; set; }

            [Newtonsoft.Json.JsonProperty("Password", Required = Newtonsoft.Json.Required.Default)]
            [System.ComponentModel.DataAnnotations.Required]
            [System.ComponentModel.DataAnnotations.StringLength(32, MinimumLength = 0)]
            public string Password { get; set; }

            [Newtonsoft.Json.JsonProperty("IP", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string IP { get; set; }

            [Newtonsoft.Json.JsonProperty("Mask", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string Mask { get; set; }

            [Newtonsoft.Json.JsonProperty("Gateway", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string Gateway { get; set; }

            [Newtonsoft.Json.JsonProperty("MAC", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string MAC { get; set; }

            /// <summary>Signal strength percentage</summary>
            [Newtonsoft.Json.JsonProperty("Strength", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public double Strength { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }


        }
     
        public class Bluetooth
        {
            [Newtonsoft.Json.JsonProperty("Name", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string Name { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }
        }

        public class Status
        {
            /// <summary>Status of firmware upgrade. If successful returns 1, else -1.  0 is just for information on the latest firmware request.</summary>
            [Newtonsoft.Json.JsonProperty("Status", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public double Status1 { get; set; }

            /// <summary>Compilation time (e.g.: "13:09:12")</summary>
            [Newtonsoft.Json.JsonProperty("CompileTime", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string CompileTime { get; set; }

            /// <summary>Compilation date (e.g.: "Feb 12 2020")</summary>
            [Newtonsoft.Json.JsonProperty("CompileDate", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string CompileDate { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }
        }
      
        public class Serial_Port
        {
            [Newtonsoft.Json.JsonProperty("Interface", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Interface { get; set; } = "485";

            [Newtonsoft.Json.JsonProperty("BaudRate", Required = Newtonsoft.Json.Required.Always)]
            public int BaudRate { get; set; }

            [Newtonsoft.Json.JsonProperty("DataBits", Required = Newtonsoft.Json.Required.Always)]
            public int DataBits { get; set; }

            [Newtonsoft.Json.JsonProperty("Parity", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Parity { get; set; } = "None";

            [Newtonsoft.Json.JsonProperty("StopBits", Required = Newtonsoft.Json.Required.Always)]
            public int StopBits { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }


        }
     
        public class Protocol
        {
            /*
            [System.Runtime.Serialization.EnumMember(Value = "UDP")]
            UDP = 0,
            [System.Runtime.Serialization.EnumMember(Value = "TCP")]
            TCP = 1,
            [System.Runtime.Serialization.EnumMember(Value = "ModbusTCP")]
            ModbusTCP = 2,
        */

            [Newtonsoft.Json.JsonProperty("Protocol", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Required(AllowEmptyStrings = true)]
            public string Protocol1 { get; set; } = "UDP";

            /// <summary>Port 80 is reseverd for internal HTTP server</summary>
            [Newtonsoft.Json.JsonProperty("Port", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Range(1, 65535)]
            public int Port { get; set; }

            /// <summary>Packing/RTU timeout</summary>
            [Newtonsoft.Json.JsonProperty("Timeout", Required = Newtonsoft.Json.Required.Always)]
            [System.ComponentModel.DataAnnotations.Range(10, 5000)]
            public int Timeout { get; set; }

            /// <summary>TX delay of the ModbusTCP protocol. It's required when the protocol is set to ModbusTCP.</summary>
            [Newtonsoft.Json.JsonProperty("TXDelay", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            [System.ComponentModel.DataAnnotations.Range(0, 500)]
            public int TXDelay { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }
        }
      
        public class Firmware
        {
            [Newtonsoft.Json.JsonProperty("ESP32", Required = Newtonsoft.Json.Required.DisallowNull, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string ESP32 { get; set; }

            private System.Collections.Generic.IDictionary<string, object> _additionalProperties = new System.Collections.Generic.Dictionary<string, object>();

            [Newtonsoft.Json.JsonExtensionData]
            public System.Collections.Generic.IDictionary<string, object> AdditionalProperties
            {
                get { return _additionalProperties; }
                set { _additionalProperties = value; }
            }
        }             

        public class FileParameter
        {
            public FileParameter(System.IO.Stream data)
                : this(data, null)
            {
            }

            public FileParameter(System.IO.Stream data, string fileName)
                : this(data, fileName, null)
            {
            }

            public FileParameter(System.IO.Stream data, string fileName, string contentType)
            {
                Data = data;
                FileName = fileName;
                ContentType = contentType;
            }

            public System.IO.Stream Data { get; private set; }

            public string FileName { get; private set; }

            public string ContentType { get; private set; }
        }
    }
}
