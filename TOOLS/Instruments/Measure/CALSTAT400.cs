﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.SCPI;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Threading.Tasks;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.00)]
    public class CALSTAT400 : MeasureBase<CALSTAT400>, IDisposable, IConsoleDevice
    {
        private SerialPort sp;
        private byte _port = 4;
        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        public CALSTAT400SCPI SCPI { get { return new CALSTAT400SCPI(this); } }

        public int WaitTimeMeasure { get; set; }

        public string StatusQuestEven { get; internal set; }

        public class MeasureData
        {
            public MeasureData(TriLineValue t)
            {
                Voltage = t;
                Current = t;
                Desfase = t;
                PowerActive = t;
                PowerReactive = t;
                PowerAparent = t;
                PowerFactor = 0;
                Phases = t;
                Freq = 0;
                SumPowerActive = 0;
                SumPowerReactive = 0;
                SumPowerAparent = 0;
            }

            public TriLineValue Voltage { get; set; }
            public TriLineValue Current { get; set; }
            public TriLineValue Desfase { get; set; }
            public TriLineValue PowerActive { get; set; }
            public TriLineValue PowerReactive { get; set; }
            public TriLineValue PowerAparent { get; set; }
            public double PowerFactor { get; set; }
            public TriLineValue Phases { get; set; }
            public TriLineValue PF { get; set; }
            public double Freq { get; set; }
            public double SumPowerActive { get; set; }
            public double SumPowerReactive { get; set; }
            public double SumPowerAparent { get; set; }
        }

        internal MeasureData DeviceData { get; set; }

        public enum PrivateCommands
        {
            KLOC = Commands.USER_DEFINED_COMMANDS + 1,
            ENBTRANS = Commands.USER_DEFINED_COMMANDS + 2,
            ENBINPCURRHIGHT = Commands.USER_DEFINED_COMMANDS + 3,
            ENBSTATQUEST = Commands.USER_DEFINED_COMMANDS + 4,
            STATPROBEENAB = Commands.USER_DEFINED_COMMANDS + 5,
            ENABLESRE = Commands.USER_DEFINED_COMMANDS + 6,
            OUTFLAG = Commands.USER_DEFINED_COMMANDS + 7,
            STATQUESTEVENT = Commands.USER_DEFINED_COMMANDS + 8,
            OUTPFREQTYP = Commands.USER_DEFINED_COMMANDS + 9,
            OUTPFREQSIGN = Commands.USER_DEFINED_COMMANDS + 10,
            CONFTYPEMEASURE = Commands.USER_DEFINED_COMMANDS + 11,
            ENBCURRAUTORANG = Commands.USER_DEFINED_COMMANDS + 12,
        }

        public Dictionary<PrivateCommands, string> commandStrings = new Dictionary<PrivateCommands, string>()
        {
            {PrivateCommands.KLOC, ":SYST:KLOC {0}"},
            {PrivateCommands.ENBTRANS, ":CONF:TRAN:ENAB {0}, {1}"},
            {PrivateCommands.CONFTYPEMEASURE, ":CONF:COND {0}"},
            {PrivateCommands.ENBINPCURRHIGHT, ":INP:CURR:COUP {0}A"},
            {PrivateCommands.ENBCURRAUTORANG, ":SENS:CURR:RANG {0}"},
            {PrivateCommands.ENBSTATQUEST, ":STAT:QUEST:ENAB {0}"},
            {PrivateCommands.STATPROBEENAB, ":STAT:PROB{0}:ENAB {1}"},
            {PrivateCommands.ENABLESRE, "*SRE {0}"},
            {PrivateCommands.OUTFLAG, "*OUTP:FLAG {0}"},
            {PrivateCommands.STATQUESTEVENT, ":STAT:QUEST:EVEN?"},
            {PrivateCommands.OUTPFREQTYP, ":OUTP:FREQ:TYP {0}"},
            {PrivateCommands.OUTPFREQSIGN, ":OUTP:FREQ:SIGN  {0}"},
        };

        public enum Variables
        {
            CURRENT,
            VOLTAGE,
            ACTIVE_POWER,
            REACTIVE_POWER,
            APARENT_POWER,
            ACTIVE_SUM_POWER,
            REACTIVE_SUM_POWER,
            APARENT_SUM_POWER,
            POWER_FACTOR,
            VOLTAGE_BETWEEN_PHASES,
            FREQUENCY,
            PHASE_ANGLES,
        };

        private readonly Dictionary<int, string> messages = new Dictionary<int, string>
        {
            { (int)Variables.CURRENT, ":READ:CURR:AMPL? ALL" },
            { (int)Variables.VOLTAGE, ":READ:VOLT:AMPL? ALL" },
            { (int)Variables.ACTIVE_POWER, ":READ:POW:ACT? ALL" },
            { (int)Variables.REACTIVE_POWER, ":READ:POW:REAC? ALL" },
            { (int)Variables.APARENT_POWER, ":READ:POW:APP? ALL" },
            { (int)Variables.ACTIVE_SUM_POWER, ":READ:POW:ACT? SUM" },
            { (int)Variables.REACTIVE_SUM_POWER, ":READ:POW:REAC? SUM" },
            { (int)Variables.APARENT_SUM_POWER, ":READ:POW:APP? SUM" },
            { (int)Variables.POWER_FACTOR, ":READ:COS?" },
            { (int)Variables.FREQUENCY, ":READ:FREQ?" },
            { (int)Variables.PHASE_ANGLES, ":READ:ANGL? ALL" },
        };


        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumento port a byte");

            EnableInputCurrentHigh(false);
        }

        public CALSTAT400()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinTx = "\n", CaracterFinRx = "\n" };
            var spAdapter = new SerialPortAdapter(sp, "\n");
            transport = new MessageTransport(protocol, spAdapter, _logger);

            DeviceData = new MeasureData(TriLineValue.Create(0, 0, 0));
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + _port;
            }
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.BaudRate = 38400;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();

            WaitTimeMeasure = 1000;
        }

        public void StatusPresetEnebled()
        {
            SCPI.StatusPresetEnebled.Command();
        }

        public void Reset()
        {
            GetOperationComplete();
            Clear();
            SCPI.Reset.Command();
        }

        public void Clear()
        {
            SCPI.Clear.Command();
        }

        public string GetOperationComplete()
        {
            var result = SCPI.OperationCompleteQuery.Query();
            return result.Value;
        }

        public string GetIdentification()
        {
            var read = SCPI.Identification.Query();
            return read.Value;
        }

        public string GetStatusByte()
        {
            var read = SCPI.StatusByte.Query();
            return read.Value;
        }

        //*********************************************

        public void KLOC()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.KLOC, true, 1));
        }

        public void EnabledTransformationVoltage(bool status)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ENBTRANS, true, "U", status ? "on" : "off" ));
        }
        public void EnabledTransformationCurrent(bool status)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ENBTRANS, true, "I", status ? "on" : "off"));
        }

        public void EnableInputCurrentHigh(bool status)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ENBINPCURRHIGHT, true, status ? "100" : "10"));
        }

        public void EnableStatQuest()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ENBSTATQUEST, true, "3"));
        }

        public void EnableStatProbe(byte channel, int registerEvent)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.STATPROBEENAB, true, channel, registerEvent));
        }

        public void EnableSRE(int registerEvent)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ENABLESRE, true, registerEvent));
        }

        public void OutFlag(int digitalOutput)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.OUTFLAG, true, digitalOutput));
        }

        public string StatQuestEven()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.STATQUESTEVENT, true));
            return StatusQuestEven;
        }

        public enum typeMeasure
        {
            _3L,
            _4L
        }

        public void ConfTypeMeasure(typeMeasure typeMeasure)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.CONFTYPEMEASURE, true, typeMeasure == typeMeasure._3L ? "3L " : "4L"));
        }

        //**************************************************************
        //**************************************************************

        public TriLineValue ReadVoltage()
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.VOLTAGE));
            return DeviceData.Voltage;
        }

        public TriLineValue ReadPhase()
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.PHASE_ANGLES));
            return DeviceData.Phases;
        }

        public TriLineValue ReadActivePower()
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.ACTIVE_POWER));
            return DeviceData.PowerActive;
        }

        public TriLineValue ReadReactivePower()
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.REACTIVE_POWER));
            return DeviceData.PowerReactive;
        }

        public TriLineValue ReadAparentPower()
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.APARENT_POWER));
            return DeviceData.PowerAparent;
        }

        public double ReadPowerFactor()
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.POWER_FACTOR));
            return DeviceData.PowerFactor;
        }

        public MeasureData ReadAll()
        {           
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.VOLTAGE));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.CURRENT));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.FREQUENCY));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.ACTIVE_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.APARENT_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.POWER_FACTOR));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.REACTIVE_POWER));
            //SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.VOLTAGE_BETWEEN_PHASES));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.PHASE_ANGLES));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.POWER_FACTOR));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.REACTIVE_SUM_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.APARENT_SUM_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.ACTIVE_SUM_POWER));
            return DeviceData;
        }

        public Task<MeasureData> ReadAllAsync(bool? throwException = null)
        {
            return Task.Factory.StartNew<MeasureData>(() => { return ReadAll(); }, TaskCreationOptions.AttachedToParent);
        }

        public Task<TriLineValue> ReadActivePowerAsync(bool? throwException = null)
        {
            return Task.Factory.StartNew<TriLineValue>(() => { return ReadActivePower(); }, TaskCreationOptions.AttachedToParent);
        }

        public Task<TriLineValue> ReadReactivePowerAsync(bool? throwException = null)
        {
            return Task.Factory.StartNew<TriLineValue>(() => { return ReadReactivePower(); }, TaskCreationOptions.AttachedToParent);
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            switch (command.Commands)
            {
                case (int)PrivateCommands.KLOC:
                case (int)PrivateCommands.ENBTRANS:
                case (int)PrivateCommands.ENBINPCURRHIGHT:
                case (int)PrivateCommands.ENBSTATQUEST:
                case (int)PrivateCommands.STATPROBEENAB:
                case (int)PrivateCommands.ENABLESRE:
                case (int)PrivateCommands.OUTFLAG:
                case (int)PrivateCommands.ENBCURRAUTORANG:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    response = null;
                    break;
                case (int)PrivateCommands.STATQUESTEVENT:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands]);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    StatusQuestEven = response.Text;
                    break;
                case (int)Commands.READ_VARIABLE:

                    int variable = (int)command.Args[0];
                    message = new StringMessage { Text = messages[variable] };
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    switch (variable)
                    {
                        case (int)Variables.VOLTAGE:
                            DeviceData.Voltage = TriLineValue.CreateFromString(GetFormattedText(response.Text));
                            break;
                        case (int)Variables.CURRENT:
                            DeviceData.Current = TriLineValue.CreateFromString(GetFormattedText(response.Text));
                            break;
                        case (int)Variables.ACTIVE_POWER:
                            DeviceData.PowerActive = TriLineValue.CreateFromString(GetFormattedText(response.Text));
                            break;
                        case (int)Variables.REACTIVE_POWER:
                            DeviceData.PowerReactive = TriLineValue.CreateFromString(GetFormattedText(response.Text));
                            break;
                        case (int)Variables.APARENT_POWER:
                            DeviceData.PowerAparent = TriLineValue.CreateFromString(GetFormattedText(response.Text));
                            break;
                        case (int)Variables.FREQUENCY:
                            DeviceData.Freq = Convert.ToDouble(GetFormattedText(response.Text), enUS);
                            break;
                        case (int)Variables.ACTIVE_SUM_POWER:
                            DeviceData.SumPowerActive = Convert.ToDouble(GetFormattedText(response.Text), enUS);
                            break;
                        case (int)Variables.REACTIVE_SUM_POWER:
                            DeviceData.SumPowerReactive = Convert.ToDouble(GetFormattedText(response.Text), enUS);
                            break;
                        case (int)Variables.APARENT_SUM_POWER:
                            DeviceData.SumPowerAparent = Convert.ToDouble(GetFormattedText(response.Text), enUS);
                            break;
                        case (int)Variables.POWER_FACTOR:
                            DeviceData.PowerFactor = Convert.ToDouble(GetFormattedText(response.Text));
                            break;
                        case (int)Variables.PHASE_ANGLES:
                            DeviceData.Phases = TriLineValue.CreateFromString(GetFormattedText(response.Text));
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                    break;
                default:
                    throw new NotImplementedException("Comando inexistente o no implementado");

            }
        }

        public string GetFormattedText(string Text)
        {
            return Text.Replace("\r", "").Replace("-nan", "1"); ;
        }

        public void Dispose()
        {
            if (sp != null && sp.IsOpen)
                sp.Close();
        }

        public Result<string> WriteCommand(string text)
        {
            try
            {
                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                response = transport.SendMessage<SCPIMessage>(new SCPIMessage { Text = "*ESR?" });
                int result = Convert.ToInt32(response.Text); 

                return string.Empty;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class CALSTAT400SCPI : SCPIDevice
    {
        internal CALSTAT400SCPI(CALSTAT400 device)
            : base(device)
        {
        }
        public SCPICommand StatusPresetEnebled { get { return new SCPICommand(context, "STATus:PRESet"); } } // Preset enable registers and transition filters for operation and questionable status structures.
    }
}
