﻿using System;

namespace Dezac.Tests
{
    public class TestVersionAttribute : Attribute
    {
        public double Version { get; set; }

        public TestVersionAttribute()
        {

        }

        public TestVersionAttribute(double version)
        {
            Version = version;
        }
    }
}
