﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System.Collections.Generic;

namespace Dezac.Tests.Extensions
{
    public static class TestMeasureWithAnalizerExtension
    {
        public struct TestPointConfiguration
        {
            public TowerAnalizer AnalizerSelected;
            public Magnitud magnitudSelected;
            public string ResultName;
            public int WaitTimeReadMeasure;
            public int TiempoIntervalo;
            public int Iteraciones;
        }

        public enum TowerAnalizer
        {
            CVMB100Hi,
            CVMB100Low,
            CVMMiniMTE,
            CVMMiniCHROMA,
            MTE_PRS
        }

        public enum Magnitud
        {
            Voltage,
            Current,
            PotActiva,
            PotReactiva,
            PF
        }

        public static void TestMeasureWithAnalizer(this TestBase testBase, ITower tower, TestPointConfiguration tespoint)
        {
            var defs = new List<AdjustValueDef>();
            switch (tespoint.magnitudSelected)
            {
                case Magnitud.Voltage:
                    defs.Add(new AdjustValueDef(testBase.Params.V.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.V.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.V.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.V));
                    defs.Add(new AdjustValueDef(testBase.Params.V.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.V.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.V.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.V));
                    defs.Add(new AdjustValueDef(testBase.Params.V.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.V.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.V.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.V));
                    break;
                case Magnitud.Current:
                    defs.Add(new AdjustValueDef(testBase.Params.I.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.I.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.I.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.mA));
                    defs.Add(new AdjustValueDef(testBase.Params.I.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.I.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.I.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.mA));
                    defs.Add(new AdjustValueDef(testBase.Params.I.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.I.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.I.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.mA));
                    break;
                case Magnitud.PotActiva:
                    defs.Add(new AdjustValueDef(testBase.Params.KW.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.KW.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.KW.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.W));
                    defs.Add(new AdjustValueDef(testBase.Params.KW.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.KW.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.KW.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.W));
                    defs.Add(new AdjustValueDef(testBase.Params.KW.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.KW.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.KW.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.W));
                    break;
                case Magnitud.PotReactiva:
                    defs.Add(new AdjustValueDef(testBase.Params.KVAR.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.KVAR.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.KVAR.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.Kvar));
                    defs.Add(new AdjustValueDef(testBase.Params.KVAR.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.KVAR.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.KVAR.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.Kvar));
                    defs.Add(new AdjustValueDef(testBase.Params.KVAR.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.KVAR.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.KVAR.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.Kvar));
                    break;
                case Magnitud.PF:
                    defs.Add(new AdjustValueDef(testBase.Params.PF.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.PF.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.PF.L1.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.SinUnidad));
                    defs.Add(new AdjustValueDef(testBase.Params.PF.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.PF.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.PF.L2.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.SinUnidad));
                    defs.Add(new AdjustValueDef(testBase.Params.PF.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Name, 0, testBase.Params.PF.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Min(), testBase.Params.PF.L3.TestPoint(tespoint.AnalizerSelected.ToString()).TestPoint(tespoint.ResultName).Max(), 0, 0, ParamUnidad.SinUnidad));
                    break;
            }

            testBase.TestMeasureBase(defs,
              (step) =>
              {
                  var result = new List<double>();

                  TriLineValue voltage = new TriLineValue();
                  TriLineValue current = new TriLineValue();
                  TriLineValue potActiiva = new TriLineValue();
                  TriLineValue potReactiiva = new TriLineValue();
                  TriLineValue PF = new TriLineValue();

                  testBase.Delay(tespoint.WaitTimeReadMeasure, "Tiempo de espera en entes de medir");

                  switch (tespoint.AnalizerSelected)
                  {
                      case TowerAnalizer.CVMB100Low:
                      case TowerAnalizer.CVMB100Hi:
                          var measure = tespoint.AnalizerSelected == TowerAnalizer.CVMB100Low ? tower.CvmB100Low.ReadVariablesInstantaneas() : tower.CvmB100Hi.ReadVariablesInstantaneas();
                          voltage = TriLineValue.Create(measure.L1.Tension, measure.L2.Tension, measure.L3.Tension);
                          current = TriLineValue.Create(measure.L1.Corriente, measure.L2.Corriente, measure.L3.Corriente);
                          potActiiva = TriLineValue.Create(measure.L1.PotenciaActiva, measure.L2.PotenciaActiva, measure.L3.PotenciaActiva);
                          potReactiiva = TriLineValue.Create(measure.L1.PotenciaReactivaInductiva, measure.L2.PotenciaReactivaInductiva, measure.L3.PotenciaReactivaInductiva);
                          PF = TriLineValue.Create(measure.L1.FactorPotencia, measure.L2.FactorPotencia, measure.L3.FactorPotencia);
                          break;
                      case TowerAnalizer.CVMMiniCHROMA:
                      case TowerAnalizer.CVMMiniMTE:
                          var medida = tespoint.AnalizerSelected == TowerAnalizer.CVMMiniCHROMA ? tower.CvmminiChroma.ReadAllVariables().Phases : tower.CvmminiMTE.ReadAllVariables().Phases;
                          voltage = TriLineValue.Create(medida.L1.Tension, medida.L2.Tension, medida.L3.Tension);
                          current = TriLineValue.Create(medida.L1.Corriente, medida.L2.Corriente, medida.L3.Corriente);
                          potActiiva = TriLineValue.Create(medida.L1.PotenciaActiva, medida.L2.PotenciaActiva, medida.L3.PotenciaActiva);
                          potReactiiva = TriLineValue.Create(medida.L1.PotenciaReactiva, medida.L2.PotenciaReactiva, medida.L3.PotenciaReactiva);
                          PF = TriLineValue.Create(medida.L1.FactorPotencia, medida.L2.FactorPotencia, medida.L3.FactorPotencia);
                          break;
                      case TowerAnalizer.MTE_PRS:
                          var voltageWatt = tower.MTEWAT.ReadAll();
                          voltage = TriLineValue.Create(voltageWatt.Voltage.L1, voltageWatt.Voltage.L2, voltageWatt.Voltage.L3);
                          current = TriLineValue.Create(voltageWatt.Current.L1, voltageWatt.Current.L2, voltageWatt.Current.L3);
                          potActiiva = TriLineValue.Create(voltageWatt.PowerActive.L1, voltageWatt.PowerActive.L2, voltageWatt.PowerActive.L3);
                          potReactiiva = TriLineValue.Create(voltageWatt.PowerReactive.L1, voltageWatt.PowerReactive.L2, voltageWatt.PowerReactive.L3);
                          PF = TriLineValue.Create(voltageWatt.PowerFactor.L1, voltageWatt.PowerFactor.L2, voltageWatt.PowerFactor.L3);
                          break;
                  }

                  switch (tespoint.magnitudSelected)
                  {
                      case Magnitud.Voltage:
                          result.Add(voltage.ToArray());
                          break;
                      case Magnitud.Current:
                          result.Add(current.ToArray());
                          break;
                      case Magnitud.PotActiva:
                          result.Add(potActiiva.ToArray());
                          break;
                      case Magnitud.PotReactiva:
                          result.Add(potReactiiva.ToArray());
                          break;
                      case Magnitud.PF:
                          result.Add(PF.ToArray());
                          break;
                  }

                  return result.ToArray();

              },0, tespoint.Iteraciones, tespoint.TiempoIntervalo);
        }  
    }
}
