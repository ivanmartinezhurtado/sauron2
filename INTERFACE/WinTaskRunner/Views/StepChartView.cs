﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using TaskRunner.Model;
using WinTaskRunner.Services;
using System.Threading;
using TaskRunner.Enumerators;

namespace WinTaskRunner.Views
{
    public partial class StepChartView : UserControl
    {
        protected readonly AppService app;

        private SynchronizationContext sc;
        private bool alternate;

        public StepChartView()
        {
            InitializeComponent();
        }

        public StepChartView(AppService appService)
            : this()
        {
            app = appService;

            sc = SynchronizationContext.Current;

            app.Runner.AllTestStarting += (s, e) => { InitChart(); };
            //this.app.Runner.ActionExecuted += (s, e) => { sc.Post(_ => { AddPoint(e); }, null); };
        }

        public void InitChart()
        {
            Series series = chart1.Series[0];
            series.YValuesPerPoint = 2;
            series.CustomProperties = "DrawSideBySide=false";

            series.Points.Clear();
            series.XValueType = ChartValueType.Auto;
            series.YValueType = ChartValueType.Auto;

            ChartArea chartArea = chart1.ChartAreas[0];

            chartArea.AxisY.Minimum = DateTime.Now.ToOADate();
            chartArea.AxisY.LabelStyle.Format = "HH:mm:ss";
            chartArea.AxisY.IntervalType = DateTimeIntervalType.Milliseconds;

            chartArea.CursorY.IsUserEnabled = true;
            chartArea.CursorY.IsUserSelectionEnabled = true;
            chartArea.CursorY.IntervalType = DateTimeIntervalType.Milliseconds;
            chartArea.CursorY.Interval = 5;
            chartArea.CursorY.AutoScroll = true;
            chartArea.AxisY.ScaleView.Zoomable = true;
            chartArea.AxisY.ScrollBar.IsPositionedInside = true;
            chartArea.AxisY.ScaleView.SmallScrollMinSizeType = DateTimeIntervalType.Seconds;
            chartArea.AxisY.ScaleView.SmallScrollSizeType = DateTimeIntervalType.Seconds;
        }

        public void AddPoint(StepResult stepResult)
        {
            try
            {
                Series series = chart1.Series[0];

                int pos = series.Points.AddXY(stepResult.ThreadId, stepResult.StartTime, stepResult.EndTime);
                DataPoint point = series.Points[pos];
                point.Label = stepResult.Step.Name;
                //point.AxisLabel =
                if (stepResult.ExecutionResult == StepExecutionResult.Passed)
                {
                    point.Color = alternate ? Color.Blue : Color.LightBlue;
                    alternate = !alternate;
                }
                else
                    point.Color = Color.Red;

                point.BorderColor = Color.Black;
                point.ToolTip = string.Format("{0} ({1}ms)", stepResult.Step.Name, stepResult.Duration.TotalMilliseconds);
            }
            catch (Exception)
            {
                // Log
            }
        }
    }
}
