namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_FAMILIA
    {
        public T_FAMILIA()
        {
            this.PRODUCTO = new HashSet<PRODUCTO>();
            this.T_FAMILIAPARAM = new HashSet<T_FAMILIAPARAM>();
            this.T_GRUPOPARAMETRIZACION = new HashSet<T_GRUPOPARAMETRIZACION>();
            this.T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
            this.T_TESTSEQUENCEFILE = new HashSet<T_TESTSEQUENCEFILE>();
            this.T_TEST = new HashSet<T_TEST>();
        }
    
        public int NUMFAMILIA { get; set; }
        public string FAMILIA { get; set; }
        public string DESCRIPCION { get; set; }
        public string BLOQUEOTEST { get; set; }
        public string PLATAFORMA { get; set; }
        public string IDEMPLEADO { get; set; }
    
        public virtual ICollection<PRODUCTO> PRODUCTO { get; set; }
        public virtual ICollection<T_FAMILIAPARAM> T_FAMILIAPARAM { get; set; }
        public virtual ICollection<T_GRUPOPARAMETRIZACION> T_GRUPOPARAMETRIZACION { get; set; }
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }
        public virtual ICollection<T_TESTSEQUENCEFILE> T_TESTSEQUENCEFILE { get; set; }
        public virtual ICollection<T_TEST> T_TEST { get; set; }
    }
}
