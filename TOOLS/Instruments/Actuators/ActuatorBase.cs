﻿using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using log4net;
using System;

namespace Instruments.Actuators
{
    public abstract class ActuatorBase<T>
    {
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(T));

        public bool ThrowException { get; set; }

        protected MessageTransport transport;

        protected IProtocol protocol;

        protected bool SendCommand<T>(Command<T> command)
        {
            try
            {
                _logger.InfoFormat("Command {0}", command);
                ExecuteCommand(command, 3);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                command.Exception = ex;
                if (command.ThrowException.GetValueOrDefault(ThrowException))
                    throw;

                return false;
            }
        }

        private void ExecuteCommand<T>(Command<T> command, int numRetries)
        {
            bool error = false;
            do
            {
                try
                {
                    error = false;
                    InternalSendCommand(command);
                }
                catch (Exception ex)
                {
                    error = true;
                    numRetries--;

                    _logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                    if (numRetries <= 0 || ex is NotSupportedException)
                        throw;
                }
            } while (error);
        }

        protected abstract void InternalSendCommand<T>(Command<T> command);


        public enum Commands
        {
            RESET,
            READ_VARIABLE,
            READ_STATUS,
            READ_IDN,

            USER_DEFINED_COMMANDS = 9999
        };
    }
}
