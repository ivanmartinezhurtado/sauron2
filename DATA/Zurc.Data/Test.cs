namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Test")]
    public partial class Test
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime FechaAlta { get; set; }

        [StringLength(1)]
        public string Resultado { get; set; }

        public int? OrdenId { get; set; }
        [StringLength(100)]
        public string Lote { get; set; }

        //[ForeignKey("Producto")]
        //public int? ProductoId { get; set; }
        public int? ArticuloId { get; set; }
        public int? Version { get; set; }
        public int? FamiliaId { get; set; }
        public int? MatriculaId { get; set; }
        [StringLength(20)]
        public string NumSerie { get; set; }
        public int? ReportVersion { get; set; }

        //public virtual Producto Producto { get; set; }

        public virtual List<TestFase> Fases { get; set; }
    }
}
