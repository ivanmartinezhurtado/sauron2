﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Layouts;
using System.Drawing;

namespace WinTestPlayer.Controls.Expanders
{
    public class ExpanderElement : RadItem
    {
        protected LightVisualElement staticColumn;
        protected LightVisualElement slidingColumn;
        Expander control;

        AnimatedPropertySetting expandingAnimation;
        AnimatedPropertySetting collapsingAnimation;

        bool isExpanded = true;

        DockLayoutPanel columnsHolder;
        protected DockLayoutPanel chevronAndContentHolder;

        ChevronButton chevronButton;

        public bool AllowExpand { get; set; }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            staticColumn = new LightVisualElement();
            staticColumn.ThemeRole = "StaticColumn";

            slidingColumn = new LightVisualElement();
            slidingColumn.ThemeRole = "SlidingColumn";

            chevronButton = new ChevronButton();
            chevronButton.ThemeRole = "ChevronButton";

            columnsHolder = new DockLayoutPanel();
            chevronAndContentHolder = new DockLayoutPanel();

            columnsHolder.Children.Add(staticColumn);
            columnsHolder.Children.Add(slidingColumn);
            DockLayoutPanel.SetDock(staticColumn, Dock.Left);

            staticColumn.Children.Add(chevronAndContentHolder);
            chevronAndContentHolder.Children.Add(chevronButton);
            DockLayoutPanel.SetDock(chevronButton, Dock.Top);

            Children.Add(columnsHolder);

            chevronButton.Click += new EventHandler(chevronButton_Click);
        }

        ExpanderOrientation expanderOrientation;

        public ExpanderOrientation Orientation
        {
            get
            {
                return expanderOrientation;
            }
            set
            {
                expanderOrientation = value;
                OnNotifyPropertyChanged("Orientation");
            }
        }

        protected override void OnNotifyPropertyChanged(string propertyName)
        {
            base.OnNotifyPropertyChanged(propertyName);

            if (propertyName == "Orientation")
            {
                if (Orientation == ExpanderOrientation.Left)
                {
                    DockLayoutPanel.SetDock(staticColumn, Dock.Left);
                    colAnim = null;
                    expAnim = null;
                }
                else
                {
                    DockLayoutPanel.SetDock(staticColumn, Dock.Right);
                    colAnim = null;
                    expAnim = null;
                }
            }
        }

        public bool IsExpanded
        {
            get
            {
                return isExpanded;
            }
            set
            {
                isExpanded = value;
            }
        }

        public void Expand()
        {
            //if (IsExpanded)
            //{
            //    return;
            //}

            ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width + slidingColumnWidth, ElementTree.Control.Size.Height);

            slidingColumn.ResetValue(RadElement.PositionOffsetProperty, ValueResetFlags.All);

            if (Orientation == ExpanderOrientation.Left)
                slidingColumn.PositionOffset = new SizeF(+slidingColumn.BoundingRectangle.Width, 0);
            else
                slidingColumn.PositionOffset = SizeF.Empty;

            chevronButton.Expanded = true;
            IsExpanded = true;
        }

        public void Collapse()
        {
            //if (!IsExpanded)
            //{
            //    return;
            //}

            slidingColumn.ResetValue(RadElement.PositionOffsetProperty, ValueResetFlags.All);

            if (Orientation == ExpanderOrientation.Left)
                slidingColumn.PositionOffset = new SizeF(-slidingColumn.BoundingRectangle.Width, 0);
            else
                slidingColumn.PositionOffset = new SizeF(+slidingColumn.BoundingRectangle.Width, 0);

            ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width, ElementTree.Control.Size.Height);

            chevronButton.Expanded = false;
            IsExpanded = false;
        }

        protected override void OnElementTreeChanged(ComponentThemableElementTree previousTree)
        {
            base.OnElementTreeChanged(previousTree);

            control = (Expander)ElementTree.Control;
        }

        //int slidingColumnWidth = 0;

        void collapsingAnimation_AnimationFinished(object sender, AnimationStatusEventArgs e)
        {
            ((AnimatedPropertySetting)sender).AnimationFinished -= new AnimationFinishedEventHandler(collapsingAnimation_AnimationFinished);

            //slidingColumnWidth = slidingColumn.ControlBoundingRectangle.Width;
            ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width, ElementTree.Control.Size.Height);

            chevronButton.Expanded = false;
            IsExpanded = false;
        }

        void expandingAnimation_AnimationFinished(object sender, AnimationStatusEventArgs e)
        {
            ((AnimatedPropertySetting)sender).AnimationFinished -= new AnimationFinishedEventHandler(expandingAnimation_AnimationFinished);

            chevronButton.Expanded = true;
            IsExpanded = true;
        }

        AnimatedPropertySetting colAnim;
        AnimatedPropertySetting expAnim;
        int frames = 20;
        int interval = 10;
        int slidingColumnWidth = 230;

        protected virtual AnimatedPropertySetting CreateCollapsingAnimation()
        {
            if (Orientation == ExpanderOrientation.Left)
            {
                if (colAnim == null)
                    colAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        SizeF.Empty,
                        new SizeF(-slidingColumn.BoundingRectangle.Width, 0),
                        frames, interval);
            }
            else
            {
                if (colAnim == null)
                    colAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        SizeF.Empty,
                        new SizeF(+slidingColumn.BoundingRectangle.Width, 0),
                        frames, interval);
            }

            return colAnim;
        }

        protected virtual AnimatedPropertySetting CreateExpandingAnimation()
        {
            if (Orientation == ExpanderOrientation.Left)
            {
                if (expAnim == null)
                    expAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        new SizeF(-slidingColumn.BoundingRectangle.Width, 0),
                        SizeF.Empty,
                        frames, interval);
            }
            else
            {
                if (expAnim == null)
                    expAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        new SizeF(+slidingColumn.BoundingRectangle.Width, 0),
                        SizeF.Empty,
                        frames, interval);
            }

            return expAnim;
        }

        protected virtual void OnButtonClicked()
        {
            if (IsExpanded)
            {
                collapsingAnimation = CreateCollapsingAnimation();

                collapsingAnimation.AnimationFinished += new AnimationFinishedEventHandler(collapsingAnimation_AnimationFinished);

                collapsingAnimation.ApplyValue(slidingColumn);
            }
            else
            {
                ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width + slidingColumnWidth, ElementTree.Control.Size.Height);

                expandingAnimation = CreateExpandingAnimation();

                expandingAnimation.AnimationFinished += new AnimationFinishedEventHandler(expandingAnimation_AnimationFinished);

                expandingAnimation.ApplyValue(slidingColumn);
            }
        }

        void chevronButton_Click(object sender, EventArgs e)
        {
            if (AllowExpand)
                OnButtonClicked();
        }
    }
}
