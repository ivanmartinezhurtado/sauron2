﻿using Comunications.Utility;
using Dezac.Device;
using System;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace Instruments.Measure
{
    [DeviceVersion(1.00)]
    public class CVMK2Tower : DeviceBase
    {

        public CVMK2Tower()
        {
        }

        public CVMK2Tower(string port)
        {
            SetPort(port);
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
            {
                if (Modbus == null)
                    Modbus = new ModbusDeviceSerialPort(result, 9600, Parity.None, StopBits.One, _logger);

                Modbus.PortCom = result;
            }
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }


        public CVMK2Tower(SerialPort SerialPort)
        {
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public AllVariables ReadAllVariables()
        {
            var Fases = Modbus.Read<VariablesInstantaneas>();
            Fases.L1.Tension /= 10;
            Fases.L2.Tension /= 10;
            Fases.L3.Tension /= 10;

            var Trifasicas = Modbus.Read<VariablesTrifasicas>();

            AllVariables All = new AllVariables();
            All.Fases = Fases;
            All.Trifasicas = Trifasicas;

            return All;
        }

        public void Write(Registers register, ushort value)
        {
           Modbus.Write((ushort)register, value);
        }

        public void FlagTest(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void Reset()
        {
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

    
        public int ReadFrecuency()
        {
            var Freq = Modbus.ReadInt32((ushort)Registers.FRECUENCY);
            return (int)(Freq / 100);
        }


        #region Structures

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Fases;
            public VariablesTrifasicas Trifasicas;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            public Int32 Tension;
            public Int32 Corriente;
            public Int32 PotenciaActiva;
            public Int32 PotenciaReactiva;
            public Int32 FactorPotencia;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x001E)]
        public struct VariablesTrifasicas
        {
            public Int32 PotenciaActivaIII;
            public Int32 PotenciaInductivaIII;
            public Int32 PotenciaCapacitivaIII;
            public Int32 CosenoPhiIII;
            public Int32 FacttorPotenciaIII;
            public Int32 Frecuencia;
            public Int32 TensionLineaL1L2;
            public Int32 TensionLineaL2L3;
            public Int32 TensionLineaL3L1;      
        };
 
        #endregion

        public enum Registers
        {
            RESET = 2000,
            FLAG_TEST = 0x2AF8,
            FRECUENCY = 0x0034,
        };
    }
}
