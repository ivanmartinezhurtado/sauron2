﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;
using System.Globalization;
using System.Linq;

namespace Instruments.Auxiliars
{
    public abstract class InstrumentsAuxiliarBase
    {
        protected readonly ILog _logger = LogManager.GetLogger(typeof(InstrumentsAuxiliarBase));
    
        public class InstrumentsAuxiliarStatus
        {
            public bool IsBusy { get; set; }
            public bool HasWarning { get; set; }
            public bool HasError { get; set; }

            public bool IsOK { get { return !IsBusy && !HasWarning && !HasError; } }
        }

        internal InstrumentsAuxiliarStatus Status { get; set; }

        public double Tolerance { get; set; }

        public bool ThrowException { get; set; }

        protected MessageTransport transport;

        protected IProtocol protocol;

        public enum Commands
        {
            APPLY_CONFIG,
            APPLY_OFF,
            RESET,
            READ_STATUS,
            USER_DEFINED_COMMANDS = 9999
        };

        public InstrumentsAuxiliarBase()
        {
        }

        public InstrumentsAuxiliarBase(ILog logger)
        {
            Status = new InstrumentsAuxiliarStatus();
            Tolerance = 1;
            Logger = logger ?? _logger;
        }

        public virtual void Dispose()
        {
        }

        protected ILog Logger { get; set; }

        public void ApplyOff(bool throwException = true)
        {
            var cmd = new Command<Commands>(Commands.APPLY_OFF, throwException);
            SendCommand(cmd);
        }

        public void ApplyOff()
        {
            ApplyOff(true);
        }

        public void Reset(bool throwException = true)
        {
            var cmd = new Command<Commands>(Commands.RESET, throwException);
            SendCommand(cmd);
        }

        public void Reset()
        {
            Reset(false);
        }

        public virtual Result<InstrumentsAuxiliarStatus> ReadStatus(bool throwException = true)
        {
            var cmd = new Command<Commands>(Commands.READ_STATUS, throwException);

            if (SendCommand(cmd))
                return new Result<InstrumentsAuxiliarStatus>(Status);

            return new Result<InstrumentsAuxiliarStatus>(cmd.Exception);
        }

        protected bool SendCommand<T>(Command<T> command)
        {
            try
            {
                Logger.InfoFormat("Command {0}", command);
                ExecuteCommand(command, 1);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                if (command.ThrowException.GetValueOrDefault(ThrowException))
                    throw new InstrumentsAuxiliarBaseException(InstrumentsAuxiliarBaseException.Codes.PRESETS_ERROR, ex);

                return false;
            }
        }

        private void ExecuteCommand<T>(Command<T> command, int numRetries)
        {
            bool error = false;
            do
            {
                try
                {
                    error = false;
                    InternalSendCommand(command);
                }
                catch (Exception ex)
                {
                    error = true;
                    numRetries--;

                    Logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                    if (numRetries <= 0 || ex is NotSupportedException)
                        throw;
                }
            } while (error);
        }

        protected abstract string InternalSendCommand<T>(Command<T> command);

    }
}
