﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    public class AdjustValueDefAvgTol : AdjustValueDefWrapper
    {
      
        [Description("Control variable name")]
        [CategoryAttribute("1.- Identification")]
        public new string Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        [Description("Control variable unit type")]
        [CategoryAttribute("1.- Identification")]
        public new ParamUnidad Unidad
        {
            get { return base.Unidad; }
            set { base.Unidad = value; }
        }

        [Description("Control variable Average margin")]
        [CategoryAttribute("2.- Margin Average and Tolerance")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Browsable(true)]
        public new string Average
        {
            get { return base.Average; }
            set { base.Average = value; }
        }

        [Description("Control variable Tolerance margin")]
        [CategoryAttribute("2.- Margin Average and Tolerance")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Browsable(true)]
        public new string Tolerance
        {
            get { return base.Tolerance; }
            set { base.Tolerance = value; }
        }

        [Description("Control variable Varianza margin")]
        [CategoryAttribute("3.- Margin Varianza")]
        [Browsable(true)]
        public new string MaxVarianza
        {
            get { return base.MaxVarianza; }
            set { base.MaxVarianza = value; }
        }

        [Description("Variable name to resolve value in list device result variables")]
        [CategoryAttribute("4.- Variable to load value from device")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public new string VarNamesDevice
        {
            get { return base.VarNamesDevice; }
            set { base.VarNamesDevice = value; }
        }

        [Description("Variable name to resolve value in list pattern result variables")]
        [CategoryAttribute("5.- Variable to load value from pattern")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public new string VarNamesPattern
        {
            get { return base.VarNamesPattern; }
            set { base.VarNamesPattern = value; }
        }
    }
}
