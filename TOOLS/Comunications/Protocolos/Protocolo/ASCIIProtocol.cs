﻿using Comunications.Message;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Comunications.Protocolo
{
    public class ASCIIProtocol : IProtocol
    {
        private string _CaracterFinTx;
        private string _CaracterFinRx;

        internal protected byte[] endBytesCharTx;
        internal protected byte[] endBytesCharRx;

       public string CaracterFinTx
        {
            get { return _CaracterFinTx; }
            set
            {
                _CaracterFinTx = value;
                endBytesCharTx = ASCIIEncoding.ASCII.GetBytes(_CaracterFinTx);
            }
        }

       public string CaracterFinRx
        {
            get { return _CaracterFinRx; }
            set
            {
                _CaracterFinRx = value;
                endBytesCharRx = ASCIIEncoding.ASCII.GetBytes(_CaracterFinRx);
            }
        }

        public virtual byte[] BuildMessage(ITransportMessage message)
        {
            byte[] data = message.MessageFrame;
            var size = data.Length + (endBytesCharTx == null ? 0 : endBytesCharTx.Length);
            byte[] frame = new byte[size];
            data.CopyTo(frame, 0);
            if (endBytesCharTx != null)
                endBytesCharTx.CopyTo(frame, data.Length);

            return frame;
        }

        public virtual byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            var data = new List<byte>();
            byte[] ch = new byte[1];
            int endMatch = 0;
            int lenghtOutRead = message.AnswerLen.HasValue ? message.AnswerLen.Value : endBytesCharRx.Length;
            var timeOut = new Stopwatch();
            timeOut.Start();
            do
            {
                int p = streamResource.Read(ch, 0, 1);
                if (p > 0)
                {
                    byte b = ch[0];
                    data.Add(b);

                    if (message.AnswerLen.HasValue)
                        endMatch++;
                    else if (endBytesCharRx.Length > 0 && b == endBytesCharRx[endMatch])
                        endMatch++;
                    else
                        endMatch = 0;
                }
            } while (endMatch < lenghtOutRead && timeOut.ElapsedMilliseconds <= streamResource.ReadTimeout);

            timeOut.Stop();

            if (!message.AnswerLen.HasValue)
                return data.Take(data.Count - endMatch).ToArray();
            else
                return data.ToArray();
        }

        public virtual byte[] ReadRequestMessage(ITransportMessage message, IStreamResource streamResource)
        {
            throw new System.NotImplementedException();
        }
    }
}
