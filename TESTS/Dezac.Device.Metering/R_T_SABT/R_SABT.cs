﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Metering.SOAP;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class R_SABT : DeviceBase
    {
        #region DLMS SBT-CALIDAD

        private DeviceDlmsTrifasic meter;

        internal DeviceDlmsTrifasic Meter
        {
            get
            {
                if (meter == null)
                    meter = new DeviceDlmsTrifasic();

                return meter;
            }
        }

        public void SetPort(int portDevice)
        {
            Meter.SetPort(portDevice, 9600);
            _logger.InfoFormat("New R-SABT quality by  COM PORT:{0}", portDevice);
            Meter.Retries = 3;
        }

        public void StartSession()
        {
            Meter.StartSession();
        }

        #region OBIS 

        private string[] OBIS_VOLTAGE_NSTANTANEOUS = { "1.128.32.7.0.255", "1.128.52.7.0.255", "1.128.72.7.0.255" };

        private string[] OBIS_EARTH_FAULT_CURRENT_RMS = { "1.128.131.7.1.255", "1.128.131.7.2.255", "1.128.131.7.3.255" };

        private string[] OBIS_ADJUST_ANGLE_VOLTAGE_LX_LX = { "1.128.128.124.1.255", "1.128.128.124.2.255", "1.128.128.124.3.255" };

        private string[] OBIS_ANGLE_VOLTAGE_LX_LX = { "1.128.81.7.0.255", "1.128.81.7.1.255", "1.128.81.7.2.255" };

        #endregion

        public string ReadFirmwareVersion()
        {
            return Meter.ReadFirmwareVersion();
        }

        public uint ReadFabricationNumber()
        {
            return Meter.ReadFabricationNumber();
        }

        public void WriteFabricationNumber(uint bastidor)
        {
            Meter.WriteFabricationNumber(bastidor);
        }

        public uint ReadFabricationStatus()
        {
            return Meter.ReadFabricationStatus();
        }

        public void WriteFabricationStatus(ushort coderror)
        {
            Meter.WriteFabricationStatus(coderror);
        }

        public string ReadManufactureCode()
        {
            return Meter.ReadManufactureCode();
        }

        public void WriteManufactureCode(string codeManufacture)
        {
            Meter.WriteManufactureCode(codeManufacture);
        }

        public void WriteDateTimeCurrent()
        {
            Meter.WriteDateTimeCurrent();
        }

        public DateTime ReadDateTime()
        {
            return Meter.ReadDateTime();
        }

        public string ReadModel()
        {
            return Meter.ReadModel();
        }

        public void WriteModel(string model)
        {
            Meter.WriteModel(model);
        }

        public string ReadSerialNumber()
        {
            return Meter.ReadSerialNumber();
        }

        public void WriteSerialNumber(string serialNumber)
        {
            Meter.WriteSerialNumber(serialNumber);
        }

        public uint ReadCrcBinary()
        {
            return Meter.ReadCrcBinary();
        }

        public TriLineGenericValue<double> ReadVoltage()
        {
            var voltage = Meter.ReadFasesObisRegister<double>(OBIS_VOLTAGE_NSTANTANEOUS);
            _logger.InfoFormat("R-SABT quality voltage:{0}", voltage);

            voltage.L1 /= 100;
            voltage.L2 /= 100;
            voltage.L3 /= 100;
            return voltage;
        }

        public TriLineGenericValue<double> ReadCurrent()
        {
            var current = Meter.ReadFasesObisRegister<double>(OBIS_EARTH_FAULT_CURRENT_RMS);
            _logger.InfoFormat("R-SABT quality Earth fault current:{0}", current);
            current.L1 /= 50000;
            current.L2 /= 50000;
            current.L3 /= 50000;
            return current;
        }

        public TriLineGenericValue<double> ReadAngleVoltage()
        {
            var angleVoltage = Meter.ReadFasesObisRegister<double>(OBIS_ANGLE_VOLTAGE_LX_LX);
            _logger.InfoFormat("R-SABT quality Angle Voltage Lx-Lx:{0}", angleVoltage);
            angleVoltage.L1 /= 100;
            angleVoltage.L2 /= 100;
            angleVoltage.L3 /= 100;
            return angleVoltage;
        }

        public TriLineGenericValue<ushort> AdjustVoltage(double voltageReference = 230, int delFirst = 0, int samples = 2, int numMaxSamples = 4, int interval = 3200)
        {
            LogMethod();

            Meter.StartSession();

            var resultadoAjuste = new TriLineGenericValue<ushort>();

            var adjustVariables = new ConfigurationSampler()
            {
                InitDelayTime = delFirst,
                SampleNumber = samples,
                SampleMaxNumber = numMaxSamples,
                DelayBetweenSamples = interval
            };

            var gainVoltageCurrent = Meter.ReadVoltageGain<double>();

            var adjustResult = AdjustBase(adjustVariables.InitDelayTime, adjustVariables.SampleNumber, adjustVariables.SampleMaxNumber, adjustVariables.DelayBetweenSamples, 0,
                () =>
                {
                    var voltage = ReadVoltage();
                    return voltage.ToArray();
                },
                (points) =>
                {
                    resultadoAjuste.L1 = (ushort)((gainVoltageCurrent.L1 * voltageReference) / points.Average(0));
                    resultadoAjuste.L2 = (ushort)((gainVoltageCurrent.L2 * voltageReference) / points.Average(1));
                    resultadoAjuste.L3 = (ushort)((gainVoltageCurrent.L3 * voltageReference) / points.Average(2));

                    _logger.InfoFormat("R-SABT quality Gain voltage:{0}", resultadoAjuste.ToString());

                    return true;
                });

            Meter.WriteVoltageGain<ushort>(resultadoAjuste);

            return resultadoAjuste;
        }

        public TriLineGenericValue<ushort> AdjustCurrent(double currentReference = 0.1, int delFirst = 0, int samples = 2, int numMaxSamples = 4, int interval = 3200)
        {
            LogMethod();

            currentReference *= 1000;

            Meter.StartSession();

            var resultadoAjuste = new TriLineGenericValue<ushort>();

            var adjustVariables = new ConfigurationSampler()
            {
                InitDelayTime = delFirst,
                SampleNumber = samples,
                SampleMaxNumber = numMaxSamples,
                DelayBetweenSamples = interval
            };

            var gainCurrentCurrent = Meter.ReadCurrentGain<double>();

            var adjustResult = AdjustBase(adjustVariables.InitDelayTime, adjustVariables.SampleNumber, adjustVariables.SampleMaxNumber, adjustVariables.DelayBetweenSamples, 0,
                () =>
                {
                    var current = ReadCurrent();
                    current.L1 *= 50000;
                    current.L2 *= 50000;
                    current.L3 *= 50000;
                    _logger.InfoFormat("R-SABT quality 1/50 current:{0}", current.ToString());
                    return current.ToArray();
                },
                (points) =>
                {
                    resultadoAjuste.L1 = (ushort)((gainCurrentCurrent.L1 * currentReference * 50) / points.Average(0));
                    resultadoAjuste.L2 = (ushort)((gainCurrentCurrent.L2 * currentReference * 50) / points.Average(1));
                    resultadoAjuste.L3 = (ushort)((gainCurrentCurrent.L3 * currentReference * 50) / points.Average(2));

                    _logger.InfoFormat("R-SABT quality Gain current:{0}", resultadoAjuste.ToString());

                    return true;
                });

            Meter.WriteCurrentGain<ushort>(resultadoAjuste);

            return resultadoAjuste;
        }

        public TriLineGenericValue<short> AdjustAngleVoltageGain()
        {
            var gainAngleVoltage = ReadAngleVoltage();

            var gainL1 =Convert.ToInt16(gainAngleVoltage.L1 * 100);
            var gainL2 = Convert.ToInt16((240 - gainAngleVoltage.L2) * 100);
            var gainL3 = Convert.ToInt16((120 - gainAngleVoltage.L3) * 100);

            var gainAngleVoltageValue = TriLineGenericValue<short>.Create(gainL1, gainL2, gainL3);

            _logger.InfoFormat("R-SABT quality Gain Angle voltage:{0}", gainAngleVoltageValue.ToString());

            Meter.WriteFasesObisRegister<short>(OBIS_ADJUST_ANGLE_VOLTAGE_LX_LX, gainAngleVoltageValue);

            return gainAngleVoltageValue;
        }

        public void WriteVoltageGain(ushort gainL1 = 25544, ushort gainL2= 25544, ushort gainL3= 25544)
        {
            Meter.WriteVoltageGain<ushort>(TriLineGenericValue<ushort>.Create(gainL1, gainL2, gainL3));
        }

        public void WriteCurrentGain(ushort gainL1 = 23458, ushort gainL2 = 23458, ushort gainL3 = 23458)
        {
            Meter.WriteCurrentGain<ushort>(TriLineGenericValue<ushort>.Create(gainL1, gainL2, gainL3));
        }

        public void WriteAngleVoltageGain(short gainL1 = 1, short gainL2 = 1, short gainL3 = 1)
        {
            Meter.WriteFasesObisRegister<short>(OBIS_ADJUST_ANGLE_VOLTAGE_LX_LX, TriLineGenericValue<short>.Create(gainL1, gainL2, gainL3));
        }

        public TriLineGenericValue<short> ReadAngleVoltageGain()
        {
            return Meter.ReadFasesObisRegister<short>(OBIS_ADJUST_ANGLE_VOLTAGE_LX_LX);
        }

        #endregion

        #region SOAP

        private R_SABTSoapTransport _soap;

        protected R_SABTSoapTransport Soap
        {
            get
            {
                if (_soap == null)
                    _soap = new R_SABTSoapTransport();

                return _soap;
            }
        }

        public string ReadSoapFile(string file)
        {
            LogMethod();

            var response = Soap.SynchReport(file);

            if (!response.EndsWith("</Report>"))
                response = response.Substring(0, response.IndexOf("</Report>") + 9);

            _logger.InfoFormat("response: {0}", response);

            if (string.IsNullOrEmpty(response))
                TestException.Create().UUT.COMUNICACIONES.SOAP_SERVICE(string.Format("SynchReport file:{0}", file)).Throw();

            XElement xelement = XElement.Parse(response);

            return xelement.ToString();
        }

        public void WriteSoapPathFile(string PathfileXml)
        {
            LogMethod();

            var xmlFile = XElement.Load(PathfileXml);

            var xElement = xmlFile.Elements();
            if (xElement == null)
                throw new Exception("Error el fichero XML no tiene el formato correcto o no es un fichero de ORDER");

            var idPet = from nm in xmlFile.Elements("Order")
                        select nm.Attribute("IdPet").Value;

            Soap.Order(xmlFile.ToString(), Convert.ToUInt32(idPet.FirstOrDefault()));
        }

        public void WriteSoapFile(string fileXml)
        {
            LogMethod();

            var xmlFile = XElement.Parse(fileXml);

            var xElement = xmlFile.Elements();
            if (xElement == null)
                throw new Exception("Error el fichero XML no tiene el formato correcto o no es un fichero de ORDER");

            var idPet = xmlFile.Attributes().Where((p) => p.Name == "IdPet").FirstOrDefault().Value;            //       select nm.Attribute("IdPet").Value;

            Soap.Order(xmlFile.ToString(), Convert.ToUInt32(idPet));

        //   Soap.Order(xmlFile.ToString(), idPet);
        }

        #endregion

        public override void Dispose()
        {
            if (meter != null)
                meter.Dispose();                        
         }
    }
}
