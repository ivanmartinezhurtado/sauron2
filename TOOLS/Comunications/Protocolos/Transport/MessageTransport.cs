using Comunications.Protocolo;
using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Comunications.Message
{
    public class MessageTransport : IDisposable
	{
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(MessageTransport));
		private object _syncLock = new object();
		private int _retries = 3;
		private int _waitToRetryMilliseconds = 250;
		protected IStreamResource _streamResource;

        private IProtocol protocol;

        public MessageTransport(IProtocol protocol, IStreamResource streamResource)
		{
			Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");

			_streamResource = streamResource;
            this.protocol = protocol;
            Logger = _logger;

            LogRawMessage = false;
            LogMessage = true;
		}

        public MessageTransport(IProtocol protocol, IStreamResource streamResource, ILog logger)
            : this(protocol, streamResource)
        {
            Logger = logger ?? _logger;
        }

        protected ILog Logger { get; set; }

        public IStreamResource StreamResource
        {
            get { return _streamResource; }
            set { _streamResource = value; }
        }

		/// <summary>
		/// Number of times to retry sending message after encountering a failure such as an IOException, 
		/// TimeoutException, or a corrupt message.
		/// </summary>
		public int Retries
		{
			get { return _retries; }
			set { _retries = value; }
		}

		/// <summary>
		/// Gets or sets the number of milliseconds the tranport will wait before retrying a message after receiving 
		/// an ACKNOWLEGE or SLAVE DEVICE BUSY slave exception response.
		/// </summary>
		public int WaitToRetryMilliseconds
		{
			get
			{
				return _waitToRetryMilliseconds;
			}
			set
			{
				if (value < 0)
					throw new ArgumentException("Debe especificar un valor superior a 0!");

				_waitToRetryMilliseconds = value;
			}
		}

		/// <summary>
		/// Gets or sets the number of milliseconds before a timeout occurs when a read operation does not finish.
		/// </summary>	
		public int ReadTimeout
		{
			get { return StreamResource.ReadTimeout; }
			set { StreamResource.ReadTimeout = value; }
		}

		/// <summary>
		/// Gets or sets the number of milliseconds before a timeout occurs when a write operation does not finish.
		/// </summary>
		public int WriteTimeout
		{
			get { return StreamResource.WriteTimeout; }
			set { StreamResource.WriteTimeout = value; }
		}

        public bool LogRawMessage { get; set; }
        public bool LogMessage { get; set; }
		
        public virtual T SendMessage<T>(T message) where T : ITransportMessage, new()
		{
            ITransportMessage response = null;
			int attempt = 1;
			bool success = false;

			do
			{
                try
                {
                    lock (_syncLock)
                    {
                        Write(message);

                        if (message.HasResponse)
                            response = ReadResponse<T>(message);
                    }

                    if(message.HasResponse)
                        ValidateResponse(message, response);

                    success = true;
                }
                catch (Exception e)
                {
                    if (e is FormatException || 
                        e is ArgumentException ||
                        e is TimeoutException ||
                        e is NotImplementedException ||
                        e is IOException)
                    {
                        Logger.WarnFormat("{0}, {1} retries remaining - {2}", e.GetType().Name, _retries - attempt + 1, e);

                        if (attempt++ > _retries)
                        {
                            Logger.ErrorFormat("{0} -> {1}", e.GetType().Name, e);

                            ExceptionHandler(e, true);
                        }
                        else ExceptionHandler(e);

                        StreamResource.HandleMessageException(e);

                        Thread.Sleep(WaitToRetryMilliseconds);
                    }
                    else
                    {
                        Logger.ErrorFormat("{0} -> {1}", e.GetType().Name, e);
                        ExceptionHandler(e, true);
                    }
                }
			} while (!success);

			return (T) response;
		}       

        public event EventHandler<MessageExceptionHandler> OnMessageException;

        protected void ExceptionHandler(Exception ex, bool Throw = false)
        {
            var handler = new MessageExceptionHandler { Exception = ex, Throw = Throw };

            if (OnMessageException != null)
                OnMessageException(this, handler);

            if (handler.Throw)
                throw handler.Exception;
        }

        public virtual T CreateMessage<T>(byte[] frame) where T : ITransportMessage, new()
        {
            ITransportMessage message = new T();
            message.Initialize(frame);

            return (T)message;
        }

        protected virtual void ValidateResponse(ITransportMessage request, ITransportMessage response)
		{
            if (!request.HasResponse)
                return;

            IValidateMessage validatableMessage = request as IValidateMessage;

            if (validatableMessage != null)
                validatableMessage.ValidateResponse(response);
        }

        protected virtual byte[] ReadRequestResponse(ITransportMessage message)
        {
            byte[] frame = protocol.ReadMessage(message, _streamResource);

            if (LogRawMessage)
                Logger.DebugFormat("{0} --> RX: {1}", StreamResource.PortName, GetLogFrameString(frame));

            if (frame.Length < 1)
                throw new IOException("Premature end of stream, message truncated.");

            return frame;
        }

        protected virtual byte[] ReadRequestMessage(ITransportMessage message)
        {
            byte[] frame = protocol.ReadRequestMessage(message, _streamResource);

            if (LogRawMessage)
                Logger.DebugFormat("{0} --> RX: {1}", StreamResource.PortName, GetLogFrameString(frame));

            if (frame.Length < 1)
                throw new IOException("Premature end of stream, message truncated.");

            return frame;
        }

        public virtual ITransportMessage ReadResponse<T>(T message) where T : ITransportMessage, new()
        {
            var frame = ReadRequestResponse(message);
            var response = CreateMessage<T>(frame);

            if (LogMessage)
                Logger.DebugFormat("{0} --> RX Msg: {1}", StreamResource.PortName, response);
            
            return response;
        }

        protected virtual void Write(ITransportMessage message)
        {
            StreamResource.DiscardInBuffer();

            if (LogMessage)
                Logger.DebugFormat("{0} --> TX Msg: {1}", StreamResource.PortName, message);

            byte[] data = BuildRequestMessageFrame(message);

            if (LogRawMessage)
                Logger.DebugFormat("{0} --> TX: {1}", StreamResource.PortName, GetLogFrameString(data));

            StreamResource.Write(data, 0, data.Length);
        }

        protected virtual byte[] BuildRequestMessageFrame(ITransportMessage message)
        {
            return protocol.BuildMessage(message);
        }

        protected virtual string GetLogFrameString(byte[] frame)
        {
            return string.Join("-", frame);
        }

		protected virtual void Dispose(bool disposing)
		{
            if (disposing && _streamResource != null)
            { 
                _streamResource.Dispose();
                _streamResource = null;
            }
		}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
	}
}
