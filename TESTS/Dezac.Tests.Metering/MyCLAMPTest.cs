﻿using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.03)]
    public class MyCLAMPTest : TestBase
    {
        private const int PILOTO_ERROR = 17;
        private const int IN_PRESENCIA_UUT = 9;
        private const int IN_BLOQUE_SEGURIDAD = 23;

        private MyCLAMP myClamp;
        private Tower3 tower;

        public MyCLAMP MyClamp
        {
            get
            {
                if (myClamp == null)
                    myClamp = new MyCLAMP();

                return myClamp;
            }
        }

        public void TestInitialization()
        {
            myClamp = AddInstanceVar(new MyCLAMP(), "UUT");

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.ShutdownSources();
            tower.IO.DO.Off(PILOTO_ERROR);

            SamplerWithCancel((s) =>
            {
                if (s == 8)
                    Shell.MsgBox("ATENCION BLOQUE DE SEGURIDAD NO CONECTADO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return tower.IO.DI.WaitOn(IN_BLOQUE_SEGURIDAD, 2000);
            }, "Error: No se ha detectado el bloque de seguridad conectado", 10, 500);

            SamplerWithCancel((p) => { return (tower.IO.DI[IN_PRESENCIA_UUT]); }, "Error: No se ha detectado el equipo", 2, 100, 1000);

            tower.MTEPS.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.5, ParamUnidad.PorCentage);
            tower.MTEPS.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 35000, ParamUnidad.ms);

            if (tower.MTEPS.TypeSource == Instruments.PowerSource.MTEPPS.typeSourceEnum.SPE120)
                tower.ActiveCurrentHighLowUnionMTE(true);
        }

        public void TestBattery(MyCLAMP.BateryEnum escala = MyCLAMP.BateryEnum.HI)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Bateria.TestPoint(escala.ToString()).Name, 3, ParamUnidad.V);

            if (tower.LAMBDA.Presets.Voltage != voltage) 
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltage, Consignas.GetDouble(Params.I.Null.EnVacio.Vnom.Name,0.1,ParamUnidad.A));

            var level = myClamp.ReadBateryLevel();

            Assert.AreEqual(string.Format("BATERIA {0}", escala.ToString()), level, escala, Error().UUT.MEDIDA.MARGENES(String.Format("Error: No se detecta el nivel correcto de bateria: detectado {0}, esperado {1}", level, escala)), ParamUnidad.SinUnidad);
        }

        public void TestComunication()
        {
            MyClamp.FindDevice();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, MyClamp.ReadFirmwareVersion(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error: la version de firmware del equipo no coincide con la de la BBDD"), ParamUnidad.SinUnidad);
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, MyClamp.ReadHardwareVersion(), Identificacion.VERSION_HARDWARE, Error().UUT.HARDWARE.NO_COINCIDE("Error: la version de hardware del equipo no coincide con la de la BBDD"), ParamUnidad.SinUnidad);
        }

        public void TestPulsador()
        {
            myClamp.WriteProductStatus(MyCLAMP.ProdStatus.TEST);

            Assert.IsFalse("PULSADOR_REPOSO", MyClamp.ReadStatusPulsador(), Error().PROCESO.PRODUCTO.BLOQUEADO("Error: El pulsador estaba pulsado antes de empezar el test de pulsador"));

            var imageView = new ImageView("TEST PULSADOR", this.PathCameraImages + "\\MYCLAMP\\PULSADOR.png");
            var frm = this.ShowForm(() => imageView, MessageBoxButtons.RetryCancel);
            try
            {
                SamplerWithCancel((s) =>
                {
                    if (s == 20)
                    {
                        var diagResult = Shell.MsgBox("¿DESEA CONTINUAR CON EL TEST DE TECLADO?", "TEST DE TECLADO", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (diagResult == DialogResult.No)
                            throw new Exception("Error en la comprobación del pulsador");
                    }

                    var result = MyClamp.ReadStatusPulsador();
                    return result; 
                }, "Error en la comprobación del pulsador", 40, 500, 2000);
            }
            finally
            {
                this.CloseForm(frm);
            }
            
            Resultado.Set("PULSADOR", "OK", ParamUnidad.SinUnidad);

            myClamp.WriteProductStatus(MyCLAMP.ProdStatus.NORMAL);

            Assert.AreEqual("ESTADO PRODUCCION", myClamp.ReadProductStatus(), MyCLAMP.ProdStatus.NORMAL, Error().PROCESO.PRODUCTO.BLOQUEADO("Error: No se ha podido volver al modo de trabajo normal"), ParamUnidad.SinUnidad);

            Assert.IsFalse("PULSADOR_DESPULSADO", MyClamp.ReadStatusPulsador(), Error().PROCESO.PRODUCTO.BLOQUEADO("Error: El pulsador sigue pulsado despues del test"));
        }

        public void TestLeds()
        {
            foreach (MyCLAMP.Leds led in Enum.GetValues(typeof(MyCLAMP.Leds)))
            {
                MyClamp.WriteLed(led);

                var imageView = new ImageView(string.Format("TEST LED {0}", led.ToString()), string.Format("{0}\\MYCLAMP\\LED_{1}.png", this.PathCameraImages, led.ToString()));
                var frm = this.ShowForm(() => imageView, MessageBoxButtons.OKCancel);
                try
                {
                    SamplerWithCancel((s) =>
                    {
                        if (s == 10)
                        {
                            var diagResult = Shell.MsgBox("¿DESEA CONTINUAR CON EL TEST DE LEDS?", "TEST DE LEDS", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (diagResult == DialogResult.No)
                                throw new Exception(string.Format("Error: LED {0} no se enciende", led.ToString()));
                        }

                        MyClamp.WriteLed(led);
                        return frm.DialogResult == DialogResult.OK;
                    }, string.Format("Error: LED {0} no se enciende",led.ToString()), 20, 2000);
                }
                finally
                {
                    this.CloseForm(frm);
                }
                Resultado.Set(string.Format("LED_{0}", led.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestAdjust(int delFirts = 2, int initCount = 3, int samples = 14, int timeInterval = 1100)
        {
            var adjustCurrent = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 0, ParamUnidad.A),
                L2 = 0,
                L3 = 0,
            };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, adjustCurrent, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var desviacionAjuste = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint("DESVIACION").Name, 0, ParamUnidad.PorCentage);
            var corrienteObjetivo = adjustCurrent.L1 * (1 + (desviacionAjuste / 100)); 
            var gainBBDD = new AdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var measureGains = myClamp.CalculateAdjust(delFirts, initCount, samples, timeInterval, corrienteObjetivo,
            (value) =>
            {
                gainBBDD.Value = value;

                return !HasError(gainBBDD, false);
            });

            MyClamp.WriteGainCurrent(measureGains.Item2);

            Resultado.Set("GAIN", measureGains.Item2.ToString(), ParamUnidad.Puntos);
            Resultado.Set("DESVIACION", desviacionAjuste.ToString(), ParamUnidad.PorCentage);
        }

        public void TestVerification(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100, EscalaEnum escala = EscalaEnum.ESC1)
        {
            var verificationCurrent = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Verificacion.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
                L2 = 0,
                L3 = 0,
            };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, verificationCurrent, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var desviacionAjuste = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint("DESVIACION").Name, 0, ParamUnidad.PorCentage);
            var adjustValue = new AdjustValueDef(Params.I.L1.Verificacion.TestPoint(escala.ToString()).Name, 0,0,0, Params.I.Null.Verificacion.TestPoint(escala.ToString()).Avg(), Params.I.Null.Verificacion.TestPoint(escala.ToString()).Tol(), ParamUnidad.A);

            TestCalibracionBase(adjustValue, () =>
                {
                    var currentRead = myClamp.ReadCurrent();
                    return currentRead / (1 + (desviacionAjuste / 100D));
                }, () =>
                {
                    return adjustValue.Average;
                }, 2, countSamples, timeInterval, "Error: Corriente fuera de margenes");
        }

        public void TestCostumization()
        {
            myClamp.WriteNumSerie(TestInfo.NumSerie);

            var num = myClamp.ReadNumSerie();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, num, TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_GRABADO(string.Format("Error: No se ha podido grabar el numero de serie, numero leido {0}", num)), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (string.IsNullOrEmpty(TestInfo.NumSerie))
                tower.IO.DO.On(PILOTO_ERROR);

            MyClamp.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.Dispose();
            }        
        }

        public enum EscalaEnum
        {
            ESC1,
            ESC2,
            ESC3,
        }
    }
}
