﻿using System;

namespace TaskRunner.Model
{
    public class ResultItem
    {
        public string Name { get; set; }
        public int NumInstance { get; set; }
        public Step Step { get; set; }
        public DateTime Timestamp { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            if (Name != null)
            {
                if (NumInstance > 1)
                    return string.Format("(#{0}) {4} = {1} - {2:HH:mm:ss} - {3}",
                        NumInstance, Step.Name, Timestamp, Value, Name);

                return string.Format("{3} = {0} - {1:HH:mm:ss} - {2}",
                    Step.Name, Timestamp, Value, Name);
            }
            else if (NumInstance > 1)
                return string.Format("(#{0}) {1} - {2:HH:mm:ss} - {3}",
                    NumInstance, Step.Name, Timestamp, Value);

            return string.Format("{0} - {1:HH:mm:ss} - {2}",
                Step.Name, Timestamp, Value);
        }
    }
}
