﻿using Dezac.Core.Exceptions;
using Instruments.Utility;
using log4net;
using System;
using System.Threading;

namespace Instruments.PowerSource
{
    public abstract class PowerSourceACBase<T> : PowerSourceBase<T>
    {
        public class PowerSourceACData<T>
        {
            public PowerSourceACData(T t)
            {
                Voltage = t;
                Current = t;
                Desfase = t;
                PF = t;
                Power = t;
                PowerFactor = t;
                Freq = 0;
            }
            public double Freq { get; set; }
            public T Voltage { get; set; }
            public T Current { get; set; }
            public T Desfase { get; set; }
            public T Power { get; set; }
            public T PowerFactor { get; set; }
            public T PF { get; set; }
        }

        public PowerSourceACData<T> Presets { get; internal set; }
        internal PowerSourceACData<T> DevicePreset { get; set; }
        internal PowerSourceACData<T> DeviceData { get; set; }

        public PowerSourceACBase(T t, ILog logger):base(t, logger)
        {
            Presets = new PowerSourceACData<T>(t);
            DeviceData = new PowerSourceACData<T>(t);
            DevicePreset = new PowerSourceACData<T>(t);
            Status = new PowerSourceStatus();
            Tolerance = 0.5;
            TimeOutStabilitation = 30000;
            VoltageOffMin = 20;
            RetryStabilisation = 3;
            Logger = logger ?? _logger;
        }

        protected override void ApplyPresetsBase(bool forceApply = false)
        {
            bool apply = false;

            var result = ReadStatus().Value;
            if (result.HasError || result.HasWarning)
            {
                Logger.InfoFormat("Starting Reset in PowerSource by Error detection");
                Reset();
                forceApply = true;
                Thread.Sleep(3000);
            }

            if (forceApply || !DevicePreset.Voltage.Equals(Presets.Voltage))
            {
                var cmd = new Command<Commands>(Commands.SET_VOLTAGE, true);
                Logger.InfoFormat("Apply Voltage: {0}", Presets.Voltage);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (forceApply || !DevicePreset.Current.Equals(Presets.Current))
            {
                var cmd = new Command<Commands>(Commands.SET_CURRENT, true);
                Logger.InfoFormat("Apply Current: {0}", Presets.Current);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (forceApply || !DevicePreset.PF.Equals(Presets.PF))
            {
                var cmd = new Command<Commands>(Commands.SET_PF, true);
                Logger.InfoFormat("Apply PF: {0}", Presets.PF);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (forceApply || DevicePreset.Freq != Presets.Freq)
            {
                var cmd = new Command<Commands>(Commands.SET_FREQ, true);
                Logger.InfoFormat("Apply Freq: {0}", Presets.Freq);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (forceApply || !DevicePreset.Desfase.Equals(Presets.Desfase))
            {
                var cmd = new Command<Commands>(Commands.SET_DESFASE, true);
                Logger.InfoFormat("Apply GapPhase: {0}", Presets.Desfase);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (apply)
            {
                var cmd = new Command<Commands>(Commands.APPLY_CONFIG, true);
                SendCommand(cmd);
                IsOn = true;
            }

            if (Status.HasError)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE($"ERROR INTERNO FUENTE {Status.ErrorDescription}").Throw();
        }

        protected void ApplyPresetsBase(T voltage, T current, double freq, bool forceApply = false)
        {
            ConfigPresets(voltage, current, freq, DevicePreset.PF, DevicePreset.Desfase);
            ApplyPresetsBase(forceApply);
        }

        protected void ApplyPresetsBase(T voltage, T current, double freq, T PF, T desfase, bool forceApply= false)
        {
            ConfigPresets(voltage, current, freq, PF, desfase);
            ApplyPresetsBase(forceApply);
        }

        protected bool ApplyAndWaitStabilisationBase(bool forceApply = false)
        {
            return internalApplyAndWaitStabilisation(forceApply);
        }

        protected bool ApplyAndWaitStabilisationBase(T voltage, T current, double freq, bool forceApply = false, Func<bool> validator = null)
        {
            ConfigPresets(voltage, current, freq, DevicePreset.PF, DevicePreset.Desfase);
            return internalApplyAndWaitStabilisation(forceApply, validator);
        }

        protected bool ApplyAndWaitStabilisationBase(T voltage, T current, double freq, T pf, T desfase, bool forceApply = false, Func<bool> validator = null)
        {
            ConfigPresets(voltage, current, freq, pf, desfase);
            return internalApplyAndWaitStabilisation(forceApply, validator);
        }

        public override void Reset()
        {
            var cmd = new Command<Commands>(Commands.RESET, true);
            SendCommand(cmd);
            IsOn = false;
        }

        //******************************************************************

        private void ConfigPresets(T voltage, T current, double freq, T PF, T desfase)
        {
            this.Presets.Voltage = voltage;
            this.Presets.Current = current;
            this.Presets.Freq = freq;
            this.Presets.PF = PF;
            this.Presets.Desfase = desfase;
        }
    }
}
