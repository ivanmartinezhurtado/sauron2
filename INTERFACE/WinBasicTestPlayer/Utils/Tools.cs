﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WinTestPlayer.Utils
{
    public static class Tools
    {
        public static string CrearUrlFileInstruccion(byte[] pdf)
        {
            string fileName = Path.GetTempFileName() + ".pdf";

            File.WriteAllBytes(fileName, pdf);

            return fileName;
        }

        public static void DelayCall(int msec, Action fn)
        {
            var sc = SynchronizationContext.Current;

            Task.Run(() =>
            {
                Thread.Sleep(msec);
                sc.Post(_ => { fn(); }, null);
            });
        }
    }
}
