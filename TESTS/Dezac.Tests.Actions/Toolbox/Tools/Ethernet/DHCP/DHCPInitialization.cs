﻿using Dezac.Core.Utility;
using System.ComponentModel;
using System.Drawing;
using WinDHCP.Library;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(DHCPInitializationDescription))]
    public class DHCPInitialization : ActionBase
    {
        [DefaultValue("DHCPServer")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto DHCP Server")]
        public string DHCPServerName { get; set; }

        [Category("3. Configuration")]
        [DefaultValue("100.0.0.2")]
        [Description("Dirección IP Inicial del rango de IP del servidor")]
        public string StartIp { get; set; }

        [Category("3. Configuration")]
        [DefaultValue("100.0.0.254")]
        [Description("Dirección IP Final del rango de IP del servidor")]
        public string EndIp { get; set; }

        [DefaultValue("255.255.255.0")]
        [Category("3. Configuration")]
        [Description("Dirección Mascara Subred")]
        public string SubnetMask { get; set; }

        [Category("3. Configuration")]
        [Description("Dirección IP de la puerta de enlace")]
        public string Getaway { get; set; }

        [Category("3.2 Administrator")]
        [DefaultValue(false)]
        [Description("Habilitar si soy Administrador")]
        public bool isAdmin { get; set; }


        public override void Execute()
        {
            if (string.IsNullOrEmpty(EndIp))
            { 
                var endAddressTmp = StartIp.Split('.');
                endAddressTmp.SetValue("254", 3);
                EndIp = string.Join(".", endAddressTmp);
            }

            if (string.IsNullOrEmpty(SubnetMask))
                SubnetMask = "255.255.255.0";

            if (string.IsNullOrEmpty(Getaway))
                Getaway = StartIp;

            NetUtils.ConfigIP(StartIp, Getaway, SubnetMask, isAdmin);

            var server = new DhcpServer();
            server.ConfigIP(StartIp, EndIp, SubnetMask, Getaway);
            SetSharedVariable(DHCPServerName ?? "DHCPServer", server);
        }
    }

    public class DHCPInitializationDescription : ActionDescription
    {
        public override string Name { get { return "DHCPInitialization"; } }
        public override string Description { get { return "Inicializa y configura sobre un adaptador de red una instancia de un Servidor DHCP en el propio PC"; } }
        public override string Category { get { return "Tools/Ethernet/DHCP"; } }
        public override Image Icon { get { return Properties.Resources.DHCP; } }
    }
}
