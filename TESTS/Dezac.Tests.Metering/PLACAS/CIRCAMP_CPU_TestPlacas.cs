﻿using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Metering.Placas
{
    [TestVersion(1.03)]
    public class CIRCAMP_CPU_TestPlacas : TestBase
    {
        CirwattDlms sbt;

        public void TestInitialization()
        {
            sbt = AddInstanceVar(new CirwattDlms(), "UUT");
            sbt.Retries = 4;

            logger.InfoFormat("COM PORT:{0}", sbt.PuertoCom);

            Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //***********************************************************************************

        public void TestComunicacionesOpticas()
        {
            sbt.PuertoCom = (byte)Comunicaciones.SerialPort2;
            logger.InfoFormat("COM PORT:{0}", sbt.PuertoCom);

            sbt.IniciarSesion();

            sbt.EscribirEstadoFabricacion(9999);

            var versionSW = sbt.LeerVersionSW();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, versionSW, Identificacion.VERSION_FIRMWARE_SBT, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. la versión de firmware es incorrecto."), ParamUnidad.SinUnidad);
        }

        public void TestDisplayCPU(byte tipoDisplay)
        {
            sbt.IniciarSesion();

            sbt.WriteActivateDisplay(tipoDisplay);
        }

        public void TestLedsCPU()
        {
            sbt.IniciarSesion();

            sbt.WriteActivateLeds(CirwattDlms.enumLeds.ALL_INPUTS);
        }

        public void TestPPM()
        {
            var tower = GetVariable<TowerBoard>("TOWER");

            tower.HP53131A
              .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_50)
              .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.LOW, 2) //1.5 Level pasar a Double
              .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 9);

            tower.IO.DO.OnWait(1500, 18);

            var ppmMeasure = tower.MeasureWithFrecuency(InputMuxEnum.IN12, 10000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);

            Assert.IsValidMaxMin(Params.FREQ.Null.RTC.Name, ppmMeasure, ParamUnidad.Hz, Params.FREQ.Null.RTC.Min(), Params.FREQ.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("RTC"));

            tower.IO.DO.Off(18);
           
            var ppmCalc = Math.Round(1 * ((1 / ppmMeasure) * 512), 9);
            var TempEquipo = sbt.Leer_Temperatura();
            var PPM_Calc_Temp = (short)(ppmCalc - (Math.Pow((TempEquipo - 25), 2) * (-39)) / 1000);

            sbt.EscribirPPM(PPM_Calc_Temp);

            var ppmRead = sbt.Leer_PPM();

            Assert.IsValidMaxMin(Params.PPM.Null.RTC.Name, ppmRead, ParamUnidad.PorCentage, Params.PPM.Null.RTC.Min(), Params.PPM.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("PPM"));
        }

        public void TestFinish()
        {
            if (sbt != null)
                sbt.Dispose();
        }
    }
}
