namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TestFases")]
    public partial class TestFases
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Test")]
        public int TestId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        [ForeignKey("Fase")]
        public string FaseId { get; set; }

        public int Secuencia { get; set; }

        public virtual Fase Fase { get; set; }
        public virtual Test Test { get; set; }
    }
}
