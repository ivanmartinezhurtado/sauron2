﻿using Comunications.Ethernet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Device
{
    public class ApiDevice : DeviceBase
    {
        protected ApiClient Api { get; set; }
        protected string BaseUrl { get; set; }

        private System.Threading.CancellationToken ct = System.Threading.CancellationToken.None;
        protected void SetApiDevice(string baseUrl)
        {
            BaseUrl = baseUrl;
            Api = new ApiClient(BaseUrl);
        }

        protected async Task<T> Get<T>(string path)
        {
            return await Api.GetAsync<T>(path, ct);
        }

        protected Task Put<T>(string path, T body)
        {
            return Api.PutAsync<T>(path, body, ct);
        }

        protected async Task Post(string path, Stream body)
        {
            await Api.PostAsync(path, body, ct);
        }

        public override void Dispose()
        {
            if(Api != null)
                Api.Dispose();
        }

    }
}
