﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace Dezac.Device.ThermalStations
{
    [DeviceVersion(1.00)]
    public class CRONOTERMOSTATOTACTIL : DeviceBase
    {
        public CRONOTERMOSTATOTACTIL()
        {
        }

        public CRONOTERMOSTATOTACTIL(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public CRONOTERMOSTATOTACTIL(SerialPort SerialPort)
        {
            SetSerialPort(SerialPort);
        }

        public void SetSerialPort(SerialPort SerialPort)
        {
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Modbus read operations
        
        public ushort ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.SOFTWARE_VERSION);
        }

        public Keys ReadKeyboard()
        {
            LogMethod();
            return (Keys)Modbus.ReadRegister((ushort)Registers.KEYBOARD);
        }

        public List<MaskedKeys> ReadKeyboardMask()
        {
            LogMethod();
            var keyboard = Modbus.ReadRegister((ushort)Registers.KEYBOARD_MASK);

            return GetEnumItemsFromUshort<MaskedKeys>(keyboard);
        }

        public ushort ReadTemperature()
        {
            return Modbus.ReadRegister((ushort)Registers.TEMPERATURE);
        }

        #endregion

        #region Modbus write operations
        
        public void WriteBacklightAndDisplay(bool estado)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)(estado ? 2 : 3));
        }

        public void WriteKeyboardTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)1);
        }

        public void Write434MHzFrequencyTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)4);
        }

        public void Write433MHzFrequencyTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)5);
        }

        public void WriteTemperatureTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)6);
        }

        public void WriteEndTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)0xA);
        }

        public void WriteConsumptionMeasure()
        {
            LogMethod();
            Modbus.WithTimeOut((m) => { m.Write((ushort)Registers.TESTING_OPERATIONS, (ushort)0xB); });
        }

        #endregion

        #region Data checking

        public double ReadAndCheckTemperatureProbe()
        {
            var probe = ReadTemperature();

            if (Enum.IsDefined(typeof(TemperatureProbeErrorCode), probe))
                throw new Exception(string.Format("La sonda de temperatura informa del siguiente error {0}", ((TemperatureProbeErrorCode)probe).ToString()));

            return probe / 10D;
        }
        
        #endregion

        #region Dictionarys

        //public Dictionary<Keys, string> diccionarioTeclas = new Dictionary<Keys, string>() 
        //{
        //    {Keys.ECO, "modo eco"},
        //    {Keys.ENTER, "introducir"},
        //    {Keys.FAN, "ventilador"},
        //    {Keys.INFO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //    {Keys.ECO, "Modo eco"},
        //};

        #endregion

        #region Enumerators
        
        public enum TemperatureProbeErrorCode : ushort
        {
            LOW_THRESHOLD = 0x0002,
            HIGH_THRESHOLD = 0x0001,
            NTC_OPEN = 0x0003,
            NTC_SHORTCIRCUITED = 0x0004
        }

        public enum Keys
        {
            NO_KEY_OR_MORE = 0,
            INFO = 1111,
            SET = 2222,
            WORK_MODE = 3333,
            PLUS = 4444,
            ENTER = 5555,
            MINUS = 6666,
            FAN = 7777,
            PROGRAM = 8888,
            ECO = 9999
        }

        [Flags]
        public enum MaskedKeys
        {
            NONE = 0x0000,
            INFO = 0x0001,
            SET = 0x0002,
            WORK_MODE = 0x0004,
            PLUS = 0x0008,
            PROGRAM = 0x0010,
            ECO = 0x0020,
            ENTER = 0x0040,
            MINUS = 0x0080,
            FAN = 0x0100,
        }

        public enum Registers
        {
            TESTING_OPERATIONS = 0x0010,
            KEYBOARD = 0x0001,
            KEYBOARD_MASK = 0x0002,
            SOFTWARE_VERSION = 0x0000,
            TEMPERATURE = 0x0003,
        }

        #endregion
    }
}
