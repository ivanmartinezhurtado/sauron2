﻿using System;
using System.Collections.Generic;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class EKOR_RPA100 : EKOR_SAFE_OLD
    {
        private const string AUTOCALIBRATIONVOLTAGE = "CAL A {0:0000} {1} {2} {3} {4} C=3000";
        public EKOR_RPA100()
        {
        }

        public EKOR_RPA100(int port)
        {
            SetPort(port);
        }

        public override void CalibrationVoltage(ChannelVoltage channel, List<double> current_uA, Scale scale = Scale.Fina)
        {
            LogMethod();
            var valueChannel = (ushort)channel;
            var param = new List<string>();
            param.Add(valueChannel.ToString("X4"));
            param.Add(((ushort)scale).ToString());
            param.AddRange(current_uA.ConvertAll((p) => Math.Round(p * 1000).ToString()));

            Cirbus.Write(AUTOCALIBRATIONVOLTAGE, listErrorCalibration, param.ToArray());
        }

        [Flags]
        public new enum enumInputs
        {
            IN1,
            IN2,
            IN3,
            IN4,
            IN5,
            IN6,
            IN7,
            IN8,
            IN9,
            IN10
        }

        public new struct VoltageMeasureVerification
        {
            public string RESP;
            public double VL1;
            public double VL2;
            public double VL3;
            public double VN;
            public double VLK;

            public double VoltageL1 { get { return VL1 / 100; } set { VL1 = value; } }
            public double VoltageL2 { get { return VL2 / 100; } set { VL2 = value; } } 
            public double VoltageL3 { get { return VL3 / 100; } set { VL3 = value; } } 
        }

        public new struct StructCalibrationMeasure
        {
            public string resp;
            public int factor_IL1;
            public int factor_IL2;
            public int factor_IL3;
            public int factor_IH;
            public int factor_VL1;
            public int factor_VL2;
            public int factor_VL3;
            public string delimiter;
            public int offset_IL1;
            public int offset_IL2;
            public int offset_IL3;
            public int offset_IH;
            public int offset_VL1;
            public int offset_VL2;
            public int offset_VL3;

            public override string ToString()
            {
                return string.Format("{0:00000000000} {1:00000000000} {2:00000000000} {3:00000000000} {4:00000000000} {5:00000000000} {6:00000000000} {7:00000} {8:00000} {9:00000} {10:00000} {11:00000} {12:00000} {13:00000}",
                    factor_IL1, factor_IL2, factor_IL3, factor_IH, factor_VL1, factor_VL2, factor_VL3, offset_IL1, offset_IL2, offset_IL3, offset_IH, offset_VL1, offset_VL2, offset_VL3);
            }
        }
    }
}
