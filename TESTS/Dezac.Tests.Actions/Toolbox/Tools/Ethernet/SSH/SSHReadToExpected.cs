﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(SSHReadToExpectedActionDescription))]
    public class SSHReadToExpected : ActionBase
    {
        [Category("3. Configuation")]
        [Description("Especifíca el texto de respuesta hasta dónde se desea leer del terminal")]
        public ActionVariable ExpectedText { get; set; } = new ActionVariable();

        [DefaultValue("SSH_Device")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }

        [Category("3. Configuation")]
        [Description("TimeOut en milisegundos")]
        public int TimeOut { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var ssh = GetVariable(DeviceName.Replace('-', '_')) as SSHClient;
            if (ssh == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el SSHClient").Throw();

            var expectedText = VariablesTools.ResolveText(ExpectedText.Value.ToString());

            testBase.SamplerWithCancel((p) =>
            {
                var response = ssh.Shell.Expect(expectedText, TimeSpan.FromMilliseconds(TimeOut));

                if (response == null)
                    return false;

                Context.ResultList.Add(response);
                testBase.Logger.DebugFormat(response);

                return true;

            }, "No se ha encontrado el texto esperado en la respuesta por SSH", 5, 100, 500);
        }
    }

    public class SSHReadToExpectedActionDescription : ActionDescription
    {
        public override string Name { get { return "SSHShellReadToExpected"; } }
        public override string Description { get { return "Lee hasta el texto esperado por una tty de la sesión SSH"; } }
        public override string Category { get { return "Tools/Ethernet/SSH"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
