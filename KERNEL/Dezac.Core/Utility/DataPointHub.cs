﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace Dezac.Core.Utility
{
    public static class DataPointHub
    {
        public static EventHandler<DataTimePoint> PointAdded;

        public static void Add(object sender, DataTimePoint point)
        {
            if (PointAdded != null)
                PointAdded(sender, point);
        }

        public static void Add(object sender, string name, double value, string source)
        {
            Add(sender, new DataTimePoint { Name = name, Time = DateTime.Now, Value = value, Source = source });
        }

        public static double? TryAdd(object sender, string name, object value, string source)
        {
            if (value == null)
                return null;

            if (value is double)
            {
                Add(sender, name, (double)value, source);
                return (double)value;
            }

            double dblValue;

            if (double.TryParse(value.ToString(), out dblValue))
            {
                Add(sender, name, dblValue, source);
                return dblValue;
            }

            return null;
        }

        //public static bool TryAddObject(object sender, string name, object value, string resource)
        //{
        //    if (value == null)
        //        return false;

        //    if (TryAdd(sender, name, value, resource))
        //        return true;

        //    var props = JsonHelper.DeserializeObjectAndFlattenList(value);

        //    foreach (var kvp in props)
        //        DataPointHub.TryAdd(sender, kvp.Name, kvp.Value, resource);

        //    return props.Any();
        //}
    }
}
