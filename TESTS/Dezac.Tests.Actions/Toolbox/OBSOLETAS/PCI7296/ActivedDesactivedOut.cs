﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ActivedDesactivedOutDescription))]
    

    public class ActivedDesactivedOut : ActionBase
    {
        [Description("valor de la salida que des/activamos,")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Output { get; set; }

        [Description("Tiempo de espera antes de relizar acción en milisegundos")]
        [DefaultValue(10)]
        public int WaitTimeBefore { get; set; }

        [Description("Tiempo de espera después de relizar acción en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeAfter { get; set; }

        [Description("Salida que activamos para despues detectar la entrada solicitada")]
        [DefaultValue(StatusOutput.ACTIVED)]
        public StatusOutput Action { get; set; }

        public enum StatusOutput
        {
            ACTIVED,
            DESACTIVED
        }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                throw Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            System.Threading.Thread.Sleep(WaitTimeBefore);

            if (string.IsNullOrEmpty(Output))
            if (testBase.Shell.MsgBox("Error, no se ha indicado ninguna Output para activar", "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.OK, MessageBoxIcon.Error) != DialogResult.Yes)
                throw Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("ninguna Output para activar");
            //  throw new Exception("Error, no se ha indicado ninguna Output para activar");

            var output = Convert.ToByte(VariablesTools.ResolveValue(Output));

            if (Action == StatusOutput.ACTIVED)
                tower.IO.DO.On(output);
            else
                tower.IO.DO.Off(output);

            System.Threading.Thread.Sleep(WaitTimeAfter);
        }
    }

    public class ActivedDesactivedOutDescription : ActionDescription
    {
        public override string Name { get { return "ActivedDesactivedOut"; } }
        public override string Description { get { return "Acción genérica para des/activar las salidas de la torre"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.output; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }
}
