﻿using Dezac.Device.Bluetooth.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Device.Bluetooth
{
    public class TCP_RSDevice : BluetoothDevice
    {        

        public static string DEFAULT_PIN = "000000";
        public static CVM_E3Device Find(int timeOut = 10000)
        {
            var result = BluetoothHelper.Scan("E3-Mini", timeOut);
            if (result != null)
                return new CVM_E3Device { BluetoothAddress = result.BluetoothAddress, PIN = DEFAULT_PIN };

            return null;
        }

        public TCP_RSDevice()
        {
        }

        public TCP_RSDevice(string name)
            : base(name)
        {
        }

        public static TCP_RSDevice Find(string serialNumber, int timeOut = 10000)
        {
            DEFAULT_PIN = serialNumber.Substring(serialNumber.Length - 6);
            var result = BluetoothHelper.Scan(string.Format("TCPRS1-{0}", serialNumber.Substring(serialNumber.Length - 4)), timeOut);
            if (result != null)
                return new TCP_RSDevice { BluetoothAddress = result.BluetoothAddress, PIN = DEFAULT_PIN };

            return null;
        }      
    }
}
