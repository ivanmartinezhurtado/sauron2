﻿using Dezac.Device.Measure;
using Dezac.Tests.Model;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.01)]
    public class CVMB1XXTestModuloProfiBus : CVMB1XXTestModulosGenerico
    {
        public void TestInitialization(byte portCVM)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            //CVMDummy = AddInstanceVar(new CVMB1XX(portCVM), "UUT");
            //CVMDummy.Modbus.PerifericNumber = Comunicaciones.Periferico;
            //CVMDummy.Modbus.PortCom = portCVM;
            //Resultado.Set("VERSION_DEVICE", string.Format("{0}", CVMDummy.GetDeviceVersion()), ParamUnidad.SinUnidad);
            //Resultado.Set("CLASSE_DEVICE", string.Format("{0}", CVMDummy.GetType().Name), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (CVMDummy != null)
                TestDispose();
        }

    }
}
