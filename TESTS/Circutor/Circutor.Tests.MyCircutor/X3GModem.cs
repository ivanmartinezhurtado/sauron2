﻿using log4net;
using System;
using System.Diagnostics;
using System.IO.Ports;

namespace Circutor.Tests.MyCircutor
{
    public class X3GModem : IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger("X3GModem");

        private SerialPort device;

        private string MESCopyAppPath = @"C:\Program Files\Cinterion\Module Exchange Suite\";

        public X3GModem(int port)
        {
            device = new SerialPort("COM" + port, 19200);
            device.NewLine = "\r\n";
        }

        public void CopyFile(string sourceFileName, string targetFileName)
        {
            var proc = Process.Start($"{MESCopyAppPath}\\MESCopy.exe", $@"{sourceFileName} mod:a:\{targetFileName}");
            proc.WaitForExit();

            if (proc.ExitCode != 0)
                throw new Exception("Imposible copiar el archivo!");

            proc = Process.Start($"{MESCopyAppPath}\\MESClose.exe");
            proc.WaitForExit();

            System.Threading.Thread.Sleep(5000);
        }

        public void Reset()
        {
            SendATCommand("AT+CFUN=1,1");
        }

        public bool IsLocked()
        {
            var result = SendATCommand("AT+CLCK=\"SC\",2");

            return result.IndexOf("+CLCK: 1", StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        public void Lock(string pin)
        {
            SendATCommand($"AT+CLCK=\"SC\",1,\"{pin}\"");
        }

        public void Unlock(string pin)
        {
            SendATCommand($"AT+CLCK=\"SC\",0,\"{pin}\"");
        }

        public string SendATCommand(string cmd)
        {
            logger.Info(cmd);

            if (!device.IsOpen)
                device.Open();

            device.WriteLine(cmd);

            string result = null;
            bool end = false;

            do
            {
                var line = device.ReadLine();
                if (line == "ERROR")
                    throw new Exception(result);

                end = line == "OK";
                if (!end)
                {
                    if (result == null)
                        result = line;
                    else
                        result += "\n" + line;
                }
            } while (!end);

            logger.InfoFormat(result);

            return result;
        }

        public string ReadLine()
        {
            return device.ReadLine();
        }

        public void Dispose()
        {
            device?.Dispose();
        }
    }
}
