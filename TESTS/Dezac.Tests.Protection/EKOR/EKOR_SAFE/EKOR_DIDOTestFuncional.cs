﻿using Dezac.Device.Protection;
using Dezac.Tests.Actions;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.17)]
    public class EKOR_DIDOTestFuncional : TestBase
    {
        protected Tower3 tower;
        protected EKOR_DIDO ekor;
        public const string IMAGE_PATH = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\EKOR_DIDO\";


        private const byte OUT_CLK = 55;
        private const byte OUT_MODULO_USB = 43;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.Active24VDC();         

            tower.WaitBloqSecurity(5000);
        }

        public void TestComunicationsUSB(string nameDevicePortVirtual)
        {
            var portEkor = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));

            ekor = new EKOR_DIDO(portEkor);
            ekor.Cirbus.PerifericNumber = 1;
            var retriesCirbus = ekor.Cirbus.Retries;
            ekor.Cirbus.Retries = 1;
            Logger.Info("Se ha creado el Objeto ComCirbus");
            
            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    ekor.RecoveryDefaultSettings(portEkor, 9600, 1);
                    Delay(2000, "");
                }

                var versionEkor = ekor.ReadVersion();
                
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionEkor.version, Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del eKOR no es correcta"), ParamUnidad.SinUnidad);

                var versionDeveloper = ekor.ReadVersionDesarrollo();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_DEVELOPER, versionDeveloper.version, Identificacion.VERSION_DEVELOPER.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware de desarrollo del eKOR no es correcta"), ParamUnidad.SinUnidad);

                var versionMicro = ekor.ReadMicrocontrolerType();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, versionMicro.cpu, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.HARDWARE.NO_COINCIDE("Error, la  tipo de microcontrolador no es el correcto"), ParamUnidad.SinUnidad);

                Resultado.Set("CPU REVISION", versionMicro.revision, ParamUnidad.SinUnidad);

                var versionBoot = ekor.ReadVersionBoot();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_BOOT, versionBoot, Identificacion.VERSION_BOOT.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, version de boot incrrecto"), ParamUnidad.SinUnidad);

                var crcBoot = ekor.ReadCRCBoot();

                Assert.AreEqual(ConstantsParameters.Identification.CRC_BOOT, crcBoot, Identificacion.CRC_BOOT.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, crc de boot incorrecto"), ParamUnidad.SinUnidad);

                return true;

            }, "Error de comunicaciones por el puerto USB al preguntar Version firmware el eKOR", 5, 1000, 1000, false, false);

            ekor.Cirbus.Retries = retriesCirbus;
            SamplerWithCancel((p) =>
            {
                var result = ekor.ReadPowerBitState((int)EKOR_DIDO.enumPowerStates.VUSB_OK);

                Resultado.Set("VUSB_OK", result.ToString(), ParamUnidad.SinUnidad);

                return result;

            }, string.Format("Error. no se detecta la entrada {0} activada", EKOR_DIDO.enumPowerStates.VUSB_OK.ToString()), 2, 500, 100, false, false);

            Delay(500, "Espera al FAT I y despues al FAT FORMAT");

            var stateFlash = ekor.ReadStateFLASH();
            Resultado.Set("FLAH_ID", stateFlash.ID, ParamUnidad.SinUnidad);
            Resultado.Set("FLAH_STATUS", stateFlash.state.ToString(), ParamUnidad.SinUnidad);

            ekor.FATFormat();

            Assert.IsTrue(stateFlash.state == EKOR_SAFE.stateFLAASH.READY, Error().UUT.MEMORIA.SETUP(string.Format("Error, estado de la memoria flash en {0}", stateFlash.state.ToString())));

            ekor.WriteDateTime();

            EKOR_DIDO.ModelHardware model = EKOR_DIDO.ModelHardware.FAB_DIDO;

            ekor.WriteConfigurationDefaultFab(model);

            DriveInfo usbDrive = null;

            ResetUSB();

            SamplerWithCancel((p) =>
            {
                if (p == 30)
                    ResetUSB();

                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKOR_DISK", ParamUnidad.SinUnidad).Trim())
                    {
                        usbDrive = drive;
                        break;
                    }

                if (usbDrive == null)
                    throw new Exception("Error no se detecta la unidad USB en el S.O.");

                logger.InfoFormat("USB_DRIVE RottDirectory: {0}", usbDrive.RootDirectory.Name);
                Resultado.Set("USB_DRIVE", usbDrive.VolumeLabel, ParamUnidad.SinUnidad);

                return !string.IsNullOrEmpty(usbDrive.RootDirectory.Name);

            }, "Error No se ha encontrado el dispositivo USB del EKOR", 60, 1000, 7500, false, false);                       
        }

        public void TestComunicationRS485()
        {
            ekor = new EKOR_DIDO();
            ekor.SetPort(7);
            var ekorPort = ekor.Cirbus.PortCom;
            ekor.Cirbus.ClosePort();
            ekor.Cirbus.PortCom = 7; // Comunicaciones.SerialPort;            

            var comRS485 = new List<OutRs485>();
            comRS485.Add(new OutRs485() { Description = "RS485_X6_1", Out0 = 0, Out1 = 0, Out2 = 0, Out3 = 1 });
            comRS485.Add(new OutRs485() { Description = "RS485_X6_2", Out0 = 1, Out1 = 0, Out2 = 0, Out3 = 0 });
            comRS485.Add(new OutRs485() { Description = "RS485_X4_1", Out0 = 0, Out1 = 1, Out2 = 0, Out3 = 0 });
            comRS485.Add(new OutRs485() { Description = "RS485_X4_2", Out0 = 0, Out1 = 0, Out2 = 1, Out3 = 0 });

            foreach (OutRs485 com in comRS485)
            {
                var outs = new List<int>();
                if (com.Out0 == 1)
                    outs.Add(0);
                if (com.Out1 == 1)
                    outs.Add(1);
                if (com.Out2 == 1)
                    outs.Add(2);
                if (com.Out3 == 1)
                    outs.Add(3);

                if (outs.Count > 0)
                    tower.IO.DO.OnWait(250, outs.ToArray());

                SamplerWithCancel((p) =>
                {
                    var versionEkor = ekor.ReadVersion();                                  
                    return true;
                }, string.Format("Error de comunicaciones por el puerto {0} ", com.Description), 2, 100, 0, true, true);

                Resultado.Set(string.Format("TEST {0}", com.Description), "OK", ParamUnidad.SinUnidad);

                if (outs.Count > 0)
                    tower.IO.DO.OffWait(100, outs.ToArray());
            }

            tower.IO.DO.OnWait(250, 3);
            //ekor.Cirbus.ClosePort();
            //ekor.Cirbus.PortCom = ekorPort;

            var inputsValue = new List<EKOR_DIDO.enumPowerStates>();
            inputsValue.Add(EKOR_DIDO.enumPowerStates.VCC_OK);
            inputsValue.Add(EKOR_DIDO.enumPowerStates.VAUX_OK);

            foreach (EKOR_DIDO.enumPowerStates input in inputsValue)
                SamplerWithCancel((p) =>
                {
                    var result = ekor.ReadPowerBitState((int)input);
                    Resultado.Set(string.Format(input.ToString()), (string)result.ToString(), ParamUnidad.SinUnidad);
                    return (bool)result;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.ToString()), 3, 500, 100, false, false);

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));
        }

        public void TestTeclaSIDE()
        {
            logger.Info("Test key SIDE");

            var imageView = new ImageView("Pulse la tecla  SIDE", string.Format("{0}\\TECLA_SIDE.png", IMAGE_PATH));

            var frm = this.ShowForm(() => imageView, MessageBoxButtons.RetryCancel);
            try
            {
                SamplerWithCancel((s) =>
                {
                    if (s == 40)
                    {
                        var diagResult = Shell.MsgBox("¿DESEA CONTINUAR CON EL TEST DE TECLA?", "TEST DE TECLA", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (diagResult == DialogResult.No)
                            throw new Exception("Error en la comprobación de tecla SIDE");
                    }

                    return ekor.ReadKeyState((int)EKOR_DIDO.enumKeyboard.SIDE);
                }, string.Format("Error en la comprobación de tecla SIDE"), 100, 500, 1000);
            }
            finally
            {
                this.CloseForm(frm);
            }

            SamplerWithCancel((s) =>
            {
                return !ekor.ReadKeyState((int)EKOR_DIDO.enumKeyboard.SIDE);
            }, string.Format("Error en la comprobación de tecla SIDE, encallada al pulsarla"),20,500,1000);

            Resultado.Set("KEY_SIDE", "OK", ParamUnidad.SinUnidad);

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));
        }

        public void TestInputs()
        {
            var ActivePower = new List<int>();
            logger.Info("TEST INPUTS a 230VAC");
            tower.IO.DO.On(15); //230Vac Aux.
            ActivePower.Add(17, 18);
            internalTestInputsPrueba(ActivePower, 19, "230Vac");
            tower.IO.DO.Off(15); //230Vac Aux.

            logger.Info("TEST INPUTS a 24DCVAC");
            ActivePower.Add(20, 21);
            internalTestInputsPrueba(ActivePower, 22, "24Vdc");

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));
        }

        public void TestLeds()
        {
            var ledsPares = new List<EKOR_DIDO.enumLEDSDIDO>();
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.COM);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.IN2);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.IN4);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.IN6);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.IN8);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.IN10);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.OUT2);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.OUT4);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.ALARM2);
            ledsPares.Add(EKOR_DIDO.enumLEDSDIDO.ALARM4);

            foreach (EKOR_DIDO.enumLEDSDIDO led in ledsPares)
                ekor.ActivaLED((int)led);

            if (Shell.ShowDialog("Test LEDS PARES", () => new ImageView("Se ven los Leds como en la imagen patron", string.Format("{0}LEDS_PARES.png", IMAGE_PATH)), MessageBoxButtons.YesNo) != DialogResult.Yes)
                throw new Exception("Error Leds incorrectos");

            foreach (EKOR_DIDO.enumLEDSDIDO led in ledsPares)
                ekor.DesactivaLED((int)led);

            var ledsImpares = new List<EKOR_DIDO.enumLEDSDIDO>();
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.IN1);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.IN3);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.IN5);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.IN7);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.IN9);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.OUT1);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.OUT3);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.ALARM1);
            ledsImpares.Add(EKOR_DIDO.enumLEDSDIDO.ALARM3);

            foreach (EKOR_DIDO.enumLEDSDIDO led in ledsImpares)
                ekor.ActivaLED((int)led);

            if (Shell.ShowDialog("Test LEDS IMPARES", () => new ImageView("Se ven los Leds como en la imagen patron", string.Format("{0}LEDS_IMPARES.png", IMAGE_PATH)), MessageBoxButtons.YesNo) != DialogResult.Yes)
                throw new Exception("Error Leds incorrectos");

            foreach (EKOR_DIDO.enumLEDSDIDO led in ledsImpares)
                ekor.DesactivaLED((int)led);

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));

        }

        public void TestOutputs()
        {
            var ouputsValue = new List<KeyValuePair<InputsControl, EKOR_DIDO.enumOutputs>>();
            ouputsValue.Add(new KeyValuePair<InputsControl, EKOR_DIDO.enumOutputs>(new InputsControl() { NO = 9, NC = 10 }, EKOR_DIDO.enumOutputs.RELE1));
            ouputsValue.Add(new KeyValuePair<InputsControl, EKOR_DIDO.enumOutputs>(new InputsControl() { NO = 11, NC = 12 }, EKOR_DIDO.enumOutputs.RELE2));
            ouputsValue.Add(new KeyValuePair<InputsControl, EKOR_DIDO.enumOutputs>(new InputsControl() { NO = 13, NC = 14 }, EKOR_DIDO.enumOutputs.RELE3));
            ouputsValue.Add(new KeyValuePair<InputsControl, EKOR_DIDO.enumOutputs>(new InputsControl() { NO = 15, NC = 16 }, EKOR_DIDO.enumOutputs.RELE4));
            
            foreach (KeyValuePair<InputsControl, EKOR_DIDO.enumOutputs> output in ouputsValue)
            {
                this.Logger.InfoFormat("{0}", output.Value.ToString());

                ekor.ActivaDigitalOutput((int)output.Value);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Key.NO];
                }, string.Format("Error. no se detectado la entrada NO {0} activada de la Output {1} ACTIVADA", output.Key.NC, output.Value.ToString()), 5, 200, 200);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Key.NC];
                }, string.Format("Error. no se ha detectado la entrada NC {0} desactivada de la Output {1} ACTIVADA", output.Key.NO, output.Value.ToString()), 5, 200, 200);

                ekor.DesactivaDigitalOutput((int)output.Value);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Key.NC];
                }, string.Format("Error. no se detectado la entrada NC {0} activada de la Output {1} DESACTIVADA", output.Key.NC, output.Value.ToString()), 5, 200, 200);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Key.NO];
                }, string.Format("Error. no se ha detectado la entrada NO {0} desactivada de la Output {1} DESACTIVADA", output.Key.NO, output.Value.ToString()), 5, 200, 200);

                Resultado.Set(string.Format("{0} N.O.", output.Value.ToString()), "OK", ParamUnidad.SinUnidad);
                Resultado.Set(string.Format("{0} N.C.", output.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));
        }

        public void TestCalibrationClock()
        {
            tower.HP53131A
                .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
                .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 2.4)
                .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            tower.MUX.Reset();
            tower.MUX.Connect(InputMuxEnum.IN2, Instruments.IO.Outputs.FreCH1);

            try
            {
                TestMeasureBase(defs,
                (step) =>
                {
                    double temperature = ekor.ReadTemperatureI2C();
                    Resultado.Set(Params.TEMP.Null.Ajuste.TestPoint("RELOJ").Name, temperature, ParamUnidad.Grados);

                    ekor.StartSignalClok(015);

                    var medidaFreqMed = tower.HP53131A.RunMeasure(HP53131A.Variables.FREQ, 8000, HP53131A.Channels.CH1);
                    Resultado.Set(Params.FREQ.Null.Ajuste.TestPoint("RELOJ").Name, medidaFreqMed.Value, ParamUnidad.Hz);

                    SamplerWithCancel((p) =>
                    {
                        ekor.ReadVersion();
                        return true;
                    }, "Error de comunicaciones despues de esperar 10seg. al enviar CLK_DE2ZACFAB", 2, 3000, 8000, true, true);

                    var calibrationClk = ekor.CalibrationClock(medidaFreqMed.Value, temperature);

                    return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
                }, 0, 2, 1000);
            }
            finally
            {
                tower.MUX.Reset();


                var defsTicks = new List<AdjustValueDef>();

                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("MAX_TICK", ParamUnidad.Hz).Null.Ajuste.Name, Min = Params.PARAMETER("MAX_TICKS", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("MAX_TICKS", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.Hz });
                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("AVG_TICK", ParamUnidad.Hz).Null.Ajuste.Name, Min = Params.PARAMETER("AVG_TICKS", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("AVG_TICKS", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.Hz });
                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("MIN_TICK", ParamUnidad.Hz).Null.Ajuste.Name, Min = Params.PARAMETER("MIN_TICKS", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("MIN_TICKS", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.Hz });
                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("TIME_TICK", ParamUnidad.s).Null.Ajuste.Name, Min = Params.PARAMETER("TIME_TICKS", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("TIME_TICKS", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.s });

                TestMeasureBase(defsTicks,
                (step) =>
                {
                    var ticks = ekor.ReadCountTicksLastTest();
                    return new double[] { ticks.MaxTick, ticks.AvgTick, ticks.MinTick, ticks.TimeTick };
                }, 0, 1, 1000);
            }

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));
        }

        public void TestCustomize()
        {
            ekor.WriteSerialNumber(TestInfo.NumSerie);

            ekor.WriteBastidor(TestInfo.NumBastidor.Value.ToString());

            var rdmNumberToWriteEEPROM = new Random().Next(10, 100);
            if (rdmNumberToWriteEEPROM == ekor.ReadEEPROM())
                rdmNumberToWriteEEPROM += 1;
            ekor.WriteEEPROM(rdmNumberToWriteEEPROM);
            Delay(500, "Escribiendo en EEPROM");

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            Delay(3000, "Reset");
            tower.IO.DO.Off(23);
            tower.IO.DO.On(OUT_MODULO_USB);
            tower.Chroma.ApplyAndWaitStabilisation();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Other("USB").ConCarga.Vnom.Name, 1, ParamUnidad.V), Consignas.GetDouble(Params.I.Other("USB").ConCarga.Name, 1, ParamUnidad.A));
            Delay(1000, "inicio reset");

            var bastidor = ekor.ReadBastidor();

            var bastidorInfo = TestInfo.NumBastidor.Value.ToString().PadLeft(8, '0');

            Assert.IsTrue(bastidor.Trim() == bastidorInfo, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, el numero de bastidor leido no corresponde con el grabado en el equipo"));

            var numserie = ekor.ReadSerialNumber();

            Assert.IsTrue(numserie.Trim() == TestInfo.NumSerie.Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, el numero de serie leido no corresponde con el grabado en el equipo"));

            var reading = ekor.ReadEEPROM();

            Assert.AreEqual(reading, rdmNumberToWriteEEPROM, Error().UUT.EEPROM.NO_CORRESPONDE_CON_ENVIO("Error en EEPROM, valor leído diferente al escrito anteriormente"));

            Resultado.Set("EEPROM", "OK", ParamUnidad.SinUnidad);

            TestDesviationClock();

            var modelVersion = ekor.ReadVersion();            

            EKOR_DIDO.ModelHardware model = EKOR_DIDO.ModelHardware.DIDO;

            ekor.WriteConfigurationDefaultFab(model);

            Delay(500, "Grabando configuración");

            ResetUSB();

            var actionPortSearch = new WMISerialPortInitialize();
            actionPortSearch.SerialPortToFind = "Ekor MCBSTM32 MSD/VCOM";
            actionPortSearch.DataOutput.Key = "ekorVirtualCom";
            actionPortSearch.Execute();

            var portUSB = Convert.ToByte(GetVariable<string>("ekorVirtualCom", "ekorVirtualCom").Replace("COM", ""));

            var modelRead = ekor.ReadModelModbus(portUSB);
            Assert.AreEqual(ConstantsParameters.Identification.MODELO_CLIENTE, modelRead.ToString(), Identificacion.MODELO_CLIENTE, Error().UUT.COMUNICACIONES.NO_CORRESPONDE_CON_ENVIO("Modelo Cliente leido diferente al especificado"), ParamUnidad.SinUnidad);
            var deviceRead = ekor.ReadDeviceModbus(portUSB);
            Assert.AreEqual(ConstantsParameters.Identification.MODELO, deviceRead.ToString(), Identificacion.MODELO, Error().UUT.HARDWARE.NO_CORRESPONDE_CON_ENVIO("Modelo Hardware Cliente leido diferente al grabado"), ParamUnidad.SinUnidad);

            DriveInfo usbDrive = null;

            SamplerWithCancel((p) =>
            {
                if (p == 30)
                    ResetUSB();

                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKOR_DISK", ParamUnidad.SinUnidad).Trim())
                    {
                        usbDrive = drive;
                        break;
                    }

                if (usbDrive == null)
                    throw new Exception("Error no se detecta la unidad USB en el S.O al finalizar el equipo");

                logger.InfoFormat("USB_DRIVE RottDirectory: {0}", usbDrive.RootDirectory.Name);
                Resultado.Set("USB_DRIVE_FINISH", usbDrive.VolumeLabel, ParamUnidad.SinUnidad);

                return !string.IsNullOrEmpty(usbDrive.RootDirectory.Name);

            }, "Error No se ha encontrado el dispositivo USB del EKOR al finalizar el equipo", 60, 1000, 2500, false, false);

            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", ekor.Cirbus.PortCom));
        }

        public void TestFinish()
        {
            if (tower != null)
            {
                try
                {
                    if (ekor != null)
                        ekor.Dispose();
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("Excepcion en el disposed ekor por {0}", ex.Message);
                }

                tower.ShutdownSourcesQuick();

                tower.IO.DO.Off(OUT_MODULO_USB, OUT_CLK, 17, 18, 19, 20, 21, 23, 0, 1, 2, 3);

                tower.Dispose();
            }
        }

        public void TestBattery()
        {            
            var param = new AdjustValueDef(Params.V.Null.Bateria, ParamUnidad.V);
            var vRef = Consignas.GetDouble(Params.V.Null.TestPoint("REF_BATTERY").Name, 3, ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                var batteryLevel = ekor.ReadBattery(vRef);
                return batteryLevel;
            }, 0, 3, 2000);
        }

        public void TestEEPROM()
        {
            var rdm = new Random();
            var number = rdm.Next(10, 100);

            if (number == ekor.ReadEEPROM())
                number += 1;

            ekor.WriteEEPROM(number);

            var reading = ekor.ReadEEPROM();

            Assert.AreEqual(reading, number, Error().UUT.EEPROM.NO_CORRESPONDE_CON_ENVIO("Error EEPROM, valor leído diferente al escrito anteriormente"));
        }      
        

        
        //***************************************

        public struct OutRs485
        {
            public string Description;
            public byte Out0;
            public byte Out1;
            public byte Out2;
            public byte Out3;
        }

        public struct InputsControl
        {
            public byte NO;
            public byte NC;
        }

        private void internalTestInputs(List<int> ActivePower, int outPar, string typeTest)
        {
            tower.IO.DO.On(ActivePower.ToArray());

            Delay(1000, "");

            var inputsValue = new List<KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>>();
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN1", EKOR_DIDO.enumInputsDIDO.IN1));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN2", EKOR_DIDO.enumInputsDIDO.IN2));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN3", EKOR_DIDO.enumInputsDIDO.IN3));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN4", EKOR_DIDO.enumInputsDIDO.IN4));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN5", EKOR_DIDO.enumInputsDIDO.IN5));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN6", EKOR_DIDO.enumInputsDIDO.IN6));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN7", EKOR_DIDO.enumInputsDIDO.IN7));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN8", EKOR_DIDO.enumInputsDIDO.IN8));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN9", EKOR_DIDO.enumInputsDIDO.IN9));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN10", EKOR_DIDO.enumInputsDIDO.IN10));

            foreach (KeyValuePair<string, EKOR_DIDO.enumInputsDIDO> input in inputsValue)
            {
                this.Logger.InfoFormat("Test entrada {0}", input.Key);

                if (input.Key.ToString().Contains("IMPAR"))
                    tower.IO.DO.Off(outPar);
                else
                    tower.IO.DO.On(outPar);

                SamplerWithCancel((p) =>
                {
                    return ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Key), 3, 500, 200, false, false);

                if (input.Key.ToString().Contains("IMPAR"))
                    tower.IO.DO.On(outPar);
                else
                    tower.IO.DO.Off(outPar); 

                SamplerWithCancel((p) =>
                {
                    return !ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Key), 3, 500, 200, false, false);

                Resultado.Set(string.Format("{0}_{1}", input.Key, typeTest), "OK", ParamUnidad.SinUnidad);
            }

            tower.IO.DO.Off(ActivePower.ToArray());

            Delay(1000, "");
        }

        private void internalTestInputsPrueba(List<int> ActivePower, int outPar, string typeTest)
        {
            tower.IO.DO.On(ActivePower.ToArray());

            Delay(200, "");

            var inputsValue = new List<KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>>();
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN1", EKOR_DIDO.enumInputsDIDO.IN1));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN2", EKOR_DIDO.enumInputsDIDO.IN2));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN3", EKOR_DIDO.enumInputsDIDO.IN3));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN4", EKOR_DIDO.enumInputsDIDO.IN4));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN5", EKOR_DIDO.enumInputsDIDO.IN5));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN6", EKOR_DIDO.enumInputsDIDO.IN6));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN7", EKOR_DIDO.enumInputsDIDO.IN7));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN8", EKOR_DIDO.enumInputsDIDO.IN8));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("IMPAR_IN9", EKOR_DIDO.enumInputsDIDO.IN9));
            inputsValue.Add(new KeyValuePair<string, EKOR_DIDO.enumInputsDIDO>("PAR_IN10", EKOR_DIDO.enumInputsDIDO.IN10));

            //tower.IO.DO.PulseOn(1000, outPar);
            tower.IO.DO.Off(outPar);
            foreach (KeyValuePair<string, EKOR_DIDO.enumInputsDIDO> input in inputsValue)
            {
                var prueba = input.Key.Contains("IMPAR") ? "Activada" : "Desactivada";
                this.Logger.InfoFormat("Test entrada {0} {1}", input.Key, prueba);

                //if (input.Key.ToString().Contains("IMPAR"))
                //    tower.IO.DO.Off(outPar);
                //else
                //    tower.IO.DO.On(outPar);

                SamplerWithCancel((p) =>
                {
                    if (input.Key.Contains("IMPAR"))
                        return ekor.ReadDigitalInput((int)input.Value);
                    else
                        return !ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} {1}", input.Key, prueba), 3, 500, 0, false, false);
            }

            tower.IO.DO.On(outPar);
            foreach (KeyValuePair<string, EKOR_DIDO.enumInputsDIDO> input in inputsValue)
            {
                var prueba = !input.Key.Contains("IMPAR") ? "Activada" : "Desactivada";
                this.Logger.InfoFormat("Test entrada {0} {1}", input.Key, prueba);
                //if (input.Key.ToString().Contains("IMPAR"))
                //    tower.IO.DO.On(outPar);
                //else
                //    tower.IO.DO.Off(outPar); 

                SamplerWithCancel((p) =>
                {
                    if (input.Key.Contains("IMPAR"))
                        return !ekor.ReadDigitalInput((int)input.Value);
                    else
                        return ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} {1}", input.Key, prueba), 3, 500, 0, false, false);

                Resultado.Set(string.Format("{0}_{1}", input.Key, typeTest), "OK", ParamUnidad.SinUnidad);
            }

            tower.IO.DO.Off(ActivePower.ToArray());

            Delay(200, "");
        }

        private void TestDesviationClock()
        {
            var timeEkor = ekor.ReadDateTime();

            logger.InfoFormat("EKOR_WTP FECHA:{0} HORA:{1}", timeEkor.ToShortDateString(), timeEkor.ToLongTimeString());
            logger.InfoFormat("PC FECHA:{0} HORA:{1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());

            double timeDiff = (DateTime.Now.Subtract(timeEkor)).TotalSeconds;

            logger.InfoFormat("DERIVA RELOJ:{0}", timeDiff);

            Assert.AreBetween(Params.TIME.Null.Offset.Name, timeDiff, Params.TIME.Null.Offset.Min(), Params.TIME.Null.Offset.Max(), Error().UUT.MEDIDA.MARGENES("Error. El equipo tiene una deriva de reloj fuera de margenes"), ParamUnidad.s);

            ekor.WriteDateTime();
        }

        private void ResetUSB(Action func = null)
        {
            try
            {
                ekor.Cirbus.ClosePort();
            }
            catch (Exception)
            {

            }

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(2500, "");

            if (func != null)
                func();

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.TestPoint("USB_CARGA").Vnom.Name, 5, ParamUnidad.V), Consignas.GetDouble(Params.I.Null.TestPoint("USB_CARGA").Vnom.Name, 1, ParamUnidad.A));

            Delay(4000, "Esperando conexión USB");
        }
    }
}
