﻿using Instruments.SCPI;

namespace Instruments.Measure
{
    public class MTX3293SCPI : SCPIDevice
    {
        internal MTX3293SCPI(MTX3293 device)
            : base(device)
        {
        }       
    }
}
