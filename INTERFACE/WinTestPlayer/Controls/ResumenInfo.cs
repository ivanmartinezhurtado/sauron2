﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dezac.Entities;

namespace WinTestPlayer.Controls
{
    public partial class ResumenInfo : UserControl
    {
        private MainViewController controller;

        public ResumenInfo()
        {
            InitializeComponent();
        }

        public void SetController(MainViewController controller)
        {
            this.controller = controller;
            this.controller.InfoChanged += RefreshView;
        }

        public void RefreshView(object sender, EventArgs e)
        {
            if (controller.Operario == null)
            {
                lblOperario.Text = "Ningún operario identificado!";
                lblOperario.UseStyleColors = true;
            }
            else
            {
                lblOperario.Text = string.Format("{0}", controller.Operario.IDOPERARIO);
                lblOperario.UseStyleColors = false;
            }
        }
    }
}
