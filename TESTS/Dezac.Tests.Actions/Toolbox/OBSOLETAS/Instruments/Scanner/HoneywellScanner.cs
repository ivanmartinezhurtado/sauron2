﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Extensions;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(HoneywellScannerReadActionDescription))]
    
    public class HoneywellScannerRead : ActionBase
    {
        [Category("Configuracion")]
        [DefaultValue("$HoneywellPort")]
        [Description("Nombre del puerto serie encontado por WMI o valor del Puerto serie")]
        public string PortComName { get; set; }

        [Category("Configuracion")]
        [DefaultValue("NameValue")]
        [Description("Nombre de la variable donde se guardara el valor del codigo  leido")]
        public string VariableName { get; set; }

        [CategoryAttribute("Configuracion Variables a assignar")]
        public VariableItem[] AssignedVarNames { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                testBase.Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TEST_BASE");

            if (string.IsNullOrEmpty(PortComName))
                testBase.Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA("PortComName").Throw();

            var result = testBase.ScannerUSBComm(VariablesTools.ResolveText(PortComName)).Split(new string[] { "<BR>" }, System.StringSplitOptions.RemoveEmptyEntries); //"CIRCUTOR<BR>125517/8<BR>4621722710<BR>2018/28<BR>";

            Context.ResultList.Add(string.Format("{0}={1}", VariableName, result));
            Context.Variables.AddOrUpdate(VariableName, result);

            var variables = AssignedVarNames;
            var i = 0;
            foreach (var item in variables)
            {
                var variableLoad = result[i].Trim();
                Logger.InfoFormat("Variable:{0}  Assigned Value:{1}", item.VariableName, variableLoad);
                SetVariable(item.VariableName, variableLoad);
                i++;
            }
        }
    }

    public class HoneywellScannerReadActionDescription : ActionDescription
    {
        public override string Name { get { return "HoneywellScannerRead"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
        public override string Dependencies { get { return "InitTestBase;WMISerialPortInitialize"; } }
    }

}
