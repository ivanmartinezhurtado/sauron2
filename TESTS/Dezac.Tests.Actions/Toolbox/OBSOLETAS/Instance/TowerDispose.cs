﻿using Dezac.Tests.Services;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(TowerDisposeDescription))]
    
    public class TowerDispose : ActionBase
    {       
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            tower.Dispose();
        }
    }

    public class TowerDisposeDescription : ActionDescription
    { 
        public override string Name { get { return "TowerDispose"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.chess_tower_icon; } }
        public override string Description { get { return "Action que cierra los puertos y libera la memoria de la clase torre"; } }
    }
}
