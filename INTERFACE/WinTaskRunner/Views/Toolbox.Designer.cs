﻿namespace WinTaskRunner.Views
{
    partial class Toolbox
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.treeView = new Telerik.WinControls.UI.RadTreeView();
            this.searchBox = new Telerik.WinControls.UI.RadTextBox();
            this.mnuNodo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuVerProductos = new System.Windows.Forms.ToolStripMenuItem();
            this.verInformacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchBox)).BeginInit();
            this.mnuNodo.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // treeView
            // 
            this.treeView.AllowDragDrop = true;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageList = this.imageList;
            this.treeView.Location = new System.Drawing.Point(0, 20);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(400, 549);
            this.treeView.SpacingBetweenNodes = -1;
            this.treeView.TabIndex = 1;
            this.treeView.Text = "treeView";
            this.treeView.DragEnding += new Telerik.WinControls.UI.RadTreeView.DragEndingHandler(this.treeView_DragEnding);
            this.treeView.DragStarting += new Telerik.WinControls.UI.RadTreeView.DragStartingHandler(this.treeView_DragStarting);
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView_DragEnter);
            // 
            // searchBox
            // 
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox.Location = new System.Drawing.Point(0, 0);
            this.searchBox.Name = "searchBox";
            this.searchBox.NullText = "Buscar...";
            this.searchBox.Size = new System.Drawing.Size(400, 20);
            this.searchBox.TabIndex = 0;
            this.searchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            // 
            // mnuNodo
            // 
            this.mnuNodo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuVerProductos,
            this.verInformacionToolStripMenuItem});
            this.mnuNodo.Name = "mnuNodo";
            this.mnuNodo.Size = new System.Drawing.Size(159, 48);
            // 
            // mnuVerProductos
            // 
            this.mnuVerProductos.Name = "mnuVerProductos";
            this.mnuVerProductos.Size = new System.Drawing.Size(158, 22);
            this.mnuVerProductos.Text = "Ver Productos";
            this.mnuVerProductos.Click += new System.EventHandler(this.mnuVerProductos_Click);
            // 
            // verInformacionToolStripMenuItem
            // 
            this.verInformacionToolStripMenuItem.Name = "verInformacionToolStripMenuItem";
            this.verInformacionToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.verInformacionToolStripMenuItem.Text = "Ver Informacion";
            this.verInformacionToolStripMenuItem.Click += new System.EventHandler(this.verInformacionToolStripMenuItem_Click);
            // 
            // Toolbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.searchBox);
            this.Name = "Toolbox";
            this.Size = new System.Drawing.Size(400, 569);
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchBox)).EndInit();
            this.mnuNodo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private Telerik.WinControls.UI.RadTextBox searchBox;
        private Telerik.WinControls.UI.RadTreeView treeView;
        private System.Windows.Forms.ContextMenuStrip mnuNodo;
        private System.Windows.Forms.ToolStripMenuItem mnuVerProductos;
        private System.Windows.Forms.ToolStripMenuItem verInformacionToolStripMenuItem;
    }
}

