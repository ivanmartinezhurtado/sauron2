﻿namespace WinTestPlayer.Views
{
    partial class BoxReportConfigView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtCajaActual = new Telerik.WinControls.UI.RadTextBox();
            this.txtUnidadesCaja = new Telerik.WinControls.UI.RadTextBox();
            this.lblCommand = new System.Windows.Forms.Label();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCajaActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnidadesCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.radPanel1.Controls.Add(this.pictureBox2);
            this.radPanel1.Controls.Add(this.txtCajaActual);
            this.radPanel1.Controls.Add(this.txtUnidadesCaja);
            this.radPanel1.Controls.Add(this.lblCommand);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(431, 350);
            this.radPanel1.TabIndex = 6;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WinTestPlayer.Properties.Resources.DropBoxicon32x32;
            this.pictureBox2.Location = new System.Drawing.Point(377, 23);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 35);
            this.pictureBox2.TabIndex = 28;
            this.pictureBox2.TabStop = false;
            // 
            // txtCajaActual
            // 
            this.txtCajaActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtCajaActual.Location = new System.Drawing.Point(214, 247);
            this.txtCajaActual.Name = "txtCajaActual";
            this.txtCajaActual.Size = new System.Drawing.Size(119, 21);
            this.txtCajaActual.TabIndex = 27;
            this.txtCajaActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUnidadesCaja
            // 
            this.txtUnidadesCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtUnidadesCaja.Location = new System.Drawing.Point(214, 160);
            this.txtUnidadesCaja.Name = "txtUnidadesCaja";
            this.txtUnidadesCaja.Size = new System.Drawing.Size(119, 21);
            this.txtUnidadesCaja.TabIndex = 26;
            this.txtUnidadesCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCommand
            // 
            this.lblCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCommand.BackColor = System.Drawing.Color.Transparent;
            this.lblCommand.CausesValidation = false;
            this.lblCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCommand.Location = new System.Drawing.Point(6, 78);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(418, 32);
            this.lblCommand.TabIndex = 23;
            this.lblCommand.Text = "Configurar el Box Report";
            this.lblCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel1.Location = new System.Drawing.Point(101, 247);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(95, 22);
            this.radLabel1.TabIndex = 19;
            this.radLabel1.Text = "Caja actual:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinTestPlayer.Properties.Resources.dezac;
            this.pictureBox1.Location = new System.Drawing.Point(21, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 43);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel3.Location = new System.Drawing.Point(56, 157);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(140, 22);
            this.radLabel3.TabIndex = 7;
            this.radLabel3.Text = "Unidades por caja";
            // 
            // BoxReportConfigView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Name = "BoxReportConfigView";
            this.Size = new System.Drawing.Size(431, 350);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCajaActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnidadesCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Telerik.WinControls.UI.RadTextBox txtCajaActual;
        private Telerik.WinControls.UI.RadTextBox txtUnidadesCaja;

    }
}
