﻿using Dezac.Core.Exceptions;
using Instruments.Utility;
using log4net;
using System.Threading;

namespace Instruments.PowerSource
{
    public abstract class PowerSourceDCBase<T> : PowerSourceBase<T>
    {
        public class PowerSourceDCData<T>
        {
            public PowerSourceDCData(T t)
            {
                Voltage = t;
                Current = t;
            }
            public T Voltage { get; set; }
            public T Current { get; set; }
        }

        public PowerSourceDCData<T> Presets { get; internal set; }
        internal PowerSourceDCData<T> DevicePreset { get; set; }
        internal PowerSourceDCData<T> DeviceData { get; set; }

        public PowerSourceDCBase(T t, ILog logger): base(t, logger)
        {
            Presets = new PowerSourceDCData<T>(t);
            DeviceData = new PowerSourceDCData<T>(t);
            DevicePreset = new PowerSourceDCData<T>(t);
            Status = new PowerSourceStatus();
            Tolerance = 0.5;
            TimeOutStabilitation = 30000;
            RetryStabilisation = 3;
            VoltageOffMin = 20;
            Logger = logger ?? _logger;
        }

        public void Apply(bool forceApply = false)
        {
            ApplyPresetsBase(forceApply);
        }

        public void ApplyPresets(T voltage, T current, bool forceApply = false)
        {
            this.Presets.Voltage = voltage;
            this.Presets.Current = current;
            ApplyPresetsBase(forceApply);
        }

        public bool ApplyAndWaitStabilisation(bool forceApply = false)
        {
            return internalApplyAndWaitStabilisation(forceApply);
        }

        public bool ApplyPresetsAndWaitStabilisation(T voltage, T current, bool forceApply = false)
        {
            this.Presets.Voltage = voltage;
            this.Presets.Current = current;
            return internalApplyAndWaitStabilisation(forceApply);
        }

        public override void Reset()
        {
            var cmd = new Command<Commands>(Commands.RESET, true);
            SendCommand(cmd);
            IsOn = false;
        }

        //*******************************************************************

        protected override void ApplyPresetsBase(bool forceApply)
        {
            bool apply = false;

            var result = ReadStatus().Value;
            if (result.HasError || result.HasWarning)
            {
                Logger.InfoFormat("Starting Reset in PowerSource by Error detection");
                Reset();
                Thread.Sleep(2000);
                forceApply = true;
            }

            if (forceApply || !DevicePreset.Voltage.Equals(Presets.Voltage))
            {
                var cmd = new Command<Commands>(Commands.SET_VOLTAGE, true);
                Logger.InfoFormat("Apply Voltage: {0}", Presets.Voltage);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (forceApply || !DevicePreset.Current.Equals(Presets.Current))
            {
                var cmd = new Command<Commands>(Commands.SET_CURRENT, true);
                Logger.InfoFormat("Apply Current: {0}", Presets.Current);
                if (SendCommand(cmd))
                    apply = true;
            }

            if (apply)
            {
                var cmd = new Command<Commands>(Commands.APPLY_CONFIG, true);
                SendCommand(cmd);
                IsOn = true;
            }

            if (Status.HasError)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE($"ERROR INTERNO FUENTE {Status.ErrorDescription}").Throw();
        }
    }
}
