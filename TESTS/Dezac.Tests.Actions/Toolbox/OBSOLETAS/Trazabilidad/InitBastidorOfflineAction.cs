﻿using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Drawing;
using System.Windows.Forms;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(InitBastidorOfflineActionDescription))]
    
    public class InitBastidorOfflineAction : ActionBase
    {
        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null)
                throw new Exception("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            UInt64 bastidor = 0;

            if (data.TestInfo.NumBastidor.HasValue)
                bastidor = data.TestInfo.NumBastidor.Value;
            else
            {
                bastidor = GetIntroBastiorByUser(null);

                data.TestInfo.NumBastidor = bastidor;

                SequenceContext.Current.Variables.AddOrUpdate(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor);
            }
        }

        private UInt64 GetIntroBastiorByUser(string device)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            if (string.IsNullOrEmpty(device))
                device = " a testear";

            string msg = context.NumInstance > 1 || context.RunningInstances > 1 ?
                string.Format("ENTRADA DEL NUMERO DE BASTIDOR DEL TEST {0}", context.NumInstance) :
                "ENTRADA DEL NUMERO DE BASTIDOR";

            var bastidor = iShell.ShowDialog<string>(msg, () =>
            {
                return new InputKeyBoard(string.Format("Leer código de barras del bastidor del equipo {0}", device));
            }
           , MessageBoxButtons.OKCancel, (c, d) =>
           {
               if (d == DialogResult.OK)
                   return ((InputKeyBoard)c).TextValue;

               return string.Empty;
           });

            if (string.IsNullOrEmpty(bastidor))
                throw new Exception("El usuario ha cancelado la entrada del bastidor!");

            if (bastidor.Length > 12)
                throw new Exception("La longitud del bastidor introducido es incorrecta");

            return Convert.ToUInt64(bastidor);
        }
    }

    public class InitBastidorOfflineActionDescription : ActionDescription
    {
        public override string Name { get { return "InitBastidorOfflineAction"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
    }
}
