﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(Prueba_LAC25Description))]
    

    public class Prueba_LAC25 : ActionBase
    {
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();;

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");

            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(24, 0);

            tower.Actuator_LAC25.RunInitialization();

            tower.Actuator_LAC25.SetPIDConstants(Instruments.Actuators.SMAC_LAC25.AxisActuator.LINEAL, 65, 2970, 91, -1330, 10, 5000);
            tower.Actuator_LAC25.SetPIDConstants(Instruments.Actuators.SMAC_LAC25.AxisActuator.ROTATIVO, 45, 162, 1, 0, 10, 5000);

            tower.Actuator_LAC25.WriteAndRunHomingLineal();
            tower.Actuator_LAC25.WriteAndRunHomingRotative();

            //tower.Actuator_LAC25.WriteAndRunFindSelector(30, 231072, 5243, 32767, 9243, 5243, 5000, 5000, 30000, 500, 5000);

            tower.Actuator_LAC25.MotorOff();
        }
    }

    public class Prueba_LAC25Description : ActionDescription
    {
        public override string Name { get { return "LAC_25 Testing"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Transport_Engine_icon; } }
    }
}
