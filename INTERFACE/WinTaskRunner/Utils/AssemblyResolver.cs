﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WinTaskRunner.Utils
{
    public static class AssemblyResolver
    {
        public static Assembly Resolve(string name)
        {
            AssemblyName asm = new AssemblyName(name);

            name = asm.Name;
            if (!name.EndsWith(".dll", StringComparison.InvariantCultureIgnoreCase))
                name += ".dll";

            string pathAssemblies = ConfigurationManager.AppSettings["PathAssemblies"];

            if (string.IsNullOrEmpty(pathAssemblies))
                return null;

            var assemblies = pathAssemblies.Split(';');

            foreach (var path in assemblies)
            {
                string fileName = Path.Combine(path, name);

                if (File.Exists(fileName))
                    return Assembly.LoadFrom(fileName);
            }

            return null;
        }

    }
}
