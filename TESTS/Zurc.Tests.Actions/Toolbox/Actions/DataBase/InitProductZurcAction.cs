﻿using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Zurc.Data;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.0)]
    [DesignerAction(Type = typeof(InitProductZurcActionDescription))]
    public class InitProductZurcAction : ActionBase
    {
        public string CodigoArticulo { get; set; }
        public int Version { get; set; }

        public string Fase { get; set; }
        public string IdEmpleado { get; set; }
        [DefaultValue(ModoTest.SAT)]
        public ModoTest ModoTest { get; set; }

        public bool PedirNumOrden { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;

            //if (context.RunMode == RunMode.Release)
            //    return;

            int NumProducto = GetVariable<int>("NumProducto", 0);

            Version = GetVariable<int>("Version", Version);

            var value = GetVariable("NumFase");
            if (value != null)
                Fase = value.ToString();

            string numOrden = null;

            value = GetVariable("NumOrden");
            if (value != null)
                numOrden = value.ToString();

            value = GetVariable("IdEmpleado");
            if (value != null)
                IdEmpleado = value.ToString();

            Producto producto;
            Empleado operario;
            OrdenFabricacion orden = null;
            List<GrupoParametrizacion> parametros;
            string fase = Fase;
            List<string> direcciones = null;
            List<string> Atributos = null;
            string Ean = string.Empty;
            string EanMarca = string.Empty;
            Dictionary<string, string> DescripcionLenguagesCliente = null;
            string codigoCircutor = null;
            //string costumerCode2 = null;
            string TestLocationDescription = null;
            int? versionReport = null;

            using (var svc = new ZurcService())
            {
                if (string.IsNullOrEmpty(numOrden) && PedirNumOrden)
                {
                    var iShell = context.Services.Get<IShell>();
                    numOrden = iShell.ShowDialog<string>("ATENCIÓN", () =>
                    {
                        return new InputKeyBoard("Lee el Nº de Orden");
                    }
                    , MessageBoxButtons.OKCancel, (c, d) =>
                    {
                        if (d == DialogResult.OK)
                            return ((InputKeyBoard)c).TextValue;

                        return string.Empty;
                    });

                    if (string.IsNullOrEmpty(numOrden) && PedirNumOrden)
                        throw new Exception("Se ha cancelado la lectura del nº de Orden");
                }

                if (!string.IsNullOrEmpty(numOrden))
                    orden = svc.GetOrdenByCodigo(numOrden);

                //if (!string.IsNullOrEmpty(IdEmpleado))
                //{
                //    operario = svc.GetOperarioByIdEmpleado(IdEmpleado);
                //    if (operario == null)
                //        throw new Exception("Operario no encontrado");

                //    if (orden == null)
                //    {
                //        var dmf = svc.GetLastDiarioMarcaje(operario.IDEMPLEADO);
                //        if (dmf != null && dmf.HOJAMARCAJEFASE != null)
                //        {
                //            orden = svc.GetOrden(dmf.HOJAMARCAJEFASE.NUMORDEN);
                //            if (orden == null)
                //                throw new Exception("Orden inexistente");

                //            fase = dmf.IDFASE;
                //        }
                //    }
                //}
                //else
                //    operario = null;

                if (orden != null)
                {
                    NumProducto = orden.ArticuloId;
                    Version = orden.Version;
                }
                else if (!string.IsNullOrEmpty(CodigoArticulo))
                {
                    var articulo = svc.GetArticuloByCodigo(CodigoArticulo);
                    if (articulo != null)
                        NumProducto = articulo.Id;
                }
                
                producto = svc.GetProductoByIOrdenFab(NumProducto, Version);

                if (producto == null)
                    throw new Exception("Producto inexistente");

                if (producto.Articulo.Familia == null)
                    throw new Exception("Producto incorrecto o no tiene familia asignada");

                parametros = svc.GetGruposParametrizacion(producto, fase.ToString());
                if (parametros == null)
                    throw new Exception("Grupos de parametrización inexistentes");

                //if (producto.NUMDIRECCION.HasValue)
                //    direcciones = svc.GetDirecciones(producto.NUMDIRECCION.Value);

                //codigoCircutor = svc.GetCodigoCircutor(producto.NUMPRODUCTO);
                //costumerCode2 = svc.GetCostumerCode2(producto.NUMPRODUCTO);
                //Atributos = svc.GetAtributosCliente(producto.NUMPRODUCTO, codigoCircutor);
                //Ean = svc.GetEANCode(producto.NUMPRODUCTO, codigoCircutor);
                //EanMarca = svc.GetEANMarcaCode(producto.NUMPRODUCTO, codigoCircutor);
                //DescripcionLenguagesCliente = svc.GetDescripcionLenguagesCliente(producto.NUMPRODUCTO, codigoCircutor);

                //var lastTest = svc.GetLastAutoTest(Dns.GetHostName());
                //if (lastTest != null)       
                //    TestLocationDescription = lastTest.Item2;

                //versionReport = svc.GetVersionTestReport(producto.T_FAMILIA.NUMFAMILIA);
                var configuracion = parametros.Where(p => p.TipoGrupoId == 5).FirstOrDefault();
                if (configuracion != null)
                {
                    var versionReportProduct = configuracion.Valores.Where(p => p.Parametro.Nombre == "TEST_REPORT_VERSION").FirstOrDefault();
                    if (versionReportProduct != null)
                        versionReport = Convert.ToInt32(versionReportProduct.Valor);
                }
                if (versionReport == null)
                    versionReport = 1;
            }

            if (NumProducto > 0 && NumProducto != producto.ArticuloId)
                throw new Exception("El producto configurado en la acción no coincide con el de la orden");

            if (Version > 0 && Version != producto.Version)
                throw new Exception("La versión del producto configurado en la acción no coincide con el de la orden");

            var sequenceName = GetVariable("SequenceName");
            var sequenceVersion = GetVariable("SequenceVersion");

            var data = new TestContextModel
            {
                IdEmpleado = IdEmpleado,
                IdOperario = IdEmpleado,
                NumFamilia = producto.Articulo.FamiliaId,
                NumProducto = producto.ArticuloId,
                ModelLeader = producto.Articulo.Familia.EmpleadoId,    
                Version = producto.Version,
                Codigo = producto.Articulo.Codigo,
                IdFase = fase,
                modoPlayer = ModoTest,
                Direcciones = direcciones,
                Atributos = Atributos,
                DescripcionLenguagesCliente = DescripcionLenguagesCliente,
                PC = Dns.GetHostName(),
                LocationTest = TestLocationDescription,
                DescFamilia = producto.Articulo.Descripcion,
                EAN = Ean,
                EAN_MARCA = EanMarca,
                SequenceName = sequenceName != null ? sequenceName.ToString()  : string.Format("{0}_{1}_{2}",producto.ArticuloId, producto.Version, fase),
                SequenceVersion = sequenceVersion != null ? sequenceVersion.ToString() : "1",
                SauronInterface = Application.ProductName,
                SauronVersion = Application.ProductVersion,
            };

            data.TestInfo.CodCircutor = codigoCircutor;
            data.TestInfo.ReportVersion = versionReport.Value;

            if (orden != null)
            {
                data.NumOrden = orden.Id;
                data.Cantidad = orden.Unidades;
                data.CantidadPendiente = orden.UnidadesPendientes;
                data.Lote = orden.Lote;
            }
            else
                data.NumOrden = 0;

            parametros.ForEach(p =>
            {
                ParamValueCollection list = data.GetGroupList(p.TipoGrupoId);
                if (list != null)
                    foreach (var vp in p.Valores)
                    {
                        ParamValue pv = new ParamValue
                        {
                            IdGrupo = vp.GrupoId,
                            IdTipoGrupo = (TipoGrupo)p.TipoGrupoId,
                            Grupo = list.Name,
                            IdParam = vp.ParametroId,
                            ValorInicio = vp.ValorInicio,
                            Valor = vp.Valor,
                            IdTipoValor = vp.Parametro.TipoValorId,
                            IdUnidad = (ParamUnidad)vp.Parametro.UnidadId,
                            Unidad = vp.Parametro.Unidad.Nombre,
                            Name = vp.Parametro.Nombre,
                            IdCategoria = vp.CategoriaId
                        };

                        list.Add(pv);
                    }
            });

            //data.ConfigurationFiles.LoadAllFilesFromBBDD(producto);

            var numBastidor = GetVariable(ConstantsParameters.TestInfo.NUM_BASTIDOR);
            if (numBastidor != null && !string.IsNullOrEmpty(numBastidor.ToString()))
                data.TestInfo.NumBastidor = Convert.ToUInt64(numBastidor);

            context.Services.Add<ITestContext>(data);
        }
    }

    public class InitProductZurcActionDescription : ActionDescription
    {
        public override string Name { get { return "InitProductZurc"; } }
        public override string Category { get { return "ZurcTest/Database"; } }
        public override Image Icon { get { return Properties.Resources.DatabaseParameters; } }
    }
}