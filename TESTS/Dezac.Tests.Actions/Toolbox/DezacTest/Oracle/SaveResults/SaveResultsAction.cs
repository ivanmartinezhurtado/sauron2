﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.26)]
    [DesignerAction(Type = typeof(SaveResultsActionDescription))]

    public class SaveResultsAction : ActionBase
    {
        public override void Execute()
        {
            SequenceContext.Current.SaveResultsMethod = () => SaveResults.SaveResultsToDDBB();
            Logger.Info("La grabación de resultados se realizará al terminar el test.");
        }  
    }

    public class SaveResultsActionDescription : ActionDescription
    {
        public override string Name { get { return "SaveResults"; } }
        public override string Category { get { return "DezacTest/Oracle/SaveResults"; } }
        public override string Description { get { return "Action que sirve para guardar los resultados de la secuencia en la Base de Datos"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }
}
