﻿using Dezac.Core.Model;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using WinTaskRunner.Design;
using WinTaskRunner.Design.Services;
using WinTaskRunner.Services;
using WinTaskRunner.Utils;
using WinTaskRunner.Views;

namespace WinTaskRunner
{
    public partial class MainForm : RadForm, IEnvironmentService
    {
        private Toolbox toolbox;
        private ComponentsView navigator;
        private PropertyGrid stepProps;
        private PropertyView actionProps;
        private LogViewer logViewer;
        private ResultsParamView resultsParamViewer;
        private VariablesView varsViewer;
        private InstrumentsView InstrumentsPowerSource;
        private InstrumentsView InstrumentsDevice;
        private InstrumentsView InstrumentsIO;
        private ModbusRegisterView ModbusRegister;
        private InspectorViewer InspectorViewer;
        private SynchronizationContext sc;

        protected readonly AppService app;
        protected readonly RunnerService runnerService;
        protected readonly DocumentService documentService;
        protected readonly EventBus eventBus;
        protected readonly ICacheService cacheSvc;
        protected readonly IShell shell;

        public Dictionary<string, object> Environment { get; set; }

        private PlayConfig playConfig;
        private PlayConfig previousPlayConfig;

        public MainForm()
        {
            InitializeComponent();

            sc = SynchronizationContext.Current;
            cmdNumEnsayo.TextBoxElement.TextBoxItem.ReadOnly = true;
            cmdPlay.Shortcuts.Add(new Telerik.WinControls.RadShortcut(Keys.None, Keys.F5));
            cmdPlayStep.Shortcuts.Add(new Telerik.WinControls.RadShortcut(Keys.None, Keys.F8));
            WindowState = FormWindowState.Maximized;
            ControlBox = true;
        }

        public MainForm(AppService appService
            , RunnerService runnerService
            , IDocumentService documentService
            , EventBus eventBus
            , IShell shell)
            : this()
        {
            app = appService;
            this.runnerService = runnerService;
            this.documentService = documentService as DocumentService;
            this.eventBus = eventBus;
            this.shell = shell;

            string type = app.Config.ModeType;
            string mode = type == "SYNC" ? "" : string.Format("Ejecución: {0}", type);

            Text = string.Format("TaskRunner - v.{0}     {1}", Application.ProductVersion, mode);

            ((ShellService)this.shell).Init(this);

            cacheSvc = new CacheService();

            this.documentService.Manager = radDock;
            this.documentService.ActiveDocumentChanged += OnActiveDocumentChanged;

            Environment = new Dictionary<string, object>();

            HookRunnerEvents();
            AddDefaultViews();
            UpdateUIState();

            eventBus.Register("ShowObjectProperties", (s, e, a) =>
            {
                stepProps.SelectedObject = a;
            });

            cmdNumEnsayoLabel.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            cmdNumEnsayo.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            cmdSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            commandBarDropDownTypeTest.SelectedItem = commandBarDropDownTypeTest.Items.FirstOrDefault();

            app.Runner.TestEnd += OnTestEnded;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            AddNew();

            if (!string.IsNullOrEmpty(app.Config.LoadSequence))
                documentService.CurrentDocumentManager.Open(app.Config.LoadSequence);

            buttonForceDesign = true;
            commandBarToggleButtonDiseño.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        }

        private void OnTestEnded(object sender, SequenceContext e)
        {
            if (e.NumInstance != 0)
                try
                {
                    e.SaveResultsMethod?.Invoke();
                    try
                    {
                        LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, true, e.NumInstance, e.Services.Get<ITestContext>(), false);

                        Dictionary<string, byte[]> images = e.Variables.Get("IMAGES_FILES", new Dictionary<string, byte[]>());
                        ITestContext context = e.Services.Get<ITestContext>();
                        if (context == null)
                            return;

                        int? numTestFase = context.TestInfo.NumTestFase;
                        if (numTestFase.HasValue)
                            foreach (KeyValuePair<string, byte[]> image in images)
                                DataUtils.SaveFileVANumTestFase(numTestFase.Value, image.Key, "591", image.Value);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.StackTrace, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    e.TestResult.MainStepResult.ExecutionResult = StepExecutionResult.Failed;
                    e.TestResult.MainStepResult.Exception = ex;
                    LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, true, e.NumInstance, e.Services.Get<ITestContext>(), false);

                    ITestContext context = e.Services.Get<ITestContext>();
                    if (context == null)
                        return;

                    int? numTestFase = context.TestInfo.NumTestFase;
                    if (numTestFase.HasValue)
                    {
                        Dictionary<string, byte[]> images = e.Variables.Get("IMAGES_FILES", new Dictionary<string, byte[]>());

                        foreach (KeyValuePair<string, byte[]> image in images)
                            DataUtils.SaveFileVANumTestFase(numTestFase.Value, image.Key, "591", image.Value);
                    }
                }
        }

        private void HookRunnerEvents()
        {
            TaskRunner.SequenceRunner runner = runnerService.Runner;

            runner.RunningStateChanged += (s, e) => { UpdateUIState(); };
            runner.AllTestStarting += (s, e) => { EnableButtons(false); };
            runner.AllTestEnded += (s, e) =>
            {
                Dezac.Tests.Actions.Views.VariableSelectorView.VariablesList = e.Variables;
                EnableButtons(true);
                Play(playConfig);
            };
        }

        void OnActiveDocumentChanged(object sender, IDocument document)
        {
            if (document != null)
                UpdateUIState();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (documentService != null && documentService.CurrentDocument != null)
            {
                if (!string.IsNullOrEmpty(documentService.CurrentDocument.FileName))
                {
                    DialogResult resp = MessageBox.Show("¿Quieres grabar la secuencia activa?", "¡Atencion! Guardado de seguridad al cerrar el programa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resp == DialogResult.Yes)
                        documentService.Save();
                }
                else
                {
                    var sequence = documentService.CurrentDocument as SequenceDocument;
                    if (sequence.Model != null)
                        if (sequence.Model.Init.Steps.Count() > 0 || sequence.Model.Main.Steps.Count() > 0 || sequence.Model.End.Steps.Count() > 0)
                            documentService.SaveAs();
                }
            }

            cacheSvc.Clear();
        }

        private void UpdateUIState()
        {
            TaskRunner.SequenceRunner runner = app.Runner;

            bool paused = runner.RunningState == RunningState.Paused;
            bool running = runner.RunningState == RunningState.Running || paused;
            Telerik.WinControls.ElementVisibility runningVisible = running ? Telerik.WinControls.ElementVisibility.Visible : Telerik.WinControls.ElementVisibility.Collapsed;

            cmdPlay.Enabled = !running || paused;
            cmdStop.Enabled = running;
            cmdPause.Enabled = !paused;
            cmdPlayStep.Enabled = paused;
            cmdPause.Visibility = runningVisible;
            cmdPlayStep.Visibility = runningVisible;
            cmdStop.Visibility = runningVisible;
            cmdNumTests.Enabled = !running;
            cmdNumTestsP.Enabled = !running;
        }

        private void EnableButtons(bool enable)
        {
            sc.Post(_ =>
            {
                radMenu1.Enabled = enable;
            }, null);
        }

        private void AddNew()
        {
            documentService.AddNew(SequenceDocument.DOC_TYPE);
        }

        public void Save()
        {
            documentService.Save();
        }

        public void SaveAs()
        {
            documentService.SaveAs();
        }

        private void OpenFromDb()
        {
            ProductsViewModel item = null;
            int tipoSecuencia = 0;
            string nombreFichero = "";

            using (var prodView = new ProductsView())
            {
                if (shell.ShowDialog("Seleccione el producto", prodView, MessageBoxButtons.OKCancel) == DialogResult.OK)
                    item = prodView.Current;
            }

            if (item == null)
                return;

            List<ProductFaseViewModel> fases;

            using (var db = new DezacContext())
            {
                fases = db.PRODUCTOFASE
                    .Include("FASE")
                    .Where(p => p.NUMPRODUCTO == item.NUMPRODUCTO && p.VERSION == item.VERSION && p.FASETEST == "S")
                    .Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                    .ToList();
            }

            ProductFaseViewModel fase = null;
            if (fases.Count == 1)
                fase = fases.FirstOrDefault();
            else
            {
                fase = shell.ShowTableDialog<ProductFaseViewModel>("FASE", fases);
                if (fase == null)
                    return;
            }

            Tuple<int, byte[]> data = null;

            using (var svc = new DezacService())
            {
                var PRODUCTO = new PRODUCTO { NUMFAMILIA = item.NUMFAMILIA, NUMPRODUCTO = item.NUMPRODUCTO, VERSION = item.VERSION };
                try
                {
                    data = svc.GetSequenceFileData(PRODUCTO, fase.IDFASE, ref tipoSecuencia, ref nombreFichero);
                }
                catch (Exception ex)
                {
                    shell.MsgBox(ex.Message, "Error");
                }
            }

            if (data == null)
                return;

            string fileName = "";
            switch (tipoSecuencia)
            {
                case 1:
                    fileName = string.Format("{0}_{1}.SEQ", item.FAMILIA, fase.FASE);
                    break;
                case 2:
                    fileName = string.Format("{0}_{1}_{2}.SEQ", item.FAMILIA, item.NUMPRODUCTO, fase.FASE);
                    break;
                case 3:
                    fileName = string.Format("{0}_{1}_V{2}_{3}.SEQ", item.FAMILIA, item.NUMPRODUCTO, item.VERSION, fase.FASE);
                    break;

            };

            IDocument doc = documentService.Open(SequenceDocument.DOC_TYPE, new MemoryStream(data.Item2), fileName);

            doc.Properties.Add("FileName", fileName);
            doc.Properties.Add("SequenceId", data.Item1);
            doc.Properties.Add("Producto", item);
            doc.Properties.Add("NumProducto", item.NUMPRODUCTO);
            doc.Properties.Add("Version", item.VERSION);
            doc.Properties.Add("NumFase", fase.IDFASE);
            doc.Properties.Add("Fase", fase);
            doc.Properties.Add("TipoSecuencia", tipoSecuencia);
        }

        private void SaveToDb()
        {
            var sequence = documentService.CurrentDocument as SequenceDocument;
            if (sequence == null)
                return;

            var dialog = new WPFDialogView("", new SequenceView(shell, sequence), null);
            dialog.Show();
        }

        private void Play(PlayConfig config)
        {
            sc.Post(_ =>
            {

                if (config.NumSequentialExecuted >= config.NumTests || config.Canceled)
                {
                    cmdNumTests.Text = config.NumTests.ToString();
                    return;
                }
                cmdNumTests.Text = string.Format("{0} / {1}", config.NumSequentialExecuted, config.NumTests);

                config.NumSequentialExecuted++;
                config.RunTestNumber++;

                app.Runner.Play((_numInstance, _sharedVariables) =>
                {
                    SequenceDocument sequence = config.Sequence;

                    var context = new SequenceContext(sequence.Model, _numInstance);
                    context.Services.Add<IShell>(shell);
                    context.Services.Add<IEnvironmentService>(this);
                    context.Services.Add<ICacheService>(cacheSvc);
                    context.Services.Add<IAssemblyResolveService>(app.AssemblyResolver);
                    context.Services.Add<IInstanceResolveService>(InstanceResolveService.Default);
                    context.RunMode = config.RunMode;

                    if (sequence.Properties.ContainsKey("NumProducto"))
                        context.Variables.AddOrUpdate("NumProducto", sequence.Properties["NumProducto"]);

                    if (sequence.Properties.ContainsKey("Version"))
                        context.Variables.AddOrUpdate("Version", sequence.Properties["Version"]);

                    if (sequence.Properties.ContainsKey("NumFase"))
                        context.Variables.AddOrUpdate("NumFase", sequence.Properties["NumFase"]);

                    if (config.NumOrden.HasValue)
                        context.Variables.AddOrUpdate("NumOrden", config.NumOrden.Value);

                    context.Variables.AddOrUpdate("SequenceName", Path.GetFileNameWithoutExtension(sequence.Model.Name));
                    context.Variables.AddOrUpdate("SequenceVersion", sequence.Model.Version ?? "1");

                    SequenceContext prevContext = config.GetTest(config.RunTestNumber - 1, _numInstance, true);
                    if (prevContext != null)
                    {
                        List<string> value = prevContext.SharedVariables.Get<List<string>>(ConstantsParameters.TestInfo.BASTIDORES, () => null);
                        _sharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, value);
                    }
                    else if (previousPlayConfig != null)
                        prevContext = previousPlayConfig.GetTest(1);

                    config.Add(config.RunTestNumber, context);

                    return context;

                }, config.NumTestsParalel);

            }, null);

        }

        #region Menu CommandButton Click event

        private void CommandBarButton1_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void radMenu1NuevoEnsayo_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void radMenu1AbrirEnsayo_Click(object sender, EventArgs e)
        {
            IDocument doc = documentService.Open(SequenceDocument.DOC_TYPE);
        }

        private void radMenu1DescargarEnsayo_Click(object sender, EventArgs e)
        {
            OpenFromDb();
        }

        private void radMenu1Guardar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void radMenu1SaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void radMenuSubir_Click(object sender, EventArgs e)
        {
            SaveToDb();
        }

        private void cmdPlay_Click(object sender, EventArgs e)
        {
            if (app.Runner.RunningState == RunningState.Paused)
            {
                app.Runner.PlayAll();
                return;
            }

            var sequence = documentService.CurrentDocument as SequenceDocument;
            if (sequence == null)
                return;

            if (playConfig != null && playConfig.Sequence == sequence)
                previousPlayConfig = playConfig;
            else
                previousPlayConfig = null;

            if (string.IsNullOrEmpty(cmdNumTests.Text))
                cmdNumTests.Text = "1";

            if (string.IsNullOrEmpty(cmdNumTestsP.Text))
                cmdNumTestsP.Text = "1";

            playConfig = new PlayConfig
            {
                RunMode = commandBarDropDownTypeTest.SelectedItem.Text == "Debug" ? RunMode.Debug : RunMode.Testing,
                Sequence = sequence,
                NumTests = Convert.ToInt32(cmdNumTests.Text),
                NumTestsParalel = Convert.ToInt32(cmdNumTestsP.Text)
            };

            if (!string.IsNullOrWhiteSpace(cmdNumEnsayo.Text))
                playConfig.NumOrden = Convert.ToInt32(cmdNumEnsayo.Text);

            //ConfigureViewsSequence(TypeView.Run);    

            Play(playConfig);

        }

        private void cmdPause_Click(object sender, EventArgs e)
        {
            app.Runner.Pause();
        }

        private void cmdPlayStep_Click(object sender, EventArgs e)
        {
            app.Runner.PlayStep();
        }

        private void cmdStop_Click(object sender, EventArgs e)
        {
            app.Runner.Stop();
            playConfig.Canceled = true;
        }

        private void cmdPlot_Click(object sender, EventArgs e)
        {
            new PlotForm().Show();
        }

        private void radMenu3NewOF_Click(object sender, EventArgs e)
        {
            using (var ctrl = new NewOrderView(shell))
            {
                if (shell.ShowDialog("Generar Orden de Ensayo", ctrl, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    cmdNumEnsayoLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    cmdNumEnsayo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    cmdSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                    cmdNumEnsayo.Text = ctrl.NumOrden.ToString();
                }
            }
        }

        private void radMenu3FindOF_Click(object sender, EventArgs e)
        {
            List<ORDEN> ordenes = null;

            using (var db = new DezacContext())
            {
                ordenes = db.ORDEN
                    .Where(p => p.TRAPARTESINO == "N" && p.UBICACION == "Ensayo")
                    .OrderByDescending(p => p.NUMORDEN)
                    .Take(1000)
                    .ToList();
            }

            ORDEN orden = shell.ShowTableDialog<ORDEN>("ÓRDENES", new TableGridViewConfig
            {
                Data = ordenes,
                Fields = "NUMORDEN;NUMPRODUCTO;VERSION;DESCRIPCION;CANTIDAD"

            });
            if (orden != null)
            {
                cmdNumEnsayoLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                cmdNumEnsayo.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                cmdSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                cmdNumEnsayo.Text = orden.NUMORDEN.ToString();
            }
        }

        //*******************************************************

        #endregion

        #region commandBarButton Click

        private bool buttonForceDesign = false;
        private bool buttonForceMix = false;
        private bool buttonFroceRun = false;

        private void commandBarGuardar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void commandBarToggleButtonDiseño_Click(object sender, EventArgs e)
        {
            buttonForceDesign = true;
        }

        private void commandBarToggleButtonDiseño_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (buttonForceDesign)
            {
                if (commandBarToggleButtonDiseño.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ConfigureViewsSequence(TypeView.Design);
                    buttonForceDesign = false;
                }
                else
                    commandBarToggleButtonDiseño.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            }
        }

        private void commandBarToggleButtonVistaMixto_Click(object sender, EventArgs e)
        {
            buttonForceMix = true;
        }

        private void commandBarToggleButtonVistaMixto_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (buttonForceMix)
            {
                if (commandBarToggleButtonVistaMixto.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ConfigureViewsSequence(TypeView.Mix);
                    buttonForceMix = false;
                }
                else
                    commandBarToggleButtonVistaMixto.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            }
        }

        private void commandBarToggleButtonVistaRun_Click(object sender, EventArgs e)
        {
            buttonFroceRun = true;
        }

        private void commandBarToggleButtonVistaRun_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (buttonFroceRun)
            {
                if (commandBarToggleButtonVistaRun.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ConfigureViewsSequence(TypeView.Run);
                    buttonFroceRun = false;
                }
                else
                    commandBarToggleButtonVistaRun.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            }
        }

        //********************************************************

        private void commandBarToggleButtonInstrumntos_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (commandBarToggleButtonInstrumntos.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                commandBarToggleButtonPowerSource.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                commandBarToggleButtonInOut.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                commandBarToggleButtonInstrumentDevice.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            }
            else
            {
                commandBarToggleButtonPowerSource.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                commandBarToggleButtonInOut.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                commandBarToggleButtonInstrumentDevice.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }
        }

        private void commandBarToggleButtonModbus_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (commandBarToggleButtonModbus.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                HideViewsSequence();
                commandBarToggleButtonDevice.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                documentService.SetControlHide(ModbusRegister, false);
            }
            else
                documentService.SetControlHide(ModbusRegister);
        }

        private void commandBarToggleButtonPowerSource_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (commandBarToggleButtonPowerSource.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                HideViewsSequence();
                documentService.SetControlHide(InstrumentsPowerSource, false);
            }
            else
                documentService.SetControlHide(InstrumentsPowerSource);
        }

        private void commandBarToggleButtonInOut_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (commandBarToggleButtonInOut.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                HideViewsSequence();
                documentService.SetControlHide(InstrumentsIO, false);
            }
            else
                documentService.SetControlHide(InstrumentsIO);
        }

        private void commandBarToggleButtonInstrumentDevice_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (commandBarToggleButtonInstrumentDevice.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                HideViewsSequence();
                documentService.SetControlHide(InstrumentsDevice, false);
            }
            else
                documentService.SetControlHide(InstrumentsDevice);
        }

        private void CommandBarToggleButton1_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (commandBarToggleButtonDevice.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                HideViewsSequence();
                commandBarToggleButtonModbus.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                documentService.SetControlHide(InspectorViewer, false);
            }
            else
                documentService.SetControlHide(InspectorViewer);
        }

        //********************************************************

        #endregion

        #region PerspectiveViewToolWindows

        public void AddDefaultViews()
        {
            resultsParamViewer = DI.Resolve<ResultsParamView>();
            stepProps = new PropertyGrid();
            actionProps = DI.Resolve<PropertyView>();
            toolbox = DI.Resolve<Toolbox>();
            navigator = DI.Resolve<ComponentsView>();
            logViewer = DI.Resolve<LogViewer>();
            varsViewer = DI.Resolve<VariablesView>();
            InstrumentsPowerSource = DI.Resolve<InstrumentsView>();
            InstrumentsDevice = DI.Resolve<InstrumentsView>();
            InstrumentsIO = DI.Resolve<InstrumentsView>();
            ModbusRegister = DI.Resolve<ModbusRegisterView>();
            InspectorViewer = DI.Resolve<InspectorViewer>();

            ToolWindow results = documentService.AddToolWindow(varsViewer, DockPosition.Right, "Variables Viewer", null, 500, 0, 500, 200);
            documentService.AddToolWindow(resultsParamViewer, DockPosition.Bottom, "Param Results Viewer", results, 500, 0, 500, 200);

            ToolWindow toolBoxForm = documentService.AddToolWindow(toolbox, DockPosition.Left, "Toolbox", null, 400, 0, 400, 200);
            documentService.AddToolWindow(navigator, DockPosition.Bottom, "Component Navigator", toolBoxForm, 400, 0, 400, 200);

            ToolWindow stepPropsForm = documentService.AddToolWindow(stepProps, DockPosition.Right, "Step Properties", null, 400, 0, 400, 200);
            documentService.AddToolWindow(actionProps, DockPosition.Bottom, "Action Properties", stepPropsForm, 400, 0, 400, 200);

            documentService.AddToolWindow(ModbusRegister, DockPosition.Right, "Modbus Register View", null, 1600, 0, 1600, 800);

            documentService.AddToolWindow(InspectorViewer, DockPosition.Right, "Inspector Device View", null, 1600, 0, 1600, 800);

            InstrumentsDevice.Category = "Instruments";
            ToolWindow dock = documentService.AddToolWindow(InstrumentsDevice, DockPosition.Left, "Instruments", null, 620, 0, 620, 500);

            InstrumentsIO.Category = "In/Out";
            documentService.AddToolWindow(InstrumentsIO, DockPosition.Left, "In / Out", null, 620, 0, 620, 500);

            InstrumentsPowerSource.Category = "PowerSource";
            documentService.AddToolWindow(InstrumentsPowerSource, DockPosition.Left, "Power Source", null, 525, 0, 525, 500);

            documentService.AddToolWindow(logViewer, DockPosition.Bottom, "Log Viewer", null, Width, 200, 800, 300);
        }

        private enum TypeView
        {
            Design,
            Run,
            Mix
        }

        private class SizeTab
        {
            public int Width { get; set; }
            public int SizeControls { get; set; }
            public int SizeResults { get; set; }

            public SizeTab(TypeView typeView, int with)
            {
                switch (typeView)
                {
                    case TypeView.Design:
                        SizeControls = with / 3;
                        SizeResults = 0;
                        break;
                    case TypeView.Run:
                        SizeControls = 0;
                        SizeResults = with - (with / 4);
                        break;
                    case TypeView.Mix:
                        int parts = with / 5;
                        SizeControls = parts;
                        SizeResults = parts * 2;
                        break;
                }
            }
        }

        private void ConfigureViewsSequence(TypeView typeView)
        {
            HideViewsOthers();

            if (typeView == TypeView.Run)
            {
                commandBarToggleButtonVistaMixto.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                commandBarToggleButtonDiseño.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }
            else if (typeView == TypeView.Mix)
            {
                commandBarToggleButtonVistaRun.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                commandBarToggleButtonDiseño.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }
            else if (typeView == TypeView.Design)
            {
                commandBarToggleButtonVistaRun.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                commandBarToggleButtonVistaMixto.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }

            var sizeTabs = new SizeTab(typeView, Width);
            documentService.SetControlParentSize(toolbox, new Size(sizeTabs.SizeControls, 0));
            documentService.SetControlParentSize(navigator, new Size(sizeTabs.SizeControls, 0));
            documentService.SetControlParentSize(stepProps, new Size(sizeTabs.SizeControls, 0));
            documentService.SetControlParentSize(actionProps, new Size(sizeTabs.SizeControls, 0));
            documentService.SetControlParentSize(varsViewer, new Size(sizeTabs.SizeResults, 0));
            documentService.SetControlParentSize(resultsParamViewer, new Size(sizeTabs.SizeResults, 0));
            documentService.SetDocumnentsHide(false);
        }

        private void HideViewsSequence()
        {
            commandBarToggleButtonDiseño.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonVistaRun.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonVistaMixto.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            documentService.SetControlHide(toolbox);
            documentService.SetControlHide(navigator);
            documentService.SetControlHide(stepProps);
            documentService.SetControlHide(actionProps);
            documentService.SetControlHide(varsViewer);
            documentService.SetControlHide(resultsParamViewer);
            documentService.SetDocumnentsHide();
        }

        private void HideViewsOthers()
        {
            commandBarToggleButtonInstrumntos.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonPowerSource.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonModbus.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonInstrumentDevice.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonInOut.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
            commandBarToggleButtonDevice.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;

            documentService.SetControlHide(InstrumentsPowerSource);
            documentService.SetControlHide(InstrumentsDevice);
            documentService.SetControlHide(InstrumentsIO);
            documentService.SetControlHide(ModbusRegister);
            documentService.SetControlHide(InspectorViewer);
        }


        #endregion

        private void RadMenuItem6_Click(object sender, EventArgs e)
        {
            using (var frm = new AboutIDPDezac())
            {
                frm.ShowDialog();
            }
        }
    }
}

