﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uEye;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Configuration;


namespace Dezac.Tests.Visio
{
    public class CameraIDS : Camera
    {
        private uEye.Camera camera;
        private Bitmap ImageCamera;

        public CameraIDSSettings CameraSettings { get; set; }
        public IntPtr DisplayHandle { get; set; }

        public CameraIDS(CameraIDSSettings cameraSetting)
        {
            DisplayHandle = IntPtr.Zero;
            CameraSettings = cameraSetting;
            CreateCamera();
        }

        protected override void CreateCamera()
        {
            var statusRet = uEye.Defines.Status.NO_SUCCESS;

            camera = new uEye.Camera();

            if (CameraSettings != null)
            {
                var setting = CameraSettings;

                if (!string.IsNullOrEmpty(setting.SerialNumber))
                {
                    var infoCamara = CameraUtils.GetCameraInfoIDSBySerialNumber(setting.SerialNumber);
                    statusRet = camera.Init((Int32)infoCamara.DeviceID | (Int32)uEye.Defines.DeviceEnumeration.UseDeviceID);
                    if (statusRet != uEye.Defines.Status.SUCCESS)
                    {
                        camera.Exit();
                        throw new Exception("Initializing the camera failed");
                    }
                }
            }else
            {
                camera.Exit();
                throw new Exception("Initializing the camera failed by CameraViewInfo is null");
            }

            SetSettings(true);

            camera.EventFrame += ProcessFrameMethod;

            System.Threading.Thread.Sleep(1000);

            var result = camera.Acquisition.Freeze();
            if (result != uEye.Defines.Status.SUCCESS)
            {
                camera.Exit();
                throw new Exception(string.Format("Error CamaraIDS by {0} in Acquisition.Freeze failed", result));
            }
        }

        public override void Start()
        {
            if (camera == null)
                throw new Exception("Error no hay una camara instanciada para iniciar la captura de imagen");

            bool started;
            camera.Acquisition.HasStarted(out started);

            if (!started)
            {
                var result = camera.Acquisition.Capture();
                if (result != uEye.Defines.Status.Success)
                {
                    camera.Exit();
                    throw new Exception(string.Format("Error CamaraIDS by {0} in Acquisition failed", result));
                }
            }
        }

        public override void Stop()
        {    
            camera?.Acquisition.Stop();          
        }

        public Bitmap GetImage(IntPtr displayHandle)
        {
            Bitmap bitmap;
            Int32 s32MemID;
            camera.Memory.GetActive(out s32MemID);
            camera.Memory.Lock(s32MemID);
            camera.Memory.ToBitmap(s32MemID, out bitmap);
            var image = bitmap.Clone() as Bitmap;
            bitmap.Dispose();
            camera.Memory.Unlock(s32MemID);
            camera.Display.Render(s32MemID, displayHandle, uEye.Defines.DisplayRenderMode.FitToWindow);
            return image;
        }

        public void ProcessFrameMethod(object sender, EventArgs arg)
        {
            // convert sender object to our camera object
            uEye.Camera camera = sender as uEye.Camera;
            if (camera.IsOpened)
            {
                uEye.Defines.DisplayMode mode;
                camera.Display.Mode.Get(out mode);
                // only display in dib mode
                if (mode == uEye.Defines.DisplayMode.DiB)
                {
                    Bitmap bitmap;
                    Int32 s32MemID;
                    camera.Memory.GetActive(out s32MemID);
                    camera.Memory.Lock(s32MemID);
                    camera.Memory.ToBitmap(s32MemID, out bitmap);
                    ImageCamera = bitmap.Clone() as Bitmap;
                    bitmap.Dispose();
                    camera.Memory.Unlock(s32MemID);
                    if (DisplayHandle != IntPtr.Zero)
                        camera.Display.Render(s32MemID, DisplayHandle, uEye.Defines.DisplayRenderMode.FitToWindow);
                }
            }
        }
      
        public void SaveImage(string pathSaveImage)
        {
            bool ended = false;
            var result = camera.Acquisition.Freeze();
            if (result != uEye.Defines.Status.SUCCESS)
            {
                camera.Exit();
                throw new Exception(string.Format("Error CamaraIDS by {0} in Acquisition failed", result));
            }

            while(!ended)
                camera.Acquisition.IsFinished(out ended);
            ;

            result= camera.Image.Save(pathSaveImage);
            if (result != uEye.Defines.Status.SUCCESS)
            {
                camera.Exit();
                throw new Exception(string.Format("Error CamaraIDS by {0} in camera.Image.Save failed", result));
            }
        }

        public void LoadParameter(string pathLoadParameter)
        {
            camera.Parameter.Load(pathLoadParameter);
        }

        public void SaveParameter(string pathSaveParameter)
        {
            camera.Parameter.Save(pathSaveParameter);
        }

        public CameraIDSSettings SetSettings(bool init=false)
        {
            var cameraSettingRead = GetSettings();
         
            if (cameraSettingRead.PixelClock != CameraSettings.PixelClock)
                camera.Timing.PixelClock.Set(CameraSettings.PixelClock);

            camera.Timing.Framerate.Set(CameraSettings.Framerate);

            if (cameraSettingRead.AutoGainShutter != CameraSettings.AutoGainShutter)
                camera.AutoFeatures.Sensor.GainShutter.SetEnable(CameraSettings.AutoGain);

            if (cameraSettingRead.AutoGain != CameraSettings.AutoGain)
                camera.AutoFeatures.Sensor.Gain.SetEnable(CameraSettings.AutoGain);

            camera.Gain.Hardware.Scaled.SetMaster(CameraSettings.Gain);

            if (cameraSettingRead.AutoFocusEnable != CameraSettings.AutoFocusEnable)
                camera.Focus.Auto.SetEnable(CameraSettings.AutoFocusEnable);

            camera.Focus.Manual.Set(CameraSettings.FocusManual);

            camera.BlackLevel.Offset.Set(CameraSettings.BlackLevel);
            
            if (cameraSettingRead.AutoWhiteBalance != CameraSettings.AutoWhiteBalance)
                camera.AutoFeatures.Sensor.Whitebalance.SetEnable(CameraSettings.AutoWhiteBalance);

            camera.Saturation.Set(CameraSettings.Saturation);

            if (cameraSettingRead.Model == CameraModel.XS)
            {
                camera.AutoFeatures.Software.WhiteBalance.Offset.Set(CameraSettings.RedOffset, CameraSettings.BlueOffset);

                camera.Zoom.Set(CameraSettings.Zoom);
            }
            else
            {
                camera.Gain.Hardware.Scaled.SetBlue(CameraSettings.BlueOffset);
                camera.Gain.Hardware.Scaled.SetRed(CameraSettings.RedOffset);
                camera.Gain.Hardware.Scaled.SetGreen(0);
                camera.Gain.Hardware.Scaled.SetMaster(0);

                camera.Gamma.Hardware.SetEnable(false);

                if (CameraSettings.ExposureTime > 500)
                    camera.Timing.Exposure.Long.Enabled = true;
            }

            camera.Timing.Exposure.Set(CameraSettings.ExposureTime);
            double doubleValue;
            camera.Timing.Exposure.Get(out doubleValue);
            if (Math.Abs(CameraSettings.ExposureTime - doubleValue) > 2)
                throw new Exception("El tiempo de exposicion no se ha podido configurar correctamente");

            if (init)
            {
                try
                {
                    camera.Parameter.Save(ConfigurationManager.AppSettings["PathImageTestSave"] + "config.ini");
                    Thread.Sleep(500);
                    CameraSettings.SensorGain = Int32.Parse(File.ReadAllLines(ConfigurationManager.AppSettings["PathImageTestSave"] + "config.ini").ToList()
                        .Where((o) => o.Contains("Sensor digital gain=")).First().Replace("Sensor digital gain=", ""));
                    File.Delete(ConfigurationManager.AppSettings["PathImageTestSave"] + "config.ini");

                }
                catch
                {
                }

                camera.Size.ImageFormat.Set(CameraSettings.ImageFormat);  
                camera.PixelFormat.Set(CameraSettings.DictionaryColorMode[CameraSettings.ColorMode]);
                camera.Sharpness.Set(CameraSettings.Sharpness);

                var statusRet = camera.Memory.Allocate();
                if (statusRet != uEye.Defines.Status.SUCCESS)
                {
                    camera.Exit();
                    throw new Exception(string.Format("Error CamaraIDS by {0} in Allocating memory failed", statusRet));
                }
            }
            return cameraSettingRead;
        }

        public CameraIDSSettings GetSettings()
        {
            double valueDbl;
            int valueInt;
            bool valueBool;
            uint valueUint;

            var cameraInfoReading = new CameraIDSSettings();

            camera.Timing.Exposure.Get(out valueDbl);
            cameraInfoReading.ExposureTime = valueDbl;

            camera.Saturation.Get(out valueInt);
            cameraInfoReading.Saturation = valueInt;

            camera.Zoom.Get(out valueDbl);
            cameraInfoReading.Zoom = valueDbl;

            camera.Focus.Auto.GetEnable(out valueBool);
            cameraInfoReading.AutoFocusEnable = valueBool;

            camera.Focus.Manual.Get(out valueUint);
            cameraInfoReading.FocusManual = valueUint;

            camera.AutoFeatures.Sensor.GainShutter.GetEnable(out valueBool);
            cameraInfoReading.AutoGain = valueBool;

            camera.Gain.Hardware.Scaled.GetMaster(out valueInt);
            cameraInfoReading.Gain = valueInt;

            camera.BlackLevel.Offset.Get(out valueInt);
            cameraInfoReading.BlackLevel = valueInt;

            camera.Timing.Framerate.Get(out valueDbl);
            cameraInfoReading.Framerate = valueDbl;

            camera.Timing.PixelClock.Get(out valueInt);
            cameraInfoReading.PixelClock = valueInt;

            int valueInt2;
            camera.AutoFeatures.Sensor.Whitebalance.GetEnable(out valueBool);
            cameraInfoReading.AutoWhiteBalance = valueBool;

            camera.AutoFeatures.Software.WhiteBalance.Offset.Get(out valueInt, out valueInt2);  
            cameraInfoReading.RedOffset=valueInt;
            cameraInfoReading.BlueOffset = valueInt2;

            camera.Sharpness.Get(out valueInt);
            cameraInfoReading.Sharpness = valueInt;

            camera.AutoFeatures.Sensor.GainShutter.GetEnable(out valueBool);
            cameraInfoReading.AutoGainShutter = valueBool;

            var info = new uEye.Types.SensorInfo();
            camera.Information.GetSensorInfo(out info);
            cameraInfoReading.Model = info.SensorName == "UI388xLE-C" ? CameraModel.UI_388xLE : CameraModel.XS;

            return cameraInfoReading;
        }

        public override Bitmap GetImage()
        {
            ImageCamera = null;
            bool ended = false;
            var result = camera.Acquisition.Freeze();
            if (result != uEye.Defines.Status.SUCCESS)
                throw new Exception(string.Format("Error CamaraIDS by {0} in Acquisition.Freeze failed", result));

            Stopwatch imageStopwatch = new Stopwatch();

            imageStopwatch.Start();

            while (!ended && imageStopwatch.Elapsed.TotalSeconds < CameraSettings.ExposureTime + 5)
                camera.Acquisition.IsFinished(out ended);
            ;

            imageStopwatch.Stop();
            imageStopwatch.Reset();
            imageStopwatch.Start();

            while (ImageCamera == null && imageStopwatch.Elapsed.TotalSeconds < 2)
                Thread.Sleep(100);

            imageStopwatch.Stop();

            if (ImageCamera != null)
                return ImageCamera;

            return null;
        }

        public override void Dispose()
        {
            camera?.Acquisition.Stop();
            camera?.Exit();
            camera = null;
        }
    }
}
