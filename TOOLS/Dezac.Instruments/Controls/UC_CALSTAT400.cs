﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using Instruments.Router;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(CalStatDescription))]
    public partial class UC_CALSTAT400 : UserControl, IDisposable
    {

        private static readonly string TAG = "CALSTAT";

        private MyComputer computer = new MyComputer();
        private Vars dataVars = new Vars();
        private CALSTAT400 watt;

        private bool timerEnabled;

        private SynchronizationContext sc;

        public Type InstrumentType
        {
            get
            {
                return typeof(CALSTAT400);
            }
        }

        public CALSTAT400 Instrument
        {
            get
            {
                if (watt == null)
                        watt = new CALSTAT400();

                return watt;
            }
            set
            {
                if (watt != null)
                {
                    watt.Dispose();
                    watt = null;
                }
                watt = value;
            }
        }

        public class Vars
        {
            public double TensionL1 { get; set; }
            public double CorrienteL1 { get; set; }
            public double PotenciaActivaL1 { get; set; }
            public double PotenciaReactivaCapicitivaL1 { get; set; }
            public double PotenciaAparenteL1 { get; set; }
            public double FactorPotenciaL1 { get; set; }
            public double CosenoPhiL1 { get; set; }

            public double TensionL2 { get; set; }
            public double CorrienteL2 { get; set; }
            public double PotenciaActivaL2 { get; set; }
            public double PotenciaReactivaCapicitivaL2 { get; set; }
            public double PotenciaAparenteL2 { get; set; }
            public double FactorPotenciaL2 { get; set; }
            public double CosenoPhiL2 { get; set; }

            public double TensionL3 { get; set; }
            public double CorrienteL3 { get; set; }
            public double PotenciaActivaL3 { get; set; }
            public double PotenciaReactivaCapicitivaL3 { get; set; }
            public double PotenciaAparenteL3 { get; set; }
            public double FactorPotenciaL3 { get; set; }
            public double CosenoPhiL3 { get; set; }

            public double Frecuencia { get; set; }
            public double TensionLineaL1L2 { get; set; }
            public double TensionLineaL2L3 { get; set; }
            public double TensionLineaL3L1 { get; set; }
            public double PotenciaActivaIII { get; set; }
            public double PotenciaCapacitivaIII { get; set; }
            public double PotenciaAparenteIII { get; set; }
        }

        public UC_CALSTAT400()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            try
            {
                sc = SynchronizationContext.Current;

                bsAllVars.DataSource = dataVars;

                var ports = new MyComputer().SerialPortNames.ToArray();
                if (ports.Length == 0)
                {
                    errorProvider1.SetError(cmb_Port, "No se ha encontrado puertos en el PC");
                    return;
                }

                cmb_Port.Items.AddRange(ports);
                cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM2");
                if (cmb_Port.SelectedItem == null)
                    cmb_Port.SelectedIndex = 0;
            }
            catch (Exception)
            {
                errorProvider1.SetError(cmb_Port, "No se ha encontrado la MTE");
            }

        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
            else
            {
                var port = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                ToggleButtonsState((Button)sender);
                try
                {
                    Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                }
                catch (Exception ex)
                {
                    Logger.Error("Error al activar rele 120A en el MTE PRS400.3", ex);
                    ToggleButtonsState((Button)sender);
                    return;
                }

                ReadAllVars();
                timer.Interval = Convert.ToInt32(txtInterval.Text);
                timer.Enabled = checkBox1.Checked;
                EnableTimer();
                ToggleButtonsState((Button)sender);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ToggleButtonsState((Button)sender);

            var task = Task.Factory.StartNew((Action)(() => { this.Instrument.Reset(); }));

            LifeMethod("Reseteando", task);

            timerEnabled = false;

            ToggleButtonsState((Button)sender);
        }

        private void LoadDataVar(CALSTAT400.MeasureData variables)
        {
            dataVars.TensionL1 = variables.Voltage.L1;
            dataVars.CorrienteL1 = variables.Current.L1;
            dataVars.FactorPotenciaL1 = variables.Phases.L1;
            dataVars.CosenoPhiL1 = variables.PowerFactor;
            dataVars.PotenciaActivaL1 = variables.PowerActive.L1;
            dataVars.PotenciaAparenteL1 = variables.PowerAparent.L1;
            dataVars.PotenciaReactivaCapicitivaL1 = variables.PowerReactive.L1;

            dataVars.TensionL2 = variables.Voltage.L2;
            dataVars.CorrienteL2 = variables.Current.L2;
            dataVars.FactorPotenciaL2 = variables.Phases.L2;
            dataVars.CosenoPhiL2 = variables.PowerFactor;
            dataVars.PotenciaActivaL2 = variables.PowerActive.L2;
            dataVars.PotenciaAparenteL2 = variables.PowerAparent.L2;
            dataVars.PotenciaReactivaCapicitivaL2 = variables.PowerReactive.L2;

            dataVars.TensionL3 = variables.Voltage.L3;
            dataVars.CorrienteL3 = variables.Current.L3;
            dataVars.FactorPotenciaL3 = variables.Phases.L3;
            dataVars.CosenoPhiL3 = variables.PowerFactor;
            dataVars.PotenciaActivaL3 = variables.PowerActive.L3;
            dataVars.PotenciaAparenteL3 = variables.PowerAparent.L3;
            dataVars.PotenciaReactivaCapicitivaL3= variables.PowerReactive.L3;

            dataVars.Frecuencia = variables.Freq;
  
            dataVars.PotenciaActivaIII = variables.SumPowerActive;
            dataVars.PotenciaAparenteIII = variables.SumPowerAparent;
            dataVars.PotenciaCapacitivaIII = variables.SumPowerReactive;
        }

        private void ToggleButtonsState(Button btn)
        {
            if (btn.Equals(btnReset))
                btnReset.Enabled = !btnReset.Enabled;
            if (btn.Equals(btnOn))
                btnOn.Enabled = !btnOn.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
            
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }
 
        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value+=10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        public void Close()
        {
            Instrument.Dispose();
        }

        private void ReadAllVars()
        {
            try
            {
                var task = Task.Factory.StartNew((Func<CALSTAT400.MeasureData>)(() => { return (CALSTAT400.MeasureData)this.Instrument.ReadAll(); }));

                LifeMethod("Lectura de las variables instantaneas", task);

                var variables = task.Result;

                LoadDataVar(variables);

                bsAllVars.ResetBindings(false);

            }
            catch (Exception ex)
            {
                Logger.Error("Error al leer variables del MTE PRS400.3", ex);
            }        
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;
            ReadAllVars();
            timer.Enabled = checkBox1.Checked;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) checkBox1.BackColor = Color.DarkGreen;
            else checkBox1.BackColor = Color.DimGray;
        }

        private void EnableTimer()
        {
            if (checkBox1.Checked)
            {
                timerEnabled = true;
                Task.Factory.StartNew(OnGraphTask);
            }
        }

        private void OnGraphTask()
        {
            while (timerEnabled)
            {
                try
                {
                    UpdateGraphs();
                }
                catch { }
                Thread.Sleep(1000);
            }
        }

        private void UpdateGraphs()
        {
            sc.Post(_ =>
            {               
                DataPointHub.Add(this, "I1", dataVars.CorrienteL1, TAG);
                DataPointHub.Add(this, "I2", dataVars.CorrienteL2, TAG);
                DataPointHub.Add(this, "I3", dataVars.CorrienteL3, TAG);

                DataPointHub.Add(this, "V1", dataVars.TensionL1, TAG);
                DataPointHub.Add(this, "V2", dataVars.TensionL2, TAG);
                DataPointHub.Add(this, "V3", dataVars.TensionL3, TAG);               
            }, null);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (watt != null)
                watt.Dispose();

            base.Dispose(disposing);
        }
    }
    public class CalStatDescription : ControlDescription
    {
        public override string Name { get { return "CALSTAT400"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Line_chart_icon; } }
        public override Image IconBig { get { return Properties.Resources.PRS_400_3_1; } }
        public override Type SubcomponentType { get { return typeof(CALSTAT400); } }
    }
}
