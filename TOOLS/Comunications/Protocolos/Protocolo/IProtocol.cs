﻿using Comunications.Message;

namespace Comunications.Protocolo
{
    public interface IProtocol
    {
        string CaracterFinTx { set; get; }
        string CaracterFinRx { set; get; }
        
        byte[] BuildMessage(ITransportMessage message);                       // Formato
        byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource);
        byte[] ReadRequestMessage(ITransportMessage message, IStreamResource streamResource);
    }
}
