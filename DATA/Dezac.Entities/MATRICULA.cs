namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class MATRICULA
    {
        public MATRICULA()
        {
            this.ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            this.ESTRUCTURAMATRICULA = new HashSet<ESTRUCTURAMATRICULA>();
            this.ORDENPRODUCTO = new HashSet<ORDENPRODUCTO>();
            this.T_LOG = new HashSet<T_LOG>();
            this.T_TEST = new HashSet<T_TEST>();
            this.ESTRUCTURAMATRICULA1 = new HashSet<ESTRUCTURAMATRICULA>();
        }
    
        public int NUMMATRICULA { get; set; }
        public System.DateTime FECHAALTA { get; set; }
        public string USUARIO { get; set; }
        public Nullable<System.DateTime> FECHAIMPRESION { get; set; }
        public string SITUACION { get; set; }
    
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }
        public virtual ICollection<ESTRUCTURAMATRICULA> ESTRUCTURAMATRICULA { get; set; }
        public virtual ICollection<ORDENPRODUCTO> ORDENPRODUCTO { get; set; }
        public virtual ICollection<T_LOG> T_LOG { get; set; }
        public virtual ICollection<T_TEST> T_TEST { get; set; }
        public virtual ICollection<ESTRUCTURAMATRICULA> ESTRUCTURAMATRICULA1 { get; set; }
    }
}
