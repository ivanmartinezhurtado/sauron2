﻿using System;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;
using Xunit;

namespace TaskRunnerTesting
{
    public class ClassTestRunnerTesting
    {
        private static object syncObject = new object();

        [Fact]
        public void PlayExecuteTestParalel()
        {
            var sequence = SequenceModel.LoadFile(@"C:\SAURON_SYSTEM\SEQUENCES\Pruebas\TestParalelismo.seq");
            var runner = new SequenceRunner();
            runner.Play((numInstance, sharedVariables) =>
            {
                var context = new SequenceContext(sequence, numInstance);
                context.RunMode = TaskRunner.Enumerators.RunMode.Debug;
                return context;
            });

            while (runner.RunningState != TaskRunner.Enumerators.RunningState.Completed)
            {
                Application.DoEvents();
            }
            var result = runner.RunnerInfo;
            //if(result.Results.)
            var time = Convert.ToDouble(result.Variables.Get("DiffTime"));

            Assert.True(time < 5);
        }
    }
}
