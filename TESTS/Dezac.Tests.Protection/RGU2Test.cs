﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.17)]
    public class RGU2Test : TestBase
    {
        #region Constants

        internal const int LIGHT = 21;
        internal const int EXTERNAL_INPUT = 19;
        internal const int RESET_BUTTON = 9;
        internal const int TEST_BUTTON = 10;
        internal const int PROG_BUTTON = 11;
        internal const int TOROIDAL_OUTPUT = 18;

        internal const int TRIGGER_RELAY_NORMALLY_CLOSED = 10;
        internal const int TRIGGER_RELAY_NORMALLY_OPEN = 9;
        internal const int RELAY_AUXILIAR = 11;

        internal const int OUT_PISTON_IZQUIERDA = 5;
        internal const int OUT_PISTON_DERECHA = 7;
        internal const int OUT_PISTON_CENTRAL = 6;

        internal const int IN_PISTON_IZQUIERDA = 12;
        internal const int IN_PISTON_DERECHA = 13;
        internal const int IN_PISTON_CENTRAL = 14;
        internal const int IN_ANCLA = 15;

        #endregion

        private Tower1 tower;
        private RGU2 rgu2;
        string CAMERA_IDS;

        public void TestInitialization(int port = 0)
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower1>("TOWER", () => { return new Tower1(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (port == 0)
                port = Comunicaciones.SerialPort;

            rgu2 = new RGU2(port);

            rgu2.Modbus.PerifericNumber = Comunicaciones.Periferico; ;

            ((ModbusDeviceSerialPort)rgu2.Modbus).RtsEnable = true;

            ((ModbusDeviceSerialPort)rgu2.Modbus).DtrEnable = true;

            tower.MUX.Reset();     

            tower.PTE.ApplyOff();

            tower.IO.DO.Off(5, 6, 7, 8, 43);

            tower.Active24VDC();

            tower.ActiveElectroValvule();

            SamplerWithCancel((p) =>
            {
                tower.WaitBloqSecurity(100);
                return true;
            }, "Error. No se detecta el equipo o se ha cancelado el test.", 40, 500, 3000);

            Delay(1000, "Espera antes de meter puntas");

            tower.IO.DO.OnWait(1500, OUT_PISTON_CENTRAL);
            tower.IO.DO.OnWait(1500, OUT_PISTON_IZQUIERDA, OUT_PISTON_DERECHA);

            Assert.IsTrue(tower.IO.DI.Read(IN_PISTON_IZQUIERDA).Value, Error().HARDWARE.TORRE.BORNE("Error. No se detectan los bornes izquierdos del equipo"));

            Assert.IsTrue(tower.IO.DI.Read(IN_PISTON_DERECHA).Value, Error().HARDWARE.TORRE.BORNE("Error. No se detectan los bornes derechos del equipo"));

            Assert.IsTrue(tower.IO.DI.Read(IN_PISTON_CENTRAL).Value, Error().HARDWARE.TORRE.BORNE("Error. No se detectan los bornes centrales o de comunicaciones del equipo"));

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);
        }

        public void TestConsumption(int TimeaWait=2000, int TimeInterval=1500, int NumSamples=3)
        {
            tower.PTE.InitConfig();

            tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;

            tower.PTE.Presets.Voltage = 230;

            tower.PTE.ApplyAndWaitStabilisation();

            Delay(TimeaWait, "Espera a la estabilización del consumo del RGU-2");

            TestConsumo(Params.KW.Null.EnVacio.Vnom.Name, Params.KW.Null.EnVacio.Vnom.Min(), Params.KW.Null.EnVacio.Vnom.Max(), () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vnom fuera de margenes", TimeInterval, NumSamples);

            TestConsumo(Params.KVAR.Null.EnVacio.Vnom.Name, Params.KVAR.Null.EnVacio.Vnom.Min(), Params.KVAR.Null.EnVacio.Vnom.Max(), () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vnom fuera de margenes", TimeInterval, NumSamples);
        }

        public void TestCommunications()
        {               

            SamplerWithCancel((p) => {rgu2.FlagTest(); return true;}, "Error, El equipo no comunica", 3, 500, 300, false, false);

            var firmwareVersion = rgu2.ReadFirmwareVersion();

            Assert.IsTrue(firmwareVersion == Convert.ToUInt16(Identificacion.VERSION_FIRMWARE), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error Version de Firmware incorrecta"));

            Resultado.Set("VERSION_FIRMWARE", firmwareVersion, ParamUnidad.SinUnidad);

            rgu2.WriteErrorCode();
        }

        public void TestDisplay()
        {
            rgu2.FlagTest();

            var timeDisplay = 2000;

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.ACTIVATE_IMPAIR_SEGMENTS_PAIR_COMMONS);
            Delay(timeDisplay, "Cambiando display");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "RGU2_IMPARES_PARES", "IMPARES_PARES", "RGU2");

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.ACTIVATE_IMPAIR_SEGMENTS_IMPAIR_COMMONS);
            Delay(timeDisplay, "Cambiando display");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "RGU2_IMPARES_IMPARES", "IMPARES_IMPARES", "RGU2");

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.ACTIVATE_PAIR_SEGMENTS_PAIR_COMMONS);
            Delay(timeDisplay, "Cambiando display");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "RGU2_PARES_PARES", "PARES_PARES", "RGU2");

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.ACTIVATE_PAIR_SEGMENTS_IMPAIR_COMMONS);
            Delay(timeDisplay, "Cambiando display");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "RGU2_PARES_IMPARES", "PARES_IMPARES", "RGU2");

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.DEACTIVATE_ALL_SEGMENTS);
        }

        public void TestLeds()
        {
            rgu2.FlagTest();

            var timeLeds = 1000;

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.ACTIVATE_IMPAIR_SEGMENTS_IMPAIR_COMMONS);
            Delay(timeLeds, "Cambiando LEDS");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "RGU2_ODDS_LEDS", "ODDS_LEDS", "RGU2");

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.ACTIVATE_PAIR_SEGMENTS_PAIR_COMMONS);
            Delay(timeLeds, "Cambiando LEDS");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "RGU2_EVEN_LEDS", "EVEN_LEDS", "RGU2");
        }

        public void TestButtons(int timeOut=500)
        {
            var dictionaryKeyboardOutputs = new Dictionary<RGU2.InputReadings, byte>()
            {
                {RGU2.InputReadings.EXTERNAL_INPUT, EXTERNAL_INPUT },
                {RGU2.InputReadings.PROG_BUTTON, PROG_BUTTON },
                {RGU2.InputReadings.RESET_BUTTON, RESET_BUTTON },
                {RGU2.InputReadings.TEST_BUTTON, TEST_BUTTON }
            };

            var allInputs = new int[]{ RESET_BUTTON, TEST_BUTTON, PROG_BUTTON, EXTERNAL_INPUT };

            tower.IO.DO.OffWait(500, allInputs);
            rgu2.WriteClearKeyboard();

            foreach (KeyValuePair<RGU2.InputReadings,byte> key in dictionaryKeyboardOutputs)
            {
                rgu2.WriteClearKeyboard();

                SamplerWithCancel((p) =>
                {
                    tower.IO.DO.OnWait(500, key.Value);
                    Delay(timeOut, "Esperando activacion teclas");
                    var keyRead = rgu2.ReadKeyboard(); 
                    tower.IO.DO.Off(key.Value);
                    return keyRead.HasFlag(key.Key);
                }, String.Format("Error: No se detecta la activacion de la {0}", key.Key.GetDescription()));

                Resultado.Set(String.Format("INPUTS_{0}", key.Key.GetDescription().ToUpper()), "OK", ParamUnidad.SinUnidad);
            }         
            rgu2.WriteClearKeyboard();
        }

        public void TestRele()
        {
            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.DEACTIVATE_ALL_SEGMENTS);

            rgu2.WriteOutputs(RGU2.OutputOperations.OFF_ALL_LEDS_OUTPUTS);

            Delay(1000, "Esperando desactivacion");

            bool triggerRelayLogic = rgu2.ReadTriggerRelayLogic();
            bool auxiliarRelayLogic = rgu2.ReadAuxiliarRelayLogic();

            rgu2.WriteOutputs(RGU2.OutputOperations.ACTIVATE_TRIGGER_RELAY);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI.Read(TRIGGER_RELAY_NORMALLY_CLOSED).Value == triggerRelayLogic
                && tower.IO.DI.Read(TRIGGER_RELAY_NORMALLY_OPEN).Value == !triggerRelayLogic;
            }, "Error: No se activa Rele de disparo", 70, 100);          

            rgu2.WriteOutputs(RGU2.OutputOperations.ACTIVATE_AUXILIAR_RELAY);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI.Read(RELAY_AUXILIAR).Value == !auxiliarRelayLogic;
            }, "Error: No se activa Rele auxiliar", 70, 100);

            rgu2.WriteOutputs(RGU2.OutputOperations.OFF_ALL_LEDS_OUTPUTS);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI.Read(TRIGGER_RELAY_NORMALLY_CLOSED).Value == !triggerRelayLogic
                    && tower.IO.DI.Read(TRIGGER_RELAY_NORMALLY_OPEN).Value == triggerRelayLogic;

            }, "Error: no se desactiva Rele disparo", 70, 100);

            Resultado.Set("RELE_DISPARO", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI.Read(RELAY_AUXILIAR).Value == auxiliarRelayLogic;
            }, "Error: no se desactiva Rele auxialiar", 70, 100);

            Resultado.Set("RELE_AUXILIAR", "OK", ParamUnidad.SinUnidad);

            rgu2.WriteOutputs(RGU2.OutputOperations.OFF_ALL_LEDS_OUTPUTS);
        }

        public void TestAdjust()
        {
            var tolerance = Configuracion.GetDouble("PTE_TOLERANCIA", 0.5, ParamUnidad.PorCentage);
            var timeOut = Configuracion.GetDouble("PTE_TIMEOUT", 35000, ParamUnidad.PorCentage);

            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);

            tower.PTE.ApplyPresetsAndWaitStabilisationWithValidator(voltage, 0, 50, false, () =>
            {
                var voltageOk = Math.Abs((voltage * 10 - tower.CvmK2.ReadAllVariables().Fases.L1.Tension) / voltage * 10) < tolerance;
                return voltageOk;
            });

            rgu2.FlagTest();
              
            rgu2.WriteAdjustOperation(RGU2.AdjustOperation.REFERENCE_VOLTAGE);
            Delay(2000, "Esperando el calculo del ajuste voltage");
            var defs = new AdjustValueDef { Name = Params.GAIN_V.Null.Ajuste.Name, Min = Params.GAIN_V.Null.Ajuste.Min(), Max = Params.GAIN_V.Null.Ajuste.Max(), Unidad = ParamUnidad.Puntos };
            TestMeasureBase(defs, (step) =>
            {
                return rgu2.ReadAdjustConstant(RGU2.AdjustType.ADJUST_VOLTAGE);
            }, 2, 4, 500);

            var current = Consignas.GetDouble(Params.I.Null.Ajuste.esc_1.Name, 0.1, ParamUnidad.A);
            tower.PTE.ApplyPresetsAndWaitStabilisationWithValidator(voltage, current, 50, false, () =>
                {
                    var voltageOk = Math.Abs((voltage * 10 - tower.CvmK2.ReadAllVariables().Fases.L1.Tension) / voltage * 10) < tolerance;
                    var currentOk = Math.Abs((current * 10000 - tower.CvmK2.ReadAllVariables().Fases.L1.Corriente) / (current * 10000)) < tolerance;
                    return voltageOk && currentOk;
                });

            rgu2.WriteAdjustOperation(RGU2.AdjustOperation.SCALE_1);
            Delay(2000, "Esperando el calculo del ajuste corriente esc.1");
            defs = new AdjustValueDef { Name = Params.GAIN_I.Null.Ajuste.esc_1.Name, Min = Params.GAIN_I.Null.Ajuste.esc_1.Min(), Max = Params.GAIN_I.Null.Ajuste.esc_1.Max(), Unidad = ParamUnidad.Puntos };
            TestMeasureBase(defs, (step) =>
            {
                return rgu2.ReadAdjustConstant(RGU2.AdjustType.ADJUST_CURRENT_1);
            }, 2, 4, 500);

            current = Consignas.GetDouble(Params.I.Null.Ajuste.esc_2.Name, 1, ParamUnidad.A);
            tower.PTE.ApplyPresetsAndWaitStabilisationWithValidator(voltage, current, 50, false, () =>
            {
                var voltageOk = Math.Abs((voltage * 10 - tower.CvmK2.ReadAllVariables().Fases.L1.Tension) / voltage * 10) < tolerance;
                var currentOk = Math.Abs((current * 10000 - tower.CvmK2.ReadAllVariables().Fases.L1.Corriente) / (current * 10000)) < tolerance;
                return voltageOk && currentOk;
            });

            rgu2.WriteAdjustOperation(RGU2.AdjustOperation.SCALE_2);
            Delay(2000, "Esperando el calculo del ajuste corriente esc.2");
            defs = new AdjustValueDef { Name = Params.GAIN_I.Null.Ajuste.esc_2.Name, Min = Params.GAIN_I.Null.Ajuste.esc_2.Min(), Max = Params.GAIN_I.Null.Ajuste.esc_2.Max(), Unidad = ParamUnidad.Puntos };
            TestMeasureBase(defs, (step) =>
            {
                return rgu2.ReadAdjustConstant(RGU2.AdjustType.ADJUST_CURRENT_2);
            }, 2, 4, 500);

            current = Consignas.GetDouble(Params.I.Null.Ajuste.esc_3.Name, 2, ParamUnidad.A);
            tower.PTE.ApplyPresetsAndWaitStabilisationWithValidator(voltage, current, 50, false, () =>
            {
                var voltageOk = Math.Abs((voltage * 10 - tower.CvmK2.ReadAllVariables().Fases.L1.Tension) / voltage * 10) < tolerance;
                var currentOk = Math.Abs((current * 10000 - tower.CvmK2.ReadAllVariables().Fases.L1.Corriente) / (current * 10000)) < tolerance;
                return voltageOk && currentOk;
            });

            rgu2.WriteAdjustOperation(RGU2.AdjustOperation.SCALE_3);
            Delay(2000, "Esperando el calculo del ajuste corriente esc.3");
            defs = new AdjustValueDef { Name = Params.GAIN_I.Null.Ajuste.esc_3.Name, Min = Params.GAIN_I.Null.Ajuste.esc_3.Min(), Max = Params.GAIN_I.Null.Ajuste.esc_3.Max(), Unidad = ParamUnidad.Puntos };
            TestMeasureBase(defs, (step) =>
            {
                return rgu2.ReadAdjustConstant(RGU2.AdjustType.ADJUST_CURRENT_3);
            }, 2, 4, 500);

            current = Consignas.GetDouble(Params.I.Null.Ajuste.esc_4.Name, 10, ParamUnidad.A);
            tower.PTE.ApplyPresetsAndWaitStabilisationWithValidator(voltage, current, 50, false, () =>
            {
                var voltageOk = Math.Abs((voltage * 10 - tower.CvmK2.ReadAllVariables().Fases.L1.Tension) / voltage) < tolerance * 10;
                var currentOk = Math.Abs((current * 10000 - tower.CvmK2.ReadAllVariables().Fases.L1.Corriente) / (current * 10000)) < tolerance;
                return voltageOk && currentOk;
            });

            rgu2.WriteAdjustOperation(RGU2.AdjustOperation.SCALE_4);
            Delay(2000, "Esperando el calculo del ajuste corriente esc.4");
            defs = new AdjustValueDef { Name = Params.GAIN_I.Null.Ajuste.esc_4.Name, Min = Params.GAIN_I.Null.Ajuste.esc_4.Min(), Max = Params.GAIN_I.Null.Ajuste.esc_4.Max(), Unidad = ParamUnidad.Puntos };
            TestMeasureBase(defs, (step) =>
            {
                return rgu2.ReadAdjustConstant(RGU2.AdjustType.ADJUST_CURRENT_4);
            }, 2, 4, 500);

            tower.PTE.ApplyPresetsAndWaitStabilisation(voltage, 0, 50);
        }

        public void TestToroidal()
        {
            tower.IO.DO.Off(TOROIDAL_OUTPUT);

            rgu2.FlagTest();

            rgu2.WriteDisplaySegments(RGU2.DisplayOperations.DEACTIVATE_ALL_SEGMENTS);

            rgu2.WriteTransformer(RGU2.TransfomerOperations.CLEAR_TRANSFORMER_TEST_RESULT);

            Delay(500, "borrado del flag de test de trafo");

            rgu2.WriteTransformer(RGU2.TransfomerOperations.ENFORCE_TRANSFORMER_TEST);

            Delay(500, "inicio test toroidal");

            SamplerWithCancel((p) =>
            {
                return rgu2.ReadToroidalState() == 0;
            }, "Error. El equipo no detecta el Trafo cuando esta conectado", 70, 100);

            Resultado.Set("DETECCION_TOROIDAL_CONECTADO", "OK", ParamUnidad.SinUnidad);

            tower.IO.DO.On(TOROIDAL_OUTPUT);

            rgu2.WriteTransformer(RGU2.TransfomerOperations.CLEAR_TRANSFORMER_TEST_RESULT);

            Delay(500, "borrado del flag de test de trafo");
 
            rgu2.WriteTransformer(RGU2.TransfomerOperations.ENFORCE_TRANSFORMER_TEST);

            Delay(500, "inicio test toroidal");

            SamplerWithCancel((p) =>
            {
                return rgu2.ReadToroidalState() != 0;
            }, "Error. El equipo detecta el Trafo cuando no esta conectado", 70, 100);

            Resultado.Set("DETECCION_TOROIDAL_DESCONECTADO", "OK", ParamUnidad.SinUnidad);

            rgu2.WriteTransformer(RGU2.TransfomerOperations.CLEAR_TRANSFORMER_TEST_RESULT);

            tower.IO.DO.Off(TOROIDAL_OUTPUT);
        }

        public void TestTrigger()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230, ParamUnidad.V);

            var current = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 0.03, ParamUnidad.SinUnidad);

            tower.PTE.ApplyPresetsAndWaitStabilisation(voltage, 0.01, 50);

            rgu2.Reset();

            SamplerWithCancel((p) =>
            {
                rgu2.WriteTriggerCurrent(RGU2.TriggerCurrents.CURRENT_A03);
                return true;
            }, "Error de comunicaciones después del reset", 5);
            
            rgu2.WriteTriggerTime(RGU2.TriggerTimes.TIME_INS);

            Delay(2500, "Configurando PTE para el trigger");

            tower.PTE.ResetTimerTripMonitor();

            tower.PTE.ApplyConfigMonitor(3, PTE.Monitor.EventType.TRIP, PTE.Monitor.EventTrigger.ActivarseDesactivarse);

            tower.PTE.DesActivaTimer(108);

            tower.PTE.ApplyCurrentAndStartTimer(current);

            SamplerWithCancel((p) => rgu2.ReadTriggerCurrent() != 0, "Error el equipo no ha disparado, al no leer corriente de fuga diferente de 0", 10, 200);

            var result = rgu2.ReadTriggerCurrent();

            var time = tower.PTE.ReadTime();
                                          
            Assert.AreBetween(Params.I.Null.TRIGGER.Name, result, Params.I.Null.TRIGGER.Min(), Params.I.Null.TRIGGER.Max(), Error().UUT.MEDIDA.MARGENES("Error. valor de corriente de disparo fuera de margenes"), ParamUnidad.A);

            Assert.AreBetween(Params.TIME.Null.TRIGGER.Name, time, Params.TIME.Null.TRIGGER.Min(), Params.TIME.Null.TRIGGER.Max(), Error().UUT.MEDIDA.MARGENES("Error. valor de tiempo de disparo fuera de margenes"), ParamUnidad.s);
        }

        public void Customization()
        {
            rgu2.FlagTest();

            rgu2.WriteErrorCode(0);

            rgu2.WriteModel(Convert.ToUInt16(Identificacion.MODELO));

            rgu2.WriteInitialMessage(Identificacion.SUBMODELO);

            Resultado.Set("MENSAJE_INICIAL", Identificacion.SUBMODELO, ParamUnidad.SinUnidad);

            rgu2.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));
            Delay(2000, "Ecritura del numero de serie");

            tower.PTE.ApplyOffAllAndWaitStabilisation();

            Delay(1500, "Reset de la fuente PTE");

            tower.PTE.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            Delay(5000, "Estabilizando voltaje y comunicaciones del equipo");
            SamplerWithCancel((p) => { rgu2.FlagTest(); return true; }, "Error de comunicaciones después del reset", 5);

            var errorCode = rgu2.ReadErrorCode();

            Assert.AreEqual("ERROR_CODE", errorCode, 0, Error().SOFTWARE.NO_CONTROLADO.ERROR_GRABACION("Error. El codigo de error no se ha grabado correctamente."),ParamUnidad.Numero);

            var deviceModel = rgu2.ReadModel();

            Assert.AreEqual("MODELO", deviceModel, Convert.ToUInt16(Identificacion.MODELO), Error().UUT.CONFIGURACION.NO_GRABADO("Error. el número de modelo no se ha grabado correctamente"),ParamUnidad.SinUnidad);

            var numeroSerie = rgu2.ReadSerialNumber();

            Assert.AreEqual("NUMERO_SERIE", numeroSerie.ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error.El Numero Serie leido no corresponde con el grabado."), ParamUnidad.SinUnidad);

            var initialMessage = rgu2.ReadInitialMessage();

            Assert.AreEqual("MENSAJE_INICIAL", initialMessage, Identificacion.SUBMODELO, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. El mensaje inicial no corresponde con el grabado"), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (rgu2 != null)
                rgu2.Dispose();
            if (tower != null)
            {
                tower.PTE.ApplyOffAllAndWaitStabilisation();
                tower.IO.DO.OffWait(500, 21, OUT_PISTON_CENTRAL, OUT_PISTON_DERECHA, OUT_PISTON_IZQUIERDA);
                Delay(1000, "");

                tower.IO.DO.OnWait(500, 12);
                tower.IO.DO.OffWait(500, 12);

                tower.IO.DO.Off(4);
                tower.Dispose();
            }
        }
    }
}
