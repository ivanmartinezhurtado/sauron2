﻿using System;
using System.Collections.Generic;
using TaskRunner;
using WinTaskRunner.Utils;

namespace WinTaskRunner.Services
{
    public class AppService
    {
        private readonly AssemblyResolverService assemblyResolver;

        private Dictionary<string, string> userSettings;

        public AppConfig Config { get; private set; }
        public SequenceRunner Runner { get; private set; }
        public AssemblyResolverService AssemblyResolver { get { return assemblyResolver; } }

        public AppService(AssemblyResolverService assemblyResolver
            , RunnerService runnerService)
        {
            this.assemblyResolver = assemblyResolver;

            Runner = runnerService.Runner;

            LoadConfiguration();

            this.assemblyResolver.DefaultPathFiles = Config.PathCurrentAsssembly;

            AppDomain.CurrentDomain.AssemblyResolve += (s, e) => assemblyResolver.Resolve(e.Name);
        }

        public void LoadConfiguration()
        {
            Config = AppProfile.DeserializeFile<AppConfig>(AppProfile.GetConfigFile());
        }

        private Dictionary<string, string> UserSettings
        {
            get
            {
                if (userSettings == null)
                    userSettings = AppProfile.DeserializeDataUserFile<Dictionary<string, string>>("AppSettings.json") ?? new Dictionary<string, string>();

                return userSettings;
            }
        }

        public string GetUserSettings(string key, string defValue = null)
        {
            if (UserSettings.ContainsKey(key))
                return UserSettings[key];

            return defValue;
        }

        public void SetUserSettings(string key, string value)
        {
            if (UserSettings.ContainsKey(key))
                UserSettings[key] = value;
            else
                UserSettings.Add(key, value);

            AppProfile.SerializeDataUserFile("AppSettings.json", UserSettings);
        }
    }
}
