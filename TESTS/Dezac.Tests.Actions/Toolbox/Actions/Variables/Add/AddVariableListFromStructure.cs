﻿using Dezac.Tests.Actions.Kernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AddVariableListFromStructureDescription))]
    public class AddVariableListFromStructure : ActionBase
    {

        [Description("Nombre del dato que contiene la estructura para convertir a una lista con el valor de sus campos")]
        [Category("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var List = new List<KeyValuePair<string, object>>();

            var Struct = DataInput.Value;
            var fields = RuntimeReflectionExtensions.GetRuntimeFields(Struct.GetType());
            
            foreach (var field in fields)
            {
                var value = field.GetValue(Struct);
                var key = field.Name;

                var keyValue = new KeyValuePair<string, object>(key, value);

                Logger.InfoFormat("Field {0} with type {1} and value = {2} added to list {3}", field.Name, field.FieldType.Name, value, DataOutput);              
                List.Add(keyValue);                
            }

            DataOutput.Value = List;
        }      
    }

    public class AddVariableListFromStructureDescription : ActionDescription
    {
        public override string Description { get { return "Action para transformar una estructura en una lista"; } }
        public override string Name { get { return "AddVariableListFromStructure"; } }
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
