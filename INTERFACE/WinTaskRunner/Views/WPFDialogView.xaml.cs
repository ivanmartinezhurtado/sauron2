﻿using System.Windows;
using System.Windows.Controls;

namespace WinTaskRunner.Views
{
    /// <summary>
    /// Interaction logic for WPFDialogView.xaml
    /// </summary>
    public partial class WPFDialogView : Window
    {
        public MessageBoxResult Result { get; private set; } = MessageBoxResult.None;

        public WPFDialogView()
        {
            InitializeComponent();
        }

        public WPFDialogView(string title, Control control, MessageBoxButton? buttons, WindowStyle windowStyle = WindowStyle.SingleBorderWindow)
            : this()
        {
            Title = title;
            Panel.Width = control.Width;
            Panel.Height = control.Height;
            Width = control.Width + 8;
            Height = control.Height + 66;
            ResizeMode = ResizeMode.NoResize;
            Panel.Children.Add(control);
            WindowStyle = windowStyle;

            if (buttons != null)
            {
                buttonOK.Visibility = buttons == MessageBoxButton.OK || buttons == MessageBoxButton.OKCancel ? Visibility.Visible : Visibility.Collapsed;
                buttonYes.Visibility = buttons == MessageBoxButton.YesNo || buttons == MessageBoxButton.YesNoCancel ? Visibility.Visible : Visibility.Collapsed;
                buttonNo.Visibility = buttons == MessageBoxButton.YesNo || buttons == MessageBoxButton.YesNoCancel ? Visibility.Visible : Visibility.Collapsed;
                buttonCancel.Visibility = buttons == MessageBoxButton.OKCancel || buttons == MessageBoxButton.YesNoCancel ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                buttonOK.Visibility = Visibility.Collapsed;
                buttonYes.Visibility = Visibility.Collapsed;
                buttonNo.Visibility = Visibility.Collapsed;
                buttonCancel.Visibility = Visibility.Collapsed;
            }
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;
            Close();
        }

        private void buttonNo_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;
            Close();
        }

        private void buttonYes_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
            Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            Close();
        }

        public override void BeginInit()
        {
            base.BeginInit();
        }
    }
}
