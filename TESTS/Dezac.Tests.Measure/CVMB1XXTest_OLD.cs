﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.15)]
    public class CVMB1XXTest : TestBase
    {
        internal const byte INPUT_MUX_MULT_DC_VN2_12V = 2;
        internal const byte INPUT_MUX_MULT_DC_3V3 = 3;
        internal const byte INPUT_MUX_FREC_RELOG = 4;
        internal const byte ACTIVATE_IN1 = 18;
        internal const byte ACTIVATE_IN2 = 17;
        private const string IMAGE_PATH = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\CVMB1XX\";
        private const byte OUT_TRANSISTOR_1 = 12;
        private const byte OUT_TRANSISTOR_2 = 11;
        private const byte OUT_RELAY_1 = 9;
        private const byte OUT_RELAY_2 = 10;

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }
        private CVMB1XX cvm;
        public CVMB1XX Cvm
        {
            get
            {
                if (cvm == null)
                    cvm = new CVMB1XX(Comunicaciones.SerialPort);

                return cvm;
            }
        }
        private SerialPort sp;
        private CVMB1XX.IdVector hardwareID;
        public  bool IsDC { get; set; }

        public virtual void TestInitialization(int portCVM = 0)
        {           

            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            Cvm.Modbus.PortCom = portCVM;
            Cvm.Modbus.PerifericNumber = Comunicaciones.Periferico;

            Tower.On();
            Tower.OpenVoltageCircuit();
            Tower.OpenCurrentCircuit(false, false, false);

            var supplyTypeCvmFromBBDD = CVMB1XX.typeSource.NULL;
            Enum.TryParse<CVMB1XX.typeSource>(Configuracion.GetString("HARDWARE_ID_SOURCE", CVMB1XX.typeSource.AC.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out supplyTypeCvmFromBBDD);

            IsDC = supplyTypeCvmFromBBDD == CVMB1XX.typeSource.DC ? true : false;

            if (IsDC)
                Tower.IO.DO.On(23);

            Tower.MTEWAT.Output120A = false;

        }

        public void TestConsumptionLoaded()
        {           
            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = IsDC == true ? TypePowerSource.LAMBDA : TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.ConCarga,
                typeTestVoltage = TypeTestVoltage.VoltageMinimo,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(Tower, configuration);

            configuration.typeTestVoltage = TypeTestVoltage.VoltageMaximo;
            this.TestConsumo(Tower, configuration);

            configuration.typeTestVoltage = TypeTestVoltage.VoltageNominal;
            this.TestConsumo(Tower, configuration);
        }

        public void TestConsumptionUnLoaded()
        {           
            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = IsDC == true ? TypePowerSource.LAMBDA : TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageMinimo,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(Tower, configuration);

            configuration.typeTestVoltage = TypeTestVoltage.VoltageMaximo;
            this.TestConsumo(Tower, configuration);

            configuration.typeTestVoltage = TypeTestVoltage.VoltageNominal;
            this.TestConsumo(Tower, configuration);
        }

        public void TestComunications()
        {
            SamplerWithCancel((p) => { Cvm.ReadSoftwareVersion(); return true; }, "No se ha podido comunicar con el equipo", 10, 1000, 0, false, false);
            
            var version = Cvm.ReadSoftwareVersion().CToNetString();

            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, version, ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

            SamplerWithCancel((p) => Identificacion.VERSION_FIRMWARE == version, "Error Version de Firmware incorrecta", 10, 1000);
        }

        public void TestSetupDefault()
        {
            Cvm.WriteBastidor((int)TestInfo.NumBastidor);
            Delay(1500, "Esperando grabación del numero de bastidor");

            DateTime fecha = DateTime.UtcNow;

            Cvm.WriteDateTime(fecha);          

            var date = Cvm.Modbus.Read<CVMB1XX.FechaHora>((ushort)CVMB1XX.Registers.FECHA_HORA);

            if (date.Year != fecha.Year && date.Month != fecha.Month && date.Day != fecha.Day)
                throw new Exception("Error Fecha grabada incorrecta");           
            
            Cvm.WriteSetupDefault();

            hardwareID = new CVMB1XX.IdVector();

            var board = CVMB1XX.typeBoard.NULL;
            Enum.TryParse<CVMB1XX.typeBoard>(Configuracion.GetString("HARDWARE_ID_BOARD", CVMB1XX.typeBoard.CPU.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out board);
            hardwareID.Board = board;

            var display = CVMB1XX.typeDisplay.NULL;
            Enum.TryParse<CVMB1XX.typeDisplay>(Configuracion.GetString("HARDWARE_ID_DISPLAY", CVMB1XX.typeDisplay.D3.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out display);
            hardwareID.Display = display;

            var source = CVMB1XX.typeSource.NULL;
            Enum.TryParse<CVMB1XX.typeSource>(Configuracion.GetString("HARDWARE_ID_SOURCE", CVMB1XX.typeSource.AC.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out source);
            hardwareID.Source = source;

            var measure = CVMB1XX.typeMeasure.NULL;
            Enum.TryParse<CVMB1XX.typeMeasure>(Configuracion.GetString("HARDWARE_ID_MEASURE", CVMB1XX.typeMeasure.I5A.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out measure);
            hardwareID.Measure = measure;

            var measureIN = CVMB1XX.typeMeasureIN.NULL;
            Enum.TryParse<CVMB1XX.typeMeasureIN>(Configuracion.GetString("HARDWARE_ID_MEASURE_IN", CVMB1XX.typeMeasureIN.IN5A.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out measureIN);
            hardwareID.MeasureIN = measureIN;

            var device = CVMB1XX.typeDevice.NULL;
            Enum.TryParse<CVMB1XX.typeDevice>(Configuracion.GetString("HARDWARE_ID_DEVICE", CVMB1XX.typeDevice.CVMB.GetDescription(), ParamUnidad.SinUnidad).ToUpper().Trim(), out device);
            hardwareID.Device = device;

            var frecuency = CVMB1XX.typeFrecuency.NULL;
            Enum.TryParse<CVMB1XX.typeFrecuency>(Configuracion.GetString("HARDWARE_ID_FRECUENCY", CVMB1XX.typeFrecuency._50Hz.GetDescription(), ParamUnidad.SinUnidad).Trim(), out frecuency);
            hardwareID.Frecuency = frecuency;

            hardwareID.Unknow = 0;

            Cvm.WriteIDHardware(hardwareID);

            Delay(2500, "Escribiendo vector de hardware");

            var readHardwareID = Cvm.ReadIDHardware();

            Assert.AreEqual(hardwareID, readHardwareID, "Error Identificador de Hardware grabado incorrecto");           
        }

        public void TestDisplay()
        {
            Cvm.FlagTest();

            string[] ColorDisplay = { "BLANCO", "NEGRO", "CUADROS", "LETRAS_NEGRAS", "LETRAS_BLANCAS", "ROJO", "VERDE", "AZUL" };

            for (int i = 0; i <= 7; i++)
            {
                Cvm.Write(CVMB1XX.Registers.DISPLAY, (ushort)i);

                if (Shell.ShowDialog("Test DISPLAY", () => new ImageView(ColorDisplay[i].Replace("_"," "), IMAGE_PATH + ColorDisplay[i] + ".jpg"), MessageBoxButtons.YesNo, "Se ve el Display como en la imagen patron") != DialogResult.Yes)
                    throw new Exception("Error Display incorrecto");

                Resultado.Set(string.Format("DISPLAY_{0}", ColorDisplay[i]), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestComunicationsBACnet()
        {         
            Cvm.WriteBacnetID(TestInfo.BacNetID.Value);
            Resultado.Set("BACNET_ID", TestInfo.BacNetID.Value, ParamUnidad.SinUnidad);
           
            Cvm.WriteModeBACnet485();

            Cvm.Modbus.ClosePort();

            sp = new SerialPort("COM" + 7, 38400, Parity.None, 8, StopBits.One);
            sp.Open();

            List<byte> received = new List<byte>();

            int bytesToRead = 8;
            byte[] buffer = new byte[bytesToRead];

            Delay(2000, "Esperando que el equipo empieze a lanzar tramas");
            SamplerWithCancel(
            (p) =>
            {
                received.Clear();
                sp.Read(buffer, 0, bytesToRead);

                foreach (byte elem in buffer)
                    received.Add(elem);

                return received[0] == 85 && received[1] == 255;
            }, "No se comunica por BACnet", 20, 500);

            sp.Close();
            sp.Dispose();

            Resultado.Set("COMUNICATIONS_BACNET", "OK", ParamUnidad.SinUnidad);

            if (IsDC)
                Tower.LAMBDA.ApplyOffAndWaitStabilisation();
            else
                Tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(1000, "Asegurando apagado del equipo");

            if (IsDC)
                Tower.LAMBDA.ApplyAndWaitStabilisation(48, 0);
            else               
                Tower.Chroma.ApplyAndWaitStabilisation(230, 0, 50);

            SamplerWithCancel((p) => { Cvm.ReadSoftwareVersion(); return true; }, "No se ha podido comunicar con el equipo", 20, 1000, 5000, false, false);          
        }

        public virtual void TestConector()
        {
            Cvm.FlagTest();

            SamplerWithCancel((p) =>
            {
                return Cvm.Modbus.Read<CVMB1XX.Conector>().IsValid;
            }, "Error conector posterior no detectado", 15, 1000, 500);

            var param = new AdjustValueDef(Params.V_DC.Null.TestPoint("VN212V"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
            }, 0, 3, 2000);

            param = new AdjustValueDef(Params.V_DC.Null.TestPoint("P3V3"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.IN3, MagnitudsMultimeter.VoltDC); 
            }, 0, 3, 2000);         

            Tower.HP53131A
                .PresetAndConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_50)
                .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.AUTO_ON, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.LOW, 1.5D)
                .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.NEG, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 9);

            double temperatura = 0;
            param = new AdjustValueDef(Params.TEMP.Null.TestPoint("CVM"), ParamUnidad.Grados);
            TestMeasureBase(param,
            (step) =>
            {
                temperatura = Cvm.ReadTemperature();
                return temperatura;
            }, 0, 3, 2000);

            double CalcFactor = 32768 * (0.034 * Math.Pow(temperatura - 25, 2));
            double FactorPPMcalc = (32768 + 0.9) - (CalcFactor / 1000000);
        
            param = new AdjustValueDef(Params.FREQ.Null.RTC, ParamUnidad.Hz);
            param.Min = FactorPPMcalc - 1;
            param.Max = FactorPPMcalc + 1;

            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureWithFrecuency(InputMuxEnum.IN4, 10000);
            }, 0, 3, 2000);
        }

        public void TestLeds()
        {            
            Cvm.Write(CVMB1XX.Registers.LEDS, (ushort)1);
            if (Shell.ShowDialog( "Test LEDS", () => new ImageView("LED CPU", IMAGE_PATH + "LED_CPU.jpg"), MessageBoxButtons.YesNo,"Se ve el led CPU encendido") != DialogResult.Yes)
                throw new Exception("Error Led CPU");
            Resultado.Set("LED_CPU", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.LEDS, (ushort)2);
            if (Shell.ShowDialog("Test LEDS", () => new ImageView("LED TECLADO", IMAGE_PATH + "LEDS_TECLADO.jpg"), MessageBoxButtons.YesNo, "Se ve el led del TECLADO encendido") != DialogResult.Yes)            
                throw new Exception("Error Led Teclado");
            Resultado.Set("LED_KEYBOARD", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.LEDS, (ushort)4);
            if (Shell.ShowDialog("Test LEDS", () => new ImageView("LED ALARMA", IMAGE_PATH + "LED_ALARMA.jpg"), MessageBoxButtons.YesNo, "Se ve el led de ALARMA encendido") != DialogResult.Yes)
                throw new Exception("Error Led Alarma");
            Resultado.Set("LED_ALARM", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.LEDS, (ushort)0);         
        }

        public void TestKeyboard()
        {         
            foreach (CVMB1XX.Keys key in Enum.GetValues(typeof(CVMB1XX.Keys)))
            {
                var frm = this.ShowForm(() => new ImageView("Test Teclado", IMAGE_PATH + key.GetDescription() + ".jpg"), MessageBoxButtons.AbortRetryIgnore, "Pulse la tecla mostrada en la imagen");

                try
                {
                    SamplerWithCancel((p) =>
                    {
                        if (Cvm.ReadKeyboard()[(int)key] > 100)
                            return true;
                        return false;
                    }
                    , string.Format("No se detecta la tecla {0}", key.GetDescription().Replace("_", " ")), 40, 500);
                }                
                finally
                {
                    this.CloseForm(frm);
                }

                Resultado.Set(key.ToString(), "OK", ParamUnidad.SinUnidad);
            }                
        }

        public void Reset()
        {
            Cvm.Reset();
            SamplerWithCancel((p) => { Cvm.ReadSoftwareVersion(); return true; }, "No se ha podido comunicar con el equipo despues del Reset", 20, 1000);
        }
        
        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var transformationRelation = 5;
            Cvm.WriteRelacionTransformacion(CVMB1XX.Escalas.E5A);

            var offsetsV = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = Cvm.ReadVariablesInstantaneas();
                    return new double[] { vars.L1.Tension, vars.L2.Tension, vars.L3.Tension, 
                        vars.L1.Corriente, vars.L2.Corriente, vars.L3.Corriente };

                }, delFirts, samples, timeInterval);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 151, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1.1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.PowerSourceIII.ApplyAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            //var defsFreq = new AdjustValueDef { Name = Params.FREQ.Null.Ajuste.Name, Min = Params.FREQ.Null.Ajuste.Min, Max = Params.FREQ.Null.Ajuste.Max, Unidad = ParamUnidad.Hz };

            Cvm.FlagTest();

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A);
            crucePistasI.L1.Average = ConsignasI.L1;
            crucePistasI.L2.Average = ConsignasI.L2;
            crucePistasI.L3.Average = ConsignasI.L3;

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = Cvm.ReadVariablesInstantaneas();
                    return new double[] { vars.L1.Tension, vars.L2.Tension, vars.L3.Tension, 
                        vars.L1.Corriente * transformationRelation, vars.L2.Corriente * transformationRelation , vars.L3.Corriente * transformationRelation };

                }, delFirts, samples, timeInterval);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }       

        public void TestDigitalOutputs()
        {         

            Cvm.Write(CVMB1XX.Registers.DO, 0);
            Assert.IsTrue(Tower.IO.DI.ReadAllOff(OUT_TRANSISTOR_1, OUT_TRANSISTOR_2, OUT_RELAY_1, OUT_RELAY_2), "Error, salida activada al desactivar las salidas");
         
            Cvm.Write(CVMB1XX.Registers.DO, 1);
            Assert.IsTrue(Tower.IO.DI.WaitOn(OUT_TRANSISTOR_1, 500), "Error, salida 1 de transistor");
            Resultado.Set("OUT1_TRANSISTOR", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.DO, 2);
            Assert.IsTrue(Tower.IO.DI.WaitOn(OUT_TRANSISTOR_2, 500), "Error, salida 2 de transistor");
            Resultado.Set("OUT2_TRANSISTOR", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.DO, 4);
            Assert.IsTrue(Tower.IO.DI.WaitOn(OUT_RELAY_1, 500), "Error, salida 1 de relé");
            Resultado.Set("OUT1_RELAY", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.DO, 8);
            Assert.IsTrue(Tower.IO.DI.WaitOn(OUT_RELAY_2, 500), "Error, salida 2 de relé");
            Resultado.Set("OUT2_RELAY", "OK", ParamUnidad.SinUnidad);

            Cvm.Write(CVMB1XX.Registers.DO, 0);

            Assert.IsTrue(Tower.IO.DI.WaitAllOff(200, OUT_TRANSISTOR_1, OUT_TRANSISTOR_2, OUT_RELAY_1, OUT_RELAY_2), "Error, salida activada al desactivar las salidas");
        }

        public void TestDigitalInputs()
        {
            Cvm.FlagTest();

            Tower.IO.DO.Off(ACTIVATE_IN1, ACTIVATE_IN2);

            CVMB1XX.Inputs entradas = new CVMB1XX.Inputs();

            Sampler.Run(5, 500, (step) =>
            {
                entradas = Cvm.ReadInputs();
                step.Cancel = entradas.Input1 == 0 && entradas.Input2 == 0;
            });

            Assert.AreEqual(entradas.Input1, 0, "Error entrada 1, debería estar desactivada");
            Assert.AreEqual(entradas.Input2, 0, "Error entrada 2, debería estar desactivada");

            Tower.IO.DO.On(ACTIVATE_IN1);

            Sampler.Run(5, 500, (step) =>
            {
                entradas = Cvm.ReadInputs();
                step.Cancel = entradas.Input1 == 1 && entradas.Input2 == 0;
            });

            Assert.AreEqual(entradas.Input1, 1, "Error entrada 1, debería estar activada");
            Assert.AreEqual(entradas.Input2, 0, "Error entrada 2, debería estar desactivada y la 1 activada");
            Resultado.Set("INPUT1", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.Off(ACTIVATE_IN1);
            Tower.IO.DO.On(ACTIVATE_IN2);

            Sampler.Run(5, 500, (step) =>
            {
                entradas = Cvm.ReadInputs();
                step.Cancel = entradas.Input1 == 0 && entradas.Input2 == 1;
            });

            Assert.AreEqual(entradas.Input1, 0, "Error entrada 1, debería estar desactivada y la 2 activada");
            Assert.AreEqual(entradas.Input2, 1, "Error entrada 2, debería estar activada y la 1 desactivada");
            Resultado.Set("INPUT2", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.Off(ACTIVATE_IN1, ACTIVATE_IN2);
        }
 
        //**********************************************************************************************************

        public void TestAdjustPhase([Description("Escala = 5 | 1 | 250")] CVMB1XX.Escalas Escala = CVMB1XX.Escalas.E5A, int delFirts = 10, int initCount = 10, int samples = 20, int timeInterval = 1100)
        {
            Cvm.FlagTest();

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Escala == CVMB1XX.Escalas.E5A ? Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A) : Escala == CVMB1XX.Escalas.E1A ? Consignas.GetDouble(Params.I.Null.Ajuste.esc_1.Name, 1, ParamUnidad.A) : Consignas.GetDouble(Params.I.Null.Ajuste.esc_MC.Name, 0.25D, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            Tower.PowerSourceIII.ApplyAndWaitStabilisation(adjustVoltage, adjustCurrent, frecuency, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            Delay(1000, "Esperando reinicio equipo");
   

            var parameter = Escala == CVMB1XX.Escalas.E5A ? Params.GAIN_DESFASE.L1.Ajuste.esc_5A : Escala == CVMB1XX.Escalas.E1A ? Params.GAIN_DESFASE.L1.Ajuste.esc_1A : Params.GAIN_DESFASE.L1.Ajuste.esc_MC;
            var defs = new TriAdjustValueDef(new AdjustValueDef(parameter, ParamUnidad.Puntos));

            var result = Cvm.AjusteDesfase(delFirts, initCount, samples, timeInterval, Escala, powerFactor,
                (value) =>
                {
                    defs.L1.Value = value.L1;
                    defs.L2.Value = value.L2;
                    defs.L3.Value = value.L3;

                    return !defs.HasMinMaxError();
                },
                (potencias) => (potencias.ActivaL1 > 0 && potencias.ActivaL2 > 0 && potencias.ActivaL3 > 0 && potencias.ReactivaL1 > 0 && potencias.ReactivaL2 > 0 && potencias.ReactivaL3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, string.Format("Error valores del ajuste de ganacia del desfase a {0} fuera de margenes", Escala.ToString()));
        }

        public void TestAdjustVoltage(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            Tower.PowerSourceIII.ApplyAndWaitStabilisation(adjustVoltage, adjustCurrent, frecuency, 0, TriLineValue.Create(0, 120, 240));

            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_V.L1.Ajuste, ParamUnidad.Puntos), true);
            defs.Neutro.Max = Params.GAIN_V.LN.Ajuste.Max();
            defs.Neutro.Min = Params.GAIN_V.LN.Ajuste.Min();

            var adjustVoltageMTE = Tower.PowerSourceIII.ReadVoltage().Value;
            Logger.DebugFormat(string.Format("L1 = {0}",adjustVoltageMTE.L1.ToString()));
            Logger.DebugFormat(string.Format("L2 = {0}", adjustVoltageMTE.L2.ToString()));
            Logger.DebugFormat(string.Format("L3 = {0}", adjustVoltageMTE.L3.ToString()));

            var result = Cvm.AjusteTension(delFirts, initCount, samples, timeInterval, adjustVoltageMTE, 
                (value) =>
                {
                    defs.L1.Value = value.L1;
                    defs.L2.Value = value.L2;
                    defs.L3.Value = value.L3;
                    defs.Neutro.Value = value.Neutro;

                    return !defs.HasMinMaxError();
                },
                (tensiones) => (tensiones.TensionL1 > 0 && tensiones.TensionL2 > 0 && tensiones.TensionL3 > 0 && tensiones.TensionNeutro > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, "Error valores de ajuste de ganacia de la tensión fuera de margenes");
        }

        public void TestAdjustCurrent([Description("Escala = 5 | 1 | 250")] CVMB1XX.Escalas Escala = CVMB1XX.Escalas.E5A, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Escala == CVMB1XX.Escalas.E5A ? Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A) : Escala == CVMB1XX.Escalas.E1A ? Consignas.GetDouble(Params.I.Null.Ajuste.esc_1A.Name, 1, ParamUnidad.A) : Consignas.GetDouble(Params.I.Null.Ajuste.esc_MC.Name, 0.25D, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            Tower.PowerSourceIII.ApplyAndWaitStabilisation(adjustVoltage, adjustCurrent, frecuency, 45, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            Delay(1000, "Espera");
            var parameter = Escala == CVMB1XX.Escalas.E5A ? Params.GAIN_I.L1.Ajuste.esc_5A : Escala == CVMB1XX.Escalas.E1A ? Params.GAIN_I.L1.Ajuste.esc_1A : Params.GAIN_I.L1.Ajuste.esc_MC;
            var defs = new TriAdjustValueDef(new AdjustValueDef(parameter, ParamUnidad.Puntos));
            var parameterLN = Escala == CVMB1XX.Escalas.E5A ? Params.GAIN_I.LN.Ajuste.esc_5A : Escala == CVMB1XX.Escalas.E1A ? Params.GAIN_I.LN.Ajuste.esc_1A : Params.GAIN_I.LN.Ajuste.esc_MC;
            defs.Neutro = new AdjustValueDef(parameterLN.Name, 0, parameterLN.Min(), parameterLN.Max(), 0, 0, ParamUnidad.Puntos);

            var result = Cvm.AjusteCorriente(delFirts, initCount, samples, timeInterval, Escala, adjustCurrent,
                (value) =>
                {
                    defs.L1.Value = value.L1;
                    defs.L2.Value = value.L2;
                    defs.L3.Value = value.L3;
                    defs.Neutro.Value = value.Neutro;

                    return !defs.HasMinMaxError();
                },
                (corrientes) => (corrientes.CorrienteL1 > 0 && corrientes.CorrienteL2 > 0 && corrientes.CorrienteL3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, string.Format("Error valores del ajuste de ganacia de corriente a {0} fuera de margenes", Escala.ToString()));
        }

        public void TestAdjustNeutralCurrentCalculate([Description("Escala = 5 | 1 | 250")] CVMB1XX.Escalas Escala = CVMB1XX.Escalas.E5A, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Escala == CVMB1XX.Escalas.E5A ? Consignas.GetDouble(Params.I.Null.Ajuste.esc_5A.Name, 5, ParamUnidad.A) : Escala == CVMB1XX.Escalas.E1A ? Consignas.GetDouble(Params.I.Null.Ajuste.esc_1.Name, 1, ParamUnidad.A) : Consignas.GetDouble(Params.I.Null.Ajuste.esc_MC.Name, 0.25D, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            Tower.PowerSourceIII.ApplyAndWaitStabilisation(TriLineValue.Create(adjustVoltage, 0, 0), TriLineValue.Create(adjustCurrent, 0, 0), frecuency, 0, TriLineValue.Create(0, 120, 240));

            var parameter = Escala == CVMB1XX.Escalas.E5A ? Params.GAIN_I.LN_CALC.Ajuste.esc_5A : Escala == CVMB1XX.Escalas.E1A ? Params.GAIN_I.LN_CALC.Ajuste.esc_1A : Params.GAIN_I.LN_CALC.Ajuste.esc_MC;
            var defs = new AdjustValueDef(parameter.Name, 0, parameter.Min(), parameter.Max(), 0, 0, ParamUnidad.Puntos);

            var result = Cvm.AjusteCorrienteNeutro(delFirts, initCount, samples, timeInterval, Escala, adjustCurrent,
             (value) =>
             {
                 defs.Value = value;
                 return defs.IsValid();
             });

            var currents = Tower.PowerSourceIII.ReadCurrent();

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, string.Format("Error valores del ajuste de ganacia de la corriente Neutro calculada a {0} fuera de margenes", Escala.ToString()));
        }

        //**********************************************************************************************************

        public virtual void TestVerification([Description("Escala = 5 | 1 | 250")] CVMB1XX.Escalas Escala = CVMB1XX.Escalas.E5A, int initCount = 3, int samples = 6, int timeInterval = 1100)
        
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Escala == CVMB1XX.Escalas.E5A ? Consignas.GetDouble(Params.I.Null.Verificacion.esc_5A.Name, 5, ParamUnidad.A) : Escala == CVMB1XX.Escalas.E1A ? Consignas.GetDouble(Params.I.Null.Verificacion.esc_1.Name, 1, ParamUnidad.A) : Consignas.GetDouble(Params.I.Null.Verificacion.esc_MC.Name, 0.25D, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50, ParamUnidad.Hz);

            Tower.PowerSourceIII.ApplyAndWaitStabilisation(adjustVoltage, adjustCurrent, frecuency, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var toleranciaV = Params.V.Null.Verificacion.Tol() * (Escala == CVMB1XX.Escalas.E5A ? 1 : 2);
            var toleranciaI = Params.I.Null.Verificacion.Tol() * (Escala == CVMB1XX.Escalas.E5A ? 1 : 2);
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();
            var toleranciaKvar = Params.KVAR.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.KVAR.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaKvar, ParamUnidad.Var));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaI, ParamUnidad.A));

            Cvm.WriteRelacionTransformacion(Escala);

            Delay(2000, "Esperando relacion transformacion se aplique");

            var rel = (double)(Escala == CVMB1XX.Escalas.MC ? 4 : Escala == CVMB1XX.Escalas.E5A ? 0.2 : 1);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadVariablesInstantaneas();

                    return new double[] { 
                    vars.L1.PotenciaActiva / rel,  vars.L2.PotenciaActiva / rel , vars.L3.PotenciaActiva / rel,
                    vars.L1.PotenciaReactivaInductiva / rel , vars.L2.PotenciaReactivaInductiva / rel, vars.L3.PotenciaReactivaInductiva / rel, 
                    vars.L1.Tension, vars.L2.Tension,vars.L3.Tension, 
                    vars.L1.Corriente / rel , vars.L2.Corriente / rel , vars.L3.Corriente / rel };
                },
                () =>
                {
                    var config = Tower.MTEWAT.GetFunctionConfig(1);
                    var ReadWattimeter = Tower.MTEWAT.ReadAll();                
                    var varsList = new List<double>() { 
                         ReadWattimeter.PowerActive.L1  ,  ReadWattimeter.PowerActive.L2,  ReadWattimeter.PowerActive.L3, 
                        ReadWattimeter.PowerReactive.L1, ReadWattimeter.PowerReactive.L2, ReadWattimeter.PowerReactive.L3, 
                        ReadWattimeter.Voltage.L1, ReadWattimeter.Voltage.L2, ReadWattimeter.Voltage.L3, 
                        ReadWattimeter.Current.L1,  ReadWattimeter.Current.L2 ,  ReadWattimeter.Current.L3};

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();                      
        }

        public void TestCustomization()
        {
            Cvm.FlagTest();
            Delay(500, "Esperando grabación del numero de serie");

            CVMB1XX.NeutralLimits neutralLimits = new CVMB1XX.NeutralLimits() { LimitsCurrent = 50, LimitsVoltage = 1000 };
            Cvm.WriteNeutralLimits(neutralLimits);
            Delay(5000, "Esperando grabación de limites de neutro");

            Cvm.WriteNumSerie(Convert.ToInt64(TestInfo.NumSerie));
            Delay(5000, "Esperando grabación del numero de serie");

            Cvm.Reset();

            SamplerWithCancel((p) => { Cvm.ReadSoftwareVersion(); return true; }, "No se ha podido comunicar con el equipo despues del Reset", 20, 1000, 2000, false, false);

            var limits = Cvm.ReadNeutralLimits();

            Assert.IsTrue(limits.LimitsVoltage == neutralLimits.LimitsVoltage && limits.LimitsCurrent == neutralLimits.LimitsCurrent, "Error, valor de limites leidos diferentes a los grabados");

            var numserie = Cvm.ReadNumSerie();

            var bastidor = Cvm.ReadBastidor();

            Resultado.Set(ConstantsParameters.TestInfo.NUM_SERIE, numserie, ParamUnidad.SinUnidad, TestInfo.NumSerie);

            Resultado.Set(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor, ParamUnidad.SinUnidad, TestInfo.NumBastidor.Value);

            Assert.AreEqual(Convert.ToInt64(numserie), Convert.ToInt64(TestInfo.NumSerie), "Error, valor de numero de serie leido diferente al grabado");

            Assert.AreEqual(bastidor, TestInfo.NumBastidor.Value, "Error, valor de numero de bastidor leido diferente al grabado");

            Delay(2000, "Esperando al equipo");
            DateTime cvmDateTime = Cvm.ReadDatetime();

            double timeDiff = (DateTime.UtcNow.Subtract(cvmDateTime)).TotalSeconds;

            var secondsLimit = Margenes.GetDouble(Params.TIME.Null.Verificacion.TestPoint("OFFSET_MAX").Name, 30, ParamUnidad.s);

            Assert.AreGreater(secondsLimit, Math.Abs(timeDiff), "Error. El reloj del equipo ha perdido mas de 30 segundos");

            Cvm.WriteSetupDefault();
            Delay(500, "Configurando equipo con el Setup por defecto");

            var readHardwareID = Cvm.ReadIDHardware();

            Assert.AreEqual(hardwareID, readHardwareID, "Error Identificador de Hardware grabado incorrecto");

            //Caqnviar por borrado final DeleteMaxMIn and DeleteEnergies
            Cvm.ErasedEnded();
            Delay(500, "Borrado Final");

            Cvm.FlagTest(false);
        }

        public void TestFinish()
        {
            if (sp != null)
            {
                if (sp.IsOpen)
                    sp.Close();
                sp.Dispose();
            }

            if (Cvm != null)
                Cvm.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();
                Tower.IO.DO.Off(23);
                Tower.Dispose();
            }
        }


        //*****************************************************************************//
        public void WriteSerialNumberDatalogger()
        {
            cvm.FlagTest();
            cvm.WriteNumSerieModulo(Convert.ToInt64(TestInfo.NumSerie));
        }
    }

}