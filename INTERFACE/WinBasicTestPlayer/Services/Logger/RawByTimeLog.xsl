<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="IdEmpleado"></xsl:param>
  <xsl:param name="NumProducto"></xsl:param>
  <xsl:param name="Version"></xsl:param>
  <xsl:param name="DescripcionProducto"></xsl:param>
  <xsl:param name="IdFase"></xsl:param>
  <xsl:param name="PC"></xsl:param>
  <xsl:param name="NumSerie"></xsl:param>
  <xsl:param name="NumBastidor"></xsl:param>
  <xsl:param name="Fecha"></xsl:param>

  <xsl:template match="/">
        <html>
            <head>
                <style>
                  body { font-family: arial; font-size: 12pt; }
                  .step {  }
                  .step-title { padding: 1px; background-color: lightblue; border: 1px solid black; font-size: 14pt; font-weight: bold; color: black;  }
                  .step-content { border-color:black;  border: 1px solid black; padding: 5px; font-size: 10pt; }
                  .step-Init { padding: 1px; background-color: lightgray; border: 1px solid black; font-size: 14pt; font-weight: bold; color: black; }
                  .step-Main { padding: 1px; background-color: lightgray; border: 1px solid black; font-size: 14pt; font-weight: bold; color: black; }
                  .step-End { padding: 1px; background-color: lightgray; border: 1px solid black; font-size: 14pt; font-weight: bold; color: black; }
                  .msg { padding: 1px; font-size: 10pt; font-weight: bold;  color: Darkblue; }
                  .test-uut {font-size: 10pt; color: Darkblue; }
                  .uut {font-size: 10pt; color: DarkGreen;}
                  .level-WARN { color: DarkOrange; font-size: 10pt; }
                  .level-ERROR { color: red; font-size: 10pt; }
                  .logger-Instruments-PowerSource-MTEPPS { font-size: 10pt; }
                  .logger-Instruments-PowerSource-Chroma { font-size: 10pt; }
                  .logger-Instruments-Measure-HP34401A { font-size: 10pt; }
                  .logger-Instruments-IO-PCI7296 { font-size: 10pt; color: SaddleBrown;}
                  .logger-UUT { font-size: 10pt; color: Black; }
                   table { width: 100%; border: 1px solid black; text-align: left; }
                   th { font-size: 14pt; font-weight: bold; color: black; text-align: left;}
                  .caption { background-color: Black; font-size: 18pt; font-weight: bold; color: white; text-align: center;}
                </style>
            </head>
            <body>                 
              <table>
                <tr>
                  <td  colspan="3" class="caption">TEST REPORT</td>
                </tr>
                <tr>
                  <td>
                    <b>Producto: </b>
                    <xsl:value-of select="$NumProducto"/>/<xsl:value-of select="$Version"/> - <xsl:value-of select="$DescripcionProducto"/>
                  </td>
                  <td>
                    <b>Bastidor: </b>
                    <xsl:value-of select="$NumBastidor"/>
                  </td>
                  <td>
                    <b>Fecha: </b>
                    <xsl:value-of select="$Fecha"/>
                  </td>
                </tr>
              </table>      
              <xsl:apply-templates/>        
           </body>
        </html>
    </xsl:template>
    <xsl:template match="EventInfo[@Type='step']">
        <div class="step">
            <xsl:element name="div">
                <xsl:attribute name="class">
                    step-title step-<xsl:value-of select="@Id"/> level-<xsl:value-of select="@Level"/>
                </xsl:attribute>
                 <xsl:value-of select="@Text"/>
            </xsl:element>
            <div class="step-content">
                <xsl:apply-templates select="Events"/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="EventInfo[@Type='msg']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/>
                <xsl:if test="starts-with(@LoggerName, 'UUT')">
                    uut
                </xsl:if>
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> - <xsl:value-of select="@LoggerName"/> - <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventInfo[@Type='msg' and starts-with(@LoggerName, 'TEST_UUT')]">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/> test-uut
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="@LoggerName" />
                <xsl:with-param name="replace" select="'TEST_UUT'" />
                <xsl:with-param name="by" select="''" />
            </xsl:call-template>            
            <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventInfo[@Type='msg' and starts-with(@LoggerName, 'UUT')]">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/> uut
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="@LoggerName" />
                <xsl:with-param name="replace" select="'UUT'" />
                <xsl:with-param name="by" select="''" />
            </xsl:call-template>
           <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventInfo[@Type='msg' and @LoggerName='PARAMS_UUT']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/> uut
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -  <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventInfo[@Type='msg' and @LoggerName='TaskRunner-SequenceRunner']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/>
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> - <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventInfo[@LoggerName='Dezac-Device-Metering-Web_References-TransportSoap']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/>
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -
            <pre>
                <xsl:value-of select="@Text"/>
            </pre>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


</xsl:stylesheet>
