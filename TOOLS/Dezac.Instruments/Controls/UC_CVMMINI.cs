﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(CvmMiniDescription))]
    public partial class UC_CVMMINI : UserControl, IDisposable
    {
        private MyComputer computer = new MyComputer();
        private Vars dataVars = new Vars();
        private CVMMINITower mini;

        public Type InstrumentType
        {
            get
            {
                return typeof(CVMMINITower);
            }
        }

        public CVMMINITower Instrument
        {
            get
            {
                if (mini == null)
                    mini = new CVMMINITower();

                return mini;
            }
            set
            {
                if (mini != null)
                {
                    mini.Dispose();
                    mini = null;
                }
                mini = value;
            }
        }

        public class Vars
        {
            public Int32 TensionL1 { get; set; }
            public Int32 CorrienteL1 { get; set; }
            public Int32 PotenciaActivaL1 { get; set; }
            public Int32 PotenciaReactivaL1 { get; set; }
            public Int32 FactorPotenciaL1 { get; set; }

            public Int32 TensionL2 { get; set; }
            public Int32 CorrienteL2 { get; set; }
            public Int32 PotenciaActivaL2 { get; set; }
            public Int32 PotenciaReactivaL2 { get; set; }
            public Int32 FactorPotenciaL2 { get; set; }

            public Int32 TensionL3 { get; set; }
            public Int32 CorrienteL3 { get; set; }
            public Int32 PotenciaActivaL3 { get; set; }
            public Int32 PotenciaReactivaL3 { get; set; }
            public Int32 FactorPotenciaL3 { get; set; }

            public Int32 PotenciaActivaIII { get; set; }
            public Int32 PotenciaInductivaIII { get; set; }
            public Int32 PotenciaCapacitivaIII { get; set; }
            public Int32 CosenoPhiIII { get; set; }
            public Int32 FacttorPotenciaIII { get; set; }
            public Int32 Frecuencia { get; set; }
            public Int32 TensionLineaL1L2 { get; set; }
            public Int32 TensionLineaL2L3 { get; set; }
            public Int32 TensionLineaL3L1 { get; set; }     
        }

        public UC_CVMMINI()
        {
            InitializeComponent();
                
            cmb_Port.Items.AddRange(computer.SerialPortNames.ToArray());
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM8");

            string[] PerifericType = new string[] { "Chroma", "MTE" };

            cmbPeriferic.Items.AddRange(PerifericType);
            cmbPeriferic.SelectedIndex = cmbPeriferic.FindStringExact("Chroma");

            bsAllVars.DataSource = dataVars;
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
            else
            {             
                if (cmbPeriferic.SelectedItem == null)
                    errorProvider1.SetError(cmbPeriferic, "Needs to select a Periferic number");
                else
                {
                    var perif = cmbPeriferic.SelectedItem.ToString();
                    Instrument.SetPort(Convert.ToInt32(cmb_Port.SelectedItem.ToString().Replace("COM", "")), perif == "Chroma" ? (byte)1 : (byte)6);

                    ToggleButtonsState((Button)sender);
                    timer.Interval = Convert.ToInt32(txtInterval.Text);
                    ReadAllVariables();
                    ToggleButtonsState((Button)sender);
                }          
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ToggleButtonsState((Button)sender);

            StartTask("Reseteando");

            Task.Factory.StartNew((Action)(() => { this.Instrument.Reset(); }))
                .ContinueWith(t =>
                {
                    LoadDataVar(new CVMMINITower.AllVariables());
                    EndTask();
                    ToggleButtonsState((Button)sender);

                }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void LoadDataVar(CVMMINITower.AllVariables variables)
        {
            dataVars.TensionL1 = variables.Phases.L1.Tension;
            dataVars.CorrienteL1 = variables.Phases.L1.Corriente;
            dataVars.FactorPotenciaL1 = variables.Phases.L1.FactorPotencia;
            dataVars.PotenciaActivaL1 = variables.Phases.L1.PotenciaActiva;
            dataVars.PotenciaReactivaL1 = variables.Phases.L1.PotenciaReactiva;

            dataVars.TensionL2 = variables.Phases.L2.Tension;
            dataVars.CorrienteL2 = variables.Phases.L2.Corriente;
            dataVars.FactorPotenciaL2 = variables.Phases.L2.FactorPotencia;
            dataVars.PotenciaActivaL2 = variables.Phases.L2.PotenciaActiva;
            dataVars.PotenciaReactivaL2 = variables.Phases.L2.PotenciaReactiva;

            dataVars.TensionL3 = variables.Phases.L3.Tension;
            dataVars.CorrienteL3 = variables.Phases.L3.Corriente;
            dataVars.FactorPotenciaL3 = variables.Phases.L3.FactorPotencia;
            dataVars.PotenciaActivaL3 = variables.Phases.L3.PotenciaActiva;
            dataVars.PotenciaReactivaL3 = variables.Phases.L3.PotenciaReactiva;

            dataVars.TensionLineaL1L2 = variables.Trifasicas.TensionLineaL1L2;
            dataVars.TensionLineaL2L3 = variables.Trifasicas.TensionLineaL2L3;
            dataVars.TensionLineaL3L1 = variables.Trifasicas.TensionLineaL3L1;

            dataVars.CosenoPhiIII = variables.Trifasicas.CosenoPhiIII;
            dataVars.FacttorPotenciaIII = variables.Trifasicas.FactorPotenciaIII;
            dataVars.Frecuencia = variables.Trifasicas.Frecuencia;
            dataVars.PotenciaActivaIII = variables.Trifasicas.PotenciaActivaIII;
            dataVars.PotenciaCapacitivaIII = variables.Trifasicas.PotenciaCapacitivaIII;
            dataVars.PotenciaInductivaIII = variables.Trifasicas.PotenciaInductivaIII;

            bsAllVars.ResetBindings(false);
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private void ReadAllVariables()
        {
            try
            {
                StartTask("Lectura de las variables instantaneas");
                var task = Task.Factory.StartNew((Func<CVMMINITower.AllVariables>)(() => { return (CVMMINITower.AllVariables)this.Instrument.ReadAllVariables(); }))
                    .ContinueWith((Task<CVMMINITower.AllVariables> t) =>
                    {
                        LoadDataVar(t.Result);
                        EndTask();
                        timer.Enabled = checkBox1.Checked;
                    }, TaskScheduler.FromCurrentSynchronizationContext());

            }
            catch (Exception ex)
            {
                Logger.Error("Error al leer variables del CVM-MINI", ex);
            }        
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 50;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;    
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;
            ReadAllVariables();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            checkBox1.BackColor = checkBox1.Checked ? Color.DarkGreen : Color.DimGray;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (mini != null)
                mini.Dispose();

            base.Dispose(disposing);
        }

        private void cmbPeriferic_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;            

            //if (Convert.ToString(cb.SelectedItem).Equals("Chroma"))
            //    Instrument = Tower.CvmminiChroma;
            //else
            //    Instrument = Tower.CvmminiMTE;
        }

    }

    public class CvmMiniDescription : ControlDescription
    {
        public override string Name { get { return "CVMMINI"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Fh_cvmmini_00b; } }
        public override Image IconBig { get { return Properties.Resources.CVM_MINI; } }
        public override Type SubcomponentType { get { return typeof(CVMMINITower); } }
    }
}
