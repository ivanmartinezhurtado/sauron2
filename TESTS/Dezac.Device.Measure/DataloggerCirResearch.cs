﻿using Dezac.Core.Utility;
using log4net;
using System;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class DataloggerCirResearch : DeviceBase
    {
        public SerialPort sp;
        public ModbusDeviceSerialPort modbus;
        public int portNumber;

        protected static readonly ILog logger = LogManager.GetLogger("TEST_UUT");

        public DataloggerCirResearch()
        {
        }

        public DataloggerCirResearch(int SerialPort)
        {
            SetSerialPort(SerialPort);
        }

        public void SetSerialPort(int serialPort)
        {
            portNumber = serialPort;
        }

        public SerialPort SP
        {
            get
            {
                if (sp == null)
                    sp = new SerialPort("COM" + portNumber, 115200, Parity.None, 8, StopBits.One);

                sp.ReadTimeout = 500;
                sp.WriteTimeout = 500;
                sp.NewLine = "\r";

                if (modbus != null)
                    if (modbus.IsOpen)
                        modbus.ClosePort();

                if (!sp.IsOpen)
                    sp.Open();

                return sp;
            }
        }

        public ModbusDeviceSerialPort Modbus
        {
            get
            {
                if (modbus == null)
                    modbus = new ModbusDeviceSerialPort(portNumber, 115200);

                if (sp != null)
                    if (sp.IsOpen)
                        sp.Close();

                if (!modbus.IsOpen)
                    modbus.OpenPort();

                modbus.PerifericNumber = 1;

                return modbus;
            }
        }

        public string WriteClearParameters()
        {
            LogMethod();
            return WriteAndReadSerialPort("#DEFAULT_ALL", "RESET NVRAM");
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            return WriteAndReadSerialPort("#VER");
        }

        public Int32 ReadCRC()
        {
            LogMethod();
            var crc= Int32.Parse(WriteAndReadSerialPort("#CRC","0x").Replace("0x",string.Empty), NumberStyles.HexNumber);

            return crc;
        }

        public string WriteManufacturingMode(State state)
        {
            LogMethod();
            return WriteAndReadSerialPort("#MAN=" + state.ToString(),"MANUFACTURING MODE ON");
        }

        public double ReadBatteryVoltage()
        {
            LogMethod();
            return Convert.ToDouble(WriteAndReadSerialPort("#VBAT"), new CultureInfo("en-US"));
        }

        public void WriteLedState(Leds led, State state)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("#{0}={1}", led.ToString(), state.ToString()), string.Format("{0}={1}", led.ToString(), state.ToString()));

            if (!result.Contains("OK"))
                throw new Exception("Error. El equipo indica que no ha podido encender el LED");
        }

        public string ReadPushButton()
        {
            LogMethod();
            return WriteAndReadSerialPort(string.Empty, "PushButton");
        }

        public double ReadLineVoltage()
        {
            LogMethod();
            return Convert.ToDouble(WriteAndReadSerialPort("#VAC").Replace("\r\n", string.Empty), new CultureInfo("en-US"));
        }

        public string ReadHardwareVersion()
        {
            LogMethod();
            return Modbus.ReadHoldingRegister((ushort)Registers.HARDWARE_VERSION).ToString();
        }

        public ushort ReadFrameNumber()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER);
        }

        public string ReadProductVersion()
        {
            LogMethod();
            return Modbus.ReadStringHoldingRegister((ushort)Registers.PRODUCT_VERSION, 1);
        }

        public ushort ReadCalibrationFactorA()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.FACTOR_ADC_CALIBRATION_A);
        }

        public ushort ReadCalibrationFactorB()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.FACTOR_ADC_CALIBRATION_B);
        }

        public int ReadSerialNumber(int frameNumber)
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.FRAME_NUMBER);
        }


        public CalibrationResults ReadCalibrationResults()
        {
            LogMethod();

            sp.WriteLine("#CAL");

            Thread.Sleep(2000);

            var readings = sp.ReadExisting().Replace("\r", string.Empty).Trim('\n').Split('\n');
            //var readings = WriteAndReadSerialPort("#CAL").Split('\n');

            if (readings.Length != 2)
                throw new Exception("Error, los datos de calibración devueltos por el equipo tienen un formato incorrecto");

            var calibResults = new CalibrationResults();
     
            calibResults.averageFrequency = Convert.ToDouble(readings[1], new CultureInfo("en-US"));
            calibResults.calibrationValue = Convert.ToDouble(readings[0], new CultureInfo("en-US"));
            
            return calibResults;
        }
     
        //------------------------------------------------
        //---- MODBUS ------------------------------------

        public void WriteTestMode()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ACTIVE_PAGE, (ushort)0x0705);
        }

        public void WriteHardwareVersion(string hardwareVersion)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.HARDWARE_VERSION, Convert.ToUInt16(hardwareVersion));
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SERIAL_NUMBER, (int)serialNumber);
        }

        public void WriteFrameNumber(uint frameNumber)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FRAME_NUMBER, frameNumber);
        }

        public void WriteProductVersion(string versionID)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PRODUCT_VERSION, Convert.ToUInt16(versionID));
        }

        public void WriteCalibrationFactorA(double calibrationFactor)
        {
            Modbus.Write((ushort)Registers.FACTOR_ADC_CALIBRATION_A, (float)calibrationFactor);
        }

        public void WriteCalibrationFactorB(double calibrationFactor)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FACTOR_ADC_CALIBRATION_B, (float)calibrationFactor);
        }

        public override void Dispose()
        {
            if (modbus != null)
            {
                modbus.Dispose();
                modbus = null;
            }

            if (sp != null)
            {
                sp.Dispose();
                sp = null;
            }
        }

        private string WriteAndReadSerialPort(string tx, string rx = "\n")
        {
            LogMethod();
            string tramaRx = "";

            if (!string.IsNullOrEmpty(tx))
            {
                SP.WriteLine(tx);

                logger.InfoFormat("TX:{0} ", tx);
            }
            
            var sampler = Sampler.Run(
            () => { return new SamplerConfig { Interval = 500, InitialDelay = 100, NumIterations = 5, CancelOnException = false, ThrowException = false }; },
            (step) =>
            {
                try
                {
                    tramaRx += SP.ReadLine();

                    logger.InfoFormat("RX: {0}", tramaRx);

                    step.Cancel = tramaRx.Contains(rx) && tramaRx.Length > rx.Length;          
                }
                catch (TimeoutException)
                {
                    // logger.WarnFormat("{0}, ", e.GetType().Name, e);
                }
            });

          SP.DiscardInBuffer();

          if(!sampler.Canceled)
           throw new Exception( "No se ha recibido la trama RX esperada");

          return tramaRx.Replace("\n", "");
        }

        private int convertDoubleToIntIEEE754(double value)
        {
            var valueToConvert = BitConverter.GetBytes((float)value);

            int outputValue = 0;
            for (int i = valueToConvert.Count(); i > 0; i--)
            {
                outputValue = outputValue << 8;
                outputValue += valueToConvert[i - 1];
            }

            return outputValue;
        }

        public struct CalibrationResults
        {
            public double calibrationValue;
            public double averageFrequency;
        }

        public enum Leds
        {
            LED1,
            LED2
        }

        public enum State
        {
            ON,
            OFF
        }

        public enum Registers
        {
            SERIAL_NUMBER = 0,
            HARDWARE_VERSION = 1,
            PRODUCT_VERSION = 3,
            FACTOR_ADC_CALIBRATION_A = 4,
            FACTOR_ADC_CALIBRATION_B = 6,
            ACTIVE_PAGE = 21,
            FRAME_NUMBER = 65 //0x41
        }
    }
}
