﻿using Instruments.Router;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(ChangeRouterIPDescription))]
    
    public class ChangeRouterIP : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del usuario en el router")]
        [DefaultValue("root")]
        public string UserName { get; set; }

        [Category("3. Configuration")]
        [Description("Contraseña")]
        [DefaultValue("dezac2309")]
        public string Password { get; set; }

        [Category("3. Configuration")]
        [Description("IP del router")]
        [DefaultValue("100.0.0.2")]
        public string IP { get; set; }

        [Category("3. Configuration")]
        [Description("Máscara de la IP del router")]
        [DefaultValue("255.255.255.0")]
        public string MaskIP { get; set; }

        [Category("3. Configuration")]
        [Description("Empezar una DHCP")]
        [DefaultValue(3)]
        public int StartDHCP { get; set; }

        [Category("3. Configuration")]
        [Description("Renovación de la IP del router")]
        [DefaultValue(true)]
        public bool RenewMyIP { get; set; }

        public override void Execute()
        {
            Router router = Router.Default();
            if (router == null)
                throw new System.Exception("Error, no se ha encontrado router DLink810 conectado a este PC o se ha podido iniciar DHCPServer");

            if (!string.IsNullOrEmpty(UserName))
            {
                router.UserName = UserName;
                router.Password = Password;
            }

            var config = new RouterConfig
            {
                LanIP = IP,
                LanMaskIP = MaskIP,
                StartDHCP = StartDHCP                     
            };

            if (router.RouterIP == IP && MaskIP == "255.255.255.0" && router is RouterDLink615)      
                return;
            
            var newConfig = router.ApplyConfig(config);
            Assert.AreEqual(config.LanIP, newConfig.LanIP, Error().SOFTWARE.ERROR_TRANSFERENCIA.ERROR_GRABACION("Error no se ha cambiando la IP del router"));
        }
    }

    public class ChangeRouterIPDescription : ActionDescription
    {
        public override string Name { get { return "ChangeRouterIP"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override string Description { get { return "Action para cambiar la IP del router"; } }
        public override Image Icon { get { return Properties.Resources.router_icon; } }

    }
}
