﻿namespace TaskRunner.Enumerators
{
    public enum StepExecutionResult
    {
        Pending = 0,
        Repeated = 1,
        Continued = 2,
        Aborted = 3,
        Failed = 4,
        Omitted = 5,
        Passed = 6,
        Disabled = 7,
    }
}
