namespace Zurc.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Parametro")]
    public partial class Parametro
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TipoParametro")]
        public int TipoParametroId { get; set; }

        [ForeignKey("Unidad")]
        public int UnidadId { get; set; }

        [ForeignKey("TipoValorParametro")]
        public int TipoValorId { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(200)]
        public string Descripcion { get; set; }

        public Unidad Unidad { get; set; }
        public TipoParametro TipoParametro { get; set; }
        public TipoValorParametro TipoValorParametro { get; set; }
    }
}
