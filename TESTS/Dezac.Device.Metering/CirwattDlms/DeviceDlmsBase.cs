﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.DLMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.03)]
    public abstract class DeviceDlmsBase : DeviceBase
    { 
        private DLMSDeviceSerialPort dlms;

        public DLMSDeviceSerialPort Dlms {
            get
            {
                if (dlms == null)
                {
                    dlms = new DLMSDeviceSerialPort();
                    dlms.ClientAddress = 73;
                    dlms.password = "qxzbravo";
                    //dlms.ServerAddres(1, 16);
                }
                return dlms;
            }   
        }

        public void SetPort(int port, int baudrate)
        {
            Dlms.PortCom =  port;
            Dlms.BaudRate = baudrate;
        }

        public bool IsOpen { get { return Dlms.IsOpen; } }

        public byte TimeOut { set { Dlms.WaitTime = value; } }

        public byte Retries { set { Dlms.RetryCount = value; } }

        public int ClientAddress { set { Dlms.ClientAddress = value; } }

        public void ServerAddres(int serverAddress, int physicalAddress)
        {
            Dlms.ServerAddres(serverAddress, physicalAddress);
        }

        public TypeMeterConection.enumTypeMeterConection Fases { get; set; }

        public controlAdjust adjustVariables { get; set; }

        public void StartSession()
        {
            LogMethod();
            Dlms.InitializeConnection();
        }

        public void CloseSession()
        {
            LogMethod();
            Dlms.Disconnect();
        }
   
        #region SerialNumber

        public string ReadSerialNumber()
        {
            LogMethod();
            return Dlms.ReadData<string>("0180801E00FF", 2);
        }

        public void WriteSerialNumber(string serialNumber)
        {
            LogMethod();
            Dlms.WriteData<UInt32>("0180801E00FF", 2, Convert.ToUInt32(serialNumber));
        }

        #endregion

        #region CodigoFabricante

        public string ReadManufactureCode()
        {
            LogMethod();
            var codCir = Dlms.ReadData<byte[]>("0180803100FF", 2);
            return ASCIIEncoding.ASCII.GetString(codCir);
        }

        public void WriteManufactureCode(string codeManufacture)
        {
            LogMethod();
            Dlms.WriteData<byte[]>("0180803100FF", 2, ASCIIEncoding.ASCII.GetBytes(codeManufacture));
        }

        #endregion

        #region CodigoUnesa

        public string ReadUnesaCode()
        {
            LogMethod();
            var unesa = Dlms.ReadData<byte[]>("0180802C00FF", 2);
            return ASCIIEncoding.ASCII.GetString(unesa);
        }

        public void WriteUnesaCode(string unesaCode)
        {
            LogMethod();
            Dlms.WriteData<byte[]>("0180802C00FF", 2, ASCIIEncoding.ASCII.GetBytes(unesaCode));
        }

        #endregion

        #region PartNumber

        public string ReadPartNumber()
        {
            LogMethod();
            var unesa = Dlms.ReadData<byte[]>("0.0.96.1.3.255", 2);
            return ASCIIEncoding.ASCII.GetString(unesa);
        }

        public void WritePartNumber(string partNumber)
        {
            LogMethod();
            Dlms.WriteData<byte[]>("0.0.96.1.3.255", 2, ASCIIEncoding.ASCII.GetBytes(partNumber));
        }

        #endregion

        #region Modelo

        public string ReadModel()
        {
            LogMethod();
            var model =  Dlms.ReadData<byte[]>("0180801F00FF", 2);
            return ASCIIEncoding.ASCII.GetString(model);
        }

        public void WriteModel(string model)
        {
            LogMethod();
            Dlms.WriteData<byte[]>("0180801F00FF", 2, ASCIIEncoding.ASCII.GetBytes(model));
        }

        #endregion

        #region FirmareVersion

        public UInt32 ReadCrcBinary()
        {
            LogMethod();
            return Dlms.ReadData<UInt32>("0180802E00FF", 2);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            var firmware = Dlms.ReadData<byte[]>("0100000200FF", 2);
            return ASCIIEncoding.ASCII.GetString(firmware);

        }

        public ushort ReadFirmwarePLCVersion()
        {
            LogMethod();
            return Dlms.ReadData<ushort>("0180802F00FF", 2);
        }

        #endregion

        #region FabricationNumber

        public UInt32 ReadFabricationNumber()
        {
            LogMethod();
            return Dlms.ReadData<UInt32>("0180802200FF", 2);
        }

        public void WriteFabricationNumber(UInt32 fabricationNumber)
        {
            LogMethod();
            Dlms.WriteData<UInt32>("0180802200FF", 2, fabricationNumber);
        }

        #endregion

        #region Fabrication Status

        public void WriteFabricationStatus(ushort fabricationStatus)
        {
            LogMethod();
            Dlms.WriteData<ushort>("0180802500FF", 2, fabricationStatus);
        }

        public ushort ReadFabricationStatus()
        {
            LogMethod();
            return Dlms.ReadData<ushort>("0180802500FF", 2);
        }

        #endregion

        #region DateTime

        public DateTime ReadDateTime()
        {
            LogMethod();
            return Dlms.ReadClock<DateTime>("0000010000FF", 2);
        }

        public void WriteDateTimeCurrent()
        {
            LogMethod();
            Dlms.WriteClock("0.0.1.0.0.255", 2, DateTime.Now);
        }

        public void WriteMDateTimeManufacture(DateTime fabricationDate)
        {
            LogMethod();
            Dlms.WriteData("0180802100FF", 2, DateTime2StringDlms(fabricationDate).HexStringToBytes());

        }

        public void WriteDateTimeVerification(DateTime verificationDate)
        {
            LogMethod();           
            Dlms.WriteData("0180802300FF", 2, DateTime2StringDlms(verificationDate).HexStringToBytes());
        }

        #endregion

        #region PPM

        public short ReadPPM()
        {
            LogMethod();
            return Dlms.ReadData<short>("0180802400FF", 2);
        }

        public void WritePPM(short PPM)
        {
            LogMethod();
            Dlms.WriteData<short>("0180802400FF", 2, PPM);
        }

        #endregion

        public void WriteLeds(ushort led)
        {
            LogMethod();
            Dlms.WriteData<ushort>("1.128.128.112.0.255", 2, led);
        }

        public double ReadTemperature()
        {
            LogMethod();
            return Dlms.ReadData<double>("0180802600FF", 2) / 10;
        }

        public string ReadPhysycalAdress()
        {
            LogMethod();
            var model = Dlms.ReadData<byte[]>("0.0.96.1.10.255", 2);
            return ASCIIEncoding.ASCII.GetString(model);
        }

        public string ReadMacPlc()
        {
            LogMethod();
            return Dlms.ReadData<string>("00001C0600FF", 2);
        }

        public void WriteReset()
        {
            LogMethod();
            Dlms.WriteData<UInt16>("0180806400FF", 2, 0xFFFF);
        }

        #region UpdateFirmware

        public void UpdateFirmware(string path)
        {
            LogMethod();
            Dlms.Update(path);
        }

        #endregion



        #region Inputs Outputs

        public enum enumStateRelay
        {
            Open = 1,
            Closed = 2,
        }

        public void WriteRelayEnergyControl(byte contador, enumStateRelay state)
        {
            LogMethod();
            //Dlms.WriteRegister<short>("0180802400FF", 2, PPM);

            //if (!Dlms.SetDisconnectorControl(contador))
            //    throw new Exception("Error de comunicaciones al escribir RelayEnergyControl por DLMS");
        }

        public void ReadRelayEnergyControl(byte contador)
        {
            LogMethod();
            //String[] obis = new String[5] { "000060030AFF", "000160030AFF", "000260030AFF", "000360030AFF", "000460030AFF" };
            var disconector = Dlms.ReadData<short>("000060030AFF", 2);
            var disconectorState = Dlms.ReadData<short>("000060030AFF", 4);
        }

        public void WriteRelayImpulseControl(byte contador, enumStateRelay state)
        {
            LogMethod();
            //String[] obis = new String[5] { "000060030BFF", "000160030BFF", "000260030BFF", "000360030BFF", "000460030BFF" };
            Dlms.WriteData<short>("000060030BFF", 2, contador);
        }

        public void ReadRelayImpulseControl(byte contador)
        {
            LogMethod();
            // String[] obis = new String[5] { "000060030BFF", "000160030BFF", "000260030BFF", "000360030BFF", "000460030BFF" };
            var disconector = Dlms.ReadData<short>("000060030BFF", 2);
            var disconectorState = Dlms.ReadData<short>("000060030BFF", 4);
        }

        public enum enumOuputs
        {
            NOTHING = 0,
            RELAY1 = 1,
            RELAY2 = 2,
            LED_PPM = 3,
        }

        public void WriteActivateOuputs(enumOuputs output)
        {
            LogMethod();
            Dlms.WriteRegister<byte>("0180807000FF", 2, (byte)output);
        }

        [Flags]
        public enum enumLeds
        {
            NULL = 0,
            ENERGY_1 = (1 << 0),
            ENERGY_2 = (1 << 1),
            ENERGY_3 = (1 << 2),
            ENERGY_4 = (1 << 3),
            INPUT_1 = (1 << 4),
            INPUT_2 = (1 << 5),
            INPUT_3 = (1 << 6),
            INPUT_4 = (1 << 7),
            SWITCH_1 = (1 << 8),
            SWITCH_2 = (1 << 9),
            SWITCH_3 = (1 << 10),
            SWITCH_4 = (1 << 11),
            PLC_GREEN = (1 << 12),
            PLC_RED = (1 << 13),
            VAR_1 = (1 << 14),
            VAR_2 = (1 << 15),
            VAR_3 = (1 << 16),
            VAR_4 = (1 << 17),
            VAR_5 = (1 << 18),
            ALL_ENERGY = ENERGY_1 | ENERGY_2 | ENERGY_3 | ENERGY_4,
            ALL_INPUTS = INPUT_1 | INPUT_2 | INPUT_3 | INPUT_4,
            ALL_SWITCH = SWITCH_1 | SWITCH_2 | SWITCH_3 | SWITCH_4,
            SEPARATE_LED = ENERGY_1 | ENERGY_3 | SWITCH_2 | SWITCH_4 | INPUT_1 | INPUT_3 | VAR_2,
            REST_SEPARATE_LED = ENERGY_2 | ENERGY_4 | INPUT_2 | INPUT_4 | SWITCH_1 | SWITCH_3 | VAR_1,
        }

        public void WriteActivateLeds(enumLeds led)
        {
            LogMethod();
            Dlms.WriteRegister<byte>("0180807000FF", 2, (byte)led);
        }

        public void WriteActivateDisplay(byte display)
        {
            LogMethod();
            Dlms.WriteRegister<byte>("0180806E00FF", 2, display);
        }

        [Flags]
        public enum enumKeyInputs
        {
            NOTHING = 0,
            IN1 = (1 << 2),
            IN2 = (1 << 3),
            IN3 = (1 << 4),
            IN4 = (1 << 5),
            REARME = (1 << 6)
        }

        public enumKeyInputs ReadInputsImpulse()
        {
            LogMethod();
            var disconector = Dlms.ReadRegister<byte>("0180806F00FF", 2);
            return (enumKeyInputs)disconector;
        }

        public enum enumKeyBoard
        {
            NOTHING = 0,
            TECLA1 = (1 << 0),
            TECLA2 = (1 << 1),
        }

        public enumKeyBoard ReadKeyboard()
        {
            LogMethod();
            var disconector = Dlms.ReadRegister<byte>("0180806F00FF", 2);
            return (enumKeyBoard)disconector;
        }

        #endregion

        //********************************************************
        //*********************************************************

        //1.128.128.12.1.255
        internal string[] OBIS_ACTIVE_GAIN_50Hz = { "0180800C01FF", "0180800C02FF", "0180800C03FF", "0180800C04FF", "0180800C05FF", "0180800C06FF" };
        //1.128.128.12.17.255
        internal string[] OBIS_ACTIVE_GAIN_60Hz = { "0180800C11FF", "0180800C12FF", "0180800C13FF", "0180800C14FF", "0180800C15FF", "0180800C16FF" };
        //1.128.128.13.1.255
        internal string[] OBIS_REACTIVE_GAIN_50Hz = { "0180800D01FF", "0180800D02FF", "0180800D03FF", "0180800D04FF", "0180800D05FF", "0180800D06FF" };
        //1.128.128.13.17.255
        internal string[] OBIS_REACTIVE_GAIN_60Hz = { "0180800D11FF", "0180800D12FF", "0180800D13FF", "0180800D14FF", "0180800D15FF", "0180800D16FF" };
        //1.128.128.14.1.255
        internal string[] OBIS_ACTIVE_OFFSET_GAIN = { "0180800E01FF", "0180800E02FF", "0180800E03FF", "0180800E04FF", "0180800E05FF", "0180800E06FF" };
        //1.128.128.15.1.255
        internal string[] OBIS_REACTIVE_OFFSET_GAIN = { "0180800F01FF", "0180800F02FF", "0180800F03FF", "0180800F04FF", "0180800F05FF", "0180800F06FF" };
        //1.128.128.16.1.255
        internal string[] OBIS_PHASE_OFFSET_GAIN_50Hz = { "0180801001FF", "0180801002FF", "0180801003FF", "0180801004FF", "0180801005FF", "0180801006FF" };
        //1.128.128.16.17.255
        internal string[] OBIS_PHASE_OFFSET_GAIN_60Hz = { "0180801011FF", "0180801012FF", "0180801013FF", "0180801014FF", "0180801005FF", "0180801006FF" };
        //1.128.128.10.1.255
        internal string[] OBIS_VOLTAGE_GAIN = { "0180800A01FF", "0180800A02FF", "0180800A03FF", "0180800A04FF", "0180800A05FF", "0180800A06FF" };
        //1.128.128.20.1.255
        internal string[] OBIS_VOLTAGE_POINTS = { "0180801401FF", "0180801402FF", "0180801403FF", "0180801404FF", "0180801405FF", "0180801406FF" };
        //1.128.128.22.1.255
        internal string[] OBIS_ACTIVE_POINTS = { "0180801601FF", "0180801602FF", "0180801603FF", "0180801604FF", "0180801605FF", "0180801606FF" };
        //1.128.128.23.1.255
        internal string[] OBIS_REACTIVE_POINTS = { "0180801701FF", "0180801702FF", "0180801703FF", "0180801704FF", "0180801705FF", "0180801706FF" };

        //1.128.128.124.1.255
        internal string[] OBIS_BETWEEN_VOLTAGE_GAIN = { "1.128.128.124.1.255", "1.128.128.124.2.255", "1.128.128.124.3.255" };
        //1.128.128.123.1.255
        internal string[] OBIS_BETWEEN_VOLTAGE = { "1.128.128.123.1.255", "1.128.128.123.2.255", "1.128.128.123.3.255" };

        //1.128.128.124.1.255
        internal string[] OBIS_CURRENT_GAIN = { "1.128.128.12.1.255", "1.128.128.12.2.255", "1.128.128.12.3.255" };
        //1.128.128.123.1.255
        internal string[] OBIS_CURRENT = { "1.128.128.22.1.255", "1.128.128.22.2.255", "1.128.128.22.3.255" };

        public struct structPhaseOffset
        {
            public double anguloDesfase;
            public double errorDesfase;
            public short puntosErrorDesfase;
        }

        public List<structPhaseOffset> phaseOffset { get; set; }

        //---------------------------------------------------------------------------------------------------
        
        internal short CalculateGap(StatisticalList points, double MiPFref, double MiFactorDesfase, int positionActiva, int positionReactiva)
        {
            var phaseOffset = new structPhaseOffset();
            phaseOffset.anguloDesfase = (Math.Atan2(points.Average(positionReactiva), points.Average(positionActiva))).ToDegree();
            phaseOffset.errorDesfase = MiPFref - phaseOffset.anguloDesfase;
            phaseOffset.puntosErrorDesfase = (short)((phaseOffset.errorDesfase * MiFactorDesfase));
            return phaseOffset.puntosErrorDesfase;
        }
       
        internal ushort CalculateVoltageGain(StatisticalList points, double voltageReference, int rang)
        {
            return (ushort)((Math.Sqrt(points.Average(rang)) * 100) / (voltageReference));
        }
        
        internal double CalculatePowerActive(StatisticalList points, double powerReference, int rang)
        {
            return (points.Average(rang) / powerReference);
        }

        private string DateTime2StringDlms(DateTime clock)
        {
            string result = "";
            byte dayW = Convert.ToByte(clock.DayOfWeek);


            result += clock.Year.ToString("X4");
            result += clock.Month.ToString("X2");
            result += clock.Day.ToString("X2");
            if (dayW == 0)
                result += "07";
            else
                result += dayW.ToString("X2");
            result += clock.Hour.ToString("X2");
            result += clock.Minute.ToString("X2");
            result += clock.Second.ToString("X2");
            result += "00800000";

            return result;
        }

        public override void Dispose()
        {
            CloseSession();
            dlms = null;
        }
    }
}
