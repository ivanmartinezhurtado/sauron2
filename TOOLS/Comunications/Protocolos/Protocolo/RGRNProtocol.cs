﻿using Comunications.Message;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comunications.Protocolo
{
    public class RGRGProtocol : ASCIIProtocol
    {
        private const byte LENGTH = 6;

        public override byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            var data = new List<byte>();
            byte[] ch = new byte[LENGTH];

            do
            {
                int p = streamResource.Read(ch, 0, 1);
                if (p > 0)
                {
                    byte b = ch[0];
                    data.Add(b);
                }
            } while (data.Count < LENGTH);

            return data.Take(data.Count).ToArray();
        }
    }
}
