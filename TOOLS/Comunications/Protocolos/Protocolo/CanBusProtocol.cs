﻿using Comunications.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comunications.Protocolo
{
    public class CanBusProtocol : ASCIIProtocol
    {
        private const byte LENGTH = 15;

        public override byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            byte[] data = new byte[LENGTH];

            int p = streamResource.Read(data, 0, LENGTH);
            
            if (p > 0)
                return data;

            return null;
        }
    }
}
