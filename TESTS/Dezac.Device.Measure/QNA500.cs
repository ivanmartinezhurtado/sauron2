﻿namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class QNA500 : DeviceBase
    {
        #region Instanciación del equipo

        public QNA500()
        {
        }

        public QNA500(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public QNA500(byte portCom = 7)
        {
            Modbus = new ModbusDeviceSerialPort(portCom, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #endregion

        #region Operaciones de registros

        public int ResetAndReadIP()
        {
            return Modbus.ReadInt32((ushort)Registers.Integers.IP_RESET);
        }

        public FasesNeutro ReadVoltages()
        {
            return Modbus.Read<FasesNeutro>((ushort)Registers.Integers.VOLTAGE_MEASUREMENTS);
        }

        public FasesNeutro ReadCurrents()
        {
            return Modbus.Read<FasesNeutro>((ushort)Registers.Integers.CURRENT_MEASUREMENTS);
        }

        public QNADateTime ReadDateTime()
        {
            return Modbus.Read<QNADateTime>((ushort)Registers.Integers.CLOCK);
        }

        public ushort ReadSynchronismSignal()
        {
            return Modbus.ReadRegister((ushort)Registers.Integers.SYNCHRONISM);
        }

        public ushort ReadPowerOnSignal()
        {
            return Modbus.ReadRegister((ushort)Registers.Integers.POWER_ON);
        }

        public int ReadExternalRam()
        {
            return Modbus.ReadInt32((ushort)Registers.Integers.EXTERNAL_RAM);
        }

        public ushort ReadProvaFileGeneration()
        {
            return Modbus.ReadRegister((ushort)Registers.Integers.CHECK_PROVA_FILE_GENERATED);
        }

        public ushort ReadProvaFileDeletion()
        {
            return Modbus.ReadRegister((ushort)Registers.Integers.CHECK_PROVA_FILE_CLEARED);
        }

        public int ReadFrameNumber()
        {
            return Modbus.ReadInt32((ushort)Registers.Integers.FRAME_NUMBER);
        }

        public IP ReadIP()
        {
            return Modbus.Read<IP>((ushort)Registers.Integers.IP);
        }

        public void WriteMAC()
        {
            Modbus.Write<MAC>((ushort)Registers.Integers.MAC, new MAC { Direction1 = 0x4545, Direction2 =  0x4545, Direction3 =  0x4545 });
        }

        public void WriteFlagDHCP()
        {
            Modbus.WriteSingleCoil((ushort)Registers.Relays.DHCP, true);
        }

        public void WriteFlagTest()
        {
            Modbus.WriteSingleCoil((ushort)Registers.Relays.FLAG_TEST, true);
        }

        public void WriteDateTime(QNADateTime dateTime)
        {
            Modbus.Write<QNADateTime>((ushort)Registers.Integers.CLOCK, dateTime);
        }

        public void WriteFrameNumber(int frameNumber)
        {
            Modbus.WriteInt32((ushort)Registers.Integers.FRAME_NUMBER, frameNumber);
        }

        public void WriteLed(Registers.Leds led, bool state)
        {
            Modbus.WriteSingleCoil((ushort)led, state);
        }

        public void WriteGenerateProvaFile()
        {
            Modbus.WriteSingleCoil((ushort)Registers.Relays.GENERATE_PROVA_FILE, true);
        }

        public void WriteClearProvaFile()
        {
            Modbus.WriteSingleCoil((ushort)Registers.Relays.GENERATE_PROVA_FILE, false);
        }

        #endregion

        #region Estructuras de datos

        public struct FasesNeutro
        {
            public int L1;
            public int L2;
            public int L3;
            public int LN;
        }

        public struct QNADateTime
        {
            public ushort Day;
            public ushort Month;
            public ushort Year;
            public ushort Hour;
            public ushort Minute;
            public ushort Second;
        }

        public struct IP
        {
            public ushort Direction1;
            public ushort Direction2;
            public ushort Direction3;
            public ushort Direction4;
            public ushort Direction5;
            public ushort Direction6;
            public ushort Direction7;
            public ushort Direction8;
            public ushort Direction9;
            public ushort Direction10;
        }

        public struct MAC
        {
            public ushort Direction1;
            public ushort Direction2;
            public ushort Direction3;
        }

        #endregion

        #region Registros

        public struct Registers
        {
            public enum Leds
            {
                LED_1 = 0x0FA0,
                LED_2 = 0x0FA1,
                LED_3 = 0x0FA2,
                LED_4 = 0x0FA3
            }

            public enum Relays
            {
                DHCP = 0x0C26,
                FLAG_TEST = 0x2AF8,
                GENERATE_PROVA_FILE = 0x0C1C
            }

            public enum Integers
            {
                MAC = 0x2788,
                VOLTAGE_MEASUREMENTS = 0xC396,
                CURRENT_MEASUREMENTS = 0xC3A0,
                CLOCK = 0x27d8,
                FRAME_NUMBER = 0x277E,
                SYNCHRONISM = 0x2793,
                POWER_ON = 0x2792,
                EXTERNAL_RAM = 0x2B02,
                CHECK_PROVA_FILE_GENERATED = 0x2B07,
                CHECK_PROVA_FILE_CLEARED= 0x2B08,
                IP = 0x2760,
                IP_RESET = 0x27BE
            }
        }

        #endregion
    }
}