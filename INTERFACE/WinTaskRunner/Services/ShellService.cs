﻿using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using WinTaskRunner.Views;

namespace WinTaskRunner.Services
{
    public class ShellService : IShell
    {
        private TaskScheduler ts;
        private SynchronizationContext sc;
        private Form owner;

        public void Init(Form owner)
        {
            sc = SynchronizationContext.Current;
            ts = TaskScheduler.FromCurrentSynchronizationContext();
            this.owner = owner;
        }

        public DialogResult MsgBox(string text, string caption)
        {
            return RadMessageBox.Show(text, caption);
        }

        public DialogResult MsgBox(string text, string caption, MessageBoxButtons buttons)
        {
            return RadMessageBox.Show(text, caption, buttons);
        }

        public DialogResult MsgBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            RadMessageIcon ricon = RadMessageIcon.Info;

            switch (icon)
            {
                case MessageBoxIcon.Question:
                    ricon = RadMessageIcon.Question;
                    break;
                case MessageBoxIcon.Exclamation:
                    ricon = RadMessageIcon.Exclamation;
                    break;
                case MessageBoxIcon.Error:
                    ricon = RadMessageIcon.Error;
                    break;
            }

            return RadMessageBox.Show(text, caption, buttons, ricon);
        }

        //**********************************************************

        public DialogResult ShowDialog(string title, Control control, MessageBoxButtons? buttons, Action<Control> onLoad = null)
        {
            DialogResult result = DialogResult.None;

            sc.Send(_ =>
            {
                var form = new DialogView(title, control, buttons);

                if (onLoad != null)
                    form.Load += (s, e) => { onLoad(form); };

                form.ActiveControl = control;
                result = form.ShowDialog(owner);
            }, null);

            return result;
        }

        public DialogResult ShowDialog(string title, Func<Control> control, MessageBoxButtons? buttons, string description = null, int waitTime = 500)
        {
            DialogResult result = DialogResult.None;
            Exception uiException = null;

            sc.Send(_ =>
            {
                try
                {
                    using (Control ctrl = control())
                    {
                        var form = ctrl as Form;
                        if (form == null)
                        {
                            form = new DialogView(title, ctrl, buttons, description, waitTime);
                            form.ActiveControl = ctrl;
                        }
                        result = form.ShowDialog(owner);
                    }
                }
                catch (Exception ex)
                {
                    uiException = ex;
                }
            }, null);

            if (uiException != null)
                throw uiException;

            return result;
        }

        public T ShowDialog<T>(string title, Func<Control> control, MessageBoxButtons? buttons, Func<Control, DialogResult, T> result)
        {
            var res = default(T);

            sc.Send(_ =>
            {

                using (Control ctrl = control())
                {

                    var form = ctrl as Form;
                    if (form == null)
                    {
                        form = new DialogView(title, ctrl, buttons);
                        form.ActiveControl = ctrl;
                    }
                    DialogResult dialogResult = form.ShowDialog(owner);
                    res = result(ctrl, dialogResult);
                }
            }, null);

            return res;
        }

        public Form ShowForm(Func<Control> control)
        {
            Form form = null;

            sc.Send(_ =>
            {
                Control ctrl = control();

                form = ctrl as Form;
                if (form == null)
                {
                    form = new DialogView(ctrl.Text, ctrl, MessageBoxButtons.OK);
                    form.ActiveControl = ctrl;
                }
                form.Show();
            }, null);

            return form;
        }

        //***********************************************************

        public void Run(Action action)
        {
            sc.Post(_ => action(), null);
        }

        public Task<T> Run<T>(Func<T> action)
        {
            return Task.Factory.StartNew<T>(() =>
            {
                return action();

            }, CancellationToken.None, TaskCreationOptions.None, ts);
        }

        public void RunSync(Action action)
        {
            sc.Send(_ => action(), null);
        }

        /// Non IShell methods

        public DialogResult ShowTableDialog(string title, object data, MessageBoxButtons buttons = MessageBoxButtons.OK)
        {
            return ShowDialog(title, new TableGridView(data), buttons);
        }

        public DialogResult ShowTableDialog(string title, TableGridViewConfig config, MessageBoxButtons buttons = MessageBoxButtons.OK)
        {
            return ShowDialog(title, new TableGridView(config), buttons);
        }

        public T ShowTableDialog<T>(string title, object data)
        {
            return ShowTableDialog<T>(title, new TableGridViewConfig { Data = data });
        }

        public T ShowTableDialog<T>(string title, TableGridViewConfig config)
        {
            using (var tbl = new TableGridView(config))
            {
                if (ShowDialog(title, tbl, MessageBoxButtons.OKCancel) == DialogResult.OK)
                    return tbl.GetCurrent<T>();
            }

            return default(T);
        }

        public T ShowInputKeyboard<T>(string title, string promptText, T defaultValue, string mask = null, string initValue = null)
        {
            using (var ctrl = new InputKeyBoard(promptText, mask, initValue))
            {
                if (ShowDialog(title, ctrl, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    string value = ctrl.TextValue;

                    Type type = typeof(T);

                    if (type.Name == "Nullable`1")
                    {
                        if (value == null || value.ToString() == "")
                            return default(T);

                        return (T)Convert.ChangeType(value, type.GetGenericArguments()[0]);
                    }

                    return (T)Convert.ChangeType(value, type);
                }
            }

            return defaultValue;
        }
    }
}
