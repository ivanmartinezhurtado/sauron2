﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Dezac.Tests.UserControls
{
    public partial class ImageViewAOI : DialogViewBase
    {
        private string imagePath;

        public ImageViewAOI()
        {
            InitializeComponent();
        }

        public ImageViewAOI(string titulo, string rutaImagen)
            : this()
        {
            ImagePath = rutaImagen;
        }

        public ImageViewAOI(string rutaImagen, int width, int height)
            : this()
        {
            Width = width;
            Height = height;
            ImagePath = rutaImagen;
        }

        public ImageViewAOI(string rutaImagen)
            : this()
        {
            ImagePath = rutaImagen;
        }

        public ImageViewAOI(Bitmap imagen)
            : this()
        {
            Imagen = imagen;
        }


        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                if (imagePath != value)
                {
                    imagePath = value;
                    Imagen = Image.FromFile(imagePath);
                }
            }
        }

        public Image Imagen
        {
            get { return PictureImage.Image; }
            set { PictureImage.Image = value; }
        }

        protected override void Dispose(bool disposing)
        {
            if (Imagen != null) 
                Imagen.Dispose();
            base.Dispose(disposing);
        }
    }
}
