﻿using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using log4net;
using System;
using System.Globalization;
using System.IO.Ports;

namespace Instruments.Auxiliars
{
    [InstrumentsVersion(1.00)]
    public class HoneywellScanner :  IDisposable
    {
        protected IProtocol protocol;
        protected MessageTransport transport;
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(HoneywellScanner));

        protected CultureInfo enUS = new CultureInfo("en-US");
        private SerialPort sp;
        private byte _port = 5;

        private const string TRIGGER_ON = "16540D";  //SYN T CR ASCII
        private const string TRIGGER_OFF = "16550D";  //SYN U CR ASCII


        public HoneywellScanner()
        {
        }

        public HoneywellScanner(byte portName)
        {
            InitializeComponent();

            protocol = new ASCIIProtocol();
            protocol.CaracterFinRx = "\n";
            protocol.CaracterFinTx = string.Empty;
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
            PortCom = portName;
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }
  
        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = sp.ReadTimeout = 5000;
            sp.BaudRate = 115200;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
            sp.DtrEnable = true;          
        }

        public string Read(bool? throwException = null)
        {
            var message = HexStringMessage.Create(enUS, TRIGGER_ON);

            if (!sp.IsOpen)
                sp.Open();

            var response = transport.SendMessage<HexStringMessage>(message);

            _logger.InfoFormat("Reading: {0} ", response);

            return response.Text;
        }

  
        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
    }
}
