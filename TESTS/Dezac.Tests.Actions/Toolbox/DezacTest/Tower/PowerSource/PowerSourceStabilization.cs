﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(PowerSourceStabilizationDescription))]
    public class PowerSourceStabilization : ActionBase
    {
        [Category("3.1 Voltage")]
        [DefaultValue(230)]
        [Description("Voltage L1")]
        public double V_L1 { get; set; }

        [Category("3.1 Voltage")]
        [DefaultValue(230)]
        [Description("Voltage L2")]
        public double V_L2 { get; set; }

        [Category("3.1 Voltage")]
        [Description("Voltage L3")]
        [DefaultValue(230)]
        public double V_L3 { get; set; }

        [Category("3.2 Current")]
        [DefaultValue(0)]
        [Description("Corriente L1")]
        public double I_L1 { get; set; }

        [CategoryAttribute("3.2 Current")]
        [DefaultValue(0)]
        [Description("Corriente L2")]
        public double I_L2 { get; set; }

        [CategoryAttribute("3.2 Current")]
        [DefaultValue(0)]
        [Description("Corriente L3")]
        public double I_L3 { get; set; }

        [CategoryAttribute("3.3 Power Factor Angle")]
        [DefaultValue(0)]
        [Description("Angulo Factor de potencia L1")]
        public double PF_L1 { get; set; }

        [CategoryAttribute("3.3 Power Factor Angle")]
        [DefaultValue(0)]
        [Description("Angulo Factor de potencia L2")]
        public double PF_L2 { get; set; }

        [CategoryAttribute("3.3 Power Factor Angle")]
        [DefaultValue(0)]
        [Description("Angulo Factor de potencia L3")]
        public double PF_L3 { get; set; }

        [CategoryAttribute("3.4 Gap between lines")]
        [DefaultValue(0)]
        [Description("Angulo L1")]
        public double Gap_L1 { get; set; }

        [CategoryAttribute("3.4 Gap between lines")]
        [DefaultValue(120)]
        [Description("Angulo L2")]
        public double Gap_L2 { get; set; }

        [CategoryAttribute("3.4 Gap between lines")]
        [DefaultValue(240)]
        [Description("Angulo L3")]
        public double Gap_L3 { get; set; }

        [CategoryAttribute("3.5 Frequency")]
        [DefaultValue(50)]
        [Description("Frecuencia")]
        public double Frequency { get; set; }

        [CategoryAttribute("3. Configuration")]
        [DefaultValue(false)]
        [Description("Estabilizacion Interna")]
        public bool InternalStabilization { get; set; }

        [CategoryAttribute("3. Configuration")]
        [DefaultValue(0.1)]
        [Description("Tolerance para la estabilizacion interna de la MTE")]
        public double Tolerance { get; set; }

        [CategoryAttribute("3. Configuration")]
        [DefaultValue(3000)]
        [Description("Espera antes de la primera muestra")]
        public int WaitBeforeSample { get; set; }

        [CategoryAttribute("3. Configuration")]
        [DefaultValue(35000)]
        [Description("TimeOut")]
        public int Timeout { get; set; }

        [CategoryAttribute("3.6 Estabilization")]
        [DefaultValue(5)]
        [Description("Numero de muestras consecutivas")]
        public int Samples { get; set; }

        [CategoryAttribute("3.6 Estabilization")]
        [DefaultValue(1500)]
        [Description("Timepo entre muestras consecutivas")]
        public int TimeSamples { get; set; }

        [CategoryAttribute("3.6 Estabilization")]
        [DefaultValue(10)]
        [Description("Numero maximo de muestras")]
        public int MaxSamples { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(0.1)]
        [Description("Tolerancia voltage")]
        public double VoltageTolerance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(100)]
        [Description("Maxima varianza voltage")]
        public double VoltageMaxVariance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(0.1)]
        [Description("Tolerancia corriene")]
        public double CurrentTolerance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(100)]
        [Description("Maxima varianza corriene")]
        public double CurrentMaxVariance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(1)]
        [Description("Maxima grados de error para el angulo del factor potencia")]
        public double PFDegreeLimit { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(100)]
        [Description("Maxima varianza de error para el angulo del factor potencia")]
        public double PFDegreeMaxVariance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(1)]
        [Description("Maxima grados desfase")]
        public double GapDegreeLimit { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(100)]
        [Description("Maxima varianza desfase")]
        public double GapDegreeMaxVariance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(0.1)]
        [Description("Maxima tolerancia frecuencia")]
        public double FrequencyTolerance { get; set; }

        [CategoryAttribute("4.1 Estabilization Validation")]
        [DefaultValue(100)]
        [Description("Maxima varianza frecuencia")]
        public double FrequencyMaxVaraince { get; set; }

        [CategoryAttribute("4. Results")]
        [Description("Descripcion del punto  de ensayo")]
        [DefaultValue("")]
        public string TestPoint { get; set; }

        [CategoryAttribute("3.7 Comunications")]
        [DefaultValue(null)]
        [Description("Puerto Serie PSIII")]
        public string SerialPort { get; set; }

        [CategoryAttribute("3.7 Comunications")]
        [DefaultValue(null)]
        [Description("Puerto Serie Patron")]
        public string SerialPortPatron { get; set; }

        [CategoryAttribute("3.7 Comunications")]
        [DefaultValue(true)]
        [Description("Use MTE Watimetro Canal Salida 120A")]
        public bool Output120A { get; set; }

        [CategoryAttribute("3.7 Comunications")]
        [DefaultValue(false)]
        [Description("Use MTE Watimetro")]
        public bool UseWattimetro { get; set; }

        private ITower tower;
        private TestBase testBase;

        public override void Execute()
        {
            testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();

            if (!string.IsNullOrEmpty(SerialPort))
            {
                var portPowerSource = Convert.ToInt32(VariablesTools.ResolveValue(SerialPort));
                tower.PortMte = portPowerSource;
                tower.PortZera = portPowerSource;
            }

            if (!string.IsNullOrEmpty(SerialPortPatron))
            {
                var portPatron = Convert.ToByte(VariablesTools.ResolveValue(SerialPortPatron));
                tower.PortMteWattimetro = portPatron;
            }

            var config = new PowerSourceIIIStabilizationExtension.PowerSourceIIIConfiguration()
            {
                V_L1 = V_L1,
                V_L2 = V_L2,
                V_L3 = V_L3,
                I_L1 = I_L1,
                I_L2 = I_L2,
                I_L3 = I_L3,
                PF_L1 = PF_L1,
                PF_L2 = PF_L2,
                PF_L3 = PF_L3,
                Gap_L1 = Gap_L1,
                Gap_L2 = Gap_L2,
                Gap_L3 = Gap_L3,
                Frequency = Frequency,
                InternalStabilization = InternalStabilization,
                Samples = Samples,
                MaxSamples = MaxSamples,
                InitDelay = WaitBeforeSample,
                Timeout = Timeout,
                TimeSamples = TimeSamples,
                TestPoint = TestPoint,
                Tolerance = Tolerance,
                VoltageTolerance = VoltageTolerance,
                VoltageMaxVariance = VoltageMaxVariance,
                CurrentTolerance = CurrentTolerance,
                CurrentMaxVariance = CurrentMaxVariance,
                PFDegreeTolerance = PFDegreeLimit,
                PFDegreeMaxVariance = PFDegreeMaxVariance,
                FrequencyMaxVaraince = FrequencyMaxVaraince,
                FrequencyTolerance = FrequencyTolerance,
                GapDegreeMaxVariance = GapDegreeMaxVariance,
                GapDegreeTolerence = GapDegreeLimit,
                UseWatimetro = UseWattimetro,
                Output120A = Output120A
            };

           testBase.PowerSourceIIIStabilization(tower, config);
        }

    }
    
    public class PowerSourceStabilizationDescription : ActionDescription
    {
        public override string Name { get { return "PowerSourceStabilization"; } }
        public override string Description { get { return "Consgina y estabilización de precisión para las fuentes de ajuste"; } }
        public override string Category { get { return "DezacTest/Tower/PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;PowerSourcesShutdown:2;TowerDispose:2"; } }
    }
}