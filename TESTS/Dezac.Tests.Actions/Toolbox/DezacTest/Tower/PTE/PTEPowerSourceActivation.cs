﻿using Dezac.Tests.Services;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.TowerTools
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(PTEPowerSourceActivationActionDescription ))]
    [Browsable(true)]
    public class PTEPowerSourceActivation : ActionBase
    {
        [DefaultValue(12)]
        [Description("La tensión de consigna para la fuente")]
        public double Voltage { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();

            tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;
            tower.PTE.Presets.Voltage = Voltage;
            tower.PTE.ApplyAndWaitStabilisation();
        }
    }

    public class PTEPowerSourceActivationActionDescription : ActionDescription
    {
        public override string Name { get { return "ActivatePTE"; } }
        public override string Category { get { return "DezacTest/Tower/TowerI"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }
}
