﻿using Dezac.Tests.AOI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.AOI.Models
{
    public class AOIStatistics
    {
        [JsonRequired]
        private List<HalconInspectionResults> testResults;
        [JsonIgnore]
        public double DPMO { get { if (Total == 0) return 0; return 1000000.0 * KO / Total; } }
        [JsonIgnore]
        public List<int> NumProductList { get { if (testResults.Count == 0) return new List<int>(); return testResults.Select((s) => s.NumProduct).Distinct().ToList(); } }
        [JsonIgnore]
        public List<int> NumOrdenList { get { if (testResults.Count == 0) return new List<int>(); return testResults.Select((s) => s.NumOrden).Distinct().ToList(); } }
        [JsonIgnore]
        public List<string> IdFaseList { get { if (testResults.Count == 0) return new List<string>(); return testResults.Select((s) => s.IdFase).Distinct().ToList(); } }
        [JsonIgnore]
        public DateTime Date { get { if (testResults.Count == 0) return DateTime.MinValue; return testResults.Max(results => results.Date); } }
        [JsonIgnore]
        public int NumProduct { get { if (testResults.Count == 0) return 0; return NumProductList[0]; } }
        [JsonIgnore]
        public int NumOrden { get { if (testResults.Count == 0) return 0; return NumOrdenList[0]; } }
        [JsonIgnore]
        public string IdFase { get { if (testResults.Count == 0) return ""; return IdFaseList[0]; } }
        [JsonIgnore]
        public int NumCircuitos { get { if (testResults.Count == 0) return 0; return testResults.Count * testResults[0].Boards.Count - (Descartado / testResults[0].Boards[0].Weldigns.Count); } }
        [JsonIgnore]
        public int Total { get { if (testResults.Count == 0) return 0; return OK + KO; } }
        [JsonIgnore]
        public int OK
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.SIN_REPARACION || repair == RepairEnum.FALSO_FALLO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int KO
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair != RepairEnum.SIN_REPARACION && repair != RepairEnum.FALSO_FALLO && repair != RepairEnum.DESCARTADO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int FalsoFallo
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.FALSO_FALLO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int FaltaEstaño
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.FALTA_ESTAÑO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int FormaDefectuosa
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.FORMA_DEFECTUOSA)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int Cruce
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.CRUCE)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int PataLarga
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.PATA_LARGA)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int FaltaComponente
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.FALTA_COMPONENTE)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int ComponenteLevantado
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.COMPONENTE_LEVANTADO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int OtroFallo
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.OTRO_FALLO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public int Descartado
        {
            get
            {
                int number = 0;
                foreach (HalconInspectionResults test in testResults)
                foreach (var repair in test.WeldingsRepair)
                    if (repair == RepairEnum.DESCARTADO)
                        number++;
                return number;
            }
        }
        [JsonIgnore]
        public List<int> WeldingFailureCount
        {
            get
            {
                List<int> list = new List<int>(new int[testResults[0].Boards[0].Weldigns.Count]);
                foreach (HalconInspectionResults test in testResults)
                foreach (var board in test.Boards)
                    for (int i = 0; i < board.Weldigns.Count; i++)
                        if (board.Weldigns[i].Repair != RepairEnum.FALSO_FALLO
                            && board.Weldigns[i].Repair != RepairEnum.SIN_REPARACION
                            && board.Weldigns[i].Repair != RepairEnum.DESCARTADO)
                            list[i]++;
                return list;
            }
        }
        [JsonIgnore]
        public List<Dictionary<RepairEnum, int>> WeldingFailureCountByType
        {
            get
            {
                List<Dictionary<RepairEnum, int>> list = new List<Dictionary<RepairEnum, int>>();
                for (int i = 0; i < testResults[0].Boards[0].Weldigns.Count; i++)
                    list.Add(new Dictionary<RepairEnum, int>()
                    {
                        {RepairEnum.COMPONENTE_LEVANTADO, 0 },
                        {RepairEnum.CRUCE, 0 },
                        {RepairEnum.FALSO_FALLO, 0 },
                        {RepairEnum.FALTA_COMPONENTE, 0 },
                        {RepairEnum.FALTA_ESTAÑO, 0 },
                        {RepairEnum.FORMA_DEFECTUOSA, 0 },
                        {RepairEnum.OTRO_FALLO, 0 },
                        {RepairEnum.PATA_LARGA, 0 },
                        {RepairEnum.SIN_REPARACION, 0 },
                        {RepairEnum.DESCARTADO, 0 },
                    });

                foreach (HalconInspectionResults test in testResults)
                foreach (var board in test.Boards)
                    for (int i = 0; i < board.Weldigns.Count; i++)
                        list[i][board.Weldigns[i].Repair]++;
                return list;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> OKTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.SIN_REPARACION || repair == RepairEnum.FALSO_FALLO)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> KOTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair != RepairEnum.SIN_REPARACION && repair != RepairEnum.FALSO_FALLO && repair != RepairEnum.DESCARTADO)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> FalsoFalloTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.FALSO_FALLO)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> FaltaEstañoTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.FALTA_ESTAÑO)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> FormaDefectuosaTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.FORMA_DEFECTUOSA)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> CruceTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.CRUCE)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> PataLargaTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.PATA_LARGA)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> FaltaComponenteTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.FALTA_COMPONENTE)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> ComponenteLevantadoTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.COMPONENTE_LEVANTADO)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }
        [JsonIgnore]
        public List<KeyValuePair<DateTime, int>> OtroFalloTimeSerie
        {
            get
            {
                List<KeyValuePair<DateTime, int>> serie = new List<KeyValuePair<DateTime, int>>();
                foreach (HalconInspectionResults test in testResults)
                {
                    int number = 0;
                    foreach (var repair in test.WeldingsRepair)
                        if (repair == RepairEnum.OTRO_FALLO)
                            number++;
                    serie.Add(new KeyValuePair<DateTime, int>(test.Date, number));
                }
                return serie;
            }
        }

        public void Add(HalconInspectionResults results)
        {
            testResults.Add(results);
        }

        public void JointStatistics(AOIStatistics results)
        {
            testResults.AddRange(results.testResults);
        }

        public AOIStatistics()
        {
            testResults = new List<HalconInspectionResults>();
        }

        public class MyList<T> : List<T>
        {
            public MyList()
              : base()
            {
            }

            public MyList(List<T> list)
            {
                this.Clear();
                this.AddRange(list);
            }

            public override string ToString()
            {
                string s = "";
                foreach (T element in this)
                    s += $"{element}, ";
                return s;
            }
        }
    }

}
