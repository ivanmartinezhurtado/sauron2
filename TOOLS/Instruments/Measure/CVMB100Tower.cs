﻿using Comunications.Utility;
using Dezac.Device;
using log4net;
using System;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace Instruments.Measure
{
    [DeviceVersion(1.04)]
    public class CVMB100Tower :DeviceBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("CVMB100Tower");

        public CVMB100Tower()
        {

        }

        public CVMB100Tower(string port, byte periferico)
        {
            SetPort(port, periferico);
        }


        public CVMB100Tower(SerialPort serialPort)
        {
            serialPort.BaudRate = 19200;
            Modbus = new ModbusDeviceSerialPort(serialPort, logger);
        }

        public void SetPort(string port, byte periferico, int baudRate = 19200)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
            {
                if (Modbus == null)
                    Modbus = new ModbusDeviceSerialPort(result, baudRate, Parity.None, StopBits.One, logger);

                Modbus.PortCom = result;
                Modbus.PerifericNumber = periferico;
            }
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            if (Modbus != null)
            {
                Modbus.Dispose();
                Modbus = null;
            }
        }

        public VariablesInstantaneas ReadVariablesInstantaneas()
        {
            return Modbus.Read<VariablesInstantaneas>();
        }

        public VariablesTrifasicas ReadVariablesTrifasicas()
        {
            return Modbus.Read<VariablesTrifasicas>();
        }

        public VariablesAngulos ReadAngulos()
        {
            return Modbus.Read<VariablesAngulos>();
        }

        #region Armonicos 

        public Int32 ReadArmonicoFundamental_VL1()
        {
            return Modbus.ReadInt32((ushort)Registers.ARMONICOS_V_L1);
        }

        public Int32 ReadArmonicoFundamental_IL1()
        {
            return Modbus.ReadInt32((ushort)Registers.ARMONICOS_I_L1);
        }

        public Armonicos2_25 ReadArmonicos2_25_VL1()
        {
            return Modbus.Read<Armonicos2_25>((ushort)Registers.ARMONICOS_2_V_L1);
        }

        public Armonicos2_25 ReadArmonicos2_25_IL1()
        {
            return Modbus.Read<Armonicos2_25>((ushort)Registers.ARMONICOS_2_I_L1);
        }

        public Armonicos26_50 ReadArmonicos26_50_VL1()
        {
            return Modbus.Read<Armonicos26_50>((ushort)Registers.ARMONICOS_26_V_L1);
        }

        public Armonicos26_50 ReadArmonicos26_50_IL1()
        {
            return Modbus.Read<Armonicos26_50>((ushort)Registers.ARMONICOS_26_I_L1);
        }

        //*************** L2 *****************************

        public Int32 ReadArmonicoFundamental_VL2()
        {
            return Modbus.ReadInt32((ushort)Registers.ARMONICOS_V_L2);
        }

        public Int32 ReadArmonicoFundamental_IL2()
        {
            return Modbus.ReadInt32((ushort)Registers.ARMONICOS_I_L2);
        }

        public Armonicos2_25 ReadArmonicos2_25_VL2()
        {
            return Modbus.Read<Armonicos2_25>((ushort)Registers.ARMONICOS_2_V_L2);
        }

        public Armonicos2_25 ReadArmonicos2_25_IL2()
        {
            return Modbus.Read<Armonicos2_25>((ushort)Registers.ARMONICOS_2_I_L2);
        }

        public Armonicos26_50 ReadArmonicos26_50_VL2()
        {
            return Modbus.Read<Armonicos26_50>((ushort)Registers.ARMONICOS_26_V_L2);
        }

        public Armonicos26_50 ReadArmonicos26_50_IL2()
        {
            return Modbus.Read<Armonicos26_50>((ushort)Registers.ARMONICOS_26_I_L2);
        }


        //*************** L3 *****************************

        public Int32 ReadArmonicoFundamental_VL3()
        {
            return Modbus.ReadInt32((ushort)Registers.ARMONICOS_V_L3);
        }

        public Int32 ReadArmonicoFundamental_IL3()
        {
            return Modbus.ReadInt32((ushort)Registers.ARMONICOS_I_L3);
        }

        public Armonicos2_25 ReadArmonicos2_25_VL3()
        {
            return Modbus.Read<Armonicos2_25>((ushort)Registers.ARMONICOS_2_V_L3);
        }

        public Armonicos2_25 ReadArmonicos2_25_IL3()
        {
            return Modbus.Read<Armonicos2_25>((ushort)Registers.ARMONICOS_2_I_L2);
        }

        public Armonicos26_50 ReadArmonicos26_50_VL3()
        {
            return Modbus.Read<Armonicos26_50>((ushort)Registers.ARMONICOS_26_V_L3);
        }

        public Armonicos26_50 ReadArmonicos26_50_IL3()
        {
            return Modbus.Read<Armonicos26_50>((ushort)Registers.ARMONICOS_26_I_L3);
        }

        #endregion

        public void FlagTest(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void Reset()
        {
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        #region Structures And Enums
        
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Fases;
            public VariablesTrifasicas Trifasicas;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VariablesFase
        {
            private Int32 tension;
            private Int32 corriente;
            private Int32 potenciaActiva;
            private Int32 potenciaReactivaInductiva;
            private Int32 potenciaReactivaCapicitiva;
            private Int32 potenciaAparente;
            private Int32 factorPotencia;
            private Int32 cosenoPhi;

            public double Tension { get { return Convert.ToDouble(tension) / 100; } }
            public double Corriente { get { return Convert.ToDouble(corriente) / 1000; } }
            public double PotenciaActiva { get { return Convert.ToDouble(potenciaActiva) ; } }
            public double PotenciaReactivaInductiva { get { return Convert.ToDouble(potenciaReactivaInductiva) ; } }
            public double PotenciaReactivaCapicitiva { get { return Convert.ToDouble(potenciaReactivaCapicitiva) ; } }
            public double PotenciaAparente { get { return Convert.ToDouble(potenciaAparente) ; } }
            public double FactorPotencia { get { return Convert.ToDouble(factorPotencia) / 100; } }
            public double CosenoPhi { get { return Convert.ToDouble(cosenoPhi) / 100; } }

            public double Angle { get { return Math.Acos(CosenoPhi) * (180 / Math.PI); } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0030)]
        public struct VariablesTrifasicas
        {
            private Int32 tensionNeutro;
            private Int32 corrienteNeutro;
            private Int32 frecuencia;
            private Int32 tensionLineaL1L2;
            private Int32 tensionLineaL2L3;
            private Int32 tensionLineaL3L1;
            private Int32 tensionLineaIII;
            private Int32 tensionFaseIII;
            private Int32 corrienteIII;
            private Int32 potenciaActivaIII;
            private Int32 potenciaInductivaIII;
            private Int32 potenciaCapacitivaIII;
            private Int32 potenciaAparenteIII;
            private Int32 facttorPotenciaIII;
            private Int32 cosenoPhiIII;

            public double TensionNeutro { get { return Convert.ToDouble(tensionNeutro) / 100; } }
            public double CorrienteNeutro { get { return Convert.ToDouble(corrienteNeutro/ 1000); } }
            public double Frecuencia { get { return Convert.ToDouble(frecuencia) / 100; } }
            public double TensionLineaL1L2 { get { return Convert.ToDouble(tensionLineaL1L2) / 100; } }
            public double TensionLineaL2L3 { get { return Convert.ToDouble(tensionLineaL2L3) / 100; } }
            public double TensionLineaL3L1 { get { return Convert.ToDouble(tensionLineaL3L1) / 100; } }
            public double TensionLineaIII { get { return Convert.ToDouble(tensionLineaIII) / 100; } }
            public double TensionFaseIII { get { return Convert.ToDouble(tensionFaseIII) / 100; } }
            public double CorrienteIII { get { return Convert.ToDouble(corrienteIII) / 1000; } }
            public double PotenciaActivaIII { get { return Convert.ToDouble(potenciaActivaIII) ; } }
            public double PotenciaInductivaIII { get { return Convert.ToDouble(potenciaInductivaIII) ; } }
            public double PotenciaCapacitivaIII { get { return Convert.ToDouble(potenciaCapacitivaIII) ; } }
            public double PotenciaAparenteIII { get { return Convert.ToDouble(potenciaAparenteIII) ; } }
            public double FacttorPotenciaIII { get { return Convert.ToDouble(facttorPotenciaIII) / 1000; } }
            public double CosenoPhiIII { get { return Convert.ToDouble(cosenoPhiIII) / 1000; } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x1770)]
        public struct VariablesAngulos
        {
            private Int32 anguloV1V2;
            private Int32 anguloV2V3;
            private Int32 anguloV3V1;
            private Int32 anguloV1I1;
            private Int32 anguloV2I2;
            private Int32 anguloV3I3;

            public double AnguloV1V2 { get { return Convert.ToDouble(anguloV1V2 / 1000D); } }
            public double AnguloV2V3 { get { return Convert.ToDouble(anguloV2V3 / 1000D); } }
            public double AnguloV3V1 { get { return Convert.ToDouble(anguloV3V1 / 1000D); } }
            public double AnguloV1I1 { get { return Convert.ToDouble(anguloV1I1 / 1000D); } }
            public double AnguloV2I2 { get { return Convert.ToDouble(anguloV2I2 / 1000D); } }
            public double AnguloV3I3 { get { return Convert.ToDouble(anguloV3I3 / 1000D); } }
        };


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Armonicos2_25
        {
            public Int16 Armonico2;
            public Int16 Armonico3;
            public Int16 Armonico4;
            public Int16 Armonico5;
            public Int16 Armonico6;
            public Int16 Armonico7;
            public Int16 Armonico8;
            public Int16 Armonico9;
            public Int16 Armonico10;
            public Int16 Armonico11;
            public Int16 Armonico12;
            public Int16 Armonico13;
            public Int16 Armonico14;
            public Int16 Armonico15;
            public Int16 Armonico16;
            public Int16 Armonico17;
            public Int16 Armonico18;
            public Int16 Armonico19;
            public Int16 Armonico20;
            public Int16 Armonico21;
            public Int16 Armonico22;
            public Int16 Armonico23;
            public Int16 Armonico24;
            public Int16 Armonico25;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Armonicos26_50
        {
            public Int16 Armonico26;
            public Int16 Armonico27;
            public Int16 Armonico28;
            public Int16 Armonico29;
            public Int16 Armonico30;
            public Int16 Armonico31;
            public Int16 Armonico32;
            public Int16 Armonico33;
            public Int16 Armonico34;
            public Int16 Armonico35;
            public Int16 Armonico36;
            public Int16 Armonico37;
            public Int16 Armonico38;
            public Int16 Armonico39;
            public Int16 Armonico40;
            public Int16 Armonico41;
            public Int16 Armonico42;
            public Int16 Armonico43;
            public Int16 Armonico44;
            public Int16 Armonico45;
            public Int16 Armonico46;
            public Int16 Armonico47;
            public Int16 Armonico48;
            public Int16 Armonico49;
            public Int16 Armonico50;
        }

        public enum Registers
        {
            RESET = 0x07D0,
            FLAG_TEST = 0x2AF8,

            ARMONICOS_V = 0x125C,
            ARMONICOS_I = 0x132E,
            ARMONICOS_V_L1 = 0x125C,
            ARMONICOS_2_V_L1 = 0x125E,
            ARMONICOS_26_V_L1 = 0x1276,

            ARMONICOS_I_L1 = 0x132E,
            ARMONICOS_2_I_L1 = 0x1330,
            ARMONICOS_26_I_L1 = 0x1348,

            ARMONICOS_V_L2 = 0x1290,
            ARMONICOS_2_V_L2 = 0x1292,
            ARMONICOS_26_V_L2 = 0x12AA,

            ARMONICOS_I_L2 = 0x1362,
            ARMONICOS_2_I_L2 = 0x1364,
            ARMONICOS_26_I_L2 = 0x137C,

            ARMONICOS_V_L3 = 0x12C4,
            ARMONICOS_2_V_L3 = 0x12C6,
            ARMONICOS_26_V_L3 = 0x12DE,

            ARMONICOS_I_L3 = 0x1396,
            ARMONICOS_2_I_L3 = 0x1398,
            ARMONICOS_26_I_L3 = 0x13B0,
        }

        #endregion
    }
}
