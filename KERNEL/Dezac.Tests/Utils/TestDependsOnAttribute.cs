﻿using System;

namespace Dezac.Tests
{
    public class TestDependsOnAttribute : Attribute
    {
        public Type Type { get; set; }

        public TestDependsOnAttribute(Type type)
        {
            Type = type;
        }
    }
}
