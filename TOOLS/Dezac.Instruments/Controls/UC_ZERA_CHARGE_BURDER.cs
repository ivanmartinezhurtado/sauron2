﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Auxiliars;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(ZERA_CHARGE_BURDERDescription))]
    public partial class UC_ZERA_CHARGE_BURDER : UserControl, IDisposable
    {
        private static readonly string TAG = "ZERA_CHARGE_BENCH";

        public int numGraphs = 0;

        private SynchronizationContext sc;

        private ZERA_CHARGE_BENCH zera;

        public Type InstrumentType
        {
            get
            {
                return typeof(ZERA_CHARGE_BENCH);
            }
        }

        public ZERA_CHARGE_BENCH Instrument
        {
            get
            {
                if (zera == null)
                    zera = new ZERA_CHARGE_BENCH();

                return zera;
            }
            set
            {
                if (zera != null)
                {
                    zera.Dispose();
                    zera = null;
                }

                zera = value;
            }
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }
        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }
        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        public UC_ZERA_CHARGE_BURDER()
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            numGraphs = 0;

            if (disposing && (components != null))
                components.Dispose();

            if (Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Instrument.SetPort(cmb_Port.SelectedText);
        }

        //Seleccionar y encender la carga
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    Instrument.SelectBurden(ZERA_CHARGE_BENCH);
        //   // Instrument.SwitchBurden(true);
        //}

        //private void button5_Click(object sender, EventArgs e)
        //{
        //    Instrument.SelectBurden(ZERA_CHARGE_BENCH.Burden.IEC60);
        //   // Instrument.SwitchBurden(true);
        //}
       
        //private void button6_Click(object sender, EventArgs e)
        //{
        //    Instrument.SelectBurden(ZERA_CHARGE_BENCH.Burden.ANSI);
        //  //  Instrument.SwitchBurden(true);
        //}

        /*//Introducir la carga y el factor de potencia 
         
        private void txtI1_TextChanged(object sender, EventArgs e)
        {
            Instrument.SetVa(ZERA_CHARGE_BENCH.Burden.IEC50);

        }
        private void txtI2_TextChanged(object sender, EventArgs e)
        {
            Instrument.SetVa(ZERA_CHARGE_BENCH.Burden.IEC60);
        }
        private void txtI3_TextChanged(object sender, EventArgs e)
        {
            Instrument.SetVa(ZERA_CHARGE_BENCH.Burden.ANSI);
        }
        */

        //Lectura de VA y factor de potencia 
        private void btnRead_Click(object sender, EventArgs e)
        {

        }

        /*private void txtI1_TextChanged(object sender, EventArgs e)
        {
            Instrument.ReadVa(ZERA_CHARGE_BENCH.Burden.IEC50);
            
        }
        private void txtI2_TextChanged(object sender, EventArgs e)
        {
            Instrument.ReadVa(ZERA_CHARGE_BENCH.Burden.IEC60);
        }
        private void txtI3_TextChanged(object sender, EventArgs e)
        {
            Instrument.ReadVa(ZERA_CHARGE_BENCH.Burden.ANSI);
        }
        */



        //RESTORE DEFAULT SETTINGS = RESET

        private void btnReset_Click(object sender, EventArgs e)
        {

            try
            {
                ToggleButtonsState((Button)sender);

                Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));

                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.RestoreDefaultSettings(); }));

                LifeMethod("Reseteando", task);
            }
            catch (Exception ex)
            {
                Logger.Error("Error al resetear ZERA_CHARGE_BURDER", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        public class ZERA_CHARGE_BURDERDescription : ControlDescription
        {
            public override string Name { get { return "ZERA_CHARGE_BURDER"; } }
            public override string Category { get { return "Instruments"; } }
            public override Image Icon { get { return Properties.Resources.Flash_Updater_icon; } }
            public override Image IconBig { get { return Properties.Resources.ZERA_MT; } }
            public override Type SubcomponentType { get { return typeof(ZERA_CHARGE_BENCH); } }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
