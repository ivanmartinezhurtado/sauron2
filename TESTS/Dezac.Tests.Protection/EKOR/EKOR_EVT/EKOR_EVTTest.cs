﻿using Dezac.Tests.Actions.InstrumentsTools;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using TaskRunner.Model;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.04)]
    public class EKOR_EVTTest : TestBase
    {
        private const byte ko1 = 0;
        private const byte ok1 = 1;
        private const byte ko2 = 2;
        private const byte ok2 = 3;
        private const byte ko3 = 4;
        private const byte ok3 = 5;
        private const byte ko4 = 6;
        private const byte ok4 = 7;

        //USB4761 usb;
        Tower3 tower;

        public DeviceInstance Sensors { get; set; }

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER3", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            if (SequenceContext.Current.RunningInstances > 1)
            {
                Sensors = MultipleInitialization();
                WaitAllTestsAndRunOnlyOnce("_INITIALIZATION_", InternalInitialization);
            }
            else
                InternalInitialization();
        }

        private void InternalInitialization()
        {
            tower.UsbIO.DO.OffRange(0, 7);
        }

        private DeviceInstance MultipleInitialization()
        {
            var sensor = new DeviceInstance();

            if(NumInstance == 1)
            {
                sensor.ledOk = ok1;
                sensor.ledError = ko1;
                sensor.NameStepIR = "STEP_1";
                sensor.NameStepDC = "STEP_2";
            }

            if (NumInstance == 2)
            {
                sensor.ledOk = ok2;
                sensor.ledError = ko2;
                sensor.NameStepIR = "STEP_3";
                sensor.NameStepDC = "STEP_4";
            }

            if (NumInstance == 3)
            {
                sensor.ledOk = ok3;
                sensor.ledError = ko3;
                sensor.NameStepIR = "STEP_5";
                sensor.NameStepDC = "STEP_6";
            }

            if (NumInstance == 4)
            {
                sensor.ledOk = ok4;
                sensor.ledError = ko4;
                sensor.NameStepIR = "STEP_7";
                sensor.NameStepDC = "STEP_8";
            }

            return sensor;
        }

        public void TestFinish()
        {
            if (SequenceContext.Current.RunningInstances > 1)
            {
                MultipleTestFinish();
                RunOnlyLastTest("_FINISH_", () =>
                {
                    if (tower != null)
                        tower.Dispose();
                });
            }
            else
                InternalTestFinish();           
        }

        private void InternalTestFinish()
        {
            var result = string.Empty;
           
            result = Resultado.GetString("RESULT", "", ParamUnidad.SinUnidad);       

            if(result == "PASS")
            {
                tower.UsbIO.DO.On(ok1);
                tower.UsbIO.DO.On(ok2);
                tower.UsbIO.DO.On(ok3);
                tower.UsbIO.DO.On(ok4);
            }            
            else
            {
                if(result == "FAIL_STEP_1" || result == "FAIL_STEP_2")
                {
                    tower.UsbIO.DO.On(ko1);
                    tower.UsbIO.DO.On(ko2);
                    tower.UsbIO.DO.On(ok2);
                    tower.UsbIO.DO.On(ko3);
                    tower.UsbIO.DO.On(ok3);
                    tower.UsbIO.DO.On(ko4);
                    tower.UsbIO.DO.On(ok4);

                    throw new Exception("HA FALLADO LA PLACA 1");
                }
                if (result == "FAIL_STEP_3" || result == "FAIL_STEP_4")
                {
                    tower.UsbIO.DO.On(ok1);

                    tower.UsbIO.DO.On(ko2);

                    tower.UsbIO.DO.On(ko3);
                    tower.UsbIO.DO.On(ok3);
                    tower.UsbIO.DO.On(ko4);
                    tower.UsbIO.DO.On(ok4);

                    throw new Exception("HA FALLADO LA PLACA 2");
                }
                if (result == "FAIL_STEP_5" || result == "FAIL_STEP_6")
                {
                    tower.UsbIO.DO.On(ok1);
                    tower.UsbIO.DO.On(ok2);

                    tower.UsbIO.DO.On(ko3);

                    tower.UsbIO.DO.On(ko4);
                    tower.UsbIO.DO.On(ok4);

                    throw new Exception("HA FALLADO LA PLACA 3");
                }
                if(result == "FAIL_STEP_7" || result == "FAIL_STEP_8")
                {
                    tower.UsbIO.DO.On(ok1);
                    tower.UsbIO.DO.On(ok2);
                    tower.UsbIO.DO.On(ok3);

                    tower.UsbIO.DO.On(ko4);

                    throw new Exception("HA FALLADO LA PLACA 4");
                }


                if (tower != null)
                    tower.Dispose();
            }
        }

        private void MultipleTestFinish()
        {
            var context = SequenceContext.Current;
            var result = context.SharedVariables.Get<string>("RESULT");
            var vars = context.SharedVariables;

            foreach (var sharedVars in vars.VariableList)
                if (sharedVars.Key.Contains(Sensors.NameStepIR))
                    Resultado.Set(sharedVars.Key.Replace("3", "1").Replace("4", "2").Replace("5", "1").Replace("6", "2").Replace("7", "1").Replace("8", "2"), (double)sharedVars.Value, ParamUnidad.SinUnidad, GetSharedVariable<double>("RESISTANCE_MIN"), GetSharedVariable<double>("RESISTANCE_MAX"));
                else if (sharedVars.Key.Contains(Sensors.NameStepDC))
                    Resultado.Set(sharedVars.Key.Replace("3", "1").Replace("4", "2").Replace("5", "1").Replace("6", "2").Replace("7", "1").Replace("8", "2"), (double)sharedVars.Value, ParamUnidad.SinUnidad, GetSharedVariable<double>("CURRENT_MIN"), GetSharedVariable<double>("CURRENT_MAX"));

            Resultado.Set("RESULT", result.ToString(), ParamUnidad.SinUnidad);

            if (result == "PASS")
                tower.UsbIO.DO.On(Sensors.ledOk);
            else
            {
                if (result.Contains(Sensors.NameStepIR))
                {
                    tower.UsbIO.DO.On(Sensors.ledError);
                    Error().UUT.MEDIDA.MARGENES("IR").Throw();
                }
                else if (result.Contains(Sensors.NameStepDC))
                {
                    tower.UsbIO.DO.On(Sensors.ledError);
                    Error().UUT.MEDIDA.MARGENES("DC").Throw();
                }
                else if (Convert.ToInt32(result.Replace("FAIL_STEP_", "")) < Convert.ToInt32(Sensors.NameStepDC.Replace("STEP_", "")))
                {
                    tower.UsbIO.DO.On(Sensors.ledOk);
                    tower.UsbIO.DO.On(Sensors.ledError);
                    Error().PROCESO.ACTION_EXECUTE.TEST_CANCELADO("NO SE HA TESTEADO LA PLACA").Throw();
                }
                else
                {
                    tower.UsbIO.DO.On(Sensors.ledOk);
                    Resultado.Set("RESULT", "PASS", ParamUnidad.SinUnidad);
                }
            }
        }

        ///////// TEST SENSOR RESISTIVO 36KV /////////

        public void TestConfigurationSteps()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                WaitAllTestsAndRunOnlyOnce("_CONFIG_", InternalConfigSteps);
            else
                InternalConfigSteps();
        }

        public void ExecuteTest()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                try
                {
                    WaitAllTestsAndRunOnlyOnce("_EXECUTE_", InternalExecuteTest);
                }
                catch (Exception) { }
            else
                InternalExecuteTest();           
        }

        private void InternalConfigSteps()
        {
            var clearStepsAction = new ClearDielectricStrengthSetPoints();
            clearStepsAction.Execute();

            ConfigStep(true, "1");
            ConfigStep(true, "1");

            ConfigStep(true, "2");
            ConfigStep(true, "2");

            ConfigStep(true, "3");
            ConfigStep(true, "3");

            ConfigStep(true, "4");
            ConfigStep(true, "4");
        }

        private void ConfigStep(bool dataFromBBDD, string Channel)
        {
            var configStepsAction = new DielectricStrengthSetSteps();
            configStepsAction.BBDD = dataFromBBDD;
            configStepsAction.ChannelsHigh = Channel;
            configStepsAction.ChannelsLow = "0";
            configStepsAction.LOW = 0;
            configStepsAction.MaximumCurrent = 1;
            configStepsAction.Mode = "AC";
            configStepsAction.MinimumResistance = 0;
            configStepsAction.MaximumResistance = 1;
            configStepsAction.RAMP = 0;
            configStepsAction.Time = 1;

            configStepsAction.Execute();
        }

        private void InternalExecuteTest()
        {
            var executeAction = new ExecuteTestDielectricStrength();
            executeAction.ExpectedResult = Instruments.Auxiliars.DielectricStrengthTester.TestResult.PASS;
            executeAction.TimeOut = 20;
            executeAction.resultName = "RESULT_";

            executeAction.Execute();
        }


        public class DeviceInstance
        {
            public byte ledOk { get; set; }
            public byte ledError { get; set; }
            public string NameStepIR { get; set; }
            public string NameStepDC { get; set; }
        }
    }
}
