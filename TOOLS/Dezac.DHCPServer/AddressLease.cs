using System;

namespace WinDHCP.Library
{
    public class AddressLease
    {
        public string HostName { get; set; }

        public PhysicalAddress Owner { get; set; }

        public InternetAddress Address { get; set; }

        public DateTime Expiration { get; set; }

        public Int32 SessionId { get; set; }

        public Boolean Acknowledged { get; set; }

        public AddressLease(PhysicalAddress owner, InternetAddress address, string hostName = "")
            : this(owner, address, DateTime.Now.AddDays(1), hostName)
        {
        }

        public AddressLease(PhysicalAddress owner, InternetAddress address, DateTime expiration, string hostName = "")
        {
            Owner = owner;
            Address = address;
            Expiration = expiration;
            HostName = hostName;
        }
    }
}
