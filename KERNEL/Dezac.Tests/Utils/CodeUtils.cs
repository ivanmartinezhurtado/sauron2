﻿using System;

namespace Dezac.Tests.Utils
{
    public static class CodeUtils
    {
        public static void Safe(Action action, string errorMessage)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                throw new Exception(errorMessage, ex);
            }
        }
    }
}
