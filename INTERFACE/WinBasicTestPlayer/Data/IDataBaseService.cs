﻿using log4net;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace WinTestPlayer.Data
{
    public interface ITestModelDataBaseService: IDisposable
    {
        int GetNumEquiposOK(string lote);
        int GetNumEquiposNG(string lote);
    }

    public class DataBaseService
    {
        protected static readonly ILog logger = LogManager.GetLogger("DataBaseService");

        public enum DatabaseTypeEnum
        {
            SQLServer,
            MySql,
            SqLite,
            Oracle,
            Oledb,
            Csv
        }

        public DatabaseTypeEnum DatabaseType { get; set; }

        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DataBaseService"]?.ConnectionString;
            }
        }

        public object ExecuteScalar(string command)
        {
            DbProviderFactory db = DbProviderFactories.GetFactory(GetProviderName());
            using (var cn = db.CreateConnection())
            {
                cn.ConnectionString = ConnectionString;
                cn.Open();

                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = command;
                    var result = cmd.ExecuteScalar();
                    logger.InfoFormat("result: {0}", result);
                    return result;
                }
            }
        }

        public DataTable ExecuteReader(string command)
        {
            DbProviderFactory db = DbProviderFactories.GetFactory(GetProviderName());

            using (var cn = db.CreateConnection())
            {
                cn.ConnectionString = ConnectionString;
                cn.Open();

                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = command;
                    var dataReader = cmd.ExecuteReader();
                    logger.InfoFormat("dataReader: {0}", dataReader);
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);
                    return dataTable;
                }
            }
        }

        public void ExecuteNonQuery(string command)
        {
            DbProviderFactory db = DbProviderFactories.GetFactory(GetProviderName());
            using (var cn = db.CreateConnection())
            {
                cn.ConnectionString = ConnectionString;
                cn.Open();

                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = command;
                    var result = cmd.ExecuteNonQuery();
                    logger.InfoFormat("Affected rows: {0}", result);
                }
            }
        }

        private string GetProviderName()
        {
            string providerName;

            switch (DatabaseType)
            {
                case DatabaseTypeEnum.Oracle:
                    providerName = "System.Data.OracleClient";
                    break;
                case DatabaseTypeEnum.MySql:
                    providerName = "System.Data.MySql";
                    break;
                case DatabaseTypeEnum.SqLite:
                    providerName = "System.Data.SqLite";
                    break;
                case DatabaseTypeEnum.Oledb:
                    providerName = "System.Data.OleDb";
                    break;
                default:
                    providerName = "System.Data.SqlClient";
                    break;
            }

            return providerName;
        }   

    }
}
