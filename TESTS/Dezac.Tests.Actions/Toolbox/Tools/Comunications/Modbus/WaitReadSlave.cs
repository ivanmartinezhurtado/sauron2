﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Tests.Model;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WaitReadSlaveDescription))]
    public class WaitReadSlave : ActionBase
    {
        [Description("Nombre de la instancia del objecto creado")]
        [Category("Instancia")]
        [DefaultValue("ModbusDeviceSlave")]
        public string DeviceName { get; set; }

        [Description("Nombre de la variable donde guardar el valor")]
        [Category("Resultado")]
        public string OutputVarName { get; set; }

        [Category("Resultados")]
        [Description("Unidades con las que trabaja el valor a obtener")]
        public ParamUnidad OutputUnidades { get; set; }

        [DefaultValue(500)]
        [Description("Tiempo de espera antes de realizar otra comunicación")]
        public int TimeInterval { get; set; }

        [DefaultValue(0)]
        [Description("Tiempo de espera antes de realizar la primera comunicación")]
        public int InitialDelay { get; set; }

        [DefaultValue(1)]
        [Description("numero de respeticiones de lectura")]
        public int RetryReads { get; set; }

        [DefaultValue(false)]
        [Description("si se habilita la cancelación si se encuenrta una excepción")]
        public bool CancelOnException { get; set; }


        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDeviceSerialPort;

            if (!uut.IsOpen)
                uut.OpenPort();

            var nbytesOld = 0;
            var sampler = Sampler.Run(
             () => { return new SamplerConfig { Interval = TimeInterval, InitialDelay = 0, NumIterations = RetryReads, CancelOnException = CancelOnException, ThrowException = false, CancellationToken = SequenceContext.Current.CancellationToken }; },
             (step) =>
             {
                 if (uut.BytesToRead > 5)
                 {
                     var nbytes = uut.BytesToRead;
                     if (nbytes == nbytesOld)
                     {
                         var resp = uut.ReadRequest();
                     }
                     else
                         nbytesOld = nbytes;
                 }
             });

            if (uut.IsOpen)
                uut.ClosePort();
        }
    }

    public class WaitReadSlaveDescription : ActionDescription
    {
        public override string Name { get { return "Wait Read Slave"; } }
        public override string Description { get { return "Espera la lectura de una trama Modbus"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitModbusDeviceAction;DisposedModbusDeviceAction:2"; } }
    }
}
