﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using WinDHCP.Library;

namespace Instruments.Router
{
    public class RouterVirtual : Router
    {
        private DhcpServer serverDhcp = null;
        public RouterVirtual()
            : base()
        {
            serverDhcp = new DhcpServer();
            var config = ReadConfig();
            serverDhcp.ConfigIP(config.LanIP, "255.255.255.0", config.LanGatewayIP);
        }

        public override RouterConfig ReadConfig()
        {
            var config = new RouterConfig();
            var card = NetUtils.GetNetworkInterfaceTest();

            if (card == null)
                for (var i = 0; i <= 2; i++)
                {
                    System.Threading.Thread.Sleep(1000);
                    card = NetUtils.GetNetworkInterfaceTest();
                    if (card != null)
                        break;
                }

            if (card == null)
                TestException.Create().SOFTWARE.DRIVER.CONFIGURACION_INCORRECTA("no encuentra un NetworkInterface").Throw();

            config.Domain = card.Name;
            config.EnableDHCP = false;
            config.HostName = Environment.MachineName;
            config.LanGatewayIP = NetUtils.GetMyIP();
            config.LanIP = NetUtils.GetMyIP();
            config.LanMaskIP = "255.255.255.0";
            RouterIP = config.LanIP;
            return config;
        }

        public override RouterConfig ApplyConfig(RouterConfig config)
        {
            serverDhcp.Stop();
            var gateway = config.LanGatewayIP == null ? config.LanIP : config.LanGatewayIP;
            NetUtils.ConfigIP(config.LanIP , gateway, config.LanMaskIP);
            serverDhcp.ConfigIP(config.LanIP, config.LanMaskIP, gateway);
            var Readconfig = ReadConfig();
            serverDhcp.Start();
            return Readconfig;
        }

      
        public override List<ClientEntry> GetConnectedClientsFromWeb(bool excludeMe, bool PRS600 = true)
        {
            var list = new List<ClientEntry>();
            var clients = serverDhcp.ClientsActive;
            foreach (var cli in clients)
                list.Add(new ClientEntry()
                { 
                    MAC = cli.Value.Owner.MAC, 
                    Name = cli.Value.HostName, 
                    IP = cli.Value.Address.ToString(), 
                    Expire = cli.Value.Expiration.ToShortDateString() 
                });

            if (excludeMe)
                list = list.Where(p => !p.Name.Contains("PC-")).ToList();

            if (PRS600)
                list = list.Where(p => !p.Name.Contains("PRS600")).ToList();

            return list;
        }

        public override void DeleteDHCPEntry(string ip, string mac)
        {

        }

        public override RouterConfigWifi ReadConfigWifi()
        {
            throw new System.NotImplementedException();
        }

        public override RouterConfigWifi ApplyConfigWifi(RouterConfigWifi config)
        {
            throw new System.NotImplementedException();
        }

        public override void Disposed()
        {
            if (serverDhcp != null)
            {
                serverDhcp.Stop();
                serverDhcp = null;
            }

            instance = null;    
        }
    }
}
