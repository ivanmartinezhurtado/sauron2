﻿using TaskRunner;

namespace WinTestPlayer.Services
{
    public class RunnerService
    {
        public SequenceRunner Runner { get; private set; }

        public RunnerService()
        {
            Runner = new SequenceRunner();
        }

        //public void Play(SequenceDocument doc)
        //{
        //    Runner.Play(doc.Model);
        //}
    }
}
