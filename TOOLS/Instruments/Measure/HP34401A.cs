﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using Ivi.Visa.Interop;
using System;
using System.Collections.Generic;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.00)]
    public class HP34401A : MeasureBase<HP34401A>, IDisposable, IConsoleDevice
    {
        public enum PrivateCommands
        {
            READ_BUSY = Commands.USER_DEFINED_COMMANDS + 1
        };

        private FormattedIO488 _Gpib;
        private ResourceManager mgr;
        private byte _adrr = 3;
        private bool _IsOpen = false;
        private string idn;

        public HP34401A()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinRx = "\n",  CaracterFinTx = "\n" };
            transport = new MessageTransport(protocol, new GpibPortAdapter(_Gpib), _logger);
            mgr = new ResourceManager();
        }

        public byte PortAdress
        {
            get { return _adrr; }
            set
            {
                Close();
                _adrr = value;
                Open();
            }
        }

        private void InitializeComponent()
        {
            if (_Gpib == null)
                _Gpib = new FormattedIO488();
        }

        private void Close()
        {
            if ((_IsOpen == true) && (_Gpib.IO != null)) _Gpib.IO.Close();
            _IsOpen = false;
        }

        private void Open()
        {
            if ((_IsOpen == true) && (_Gpib.IO != null)) _Gpib.IO.Close();
            _Gpib.IO = (IMessage)mgr.Open("GPIB0::" + _adrr.ToString(), AccessMode.NO_LOCK, 2000, "");
            _Gpib.IO.TerminationCharacterEnabled = true;
            _IsOpen = true;
            //_Gpib.IO.TerminationCharacter = 
        }

        private bool IsOpen()
        {
            return _Gpib != null && _Gpib.IO != null;
        }

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));
            return idn;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            Result<string> response;

            if (!IsOpen())
                Open();

            switch (command.Commands)
            {
                case (int)PrivateCommands.READ_BUSY:
                    response =WriteCommand("*OPC");
                    Status.IsBusy = response.Value != "1" && response.Value != "ON";
                    break;

                case (int)Commands.READ_IDN:
                    response = WriteCommand("*IDN?");
                    var resp = response.Value.Split(',');
                    idn = string.Format("{0}, MODEL={1}, SN={2}", resp[0], resp[1], resp[2]);
                    break;

                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            Close();
        }

        public Result<string> WriteCommand(string text)
        {
            try
            {
                if (!IsOpen())
                    Open();

                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                response = transport.SendMessage<SCPIMessage>(new SCPIMessage { Text = "*ESR?" });
                int result = Convert.ToInt32(response.Text);
                if (result > 0)
                    return ReadError();

                return string.Empty;
            }
            catch (Exception)
            {
                return ReadError();
            }
        }

        private Result<string> ReadError()
        {
            try
            {
                var message = new StringMessage { Text = "SYSTem:ERRor?" };
                string text = string.Empty;
                int code;

                do
                {
                    var response = transport.SendMessage<StringMessage>(message);
                    string result = response.Text;
                    code = Convert.ToInt32(result.Substring(0, result.IndexOf(',')));
                    if (code < 0 || text == string.Empty)
                        text += result + Environment.NewLine;
                } while (code < 0);

                return new Result<string>(text);
            }
            catch (Exception ex)
            {
                return new Result<string>(ex);
            }
        }

        public HP34401ASCPI SCPI { get { return new HP34401ASCPI(this); } }
    }

    public class HP34401AException : InstrumentException
    {
        public enum Codes
        {
            INVALID_CHARACTER = -101,
            SYNTAX_ERROR = -102,
            INVALID_SEPARATOR = -103,
            DATATYPE_ERROR = -104,
            GET_NOT_ALLOWED = -105,
            PARAMETER_NOT_ALLOWED = -106,
            MISSING_PARAMETER = -107
            // TODO: Continue -> pág.: .167
        };

        private static readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Codes.INVALID_CHARACTER, "Carácter inválido en la cadena de comando!" },
            { (int)Codes.SYNTAX_ERROR, "Sintaxis inválida en la cadena de comando!" }
        };

        public HP34401AException(Codes code)
            : base((int)code)
        {
        }

        public string DecodedMessage
        {
            get
            {
                string message = null;

                messages.TryGetValue(this.Code, out message);

                return message ?? "Error desconocido!";
            }
        }
    }
}
