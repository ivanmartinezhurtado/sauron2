namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.RF_ANOMALIA")]
    public partial class RF_ANOMALIA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RF_ANOMALIA()
        {
            RF_ACTUACION = new HashSet<RF_ACTUACION>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMANOMALIA { get; set; }

        public int NUMREPROCESO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDANOMALIA { get; set; }

        [StringLength(500)]
        public string NOTAS { get; set; }

        public virtual ANOMALIAREPFAB ANOMALIAREPFAB { get; set; }

        public virtual REPROCESOFAB REPROCESOFAB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RF_ACTUACION> RF_ACTUACION { get; set; }
    }
}
