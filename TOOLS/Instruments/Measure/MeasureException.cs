﻿using Instruments.Utility;
using System.Collections.Generic;

namespace Instruments.Measure
{
    public class MeasureException : InstrumentException
    {
        public enum Codes
        {
            BUSY,
            INTERNAL_ERROR,
            OPERATION_NO_SUCCES
        };

        private static readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Codes.BUSY, "El equipo está ocupado!" },
            { (int)Codes.INTERNAL_ERROR, "Error interno del dispositivo!" },
              { (int)Codes.OPERATION_NO_SUCCES, "Error no se ha terminado la operacion solicitada correctamente!" }
        };

        public MeasureException(Codes code)
            : base((int)code)
        {
        }

        public string DecodedMessage
        {
            get
            {
                string message = null;

                messages.TryGetValue(this.Code, out message);

                return message ?? "Error desconocido!";
            }
        }
    }
}
