﻿using Dezac.Core.Utility;

namespace Instruments.Measure
{
    public interface IMeasure
    {
        Result<T> Read<T>(int variable, bool? throwException = null);
    }
}
