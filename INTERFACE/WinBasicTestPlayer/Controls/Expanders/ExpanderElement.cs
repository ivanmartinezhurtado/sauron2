﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Layouts;
using System.Drawing;

namespace WinTestPlayer.Controls.Expanders
{
    public class ExpanderElement : RadItem
    {
        protected LightVisualElement staticColumn;
        protected LightVisualElement slidingColumn;
        Expander control;

        AnimatedPropertySetting expandingAnimation;
        AnimatedPropertySetting collapsingAnimation;

        bool isExpanded = true;

        DockLayoutPanel columnsHolder;
        protected DockLayoutPanel chevronAndContentHolder;

        ChevronButton chevronButton;

        public bool AllowExpand { get; set; }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            staticColumn = new LightVisualElement();
            staticColumn.ThemeRole = "StaticColumn";

            slidingColumn = new LightVisualElement();
            slidingColumn.ThemeRole = "SlidingColumn";

            chevronButton = new ChevronButton();
            chevronButton.ThemeRole = "ChevronButton";

            columnsHolder = new DockLayoutPanel();
            chevronAndContentHolder = new DockLayoutPanel();

            columnsHolder.Children.Add(staticColumn);
            columnsHolder.Children.Add(slidingColumn);
            DockLayoutPanel.SetDock(staticColumn, Dock.Left);

            staticColumn.Children.Add(chevronAndContentHolder);
            chevronAndContentHolder.Children.Add(chevronButton);
            DockLayoutPanel.SetDock(chevronButton, Dock.Top);

            this.Children.Add(columnsHolder);

            chevronButton.Click += new EventHandler(chevronButton_Click);
        }

        ExpanderOrientation expanderOrientation;

        public ExpanderOrientation Orientation
        {
            get
            {
                return expanderOrientation;
            }
            set
            {
                expanderOrientation = value;
                this.OnNotifyPropertyChanged("Orientation");
            }
        }

        protected override void OnNotifyPropertyChanged(string propertyName)
        {
            base.OnNotifyPropertyChanged(propertyName);

            if (propertyName == "Orientation")
            {
                if (this.Orientation == ExpanderOrientation.Left)
                {
                    DockLayoutPanel.SetDock(staticColumn, Dock.Left);
                    colAnim = null;
                    expAnim = null;
                }
                else
                {
                    DockLayoutPanel.SetDock(staticColumn, Dock.Right);
                    colAnim = null;
                    expAnim = null;
                }
            }
        }     

        public bool IsExpanded
        {
            get
            {
                return isExpanded;
            }
            set
            {
                isExpanded = value;
            }
        }

        public void Expand()
        {
            //if (IsExpanded)
            //{
            //    return;
            //}

            this.ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width + slidingColumnWidth, this.ElementTree.Control.Size.Height);

            this.slidingColumn.ResetValue(RadElement.PositionOffsetProperty, ValueResetFlags.All);

            if (this.Orientation == ExpanderOrientation.Left)
                this.slidingColumn.PositionOffset = new SizeF(+this.slidingColumn.BoundingRectangle.Width, 0);
            else
                this.slidingColumn.PositionOffset = SizeF.Empty;

            this.chevronButton.Expanded = true;
            this.IsExpanded = true;
        }

        public void Collapse()
        {
            //if (!IsExpanded)
            //{
            //    return;
            //}

            this.slidingColumn.ResetValue(RadElement.PositionOffsetProperty, ValueResetFlags.All);

            if (this.Orientation == ExpanderOrientation.Left)
                this.slidingColumn.PositionOffset = new SizeF(-this.slidingColumn.BoundingRectangle.Width, 0);
            else
                this.slidingColumn.PositionOffset = new SizeF(+this.slidingColumn.BoundingRectangle.Width, 0);

            this.ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width, this.ElementTree.Control.Size.Height);

            this.chevronButton.Expanded = false;
            this.IsExpanded = false;
        }

        protected override void OnElementTreeChanged(ComponentThemableElementTree previousTree)
        {
            base.OnElementTreeChanged(previousTree);

            control = (Expander)this.ElementTree.Control;
        }

        //int slidingColumnWidth = 0;

        void collapsingAnimation_AnimationFinished(object sender, AnimationStatusEventArgs e)
        {
            ((AnimatedPropertySetting)sender).AnimationFinished -= new AnimationFinishedEventHandler(collapsingAnimation_AnimationFinished);

            //slidingColumnWidth = slidingColumn.ControlBoundingRectangle.Width;
            this.ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width, this.ElementTree.Control.Size.Height);

            this.chevronButton.Expanded = false;
            this.IsExpanded = false;
        }

        void expandingAnimation_AnimationFinished(object sender, AnimationStatusEventArgs e)
        {
            ((AnimatedPropertySetting)sender).AnimationFinished -= new AnimationFinishedEventHandler(expandingAnimation_AnimationFinished);

            this.chevronButton.Expanded = true;
            this.IsExpanded = true;
        }

        AnimatedPropertySetting colAnim;
        AnimatedPropertySetting expAnim;
        int frames = 20;
        int interval = 10;
        int slidingColumnWidth = 230;

        protected virtual AnimatedPropertySetting CreateCollapsingAnimation()
        {
            if (this.Orientation == ExpanderOrientation.Left)
            {
                if (colAnim == null)
                    colAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        SizeF.Empty,
                        new SizeF(-this.slidingColumn.BoundingRectangle.Width, 0),
                        frames, interval);
            }
            else
            {
                if (colAnim == null)
                    colAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        SizeF.Empty,
                        new SizeF(+this.slidingColumn.BoundingRectangle.Width, 0),
                        frames, interval);
            }

            return colAnim;
        }

        protected virtual AnimatedPropertySetting CreateExpandingAnimation()
        {
            if (this.Orientation == ExpanderOrientation.Left)
            {
                if (expAnim == null)
                    expAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        new SizeF(-this.slidingColumn.BoundingRectangle.Width, 0),
                        SizeF.Empty,
                        frames, interval);
            }
            else
            {
                if (expAnim == null)
                    expAnim = new AnimatedPropertySetting(RadElement.PositionOffsetProperty,
                        new SizeF(+this.slidingColumn.BoundingRectangle.Width, 0),
                        SizeF.Empty,
                        frames, interval);
            }

            return expAnim;
        }

        protected virtual void OnButtonClicked()
        {
            if (IsExpanded)
            {
                collapsingAnimation = CreateCollapsingAnimation();

                collapsingAnimation.AnimationFinished += new AnimationFinishedEventHandler(collapsingAnimation_AnimationFinished);

                collapsingAnimation.ApplyValue(this.slidingColumn);
            }
            else
            {
                this.ElementTree.Control.Size = new Size(staticColumn.ControlBoundingRectangle.Width + slidingColumnWidth, this.ElementTree.Control.Size.Height);

                expandingAnimation = CreateExpandingAnimation();

                expandingAnimation.AnimationFinished += new AnimationFinishedEventHandler(expandingAnimation_AnimationFinished);

                expandingAnimation.ApplyValue(this.slidingColumn);
            }
        }

        void chevronButton_Click(object sender, EventArgs e)
        {
            if (this.AllowExpand)
                OnButtonClicked();
        }
    }
}
