﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests
{
    public enum TipoPotencia
    {
        Activa = 0,
        React = 1
    }

    public enum TestPointsName
    {
        TP_IbCOS05,
        TP_IbSEN05,
        TP_IMAX,
        TP_IMIN,
        TP_ARRANQUE,
        TP_MARCHA_VACIO
    }

    public class MeterType
    {
        public TipoConexionEnum TipoConexion { get; set; }
        public TipoEquipoEnum TipoEquipo { get; set; }
        public PrecisionClass PrecisionProperty { get; set; }
        public VoltagePropertyClass VoltageProperty { get; set; }
        public CurrentPropertyClass CurrentProperty { get; set; }
        public FrequencyPropertyClass FrequencyProperty { get; set; }

        public enum TipoEquipoEnum
        {
            MONOFASICO = 2,
            TRIFASICO_SIN_NEUTRO = 3,
            TRIFASICO = 4,
            QUATRIFASICO = 1,
            SIXFASICO = 6
        }

        public enum TipoConexionEnum
        {
            DIRECTO,
            INDIRECTO
        }

        public class PrecisionClass
        {
            public double PrecisionActiva { get; set; }
            public double PrecisionReacctiva { get; set; }
            public double PrecisionIminActiva { get; set; }
            public double PrecisionIminReactiva { get; set; }

            public PrecisionClass(double precisionActiva, double precisionReactiva)
            {
                this.PrecisionActiva = precisionActiva;
                this.PrecisionReacctiva = precisionReactiva;
                this.PrecisionIminActiva = PrecisionActiva + 0.5;
                this.PrecisionIminReactiva = precisionReactiva + 0.5;

            }
        }
        private readonly Dictionary<string, PrecisionClass> PrecisionList = new Dictionary<string, PrecisionClass>
            {
                { "02", new PrecisionClass(0.18, 0.45) },
                { "05", new PrecisionClass(0.45,0.8) },
                { "10", new PrecisionClass(0.8,1.8) },
                { "12", new PrecisionClass(0.8,1.8) },
            };

        public class VoltagePropertyClass
        {
            public TipoEquipoEnum TipoEquipo { get; set; }
            public double VoltageNominal { get; set; }
            public double VoltageMaximo { get; set; }
            public double VoltageMinimo { get; set; }
            public bool IsMultiRangeVoltage { get; set; }

            public VoltagePropertyClass(TipoEquipoEnum tipoEquipo, double voltageNominal, double voltageMaximo, double voltageMinimo, bool isMultiRangeVoltage = false)
            {
                this.TipoEquipo = tipoEquipo;
                this.VoltageNominal = voltageNominal;
                this.VoltageMaximo = voltageMaximo;
                this.VoltageMinimo = voltageMinimo;
                this.IsMultiRangeVoltage = isMultiRangeVoltage;
            }
        }
        private readonly Dictionary<string, VoltagePropertyClass> voltageMeasure = new Dictionary<string, VoltagePropertyClass>
            {
                { "L", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,57,100,57) },
                { "M", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,63.5,110,63.5)},
                { "N", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,127,220,127)},
                { "O", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,220,380,220)},
                { "Q", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,230,400,230)},
                { "T", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,110,190,110)},
                { "U", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,230,400,127, true)},
                { "V", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,230,400,57, true)},
                { "W", new VoltagePropertyClass(TipoEquipoEnum.TRIFASICO,66,115,66)},
                { "E", new VoltagePropertyClass(TipoEquipoEnum.MONOFASICO,230,230,230)},
                { "B", new VoltagePropertyClass(TipoEquipoEnum.MONOFASICO,127,127,127)},
                { "1", new VoltagePropertyClass(TipoEquipoEnum.MONOFASICO,120,120,120)},
            };

        public class CurrentPropertyClass
        {
            public TipoConexionEnum TipoConexion { get; set; }
            public double CorrienteBase { get; set; }
            public double CorrienteMaxima { get; set; }
            public double CorrienteMinima { get; set; }

            public CurrentPropertyClass(TipoConexionEnum tipoConexion, double corrienteBase, double corrienteMaxima)
            {
                this.TipoConexion = tipoConexion;
                this.CorrienteBase = corrienteBase;
                this.CorrienteMaxima = corrienteMaxima;
                this.CorrienteMinima = tipoConexion == TipoConexionEnum.DIRECTO ? corrienteBase * 0.05 : corrienteBase * 0.01;
            }
        }
        private readonly Dictionary<string, CurrentPropertyClass> currentMeasure = new Dictionary<string, CurrentPropertyClass>
            {
                { "TE", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,3,5) },
                { "T1", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,1,2) },
                { "T2", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,2.5,10) },
                { "T5", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,5,10) },
                { "T6", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,5,6) },
                { "T7", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,1,6) },
                { "T8", new CurrentPropertyClass(TipoConexionEnum.INDIRECTO,1,10) },
                { "D1", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,10,100) },
                { "D2", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,5,60) },
                { "D3", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,10,120) },
                { "D4", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,10,60) },
                { "D5", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,15,120) },
                { "D6", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,5,100) },
                { "D7", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,5,60) }, //Es 65A pero por Error de diseño en los CEM se baja a 60A -> C.Cabezas 
                { "D8", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,5,65) },
                { "S1", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,10,100) },
                { "S4", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,10,60) },
                { "S7", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,5,65) },
                { "S2", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,5,60) },
                { "S8", new CurrentPropertyClass(TipoConexionEnum.DIRECTO,10,40) },
            };

        public class FrequencyPropertyClass
        {
            public double Frequency { get; set; }
            public bool IsAutomatico { get; set; }

            public FrequencyPropertyClass(double frequency, bool isAutomatico)
            {
                this.Frequency = frequency;
                this.IsAutomatico = isAutomatico;
            }
        }
        private readonly Dictionary<string, FrequencyPropertyClass> frequqncyMeasure = new Dictionary<string, FrequencyPropertyClass>
            {
                { "A", new FrequencyPropertyClass(50,false) },
                { "B", new FrequencyPropertyClass(60,false) },
                { "C", new FrequencyPropertyClass(50,true) },
            };

        public MeterType(string modelo)
        {
            var modelArray = modelo.ToArray();
            TipoEquipo = (TipoEquipoEnum)Convert.ToByte(modelArray[0].ToString());
            PrecisionProperty = this.PrecisionList[(modelArray[1].ToString() + modelArray[2]).ToString()];
            VoltageProperty = this.voltageMeasure[modelArray[3].ToString()];
            CurrentProperty = this.currentMeasure[(modelArray[4].ToString() + modelArray[5]).ToString()];
            FrequencyProperty = this.frequqncyMeasure[modelArray[6].ToString()];
            TipoConexion = CurrentProperty.TipoConexion;
        }
    }
}
