﻿using System;

namespace Dezac.Core.Utility
{
    public class DesignerControlAttribute : Attribute
    {
        public Type Type { get; set; }

    }
}
