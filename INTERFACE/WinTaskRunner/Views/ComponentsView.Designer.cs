﻿namespace WinTaskRunner.Views
{
    partial class ComponentsView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeView = new Telerik.WinControls.UI.RadTreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.searchBox = new Telerik.WinControls.UI.RadTextBox();
            this.mnuNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdInspect = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInstruments = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsOpenInstrument = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSnippets = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdSnippetsPath = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDeviceGenerator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDeviceLibraryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchBox)).BeginInit();
            this.mnuNode.SuspendLayout();
            this.mnuInstruments.SuspendLayout();
            this.mnuSnippets.SuspendLayout();
            this.mnuDeviceGenerator.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.AllowDragDrop = true;
            this.treeView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageList;
            this.treeView.Location = new System.Drawing.Point(0, 20);
            this.treeView.Name = "treeView";
            // 
            // 
            // 
            this.treeView.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 20, 150, 250);
            this.treeView.Size = new System.Drawing.Size(400, 549);
            this.treeView.SpacingBetweenNodes = -1;
            this.treeView.TabIndex = 1;
            this.treeView.DragEnding += new Telerik.WinControls.UI.RadTreeView.DragEndingHandler(this.treeView_DragEnding);
            this.treeView.NodeMouseDown += new Telerik.WinControls.UI.RadTreeView.TreeViewMouseEventHandler(this.treeView_NodeMouseDown);
            this.treeView.NodeExpandedChanging += new Telerik.WinControls.UI.RadTreeView.RadTreeViewCancelEventHandler(this.treeView_NodeExpandedChanging);
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView_DragEnter);
            this.treeView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseMove);
            this.treeView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseUp);
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // searchBox
            // 
            this.searchBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox.Location = new System.Drawing.Point(0, 0);
            this.searchBox.Name = "searchBox";
            this.searchBox.NullText = "Buscar...";
            // 
            // 
            // 
            this.searchBox.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.searchBox.RootElement.StretchVertically = true;
            this.searchBox.Size = new System.Drawing.Size(400, 20);
            this.searchBox.TabIndex = 1;
            this.searchBox.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
            // 
            // mnuNode
            // 
            this.mnuNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdInspect});
            this.mnuNode.Name = "mnuNode";
            this.mnuNode.Size = new System.Drawing.Size(142, 26);
            // 
            // cmdInspect
            // 
            this.cmdInspect.Name = "cmdInspect";
            this.cmdInspect.Size = new System.Drawing.Size(141, 22);
            this.cmdInspect.Text = "Inspeccionar";
            this.cmdInspect.Click += new System.EventHandler(this.cmdInspect_Click);
            // 
            // mnuInstruments
            // 
            this.mnuInstruments.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsOpenInstrument});
            this.mnuInstruments.Name = "mnuNode";
            this.mnuInstruments.Size = new System.Drawing.Size(101, 26);
            // 
            // tsOpenInstrument
            // 
            this.tsOpenInstrument.Name = "tsOpenInstrument";
            this.tsOpenInstrument.Size = new System.Drawing.Size(100, 22);
            this.tsOpenInstrument.Text = "Abrir";
            this.tsOpenInstrument.Click += new System.EventHandler(this.tsOpenInstrument_Click);
            // 
            // mnuSnippets
            // 
            this.mnuSnippets.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSnippetsPath});
            this.mnuSnippets.Name = "mnuNode";
            this.mnuSnippets.Size = new System.Drawing.Size(220, 26);
            // 
            // cmdSnippetsPath
            // 
            this.cmdSnippetsPath.Name = "cmdSnippetsPath";
            this.cmdSnippetsPath.Size = new System.Drawing.Size(219, 22);
            this.cmdSnippetsPath.Text = "Configurar Ruta Secuencias";
            this.cmdSnippetsPath.Click += new System.EventHandler(this.cmdSnippetsPath_Click);
            // 
            // mnuDeviceGenerator
            // 
            this.mnuDeviceGenerator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.deleteDeviceLibraryToolStripMenuItem});
            this.mnuDeviceGenerator.Name = "mnuNode";
            this.mnuDeviceGenerator.Size = new System.Drawing.Size(219, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::WinTaskRunner.Properties.Resources.openfolderHS;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem1.Text = "Cargar Device Library (*.dll)";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem1_Click);
            // 
            // deleteDeviceLibraryToolStripMenuItem
            // 
            this.deleteDeviceLibraryToolStripMenuItem.Image = global::WinTaskRunner.Properties.Resources.if_notebook_delete_599741;
            this.deleteDeviceLibraryToolStripMenuItem.Name = "deleteDeviceLibraryToolStripMenuItem";
            this.deleteDeviceLibraryToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.deleteDeviceLibraryToolStripMenuItem.Text = "Delete Device Library";
            this.deleteDeviceLibraryToolStripMenuItem.Click += new System.EventHandler(this.DeleteDeviceLibraryToolStripMenuItem_Click);
            // 
            // ComponentsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.searchBox);
            this.Name = "ComponentsView";
            this.Size = new System.Drawing.Size(400, 569);
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchBox)).EndInit();
            this.mnuNode.ResumeLayout(false);
            this.mnuInstruments.ResumeLayout(false);
            this.mnuSnippets.ResumeLayout(false);
            this.mnuDeviceGenerator.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadTreeView treeView;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ContextMenuStrip mnuNode;
        private System.Windows.Forms.ToolStripMenuItem cmdInspect;
        private System.Windows.Forms.ContextMenuStrip mnuInstruments;
        private System.Windows.Forms.ToolStripMenuItem tsOpenInstrument;
        private Telerik.WinControls.UI.RadTextBox searchBox;
        private System.Windows.Forms.ContextMenuStrip mnuSnippets;
        private System.Windows.Forms.ToolStripMenuItem cmdSnippetsPath;
        private System.Windows.Forms.ContextMenuStrip mnuDeviceGenerator;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteDeviceLibraryToolStripMenuItem;
    }
}
