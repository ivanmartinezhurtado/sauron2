﻿using Dezac.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Dezac.Core.Utility
{
    public class Sampler
    {
        public SamplerConfig Config { get; set; }
        public bool Canceled { get; internal set; }
        public AggregateException Exceptions { get; set; }

        public bool HasExceptions { get { return Exceptions != null;  } }

        public Sampler()
        {
            Config = new SamplerConfig { Interval = 1000 };
        }

        public Sampler(int numIterations, int interval)
        {
            Config = new SamplerConfig { NumIterations = numIterations, Interval = interval };
        }

        public static Sampler Run(int numIterations, int interval, Action<SamplerStep> iterator)
        {
            return new Sampler(numIterations, interval).Run(iterator);
        }

        public static Sampler Run(Func<SamplerConfig> config, Action<SamplerStep> iterator)
        {
            return new Sampler { Config = config() }.Run(iterator);
        }

        public Sampler Run(Action<SamplerStep> iterator)
        {
            Run((Action)null, iterator);

            return this;
        }

        public void Run(Action init, Action<SamplerStep> iterator)
        {
            if (init != null)
                init();

            if (Config.InitialDelay > 0)
            {
                Thread.Sleep(Config.InitialDelay);
                CheckCancellationRequested();
            }

            SamplerStep stepModel = new SamplerStep { Sampler = this, Interval = Config.Interval };

            List<Exception> exceptions = null;

            for (int i = 0; i < Config.NumIterations; i++)
            {
                stepModel.Step = i;

                try
                {
                    iterator(stepModel);
                }
                catch (Exception ex)
                {
                    if (exceptions == null)
                        exceptions = new List<Exception>();

                    exceptions.Add(ex);

                    if (Config.CancelOnException)
                        break;
                }

                CheckCancellationRequested();

                if (stepModel.Cancel)
                {
                    this.Canceled = true;
                    break;
                }

                Thread.Sleep(stepModel.Interval);
            }

            if (exceptions != null && !this.Canceled)
            {
                Exceptions = new AggregateException(exceptions);

                if (Config.ThrowException)
                    throw Exceptions.InnerException;
            }
        }

        private void CheckCancellationRequested()
        {
            if (Config.CancellationToken != null && Config.CancellationToken.IsCancellationRequested)
            {
                this.Canceled = true;
                TestException.Create().SOFTWARE.SAURON.PARALELISMO_CANCELADO().Throw();
            }
        }

        public void ThrowExceptionIfExists()
        {
            if (Exceptions != null)
                throw Exceptions;
        }

        public class SamplerStep
        {
            public Sampler Sampler { get; internal set; }
            public int Step { get; set; }
            public bool Cancel { get; set; }
            public int Interval { get; set; }
        }
    }

    public class SamplerConfig
    {
        public int NumIterations { get; set; }
        public int Interval { get; set; }
        public int InitialDelay { get; set; }
        public bool CancelOnException { get; set; }
        public bool ThrowException { get; set; }
        public CancellationToken CancellationToken { get; set; }
    }

    public struct controlAdjust
    {
        public int delFirst;
        public int initCount;
        public int samples;
        public int interval;
        public int diffMaxRangeValid;
    }
}
