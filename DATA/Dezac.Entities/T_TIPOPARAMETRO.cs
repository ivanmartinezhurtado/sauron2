namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TIPOPARAMETRO
    {
        public T_TIPOPARAMETRO()
        {
            this.T_PARAMETRO = new HashSet<T_PARAMETRO>();
        }
    
        public int NUMTIPOPARAM { get; set; }
        public string TIPOPARAM { get; set; }
        public string DESCRIPCION { get; set; }
    
        public virtual ICollection<T_PARAMETRO> T_PARAMETRO { get; set; }
    }
}
