﻿using Dezac.Core.Utility;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(HP34401Description))]
    public partial class UC_HP_34401 : UserControl, IDisposable
    {
        private HP34401A hp;

        public Type InstrumentType
        {
            get
            {
                return typeof(HP34401A);
            }
        }

        public HP34401A Instrument
        {
            get
            {
                if (hp == null)
                    hp = new HP34401A();

                return hp;
            }
            set
            {
                if (hp != null)
                {
                    hp.Dispose();
                    hp = null;
                }

                hp = value;
            }
        }

        public UC_HP_34401()
        {
            InitializeComponent();

            string[] Adress=new string[]{"ADDR1","ADDR2","ADDR3"};

            cmb_Port.Items.AddRange(Adress);
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("ADDR" + Instrument.PortAdress.ToString());
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
            else
            {
                Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
                ToggleButtonsState((Button)sender);
                Instrument.SCPI.Reset.Command();
                ToggleButtonsState((Button)sender);
            }
        }
         
        private void ToggleButtonsState(Button btn)
        {
            if (btn.Equals(btnReset))
                btnReset.Enabled = !btnReset.Enabled;
            if (btn.Equals(btnIAC))
                btnIAC.Enabled = !btnIAC.Enabled;
            if (btn.Equals(btnIDC))
                btnIDC.Enabled = !btnIDC.Enabled;
            if (btn.Equals(btnVDC))
                btnVDC.Enabled = !btnVDC.Enabled;
            if (btn.Equals(btnVAC))
                btnVAC.Enabled = !btnVAC.Enabled;
            if (btn.Equals(btnR))
                btnR.Enabled = !btnR.Enabled;
        }

       private void btnVAC_Click(object sender, EventArgs e)
       {
           errorProvider1.Clear();
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
           {
               Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
               Instrument.ThrowException = true;
               ToggleButtonsState((Button)sender);
               var iden = Instrument.SCPI.Identification.Query();
               lbl_Vers_tester.Text = iden.ToString();
               var res = Instrument.SCPI.MEASure.VOLTage.AC().Query();
               lbl_Mesur_Mult.Text = res.ToString();
               ToggleButtonsState((Button)sender);
           }       
       }

       private void btnVDC_Click(object sender, EventArgs e)
       {
           errorProvider1.Clear();
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
           {
               Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
               Instrument.ThrowException = true;
               ToggleButtonsState((Button)sender);
               var iden = Instrument.SCPI.Identification.Query();
               lbl_Vers_tester.Text = iden.ToString();
               var res = Instrument.SCPI.MEASure.VOLTage.DC().Query();
               lbl_Mesur_Mult.Text = res.ToString();
               ToggleButtonsState((Button)sender);
           }       
       }

       private void btnIAC_Click(object sender, EventArgs e)
       {
           errorProvider1.Clear();
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
           {
               Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
               Instrument.ThrowException = true;
               ToggleButtonsState((Button)sender);
               var iden = Instrument.SCPI.Identification.Query();
               lbl_Vers_tester.Text = iden.ToString();
               var res = Instrument.SCPI.MEASure.CURRent.AC().Query();
               lbl_Mesur_Mult.Text = res.ToString();
               ToggleButtonsState((Button)sender);
           }       
       }

       private void btnIDC_Click(object sender, EventArgs e)
       {
           errorProvider1.Clear();
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
           {
               Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
               Instrument.ThrowException = true;
               ToggleButtonsState((Button)sender);
               var iden = Instrument.SCPI.Identification.Query();
               lbl_Vers_tester.Text = iden.ToString();
               var res = Instrument.SCPI.MEASure.CURRent.DC().Query();
               lbl_Mesur_Mult.Text = res.ToString();
               ToggleButtonsState((Button)sender);
           }       
       }

       private void btnR_Click(object sender, EventArgs e)
       {
           errorProvider1.Clear();
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
           {
               Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
               Instrument.ThrowException = true;
               ToggleButtonsState((Button)sender);
               var iden = Instrument.SCPI.Identification.Query();
               lbl_Vers_tester.Text = iden.ToString();
               var res = Instrument.SCPI.MEASure.RESistance().Query();
               lbl_Mesur_Mult.Text = res.ToString();
               ToggleButtonsState((Button)sender);
           }       
       }

       private void btnClear_Click(object sender, EventArgs e)
       {
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
           {
               Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
               ToggleButtonsState((Button)sender);
               Instrument.SCPI.Clear.Command();
               ToggleButtonsState((Button)sender);
           }
       }

       protected override void Dispose(bool disposing)
       {
           if (disposing && (components != null))
               components.Dispose();
           Instrument.Dispose();
           base.Dispose(disposing);
       }
    }
    public class HP34401Description : ControlDescription
    {
        public override string Name { get { return "Multimetro HP34401"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override Image IconBig { get { return Properties.Resources.HP34401A1; } }
        public override Type SubcomponentType { get { return typeof(HP34401A); } }
    }
}
