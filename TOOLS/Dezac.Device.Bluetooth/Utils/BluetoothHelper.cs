﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Windows.Devices.Bluetooth.Advertisement;

namespace Dezac.Device.Bluetooth.Utils
{
    public static class BluetoothHelper
    {
        static BluetoothHelper()
        {

        }

        public static List<BluetoothScanResult> Scan(int timeOut = 10000)
        {
            return Scan((Action<BluetoothScanSelector>)null, timeOut);
        }

        public static List<BluetoothScanResult> Scan(Action<BluetoothScanSelector> selector, int timeOut = 10000)
        {
            var result = new List<BluetoothScanResult>();

            var watcher = new BluetoothLEAdvertisementWatcher
            {
                ScanningMode = BluetoothLEScanningMode.Active
            };

            DateTime last = DateTime.Now;

            watcher.Received += (sender, eventArgs) =>
            {
                var args = new BluetoothScanSelector { LocalName = eventArgs.Advertisement.LocalName, BluetoothAddress = eventArgs.BluetoothAddress, RawSignalStrengthInDBm = eventArgs.RawSignalStrengthInDBm, Select = true };

                if (selector != null)
                    selector(args);

                if (args.Select)
                    result.Add(new BluetoothScanResult { BluetoothAddress = eventArgs.BluetoothAddress, RawSignalStrengthInDBm = eventArgs.RawSignalStrengthInDBm });

                //if (args.Cancel)
                //    last = DateTime.MinValue;
                //else
                //    last = DateTime.Now;
            };

            watcher.Start();

            var timeResult = 0D;

            do
            {
                Thread.Sleep(1000);
                timeResult = (DateTime.Now.Subtract(last)).TotalMilliseconds;
            } while (timeResult < timeOut);

            watcher.Stop();

            return result;
        }

        public static BluetoothScanResult Scan(string patterName, int timeOut = 10000)
        {
            return Scan((e) =>
            {
                e.Select = e.LocalName.IndexOf(patterName, StringComparison.InvariantCultureIgnoreCase) >= 0;
                e.Cancel = e.Select;
            }, timeOut).FirstOrDefault();
        }
    }

    public class BluetoothScanSelector
    {
        public System.String LocalName { get; set; }
        public System.UInt64 BluetoothAddress { get; internal set; }
        public System.Int16 RawSignalStrengthInDBm { get; internal set; }
        public bool Select { get; set; }
        public bool Cancel { get; set; }
    }

    public class BluetoothScanResult
    {
        public System.UInt64 BluetoothAddress { get; internal set; }
        public System.Int16 RawSignalStrengthInDBm { get; internal set; }
    }
}
