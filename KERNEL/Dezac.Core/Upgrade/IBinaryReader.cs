﻿namespace Dezac.Core.Upgrade
{
    public interface IBinaryReader
    {
        string FileName { get; }
        void Load(string fileName);
        byte[] ReadRecord(int numRecord);
        int RecordsCount { get; }
    }
}
