﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.Styles;
using Telerik.WinControls;

namespace WinTestPlayer.Controls.Expanders
{
    class ChevronButtonStateManager : ItemStateManagerFactory
    {
        protected override StateNodeBase CreateSpecificStates()
        {
            CompositeStateNode baseStates = new CompositeStateNode("States");

            StateNodeBase expander = new StateNodeWithCondition("Expanded", new SimpleCondition(ChevronButton.IsExpandedProperty, true));

            baseStates.AddState(expander);

            return baseStates;
        }
    }
}
