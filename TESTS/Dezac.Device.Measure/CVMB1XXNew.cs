﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.09)]
    public class CVMB1XX : DeviceBase
    {
        public CVMB1XX()
        {
        }

        public CVMB1XX(int port)
        {
            SetPort(port);
        }

        //public CVMB1XX(string hostname, int port)
        //{
        //    Modbus = new ModbusDeviceSerialPort(port, 19200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
        //}

        public void SetPort(int port, int baudRate = 19200, int perifericNumber = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)perifericNumber;           
        }

        public CVMB1XX(SerialPort serialPort)
        {
            SetSerialPort(serialPort);

        }

        public void SetSerialPort(SerialPort serialPort)
        {
            serialPort.BaudRate = 19200;
            Modbus = new ModbusDeviceSerialPort(serialPort, _logger);
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.VERSION_SOFT, 6);
        }

        public double ReadTemperature()
        {
            LogMethod();

            var value = Modbus.ReadInt32((ushort)Registers.TEMPERATURE);

            return Convert.ToDouble(value) / 100;
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.BASTIDOR, bastidor);
        }

        public int ReadBastidor()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public string ReadNumSerie()
        {
            return Modbus.ReadString((ushort)Registers.NUM_SERIE, 6);
        }

        public string ReadNumSerieModulo()
        {
            LogMethod();
           
            return Modbus.ReadString((ushort)Registers.NUM_SERIE_MODULO, 6);
        }

        public ushort ReadDetectionModulo()
        {
            LogMethod();

            return Modbus.ReadMultipleRegister((ushort)Registers.DETECCION_MODULO, 20)[1];
        }

        public ushort ReadTypeModulo()
        {
            LogMethod();

            return Modbus.ReadMultipleRegister((ushort)Registers.DETECCION_MODULO, 20)[2];
        }       

        public byte[] ReadVersionBootModulo()
        {
            LogMethod();

            return Modbus.ReadBytes((ushort)Registers.VERSION_BOOT_MODULO, 2);
        }

        public VariablesInstantaneas ReadVariablesInstantaneas()
        {
            return Modbus.Read<VariablesInstantaneas>();
        }                

        public Potencias ReadPowers()
        {
            return Modbus.Read<Potencias>();
        }

        public Inputs ReadInputs()
        {
            return Modbus.Read<Inputs>();
        }

        public FasesNeutro ReadGanaciaTension()
        {
            return Modbus.Read<FasesNeutro>((ushort)Registers.AJUSTE_GANANCIA_TENSION);
        }

        public FasesNeutro ReadGanaciaCorriente(Escalas escala)
        {
            if (escala == Escalas.E1A)
                return Modbus.Read<FasesNeutro>((ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_1A);
            else if (escala == Escalas.E5A)
                return Modbus.Read<FasesNeutro>((ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_5A);
            else
                return Modbus.Read<FasesNeutro>((ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_MC);
        }

        public GanaciasINeutro ReadGanaciaCorrienteNeutro()
        {
            return Modbus.Read<GanaciasINeutro>((ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_IN);
        }

        public double ReadCorrienteNeutro()
        {
            return Modbus.ReadInt32((ushort)Registers.CORRIENTE_NEUTRO) / 1000;
        }

        public NeutralLimits ReadNeutralLimits()
        {
            return Modbus.Read<NeutralLimits>();
        }

        public DateTime ReadDatetime()
        {
            return Modbus.Read<FechaHora>((ushort)Registers.FECHA_HORA).ToDateTime();
        }

        public bool ReadInputStatus()
        {
            return Modbus.Read<ModuleInputs>().inputState;
        }

        public ushort ReadDHCPFlag()
        {
            return Modbus.ReadRegister((ushort)Registers.DHCP);
        }

        public TCP ReadTCPConfiguration()
        {
            var tcp = Modbus.Read<TCP>();

            return tcp;
        }

        public string ReadNeuronID()
        {
            var neuronIDArray = Modbus.ReadMultipleRegister((ushort)Registers.NEURON_ID, 3);
            return neuronIDArray[0].ToString("x4").ToUpper() + neuronIDArray[1].ToString("x4").ToUpper() + neuronIDArray[2].ToString("x4").ToUpper();
        }

        public MAC ReadMac()
        {
            LogMethod();
            return Modbus.Read<MAC>();
        }

        public void WriteMACTCP(string mac)
        {
            LogMethod();

            var macReading = new MAC();
            var macArray = ModbusUtility.HexToBytes(mac);
            macReading.Field_1 = macArray[0];
            macReading.Field_2 = macArray[1];
            macReading.Field_3 = macArray[2];
            macReading.Field_4 = macArray[3];
            macReading.Field_5 = macArray[4];
            macReading.Field_6 = macArray[5];

            Modbus.Write<MAC>(macReading);
        }

        public MAC ReadModuleMac()
        {
            LogMethod();
            return Modbus.Read<MAC>((ushort)Registers.MODULE_MAC);
        }

        public TCP ReadModuleIPConfig()
        {
            return Modbus.Read<TCP>((ushort)Registers.MODULE_IP_CONFIG);
        }

        public string ReadKernelVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.KERNEL_VERSION, 4);
        }

        public string ReadPowerStudioVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.POWER_STUDIO_VERSION, 4);
        }

        public ushort ReadStatus()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.STATUS);
        }

        public ushort[] ReadKeyboard()
        {
            LogMethod();
            
            return Modbus.ReadMultipleRegister((ushort)Registers.DIFFERENCE, 3);
        }

        public Keys ReadKeyboardState()
        {
            LogMethod();

            Keys activeKeys = Keys.NINGUNA_TECLA;

            var keys = Modbus.ReadMultipleRegister((ushort)Registers.DIFFERENCE, 3);

            if (keys[0] > 100)
                if (activeKeys == Keys.NINGUNA_TECLA)
                    activeKeys = Keys.TECLA_IZQUIERDA;
                else
                    activeKeys += (ushort)Keys.TECLA_IZQUIERDA;        
                   
            if (keys[1] > 100)
                if (activeKeys == Keys.NINGUNA_TECLA)
                    activeKeys = Keys.TECLA_CENTRO;
                else
                    activeKeys += (ushort)Keys.TECLA_CENTRO;

            if (keys[2] > 100)
                if (activeKeys == Keys.NINGUNA_TECLA)
                    activeKeys = Keys.TECLA_DERECHA;
                else
                    activeKeys += (ushort)Keys.TECLA_DERECHA;

            return activeKeys;
        }



        public void WriteBacnetID(int BacnetID)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.BACNET_ID, BacnetID);
        }

        public void WriteModeBACnet485()
        {
            try
            {
                Modbus.WithTimeOut((m) => { m.Write((ushort)Registers.BACNET_RS485, (ushort)1); });
            }
            catch (Exception)
            {
            }
        }

        public void WriteServicePinFlag()
        {
            Modbus.Write((ushort)Registers.SERVICE_PIN_FLAG, (ushort)1);
        }

        public void WriteTCPConfiguration(TCP tcpConfig)
        {
            Modbus.Write<TCP>(tcpConfig);
        }

        public void WriteDHCPFlag(bool estado)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DHCP, (ushort)(estado ? 0x0001 : 0x0000));
        }

        public void WriteOutputs(OutputState output1, OutputState output2, OutputState output3, OutputState output4, OutputState output5, OutputState output6, OutputState output7, OutputState output8)
        {
            LogMethod();

            var OutputRegisterStateDictionary = new Dictionary<DigitalOutputsRegisters, OutputState>();

            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_1, output1);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_2, output2);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_3, output3);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_4, output4);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_5, output5);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_6, output6);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_7, output7);
            OutputRegisterStateDictionary.Add(DigitalOutputsRegisters.OUTPUT_8, output8);

            foreach (KeyValuePair<DigitalOutputsRegisters, OutputState> currentOutput in OutputRegisterStateDictionary)
            {
                Modbus.WriteInt32((ushort)currentOutput.Key, currentOutput.Value == OutputState.ON ? 65537 : 0);
                Thread.Sleep(1000);
            }
        }

        public void FlagTest(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void FlagTestDIO(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST_DIO, state);
        }

        public void Reset()
        {
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
            Thread.Sleep(1000);
        }

        public void Write(Registers register, ushort value)
        {
            Modbus.Write((ushort)register, value);
        }

        public void WriteNumSerie(long value)
        {
            string text = value.ToString().PadLeft(12, '0'); //6 Registros

            Modbus.WriteString((ushort)Registers.NUM_SERIE, text);
        }

        public void WriteNumSerieModulo(long value)
        {
            string text = value.ToString().PadLeft(12, '0'); //6 Registros

            Modbus.WriteString((ushort)Registers.NUM_SERIE_MODULO, text);
        }

        public void WriteNeutralLimits(NeutralLimits value)
        {
            Modbus.Write<NeutralLimits>(value);
        }

        public void WriteDateTime(DateTime? date = null)
        {
            Modbus.Write<FechaHora>((ushort)Registers.FECHA_HORA, FechaHora.Create(date));
        }

        public void WriteRelacionTransformacion(Escalas escala, bool INeutro = false)
        {
            ConfigRelacionTransformacion value;

            uint CorrienteNeutro = 0;

            if (escala == Escalas.E1A)
            {
                if (!INeutro) CorrienteNeutro = 1;
                value = new ConfigRelacionTransformacion { PrimarioTension = 1, SecundarioTension = 10, PrimarioCorriente = 100, SecundarioCorriente = 1, PrimarioCorrienteNeutro = CorrienteNeutro, SecundarioCorrienteNeutro = CorrienteNeutro, TensionNominal = 23000 };
            }
            else if (escala == Escalas.E5A)
            {
                if (!INeutro) CorrienteNeutro = 5;
                value = new ConfigRelacionTransformacion { PrimarioTension = 1, SecundarioTension = 10, PrimarioCorriente = 100, SecundarioCorriente = 5, PrimarioCorrienteNeutro = CorrienteNeutro, SecundarioCorrienteNeutro = CorrienteNeutro, TensionNominal = 23000 };
            }
            else
                value = new ConfigRelacionTransformacion { PrimarioTension = 1, SecundarioTension = 10, PrimarioCorriente = 100, SecundarioCorriente = 250, PrimarioCorrienteNeutro = CorrienteNeutro, SecundarioCorrienteNeutro = CorrienteNeutro, TensionNominal = 23000 };

             Modbus.Write<ConfigRelacionTransformacion>(value);
        }

        public void WriteGananciaDesfase(Escalas escala, Fases value)
        {
            ushort address = 1;

            if (escala == Escalas.E1A)
                address = (ushort)Registers.AJUSTE_GANANCIA_DESFASE_1A;
            else if (escala == Escalas.E5A)
                address = (ushort)Registers.AJUSTE_GANANCIA_DESFASE_5A;
            else
                address = (ushort)Registers.AJUSTE_GANANCIA_DESFASE_MC;

            Modbus.Write<Fases>(address, value);
        }

        public void WriteGananciaTension(FasesNeutro result)
        {
            Modbus.Write<FasesNeutro>((ushort)Registers.AJUSTE_GANANCIA_TENSION, result);
        }

        public void WriteGananciaCorriente(Escalas escala, FasesNeutro value)
        {
            ushort address = 1;

            if (escala == Escalas.E1A)
                address = (ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_1A;
            else if (escala == Escalas.E5A)
                address = (ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_5A;
            else
                address = (ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_MC;

            Modbus.Write<FasesNeutro>(address, value);

        }

        public void WriteGananciaCorrienteNeutro(GanaciasINeutro result)
        {
            var address = (ushort)Registers.AJUSTE_GANANCIA_CORRIENTE_IN;
            Modbus.Write<GanaciasINeutro>(address, result);
        }

        public void WriteIDHardware(IdVector value)
        {
            Modbus.Write<IdVector>(value);
        }

        public void WriteSetupDefault(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.SETUP_DEFAULT, state);
        }

        public void DeleteMaxMinAndDemand(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_MAX_MIN, state);
        }

        public void DeleteEnergies(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_ENERGIES, state);
        }

        public void ErasedEnded(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.ERASED_ENDED, state);
        }

        public IdVector ReadIDHardware()
        {
            return Modbus.Read<IdVector>();
        }

        public Tuple<bool, Fases> AjusteDesfase(int delFirst, int initCount, int numMuestras, int intervalo, CVMB1XX.Escalas escala, double angulo, Func<Fases, bool> validacionAjuste, Func<Potencias, bool> validacionMuestra = null)
        {
            var relTransf = escala == Escalas.E5A ? 5 : 1;

            FlagTest();

            WriteRelacionTransformacion(escala);
            Fases fases = new Fases { L1 = 0, L2 = 0, L3 = 0 };
            WriteGananciaDesfase(escala, fases);

            var list = new StatisticalList(6);
            var result = new Fases();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var potencias = ReadPowers();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null && !validacionMuestra(potencias))
                        return;

                    list.Add(potencias.ActivaL1 * relTransf, potencias.ActivaL2 * relTransf, potencias.ActivaL3 * relTransf, potencias.ReactivaL1 * relTransf, potencias.ReactivaL2 * relTransf, potencias.ReactivaL3 * relTransf);

                    _logger.InfoFormat(string.Format("POT.ACTIVAS : L1 = {0} // L2 = {1} // L3 = {2}", potencias.ActivaL1 * relTransf, potencias.ActivaL2 * relTransf, potencias.ActivaL3 * relTransf));
                    _logger.InfoFormat(string.Format("POT.REACTIVAS : L1 = {0} // L2 = {1} // L3 = {2}", potencias.ReactivaL1 * relTransf, potencias.ReactivaL2 * relTransf, potencias.ReactivaL3 * relTransf));

                    var range = list.MaxRange();
                    if (range > 5)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 5);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {

                        result.L1 = Convert.ToInt32((Math.Atan2(list.Average(3), list.Average(0)) - angulo.ToRadians()) * 100000);
                        result.L2 = Convert.ToInt32((Math.Atan2(list.Average(4), list.Average(1)) - angulo.ToRadians()) * 100000);
                        result.L3 = Convert.ToInt32((Math.Atan2(list.Average(5), list.Average(2)) - angulo.ToRadians()) * 100000);

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGananciaDesfase(escala, result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        public Tuple<bool, FasesNeutro> AjusteTension(int delFirst, int initCount, int numMuestras, int intervalo, TriLineValue tensionRef, Func<FasesNeutro, bool> validacionAjuste, Func<TensionesCorrientes, bool> validacionMuestra = null)
        {
            FlagTest();

            WriteRelacionTransformacion(Escalas.E5A);

            var GanaciasTension = ReadGanaciaTension();

            var list = new StatisticalList(4);
            var result = new FasesNeutro();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var tensiones = Modbus.Read<CVMB1XX.TensionesCorrientes>();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null && !validacionMuestra(tensiones))
                        return;

                    list.Add(tensiones.TensionL1, tensiones.TensionL2, tensiones.TensionL3, tensiones.TensionNeutro);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {
                        result.L1 = Convert.ToInt32(tensionRef.L1 * GanaciasTension.L1 / list.Average(0));
                        result.L2 = Convert.ToInt32(tensionRef.L2 * GanaciasTension.L2 / list.Average(1));
                        result.L3 = Convert.ToInt32(tensionRef.L3 * GanaciasTension.L3 / list.Average(2));
                        result.Neutro = Convert.ToInt32(tensionRef.L1 * GanaciasTension.Neutro / list.Average(3));

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGananciaTension(result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        public Tuple<bool, FasesNeutro> AjusteCorriente(int delFirst, int initCount, int numMuestras, int intervalo, CVMB1XX.Escalas escala, TriLineValue CorrienteRef, Func<FasesNeutro, bool> validacionAjuste, Func<TensionesCorrientes, bool> validacionMuestra = null)
        {
            var relTransf = escala == Escalas.E5A ? 5 : 1;

            FlagTest();

            WriteRelacionTransformacion(escala);


            var GanaciasCorriente = ReadGanaciaCorriente(escala);

            var list = new StatisticalList(4);
            var result = new FasesNeutro();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var corrientes = Modbus.Read<CVMB1XX.TensionesCorrientes>();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null && !validacionMuestra(corrientes))
                        return;

                    list.Add(corrientes.CorrienteL1 * relTransf, corrientes.CorrienteL2 * relTransf, corrientes.CorrienteL3 * relTransf, corrientes.CorrienteNeutro);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {
                        if (escala == Escalas.MC) CorrienteRef = 1;

                        result.L1 = Convert.ToInt32(CorrienteRef.L1 * GanaciasCorriente.L1 / list.Average(0));
                        result.L2 = Convert.ToInt32(CorrienteRef.L2 * GanaciasCorriente.L2 / list.Average(1));
                        result.L3 = Convert.ToInt32(CorrienteRef.L3 * GanaciasCorriente.L3 / list.Average(2));
                        if (escala == Escalas.MC)
                        {
                            result.Neutro = result.L3;
                        }
                        else
                            result.Neutro = Convert.ToInt32(CorrienteRef.L3 * GanaciasCorriente.Neutro / (list.Average(3) * 100));

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGananciaCorriente(escala, result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        public Tuple<bool, GanaciasINeutro> AjusteCorrienteNeutro(int delFirst, int initCount, int numMuestras, int intervalo, CVMB1XX.Escalas escala, double CorrienteRef, Func<Int32, bool> validacionAjuste, Func<TensionesCorrientes, bool> validacionMuestra = null)
        {
            var relTransf = escala == Escalas.E5A ? 5 : 1;

            FlagTest();

            WriteRelacionTransformacion(escala, true);

            var GanaciasCorriente = ReadGanaciaCorrienteNeutro();

            var list = new StatisticalList(1);

            GanaciasINeutro result = new GanaciasINeutro();

            var sampler = Sampler.Run(
             () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
             (step) =>
             {
                 var CorrienteNeutroCalc = Modbus.Read<TensionesCorrientes>();

                 if (step.Step < delFirst)
                     return;

                 if (validacionMuestra != null && !validacionMuestra(CorrienteNeutroCalc))
                     return;

                 list.Add(CorrienteNeutroCalc.CorrienteNeutro * relTransf);

                 var range = list.MaxRange();
                 if (range > 2)
                 {
                     _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                     list.Clear();
                 }

                 if (list.Count(0) >= initCount)
                 {
                     Int32 isvalid = 0;

                     result.I1 = GanaciasCorriente.I1;
                     result.I5 = GanaciasCorriente.I5;
                     result.IMC = GanaciasCorriente.IMC;

                     if (escala == Escalas.E1A)
                     {
                         result.I1 = Convert.ToInt32(CorrienteRef * GanaciasCorriente.I1 / list.Average(0));
                         isvalid = result.I1;
                     }

                     if (escala == Escalas.E5A)
                     {
                         result.I5 = Convert.ToInt32(CorrienteRef * GanaciasCorriente.I5 / list.Average(0));
                         isvalid = result.I5;
                     }

                     if (escala == Escalas.MC)
                     {
                         CorrienteRef = 1;
                         result.IMC = Convert.ToInt32(CorrienteRef * GanaciasCorriente.IMC / list.Average(0));
                         isvalid = result.IMC;
                     }

                     var valid = validacionAjuste(isvalid);

                     if (!valid)
                         list.Clear();

                     step.Cancel = valid;
                 }
             });

            if (sampler.Canceled)
            {
                FlagTest();        
                WriteGananciaCorrienteNeutro(result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        #region Structures And Enums

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Fases;
            public VariablesTrifasicas Trifasicas;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            private Int32 tension;
            private Int32 corriente;
            private Int32 potenciaActiva;
            private Int32 potenciaReactivaInductiva;
            private Int32 potenciaReactivaCapicitiva;
            private Int32 potenciaAparente;
            private Int32 factorPotencia;
            private Int32 cosenoPhi;

            public double Tension { get { return Convert.ToDouble(tension) / 100; } }
            public double Corriente { get { return Convert.ToDouble(corriente) / 100000; } }
            public double PotenciaActiva { get { return Convert.ToDouble(potenciaActiva) / 100; } }
            public double CosenoPhi { get { return Convert.ToDouble(cosenoPhi) / 1000; } }
            public double PotenciaActivaAltaResolucion { get { return Convert.ToDouble(Tension * Corriente * FactorPotencia); } }
            public double PotenciaReactivaInductiva { get { return Convert.ToDouble(potenciaReactivaInductiva) / 100; } }
            public double PotenciaReactivaInductivaAltaResolucion { get { return Convert.ToDouble(Tension * Corriente * Math.Sin(Math.Acos(FactorPotencia))); } }
            public double PotenciaReactivaCapicitiva { get { return Convert.ToDouble(potenciaReactivaCapicitiva) / 100; } }
            public double PotenciaReactivaCapacitivaAltaResolucion { get { return Convert.ToDouble(Tension * Corriente * Math.Sin(Math.Acos(FactorPotencia))); } }
            public double PotenciaAparente { get { return Convert.ToDouble(potenciaAparente) / 10; } }
            public double FactorPotencia { get { return Convert.ToDouble(factorPotencia) / 100; } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0030)]
        public struct VariablesTrifasicas
        {
            private Int32 tensionNeutro;
            private Int32 corrienteNeutro;
            private Int32 frecuencia;
            private Int32 tensionLineaL1L2;
            private Int32 tensionLineaL2L3;
            private Int32 tensionLineaL3L1;
            private Int32 tensionLineaIII;
            private Int32 tensionFaseIII;
            private Int32 corrienteIII;
            private Int32 potenciaActivaIII;
            private Int32 potenciaInductivaIII;
            private Int32 potenciaCapacitivaIII;
            private Int32 potenciaAparenteIII;
            private Int32 facttorPotenciaIII;
            private Int32 cosenoPhiIII;

            public double TensionNeutro { get { return Convert.ToDouble(tensionNeutro) / 100; } }
            public double CorrienteNeutro { get { return Convert.ToDouble(corrienteNeutro / 1000); } }
            public double Frecuencia { get { return Convert.ToDouble(frecuencia) / 100; } }
            public double TensionLineaL1L2 { get { return Convert.ToDouble(tensionLineaL1L2) / 100; } }
            public double TensionLineaL2L3 { get { return Convert.ToDouble(tensionLineaL2L3) / 100; } }
            public double TensionLineaL3L1 { get { return Convert.ToDouble(tensionLineaL3L1) / 100; } }
            public double TensionLineaIII { get { return Convert.ToDouble(tensionLineaIII) / 100; } }
            public double TensionFaseIII { get { return Convert.ToDouble(tensionFaseIII) / 100; } }
            public double CorrienteIII { get { return Convert.ToDouble(corrienteIII) / 1000; } }
            public double PotenciaActivaIII { get { return Convert.ToDouble(potenciaActivaIII); } }
            public double PotenciaInductivaIII { get { return Convert.ToDouble(potenciaInductivaIII); } }
            public double PotenciaCapacitivaIII { get { return Convert.ToDouble(potenciaCapacitivaIII); } }
            public double PotenciaAparenteIII { get { return Convert.ToDouble(potenciaAparenteIII); } }
            public double FacttorPotenciaIII { get { return Convert.ToDouble(facttorPotenciaIII) / 1000; } }
            public double CosenoPhiIII { get { return Convert.ToDouble(cosenoPhiIII) / 1000; } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 40500)]
        public struct TensionesCorrientes
        {
            private FasesNeutro tension;
            private FasesNeutro corrientes;

            public double TensionL1 { get { return tension.L1 / 100; } }
            public double TensionL2 { get { return tension.L2 / 100; } }
            public double TensionL3 { get { return tension.L3 / 100; } }
            public double TensionNeutro { get { return tension.Neutro / 100; } }
            public double CorrienteL1 { get { return corrientes.L1 / 100000; } }
            public double CorrienteL2 { get { return corrientes.L2 / 100000; } }
            public double CorrienteL3 { get { return corrientes.L3 / 100000; } }
            public double CorrienteNeutro { get { return corrientes.Neutro / 100000; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FasesNeutro
        {
            private Int32 l1;
            private Int32 l2;
            private Int32 l3;
            private Int32 neutro;

            public double L1 { get { return Convert.ToDouble(l1); } set { l1 = Convert.ToInt32(value); } }
            public double L2 { get { return Convert.ToDouble(l2); } set { l2 = Convert.ToInt32(value); } }
            public double L3 { get { return Convert.ToDouble(l3); } set { l3 = Convert.ToInt32(value); } }
            public double Neutro { get { return Convert.ToDouble(neutro); } set { neutro = Convert.ToInt32(value); } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.POTENCIAS)]
        public struct Potencias
        {
            private Fases Activa;
            private Fases Reactiva;

            public double ActivaL1 { get { return Activa.L1 / 100D; } }
            public double ActivaL2 { get { return Activa.L2 / 100D; } }
            public double ActivaL3 { get { return Activa.L3 / 100D; } }
            public double ReactivaL1 { get { return Reactiva.L1 / 100D; } }
            public double ReactivaL2 { get { return Reactiva.L2 / 100D; } }
            public double ReactivaL3 { get { return Reactiva.L3 / 100D; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Fases
        {
            private Int32 l1;
            private Int32 l2;
            private Int32 l3;

            public double L1 { get { return Convert.ToDouble(l1); } set { l1 = Convert.ToInt32(value); } }
            public double L2 { get { return Convert.ToDouble(l2); } set { l2 = Convert.ToInt32(value); } }
            public double L3 { get { return Convert.ToDouble(l3); } set { l3 = Convert.ToInt32(value); } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ID_HARDWARE)]
        public struct IdVector
        {
            public ushort typeBoardTypeDisplay;
            public ushort typeSourcetypeMeasure;
            public ushort typeMesureInTypeDevice;
            public ushort typeFrecuencyUnknow;


            public typeBoard Board
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeBoardTypeDisplay = (ushort)((valuebyte & 0X00FF) << 8);
                }
                get
                {
                    return (typeBoard)((typeBoardTypeDisplay & 0xFF00) >> 8);
                }
            }

            public typeDisplay Display
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeBoardTypeDisplay += (ushort)(valuebyte & 0X00FF);
                }
                get
                {
                    return (typeDisplay)(typeBoardTypeDisplay & 0x00FF);
                }
            }

            public typeSource Source
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeSourcetypeMeasure = (ushort)((valuebyte & 0X00FF) << 8);
                }
                get
                {
                    return (typeSource)((typeSourcetypeMeasure & 0xFF00) >> 8);
                }
            }

            public typeMeasure Measure
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeSourcetypeMeasure += (ushort)(valuebyte & 0X00FF);
                }
                get
                {
                    return (typeMeasure)(typeSourcetypeMeasure & 0x00FF);
                }
            }

            public typeMeasureIN MeasureIN
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeMesureInTypeDevice = (ushort)((valuebyte & 0X00FF) << 8);
                }
                get
                {
                    return (typeMeasureIN)((typeMesureInTypeDevice & 0xFF00) >> 8);
                }
            }

            public typeDevice Device
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeMesureInTypeDevice += (ushort)(valuebyte & 0X00FF);
                }
                get
                {
                    return (typeDevice)(typeMesureInTypeDevice & 0x00FF);
                }
            }

            public typeFrecuency Frecuency
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeFrecuencyUnknow = (ushort)((valuebyte & 0X00FF) << 8);
                }
                get
                {
                    return (typeFrecuency)((typeFrecuencyUnknow & 0xFF00) >> 8);
                }
            }

            public ushort Unknow
            {
                set
                {
                    var valuebyte = (byte)value;
                    typeFrecuencyUnknow += (ushort)(valuebyte & 0X00FF);
                }
                get
                {
                    return (ushort)(typeFrecuencyUnknow & 0x00FF);
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct GanaciasINeutro
        {
            public Int32 I5;
            public Int32 I1;
            public Int32 IMC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x2710)]
        public struct ConfigRelacionTransformacion
        {
            public UInt32 PrimarioTension;
            public UInt32 SecundarioTension;
            public UInt32 PrimarioCorriente;
            public UInt32 SecundarioCorriente;
            public UInt32 PrimarioCorrienteNeutro;
            public UInt32 SecundarioCorrienteNeutro;
            public UInt32 TensionNominal;
        };


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TEST_CONECTOR)]
        public struct Conector
        {
            public Int16 B13_B14;
            public Int16 B15_B16;
            public Int16 B20_B21;
            public Int16 B23_B24;
            public Int16 A17_A18;
            public Int16 A21_A22;

            public bool IsValid { get { return B13_B14 == 0 && B15_B16 == 0 && B20_B21 == 0 && B23_B24 == 0 && A17_A18 == 0 && A21_A22 == 0; } }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.LIMITES_NEUTRO)]
        public struct NeutralLimits
        {
            public Int32 LimitsVoltage;
            public Int32 LimitsCurrent;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x59D8)]
        public struct Inputs
        {
            public Int32 Input1;
            public Int32 Input2;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FechaHora
        {
            public UInt16 Year;
            public UInt16 Month;
            public UInt16 Day;
            public UInt16 Hour;
            public UInt16 Minutes;
            public UInt16 Seconds;

            public static FechaHora Create(DateTime? date = null)
            {
                DateTime fecha = date.GetValueOrDefault(DateTime.Now);

                FechaHora value = new FechaHora
                {
                    Year = (ushort)fecha.Year,
                    Month = (ushort)fecha.Month,
                    Day = (ushort)fecha.Day,
                    Hour = (ushort)fecha.Hour,
                    Minutes = (ushort)fecha.Minute,
                    Seconds = (ushort)fecha.Second,
                };

                return value;
            }

            public DateTime ToDateTime()
            {
                return new DateTime(Year, Month, Day, Hour, Minutes, Seconds);
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.INPUT_STATUS)]
        public struct ModuleInputs
        {
            private ushort _input1;
            private ushort _input2;
            private ushort _input3;
            private ushort _input4;
            private ushort _input5;
            private ushort _input6;
            private ushort _input7;
            private ushort _input8;

            public bool inputState { get { return _input1 == 0 && _input2 == 0 && _input3 == 0 && _input4 == 0 && _input5 == 0 && _input6 == 0 && _input7 == 0 && _input8 == 0; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TCP_CONFIGURATION)]
        public struct TCP
        {
            private Address _ip;
            private Address _mask;
            private Address _gateway;

            public string IP
            {
                get
                {
                    return _ip.Field_1.ToString() + "." + _ip.Field_2.ToString() + "." + _ip.Field_3.ToString() + "." + _ip.Field_4.ToString();
                }
                set
                {
                    var fields = value.Split('.');

                    if (fields.Length != 4)
                        throw new Exception("Formato de la IP Incorrecto");

                    byte.TryParse(fields[0], out _ip.Field_1);
                    byte.TryParse(fields[1], out _ip.Field_2);
                    byte.TryParse(fields[2], out _ip.Field_3);
                    byte.TryParse(fields[3], out _ip.Field_4);
                }
            }
            public string Mask
            {
                get
                {
                    return _mask.Field_1.ToString() + "." + _mask.Field_2.ToString() + "." + _mask.Field_3.ToString() + "." + _mask.Field_4.ToString();
                }
                set
                {
                    var fields = value.Split('.');

                    if (fields.Length != 4)
                        throw new Exception("Formato de la máscara Incorrecto");

                    byte.TryParse(fields[0], out _mask.Field_1);
                    byte.TryParse(fields[1], out _mask.Field_2);
                    byte.TryParse(fields[2], out _mask.Field_3);
                    byte.TryParse(fields[3], out _mask.Field_4);
                }
            }
            public string Gateway
            {
                get
                {
                    return _gateway.Field_1.ToString() + "." + _gateway.Field_2.ToString() + "." + _gateway.Field_3.ToString() + "." + _gateway.Field_4.ToString();
                }
                set
                {
                    var fields = value.Split('.');

                    if (fields.Length != 4)
                        throw new Exception("Formato de la Gateway Incorrecto");

                    byte.TryParse(fields[0], out _gateway.Field_1);
                    byte.TryParse(fields[1], out _gateway.Field_2);
                    byte.TryParse(fields[2], out _gateway.Field_3);
                    byte.TryParse(fields[3], out _gateway.Field_4);
                }
            }

            public string RangIP
            {
                get
                {
                    return _ip.Field_1.ToString() + "." + _ip.Field_2.ToString() + "." + _ip.Field_3.ToString();
                }
            }
        }

        public struct Address
        {
            public byte Field_1;
            public byte Field_2;
            public byte Field_3;
            public byte Field_4;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAC)]
        public struct MAC
        {
            public byte Field_1;
            public byte Field_2;
            public byte Field_3;
            public byte Field_4;
            public byte Field_5;
            public byte Field_6;

            public byte[] getMAC { get { return new byte[] { Field_1, Field_2, Field_3, Field_4, Field_5, Field_6 }; } }

            public override string ToString()
            {
                return BitConverter.ToString(getMAC).Replace("-", ":");
            }
        }

        public enum Registers
        {
            RESET = 0x07D0,
            BORRADO_ENERGIA = 2100,
            FLAG_TEST = 0x2AF8,
            FLAG_TEST_DIO = 0X2AF9,
            FECHA_HORA = 0x283C,
            VERSION_SOFT = 0x2AF8,
            NUM_SERIE = 0x2756,
            ID_HARDWARE = 0X276A,
            NUM_SERIE_MODULO = 0x285A,
            DETECCION_MODULO = 0x2918,
            FECHA_MODULO = 0x28AA,
            AJUSTE_GANANCIA_DESFASE_5A = 0x9C62,
            AJUSTE_GANANCIA_DESFASE_1A = 0x9C74,
            AJUSTE_GANANCIA_DESFASE_MC = 0x9C88,
            AJUSTE_GANANCIA_TENSION = 0x9C40,
            AJUSTE_GANANCIA_CORRIENTE_5A = 0x9C4A,
            AJUSTE_GANANCIA_CORRIENTE_1A = 0x9C6A,
            AJUSTE_GANANCIA_CORRIENTE_MC = 0x9C7E,
            AJUSTE_GANANCIA_CORRIENTE_IN = 0x9C92,
            CORRIENTE_NEUTRO = 0X9E42,
            POTENCIAS = 0x9E44,
            LEDS = 0x9E52,
            DO = 0x9E54,
            DISPLAY = 0x9E56,
            TEMPERATURE = 0x2852,
            VERSION_BOOT_MODULO = 0x2860,
            BASTIDOR = 10130, //2 Registros
            SETUP_DEFAULT = 3004, // 1 Rele 05
            DELETE_MAX_MIN = 0X0848,
            DELETE_ENERGIES = 0X0834,
            ERASED_ENDED = 0X0C1C,
            BACNET_ID = 10230, // 2 Registros
            BACNET_RS485 = 10050, // 1 Registro
            TEST_CONECTOR = 0x9E57, // 6 Registros
            LIMITES_NEUTRO = 23012,
            BORRADO_FINAL = 3100,           
            INPUT_STATUS = 0x2882, // 8 REGISTROS
            DHCP = 62010, // 1 registro
            TCP_CONFIGURATION = 62000, // 6 registros
            MAC = 0xF236, // 3 registros
            MODULE_MAC = 0xE2A0,
            MODULE_IP_CONFIG = 0xE290, // 2 Registros
            SERVICE_PIN_FLAG = 0xE678,
            NEURON_ID = 0xE67A,
            KERNEL_VERSION = 0xE298,
            POWER_STUDIO_VERSION = 0xE29C,
            STATUS = 0x07D0,
            DIFFERENCE = 0x75AB,
            CONFIG_MODULO_TCP_MODBUS = 62720
        }

        public ConfigComunications485 ReadModuleCommunicationsConfig485()
        {
            LogMethod();
            return Modbus.Read<ConfigComunications485>();
        }

        public void WriteModuleComunicationsConfig485(ConfigComunications485 config)
        {
            LogMethod();
            Modbus.Write<ConfigComunications485>(config);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIG_MODULO_TCP_MODBUS)]
        public struct ConfigComunications485
        {
            public ushort baudRate;
            public ushort parity;
            public ushort lenght;
            public ushort stopBits;

            public BaudRateModules BaudRate { get { return (BaudRateModules)baudRate; } set { baudRate = (ushort)value; } }
        }

        public enum BaudRateModules
        {
            _19200 = 4,
            _38400 = 5
        }
        public enum DigitalOutputsRegisters
        {
            OUTPUT_1 = 0xC428, // 2 REGISTROS, UNO PARA INDICAR SALIDA MANUAL O AUTOMATICA Y OTRO PARA FORZAR EL VALOR
            OUTPUT_2 = 0xC43C,
            OUTPUT_3 = 0xC450,
            OUTPUT_4 = 0xC464,
            OUTPUT_5 = 0xC478,
            OUTPUT_6 = 0xC48C,
            OUTPUT_7 = 0xC4A0,
            OUTPUT_8 = 0xC4B4,
        }
        public enum Escalas
        {
            E1A = 1,
            E5A = 5,
            MC = 250
        }

        public enum OutputState
        {
            ON,
            OFF
        }

        [Flags]
        public enum Keys
        {
            TECLA_IZQUIERDA = 1,
            TECLA_CENTRO = 2,
            TECLA_DERECHA = 4,     
            NINGUNA_TECLA = 8              
        }

        public enum typeBoard
        {
            CPU = 0x0001,
            DIORELES = 0x0002,
            DIOOPTOS = 0x0003,
            ANALOG = 0x0004,
            LON_WORKS = 0x0005,
            EMBEDDED = 0x0006,
            PROFIBUS = 0x0007,
            MBUS = 0x0008,
            TCPMODBUS = 0x0009,
            ILEAK = 0X000A,
            CPU_UNBRANDED = 0x0064,
            NULL = 0xFFFF
        }

        public enum typeDisplay
        {
            D5 = 0x0000,
            D3 = 0x0001,
            NULL = 0xFFFF
        }

        public enum typeSource
        {
            AC = 0x0001,
            DC = 0x0002,
            NULL = 0xFFFF
        }

        public enum typeMeasure
        {
            I5A = 0x0000,
            I1A = 0x0001,
            I250mA = 0x0002,
            NULL = 0xFFFF
        }

        public enum typeMeasureIN
        {
            IN5A = 0x0000,
            IN1A = 0x0001,
            INCALC = 0x0002,
            NULL = 0xFFFF
        }

        public enum typeDevice
        {
            CVMB = 0x0000,
            CVMA = 0x0001,
            NULL = 0xFFFF
        }

        public enum typeFrecuency
        {
            _50Hz = 0x0000,
            _400Hz = 0x0001,
            NULL = 0xFFFF
        }


        public enum ModuleType
        {

            RELES = 2,
            OPTOS = 3,
            ANALOG = 4,
            LONWORKS = 5,
            DATALOGGER = 6,
            PROFIBUS = 7,
            MBUS = 8,
            TCP_MODBUS = 9,
            ILEAK = 10,
            UNIDENTIFIED = 99,
        }
       
        #endregion
    }
}