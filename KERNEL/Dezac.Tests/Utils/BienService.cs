﻿using Dezac.Data;
using Dezac.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Dezac.Tests.Utils
{
    public class BienService
    {
        public List<BienListType> ListaBienes { get; set; }

        public BienService(int numProducto, int Version, string IdFase)
        {
            ListaBienes = new List<BienListType>();

            List<BIEN> list;

            using (var svc = new DezacService())
            {
                list = svc.GetBienByProductoFase(numProducto, Version, IdFase);
            }

            var listBienesGruop = list.GroupBy(p => p.IDTIPO).ToList();

            foreach (var tiposbien in listBienesGruop)
            {
                var bienType = new BienListType(tiposbien.ToList(), tiposbien.Key);
                ListaBienes.Add(bienType);
            }
        }
    }

    public enum TypeBien
    {
        MANGUERA,
        UTIL_TIPO_MAQUINA,
        UTIL_TIPO_NO_MAQUINA,
        UTIL_ACCESORIOS,
        TORRE,
        EQUIPO_CONTRASTE_TEST,
        INSTRUMENTO_CALIBRACION,
        HERRAMIENTA,
        INFORMATICA,
        OTHERS
    }

    public class BienListType
    {
        public TypeBien tipoBien { get; internal set; }

        public List<BienClass> ListBien { get; set; }

        public BienListType(List<BIEN> list, string tipo)
        {
            ListBien = new List<BienClass>();

            switch (tipo.ToUpper())
            {
                case "1D1":
                    tipoBien = TypeBien.UTIL_TIPO_MAQUINA;
                    break;
                case "1D2":
                    tipoBien = TypeBien.UTIL_TIPO_NO_MAQUINA;
                    break;
                case "1D4":
                    tipoBien = TypeBien.UTIL_ACCESORIOS;
                    break;
                case "1D3":
                    tipoBien = TypeBien.MANGUERA;
                    break;
                case "1C1":
                    tipoBien = TypeBien.TORRE;
                    break;
                case "1J":
                    tipoBien = TypeBien.EQUIPO_CONTRASTE_TEST;
                    break;
                case "1B1":
                    tipoBien = TypeBien.INSTRUMENTO_CALIBRACION;
                    break;
                case "1B2":
                    tipoBien = TypeBien.HERRAMIENTA;
                    break;
                case "6A":
                    tipoBien = TypeBien.INFORMATICA;
                    break;
                default:
                    tipoBien = TypeBien.OTHERS;
                    break;
            }

            foreach (var bien in list)
            {
                var classBien = new BienClass(bien);
                ListBien.Add(classBien);
            }
        }

    }

    public class BienClass
    {
        public int WarningTimeOutByDays { get; set; }

        public BIEN Bien { get; internal set; }

        public List<BienClass> bienHijos { get; internal set; }

        public string Descripcion
        {
            get
            {
                if (Bien != null)
                    return Bien.DESCRIPCION;
                else
                    return "";
            }
        }

        public Image imagen
        {
            get
            {
                if (Bien != null)
                {
                    if (Bien.IMAGEN == null)
                        return Properties.Resources.Imagen_no_disponible_svg;
                    else
                        using (MemoryStream mStream = new MemoryStream(Bien.IMAGEN))
                        {
                            return Image.FromStream(mStream);
                        }
                }else
                    return Properties.Resources.Imagen_no_disponible_svg;
            }
        }

        public bool HasHjios
        {
            get
            {
                if (bienHijos != null)
                    return bienHijos.Any();
                else
                    return false;
            }
        }

        public bool HasAsistenciaMantenimiento
        {
            get
            {
                if (Bien != null)
                    return Bien.BIENASISTENCIA.Where((p) => p.ABIERTA_SN == "S" && p.MANTENIMIENTO =="S" ).Any();
                else
                    return false;
            }
        }

        public bool HasAsistenciaCalibracion
        {
            get
            {
                if (Bien != null)
                    return Bien.BIENASISTENCIA.Where((p) => p.ABIERTA_SN == "S" && p.CALIBRACION == "S").Any();
                else
                    return false;
            }
        }

        public bool HasAsistenciaReparacion
        {
            get
            {
                if (Bien != null)
                    return Bien.BIENASISTENCIA.Where((p) => p.ABIERTA_SN == "S" && p.REPARACION == "S").Any();
                else
                    return false;
            }
        }

        public bool HasAsistenciaAjuste
        {
            get
            {
                if (Bien != null)
                    return Bien.BIENASISTENCIA.Where((p) => p.ABIERTA_SN == "S" && p.AJUSTE == "S").Any();
                else
                    return false;
            }
        }

        public bool HasProgramacionMantenimiento
        {
            get
            {
                if (Bien != null)
                {
                    var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S" && p.DESCRIPCIONPROGRAMACION.ToUpper().Contains("MANTENIMIENTO")).OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
                    if (progrmacion == null || !progrmacion.FECHAULTIMAEJECUCION.HasValue || !progrmacion.PERIODICIDAD.HasValue)
                        return false;
                    else
                        return true;
                }
                return false;
            }
        }

        public bool HasProgramacionCalibracion
        {
            get
            {
                if (Bien != null)
                {
                    var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S" && p.DESCRIPCIONPROGRAMACION.ToUpper().Contains("CALIBRA")).OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
                    if (progrmacion == null || !progrmacion.FECHAULTIMAEJECUCION.HasValue || !progrmacion.PERIODICIDAD.HasValue)
                        return false;
                    else
                        return true;
                }
                return false;
            }
        }

        public bool WarningMantenimeinto
        {
            get
            {
                if (Bien != null)
                    if (HasProgramacionMantenimiento)
                    {
                        var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S" && p.DESCRIPCIONPROGRAMACION.ToUpper().Contains("MANTENIMIENTO")).OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
                        var periodicidad = new TimeSpan(progrmacion.PERIODICIDAD.Value, 0, 0, 0);
                        var dateTimeNextTask = progrmacion.FECHAULTIMAEJECUCION.Value.Add(periodicidad);
                        var DiffTime = dateTimeNextTask.Subtract(DateTime.Now);
                        if (DiffTime.TotalDays < WarningTimeOutByDays)
                            return true;
                    }

                return false;
            }
        }

        public bool WarningCalibracion
        {
            get
            {
                if (Bien != null)
                    if (HasProgramacionCalibracion)
                    {
                        var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S" && p.DESCRIPCIONPROGRAMACION.ToUpper().Contains("CALIBRA")).OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
                        var periodicidad = new TimeSpan(progrmacion.PERIODICIDAD.Value, 0, 0, 0);
                        var dateTimeNextTask = progrmacion.FECHAULTIMAEJECUCION.Value.Add(periodicidad);
                        var DiffTime = dateTimeNextTask.Subtract(DateTime.Now);
                        if (DiffTime.TotalDays < WarningTimeOutByDays)
                            return true;
                    }

                return false;
            }
        }

        public bool ErrorMantenimeinto
        {
            get
            {
                if (Bien != null)
                    if (HasProgramacionMantenimiento)
                    {
                        var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S" && p.DESCRIPCIONPROGRAMACION.ToUpper().Contains("MANTENIMIENTO")).OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
                        var periodicidad = new TimeSpan(progrmacion.PERIODICIDAD.Value, 0, 0, 0);
                        var dateTimeNextTask = progrmacion.FECHAULTIMAEJECUCION.Value.Add(periodicidad);
                        var DiffTime = dateTimeNextTask.Subtract(DateTime.Now);
                        if (DiffTime.TotalDays < 0)
                            return true;
                    }

                return false;
            }
        }

        public bool ErrorCalibraion
        {
            get
            {
                if (Bien != null)
                    if (HasProgramacionCalibracion)
                    {
                        var progrmacion = Bien.BIENPROGRAMACIONASISTENCIA.Where((p) => p.ACTIVA == "S" && p.DESCRIPCIONPROGRAMACION.ToUpper().Contains("CALIBRA")).OrderByDescending((s) => s.NUMPROGRAMACION).FirstOrDefault();
                        var periodicidad = new TimeSpan(progrmacion.PERIODICIDAD.Value, 0, 0, 0);
                        var dateTimeNextTask = progrmacion.FECHAULTIMAEJECUCION.Value.Add(periodicidad);
                        var DiffTime = dateTimeNextTask.Subtract(DateTime.Now);
                        if (DiffTime.TotalDays < 0)
                            return true;
                    }

                return false;
            }
        }

        public BienClass(BIEN bien)
        {
            WarningTimeOutByDays = 15;

            bienHijos = new List<BienClass>();

            using (var svc = new DezacService())
            {
                Bien = svc.GetBienParamsAssitencia(bien.NUMBIEN);
                var ListHijos = svc.GetBienHijos(bien.NUMBIEN);

                foreach (var hijos in ListHijos)
                    bienHijos.Add(new BienClass(hijos));
            }
        }
    }
}
