﻿using Dezac.Core.Exceptions;
using Dezac.Services;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using TaskRunner;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(SaveFileTesFaseDescription))]
    public class SaveFileTestFase : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre con el que se grabará el fichero")]
        public string FileName { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre de la variable donde se ha guardado el fichero o donde está el path del fichero")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Tipo de fichero que vamos a grabar en la Base de Datos")]
        public TipoFicheroEnum FileType { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            if (DataInput.IsKeyNullOrEmpty)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("NombreVariableFichero vacio").Throw();

            if (string.IsNullOrEmpty(FileName))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("NombreFichero vacio").Throw();

            var config = VariablesTools.ResolveValue(FileName);
            if (config == null)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("NombreVariableFichero no encontrado en las varables").Throw();

            var file = UTF8Encoding.UTF8.GetBytes(config.ToString());

            if (file == null || file.Length == 0)
                Error().PROCESO.ACTION_EXECUTE.VALOR_INCORRECTO(string.Format("Fichero {0} inexistnte", DataInput.Value)).Throw();

            DataUtils.SaveFileNumTestFase(data.TestInfo.NumTestFase.Value, FileName, ((int)FileType).ToString(), file);

            using (var svc = new DezacService())
            {
                try
                {
                    var fileSaved = svc.GetTestFaseFiles((int)(data.TestInfo.NumBastidor.Value), ((int)FileType).ToString());
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Excecion al querer recuperar el fichero {0} guardado en la BBDD de tipo {1}", FileName, (FileType).ToString());
                }
            }
        }

        public enum TipoFicheroEnum
        {
            XML = 971,
            LOGGER = 84
        }
    }

    public class SaveFileTesFaseDescription : ActionDescription
    {
        public override string Name { get { return "SaveFileTestFase"; } }
        public override string Category { get { return "DezacTest/Oracle/SaveResults"; } }
        public override string Description { get { return "Action que sirve para guardar los datos del test en un fichero"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }
}
