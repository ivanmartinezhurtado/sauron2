﻿using Dezac.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WinTestPlayer.Model
{
    public class TestModel : INotifyPropertyChanged
    {
        private int udsCajaActual;
        private int cajaActual;
        private int cajasTotales;
        private int idcajaActual;
        private int udsFaltanCajaActual;
        private int numEquiposCaja;
        private string minNumSerieCaja;
        private string maxNumSerieCaja;
        private List<string> serialNumberBoxRange;
        private string referenciaLineaCliente;
        private string referenciaOrigen;
        private string eanCode;
        private ModoPlayer modoPlayer;
        private ORDEN orden;

        public bool IsProcess { get; set; }
        public bool IsAOI { get; set; }
        public bool IsAOITrace { get; set; }
        public TestModel()
        {
            ResultadosComunes = new Dictionary<string, string>();
        }
        public string IdUsuario { get; set; }
        public string IdEmpleado { get; set; }
        public string IdOperario { get; set; }
        public string Operario { get; set; }
        public string IdFase { get; set; }
        public string DescripcionFase { get; set; }
        public int NumOrden { get { return Orden != null ? Orden.NUMORDEN : 0; } }
        public int Nivelcontroltest { get; set; }
        public int? NumAsistencia { get; set; }
        public int? NumLineaAsistencia { get; set; }
        public string NumSerieAsistencia { get; set; }
        public UInt64? BastidorTIC { get; set; }
        public bool IsReproceso { get; set; }
        public bool MantenerNumSerie { get; set; }
        public string EANMarcacode { get; set; }
        public PRODUCTO Producto { get; set; }
        public PRODUCTOFASE ProductoFase { get; set; }
        public List<T_GRUPOPARAMETRIZACION> Parametros { get; set; }
        public List<string> Direcciones { get; set; }
        public List<string> Atributos { get; set; }
        public Dictionary<string, string> DescripcionLenguagesCliente { get; set; }
        public string CodigoCircutor { get; set; }
        public string CodigoBcn { get; set; }
        public string CostumerCode2 { get; set; }
        public string DesCom { get; set; }
        public string RefVenta { get; set; }
        public bool BoxIsFinished { get; set; }
        public string Lote { get; set; }
        public bool WhitOutLine { get; set; }
        public bool NumSerieAssigned { get; set; }
        public ModoPlayer ModoPlayer
        {
            get { return modoPlayer; }
            set
            {
                if (modoPlayer != value)
                {
                    modoPlayer = value;
                    OnPropertyChanged("ModoPlayer");
                }
            }
        }
        public ORDEN Orden
        {
            get { return orden; }
            set
            {
                if (orden != value)
                {
                    orden = value;
                    OnPropertyChanged("Orden");
                }
            }
        }
        public int CajaActualID
        {
            get { return idcajaActual; }
            set
            {
                if (idcajaActual != value)
                {
                    idcajaActual = value;
                    OnPropertyChanged("CajaActualID");
                }
            }
        }
        public int CajasTotales
        {
            get { return cajasTotales; }
            set
            {
                if (cajasTotales != value)
                {
                    cajasTotales = value;
                    OnPropertyChanged("CajasTotales");
                }
            }
        }
        public int CajaActual
        {
            get { return cajaActual; }
            set
            {
                if (cajaActual != value)
                {
                    cajaActual = value;
                    OnPropertyChanged("CajaActual");
                }
            }
        }
        public string MinNumSerieCaja
        {
            get { return minNumSerieCaja; }
            set
            {
                if (minNumSerieCaja != value)
                {
                    minNumSerieCaja = value;
                    OnPropertyChanged("MinNumSerieCaja");
                }
            }
        }
        public string MaxNumSerieCaja
        {
            get { return maxNumSerieCaja; }
            set
            {
                if (maxNumSerieCaja != value)
                {
                    maxNumSerieCaja = value;
                    OnPropertyChanged("MaxNumSerieCaja");
                }
            }
        }
        public List<string> SerialNumberBoxRange
        {
            get { return serialNumberBoxRange; }
            set
            {
                if (serialNumberBoxRange != value)
                {
                    serialNumberBoxRange = value;
                    OnPropertyChanged("SerialNumberBoxRange");
                }
            }
        }
        public string ReferenciaLineaCliente
        {
            get { return referenciaLineaCliente; }
            set
            {
                if (referenciaLineaCliente != value)
                {
                    referenciaLineaCliente = value;
                    OnPropertyChanged("ReferenciaLineaCliente");
                }
            }
        }
        public string ReferenciaOrigen
        {
            get { return referenciaOrigen; }
            set
            {
                if (referenciaOrigen != value)
                {
                    referenciaOrigen = value;
                    OnPropertyChanged("ReferenciaOrigen");
                }
            }
        }
        public string EANcode
        {
            get { return eanCode; }
            set
            {
                if (eanCode != value)
                {
                    eanCode = value;
                    OnPropertyChanged("EANcode");
                }
            }
        }
        public int EquiposPorCaja
        {
            get { return udsCajaActual; }
            set
            {
                if (udsCajaActual != value)
                {
                    udsCajaActual = value;
                    OnPropertyChanged("EquiposPorCaja");
                }
            }
        }
        public int EquiposFaltanCajaActual
        {
            get { return udsFaltanCajaActual; }
            set
            {
                if (udsFaltanCajaActual != value)
                {
                    udsFaltanCajaActual = value;
                    OnPropertyChanged("EquiposFaltanCajaActual");
                }
            }
        }
        public int EquiposEntradosCajaActual
        {
            get { return numEquiposCaja; }
            set
            {
                if (numEquiposCaja != value)
                {
                    numEquiposCaja = value;
                    OnPropertyChanged("EquiposEntradosCajaActual");
                }
            }
        }
        public bool FaseNumSerieAssigned
        {
            get { return NumSerieAssigned; }
            set
            {
                if (NumSerieAssigned != value)
                {
                    NumSerieAssigned = value;
                    OnPropertyChanged("FaseNumSerieAssigned");
                }
            }
        }
        public int? RunNumInstances { get; set; }
        public int NumUtilBien { get; set; }
        public BIEN UtilBien { get; set; }
        public int NumMangueraBien { get; set; }
        public BIEN MangueraBien { get; set; }
        public string PrinterLabel { get; set; }
        public string PrinterReport { get; set; }
        public string PrinterCharacteristics { get; set; }
        public string PrinterIndividualPackage { get; set; }
        public string PrinterPackingPackage { get; set; }
        public int? ReportVersion { get; set; }
        public DateTime? FechaUltimoAutoTest { get; set; }
        public string TestLocationDescription { get; set; }
        public string Ubicacion { get; set; }
        public Dictionary<string, string> ResultadosComunes { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum ModoPlayer
    {
        SAT,
        PostVenta,
        AutoTest,
        TIC,
        PrintLabel,
        SinPermisos,
        Normal,
        Laser
    }
}
