﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(MetrixDescription))]
    public partial class UC_MTX : UserControl
    {
        MTX3293 mtx;
        private MyComputer computer;

        public Type InstrumentType
        {
            get
            {
                return typeof(MTX3293);
            }
        }


        public MTX3293 Instrument
        {
            get
            {
                if (mtx == null)
                    mtx = new MTX3293();

                return mtx;
            }
            set
            {
                if (mtx != null)
                {
                    mtx.Dispose();
                    mtx = null;
                }
                mtx = value;
            }
        }

        public UC_MTX()
        {
            InitializeComponent();
            computer = new MyComputer();

           cmbCom.Items.AddRange(computer.SerialPortNames.ToArray());
           cmbCom.SelectedIndex = cmbCom.FindStringExact("COM" + 2);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Instrument.Reset();
        }

        private void btnReadVoltage_Click(object sender, EventArgs e)
        {
                txtResult.Text = Instrument.ReadVoltage(MTX3293.measureTypes.AC, MTX3293.RangeVoltage.AUTO, false).ToString() + " V";
        }

        private void btnReadCurrent_Click(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.ReadCurrent(MTX3293.measureTypes.AC, false).ToString() + " A";
        }

        private void btnReadTemperature_Click(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.ReadTemperature(false).ToString() + " ºC";
        }

        private void btnReadCapacitance_Click(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.ReadCapacitance(MTX3293.RangeCapacitance.AUTO, false).ToString() + " F";
        }

        private void btnReadResistance_Click(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.ReadResistance(MTX3293.RangeResistance.AUTO,false).ToString() + " Ohms";
        }

        private void btnReadFrequency_Click(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.ReadFrecuency(MTX3293.RangeFrecuency.INF200KHZ, false).ToString() + " Hz";
        }

        private void btnSetFunction_Click(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.ReadFunction();
        }

        private void btnRange_Click(object sender, EventArgs e)
        {
            Instrument.SetResistanceRange(MTX3293.RangeResistance._10M);
        }

        private void btnMode_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            var mode = MTX3293.measureTypes.NULL;
            Enum.TryParse<MTX3293.measureTypes>(btn.Text, out mode);

            Instrument.SetMode(mode);
        }

        private void btnAutoRange_Click(object sender, EventArgs e)
        {
            Instrument.SetAutoRange();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //txtResult.Text = mtx.ReadAmplitudeRangeInFrecuency();
        }

        private void btnReadFuncCalc_Click(object sender, EventArgs e)
        {
           // txtResult.Text = mtx.ReadFunctionCalcdDisplaySecondary();
        }

        private void btnV_MouseEnter(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.BorderStyle = BorderStyle.FixedSingle;
        }

        private void btnV_MouseLeave(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.BorderStyle = BorderStyle.None;
        }

        private void cmbCom_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox) sender;
            try
            {
                Instrument.PortCom = Convert.ToByte(cmb.Text.Replace("COM", ""));
            }
            catch(Exception)
            {
                Logger.Error("Error, no se ha podido asignar el puerto especificado al dispositivo", new Exception());
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txtResult.Text = Instrument.help();
        }       
    }
    public class MetrixDescription : ControlDescription
    {
        public override string Name { get { return "METRIX"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override Image IconBig { get { return Properties.Resources.MTX3293; } }
        public override Type SubcomponentType { get { return typeof(MTX3293); } }
    }
}
