﻿using Dezac.Tests.UserControls;
using System;
using System.Windows.Forms;

namespace WinTestPlayer.Views
{
    public partial class LoginEmpleadoView : DialogViewBase
    {
        public LoginEmpleadoView()
        {
            InitializeComponent();
        }

        public LoginEmpleadoView(bool allowUnlock)
            : this()
        {
            this.imgLock.Visible = allowUnlock;
        }

        public bool Unlocked { get; set; }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            if (string.IsNullOrEmpty(Codigo) && !Unlocked)
                return false;

            return true;
        }

        public string Codigo
        {
            get { return txtUserName.Text; }
            set { txtUserName.Text = value; }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Unlocked = true;
            CloseView(DialogResult.OK);
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                CloseView(DialogResult.OK);
        }
    }
}
