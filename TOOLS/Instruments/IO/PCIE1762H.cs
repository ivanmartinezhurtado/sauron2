﻿using Instruments.Utility;
using log4net;
using System;

namespace Instruments.IO
{
    using Automation.BDaq;

    [InstrumentsVersion(1.00)]
    public class PCIE1762H : IOBase
    {
        private InstantDiCtrl instantDataInput;
        private InstantDoCtrl instantDataOutput;

        protected static readonly ILog logger = LogManager.GetLogger(typeof(PCIE1762H));

        private string deviceDescription = "PCIE-1762H,BID#0";

        private ChannelConfiguration config;

        public enum ChannelsEnum
        {
            DO = 0,
            DI = 1,
        }

        public PCIE1762H()
            : base(logger)
        {
            instantDataInput = new InstantDiCtrl();
            instantDataOutput = new InstantDoCtrl();
            instantDataInput.SelectedDevice = new DeviceInformation(deviceDescription);
            instantDataOutput.SelectedDevice = new DeviceInformation(deviceDescription);

            Config().SetChannels(2, 2);
            Layout();
        }

        #region Public method

        public void Active(int output, int waitTime = 0)
        {
            DO.OnWait(waitTime, output);
        }

        public void PulseActive(int pulseTime, int output)
        {
            DO.PulseOn(pulseTime, output);
        }

        public void PulseDesactive(int pulseTime, int output)
        {
            DO.PulseOff(pulseTime, output);
        }

        public void Desactive(int output, int waitime = 0)
        {
            DO.OffWait(waitime, output);
        }

        public bool InputDetection(int input)
        {
            return DI[input];
        }

        public bool InputWaitOn(int input, int timeOut)
        {
            return DI.WaitOn(input, timeOut);
        }

        public bool InputWaitOff(int input, int timeOut)
        {
            return DI.WaitOff(input, timeOut);
        }

        #endregion

        public ChannelConfiguration Config()
        {
            config = config ?? new ChannelConfiguration(this);

            return config;
        }

        public void Layout(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                ApplyDefaultLayout();
        }

        public void ApplyDefaultLayout()
        {
            Config()
                .SetChannelAsOutput((int)ChannelsEnum.DO)
                .SetChannelAsInput((int)ChannelsEnum.DI)
                .Apply();
        }

        protected override void InternalApplyConfig(IIOBaseConfiguration config)
        {
            this.config = config as ChannelConfiguration;

            foreach (IOChannel channel in this.config.Channels)
                foreach (var port in channel.Ports)
                {
                    port.Data = 0;

                    if (port.IOType == IOType.Output)
                        InternalWritePort(port);
                    else
                        InternalReadPort(port);
                }
        }

        protected override T InternalRead<T>(IOType type, int num)
        {
            var port = GetPort(type, num);
            int bit = ((num - port.MinBit) % port.BitsPerPort);
            var value = InternalReadPort(port);

            bool result = (value & (1 << bit)) > 0;

            port[bit] = result;

            return (T)Convert.ChangeType(result, typeof(T));
        }

        protected override void InternalWrite<T>(int num, T value)
        {
            var port = GetPort(IOType.Output, num);
            int bit = ((num - port.MinBit) % port.BitsPerPort);

            port[bit] = Convert.ToBoolean(value);

            InternalWritePort(port);
        }

        protected byte InternalReadPort(IOPort port)
        {
            byte portData = 0;

            ErrorCode err =
                port.IOType == IOType.Output ?
                    instantDataOutput.Read(port.RelativeNumPort, out portData) :
                    instantDataInput.Read(port.RelativeNumPort, out portData);
            if (err != ErrorCode.Success)
            // HandleError(err);
                throw new InstrumentException(111);

            return (byte)portData;
        }

        protected void InternalWritePort(IOPort port)
        {
            ErrorCode err = instantDataOutput.Write(port.RelativeNumPort, port.Data);
            if (err != ErrorCode.Success)
                throw new InstrumentException(111);
        }

        internal IOPort GetPort(IOType type, int num)
        {
            return this.config.Channels[type == IOType.Input ? (int)ChannelsEnum.DI : (int)ChannelsEnum.DO].Ports[(int)(num / 8)];
        }

        public override void Dispose()
        {
            instantDataInput.Dispose();
            instantDataOutput.Dispose();

            base.Dispose();
        }
    }
}