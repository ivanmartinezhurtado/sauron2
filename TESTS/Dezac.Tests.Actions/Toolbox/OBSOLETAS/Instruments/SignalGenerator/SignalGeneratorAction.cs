﻿using Dezac.Tests.Services;
using Instruments.Auxiliars;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(SignalGeneratorActionDescription))]
    

    public class SignalGeneratorAction : ActionBase
    {
        [DefaultValue(1)]
        public int Channel { get; set; }
        public SignalFunction Function { get; set; }
        [DefaultValue(50d)]
        public double Frequency { get; set; }
        [DefaultValue(1d)]
        public double? Amplitude { get; set; }
        [DefaultValue(UnitVoltage.VRMS)]
        public UnitVoltage UnitVoltage { get; set; }
        public double? MinVoltage { get; set; }
        public double? MaxVoltage { get; set; }
        public double? VoltageOffset { get; set; }
        public double Phase { get; set; }
        public double? DutyCycle { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            try
            {
                tower.SignalGenerator.SetSignal(new SignalInfo
                {
                    Channel = Channel,
                    Function = Function,
                    Frequency = Frequency,
                    Amplitude = Amplitude,
                    VoltageOffset = VoltageOffset,
                    MinVoltage = MinVoltage,
                    MaxVoltage = MaxVoltage,
                    Phase = Phase,
                    DutyCycle = DutyCycle,
                    UnitVoltage = UnitVoltage
                }, true);

            } catch (Exception ex)
            {
                if (tower.SignalGenerator is IDisposable)
                    ((IDisposable)tower.SignalGenerator).Dispose();

                throw ex;
            }
        }
    }

    public class SignalGeneratorActionDescription : ActionDescription
    {
        public override string Name { get { return "Signal Generator"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.ksicon16; } }
    }
}