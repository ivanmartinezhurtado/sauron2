﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.10)]
    public class RECMAX_CVM1Test : RECMAX_CVMTestBase
    {
        public override void TestAdjustCVM(double PF = 0, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Tower.PowerSourceIII.Tolerance = 0.05;

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, PF, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue() { L1 = adjustVoltage, L2 = 0, L3 = 0 }, new TriLineValue() { L1 = adjustCurrent, L2 = 0, L3 = 0 }, 50, powerFactor, DesfaseValue, true);

            var Gains = new List<AdjustValueDef>();
            Gains.Add(new AdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            Gains.Add(new AdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            Gains.Add(new AdjustValueDef(Params.GAIN_KW.L1.Ajuste.Name, 0, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var currentReadings = Tower.PowerSourceIII.ReadCurrent();

            var adjustCurrentValues = currentReadings.L1;

            var measureGains = Cvm.CalculateAdjustMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltage, adjustCurrentValues, transformationRelation,
                (value) =>
                {

                    Gains[0].Value = value.Voltage.L1;
                    Gains[1].Value = value.Current.L1;
                    Gains[2].Value = value.Power.L1;

                    return !HasError(Gains, false).Any();

                });

            foreach (var res in Gains)
                res.AddToResults(Resultado);

            if (HasError(Gains, false).Any())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(Gains, false).FirstOrDefault().Name));

            Cvm.WriteMeasureGains(measureGains.Item2);
        }

        public override void TestGapAdjustCVM(double PF = 60, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var adjustCurrentOffsets = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 0, ParamUnidad.A);
            var realAdjustCurrents = adjustCurrent + adjustCurrentOffsets;
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, PF, ParamUnidad.Grados);
           
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue() { L1 = adjustVoltage, L2 = 0, L3 = 0 }, new TriLineValue() { L1 = realAdjustCurrents, L2 = 0, L3 = 0 }, 50, angleGap, DesfaseValue, true);

            var Defs = new AdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, 0, Params.GAIN_DESFASE.L1.Ajuste.Min(), Params.GAIN_DESFASE.L1.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var powerFactor = Math.Cos((angleGap.ToRadians()));

            var voltageRef = Tower.PowerSourceIII.ReadVoltage();
            var currentRef = Tower.PowerSourceIII.ReadCurrent();

            currentRef.L1 -= adjustCurrentOffsets *= transformationRelation;

            var powerReference = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians()));

            var offsetGains = Cvm.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, powerReference, powerFactor, transformationRelation,
                (value) =>
                {
                    Defs.Value = value.L1;

                    return Defs.IsValid();
                });

            Defs.AddToResults(Resultado);

            Assert.IsTrue(offsetGains.Item1, Error().UUT.MEDIDA.MARGENES("Error valor de ajuste de ganancia del desfase fuera de márgenes"));
      
            Cvm.WritePhaseGapGains(offsetGains.Item2);
        }

        public override void TestVerificationCVM(double PF = 60, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, PF, ParamUnidad.Grados);

            //Tower.PowerSourceIII.ApplyAndWaitStabilisation(new TriLineValue { L1 = adjustVoltage, L2 = 0, L3 = 0 }, new TriLineValue { L1 = adjustCurrent, L2 = 0, L3 = 0 }, 50, angleGap, DesfaseValue, true);

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new AdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, 0, toleranciaI, ParamUnidad.A));           

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varsList = new List<double>() {
                      vars.Phases.L1.ActivePower,
                      vars.Phases.L1.Voltage,
                      vars.Phases.L1.Current};

                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = Tower.PowerSourceIII.ReadVoltage();
                    var currentRef = Tower.PowerSourceIII.ReadCurrent();

                    currentRef.L1 = (currentRef.L1) * transformationRelation;

                    double PowerActive = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians()));                       
                    

                    var varsList = new List<double>() {
                        PowerActive,
                        voltageRef.L1,
                        currentRef.L1};

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);
        }

        public override void TestHardwareVerificationCVM()
        {
            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.THD_V.L1.Name, 0, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new AdjustValueDef(Params.THD_I.L1.Name, 0, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));

            TestMeasureBase(defs,
                (step) =>
                {
                    var THD = Cvm.ReadTotalHarmonicDistorsion();
                    return new double[] { THD.VoltageL1, THD.CurrentL1 };
                }, 2, 10, 1000);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.Name, 0, Params.FREQ.Null.Verificacion.Min(), Params.FREQ.Null.Verificacion.Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(defsFreq,
              (step) =>
              {
                  var freq = Cvm.ReadFrequency();
                  return freq.Instantaneous / 100D;
              }, 2, 10, 750);
        }

        public override void TestTrigger()
        {
            Tower.ActiveCurrentCircuit(true, true, true);
            Tower.IO.DO.On(CORRIENTE_DIRECTA);
           
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 0, L3 = 0 }, new TriLineValue { L1 = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.15, ParamUnidad.A), L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);

            Rec.WriteOperatingMode(RECMAX.OperatingMode.NORMAL);

            Rec.WriteTriggerCurrent(2);
            Rec.WriteTriggerTime(0);

            ///
            Delay(100, "Cambiando de escala");
            Tower.ActiveCurrentCircuit(false, true, true);
            Delay(2100, "Estabilizando medida del equipo");

            var currentLeakVerif = new AdjustValueDef(Params.I.Other("LEAK").Verificacion.Name, 0, 0, 0, 0, Params.I.Null.TestPoint("LEAK").Tol(), ParamUnidad.s);

            TestCalibracionBase(currentLeakVerif,
               () =>
               {
                   var reading = Rec.ReadLeakCurrent();
                   return reading;
               },
               () =>
               {
                   return Tower.PowerSourceIII.ReadCurrent().L1;
               }, 1, 2, 1050, "Error, corriente medida de fuga fuera de márgenes");

            Tower.ActiveCurrentCircuit(true, true, true);
            Delay(1800, "");

            var current = Rec.ReadLeakCurrent();
            logger.InfoFormat("Corriente leída mientras esta cortocircuitada en la torre : {0}", current);

            Rec.WriteTriggerCurrent(0);
            

            SamplerWithCancel((step) =>
            {
                if (step == 3)
                {
                    Rec.FlagTest();
                    Rec.WriteOutputs(RECMAX.Outputs.REARM, RECMAX.Outputs.DISPLAY_ACTIVATION);
                    Delay(3000, "Rearmando equipo");
                    Rec.WriteOperatingMode(RECMAX.OperatingMode.NORMAL);
                }

                return Tower.IO.DI[DETECTION_MCB_L1];
            }, "Error, no se ha detectado la continuidad del magnetotermico en L1", 6, 1000, 0, false, false);


            this.InParellel(
           () =>
           {
               Tower.MUX.Reset();
               Tower.MUX.Connect2((int)InputMuxEnum.IN9, Outputs.FreCH1, (int)InputMuxEnum.IN2, Outputs.FreCH2);

               Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
               Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

               Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
               Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
           });

            var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 5000, HP53131A.Channels.CH1, () =>
                    {
                        Delay(50, "");

                        if (!Tower.IO.DI[DETECTION_OUT_BREAK_NC] && Tower.IO.DI[DETECTION_OUT_BREAK_NO])
                            logger.InfoFormat("El equipo ya está disparado antes de sacar la corriente de la torre.");
                        
                        Tower.ActiveCurrentCircuit(false, true, true);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw new Exception(string.Format("Error, el equipo no ha disparado"));
                }
            }, 0, 1, 1000);

            Tower.ActiveCurrentCircuit(true, true, true);
        }
    }   
}
