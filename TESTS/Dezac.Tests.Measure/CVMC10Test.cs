﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using FileHelpers;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.49)]
    public class CVMC10TEST : TestBase
    {

        #region Variables

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }
        private CVMC10 cvm;
        public CVMC10 Cvm
        {
            get
            {
                if (cvm == null)
                    cvm = new CVMC10(Comunicaciones.SerialPort);

                return cvm;
            }
        }
        internal string versionFirmware = "";
        internal bool tipoAlimentacionDC = false;
        internal double transformationRelation;
        internal double current;
        internal string CAMERA_IDS;

        internal byte OUTPUT_PISTON_EXPULSION_EQUIPO = 12;
        internal byte OUTPUT_NEUTRO = 15;
        private byte OUTPUT_TARIFA_1 = 17;
        private byte OUTPUT_TARIFA_2 = 18;
        internal byte KEY_BLOQUE = 8;
        private byte KEY_2_ACTUATOR = 9;
        private byte KEY_3_ACTUATOR = 10;
        private byte KEY_1_ACTUATOR = 11;

        private byte INPUT_DEVICE_OUTPUT_1 = 11;
        private byte INPUT_DEVICE_OUTPUT_2 = 12;
        private byte INPUT_DEVICE_OUTPUT_3 = 9;
        private byte INPUT_DEVICE_OUTPUT_4 = 10;

        public TriLineValue DesfaseValue { get; set; }

        internal enum LeadsInputsOutputs
        {
            IN_VI = 30,
            OUT_VI = 5,
            IN_IO = 31,
            OUT_IO = 6,
            IN_FIJACION = 32,
            OUT_FIJACION = 7,
        }

        private const int TECLADO_INICIALIZADO = 8;
        #endregion

        public virtual void TestInitialization(int portCVM = 0)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            Cvm.Modbus.PortCom = portCVM;
            Cvm.Modbus.PerifericNumber = Convert.ToByte(1);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            tipoAlimentacionDC = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad).Contains("Vdc");

            if (tipoAlimentacionDC)
                SamplerWithCancel((p) =>
                {
                    Tower.LAMBDA.Initialize();
                    Tower.LAMBDA.ApplyOff();
                    return true;
                }, "Error. No se puede establecer comunicación con la fuente DC LAMBDA", 2, 1500, 0, false, false);
            else
                Tower.Chroma.ApplyOff();

            Tower.PowerSourceIII.ApplyOff();

            DesfaseValue = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };

            Tower.IO.DO.OffWait(500, OUTPUT_PISTON_EXPULSION_EQUIPO);
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);

            tower.PowerSourceIII.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.1, ParamUnidad.PorCentage);
            tower.PowerSourceIII.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 20000, ParamUnidad.ms);

        }

        public void TestCommunications()
        {
            Delay(4000, "Espera para no intentar comunicar con el equipo mientras arranca");
            string firmwareVersion = "";
            int retrieCom = 0;

            SamplerWithCancel((r) =>
            {
                if (r > 0)
                {
                    ResetpowerSource();
                    retrieCom++;
                }

                try
                {
                    Cvm.Modbus.BaudRate = 9600;
                    SamplerWithCancel((p) =>
                    {
                        firmwareVersion = Cvm.ReadFirmwareVersion().CToNetString();
                        return true;
                    }, "Error: No se puede comunicar con el equipo.", 3, 500, 0, false, false);
                }
                catch
                {
                    Logger.Warn("Cambio de comunicaciones a 19200, recuperación equipo.");
                    Cvm.Modbus.BaudRate = 19200;
                    Delay(1000, "");
                    SamplerWithCancel((p) =>
                    {
                        firmwareVersion = Cvm.ReadFirmwareVersion().CToNetString();
                        return true;
                    }, "Error: No se puede comunicar con el equipo.", 3, 500, 0, false, false);
                    Cvm.WriteComunications();
                    Cvm.Modbus.BaudRate = 9600;
                    ResetpowerSource();
                }
                return true;
            }, "Error de comunicaciones", 2, 1000, 0, false, false);

            Resultado.Set("COMUNICACIONES_REINICIO_EQUIPO", retrieCom, ParamUnidad.SinUnidad);

            Cvm.FlagTest();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);
        }

        public virtual void TestDefaultParameters()
        {
            var hardwareVector = new CVMC10.HardwareVector();
            hardwareVector.Comunication = VectorHardware.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rs232 = VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.In_Medida = VectorHardware.GetString("MEDIDA_NEUTRO", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Flex = VectorHardware.GetString("FLEX", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele1 = VectorHardware.GetString("RELE1", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele2 = VectorHardware.GetString("RELE2", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele3 = VectorHardware.GetString("RELE3", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele4 = VectorHardware.GetString("RELE4", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "5A", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "300V", ParamUnidad.SinUnidad);
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "CVM-C10", ParamUnidad.SinUnidad);

            SetVariable("VectorHardware", hardwareVector);

            Cvm.WriteHardwareVector(hardwareVector);

            Delay(500, "Espera ");

            Cvm.WriteComunications();
            Cvm.WritePassword((ushort)Configuracion.GetDouble("PASSWORD", 1234, ParamUnidad.SinUnidad));

            var bloqueoSetup = Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad) == "NO" ? false : true;

            Cvm.WriteSetupLock(bloqueoSetup);
            Cvm.WriteTransformationRatio();
            current = Configuracion.GetDouble("SECONDARY_CURRENT", 1, ParamUnidad.A);

            int imputCurrentPrimary = 1;

            if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary5A)
                imputCurrentPrimary = 5;
            else if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary2A)
                imputCurrentPrimary = 2;
            else if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary1A)
                imputCurrentPrimary = 1;

            transformationRelation = imputCurrentPrimary / current;

            Cvm.WriteMaximumDemandConfiguration();

            Delay(500, "Espera ");

            var voltageGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 3625, ParamUnidad.Puntos);
            var currentGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO", 11500, ParamUnidad.Puntos);
            var activepowerGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KW + "_DEFECTO", 5436, ParamUnidad.Puntos);
            var gainGapDefault = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_DESFASE + "_DEFECTO", 1000, ParamUnidad.Puntos);

            Cvm.WriteDefaultGains((ushort)voltageGain, (ushort)currentGain, (ushort)activepowerGain, (ushort)gainGapDefault);
            Cvm.WriteInitialMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));
            Cvm.WriteScreenSelection(Convert.ToInt32(0x0003FFFF));
            Cvm.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumBastidor.Value));
            Cvm.WriteAllAlarmsDefault();

            if (Configuracion.GetString("CONFIG_ALARM", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                Dictionary<CVMC10.AlarmRegisters, string> alarmConfigurations = new Dictionary<CVMC10.AlarmRegisters, string>();

                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_1, Configuracion.GetString("ALARM_REL1", "PARAM_CODE=200;HI=170;LO=90;DELAYON=60;DELAYOFF=60;HYST=0;LATCH=0;STATE=NA", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_2, Configuracion.GetString("ALARM_REL2", "NO", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_3, Configuracion.GetString("ALARM_TR1", "PARAM_CODE=200;HI=170;LO=90;DELAYON=60;DELAYOFF=60;HYST=0;LATCH=0;STATE=NA", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_4, Configuracion.GetString("ALARM_TR2", "NO", ParamUnidad.SinUnidad));

                foreach (KeyValuePair<CVMC10.AlarmRegisters, string> alarm in alarmConfigurations)
                {
                    if (!alarm.Value.Contains("NO"))
                    {
                        var fileHelper = new FileHelperEngine<CVMC10_File_Alarms>();
                        fileHelper.ErrorMode = ErrorMode.IgnoreAndContinue;
                        var alarmConfiguration = fileHelper.ReadString(alarm.Value);

                        var alarmConfig = new CVMC10.AlarmConfiguration()
                        {
                            variableNumber = Convert.ToUInt16(alarmConfiguration[0].PARAM_CODE),
                            maxValue = Convert.ToInt32(alarmConfiguration[0].HI),
                            minValue = Convert.ToInt32(alarmConfiguration[0].LO),
                            alarmDelay = Convert.ToUInt16(alarmConfiguration[0].DELAYON),
                            alarmOffDelay = Convert.ToUInt16(alarmConfiguration[0].DELAYOFF),
                            histeresis = Convert.ToUInt16(alarmConfiguration[0].HYST),
                            latching = Convert.ToUInt16(alarmConfiguration[0].LATCH),
                            alarmLogic = Convert.ToUInt16(alarmConfiguration[0].STATE)
                        };

                        Cvm.WriteAlarm(alarm.Key, alarmConfig);
                    }

                    Resultado.Set(alarm.Key.ToString(), alarm.Value, ParamUnidad.SinUnidad);
                }
            }

            Delay(500, "Espera ");

            Cvm.WriteSynchronism(10000);

            ResetpowerSource();

            SamplerWithCancel((p) =>
            {
                Cvm.FlagTest();
                return true;
            }, "Error. No se puede establecer comunicación con el equipo despues del reset", 10, 500);

            var sync = Cvm.ReadSynchronism();
            Assert.AreEqual(sync, (ushort)10000, Error().UUT.COMUNICACIONES.GRABACION_SINCRONISMO("Error. El valor de sincronismo no grabado correctamente"));

            var vector = Cvm.ReadHardwareVector();
            var vectorHardwareBBDD = GetVariable<CVMC10.HardwareVector>("VectorHardware", vector);

            vector.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vector.VectorHardwareBoolean, vectorHardwareBBDD.VectorHardwareBoolean, Error().UUT.CONFIGURACION.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el grabado"));
        }

        public void TestDisplay()
        {
            Cvm.FlagTest();

            var displays = new Dictionary<string, CVMC10.DisplayRegisters>();

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            displays.Add("DISPLAY_ALL.png", CVMC10.DisplayRegisters.DISPLAY_ALL);

            foreach (KeyValuePair<string, CVMC10.DisplayRegisters> display in displays)
            {
                Cvm.DisplaySwitch(display.Value);

                Delay(2000, "Espera display");

                this.TestHalconMatchingProcedure(CAMERA_IDS, string.Format("CVMC10_{0}", display.Value.ToString()), display.Value.ToString(), "CVMC10");
            }
        }

        public void TestLeds()
        {
            Cvm.FlagTest();

            var ledsValue = new Dictionary<string, CVMC10.LedRegisters>();

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            ledsValue.Add("LED_ALARMA.png", CVMC10.LedRegisters.LED_ALARMA);
            ledsValue.Add("LED_CPU.png", CVMC10.LedRegisters.LED_CPU);
            ledsValue.Add("LED_TECLA.png", CVMC10.LedRegisters.LED_TECLA);

            Cvm.WriteLeds(CVMC10.LedRegisters.LED_ALARMA, true);
            Cvm.WriteLeds(CVMC10.LedRegisters.LED_CPU, true);
            Cvm.WriteLeds(CVMC10.LedRegisters.LED_TECLA, true);

            Delay(2000, "Espera leds");

            this.TestHalconFindLedsProcedure(CAMERA_IDS, "CVMC10_LEDS_ON", "LEDS_ON", "CVMC10");

            Cvm.WriteLeds(CVMC10.LedRegisters.LED_ALARMA, false);
            Cvm.WriteLeds(CVMC10.LedRegisters.LED_CPU, false);
            Cvm.WriteLeds(CVMC10.LedRegisters.LED_TECLA, false);

            Delay(2000, "Espera leds");

            this.TestHalconFindLedsProcedure(CAMERA_IDS, "CVMC10_LEDS_OFF", "LEDS_OFF", "CVMC10");
        }

        //[TestPoint(Required = true)]
        public void TestInitTeclado()
        {
            var sensibilityRead = 0;
            SamplerWithCancel((p) =>
            {
                try
                {
                    sensibilityRead = Cvm.ReadSensibilityKeyboard();
                    return true;
                }
                catch (Exception)
                {
                    ResetpowerSource();
                    return false;
                }
            }, "Error. No se puede establecer comunicación con el equipo al enviar Inicialización del teclado");


            if (sensibilityRead != TECLADO_INICIALIZADO)
                SamplerWithCancel((p) =>
                {
                    Cvm.WriteKeyboardConfiguration();

                    Delay(4000, "Espera tiempo de reset automatico del equipo");

                    SamplerWithCancel((t) =>
                    {
                        try
                        {
                            Cvm.ReadFirmwareVersion();
                            return true;
                        }
                        catch (Exception)
                        {
                            ResetpowerSource();
                            return false;
                        }
                    }, "Error. No se puede establecer comunicación con el equipo al enviar Inicialización del teclado");

                    SamplerWithCancel((r) =>
                    {
                        try
                        {
                            sensibilityRead = Cvm.ReadSensibilityKeyboard();
                            return true;
                        }
                        catch (Exception)
                        {
                            ResetpowerSource();
                            return false;
                        }
                    }, "Error. No se puede establecer comunicación con el equipo al enviar Inicialización del teclado");

                    return sensibilityRead == TECLADO_INICIALIZADO;
                }, "Error no se ha podido inicializar el teclado", 5, 2000, 0);
        }

        public void TestTeclado()
        {
            Cvm.FlagTest(false);

            Tower.IO.DO.Off(KEY_1_ACTUATOR, KEY_2_ACTUATOR, KEY_3_ACTUATOR);

            Delay(1500, "Esperamos para la el test de leds y teclado");

            Tower.IO.DO.On(KEY_BLOQUE);

            var keyboardValue = new Dictionary<byte, CVMC10.KeyboardKeys>();
            keyboardValue.Add(KEY_1_ACTUATOR, CVMC10.KeyboardKeys.KEY_1);
            keyboardValue.Add(KEY_2_ACTUATOR, CVMC10.KeyboardKeys.KEY_2);
            keyboardValue.Add(KEY_3_ACTUATOR, CVMC10.KeyboardKeys.KEY_3);

            var reposo = (ushort)Cvm.ReadKeys();
            if (reposo != (ushort)CVMC10.KeyboardKeys.NO_KEY)
                Error().UUT.HARDWARE.KEYBOARD("detecta tecla pulsada cuando no la hay").Throw();

            foreach (KeyValuePair<byte, CVMC10.KeyboardKeys> input in keyboardValue)
            {
                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.On(input.Key);

                    Delay(200, "Espera de la detección de teclas");

                    var sampler = Sampler.Run(5, 500, (step) =>
                    {
                        var value = (ushort)Cvm.ReadKeys();
                        step.Cancel = value == (ushort)input.Value;
                    });

                    Tower.IO.DO.Off(input.Key);

                    if (sampler.Canceled)
                        return true;

                    return false;

                }, string.Format("Error. no se detecta la tecla {0} activada", input.Value.ToString()), 1, 500);

                Resultado.Set(input.Value.ToString().Replace("_", " "), "OK", ParamUnidad.SinUnidad);
            }

            SamplerWithCancel((p) =>
            {
                var value = (ushort)Cvm.ReadKeys();
                return value == (ushort)CVMC10.KeyboardKeys.NO_KEY;
            }, "Error. el equipo detecta tecla pulsada cuando no la hay despues de probarlas", 5, 200);

            Tower.IO.DO.Off(KEY_BLOQUE);
        }

        public void TestRelay()
        {
            Cvm.FlagTest();

            var relaysValues = new Dictionary<byte, CVMC10.OutputRegisters>();

            var vector = Cvm.ReadHardwareVector();

            if (vector.Rele1)
                relaysValues.Add(INPUT_DEVICE_OUTPUT_1, Device.Measure.CVMC10.OutputRegisters.OUT_1);

            if (vector.Rele2)
                relaysValues.Add(INPUT_DEVICE_OUTPUT_2, Device.Measure.CVMC10.OutputRegisters.OUT_2);

            if (vector.Rele3)
                relaysValues.Add(INPUT_DEVICE_OUTPUT_3, Device.Measure.CVMC10.OutputRegisters.OUT_3);

            if (vector.Rele4)
                relaysValues.Add(INPUT_DEVICE_OUTPUT_4, Device.Measure.CVMC10.OutputRegisters.OUT_4);

            foreach (KeyValuePair<byte, CVMC10.OutputRegisters> relay in relaysValues)
            {
                SamplerWithCancel((p) =>
                {
                    Cvm.WriteOutputs(relay.Value, true);
                    Delay(200, "Espera para el rateo de los reles");
                    Cvm.WriteOutputs(relay.Value, false);
                    return true;
                }, "Error al realizar el rateo del rele", 10, 200);

                Cvm.WriteOutputs(relay.Value, true);

                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[relay.Key];
                }, string.Format("Error. no se ha activado la {0}", relay.Value.ToString().Replace("_", " "), 10, 500));


                Cvm.WriteOutputs(relay.Value, false);

                SamplerWithCancel((p) =>
                {
                    return !Tower.IO.DI[relay.Key];
                }, string.Format("Error. no se ha desactivado la {0}", relay.Value.ToString().Replace("_", " ")), 10, 500);

                Resultado.Set(relay.Value.ToString().Replace("_", " "), "OK", ParamUnidad.SinUnidad);
            }
        }

        public virtual void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            if (Cvm.ReadHardwareVector().In_Medida)
                Tower.IO.DO.On(OUTPUT_NEUTRO);

            var offsetsV = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = Cvm.ReadAllVariables();
                    return new double[] { vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                        vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current };

                }, delFirts, samples, timeInterval);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 0, 0, DesfaseValue, true);

            var defsFreq = new AdjustValueDef { Name = Params.FREQ.Null.Ajuste.Name, Min = Params.FREQ.Null.Ajuste.Min(), Max = Params.FREQ.Null.Ajuste.Max(), Unidad = ParamUnidad.Hz };

            Cvm.FlagTest();

            int Sincronismo = 0;
            int i = 0;

            TestMeasureBase(defsFreq,
              (step) =>
              {
                  i++;
                  Cvm.WriteSynchronism(10000);
                  Delay(1500, "Esperando grabación factor de defecto de auste de sincronismo...");
                  var freq = Cvm.ReadAllVariables().ThreePhase.Frequency;
                  Sincronismo = (int)(50000000 / (freq * 100)) + i;
                  Cvm.WriteSynchronism((ushort)Sincronismo);
                  Delay(1500, "Esperando grabación factor calculado de sincronismo...");
                  return Cvm.ReadAllVariables().ThreePhase.Frequency;

              }, 0, 5, timeInterval);

            Resultado.Set(Params.SYNC.Null.Ajuste.Name, Sincronismo, ParamUnidad.Puntos);

            Delay(2000, "Esperando grabación factor calculado de auste de sincronismo...");

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            if (Cvm.ReadHardwareVector().In_Medida)
                crucePistasI.Neutro = new AdjustValueDef(Params.I.LN.CrucePistas.Name, 0, 0, 0, ConsignasI.L1, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A);

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varslist = new List<double>(){vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                     vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varslist.Add(vars.ThreePhase.NeutralCurrent);

                    return varslist.ToArray();

                }, delFirts, samples, timeInterval);
        }

        public virtual void TestAdjust(double PF = 0, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = new TriLineValue() { L1 = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V), L2 = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V), L3 = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V) };
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, PF, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, powerFactor, DesfaseValue, true);

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            if (Cvm.ReadHardwareVector().In_Medida)
                triGains[1].Neutro = new AdjustValueDef(Params.GAIN_I.LN.Ajuste.Name, 0, Params.GAIN_I.LN.Ajuste.Min(), Params.GAIN_I.LN.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            triGains.Add(new TriAdjustValueDef(Params.GAIN_KW.L1.Ajuste.Name, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.L23.Ajuste.Name, Params.GAIN_V.L31.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var currentReadings = Tower.PowerSourceIII.ReadCurrent();

            var adjustCurrentValues = new CVMC10.TriDouble() { L1 = currentReadings.L1 - adjustCurrentOffsets.L1, L2 = currentReadings.L2 - adjustCurrentOffsets.L2, L3 = currentReadings.L3 - adjustCurrentOffsets.L3 };

            var measureGains = Cvm.CalculateAdjustMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltage.L1, adjustCurrentValues, transformationRelation,
                (value) =>
                {

                    triGains[0].L1.Value = value.Voltage.L1;
                    triGains[0].L2.Value = value.Voltage.L2;
                    triGains[0].L3.Value = value.Voltage.L3;

                    triGains[1].L1.Value = value.Current.L1;
                    triGains[1].L2.Value = value.Current.L2;
                    triGains[1].L3.Value = value.Current.L3;

                    if (triGains[1].Neutro != null)
                        triGains[1].Neutro.Value = value.neutralCurrent.Value;

                    triGains[2].L1.Value = value.Power.L1;
                    triGains[2].L2.Value = value.Power.L2;
                    triGains[2].L3.Value = value.Power.L3;

                    triGains[3].L1.Value = value.Compound.L12;
                    triGains[3].L2.Value = value.Compound.L23;
                    triGains[3].L3.Value = value.Compound.L31;

                    return !HasError(triGains, false).Any();

                });

            foreach (var res in triGains)
                res.AddToResults(Resultado);

            if (HasError(triGains, false).Any())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            Cvm.WriteMeasureGains(measureGains.Item2);

        }

        public virtual void TestGapAdjust(double PF = 60, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, PF, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleGap, DesfaseValue, true);

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.L1.Ajuste.Min(), Params.GAIN_DESFASE.L1.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var powerFactor = Math.Cos((angleGap.ToRadians()));

            var voltageRef = Tower.PowerSourceIII.ReadVoltage();
            var currentRef = Tower.PowerSourceIII.ReadCurrent();

            currentRef.L1 -= adjustCurrentOffsets.L1 *= transformationRelation;
            currentRef.L2 -= adjustCurrentOffsets.L2 *= transformationRelation;
            currentRef.L3 -= adjustCurrentOffsets.L3 *= transformationRelation;

            var powerReference = new CVMC10.TriDouble()
            {
                L1 = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians())),
                L2 = voltageRef.L2 * currentRef.L2 * Math.Cos((angleGap.ToRadians())),
                L3 = voltageRef.L3 * currentRef.L3 * Math.Cos((angleGap.ToRadians())),
            };

            var offsetGains = Cvm.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, powerReference, powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                },
                (activePowerOffset) => (activePowerOffset.L1 > 0 && activePowerOffset.L2 > 0 && activePowerOffset.L3 > 0));

            triDefs.AddToResults(Resultado);

            Assert.IsTrue(offsetGains.Item1, Error().UUT.AJUSTE.MARGENES("Error valor de ajuste de ganancia del desfase fuera de márgenes"));

            Cvm.WritePhaseGapGains(offsetGains.Item2);
        }

        //[TestPoint(Depends = "TestGapAdjust")]
        public virtual void TestVerification(double PF = 60, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, PF, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleGap, DesfaseValue, true);

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, Params.KW.Null.Verificacion.Tol(), ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.KVAR.L1.Verificacion.Name, 0, 0, 0, Params.KVAR.Null.Verificacion.Tol(), ParamUnidad.Var));
            defs.Add(new TriAdjustValueDef(Params.KVA.L1.Verificacion.Name, 0, 0, 0, Params.KVA.Null.Verificacion.Tol(), ParamUnidad.VA));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A));
            if (Cvm.ReadHardwareVector().In_Medida)
                defs[4].Neutro = new AdjustValueDef(Params.I.LN.Verificacion.Name, 0, 0, 0, current, Params.I.Null.Verificacion.Tol(), ParamUnidad.A);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varsList = new List<double>() {
                        vars.Phases.L1.ActivePower, vars.Phases.L2.ActivePower, vars.Phases.L3.ActivePower,
                        vars.Phases.L1.CapacitiveReactivePower + vars.Phases.L1.InductiveReactivePower,
                        vars.Phases.L2.CapacitiveReactivePower + vars.Phases.L2.InductiveReactivePower,
                        vars.Phases.L3.CapacitiveReactivePower + vars.Phases.L3.InductiveReactivePower,
                        vars.Phases.L1.ApparentPower, vars.Phases.L2.ApparentPower, vars.Phases.L3.ApparentPower,
                        vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                        vars.Phases.L1.Current , vars.Phases.L2.Current, vars.Phases.L3.Current,
                    };

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(vars.ThreePhase.NeutralCurrent);

                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = Tower.PowerSourceIII.ReadVoltage();
                    var currentRef = Tower.PowerSourceIII.ReadCurrent();

                    currentRef.L1 = (currentRef.L1 - adjustCurrentOffsets.L1) * transformationRelation;
                    currentRef.L2 = (currentRef.L2 - adjustCurrentOffsets.L2) * transformationRelation;
                    currentRef.L3 = (currentRef.L3 - adjustCurrentOffsets.L3) * transformationRelation;

                    TriLineValue PowerActive = new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1 * Math.Cos(angleGap.ToRadians()),
                        L2 = voltageRef.L2 * currentRef.L2 * Math.Cos(angleGap.ToRadians()),
                        L3 = voltageRef.L3 * currentRef.L3 * Math.Cos(angleGap.ToRadians()),
                    };

                    TriLineValue PowerReactive= new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1 * Math.Sin(angleGap.ToRadians()),
                        L2 = voltageRef.L2 * currentRef.L2 * Math.Sin(angleGap.ToRadians()),
                        L3 = voltageRef.L3 * currentRef.L3 * Math.Sin(angleGap.ToRadians()),
                    };

                    TriLineValue PowerApparent= new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1,
                        L2 = voltageRef.L2 * currentRef.L2,
                        L3 = voltageRef.L3 * currentRef.L3,
                    };

                    var varsList = new List<double>() {
                        PowerActive.L1, PowerActive.L2, PowerActive.L3,
                        PowerReactive.L1, PowerReactive.L2, PowerReactive.L3,
                        PowerApparent.L1, PowerApparent.L2, PowerApparent.L3,
                        voltageRef.L1, voltageRef.L2, voltageRef.L3,
                        currentRef.L1, currentRef.L2 , currentRef.L3,
                    };

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(currentRef.L1);

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);
        }

        public virtual void TestHardwareVerification()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 60, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 50, angleGap, DesfaseValue, true);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.Name, 0, Params.FREQ.Null.Verificacion.Min(), Params.FREQ.Null.Verificacion.Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(defsFreq,
              (step) =>
              {
                  var freq = Cvm.ReadFrequency();
                  return freq.Instantaneous / 100D;
              }, 2, 10, 200);

            //Cambio crítico de frecuencia de la FLUKE, de INT a POWER LINE, sin inyectar V ni I al equipo
            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleGap, DesfaseValue, true);

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.THD_V.L1.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new TriAdjustValueDef(Params.THD_I.L1.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));

            TestMeasureBase(defs,
                (step) =>
                {
                    var THD = Cvm.ReadTotalHarmonicDistorsion();
                    return new double[] { THD.VoltageL1, THD.VoltageL2, THD.VoltageL3, THD.CurrentL1, THD.CurrentL2, THD.CurrentL3 };
                }, 2, 10, 1000);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestTarifa()
        {
            Tower.IO.DO.Off(OUTPUT_TARIFA_1, OUTPUT_TARIFA_2);

            SamplerWithCancel((p) =>
            {
                return Cvm.ReadFare() == (int)CVMC10.Tarifa.TARIFA_1;
            }, "Error. No se detecta la tarifa 1", 10, 300);

            Tower.IO.DO.On(OUTPUT_TARIFA_1);

            Resultado.Set("TARIFA 1", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                return Cvm.ReadFare() == (int)CVMC10.Tarifa.TARIFA_2;
            }, "Error. No se detecta la tarifa 2", 10, 300);

            Tower.IO.DO.Off(OUTPUT_TARIFA_1);
            Tower.IO.DO.On(OUTPUT_TARIFA_2);

            SamplerWithCancel((p) =>
            {
                return Cvm.ReadFare() == (int)CVMC10.Tarifa.TARIFA_3;
            }, "Error. No se detecta la tarifa 3", 10, 300);

            Resultado.Set("TARIFA 2", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.Off(OUTPUT_TARIFA_2);
        }

        public virtual void TestCustomization()
        {
            Cvm.FlagTest();

            Cvm.WriteBacNetID(TestInfo.BacNetID.Value);

            Resultado.Set("BACNET_ID", TestInfo.BacNetID.Value, ParamUnidad.SinUnidad);

            Cvm.WriteBacNetMAC(2);

            var convenioMedida = Configuracion.GetDouble("CONVENIO_MEDIDA", 0, ParamUnidad.SinUnidad);

            var resultadoConvenioMedida = convenioMedida == 0 ? "CIRCUTOR" : convenioMedida == 1 ? "IEC" : "IEEE";

            Resultado.Set("CONVENIO_MEDIDA", resultadoConvenioMedida, ParamUnidad.SinUnidad);

            Cvm.WriteMedidaConveni((ushort)convenioMedida);

            Cvm.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));

            Delay(600, "Escribimos Numero de Serie");

            var currentComs = Cvm.ReadComunications();

            var baudRate = Configuracion.GetString("BAUD_RATE_FINAL", "9600", ParamUnidad.SinUnidad);
            var baudRateEnum = baudRate.Contains("9600") ? CVMC10.BaudRate._9600 : baudRate.Contains("19200") ? CVMC10.BaudRate._19200 : baudRate.Contains("38400") ? CVMC10.BaudRate._38400 : CVMC10.BaudRate.NULL;

            if (baudRateEnum == CVMC10.BaudRate.NULL)
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("BAUD_RATE_FINAL").Throw();

            currentComs.Velocidad = (ushort)baudRateEnum;

            Cvm.WriteComunications(currentComs);

            ResetpowerSource();

            Cvm.Modbus.BaudRate = Int32.Parse(baudRate);

            SamplerWithCancel((p) => { Cvm.FlagTest(); return true; }, "Error. El equipo no comunica al volver a encenderlo", 5, 1000);

            var comsClient = Cvm.ReadComunications();
            Assert.AreEqual("BAUD_RATE_CLIENT", (CVMC10.BaudRate)comsClient.Velocidad, (CVMC10.BaudRate)currentComs.Velocidad, Error().UUT.COMUNICACIONES.BAUDRATE("Error. valor de la velocidad de comunicación final no coincide con el esperado"), ParamUnidad.SinUnidad);

            Cvm.CleanRegisters(CVMC10.ClearRegisters.BORRAR_TODO);

            var numserie = Cvm.ReadSerialNumber();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, TestInfo.NumSerie, numserie.ToString(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error. valor de numero de serie leido no coincide con el grabado"), ParamUnidad.SinUnidad);

            var vectorHardware = Cvm.ReadHardwareVector();

            Resultado.Set("COMUNICACIONES", vectorHardware.ComunicationString, ParamUnidad.SinUnidad);
            Resultado.Set("RS232", vectorHardware.Rs232String, ParamUnidad.SinUnidad);
            Resultado.Set("SHUNT", vectorHardware.ShuntString, ParamUnidad.SinUnidad);
            Resultado.Set("MEDIDA_NEUTRO", vectorHardware.In_MedidaString, ParamUnidad.SinUnidad);
            Resultado.Set("RELE1", vectorHardware.Rele1String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE2", vectorHardware.Rele2String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE3", vectorHardware.Rele3String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE4", vectorHardware.Rele4String, ParamUnidad.SinUnidad);
            Resultado.Set("ALIMENTACION", vectorHardware.PowerSupply.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_CORRIENTE", vectorHardware.InputCurrent.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_TENSION", vectorHardware.InputVoltage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("MODELO", vectorHardware.Modelo.ToString(), ParamUnidad.SinUnidad);

            Resultado.Set(ConstantsParameters.Identification.VECTOR_HARDWARE, vectorHardware.VectorHardwareTrama, ParamUnidad.SinUnidad);

            var vectorHardwareBBDD = GetVariable<CVMC10.HardwareVector>("VectorHardware", vectorHardware);

            vectorHardware.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vectorHardwareBBDD.VectorHardwareBoolean, vectorHardware.VectorHardwareBoolean, Error().UUT.CONFIGURACION.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el de la base de datos"));

            Delay(1000, "Esperamos borrado de energia");
        }

        public void TestFinish()
        {
            if (Cvm != null)
                Cvm.Dispose();

            if (Tower != null)
            {
                try
                {
                    Tower.ShutdownSources();
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("Excepcion Tower Shutdown in TestFinish by {0}", ex.Message);
                    Tower.Dispose();
                }

                if (Tower.IO != null)
                {
                    Tower.IO.DO.Off(OUTPUT_NEUTRO);

                    Tower.IO.DO.Off(KEY_BLOQUE);

                    Tower.IO.DO.OffWait(300, (byte)LeadsInputsOutputs.OUT_IO, (byte)LeadsInputsOutputs.OUT_VI);

                    Tower.IO.DO.OffWait(300, (byte)LeadsInputsOutputs.OUT_FIJACION);

                    Tower.IO.DO.OffWait(300, 9, 10, 11);

                    Tower.IO.DO.PulseOn(500, 12);
                }
                Tower.Dispose();
            }
        }

        //**************************************************************

        public void TestReworkInitialization(int portCVM = 0)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            Cvm.Modbus.PortCom = portCVM;
            Cvm.Modbus.PerifericNumber = Convert.ToByte(1);

            Cvm.FlagTest();
        }

        public void ReworkInitialMessage()
        {
            Cvm.WriteInitialMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));
        }

        public void ReworkUpgradeFirmware(byte portCVM = 1)
        {
            Cvm.Modbus.PortCom = portCVM;
            Cvm.Modbus.PerifericNumber = Convert.ToByte(1);

            SamplerWithCancel((p) =>
            {
                Cvm.FlagTest();
                return true;
            }, "Error. No se puede establecer comunicación con el equipo.", 5, 500);

            versionFirmware = Cvm.ReadFirmwareVersion().CToNetString();

            TestInfo.NumSerie = Cvm.ReadSerialNumber().ToString();

            Resultado.Set(ConstantsParameters.TestInfo.NUM_SERIE, TestInfo.NumSerie, ParamUnidad.SinUnidad);

            Cvm.Modbus.ClosePort();
        }

        public void ValidateUpgradeFirmware()
        {
            SamplerWithCancel((p) =>
            {
                Cvm.FlagTest();
                return true;
            }, "Error. No se puede establecer comunicación con el equipo.", 5, 500);

            var firmwareVersion = Cvm.ReadFirmwareVersion().CToNetString();

            if (versionFirmware == "1.11")
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE_2, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);
            else
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            Cvm.WriteBacNetID(TestInfo.BacNetID.Value);

            Cvm.WriteBacNetMAC(2);
        }

        public void FinishRework()
        {
            if (Cvm != null)
                Cvm.Dispose();
        }

        [DelimitedRecord(";")]
        internal class CVMC10_File_Alarms
        {
            public string param_code = "";
            public string hi = "";
            public string lo = "";
            public string delayon = "";
            public string delayoff = "";
            public string hyst = "";
            public string latch = "";
            public string state = "";

            public string PARAM_CODE
            {
                get { return param_code.Replace("PARAM_CODE=", "").Trim(); }
            }
            public string HI
            {
                get { return hi.Replace("HI=", "").Trim(); }
            }
            public string LO
            {
                get { return lo.Replace("LO=", "").Trim(); }
            }
            public string DELAYON
            {
                get { return delayon.Replace("DELAYON=", "").Trim(); }
            }
            public string DELAYOFF
            {
                get { return delayoff.Replace("DELAYOFF=", "").Trim(); }
            }
            public string HYST
            {
                get { return hyst.Replace("HYST=", "").Trim(); }
            }
            public string LATCH
            {
                get { return latch.Replace("LATCH=", "").Trim(); }
            }
            public string STATE
            {
                get { return state.Replace("STATE=", "").Trim() == "NA" ? "0" : "1"; }
            }
        }

        internal void ResetpowerSource(bool sacarPuntasUtil = false)
        {
            Delay(4000, "Espera finalizacion tareas antes del apagado");

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyOff();
            else
                Tower.Chroma.ApplyOff();

            if (sacarPuntasUtil)
                Tower.IO.DO.PulseOff(500, (byte)LeadsInputsOutputs.OUT_VI, (byte)LeadsInputsOutputs.OUT_IO);

            Delay(3000, "Espera para el reset de hardware");

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyAndWaitStabilisation();
            else
                Tower.Chroma.ApplyAndWaitStabilisation();

            Delay(4000, "Espera para no intentar comunicar con el equipo mientras arranca");
        }

    }


}