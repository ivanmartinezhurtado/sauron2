﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(PulseOutputActionDescription))]
    
    public class PulseOutput : ActionBase
    {
        [Description("valor de la salida que des/activamos,")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Output { get; set; }

        [DefaultValue(500)]
        [Description("El tiempo del pulso, en milisegundos")]
        public int PulseTime { get; set; }

        [Description("El tipo de pulso, positivo o negativo")]
        [DefaultValue(pulseType.Positive)]
        public pulseType PulseType { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            if (string.IsNullOrEmpty(Output))
                if (testBase.Shell.MsgBox("Error, no se ha indicado ninguna Output para activar", "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.OK, MessageBoxIcon.Error) != DialogResult.Yes)
                    Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("ninguna Output para activar");

            var output = Convert.ToByte(VariablesTools.ResolveValue(Output));

            if (PulseType == pulseType.Negative)
                tower.IO.DO.PulseOff(PulseTime, output);
            else
                tower.IO.DO.PulseOn(PulseTime, output);
        }

        public enum pulseType
        {
            Positive,
            Negative
        }
    }

    public class PulseOutputActionDescription : ActionDescription
    {
        public override string Name { get { return "Pulse output"; } }
        public override string Description { get { return "Acción genérica para des/activar las salidas de la torre en un tiempo indicado"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.output; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }
}
