﻿using Dezac.Tests.UserControls;
using System.Windows.Forms;

namespace WinTestPlayer.Views
{
    public partial class SelectOrdenView : DialogViewBase
    {
        public SelectOrdenView(string lblDesc)
        {
            InitializeComponent();
            LblOrden.Text = lblDesc;
        }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            if (string.IsNullOrEmpty(Codigo))
                return false;

            return true;
        }

        public string Codigo
        {
            get { return txtUserName.Text; }
            set { txtUserName.Text = value; }
        }


        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                CloseView(DialogResult.OK);
        }
    }
}
