﻿using Dezac.Tests.Services;
using Instruments.IO;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;
using System.Threading;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(LaserScannedButtonDescription))]
    public class LaserScannedButton : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Botón de entrada para escanear el láser")]
        [DefaultValue(2)]
        public int InputToScan { get; set; }


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            Thread.Sleep(200);

            bool laserFinished = false;
            bool buttonPulsed = false;
            bool laserPrinting = false;

            tower.UsbIO.LogEnabled = false;
            do
            {
                testBase.CheckCancellationRequested();
                laserFinished = GetVariable<bool>("LASER_FINISHED", false);
                laserPrinting = GetVariable<bool>("LASER_PRINTING", false);
                if (tower.UsbIO.DI[InputToScan] && laserPrinting)
                {
                    cacheSvc.AddOrSet("PULSED_BUTTON", true);
                    buttonPulsed = true;
                }
                Thread.Sleep(10);
            } while (!laserFinished && !buttonPulsed);
        }
    }

    public class LaserScannedButtonDescription : ActionDescription
    {
        public override string Name { get { return "LaserScannedButton"; } }
        public override string Category { get { return "DezacTest/Laser"; } }
        public override string Description { get { return "Action que sirve para configurar el botón con el que se quiere escanear el láser"; } }
        public override Image Icon { get { return Properties.Resources.Industry_Laser_Beam_icon; } }
    }
}
