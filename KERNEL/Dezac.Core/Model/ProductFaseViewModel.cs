﻿namespace Dezac.Core.Model
{
    public class ProductFaseViewModel
    {
        public string IDFASE { get; set; }
        public string FASE { get; set; }
        public decimal? TIEMPOPREPARACION { get; set; }
        public decimal? TIEMPOSTANDARD { get; set; }
        public decimal? TIEMPOTEORICO { get; set; }
    }
}
