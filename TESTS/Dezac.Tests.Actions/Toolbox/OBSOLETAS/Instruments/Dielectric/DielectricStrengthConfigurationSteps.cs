﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Instruments.Auxiliars;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.InstrumentsTools
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(DielectricStrengthSetStepsActionDescription))]
  

    public class DielectricStrengthSetSteps : ActionBase
    {
        public enum TestMode
        {
            AC,
            DC,
            IR,
            NULL
        }


        [DefaultValue(1000D)]
        [Description("Consigna de Voltaje")]
        public double Voltage { get; set; }

        [DefaultValue(200)]
        [Description("Corriente Maxima en mA para modos AC/DC")]
        public double MaximumCurrent { get; set; }

        [DefaultValue(0)]
        public double Time { get; set; }

        [DefaultValue(0)]
        private int Step { get; set; }

        [DefaultValue("0")]
        [Description("Canales en High separados por comas")]
        public string ChannelsHigh { get; set; }

        [DefaultValue("0")]
        [Description("Canales en Low separados por comas")]
        public string ChannelsLow { get; set; }

        [DefaultValue(0)]
        [Description("Resistencia mínima para test en modo IR")]
        public double MinimumResistance { get; set; }

        [DefaultValue(0)]
        [Description("Resistencia máxima para test en modo IR")]
        public double MaximumResistance { get; set; }

        [DefaultValue(0)]
        [Description("Límite mínimo en mA para modos AC/DC")]
        public double LOW { get; set; } 

        [DefaultValue(null)]
        public double RAMP { get; set; }

        [DefaultValue((byte)2)]
        [Description("Si el valor es 0 vendra de BBDD")]
        public byte Port { get; set; }       

        private TestMode mode { get; set; }

        [DefaultValue("AC")]
        public string Mode
        {
            get
            {
                return mode.ToString();
            }
            set
            {
                if (value.ToUpper().Contains("AC"))           
                    mode = TestMode.AC;      
                else if (value.ToUpper().Contains("DC"))       
                    mode = TestMode.DC;          
                else if (value.ToUpper().Contains("IR"))       
                    mode = TestMode.IR;
                else
                    mode = TestMode.AC;
            }
        }

        [DefaultValue(false)]
        [Description("Variable para elegir si los datos vienen de BBDD")]
        public bool BBDD { get; set; }

        private double ConsignaMaxCurrent;
        private double ConsignaMinCurrent;

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            try
            {
                var wmi = new WMISerialPortInitialize();
                wmi.Execute("MoxaPort", "MOXA USB Serial Port");
                var moxa = GetVariable<string>("MoxaPort");
                Port = Convert.ToByte(moxa.Replace("COM", ""));
            }
            catch (Exception)
            {
                Port = Port == 0 ? testBase.Comunicaciones.SerialPort : Port;
            }

            using (var tester = new DielectricStrengthTester(Port))
            {
                tester.SetRemoteMode();

                var testSteps = tester.ReadAmountOfSteps();
                int actualStep = 0;

                if (Step == 0)
                    actualStep = testSteps + 1;
                else
                {
                    if (testSteps < Step)
                        throw new Exception(string.Format("La declaración de pasos de test ha de ser secuencial pero se ha intentado declarar el paso {0} cuando hay programados {1} pasos", Step, testSteps));
                    actualStep = Step;
                }
                if (BBDD)
                {
                    Mode = testBase.Consignas.GetString("MODE_STEP_" + actualStep, Mode, ParamUnidad.SinUnidad);
                    Voltage = testBase.Consignas.GetDouble(testBase.Params.V.Null.TestPoint("STEP_" + actualStep).Name, Voltage, ParamUnidad.V);
                    Time = testBase.Consignas.GetDouble(testBase.Params.TIME.Null.TestPoint("STEP_" + actualStep).Name, Time, ParamUnidad.s);
                    RAMP = testBase.Consignas.GetDouble(testBase.Params.TIME.Null.TestPoint("RAMP_STEP_" + actualStep).Name, RAMP, ParamUnidad.A); //ParamUnidad.s
                }

                if (mode != TestMode.IR)
                {
                    if (BBDD)
                    {
                        MaximumCurrent = testBase.Margenes.GetDouble(testBase.Params.I.Null.TestPoint("MAX_STEP_" + actualStep).Name, MaximumCurrent, ParamUnidad.mA);
                        LOW = testBase.Margenes.GetDouble(testBase.Params.I.Null.TestPoint("MIN_STEP_" + actualStep).Name, LOW, ParamUnidad.mA);
                    }

                    ConsignaMaxCurrent = MaximumCurrent / 1000;
                    ConsignaMinCurrent = LOW / 1000;
                }
                else
                {
                    if (BBDD)
                    {
                        MinimumResistance = testBase.Margenes.GetDouble(testBase.Params.RES.Null.TestPoint("STEP_" + actualStep + "_MIN").Name, MinimumResistance, ParamUnidad.Ohms);
                        MaximumResistance = testBase.Margenes.GetDouble(testBase.Params.RES.Null.TestPoint("STEP_" + actualStep + "_MAX").Name, MaximumResistance, ParamUnidad.Ohms);
                    }
                }

                switch (mode)
                {
                    case TestMode.AC:
                        tester.SetACTestParameters(actualStep, Voltage, ConsignaMaxCurrent, Time);
                        if (ChannelsHigh != "0")
                            tester.SetACOutputChannelHigh(actualStep, ChannelsHigh);
                        if (ChannelsLow != "0")
                            tester.SetACOutputChannelLow(actualStep, ChannelsLow);
                        tester.SetAcLimitLow(actualStep, ConsignaMinCurrent);
                        tester.SetAcTimeRamp(actualStep, RAMP);
                        break;

                    case TestMode.DC:
                        tester.SetDCTestParameters(actualStep, Voltage, ConsignaMaxCurrent, Time);
                        if (ChannelsHigh != "0")
                            tester.SetDCOutputChannelHigh(actualStep, ChannelsHigh);
                        if (ChannelsLow != "0")
                            tester.SetDCOutputChannelLow(actualStep, ChannelsLow);
                        tester.SetDcLimitLow(actualStep, ConsignaMinCurrent);
                        if (RAMP != 0)
                            tester.SetDcTimeRamp(actualStep, RAMP);
                        break;

                    case TestMode.IR:
                        tester.SetIRTestParameters(actualStep, Voltage, MinimumResistance, Time);
                        if (ChannelsHigh != "0")
                            tester.SetIROutputChannelHigh(actualStep, ChannelsHigh);
                        if (ChannelsLow != "0")
                            tester.SetIROutputChannelLow(actualStep, ChannelsLow);
                        tester.SetIRMaximumResistance(actualStep, MaximumResistance);
                        tester.SetIRTimeRamp(actualStep, RAMP);
                        break;

                    default:
                        throw new NotSupportedException("El modo de test seleccionado no es válido");
                }
            }
        }
    }

    public class DielectricStrengthSetStepsActionDescription : ActionDescription
    {
        public override string Name { get { return "DielectricStrengthSetSteps"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Flash_Updater_icon; } }
    }
}
