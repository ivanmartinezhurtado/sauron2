namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PRODUCTOBINARIO
    {
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public System.DateTime FECHA { get; set; }
        public string BINARIO { get; set; }
        public string VERBINARIO { get; set; }
        public string ACTIVO { get; set; }
    
        public virtual PRODUCTO PRODUCTO { get; set; }
    }
}
