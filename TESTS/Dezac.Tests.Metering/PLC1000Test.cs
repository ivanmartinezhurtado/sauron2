﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Data.ViewModels;
using Dezac.Device.Metering;
using Dezac.Device.Metering.SOAP;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.12)]
    public class PLC1000Test : TestBase
    {
        internal PLC1000 plc;
        internal Tower3 tower;
        internal CirwattDlms cirwatt;
        internal DateTime dateTimeLocalRestart;
        internal List<CompactDctSoapService.ConfigurationModules> modulesCheck;
        internal int InstanceNumber = 1;

        internal const int PILOTO_ERROR = 43;
        internal const int OUT_485_SWITCH_232 = 0;
        internal const int OUT_SBT_1_485_CONNECTION = 3;
        internal const int OUT_SBT_2_485_CONNECTION = 1;
        internal const int OUT_SBT_1_232_CONNECTION = 40;
        internal const int OUT_SBT_2_232_CONNECTION = 2;
        internal const int OUT_PLC_L1 = 17;
        internal const int OUT_PLC_L2 = 18;
        internal const int OUT_PLC_L3 = 19;
        internal const int OUT_PLC_PRIME_CONNECTION = 20;
        internal const int OUT_ALARM_IN_1_3_5 = 21;
        internal const int OUT_ALARM_IN_2_4_6 = 22;

        private static readonly object _syncSBT = new Object();

        private bool testDeviceAlone = false;
        private bool plc1000s = false;

        public void TestInitialization()
        {
            plc = AddInstanceVar(new PLC1000(), "UUT");

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.Active24VDC();

            tower.IO.DO.Off(PILOTO_ERROR, OUT_PLC_L2, OUT_PLC_L3);

            tower.IO.DO.On(OUT_PLC_L1);

            tower.ActiveVoltageCircuit();

            tower.ActiveCurrentCircuit(false, false, false);

            InstanceNumber = Configuracion.GetString("DOUBLE_SECUNDARY", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI" ? 2 : 1;

            if (TestInfo.ListComponents==null)
                testDeviceAlone = true;

            TestInfo.FactoryIP = Configuracion.GetString("FACTORY_IP", "192.168.42.30", ParamUnidad.SinUnidad);
        }

        // SBT  //
        //**************************************
        public void VerificationParametersSBT()
        {
            var meter = AddInstanceVar(new CirwattDlms(), "SBT");

            tower.IO.DO.Off(OUT_485_SWITCH_232, OUT_SBT_1_485_CONNECTION, OUT_SBT_2_485_CONNECTION, OUT_SBT_1_232_CONNECTION, OUT_SBT_2_232_CONNECTION);
            tower.IO.DO.On(OUT_SBT_1_232_CONNECTION, OUT_485_SWITCH_232);

            meter.PuertoCom = Comunicaciones.SerialPort;
            logger.InfoFormat("New SBT by  RS232 COM PORT:{0}", meter.PuertoCom);
            meter.Retries = 8;

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                return true;
            }, "Error comunicaciones DLMS al iniciar session", 3, 1500, 500, false, false);

            var versionSW = meter.LeerVersionSW();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, versionSW, Identificacion.VERSION_FIRMWARE_SBT, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. la versión de firmware es incorrecto."), ParamUnidad.SinUnidad);

            meter.EscribirCodigoUnesa(Identificacion.CODIGO_UNESA.Trim());

            meter.EscribirModelo(Identificacion.MODELO_SBT);

            meter.CerrarSesion();
        }

        public void WriteFactorsDefaultSBT()
        {
            List<ComponentsEstructureInProduct> ListSBT = null;

            if (!testDeviceAlone)
            {
                var numProducto = SearchNumProductoByEstructiraSubconjunto("SBT");
                if (numProducto == 0)
                    throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

                ListSBT = TestInfo.ListComponents.Where(p => p.descripcion.Contains("SBT") && p.numproducto == numProducto).ToList();
                if (ListSBT == null)
                    throw new Exception("Error no se encuentra un subconjunto SBT en la estructura para poder grabar su matricula");
            }

            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            tower.IO.DO.Off(OUT_485_SWITCH_232, OUT_SBT_1_485_CONNECTION, OUT_SBT_2_485_CONNECTION, OUT_SBT_1_232_CONNECTION, OUT_SBT_2_232_CONNECTION);
            tower.IO.DO.On(OUT_SBT_1_485_CONNECTION);

            meter.PuertoCom = Comunicaciones.SerialPort2;
            logger.InfoFormat("SBT by  RS485 COM PORT:{0}", meter.PuertoCom);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                var modelo = meter.LeerModelo();
                Assert.AreEqual(ConstantsParameters.Identification.MODELO_SBT, modelo, Identificacion.MODELO_SBT, Error().PROCESO.PRODUCTO.PRODUCTO_INCORRECTO("Error. el modelo del equipo es incorrecto."), ParamUnidad.SinUnidad);

                if (!testDeviceAlone)
                {
                    var SBT = ListSBT.FirstOrDefault();
                    if (SBT == null)
                        throw new Exception("Error no se encuentra un subconjunto SBT en la estructura con su matricula correspondiente");

                    meter.EscribirNumeroFabricacion((uint)SBT.matricula);

                    Resultado.Set("SERIAL_NUMBER_SBT", TestInfo.NumSerieSubconjunto[SBT.descripcion], ParamUnidad.SinUnidad);

                    meter.EscribirNumeroSerie(Convert.ToUInt32(TestInfo.NumSerieSubconjunto[SBT.descripcion]));
                }
                else
                {
                    meter.EscribirNumeroFabricacion((uint)TestInfo.NumBastidor.Value);

                    Resultado.Set("SERIAL_NUMBER_SBT", TestInfo.NumSerie, ParamUnidad.SinUnidad);

                    meter.EscribirNumeroSerie(Convert.ToUInt32(TestInfo.NumSerie));

                    TestInfo.NumSerieInterno = TestInfo.NumSerie;
                }

                meter.EscribirFechaFabricacion(DateTime.Now);

                meter.EscribirEstadoFabricacion(999);

                meter.EscribirFechaHoraActual();

                meter.EscribirFechaVerificacion(DateTime.Now);

                meter.EscribirPPM((short)Configuracion.GetDouble("PPM_GRABAR", 0,ParamUnidad.ppm));

                return true;

            }, "Error comunicaciones DLMS al escribir parametros internos", 5, 1500, 500, false, false);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                meter.WriteZeroOffsetActiveGain(CirwattDlms.enumFases.TRIFASICO);

                meter.WriteVoltageGain(CirwattDlms.enumFases.TRIFASICO, new List<ushort>() { 3018, 3018, 3018 });

                meter.WriteGapGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, new List<short>() { 0, 0, 0 });

                meter.WritePowerActiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, new List<ushort>() { 35594, 35594, 35594 });

                meter.WritePowerReactiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, new List<ushort>() { 35594, 35594, 35594 });

                return true;

            }, "Error comunicaciones DLMS al escribir factores de ganacia de defecto", 5, 1500, 500, false, false);

            meter.CerrarSesion();
        }

        public void TestVoltageAdjustSBT()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            Delay(5000, "Espera estabilización para calculo de Ganacia");

            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1200, SampleMaxNumber = 5 };

            var defs = new TriAdjustValueDef(Params.GAIN_V.L1.Name + "_SBT", Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                return true;
            }, "Error comunicaciones DLMS al iniciar session", 3, 1500, 500, false, false);

            var result = meter.AdjustVoltage(CirwattDlms.enumFases.TRIFASICO, adjustVoltage, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0];
                defs.L2.Value = value[1];
                defs.L3.Value = value[2];
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de voltage valores fuera de margenes");

            var gainVoltage = new List<ushort>(){ (ushort)result.Item2[0], (ushort)result.Item2[1], (ushort)result.Item2[2] };

           
            SamplerWithCancel((p) =>
            {
                meter.WriteVoltageGain(CirwattDlms.enumFases.TRIFASICO, gainVoltage);

                return true;
            }, "", 3, 1500, 500, false, false);

            meter.CerrarSesion();
        }

        public void TestGainPhaseOffsetAdjustSBT()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 60, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            Delay(2000, "Espera estabilización para calculo de Ganacia");

            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            double desfaseFactor = -22.2;

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1200, SampleMaxNumber = 5 };

            var defs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Name + "_SBT", Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                return true;
            }, "Error comunicaciones DLMS al iniciar session", 3, 1500, 500, false, false);

            var result = meter.AdjustGap(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, powerFactor, desfaseFactor, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0];
                defs.L2.Value = value[1];
                defs.L3.Value = value[2];
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de desfase valor fuera de margenes");

            SamplerWithCancel((p) =>
            {
                meter.WriteGapGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, result.Item2);

                return true;
            }, "", 3, 1500, 500, false, false);

            meter.CerrarSesion();
        }

        public void TestGainPowerAdjustSBT()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);
            var powerReference = adjustVoltage * adjustCurrent * Math.Cos(powerFactor.ToRadians());

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            Delay(2000, "Espera estabilización para calculo de Ganacia");

            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1200, SampleMaxNumber = 5 };

            var defs = new TriAdjustValueDef(Params.GAIN_KW.L1.Name + "_SBT", Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                return true;
            }, "Error comunicaciones DLMS al iniciar session", 3, 1500, 500, false, false);

            var result = meter.AdjustPowerActive(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, powerReference, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0];
                defs.L2.Value = value[1];
                defs.L3.Value = value[2];
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de potencia  valor fuera de margenes");

            var gainPower = new List<ushort>() { (ushort)result.Item2[0], (ushort)result.Item2[1], (ushort)result.Item2[2] };

            SamplerWithCancel((p) =>
            {
                meter.WritePowerActiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, gainPower);

                return true;
            }, "Error comunicaciones DLMS al iniciar session", 3, 1500, 500, false, false);

            SamplerWithCancel((p) =>
            {
                meter.WritePowerReactiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, gainPower);

                return true;
            }, "Error comunicaciones DLMS al iniciar session", 3, 1500, 500, false, false);
            
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }); 

            meter.CerrarSesion();
        }

        public void TestCRCIntegritySBT()
        {
            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                var Crc_Binario = meter.LeerCrcBinario();

                Assert.AreEqual(string.Format("{0}_SBT", ConstantsParameters.Identification.CRC_FIRMWARE), Crc_Binario.ToString(), Identificacion.CRC_FIRMWARE.ToString(), Error().UUT.MEDIDA.SETUP("Error. el CRC de integridad del SBT es incorrecto."), ParamUnidad.SinUnidad);

                return true;
            }, "", 15, 5000, 5000, false, false);

            meter.CerrarSesion();
        }

        public void TestVerificationSBT(int initCount = 3, int samples = 8, int timeInterval = 1200)
        {
            internalTestVerificationSBT(initCount, samples, timeInterval);
        }

        public void TestVerificationSBT4Q(int initCount = 3, int samples = 8, int timeInterval = 1200)
        {
            internalTestVerificationSBT(initCount, samples, timeInterval, true);
        }

        private void internalTestVerificationSBT(int initCount = 3, int samples = 8, int timeInterval = 1200, bool Q2 = false)
        {
            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 60, ParamUnidad.Grados);

            if (Q2)
                angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Other("Q2").Verificacion.Name, 120, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, angleGap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var average = adjustVoltage * adjustCurrent * Math.Cos((angleGap.ToRadians()));

            var toleranciaKw = Params.KW.Null.Verificacion.Tol();
            var toleranciaKvar = Params.KVAR.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();

            var nameKw = Params.KW.L1.Verificacion.Name + "_SBT";
            var nameKvar = Params.KVAR.L1.Verificacion.Name + "_SBT";

            if (Q2)
            {
                nameKw = Params.KW.L1.Verificacion.Q2.Name + "_SBT";
                nameKvar = Params.KVAR.L1.Verificacion.Q2.Name + "_SBT";
            }

            defs.Add(new TriAdjustValueDef(nameKw, 0, 0, average, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(nameKvar, 0, 0, average, toleranciaKvar, ParamUnidad.Var));

            Delay(4000, "Espera arranque del SBT");

            int count = 1;

            TestCalibracionBase(defs,
                () =>
                {
                    logger.DebugFormat("TestCalibracionBase SBT Reint:{0}", count);

                    var powerActive = meter.Leer_Potencia(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);
                    logger.DebugFormat("PowerActive SBT Potencia Activa: {0}", powerActive);

                    var powerReactive = meter.Leer_Potencia(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, TipoPotencia.React);
                    logger.DebugFormat("PowerActive SBT Potencia Reactiva: {0}", powerReactive);

                    var varsList = new List<double>(){
                        powerActive[0], powerActive[1], powerActive[2],
                        powerReactive[0], powerReactive[1], powerReactive[2]
                    };

                    count++;

                    return varsList.ToArray();
                },
                () =>
                {
                    List<TriLineValue> value = new List<TriLineValue>();
                    value.Add(tower.PowerSourceIII.ReadVoltage());
                    value.Add(tower.PowerSourceIII.ReadCurrent());

                    var Power = TriLinePower.Create(value[0], value[1], TriLineValue.Create(angleGap));
                    logger.DebugFormat("with SBT{0} -> Powers MTE PPS {0}", Power);

                    var varsList = new List<double>();
                    varsList.AddRange(Power.KW.ToArray());
                    varsList.AddRange(Power.KVAR.ToArray());

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            meter.CerrarSesion();

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(230, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
        }

        public void TestCostumizeSBT()
        {
            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            tower.IO.DO.Off(OUT_485_SWITCH_232, OUT_SBT_1_485_CONNECTION, OUT_SBT_2_485_CONNECTION, OUT_SBT_1_232_CONNECTION, OUT_SBT_2_232_CONNECTION);
            tower.IO.DO.On(OUT_SBT_1_232_CONNECTION, OUT_485_SWITCH_232);

            meter.PuertoCom = Comunicaciones.SerialPort;
            logger.InfoFormat("New SBT by  RS232 COM PORT:{0}", meter.PuertoCom);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                return true;
            }, "Error de comunicaciones, No inicia sesion después de hacer un reset hardware", 2, 1000, 500, true, true);

            var serialNumber = meter.LeerNumeroSerie();
            Assert.IsTrue(serialNumber.ToString() == TestInfo.NumSerieInterno.ToString(), Error().UUT.NUMERO_DE_SERIE.NO_CORRESPONDE_CON_ENVIO("Error. El número de serie grabado en el SBT no corresponde con el leido"));

            var codunesa = meter.LeerCodigoUnesa();
            Assert.AreEqual(ConstantsParameters.Identification.CODIGO_UNESA, codunesa, Identificacion.CODIGO_UNESA, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. el codigo unesa es incorrecto."), ParamUnidad.SinUnidad);

            var data = meter.LeerFechaHora();

            double timeDiff = (DateTime.Now.Subtract(data)).TotalSeconds;

            Resultado.Set("DIFF_TIME", timeDiff, ParamUnidad.s);

            Assert.AreGreater(Configuracion.GetDouble("MAX_DIFF_TIME", 30, ParamUnidad.s), timeDiff, Error().UUT.MEDIDA.MARGENES("Error. El equipo ha perdido mas de 30 segundos después del reset de hardware"));

            meter.EscribirEstadoFabricacion(0);

            Resultado.Set("CODE_ERROR", 0, ParamUnidad.SinUnidad);

            meter.CerrarSesion(false);

            meter.Dispose();
        }

        // CPU  //
        //**************************************
        public void TestFormatSDClearMemoryCPU()
        {
            SamplerWithCancel((p) => { plc.GetSizeMemoryRAM(); return true; }, "", 8, 2000, 10000, false, false);

            plc.FomatearSD();

            plc.BorrarMemoria();

            Delay(2000, "Espera borrar SD y memoria");

            RestartCompacDC();
        }

        public void TestMemoryCPU()
        {
            int memoryRam = 0;

           SamplerWithCancel((p) => { memoryRam = plc.GetSizeMemoryRAM(); return true; }, "", 8, 2000, 10000, false, false);

            Assert.AreBetween(Params.MEMORY_RAM.Null.TestPoint("TOTAL").Name, memoryRam, Params.MEMORY_RAM.Null.TestPoint("TOTAL").Min(), Params.MEMORY_RAM.Null.TestPoint("TOTAL").Max(), Error().UUT.MEMORIA.MARGENES("Error. tamaño de la memoria RAM total fuera de margenes"), ParamUnidad.MByte);

            SamplerWithCancel((p) => { CheckTestPartitionMemory(plc.StatusInternalMemory()); return true; }, "", 10, 4000, 5000, false, false);

            var temperature = plc.VerificationTemperature();
            Assert.AreBetween(Params.TEMP.Null.Verificacion.Name, temperature, Params.TEMP.Null.Verificacion.Min(), Params.TEMP.Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES("Error valor de temperatura fuera de márgenes"), ParamUnidad.Grados);

            if (!plc.DetectionSD())
                throw new Exception("Error no se detecta la SD en el PLC1000");

            Resultado.Set("SD", "OK", ParamUnidad.SinUnidad);
        }

        public void TestPulsadorMagnetico()
        {
            plc.ActiveMagneticPulse();

            var frm = this.ShowForm(() => new ImageView("COLOQUE EL PULSADOR MAGNETICO COMO EN LA IMAGEN", this.PathCameraImages + "PLC50_113\\PulsadorMagnetico.jpg"), MessageBoxButtons.RetryCancel);

            try
            {
                SamplerWithCancel((p) =>
                {
                    if (frm.DialogResult == DialogResult.Cancel)
                        throw new Exception("Test cancelado por el usuario");

                    return plc.VerificationMagneticPulse() == 0;
                }, "Error no se ha detectado el pulsador magnetico", 50, 1500, 100);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Resultado.Set("PULSADOR_MAGNETICO", "OK", ParamUnidad.SinUnidad);
        }

        public void TestWebService()
        {
            CompactDctSoapService.configuration config = null;

            SamplerWithCancel((p) => { config = plc.Soap.ReadConfiguration(true); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 10000, false, false);           

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, config.info.Vf, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_MODEM_PLC, config.info.VfComm, Identificacion.VERSION_FIRMWARE_MODEM_PLC, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware del modem instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            TestInfo.NumMAC_PLC = config.info.Macplc;
            Resultado.Set(ConstantsParameters.TestInfo.MAC_PLC, config.info.Macplc, ParamUnidad.SinUnidad);

            plc.Soap.WriteFrameLogMode(CompactDctSoapService.FrameLogMode.Capture);

            if (!testDeviceAlone)
            {
                var positionSlot = 1;

                var numProductoSBT = SearchNumProductoByEstructiraSubconjunto("SBT");
                if (numProductoSBT == 0)
                    throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

                modulesCheck = new List<CompactDctSoapService.ConfigurationModules>();

                if (Configuracion.GetString("HAS_MODULE_SBT", "SI", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
                {
                    var SBT = TestInfo.ListComponents.Where(p => p.descripcion.Contains("SBT") && p.numproducto == numProductoSBT).ToList().FirstOrDefault();
                    if (SBT == null)
                        throw new Exception("Error no se encuentra un subconjunto SBT en la estructura para poder grabar su matricula");
                    modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + TestInfo.NumSerieSubconjunto[SBT.descripcion].ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "SBT", Mod = "SBT", });

                    Resultado.Set(SBT.descripcion.ToUpper(), TestInfo.NumSerieSubconjunto[SBT.descripcion], ParamUnidad.SinUnidad);
                    TestInfo.NumSerieInterno = TestInfo.NumSerieSubconjunto[SBT.descripcion];
                    positionSlot += 1;
                }

                var numProductoPLC1000 = SearchNumProductoByEstructiraSubconjunto("PLC1000");
                if (numProductoPLC1000 == 0)
                    throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

                if (Configuracion.GetString("HAS_MODULE_PLC1000", "SI", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
                {
                    var SBT = TestInfo.ListComponents.Where(p => p.descripcion.Contains("PLC") && p.numproducto == numProductoPLC1000).ToList().FirstOrDefault();
                    if (SBT == null)
                        throw new Exception("Error no se encuentra un subconjunto PLC1000 en la estructura para poder grabar su matricula");
                    TestInfo.NumSerie = TestInfo.NumSerieSubconjunto[SBT.descripcion];

                    modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + TestInfo.NumSerie.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "PLC1000", Mod = "PLC1000", });

                    Resultado.Set(SBT.descripcion.ToUpper(), TestInfo.NumSerie, ParamUnidad.SinUnidad);
                    positionSlot += 1;
                }
            }

            logger.InfoFormat("GRABAMOS EL FICHERO DE IP PARA EL ERROR DE LA IP DE CLIENTE");
            bool resetModem = false;
            SamplerWithCancel((p) => { plc.Soap.WriteConfigurationWhitOutInfoAndModuleFromXML(Identificacion.CUSTOMIZATION_FILE_TEST, true, out  resetModem); return true; }, "Error, al escribir fichero XML de configuracion por SOAP ", 15, 5000, 5000, false, false);

            Thread.Sleep(5000);
        }

        public void TestConfigurationCPU()
        {
            SamplerWithCancel((p) => { plc.Soap.ReadConfiguration(true); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 1500, 1000, false, false);
            
            var DoubleSecondary = Configuracion.GetString("DOUBLE_SECUNDARY", "NO", ParamUnidad.SinUnidad).Trim().ToUpper()=="SI";

            plc.Soap.WriteAdditionalInfo(new CompactDctSoapService.AdditionalInfo() { Group = "Others", Key = "DoubleSecondary", Value = DoubleSecondary == true ? "Y" : "N" });

            if (!testDeviceAlone)
                plc.Soap.WriteModuleConfiguration(modulesCheck);

            Delay(2000, "Espera la grbación de los modulos");

            var config = new CompactDctSoapService.ConfigurationInfo();
            config.Cnc = Identificacion.SUBMODELO + TestInfo.NumSerie;
            config.Model = Identificacion.MODELO;
            config.YearManufacture = DateTime.Now.Year.ToString();
            plc.Soap.WriteConfiguration(config);

            var password = new CompactDctSoapService.ConfigurationPassword();
            password.ManagementPassword = Configuracion.GetString("CLAVE_GESTION", "00000001", ParamUnidad.SinUnidad);
            password.WritePassword = Configuracion.GetString("CLAVE_ESCRITURA", "qxzbravo", ParamUnidad.SinUnidad);
            password.UpdatePassword = Configuracion.GetString("CLAVE_ACTUALIZACION", "uflu84i7", ParamUnidad.SinUnidad);
            password.SearchPassword = Configuracion.GetString("BUSQUEDA_AUTOMATICA_PWD", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            plc.Soap.WritePassword(password);

            var stg = new CompactDctSoapService.ConfigurationStg();
            stg.StgVersion = Identificacion.VERSION_STG;
            plc.Soap.WriteStgVersion(stg);

            plc.Soap.WriteDateTimePCLocal();
        }

        public void TestSerialPort()
        {
            var serialPortpe = Configuracion.GetString("TYPE_SERIAL_PORT", "RS232", ParamUnidad.SinUnidad).Trim().ToUpper();
            if (serialPortpe == "RS232")
            {
                tower.IO.DO.Off(OUT_SBT_1_485_CONNECTION, OUT_SBT_2_485_CONNECTION, OUT_SBT_1_232_CONNECTION, OUT_SBT_2_232_CONNECTION);
                tower.IO.DO.On(OUT_485_SWITCH_232);
                plc.PortCom = Comunicaciones.SerialPort;
                logger.InfoFormat("CPU by  RS232 COM PORT:{0}", plc.PortCom);
            }
            else if (serialPortpe == "RS485")
            {
                tower.IO.DO.Off(OUT_485_SWITCH_232, OUT_SBT_1_485_CONNECTION, OUT_SBT_2_485_CONNECTION, OUT_SBT_1_232_CONNECTION, OUT_SBT_2_232_CONNECTION);
                plc.PortCom = Comunicaciones.SerialPort2;
                logger.InfoFormat("CPU by  RS485 COM PORT:{0}", plc.PortCom);
            }else
            {
                Resultado.Set("TEST SERIALPORT", "NO TIENE", ParamUnidad.SinUnidad);
                return;
            }
            
            var result = this.ReadLineToText(plc.SP, 15, 0, 5, 5, new string[] { "Cnc" });

            if (!result.Contains(TestInfo.NumSerie))
                throw new Exception("No hemos leido el numero deserie por RS232 igual que el grabado");

            Resultado.Set("TEST" + serialPortpe, "OK",ParamUnidad.SinUnidad);

            plc.SP.Dispose();
        }

        // PLC  //
        //**************************************
        public void TestPLCfromMeter()
        {
            var METER_PLC = new Dictionary<TestLine,string>();
            METER_PLC.Add(TestLine.PLC_L1, GetVariable<string>("METER1", "CIR0000000001"));
            METER_PLC.Add(TestLine.PLC_L2, GetVariable<string>("METER2", "CIR0000000002"));
            METER_PLC.Add(TestLine.PLC_L3, GetVariable<string>("METER3", "CIR0000000003"));

            tower.IO.DO.Off((int)TestLine.PLC_L1, (int)TestLine.PLC_L2, (int)TestLine.PLC_L3);

            foreach (TestLine line in Enum.GetValues(typeof(TestLine)))
            {
                tower.IO.DO.On((int)line);

                SamplerWithCancel((p) =>
                {
                    var device = plc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);
                    var meter = device.Where((r) => r.IsSBT == false && r.MeterID == METER_PLC[line]).FirstOrDefault();
                    if (meter == null)
                        throw new Exception("Error. no se ha detectado un contador conectado al concentrador ");
                    plc.Soap.ReadDeviceInformation(meter.MeterID);

                    if (!plc1000s)
                    {
                        logger.InfoFormat("Reading PLC Communication Quality by PLC {0} -- Retry {1} to 10", line.ToString(), p);
                        var readingPLCQuality = plc.ReadPLCCommunicationsQuality();
                        Assert.IsValidMaxMin(Params.PARAMETER("PHYLEVEL_" + line.ToString(), ParamUnidad.SinUnidad).Null.Name, 
                            readingPLCQuality, 
                            ParamUnidad.SinUnidad,
                            Params.PARAMETER("PHYLEVEL", ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                            Params.PARAMETER("PHYLEVEL", ParamUnidad.SinUnidad).Null.Verificacion.Max(),
                            Error().UUT.MEDIDA.MARGENES("Indicador Calidad PLC"));
                    }

                    return true;
                }, "Error. No se ha podido comunicar con el medidor", 15, 5000, 5000, false, false);

                tower.IO.DO.Off((int)line);
            }
        }

        public void TestPLCfromSBT()
        {
            var numSBT = (int)Configuracion.GetDouble("SBT_DETECTED", 2, ParamUnidad.SinUnidad);

            if (!testDeviceAlone)
                TestPLCfromSBTConjunto(numSBT);
            else
                SamplerWithCancel((p) =>
                {

                    var device = plc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);

                    var sbtSoap = device.Where((t) => t.IsSBT == true);

                    if (sbtSoap.Count() == 0)
                        throw new Exception("Error. no se ha detectado ningun SBT conectado al concentrador ");

                    if (sbtSoap.Count() != numSBT)
                        throw new Exception(string.Format("Error. no se ha detectado los {0} SBT conectados al concentrador", numSBT));

                    CompactDctSoapService.devices deviceSBT = null;

                    if(numSBT==1)
                        deviceSBT = sbtSoap.FirstOrDefault();
                    else
                        deviceSBT = sbtSoap.Where((t) => t.MeterID == "CIR" + TestInfo.NumSerie.ToString().PadLeft(10, '0')).FirstOrDefault();

                    if (deviceSBT == null)
                        throw new Exception("Error. no se ha encontrado el SBT esperado");

                    for (byte i = 0; i <= 2; i++)
                    {
                        plc.Soap.ReadDeviceInformation(deviceSBT.MeterID);
                        Delay(1500,"Espera");
                    }
                    return true;
                }, "", 10, 4000, 1000, false, false);
        }

        private void TestPLCfromSBTConjunto(int numSBT)
        {
            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);

            var numProductoSBT = SearchNumProductoByEstructiraSubconjunto("SBT");
            if (numProductoSBT == 0)
                throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

            var SBT = TestInfo.ListComponents.Where(p => p.descripcion.Contains("SBT") && p.numproducto == numProductoSBT).ToList().FirstOrDefault();
            if (SBT == null)
                throw new Exception("Error no se encuentra un subconjunto SBT en la estructura para poder grabar su matricula");

            uint numeroSerie = 0;

            Delay(5000, "Espera detection PLC");

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                numeroSerie = meter.LeerNumeroSerie();

                Assert.IsTrue(numeroSerie.ToString() == TestInfo.NumSerieSubconjunto[SBT.descripcion].ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El número de serie grabado en el SBT{0} no corresponde con el leido"));

                meter.CerrarSesion();

                return true;

            }, "", 5, 1500, 1000, false, false);

            SamplerWithCancel((p) =>
            {

                var device = plc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);

                var sbtSoap = device.Where((t) => t.IsSBT == true);

                if (sbtSoap.Count() == 0)
                    throw new Exception("Error. no se ha detectado ningun SBT conectado al concentrador ");

                if (sbtSoap.Count() != numSBT)
                    throw new Exception(string.Format("Error. no se ha detectado los {0} SBT conectados al concentrador", numSBT));

                var deviceSBT = sbtSoap.Where((t) => t.MeterID == "CIR" + numeroSerie.ToString().PadLeft(10, '0')).FirstOrDefault();

                if (deviceSBT == null)
                    throw new Exception("Error. no se ha encontrado el SBT esperado");

                for (byte i = 0; i <= 2; i++)
                {
                    plc.Soap.ReadDeviceInformation(deviceSBT.MeterID);
                    Delay(1500, "Espera");
                }

                return true;
            }, "", 10, 4000, 1000, false, false);


            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                meter.EscribirEstadoFabricacion(0);

                meter.EscribirReset();

                meter.CerrarSesion();

                return true;

            }, "", 5, 1500, 500, false, false);
        }
   
        // VERIFICATION CPU //
        //**************************************
        public void DefaultConfigurationDeleteNodesRestart()
        {
            bool resetModem = false;
            SamplerWithCancel((p) => { plc.Soap.WriteConfigurationWhitOutInfoAndModuleFromXML(Identificacion.CUSTOMIZATION_FILE, true, out  resetModem); return true; }, "Error, al escribir fichero XML de configuracion por SOAP ", 15, 5000, 5000, false, false);

            Thread.Sleep(5000);

            SamplerWithCancel((p) => { plc.KillConcentratorAplication(); return true; }, "Error, no se ha podido borrar la BBDD por SSH en el equipo", 5, 2000, 1000, false, false);

            plc.DeleteListNodes();

            Delay(2500, "Esperamos se desconecte el contador y borre la BD");

            plc.CloseConectionSsH();

            RestartCompacDC();

            dateTimeLocalRestart = DateTime.Now;            
        }

        public void TestExternalMemory()
        {
            string status = "";

            SamplerWithCancel((p) => { status = plc.StatusExternalMemory(); return true; }, "Error, no se ha podido establecer comunicacion SSH con el equipo despues de reiniciar", 10, 2000, 15000, false, false);

            var dateDevice = Convert.ToDateTime(status);

            var diferenceTime = dateDevice.Subtract(dateTimeLocalRestart);

            Resultado.Set("LAST_SHUT_DOWN_TIME", status, ParamUnidad.SinUnidad);
            Resultado.Set("LAST_SHUT_DOWN_DIFERENCE_TIME", diferenceTime.TotalSeconds.ToString(), ParamUnidad.SinUnidad);

            if (Math.Abs(diferenceTime.TotalSeconds) > Margenes.GetDouble("DIFERENCE_TIME_SHUT_DOWN", 120, ParamUnidad.s))
                throw new Exception("Error. memoria externa no ha registrado el ultimo reset ");
        }

        public void TestConfigVerification()
        {
            CompactDctSoapService.configuration config = null;

            SamplerWithCancel((p) => { config = plc.Soap.ReadConfiguration(true); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 5000, false, false);

            Assert.IsTrue(config.Cnc == Identificacion.SUBMODELO + TestInfo.NumSerie.ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El número de serie grabado no corresponde con el leido"));

            var dateTimeLocal = DateTime.Now;

            var fechaEquipo = config.Fecha;

            var diferenceTime = fechaEquipo.Subtract(dateTimeLocal);

            Assert.AreLesser(Params.TEMP.Other("DESVIACION").Verificacion.Name, Math.Abs(diferenceTime.TotalSeconds), Params.TEMP.Other("DESVIACION").Verificacion.Max(), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("Error. el equipo tiene una hora con una desviación de mas del establecido por BBDD"), ParamUnidad.s);

            var tareas = config.tareas;
            if (tareas.Count != Configuracion.GetDouble("TAREAS_PROGRAMADAS", 3, ParamUnidad.SinUnidad))
                throw new Exception("Error no se han detectado las misnas tareas programadas en el Concentrador que en la BBDD");

            var ifconfig = plc.IfConfig();

            Assert.IsTrue(ifconfig.IP.Contains("192.168.42.30"), Error().SOFTWARE.IP.CONFIGURACION_INCORRECTA("Error, la IP del equipo no coincide con la esperada"));
            Assert.IsTrue(ifconfig.Broadcast.Contains("192.168.42.255"), Error().UUT.MEDIDA.NO_COINCIDE("Error, el BROADCAST del equipo no coincide con el esperado"));
            Assert.IsTrue(ifconfig.Netmask.Contains("255.255.255.0"), Error().UUT.MEDIDA.NO_COINCIDE("Error, la MASCARA DE SUBRED del equipo no coincide con la esperada"));

            Resultado.Set("FACTORY_IP", TestInfo.FactoryIP, ParamUnidad.SinUnidad);
        }

        public void TestAnyNodeConected()
        {
            var numSBT = (int)Configuracion.GetDouble("SBT_DETECTED", 2, ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                var config = plc.Soap.ReadConfiguration(true);

                var deviceNotConected = plc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Registrado);
                if (deviceNotConected.Count() != 0)
                    throw new Exception("Error. Se ha detectado un contador conectado al concentrador después de haberse borrado la lista de nodos");

                var device = plc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);

                var meter = device.Where((t) => t.IsSBT == false);

                if (meter.Count() != 0)
                    throw new Exception("Error. Se ha detectado un contador conectado al concentrador después de haberse borrado la lista de nodos");

                Resultado.Set("ANY_METER_CONNECTED", "OK", ParamUnidad.SinUnidad);

                var sbt = device.Where((t) => t.IsSBT == true);

                if (sbt.Count() != numSBT)
                    throw new Exception("Error. no se ha detectado SBT conectado al concentrador después del reset");

                Resultado.Set("SBT_CONNECTED", "OK", ParamUnidad.SinUnidad);

                return true;

            }, "", 25, 5000, 5000, false, false);


            SamplerWithCancel((p) =>
            {
                plc.DeleteListNodes();
                return true;
            }, "", 5, 1000, 5000, false, false);

            plc.CloseConectionSsH();

            Delay(2500, "Esperamos se desconecte el contador y borre la BD");

        }

        // ALARMAS  //
        //**************************************
        public void TestAlarms()
        {
            var alarms = new List<CompactDctSoapService.AlarmInfo>();

            SamplerWithCancel((p) =>
            {
                alarms = plc.Soap.ReadAlarms();

                Assert.IsTrue(alarms.Count == Configuracion.GetDouble("NUMERO_ALARMAS", 6, ParamUnidad.SinUnidad), Error().UUT.CONFIGURACION.ALARMA("Error numero de alarmas detectadas incorrecto"));

                return true;
            }, "", 10, 500, 500, false, false);

            tower.IO.DO.Off(OUT_ALARM_IN_2_4_6);
            tower.IO.DO.On(OUT_ALARM_IN_1_3_5);

            SamplerWithCancel((p) =>
            {
                alarms = plc.Soap.ReadAlarms();

                foreach (CompactDctSoapService.AlarmInfo alarm in alarms)
                    if (Math.IEEERemainder(alarm.id, 2) != 0)
                        Assert.IsTrue(alarm.value.Equals("1"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 1, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                    else
                        Assert.IsTrue(alarm.value.Equals("0"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 0, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));

                return true;
            }, "", 10, 1000, 1000, false, false);

            tower.IO.DO.Off(OUT_ALARM_IN_1_3_5);
            tower.IO.DO.On(OUT_ALARM_IN_2_4_6);

            SamplerWithCancel((p) =>
            {
                alarms = plc.Soap.ReadAlarms();

                foreach (CompactDctSoapService.AlarmInfo alarm in alarms)
                    if (Math.IEEERemainder(alarm.id, 2) != 0)
                        Assert.IsTrue(alarm.value.Equals("0"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 0, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                    else
                        Assert.IsTrue(alarm.value.Equals("1"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 1, valor encontrado {0}", alarm.value.ToString())));

                return true;
            }, "", 10, 500, 500, false, false);

            tower.IO.DO.On(OUT_ALARM_IN_1_3_5, OUT_ALARM_IN_2_4_6);
        }

        public void WriteSerialNumberSGE_AL()
        {
            plc1000s = true;

            SamplerWithCancel((p) => { plc.Soap.ReadConfiguration(true); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 10000, false, false);

            modulesCheck = new List<CompactDctSoapService.ConfigurationModules>();

            modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + TestInfo.NumSerie.ToString().PadLeft(10, '0'), Type = "Inputs", Mod = "SGE-AL", });

            plc.Soap.WriteModuleConfigurationPLC1000(modulesCheck);
        }

        //PLC1000S
        //**************************************
        public void WriteSerialNumberPLC1000S()
        {
            plc1000s = true;

            SamplerWithCancel((p) => { plc.Soap.ReadConfiguration(true); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 10000, false, false);

            modulesCheck = new List<CompactDctSoapService.ConfigurationModules>();

            modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + TestInfo.NumSerie.ToString().PadLeft(10, '0'), Type = "Secondary", Mod = "SGE-PLC1000S", });

            plc.Soap.WriteModuleConfigurationPLC1000(modulesCheck);
        }

        //*********************************************

        public void TestFinish()
        {
            var meter = GetVariable<CirwattDlms>("SBT", cirwatt);
            if (meter != null)
                meter.Dispose();

     
            if (cirwatt != null)
                cirwatt.Dispose();

            if (plc != null)
                plc.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(OUT_PLC_L1, OUT_PLC_L2, OUT_PLC_L3);
                tower.IO.DO.Off(OUT_ALARM_IN_1_3_5, OUT_ALARM_IN_2_4_6);

                tower.ShutdownSources();
                tower.Dispose();
            }
        }

        //**********************************
        //**********************************

        private void RestartCompacDC()
        {
            tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(2000, "Esperamos se apague el equipo");

            tower.Chroma.ApplyAndWaitStabilisation();
        }

        private void TestModule(List<CompactDctSoapService.Module> modulesRead)
        {
            if (modulesCheck.Count != modulesRead.Count)
                throw new Exception("Error. no se han detectado el número de modulos conectados correctamente");

            foreach (CompactDctSoapService.ConfigurationModules modulo in modulesCheck)
            {
                var mod = modulesRead.Where((p) => p.Mod.Contains(modulo.Mod));
                if (mod == null)
                    throw new Exception(string.Format("Error. no se han detectado el modulo {0} conectado", modulo.Mod));

                if (mod.Count() == 0)
                    throw new Exception(string.Format("Error. no se han detectado el modulo {0} conectado", modulo.Mod));

                Resultado.Set("MODULO_" + modulo.Mod.ToUpper(), string.Format("Id={0} Slot={1}", mod.FirstOrDefault().Id, mod.FirstOrDefault().Slot), ParamUnidad.SinUnidad);
            }
        }

        private void CheckTestPartitionMemory(List<PLC1000.Memory> memory)
        {
            if (memory.Count() < 4)
                throw new Exception("Error. no se han detectado el número de particiones correctamente");

            var ArrayPartitions = new List<string> { "/dev/root", "/dev/mmcblk0p1", "tmpfs" };

            foreach (string partition in ArrayPartitions)
            {
                var mod = memory.Where((p) => p.Filesystem.EndsWith(partition));
                if (mod.Count() == 0)
                    throw new Exception(string.Format("Error. no se ha encontrado la partición {0} ", partition));

                CultureInfo enUS = new CultureInfo("en-US");

                var size = Convert.ToDouble(mod.FirstOrDefault().Size.Replace("M", ""), enUS);

                var partitionName = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Name;
                var partitionSizeMin = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Min();
                var partitionSizeMax = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Max();

                Assert.AreBetween(partitionName, size, partitionSizeMin, partitionSizeMax, Error().UUT.MEMORIA.MARGENES(string.Format("Error. tamaño de la partición{0} de la memoria interna fuera de margenes", partition)), ParamUnidad.MByte);
            }
        }

        private int SearchNumProductoByEstructiraSubconjunto(string filter)
        {
            var producto = Identificacion.ESTRUCTURA_SUBCONJUNTOS;
            var productos = producto.Split(';');
            foreach (string prod in productos)
            {
                var descNumroducto = prod.Split('=');
                if (descNumroducto.Count() <= 1)
                    throw new Exception("Error parametro ESTRUCTURA_SUBCONJUNTOS no esta bien definido, 'descripcion=numproducto;' ej: 'PLC1000=102504;'");

                if (descNumroducto[0].Trim().ToUpper().Contains(filter.ToUpper().Trim()))
                {
                    var sproducto = descNumroducto[1].Split('/');
                    return Convert.ToInt32(sproducto[0]);
                }
            }
            return 0;
        }

        public enum TestLine
        {
            PLC_L1 = 17,
            PLC_L2 = 18,
            PLC_L3 = 19
        }
    }
}
