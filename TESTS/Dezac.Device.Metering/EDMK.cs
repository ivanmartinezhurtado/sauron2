﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.05)]
    public class EDMK : DeviceBase
    {
        public ModbusDevice Modbus { get; set; }

        public EDMK()
        {
        }

        public EDMK(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Modbus.PerifericNumber = periferic;
        }

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.SOFTWARE_VERSION, 3).CToNetString();
        }

        public Key ReadKeyboard()
        {
            LogMethod();

            var keyread = Modbus.ReadMultipleCoil((ushort)Registers.KEYBOARD, 8);

            Key key = (Key)keyread[0];

            return key;
        }

        private TriPowers ReadInstantPowers(InstantPowersType powerType)
        {
            LogMethod();
            return Modbus.Read<TriPowers>((ushort)powerType);
        }

        public Powers ReadInstantPowers()
        {
            Powers powers = new Powers();
            powers.ReactivePowers = ReadInstantPowers(InstantPowersType.REACTIVE);
            powers.ActivePowers = ReadInstantPowers(InstantPowersType.ACTIVE);

            _logger.InfoFormat("REACTIVE POWERS : L1 = {0}, L2 = {1}, L3 = {2}", powers.ReactivePowers.L1HiResol, powers.ReactivePowers.L2HiResol, powers.ReactivePowers.L3HiResol);
            _logger.InfoFormat("ACTIVE POWERS : L1 = {0}, L2 = {1}, L3 = {2}", powers.ActivePowers.L1HiResol, powers.ActivePowers.L2HiResol, powers.ActivePowers.L3HiResol);
            return powers;
        }

        public Gains ReadGains(GainsType addres)
        {
            LogMethod();
            return Modbus.Read<Gains>((ushort)addres);
        }

        public GainsPhase ReadGainsPhase()
        {
            LogMethod();
            return Modbus.Read<GainsPhase>((ushort)Registers.GAINS_PHASE);
        }

        public OffsetsGains ReadOffsetsGains(OffsetsType addres)
        {
            LogMethod();
            return Modbus.Read<OffsetsGains>((ushort) addres);
        }

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();

            var hardwareVectorReading = Modbus.ReadMultipleCoil((ushort)Registers.HARDWARE_VECTOR, 96);

            return new HardwareVector(hardwareVectorReading);
        }

        public string ReadInitialMessage()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.INITIAL_MESSAGE, 4);
        }

        public ushort ReadDayManufactured()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.DAY_MANUFACTURED);
        }

        public ushort ReadMontManufactured()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.MONTH_MANUFACTURED);
        }

        public ushort ReadYearManufactured()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.YEAR_MANUFACTURED);
        }

        public Date ReadDateManufactured()
        {
            LogMethod();

            var day = ReadDayManufactured();
            var month = ReadMontManufactured();
            var year = ReadYearManufactured();

            return new Date(day, month, year);
        }       

        public Energies ReadEnergies(EnergiesType type)
        {
            LogMethod();
            return Modbus.Read<Energies>((ushort)type);
        }

        public void WriteDateManufactured(Date date)
        {
            LogMethod();

            WriteDayManufactured(date.day);
            WriteMonthManufactured(date.month);
            WriteYearManufactured(date.year);
        }

        public void WriteDayManufactured(ushort day)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DAY_MANUFACTURED, day);
        }

        public void WriteMonthManufactured(ushort month)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MONTH_MANUFACTURED, month);
        }

        public void WriteYearManufactured(ushort year)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.YEAR_MANUFACTURED, year);
        }

        public void WriteInitialMessage(string msg)
        {
            LogMethod();
            while (msg.Length < 8)
                msg += " ";

            Modbus.WriteString((ushort)Registers.INITIAL_MESSAGE, msg);
        }

        public void WritePulseActiveOut(int value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PULSE_OUT_ACTIVE, value);
        }

        public void WritePulseReactiveOut(int value) //No estaba creado y si que se necesita. (Rbutt)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PULSE_OUT_REACTIVE, value);
        }

        public void WritePrimaryCurrentRelationDefault()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PRIMARY_CURRENT, 0x0005);
        }

        public void WriteGains(GainsType typeGains, Gains gains)
        {
            FlagTest();

            LogMethod();

            Modbus.Write((ushort)typeGains, gains.Factors.L1);
            Thread.Sleep(200);

            Modbus.Write((ushort)((ushort)typeGains + 1), gains.Factors.L2);
            Thread.Sleep(200);

            Modbus.Write((ushort)((ushort)typeGains + 2), gains.Factors.L3);
            Thread.Sleep(200);
        }

        public void WritePhaseGains(GainsPhase gains)
        {
            FlagTest();

            LogMethod();

            Modbus.Write((ushort)Registers.GAINS_PHASE, gains.Phase.L1);
            Thread.Sleep(200);

            Modbus.Write((ushort)Registers.GAINS_PHASE + 1, gains.Phase.L2);
            Thread.Sleep(200);

            Modbus.Write((ushort)Registers.GAINS_PHASE + 2, gains.Phase.L3);
            Thread.Sleep(200);
        }

        public void WriteOffsetsGains(OffsetsType typeOffsets, OffsetsGains gains)
        {
            FlagTest();

            LogMethod();

            Modbus.Write((ushort)typeOffsets, gains.OffsetsFactors.L1);
            Thread.Sleep(200);

            Modbus.Write((ushort)((ushort)typeOffsets + 1), gains.OffsetsFactors.L2);
            Thread.Sleep(200);

            Modbus.Write((ushort)((ushort)typeOffsets + 2), gains.OffsetsFactors.L3);
            Thread.Sleep(200);

        }

        public void WriteSerialNumber(int sn)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, sn);
        }

        public void WriteLedState(LedsInformation leds)
        {
            LogMethod();

            Modbus.WriteMultipleCoil((ushort)Registers.LEDS_BACKLIGHT, leds.ToBoolArray());          
        }

        public void WriteLedBL()
        {
            LogMethod();
            WriteLedState(new LedsInformation(State.ON, State.OFF, State.ON));
        }

        public void WriteOutputState(Outputs output, State state)
        {
            LogMethod();
            var register = output == Outputs.RELAY1 ? Registers.OUTPUT_1 : output == Outputs.RELAY2 ? Registers.OUTPUT_2 : Registers.DIGITAL_OUTPUT;

            Modbus.WriteSingleCoil((ushort)register, state == State.ON ? true : false);
        }

        public void WriteHardwareVector(HardwareVector hardwareConfig)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_VECTOR, hardwareConfig.ToSingleCoil());
        }

        public void WriteSetupDefault(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.SETUP_DEFAULT, state);
        }

        public void WriteSetupDefaultCalibration(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.SETUP_DEFAULT_CALIBRATION, state);
        }

        public void WriteRecordSetup(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RECORD_SETUP, state);
        }

        public void WriteRecordSetupCalibration(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RECORD_SETUP_CALIBRATION, state);
        }

        public void WriteEscala(Escalas scale)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.SCALE, Convert.ToBoolean((ushort)scale));
        }

        public void WriteDeleteAllEnergies(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_ENERGIES, state);
        }

        public void WriteDeletePartialEnergies(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_PARTIAL_ENERGIES, state);
        }

        public void WriteDeleteRateEnergies(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_PARTIAL_RATE_ENERGIES, state);
        }

        public void SendDisplayInfo(DisplayState state)
        {
            WriteDisplay(displayInformation[state]);
        }

        public void WriteDisplay(params bool[] segments)
        {
            LogMethod();
            var retries = Modbus.Retries;
            Modbus.Retries = 0;
            try
            {
                Modbus.WriteMultipleCoil((ushort)Registers.DISPLAY_SEGMENTS, segments);
            }
            catch (Exception e)
            {
                _logger.Warn(String.Format("Excepcion Capturada: {0}", e.Message));
            }

            Modbus.Retries = retries;
        }

        public void ResetError(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RESET_ERROR, state);
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Calibration
        public Tuple<bool, Gains> AdjustActivePower(int delFirst, int initCount, int samples, int interval, Escalas escala, int dispersion, TriLinePower powersMTE, double rel, Func<Gains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            WriteEscala(escala);

            var gainsToRead = escala == Escalas.E5A ? GainsType.ACTIVE_E5A : GainsType.ACTIVE_E1A;
            var gains = ReadGains(gainsToRead);

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, dispersion,
                () =>
                {
                    var powersReading = ReadInstantPowers();

                    var varlist = new List<double>(){
                       powersReading.ActivePowers.L1HiResol,
                       powersReading.ActivePowers.L2HiResol,
                       powersReading.ActivePowers.L3HiResol
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.Factors.L1 = (ushort)(((powersMTE.KW.L1 * rel) * 16384D) / listValues.Average(0));
                    gains.Factors.L2 = (ushort)(((powersMTE.KW.L2 * rel) * 16384D) / listValues.Average(1));
                    gains.Factors.L3 = (ushort)(((powersMTE.KW.L3 * rel) * 16384D) / listValues.Average(2));

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, Gains> AdjustReactivePower(int delFirst, int initCount, int samples, int interval, Escalas escala, int dispersion, TriLinePower powersMTE, double rel, Func<Gains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            WriteEscala(escala);

            var gainsToRead = escala == Escalas.E5A ? GainsType.REACTIVE_E5A : GainsType.REACTIVE_E1A;
            var gains = ReadGains(gainsToRead);

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, dispersion,
                () =>
                {
                    var powersReading = ReadInstantPowers();

                    var varlist = new List<double>(){
                       powersReading.ReactivePowers.L1HiResol,
                       powersReading.ReactivePowers.L2HiResol,
                       powersReading.ReactivePowers.L3HiResol
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.Factors.L1 = (ushort)(((powersMTE.KVAR.L1 * rel) * 16384D) / listValues.Average(0));
                    gains.Factors.L2 = (ushort)(((powersMTE.KVAR.L2 * rel) * 16384D) / listValues.Average(1));
                    gains.Factors.L3 = (ushort)(((powersMTE.KVAR.L3 * rel) * 16384D) / listValues.Average(2));
                    
                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, GainsPhase> AdjustPhase(int delFirst, int initCount, int samples, int interval, TriLinePower powersMTE, double rel, int dispersion, Func<GainsPhase, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = ReadGainsPhase();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, dispersion,
                () =>
                {
                    var powersReading = ReadInstantPowers();

                    var varlist = new List<double>(){
                       powersReading.ActivePowers.L1HiResol,
                       powersReading.ActivePowers.L2HiResol,
                       powersReading.ActivePowers.L3HiResol
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    var errorPhaseL1 = ((listValues.Average(0) - (powersMTE.KW.L1 * rel)) / (powersMTE.KW.L1 * rel)) * 100D;
                    var errorPhaseL2 = ((listValues.Average(1) - (powersMTE.KW.L2 * rel)) / (powersMTE.KW.L2 * rel)) * 100D;
                    var errorPhaseL3 = ((listValues.Average(2) - (powersMTE.KW.L3 * rel)) / (powersMTE.KW.L3 * rel)) * 100D;

                    gains.Phase.L1 = (ushort)(-errorPhaseL1 / 0.265752067);
                    gains.Phase.L2 = (ushort)(-errorPhaseL2 / 0.265752067);
                    gains.Phase.L3 = (ushort)(-errorPhaseL3 / 0.265752067);

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }
          
        public Tuple<bool, OffsetsGains> AdjustOffsetActivePower(int delFirst, int initCount, int samples, int interval, Escalas escala, int dispersion, TriLinePower powersMTE, double rel, Func<OffsetsGains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            WriteEscala(escala);

            var offsetsToRead = escala == Escalas.E5A ? OffsetsType.ACTIVE_E5A : OffsetsType.ACTIVE_E1A;
            var gains = ReadOffsetsGains(offsetsToRead);

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, dispersion,
                () =>
                {
                    var powersReading = ReadInstantPowers();

                    var varlist = new List<double>(){
                       powersReading.ActivePowers.L1HiResol,
                       powersReading.ActivePowers.L2HiResol,
                       powersReading.ActivePowers.L3HiResol
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.OffsetsFactors.L1 = (ushort)((((powersMTE.KW.L1 * rel) * Math.Pow(10, 6)) - listValues.Average(0)) / 1000D);
                    gains.OffsetsFactors.L2 = (ushort)((((powersMTE.KW.L2 * rel) * Math.Pow(10, 6)) - listValues.Average(1)) / 1000D);
                    gains.OffsetsFactors.L3 = (ushort)((((powersMTE.KW.L3 * rel) * Math.Pow(10, 6)) - listValues.Average(2)) / 1000D);

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, OffsetsGains> AdjustOffsetReactivePower(int delFirst, int initCount, int samples, int interval, Escalas escala, int dispersion, TriLinePower powersMTE, double rel, Func<OffsetsGains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            WriteEscala(escala);

            var offsetsToRead = escala == Escalas.E5A ? OffsetsType.REACTIVE_E5A : OffsetsType.REACTIVE_E1A;
            var gains = ReadOffsetsGains(offsetsToRead);

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, dispersion,
                () =>
                {
                    var powersReading = ReadInstantPowers();

                    var varlist = new List<double>(){
                       powersReading.ReactivePowers.L1HiResol,
                       powersReading.ReactivePowers.L2HiResol,
                       powersReading.ReactivePowers.L3HiResol
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.OffsetsFactors.L1 = (ushort)((((powersMTE.KVAR.L1 * rel) * Math.Pow(10, 6)) - listValues.Average(0)) / 1000D);
                    gains.OffsetsFactors.L2 = (ushort)((((powersMTE.KVAR.L2 * rel) * Math.Pow(10, 6)) - listValues.Average(1)) / 1000D);
                    gains.OffsetsFactors.L3 = (ushort)((((powersMTE.KVAR.L3 * rel) * Math.Pow(10, 6)) - listValues.Average(2)) / 1000D);

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        #endregion

        #region Enums & Dictionaries
        public enum Registers
        {
            DIGITAL_OUTPUT = 0x0000,
            OUTPUT_1 = 0x0000,
            OUTPUT_2 = 0x0001,
            KEYBOARD = 0x0008,
            LEDS_BACKLIGHT = 0x0010,
            DISPLAY_SEGMENTS = 0x03E8,
            ENERGIES = 0x0000,
            ENERGIES_T1 = 0x000C,
            ENERGIES_T2 = 0x0018,
            ENERGIES_T3 = 0x0024,
            PARTIAL_ENERGIES = 0x0030,

            DELETE_ENERGIES = 0x0834,
            DELETE_PARTIAL_ENERGIES = 0x0835,
            DELETE_PARTIAL_RATE_ENERGIES = 0x0836,
            HARDWARE_VECTOR = 0x2710,
            SERIAL_NUMBER = 0x2710,
            FLAG_TEST = 0x2AF8,
            RESET_ERROR = 0x2EE0,
            SETUP_DEFAULT = 0x2EE1,
            SETUP_DEFAULT_CALIBRATION = 0x2EE2,
            RECORD_SETUP = 0x2EE3,
            RECORD_SETUP_CALIBRATION = 0x2EE4,
            INSTANT_REACTIVE_POWER = 0x2B5D,
            INSTANT_ACTIVE_POWER = 0x2B63,
            GAINS_PHASE = 0x2B2A,
            TEST_DISPLAY = 0x2F12,
            CURRENT_SCALE = 0xF123,
            ELECTRICS_VARIABLES = 0x0000,
            SOFTWARE_VERSION = 0x0578,
            SCALE = 0xF123,
            PRIMARY_CURRENT = 0x044F,
            PULSE_OUT_ACTIVE = 0x0456,
            PULSE_OUT_REACTIVE = 0x0458,
            INITIAL_MESSAGE = 0x0514,
            DAY_MANUFACTURED = 0x04B0,
            MONTH_MANUFACTURED = 0x04B1,
            YEAR_MANUFACTURED = 0x04B2,
        }

        public enum DisplayState
        {
            [Description("ningun segmento")]
            TODO_APAGADO,           
            [Description("segmentos pares")]
            EVEN,
            [Description("segmentos impares")]
            ODD,
            [Description("todos los segmentos")]
            ALL,
        }

        public enum EnergiesType
        {
            ENERGIES = 0x0000,
            ENERGIES_T1 = 0x000C,
            ENERGIES_T2 = 0x0018,
            ENERGIES_T3 = 0x0024,
            PARTIAL_ENERGIES = 0x0030
        }

        public enum GainsType
        {
            ACTIVE_E5A = 0x2AFB,
            REACTIVE_E5A = 0x2AFE,
            ACTIVE_E1A = 0x2B01,
            REACTIVE_E1A = 0X2B04
        }

        public enum OffsetsType
        {
            ACTIVE_E5A = 0x2BC0,
            REACTIVE_E5A = 0x2BC3,
            ACTIVE_E1A = 0x2BC6,
            REACTIVE_E1A = 0X2BC9
        }

        public enum InstantPowersType
        {
            REACTIVE = 0x2B5D,
            ACTIVE = 0x2B63           
        }

        [Flags]
        public enum Key
        {
            [Description("Ninguna")]
            NO_KEY = 0x00,
            [Description("Arriba")]
            UP = 0x01,
            [Description("Derecha")]
            RIGHT = 0x02,
            [Description("Arriba abajo")]
            UP_DOWN = 0x04,
            [Description("Set A")]
            SET_A = 0x08,
            [Description("CLEAR")]
            CLEAR = 0x10,
            [Description("Setup")]
            SETUP = 0x20,
            [Description("Display")]
            DISPLAY = 0x40
        }

        public enum State
        {
            ON,
            OFF
        }

        public enum Outputs
        {
            [Description("RELE 1")]
            RELAY1,
            [Description("RELE 2")]
            RELAY2
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2,
            ITFext = 7,
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _21_51Vdc = 3,
            _80_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            CVM_MINI = 1,
            CVM_MINI_ETH = 2,
            EDMK = 10
        }

        public enum Escalas
        {
            E5A,
            E1A
        }

        public Dictionary<DisplayState, bool[]> displayInformation = new Dictionary<DisplayState, bool[]>()
        {
            {DisplayState.TODO_APAGADO, new bool[128]
                {
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false
                }},                                  
              {DisplayState.EVEN, new bool[128]
                {
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true
                }},
             {DisplayState.ODD, new bool[128]
                {
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false

                }},
             {DisplayState.ALL, new bool[128]
                {
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true
                }},
        };

        #endregion

        #region Structs              

        public struct Date
        {
            public ushort day;
            public ushort month;
            public ushort year;

            public Date(int Day, int Month, int Year)
            {
                day = (ushort)Day;
                month = (ushort)Month;
                year = (ushort)Year;
            }
        }

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //[ModbusLayout(Address = (ushort)Registers.INSTANT_POWERS)]
        //public struct InstantaneousPowers
        //{
        //    private int reactivePowerL1;
        //    private int reactivePowerL2;
        //    private int reactivePowerL3;
        //    private int activePowerL1;
        //    private int activePowerL2;
        //    private int activePowerL3;

        //    //public double Frecuency
        //    //{
        //    //    get
        //    //    {
        //    //        return frec / 10D;
        //    //    }
        //    //}

        //    public double ReactivePowerL1
        //    {
        //        get
        //        {
        //            return reactivePowerL1 / 1000000D;
        //        }
        //    }

        //    public double ReactivePowerL2
        //    {
        //        get
        //        {
        //            return reactivePowerL2 / 1000000D;
        //        }
        //    }

        //    public double ReactivePowerL3
        //    {
        //        get
        //        {
        //            return reactivePowerL3 / 1000000D;
        //        }
        //    }

        //    public double ActivePowerL1
        //    {
        //        get
        //        {
        //            return activePowerL1 / 1000000D;
        //        }
        //    }

        //    public double ActivePowerL2
        //    {
        //        get
        //        {
        //            return activePowerL2 / 1000000D;
        //        }
        //    }

        //    public double ActivePowerL3
        //    {
        //        get
        //        {
        //            return activePowerL3 / 1000000D;
        //        }
        //    }
        //}

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Energies
        {
            public int ActiveEnergyPos;
            public int RectiveEnergyNeg;
            public int ReactiveInductiveEnergyPos;
            public int ReactiveCapacitiveEnergyNeg;
            public int ReactiveInductiveEnergyNeg;
            public int ReactiveCapacitiveEnergyPos;

            public bool CheckEnergiesDelete()
            {
                return ActiveEnergyPos == 0 &&
                       RectiveEnergyNeg == 0 &&
                       ReactiveInductiveEnergyPos == 0 &&
                       ReactiveCapacitiveEnergyNeg == 0 &&
                       ReactiveInductiveEnergyNeg == 0 &&
                       ReactiveCapacitiveEnergyPos == 0;
            }
        }       

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Powers
        {
            public TriPowers ReactivePowers;
            public TriPowers ActivePowers;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriPowers
        {
            private int l1 { get; set; }
            private int l2 { get; set; }
            private int l3 { get; set; }

            public int L1HiResol { get { return l1; } }

            public int L2HiResol { get { return l2; } }

            public int L3HiResol { get { return l3; } }

            public double L1
            {
                get
                {
                    return l1 / 1000000D;
                }
            }

            public double L2
            {
                get
                {
                    return l2 / 1000000D;
                }
            }

            public double L3
            {
                get
                {
                    return l3 / 1000000D;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriGains
        {
            public ushort L1 { get; set; }
            public ushort L2 { get; set; }
            public ushort L3 { get; set; }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Gains
        {
            public TriGains Factors;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct GainsPhase
        {
            public TriGains Phase;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct OffsetsGains
        {
            public TriGains OffsetsFactors;

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.LEDS_BACKLIGHT)]
        public struct LedsInformation
        {
            public State CPU;
            public State COMMUNICATIONS;
            public State BACKLIGHT;

            public LedsInformation(State state)
            {
                CPU = state;
                COMMUNICATIONS = state;
                BACKLIGHT = state;
            }

            public LedsInformation(State stateCPU, State stateComm, State stateBL)
            {
                CPU = stateCPU;
                COMMUNICATIONS = stateComm;
                BACKLIGHT = stateBL;
            }

            public bool[] ToBoolArray()
            {
                return new bool[]
                {
                    CPU == State.ON ? true : false,
                    COMMUNICATIONS == State.ON ? true : false,
                    BACKLIGHT == State.ON ? true : false,
                    false
                };
            }
        }

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele2 { get; set; }
            public bool Rele1 { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Shunt { get; set; }
            public bool Tarifas { get; set; }
            public bool Bateria { get; set; }

            public string Rele1String { get { return Rele1 == true ? "SI" : "NO"; } }
            public string Rele2String { get { return Rele2 == true ? "SI" : "NO"; } }
            public string Rs232String { get { return Rs232 == true ? "SI" : "NO"; } }
            public string ComunicationString { get { return Comunication == true ? "SI" : "NO"; } }
            public string ShuntString { get { return Shunt == true ? "SI" : "NO"; } }
            public string TarifasString { get { return Tarifas == true ? "SI" : "NO"; } }
            public string BateriaString { get { return Bateria == true ? "SI" : "NO"; } }

            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value.ToUpper().Trim();

                    if (powerSupplyString.Contains("230VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._230Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("400VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._400Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("480VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._480Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("80-265VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._80_265Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("21-51VDC"))
                    {
                        PowerSupply = VectorHardwareSupply._21_51Vdc;
                        return;
                    }
                    else
                        throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware, Valores permitidos (230VAC, 400VAC, 480VAC, 80-265VAC o 21-51VDC)");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else
                    {
                        if (inputVoltageString.Contains("110"))
                        {
                            InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                            return;
                        }
                        else
                        {
                            if (inputVoltageString.Contains("500"))
                            {
                                InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                                return;
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("5"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                        return;
                    }
                    else
                    {
                        if (inputCurrentString.Contains("2"))
                        {
                            InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                            return;
                        }
                        else
                        {
                            if (inputCurrentString.Contains("1"))
                            {
                                InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                                return;
                            }
                            else
                            {
                                if (inputCurrentString.ToUpper().Contains("ITF"))
                                {
                                    InputCurrent = VectorHardwareCurrentInputs.ITFext;
                                    return;
                                }
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString == "CVM_MINI")
                        Modelo = VectorHardwareModelo.CVM_MINI;
                    else if (modeloString == "CVM_MINI_ETH")
                        Modelo = VectorHardwareModelo.CVM_MINI_ETH;
                    else if (modeloString == "EDMK")
                        Modelo = VectorHardwareModelo.EDMK;
                    else
                        throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele1 = (vectorHardware[0] & 0x10) == 0x10;
                Rele2 = (vectorHardware[0] & 0x20) == 0x20;
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                Tarifas = (vectorHardware[2] & 0x10) == 0x10;
                Bateria = (vectorHardware[2] & 0x20) == 0x20;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele1;
                vector[5] = Rele2;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;
                vector[11] = Shunt;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[20] = Tarifas;
                vector[21] = Bateria;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }
        
        #endregion
    }
}
