﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dezac.Core.Utility;
using Instruments.Switch;
using Instruments.PowerSource;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(GE15_Description))]
    public partial class UC_GE15 : UserControl
    {
        private GE15 ge15;

        public Type InstrumentType
        {
            get
            {
                return typeof(GE15);
            }
        }

        public GE15 Instrument
        {
            get
            {
                if (ge15 == null)
                    ge15 = new GE15();

                return ge15;
            }
            set
            {
                ge15 = value;
            }
        }

        public UC_GE15()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }

    public class GE15_Description : ControlDescription
    {
        public override string Name { get { return "GE15"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_icon; } }
        public override Image IconBig { get { return Properties.Resources.battery_icon; } }
        public override Type SubcomponentType { get { return typeof(GE15); } }
        public override bool Visible => false;
    }
}