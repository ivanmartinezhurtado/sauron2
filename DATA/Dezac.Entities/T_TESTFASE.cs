namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTFASE
    {
        public T_TESTFASE()
        {
            this.T_TESTERROR = new HashSet<T_TESTERROR>();
            this.T_TESTVALOR = new HashSet<T_TESTVALOR>();
            this.T_TESTSTEP = new HashSet<T_TESTSTEP>();
            this.T_TESTFASEFILE = new HashSet<T_TESTFASEFILE>();
        }
    
        public int NUMTESTFASE { get; set; }
        public int NUMTEST { get; set; }
        public string IDFASE { get; set; }
        public string IDOPERARIO { get; set; }
        public System.DateTime FECHA { get; set; }
        public int TIEMPO { get; set; }
        public string RESULTADO { get; set; }
        public string NOTAS { get; set; }
        public Nullable<int> NUMMARCAJE { get; set; }
        public Nullable<int> NUMCAJA { get; set; }
        public string NOMBREPC { get; set; }
        public Nullable<int> NUMREINTENTOS { get; set; }
    
        public virtual DIARIOMARCAJEFASE DIARIOMARCAJEFASE { get; set; }
        public virtual FASE FASE { get; set; }
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual T_TEST T_TEST { get; set; }
        public virtual ICollection<T_TESTERROR> T_TESTERROR { get; set; }
        public virtual ICollection<T_TESTVALOR> T_TESTVALOR { get; set; }
        public virtual ICollection<T_TESTSTEP> T_TESTSTEP { get; set; }
        public virtual ICollection<T_TESTFASEFILE> T_TESTFASEFILE { get; set; }
    }
}
