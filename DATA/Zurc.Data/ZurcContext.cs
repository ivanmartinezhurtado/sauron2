﻿namespace Zurc.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ZurcContext : DbContext
    {
        public ZurcContext()
            : base("name=ZurcContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;

            this.Database.Log = Console.Write;
        }

        public virtual DbSet<Matricula> Matricula { get; set; }
        public virtual DbSet<Fase> Fase { get; set; }
        public virtual DbSet<Articulo> Articulo { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<ArticuloCliente> ArticuloCliente { get; set; }
        public virtual DbSet<ProductoFase> ProductoFase { get; set; }
        public virtual DbSet<Familia> Familia { get; set; }
        public virtual DbSet<GrupoParametrizacion> GrupoParametrizacion { get; set; }
        public virtual DbSet<Parametro> Parametro { get; set; }
        public virtual DbSet<TipoParametro> TipoParametro { get; set; }
        public virtual DbSet<Test> Test { get; set; }
        public virtual DbSet<TestError> TestError { get; set; } 
        public virtual DbSet<TestFase> TestFase { get; set; }
        public virtual DbSet<TestFaseFile> TestFaseFile { get; set; }
        public virtual DbSet<TestFases> TestFases { get; set; }
        public virtual DbSet<TestStep> TestStep { get; set; }
        public virtual DbSet<TestValor> TestValor { get; set; }
        public virtual DbSet<TipoGrupoParametrizacion> TipoGrupoParametrizacion { get; set; }
        public virtual DbSet<Unidad> Unidad { get; set; }
        public virtual DbSet<TipoValorParametro> TipoValorParametro { get; set; }
        public virtual DbSet<ValorParametro> ValorParametro { get; set; }
        public virtual DbSet<ValorParametroProducto> ValorParametroProducto { get; set; }
        public virtual DbSet<ValorParametroLog> ValorParametroLog { get; set; }
        public virtual DbSet<OrdenProducto> OrdenProducto { get; set; }
        public virtual DbSet<Empleado> Empleado { get; set; }

        public virtual DbSet<OrdenFabricacion> OrdenFabricacion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Familia>()
                .HasMany(e => e.GruposParametrizacion)
                .WithRequired(e => e.Familia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Familia>()
                .HasMany(e => e.Articulos)
                .WithRequired(e => e.Familia)
                .HasForeignKey(e => e.FamiliaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GrupoParametrizacion>()
                .HasMany(e => e.Valores)
                .WithRequired(e => e.GrupoParametrizacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Test>()
                .HasMany(e => e.Fases)
                .WithRequired(e => e.Test)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Test>()
                .HasMany(e => e.Fases)
                .WithRequired(e => e.Test)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<T_TEST>()
            //    .HasOptional(e => e.T_TESTREPORT)
            //    .WithRequired(e => e.T_TEST);

            modelBuilder.Entity<TestFase>()
                .HasMany(e => e.Archivos)
                .WithRequired(e => e.TestFase)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TestFase>()
                .HasMany(e => e.Steps)
                .WithRequired(e => e.TestFase)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TestFase>()
                .HasMany(e => e.Valores)
                .WithRequired(e => e.TestFase)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoGrupoParametrizacion>()
                .HasMany(e => e.GruposParametrizacion)
                .WithRequired(e => e.TipoGrupo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Unidad>()
                .HasMany(e => e.Parametros)
                .WithRequired(e => e.Unidad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ValorParametro>()
                .HasMany(e => e.ValoresParametroProducto)
                .WithRequired(e => e.ValorParametro)
                .HasForeignKey(e => new { e.GrupoId, e.ParametroId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoParametro>()
                .HasMany(e => e.Parametros)
                .WithRequired(e => e.TipoParametro)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoValorParametro>()
                .HasMany(e => e.Parametros)
                .WithRequired(e => e.TipoValorParametro)
                .WillCascadeOnDelete(false);
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            return this.Database.SqlQuery<TElement>(sql, parameters);
        }

        private ObjectContext ObjectContext
        {
            get { return ((IObjectContextAdapter)this).ObjectContext; }
        }

        public string DDL()
        {
            var sql = ObjectContext.CreateDatabaseScript();

            return sql;
        }

        public bool UseProxy
        {
            get { return this.Configuration.ProxyCreationEnabled; }
            set { this.Configuration.ProxyCreationEnabled = value; }
        }

        public void ChangeState<T>(T entity, EntityState state) where T : class
        {
            this.Entry<T>(entity).State = state;
        }

        //public void Attach<T>(T entity) where T : class
        //{
        //    if (ObjectContext.IsAttachedTo(entity))
        //        Detach(entity);

        //    this.Set<T>().Attach(entity);
        //}

        public void Detach<T>(T entity) where T : class
        {
            this.Entry<T>(entity).State = EntityState.Detached;
        }

        public void SetValues<T>(T entity, T fromEntity) where T : class
        {
            this.Entry<T>(entity).CurrentValues.SetValues(fromEntity);
        }

        public void LoadProperty(object entity, string navigationProperty)
        {
            this.ObjectContext.LoadProperty(entity, navigationProperty);
        }
    }
}
