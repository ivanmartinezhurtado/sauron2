namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTERROR")]
    public partial class T_TESTERROR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NROERROR { get; set; }

        public int? NUMTESTFASE { get; set; }

        public int NUMERROR { get; set; }

        [Required]
        [StringLength(50)]
        public string PUNTOERROR { get; set; }

        [StringLength(200)]
        public string EXTENSION { get; set; }

        [StringLength(50)]
        public string ERRORCODE { get; set; }

        public virtual OPE_ERROR OPE_ERROR { get; set; }

        public virtual T_IRISERROR T_IRISERROR { get; set; }

        public virtual T_TESTFASE T_TESTFASE { get; set; }
    }
}
