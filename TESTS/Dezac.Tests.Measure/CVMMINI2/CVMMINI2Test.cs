﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.37)]
    public class CVMMINI2Test : TestBase
    {
        #region Variables

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                    return tower;
            }
            
        }

        private CVMMINI2 mini2;
        public CVMMINI2 Mini2
        {
            get
            {   
                if(mini2 == null)
                    mini2 = AddInstanceVar(new CVMMINI2(7), "UUT");
                return mini2;
            }
            set
            {
                mini2 = value;
            }
        }

        internal string CAMERA_IDS;
        internal bool alimentacionDC = false;
        internal string modelo = string.Empty;

        // OUTPUTS
        internal byte BORNAS_J2_J3 = 5;
        internal byte AUX_EV = 12;
        internal byte SELEC_BORNAS_ETH = 54;
        internal byte BORNAS_J5_J8 = 53;
        internal byte BORNAS_J5_ETH = 55;
        internal byte BLOQUE_GRABACION = 6;
        internal byte FIJACION_EQUIPO = 7;
        internal byte FIJACION_CARRO = 8;
        internal byte PISTON_LEFT_KEY = 9;
        internal byte PISTON_CENTRAL_KEY = 10;
        internal byte PISTON_RIGHT_KEY = 11;
        internal byte OUT_DIGITAL_IN = 19;
        internal byte DISCONNECT_DATAMAN = 52;

        // INPUTS
        private byte DETECCION_OUT_EQUIPO = 9;
        internal byte BLOQUE_SEGURIDAD = 23; 
        internal byte PRESENCIA_EQUIPO = 24; 
        internal byte PRESENCIA_TAPA = 25;
        internal byte PRESENCIA_CUBRE_BORNAS_1 = 26;
        internal byte PRESENCIA_CUBRE_BORNAS_2 = 27;
        internal byte IN_DETECCION_MODELO_ETHERNET = 28;
        internal byte IN_DETECCION_MODELO_BORNAS = 29;
        internal byte IN_BORNAS_1 = 30;
        internal byte IN_BORNAS_2 = 31;
        internal byte IN_BORNAS_ETH = 34;
        internal byte IN_FIJACION_EQUIPO = 32;
        internal byte IN_BLOQUE_GRABACION = 33;
            
        #endregion

        public virtual void TestInitialization(int portMINI2 = 0)
        {            
            if (portMINI2 == 0)
                portMINI2 = Comunicaciones.SerialPort;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", Tower);
            }

            Mini2 = AddInstanceVar(new CVMMINI2(portMINI2), "UUT");
            Mini2.Modbus.PerifericNumber = Convert.ToByte(1);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS_DISPLAY", CAMERA_IDS);

            if (VectorHardware.GetString("ALIMENTACION", CVMMINI2.PowerSupply.VAC230.GetDescription(), ParamUnidad.SinUnidad).Trim().ToUpper().Contains("VDC"))     // Averigurar si Vcc es Vdc del Vector de Hardware
            {
                alimentacionDC = true;
                SamplerWithCancel((p) =>
                    {
                        Tower.LAMBDA.Initialize();
                        Tower.LAMBDA.ApplyOff();
                        return true;
                    }, "Error. No se puede comunicar con la fuente LAMBDA DC", 2, 1500, 0, false, false);
            }

            else
                Tower.Chroma.ApplyOff();
           
            Tower.PowerSourceIII.ApplyOff();

            Tower.MUX.Reset();
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[BLOQUE_SEGURIDAD])
                {
                    if (Shell.MsgBox("Comprovar que el CONNECTOR DE SEGURIDAD esté conectado y el utillaje CERRADO", "No se detecta el BLOQUE DE SEGURIDAD", System.Windows.Forms.MessageBoxButtons.OKCancel) == DialogResult.OK)
                        return false;
                    Error().HARDWARE.UTILLAJE.BLOQUE_SEGURIDAD().Throw();
                }
                return true;
            }, "Error. BLOQUE DE SEGURIDAD no detectado", 5, 500, 1000);

            SamplerWithCancel((p) =>
            {
                if(!Tower.IO.DI[PRESENCIA_EQUIPO])
                {
                    if(Shell.MsgBox("Comprovar que el EQUIPO SE HA INTRODUCIDO EN EL UTIL y el ANCLA ESTA COLOCADA", "No se detecta la PRESENCIA del equipo", System.Windows.Forms.MessageBoxButtons.OKCancel) == DialogResult.OK)
                        return false;
                    Error().HARDWARE.UTILLAJE.DETECCION_EQUIPO().Throw();
                }
                return true;
            }, "Error de la PRESENCIA del equipo. Equipo o ancla no detectados", 5, 500, 1000);

            Tower.IO.DO.OnWait(500, FIJACION_CARRO);
            Tower.IO.DO.OnWait(500, FIJACION_EQUIPO);
            Delay(1500, "Activando pistones de fijacion");

            if (!Tower.IO.DI[IN_FIJACION_EQUIPO])
                throw new Exception("No se detecta la FIJACION DEL EQUIPO activada");
            if (!Tower.IO.DI[PRESENCIA_TAPA])
                throw new Exception("No se detecta la TAPA DEL EQUIPO colocada");

            Tower.IO.DO.OnWait(500, AUX_EV); //Activamos válvula auxiliar
            Tower.IO.DO.OffWait(500, SELEC_BORNAS_ETH); //Aseguramos que la posición es el modelo normal con bornas

            if (!Tower.IO.DI[IN_DETECCION_MODELO_BORNAS])
                throw new Exception("No se detecta la posición del pistón de selección de modelo en el modelo BORNAS");

            Tower.IO.DO.On(BORNAS_J2_J3, BORNAS_J5_J8);

            Delay(1500, "Activando pistones de comunicaciones");

            if (!Tower.IO.DI[IN_BORNAS_1])
                throw new Exception("No se detecta BLOQUE BORNAS J2 y J3 activado");
            if (!Tower.IO.DI[IN_BORNAS_2])
                throw new Exception("No se detecta BLOQUE BORNAS J5 y J8 activado");
            if (!Tower.IO.DI[PRESENCIA_CUBRE_BORNAS_1])
                throw new Exception("No se detecta el CUBREBORNAS 1 DEL EQUIPO colocado");
            if (!Tower.IO.DI[PRESENCIA_CUBRE_BORNAS_2])
                throw new Exception("No se detecta el CUBREBORNAS 2 DEL EQUIPO colocado");


            /*if (portMINI2 == 4)                                                                                                      // TEST OFFLINE
                portMINI2 = Comunicaciones.SerialPort;

            tower = AddInstanceVar<Tower3>("TOWER");
            mini2 = AddInstanceVar(new CVMMINI2(portMINI2), "UUT");

            tower.PowerSourceIII.PortCom = 1;

            mini2.Modbus.PerifericNumber = Convert.ToByte(1);*/
        }

        public virtual void TestCommunications()
        {
            Tower.IO.DO.On(DISCONNECT_DATAMAN);
            Tower.IO.DO.Off(BLOQUE_GRABACION);

            Tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);
            
            SamplerWithCancel((p) =>
                {
                    Mini2.FlagTest();
                    return true;
                }, "Error de comunicaciones. No se ha podido establecer el modo test en el equipo", 3, 500, 2000, false, false);            

            var firmwareVersion = Mini2.ReadFirmwareVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString().Trim(), Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error de versión. La versión de firmware del equipo es incorrecta"), ParamUnidad.SinUnidad);

            var voltageAjust = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentAjust = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 4, ParamUnidad.A);
            var powerFactorAjust = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltageAjust, currentAjust, 50, powerFactorAjust, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });  // I+D: Durante todo el test, el equipo le debe circular corriente y voltaje
        }

        public void TestDefaultVariables()
        {
            // ****************** VECTOR HARDWARE *********************** //
             
            var hardwareVector = new CVMMINI2.HardwareVector();

            var PowerSource = CVMMINI2.PowerSupply.NULL;
            Enum.TryParse<CVMMINI2.PowerSupply>(VectorHardware.GetString("ALIMENTACION", CVMMINI2.PowerSupply.VAC230.GetDescription(),ParamUnidad.SinUnidad).Trim().ToUpper(), out PowerSource);
            hardwareVector.Source = PowerSource;
            Resultado.Set("ALIMENTACION", hardwareVector.Source.ToString(), ParamUnidad.SinUnidad);

            var Inputs = CVMMINI2.InputsNumber.NULL;
            Enum.TryParse<CVMMINI2.InputsNumber>(VectorHardware.GetString("NUMERO_INPUTS", CVMMINI2.InputsNumber.INPUT1.GetDescription(), ParamUnidad.SinUnidad).Trim().ToUpper(), out Inputs);
            hardwareVector.Inputs = Inputs;
            Resultado.Set("NUMERO_INPUTS", hardwareVector.Inputs.ToString(), ParamUnidad.SinUnidad);

            var Outputs = CVMMINI2.OutputsNumber.NULL;
            Enum.TryParse<CVMMINI2.OutputsNumber>(VectorHardware.GetString("NUMERO_OUTPUTS", CVMMINI2.OutputsNumber.OUTPUT1.GetDescription(), ParamUnidad.SinUnidad).Trim().ToUpper(), out Outputs);
            hardwareVector.Outputs = Outputs;
            Resultado.Set("NUMERO_OUTPUTS", hardwareVector.Outputs.ToString(), ParamUnidad.SinUnidad);

            var Communications = CVMMINI2.Communications.NULL;
            Enum.TryParse<CVMMINI2.Communications>(VectorHardware.GetString("COMUNICACIONES", CVMMINI2.Communications.RS485MODBUSBACNET.GetDescription(), ParamUnidad.SinUnidad).Trim().ToUpper(), out Communications);
            hardwareVector.Comms = Communications;
            Resultado.Set("COMUNICACIONES", hardwareVector.Comms.ToString(), ParamUnidad.SinUnidad);

            var CurrentMeasure = CVMMINI2.MeasureI.NULL;
            Enum.TryParse<CVMMINI2.MeasureI>(VectorHardware.GetString("ESCALA_MEDIDA_INTENSIDAD", CVMMINI2.MeasureI.NONEUTRAL_PRIMARY1A5A.GetDescription(), ParamUnidad.SinUnidad).Replace(" ", "_").ToUpper(), out CurrentMeasure);
            hardwareVector.Measure_I = CurrentMeasure;
            Resultado.Set("ESCALA_MEDIDA_INTENSIDAD", hardwareVector.Measure_I.ToString(), ParamUnidad.SinUnidad);

            var VoltageMeasure = CVMMINI2.MeasureV.NULL;
            Enum.TryParse<CVMMINI2.MeasureV>(VectorHardware.GetString("ESCALA_MEDIDA_VOLTAGE", CVMMINI2.MeasureV.VAC300.GetDescription(), ParamUnidad.SinUnidad).Trim().ToUpper(), out VoltageMeasure);
            hardwareVector.Measure_V = VoltageMeasure;
            Resultado.Set("ESCALA_MEDIDA_VOLTAGE", hardwareVector.Measure_V.ToString(), ParamUnidad.SinUnidad);

            var ModelType = CVMMINI2.ModelType.NULL;
            Enum.TryParse<CVMMINI2.ModelType>(VectorHardware.GetString("MODELO", CVMMINI2.ModelType.CVM_MINI2.GetDescription(), ParamUnidad.SinUnidad).Replace(" ", "_").ToUpper(), out ModelType);
            hardwareVector.Model = ModelType;
            Resultado.Set("MODELO", hardwareVector.Model.ToString(), ParamUnidad.SinUnidad);

            hardwareVector.noNeed = 0;

            hardwareVector.NotApply = 0;

            SetVariable("VectorHardware", hardwareVector);
          
            Mini2.WriteHardwareVector(hardwareVector);
            Delay(1000, "Escribiendo Vector de Hardware");

            // ****************** VECTOR PROPERTY *********************** //

            var propertyVector = new CVMMINI2.PropertyVector();

            propertyVector.OutReles = Configuracion.GetString("OUT_RELÉS", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            propertyVector.OutTransistor = Configuracion.GetString("OUT_TRANSISTOR", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            propertyVector.Inputs = Configuracion.GetString("INPUTS", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            propertyVector.Harmonics = Configuracion.GetString("HARMONICOS", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            propertyVector.THD = Configuracion.GetString("THD", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            propertyVector.PD = Configuracion.GetString("MAXIMA_DEMANDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            SetVariable("VectorProperty", propertyVector);

            Mini2.WritePropertyVector(propertyVector);
            Delay(1000, "Escribiendo Vector de Propiedades");

            Tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(3000, "Realizando reset HW");
            Tower.Chroma.ApplyAndWaitStabilisation();
            Delay(5000, "Estabilizando COMM's del equipo");

            SamplerWithCancel((p) =>
            {
                Mini2.FlagTest();
                return true;
            }, "Error de comunicaciones. No se ha podido establecer el modo test en el equipo", 10, 500, 1000, false, false);

            // ****************** CONFIGURACIONS PER DEFECTE GENERALS *********************** //

            Mini2.WriteSetupDefault();
            Delay(2000, "Esperando configuracion de setup por defecto del equipo");

            Mini2.WriteGainsDefault();
            Delay(2000, "Esperando escritura de las ganancias por defecto del equipo");
             
            if(Configuracion.GetString("MODELO_EQUIPO", "NA", ParamUnidad.SinUnidad).ToUpper() == "NEUTRO")
                Mini2.WriteModelName("\0 \0 \0 \0 \0 \0 \0 \0 \0 \0 ");        
            else
                Mini2.WriteModelName("\0 \0C\0i\0r\0c\0 \0u\0t\0o\0r");

            Delay(1000, "Esperando escritura modelo");

            var hardwareVectorReaded = Mini2.ReadHardwareVector();
            var hardwareVectorBBDD = GetVariable<CVMMINI2.HardwareVector>("VectorHardware"); 
            Assert.AreEqual(hardwareVectorReaded, hardwareVectorBBDD, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error. El vector de Hardware grabado en el equipo no coincide con la configuración de base de datos"));

            var propertyVectorReaded = Mini2.ReadPropertyVector();
            var propertyVectorBBDD = GetVariable<CVMMINI2.PropertyVector>("VectorProperty");
            Assert.AreEqual("PROPERTY_VECTOR", propertyVectorReaded.ToHexString(), propertyVectorBBDD.ToHexString(), Error().UUT.HARDWARE.NO_COINCIDE("Error. El vector de propiedades grabado en el equipo no coincide con la configuración de base de datos"), ParamUnidad.SinUnidad);                    
        }

        public virtual void TestConsumoCargaMinima(int DelFirst = 2, int NumIteraciones = 3, int TimeInterval = 500)
        {
            Mini2.WriteLoad(false);
            Delay(500, "Activando carga mínima del equipo");          

            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = alimentacionDC == true ? TypePowerSource.LAMBDA : TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.Aparente,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageMinimo,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(Tower, configuration);

            if (VectorHardware.GetString("COMUNICACIONES", "RS485MODBUSBACNET", ParamUnidad.SinUnidad) == "RS485MODBUSBACNET")
            {
                configuration.typeTestVoltage = TypeTestVoltage.VoltageMaximo;
                this.TestConsumo(Tower, configuration);
            }

            configuration.typeTestVoltage = TypeTestVoltage.VoltageNominal;
            this.TestConsumo(Tower, configuration);

            if (VectorHardware.GetString("COMUNICACIONES", "RS485MODBUSBACNET", ParamUnidad.SinUnidad) == "RS485MODBUSBACNET")
            {
                if (Tower.IO.DI[DETECCION_OUT_EQUIPO])
                    throw new Exception("ERROR en la Salida Digital del equipo. Su estado es activado y no es correcto en el test de Consumo en Carga Mínima [ESTADO CORRECTO = DESACTIVADO]");

                Resultado.Set("DIGITAL_OUTPUT_OFF", "OK", ParamUnidad.SinUnidad);
            }
        }

        public virtual void TestConsumoCargaMaxima(int DelFirst = 2, int NumIteraciones = 3, int TimeInterval = 500)
        {
            Mini2.WriteLoad(true);
            Delay(500, "Activando carga máxima del equipo");           
            
            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = alimentacionDC == true ? TypePowerSource.LAMBDA : TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.Aparente,
                typeTestPoint = TypeTestPoint.ConCarga,
                typeTestVoltage = TypeTestVoltage.VoltageMinimo,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(Tower, configuration);

            if (VectorHardware.GetString("COMUNICACIONES", "RS485MODBUSBACNET", ParamUnidad.SinUnidad) == "RS485MODBUSBACNET")
            {
                configuration.typeTestVoltage = TypeTestVoltage.VoltageMaximo;
                this.TestConsumo(Tower, configuration);
            }

            configuration.typeTestVoltage = TypeTestVoltage.VoltageNominal;
            this.TestConsumo(Tower, configuration);

            if (VectorHardware.GetString("COMUNICACIONES", "RS485MODBUSBACNET", ParamUnidad.SinUnidad) == "RS485MODBUSBACNET")
            {

                if (!Tower.IO.DI[DETECCION_OUT_EQUIPO])
                    throw new Exception("ERROR en la Salida Digital del equipo. Su estado es desactivado y no es correcto en el test de Consumo en Carga Mínima [ESTADO CORRECTO = ACTIVADO]");

                Resultado.Set("DIGITAL_OUTPUT_ON", "OK", ParamUnidad.SinUnidad);
            }
        }        

        public void TestDisplay()
        {
            Mini2.FlagTest();
            Mini2.WriteDisplayLCD(CVMMINI2.RegistersDisplay.BACKLIGHT, true);
            Delay(250, "Activacion del Backlight");

            Mini2.WriteDisplayLCD(CVMMINI2.RegistersDisplay.EVEN_SEGMENTS, true);
            Delay(1000, "Esperando captura de imagen");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CVMMINI2_EVEN_SEGMENTS", "EVEN_SEGMENTS", "CVMMINI2");

            Mini2.WriteDisplayLCD(CVMMINI2.RegistersDisplay.ODD_SEGMENTS, true);
            Delay(1000, "Esperando captura de imagen");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CVMMINI2_ODD_SEGMENTS", "ODD_SEGMENTS", "CVMMINI2");

            Mini2.WriteDisplayLCD(CVMMINI2.RegistersDisplay.ALL_SEGMENTS, true);
            Delay(1000, "Esperando captura de imagen");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CVMMINI2_ALL_SEGMENTS", "ALL_SEGMENTS", "CVMMINI2");

            Mini2.WriteDisplayLCD(CVMMINI2.RegistersDisplay.ALL_SEGMENTS, false);
            Delay(250, "Desactivacion segmentos");

            Mini2.WriteDisplayLCD(CVMMINI2.RegistersDisplay.BACKLIGHT, false);
            Delay(250, "Desactivacion del Backlight");
        }

        public void TestLEDs()
        {
            Mini2.WriteLEDS(CVMMINI2.LEDsRegisters.LED_CPU, true);
            Mini2.WriteLEDS(CVMMINI2.LEDsRegisters.LED_ALARM, true);
            Delay(2000, "Capturando imagen");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "CVMMINI2_LEDS", "LEDS", "CVMMINI2");

            Mini2.WriteLEDS(CVMMINI2.LEDsRegisters.LED_CPU, false);
            Mini2.WriteLEDS(CVMMINI2.LEDsRegisters.LED_ALARM, false);
        }

        public virtual void TestShortCircuit(int DelFirst = 2, int NumIteraciones = 3, int TimeInterval = 2000 )
        {
            // Cruce pistas sin consignas
            var V_offset = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V);
            var I_offset = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.A);

            var offsetList = new List<TriAdjustValueDef>();
            offsetList.Add(V_offset);
            offsetList.Add(I_offset);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TestMeasureBase(offsetList,(Func<int, double[]>)((step) =>
                {
                    var Variables = this.Mini2.ReadAllVariables();
                    return new double[] {
                        Variables.Phases.L1.Voltage, Variables.Phases.L2.Voltage, Variables.Phases.L3.Voltage,
                        Variables.Phases.L1.Current, Variables.Phases.L1.Current, Variables.Phases.L1.Current, };
                }), DelFirst, NumIteraciones, TimeInterval);

            var voltage_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 60, ParamUnidad.V);
            var voltage_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 120, ParamUnidad.V);
            var voltage_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 180, ParamUnidad.V);

            var current_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var current_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var current_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var consignas_V = new TriLineValue { L1 = voltage_L1, L2 = voltage_L2, L3 = voltage_L3 };
            var consignas_I = new TriLineValue { L1 = current_L1, L2 = current_L2, L3 = current_L3 };
            var desfase = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };

            // Cruce pistas con consignas de Voltaje e intensidades nulas (I+D: Se tiene que realizar por separado)
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(consignas_V, 0, 50, 0, desfase, true);

            var CrucePistas_V_Ioffset = new TriAdjustValueDef(Params.I.L1.EnVacio.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.A);
            var crucePistas_V = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistas_V.L1.Average = voltage_L1;
            crucePistas_V.L2.Average = voltage_L2;
            crucePistas_V.L3.Average = voltage_L3;

            var verif_CrucePistas_V = new List<TriAdjustValueDef>(); 
            verif_CrucePistas_V.Add(I_offset);
            verif_CrucePistas_V.Add(crucePistas_V);

            TestMeasureBase(verif_CrucePistas_V,(Func<int, double[]>)((step) =>
               {
                   var Variables = this.Mini2.ReadAllVariables();
                   return new double[] {
                        Variables.Phases.L1.Current, Variables.Phases.L2.Current, Variables.Phases.L3.Current,
                        Variables.Phases.L1.Voltage, Variables.Phases.L2.Voltage, Variables.Phases.L3.Voltage, };
               }), DelFirst, NumIteraciones, TimeInterval);

            // Cruce pistas con consignas de Intensidades y voltajes nulos (I+D: Se tiene que realizar por separado)
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, consignas_I, 50, 0, desfase, true);

            var currentRelation = Configuracion.GetDouble("RELACIÓN_TRANSFORMACIÓN", 1, ParamUnidad.SinUnidad);

            var CrucePistas_I_Voffset = new TriAdjustValueDef(Params.V.L1.EnVacio.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V);
            var crucePistas_I = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A);
            crucePistas_I.L1.Average = current_L1 * currentRelation;
            crucePistas_I.L2.Average = current_L2 * currentRelation;
            crucePistas_I.L3.Average = current_L3 * currentRelation;

            var verif_CrucePistas_I = new List<TriAdjustValueDef>();
            verif_CrucePistas_I.Add(crucePistas_I);
            verif_CrucePistas_I.Add(V_offset);

            TestMeasureBase(verif_CrucePistas_I,(Func<int, double[]>)((step) =>
                {
                    var Variables = this.Mini2.ReadAllVariables();
                    return new double[] {
                        Variables.Phases.L1.Current, Variables.Phases.L2.Current, Variables.Phases.L3.Current,
                        Variables.Phases.L1.Voltage, Variables.Phases.L2.Voltage, Variables.Phases.L3.Voltage, };
                }), DelFirst, NumIteraciones, TimeInterval);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();  
        }

        public void TestDigitalInputs()
        { 
            Tower.IO.DO.Off(OUT_DIGITAL_IN);
            Delay(1500, "Preparacion test de Digital Inputs");

            SamplerWithCancel((p) =>
            {
                if (Mini2.ReadDigitalInput() == CVMMINI2.InputsState.OFF)
                    return true;
                return false;
            }, "ERROR. Se detecta la entrada digital del equipo activada cuando no deberia", 5, 200);

            Tower.IO.DO.On(OUT_DIGITAL_IN);
            SamplerWithCancel((p) =>
            {
                return Mini2.ReadDigitalInput() == CVMMINI2.InputsState.ON;                

            }, "ERROR. No se ha detectado la entrada digital activada", 5, 200);
            Tower.IO.DO.Off(OUT_DIGITAL_IN);

            Resultado.Set("DIGITAL_INPUT", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                var valueInput = Mini2.ReadDigitalInput();
                if (valueInput == 0x00)
                    return true;
                return false;

            }, "ERROR. Se detecta la entrada digital del equipo activada cuando no deberia, despues de probarlas", 5, 200);
        }

        public void TestKeyboard()
        {
            Tower.IO.DO.OffRange(PISTON_LEFT_KEY, PISTON_RIGHT_KEY);

            var keysValue = new Dictionary<byte, CVMMINI2.Keys>();
            keysValue.Add(PISTON_LEFT_KEY, CVMMINI2.Keys.TECLA_IZQUIERDA);
            keysValue.Add(PISTON_CENTRAL_KEY, CVMMINI2.Keys.TECLA_CENTRAL);
            keysValue.Add(PISTON_RIGHT_KEY, CVMMINI2.Keys.TECLA_DERECHA);

            SamplerWithCancel((p) =>
            {
                if (Mini2.ReadKeyboard() == CVMMINI2.Keys.NO_KEY)
                    return true;
                return false;
            }, "ERROR. Se detecta una tecla activada cuando no deberia", 5, 200);

            foreach (KeyValuePair<byte, CVMMINI2.Keys> input in keysValue)
            {
                Tower.IO.DO.On(input.Key);
                Delay(500, "Esperando activacion de la tecla");

                SamplerWithCancel((p) =>
                {
                    return Mini2.ReadKeyboard() == input.Value;
                }, string.Format("ERROR comprovacion tecla. No se detecta la {0} activada", input.Value.ToString()), 5, 200);

                Tower.IO.DO.Off(input.Key);

                Resultado.Set(input.Value.ToString(), "OK", ParamUnidad.SinUnidad);
            }

            SamplerWithCancel((p) =>
            {
                var valueKey = Mini2.ReadKeyboard();
                if (valueKey == CVMMINI2.Keys.NO_KEY)
                    return true;
                return false;

            }, "ERROR. Se detecta una tecla del equipo activada cuando no deberia, despues de probarlas", 5, 200);
        }

        public virtual void TestCalibration()
        {
            Mini2.FlagTest();

            var voltageAjust = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentAjust = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 4, ParamUnidad.A);
            var powerFactorAjust = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltageAjust, currentAjust, 0, powerFactorAjust, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var listCalibrationStateError = new Dictionary<CVMMINI2.CalibrationState, CVMMINI2.ErrorVariablesCalibration>();
            listCalibrationStateError.Add(CVMMINI2.CalibrationState.ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR);
            listCalibrationStateError.Add(CVMMINI2.CalibrationState.ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR);

            var listSumatoriesError = new Dictionary<CVMMINI2.ErrorSumatorio, CVMMINI2.ErrorVariablesCalibration>();
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_MIN, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_MIN);
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_PROMEDIO_INFERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_PROMEDIO_INFERIOR);
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_PROMEDIO_SUPERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_PROMEDIO_SUPERIOR);
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_MAX, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_MAX);

            Mini2.WriteAutoCalibration(true);

            bool calibration = false;
            List<string> ErrorMessageList = new List<string>();

            try
            {
                SamplerWithCancel((p) =>
                {
                    var state = Mini2.ReadCalibrationState();

                    if (state == CVMMINI2.CalibrationState.FINALIZACION_CORRECTA_CALIBRACION)
                    {
                        logger.Debug(string.Format("Estado Calibracion : {0}", state.ToString()));
                        calibration = true;
                    }

                    else
                    {
                        logger.Debug(string.Format("Estado Calibracion : {0}", state.ToString()));
                        ErrorMessageList.Clear();

                        if((int)state > 109 && (int)state < 126)
                            foreach (var errorSum in listSumatoriesError)
                                if (((CVMMINI2.ErrorSumatorio)state - 110).HasFlag(errorSum.Key))
                                {
                                    var errorsVars = Mini2.ReadParametersErrors(errorSum.Value);
                                    foreach (CVMMINI2.ErrorParametersCalibration errorVar in Enum.GetValues(typeof(CVMMINI2.ErrorParametersCalibration)))
                                        if (errorsVars.HasFlag(errorVar))
                                            ErrorMessageList.Add(string.Format("{0} en {1}", errorSum.Key.ToString().Replace("_", " "), errorVar.ToString().Replace("_", " ")));
                                }

                        foreach (var errorState in listCalibrationStateError)
                            if (state.HasFlag(errorState.Key))
                                if (state != CVMMINI2.CalibrationState.FASE_CALIBRACION_1 && state != CVMMINI2.CalibrationState.FASE_CALIBRACION_2 && state != CVMMINI2.CalibrationState.FASE_CALIBRACION_3)
                                {
                                    var errorsVars = Mini2.ReadParametersErrors(errorState.Value);                                    
                                    foreach (CVMMINI2.ErrorParametersCalibration errorVar in Enum.GetValues(typeof(CVMMINI2.ErrorParametersCalibration)))
                                        if (errorsVars.HasFlag(errorVar))
                                            ErrorMessageList.Add(string.Format("{0} en {1}", errorState.Key.ToString().Replace("_", " "), errorVar.ToString().Replace("_", " ")));
                                }
                    }
                    if (calibration)
                        return true;
                    else
                        return false;
                }, string.Empty, 60, 500);
            }
            finally
            {
                Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

                ReadGainsAndSetToResult();
                ReadErrorsAndSetToResult();

                string ErrorMessageString = string.Empty;

                if (!calibration && ErrorMessageList.Count > 0)
                {
                    foreach (var errorMsg in ErrorMessageList)
                        ErrorMessageString += errorMsg + "\r\n";
                    Error().UUT.CALIBRACION.MARGENES(ErrorMessageString).Throw();
                }                    
            }        
        }

        public void TestVerification(int initCount = 2, int samples = 3, int timeInterval = 1100)
        {
            Mini2.FlagTest();

            var voltageRef = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var currentRef = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 45, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltageRef, currentRef, 0, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();
            var toleranciaKvar = Params.KVAR.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.KVAR.L1.Verificacion.Name, 0, 0, 0, toleranciaKvar, ParamUnidad.Kvar));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, toleranciaI, ParamUnidad.A));

            TestCalibracionBase(defs,(Func<double[]>)(() =>
                {
                    var vars = this.Mini2.ReadAllVariables().Phases;

                    return new double[] {
                    vars.L1.activePower,  vars.L2.activePower, vars.L3.activePower,
                    vars.L1.inductiveReactivePower, vars.L2.inductiveReactivePower, vars.L3.inductiveReactivePower,
                    vars.L1.Voltage, vars.L2.Voltage ,vars.L3.Voltage,
                    vars.L1.Current , vars.L2.Current , vars.L3.Current };  // factor de potencias, cos (), THDs (<5), trifásicos
                }),(Func<double[]>)(() =>
                {                                       
                    var currentRelation = Configuracion.GetDouble("RELACIÓN_TRANSFORMACIÓN", 1, ParamUnidad.SinUnidad);

                    var ReadMTEVoltage = this.Tower.PowerSourceIII.ReadVoltage();
                    var ReadMTECurrent = this.Tower.PowerSourceIII.ReadCurrent() * currentRelation;

                    var power = TriLinePower.Create((TriLineValue)ReadMTEVoltage, (TriLineValue)ReadMTECurrent, powerFactor);

                    var varsList = new System.Collections.Generic.List<double>() {
                        power.KW.L1, power.KW.L2, power.KW.L3,
                        power.KVAR.L1, power.KVAR.L2, power.KVAR.L3,
                        ReadMTEVoltage.L1, ReadMTEVoltage.L2, ReadMTEVoltage.L3,
                        ReadMTECurrent.L1, ReadMTECurrent.L2, ReadMTECurrent.L3};

                    return varsList.ToArray();

                }), initCount, samples, timeInterval);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.Name, 0, 0, 0, 0, Params.FREQ.Null.Verificacion.Tol(), ParamUnidad.Hz);
            TestCalibracionBase(defsFreq,(Func<double>)(() =>
                {
                    var frecuency = this.Mini2.ReadAllVariables().ThreePhase.Frequency;
                    return (double)frecuency;
                }),(Func<double>)(() =>
                {
                    var ReadPaternFreq = this.Tower.CvmB100Low.ReadVariablesTrifasicas().Frecuencia;                                       
                    return (double)ReadPaternFreq;

                }), initCount, samples, timeInterval, "Error en la verificación de la frecuencia");

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public virtual void TestCustomization()
        {
            Mini2.FlagTest();                   
            Mini2.WriteSerialNumber(TestInfo.NumSerie);
            Delay(250, "Escribiendo numero de Producción");

            modelo = Mini2.ReadModelName();

            Mini2.WriteBacnetRegisters(TestInfo.BacNetID.Value);
            Delay(1500, "Escribiendo BacnetID en el equipo");

            if (modelo != "CIRCUTOR")
                Mini2.WriteBacnetConfiguration();

            var alarm1 = new CVMMINI2.AlarmConfiguration32();
            alarm1.Hi = (uint)Parametrizacion.GetDouble("VALOR_HIGH_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm1.Lo = (uint)Parametrizacion.GetDouble("VALOR_LOW_ALARMA", 0, ParamUnidad.SinUnidad);
            Mini2.WriteAlarm32bits(alarm1);
            Delay(500, "Escribiendo configuración alarma 1");

            var alarm2 = new CVMMINI2.AlarmConfiguration16();
            alarm2.code = (ushort)Parametrizacion.GetDouble("CODIGO_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.delay = (ushort)Parametrizacion.GetDouble("DELAY_ON_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.hysteresis = (ushort)Parametrizacion.GetDouble("HISTERESIS_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.latch = (ushort)Parametrizacion.GetDouble("LATCH_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.delayOff = (ushort)Parametrizacion.GetDouble("DELAY_OFF_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.State = (ushort)Parametrizacion.GetDouble("ESTADO_CONTACTO_ALARMA", 0, ParamUnidad.SinUnidad);
            Mini2.WriteAlarm16bits(alarm2);
            Delay(500, "Escribiendo configuración alarma 2");

            //Grabamos Relaciones de Transformación
            //----------------------------------------
           var rel_trans_Prim_V = Parametrizacion.GetString("REL_TRANS_PRIM_V", "DEFAULT", ParamUnidad.SinUnidad);
           var rel_trans_Prim_I = Parametrizacion.GetString("REL_TRANS_PRIM_I", "DEFAULT", ParamUnidad.SinUnidad);
           var rel_trans_Sec_V = Parametrizacion.GetString("REL_TRANS_SEC_V", "DEFAULT", ParamUnidad.SinUnidad);
           var rel_trans_Sec_I = Parametrizacion.GetString("REL_TRANS_SEC_I", "DEFAULT", ParamUnidad.SinUnidad);

           var valueRelTransDefault = Mini2.ReadTransformationRatio();

            if (rel_trans_Prim_V.Trim() != "DEFAULT")
                valueRelTransDefault.primaryVoltage = Convert.ToUInt32(rel_trans_Prim_V);

            if (rel_trans_Prim_I.Trim() != "DEFAULT")
                valueRelTransDefault.primaryCurrent = Convert.ToUInt16(rel_trans_Prim_I);

            if (rel_trans_Sec_V.Trim() != "DEFAULT")
                valueRelTransDefault.secundaryVoltage = Convert.ToUInt16(rel_trans_Sec_V);

            if (rel_trans_Sec_I.Trim() != "DEFAULT")
                valueRelTransDefault.secundaryCurrent = Convert.ToUInt16(rel_trans_Sec_I);

            Mini2.WriteTransformationRatio(valueRelTransDefault);
            Delay(2000, "Realizando grabacion relación transformación");
            //----------------------------------------

            Mini2.CleanAllParameters(CVMMINI2.CleanParametersRegisters.CLEAN_ALL);
            Delay(8000, "Realizando borrado de registros");

            Tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(3000, "Realizando reset HW");
            Tower.Chroma.ApplyAndWaitStabilisation();
            Delay(5000, "Estabilizando comms del equipo");

            SamplerWithCancel((p) =>
            {
                Mini2.FlagTest();
                return true;
            }, "Error de comunicaciones. No se ha podido establecer el modo test en el equipo", 10, 500, 1000, false, false);

            var modelReading = Mini2.ReadModelName();
            modelReading = string.IsNullOrEmpty(modelReading) ? "NEUTRO" : modelReading;
            Assert.AreEqual("MODELO_FINAL", modelReading, Configuracion.GetString("MODELO_EQUIPO", "NA", ParamUnidad.SinUnidad), Error().UUT.CONFIGURACION.NO_GRABADO("Error, modelo de cliente mal grabado"), ParamUnidad.SinUnidad);

            var bacnetID = Mini2.ReadBacnetID();
            Assert.AreEqual(ConstantsParameters.TestInfo.BACNET_ID, bacnetID.ToString(), TestInfo.BacNetID.ToString(), Error().UUT.CONFIGURACION.NO_GRABADO("Error. El numero de bacnet leido no coincide con el grabado"), ParamUnidad.SinUnidad);
           
            var numSerieReaded = Mini2.ReadSerialNumber();          
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numSerieReaded, TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El numero de serie leido no coincide con el grabado"), ParamUnidad.SinUnidad);
        }

        public virtual void TestFinish()
        {
            if(Mini2 != null)
                Mini2.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();

                if (Tower.IO != null)
                {
                    Tower.IO.DO.Off(PISTON_LEFT_KEY, PISTON_CENTRAL_KEY, PISTON_RIGHT_KEY, OUT_DIGITAL_IN, DISCONNECT_DATAMAN);
                    Tower.IO.DO.OffWait(700, BLOQUE_GRABACION);                   
                    Tower.IO.DO.OffWait(300, BORNAS_J2_J3);
                    Tower.IO.DO.OffWait(500, BORNAS_J5_J8);
                    Tower.IO.DO.OffWait(700, FIJACION_EQUIPO);
                    Tower.IO.DO.OffWait(1500, SELEC_BORNAS_ETH);
                    Tower.IO.DO.OffWait(300, AUX_EV);
                    Tower.IO.DO.OffWait(700, FIJACION_CARRO);
                }

                Tower.Dispose();
            }
        }

        //ETHERNET WIFI


        internal void ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType typeSumatorie, CVMMINI2.Scale scaleName)
        {
            var sums = Mini2.ReadSumatories(typeSumatorie);

            Resultado.Set(string.Format("SUMATORIO_{0}_V1_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.V1, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_V2_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.V2, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_V3_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.V3, ParamUnidad.Puntos);

            Resultado.Set(string.Format("SUMATORIO_{0}_I1_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.I1, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_I2_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.I2, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_I3_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.I3, ParamUnidad.Puntos);

            Resultado.Set(string.Format("SUMATORIO_{0}_V12_{1}", typeSumatorie.ToString(), scaleName,ToString()), sums.V12, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_V23_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.V23, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_V31_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.V31, ParamUnidad.Puntos);

            Resultado.Set(string.Format("SUMATORIO_{0}_FREQ_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.Freq, ParamUnidad.Puntos);

            Resultado.Set(string.Format("SUMATORIO_{0}_kW1_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.KW1, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_kW2_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.KW2, ParamUnidad.Puntos);
            Resultado.Set(string.Format("SUMATORIO_{0}_kW3_{1}", typeSumatorie.ToString(), scaleName.ToString()), sums.KW3, ParamUnidad.Puntos);
        }

        internal void ReadGainsAndSetToResult()
        {
            var gainsVI = Mini2.ReadGainsVI();

            Resultado.Set("GANANCIA_V1", gainsVI.Tension.L1, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_V2", gainsVI.Tension.L2, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_V3", gainsVI.Tension.L3, ParamUnidad.Puntos);

            Resultado.Set("GANANCIA_I1", gainsVI.Current.L1, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_I2", gainsVI.Current.L2, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_I3", gainsVI.Current.L3, ParamUnidad.Puntos);

            Resultado.Set("GANANCIA_V12", gainsVI.CompoundTension.L12, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_V23", gainsVI.CompoundTension.L23, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_V31", gainsVI.CompoundTension.L31, ParamUnidad.Puntos);

            var gainsPower = Mini2.ReadPowerGains();

            Resultado.Set("GANANCIA_kW1", gainsPower.L1, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_kW2", gainsPower.L2, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_kW3", gainsPower.L3, ParamUnidad.Puntos);

            var gainsPhaseAjust = Mini2.ReadPhaseAjustGains();

            Resultado.Set("GANANCIA_AJUSTE_DESFASE1", gainsPhaseAjust.L1, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_AJUSTE_DESFASE2", gainsPhaseAjust.L2, ParamUnidad.Puntos);
            Resultado.Set("GANANCIA_AJUSTE_DESFASE3", gainsPhaseAjust.L3, ParamUnidad.Puntos);

            var gainFrequency = Mini2.ReadSynchFrequencyGain();

            Resultado.Set("GANANCIA_FRECUENCIA_SYNC", gainFrequency, ParamUnidad.Puntos);
        }

        internal void ReadErrorsAndSetToResult()
        {
            var errorSumMax = Mini2.ReadErrorVariables(CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_MAX);
            var errorSumMin = Mini2.ReadErrorVariables(CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_MIN);
            var errorSumPromSup = Mini2.ReadErrorVariables(CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_PROMEDIO_SUPERIOR);
            var errorSumPromInf = Mini2.ReadErrorVariables(CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_PROMEDIO_INFERIOR);
            var errorValorMedidoPromSup = Mini2.ReadErrorVariables(CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR);
            var errorValorMedidoPromInf = Mini2.ReadErrorVariables(CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR);

            Resultado.Set("VARIABLE_ERROR_SUMATORIO_MAX", errorSumMax, ParamUnidad.PorCentage);
            Resultado.Set("VARIABLE_ERROR_SUMATORIO_MIN", errorSumMin, ParamUnidad.PorCentage);
            Resultado.Set("VARIABLE_ERROR_SUMATORIO_PROMEDIO_SUPERIOR", errorSumPromSup, ParamUnidad.PorCentage);
            Resultado.Set("VARIABLE_ERROR_SUMATORIO_PROMEDIO_INFERIOR", errorSumPromInf, ParamUnidad.PorCentage);
            Resultado.Set("VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR", errorValorMedidoPromSup, ParamUnidad.PorCentage);
            Resultado.Set("VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR", errorValorMedidoPromInf, ParamUnidad.PorCentage);

            var erroresMedida = Mini2.ReadMeasureError();

            Resultado.Set("ERROR_MEDIDO_V1", erroresMedida.errorV1, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_V2", erroresMedida.errorV2, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_V3", erroresMedida.errorV3, ParamUnidad.PorCentage);

            Resultado.Set("ERROR_MEDIDO_I1", erroresMedida.errorI1, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_I2", erroresMedida.errorI2, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_I3", erroresMedida.errorI3, ParamUnidad.PorCentage);

            Resultado.Set("ERROR_MEDIDO_V12", erroresMedida.errorV12, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_V23", erroresMedida.errorV23, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_V31", erroresMedida.errorV31, ParamUnidad.PorCentage);

            Resultado.Set("ERROR_MEDIDO_FREQ", erroresMedida.errorFreq, ParamUnidad.PorCentage);

            Resultado.Set("ERROR_MEDIDO_kW1", erroresMedida.errorkW1, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_kW2", erroresMedida.errorkW2, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_kW3", erroresMedida.errorkW3, ParamUnidad.PorCentage);

            Resultado.Set("ERROR_MEDIDO_kVAr1", erroresMedida.errorkvar1, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_kVAr2", erroresMedida.errorkvar2, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_kVAr3", erroresMedida.errorkvar3, ParamUnidad.PorCentage);

            Resultado.Set("ERROR_MEDIDO_DESFASE_1", erroresMedida.errorDesfase1, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_DESFASE_2", erroresMedida.errorDesfase2, ParamUnidad.PorCentage);
            Resultado.Set("ERROR_MEDIDO_DESFASE_3", erroresMedida.errorDesfase3, ParamUnidad.PorCentage);
        }
    }
}