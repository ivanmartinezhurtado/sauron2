﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using TaskRunner.Enumerators;

namespace TaskRunner.Model
{
    public class TestResult
    {
        public StepResult InitStepResult { get; set; }
        public StepResult MainStepResult { get; set; }
        public StepResult EndStepResult { get; set; }
        public DateTime StartTime { get { return InitStepResult.StartTime; } }
        public DateTime EndTime { get { return EndStepResult.EndTime; } }
        public TimeSpan Duration { get { return EndTime - StartTime; } }
        public RunMode RunMode { get; internal set; }
        public int NumInstance { get; set; }
        public TestExecutionResult ExecutionResult
        {
            get
            {
                if (InitStepResult.ExecutionResult == StepExecutionResult.Passed &&
                    MainStepResult.ExecutionResult == StepExecutionResult.Passed)
                    return TestExecutionResult.Completed;
                else if (InitStepResult?.ExecutionResult == StepExecutionResult.Aborted ||
                         MainStepResult?.ExecutionResult == StepExecutionResult.Aborted)
                    return TestExecutionResult.Aborted;
                else
                    return TestExecutionResult.Faulted;
            }
        }
        public ReadOnlyCollection<StepResult> StepResults
        {
            get
            {
                List<StepResult> steps = new List<StepResult>();
                steps.AddRange(InitStepResult.Flatten(null, null).ToList());
                steps.AddRange(MainStepResult.Flatten(null, null).ToList());
                steps.AddRange(EndStepResult.Flatten(null, null).ToList());
                return steps.AsReadOnly();
            }
        }

        public TestResult(SequenceContext sequenceContext)
        {
            NumInstance = sequenceContext.NumInstance;
            InitStepResult = new StepResult(sequenceContext.NumInstance, sequenceContext.Sequence.Init, Thread.CurrentThread.ManagedThreadId, SectionEnum.Init, null, 0);
            MainStepResult = new StepResult(sequenceContext.NumInstance, sequenceContext.Sequence.Main, Thread.CurrentThread.ManagedThreadId, SectionEnum.Main, null, 0);
            EndStepResult = new StepResult(sequenceContext.NumInstance, sequenceContext.Sequence.End, Thread.CurrentThread.ManagedThreadId, SectionEnum.End, null, 0);
        }

        public IEnumerable<StepResult> GetSteps(StepExecutionResult executionResult)
        {
            List<StepResult> faultedSteps = new List<StepResult>();
            faultedSteps.AddRange(InitStepResult.GetSteps(executionResult));
            faultedSteps.AddRange(MainStepResult.GetSteps(executionResult));
            faultedSteps.AddRange(EndStepResult.GetSteps(executionResult));
            return faultedSteps;
        }
    }
}
