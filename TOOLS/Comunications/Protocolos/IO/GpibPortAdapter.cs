﻿using Ivi.Visa.Interop;
using System;
using System.Diagnostics;

namespace Comunications.Message
{
    public class GpibPortAdapter : IStreamResource
    {
        private FormattedIO488 _Gpib;

        public GpibPortAdapter(FormattedIO488 GpibPort)
        {
            Debug.Assert(GpibPort != null, "Argument GpibPort cannot be null.");
            _Gpib = GpibPort;
        }

        public int InfiniteTimeout
        {
            get { return System.Threading.Timeout.Infinite; }
        }

        public int ReadTimeout
        {
            get { return _Gpib.IO.Timeout; }
            set { _Gpib.IO.Timeout = value; }
        }

        public int WriteTimeout
        {
            get { return _Gpib.IO.Timeout; }
            set { _Gpib.IO.Timeout = value; }
        }

        public void DiscardInBuffer()
        {
            _Gpib.FlushRead();
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            byte[] frame = _Gpib.IO.Read(count);
            frame.CopyTo(buffer, 0);
            System.Threading.Thread.Sleep(100);
            return frame.Length;
        }

        public string ReadLineString()
        {
            string result = _Gpib.ReadString();
            if (result != null && result.EndsWith("\n"))
                result = result.Substring(0, result.Length - 1);

            System.Threading.Thread.Sleep(100);

            return result;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            _Gpib.IO.Write(buffer, count);
            System.Threading.Thread.Sleep(100);
        }

        public void HandleMessageException(Exception ex)
        {
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public string PortName
        {
            get
            {
                return _Gpib.IO.HardwareInterfaceName;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int ReadBufferSize
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public void WriteLineString(string tx)
        {
            _Gpib.IO.WriteString(tx);
        }

        public void WriteLineStringAsBytes(string tx, int byteInterval = 50, bool writeCR = true, bool writeLF = false)
        {
            throw new NotImplementedException();
        }
    }
}
