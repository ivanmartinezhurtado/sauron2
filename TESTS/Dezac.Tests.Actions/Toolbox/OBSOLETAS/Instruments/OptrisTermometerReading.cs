﻿using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.InstrumentsTools
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(OptrisTermometerReadingActionDescription))]
   
    public class OptrisTermometerReading : ActionBase
    {
        public string PortComName { get; set; }

        public string ResultName { get; set; }
        public int Retries { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            if (string.IsNullOrEmpty(PortComName))
                throw new Exception("No se ha pasado el puerto del Thermometro Optris");

            var optrisPort = Convert.ToByte(testBase.GetVariable<string>(PortComName, PortComName).ToUpper().Replace("COM", ""));

            testBase.TestOptris(optrisPort, ResultName, Retries);
        }
    }

    public class OptrisTermometerReadingActionDescription : ActionDescription
    {
        public override string Name { get { return "ReadOptrisTermometer"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Letter_O_dg_icon; } }
    }

}
