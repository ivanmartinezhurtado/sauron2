﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.05)]
    public class CVMMINI : DeviceBase
    {

        public CVMMINI()
        {
        }

        public CVMMINI(int port)
        {
            SetPort(port, 1, 9600);
        }

        public CVMMINI(int port, byte periferico, int baudrate)
        {
            SetPort(port, periferico, baudrate);
        }

        public CVMMINI(int port, string host, byte periferico)
        {
            SetPortAndHost(port, host, periferico);
        }

        public void SetPort(int port, byte periferico, int baudrate)
        {
            Modbus = new ModbusDeviceSerialPort(port, baudrate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Modbus.PerifericNumber = periferico;
        }

        public void SetPortAndHost(int port, string host, byte periferico)
        {
            Modbus = new ModbusDeviceTCP(host, port, ModbusTCPProtocol.TCP, _logger);
            Modbus.PerifericNumber = periferico;
        }

        public ModbusDevice Modbus { get; set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Operaciones Modbus de lectura

        public AllVariables ReadAllVariables()
        {
            LogMethod();
            var Fases = Modbus.Read<VariablesInstantaneas>();
            var Trifasicas = Modbus.Read<VariablesTrifasicas>();
            var THD = Modbus.Read<VariablesTHD>();

            AllVariables All = new AllVariables();
            All.Phases = Fases;
            All.Trifasicas = Trifasicas;
            All.THD = THD;

            return All;
        }

        public List<Outputs> ReadOutputState()
        {
            LogMethod();

            var outputs = Modbus.ReadMultipleCoil((ushort)Registers.OUTPUTS, 8);

            var outputList = new List<Outputs>();

            foreach (Outputs output in Enum.GetValues(typeof(Outputs)))
                if ((outputs[0] & (byte)output) == (byte)output)
                    outputList.Add(output);

            return outputList;
        }

        public Key ReadKeyboard()
        {
            LogMethod();

            var keyread = Modbus.ReadMultipleCoil((ushort)Registers.KEYBOARD, 8);

            Key key = (Key)keyread[0];

            return key;
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.SOFTWARE_VERSION, 3).CToNetString();
        }

        public PasswordConfig ReadPasswordConfiguration()
        {
            LogMethod();
            return Modbus.Read<PasswordConfig>();
        }

        public SetupConfig ReadSetupConfig()
        {
            LogMethod();
            return Modbus.Read<SetupConfig>();
        }

        public ScreenConfiguration ReadScreenConfig()
        {
            LogMethod();
            return Modbus.Read<ScreenConfiguration>();
        }

        public AlarmConfig ReadAlarmConfig(Alarms alarm)
        {
            LogMethod();
            return Modbus.Read<AlarmConfig>((ushort)alarm);
        }

        public string ReadInitialMessage()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.INITIAL_SCREEN_NAME, 4);
        }

        public uint ReadSerialNumber()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public MaximumDemandConfiguration ReadMaximumDemandConfiguration()
        {
            LogMethod();
            return Modbus.Read<MaximumDemandConfiguration>();
        }

        public EEPROMErrorCode ReadEEPromErrors()
        {
            LogMethod();
            var a = Modbus.ReadRegister((ushort)Registers.EEPROM_ERROR_CODE);

            return (EEPROMErrorCode)Modbus.ReadRegister((ushort)Registers.EEPROM_ERROR_CODE);
        }

        public MeasureGains ReadCalibrationFactors()
        {
            LogMethod();
            return Modbus.Read<MeasureGains>((ushort)Registers.CALIBRATION_FACTORS);
        }

        public CompoundVoltageGains ReadCompoundPhaseFactors()
        {
            LogMethod();
            return Modbus.Read<CompoundVoltageGains>((ushort)Registers.COMPOUND_GAIN_FACTORS);
        }

        public PhaseITFGains ReadITFCalibrationFactors()
        {
            LogMethod();
            return Modbus.Read<PhaseITFGains>((ushort)Registers.ITF_PHASE_ADJUST);
        }

        public ConverterPoints ReadConverterPoints()
        {
            LogMethod();
            return Modbus.Read<ConverterPoints>((ushort)Registers.CONVERTER_POINTS);
        }

        public ChannelOffsets ReadChannelOffsets()
        {
            LogMethod();
            return Modbus.Read<ChannelOffsets>();
        }

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();

            var hardwareVectorReading = Modbus.ReadMultipleCoil((ushort)Registers.HARDWARE_CONFIGURATION, 96);

            return new HardwareVector(hardwareVectorReading);
        }

        public Energies ReadEnergies()
        {
            LogMethod();
            return Modbus.Read<Energies>();
        }

        public ushort ReadROMCode()
        {
            LogMethod();
            return (ushort)Modbus.ReadRegister((ushort)Registers.ROM_CODE);
        }

        public Comunicaciones ReadCommunicationConfig()
        {
            LogMethod();
            return Modbus.Read<Comunicaciones>();
        }

        public string ReadBacnetID()
        {
            LogMethod();
            var version = (Modbus.ReadRegister((ushort)Registers.BACNET_FIRMWARE) & 0xFF00) >> 8 ;
            return version.ToString();
        }

        public FirmwareEthenet ReadFirmwareEth()
        {
            LogMethod();
            return Modbus.Read<FirmwareEthenet>();
        }

        #endregion

        #region Operaciones Modbus de escritura 

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void Reset()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public void WriteOutputState(Outputs output, State state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)outputDictionary[output], state == State.ON ? true : false);
        }

        public void WriteLedState(LedsInformation leds)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.LEDS_AND_BACKLIGHT, leds.ToBoolArray());
        }

        public void WriteClearEnergy()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_ENERGY, true);
        }

        public void WriteDemandInitialization()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.INITIALIZE_MAXIMUM_DEMAND, true);
        }

        public void WriteClearMaxMin()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_MAX_MIN, true);
        }

        public void WriteClearAll()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_ALL, true);
        }

        public void WriteMaximumDemandClear()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_MAXIMUM_MD, true);
        }

        public void WriteDisplay(params bool[] segments)
        {
            LogMethod();
            var retries = Modbus.Retries;
            Modbus.Retries = 0;
            try
            {
                Modbus.WriteMultipleCoil((ushort)Registers.DISPLAY_SEGMENTS, segments);
            }
            catch (Exception e)
            {
                _logger.Warn(String.Format("Excepcion Capturada: {0}", e.Message));
            }

            Modbus.Retries = retries;
        }

        public void WriteScreenConfiguration(ScreenConfiguration screen)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.SCREEN_SELECTION, screen.ToBoolArray());
        }

        public void WriteHardwareVector(HardwareVector hardwareConfig)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_CONFIGURATION, hardwareConfig.ToSingleCoil());
        }

        private void ApplyCalibration()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.APPLY_CALIBRATION, true);
        }

        public void WritePasswordConfiguration(PasswordConfig password)
        {
            LogMethod();
            Modbus.Write<PasswordConfig>(password);
        }

        public void WriteSetupConfiguration(SetupConfig setup)
        {
            LogMethod();
            Modbus.Write<SetupConfig>(setup);
        }

        public void WriteAlarmConfiguration(AlarmConfig alarmConfig, Alarms alarm)
        {
            LogMethod();
            Modbus.Write<AlarmConfig>((ushort)alarm, alarmConfig);
        }

        public void WriteMaximumDemandConfiguration(MaximumDemandConfiguration maximumDemand)
        {
            LogMethod();
            Modbus.Write<MaximumDemandConfiguration>(maximumDemand);
        }

        public void WriteInitialMessage(string message)
        {
            LogMethod();
            Modbus.WriteString((ushort)Registers.INITIAL_SCREEN_NAME, message);
        }

        public void WriteSerialNumber(UInt32 serialNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, (int)serialNumber);
        }

        public void WriteCalibrationFactors(MeasureGains factors)
        {
            LogMethod();
            Modbus.Write<MeasureGains>(factors);

            ApplyCalibration();
        }

        public void WriteCompoundPhaseFactors(CompoundVoltageGains factors)
        {
            LogMethod();
            Modbus.Write<CompoundVoltageGains>(factors);

            ApplyCalibration();
        }

        public void WriteITFCalibrationFactors(PhaseITFGains factors)
        {
            LogMethod();
            Modbus.Write<PhaseITFGains>(factors);
        }

        public void WriteEnergies(Energies energies)
        {
            LogMethod();
            Modbus.Write<Energies>(energies);
        }

        public void WriteCalibrationfactors(MeasureGains gains)
        {
            LogMethod();
            Modbus.Write<MeasureGains>(gains);
        }

        public void WriteBacnetID(int BacnetID)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.BACNET_ID, BacnetID);
        }

        public void WriteCommunicationConfig(Comunicaciones com)
        {
            LogMethod();
            Modbus.Write<Comunicaciones>(com);
        }

        public void WriteLCDTime(ushort time)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.LCD_OFF_TIME, time.ToBooleanArray().ToFunction0F());
        }

        public void WriteBacnetTemperatura(ushort temp)
        {
            LogMethod();
            Modbus.Write((ushort) Registers.BACNET_TEMPERATURA, temp);
        }

        public void WriteDisableComTCP()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DISABLE_COM_TCP, 2);
        }

        #endregion

        #region Methods

        public void SendDisplayInfo(DisplayState state)
        {
            WriteDisplay(displayInformation[state]);
        }

        #endregion

        #region Adjust methods

        public Tuple<bool, VoltageCurrentAndCompoundGains> CalculateAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, TriLineValue voltage, TriLineValue current, double transformationRelatio, double factorTension, Func<VoltageCurrentAndCompoundGains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = new VoltageCurrentAndCompoundGains(ReadCalibrationFactors(), ReadCompoundPhaseFactors());

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;

            //voltage.L1 *= factorTension;
            //voltage.L2 *= factorTension;
            //voltage.L3 *= factorTension;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 1000000000,
                () =>
                {
                    var allVariables = ReadConverterPoints();

                    var varlist = new List<double>(){ 
                       allVariables.VoltageL1 / allVariables.AmountOfSamples, 
                       allVariables.VoltageL2 / allVariables.AmountOfSamples, 
                       allVariables.VoltageL3 / allVariables.AmountOfSamples,

                       allVariables.CurrentL1 / allVariables.AmountOfSamples, 
                       allVariables.CurrentL2 / allVariables.AmountOfSamples, 
                       allVariables.CurrentL3 / allVariables.AmountOfSamples,

                       allVariables.VoltageL12 / allVariables.AmountOfSamples,
                       allVariables.VoltageL23 / allVariables.AmountOfSamples,
                       allVariables.VoltageL31 / allVariables.AmountOfSamples};

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.VoltageCurrent.VoltageL1 = Convert.ToUInt16(100 * Math.Pow(voltage.L1, 2) / (listValues.Average(0)) * 100);
                    gains.VoltageCurrent.VoltageL2 = Convert.ToUInt16(100 * Math.Pow(voltage.L2, 2) / (listValues.Average(1)) * 100);
                    gains.VoltageCurrent.VoltageL3 = Convert.ToUInt16(100 * Math.Pow(voltage.L3, 2) / (listValues.Average(2)) * 100);

                    gains.VoltageCurrent.CurrentL1 = Convert.ToUInt16(Math.Pow(current.L1 * 1000, 2) / (listValues.Average(3)) * 100);
                    gains.VoltageCurrent.CurrentL2 = Convert.ToUInt16(Math.Pow(current.L2 * 1000, 2) / (listValues.Average(4)) * 100);
                    gains.VoltageCurrent.CurrentL3 = Convert.ToUInt16(Math.Pow(current.L3 * 1000, 2) / (listValues.Average(5)) * 100);

                    gains.Compound.L12 = Convert.ToUInt16(100 * Math.Pow(voltage.L1 * Math.Sqrt(3), 2) / (listValues.Average(6)) * 100);
                    gains.Compound.L23 = Convert.ToUInt16(100 * Math.Pow(voltage.L2 * Math.Sqrt(3), 2) / (listValues.Average(7)) * 100);
                    gains.Compound.L31 = Convert.ToUInt16(100 * Math.Pow(voltage.L3 * Math.Sqrt(3), 2) / (listValues.Average(8)) * 100);

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, VoltageCurrentAndCompoundGains> CalculateAdjustMeasureFactorsWithEscala2(int delFirst, int initCount, int samples, int interval, TriLineValue voltage, TriLineValue current, double transformationRelatio, double factorTension, Func<VoltageCurrentAndCompoundGains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = new VoltageCurrentAndCompoundGains(ReadCalibrationFactors(), ReadCompoundPhaseFactors());

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;

            //voltage.L1 *= factorTension;
            //voltage.L2 *= factorTension;
            //voltage.L3 *= factorTension;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100000000,
                () =>
                {
                    var allVariables = ReadConverterPoints();

                    var varlist = new List<double>(){
                       allVariables.VoltageL1 / allVariables.AmountOfSamples,
                       allVariables.VoltageL2 / allVariables.AmountOfSamples,
                       allVariables.VoltageL3 / allVariables.AmountOfSamples,

                       allVariables.CurrentL1 / allVariables.AmountOfSamples,
                       allVariables.CurrentL2 / allVariables.AmountOfSamples,
                       allVariables.CurrentL3 / allVariables.AmountOfSamples,

                       allVariables.VoltageL12 / allVariables.AmountOfSamples,
                       allVariables.VoltageL23 / allVariables.AmountOfSamples,
                       allVariables.VoltageL31 / allVariables.AmountOfSamples};

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.VoltageCurrent.VoltageL1 = Convert.ToUInt16(100 * Math.Pow(voltage.L1, 2) / (listValues.Average(0)) * 100);
                    gains.VoltageCurrent.VoltageL2 = Convert.ToUInt16(100 * Math.Pow(voltage.L2, 2) / (listValues.Average(1)) * 100);
                    gains.VoltageCurrent.VoltageL3 = Convert.ToUInt16(100 * Math.Pow(voltage.L3, 2) / (listValues.Average(2)) * 100);

                    gains.VoltageCurrent.CurrentL1Scale2 = Convert.ToUInt16(Math.Pow(current.L1 * 1000, 2) / (listValues.Average(3)) * 100);
                    gains.VoltageCurrent.CurrentL2Scale2 = Convert.ToUInt16(Math.Pow(current.L2 * 1000, 2) / (listValues.Average(4)) * 100);
                    gains.VoltageCurrent.CurrentL3Scale2 = Convert.ToUInt16(Math.Pow(current.L3 * 1000, 2) / (listValues.Average(5)) * 100);

                    gains.Compound.L12 = Convert.ToUInt16(100 * Math.Pow(voltage.L1 * Math.Sqrt(3), 2) / (listValues.Average(6)) * 100);
                    gains.Compound.L23 = Convert.ToUInt16(100 * Math.Pow(voltage.L2 * Math.Sqrt(3), 2) / (listValues.Average(7)) * 100);
                    gains.Compound.L31 = Convert.ToUInt16(100 * Math.Pow(voltage.L3 * Math.Sqrt(3), 2) / (listValues.Average(8)) * 100);

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, MeasureGains> CalculateAdjustMeasureFactorsScale2(int delFirst, int initCount, int samples, int interval, TriLineValue current, double transformationRelatio, Func<MeasureGains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = ReadCalibrationFactors();

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 1000000000,
                () =>
                {
                    var allVariables = ReadConverterPoints();

                    var varlist = new List<double>(){ 
                       allVariables.CurrentL1 / allVariables.AmountOfSamples, 
                       allVariables.CurrentL2 / allVariables.AmountOfSamples, 
                       allVariables.CurrentL3 / allVariables.AmountOfSamples};

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    var cuadraticPointsConverter = ReadConverterPoints();

                    gains.CurrentL1Scale2 = Convert.ToUInt16(Math.Pow(current.L1 * 1000, 2) / (listValues.Average(0)) * 100);
                    gains.CurrentL2Scale2 = Convert.ToUInt16(Math.Pow(current.L2 * 1000, 2) / (listValues.Average(1)) * 100);
                    gains.CurrentL3Scale2 = Convert.ToUInt16(Math.Pow(current.L3 * 1000, 2) / (listValues.Average(2)) * 100);


                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, PhaseITFGains> CalculateGapAdjustFactors(int delFirst, int initCount, int samples, int interval, TriLineValue powerReference, double offsetReference, Func<PhaseITFGains, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            double angle = 0.0D;
            double angleError = 0.0D;
            double angleErrorCorrected = 0.0D;
            var result = new PhaseITFGains();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 150000000,
                () =>
                {
                    var electricalVariables = ReadAllVariables();

                    return new double[] { (electricalVariables.Phases.L1.PotenciaActiva - powerReference.L1) / powerReference.L1, 
                    (electricalVariables.Phases.L2.PotenciaActiva - powerReference.L2)  / powerReference.L2, 
                    (electricalVariables.Phases.L3.PotenciaActiva - powerReference.L3)  / powerReference.L3,};
                },
                (listValues) =>
                {
                    angle = (1 - listValues.Average(0)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (1.22E-6);
                    result.L1 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    angle = (1 - listValues.Average(1)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (1.22E-6);
                    result.L2 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    angle = (1 - listValues.Average(2)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (1.22E-6);
                    result.L3 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    return adjustValidation(result);
                }
                );

            return Tuple.Create(adjustResult.Item1, result);
        }

        #endregion

        #region Structures

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Phases;
            public VariablesTrifasicas Trifasicas;
            public VariablesTHD THD;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            public Int32 Tension;
            public Int32 Corriente;
            public Int32 PotenciaActiva;
            public Int32 PotenciaReactiva;
            public Int32 FactorPotencia;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x001E)]
        public struct VariablesTrifasicas
        {
            public Int32 PotenciaActivaIII;
            public Int32 PotenciaInductivaIII;
            public Int32 PotenciaCapacitivaIII;
            public Int32 CosenoPhiIII;
            public Int32 FactorPotenciaIII;
            public Int32 Frecuencia;
            public Int32 TensionLineaL1L2;
            public Int32 TensionLineaL2L3;
            public Int32 TensionLineaL3L1;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0030)]
        public struct VariablesTHD
        {
            public Int32 V1;
            public Int32 V2;
            public Int32 V3;
            public Int32 I1;
            public Int32 I2;
            public Int32 I3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SCREEN_SELECTION)]
        public struct ScreenConfiguration
        {
            public bool VoltagePhaseNeuter;
            public bool VoltagePhasePhase;
            public bool Current;
            public bool ActivePower;
            public bool ReactivePower;
            public bool ApparentPower;
            public bool PowerFactor;
            public bool VoltageTHD;
            public bool CurrentTHD;
            public bool TriPhasePower;
            public bool TriPhaseInductivePower;
            public bool TriPhaseCapacitivePower;
            public bool TriPhaseApparentPower;
            public bool NeuterCurrentFrequencyTemperature;
            public bool Pd;
            public bool ActivePowerPerHourConsumed;
            public bool InductivePowerPerHourConsumed;
            public bool CapacitivePowerPerHourConsumed;
            public bool ApparentPowerPerHourConsumed;
            public bool ActivePowerPerHourProduced;
            public bool InductivePowerPerHourProduced;
            public bool CapacitivePowerPerHourProduced;
            public bool ApparentPowerPerHourProduced;
            public bool VoltageHarmonics;
            public bool CurrentHarmonics;

            public bool[] ToBoolArray()
            {
                return new bool[] {
                    CurrentHarmonics,true,true,true,true,true,true,true,
                    InductivePowerPerHourConsumed, CapacitivePowerPerHourConsumed, ApparentPowerPerHourConsumed, ActivePowerPerHourProduced, InductivePowerPerHourProduced, CapacitivePowerPerHourProduced, ApparentPowerPerHourProduced, VoltageHarmonics,
                    CurrentTHD, TriPhasePower, TriPhaseInductivePower, TriPhaseCapacitivePower, TriPhaseApparentPower, NeuterCurrentFrequencyTemperature, Pd, ActivePowerPerHourConsumed,
                    VoltagePhaseNeuter, VoltagePhasePhase, Current, ActivePower, ReactivePower, ApparentPower, PowerFactor, VoltageTHD,
                };
            }  
                                
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.PASSWORD_CONFIG)]
        public struct PasswordConfig
        {
            public ushort PasswordNumber;
            public byte SetupLocking;
            private byte dummyByte;      
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP_CONFIG)]
        public struct SetupConfig
        {
            public int VoltagePrimary;
            public ushort VoltageSecondary;
            public ushort CurrentPrimary;
            public byte VoltageSimpleOrCompound;
            public byte DefaultPage;
            public byte HarmonicsCalculus;
            public byte BacklightTime;

            public string VoltageSimpleorCompoundString { get { return VoltageSimpleOrCompound == 0 ? "SI" : "NO"; } }
            public string HarmonicsCalculusString { get { return HarmonicsCalculus == 0 ? "THD" : "NO"; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.LEDS_AND_BACKLIGHT)]
        public struct LedsInformation
        {
            public State CPU;
            public State COMMUNICATIONS;
            public State BACKLIGHT;

            public LedsInformation(State state)
            {
                CPU = state;
                COMMUNICATIONS = state;
                BACKLIGHT = state;
            }

            public bool[] ToBoolArray()
            {
                return new bool[] 
                { 
                    CPU == State.ON ? false : true, 
                    COMMUNICATIONS == State.ON ? false : true, 
                    true, 
                    BACKLIGHT == State.OFF ? false : true 
                };
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AlarmConfig
        {
            public int MaxValue;
            public int MinValue;
            public ushort Delay;
            public byte VariableNumber;
            private byte dummyByte;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAXIMUM_DEMAND_CONFIGURATION)]
        public struct MaximumDemandConfiguration
        {
            public ushort valueToCalculate;
            public string ValueToCalculate
            {
                get 
                {
                    switch (valueToCalculate)
                    {
                        case 0:
                            return "NO_PD";
                        case 16:
                            return "kWIII";
                        case 34:
                            return "kVA";
                        case 36:
                            return "I_AVG";
                        default:
                            return "VALOR_DESCONOCIDO";
                    }
                }
                set
                {
                    switch (value.ToUpper().Trim())
                    {
                        case "NO_PD":
                            valueToCalculate = 0;
                            break;
                        case "kWIII":
                            valueToCalculate = 16;
                            break;
                        case "kVA":
                            valueToCalculate = 34;
                            break;
                        case "I_AVG":
                            valueToCalculate = 36;
                            break;
                        default:
                            throw new Exception("Variable introducida para el calculo de maxima demanda ínvalida. Los posibles son (NO_PD, kWIII, kVA, I_avg)");
                    }
                }
            }
            public ushort RegisterTime;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CHANNEL_OFFSETS)]
        public struct ChannelOffsets
        {
            public ushort CurrentL1;
            public ushort VoltageL1;
            public ushort CurrentL2;
            public ushort VoltageL2;
            public ushort CurrentL3;
            public ushort VoltageL3;
        }
        

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CALIBRATION_FACTORS)]
        public struct MeasureGains
        {
            public ushort CurrentL1;
            public ushort VoltageL1;
            public ushort CurrentL2;
            public ushort VoltageL2;
            public ushort CurrentL3;
            public ushort VoltageL3;
            public ushort CurrentL1Scale2;
            public ushort CurrentL2Scale2;
            public ushort CurrentL3Scale2;

            public MeasureGains(ushort current, ushort currentScale2, ushort voltage)
            {
                CurrentL1 = CurrentL2 = CurrentL3 = current;
                CurrentL1Scale2 = CurrentL2Scale2 = CurrentL3Scale2 = currentScale2;
                VoltageL1 = VoltageL2 = VoltageL3 = voltage;
            }

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COMPOUND_GAIN_FACTORS)]
        public struct CompoundVoltageGains
        {
            public CompoundVoltageGains(ushort factor)
            {
                L12 = L23 = L31 = factor;
            }

            public ushort L12;
            public ushort L23;
            public ushort L31;
        }

        public struct VoltageCurrentAndCompoundGains
        {
            public MeasureGains VoltageCurrent;
            public CompoundVoltageGains Compound;

            public VoltageCurrentAndCompoundGains(MeasureGains measureGains, CompoundVoltageGains CompoundGains)
            {
                VoltageCurrent = measureGains;
                Compound = CompoundGains;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ITF_PHASE_ADJUST)]
        public struct PhaseITFGains
        {
            public PhaseITFGains(ushort factor)
            {
                L1 = L2 = L3 = factor;
            }

            public ushort L1;
            public ushort L2;
            public ushort L3;

            public bool CheckValidGains(ushort upperLimit)
            {
                return L1 > upperLimit && L2 > upperLimit && L3 > upperLimit;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SAVE_ENERGIES)]
        public struct Energies
        {
            public int ActivePower;
            public int InductivePower;
            public int ReactivePower;

            public bool AreZero()
            {
                return (ActivePower == 0) && (InductivePower == 0) && (ReactivePower == 0);
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONVERTER_POINTS)]
        public struct ConverterPoints
        {
            public int CurrentL1;
            public int VoltageL1;
            public int CurrentL2;
            public int VoltageL2;
            public int CurrentL3;
            public int VoltageL3;
            public int VoltageL12;
            public int VoltageL23;
            public int VoltageL31;
            public int ActivePowerL1;
            public int ActivePowerL2;
            public int ActivePowerL3;
            public ushort AmountOfSamples;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COMMUNICATIONS_CONFIG)]
        public struct Comunicaciones
        {
            public byte Protocolo;
            public byte Periferico;
            public byte Velocidad;
            public byte Paridad;
            public byte Longitud;
            public byte Stop;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.FIRMWARE_VERSION_ETH)]
        public struct FirmwareEthenet
        {
            public byte ID;
            public byte VerMayorMini;
            public byte VerMenorMini;
            public byte VerMayorEth;
            public byte VerMenorEth;
            public byte VerRevisionEth;

            public override string ToString()
            {
                return string.Format("{0}.{1}.{2}.{3}.{4}.{5}", ID, VerMayorMini, VerMenorMini, VerMayorEth, VerMenorEth, VerRevisionEth);
            }
        }

        #endregion

        #region Vector de Wardware

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele2 { get; set; }
            public bool Rele1 { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Cuadrantes { get; set; }
            public bool Shunt { get; set; }
            public bool CalculoHarmonicos { get; set; }
            public bool Johnson { get; set; }
            public bool _1Fase { get; set; }

            public string Rele1String { get { return Rele1 == true ? "SI" : "NO"; } }
            public string Rele2String { get { return Rele2 == true ? "SI" : "NO"; } }
            public string Rs232String { get { return Rs232 == true ? "SI" : "NO"; } }
            public string ComunicationString { get { return Comunication == true ? "SI" : "NO"; } }
            public string ShuntString { get { return Shunt == true ? "SI" : "NO"; } }
            public string CalculoHarmonicosString { get { return CalculoHarmonicos == true ? "SI" : "NO"; } }
            public string CuadrantesString { get { return Cuadrantes == true ? "2_CUADRANTES" : "4_CUADRANTES"; } }
            public string JohnsonString { get { return Johnson == true ? "SI" : "NO"; } }
            public string _1FaseString { get { return _1Fase == true ? "1" : "3"; } }

            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value.ToUpper().Trim();

                    if (powerSupplyString.Contains("230VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._230Vac;
                        return;
                    }
                    else if(powerSupplyString.Contains("400VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._400Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("480VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._480Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("80-265VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._80_265Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("21-51VDC"))
                    {
                        PowerSupply = VectorHardwareSupply._21_51Vdc;
                        return;
                    }
                    else
                        throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware, Valores permitidos (230VAC, 400VAC, 480VAC, 80-265VAC o 21-51VDC)");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else
                    {
                        if (inputVoltageString.Contains("110"))
                        {
                            InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                            return;
                        }
                        else
                        {
                            if (inputVoltageString.Contains("500"))
                            {
                                InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                                return;
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("5"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                        return;
                    }
                    else
                    {
                        if (inputCurrentString.Contains("2"))
                        {
                            InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                            return;
                        }
                        else
                        {
                            if (inputCurrentString.Contains("1"))
                            {
                                InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                                return;
                            }
                            else
                            {
                                if (inputCurrentString.ToUpper().Contains("ITF"))
                                {
                                    InputCurrent = VectorHardwareCurrentInputs.ITFext;
                                    return;
                                }
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString == "CVM_MINI")
                        Modelo = VectorHardwareModelo.CVM_MINI;
                    else if (modeloString == "CVM_MINI_ETH")
                        Modelo = VectorHardwareModelo.CVM_MINI_ETH;
                    else       
                      throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele1 = (vectorHardware[0] & 0x10) == 0x10;
                Rele2 = (vectorHardware[0] & 0x20) == 0x20;
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                Johnson = (vectorHardware[8] & 0x02) == 0x02;
                _1Fase = (vectorHardware[10] & 0x04) == 0x04;
                Cuadrantes = (vectorHardware[10] & 0x02) == 0x02;
                CalculoHarmonicos = (vectorHardware[10] & 0x01) == 0x01;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele1;
                vector[5] = Rele2;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;
                vector[11] = Shunt;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[65] = Johnson;

                vector[81] = Cuadrantes;
                vector[80] = CalculoHarmonicos;
                vector[82] = _1Fase;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2,
            ITFext = 7,
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _21_51Vdc = 3,
            _80_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            CVM_MINI = 1,
            CVM_MINI_ETH = 2         
        }

        #endregion

        #region Dictionarys

        public Dictionary<Outputs, Registers> outputDictionary = new Dictionary<Outputs, Registers>()
        {
            {Outputs.OUTPUT_1, Registers.OUTPUT_1},
            {Outputs.OUTPUT_2, Registers.OUTPUT_2}
        };

        public Dictionary<byte, int> baudrateDictionary = new Dictionary<byte,int>()
        {
            {0, 9600}, // El unico que se parametriza a 0 es el EATON y tiene un binario especial que con 0 va a 9600... 
            {1, 2400},
            {2, 4800},
            {3, 9600},
            {4, 19200},
        };

        public Dictionary<EEPROMErrorCode, string> eepromDictionary = new Dictionary<EEPROMErrorCode, string>()
        {
            {EEPROMErrorCode.ALARM_1_NOT_INITIALIZED, "alarma 1 no inicializada"},
            {EEPROMErrorCode.ALARM_2_NOT_INITIALIZED, "alarma 2 no inicializada"},
            {EEPROMErrorCode.CALIBRATION_FACTORS_NOT_SET, "factores de calibracion sin establecer"},
            {EEPROMErrorCode.ENERGIES_NOT_SET_OR_NEGATIVE_VALUES_PRESENT, "energias sin establecer o se detectan valores negativos"},
            {EEPROMErrorCode.HARDWARE_VECTOR_NOT_SET, "vector de hardware sin establecer"},
            {EEPROMErrorCode.INCORRECT_COMMUNICATION_CONFIGURATION, "configuracion de comunicaciones no valida"},
            {EEPROMErrorCode.INCORRECT_MAIN_SETUP, "setup principal incorrecto"},
            {EEPROMErrorCode.INITIAL_MESSAGE_NOT_SET, "mensaje inicial sin establecer"},
            {EEPROMErrorCode.MAXIMUM_DEMAND_CONFIGURATION_ERROR, "configuracion de maxima demanda incorrecta"},
            {EEPROMErrorCode.PASSWORD_NOT_SET, "contraseña no configurada"},
            {EEPROMErrorCode.SERIAL_NUMBER_NOT_SET, "numero de serie sin establecer"},
            {EEPROMErrorCode.TEMPERATURE_SENSOR_ERROR, "error en el sensor de temperatura"},
        };

        public Dictionary<DisplayState, bool[]> displayInformation = new Dictionary<DisplayState, bool[]>()
        {
            {DisplayState.TODO_APAGADO, new bool[128]
                { 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false
                }},
            {DisplayState.SERGMENTOS_VERTICALES, new bool[128]
                {
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, false, false, false, false, false, false, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, true, false, true, false, true, true, false,
                    false, false, false, false, false, false, false, false,
                }
            },
            {DisplayState.SEGMENTOS_HORIZONTALES, new bool[128]
                {
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, false, false, false, false, false, false,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, true, false, true, false, false, true,
                    false, false, false, false, false, false, false, false
                }},
            {DisplayState.SIMBOLOS, new bool[128]
                {
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, true, true, true, true, true, true, true,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, false, false, false, false, false, false, false,
                    true, true, true, true, true, true, true, true
                }},
              {DisplayState.EVEN, new bool[128]
                {
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                    false, false, false, false, false, true, false, true,
                }},
             {DisplayState.ODD, new bool[128]
                {
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,
                    true, false, true, false, false, false, false, false,

                }},
             {DisplayState.ALL, new bool[128]
                {
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true,
                    true, true, true, true, true, true, true, true
                }},
        };

        #endregion

        #region Enumerados

        public enum DisplayState
        {
            [Description("ningun segmento")]
            TODO_APAGADO,
            [Description("segmentos verticales")]
            SERGMENTOS_VERTICALES,
            [Description("segmentos horizontales")]
            SEGMENTOS_HORIZONTALES,
            [Description("segmentos de símbolos")]
            SIMBOLOS,
            [Description("segmentos pares")]
            EVEN,
            [Description("segmentos impares")]
            ODD,
            [Description("todos los segmentos")]
            ALL,
        }

        public enum State
        {
            ON,
            OFF
        }

        public enum Leds
        {
            [Description("led de cpu")]
            CPU,
            [Description("led de comunicaciones")]
            COMMUNICATIONS,
            [Description("led de retroiluminación")]
            BACKLIGHT
        }

        public enum Outputs
        {
            [Description("salida 1")]
            OUTPUT_1 = 0x0005,
            [Description("salida 2")]
            OUTPUT_2 = 0x0006,
        }

        [Flags]
        public enum Key
        {
            [Description("ninguna")]
            NO_KEY = 0x00,
            [Description("display")]
            DISPLAY = 0x01,
            [Description("borrar maxima demanda")]
            CLEAR_MAX_PD = 0x02,
            [Description("borrar energia")]
            CLEAR_ENERGY = 0x04,
            [Description("de configuración")]
            SETUP = 0x10,
            [Description("de reinicio")]
            RESET = 0x20,
            [Description("de máximos")]
            MAX = 0x40,
            [Description("de mínimos")]
            MIN = 0x80
        }

        public enum Alarms
        {
            ALARM_1 = 0x047E,
            ALARM_2 = 0x04B0
        }

        [Flags]
        public enum EEPROMErrorCode
        {
            NO_ERROR = 0x0000,
            HARDWARE_VECTOR_NOT_SET = 0x0001,
            INCORRECT_COMMUNICATION_CONFIGURATION = 0x0002,
            PASSWORD_NOT_SET = 0x0004,
            INCORRECT_MAIN_SETUP = 0x0008,
            INITIAL_MESSAGE_NOT_SET = 0x0010,
            ALARM_1_NOT_INITIALIZED = 0x0020,
            ALARM_2_NOT_INITIALIZED = 0x0040,
            ENERGIES_NOT_SET_OR_NEGATIVE_VALUES_PRESENT = 0x0080,
            CALIBRATION_FACTORS_NOT_SET = 0x0100,
            SERIAL_NUMBER_NOT_SET = 0x0200,
            MAXIMUM_DEMAND_CONFIGURATION_ERROR = 0x0400,
            TEMPERATURE_SENSOR_ERROR = 0x0800,
            ERROR_BACNET_ID = 0x1000,
        }

        public enum Registers
        {
            OUTPUTS = 0x0000,
            OUTPUT_1 = 0x0000,
            OUTPUT_2 = 0x0001,
            KEYBOARD = 0x0008,
            LEDS_AND_BACKLIGHT = 0x0010,
            DISPLAY_SEGMENTS = 0x03E8,
            RESET = 2000,
            CLEAR_ENERGY = 0x0834,
            INITIALIZE_MAXIMUM_DEMAND = 0x0835,
            CLEAR_MAX_MIN = 0x0836,
            CLEAR_ALL = 0x0837,
            CLEAR_MAXIMUM_MD = 0x0838,
            SCREEN_SELECTION = 0x0898,
            HARDWARE_CONFIGURATION = 0x2710,
            FLAG_TEST = 0x2AF8,
            APPLY_CALIBRATION = 0x2B5C,
            ELECTRICAL_VARIABLES = 0x0000,
            CURRENT_HARMONICS_L1 = 0x01F4,
            CURRENT_HARMONICS_L2 = 0x0212,
            CURRENT_HARMONICS_L3 = 0x0230,
            VOLTAGE_HARMONICS_L1 = 0x02AE,
            VOLTAGE_HARMONICS_L2 = 0x02CC,
            VOLTAGE_HARMONICS_L3 = 0x02EA,
            COMMUNICATIONS_CONFIG = 0x03E8,
            PASSWORD_CONFIG = 0x041A,
            SETUP_CONFIG = 0x044C,
            ALARM_1_CONFIG = 0x047E,
            ALARM_2_CONFI = 0x04B0,
            MAXIMUM_DEMAND_CONFIGURATION = 0x04E2,
            INITIAL_SCREEN_NAME = 0x0514,
            SOFTWARE_VERSION = 0x0578,
            EEPROM_ERROR_CODE = 0x05DC,
            SERIAL_NUMBER = 0x2710,
            CALIBRATION_FACTORS = 0x2AF8,
            COMPOUND_GAIN_FACTORS = 0x2B34,
            ITF_PHASE_ADJUST = 0x2B2A,
            CONVERTER_POINTS = 0x2B5C,
            CHANNEL_OFFSETS = 0x2BC0,
            SAVE_ENERGIES = 0x4E20,
            BACNET_FIRMWARE = 0x1393,   
            ROM_CODE = 0x526C,
            LCD_OFF_TIME = 0x08FC,
            BACNET_ID = 0x1390,
            BACNET_TEMPERATURA = 0x1392,
            DISABLE_COM_TCP = 0x03E8,
            FIRMWARE_VERSION_ETH = 0x562C,
        };

        #endregion
    }
}
