﻿using Dezac.Core.Exceptions;
using System.Collections.Generic;

namespace Dezac.Device.Metering.SOAP
{
    public class SoapException : SauronException
    {    
        private static readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { 0, "Error desconocido!" },
            { 10, "Error desconocido categoria - DC" },
            { 11, "contador inactivo (no existe en la BBDD - DC" },
            { 12, "Error interno software - DC" },
            { 13, "Error interno hardware - DC" },
            { 20, "Error desconocido categoria - PRIME" },
            { 21, "contador temporalmente en fallo (TF) - PRIME" },
            { 22, "contador en fallo permanente (PF) - PRIME" },
            { 23, "contador con clave de activación fallo de espera (AKP) - PRIME" },
            { 24, "contador con clave de activación fallo de error (AKW) - PRIME" },
            { 30, "Error desconocido categoria - COSEM" },
            { 31, "Error autentificación - COSEM" },
            { 32, "Error de datos recibidos, malformado o recpeción parcial - COSEM"},
            { 33, "Error no hay datos encontados en el contador - COSEM" },
            { 34, "orden de ejecución rechazada por el contador (ecritura) - COSEM" },
            { 35, "orden de ejecución aceptada (escritura) pero no confirmada (lectura) - COSEM" },
            { 36, "orden de ejecució rechazada desde DC - COSEM" },
        };

        public SoapException(int code)
            : base(GetMessage((int)code), code.ToString())
        {
        }

        private static string GetMessage(int code)
        {
            string message = null;

            messages.TryGetValue(code, out message);

            return message ?? "Error desconocido!";
        }
    }
}
