namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ARTICULOCLIENTE
    {
        public int NUMARTICULO { get; set; }
        public string IDCLIENTE { get; set; }
        public string REFCLIENTE { get; set; }
        public string REFCLIENTE2 { get; set; }
        public Nullable<decimal> PRECIO { get; set; }
        public string ATTRIB1 { get; set; }
        public string ATTRIB2 { get; set; }
        public string ATTRIB3 { get; set; }
        public string ATTRIB4 { get; set; }
        public string ATTRIB5 { get; set; }
        public string INACTIVO { get; set; }
        public Nullable<decimal> PRECIOBORRADOR { get; set; }
        public string OBSOLETO { get; set; }
        public string NOTASOBSOLETO { get; set; }
        public string USERNAME { get; set; }
        public string OBSERVACIONESBORRADOR { get; set; }
        public string EAN13 { get; set; }
        public Nullable<int> LEADTIME { get; set; }
        public Nullable<int> LOTEOPTIMO { get; set; }
        public string IDCLIENTEFINAL { get; set; }
    
        public virtual ARTICULO ARTICULO { get; set; }
    }
}
