﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(UC_CVM_EMULATOR_Description))]
    public partial class UC_CVM_EMULATOR : UserControl, IDisposable
    {
        private static readonly string TAG = "CVMEmulator";

        private CVMEmulator cvm;
        private MyComputer computer = new MyComputer();
        private Vars dataVars = new Vars();

        private SynchronizationContext sc;

        private bool timerEnabled;
        private bool modeRead;

        public class Vars
        {
            public double TensionL1 { get; set; }
            public double CorrienteL1 { get; set; }
            public double PotenciaActivaL1 { get; set; }
            public double PotenciaReactivaInductivaL1 { get; set; }
            public double PotenciaReactivaCapicitivaL1 { get; set; }
            public double PotenciaAparenteL1 { get; set; }
            public double FactorPotenciaL1 { get; set; }
            public double CosenoPhiL1 { get; set; }
        }

        public Type InstrumentType
        {
            get
            {
                return typeof(CVMEmulator);
            }
        }

        public CVMEmulator Instrument
        {
            get
            {
                if (cvm == null)
                    cvm = new CVMEmulator();

                return cvm;
            }
            set
            {
                if (cvm != null)
                {
                    cvm.Dispose();
                    cvm = null;
                }
                cvm = value;
            }
        }

        public UC_CVM_EMULATOR()
        {
            InitializeComponent();            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;

            Instrument.GetDeviceVersion();

            cmb_Port.Items.AddRange(computer.SerialPortNames.ToArray());
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM8");

            String[] Periferic = new String[] { "1", "2", "3", "4", "5","6", "7","8" };
            cmb_Periferic.Items.AddRange(Periferic);
            cmb_Periferic.SelectedIndex = cmb_Periferic.FindStringExact("5");
            comboBoxBaudRate.SelectedIndex = comboBoxBaudRate.FindStringExact("19200");

            bsAllVars.DataSource = dataVars;
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Selecciona un Puerto Serie");
            else if (cmb_Periferic.SelectedItem == null)            
                errorProvider1.SetError(cmb_Periferic, "Selecciona un Numero de Periferico");
            else
            {
                var periferic = Convert.ToByte(cmb_Periferic.SelectedItem.ToString());
                var BaudRate = Convert.ToInt32(comboBoxBaudRate.SelectedItem.ToString());
                Instrument.SetPort(cmb_Port.SelectedItem.ToString(), periferic, BaudRate);
                ToggleButtonsState((Button)sender);
                ReadAllVariables();
                ToggleButtonsState((Button)sender);
            }
        }

        private void LoadDataVar(CVMEmulator.VariablesFase variables)
        {
            dataVars.TensionL1 = variables.Tension;
            dataVars.CorrienteL1 = variables.Corriente;
            dataVars.CosenoPhiL1 = variables.CosenoPhi;
            dataVars.FactorPotenciaL1 = variables.FactorPotencia;
            dataVars.PotenciaActivaL1 = variables.PotenciaActiva;
            dataVars.PotenciaAparenteL1 = variables.PotenciaAparente;
            dataVars.PotenciaReactivaCapicitivaL1 = variables.PotenciaReactivaCapicitiva;
            dataVars.PotenciaReactivaInductivaL1 = variables.PotenciaReactivaInductiva; 
            bsAllVars.ResetBindings(false);
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private async void ReadAllVariables()
        {
            try
            {            
                StartTask("Lectura de las variables instantaneas");
                var data = await Instrument.ModbusDevice.ReadAsync<CVMEmulator.VariablesFase>();
                LoadDataVar(data);
                EndTask();
            }
            catch (Exception ex)
            {
                Logger.Error("Error al pedir variables instantaneas al CVM-B150", ex);
            }
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;
            ReadAllVariables();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (cvm != null)
                cvm.Dispose();

            base.Dispose(disposing);
        }
    }
    public class UC_CVM_EMULATOR_Description : ControlDescription
    {
        public override string Name { get { return "CVM_EMULATOR"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.cvm_b; } }
        public override Image IconBig { get { return Properties.Resources.CVM_B100; } }
        public override Type SubcomponentType { get { return typeof(CVMEmulator); } }
    }
}
