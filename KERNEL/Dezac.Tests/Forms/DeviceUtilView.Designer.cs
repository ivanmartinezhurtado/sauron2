﻿namespace Dezac.Tests.Forms
{
    partial class DeviceUtilView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.lblMangueraDesc = new System.Windows.Forms.Label();
            this.lblUtilDesc = new System.Windows.Forms.Label();
            this.txtManguera = new System.Windows.Forms.TextBox();
            this.txtUtil = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbManquera = new System.Windows.Forms.PictureBox();
            this.pbUtil = new System.Windows.Forms.PictureBox();
            this.lblCommand = new System.Windows.Forms.Label();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbManquera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUtil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.radPanel1.Controls.Add(this.lblMangueraDesc);
            this.radPanel1.Controls.Add(this.lblUtilDesc);
            this.radPanel1.Controls.Add(this.txtManguera);
            this.radPanel1.Controls.Add(this.txtUtil);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.pbManquera);
            this.radPanel1.Controls.Add(this.pbUtil);
            this.radPanel1.Controls.Add(this.lblCommand);
            this.radPanel1.Controls.Add(this.radLabel13);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(977, 614);
            this.radPanel1.TabIndex = 6;
            // 
            // lblMangueraDesc
            // 
            this.lblMangueraDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMangueraDesc.Location = new System.Drawing.Point(506, 569);
            this.lblMangueraDesc.Name = "lblMangueraDesc";
            this.lblMangueraDesc.Size = new System.Drawing.Size(448, 36);
            this.lblMangueraDesc.TabIndex = 31;
            // 
            // lblUtilDesc
            // 
            this.lblUtilDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUtilDesc.Location = new System.Drawing.Point(21, 569);
            this.lblUtilDesc.Name = "lblUtilDesc";
            this.lblUtilDesc.Size = new System.Drawing.Size(448, 36);
            this.lblUtilDesc.TabIndex = 30;
            // 
            // txtManguera
            // 
            this.txtManguera.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtManguera.Location = new System.Drawing.Point(610, 122);
            this.txtManguera.Name = "txtManguera";
            this.txtManguera.Size = new System.Drawing.Size(208, 35);
            this.txtManguera.TabIndex = 1;
            this.txtManguera.TextChanged += new System.EventHandler(this.txtManguera_TextChanged);
            // 
            // txtUtil
            // 
            this.txtUtil.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUtil.Location = new System.Drawing.Point(127, 122);
            this.txtUtil.Name = "txtUtil";
            this.txtUtil.Size = new System.Drawing.Size(208, 35);
            this.txtUtil.TabIndex = 0;
            this.txtUtil.TextChanged += new System.EventHandler(this.txtUtil_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dezac.Tests.Properties.Resources.dezac;
            this.pictureBox1.Location = new System.Drawing.Point(21, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 43);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // pbManquera
            // 
            this.pbManquera.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbManquera.Location = new System.Drawing.Point(504, 170);
            this.pbManquera.Name = "pbManquera";
            this.pbManquera.Size = new System.Drawing.Size(448, 396);
            this.pbManquera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbManquera.TabIndex = 27;
            this.pbManquera.TabStop = false;
            // 
            // pbUtil
            // 
            this.pbUtil.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbUtil.Location = new System.Drawing.Point(21, 170);
            this.pbUtil.Name = "pbUtil";
            this.pbUtil.Size = new System.Drawing.Size(448, 396);
            this.pbUtil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUtil.TabIndex = 26;
            this.pbUtil.TabStop = false;
            // 
            // lblCommand
            // 
            this.lblCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCommand.BackColor = System.Drawing.Color.Transparent;
            this.lblCommand.CausesValidation = false;
            this.lblCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCommand.Location = new System.Drawing.Point(22, 26);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(934, 32);
            this.lblCommand.TabIndex = 23;
            this.lblCommand.Text = "Introduzca el  número de bien solicitado";
            this.lblCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel13.Location = new System.Drawing.Point(666, 83);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(97, 26);
            this.radLabel13.TabIndex = 14;
            this.radLabel13.Text = "Manguera";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel3.Location = new System.Drawing.Point(197, 83);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(68, 26);
            this.radLabel3.TabIndex = 7;
            this.radLabel3.Text = "Utillaje";
            // 
            // DeviceUtilView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Name = "DeviceUtilView";
            this.Size = new System.Drawing.Size(977, 614);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbManquera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUtil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.PictureBox pbManquera;
        private System.Windows.Forms.PictureBox pbUtil;
        private System.Windows.Forms.Label lblMangueraDesc;
        private System.Windows.Forms.Label lblUtilDesc;
        private System.Windows.Forms.TextBox txtManguera;
        private System.Windows.Forms.TextBox txtUtil;
    }
}
