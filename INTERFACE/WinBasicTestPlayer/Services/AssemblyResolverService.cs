﻿using Dezac.Tests.Services;
using System;
using System.IO;
using System.Reflection;

namespace WinTestPlayer.Services
{
    public class AssemblyResolverService : IAssemblyResolveService
    {
        public string DefaultPathFiles { get; set; }

        public Assembly Resolve(string name)
        {
            return Resolve(DefaultPathFiles, name);
        }

        public Assembly Resolve(string path, string name)
        {
            if (path == null)
                return null;

            AssemblyName asm = new AssemblyName(name);

            name = asm.Name;
            if (!name.EndsWith(".dll", StringComparison.InvariantCultureIgnoreCase))
                name += ".dll";


            string fileName = Path.Combine(path, name);

            if (File.Exists(fileName))
                return Assembly.LoadFrom(fileName);

           return null;
        }
    }
}
