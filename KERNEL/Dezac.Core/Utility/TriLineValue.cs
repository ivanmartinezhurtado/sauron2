﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dezac.Core.Utility
{
    public struct TriLineValue
    {
        public readonly static TriLineValue Zero = new TriLineValue();

        public double L1 { get; set; }
        public double L2 { get; set; }
        public double L3 { get; set; }

        public static TriLineValue Create(double v1, double v2, double v3)
        {
            return new TriLineValue { L1 = v1, L2 = v2, L3 = v3 };
        }

        public static TriLineValue Create(double v)
        {
            return new TriLineValue { L1 = v, L2 = v, L3 = v };
        }

        public static TriLineValue CreateFromString(string value, char delimiter = ',')
        {
            TriLineValue result = new TriLineValue();

            string[] tokens = value.Split(delimiter);

            CultureInfo enUS = new CultureInfo("en-US");

            result.L1 = Convert.ToDouble(tokens[0], enUS);
            result.L2 = Convert.ToDouble(tokens[1], enUS);
            result.L3 = Convert.ToDouble(tokens[2], enUS);

            return result;
        }

        public static bool operator ==(TriLineValue x, TriLineValue y)
        {
            return x.L1 == y.L1 && x.L2 == y.L2 && x.L3 == y.L3;
        }

        public static bool operator !=(TriLineValue x, TriLineValue y)
        {
            return x.L1 != y.L1 || x.L2 != y.L2 || x.L3 != y.L3;
        }

        public override bool Equals(object obj)
        {
            return (L1 == ((TriLineValue)obj).L1) && (L2 == ((TriLineValue)obj).L2) && (L3 == ((TriLineValue)obj).L3);
        }
        public override int GetHashCode()
        {
            return 0;
        }

        public static TriLineValue operator *(double x, TriLineValue y)
        {
            y.L1 *= x;
            y.L2 *= x;
            y.L3 *= x;

            return y;
        }

        public static TriLineValue operator *(TriLineValue x, TriLineValue y)
        {
            y.L1 *= x.L1;
            y.L2 *= x.L2;
            y.L3 *= x.L3;

            return y;
        }

        public static TriLineValue operator /(TriLineValue y, TriLineValue x)
        {
            y.L1 /= x.L1;
            y.L2 /= x.L2;
            y.L3 /= x.L3;

            return y;
        }

        public static TriLineValue operator /(TriLineValue y, double x)
        {
            y.L1 /= x;
            y.L2 /= x;
            y.L3 /= x;

            return y;
        }

        public static TriLineValue operator -(TriLineValue x, TriLineValue y)
        {
            x.L1 -= y.L1;
            x.L2 -= y.L2;
            x.L3 -= y.L3;

            return x;
        }

        public static TriLineValue operator +(TriLineValue x, TriLineValue y)
        {
            y.L1 += x.L1;
            y.L2 += x.L2;
            y.L3 += x.L3;

            return y;
        }

        public static TriLineValue operator +(double x, TriLineValue y)
        {
            y.L1 += x;
            y.L2 += x;
            y.L3 += x;

            return y;
        }


        public static implicit operator TriLineValue(double value)
        {
            return new TriLineValue { L1 = value, L2 = value, L3 = value };
        }

        public bool InToleranceFrom(TriLineValue pattern, double tolerance)
        {
            if (pattern == 0) return true;

            double l1 = 0;
            double l2 = 0;
            double l3 = 0;

            if (L1 == 0) l1 = 0;
            else
             l1 = (1 - (pattern.L1 / L1)) * 100;

            if (L2 == 0) l2 = 0;
            else
             l2 = (1 - (pattern.L2 / L2)) * 100;

            if (L3 == 0) l3 = 0;
            else
             l3 = (1 - (pattern.L3 / L3)) * 100;

            return Math.Abs(l1) <= tolerance && Math.Abs(l2) <= tolerance && Math.Abs(l3) <= tolerance;
        }

        public override string ToString()
        {
            return string.Format("L1={0}, L2={1}, L3={2}", L1, L2, L3);
        }

        public double[] ToArray()
        {
            var list = new List<double>();
            list.Add(L1);
            list.Add(L2);
            list.Add(L3);
            return list.ToArray();
        }

        public List<double> ToList()
        {
            var list = new List<double>();
            list.Add(L1);
            list.Add(L2);
            list.Add(L3);
            return list;
        }

        public static double[] ToArray(TriLineValue value)
        {
            var list = new List<double>();
            list.Add(value.L1);
            list.Add(value.L2);
            list.Add(value.L3);
            return list.ToArray();
        }

        public static bool isNotZero(TriLineValue x)
        {
            return x.L1 != 0 && x.L2 != 0 && x.L3 != 0;
        }
    }
}
