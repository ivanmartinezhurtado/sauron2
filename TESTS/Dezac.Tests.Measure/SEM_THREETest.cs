﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.09)]
    public class SEM_THREETest : TestBase
    {
        private Tower3 tower;
        private SEM_THREE sem;
        private string CAMERA_IDS_LEDS;

        private const byte VALV_GENERAL = 4;
        private const byte BORNES = 5;
        private const byte RECORDING = 6;
        private const byte FIXED_DEVICE = 9;
        private const byte KEY = 10;
        private const byte _24V = 14;
        private const byte LED_ERROR = 48;

        private const byte DISCONECT_DATAMAN = 52;

        #region Enums
        public enum Control
        {
            [Description("la PRESENCIA DEL EQUIPO")]
            DEVICE_PRESENCE = 24,
            [Description("la TAPA DEL EQUIPO")]
            TOP_PRESENCE = 25,
            [Description("el piston puntas derechas, no llega a su fin")]
            BORNAS1 = 30,
            [Description("el piston puntas izquierdas, no llega a su fin")]
            BORNAS2 = 31,
            STATE_RECORDING = 32,
            DISCONECT_DATAMAN = 52
        }
        #endregion

        public void TestInitialization(int port)
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            sem = AddInstanceVar(new SEM_THREE(port), "UUT");

            var ListInputsToCheck = new List<Control>();
            ListInputsToCheck.Add(Control.DEVICE_PRESENCE);
            //ListInputsToCheck.Add(Control.TOP_PRESENCE);
            //ListInputsToCheck.Add(Control.BORNAS1);
            ListInputsToCheck.Add(Control.BORNAS2);

            tower.IO.DO.On(VALV_GENERAL);
            tower.IO.DO.On(_24V);
            tower.IO.DO.On(FIXED_DEVICE);
            tower.IO.DO.Off(53);
            tower.IO.DO.Off(LED_ERROR);
            Delay(500, "Activando Piston de fijacion");
            tower.IO.DO.On(BORNES);
            Delay(200, "Activando salidas");
            tower.IO.DO.Off(DISCONECT_DATAMAN);

            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            foreach (var input in ListInputsToCheck)
                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[(byte)input];
                }, string.Format("Error, no se ha detectado {0}", input.GetDescription()), 5, 500);
        }

        public void ActivateMTE()
        {
            tower.IO.DO.On(DISCONECT_DATAMAN);

            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var phase = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };
            var consignasV = new TriLineValue { L1 = voltage, L2 = voltage, L3 = voltage };
            var consignasI = new TriLineValue { L1 = current, L2 = current, L3 = current };
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(consignasV, consignasI, 0, 0, phase, true);
        }

        public void TestConsumption()
        {           
            List<AdjustValueDef> consumptionValues = new List<AdjustValueDef>();
            consumptionValues.Add(new AdjustValueDef(Params.W.Null.EnVacio.Name, 0, Params.W.Null.EnVacio.Min(), Params.W.Null.EnVacio.Max(), 0, 0, ParamUnidad.W));
            consumptionValues.Add(new AdjustValueDef(Params.VAR.Null.EnVacio.Name, 0, Params.VAR.Null.EnVacio.Min(), Params.VAR.Null.EnVacio.Max(), 0, 0, ParamUnidad.Var));

            TestMeasureBase(consumptionValues, (step) =>
            {
                var cvmMteReadings = tower.CvmminiMTE.ReadAllVariables().Trifasicas;

                return new  double[] { cvmMteReadings.PotenciaActivaIII, cvmMteReadings.PotenciaCapacitivaIII - Math.Abs(cvmMteReadings.PotenciaInductivaIII) };
            }, 0, 5, 1000);
        }

        public void TestSetupDefault()
        {           
            var gainsDefault = new SEM_THREE.Gains();
            gainsDefault.Phase = (ushort)Configuracion.GetDouble(Params.GAIN_DESFASE.Null.TestPoint("DEFAULT").Name, 0, ParamUnidad.Puntos);
            gainsDefault.Frecuency = (int)Configuracion.GetDouble("GAIN_FREC_DEFAULT", 0, ParamUnidad.Puntos);
            gainsDefault.ActivePower = gainsDefault.ReactivePower = gainsDefault.AparentPower = (int)Configuracion.GetDouble(Params.GAIN_KW.Null.TestPoint("DEFAULT").Name, 0, ParamUnidad.Puntos);
            gainsDefault.Voltage = (int)Configuracion.GetDouble(Params.GAIN_V.Null.TestPoint("DEFAULT").Name, 0, ParamUnidad.Puntos);
            gainsDefault.Current = (int)Configuracion.GetDouble(Params.GAIN_I.Null.TestPoint("DEFAULT").Name, 0, ParamUnidad.Puntos);

            sem.WriteGains(gainsDefault, SEM_THREE.Lines.L1);
            sem.WriteGains(gainsDefault, SEM_THREE.Lines.L2);
            sem.WriteGains(gainsDefault, SEM_THREE.Lines.L3);
        }

        public void TestLedsGreen()
        {
            CAMERA_IDS_LEDS = GetVariable<string>("CAMARA_IDS_LEDS", CAMERA_IDS_LEDS);

            this.TestHalconFindLedsProcedure(CAMERA_IDS_LEDS, "SEM_GREEN_LED", "GREEN_LED", "SEM");
        }

        public void TestLedsAll()
        {
            CAMERA_IDS_LEDS = GetVariable<string>("CAMARA_IDS_LEDS", CAMERA_IDS_LEDS);

            WritePassword(SEM_THREE.Lines.L1);

            Delay(500, "Escribimos passwords para que se encienda el LED2");

            this.TestHalconFindLedsProcedure(CAMERA_IDS_LEDS, "SEM_ALL_LEDS", "ALL_LEDS", "SEM", (byte)Configuracion.GetDouble("RETRY_LEDS_COM", 5, ParamUnidad.SinUnidad));
        }


        public void TestComunications()
        {            
            SamplerWithCancel((p) =>
            {
                TestInfo.FirmwareVersion = sem.ReadSoftwareVersion().ToString();
                var versionSoftware = sem.ReadSoftwareVersion().ToString();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, TestInfo.FirmwareVersion, Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del SEM-ONE no es correcta"), ParamUnidad.SinUnidad);
                return true;
            }, "Error de comunicaciones por el puerto RS232 frontal al preguntar Version firmware al SEM-ONE", 3, 100, 1000, true, true);

            TestInfo.ProductoVersion = sem.ReadHardwareversion().ToString();
            var versionHardware = sem.ReadHardwareversion().ToString();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, TestInfo.ProductoVersion, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.HARDWARE.NO_COINCIDE("Error, la version del hardware del SEM-ONE no es correcta"), ParamUnidad.SinUnidad);

            var product = sem.ReadIDProductoCode();
            Assert.AreEqual(ConstantsParameters.Identification.MODELO, product.ToString(), Identificacion.MODELO.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, modelo del SEM-ONE incorrecto"), ParamUnidad.SinUnidad);           

            Resultado.Set("COMUNICATIONS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestSendComunicationsContinuo()
        {
            SetVariable("HALCON_END", false);

            bool endHalcon=false;
            SamplerWithCancel((p) =>
            {
                do
                {
                    sem.ReadSoftwareVersion().ToString();
                    Application.DoEvents();
                    endHalcon = GetVariable<bool>("HALCON_END");
                }
                while (!endHalcon);

                return endHalcon;
            }, "Error de comunicaciones para test de LEDS");
          }

        public void TestInitCalibration()
        {
            WritePassword(SEM_THREE.Lines.L1);

            WritePassword(SEM_THREE.Lines.L2);

            WritePassword(SEM_THREE.Lines.L3);

            Delay(200, "");

            sem.WriteAutoCalibrationAllLines(0X1F); //F CALIBRA FRECUEBNCIA TAMBIEN, 7-->CALIBRA V,I,P.

        }

        public void TestCalibration()
        {
            bool failedAutoCal = false;
            string messageException = string.Empty;
            string errorLine = string.Empty;

            try
            {
                SamplerWithCancel((p) =>
                {
                    var L1 = sem.ReadAutoCalibration(SEM_THREE.Lines.L1) == 0;
                    if (!L1)
                        errorLine = "L1";
                    var L2 = sem.ReadAutoCalibration(SEM_THREE.Lines.L2) == 0;
                    if (L1 && !L2)
                        errorLine = "L2";
                    var L3 = sem.ReadAutoCalibration(SEM_THREE.Lines.L3) == 0;

                    if (L1 && L2 && !L3)
                        errorLine = "L3";

                    if (L1 && L2 && L3)
                        return true;
                    else
                        throw new Exception(string.Format("Error, fallo en la calibración de la linea {0}", errorLine));
                }, "", 45, 1000, 100, false, false);
            }
            catch(Exception e)
            {
                messageException = e.Message;
                failedAutoCal = true;
            }

            var lines = new List<SEM_THREE.Lines>();
            lines.Add(SEM_THREE.Lines.L1);
            lines.Add(SEM_THREE.Lines.L2);
            lines.Add(SEM_THREE.Lines.L3);

            foreach (SEM_THREE.Lines CurrentLine in lines)
            {
                var defs = new List<AdjustValueDef>();
                defs.Add(new AdjustValueDef(Params.GAIN_V.Null.Ajuste.TestPoint(CurrentLine.ToString()).Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
                defs.Add(new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(CurrentLine.ToString()).Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
                defs.Add(new AdjustValueDef(Params.GAIN_KW.Null.Ajuste.TestPoint(CurrentLine.ToString()).Name, 0, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
                defs.Add(new AdjustValueDef(Params.GAIN_KVAR.Null.Ajuste.TestPoint(CurrentLine.ToString()).Name, 0, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
                defs.Add(new AdjustValueDef(string.Format("GAIN_VA_AJUSTE_{0}", CurrentLine.ToString()), 0, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
                defs.Add(new AdjustValueDef(Params.GAIN_DESFASE.Null.Ajuste.TestPoint(CurrentLine.ToString()).Name, 0, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
                defs.Add(new AdjustValueDef(string.Format("GAIN_FREC_AJUSTE_{0}", CurrentLine.ToString()), 0, Params.GAIN_FREC.Null.Ajuste.Min(), Params.GAIN_FREC.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

                TestMeasureBase(defs,
                (step) =>
                {
                    var gains = sem.ReadGains(CurrentLine);
                    return new double[] { gains.Voltage, gains.Current, gains.ActivePower, gains.ReactivePower, gains.AparentPower, gains.Phase, gains.Frecuency };
                }, 0, 3, 100);

                var defsOffsets = new List<AdjustValueDef>();
                defsOffsets.Add(new AdjustValueDef(Params.GAIN_OFFSET.Null.Ajuste.TestPoint(string.Format("V_{0}", CurrentLine.ToString())).Name, 0, Params.GAIN_OFFSET.Null.Ajuste.TestPoint("V").Min(), Params.GAIN_OFFSET.Null.Ajuste.TestPoint("V").Max(), 0, 0, ParamUnidad.Puntos));
                defsOffsets.Add(new AdjustValueDef(Params.GAIN_OFFSET.Null.Ajuste.TestPoint(string.Format("I_{0}", CurrentLine.ToString())).Name, 0, Params.GAIN_OFFSET.Null.Ajuste.TestPoint("I").Min(), Params.GAIN_OFFSET.Null.Ajuste.TestPoint("I").Max(), 0, 0, ParamUnidad.Puntos));
                defsOffsets.Add(new AdjustValueDef(Params.GAIN_OFFSET.Null.Ajuste.TestPoint(string.Format("KW_{0}", CurrentLine.ToString())).Name, 0, Params.GAIN_OFFSET.Null.Ajuste.TestPoint("KW").Min(), Params.GAIN_OFFSET.Null.Ajuste.TestPoint("KW").Max(), 0, 0, ParamUnidad.Puntos));
                defsOffsets.Add(new AdjustValueDef(Params.GAIN_OFFSET.Null.Ajuste.TestPoint(string.Format("KVAR_{0}", CurrentLine.ToString())).Name, 0, Params.GAIN_OFFSET.Null.Ajuste.TestPoint("KVAR").Min(), Params.GAIN_OFFSET.Null.Ajuste.TestPoint("KVAR").Max(), 0, 0, ParamUnidad.Puntos));
                defsOffsets.Add(new AdjustValueDef(Params.GAIN_OFFSET.Null.Ajuste.TestPoint(string.Format("VA_{0}", CurrentLine.ToString())).Name, 0, Params.GAIN_OFFSET.Null.Ajuste.TestPoint("VA").Min(), Params.GAIN_OFFSET.Null.Ajuste.TestPoint("VA").Max(), 0, 0, ParamUnidad.Puntos));

                TestMeasureBase(defsOffsets,
               (step) =>
               {
                   var offsets = sem.ReadOffsets(CurrentLine);
                   return new double[] { offsets.Voltage, offsets.Current, offsets.ActivePower, offsets.ReactivePower, offsets.AparentPower };
               }, 0, 3, 100);
            }

            if (failedAutoCal)
                throw new Exception(messageException);      
        }

        public void TestKey()
        {
            WritePassword(SEM_THREE.Lines.L1);

            SamplerWithCancel((p) =>
            {
                tower.IO.DO.Off(KEY);
                Delay(200, "");
                tower.IO.DO.On(KEY);
                Delay(500, "Pulsando Tecla");            
                return sem.ReadKey() == 1;
            }, "Error, no se ha detectado la tecla activada", 5, 500);

            SamplerWithCancel((p) =>
            {
                tower.IO.DO.Off(KEY);
                Delay(200, "Pulsando Tecla");                
                return sem.ReadKey() == 0;
            }, "Error, se ha detectado la tecla activada cuando debería estar desactivada", 5, 500);

            Resultado.Set("TEST_TECLA", "OK", ParamUnidad.SinUnidad);
        }

        public void TestVerification(SEM_THREE.Lines line ,int angle = 0, int initCount = 3, int samples = 6, int timeInterval = 1000)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 0.25, ParamUnidad.A);

            var consignasV = new TriLineValue { L1 = voltage, L2 = voltage, L3 = voltage };
            var consignasI = new TriLineValue { L1 = current, L2 = current, L3 = current };
            var angleGap = new TriLineValue { L1 = angle, L2 = angle, L3 = angle };
            var phase = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };

            if (angle != 0 && line.Equals(SEM_THREE.Lines.L1))
            {
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(consignasV, consignasI, 50, angleGap, phase, true);
                Delay(2000, "Espera Estabilizacion equipo");
            }

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKW = Params.KW.Null.Verificacion.Tol();
            var toleranciaPhase = Params.PHASE.Null.Verificacion.Tol();
            var toleranciaF = Params.FREQ.Null.Verificacion.Tol();

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.V.Null.Verificacion.TestPoint(string.Format("GAP_{0}_{1}", angle.ToString(),line.ToString())).Name, 0, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.Null.Verificacion.TestPoint(string.Format("GAP_{0}_{1}", angle.ToString(), line.ToString())).Name, 0, 0, 0, 0, toleranciaI, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.KW.Null.Verificacion.TestPoint(string.Format("GAP_{0}_{1}", angle.ToString(), line.ToString())).Name, 0, 0, 0, 0, toleranciaKW, ParamUnidad.W));
            if(angle != 0)
                defs.Add(new AdjustValueDef(Params.KVAR.Null.Verificacion.TestPoint(string.Format("GAP_{0}_{1}", angle.ToString(), line.ToString())).Name, 0, 0, 0, 0, toleranciaKW, ParamUnidad.Var));
            defs.Add(new AdjustValueDef(string.Format("VA_VERIF_GAP_{0}_{1}", angle.ToString(), line.ToString()), 0, 0, 0, 0, toleranciaKW, ParamUnidad.VA));
            defs.Add(new AdjustValueDef(Params.PF.Null.Verificacion.TestPoint(string.Format("GAP_{0}_{1}", angle.ToString(), line.ToString())).Name, 0, 0, 0, 0, toleranciaPhase, ParamUnidad.Grados));
            defs.Add(new AdjustValueDef(Params.FREQ.Null.Verificacion.TestPoint(string.Format("GAP_{0}_{1}", angle.ToString(), line.ToString())).Name, 0, 0, 0, 0, toleranciaF, ParamUnidad.Hz));

            var relTrn = sem.ReadTransfomationRelation();
            var rel = relTrn / consignasI.L1;
            logger.DebugFormat("RelacioTrans:{0}", rel);

            TestCalibracionBase(defs,
                () =>
                {
                    var variables = sem.ReadVariables(line);
                    logger.DebugFormat("V_medida:{0}  I_medida:{1} KW_medida:{2} Kvar_medida:{3}  PF_medida:{4}", variables.Voltage, variables.Current, variables.ActivePower, variables.ReactivePower, variables.PowerFactor);

                    var vars = sem.ReadCosPhiAndFrecuency(line);

                    var varsList = new List<double>();
                    //{
                    //    variables.Voltage, variables.Current / rel, variables.ActivePower / rel, variables.ReactivePower / rel , variables.AparentPower / rel, variables.PowerFactor, vars.Frecuency
                    //};
                    varsList.Add(variables.Voltage);
                    varsList.Add(variables.Current / rel);
                    varsList.Add(variables.ActivePower / rel);
                    if (angle != 0)
                        varsList.Add(variables.ReactivePower / rel);
                    varsList.Add(variables.AparentPower / rel);
                    varsList.Add(variables.PowerFactor);
                    varsList.Add(vars.Frecuency);
                 
                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = consignasV.L1;
                    var currentRef = consignasI.L1;
                    var powerFactor = Math.Cos(angleGap.L1.ToRadians());                   
                    var power = TriLinePower.Create(voltageRef, currentRef, angleGap);
                    var aparentPower = Math.Sqrt(Math.Pow(power.KW.L1, 2) + Math.Pow(power.KVAR.L1, 2));
                    var frecRef = 50;

                    logger.DebugFormat("V_medida Patron:{0}  I_medida Patron:{1} KW_medida Patron:{2} Kvar_medida Patron:{3}  PF_medida Patron:{4}", voltageRef, currentRef, power.KW.L1, power.KVAR.L1, angleGap.L1);

                    var varsList = new List<double>();
                    //{
                    //    voltageRef, currentRef, power.KW.L1, power.KVAR.L1, aparentPower, powerFactor, frecRef
                    //};
                    varsList.Add(voltageRef);
                    varsList.Add(currentRef);
                    varsList.Add(power.KW.L1);
                    if (angle != 0)
                        varsList.Add(power.KVAR.L1);
                    varsList.Add(aparentPower);
                    varsList.Add(powerFactor);
                    varsList.Add(frecRef);                    

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);
        }

        public void TestCustomize()
        {
            //sem.WriteModelProduction(Convert.ToInt16(TestInfo.NumBastidor.Value));

            tower.ActiveCurrentCircuit(true, true, true);

            WritePassword(SEM_THREE.Lines.L1);

            var serialNumberToWrite = Convert.ToInt32(TestInfo.NumSerie.Replace("/", "").Replace("-",""));
            sem.WriteSerialNumber(serialNumberToWrite);

            TestInfo.NumSerie = TestInfo.NumSerie.Replace("-","").Insert(4,"-");

            Delay(200, "Escribiendo numero de serie");

            tower.ActiveVoltageCircuit(false, false, false, false);
            Delay(1000, "Reseteando equipo");
            tower.ActiveVoltageCircuit();

            SamplerWithCancel((p) =>
            {
                sem.ReadSerialNumber();
                return true;
            }, "Error de comunicaciones despues de realizar el reset", 3, 300, 1000, true, true);
            var serialNumberReaded = sem.ReadSerialNumber();

            Assert.AreEqual("SERIAL_NUMBER", serialNumberReaded.ToString("0000000000"), serialNumberToWrite.ToString("0000000000"), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, el número de serie no coincide con el grabado"), ParamUnidad.SinUnidad);

            sem.ResetCounters();

            SamplerWithCancel((p) =>
            {
                var alarms = sem.ReadAlarmsInstant();
                logger.DebugFormat("alarmas:{0}", alarms);
                var ener = sem.ReadEnergies();
                logger.DebugFormat("Energy:{0}", ener);           
                return true;
            }, "Error de comunicaciones", 2, 100, 1000, true, true);

            TestInfo.NumSerie = TestInfo.NumSerie.Replace("-", "").Insert(4, "-");

            Resultado.Set(ConstantsParameters.Identification.NUMERO_SERIE_CLIENTE, TestInfo.NumSerie, ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (sem != null)
                sem.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.IO.DO.Off(KEY, DISCONECT_DATAMAN, RECORDING);
                tower.IO.DO.OffWait(500, FIXED_DEVICE);
                tower.IO.DO.OffWait(500, BORNES);
                tower.IO.DO.Off(_24V);
                tower.IO.DO.Off(VALV_GENERAL);
                tower.Dispose();
            }

            if(IsTestError)
                tower.IO.DO.On(LED_ERROR);
        }

        private void WritePassword(SEM_THREE.Lines line)
        {
            sem.WritePasswords(new SEM_THREE.Passwords() { Password1 = 0x0043, Password2 = 0x0052, Password3 = 0x0031, Password4 = 0x0039, Password5 = 0x0030, Password6 = 0x0036 }, line);
        }
    }
}
