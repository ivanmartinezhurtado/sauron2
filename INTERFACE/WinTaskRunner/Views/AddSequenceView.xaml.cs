﻿using Dezac.Data;
using Dezac.Services;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WinTaskRunner.Views
{
    public partial class AddSequenceView : Window
    {
        public MessageBoxResult Result { get; private set; } = MessageBoxResult.None;
        public string Description { get; private set; }
        public string FileName { get; private set; }
        public FASE Fase { get; private set; }

        public AddSequenceView(T_FAMILIA familia)
        {
            InitializeComponent();
            labelFamily.Content = familia.FAMILIA;
            using (var srv = new DezacService())
            {
                List<FASE> testFases = srv.GetFamilyTestFases(familia.NUMFAMILIA);
                comboBoxFase.ItemsSource = testFases.Select((fase) => $"{fase.IDFASE}-{fase.DESCRIPCION}").ToList();
            }
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxDescription.Text) ||
                string.IsNullOrWhiteSpace(textBoxNombreFichero.Text) ||
                comboBoxFase == null)
                return;

            Result = MessageBoxResult.OK;
            Description = textBoxDescription.Text;
            FileName = textBoxNombreFichero.Text;
            using (var srv = new DezacContext())
            {
                string idFase = ((string)comboBoxFase.SelectedItem).Split('-')[0];
                Fase = srv.FASE.Where(f => f.IDFASE == idFase).ToList().FirstOrDefault();
            }
            Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            Close();
        }
    }
}
