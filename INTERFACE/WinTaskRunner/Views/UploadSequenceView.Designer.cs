﻿namespace WinTaskRunner.Views
{
    partial class UploadSequenceView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSeq = new System.Windows.Forms.Label();
            this.rbNewSeq = new System.Windows.Forms.RadioButton();
            this.rbSameSeq = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblFamilia = new System.Windows.Forms.Label();
            this.lblProducto = new System.Windows.Forms.Label();
            this.lblFase = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.btnFamilia = new System.Windows.Forms.Button();
            this.btnProducto = new System.Windows.Forms.Button();
            this.btnFase = new System.Windows.Forms.Button();
            this.btnUsuario = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbSeq = new System.Windows.Forms.ComboBox();
            this.txtDescripcionSecuencia = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.radCheckBoxReproceso = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxReproceso)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "PRODUCTO:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "FAMILIA:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(48, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "FASE:";
            // 
            // lblSeq
            // 
            this.lblSeq.AutoSize = true;
            this.lblSeq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeq.Location = new System.Drawing.Point(48, 127);
            this.lblSeq.Name = "lblSeq";
            this.lblSeq.Size = new System.Drawing.Size(40, 13);
            this.lblSeq.TabIndex = 9;
            this.lblSeq.Text = "TIPO:";
            // 
            // rbNewSeq
            // 
            this.rbNewSeq.AutoSize = true;
            this.rbNewSeq.Location = new System.Drawing.Point(269, 125);
            this.rbNewSeq.Name = "rbNewSeq";
            this.rbNewSeq.Size = new System.Drawing.Size(109, 17);
            this.rbNewSeq.TabIndex = 10;
            this.rbNewSeq.Text = "Nuevo sequencia";
            this.rbNewSeq.UseVisualStyleBackColor = true;
            this.rbNewSeq.Click += new System.EventHandler(this.rbTipoSecuencia_Click);
            // 
            // rbSameSeq
            // 
            this.rbSameSeq.AutoSize = true;
            this.rbSameSeq.Checked = true;
            this.rbSameSeq.Location = new System.Drawing.Point(148, 125);
            this.rbSameSeq.Name = "rbSameSeq";
            this.rbSameSeq.Size = new System.Drawing.Size(107, 17);
            this.rbSameSeq.TabIndex = 12;
            this.rbSameSeq.TabStop = true;
            this.rbSameSeq.Text = "Misma sequencia";
            this.rbSameSeq.UseVisualStyleBackColor = true;
            this.rbSameSeq.Click += new System.EventHandler(this.rbTipoSecuencia_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(48, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "DESCRIPCIÓN:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(51, 375);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(600, 59);
            this.txtDescripcion.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(48, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "USUARIO:";
            // 
            // lblFamilia
            // 
            this.lblFamilia.Location = new System.Drawing.Point(145, 34);
            this.lblFamilia.Name = "lblFamilia";
            this.lblFamilia.Size = new System.Drawing.Size(389, 13);
            this.lblFamilia.TabIndex = 1;
            this.lblFamilia.Text = "FAMILIA";
            // 
            // lblProducto
            // 
            this.lblProducto.Location = new System.Drawing.Point(145, 65);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(389, 13);
            this.lblProducto.TabIndex = 4;
            this.lblProducto.Text = "PRODUCTO";
            // 
            // lblFase
            // 
            this.lblFase.Location = new System.Drawing.Point(145, 96);
            this.lblFase.Name = "lblFase";
            this.lblFase.Size = new System.Drawing.Size(285, 13);
            this.lblFase.TabIndex = 7;
            this.lblFase.Text = "FASE";
            // 
            // lblUsuario
            // 
            this.lblUsuario.Location = new System.Drawing.Point(145, 323);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(389, 13);
            this.lblUsuario.TabIndex = 20;
            this.lblUsuario.Text = "USUARIO";
            // 
            // btnFamilia
            // 
            this.btnFamilia.Location = new System.Drawing.Point(576, 29);
            this.btnFamilia.Name = "btnFamilia";
            this.btnFamilia.Size = new System.Drawing.Size(75, 23);
            this.btnFamilia.TabIndex = 2;
            this.btnFamilia.Text = "...";
            this.btnFamilia.UseVisualStyleBackColor = true;
            this.btnFamilia.Click += new System.EventHandler(this.btnFamilia_Click);
            // 
            // btnProducto
            // 
            this.btnProducto.Location = new System.Drawing.Point(576, 60);
            this.btnProducto.Name = "btnProducto";
            this.btnProducto.Size = new System.Drawing.Size(75, 23);
            this.btnProducto.TabIndex = 5;
            this.btnProducto.Text = "...";
            this.btnProducto.UseVisualStyleBackColor = true;
            this.btnProducto.Click += new System.EventHandler(this.btnProducto_Click);
            // 
            // btnFase
            // 
            this.btnFase.Location = new System.Drawing.Point(576, 91);
            this.btnFase.Name = "btnFase";
            this.btnFase.Size = new System.Drawing.Size(75, 23);
            this.btnFase.TabIndex = 8;
            this.btnFase.Text = "...";
            this.btnFase.UseVisualStyleBackColor = true;
            this.btnFase.Click += new System.EventHandler(this.btnFase_Click);
            // 
            // btnUsuario
            // 
            this.btnUsuario.Location = new System.Drawing.Point(576, 318);
            this.btnUsuario.Name = "btnUsuario";
            this.btnUsuario.Size = new System.Drawing.Size(75, 23);
            this.btnUsuario.TabIndex = 21;
            this.btnUsuario.Text = "...";
            this.btnUsuario.UseVisualStyleBackColor = true;
            this.btnUsuario.Click += new System.EventHandler(this.btnUsuario_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(48, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "NOMBRE FICHERO:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(51, 283);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(600, 20);
            this.txtNombre.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(48, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "SECUENCIA:";
            // 
            // cbSeq
            // 
            this.cbSeq.FormattingEnabled = true;
            this.cbSeq.Location = new System.Drawing.Point(148, 154);
            this.cbSeq.Name = "cbSeq";
            this.cbSeq.Size = new System.Drawing.Size(503, 21);
            this.cbSeq.TabIndex = 14;
            // 
            // txtDescripcionSecuencia
            // 
            this.txtDescripcionSecuencia.Location = new System.Drawing.Point(51, 209);
            this.txtDescripcionSecuencia.Multiline = true;
            this.txtDescripcionSecuencia.Name = "txtDescripcionSecuencia";
            this.txtDescripcionSecuencia.Size = new System.Drawing.Size(600, 35);
            this.txtDescripcionSecuencia.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(48, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "DESCRIPCIÓN SECUENCIA:";
            // 
            // radCheckBoxReproceso
            // 
            this.radCheckBoxReproceso.Location = new System.Drawing.Point(494, 93);
            this.radCheckBoxReproceso.Name = "radCheckBoxReproceso";
            this.radCheckBoxReproceso.Size = new System.Drawing.Size(73, 18);
            this.radCheckBoxReproceso.TabIndex = 24;
            this.radCheckBoxReproceso.Text = "Reproceso";
            // 
            // UploadSequenceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.radCheckBoxReproceso);
            this.Controls.Add(this.txtDescripcionSecuencia);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbSeq);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnUsuario);
            this.Controls.Add(this.btnFase);
            this.Controls.Add(this.btnProducto);
            this.Controls.Add(this.btnFamilia);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.lblFase);
            this.Controls.Add(this.lblProducto);
            this.Controls.Add(this.lblFamilia);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblSeq);
            this.Controls.Add(this.rbNewSeq);
            this.Controls.Add(this.rbSameSeq);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UploadSequenceView";
            this.Size = new System.Drawing.Size(704, 455);
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxReproceso)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSeq;
        private System.Windows.Forms.RadioButton rbNewSeq;
        private System.Windows.Forms.RadioButton rbSameSeq;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblFamilia;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.Label lblFase;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Button btnFamilia;
        private System.Windows.Forms.Button btnProducto;
        private System.Windows.Forms.Button btnFase;
        private System.Windows.Forms.Button btnUsuario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbSeq;
        private System.Windows.Forms.TextBox txtDescripcionSecuencia;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxReproceso;
    }
}
