﻿using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class REC3 : DeviceBase
    {
        public REC3()
        {
        }

        public REC3(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public RegisterIDP ReadRegisterIDP()
        {
            LogMethod();

            return Modbus.Read<RegisterIDP>();
        }

        public CalibOffsetRdo ReadCalibOffsetRdo()
        {
            LogMethod();

            return Modbus.Read<CalibOffsetRdo>();
        }

        public CalibFactorRdo ReadCalibFactorRdo()
        {
            LogMethod();

            return Modbus.Read<CalibFactorRdo>();
        }

        public CuadraticPoints ReadCuadraticPoint()
        {
            LogMethod();

            return Modbus.Read<CuadraticPoints>();
        }
      
        public RdoRdValue ReadRdoRdValue()
        {
            LogMethod();

            var RdoValue= Modbus.Read<RdoRdValue>();

            if (RdoValue.ValueRdoRdNeutro > 60000)
                RdoValue.ValueRdoRdNeutro = (ushort)(RdoValue.ValueRdoRdNeutro - 65365);

            if (RdoValue.ValueRdoRdFase1 > 60000)
                RdoValue.ValueRdoRdFase1 = (ushort)(RdoValue.ValueRdoRdFase1 - 65365);

            if (RdoValue.ValueRdoRdFase2 > 60000)
                RdoValue.ValueRdoRdFase2 = (ushort)(RdoValue.ValueRdoRdFase2 - 65365);

            if (RdoValue.ValueRdoRdFase3 > 60000)
                RdoValue.ValueRdoRdFase3 = (ushort)(RdoValue.ValueRdoRdFase3 - 65365);

            return RdoValue;
        }

        public RegistersLevelInsolation ReadLevelIsolation()
        {
            LogMethod();

            return Modbus.Read<RegistersLevelInsolation>();
        }

        public RegistersRccLevelInsolation ReadLevelRccIsolation()
        {
            LogMethod();

            return Modbus.Read<RegistersRccLevelInsolation>();
        }

        public void WriteNumSerie(UInt32 value)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersIDP.NumSerie, (int)value);
        }

        public void WriteBastidor(int value)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersIDP.Bastidor, value);
        }

        public void WriteBastidorPCB(int value)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersIDP.BastidorPCB, value);
        }

        public void WriteErrorCode(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersIDP.ErrorCode, value);
        }

        public void WriteNormalModeRegisters(RegistersNormalMode register, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)register, value);
        }

        public ushort ReadNormalModeRegisters(RegistersNormalMode register)
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)register);
        }

        public void WriteFactoryModeRegisters(RegistersFactoryMode register, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)register, value);
        }

        public ushort ReadFactoryModeRegisters(RegistersFactoryMode register)
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)register);
        }

        public void WriteCostimuzeRegisters(RegistersCostumize register, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)register, value);
        }

        public ushort ReadCostimuzeRegisters(RegistersCostumize register)
        {
            LogMethod();

           return Modbus.ReadRegister((ushort)register);
        }

        public void WriteModel(ModeloValue value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.Model, (ushort)value);
        }

        public ModeloValue ReadModel()
        {
            LogMethod();

            return (ModeloValue)Modbus.ReadRegister((ushort)RegistersCostumize.Model);
        }
   
        public void WriteSubModel(string value)
        {
            LogMethod();

            Modbus.WriteString((ushort)RegistersCostumize.Submodel, value);
        }

        public string ReadSubModel()
        {
            LogMethod();

           return Modbus.ReadString((ushort)RegistersCostumize.Submodel, 2);
        }

        public void WriteParametrizacionCode(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.ParamCode, value);
        }

        public ushort ReadParametrizacionCode()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)RegistersCostumize.ParamCode);
        }

        public void WriteSelectionRdRdo(SelectionRdRdoValue value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.SelecctionRdoRd, (ushort)value);
        }

        public SelectionRdRdoValue ReadSelectionRdRdo()
        {
            LogMethod();

           return (SelectionRdRdoValue)Modbus.ReadRegister((ushort)RegistersCostumize.SelecctionRdoRd);
        }

        public void WriteSecurityRelayBloq(SecurityRelayBloqValue value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.SecurityRelayBloq, (ushort)value);
        }

        public SecurityRelayBloqValue ReadSecurityRelayBloq()
        {
            LogMethod();

            return (SecurityRelayBloqValue)Modbus.ReadRegister((ushort)RegistersCostumize.SecurityRelayBloq);
        }

        public void WriteNumReconexionSecuence(NumReconexionSecuenceValue value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.ReconexionSecuence, (ushort)value);
        }

        public NumReconexionSecuenceValue ReadNumReconexionSecuence()
        {
            LogMethod();

           return (NumReconexionSecuenceValue)Modbus.ReadRegister((ushort)RegistersCostumize.ReconexionSecuence);
        }
        
        public void WriteSupervisionAjust(SupervisionAjust1Value value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.SupervisionAdjust, (ushort)value);
        }

        public SupervisionAjust1Value ReadSupervisionAjust()
        {
            LogMethod();

           return (SupervisionAjust1Value)Modbus.ReadRegister((ushort)RegistersCostumize.SupervisionAdjust);
        }

        public void WriteRearmeMode(RearmeModeValue value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCostumize.RearmeMode, (ushort)value);
        }

        public RearmeModeValue ReadRearmeMode()
        {
            LogMethod();

            return (RearmeModeValue)Modbus.ReadRegister((ushort)RegistersCostumize.RearmeMode);
        }

        public void WriteLevelIsolation(RegistersLevelIsolation Register, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Register, value);
        }
 
        public void Reset()
        {
            LogMethod();

            Modbus.Write((ushort)RegistersNormalMode.ResetRearmeCommand, (ushort)2);
        }

        public void Rearme()
        {
            LogMethod();

            Modbus.Write((ushort)RegistersNormalMode.ResetRearmeCommand, (ushort)1);
        }

        public void WorkMode(WorkModeValue value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersNormalMode.TestModeCommand, (ushort)value);
        }

        public void WriteReconnectionTableElement(byte position, ushort value)
        {
            if (position>31)
                throw new Exception(string.Format("Posición de la tabla de reconexión introducido inválido, se intenta escribir sobre la posición {0} cuando la máxima posicion es la 32", position + 1));
            if (value > 2046)
                throw new Exception(string.Format("Valor a escribir en la tabla de reconexionado inválido, se intenta escribir un valor {0} cuando el valor máximo es 2047", value));

            LogMethod();

            ushort valueToSend = position;

            valueToSend = (ushort)(valueToSend << 11);

            valueToSend += value;

            Modbus.Write((ushort)RegistersNormalMode.ReconnectionTable, valueToSend);
        }

        public void WriteDeleteFlashPage(DeletePageSetup value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.PageSetupDelete, (ushort)value);
        }  

        public void WriteMeasureCommand(MeasureCommand value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.MeasureCommand, (ushort)value);
        }

        public void WriteMeasurePointsCuadraticCommand(CuadraticPointsCommand value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.CuadraticPointsCalculateCommand, (ushort)value);
        }

        public UInt16 ReadFimrwareVersion()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)RegistersInternals.FirmwareVersion);
        }

        public ErrorState ReadErrorState(bool throwException = true)
        {
            LogMethod();

            var val= (ErrorState)Modbus.ReadRegister((ushort)RegistersInternals.ErrorState);

            if (throwException)
                switch (val)
                {
                    case ErrorState.ErrorParam:
                        throw new Exception("Error Equipo con parametrizacion incorrecta (Leds Verde + Rojo parpadeo)");

                    case ErrorState.ErrorFlashPageSetup:
                        throw new Exception("Error en la pagina Flash de SETUP (Leds Verde + Rojo parpadeo)");

                    case ErrorState.ErrorFlashPageIDP:
                        throw new Exception("Error en la pagina Flash de IDP (Leds Verde + Rojo parpadeo)");

                    case ErrorState.ErrorVhValue:
                        throw new Exception("Error el valor en Vh=12V no es correcto (Leds Verde + Rojo parpadeo)");
                }

            return val;
        }

        public InputState ReadInputState()
        {
            LogMethod();

            return (InputState)Modbus.ReadRegister((ushort)RegistersInternals.InputState);
        }

        public OuputState ReadOuputState()
        {
            LogMethod();

            return (OuputState)Modbus.ReadRegister((ushort)RegistersInternals.OuputState);
        }

        public void WriteOutputs(OuputState value)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.Outputs, (ushort)value);
        }

        public ushort ReadReconexionNumberByTrip()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)RegistersInternals.ReconexionNumber);
        }

        public ushort ReadVoltageLine()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)RegistersInternals.VoltageLine);
        }

        public FasesWithFugas ReadFasesWithFugas()
        {
            LogMethod();

            return (FasesWithFugas)Modbus.ReadRegister((ushort)RegistersInternals.FasesWithFugas);
        }

        public ushort ReadRecBloqStatus()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)RegistersInternals.RecBloqStatus);
        }

        public ushort ReadValueRcc()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)RegistersInternals.ValueRcc);
        }

        public CalibOffsetRcc ReadCalibOffsetRcc()
        {
            LogMethod();

            return Modbus.Read <CalibOffsetRcc>();
        }

        public CalibFactorRcc ReadCalibFactorRcc()
        {
            LogMethod();

            return Modbus.Read<CalibFactorRcc>();
        }

        public ushort ReadReconnectionTableElement(byte tablePosition)
        {
            if (tablePosition > 31)
                throw new Exception("Posición de la tabla a leer fuera de márgenes");

            ushort positionToRead = tablePosition;

            positionToRead = (ushort)(positionToRead << 11);

            positionToRead += 0x7FF;

            Modbus.Write((ushort)RegistersNormalMode.ReconnectionTable, positionToRead);

            var reading = Modbus.ReadRegister((ushort)RegistersNormalMode.ReconnectionTable);

            return reading;
        }

        public void WriteCalibFactorRdo(int[] CalibFactor)
        {
            if (CalibFactor.Length > 2)
                WriteCalibFactorRdo(CalibFactor[0], CalibFactor[1], CalibFactor[2], CalibFactor[3]);
            else
                WriteCalibFactorRdo(CalibFactor[0], CalibFactor[1]);

        }

        public void WriteCalibFactorRdo(int CalibFactorDefault)
        {
            WriteCalibFactorRdo(CalibFactorDefault, CalibFactorDefault, CalibFactorDefault, CalibFactorDefault);
        }

        public void WriteCalibFactorRdo(int CalibFactorRdoNeutro, int CalibFactorRdoFase1)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersCalib.CalibFactorRdoNeutro, CalibFactorRdoNeutro);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibFactorRdoFase1, CalibFactorRdoFase1);
        }

        public void WriteCalibFactorRdo(int CalibFactorRdoNeutro, int CalibFactorRdoFase1, int CalibFactorRdoFase2, int CalibFactorRdoFase3)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersCalib.CalibFactorRdoNeutro, CalibFactorRdoNeutro);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibFactorRdoFase1, CalibFactorRdoFase1);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibFactorRdoFase2, CalibFactorRdoFase2);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibFactorRdoFase3, CalibFactorRdoFase3);
        }

        public void WriteCalibOffsetRdo(int[] CalibOffset)
        {
            if (CalibOffset.Length > 2)
                WriteCalibOffsetRdo(CalibOffset[0], CalibOffset[1], CalibOffset[2], CalibOffset[3]);
            else
                WriteCalibOffsetRdo(CalibOffset[0], CalibOffset[1]);

        }

        public void WriteCalibOffsetRdo(int CalibOffsetDefault)
        {

            WriteCalibOffsetRdo(CalibOffsetDefault, CalibOffsetDefault, CalibOffsetDefault, CalibOffsetDefault);
        }

        public void WriteCalibOffsetRdo(int CalibOffsetRdoNeutro, int CalibOffsetRdoFase1)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersCalib.CalibOffsetRdoNeutro, CalibOffsetRdoNeutro);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibOffsetRdoFase1, CalibOffsetRdoFase1);
        }

        public void WriteCalibOffsetRdo(int CalibOffsetRdoNeutro, int CalibOffsetRdoFase1, int CalibOffsetRdoFase2, int CalibOffsetRdoFase3)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)RegistersCalib.CalibOffsetRdoNeutro, CalibOffsetRdoNeutro);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibOffsetRdoFase1, CalibOffsetRdoFase1);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibOffsetRdoFase2, CalibOffsetRdoFase2);
            Modbus.WriteInt32((ushort)RegistersCalib.CalibOffsetRdoFase3, CalibOffsetRdoFase3);
        }

        public void WriteCalibOffsetRcc(ushort CalibOffsetRccVh, ushort CalibOffsetRccVshunt)
        {

            LogMethod();

            Modbus.Write((ushort)RegistersCalib.CalibOffsetRccVh, CalibOffsetRccVh);
            Modbus.Write((ushort)RegistersCalib.CalibOffsetRccVshunt, CalibOffsetRccVshunt);
        }

        public void WriteCalibFactorRcc(ushort CalibFactorRccVh, ushort CalibFactorRccVshunt)
        {
            LogMethod();

            Modbus.Write((ushort)RegistersCalib.CalibFactorRccVh, CalibFactorRccVh);
            Modbus.Write((ushort)RegistersCalib.CalibFactorRccVshunt, CalibFactorRccVshunt);
        }


        public void SendReconnectionTable(ReconnectionTableConfiguration reconnectionInfo)
        {
            if (reconnectionInfo.HasReconnections)
            {
                byte tablePosition = 2;
                WriteReconnectionTableElement(0, reconnectionInfo.TableSize);
                WriteReconnectionTableElement(1, reconnectionInfo.TotalAmountOfReconnections);
                foreach (var stage in reconnectionInfo.listStages)
                    if (stage.HasValue)
                        tablePosition = SendReconnectionStage(stage, tablePosition);
                    else
                        continue;
            }
        }

        public byte SendReconnectionStage(ReconnectionTableConfiguration.ReconnectionStage? reconnectionStage, byte stagePosition)
        {
            WriteReconnectionTableElement(stagePosition, reconnectionStage.Value.AmountOfReconnections);
            stagePosition++;
            WriteReconnectionTableElement(stagePosition, reconnectionStage.Value.ReconnectionTime);
            stagePosition++;
            WriteReconnectionTableElement(stagePosition, reconnectionStage.Value.ResetTime);
            stagePosition++;
            return stagePosition;
        }

        //-----------------------------------------------------------------------------
        //----------------   TEST PLACAS ----------------------------------------------
        //-----------------------------------------------------------------------------
        public double ReadValueMeasureV3()
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.ValueMeasureV3, (ushort)0x1111);

            Thread.Sleep(1500);

            return Modbus.ReadRegister((ushort)RegistersFactoryMode.ValueMeasureV3) / 100D;
        }

        public double ReadValueV12()
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.ValueMeasureV12, (ushort)0x1111);

            Thread.Sleep(1500);

            var res12 = Modbus.ReadRegister((ushort)RegistersFactoryMode.ValueMeasureV12);

            var res3 = ReadValueMeasureV3();

            return res12 * (res3 / Math.Pow(2, 12)) / 0.175;
        }

        public double ReadValueMeasureImotor()
        {
            LogMethod();

            Modbus.Write((ushort)RegistersFactoryMode.ValueMeasureImotor, (ushort)0x1111);

            Thread.Sleep(1500);

            var res = Modbus.ReadRegister((ushort)RegistersFactoryMode.ValueMeasureImotor);

            var res3 = ReadValueMeasureV3();

            return res * (res3 / Math.Pow(2, 12)) / 3.3;
        }


        //-----------------------------------------------------------------------------
        //-----------------------------------------------------------------------------

        public enum SelectionRdRdoValue
        {
            _30mA = 0,
            _300mA = 1,
            Custom = 2,
        }

        public enum SecurityRelayBloqValue
        {
            Standard = 0,
            Positive = 1,
        }

        public enum NumReconexionSecuenceValue
        {
            AnyReconexion = 0,
            Custom = 1,
            GE = 2,
            Circutor = 3,
            GE_RCBO = 4
        }

        public enum SupervisionAjust1Value
        {
            Whithout_Supervision = 0,
            Undervoltage = 1,
            Overvoltage = 2,
            Undervoltage_And_Overvoltage = 3
        }

        public enum ModeloValue
        {
            RCB2Polos = 0,
            RCB4Polos = 1,
            RCBO2Polos = 2,
        }

        public enum RearmeModeValue
        {
            WithSupervision = 0,
            Temporization = 1,
            microSwitch = 2,
        }

        public enum WorkModeValue
        {
            Normal = 1,
            Test = 2,
        };

        public enum DeletePageSetup
        {
            PageParametrizacion = 0x1111,
            PageSetup = 0x2222,
            PagIDP = 0x3333,
            Tablas = 0xFFFF,
        };

        public enum MeasureCommand
        {
            Rdo = 0x1111,
            Rcc = 0x2222,
        };

        public enum CuadraticPointsCommand
        {
            ClearData = 0x0000,
            MeasureVc = 0x1111,
        }

        public enum ErrorState
        {
            NoError = 0,
            ErrorFlashPageSetup = 1,
            ErrorParam = 2,
            ErrorFlashPageIDP = 4,
            ErrorVhValue = 8,
        };

        public enum InputState
        {
            AllOff = 0,
            Trip = 1,
            DisabledSupervisionRdoRd = 2,
        };

        [Flags]
        public enum OuputState
        {
            AllOff = 0,
            ReleyBloq = 1,
            GreenLed = 2,
            RedLed = 4,
            MotorUp = 8,
            MotorDown = 0x0010,
        };

        public enum FasesWithFugas
        {
            Neutro = 0,
            L1 = 1,
            L2 = 2,
            L3 = 3,
            AnyFugas = 4,
        };

        public enum RegistersInternals
        {
            FirmwareVersion = 0x005A,
            ErrorState = 0x005B,
            DeviceState = 0x005C,
            InputState = 0x005D,
            OuputState = 0x005E,
            ReconexionNumber = 0x005F,
            VoltageLine = 0x0060,
            FasesWithFugas=0x0065,
            RecBloqStatus=0x0066,
            ValueRcc=0x0067,
        };

        public enum RegistersIDP
        {
            NumSerie = 0x0000,
            Bastidor = 0x0002,
            BastidorPCB = 0x0004,
            ErrorCode = 0x0006,
        };

        public enum RegistersCalib
        {
            CalibFactorRdoNeutro = 0x0007,
            CalibFactorRdoFase1 = 0x0009,
            CalibFactorRdoFase2 = 0x000B,
            CalibFactorRdoFase3 = 0x000D,
            CalibOffsetRdoNeutro = 0x000F,
            CalibOffsetRdoFase1 = 0x0011,
            CalibOffsetRdoFase2 = 0x0013,
            CalibOffsetRdoFase3 = 0x0015,
            CalibOffsetRccVshunt = 0x0017,
            CalibOffsetRccVh = 0x0018,
            CalibFactorRccVshunt = 0x0019,
            CalibFactorRccVh = 0x001A,
        };

        public enum RegistersCostumize
        {
            SelecctionRdoRd = 0x002B,
            SecurityRelayBloq = 0x002C,
            ReconexionSecuence = 0x002D,
            Model = 0x003A,
            Submodel = 0x003B,
            SupervisionAdjust = 0x003D,
            RearmeMode = 0x0044,
            ParamCode = 0x0050,
        };

        public enum RegistersLevelIsolation
        {
            Rdo30mAHighLevel = 0x003E,
            Rd30mALowLevel = 0x003F,
            Rdo300mAHighLevel = 0x0040,
            Rd300mALowLevel = 0x0041,
            RdoCustomHighLevel = 0x0042,
            RdCustomLowLevel = 0x0043,
            RccoCustomHighLevel = 0x0045,
            RccCustomLowLevel = 0x0046,
        };

        public enum RegistersNormalMode
        {
            ResetRearmeCommand = 0x007A,
            TestModeCommand = 0x007B,
            ReconnectionTable = 0x007C,
        };

        public enum RegistersFactoryMode
        {
            PageSetupDelete = 0x009A,
            Outputs = 0x009B,
            ValueMeasureV3 = 0x009C,
            ValueMeasureV12 = 0x009D,
            ValueMeasureImotor = 0x009E,
            MeasureCommand = 0x009F,
            CuadraticPointsCalculateCommand = 0x00A0,
        };


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct RegisterIDP
        {
            public UInt32 NumSerie;
            public UInt32 Bastidor;
            public UInt32 BastidorPCB;
            public UInt16 ErrorCode;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x003E)]
        public struct RegistersLevelInsolation
        {
            public UInt16 Rdo30mAHighLevel;
            public UInt16 Rd30mALowLevel;
            public UInt16 Rdo300mAHighLevel;
            public UInt16 Rd300mALowLevel;
            public UInt16 RdoCustomHighLevel;
            public UInt16 RdCustomLowLevel;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0045)]
        public struct RegistersRccLevelInsolation
        {
            public UInt16 RccoCustomHighLevel;
            public UInt16 RccCustomLowLevel;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0007)]
        public struct CalibFactorRdo
        {
            public Int32 CalibFactorRdoNeutro;
            public Int32 CalibFactorRdoFase1;
            public Int32 CalibFactorRdoFase2;
            public Int32 CalibFactorRdoFase3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x000F)]
        public struct CalibOffsetRdo
        {
            public Int32 CalibOffsetRdoNeutro;
            public Int32 CalibOffsetRdoFase1;
            public Int32 CalibOffsetRdoFase2;
            public Int32 CalibOffsetRdoFase3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0017)]
        public struct CalibOffsetRcc
        {
            public UInt16 CalibOffsetRccVshunt;
            public UInt16 CalibOffsetRccVh;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0019)]
        public struct CalibFactorRcc
        {
            public UInt16 CalibFactorRccVshunt;
            public UInt16 CalibFactorRccVh;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0061)]
        public struct RdoRdValue
        {
            public UInt16 ValueRdoRdNeutro;
            public UInt16 ValueRdoRdFase1;
            public UInt16 ValueRdoRdFase2;
            public UInt16 ValueRdoRdFase3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x00A1)]
        public struct CuadraticPoints
        {
            public Int32  ValuePointsCuadratics;
            public UInt16 ValueRccVshunt;
            public UInt16 ValueRccVh;
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public struct ReconnectionTableConfiguration
        {
            public bool HasReconnections
            {
                get
                {
                    bool hasReconnections = false;
                    listStages.ForEach(x => hasReconnections |= x != null);
                    return hasReconnections;
                }
            }

            public ReconnectionTableConfiguration(int reconnectionStages)
            {
                if (reconnectionStages > 10)
                    throw new Exception("Error en la configuración de la tabla de reconexión, el numero de etapas esta limitado a 10");
                listStages = new List<ReconnectionStage?>(reconnectionStages);
            }

            public ushort TableSize
            {
                get
                {
                    ushort size = 2;
                    listStages.ForEach((x) => size += (ushort)(x.HasValue ? 3 : 0));
                    if (size > 32)
                        throw new Exception(string.Format("Error en la configuración de la tabla de reconexión, la capacidad de la tabla es de 32 elementos, mientras que se intenta introducir {0} elementos", size));
                    return size;
                }
            }
            public ushort TotalAmountOfReconnections
            {
                get
                {
                    ushort totalReconnections = 0; 
                    listStages.ForEach((x) => totalReconnections += (ushort)(x.HasValue ? x.Value.AmountOfReconnections : 0));
                    if (totalReconnections > 2046)
                        throw new Exception(string.Format("Error en la configuración de la tabla de reconexión, la cantidad total de reconexiones no puede superar las 2046 reconexiones, mientras que se intenta introducir {0} reconexiones", totalReconnections));
                    return totalReconnections; 
                }
            }

            public List<ReconnectionStage?> listStages;

            public struct ReconnectionStage
            {
                public ushort AmountOfReconnections;
                public ushort ReconnectionTime;
                public ushort ResetTime;

                public ReconnectionStage(string info)
                {
                    List<string> reconnectionConfiguration = new List<string>(3);
                    var reconnectionParameters = info.Split(';');

                    if (reconnectionParameters.Length != 3)
                        throw new Exception("Error en la configuración de la etapa de reconexión, numero de parametros incorrectos");
                    
                    foreach (string reconnectionParameter in reconnectionParameters)
                    {
                        ushort checkedValue = Convert.ToUInt16(reconnectionParameter);
                        if (checkedValue > 2046)
                            throw new Exception(string.Format("Error en la configuración de la etapa de reconexión, valor de parametro fuera de márgenes, valor máximo 2046, valor introducido: {0}", checkedValue));
                        reconnectionConfiguration.Add(reconnectionParameter);
                    }
                    AmountOfReconnections = Convert.ToUInt16(reconnectionConfiguration[0]);
                    ReconnectionTime = Convert.ToUInt16(reconnectionConfiguration[1]);
                    ResetTime = Convert.ToUInt16(reconnectionConfiguration[2]);
                }

                public override string ToString()
                {
                    return "{" + AmountOfReconnections.ToString() + ";" + ReconnectionTime.ToString() + ";" + ResetTime.ToString() + "}";
                }
            }

            public string GetStageListString()
            {
                if (HasReconnections)
                {
                    var stageList = new List<string>();
                    listStages.ForEach(x => stageList.Add(x.ToString()));
                    return string.Join(", ", stageList);
                }
                else
                    return string.Empty;
            }
        }
    }
}
