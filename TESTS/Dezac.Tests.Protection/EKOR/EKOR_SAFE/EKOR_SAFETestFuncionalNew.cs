﻿using Comunications;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.02)]
    public class EKOR_SAFETestFuncionalNew : EKOR_SAFETestBaseNew
    {
        private const int OUT_KEY_SIDE = 54;
        private const int OUT_ACTIVATE_VGEN = 41;

        private int numUSBRecovery = 0;

        public virtual void OnMessageException(object sender, MessageExceptionHandler e)
        {
            if (e.Exception is TimeoutException || e.Exception is System.IO.IOException)
            {
                if (e.Throw)
                {
                    ResetUSB();
                    numUSBRecovery += 1;
                }

            }
            else
                logger.Warn(e.Exception.Message);
        }
        
        public void TestInitialization()
        {
            var tower3 = tower as Tower3;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower3))
            {
                tower3 = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower3);
            }
            if (tower3 == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            tower = tower3;
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            util = new UtillajeEkorSafe(this, tower) as IUtillajeEkor;

            TestPlacas = false;

            Ekor = new EKOR_SAFE_OLD(Comunicaciones.SerialPort);

            tower.ActiveVoltageCircuit();
        }

        public void TestLedSIDE()
        {
            CAMERA_IDS_LEDS = GetVariable<string>("CAMARA_IDS_LEDS", CAMERA_IDS_LEDS);

            tower.IO.DO.On(OUT_KEY_SIDE);

            Delay(500, "Espera leds");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LEDS, "EKOR_LED_SIDE", "LED_SIDE", "EKOR_SAFE");

            tower.IO.DO.Off(OUT_KEY_SIDE);
        }

        public override void TestVUSB(string nameDevicePortVirtual)
        {
            SamplerWithCancel((p) =>
            {
                var versionEkor = Ekor.ReadVersion();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionEkor.version, Identificacion.VERSION_FIRMWARE.Trim(), Error().UUT.FIRMWARE.NO_COINCIDE("version del firmware"), ParamUnidad.SinUnidad);

                var versionMicro = Ekor.ReadMicrocontrolerType();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, versionMicro.cpu, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.FIRMWARE.NO_COINCIDE("tipo microcontrolador"), ParamUnidad.SinUnidad);

                Resultado.Set("CPU REVISION", versionMicro.revision, ParamUnidad.SinUnidad);

                var versionBoot = Ekor.ReadVersionBoot();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_BOOT, versionBoot, Identificacion.VERSION_BOOT.Trim(), Error().UUT.FIRMWARE.NO_COINCIDE("version de boot"), ParamUnidad.SinUnidad);

                return true;

            }, "Error de comunicaciones por el puerto USB frontal al preguntar Version firmware el eKOR", 5, 1000, 1000, false, false,
            (p) =>
            {
                Error().UUT.COMUNICACIONES.TIME_OUT().Throw();
            });

            Ekor.WriteBastidor(TestInfo.NumBastidor.Value.ToString());

            base.TestVUSB();

            var stateFlash = Ekor.ReadStateFLASH();

            Resultado.Set("FLAH_ID", stateFlash.ID, ParamUnidad.SinUnidad);
            Resultado.Set("FLAH_STATUS", stateFlash.state.ToString(), ParamUnidad.SinUnidad);

            Assert.IsTrue(stateFlash.state == EKOR_SAFE_OLD.stateFLAASH.READY, Error().UUT.HARDWARE.ALARMA(string.Format("memoria flash {0}", stateFlash.state.ToString())));
        }

        public void TestVAUX()
        {
            base.TestVAUX();
        }

        public void TestVGEN()
        {
            tower.IO.DO.On(OUT_ACTIVATE_VGEN);
            tower.Chroma.ApplyOffAndWaitStabilisation();
            base.TestVGEN();
            tower.IO.DO.Off(OUT_ACTIVATE_VGEN);
        }

        public void TestFinish()
        {
            interalTestFinish();            
        }
        ///////////////////////////////////////////////////////

        private void ResetUSB(Action func = null)
        {
            try
            {
                Ekor.Cirbus.ClosePort();
            }
            catch (Exception)
            {

            }

            util.ResetUSB();
        }
    }
}
