﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(TestConsumoDescription))]
    public class TestConsumo : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Lectura de medida del tiempo de espera")]
        [DefaultValue(500)]
        public int WaitTimeReadMeasure { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de voltaje de test")]
        public TypeTestVoltage typeTestVoltage { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de punto de test")]
        public TypeTestPoint typeTestPoint { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de ejecución de test")]
        public TypeTestExecute typeTestExecute { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de Fuente de alimentación")]
        public Extensions.TypePowerSource typePowerSource { get; set; }

        [Category("3. Configuration")]
        [Description("Voltaje")]
        [DefaultValue(230)]
        public double Voltage { get; set; }

        [Category("3. Configuration")]
        [Description("Frecuencia")]
        [DefaultValue(50)]
        public double Frecuency { get; set; }

        [Category("3. Configuration")]
        [Description("Corriente")]
        [DefaultValue(1)]
        public double Current { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de intervalo")]
        [DefaultValue(1000)]
        public int IntervalTime { get; set; }

        [Category("3. Configuration")]
        [Description("Iteraciones")]
        [DefaultValue(4)]
        public int Iterations { get; set; }

        [Category("4. Results")]
        [Description("Nombre del resultado")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            var configuration = new TestPointConfiguration()
            {
                Current = Current,
                Frecuency = Frecuency,
                Iteraciones = Iterations,
                ResultName = DataOutput,
                TiempoIntervalo = IntervalTime,
                typePowerSource = typePowerSource,
                typeTestExecute = typeTestExecute,
                typeTestPoint = typeTestPoint,
                typeTestVoltage = typeTestVoltage,
                Voltage = Voltage,
                WaitTimeReadMeasure = WaitTimeReadMeasure
            };

            testBase.TestConsumo(tower, configuration);
        }
    }

    public class TestConsumoDescription : ActionDescription
    {
        public override string Name { get { return "TestConsumption"; } }
        public override string Description { get { return "Acción genérica para medir el Consumo"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
