namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTSEQUENCEFILE
    {
        public int NUMTSF { get; set; }
        public int NUMFAMILIA { get; set; }
        public Nullable<int> NUMPRODUCTO { get; set; }
        public Nullable<int> VERSION { get; set; }
        public string NOMBREFICHERO { get; set; }
        public string DESCRIPCION { get; set; }
        public System.DateTime FECHAALTA { get; set; }
        public string USUARIOALTA { get; set; }
        public byte[] FICHERO { get; set; }
        public Nullable<System.DateTime> FECHAVALIDACION { get; set; }
        public string USUARIOVALIDACION { get; set; }
        public string IDFASE { get; set; }
    
        public virtual PRODUCTO PRODUCTO { get; set; }
        public virtual T_FAMILIA T_FAMILIA { get; set; }
    }
}
