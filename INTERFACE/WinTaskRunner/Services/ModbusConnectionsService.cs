﻿using Dezac.Device;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace WinTaskRunner.Services
{
    public class ModbusConnectionsService : IDisposable
    {
        public List<ModbusConnection> Devices { get; set; } = new List<ModbusConnection>();

        public ModbusConnection Add(ModbusConnection.ModbusConnectionType type)
        {
            var device = new ModbusConnection { Type = type };

            device.Name = string.Format("{0}_{1}", type, device.Port);

            Devices.Add(device);

            return device;
        }

        public void Delete(ModbusConnection device)
        {
            Devices.Remove(device);
        }

        public void Dispose()
        {
            Devices.ForEach(p => p.Dispose());
            Devices.Clear();
        }
    }

    public class ModbusConnection : IDisposable
    {
        private static readonly ILog _logger = LogManager.GetLogger("MODBUS");

        public event EventHandler<bool> ConnectionStateChanged;

        public enum ModbusConnectionType
        {
            Serial,
            TCP,
            TCP_RTU
        }

        public enum EnumBaudRate
        {
            _9600 = 9600,
            _19200 = 19200,
            _38400 = 38400,
            _57600 = 57600,
            _115200 = 115200,
        }

        private ModbusDevice device;

        [ReadOnly(false)]
        public string Name { get; set; }
        [ReadOnly(false)]
        public ModbusConnectionType Type { get; set; }
        [ReadOnly(false)]
        public string HostName { get; set; }
        [ReadOnly(false)]
        public int Port { get; set; }
        [ReadOnly(false)]
        [DefaultValue(EnumBaudRate._9600)]
        public EnumBaudRate BaudRate { get; set; }

        [ReadOnly(false)]
        [DefaultValue(System.IO.Ports.Parity.None)]
        public System.IO.Ports.Parity Parity { get; set; }

        [ReadOnly(false)]
        public byte Periferic { get; set; }

        public ModbusConnection()
        {
            Periferic = 1;
            Port = 1;
        }

        [JsonIgnore]
        [Browsable(false)]
        public ModbusDevice Device
        {
            get
            {
                if (device == null)
                    Open();

                return device;
            }
        }

        public void Open()
        {
            Close();

            if (Type == ModbusConnectionType.Serial)
                device = new ModbusDeviceSerialPort(Port, (Int32)BaudRate, Parity, System.IO.Ports.StopBits.One, _logger);
            else if (Type == ModbusConnectionType.TCP)
                device = new ModbusDeviceTCP(HostName, Port, ModbusTCPProtocol.TCP, _logger);
            else if (Type == ModbusConnectionType.TCP_RTU)
                device = new ModbusDeviceTCP(HostName, Port, ModbusTCPProtocol.RTU, _logger);

            device.PerifericNumber = Periferic;

            SetReadOnlyProperties(true);

            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, true);
        }

        public void Close()
        {
            SetReadOnlyProperties(false);

            if (device != null)
            {
                device.Dispose();
                device = null;

                if (ConnectionStateChanged != null)
                    ConnectionStateChanged(this, false);
            }
        }

        public void Dispose()
        {
            Close();
        }

        private void SetReadOnlyProperties(bool readOnly)
        {
            PropertyDescriptorCollection descriptors = TypeDescriptor.GetProperties(GetType());

            foreach (PropertyDescriptor descriptor in descriptors)
            {
                var attrib = (ReadOnlyAttribute)descriptor.Attributes[typeof(ReadOnlyAttribute)];

                FieldInfo isReadOnly = attrib.GetType().GetField("isReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
                isReadOnly.SetValue(attrib, readOnly);
            }
        }
    }
}
