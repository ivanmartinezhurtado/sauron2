﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(WriteRegisterDescription))]
    public class WriteRegister : ActionBase
    {
        [Description("Dirección Modbus en decimal")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ushort Address { get; set; }

        [Description("Tipo registro Modbus HoldingRegister (03) InputRegister (04)")]
        [Category("3. Configuration")]
        [DefaultValue(TipoRegisterModbus.InputRegister)]
        public TipoRegisterModbus RegisterType { get; set; }

        [Description("Habilita en Modbus SingleInputRegister (06)")]
        [Category("3. Configuration")]
        [DefaultValue(false)]
        public bool UseSingleRegister06 { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        public ActionVariable SendValue { get; set; } = new ActionVariable();

        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDevice;
            var value = Convert.ToUInt16(SendValue.Value);
            uut.UseFunction06 = UseSingleRegister06;
            Context.ResultList.Add(string.Format("{0} - WriteInt16 1 Register Address {1} -> {2}", name, Address, value));
            Logger.InfoFormat("{0} - WriteInt16 1 Register Address {1} -> {2}", name, Address, value);
            uut.Write(Address, value);
        }
    }

    public class WriteRegisterDescription : ActionDescription
    {
        public override string Name { get { return "WriteRegister"; } }
        public override string Description { get { return "Escribe un registro modbus"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitModbusDeviceAction;DisposedModbusDeviceAction:2"; } }
    }
}
