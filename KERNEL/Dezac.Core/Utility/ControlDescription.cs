﻿using System;
using System.Drawing;

namespace Dezac.Core.Utility
{
    public class ControlDescription
    {
        public virtual string Name { get { return null; } }
        public virtual string Description { get { return null; } }
        public virtual string Category { get { return null; } }
        public virtual Image Icon { get { return Properties.Resources.Led; } }
        public virtual Image IconBig { get { return Properties.Resources.Led; } }
        public virtual Type SubcomponentType { get { return null; } }
        public virtual bool Visible { get { return true; } }
    }
}
