namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GrupoParametrizacion")]
    public partial class GrupoParametrizacion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TipoGrupo")]
        public int TipoGrupoId { get; set; }

        [Required]
        [StringLength(10)]
        [ForeignKey("Fase")]
        public string FaseId { get; set; }

        [Required]
        [StringLength(1)]
        public string TipoES { get; set; }

        [ForeignKey("Familia")]
        public int FamiliaId { get; set; }

        public Fase Fase { get; set; }
        public Familia Familia { get; set; }
        public TipoGrupoParametrizacion TipoGrupo { get; set; }
        public List<ValorParametro> Valores { get; set; }
    }
}
