﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;

namespace Instruments.Auxiliars
{
    [InstrumentsVersion(1.00)]
    public class FEASA_LedAnalyzer :  IDisposable
    {
        protected IProtocol protocol;
        protected MessageTransport transport;
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(FEASA_LedAnalyzer));

        protected CultureInfo enUS = new CultureInfo("en-US");
        private SerialPort sp;
        private byte portCom;

        private HSIResult lastHSIResult;
        private RGBIResult lastRGBIResult;

        public byte PortCom
        {
            get { return portCom; }
            set
            {
                portCom = value;

                if (sp == null)
                    return;

                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + portCom;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + portCom;
            }
        }

        public FEASA_LedAnalyzer()
        {
        }

        public void SetPort(byte _port)
        {
            PortCom = _port;

            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = sp.ReadTimeout = 5000;
            sp.BaudRate = 57600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + portCom.ToString();
            sp.DtrEnable = true;

            protocol = new ASCIIProtocol();
            protocol.CaracterFinRx = "\n";
            protocol.CaracterFinTx = "\n";
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
        }

        public void Capture(CaptureModes mode)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.CAPTURE, true, (int)mode));
        }

        public HSIResult GetHSI(int fiber)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.GET_HSI, true, fiber));
            return lastHSIResult;
        }

        public RGBIResult GetRGBI(int fiber)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.GET_RGBI, true, fiber));
            return lastRGBIResult;
        }

        public void PutFactor(int factor)
        {
            if (factor < 1 || factor > 15)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO("El factor de exposicion debe estar entre 1 y 15").Throw();
            SendCommand(new Command<PrivateCommands>(PrivateCommands.PUT_FACTOR, true, factor));
        }

        protected bool SendCommand<T>(Command<T> command)
        {
            try
            {
                _logger.InfoFormat("Command {0}", command);
                ExecuteCommand(command, 3);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var sauronExc = ex as SauronException;
                if (sauronExc != null)
                    throw;

                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO(ex.Message).Throw();
                return false;
            }
        }

        private void ExecuteCommand<T>(Command<T> command, int numRetries)
        {
            bool error = false;
            do
            {
                try
                {
                    error = false;
                    InternalSendCommand(command);
                }
                catch (Exception ex)
                {
                    error = true;
                    numRetries--;

                    _logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                    if (numRetries <= 0 || ex is NotSupportedException)
                        throw;
                }
            } while (error);
        }

        private void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;
            switch (command.Commands)
            {
                case (int)PrivateCommands.CAPTURE:
                case (int)PrivateCommands.PUT_FACTOR:
                    message = StringMessage.Create(enUS, dictionaryCommands[command.Commands], command.Args[0]);
                    response = transport.SendMessage<StringMessage>(message);
                    break;
                case (int)PrivateCommands.GET_HSI:
                    message = StringMessage.Create(enUS, dictionaryCommands[command.Commands], command.Args[0]);
                    response = transport.SendMessage<StringMessage>(message);
                    lastHSIResult = new HSIResult(response.Text);
                    break;
                case (int)PrivateCommands.GET_RGBI:
                    message = StringMessage.Create(enUS, dictionaryCommands[command.Commands], command.Args[0]);
                    response = transport.SendMessage<StringMessage>(message);
                    lastRGBIResult = new RGBIResult(response.Text);
                    break;
            }
        }
        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }

        public enum CaptureModes
        {
            Low = 1,
            Medium = 2,
            High = 3,
            Super = 4,
            Ultra = 5
        }

        private enum PrivateCommands
        {
            CAPTURE = 1,
            GET_HSI = 2,
            GET_RGBI = 3,
            PUT_FACTOR = 4,
        }

        private static readonly Dictionary<int, string> dictionaryCommands = new Dictionary<int, string>
        {
            { (int)PrivateCommands.CAPTURE, "C{0:0}" },
            { (int)PrivateCommands.GET_HSI, "GETHSI{0:00}" },
            { (int)PrivateCommands.GET_RGBI, "GETRGBI{0:00}" },
            { (int)PrivateCommands.PUT_FACTOR, "PUTFACTOR{0:00}" },
        };

        public struct HSIResult
        {
            public double Hue;
            public int Saturation;
            public int Intensity;

            public HSIResult(string response)
            {
                var aux = response.Replace("\r", "").Replace(".",",").Split(' ');
                Hue = double.Parse(aux[0]);
                Saturation = int.Parse(aux[1]);
                Intensity = int.Parse(aux[2]);
            }
        }

        public struct RGBIResult
        {
            public int Red;
            public int Green;
            public int Blue;
            public int Intensity;

            public RGBIResult(string response)
            {
                var aux = response.Replace("\r", "").Replace(".", ",").Split(' ');
                Red = int.Parse(aux[0]);
                Green = int.Parse(aux[1]);
                Blue = int.Parse(aux[2]);
                Intensity = int.Parse(aux[3]);
            }
        }
    }
}
