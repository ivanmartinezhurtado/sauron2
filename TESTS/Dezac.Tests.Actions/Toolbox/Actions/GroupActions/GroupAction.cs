﻿using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]  
    [DesignerAction(Type = typeof(GroupActionDescription))]
    public class GroupAction : ActionBase
    {
        public override void Execute()
        {
        }

        [Browsable(false)]
        public override bool IsGroupAction => true;

    }

    public class GroupActionDescription : ActionDescription
    {
        public override string Name { get { return "GroupAction"; }}
        public override string Category { get { return "Actions/GroupActions"; } }
        public override Image Icon { get { return Properties.Resources.GroupAction; } }
        public override string Description { get { return "Action para agrupar otros actions en una nivel inferior (hijos)"; } }
    }
}
