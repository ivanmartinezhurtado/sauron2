﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using TaskRunner.Tools;
using WinTaskRunner.Design;
using WinTaskRunner.Design.Documents;
using WinTaskRunner.Design.Services;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ScriptActionDescription))]
    public class ScriptAction : ActionBase
    {
        private string code;
        private Type program;

        private string templateScript = @"
using System;
using System.Linq;
using Dezac.Tests;
using Dezac.Tests.Actions.Utility;
{0}    

namespace {1}
{{
    public class {3} : {4}
    {{
        {2}
    }}
}}";

        public enum ScriptCodeType
        {
            CodeBlock,
            Class,
        }

        [Category("3. Configuration")]
        [Description("Método principal")]
        public string MainMethod { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de Script 'Class' o 'CodeBlock'")]
        [DefaultValue(ScriptCodeType.Class)]
        public ScriptCodeType ScriptType { get; set; }

        [Category ("3. Configuration")]
        [Description("Clase Base, por defecto es TestScriptBase")]
        [DefaultValue("TestScriptBase")]
        public string BaseClass { get; set; }

        [Category("3. Configuration")]
        [Description("Campo donde se debe escribir el código que se desea")]
        [EditorAttribute(typeof(ScriptEditor), typeof(UITypeEditor))]
        [DefaultValue("\npublic void Main()\n{\n\n}\n")]
        public string Code
        {
            get { return code; }
            set
            {
                code = value;
                program = null;
            }
        }

        public override void Execute()
        {
            if (program == null)
                Build();

            object instance = null;

            try
            {
                instance = Activator.CreateInstance(program);

                CallUtils.InvokeMethod(instance, MainMethod ?? "Main");

            }catch(Exception ex)
            {
                var exception = ex.InnerException;
                if (exception != null) 
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO(exception.Message).Throw() ;
      
                else
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO(ex.Message).Throw() ;
            }
            finally
            {
                if (instance is IDisposable)
                    ((IDisposable)instance).Dispose();
            }
        }

        private void Build()
        {
            var csc = new CSharpCodeProvider();
            var parameters = new CompilerParameters();
            parameters.GenerateExecutable = false;
            parameters.GenerateInMemory = true;

            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(p => !p.IsDynamic && p.Location.EndsWith(".dll"))
                .GroupBy(p => p.FullName);

            foreach (var asm in assemblies)
                parameters.ReferencedAssemblies.Add(asm.First().Location);

            string nameSpace = string.Format("Test{0}", DateTime.Now.Ticks);

            string usings = string.Empty;
            string src = string.Empty;

            using(var sr = new StringReader(Code))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                    if (line.StartsWith("using ", StringComparison.InvariantCultureIgnoreCase))
                        usings += line;
                    else
                        src += line;
            }

            string source = null;

            if (ScriptType == ScriptCodeType.CodeBlock)
                src = string.Format("public void {0}() {{\n{1}\n}}", MainMethod ?? "Main", src);

            string className = string.Format("Script{0}", DateTime.Now.Ticks);

            source = string.Format(templateScript, 
                usings, nameSpace, src, className, BaseClass ?? "Object");

            var sb = new StringBuilder();
            CompilerResults results = csc.CompileAssemblyFromSource(parameters, source);
            results.Errors.Cast<CompilerError>()
                .ToList()
                .ForEach(error => sb.Append(error.ErrorText));

            if (sb.Length > 0)
                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO(sb.ToString()).Throw() ;

            Assembly assembly = results.CompiledAssembly;
            program = assembly.GetType(nameSpace + "." + className);
        }
    }

    public class ScriptActionDescription : ActionDescription
    {
        public override string Name { get { return "ScriptAction"; } }
        public override string Category { get { return "Actions/Developer"; } }
        public override string Description { get { return "Crea un programa en código C# con contenido de la propiedad 'Code'"; } }
        public override Image Icon { get { return Properties.Resources.Other_xml_icon; } }
    }

    class ScriptEditor : UITypeEditor
    {
        private Dictionary<object, SourceCodeDocument> docs = new Dictionary<object,SourceCodeDocument>();

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            SourceCodeDocument doc = null;

            if (!docs.TryGetValue(context.Instance, out doc))
            {
                ScriptAction action = (ScriptAction)context.Instance;

                var docService = ServiceContainer.Resolve<IDocumentService>();

                doc = docService.AddNew(".cs") as SourceCodeDocument;
                doc.Text = action.Code;
                doc.TextChanged += (s, e) =>
                {
                    action.Code = e;
                };

                docs.Add(context.Instance, doc);
            }

            return doc.Text;
        }
    }
}
