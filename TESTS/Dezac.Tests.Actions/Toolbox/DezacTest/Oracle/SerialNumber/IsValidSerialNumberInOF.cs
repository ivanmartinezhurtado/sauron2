﻿using Dezac.Services;
using Dezac.Tests.Services;
using System;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(IsValidSerialNumberInOFDescription))]
    public class IsValidSerialNumberInOF : ActionBase
    {
        public override void Execute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            if (data.modoPlayer == ModoTest.SAT || data.modoPlayer== ModoTest.PostVenta)
                return;

            if (data == null && data.TestInfo == null)
                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("Error este action tiene que tener contexto de test para ejecutarlo").Throw();

            using (DezacService svc = new DezacService())
            {
                var isValid = svc.IsSerialNumberValidInOF(data.TestInfo.NumSerie, data.NumOrden.Value);

                Assert.IsTrue(isValid, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el numero de serie no esta en el rango de numeros de serie de la Orden de Fabricación actual"));
            }
        }
    }

    public class IsValidSerialNumberInOFDescription : ActionDescription
    {
        public override string Name { get { return "IsValidSerialNumberInRange"; } }
        public override string Category { get { return "DezacTest/Oracle/SerialNumber"; } }
        public override string Description { get { return "Action para saber si el número de serie del producto es correcto"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
