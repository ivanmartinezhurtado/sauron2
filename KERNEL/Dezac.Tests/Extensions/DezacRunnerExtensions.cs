﻿using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Extensions
{
    public class DezacRunnerExtensions : IRunnerExtensions
    {
        public void SaveFileNumTestFase(int numTestFase, string name, string tipo, byte[] data)
        {
            DataUtils.SaveFileNumTestFase(numTestFase, name, tipo, data);
        }

        public void SaveFileVANumTestFase(int numTestFase, string name, string tipo, byte[] data)
        {
            DataUtils.SaveFileVANumTestFase(numTestFase, name, tipo, data);
        }

        public void SaveProcessFaseFile(int numRegister, byte[] data)
        {
            DataUtils.SaveProcessFaseFile(numRegister, data);
        }
    }
}
