﻿using Dezac.Core.Utility;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Extensions
{
    public static class TestMeasureCalibrationDeviceExtension
    {
        public static void TestMeasure(this TestBase testBase, IListTriOrAdjustValueDef defs, Func<double[]> funcVariables, ConfigurationSampler configuration, bool AddToResult)
        {
            if (configuration.SampleMaxNumber == 0 || configuration.SampleNumber == 0)
                testBase.Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("numMuestrasToAverage = 0").Throw();

            var list = new StatisticalList(defs.CrateListName()) { Name = "TestMeasure" };

            var stepOk = 0;

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = configuration.DelayBetweenSamples, InitialDelay = configuration.InitDelayTime, NumIterations = configuration.SampleMaxNumber, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var iteracion = step.Step + 1;
                   testBase.Logger.InfoFormat("Iteracion:{0} de {1}", iteracion, configuration.SampleMaxNumber);

                   var varsVariables = funcVariables();
                   defs.FillVars(varsVariables);
                   AddToLogger(testBase, defs);
                   if (!defs.HasIncorrect().Any())
                   {
                       testBase.Logger.InfoFormat("Muestra buena:{0} de {1} en la iteracion {2} de {3}", ++stepOk, configuration.SampleNumber, iteracion, configuration.SampleMaxNumber);
                       list.Add(varsVariables);
                       AddToLoggerList(testBase, list);

                       if (list.Count(0) >= configuration.SampleNumber)
                       {
                           testBase.Logger.Info("Muestra buenas suficientes -> Rellenamos variable con promediado y calculo de la Varianza");
                           defs.FillVars(list.Averages());   
                           defs.FillVarsVariance(list.Variances());
                           AddToLogger(testBase, defs);
                           if (!defs.HasIncorrectVariance().Any())
                               step.Cancel = true;
                           else
                           {
                               testBase.Logger.WarnFormat("Muestra varianza mala en la iteracion {0} de {1} - Borramos primera muestra cogida lista", iteracion, configuration.SampleMaxNumber);
                               stepOk--;
                               list.FirstClear();
                           }
                       }
                   }
                   else
                   {
                       testBase.Logger.WarnFormat("Muestra mala en la iteracion {0} de {1} - Borramos lista", iteracion, configuration.SampleMaxNumber);
                       defs.HasIncorrect().ForEach(p => { testBase.Logger.WarnFormat("Error: {0}", p.ToString()); });
                       stepOk = 0;

                       list.Clear();
                       if (configuration.SampleMaxNumber - iteracion < configuration.SampleNumber)
                       {
                           AddVariablesResult(testBase, defs, AddToResult, "TestMeasure");
                           testBase.Error().UUT.CALIBRACION.MARGENES(defs.HasIncorrect().FirstOrDefault().Name).Throw();
                       }
                   }
               });
            
            AddVariablesResult(testBase, defs, AddToResult, "TestMeasure");

            if (!sampler.Canceled)
            {
                if (defs.HasIncorrect().Any())
                {
                    var resultError = defs.HasIncorrect().FirstOrDefault().Name;
                    testBase.Error().UUT.CALIBRACION.MARGENES(resultError).Throw();
                }
                else
                    testBase.Error().UUT.CALIBRACION.MARGENES("NUMERO DE MUESTRAS INSUFICIENTES").Throw();
            }
        }

        public static void TestCalibrationWithVariance(this TestBase testBase, IListTriOrAdjustValueDef defs, Func<double[]> funcVariables, Func<double[]> funcPatron, ConfigurationSampler configuration)
        {
            if (configuration.SampleMaxNumber == 0 || configuration.SampleNumber == 0)
                testBase.Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("numMuestrasToAverage = 0").Throw();

            var list = new StatisticalList(defs.CrateListName()) { Name = "Calibration" };
            var listPatron = new StatisticalList(defs.CrateListName("PATRON")) { Name = "TestCalibrationWithVariance" };

            var stepOk = 0;

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = configuration.DelayBetweenSamples, InitialDelay = configuration.InitDelayTime, NumIterations = configuration.SampleMaxNumber, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var iteracion = step.Step + 1;
                   testBase.Logger.InfoFormat("Iteracion:{0} de {1}", iteracion, configuration.SampleMaxNumber);

                   var varsVariables = Task.Factory.StartNew<double[]>(funcVariables);
                   var varsPatron = Task.Factory.StartNew<double[]>(funcPatron);

                   Task.WaitAll(varsVariables, varsPatron);
                   defs.FillVars(varsVariables.Result, varsPatron.Result);
                   AddToLogger(testBase, defs);

                   try
                   {
                       if (!defs.HasIncorrect().Any())
                       {
                           testBase.Logger.InfoFormat("Muestra buena:{0} de {1} en la iteracion {2} de {3}", ++stepOk, configuration.SampleNumber, iteracion, configuration.SampleMaxNumber);
                           list.Add(varsVariables.Result);
                           listPatron.Add(varsPatron.Result);

                           if (list.Count(0) >= configuration.SampleNumber)
                           {
                               testBase.Logger.Info("Muestra buenas suficientes -> Rellenamos variable con promediado y calculo de la Varianza");
                               defs.FillVars(list.Averages(), listPatron.Averages());
                               defs.FillVarsVariance(list.Variances());
                               AddToLogger(testBase, defs);
                               if (!defs.HasIncorrectVariance().Any())
                                   step.Cancel = true;
                               else
                               {
                                   testBase.Logger.WarnFormat("Muestra varianza mala en la iteracion {0} de {1} - Borramos primera muestra cogida lista", iteracion, configuration.SampleMaxNumber);
                                   stepOk--;
                                   list.FirstClear();
                               }
                           }
                       }
                       else
                       {
                           testBase.Logger.WarnFormat("Muestra mala en la iteracion {0} de {1} - Borramos lista", iteracion, configuration.SampleMaxNumber);
                           defs.HasIncorrect().ForEach(p => { testBase.Logger.WarnFormat("Error: {0}", p.ToString()); });
                           stepOk = 0;

                           list.Clear();
                           if (configuration.SampleMaxNumber - iteracion <= configuration.SampleNumber)
                               testBase.Error().UUT.CALIBRACION.MARGENES(defs.HasIncorrect().FirstOrDefault().Name).Throw();
                       }
                   }
                   finally
                   {
                       AddVariablesResult(testBase, defs, true, "TestCalibration", true);
                   }
               });         

            if (!sampler.Canceled)
            {
                if (defs.HasIncorrect().Any())
                {
                    var resultError = defs.HasIncorrect().FirstOrDefault().Name;
                    testBase.Error().UUT.CALIBRACION.MARGENES(resultError).Throw();
                }
                else
                    testBase.Error().UUT.CALIBRACION.MARGENES("NUMERO DE MUESTRAS INSUFICIENTES").Throw();
            }
        }

        public static void TestCalibrationComparationWithVariance(this TestBase testBase, IListTriOrAdjustValueDef defsDevice, Func<double[]> funcDevice, Func<double[]> funcDevicePatron, IListTriOrAdjustValueDef defsInstruments, Func<double[]> funcInstruments, Func<double[]> funcInstrumentsPatron, ConfigurationSampler configuration)
        {
            if (configuration.SampleMaxNumber == 0 || configuration.SampleNumber == 0)
                testBase.Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("numMuestrasToAverage = 0").Throw();

            var listDevice = new StatisticalList(defsDevice.CrateListName()) { Name = "TestCalibrationComparationWithVariance" };
            var listDevicePatron = new StatisticalList(defsDevice.CrateListName("PATRON")) { Name = "TestCalibrationComparationWithVariance" };

            var listInstruments = new StatisticalList(defsInstruments.CrateListName()) { Name = "TestCalibrationComparationWithVariance" };
            var listInstrumentsPatron = new StatisticalList(defsInstruments.CrateListName("PATRON")) { Name = "TestCalibrationComparationWithVariance" };

            var stepOk = 0;

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = configuration.DelayBetweenSamples, NumIterations = configuration.SampleMaxNumber, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var iteracion = step.Step + 1;
                   testBase.Logger.InfoFormat("Iteracion:{0} de {1}", iteracion, configuration.SampleMaxNumber);

                   var varsVariablesDevice = Task.Factory.StartNew<double[]>(funcDevice);
                   var varsVariablesDevicePatron = Task.Factory.StartNew<double[]>(funcDevicePatron);
                   var varsVariablesInstruments = Task.Factory.StartNew<double[]>(funcInstruments);
                   var varsVariablesInstrumentsPatron = Task.Factory.StartNew<double[]>(funcInstrumentsPatron);

                   Task.WaitAll(varsVariablesDevice, varsVariablesDevicePatron, varsVariablesInstruments, varsVariablesInstrumentsPatron);

                   var tasks = new List<Task<bool>>();
                   tasks.Add(Task.Factory.StartNew<bool>(() =>
                   {
                       var res = ControlDefsCalibration(testBase, defsDevice, varsVariablesDevice.Result, varsVariablesDevicePatron.Result, listDevice, listDevicePatron, iteracion, configuration, stepOk);
                       stepOk = res.Item2;
                       return res.Item1;
                   }));

                   tasks.Add(Task.Factory.StartNew<bool>(() =>
                   {
                       var res = ControlDefsCalibration(testBase, defsInstruments, varsVariablesInstruments.Result, varsVariablesInstrumentsPatron.Result, listInstruments, listInstrumentsPatron, iteracion, configuration, stepOk);
                       stepOk = res.Item2;
                       return res.Item1;
                   }));

                   Task.WaitAll(tasks.ToArray());

                   step.Cancel = tasks[0].Result && tasks[1].Result;
               });

            if (!sampler.Canceled)
            {
                if (!defsDevice.HasIncorrect().Any())
                {
                    var resultError = defsDevice.HasIncorrect().FirstOrDefault().Name;
                    testBase.Error().UUT.CALIBRACION.MARGENES(resultError).Throw();
                }
                else
                    testBase.Error().UUT.CALIBRACION.MARGENES("NUMERO DE MUESTRAS INSUFICIENTES").Throw();
            }
        }

        //*****************************************************


        public static void AddVariablesResult(this TestBase testBase, IListTriOrAdjustValueDef result, bool AddToResult, string TestPoint, bool withVariance = false)
        {
            bool showRelativeError = false;
            
            foreach (var res in result.List)
            {
                testBase.SetVariable(res.Name, res.Value);
                testBase.AddResultValue("RESULTADO-> " + res.Name, res.Value);
                testBase.Logger.InfoFormat("RESULTADO -> {0} = {1}", res.Name, res.ToString());
                DataPointHub.TryAdd(testBase, res.Name, res.Value, "TestMeasure");

                if (AddToResult)
                {
                    if (!res.IsMaxMin)
                        showRelativeError = true;

                    testBase.Resultado.Set(res, showRelativeError, withVariance);
                }
            }
        }       

        private static void AddToLogger(TestBase testBase, IListTriOrAdjustValueDef defs)
        {
            foreach (var res in defs.List)
            {
                testBase.Logger.InfoFormat("{0}", res.ToString());
                testBase.AddResultValue(res.Name, res.Value);
                DataPointHub.TryAdd(testBase, res.Name, res.Value, "DEVICE");
            }
        }

        private static void AddToLoggerList(TestBase testBase, StatisticalList List)
        {
            foreach (var res in List.series)
            {
                testBase.Logger.InfoFormat("{0}", res.ToString());
                testBase.AddResultValue(res.Name, res.ToString());
            }
        }

        private static  Tuple<bool, int> ControlDefsCalibration(TestBase testBase, IListTriOrAdjustValueDef defsDevice, double[] varsVariablesDevice, double[] varsVariablesDevicePatron, StatisticalList listDevice, StatisticalList listDevicePatron, int iteracion, ConfigurationSampler configuration, int stepOk)
        {
            var result = false;
            defsDevice.FillVars(varsVariablesDevice, varsVariablesDevicePatron);
            if (!defsDevice.HasIncorrect().Any())
            {
                testBase.Logger.InfoFormat("Muestra buena:{0} de {1} en la iteracion {2} de {3}", ++stepOk, configuration.SampleNumber, iteracion, configuration.SampleMaxNumber);
                listDevice.Add(varsVariablesDevice);
                listDevicePatron.Add(varsVariablesDevicePatron);

                if (listDevice.Count(0) >= configuration.SampleNumber)
                {
                    testBase.Logger.Info("Muestra buenas suficientes -> Rellenamos variable con promediado y calculo de la Varianza");
                    defsDevice.FillVars(listDevice.Averages(), listDevicePatron.Averages());
                    defsDevice.FillVarsVariance(listDevice.Variances());

                    if (!defsDevice.HasIncorrectVariance().Any())
                        result = true;
                    else
                    {
                        testBase.Logger.WarnFormat("Muestra varianza mala en la iteracion {0} de {1} - Borramos primera muestra cogida lista", iteracion, configuration.SampleMaxNumber);
                        stepOk--;
                        listDevice.FirstClear();
                    }
                }
                AddToLogger(testBase, defsDevice);
            }
            else
            {
                testBase.Logger.WarnFormat("Muestra mala en la iteracion {0} de {1} - Borramos lista", iteracion, configuration.SampleMaxNumber);
                defsDevice.HasIncorrect().ForEach(p => { testBase.Logger.WarnFormat("Error: {0}", p.ToString()); });
                listDevice.Clear();
            }

            return Tuple.Create<bool, int>(result, stepOk);
        }

    }
}
