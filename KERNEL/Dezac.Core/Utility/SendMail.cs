﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;

namespace Dezac.Core.Utility
{
    public static class SendMail
    {
        public static void Send(string subject, string messageBody, int recipients, string log = null)
        {
            var destinatari = new List<string>();

            switch(recipients)
            {
                case 28:
                    destinatari.Add("albert@dezac.com");
                    break;
                case 379:
                    destinatari.Add("amartin@dezac.com");
                    break;
                case 394:
                    destinatari.Add("mcherta@dezac.com");
                    break;
                case 985:
                case 418:
                    destinatari.Add("rbutt@dezac.com");
                    break;
                default:
                    destinatari.Add("flsanchez@dezac.com");
                    break;

            }

            Send(subject, messageBody, destinatari, null, log);
        }

        public static void Send(string subject, string messageBody, string recipients, string log = null)
        {
            var destinatari = new List<string> { recipients };
            Send(subject, messageBody, destinatari, null, log);
        }

        public static void Send(string subject, string messageBody, bool onlyML = false, bool onlyProduccion = false, bool onlySoft= false, bool OT = false, string log = null)
        {
            var destinatariosSOFT = new List<string>()
            {
                "flsanchez@dezac.com",
                "imartinez@dezac.com",
                "cjordan@dezac.com",
            };

            var destinatariosML = new List<string>()
            {
                "albert@dezac.com",
                "amartin@dezac.com",
                "mcherta@dezac.com",
                "rbutt@dezac.com",
                "jborras@dezac.com"
            };

            var destinatariosProduccion = new List<string>()
            {
                "mgutierrez@dezac.com",
                "ccabezas@dezac.com",
            };

            var destinatariosOT = new List<string>()
            {
                "acastillo@dezac.com",
            };

            var destinatarios = new List<string>();

            if (onlySoft)
                destinatarios.AddRange(destinatariosSOFT);

            if (onlyML)
                destinatarios.AddRange(destinatariosML);

            if (onlyProduccion)
                destinatarios.AddRange(destinatariosProduccion);

            if (OT)
                destinatarios.AddRange(destinatariosOT);

            Send(subject, messageBody, destinatarios, null, log);
        }

        public static void Send(string subject, string messageBody, List<string> recipients, List<string> recipientsCopy, string log = null)
        {
            /*-------------------------MENSAJE DE CORREO----------------------*/
            //Creamos un nuevo Objeto de mensaje
            MailMessage mmsg = new MailMessage();

            //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario
            //Asunto
            mmsg.Subject = subject;
            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

            //Direccion de correo electronico a la que queremos enviar el mensaje
            foreach (string recipient in recipients)
                mmsg.To.Add(recipient);

            //Direccion de correo electronico que queremos que reciba una copia del mensaje
            if (recipientsCopy != null)
                foreach (string copy in recipientsCopy)
                    mmsg.Bcc.Add(copy);

            //Cuerpo del Mensaje
            mmsg.Body = messageBody;     
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = false; //Si no queremos que se envíe como HTML

            //Correo electronico desde la que enviamos el mensaje
            mmsg.From = new MailAddress("tsd@dezac.com");

            //Ficheros adjuntos
            if (!string.IsNullOrEmpty(log))
            {
                Attachment data = new Attachment(log, "text/html");

                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(log);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(log);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(log);

                mmsg.Attachments.Add(data);
            }
            /*-------------------------CLIENTE DE CORREO----------------------*/
            //Creamos un objeto de cliente de correo
            SmtpClient cliente = new SmtpClient();

            //Hay que crear las credenciales del correo emisor
            cliente.Credentials =
                new System.Net.NetworkCredential("tsd@dezac.com", "D4rkF4com");

            //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
            /*
            cliente.Port = 587;
            cliente.EnableSsl = true;
            */
            cliente.Host = "mail.dezac.com"; //Para Gmail "smtp.gmail.com";

            /*-------------------------ENVIO DE CORREO----------------------*/
            try
            {
                //Enviamos el mensaje      
                cliente.Send(mmsg);
            }
            catch (Exception)
            {
                //Aquí gestionamos los errores al intentar enviar el correo
            }
        }
    }
}
