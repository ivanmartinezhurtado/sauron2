﻿using Dezac.Data.ViewModels;
using Dezac.Tests.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(STMFlashLoaderActionDescription))]
    public class STMFlashLoaderAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Path del nombre completo del binario para encontrar el fichero (*.bin, *.hex *.s16)")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string BinaryFileName { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre a mapa de memoria para el programa del equipoName to Map memory Device program")]
        [DefaultValue(Target.STM32F0_3x_32K)]
        public Target DeviceTarget { get; set; }

        [Category("3. Configuration")]
        [Description("Marca para habilitar borrar todo al instante")]
        [DefaultValue(true)]
        public bool Erase { get; set; }

        [Category("3. Configuration")]
        [Description("Marca para habilitar la verificación del exito del programa")]
        [DefaultValue(true)]
        public bool Verify { get; set; }

        [Category("3.1 Comunication")]
        [Description("Puerto de serie para usar el programa del equìpo")]
        public int ComPort { get; set; }

        [DefaultValue(BaudRateEnum.Bps9600)]
        [Category("3.1 Comunication")]
        [Description("Velocidad a usar en el programa del equipo")]
        public BaudRateEnum BaudRate { get; set; }

        [DefaultValue(95)]
        [Category("3. Configuration")]
        [Description("Tiempo de espera para que el programa lance una excepción")]
        public int TimeOut { get; set; }

        public override void Execute()
        {
            //STMFlashLoader.exe -c --pn 13 --br 9600 --db 8 --pr EVEN --sb 1 --to 100 -i STM32F0_3x_32K  -e --all -d --fn C:\SAURON_SYSTEM\SEQUENCES\BRIDGE-LR\180830_1053_Bridge_LR_CR_v2E.hex −-v
            var ProgramPath = @"C:\Program Files (x86)\STMicroelectronics\Software\Flash Loader Demo\STMFlashLoader.exe ";

            if (!File.Exists(ProgramPath))
                Error().SOFTWARE.STM32_FLASH_LOADER.NO_INSTALADO("NO SE ENCUANTRA PATH DEL PROGRAMA").Throw();

           var pathBinary = GetVariable("BinaryPath");
            if (pathBinary == null)
            {
                if (string.IsNullOrEmpty(BinaryFileName))
                    Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("BinaryFileName").Throw();

                if (!File.Exists(BinaryFileName))
                    Error().SOFTWARE.STM32_FLASH_LOADER.VARIABLE_NO_ENCONTRADA(BinaryFileName).Throw();
            }
            else
                BinaryFileName = pathBinary.ToString();

            var Arguments = new StringBuilder();
            Arguments.Append("-c ");
            Arguments.AppendFormat("--pn {0} ", ComPort);
            Arguments.AppendFormat("--br {0} ", (int)BaudRate);// (e.g 115200, 57600..., default 57600)
            Arguments.AppendFormat("--db {0} ", 8);// (value in { 5,6,7,8}..., default 8)
            Arguments.AppendFormat("--pr {0} ", "EVEN"); // (value in { NONE,ODD,EVEN}..., default EVEN)
            Arguments.AppendFormat("--sb {0} ", 1); // (value in { 1,1.5,2}..., default 1)
            Arguments.AppendFormat("--ec {0} ", "OFF"); /// (value ON or OFF..., default is OFF)
            Arguments.AppendFormat("--to {0} ", 100); // ((ms)e.g 1000, 2000, 3000..., default 5000) 
            Arguments.AppendFormat("-i {0} ", TargetList[DeviceTarget]);
            if (Erase)
                Arguments.Append("-e --all ");
            Arguments.AppendFormat("-d --fn {0} ", BinaryFileName);
            if (Verify)
                Arguments.Append("--v ");

            Logger.InfoFormat("Arguments: {0}", Arguments.ToString());

            var ps = new ProcessStartInfo(ProgramPath, Arguments.ToString());
            ps.RedirectStandardOutput = false;
            ps.UseShellExecute = true;
            ps.CreateNoWindow = true;

            using (var proc = new Process())
            {
                proc.StartInfo = ps;
                proc.Start();

                var timeInit = new TimeSpan(0, 0, TimeOut);
                var timeNow = DateTime.Now;
                do
                {
                    var current = DateTime.Now;
                    if (current.Subtract(timeNow).TotalSeconds > timeInit.TotalSeconds)
                    {
                        proc.Kill();
                        Error().SOFTWARE.STM32_FLASH_LOADER.ERROR_GRABACION("TIME OUT").Throw();
                    }

                } while (!proc.HasExited);

               if(proc.ExitCode != 0)
                    Error().SOFTWARE.STM32_FLASH_LOADER.ERROR_GRABACION("EXIT_CODE").Throw();
            }
        }

        private readonly Dictionary<Target, string> TargetList = new Dictionary<Target, string>
            {
            { Target.STM8_16K, "STM8_16K" },
            { Target.STM8_32K, "STM8_32K" },
            { Target.STM8_64K, "STM8_64K" },
            { Target.STM8_128K, "STM8_128K" },
            { Target.STM8_256K, "STM8_256K" },
            { Target.STM8L_8K, "STM8L_8K" },
            { Target.STM8L_16K, "STM8L_16K" },
            { Target.STM8L_32K, "STM8L_32K" },
            { Target.STM8L_64K, "STM8L_64K" },
            { Target.STM32F0_3x_16K, "STM32F0_3x_16K" },
            { Target.STM32F0_3x_32K, "STM32F0_3x_32K" },
            { Target.STM32F0_4x_16K, "STM32F0_4x_16K" },
            { Target.STM32F0_4x_32K, "STM32F0_4x_32K" },
            { Target.STM32F0_5x_3x_16K, "STM32F0_5x_3x_16K" },
            { Target.STM32F0_5x_3x_32K , "STM32F0_5x_3x_32K" },
            { Target.STM32F0_5x_3x_64K, "STM32F0_5x_3x_64K" },
            { Target.STM32F0_7x_64K, "STM32F0_7x_64K" },
            { Target.STM32F0_7x_128K, "STM32F0_7x_128K" },
            { Target.STM32F0_9x_256K, "STM32F0_9x_256K" },
            { Target.STM32F1_Connectivity_line_64K, "STM32F1_Connectivity-line_64K" },
            { Target.STM32F1_Connectivity_line_128K, "STM32F1_Connectivity_line_128K" },
            { Target.STM32F1_Connectivity_line_256K, "STM32F1_Connectivity-line_256K" },
            { Target.STM32F1_High_density_256K, "STM32F1_High-density_256K" },
            { Target.STM32F1_High_density_384K, "STM32F1_High-density_384K" },
            { Target.STM32F1_High_density_512K, "STM32F1_High-density_512K" },
            { Target.STM32F1_High_density_value_256K, "STM32F1_High-density-value_256K" },
            { Target.STM32F1_High_density_value_384K, "STM32F1_High-density-value_384K" },
            { Target.STM32F1_High_density_value_512K, "STM32F1_High-density-value_512K" },
            { Target.STM32F1_Low_density_16K, "STM32F1_Low-density_16K" },
            { Target.STM32F1_Low_density_32K, "STM32F1_Low-density_32K" },
            { Target.STM32F1_Low_density_value_16K, "STM32F1_Low-density-value_16K" },
            { Target.STM32F1_Low_density_value_32K, "STM32F1_Low-density-value_32K" },
            { Target.STM32F1_Med_density_64K, "STM32F1_Med-density_64K" },
            { Target.STM32F1_Med_density_128K, "STM32F1_Med-density_128K" },
            { Target.STM32F1_Med_density_value_64K, "STM32F1_Med-density_128K" },
            { Target.STM32F1_Med_density_value_128K, "STM32F1_Med-density-value_128K" },
            { Target.STM32F1_XL_density_768K, "STM32F1_XL-density_768K" },
            { Target.STM32F1_XL_density_1024K, "STM32F1_XL-density_1024K" },
            { Target.STM32F2_128K, "STM32F2_128K" },
            { Target.STM32F2_256K, "STM32F2_256K" },
            { Target.STM32F2_512K, "STM32F2_512K" },
            { Target.STM32F2_768K, "STM32F2_768K" },
            { Target.STM32F2_1024K, "STM32F2_1024K" },
            { Target.STM32F3_02_01_64K, "STM32F3_02_01_64K" },
            { Target.STM32F3_03_02_256K, "STM32F3_03_02_256K" },
            { Target.STM32F3_03_02_512K, "STM32F3_03_02_512K" },
            { Target.STM32F3_7x_8x_256K, "STM32F3_7x_8x_256K" },
            { Target.STM32F3_34_03_64K, "STM32F3_34_03_64K" },
            { Target.STM32F4_01_256K, "STM32F4_01_256K" },
            { Target.STM32F4_01_512K, "STM32F4_01_512K" },
            { Target.STM32F4_05_07_15_17_1024K, "STM32F4_05_07_15_17_1024K" },
            { Target.STM32F4_10_128K, "STM32F4_10_128K" },
            { Target.STM32F4_11_512K, "STM32F4_11_512K" },
            { Target.STM32F4_12_1024K, "STM32F4_12_1024K" },
            { Target.STM32F4_27_37_29_39_2048K, "STM32F4_27_37_29_39_2048K" },
            { Target.STM32F4_46_512K, "STM32F4_46_512K" },
            { Target. STM32F4_69_79_2048K, "STM32F4_69_79_2048K" },
            { Target.STM32F7_4x_5x_1024K, "STM32F7_4x_5x_1024K" },
            { Target.STM32L0_x3_x2_x1_64K,  "STM32L0_x3_x2_x1_64K" },
            { Target.STM32L0_x3_x2_x1_192K, "STM32L0_x3_x2_x1_192K" },
            { Target.STM32L1_Cat1_32K, "STM32L1_Cat1_32K" },
            { Target.STM32L1_Cat1_64K, "STM32L1_Cat1_64K" },
            { Target.STM32L1_Cat1_128K, "STM32L1_Cat1_128K" },
            { Target.STM32L1_Cat2_32K, "STM32L1_Cat2_32K" },
            { Target.STM32L1_Cat2_64K, "STM32L1_Cat2_64K" },
            { Target.STM32L1_Cat2_128K, "STM32L1_Cat2_128K" },
            { Target.STM32L1_Cat3_256K, "STM32L1_Cat3_256K" },
            { Target.STM32L1_Cat4_256K, "STM32L1_Cat4_256K" },
            { Target.STM32L1_Cat4_384K, "STM32L1_Cat4_384K" },
            { Target.STM32L1_Cat5_512K, "STM32L1_Cat5_512K" },
            { Target.STM32L4x_6_1024K, "STM32L4x_6_1024K" },
            { Target.STR91xFA, "STR91xFA" },
            { Target.STR91xFAWy2, "STR91xFAWy2" },
            { Target.STR91xFAWy4, "STR91xFAWy4" },
            { Target.STR91xFAWy6, "STR91xFAWy6" },
            { Target.STR91xFAWy7, "STR91xFAWy7" },
            { Target.STR750F, "STR750F" }
            };

        public enum Target
        {
            STM8_16K,
            STM8_32K,
            STM8_64K,
            STM8_128K,
            STM8_256K,
            STM8L_8K,
            STM8L_16K,
            STM8L_32K,
            STM8L_64K,
            STM32F0_3x_16K,
            STM32F0_3x_32K,
            STM32F0_4x_16K,
            STM32F0_4x_32K,
            STM32F0_5x_3x_16K,
            STM32F0_5x_3x_32K,
            STM32F0_5x_3x_64K,
            STM32F0_7x_64K,
            STM32F0_7x_128K,
            STM32F0_9x_256K,
            STM32F1_Connectivity_line_64K,
            STM32F1_Connectivity_line_128K,
            STM32F1_Connectivity_line_256K,
            STM32F1_High_density_256K,
            STM32F1_High_density_384K,
            STM32F1_High_density_512K,
            STM32F1_High_density_value_256K,
            STM32F1_High_density_value_384K,
            STM32F1_High_density_value_512K,
            STM32F1_Low_density_16K,
            STM32F1_Low_density_32K,
            STM32F1_Low_density_value_16K,
            STM32F1_Low_density_value_32K,
            STM32F1_Med_density_64K,
            STM32F1_Med_density_128K,
            STM32F1_Med_density_value_64K,
            STM32F1_Med_density_value_128K,
            STM32F1_XL_density_768K,
            STM32F1_XL_density_1024K,
            STM32F2_128K,
            STM32F2_256K,
            STM32F2_512K,
            STM32F2_768K,
            STM32F2_1024K,
            STM32F3_02_01_64K,
            STM32F3_03_02_256K,
            STM32F3_03_02_512K,
            STM32F3_7x_8x_256K,
            STM32F3_34_03_64K,
            STM32F4_01_256K,
            STM32F4_01_512K,
            STM32F4_05_07_15_17_1024K,
            STM32F4_10_128K,
            STM32F4_11_512K,
            STM32F4_12_1024K,
            STM32F4_27_37_29_39_2048K,
            STM32F4_46_512K,
            STM32F4_69_79_2048K,
            STM32F7_4x_5x_1024K,
            STM32L0_x3_x2_x1_64K,
            STM32L0_x3_x2_x1_192K,
            STM32L1_Cat1_32K,
            STM32L1_Cat1_64K,
            STM32L1_Cat1_128K,
            STM32L1_Cat2_32K,
            STM32L1_Cat2_64K,
            STM32L1_Cat2_128K,
            STM32L1_Cat3_256K,
            STM32L1_Cat4_256K,
            STM32L1_Cat4_384K,
            STM32L1_Cat5_512K,
            STM32L4x_6_1024K,
            STR91xFA,
            STR91xFAWy2,
            STR91xFAWy4,
            STR91xFAWy6,
            STR91xFAWy7,
            STR750F,
        };

    }

    public class STMFlashLoaderActionDescription : ActionDescription
    {
        public override string Name { get { return "STMFlashLoader"; } }
        public override string Category { get { return "Tools/Firmware"; } }
        public override string Description  { get { return "Action para programar STM32 uC by UART";   }  }
        public override Image Icon { get { return Properties.Resources.stmicroelectronics; } }
    }



}
