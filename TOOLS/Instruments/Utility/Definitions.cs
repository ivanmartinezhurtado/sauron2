﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.Utility
{
    public enum Fases
    {
        L1 = 1,
        L2 = 2,
        L3 = 3,
        Neutro = 4
    }

    public enum OutputCurrentModes
    {
        CURRENT,
        VOLTAGE_FROM_CURRENT
    }

    public enum AC_DC
    {
        AC,
        DC,
    }
}
