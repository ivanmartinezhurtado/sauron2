﻿using System;

namespace Dezac.Core.Model
{
    public class ProductsViewModel
    {
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public Nullable<System.DateTime> FECHAVERSION { get; set; }
        public string ACTIVO { get; set; }

        public int NUMFAMILIA { get; set; }
        public string FAMILIA { get; set; }
        public string DESCRIPCION { get; set; }
    }
}
