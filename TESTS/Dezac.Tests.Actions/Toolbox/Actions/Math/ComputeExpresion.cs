﻿using Dezac.Tests.Actions.Kernel;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.06)]
    [DesignerAction(Type = typeof(ComputeExpresionDescription))]
    public class ComputeExpresion : ActionBase
    {
        [Description("Ecuación que se quiere calcular")]
        [Category("3. Configuration")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Equation { get; set; } //TODO: No se puede cambiar a ActionVariable porque es una expresión, se deja en string

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if (DataOutput.IsKeyNullOrEmpty)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataOutput").Throw();

            DataOutput.Value = VariablesTools.ResolveEquation(Equation);
        }
    }

    public class ComputeExpresionDescription : ActionDescription
    {
        public override string Name { get { return "ComputeExpresion"; } }
        public override string Description { get { return "Calcula el resultado de una equación. Para utilizar variables usar '$'. Ej: Abs($Error/2). Admite expresiones como Abs, Sin, Cos, Tan, Sqrt, Pow, PI, etc."; } } //ToDo Miki: La descripción no se acaba de entender. 
        public override string Category { get { return "Actions/Math"; } }
        public override Image Icon { get { return Properties.Resources.Math; } }
    }
}
