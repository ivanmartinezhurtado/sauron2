namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ORDENPROCESOFASE")]
    public partial class ORDENPROCESOFASE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMREGISTRO { get; set; }

        public DateTime FECHAALTA { get; set; }

        public int NUMORDEN { get; set; }

        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDFASE { get; set; }

        public int SECUENCIA { get; set; }

        [Required]
        [StringLength(3)]
        public string CODIGOGRUPO { get; set; }

        public DateTime FECHA { get; set; }

        public int TIEMPO { get; set; }

        [Required]
        [StringLength(1)]
        public string RESULTADO { get; set; }

        [StringLength(200)]
        public string NOTAS { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual ORDEN ORDEN { get; set; }
    }
}
