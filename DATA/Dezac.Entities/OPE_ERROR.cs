namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class OPE_ERROR
    {
        public OPE_ERROR()
        {
            this.ORDENPRODUCTOERROR = new HashSet<ORDENPRODUCTOERROR>();
            this.T_TESTERROR = new HashSet<T_TESTERROR>();
        }
    
        public int NUMERROR { get; set; }
        public string DESCRIPCION { get; set; }
        public int NUMFAMILIA { get; set; }
    
        public virtual T_FAMILIAERROR T_FAMILIAERROR { get; set; }
        public virtual ICollection<ORDENPRODUCTOERROR> ORDENPRODUCTOERROR { get; set; }
        public virtual ICollection<T_TESTERROR> T_TESTERROR { get; set; }
    }
}
