﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.01)]
    public class NRG96ShurtelTest : TestBase
    {
        NRG96 nrg;
        MTEPPS mte;
        CVMMINI cvmminiConsumo;
        public double transformationRelation;
        
        public void TestInitialization(byte nrgSerialPort = 0, byte mteSerialPort = 0, byte cvmSerialPort = 0)
        {
            nrg = new NRG96(nrgSerialPort == 0 ? Comunicaciones.SerialPort : nrgSerialPort);
            nrg.Modbus.BaudRate = Comunicaciones.BaudRate;
            nrg.Modbus.PerifericNumber = Comunicaciones.Periferico;

            mte = new MTEPPS();
            mte.PortCom = mteSerialPort == 0 ? Comunicaciones.SP0 : mteSerialPort;

            cvmminiConsumo = new CVMMINI(cvmSerialPort == 0 ? Comunicaciones.SP1 : cvmSerialPort);
            cvmminiConsumo.Modbus.PerifericNumber = Comunicaciones.PerifericoModulo;
        }

        public void TestConsumo()
        {
            Shell.ShowDialog("Encendido del equipo", new Label() { Text = "Por favor, inserte el equipo en el utillaje, cierre las bridas de presión y por último encienda el equipo usando el interruptor trasero del utillaje", Width = 400, Height = 80, Padding = new Padding(10) }, MessageBoxButtons.OK);

            cvmminiConsumo.WriteOutputState(CVMMINI.Outputs.OUTPUT_1, CVMMINI.State.ON);

            Delay(5500, "Esperando encendido del equipo e instrumentos");

            TestConsumo(Params.KW.Null.EnVacio.Vmin.Name, Params.KW.Null.EnVacio.Vmin.Min(), Params.KW.Null.EnVacio.Vmin.Max(),
                () => cvmminiConsumo.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vmin fuera de margenes");

            TestConsumo(Params.KVAR.Null.EnVacio.Vmin.Name, Params.KVAR.Null.EnVacio.Vmin.Min(), Params.KVAR.Null.EnVacio.Vmin.Max(),
                () => cvmminiConsumo.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vmin fuera de margenes");
        }

        public void TestCommunications()
        {
            nrg.WriteFlagTest();

            var softwareVersion = nrg.ReadSoftwareVersion();

            Assert.AreEqual("VERSION_FIRMWARE", softwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error de version de firmware, la versión grabada en el equipo es incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestLeds()
        {
            nrg.WriteLedState(true);

            var ledDialogResult = Shell.ShowDialog("PRUEBA DE LEDS", new ImageView("Comprobar que se enciende el led que aparece en la imagen", PathImageSave + this.GetType().Name + "\\LED.png"), System.Windows.Forms.MessageBoxButtons.YesNo);

            Assert.AreEqual(ledDialogResult, DialogResult.Yes, Error().PROCESO.OPERARIO.LED("Error al comprobar el led, el operario indica que no se enciende"));
        }

        public void TestROMCode()
        {
            var romCode = nrg.ReadROMCode();

            Assert.AreEqual("ESTADO_EEPROM", Identificacion.ROM_CODE, romCode, Error().UUT.EEPROM.NO_COINCIDE("Error en la memoria ROM del equipo, el codigo ROM no coincide con el de la BDD"), ParamUnidad.SinUnidad);
        }

        public void TestSetupDefault()
        {
            nrg.WriteFlagTest();

            nrg.WriteDefaultCommunicationConfiguration();

            transformationRelation = Configuracion.GetDouble("RELACION_TRANSFORMACION", 1, ParamUnidad.SinUnidad);

            NRG96.SetupConfiguration setupConfiguration = new NRG96.SetupConfiguration((int)Configuracion.GetDouble("TENSION_PRIMARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES", "SIMPLE", ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO", 0, ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            nrg.WriteSetupConfiguration(setupConfiguration);

            NRG96.MaximumDemandConfiguration maximumDemandConfiguration = new NRG96.MaximumDemandConfiguration()
            {
                VariabletoCalculate = Configuracion.GetString("VARIABLE_MAXIMA_DEMANDA", "NO_PD", ParamUnidad.SinUnidad),
                RegisterTime = (ushort)Configuracion.GetDouble("TIEMPO_REGISTRO_MAXIMA_DEMANDA", 15, ParamUnidad.SinUnidad)
            };

            nrg.WriteMaximumDemandConfiguration(maximumDemandConfiguration);

            //NRG96.TriCalibrationFactors compoundFactors = new NRG96.TriCalibrationFactors((ushort)Configuracion.GetDouble("FACTOR_TENSIONES_COMPUESTAS", 8790, ParamUnidad.SinUnidad));

            //nrg.WriteCompoundvoltageFactors(compoundFactors);

            NRG96.HardwareVector hardwareVector = new NRG96.HardwareVector()
                {
                    CalculoHarmonicos = Configuracion.GetString("CALCULO_HARMONICOS_VECTOR_HARDWARE", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                    Comunication = Configuracion.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad) == "SI",
                    Rs232 = Configuracion.GetString("RS232/RS485", "RS485", ParamUnidad.SinUnidad) == "RS232",
                    InputCurrentString = Configuracion.GetString("CORRIENTE_ENTRADA", "5", ParamUnidad.SinUnidad),
                    InputVoltageString = Configuracion.GetString("TENSION_ENTRADA", "300", ParamUnidad.SinUnidad),
                    ModeloString = Configuracion.GetString("MODELO", "CVM_NRG_96", ParamUnidad.SinUnidad),
                    PowerSupplyString = Configuracion.GetString("TENSION_ALIMENTACION", "230Vac", ParamUnidad.SinUnidad),
                    Rele = Configuracion.GetString("RELE", "SI", ParamUnidad.SinUnidad) == "SI",
                    Shunt = Configuracion.GetString("SHUNT", "NO", ParamUnidad.SinUnidad) == "SI",
                    PaginasDePotencia = Configuracion.GetString("PAGINA_POTENCIA", "NO", ParamUnidad.SinUnidad) == "SI"
                };

            nrg.WriteHardwareVector(hardwareVector);

            nrg.WriteCalibrationFactors(new NRG96.CalibrationFactors((ushort)Configuracion.GetDouble(Params.GAIN_I.Null.TestPoint("DEFECTO").Name, 23815, ParamUnidad.SinUnidad), (ushort)Configuracion.GetDouble(Params.GAIN_V.Null.TestPoint("DEFECTO").Name, 11400, ParamUnidad.SinUnidad)));

            nrg.WriteITFCalibrationFactors(new NRG96.TriCalibrationFactors((ushort)Configuracion.GetDouble(Params.GAIN_DESFASE.Null.TestPoint("DEFECTO").Name, 0, ParamUnidad.SinUnidad)));

            NRG96.PasswordConfiguration passwordConfiguration = new NRG96.PasswordConfiguration()
                {
                    Password = (ushort)Configuracion.GetDouble("CONTRASEÑA", 1234, ParamUnidad.SinUnidad),
                    SetupLocking = (Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI") ? (byte)1 : (byte)0
                };

            nrg.WritePasswordInformation(passwordConfiguration);

            nrg.WriteInitialScreenMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));

            nrg.WriteSerialNumber((uint)Configuracion.GetDouble("NUMERO_SERIE_TEST", 12345678, ParamUnidad.SinUnidad));

            NRG96.ScreenConfiguration screenConfiguration = new NRG96.ScreenConfiguration()
            {
                ActivePower = Configuracion.GetString("PANTALLA_POTENCIA_ACTIVA", "SI", ParamUnidad.SinUnidad) == "SI",
                Current = Configuracion.GetString("PANTALLA_CORRIENTE", "SI", ParamUnidad.SinUnidad) == "SI",
                CurrentTHD = Configuracion.GetString("PANTALLA_THD_CORRIENTE", "SI", ParamUnidad.SinUnidad) == "SI",
                PdHzCos = Configuracion.GetString("PANTALLA_PD_HZ_COS", "SI", ParamUnidad.SinUnidad) == "SI",
                PdHzPF = Configuracion.GetString("PANTALLA_PD_HZ_PF", "SI", ParamUnidad.SinUnidad) == "SI",
                PhasesPd = Configuracion.GetString("PANTALLA_PD_FASES", "SI", ParamUnidad.SinUnidad) == "SI",
                PowerFactor = Configuracion.GetString("PANTALLA_FACTOR_POTENCIA", "SI", ParamUnidad.SinUnidad) == "SI",
                ReactivePower = Configuracion.GetString("PANTALLA_POTENCIA_REACTIVA", "SI", ParamUnidad.SinUnidad) == "SI",
                ThreePhaseActiveInductiveCapacitivePower = Configuracion.GetString("PANTALLA_ACTIVA_INDUCTIVA_CAPACITIVA", "SI", ParamUnidad.SinUnidad) == "SI",
                ThreePhaseActiveReactiveApparentPower = Configuracion.GetString("PANTALLA_ACTIVA_REACTIVA_APARENTE", "SI", ParamUnidad.SinUnidad) == "SI",
                VoltagePhaseNeuter = Configuracion.GetString("PANTALLA_TENSION", "SI", ParamUnidad.SinUnidad) == "SI",
                VoltagePhasePhase = Configuracion.GetString("PANTALLA_TENSION_ENTRE_FASES", "SI", ParamUnidad.SinUnidad) == "SI",
                VoltageTHD = Configuracion.GetString("PANTALLA_THD_TENSION", "SI", ParamUnidad.SinUnidad) == "SI",
            };

            //nrg.WriteScreenConfiguration(screenConfiguration);

            // comunicaciones bacnet

            nrg.WriteAllInformationClear();

            NRG96.AlarmConfiguration alarmConfiguration = new NRG96.AlarmConfiguration()
            {
                Delay = (ushort)Configuracion.GetDouble("RETRASO_ALARMA", 0, ParamUnidad.SinUnidad),
                MaximumValue = (int)Configuracion.GetDouble("VALOR_MAXIMO_ALARMA", 0, ParamUnidad.SinUnidad),
                MinimumValue = (int)Configuracion.GetDouble("VALOR_MINIMO_ALARMA", 0, ParamUnidad.SinUnidad),
                VariableNumber = (byte)Configuracion.GetDouble("VARIABLE_ALARMA", 0, ParamUnidad.SinUnidad),
            };

            nrg.WriteAlarmConfiguration(alarmConfiguration);
            NRG96.EnergyConsumptionInformation energyInformation = new NRG96.EnergyConsumptionInformation
            {
                Active = 0x01010101,
                Inductive = 0x12121212,
                Capacitive = 0x23232323
            };

            nrg.WriteEnergyInformation(energyInformation);

            Delay(2000, "Asegurando grabado de energias");

            cvmminiConsumo.WriteOutputState(CVMMINI.Outputs.OUTPUT_1, CVMMINI.State.OFF);

            Delay(3000, "Asegurando apagado del equipo");
            
            cvmminiConsumo.WriteOutputState(CVMMINI.Outputs.OUTPUT_1, CVMMINI.State.ON);
            
            Delay(6000, "Asegurando encendido del equipo");

            nrg.WriteFlagTest();

            var eepromErrors = nrg.ReadEEPROMErrorCodes();

            Assert.AreEqual(eepromErrors, NRG96.EEPROMErrorCodes.NO_ERROR, Error().UUT.EEPROM.NO_COINCIDE(string.Format("Error en la eeprom del equipo, tras el reinicio se notifica del siguiente error", eepromErrors.GetDescription())));

            var energyReadings = nrg.ReadEnergyInformation();

            Assert.AreEqual(energyInformation.Active, energyReadings.Active, Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error en la grabación de energias, la energia activa recuperada no coincide con la grabada"));
            Assert.AreEqual(energyInformation.Capacitive, energyReadings.Capacitive, Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error en la grabación de energias, la energia capacitiva recuperada no coincide con la grabada"));
            Assert.AreEqual(energyInformation.Inductive, energyReadings.Inductive, Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error en la grabación de energias, la energia inductiva recuperada no coincide con la grabada"));

            nrg.WriteAllInformationClear();
        }

        public void TestDisplay()
        {
            nrg.WriteFlagTest();

            nrg.SendDisplayInfo(NRG96.DisplayState.NO_SEGMENTS);

            nrg.WriteBacklightsState(false, false);

            var dialogResult = Shell.ShowDialog("TEST DE DISPLAY", () => { return new ImageView("Se muestra el display como en la imagen patrón?", Properties.Resources.IMAGEN_PATRON) { Width = 600, Height = 450 }; }, MessageBoxButtons.YesNo, waitTime: 500);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.HARDWARE.DISPLAY("Error de display al comprobar el apagado del backlight"));

            nrg.WriteBacklightsState(true, true);

            nrg.SendDisplayInfo(NRG96.DisplayState.HORIZONTAL_SEGMENTS);

            dialogResult = Shell.ShowDialog("TEST DE DISPLAY", () => { return new ImageView("Se muestra el display como en la imagen patrón?", Properties.Resources.HORIZONTAL_SEGMENTS) { Width = 600, Height = 450 }; }, MessageBoxButtons.YesNo, waitTime: 500);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.HARDWARE.DISPLAY("Error de display al comprobar el backlight o los segmentos horizontales"));

            Resultado.Set("BACKLIGHT", "OK", ParamUnidad.SinUnidad);

            nrg.SendDisplayInfo(NRG96.DisplayState.VERTICAL_SEGMENTS);

            dialogResult = Shell.ShowDialog("TEST DE DISPLAY", () => { return new ImageView("Se muestra el display como en la imagen patrón?", Properties.Resources.VERTICAL_SEGMENTS) { Width = 600, Height = 450 }; }, MessageBoxButtons.YesNo, waitTime: 500);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.HARDWARE.DISPLAY("Error de display al comprobar los segmentos verticales"));

            nrg.SendDisplayInfo(NRG96.DisplayState.SYMBOL_SEGMENTS);

            dialogResult = Shell.ShowDialog("TEST DE DISPLAY", () => { return new ImageView("Se muestra el display como en la imagen patrón?", Properties.Resources.SYMBOL_SEGMENTS) { Width = 600, Height = 450 }; }, MessageBoxButtons.YesNo, waitTime: 500);

            Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.HARDWARE.DISPLAY("Error de display al comprobar los segmentos de los símbolos"));

            Resultado.Set("DISPLAY", "OK", ParamUnidad.SinUnidad);

            nrg.SendDisplayInfo(NRG96.DisplayState.NO_SEGMENTS);
        }

        public void TestKeyboard()
        {
            nrg.WriteFlagTest();

            Dictionary<NRG96.KeyboardKeysRegisters, Bitmap> imageDictionary = new Dictionary<NRG96.KeyboardKeysRegisters, Bitmap>()
            {
                {NRG96.KeyboardKeysRegisters.RESET_KEY, Properties.Resources.RESET_KEY},
                {NRG96.KeyboardKeysRegisters.MIN_KEY, Properties.Resources.MIN_KEY},
                {NRG96.KeyboardKeysRegisters.MAX_KEY, Properties.Resources.MAX_KEY},
                {NRG96.KeyboardKeysRegisters.DISPLAY_KEY, Properties.Resources.DISPLAY_KEY},
            };
            
            foreach (NRG96.KeyboardKeysRegisters item in Enum.GetValues(typeof(NRG96.KeyboardKeysRegisters)))
            {
                Shell.ShowDialog("TEST DE TECLADO", new ImageView("Mantenga pulsada la tecla destacada en la imagen y pulse aceptar", imageDictionary[item]) { Width = 600, Height = 450 }, MessageBoxButtons.OK);

                var keyReading = nrg.ReadKey();

                var keyboardReading = keyReading == item;

                Resultado.Set(item.ToString(), keyboardReading ? "OK" : "KO", ParamUnidad.SinUnidad);
                
                Assert.IsTrue(keyboardReading, Error().UUT.HARDWARE.SETUP(string.Format("Error de teclado al comprobar la tecla {0}", item.GetDescription())));
            }
        }

        public void TestRele()
        {
            nrg.WriteFlagTest();

            bool estadoSalida = true;

            nrg.WriteDigitalOutput(true);

            var digitalOutput = Shell.ShowDialog("TEST DE SALIDA DIGITAL", () => 
            {
                return new ImageView("Se ha encendido la luz destacada?", Properties.Resources.DIGITAL_OUTPUT_ON) { Width = 600, Height = 450 };
            }, MessageBoxButtons.YesNo, waitTime: 500);

            estadoSalida |= digitalOutput == DialogResult.Yes;

            if (!estadoSalida)
                Resultado.Set("OUTPUT", "KO", ParamUnidad.SinUnidad);

            Assert.AreEqual(digitalOutput, DialogResult.Yes, Error().PROCESO.OPERARIO.PARAMETRO_INCOHERENTE("Error en la salida digital, el operario notifica que la salida digital no se enciende"));
            nrg.WriteDigitalOutput(false);

            digitalOutput = Shell.ShowDialog("TEST DE SALIDA DIGITAL", () => 
            { 
                return new ImageView("Se ha apagado la luz destacada?", Properties.Resources.DIGITAL_OUTPUT_OFF) { Width = 600, Height = 450 }; 
            }, MessageBoxButtons.YesNo, waitTime: 500);

            estadoSalida |= digitalOutput == DialogResult.Yes;

            Resultado.Set("OUTPUT", estadoSalida ? "OK" : "KO", ParamUnidad.SinUnidad);

            nrg.WriteBacklightsState(false, false);

            Assert.AreEqual(digitalOutput, DialogResult.Yes, Error().PROCESO.OPERARIO.PARAMETRO_INCOHERENTE("Error en la salida digital, el operario notifica que la salida digital no se apaga"));
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            //if (cvm.ReadHardwareVector().In_Medida)
            //    tower.IO.DO.On(OUTPUT_NEUTRO);

            var offsetsV = new TriAdjustValueDef(Params.V.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.V.Null.CrucePistas.Min(), Params.V.Null.CrucePistas.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.I.Null.CrucePistas.Min(), Params.I.Null.CrucePistas.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            mte.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = nrg.ReadPhaseVariables();
                    return new double[] { vars.L1.Tension / 10D, vars.L2.Tension / 10D, vars.L3.Tension / 10D, 
                        vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D};

                }, delFirts, samples, timeInterval);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            mte.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            //if (cvm.ReadHardwareVector().In_Medida)
            //    crucePistasI.Neutro = new AdjustValueDef(Params.I.LN.CrucePistas.Name, 0, 0, 0, ConsignasI.L1, Params.I.Null.CrucePistas.Tol, ParamUnidad.A);

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = nrg.ReadPhaseVariables();

                    return new double[] { vars.L1.Tension / 10D, vars.L2.Tension/ 10D, vars.L3.Tension/ 10D, 
                        vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D};
                }, delFirts, samples, timeInterval);

            mte.ApplyOffAndWaitStabilisation();
        }

        public void TestAdjustVoltageCurrent(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var measureGains = nrg.CalculateVoltageCurrentCalibrationFactors(delFirts, initCount, samples, timeInterval, realVoltageCurrent.Voltage, realVoltageCurrent.Current, transformationRelation,
                (value) =>
                {
                    triGains[0].L1.Value = value.VoltageL1;
                    triGains[0].L2.Value = value.VoltageL2;
                    triGains[0].L3.Value = value.VoltageL3;

                    triGains[1].L1.Value = value.CurrentL1;
                    triGains[1].L2.Value = value.CurrentL2;
                    triGains[1].L3.Value = value.CurrentL3;

                    return !HasError(triGains, false).Any();
                });

            triGains.ForEach(x => x.AddToResults(Resultado));

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            nrg.WriteFlagTest();
            nrg.WriteCalibrationFactors(measureGains.Item2);

        }

        public void TestVerificationVoltageCurrent(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            nrg.WriteFlagTest();
            nrg.WriteRecoverCalibrationValuesFromEEPROM();

            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A));

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = nrg.ReadPhaseVariables();
                    var varsList = new List<double>() 
                            { 
                                vars.L1.Tension / 10D, vars.L2.Tension/ 10D, vars.L3.Tension/ 10D, 
                                vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D
                            };
                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = mte.ReadVoltage();
                    var currentRef = mte.ReadCurrent();

                    var varsList = new List<double>() 
                            { 
                                voltageRef.L1, voltageRef.L2, voltageRef.L3, 
                                currentRef.L1 * transformationRelation, currentRef.L2 * transformationRelation, currentRef.L3 *transformationRelation
                            };
                    return varsList.ToArray();
                }, countSamples, maxSamples, timeInterval);
        }

        public void TestAdjustCompoundVoltages(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var triGains = new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var adjustVoltageValues = TriLineValue.Create(realVoltageCurrent.Voltage.L1, realVoltageCurrent.Voltage.L2, realVoltageCurrent.Voltage.L3);

            var measureGains = nrg.CalculateCompoundMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltageValues, transformationRelation,
                (value) =>
                {
                    triGains.L1.Value = value.L1;
                    triGains.L2.Value = value.L2;
                    triGains.L3.Value = value.L3;

                    return !triGains.HasMinMaxError();
                });

            triGains.AddToResults(Resultado);

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de tensiones compuestas fuera de margenes"));

            nrg.WriteCompoundvoltageFactors(measureGains.Item2);
        }

        public void TestPhaseGapAdjust(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationPhaseGap();

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var power = TriLinePower.Create(realVoltageCurrent.Voltage, realVoltageCurrent.Current * transformationRelation, realVoltageCurrent.powerFactor);

            var offsetGains = nrg.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, power.KW, realVoltageCurrent.powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                });

            triDefs.AddToResults(Resultado);

            if (!offsetGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de desfase fuera de margenes"));

            nrg.WriteFlagTest();
            nrg.WriteITFCalibrationFactors(offsetGains.Item2);
        }
        
        public void TestVerificationPhaseGap(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            nrg.WriteFlagTest();
            nrg.WriteRecoverCalibrationValuesFromEEPROM();

            var realVoltageCurrent = internalTestAdjustVerificationPhaseGap();

            var toleranciaKw = Params.KW.Null.Verificacion.Tol();
            var defs = new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = nrg.ReadPhaseVariables();
                    var varsList = new List<double>() 
                            { 
                               vars.L1.PotenciaActiva, vars.L2.PotenciaActiva, vars.L3.PotenciaActiva,
                            };
                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = mte.ReadVoltage();
                    var currentRef = mte.ReadCurrent();

                    var power = TriLinePower.Create(voltageRef, currentRef * transformationRelation, realVoltageCurrent.powerFactor);
                    return power.KW.ToArray();

                }, countSamples, maxSamples, timeInterval, "Error en la verificación del desfase fuera de margenes");
        }

        public void TestVerification()
        {
            //Comprobar tensiones de fase, compuestas, corrientes, medida de frecuencia, THD del equipo

            List<TriAdjustValueDef> thdMargins = new List<TriAdjustValueDef>();

            thdMargins.Add(new TriAdjustValueDef(Params.THD_V.L1.Verificacion.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            thdMargins.Add(new TriAdjustValueDef(Params.THD_I.L1.Verificacion.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));

            TestMeasureBase(thdMargins, (step) =>
                {
                    var thdReadings = nrg.ReadTHD();

                    return new double[] { thdReadings.VoltageL1, thdReadings.VoltageL2, thdReadings.VoltageL3, 
                                thdReadings.CurrentL1, thdReadings.CurrentL2, thdReadings.CurrentL3};
                }, 0, 10, 500);

            thdMargins.ForEach(x => x.AddToResults(Resultado));

            if (thdMargins.Any(x => x.HasMinMaxError()))
                throw new Exception("Error en la verificación del equipo, valores de THD fuera de márgenes");

            mte.ApplyOffAndWaitStabilisation();

            Delay(3000, "Aseguramos apagado fuente MTE para evitar lecturas de energias en el equipo");

            nrg.WriteAllInformationClear();
        }

        public void TestCustomize()
        {
            nrg.WriteFlagTest();

            NRG96.SetupConfiguration setupConfiguration = new NRG96.SetupConfiguration
                ((int)Configuracion.GetDouble("TENSION_PRIMARIO_CLIENTE", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO_CLIENTE", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO_CLIENTE", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES_CLIENTE", "SIMPLE", ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO_CLIENTE", 0, ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO_CLIENTE", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO_CLIENTE", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            nrg.WriteSetupConfiguration(setupConfiguration);

            TestInfo.NumSerie = this.GetVariable<string>(ConstantsParameters.TestInfo.NUM_SERIE);

            if (!string.IsNullOrEmpty(TestInfo.NumSerie))
                nrg.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));
        }

        public void TestFinish()
        {
            if (mte != null)
                try
                {
                    mte.ApplyOffAndWaitStabilisation();

                }
                catch (Exception)
                {
                }
                finally
                {
                    mte.Dispose();
                }

            if (cvmminiConsumo != null)
            {
                cvmminiConsumo.WriteOutputState(CVMMINI.Outputs.OUTPUT_1, CVMMINI.State.OFF);
                cvmminiConsumo.Dispose();
            }
            if (nrg != null)
                nrg.Dispose();
        }

        private struct VoltageCurrentReading
        {
            public TriLineValue Voltage { get; set; }
            public TriLineValue Current { get; set; }
            public double powerFactor { get; set; }
        };

        private VoltageCurrentReading internalTestAdjustVerificationVoltageCurrent()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L2 = Consignas.GetDouble(Params.I.L2.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L3 = Consignas.GetDouble(Params.I.L3.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA)
            };
            var consigAdjustCurrents = new TriLineValue()
            {
                L1 = adjustCurrent + adjustCurrentOffsets.L1,
                L2 = adjustCurrent + adjustCurrentOffsets.L2,
                L3 = adjustCurrent + adjustCurrentOffsets.L3
            };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            mte.ApplyPresetsAndWaitStabilisation(adjustVoltage, consigAdjustCurrents, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var currentReadings = mte.ReadCurrent();
            var voltageReadings = mte.ReadVoltage();

            var realAdjustCurrents = new TriLineValue()
            {
                L1 = currentReadings.L1 - adjustCurrentOffsets.L1,
                L2 = currentReadings.L2 - adjustCurrentOffsets.L2,
                L3 = currentReadings.L3 - adjustCurrentOffsets.L3
            };

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(realAdjustCurrents.L1, realAdjustCurrents.L2, realAdjustCurrents.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        private VoltageCurrentReading internalTestAdjustVerificationPhaseGap()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L2 = Consignas.GetDouble(Params.I.L2.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L3 = Consignas.GetDouble(Params.I.L3.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA)
            };
            var consigAdjustCurrents = new TriLineValue()
            {
                L1 = adjustCurrent + adjustCurrentOffsets.L1,
                L2 = adjustCurrent + adjustCurrentOffsets.L2,
                L3 = adjustCurrent + adjustCurrentOffsets.L3
            };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 45, ParamUnidad.Grados);

            mte.ApplyPresetsAndWaitStabilisation(adjustVoltage, consigAdjustCurrents, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var currentReadings = mte.ReadCurrent();
            var voltageReadings = mte.ReadVoltage();

            var realAdjustCurrents = new TriLineValue()
            {
                L1 = currentReadings.L1 - adjustCurrentOffsets.L1,
                L2 = currentReadings.L2 - adjustCurrentOffsets.L2,
                L3 = currentReadings.L3 - adjustCurrentOffsets.L3
            };

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(realAdjustCurrents.L1, realAdjustCurrents.L2, realAdjustCurrents.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }
    }
}

