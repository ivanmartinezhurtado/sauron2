﻿using Dezac.Core.Enumerate;
using System;
using System.Collections.Generic;

namespace Dezac.Data.ViewModels
{
    public class TestView
    {
        public int NUMTEST { get; set; }
        public int NUMTESTFASE { get; set; }
        //public Nullable<int> NUMFABRICACION { get; set; }
        public int NUMORDEN { get; set; }
        public System.DateTime FECHA { get; set; }
        public Nullable<int> TIEMPO { get; set; }
        public string RESULTADO { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
        public Nullable<int> NUMFAMILIA { get; set; }
        public Nullable<int> NUMPRODUCTO { get; set; }
        public Nullable<int> VERSION { get; set; }
        public string DESCRIPCION { get; set; }

        public string NROSERIE { get; set; }
        public int? NUMCAJA { get; set; }

        public string IDFASE { get; set; }
        public string FASE { get; set; }
        public string IDOPERARIO { get; set; }

        public int IDCAJA { get; set; }
        public IEnumerable<T_TESTERROR> Errors { get; set; }
        public IEnumerable<TestValorView> Results { get; set; }

        public class TestValorView
        {
            public string Parametro { get; set; }
            public string Valor { get; set; }
            public string Funcion { get; set; }
            public Nullable<decimal> ValorMax { get; set; }
            public Nullable<decimal> ValorMin { get; set; }
            public string ValorEsperado { get; set; }
        }
    }

    public class TestIndicators
    {
        public double NumOK { get; set; }
        public double NumError { get; set; }
        public double NumFPY { get; set; }
        public double NumTiempoMedio { get; set; }
    }

    public class IndicatorsDescriptions
    {
        public string Descripcion { get; set; }
        public double Valor { get; set; }
    }

    public class TestOrdenProductoResult
    {
        public DateTime? Fecha { get; set; }
        public int? IdBastidor { get; set; }
        public string NumSerie { get; set; }
        public string PuntoError { get; set; }
        public string MsgError { get; set; }
        public int? NumCaja { get; set; }
    }

    public class LaserItemViewModel
    {
        public string DescripcionArticulo { get; set; }
        public string CodigoBcn { get; set; }
        public string DescripcionIT { get; set; }
        public DateTime? FechaPdf { get; set; }
        public byte[] DocPdf { get; set; }
        public Int32 NumBien { get; set; }
        public string Codigo { get; set; }
        public string descripcion { get; set; }
        public byte[] Imagen { get; set; }
    }

    public class BinaryItemViewModel
    {
        public string DescripcionArticulo { get; set; }
        public string CodigoBcn { get; set; }
        public string versionBinario { get; set; }
        public string Tipofichero { get; set; }
        public string NombreFichero { get; set; }
        public byte[] Fichero { get; set; }
        public string PathFicheroTest { get; set; }
    }

    public class LabelItemViewModel
    {
        public string IdFase { get; set; }
        public int NumArticulo { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }
        public string CodigoBCN { get; set; }
        public int NumComponente { get; set; }
        public int VersionComponente { get; set; }
        public string Papel { get; set; }
        public byte[] Data { get; set; }
    }

    public class ComponentsEstructureInProduct
    {
        public int numproducto { get; set; }
        public int version { get; set; }
        public string descripcion { get; set; }
        public int nivel { get; set; }
        public string trazabilidad { get; set; }
        public int childs { get; set; }
        public string path { get; set; }

        public int matricula { get; set; }
        public int cantidad { get; set; }
    }

    public class DireccionesViewModel
    {
        public string DIRECCION1 { get; set; }
        public string DIRECCION2 { get; set; }
        public string DIRECCION3 { get; set; }
        public string DIRECCION4 { get; set; }
    }

    public class AtributosViewModel
    {
        public string ATRIB1 { get; set; }
        public string ATRIB2 { get; set; }
        public string ATRIB3 { get; set; }
        public string ATRIB4 { get; set; }
        public string ATRIB5 { get; set; }
    }

    public class DescripcionLenguages
    {
        public string IDIDIOMA { get; set; }
        public string DESCRIPCION { get; set; }
    }

    public class ErrorsIrisDescription
    {
        public string DESCRIPCION { get; set; }
        public string DIAGNOSTICO { get; set; }
    }

    public class TemplateView
    {
        public string KEY { get; set; }
        public string VALUE { get; set; }
    }

    public class DeviceDataView
    {
        public int NUMORDEN { get; set; }
        public int NUMFABRICACION { get; set; }
        public int NUMMATRICULA { get; set; }
        public string NROSERIE { get; set; }
        public DateTime FECHAMAT { get; set; }
        public int NUMCAJA { get; set; }
        public int IDCAJA { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public string DESCRIPCION { get; set; }
    }

    public class BoxesView
    {
        public int NUMCAJA { get; set; }
        public string DESDENROSERIE { get; set; }
        public string HASTANROSERIE { get; set; }
        public int NUMEROCAJA { get; set; }
        public string TESTCAJA { get; set; }
        public int EQUIPOS { get; set; }
        public int UDSCAJA { get; set; } 
    }


    public class PackagingLabels
    {
        public string EMBALAJE { get; set; }
        public LabelTamplete.LabelTemplate PLANTILLA { get; set; }
    }

    public enum TestRepeatStatus
    {
        PASS,
        REPEAT_1,
        NOT_PASS_2,
        NOT_PASS_1,
        REPEAT_2
    }

    public class ResultadoFaseBySecuencias
    {
        public int SECUENCIA { get; set; }
        public int? NUMTEST { get; set; }
        public string IDFASE { get; set; }

        public DateTime? FECHAMAXTESTFASE { get; set; }
        public string RESULTADOMAXTESTFASE { get; set; }

        public DateTime? FECHAMAXTESTCONORDEN { get; set; }
        public string RESULTADOMAXTESTCONORDEN { get; set; }

        public string RESULTADO { get; set; }
    }
}
