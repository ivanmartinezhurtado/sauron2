﻿namespace Dezac.Tests.UserControls
{
    public partial class ProgressBarView : DialogViewBase, IFocusable
    {
        public ProgressBarView() 
            : base()
        {
            InitializeComponent();
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public int Value
        {
            get { return Progress.Value; }
            set { Progress.Value = value; }
        }

        public int MaxValue
        {
            get { return Progress.Maximum; }
            set { Progress.Maximum = value; }
        }

        public void SetValue(int value, string message, int? maxValue = null)
        {
            Value = value;
            Message = message;

            if (maxValue.HasValue)
                MaxValue = maxValue.Value;
        }
    }
}
