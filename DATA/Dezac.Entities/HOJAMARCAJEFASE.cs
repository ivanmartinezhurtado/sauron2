namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class HOJAMARCAJEFASE
    {
        public HOJAMARCAJEFASE()
        {
            this.DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
        }
    
        public int NUMHOJA { get; set; }
        public int NUMORDEN { get; set; }
        public string NOTAS { get; set; }
    
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }
        public virtual ORDEN ORDEN { get; set; }
    }
}
