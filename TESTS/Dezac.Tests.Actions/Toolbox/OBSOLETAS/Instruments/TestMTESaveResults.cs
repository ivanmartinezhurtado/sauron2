﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [Obsolete("Este Action en un futuro desaparecerá, no utilizar")]
    [DesignerAction(Type = typeof(TestMTESaveResutlsDescription))]
  
    public class TestMTESaveResults : ActionBase
    {
        [DefaultValue(230)]
        public double Voltage { get; set; }

        [DefaultValue(50)]
        public double Frecuency { get; set; }

        [DefaultValue(0)]
        public double Current { get; set; }

        [DefaultValue(false)]
        public bool Output120A { get; set; }

        public string TestPointName { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<Tower3>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            var voltage = TriLineValue.Create(Voltage);
            var current = TriLineValue.Create(Current);

            tower.MTEPS.Output120A = Output120A;

            tower.MTEPS.ApplyOffAndWaitStabilisation();

            var ini = DateTime.Now;

            tower.MTEPS.ApplyPresetsAndWaitStabilisation(voltage, current, Frecuency, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var end = DateTime.Now;

            var lapse = end - ini;

            testBase.Resultado.Set(string.Format("{0}_STABILIZATION_TIME", TestPointName), lapse.ToString("s\\.fff"), ParamUnidad.s);

            if (Voltage != 0)
            {
                testBase.Resultado.Set(string.Format("{0}_VOLTAGE_L1", TestPointName), tower.MTEPS.ReadVoltage().L1, ParamUnidad.V);

                testBase.Resultado.Set(string.Format("{0}_VOLTAGE_L2", TestPointName), tower.MTEPS.ReadVoltage().L2, ParamUnidad.V);

                testBase.Resultado.Set(string.Format("{0}_VOLTAGE_L3", TestPointName), tower.MTEPS.ReadVoltage().L3, ParamUnidad.V);
            }

            if (Current != 0)
            {
                testBase.Resultado.Set(string.Format("{0}_CURRENT_L1", TestPointName), tower.MTEPS.ReadCurrent().L1, ParamUnidad.A);

                testBase.Resultado.Set(string.Format("{0}_CURRENT_L2", TestPointName), tower.MTEPS.ReadCurrent().L2, ParamUnidad.A);

                testBase.Resultado.Set(string.Format("{0}_CURRENT_L3", TestPointName), tower.MTEPS.ReadCurrent().L3, ParamUnidad.A);
            }

        }
    }

    public class TestMTESaveResutlsDescription : ActionDescription
    {
        public override string Name { get { return "TestMTESaveResults"; } }
        public override string Description { get { return "Acción espeficia para autotest de torre con MTE que guarda valores de estabilización"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }


}
