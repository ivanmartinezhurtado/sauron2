﻿using Comunications.Ethernet;
using log4net;
using System;
using System.Drawing;
using Dezac.Core.Utility;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(TelnetFinalizedDescription))]
    public class TelnetFinalized : ActionBase
    {
        [Category("3. Instance")]
        [Description("Nombre del equipo")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var sender = GetVariable(DeviceName) as TelnetConnection;

            if (sender == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha llamado al metodo de inicialización del Telnet").Throw();

            sender.Dispose();

            sender = null;
          
            Context.Variables.Remove(DeviceName);
        }
    }

    public class TelnetFinalizedDescription : ActionDescription
    {
        public override string Name { get { return "TelnetFinalized"; } }
        public override string Description { get { return "Disposed una instancia de las comunicaciones Telnet"; } }
        public override string Category { get { return "Tools/Ethernet/Telnet"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
