﻿using Instruments.IO;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;


namespace Dezac.Instruments.Controls
{
    public partial class OutputControl : UserControl
    {
        public OutputControl()
        {
            InitializeComponent();
        }

        private bool state;

        public int NumIO { get; set; }
        public IOBase IO { get; set; }
        public String text { get; set; }
        public byte outType { get; set; }

        private ToolTip toolTip = new ToolTip();
        private int btnIndex
        {
            get
            {
                var index = outType == (byte)OutputType.valvulas ? 2
                    : outType == (byte)OutputType.reles || outType == (byte)OutputType.others ? 0
                    : 0;

                return index;
            }
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();         
            UpdateUI();
        }

        private void btState_Click(object sender, EventArgs e)
        {           

            switch (NumIO)        
	        {
                case 16:
                    if (MessageBox.Show("Estas seguro que deseas reiniciar todos los dispositivos de la torre ?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                             == DialogResult.OK)
                    {
                        ToogleState();
                        Thread.Sleep(5000);
                        ToogleState();
                    }
                    break;

                default: 
                    ToogleState();
                    break;                
            }
        }

        private void ToogleState()
        {
            if (IO.GetType() == typeof(USB4761))
            {
                IO.DO[NumIO] = !state;
                state = !state;
            }
            else
                IO.DO[NumIO] = !IO.DO[NumIO];

            UpdateUI();   
        }

        public void UpdateUI(bool logic = false, bool changeState = true, bool OutEnabled = false)
        {
            if (logic)
            {
                if (changeState)
                    state = false;
                Enabled = OutEnabled;

            }
            else
                if (IO.GetType() != typeof(USB4761))
                state = IO.DO[NumIO];

            btState.ImageIndex = state ? 1 : btnIndex;
            btState.Text = NumIO.ToString();
        }

        public void UpdateUI45(bool stateOut)
        {
            state = stateOut;
            
            btState.BackColor = state ? Color.LightGreen : SystemColors.Control;
            btState.ImageIndex = state ? 1 : 0;
            btState.Text = text == null ? NumIO.ToString() : text;

            IO.DO[NumIO] = state;
        }


        private void OutputControl_MouseEnter(object sender, EventArgs e)
        {
            btState.ImageIndex = state ? 1 : 3;

            toolTip.AutoPopDelay = 5000;
            toolTip.InitialDelay = 1000;
            toolTip.ReshowDelay = 500;
            toolTip.ForeColor = Color.Black;
            toolTip.ShowAlways = true;

            toolTip.SetToolTip(this.btState, text);
        }

        private void OutputControl_MouseLeave(object sender, EventArgs e)
        {
            btState.ImageIndex = state ? 1 : btnIndex;
        }

        public enum OutputType
        {
            valvulas = 0,           
            mux = 1,
            reles = 2,
            others = 3
        }
    }

    public class OutputsEventArgs : EventArgs
    {
        public byte Value { get; set; }
        public bool State { get; set; }

        public OutputsEventArgs(byte value, bool state)
        {
            this.Value = value;
            this.State = state;
        }
       
    }
}
