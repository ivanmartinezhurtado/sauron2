﻿namespace Dezac.Instruments.Controls
{
    partial class UC_LAMBDA
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtI1 = new System.Windows.Forms.MaskedTextBox();
            this.txtV1 = new System.Windows.Forms.MaskedTextBox();
            this.btnOn = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.btnOff = new System.Windows.Forms.Button();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtI1
            // 
            this.txtI1.BackColor = System.Drawing.Color.Black;
            this.txtI1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtI1.Font = new System.Drawing.Font("Lucida Console", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI1.ForeColor = System.Drawing.Color.LawnGreen;
            this.txtI1.Location = new System.Drawing.Point(363, 25);
            this.txtI1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtI1.Mask = "9.999";
            this.txtI1.Name = "txtI1";
            this.txtI1.Size = new System.Drawing.Size(140, 30);
            this.txtI1.TabIndex = 0;
            this.txtI1.Text = "0000";
            this.txtI1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtV1
            // 
            this.txtV1.BackColor = System.Drawing.Color.Black;
            this.txtV1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtV1.Font = new System.Drawing.Font("Lucida Console", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV1.ForeColor = System.Drawing.Color.LawnGreen;
            this.txtV1.Location = new System.Drawing.Point(195, 25);
            this.txtV1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtV1.Mask = "999.99";
            this.txtV1.Name = "txtV1";
            this.txtV1.Size = new System.Drawing.Size(142, 30);
            this.txtV1.TabIndex = 0;
            this.txtV1.Text = "02400";
            this.txtV1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.Transparent;
            this.btnOn.FlatAppearance.BorderSize = 0;
            this.btnOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.Black;
            this.btnOn.Location = new System.Drawing.Point(83, 27);
            this.btnOn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(89, 73);
            this.btnOn.TabIndex = 2;
            this.btnOn.Text = "ON";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.BackColor = System.Drawing.Color.Black;
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.ForeColor = System.Drawing.Color.LawnGreen;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(7, 106);
            this.cmb_Port.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(97, 28);
            this.cmb_Port.TabIndex = 0;
            // 
            // btnOff
            // 
            this.btnOff.BackColor = System.Drawing.Color.Transparent;
            this.btnOff.FlatAppearance.BorderSize = 0;
            this.btnOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOff.ForeColor = System.Drawing.Color.Black;
            this.btnOff.Location = new System.Drawing.Point(527, 28);
            this.btnOff.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOff.Name = "btnOff";
            this.btnOff.Size = new System.Drawing.Size(89, 73);
            this.btnOff.TabIndex = 3;
            this.btnOff.Text = "OFF";
            this.btnOff.UseVisualStyleBackColor = false;
            this.btnOff.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // pbStatus
            // 
            this.pbStatus.BackColor = System.Drawing.Color.Silver;
            this.pbStatus.Location = new System.Drawing.Point(195, 65);
            this.pbStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(308, 15);
            this.pbStatus.TabIndex = 39;
            this.pbStatus.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DimGray;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.button1.Location = new System.Drawing.Point(220, 103);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 38);
            this.button1.TabIndex = 40;
            this.button1.Text = "READ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // LAMBDA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Dezac.Instruments.Properties.Resources.LambdaOK;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.txtV1);
            this.Controls.Add(this.txtI1);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.btnOff);
            this.Controls.Add(this.btnOn);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "LAMBDA";
            this.Size = new System.Drawing.Size(680, 146);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.Button btnOff;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.MaskedTextBox txtV1;
        private System.Windows.Forms.MaskedTextBox txtI1;
        private System.Windows.Forms.Button button1;
    }
}
