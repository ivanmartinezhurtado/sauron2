﻿using Dezac.Core.Exceptions;
using Dezac.Tests;
using Dezac.Tests.Forms;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Zurc.Data;

namespace Zurc.Tests.Actions.Services
{
    public static class BastidorService
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(BastidorService));

        private static List<InputMatrixImageItemView> items;

        public static void BastidorController(TestBase testBase, int Rows = 1, int Columns = 1, string bastidor = null)
        {
            logger.Info("Validando bastidores");

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var data = context.Services.Get<ITestContext>();
            if (data == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CARGA_SERVICIO("ITestContext").Throw();


            if (data.modoPlayer == ModoTest.TIC)
            {
                List<string> matricula = new List<string>();
                matricula.Add(data.TestInfo.NumBastidor.ToString());
                SequenceContext.Current.SharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, matricula);
            }
            else
            {
                var bastidores = testBase.GetSharedVariable<List<string>>(ConstantsParameters.TestInfo.BASTIDORES, null);
                if (bastidores == null)
                {
                    items = new List<InputMatrixImageItemView>();

                    var result = iShell.ShowDialog("BASTIDORES", () => { return new InputMatrixImageView(Rows, Columns, context.TotalNumInstances, CreateMatrixItemController, ValidateView); }, MessageBoxButtons.OKCancel);

                    Assert.IsTrue(result == DialogResult.OK, TestException.Create().UUT.NUMERO_DE_BASTIDOR.CANCELADO_POR_USUARIO("El usuario ha cancelado la entrada del bastidor!"));
                }
            }

            if (data.NumOrden != null && data.NumProducto == 0)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("NO HAY ORDEN o PRODUCTO").Throw();

            logger.Info("Bastidores validados correctamente");
        }

        private static Control CreateMatrixItemController(int row, int col, int num)
        {
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            Image image = Properties.Resources.barcode_scan_icon; 

            var ctrl = new InputMatrixImageItemView
            {
                Title = string.Format("Nº BASTIDOR {0}", num + 1),
                Image = image
            };

            var width = ctrl.Width * col < (Screen.PrimaryScreen.Bounds.Width - 100) ? ctrl.Width : (Screen.PrimaryScreen.Bounds.Width - 100) / col;
            ctrl.Width = width;

            items.Add(ctrl);

            return ctrl;
        }

        private static bool ValidateView(InputMatrixImageView form)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            var codes = items.Select(p => p.InputText).ToList();

            int num;

            var sb = new StringBuilder();

            for (int i = 0; i < codes.Count; i++)
            {
                var isEmpty = string.IsNullOrEmpty(codes[i]);
                var view = form.GetView<InputMatrixImageItemView>(i);

                if (!view.IsDisabled)
                {
                    if (isEmpty)
                    {
                        view.IsValid = false;
                        sb.AppendFormat("Falta informar el bastidor nº {0}!\n", (i + 1));
                    }
                    else if (!int.TryParse(codes[i], out num))
                    {
                        view.IsValid = false;
                        sb.AppendFormat("El nº de bastidor {0} no es válido!\n", (i + 1));
                    }
                    else
                        view.IsValid = true;
                }
            }

            if (codes.GroupBy(p => p).Any(p => p.Count() > 1 && !string.IsNullOrEmpty(p.Key)))
                sb.Append("Hay nº de bastidores repetidos!");

            if (sb.Length > 0)
            {
                iShell.MsgBox(sb.ToString(), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            SequenceContext.Current.SharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, codes);

            foreach(InputMatrixImageItemView item in items)
            {
                item.Image.Dispose();
                item.Image = null;
            }

            return true;
        }

        public static void BastidorValidation()
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var data = context.Services.Get<ITestContext>();
            if (data == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CARGA_SERVICIO("ITestContext").Throw();

            var bastidores = context.SharedVariables.Get<List<string>>(ConstantsParameters.TestInfo.BASTIDORES);

            Assert.IsNotNull(bastidores, TestException.Create().UUT.NUMERO_DE_BASTIDOR.CANCELADO_POR_USUARIO("El usuario ha cancelado la entrada del bastidor!"));

            var text = bastidores[context.NumInstance - 1];

            if (context.RunningInstances == 1)
            {
                if (string.IsNullOrEmpty(text))
                    Assert.IsFalse(string.IsNullOrEmpty(text), TestException.Create().UUT.NUMERO_DE_BASTIDOR.CANCELADO_POR_USUARIO("El usuario ha cancelado la entrada del bastidor!"));
            }
            else
            {
                if (string.IsNullOrEmpty(text))
                    return;
            }
            int bastidor = Convert.ToInt32(text);

            using (var svc = new ZurcService())
            {
                if (!IsIDPBastidor(bastidor))
                {
                    var matricula = svc.GetMatricula(bastidor);
                    if (matricula == null)
                    {
                        var producto = svc.GetArticulo(data.NumProducto);

                        var msg = string.Format("Matrícula {0} del equipo {1} no registrado!, ¿Desea continuar?", bastidor, producto.Descripcion);
                        var resp = iShell.ShowDialog("ATENCÍON", new Label() { Text = msg, Width = 400, Height = 100, Padding = new Padding(10) }, MessageBoxButtons.OKCancel);
                        if (resp != DialogResult.OK)
                            throw new Exception();
                    }

                    if (context.RunMode == RunMode.Release)
                    if (data.NumOrden == null || data.NumOrden.GetValueOrDefault() == 0)
                        throw new Exception("No se puede realizar un test en modo PRODUCCION o en modo TIC sin Orden de Fabricacion");

                    //logger.InfoFormat("Comprobamos si este bastidor es OK en las fases anteriores");

                    //svc.GetResultadoNumTestFasesIsOK(bastidor, data.NumOrden, data.IdFase, data.IsReproceso);
                    
                    ValidaMatricula(data, svc, bastidor);
                }
            }
            
            data.TestInfo.NumBastidor = Convert.ToUInt64(bastidor);
            data.TestInfo.ProductoVersion = string.Format("{0}/{1}", data.NumProducto, data.Version);
            data.TestInfo.Producto = data.NumProducto.ToString();

            context.Variables.AddOrUpdate(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor);

            logger.InfoFormat("Nº de Bastidor: {0}", bastidor);
        }

        private static void ValidaMatricula(ITestContext data, ZurcService svc, int bastidor)
        {
            if (data.NumOrden.GetValueOrDefault() == 0)
            {
                svc.TestProductSameCkeck(bastidor, data.NumProducto);
                return;
            }

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var op = svc.GetOrdenProducto(data.NumOrden.Value, bastidor);
            if (op == null)
                return;

            if (op.NumSerie != null)
            {
                //if (SequenceContext.Current.RunMode == RunMode.Release)
                //{
                    var test = svc.GetTest(data.NumOrden.Value, bastidor);
                    if (test.NumSerie != null && op.NumSerie != test.NumSerie)
                        TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("NUMERO DE SERIE DIFERENTE ENTRE ORDENPRODUCTO Y TEST").Throw();

                    var orden = svc.GetOrden(data.NumOrden.Value);
                    var msg = string.Format("! ATENCIÓN, ESTE EQUIPO YA ESTA REGISTRADO EN LA ORDEN {0} CON NUMERO DE SERIE {1}\n\n- PULSE SI PARA MANTENER EL Nº DE SERIE\n- PULSE NO PARA OBTENER UN NUEVO Nº DE SERIE\n- PULSE CANCELAR PARA ABORTAR EL TEST", 
                        orden.Codigo, test.NumSerie/*, op.NumCaja*/);

                var resp = iShell.ShowDialog("EQUIPO REGISTRADO", new Label() { Text = msg, Width = 400, Height = 100, Padding = new Padding(10) }, MessageBoxButtons.YesNoCancel);
                if (resp == DialogResult.Yes)
                    data.TestInfo.NumSerie = op.NumSerie;
                //data.NumCajaActual = op.NumCaja.Value;
                    else if (resp == DialogResult.Cancel)
                        throw new Exception("Test cancelado al por el usuario");
                //}
            }
        }

        private static bool IsIDPBastidor(int bastidor)
        {
            switch (bastidor)
            {
                case 985:
                case 379:
                case 392:
                case 393:
                case 230:
                case 394:
                case 666:
                case 413:
                    return true;
            }

            return false;
        }
    }
}
