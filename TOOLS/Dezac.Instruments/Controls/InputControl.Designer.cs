﻿namespace Dezac.Instruments.Controls
{
    partial class InputControl
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputControl));
            this.ImageListIO = new System.Windows.Forms.ImageList(this.components);
            this.btState = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // ImageListIO
            // 
            this.ImageListIO.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListIO.ImageStream")));
            this.ImageListIO.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListIO.Images.SetKeyName(0, "btnRed.png");
            this.ImageListIO.Images.SetKeyName(1, "btnGreen.png");
            // 
            // btState
            // 
            this.btState.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btState.BackColor = System.Drawing.Color.Transparent;
            this.btState.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btState.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btState.ImageIndex = 0;
            this.btState.ImageList = this.ImageListIO;
            this.btState.Location = new System.Drawing.Point(0, 0);
            this.btState.Margin = new System.Windows.Forms.Padding(0);
            this.btState.Name = "btState";
            this.btState.Size = new System.Drawing.Size(30, 32);
            this.btState.TabIndex = 0;
            this.btState.Text = "50";
            this.btState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InputControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btState);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "InputControl";
            this.Size = new System.Drawing.Size(30, 32);
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.ImageList ImageListIO;
        private System.Windows.Forms.Label btState;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
