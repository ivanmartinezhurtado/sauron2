namespace Dezac.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.ARTICULOCLIENTE")]
    public partial class ARTICULOCLIENTE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMARTICULO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string IDCLIENTE { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(35)]
        public string REFCLIENTE { get; set; }

        [StringLength(20)]
        public string REFCLIENTE2 { get; set; }

        public decimal? PRECIO { get; set; }

        [StringLength(20)]
        public string ATTRIB1 { get; set; }

        [StringLength(20)]
        public string ATTRIB2 { get; set; }

        [StringLength(20)]
        public string ATTRIB3 { get; set; }

        [StringLength(20)]
        public string ATTRIB4 { get; set; }

        [StringLength(20)]
        public string ATTRIB5 { get; set; }

        [StringLength(1)]
        public string INACTIVO { get; set; }

        public decimal? PRECIOBORRADOR { get; set; }

        [Required]
        [StringLength(1)]
        public string OBSOLETO { get; set; }

        [StringLength(500)]
        public string NOTASOBSOLETO { get; set; }

        [StringLength(30)]
        public string USERNAME { get; set; }

        [StringLength(500)]
        public string OBSERVACIONESBORRADOR { get; set; }

        [StringLength(13)]
        public string EAN13 { get; set; }

        public int? LEADTIME { get; set; }

        public int? LOTEOPTIMO { get; set; }

        [StringLength(10)]
        public string IDCLIENTEFINAL { get; set; }

        [Required]
        [StringLength(1)]
        public string VALIDARVERSION { get; set; }

        [StringLength(13)]
        public string EAN13MARCA { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }
    }
}
