﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Dezac.Data.ViewModels;
using WinTestPlayer.Services;
using Dezac.Core.Enumerate;

namespace WinTestPlayer.Views
{
    public partial class LabelTemplateView : UserControl
    {
        protected readonly AppService app;
        protected readonly ReportsService reportService;

        private int producto;
        private int version;
        private LabelTamplete.LabelTemplate template;
        private int bastidor;

        private Image defaultImage;
        private string labelPath;
        private string imagePath;
        private Dictionary<string, string> JSONFile;

        public LabelTemplateView()
        {
            InitializeComponent();
        }

        public LabelTemplateView(AppService app, ReportsService reportsService)
            : this()
        {
            reportService = reportsService;
            this.app = app;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            foreach (object tmp in Enum.GetValues(typeof(LabelTamplete.LabelTemplate)))
                txtTipoEtiqueta.Items.Add(tmp);

            defaultImage = imgView.Image;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                imgView.Image = defaultImage;

                grid.Enabled = false;
                ValidateData();

                labelPath = reportService.GetLabelPath(producto, version, null, template);
                if (string.IsNullOrEmpty(labelPath))
                    throw new Exception(string.Format("Plantilla de {0} inexistente o no validada para este producto/version", template.ToString()));

                SetGrid(reportService.GetFieldsFile(labelPath));

                if (bastidor != 0)
                    SetImageAndGridJsonValues();
                else
                    SetImageAndGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al importar la etiqueta. " + ex.Message, "Print Label Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SetDefaultImage();
            }
            finally
            {
                grid.Enabled = true;
            }
        }

        private void ValidateData()
        {
            producto = 0;
            version = 0;
            template = 0;
            bastidor = 0;

            int.TryParse(txtProducto.Text, out producto);
            if (producto == 0)
                throw new Exception("No se ha especificado un producto");

            int.TryParse(txtVersion.Text, out version);
            if (version == 0)
                throw new Exception("No se ha especificado la versión del producto");

            Enum.TryParse(txtTipoEtiqueta.Text, out template);
            if (template == 0)
                throw new Exception("No se ha especificado una plantilla de etiqueta");

            if (!string.IsNullOrEmpty(txtBastidor.Text))
            {
                int.TryParse(txtBastidor.Text, out bastidor);
                if (bastidor == 0)
                    throw new Exception("No se ha especificado un bastidor");
            }
        }

        private void SetImage(string pathImage)
        {
            imgView.Image = Image.FromFile(pathImage);
        }

        private void SetDefaultImage()
        {
            imgView.Image.Dispose();
            imgView.Image = defaultImage;

        }

        private void SetGrid(List<TemplateView> templateData)
        {
            bsList.DataSource = null;
            bsList.DataSource = templateData;
        }

        private List<TemplateView> GetGrid()
        {
            return grid.Rows.Select(p => (TemplateView)p.DataBoundItem).ToList();
        }

        private void SetImageAndGrid()
        {
            SetGrid(reportService.GetFieldsFile(labelPath));
            string imgPath = reportService.ExportBarTenderFile(labelPath);
            SetImage(imgPath);
        }

        private void SetImageAndGridJsonValues()
        {
            string labelData = reportService.GetFileLabelData(bastidor, "PRINT_LABEL_" + template.ToString());

            if (labelData == null)
            {
                MessageBox.Show(string.Format("No se ha obtenido un fichero con las variables de etiqueta para el bastidor : {0}", bastidor), "FICHERO ETIQUETA EXISTENTE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SetImageAndGrid();
                return;
            }

            JSONFile = Utils.AppProfile.DeserializeFile<Dictionary<string, string>>(labelData);

            var templateData = new List<TemplateView>();
            foreach (Telerik.WinControls.UI.GridViewRowInfo row in grid.Rows)
            {
                string value = JSONFile.Where(p => p.Key == ((TemplateView)row.DataBoundItem).KEY).Select(p => p.Value).FirstOrDefault();
                templateData.Add(new TemplateView { KEY = ((TemplateView)row.DataBoundItem).KEY, VALUE = value });
            }

            SetGrid(templateData);
            imagePath = reportService.SetFieldsFileAndExportImage(labelPath, GetGrid());
            SetImage(imagePath);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(labelPath) && template != 0)
                MessageBox.Show("No se ha cargado ningún archivo de etiqueta.", "Print Label Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

            var dataPrint = new Dictionary<string, string>();

            foreach (Telerik.WinControls.UI.GridViewRowInfo row in grid.Rows)
                dataPrint.Add(((TemplateView)row.DataBoundItem).KEY, ((TemplateView)row.DataBoundItem).VALUE);

            reportService.PrintLabelFromData(app, dataPrint, labelPath, template);

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (labelPath == null)
                return;

            SetDefaultImage();

            List<TemplateView> templateData = GetGrid();
            SetGrid(templateData);
            imagePath = reportService.SetFieldsFileAndExportImage(labelPath, GetGrid());
            SetImage(imagePath);
        }

        private void txtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
                Search();
        }

    }
}
