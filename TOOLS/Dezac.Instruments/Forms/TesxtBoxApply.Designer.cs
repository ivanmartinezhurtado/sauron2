﻿namespace Dezac.Instruments.Forms
{
    partial class TesxtBoxApply
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAmplitud = new System.Windows.Forms.TextBox();
            this.txtHarmonic = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOn = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbPhase = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbCurrent = new System.Windows.Forms.RadioButton();
            this.rbVoltage = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtAmplitud
            // 
            this.txtAmplitud.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmplitud.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmplitud.ForeColor = System.Drawing.Color.Red;
            this.txtAmplitud.Location = new System.Drawing.Point(238, 100);
            this.txtAmplitud.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtAmplitud.Name = "txtAmplitud";
            this.txtAmplitud.Size = new System.Drawing.Size(87, 26);
            this.txtAmplitud.TabIndex = 106;
            this.txtAmplitud.Text = "0";
            this.txtAmplitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtHarmonic
            // 
            this.txtHarmonic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHarmonic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHarmonic.ForeColor = System.Drawing.Color.Blue;
            this.txtHarmonic.Location = new System.Drawing.Point(122, 100);
            this.txtHarmonic.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtHarmonic.Name = "txtHarmonic";
            this.txtHarmonic.Size = new System.Drawing.Size(84, 26);
            this.txtHarmonic.TabIndex = 103;
            this.txtHarmonic.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.PowerSourceMTE;
            this.pictureBox1.Location = new System.Drawing.Point(3, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(74, 39);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 102;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(125, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 16);
            this.label7.TabIndex = 92;
            this.label7.Text = "Harmonic";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(234, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 16);
            this.label4.TabIndex = 91;
            this.label4.Text = "Amplitude (%)";
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(80, 134);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(96, 39);
            this.btnOn.TabIndex = 109;
            this.btnOn.Text = "Apply";
            this.btnOn.UseVisualStyleBackColor = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(23, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 16);
            this.label15.TabIndex = 112;
            this.label15.Text = "Phase";
            // 
            // cmbPhase
            // 
            this.cmbPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPhase.FormattingEnabled = true;
            this.cmbPhase.Items.AddRange(new object[] {
            "L1",
            "L2",
            "L3"});
            this.cmbPhase.Location = new System.Drawing.Point(12, 98);
            this.cmbPhase.Name = "cmbPhase";
            this.cmbPhase.Size = new System.Drawing.Size(78, 28);
            this.cmbPhase.TabIndex = 111;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DimGray;
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(184, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 39);
            this.button1.TabIndex = 113;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.rbCurrent);
            this.groupBox5.Controls.Add(this.rbVoltage);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(104, 11);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(234, 51);
            this.groupBox5.TabIndex = 114;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "HARMONICS";
            // 
            // rbCurrent
            // 
            this.rbCurrent.AutoSize = true;
            this.rbCurrent.Location = new System.Drawing.Point(124, 25);
            this.rbCurrent.Name = "rbCurrent";
            this.rbCurrent.Size = new System.Drawing.Size(100, 20);
            this.rbCurrent.TabIndex = 1;
            this.rbCurrent.TabStop = true;
            this.rbCurrent.Text = "CURRENT";
            this.rbCurrent.UseVisualStyleBackColor = true;
            // 
            // rbVoltage
            // 
            this.rbVoltage.AutoSize = true;
            this.rbVoltage.Checked = true;
            this.rbVoltage.Location = new System.Drawing.Point(12, 25);
            this.rbVoltage.Name = "rbVoltage";
            this.rbVoltage.Size = new System.Drawing.Size(96, 20);
            this.rbVoltage.TabIndex = 0;
            this.rbVoltage.TabStop = true;
            this.rbVoltage.Text = "VOLTAGE";
            this.rbVoltage.UseVisualStyleBackColor = true;
            // 
            // TesxtBoxApply
            // 
            this.AcceptButton = this.btnOn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(350, 179);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cmbPhase);
            this.Controls.Add(this.btnOn);
            this.Controls.Add(this.txtAmplitud);
            this.Controls.Add(this.txtHarmonic);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TesxtBoxApply";
            this.ShowIcon = false;
            this.Text = "HARMONICS";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtAmplitud;
        private System.Windows.Forms.TextBox txtHarmonic;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbPhase;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbCurrent;
        private System.Windows.Forms.RadioButton rbVoltage;
    }
}