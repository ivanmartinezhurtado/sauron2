﻿namespace WinTestPlayer.Controls
{
    partial class ResumenInfo
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOperario = new MetroFramework.Controls.MetroLabel();
            this.pbOK = new MetroFramework.Controls.MetroProgressBar();
            this.spOK = new MetroFramework.Controls.MetroProgressSpinner();
            this.lblOK = new MetroFramework.Controls.MetroLabel();
            this.lblPorOK = new MetroFramework.Controls.MetroLabel();
            this.lblPorErr = new MetroFramework.Controls.MetroLabel();
            this.lblErr = new MetroFramework.Controls.MetroLabel();
            this.spErr = new MetroFramework.Controls.MetroProgressSpinner();
            this.pbErr = new MetroFramework.Controls.MetroProgressBar();
            this.SuspendLayout();
            // 
            // lblOperario
            // 
            this.lblOperario.AutoSize = true;
            this.lblOperario.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblOperario.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblOperario.Location = new System.Drawing.Point(15, 12);
            this.lblOperario.Name = "lblOperario";
            this.lblOperario.Size = new System.Drawing.Size(240, 25);
            this.lblOperario.Style = MetroFramework.MetroColorStyle.Red;
            this.lblOperario.TabIndex = 0;
            this.lblOperario.Text = "Ningún operario identificado";
            this.lblOperario.UseStyleColors = true;
            // 
            // pbOK
            // 
            this.pbOK.Location = new System.Drawing.Point(15, 294);
            this.pbOK.Name = "pbOK";
            this.pbOK.Size = new System.Drawing.Size(281, 23);
            this.pbOK.TabIndex = 1;
            // 
            // spOK
            // 
            this.spOK.Location = new System.Drawing.Point(312, 272);
            this.spOK.Maximum = 100;
            this.spOK.Name = "spOK";
            this.spOK.Size = new System.Drawing.Size(40, 40);
            this.spOK.Spinning = false;
            this.spOK.TabIndex = 2;
            this.spOK.UseSelectable = true;
            this.spOK.Value = 75;
            // 
            // lblOK
            // 
            this.lblOK.AutoSize = true;
            this.lblOK.Location = new System.Drawing.Point(15, 272);
            this.lblOK.Name = "lblOK";
            this.lblOK.Size = new System.Drawing.Size(77, 19);
            this.lblOK.TabIndex = 3;
            this.lblOK.Text = "Nº Test OK:";
            // 
            // lblPorOK
            // 
            this.lblPorOK.AutoSize = true;
            this.lblPorOK.Location = new System.Drawing.Point(358, 293);
            this.lblPorOK.Name = "lblPorOK";
            this.lblPorOK.Size = new System.Drawing.Size(20, 19);
            this.lblPorOK.TabIndex = 4;
            this.lblPorOK.Text = "%";
            // 
            // lblPorErr
            // 
            this.lblPorErr.AutoSize = true;
            this.lblPorErr.Location = new System.Drawing.Point(358, 357);
            this.lblPorErr.Name = "lblPorErr";
            this.lblPorErr.Size = new System.Drawing.Size(20, 19);
            this.lblPorErr.TabIndex = 8;
            this.lblPorErr.Text = "%";
            // 
            // lblErr
            // 
            this.lblErr.AutoSize = true;
            this.lblErr.Location = new System.Drawing.Point(15, 336);
            this.lblErr.Name = "lblErr";
            this.lblErr.Size = new System.Drawing.Size(77, 19);
            this.lblErr.TabIndex = 7;
            this.lblErr.Text = "Nº Test KO:";
            // 
            // spErr
            // 
            this.spErr.Location = new System.Drawing.Point(312, 336);
            this.spErr.Maximum = 100;
            this.spErr.Name = "spErr";
            this.spErr.Size = new System.Drawing.Size(40, 40);
            this.spErr.Spinning = false;
            this.spErr.Style = MetroFramework.MetroColorStyle.Red;
            this.spErr.TabIndex = 6;
            this.spErr.UseSelectable = true;
            this.spErr.UseStyleColors = true;
            this.spErr.Value = 75;
            // 
            // pbErr
            // 
            this.pbErr.Location = new System.Drawing.Point(15, 358);
            this.pbErr.Name = "pbErr";
            this.pbErr.Size = new System.Drawing.Size(281, 23);
            this.pbErr.TabIndex = 5;
            // 
            // ResumenInfo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblPorErr);
            this.Controls.Add(this.lblErr);
            this.Controls.Add(this.spErr);
            this.Controls.Add(this.pbErr);
            this.Controls.Add(this.lblPorOK);
            this.Controls.Add(this.lblOK);
            this.Controls.Add(this.spOK);
            this.Controls.Add(this.pbOK);
            this.Controls.Add(this.lblOperario);
            this.Name = "ResumenInfo";
            this.Size = new System.Drawing.Size(392, 439);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblOperario;
        private MetroFramework.Controls.MetroProgressBar pbOK;
        private MetroFramework.Controls.MetroProgressSpinner spOK;
        private MetroFramework.Controls.MetroLabel lblOK;
        private MetroFramework.Controls.MetroLabel lblPorOK;
        private MetroFramework.Controls.MetroLabel lblPorErr;
        private MetroFramework.Controls.MetroLabel lblErr;
        private MetroFramework.Controls.MetroProgressSpinner spErr;
        private MetroFramework.Controls.MetroProgressBar pbErr;
    }
}
