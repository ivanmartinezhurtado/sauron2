﻿using TaskRunner.Enumerators;

namespace WinTestPlayer.Views
{
    partial class HistorialView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.LineSeries lineSeries1 = new Telerik.WinControls.UI.LineSeries();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.Data.GroupDescriptor groupDescriptor1 = new Telerik.WinControls.Data.GroupDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn5 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn6 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistorialView));
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            this.chart = new Telerik.WinControls.UI.RadChartView();
            this.gridComp = new Telerik.WinControls.UI.RadGridView();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.grid = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.boxGrid = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.txtOrden = new Telerik.WinControls.UI.CommandBarTextBox();
            this.btnFiltrar = new Telerik.WinControls.UI.CommandBarButton();
            this.btnOrdenPrint = new Telerik.WinControls.UI.CommandBarButton();
            this.btnCaja = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel2 = new Telerik.WinControls.UI.CommandBarLabel();
            this.txtBastidor = new Telerik.WinControls.UI.CommandBarTextBox();
            this.btnBastidorPrint = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator8 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator9 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator10 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarLabel3 = new Telerik.WinControls.UI.CommandBarLabel();
            this.btnLabelManual = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarRowElement4 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarLabel4 = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarLabel5 = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarLabel6 = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarTextBox2 = new Telerik.WinControls.UI.CommandBarTextBox();
            this.bsList = new System.Windows.Forms.BindingSource(this.components);
            this.bsBoxList = new System.Windows.Forms.BindingSource(this.components);
            this.enumBinder1 = new Telerik.WinControls.UI.Data.EnumBinder();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComp.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid.MasterTemplate)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boxGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxGrid.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsBoxList)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            this.chart.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.TickOrigin = null;
            this.chart.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.chart.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart.Location = new System.Drawing.Point(0, 0);
            this.chart.Name = "chart";
            // 
            // 
            // 
            this.chart.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 1127, 236);
            lineSeries1.HorizontalAxis = categoricalAxis1;
            lineSeries1.Name = "lineChart";
            lineSeries1.ShowLabels = true;
            lineSeries1.VerticalAxis = linearAxis1;
            this.chart.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            lineSeries1});
            this.chart.ShowGrid = false;
            this.chart.ShowLegend = true;
            this.chart.ShowPanZoom = true;
            this.chart.Size = new System.Drawing.Size(1127, 236);
            this.chart.TabIndex = 1;
            this.chart.Text = "radChartView1";
            // 
            // gridComp
            // 
            this.gridComp.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gridComp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridComp.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridComp.MasterTemplate.AllowAddNewRow = false;
            this.gridComp.MasterTemplate.MultiSelect = true;
            this.gridComp.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridComp.Name = "gridComp";
            this.gridComp.ReadOnly = true;
            // 
            // 
            // 
            this.gridComp.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 300, 187);
            this.gridComp.Size = new System.Drawing.Size(300, 187);
            this.gridComp.TabIndex = 0;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radPageView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radCommandBar1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1148, 752);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(2, 42);
            this.radPageView1.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1144, 708);
            this.radPageView1.TabIndex = 1;
            this.radPageView1.Text = "radPageView1";
            this.radPageView1.SelectedPageChanged += new System.EventHandler(this.RadPageView1_SelectedPageChanged);
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.grid);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(52F, 24F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 33);
            this.radPageViewPage1.Margin = new System.Windows.Forms.Padding(2);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1123, 664);
            this.radPageViewPage1.Text = "Equipos";
            // 
            // grid
            // 
            this.grid.BackColor = System.Drawing.SystemColors.Control;
            this.grid.Cursor = System.Windows.Forms.Cursors.Default;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.grid.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grid.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.grid.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.grid.MasterTemplate.AutoGenerateColumns = false;
            this.grid.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "NUMFABRICACION";
            gridViewTextBoxColumn1.HeaderText = "Nº Fabricacion";
            gridViewTextBoxColumn1.Name = "NUMFABRICACION";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 112;
            gridViewDecimalColumn1.DataType = typeof(int);
            gridViewDecimalColumn1.EnableExpressionEditor = false;
            gridViewDecimalColumn1.FieldName = "NUMMATRICULA";
            gridViewDecimalColumn1.HeaderText = "Nº Matrícula";
            gridViewDecimalColumn1.IsAutoGenerated = true;
            gridViewDecimalColumn1.Name = "NUMMATRICULA";
            gridViewDecimalColumn1.ReadOnly = true;
            gridViewDecimalColumn1.Width = 131;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "NROSERIE";
            gridViewTextBoxColumn2.HeaderText = "Nº Serie";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.Name = "NROSERIE";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 169;
            gridViewTextBoxColumn3.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "FECHAMAT";
            gridViewTextBoxColumn3.HeaderText = "Fecha";
            gridViewTextBoxColumn3.Name = "FECHAMAT";
            gridViewTextBoxColumn3.Width = 123;
            gridViewTextBoxColumn4.DataType = typeof(int);
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "NUMCAJA";
            gridViewTextBoxColumn4.HeaderText = "Nº Caja";
            gridViewTextBoxColumn4.Name = "NUMCAJA";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn5.DataType = typeof(System.Nullable<int>);
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "IDCAJA";
            gridViewTextBoxColumn5.HeaderText = "Caja";
            gridViewTextBoxColumn5.Name = "IDCAJA";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.VisibleInColumnChooser = false;
            gridViewTextBoxColumn5.Width = 77;
            gridViewDecimalColumn2.DataType = typeof(int);
            gridViewDecimalColumn2.EnableExpressionEditor = false;
            gridViewDecimalColumn2.FieldName = "NUMPRODUCTO";
            gridViewDecimalColumn2.HeaderText = "Nº Producto";
            gridViewDecimalColumn2.IsAutoGenerated = true;
            gridViewDecimalColumn2.Name = "NUMPRODUCTO";
            gridViewDecimalColumn2.ReadOnly = true;
            gridViewDecimalColumn2.Width = 119;
            gridViewDecimalColumn3.DataType = typeof(int);
            gridViewDecimalColumn3.EnableExpressionEditor = false;
            gridViewDecimalColumn3.FieldName = "VERSION";
            gridViewDecimalColumn3.HeaderText = "Versión";
            gridViewDecimalColumn3.IsAutoGenerated = true;
            gridViewDecimalColumn3.Name = "VERSION";
            gridViewDecimalColumn3.ReadOnly = true;
            gridViewDecimalColumn3.Width = 56;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "DESCRIPCION";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "DESCRIPCION";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 218;
            gridViewCheckBoxColumn1.AllowFiltering = false;
            gridViewCheckBoxColumn1.EnableExpressionEditor = false;
            gridViewCheckBoxColumn1.EnableHeaderCheckBox = true;
            gridViewCheckBoxColumn1.FieldName = "CHECK";
            gridViewCheckBoxColumn1.MinWidth = 20;
            gridViewCheckBoxColumn1.Name = "CHECK";
            gridViewCheckBoxColumn1.Width = 26;
            this.grid.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewDecimalColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewTextBoxColumn6,
            gridViewCheckBoxColumn1});
            this.grid.MasterTemplate.DataSource = this.bsList;
            this.grid.MasterTemplate.EnableFiltering = true;
            sortDescriptor1.PropertyName = "NUMCAJA";
            groupDescriptor1.GroupNames.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.grid.MasterTemplate.GroupDescriptors.AddRange(new Telerik.WinControls.Data.GroupDescriptor[] {
            groupDescriptor1});
            this.grid.MasterTemplate.MultiSelect = true;
            this.grid.MasterTemplate.ShowGroupedColumns = true;
            this.grid.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.grid.Name = "grid";
            this.grid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.grid.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 1123, 660);
            this.grid.Size = new System.Drawing.Size(1123, 664);
            this.grid.TabIndex = 5;
            this.grid.Text = "txtBastidor";
            this.grid.SelectionChanged += new System.EventHandler(this.Grid_SelectionChanged);
            this.grid.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.Grid_CellDoubleClick);
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.boxGrid);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(38F, 24F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Margin = new System.Windows.Forms.Padding(2);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(1123, 660);
            this.radPageViewPage2.Text = "Cajas";
            // 
            // boxGrid
            // 
            this.boxGrid.BackColor = System.Drawing.SystemColors.Control;
            this.boxGrid.Cursor = System.Windows.Forms.Cursors.Default;
            this.boxGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.boxGrid.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.boxGrid.ForeColor = System.Drawing.SystemColors.ControlText;
            this.boxGrid.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.boxGrid.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.boxGrid.MasterTemplate.AutoGenerateColumns = false;
            this.boxGrid.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn7.DataType = typeof(int);
            gridViewTextBoxColumn7.FieldName = "NUMEROCAJA";
            gridViewTextBoxColumn7.HeaderText = "ID Caja";
            gridViewTextBoxColumn7.Name = "NUMEROCAJA";
            gridViewTextBoxColumn7.Width = 36;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.EnableExpressionEditor = false;
            gridViewTextBoxColumn8.FieldName = "NUMCAJA";
            gridViewTextBoxColumn8.HeaderText = "Nº Caja";
            gridViewTextBoxColumn8.Name = "NUMCAJA";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn8.Width = 79;
            gridViewTextBoxColumn9.EnableExpressionEditor = false;
            gridViewTextBoxColumn9.FieldName = "DESDENROSERIE";
            gridViewTextBoxColumn9.HeaderText = "Desde Nº Serie";
            gridViewTextBoxColumn9.IsAutoGenerated = true;
            gridViewTextBoxColumn9.Name = "DESDENROSERIE";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 270;
            gridViewDecimalColumn4.DataType = typeof(string);
            gridViewDecimalColumn4.EnableExpressionEditor = false;
            gridViewDecimalColumn4.FieldName = "HASTANROSERIE";
            gridViewDecimalColumn4.HeaderText = "Hasta Nº Serie";
            gridViewDecimalColumn4.IsAutoGenerated = true;
            gridViewDecimalColumn4.Name = "HASTANROSERIE";
            gridViewDecimalColumn4.ReadOnly = true;
            gridViewDecimalColumn4.Width = 210;
            gridViewTextBoxColumn10.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn10.EnableExpressionEditor = false;
            gridViewTextBoxColumn10.FieldName = "TESTCAJA";
            gridViewTextBoxColumn10.HeaderText = "Test Caja";
            gridViewTextBoxColumn10.Name = "TESTCAJA";
            gridViewTextBoxColumn10.Width = 197;
            gridViewDecimalColumn5.DataType = typeof(int);
            gridViewDecimalColumn5.EnableExpressionEditor = false;
            gridViewDecimalColumn5.FieldName = "EQUIPOS";
            gridViewDecimalColumn5.HeaderText = "Equipos";
            gridViewDecimalColumn5.IsAutoGenerated = true;
            gridViewDecimalColumn5.Name = "EQUIPOS";
            gridViewDecimalColumn5.ReadOnly = true;
            gridViewDecimalColumn5.Width = 190;
            gridViewDecimalColumn6.DataType = typeof(int);
            gridViewDecimalColumn6.EnableExpressionEditor = false;
            gridViewDecimalColumn6.FieldName = "UDSCAJA";
            gridViewDecimalColumn6.HeaderText = "Unidades Caja";
            gridViewDecimalColumn6.IsAutoGenerated = true;
            gridViewDecimalColumn6.Name = "UDSCAJA";
            gridViewDecimalColumn6.ReadOnly = true;
            gridViewDecimalColumn6.Width = 91;
            gridViewCheckBoxColumn2.AllowFiltering = false;
            gridViewCheckBoxColumn2.EnableExpressionEditor = false;
            gridViewCheckBoxColumn2.EnableHeaderCheckBox = true;
            gridViewCheckBoxColumn2.FieldName = "CHECK";
            gridViewCheckBoxColumn2.MinWidth = 20;
            gridViewCheckBoxColumn2.Name = "CHECK";
            gridViewCheckBoxColumn2.Width = 37;
            this.boxGrid.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewDecimalColumn4,
            gridViewTextBoxColumn10,
            gridViewDecimalColumn5,
            gridViewDecimalColumn6,
            gridViewCheckBoxColumn2});
            this.boxGrid.MasterTemplate.DataSource = this.bsBoxList;
            this.boxGrid.MasterTemplate.EnableFiltering = true;
            this.boxGrid.MasterTemplate.MultiSelect = true;
            this.boxGrid.MasterTemplate.ShowGroupedColumns = true;
            sortDescriptor2.PropertyName = "NUMCAJA";
            this.boxGrid.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.boxGrid.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.boxGrid.Name = "boxGrid";
            this.boxGrid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.boxGrid.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 1123, 660);
            this.boxGrid.Size = new System.Drawing.Size(1123, 660);
            this.boxGrid.TabIndex = 6;
            this.boxGrid.SelectionChanged += new System.EventHandler(this.BoxGrid_SelectionChanged);
            this.boxGrid.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.BoxGrid_CellDoubleClick);
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(2, 2);
            this.radCommandBar1.Margin = new System.Windows.Forms.Padding(2);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1,
            this.commandBarRowElement4});
            this.radCommandBar1.Size = new System.Drawing.Size(1144, 65);
            this.radCommandBar1.TabIndex = 0;
            this.radCommandBar1.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.commandBarLabel1,
            this.txtOrden,
            this.btnFiltrar,
            this.btnOrdenPrint,
            this.btnCaja,
            this.commandBarSeparator1,
            this.commandBarSeparator2,
            this.commandBarSeparator3,
            this.commandBarSeparator4,
            this.commandBarSeparator5,
            this.commandBarLabel2,
            this.txtBastidor,
            this.btnBastidorPrint,
            this.commandBarSeparator6,
            this.commandBarSeparator7,
            this.commandBarSeparator8,
            this.commandBarSeparator9,
            this.commandBarSeparator10,
            this.commandBarLabel3,
            this.btnLabelManual});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "Nº Orden";
            // 
            // txtOrden
            // 
            this.txtOrden.DisplayName = "commandBarTextBox1";
            this.txtOrden.Name = "txtOrden";
            this.txtOrden.Text = "";
            this.txtOrden.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOrden_KeyPress);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Text = "";
            this.btnFiltrar.ToolTipText = "Buscar";
            this.btnFiltrar.Click += new System.EventHandler(this.Click_Filtrar);
            // 
            // btnOrdenPrint
            // 
            this.btnOrdenPrint.DisplayName = "commandBarButton1";
            this.btnOrdenPrint.Image = global::WinTestPlayer.Properties.Resources.Barcode_iconMuliple;
            this.btnOrdenPrint.Name = "btnOrdenPrint";
            this.btnOrdenPrint.Text = "";
            this.btnOrdenPrint.ToolTipText = "Imprimir Etiquetas Equipo";
            this.btnOrdenPrint.Click += new System.EventHandler(this.btLabel_Click);
            // 
            // btnCaja
            // 
            this.btnCaja.DisplayName = "commandBarButton1";
            this.btnCaja.Image = global::WinTestPlayer.Properties.Resources.DropBoxicon32x32;
            this.btnCaja.Name = "btnCaja";
            this.btnCaja.Text = "commandBarButton1";
            this.btnCaja.ToolTipText = "Imprimir Etiqueta Caja";
            this.btnCaja.VisibleInStrip = false;
            this.btnCaja.Click += new System.EventHandler(this.btLabelBox_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisplayName = "commandBarSeparator4";
            this.commandBarSeparator4.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.DisplayName = "commandBarSeparator5";
            this.commandBarSeparator5.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel2
            // 
            this.commandBarLabel2.DisplayName = "commandBarLabel2";
            this.commandBarLabel2.Name = "commandBarLabel2";
            this.commandBarLabel2.Text = "Nº Bastidor";
            // 
            // txtBastidor
            // 
            this.txtBastidor.DisplayName = "commandBarTextBox1";
            this.txtBastidor.Name = "txtBastidor";
            this.txtBastidor.Text = "";
            this.txtBastidor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBastidor_KeyPress);
            // 
            // btnBastidorPrint
            // 
            this.btnBastidorPrint.DisplayName = "commandBarButton1";
            this.btnBastidorPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnBastidorPrint.Image")));
            this.btnBastidorPrint.Name = "btnBastidorPrint";
            this.btnBastidorPrint.Text = "";
            this.btnBastidorPrint.ToolTipText = "Imprimir Etiquetas por Bastidor";
            this.btnBastidorPrint.Click += new System.EventHandler(this.BtnPrintBastidor_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.DisplayName = "commandBarSeparator6";
            this.commandBarSeparator6.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.DisplayName = "commandBarSeparator7";
            this.commandBarSeparator7.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator8
            // 
            this.commandBarSeparator8.DisplayName = "commandBarSeparator8";
            this.commandBarSeparator8.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator8.Name = "commandBarSeparator8";
            this.commandBarSeparator8.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator9
            // 
            this.commandBarSeparator9.DisplayName = "commandBarSeparator9";
            this.commandBarSeparator9.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator9.Name = "commandBarSeparator9";
            this.commandBarSeparator9.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator10
            // 
            this.commandBarSeparator10.DisplayName = "commandBarSeparator10";
            this.commandBarSeparator10.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.commandBarSeparator10.Name = "commandBarSeparator10";
            this.commandBarSeparator10.VisibleInOverflowMenu = false;
            // 
            // commandBarLabel3
            // 
            this.commandBarLabel3.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.commandBarLabel3.DisplayName = "commandBarLabel3";
            this.commandBarLabel3.Name = "commandBarLabel3";
            this.commandBarLabel3.Text = "Impresión Manual";
            // 
            // btnLabelManual
            // 
            this.btnLabelManual.DisplayName = "commandBarButton1";
            this.btnLabelManual.Image = ((System.Drawing.Image)(resources.GetObject("btnLabelManual.Image")));
            this.btnLabelManual.Name = "btnLabelManual";
            this.btnLabelManual.Text = "Etiqueta Manual";
            this.btnLabelManual.ToolTipText = "Etiqueta Manual";
            this.btnLabelManual.Click += new System.EventHandler(this.btLabelManual_Click);
            // 
            // commandBarRowElement4
            // 
            this.commandBarRowElement4.MinSize = new System.Drawing.Size(25, 25);
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            // 
            // 
            // 
            this.commandBarStripElement2.Grip.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement2.Grip.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            // 
            // 
            // 
            this.commandBarStripElement2.OverflowButton.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement2.OverflowButton.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.commandBarStripElement2.GetChildAt(0))).TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.commandBarStripElement2.GetChildAt(0))).DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement2.GetChildAt(2))).DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarLabel4
            // 
            this.commandBarLabel4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarLabel4.DisplayName = "commandBarLabel3";
            this.commandBarLabel4.Name = "commandBarLabel4";
            this.commandBarLabel4.Text = "Nº Orden:";
            this.commandBarLabel4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarLabel5
            // 
            this.commandBarLabel5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarLabel5.DisplayName = "commandBarLabel3";
            this.commandBarLabel5.Name = "commandBarLabel5";
            this.commandBarLabel5.Text = "Nº Orden:";
            this.commandBarLabel5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarLabel6
            // 
            this.commandBarLabel6.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarLabel6.DisplayName = "commandBarLabel3";
            this.commandBarLabel6.Name = "commandBarLabel6";
            this.commandBarLabel6.Text = "Nº Orden:";
            this.commandBarLabel6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarTextBox2
            // 
            this.commandBarTextBox2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarTextBox2.DisplayName = "commandBarTextBox1";
            this.commandBarTextBox2.Name = "commandBarTextBox2";
            this.commandBarTextBox2.Text = "";
            this.commandBarTextBox2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // bsList
            // 
            this.bsList.DataSource = typeof(Dezac.Data.ViewModels.DeviceDataView);
            // 
            // bsBoxList
            // 
            this.bsBoxList.DataSource = typeof(Dezac.Data.ViewModels.BoxesView);
            // 
            // enumBinder1
            // 
            this.enumBinder1.Source = typeof(TestExecutionResult);
            gridViewComboBoxColumn1.DataSource = this.enumBinder1;
            gridViewComboBoxColumn1.DataType = typeof(TestExecutionResult);
            gridViewComboBoxColumn1.DisplayMember = "Description";
            gridViewComboBoxColumn1.FieldName = "ExecutionResult";
            gridViewComboBoxColumn1.HeaderText = "ExecutionResult";
            gridViewComboBoxColumn1.IsAutoGenerated = true;
            gridViewComboBoxColumn1.Name = "ExecutionResult";
            gridViewComboBoxColumn1.ReadOnly = true;
            gridViewComboBoxColumn1.ValueMember = "Value";
            this.enumBinder1.Target = gridViewComboBoxColumn1;
            // 
            // HistorialView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "HistorialView";
            this.Size = new System.Drawing.Size(1148, 752);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComp.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridComp)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.boxGrid.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsBoxList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion


        private System.Windows.Forms.BindingSource bsList;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder1;
        private Telerik.WinControls.UI.RadChartView chart;
        private Telerik.WinControls.UI.RadGridView gridComp;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel4;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel5;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel6;
        private Telerik.WinControls.UI.CommandBarTextBox commandBarTextBox2;
        private System.Windows.Forms.BindingSource bsBoxList;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadGridView grid;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadGridView boxGrid;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarTextBox txtOrden;
        private Telerik.WinControls.UI.CommandBarButton btnFiltrar;
        private Telerik.WinControls.UI.CommandBarButton btnOrdenPrint;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel2;
        private Telerik.WinControls.UI.CommandBarTextBox txtBastidor;
        private Telerik.WinControls.UI.CommandBarButton btnBastidorPrint;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator8;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator9;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator10;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel3;
        private Telerik.WinControls.UI.CommandBarButton btnLabelManual;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement4;
        private Telerik.WinControls.UI.CommandBarButton btnCaja;
    }
}
