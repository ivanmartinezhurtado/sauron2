﻿using System;
using System.IO;
using System.Windows.Forms;

namespace WinTaskRunner.Design.Services
{
    public interface IDocumentService
    {
        event EventHandler<IDocument> ActiveDocumentChanged;

        IDocumentManager CurrentDocumentManager { get; }
        IDocument CurrentDocument { get; }
        IDocument AddNew(string type);
        IDocument Open(string type);
        IDocument Open(string type, Stream stream, string nameDoc);
        void Save();
        void AddDocument(Control control, string title = null, int width = 0, int height = 0);
        void AddDocument(Control control, IDocument document, int width = 0, int height = 0);
    }
}
