﻿using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(AddVariablesActionDescription))]
    public class AddVariablesAction : ActionBase
    {
        [Description("Instancia/ hilo (Equipo en paralelo) de los datos a asignar")]
        [Category("3. Configuration")]
        public int? Instance { get; set; }

        [Description("Lista de los datos a crear para utilizar en el test")]
        [Category("4. Results")]
        public new TextValuePair[] DataOutput { get; set; }
        [Description("Lista de los datos compartidos a crear para utilizar en el test")]
        [Category("4. Results")]
        public new TextValuePair[] SharedDataOutput { get; set; }

        public override void Execute()
        {
            int instance;
            var NumInstance = SequenceContext.Current.NumInstance;
            instance = !Instance.HasValue ? NumInstance : Instance.Value;

            if (NumInstance == instance)
            {
                if (DataOutput != null)
                {
                    foreach (var item in DataOutput)
                    {
                        var value = VariablesTools.ResolveValue(item.Value);
                        Logger.InfoFormat("Instance {0} Add Variable {1} = {2}", NumInstance, item.Key, value);
                        Context.ResultList.Add(string.Format("Instance {0} Add Variable {1} = {2}", NumInstance, item.Key, value));
                        SetVariable(item.Key, value);
                    }
                }
                if (SharedDataOutput != null)
                {
                    foreach (var item in SharedDataOutput)
                    {
                        var value = VariablesTools.ResolveValue(item.Value);
                        Logger.InfoFormat("Instance {0} Add SharedVariable {1} = {2}", NumInstance, item.Key, value);
                        Context.ResultList.Add(string.Format("Instance {0} Add SharedVariable {1} = {2}", NumInstance, item.Key, value));
                        SetSharedVariable(item.Key, value);
                    }
                }
            }
        }

    }


    public class AddVariablesActionDescription : ActionDescription
    {
        public override string Description { get { return "Este action sirve para crear las variables del valor que puede venir de un valor constante, del contexto del test, de TestInfo o de la base se datos de Dezac para reutilizar su valor";  } }
        public override string Name { get { return "AddVariables"; }}
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
