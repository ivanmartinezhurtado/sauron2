﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace Instruments.PowerSource
{
    public abstract class PowerSourceBase<T>
    {
        protected readonly ILog _logger = LogManager.GetLogger(typeof(T));

        public class PowerSourceStatus
        {
            public bool IsBusy { get; set; }
            public bool HasWarning { get; set; }
            public bool HasError { get; set; }
            public bool IsOK { get { return !IsBusy && !HasWarning && !HasError; } }
            public string ErrorDescription { get; set; }
        }

        internal PowerSourceStatus Status { get; set; }

        public double Tolerance { get; set; }

        public double VoltageOffMin { get; set; }

        public byte RetryStabilisation { get; set; }

        public int TimeOutStabilitation { get; set; }

        public bool IsOn { get; set; }

        public enum Commands
        {
            SET_VOLTAGE,
            SET_CURRENT,
            SET_CURRENT_RANGE,
            SET_VOLTAGE_LIMIT,
            SET_PF,
            SET_FREQ,
            SET_DESFASE,
            APPLY_CONFIG,

            APPLY_OFF,
            RESET,

            READ_VOLTAGE,
            READ_CURRENT,
            READ_POWER,
            READ_POWERFACTOR,
            READ_FREQ,
            READ_DESFASE,
            READ_STATUS,
            READ_IDN,

            USER_DEFINED_COMMANDS = 9999
        };

        public PowerSourceBase(T t, ILog logger)
        {
            Status = new PowerSourceStatus();
            Tolerance = 0.5;
            TimeOutStabilitation = 30000;
            VoltageOffMin = 20;
            RetryStabilisation = 3;
            Logger = logger ?? _logger;
        }

        protected ILog Logger { get; set; }

        protected IProtocol protocol { get; set; }

        protected MessageTransport transport { get; set; }

        public enum statusStabilization
        {
            OK,
            INTERNAL_ERROR,
            STABILISATION_ERROR
        }   

        protected bool SendCommand<T>(Command<T> command)
        {
            try
            {
                //Logger.DebugFormat("Command {0}", command);
                ExecuteCommand(command, 1);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                var sauronExc = ex as SauronException;
                if (sauronExc != null)
                    throw;

                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO(ex.Message).Throw();
                return false;
            }
        }

        private void ExecuteCommand<T>(Command<T> command, int numRetries)
        {
            bool error = false;
            do
            {
                try
                {
                    error = false;
                    InternalSendCommand(command);
                }
                catch (Exception ex)
                {
                    error = true;
                    numRetries--;

                    Logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                    if (numRetries <= 0 || ex is NotSupportedException)
                        throw;
                }
            } while (error);
        }

        public string GetInstrumentVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(InstrumentsVersionAttribute), true).FirstOrDefault() as InstrumentsVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }

        //************************************************

        protected bool internalApplyAndWaitStabilisation(bool forceApply = false, Func<bool> validator = null)
        {
            var status = statusStabilization.STABILISATION_ERROR;

            for (byte i = 0; i < RetryStabilisation; i++)
            {
                try
                {
                    ApplyPresetsBase(forceApply);
                    status = WaitStabilisation(validator);
                }
                catch (Exception ex)
                {
                    Logger.InfoFormat("EXCEPCION BY PowerSource in ApplyAndWaitStabilisation by {0}", ex.Message);
                    throw;
                }

                if (status == statusStabilization.OK)
                    break;
            }

            if (status == statusStabilization.STABILISATION_ERROR)
                PostActionWhenStabilizationError();

            if (status == statusStabilization.INTERNAL_ERROR)
                PostActionWhenInternalError();

            return true;
        }

        protected virtual void  PostActionWhenStabilizationError()
        {
            TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE("AL ESTABILIZAR LA FUENTE").Throw();
        }

        protected virtual void PostActionWhenInternalError()
        {
            TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE($"ERROR INTERNO FUENTE {Status.ErrorDescription}").Throw();
        }

        private statusStabilization WaitStabilisation(Func<bool> validator = null)
        {
            DateTime start = DateTime.Now;
            bool valid = false;
            var statusError = statusStabilization.OK;

            Logger.InfoFormat("Begin check stabilisation...");

            while (!valid && (DateTime.Now - start).TotalMilliseconds < TimeOutStabilitation)
            {
                var status = ReadStatus();

                if (status.Value.IsOK)
                {
                   
                    valid = ValidateEstabilization(Tolerance);
                    if (valid && validator != null)
                        valid = validator();
                }
                else if (status.Value.HasError)
                {
                    
                    Logger.InfoFormat("POWER SOURCE INTERNAL ERROR en {0} ms.", (DateTime.Now - start).TotalMilliseconds);
                    return statusStabilization.INTERNAL_ERROR;
                }

                Thread.Sleep(500);
            }

            Logger.InfoFormat("End check stabilisation, result: {0}, {1} ms", valid ? "OK" : "ERROR", (DateTime.Now - start).TotalMilliseconds);

            if (!valid)
                statusError = statusStabilization.STABILISATION_ERROR;

            return statusError;
        }

        protected Result<PowerSourceStatus> ReadStatus(bool throwException = true)
        {
            var cmd = new Command<Commands>(Commands.READ_STATUS, throwException);
            if (SendCommand(cmd))
                return new Result<PowerSourceStatus>(Status);

            return new Result<PowerSourceStatus>(cmd.Exception);
        }

        public void ApplyOff()
        {
            var cmd = new Command<Commands>(Commands.APPLY_OFF, true);
            SendCommand(cmd);
            IsOn = false;
            ResetPresets();
        }

        public void ApplyOffAndWaitStabilisation()
        {
            ApplyOff();
            WaitStabilisation();
            ResetPresets();
        }

        public abstract void Reset();

        protected abstract void ApplyPresetsBase(bool forceApply);

        protected abstract void InternalSendCommand<T>(Command<T> command);

        protected abstract bool ValidateEstabilization(double tolerance);

        protected abstract void ResetPresets();
    }
}
