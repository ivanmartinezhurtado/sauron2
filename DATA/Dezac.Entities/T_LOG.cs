namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_LOG
    {
        public int NUMLOG { get; set; }
        public System.DateTime FECHA { get; set; }
        public string IDTIPOEVENTO { get; set; }
        public Nullable<int> IDTORRE { get; set; }
        public Nullable<int> IDDISPOSITIVO { get; set; }
        public Nullable<int> IDSOFTWARE { get; set; }
        public Nullable<int> SESION { get; set; }
        public string IDOPERARIO { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
    
        public virtual BIEN BIEN { get; set; }
        public virtual BIEN BIEN1 { get; set; }
        public virtual BIEN BIEN2 { get; set; }
        public virtual MATRICULA MATRICULA { get; set; }
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual T_TIPOEVENTO T_TIPOEVENTO { get; set; }
    }
}
