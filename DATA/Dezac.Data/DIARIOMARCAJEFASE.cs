namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.DIARIOMARCAJEFASE")]
    public partial class DIARIOMARCAJEFASE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DIARIOMARCAJEFASE()
        {
            DIARIOMARCAJEFASE1 = new HashSet<DIARIOMARCAJEFASE>();
            T_TESTFASE = new HashSet<T_TESTFASE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMMARCAJE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public int? NUMHOJA { get; set; }

        public DateTime FECHAREGISTRO { get; set; }

        public DateTime FECHA { get; set; }

        [StringLength(1)]
        public string TIPO { get; set; }

        [StringLength(10)]
        public string IDFASE { get; set; }

        [Required]
        [StringLength(1)]
        public string INICIOFIN { get; set; }

        public int? NUMMARCAJEINICIO { get; set; }

        public int? CANTIDAD { get; set; }

        public int? NUMMARCAJEACTIVIDAD { get; set; }

        [Required]
        [StringLength(1)]
        public string HORAINCORRECTA { get; set; }

        [StringLength(50)]
        public string NOTASHORAINCORRECTA { get; set; }

        [Required]
        [StringLength(1)]
        public string INSERCIONPAREJA { get; set; }

        public DateTime? TIME_ID { get; set; }

        [StringLength(200)]
        public string NOTAS { get; set; }

        [StringLength(1)]
        public string INICIOSESION { get; set; }

        [StringLength(50)]
        public string HOST { get; set; }

        public virtual DIARIOMARCAJE DIARIOMARCAJE { get; set; }

        public virtual HOJAMARCAJEFASE HOJAMARCAJEFASE { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE1 { get; set; }

        public virtual DIARIOMARCAJEFASE DIARIOMARCAJEFASE2 { get; set; }

        public virtual FASE FASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }
    }
}
