﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(PteDescription))]
    public partial class UC_PTE : UserControl
    {

        private MyComputer computer = new MyComputer();

        private double Voltage;
        private bool StatusBooster=true;

        private PTE pte;

        public Type InstrumentType
        {
            get
            {
                return typeof(PTE);
            }
        }


        public PTE Instrument
        {
            get
            {
                if (pte == null)
                    pte = new PTE();

                return pte;
            }
            set
            {
                if (pte != null)
                {
                    pte.Dispose();
                    pte = null;
                }
                pte = value;
            }
        }

        public UC_PTE()
        {
            InitializeComponent();          

            cmb_Port.Items.AddRange(computer.SerialPortNames.ToArray());
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM1");
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            ValidatePortCom();

            if (ValidatePresetsPTE100V() == true)
                try
                {
                    Instrument.SelecctionSource = PTE.SourceType.PTE_100V;

                    ToggleButtonsState((Button)sender);

                    var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyPresets(Voltage, 0, 0); }));

                    LifeMethod("Encendiendo", task);

                    ToggleButtonsState((Button)sender);

                }
                catch (Exception ex)
                {
                    Logger.Error("Error al encender el PTE", ex);
                }
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            ValidatePortCom();

            try
            {
                ToggleButtonsState((Button)sender);

                Instrument.SelecctionSource = PTE.SourceType.PTE_100V;

                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyOff(); }));

                LifeMethod("Apagando", task);

                ToggleButtonsState((Button)sender);

            }
            catch (Exception ex)
            {
                Logger.Error("Error al apagar la PTE", ex);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ValidatePortCom();

            try
            {
                ToggleButtonsState((Button)sender);

                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.Reset(); }));

                LifeMethod("Reset", task);

                ToggleButtonsState((Button)sender);

            }
            catch (Exception ex)
            {
                Logger.Error("Error al resetear la PTE", ex);
            }
        }

        private bool ValidatePresetsPTE100V()
        {
            try
            {
                Voltage = GetAsDouble(txtV1);

                if (Voltage > 500)
                {
                    errorProvider1.SetError(txtV1, "Voltage must lower 500V");
                    return false;
                }
                Instrument.InitConfig();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("Error al validar presets a la PTE", ex);
            }
            return true;
        }

        private void ValidatePortCom()
        {
            try
            {
                errorProvider1.Clear();
                if (cmb_Port.SelectedItem == null)
                    errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
                else
                    Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));

            }
            catch (Exception ex)
            {
                Logger.Error("Error al validar Validate PortCom emn la PTE", ex);
            }
        }

        private void btnResetTimer_Click(object sender, EventArgs e)
        {
            Instrument.ResetTimer();
        }

        private void btnResetMonitor_Click(object sender, EventArgs e)
        {
            Instrument.ResetMonitor();
        }

        private void btnResetTrip_Click(object sender, EventArgs e)
        {
            Instrument.ResetTrip();
        }

        private void btBooster_Click(object sender, EventArgs e)
        {
            Instrument.ActiveBooster(StatusBooster);
            StatusBooster = !StatusBooster;
        }                  

        private void ToggleButtonsState(Button btn)
        {
            if (btn.Equals(btnOnPTE100V))
                btnOnPTE100V.Enabled = !btnOnPTE100V.Enabled;
            if (btn.Equals(btnOffPTE100V))
                btnOffPTE100V.Enabled = !btnOffPTE100V.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private double GetAsDouble(TextBox txt)
        {
            if (string.IsNullOrEmpty(txt.Text))
                return 0;

            return Convert.ToDouble(txt.Text);
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if(Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }
    public class PteDescription : ControlDescription
    {
        public override string Name { get { return "PTE"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.PowerSource; } }
        public override Image IconBig { get { return Properties.Resources.PowerSourcePTE; } }
        public override Type SubcomponentType { get { return typeof(PTE); } }
    }
}
