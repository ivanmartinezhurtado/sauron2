﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.ComponentModel;

namespace WinTestPlayer.Controls.Expanders
{
    class SettingsButton : LightVisualElement
    {
        static SettingsButton()
        {
            ItemStateManagerFactoryRegistry.AddStateManagerFactory(new SettingsButtonStateManager(), typeof(SettingsButton));
        }

        public static RadProperty IsSelectedProperty = RadProperty.Register(
         "Selected",
         typeof(bool),
         typeof(ChevronButton),
         new RadElementPropertyMetadata(true));

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool Selected
        {
            get
            {
                return (bool)this.GetValue(IsSelectedProperty);
            }
            set
            {
                this.SetValue(IsSelectedProperty, value);
            }
        }

        
    }
}
