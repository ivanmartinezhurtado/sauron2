﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;
using TaskRunner.Model;
using WinTaskRunner.Design;

namespace WinTaskRunner.Services
{
    public class SequenceDocument : IDocument
    {
        public event CancelEventHandler BeforeSave;

        public static string DOC_TYPE = ".seq";       // Podria ser un mimetype

        public SequenceModel Model { get; private set; }
        public string FileName { get; set; }
        public int RunTestNumber { get; set; }

        public Dictionary<string, object> Properties { get; private set; }

        public SequenceDocument()
        {
            Properties = new Dictionary<string, object>();

            AddNew();
        }

        public SequenceModel AddNew()
        {
            Model = SequenceModel.CreateNew();
            FileName = null;

            return Model;
        }

        public void Load(string fileName)
        {
            Model = SequenceModel.LoadFile(fileName);
            FileName = fileName;
        }

        public void Load(Stream stream)
        {
            byte[] data = stream is MemoryStream ? ((MemoryStream)stream).ToArray() : null;

            if (data == null)
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    data = ms.ToArray();
                }

            string content = UTF8Encoding.UTF8.GetString(data);

            Model = SequenceModel.Load(content);
        }

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(FileName))
                    return "Untitled";

                return Path.GetFileNameWithoutExtension(FileName);
            }
            set
            {
                FileName = value;
            }
        }

        public bool IsNew
        {
            get { return FileName == null; }
        }

        private bool ValidateBeforeSave()
        {
            if (BeforeSave == null)
                return true;

            var e = new CancelEventArgs();
            BeforeSave(this, e);

            return !e.Cancel;
        }

        public void Save()
        {
            if (!ValidateBeforeSave())
                return;

            Model.Save(FileName);
        }

        public void Save(string fileName)
        {
            if (!ValidateBeforeSave())
                return;

            FileName = fileName;
            Model.Save(FileName);
        }

        public void Save(Stream stream, Encoding encoding = null)
        {
            if (!ValidateBeforeSave())
                return;

            Model.Save(stream, encoding);
        }

        public string DocType
        {
            get { return DOC_TYPE; }
        }
    }
}
