namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTCONFIGFILEPRODUCTO")]
    public partial class T_TESTCONFIGFILEPRODUCTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTCFP { get; set; }

        public int NUMTCF { get; set; }

        public int NUMPRODUCTO { get; set; }

        public int? VERSION { get; set; }

        public DateTime FECHAALTA { get; set; }

        public virtual T_TESTCONFIGFILE T_TESTCONFIGFILE { get; set; }
    }
}
