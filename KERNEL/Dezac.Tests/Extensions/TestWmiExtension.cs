﻿using System.Management;

namespace Dezac.Tests.Extensions
{

    public static class TestWmiExtension
    {
        public static string WMISerialPortInitialize(this TestBase testBase, string SerialPortToFind)
        {
            string comportInfo = string.Empty;

            testBase.SamplerWithCancel((p) =>
            {
                var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_SerialPort");
                foreach (ManagementObject queryObj in searcher.Get())
                    if (queryObj["Caption"].ToString().ToUpper().Contains(SerialPortToFind.ToUpper()))
                    {
                        testBase.Logger.InfoFormat("serial port : {0}", queryObj["Caption"]);
                        var comPortFind = queryObj["Caption"].ToString().Replace(SerialPortToFind, "");
                        var posInit = (comPortFind.IndexOf("(COM"));
                        if (posInit != -1)
                        {
                            var stringSearch = comPortFind.Substring(posInit);
                            comportInfo = stringSearch.Replace(")", "").Replace("(", "");
                            break;
                        }
                    }

                if (string.IsNullOrEmpty(comportInfo))
                {
                    searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity");
                    foreach (ManagementObject queryObj in searcher.Get())
                        if (queryObj["Caption"] != null)
                            if (queryObj["Caption"].ToString().ToUpper().Contains(SerialPortToFind.ToUpper()))
                            {
                                testBase.Logger.InfoFormat("serial port : {0}", queryObj["Caption"]);
                                var comPortFind = queryObj["Caption"].ToString().Replace(SerialPortToFind, "");
                                var posInit = (comPortFind.IndexOf("(COM"));
                                if (posInit != -1)
                                {
                                    var stringSearch = comPortFind.Substring(posInit);
                                    comportInfo = stringSearch.Replace(")", "").Replace("(", "");
                                    break;
                                }
                            }
                }

                if (string.IsNullOrEmpty(comportInfo))
                    testBase.Error().PROCESO.ACTION_EXECUTE.DISPOSITIVO_NO_ENCONTRADO("SERIAL PORT " + SerialPortToFind).Throw();

                return true;

            }, "", 3, 3000, 1000, false, false);

            return comportInfo;
        }
    }
}