﻿namespace WinTaskRunner.Design
{
    public class ServiceContainer
    {
        public static IServiceResolver Default { get; set; }

        public static T Resolve<T>()
        {
            return Default.Resolve<T>();
        }
    }
}
