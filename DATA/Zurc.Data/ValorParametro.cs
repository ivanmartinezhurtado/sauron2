namespace Zurc.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ValorParametro")]
    public partial class ValorParametro
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("GrupoParametrizacion")]
        public int GrupoId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Parametro")]
        public int ParametroId { get; set; }

        [Required]
        [StringLength(200)]
        public string Valor { get; set; }

        [StringLength(200)]
        public string ValorInicio { get; set; }

        public DateTime? FechaAlta { get; set; }

        [StringLength(10)]
        public string CategoriaId { get; set; }

        [JsonIgnore]
        public GrupoParametrizacion GrupoParametrizacion { get; set; }

        public Parametro Parametro { get; set; }

        public List<ValorParametroProducto> ValoresParametroProducto { get; set; }

        /// <summary>
        /// Propiedades no mapeadas
        /// </summary>
        /// 
        private string valorFamilia { get; set; }

        [NotMapped]
        public bool DeProducto { get; set; }

        [NotMapped]
        [JsonIgnore]
        public string ValorProducto
        {
            get { return DeProducto ? Valor : null; }
            set
            {
                DeProducto = true;
                valorFamilia = value;
                Valor = value;
            }
        }

        [NotMapped]
        public string ValorFamilia
        {
            get { return DeProducto ? valorFamilia : Valor; }
        }
    }
}
