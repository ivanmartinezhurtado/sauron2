﻿using Dezac.Core.Utility;
using log4net;
using System;
using System.Threading;

namespace Instruments.IO
{
    public abstract class IOBase : IDisposable
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IOBase));

        public bool ThrowException { get; set; }

        public bool LogEnabled { get; set; }

        public InputList<bool> DI { get; protected set; }
        public DigitalOutputList DO { get; protected set; }

        public IOBase(ILog logger)
        {
            Logger = logger ?? _logger;
            LogEnabled = true;
        }

        protected ILog Logger { get; set; }

        internal virtual void ApplyConfig(IIOBaseConfiguration config)
        {
            DI = new InputList<bool>(this, config.NumDI);
            DO = new DigitalOutputList(this, config.NumDO);

            InternalApplyConfig(config);
        }

        protected virtual void InternalApplyConfig(IIOBaseConfiguration config)
        {
        }

        internal Result<T> Read<T>(IOType type, int num)
        {
            lock (this)
            {
                T value = InternalRead<T>(type, num);

                if (LogEnabled)
                {
                    string valueString = value.ToString() == "False" ? "OFF" : "ON";
                    Logger.InfoFormat($"IN {num} => {valueString}");
                }

                return new Result<T>(value);
            }
        }

        internal Result<bool> Write<T>(int num, T value, bool? throwException = null)
        {
            lock (this)
            {
                InternalWrite(num, value);

                if (LogEnabled)
                {
                    string valueString = value.ToString() == "False" ? "OFF" : "ON";
                    Logger.InfoFormat($"OUT {num} => {valueString}");
                }

                return new Result<bool>(true);
            }
        }

        protected abstract T InternalRead<T>(IOType type, int num);
        protected abstract void InternalWrite<T>(int num, T value);

        public class InputList<T>
        {
            private IOBase ioBase;

            internal InputList(IOBase ioBase, int num)
            {
                this.ioBase = ioBase;
                this.Count = num;
            }

            public int Count { get; private set; }

            public virtual T this[int index]
            {
                get
                {
                    return Read(index).Value;
                }
            }

            public Result<T> Read(int num)
            {
                return ioBase.Read<T>(IOType.Input, num).Value;
            }
        }

        public class OutputList<T>
        {
            private IOBase ioBase;

            protected OutputList(IOBase ioBase, int num)
            {
                this.ioBase = ioBase;
                this.Count = num;
            }

            public int Count { get; private set; }

            public T this[int index]
            {
                get
                {
                    return Read(index).Value;
                }
                set
                {
                    Write(index, value);
                }
            }

            public Result<T> Read(int num)
            {
                return ioBase.Read<T>(IOType.Output, num).Value;
            }

            public Result<bool> Write(int num, T value, bool? throwException = null)
            {
                return ioBase.Write<T>(num, value, throwException);
            }
        }

        public class DigitalOutputList : OutputList<bool>, IIOOutput
        {
            internal DigitalOutputList(IOBase ioBase, int num) 
                : base(ioBase, num)
            {
            }

            public Result<bool> On(int num, bool? throwException = null)
            {
                return Write(num, true, throwException);
            }

            public bool On(params int[] nums)
            {
                foreach (int n in nums)
                {
                    if (!Write(n, true).Value)
                        return false;

                    Thread.Sleep(10);
                }
                return true;
            }

            public bool OnRange(int start, int end)
            {
                for (int n = start; n <= end; n++)
                {
                    if (!Write(n, true).Value)
                        return false;

                    Thread.Sleep(10);
                }

                return true;
            }

            public Result<bool> Off(int num, bool? throwException = null)
            {
                return Write(num, false);
            }

            public bool Off(params int[] nums)
            {
                foreach (int n in nums)
                {
                    if (!Write(n, false).Value)
                        return false;

                    Thread.Sleep(10);
                }
                return true;
            }

            public bool OffRange(int start, int end)
            {
                for (int n = start; n <= end; n++)
                {
                    if (!Write(n, false).Value)
                        return false;

                    Thread.Sleep(10);
                }
                return true;
            }

            public bool PulseOn(int milliseconds, params int[] num)
            {
                if (On(num))
                {
                    Thread.Sleep(milliseconds);
                    return Off(num);
                }

                return false;
            }

            public bool PulseOff(int milliseconds, params int[] num)
            {
                if (Off(num))
                {
                    Thread.Sleep(milliseconds);
                    return On(num);
                }

                return false;
            }

            public bool PulseOnWait(Time pulseTime, Time waitTime,  params int[] num)
            {
                if (On(num))
                {
                    Thread.Sleep(pulseTime.ms);
                    return OffWait(waitTime.ms, num);
                }

                return false;
            }

            public bool PulseOffWait(Time pulseTime, Time waitTime,  params int[] num)
            {
                if (Off(num))
                {
                    Thread.Sleep(pulseTime.ms);
                    return OnWait(waitTime.ms, num);
                }

                return false;
            }

            public bool OnWait(int milliseconds, params int[] num)
            {
                if (On(num))
                {
                    Thread.Sleep(milliseconds);
                    return true;
                }

                return false;
            }

            public bool OffWait(int milliseconds, params int[] num)
            {
                if (Off(num))
                {
                    Thread.Sleep(milliseconds);
                    return true;
                }

                return false;
            }
        }

        public virtual void Dispose()
        {
        }

        public class Time
        {
            private int time;
            public int ms { get { return time; } set { time = value; } }
            public double s { get { return time / 1000.0; } set { time = (int)value * 1000; }  }

            public Time(int _ms)
            {
                ms = _ms;
            }
        }
    }
}
