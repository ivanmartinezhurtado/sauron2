﻿using System;

namespace Dezac.Tests
{
    public enum TestPointLocationEnum
    {
        Init,
        Main,
        End
    };

    public class TestPointAttribute : Attribute
    {
        public bool Required { get; set; }
        public TestPointLocationEnum Location { get; set; }

        public string Depends { get; set; }

        public TestPointAttribute()
        {
            Location = TestPointLocationEnum.Main;
        }
    }
}
