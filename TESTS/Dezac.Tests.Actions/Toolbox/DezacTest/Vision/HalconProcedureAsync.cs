﻿using Dezac.Tests.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(HalconProcedureAsyncDescription))]
    public class HalconProcedureAsync : ActionBase
    {

        [Category("3. Configuration")]
        [Description("Lista de nombres de test de visión artificial que se quieren procesar, si está vacío se procesan todos")]
        public string[] ListTestName { get; set; }

        public override void Execute()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                WaitAllTestsAndRunOnlyOnce("_HALCON_PROCEDURE_", InternalExecute);
            else
                InternalExecute();
        }

        private void InternalExecute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            if (ListTestName != null)
            {
                foreach (string testName in ListTestName)
                {
                    var testInputs = GetVariable<TestHalconExtension.ProcedureInputs>(String.Format("HALCON_PROCEDURE_{0}", testName));
                    var halconResultTupla = testBase.TestHalconProcedureAsync(testInputs);

                    if (halconResultTupla.Item1 != TestHalconExtension.ResultImageState.OK)
                        ExceptionMessage(testInputs.TypeProcedure, halconResultTupla.Item2.ToString(), testInputs.HalconTestName);

                    var cancel = SequenceContext.Current.CancellationToken;
                    if (cancel.IsCancellationRequested)
                        Error().SOFTWARE.SECUENCIA_TEST.SAMPLER_WITH_CANCEL_MAL_IMPLEMENTADO("Sampler Canceled").Throw();

                    testBase.RemoveVariable(String.Format("HALCON_PROCEDURE_{0}", testName));
                } 
            }
            else
            {
                var dictionaryTestName = testBase.Variables.VariableList.Where((p) => p.Key.Contains("HALCON_PROCEDURE")).ToDictionary(o => o.Key, o => (TestHalconExtension.ProcedureInputs)o.Value);

                foreach (KeyValuePair<string, TestHalconExtension.ProcedureInputs> testName in dictionaryTestName)
                {
                    var halconResultTupla = testBase.TestHalconProcedureAsync(testName.Value);
                    if (halconResultTupla.Item1 != TestHalconExtension.ResultImageState.OK)
                        ExceptionMessage(testName.Value.TypeProcedure, halconResultTupla.Item2.ToString(), testName.Value.HalconTestName);

                    testBase.RemoveVariable(testName.Key.ToString());
                }
            }           
        }

        private void ExceptionMessage(TestHalconExtension.TypeProcedure type, string halconError, string image)
        {
            var errorMessage = "";
            switch (type)
            {
                case TestHalconExtension.TypeProcedure.FindLeds:
                    errorMessage = string.Format("Error no se detecta el LED {0} en la imagen {1}", halconError, image);
                    break;
                case TestHalconExtension.TypeProcedure.ReadBarCode:
                    errorMessage = string.Format("Error: No se ha podido leer el codigo de barras", image);
                    break;
                default:
                    errorMessage = string.Format("{0} en la imagen {1}", halconError, image);
                    break;
            }
            throw new Exception(errorMessage);
        }
    }

    public class HalconProcedureAsyncDescription : ActionDescription
    {
        public override string Name { get { return "HalconProcedureAsync"; } }
        public override string Description { get { return "Este action sirve para coger una lista de nombres de test de visión artificial hecha anteriormente para evaluarla"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
