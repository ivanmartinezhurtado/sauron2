﻿using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(HalconCaptureAsyncDescription))]
    public class HalconCaptureAsync : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del test de visión artificial")]
        public ActionVariable TestName { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Nombre para guardar la imagen")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Nombre del equipo de test")]
        public ActionVariable DeviceName { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Número de capturas (reintentos)")]
        [DefaultValue((byte)1)]
        public byte Captures { get; set; }

        [Category("3. Configuration")]
        [Description("Action antes de hacer las capturas")]
        public RunMethodAction Action { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del parámetro dentro de la base de datos (Si es diferente a DataInput))")]
        public ActionVariable ParameterName { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Tipo de procedimiento que el action debe transformar")]
        [DefaultValue(TestHalconExtension.TypeProcedure.FindLeds)]
        public TestHalconExtension.TypeProcedure Type { get; set; }

        [Category("3.1 Camera Selection")]
        [Description("Número de serie de la cámara (no necesario si tiene DataInput)")]
        public ActionVariable SerialNumber { get; set; } = new ActionVariable();

        [Category("3.2 Crop and Resize image")]
        [Description("Fila 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        public ActionVariable Row1 { get; set; } = new ActionVariable();

        [Category("3.2 Crop and Resize image")]
        [Description("Columna 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        public ActionVariable Column1 { get; set; } = new ActionVariable();

        [Category("3.2 Crop and Resize image")]
        [Description("Fila 2 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        public ActionVariable Row2 { get; set; } = new ActionVariable();

        [Category("3.2 Crop and Resize image")]
        [Description("Columna 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        public ActionVariable Column2 { get; set; } = new ActionVariable();

        [Category("3.2 Crop and Resize image")]
        [Description("Factor de redimensión para una imagen con menos resolución (1 significa sin reducción de resolución)")]
        public ActionVariable ResizeFactor { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            if (ParameterName == null)
                ParameterName = String.Empty;

            testBase.TestHalconCaptureAsync(
                Type,
                SerialNumber.Value.ToString(),
                TestName.Value.ToString(),
                DataInput.Value.ToString(),
                DeviceName.Value.ToString(), 
                Captures,
                (s) => Action?.Execute(), ParameterName.Value.ToString(),
                Convert.ToInt32(Row1.Value.ToString().Replace(".", ",")),
                Convert.ToInt32(Column1.Value.ToString().Replace(".", ",")),
                Convert.ToInt32(Row2.Value.ToString().Replace(".", ",")),
                Convert.ToInt32(Column2.Value.ToString().Replace(".", ",")),
                Convert.ToDouble(ResizeFactor.Value.ToString().Replace(".", ",")));
        }
    }

    public class HalconCaptureAsyncDescription : ActionDescription
    {
        public override string Name { get { return "HalconCaptureAsync"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override string Description { get { return "Este action sirve para hacer una imagen de un equipo desde la secuencia asíncronamente, eso quiere decir que se hace aparte del test"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
        public override string Dependencies { get { return "InitTestBase"; } }

    }
}
