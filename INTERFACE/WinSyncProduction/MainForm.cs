﻿using AssemblyResolve;
using Dezac.Data;
using Dezac.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace WinSyncProduction
{
    public partial class MainForm : Form
    {
        private string Modo = "SYNC";
        private string DevicePC = "";
        private BIEN torre = null;
        private bool FailedLoadApp = false;

        private List<FileAssemblyItem> listFileUpdate;

        public List<FileAssemblyItem> ListFileUpdate
        {
            get
            {
                if (listFileUpdate == null)
                    listFileUpdate = new List<FileAssemblyItem>();

                return listFileUpdate;
            }
        }

        public string ApplicationSelected { get; set; }

        public string ApplicationNotSelected
        {
            get
            {
                return ApplicationSelected.Trim().ToUpper() == "WINTASKRUNNER" ? "WinTestPlayer" : "WinTaskRunner";
            }
        }

        //************************************************************

        public MainForm(string argument)
        {
            InitializeComponent();
            this.Text = string.Format("WinSyncProduction  v.{0}", Application.ProductVersion);
            ApplicationSelected = argument;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    if (!Directory.Exists(ConfigurationManager.AppSettings["ReleasePath"]))
                    {
                        MessageBox.Show($@"Se ejecutara la aplicación sin buscar actualizaciones. No se puede accede a {ConfigurationManager.AppSettings["ReleasePath"]}", "ERROR DE RED", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        FailedLoadApp = true;
                    }
                }
                else
                {
                    MessageBox.Show($@"Se ejecutara la aplicación sin buscar actualizaciones. No hay red o no esta disponible", "ERROR DE RED", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    FailedLoadApp = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($@"Se ejecutara la aplicación sin buscar actualizaciones. {ex.Message} en {ex.StackTrace}", "ERROR DE RED", MessageBoxButtons.OK, MessageBoxIcon.Error);
                FailedLoadApp = true;
            }

            if (!FailedLoadApp)
            {
                DevicePC = Dns.GetHostName();

                using (var svc = new DezacService())
                {
                    torre = svc.GetTorreByPC(DevicePC);
                    if (torre != null)
                        Modo = torre.IDENTIFICACIONTEST == 1 ? "LOCAL" : "SYNC";
                }
            }

            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                if (FailedLoadApp)
                {
                    var pathApplicationLocal = string.Format("C:\\SAURON_SYSTEM\\{0}\\{0}.exe", ApplicationSelected);

                    var Project = Tools.LoadProject(ApplicationSelected, Application.StartupPath);
                    if (Project != null)
                        pathApplicationLocal = Path.Combine(Project.TargetPath, Project.ApplicationFile.NameAssembly);

                    ExecuteProcess(pathApplicationLocal);
                }
                else if (ApplicationSelected.Trim().ToUpper() == "TSD2013")
                {
                    CopyFiles(ConfigurationManager.AppSettings["ReleaseTSD2013Path"], "C:\\SAURON_SYSTEM\\TSD2013");
                    ExecuteProcess("C:\\SAURON_SYSTEM\\TSD2013\\TSDEZAC.exe");
                }
                else if (!string.IsNullOrEmpty(Modo) && Modo.Trim().ToUpper() == "LOCAL")
                {
                    var body = new StringBuilder();
                    var path = string.Format("C:\\Users\\{0}\\Documents\\GitLab\\SAURON\\INTERFACE\\{1}\\bin\\Debug", Environment.UserName, ApplicationSelected);
                    var pathApplicationLocal = Path.Combine(path, ApplicationSelected + ".exe");

                    body.AppendLine(string.Format("CLIENTE SYNCRONIZACION LOCAL -> {0} ", ApplicationSelected));
                    body.AppendLine("--------------------------------------------------");
                    body.AppendLine("");

                    if (torre != null)
                        body.AppendLine(string.Format("TORRE: {0}   UBICACION: {1}", torre.DESCRIPCION, torre.UBICACION));

                    body.AppendLine(string.Format("EN EL PC: {0}", DevicePC));
                    body.AppendLine("--------------------------------------------------");
                    SendMail.EnviarCorreo("CLIENTE DE SYNCRONIZACION", body.ToString());

                    ExecuteProcess(pathApplicationLocal);
                }
                else
                {
                    var project = LoadProjects(ApplicationSelected);
                    var pathApplication = SyncFiles(project);

                    ExecuteProcess(pathApplication);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(@" AVISAR A IDP SOFTWARE -> Excepción al ejecutar la aplicación por {0}", ex.Message), "ERROR NO CONTROLADO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Close();
            }
        }

        private ProjectItem LoadProjects(string application)
        {
            try
            {          
                var Project = Tools.LoadProject(application, Application.StartupPath);
                if (Project == null)
                {
                    MessageBox.Show(string.Format("Error no se encuentra el projecto {0} en el fichero app.json", application), "ERROR AL CARGAR PATH PROYECTO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }

                var fileJson = Path.Combine(Project.SourcePath + "\\current.json");

                if (File.Exists(fileJson))
                {
                    listFileUpdate = Tools.LoadFile(fileJson);
                    var fileAssemblyItem = listFileUpdate.Where((p) => p.Name == Project.Name).FirstOrDefault();
                    if (fileAssemblyItem != null)
                    {
                        Project.CurrentVersion = fileAssemblyItem.CurrentVersion;
                        var PublisPathAssembly = Path.Combine(Project.SourcePath, Project.CurrentVersion);
                        var filePathApplication = Path.Combine(PublisPathAssembly, Project.ApplicationFile.NameAssembly);
                        if (!File.Exists(filePathApplication))
                        {
                            MessageBox.Show(string.Format("Error no se encuentra el fichero {0} en la ruta {1}", Project.ApplicationFile, Project.SourcePath), "ERROR AL CARGAR PATH PROYECTO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return null;
                        }
                    }
                }

                if (!Directory.Exists(Project.TargetPath))
                    Directory.CreateDirectory(Project.TargetPath);

                return Project;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepción por " + ex.Message, "ERROR SYNCRONIZACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private string SyncFiles(ProjectItem Project)
        {
            var pathApplication = "";
                    
           lblVersionUpdate.Text = string.Format("{0}  >>>   {1}", Project.Name, Project.CurrentVersion);
           pathApplication = Path.Combine(Project.TargetPath, Project.ApplicationFile.NameAssembly);
           var PublisPathAssembly = Path.Combine(Project.SourcePath, Project.CurrentVersion);
           CopyFiles(PublisPathAssembly, Project.TargetPath);

           return pathApplication;
        }

        private void CopyFiles(string pathApplication, string productionPath)
        {
            progressBar1.Value = 0;

            Directory.CreateDirectory(productionPath);

            var listDirectories = Directory.GetDirectories(pathApplication, "*", SearchOption.AllDirectories).ToList();

            foreach (var dir in listDirectories)
                Directory.CreateDirectory(dir.Replace(pathApplication, productionPath));

            var listInnerFiles = Directory.GetFiles(pathApplication, "*.*", SearchOption.AllDirectories).ToList();
            progressBar1.Maximum = listInnerFiles.Count;

            foreach (var file in listInnerFiles)
            {
                UpdateUI(Path.GetFileName(file));

                File.Copy(file, file.Replace(pathApplication, productionPath), true);

                Application.DoEvents();
            }
        }

        private void UpdateUI(string filename)
        {
            progressBar1.Value++;
            lblInfomation.Text = filename;
        }

        private void ExecuteProcess(string pathApplication)
        {
            if (!File.Exists(pathApplication))
                MessageBox.Show(string.Format("!No existe el Path para ejecutar la aplicacion {0} ", ApplicationSelected), "ERROR SYNCRONIZACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                ProcessStartInfo info = new ProcessStartInfo(pathApplication);
                 var directory = Path.GetFileNameWithoutExtension(pathApplication);
                info.WorkingDirectory = "C:\\SAURON_SYSTEM\\" + directory;
                info.UseShellExecute = false;
                Process.Start(info);
            }
        }
    }
}
