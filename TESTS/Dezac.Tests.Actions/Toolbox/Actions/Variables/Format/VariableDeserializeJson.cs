﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(VariableDeserializeJsonDescription))]
    public class VariableDeserializeJson : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Variable con los datos de entrada")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if (DataOutput.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DataOutput.Key).Throw();
            
            string json = DataInput.Value.ToString();

            dynamic dynJson = JsonConvert.DeserializeObject(json);
            foreach (var item in dynJson)
               Context.Variables.AddOrUpdate(string.Format("{0}_{1}",DataOutput, item.Name), item.Value);
        }
    }

    public class VariableDeserializeJsonDescription : ActionDescription
    {
        public override string Name { get { return "VariableDeserializeJson"; } }
        public override string Description { get { return "Este action sirve para deserializar una variable con formato JSON"; }}
        public override string Category { get { return "Actions/Variables/Format"; } }
        public override Image Icon { get { return Properties.Resources.format; } }
    }
}
