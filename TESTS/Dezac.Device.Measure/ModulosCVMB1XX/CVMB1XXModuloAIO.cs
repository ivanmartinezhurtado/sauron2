﻿using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure.ModulosCVMB1XX
{
    [DeviceVersion(1.00)]
    public class CVMB1XXModuloAIO : DeviceBase
    {
         public CVMB1XXModuloAIO()
        {
        }

        public CVMB1XXModuloAIO(int port)
        {
            SetPort(port);
        }
       
        public void SetPort(int port, int perifericNumber = 100)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 115200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)perifericNumber;           
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public List<double> CalculaFactorCalibrationVoltage(List<double> OutsMeasures)
        {
            var FactorsCalibrationCalculates = new List<double>();
            var CurrentFactorCalibrationVoltage = ReadStructureFactorCalibrationVoltage();

            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput1 * (10000 / (OutsMeasures[0] * 1000)));
            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput2 * (10000 / (OutsMeasures[1] * 1000)));
            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput3 * (10000 / (OutsMeasures[2] * 1000)));
            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput4 * (10000 / (OutsMeasures[3] * 1000)));
            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput5 * (10000 / (OutsMeasures[4] * 1000)));
            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput6 * (10000 / (OutsMeasures[5] * 1000)));
            FactorsCalibrationCalculates.Add((double)CurrentFactorCalibrationVoltage.FactorOutput7 * (10000 / (OutsMeasures[6] * 1000)));

            return FactorsCalibrationCalculates;

            //var FactorCalibration = fcActual * (10000 / (valueMeasure * 1000)); //mv fcActual * (10 / X) --->V
        }      


        public double CalculaFactorVoltage(double readingMeasure, double factorActual)
        {
            var factorCalibration = factorActual * (10000 / (readingMeasure * 1000));
            return factorCalibration;
        }

        public double CalculaFactorCalibrationInput(AnalogInputPoints register)
        {
            var fcActual = ReadInputsPointsWithOutCalibrated(register);
            return (double)8192 * (32768 / (double)fcActual);
        }

        public int ReadInputsPointsWithOutCalibrated(AnalogInputPoints register)
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)register);
        }

        public int ReadInputsPointsCalibrated(AnalogInputPoints input)
        {
            var InputAndRegistersPointsCalibration = new Dictionary<AnalogInputPoints, AnalogInputsPointsCalibrated>();
            InputAndRegistersPointsCalibration.Add(AnalogInputPoints.INPUT1, AnalogInputsPointsCalibrated.INPUT1);
            InputAndRegistersPointsCalibration.Add(AnalogInputPoints.INPUT2, AnalogInputsPointsCalibrated.INPUT2);
            InputAndRegistersPointsCalibration.Add(AnalogInputPoints.INPUT3, AnalogInputsPointsCalibrated.INPUT3);
            InputAndRegistersPointsCalibration.Add(AnalogInputPoints.INPUT4, AnalogInputsPointsCalibrated.INPUT4);

            LogMethod();
            foreach (KeyValuePair<AnalogInputPoints, AnalogInputsPointsCalibrated> relationInputRegister in InputAndRegistersPointsCalibration)
                if (relationInputRegister.Key == input)
                    return Modbus.ReadInt32((ushort)relationInputRegister.Value);

            throw new Exception("La entrada que se intenta leer no existe");
        }

        public double ReadFactorCalibrationVoltage(RegistersFactorCalibrationVoltage register)
        {
            LogMethod();
            return (double)Modbus.ReadRegister((ushort) register);
        }
        public VoltageFactorCalibrationAnalogModules ReadStructureFactorCalibrationVoltage()
        {
            LogMethod();
            return Modbus.Read<VoltageFactorCalibrationAnalogModules>();
        }

        public int ReadFactorCalibrationCurrent(RegistersFactorCalibrationCurrent register)
        {
            LogMethod();
            return Modbus.Read<UInt16>((ushort)register);
        }

        public OutputsModuleAnalog ReadValueAllAnalogOuts()
        {
            LogMethod();
            //Modbus.WriteInt32((ushort)Registers.VALOR_ANALOG_ALL_OUT, value);
            return Modbus.Read<OutputsModuleAnalog>();
        }

        public void WriteFactorCalibrationInputs(AnalogInputPoints input, double FactorCalibration)
        {
            LogMethod();
            FlagTest();

            var InputAndRegistersFactorCalibration = new Dictionary<AnalogInputPoints, RegistersFactorCalibrationInputs>();
            InputAndRegistersFactorCalibration.Add(AnalogInputPoints.INPUT1, RegistersFactorCalibrationInputs.FACTOR_CALIBRATION_IN1);
            InputAndRegistersFactorCalibration.Add(AnalogInputPoints.INPUT2, RegistersFactorCalibrationInputs.FACTOR_CALIBRATION_IN2);
            InputAndRegistersFactorCalibration.Add(AnalogInputPoints.INPUT3, RegistersFactorCalibrationInputs.FACTOR_CALIBRATION_IN3);
            InputAndRegistersFactorCalibration.Add(AnalogInputPoints.INPUT4, RegistersFactorCalibrationInputs.FACTOR_CALIBRATION_IN4);

            foreach (KeyValuePair<AnalogInputPoints, RegistersFactorCalibrationInputs> relationInputRegister in InputAndRegistersFactorCalibration)
                if (relationInputRegister.Key == input)
                    Modbus.Write((ushort)relationInputRegister.Value, (ushort)FactorCalibration);
        }

        public void WriteFactorCalibrationVoltage(RegistersFactorCalibrationVoltage register, double FactorCalibration)
        {
            LogMethod();

            Modbus.Write((ushort)register, (ushort)FactorCalibration);
        }


        public void WriteFactorCalibrationCurrent(RegistersFactorCalibrationCurrent register, double value)
        {
            LogMethod();
            Modbus.Write((ushort)register, (ushort)value);
        }

        public void FlagTest(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void WriteDefaultFactors(bool state = true)
        {
            Modbus.WriteSingleCoil((ushort)Registers.DEFAULTS_FACTORS_CALIBRATION, state);
        }

        public void WriteOuputsModeModuleAnalog(AnalogModulesOuputsModes value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MODE_ANALOG_OUT, (ushort)value);
        }

        public void WriteOutputsValues(OutputsModuleAnalog value)
        {
            LogMethod();
            //Modbus.WriteInt32((ushort)Registers.VALOR_ANALOG_ALL_OUT, value);
            Modbus.Write<OutputsModuleAnalog>(value);
        }

        #region Structures and Enums
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VALOR_ANALOG_ALL_OUT)]
        public struct OutputsModuleAnalog
        {
            public Int32 Output1 { get; set; }
            public Int32 Output2 { get; set; }
            public Int32 Output3 { get; set; }
            public Int32 Output4 { get; set; }
            public Int32 Output5 { get; set; }
            public Int32 Output6 { get; set; }
            public Int32 Output7 { get; set; }
            public Int32 Output8 { get; set; }

            public void SetAllOutsOFF()
            {
                Output1 = Output2 = Output3 = Output4 = Output5 = Output6 = Output7 = Output8 = 0;
            }

            public void SetAllOuputsValue(int value)
            {
                Output1 = Output2 = Output3 = Output4 = Output5 = Output6 = Output7 = Output8 = value;
            }

            public void ActiveOutput(RelayAnalogOuts output)
            {
                SetAllOutsOFF();

                switch(output)
                {
                    case RelayAnalogOuts.OUTPUT1: Output1 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT2: Output2 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT3: Output3 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT4: Output4 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT5: Output5 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT6: Output6 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT7: Output7 = 20000;
                        break;
                    case RelayAnalogOuts.OUTPUT8: Output8 = 20000;
                        break;
                    default: SetAllOutsOFF();
                        break;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.FACTOR_CALIBRATION_VOLTAGE)]
        public struct VoltageFactorCalibrationAnalogModules
        {
            public Int16 FactorOutput1 { get; set; }
            public Int16 FactorOutput2 { get; set; }
            public Int16 FactorOutput3 { get; set; }
            public Int16 FactorOutput4 { get; set; }
            public Int16 FactorOutput5 { get; set; }
            public Int16 FactorOutput6 { get; set; }
            public Int16 FactorOutput7 { get; set; }
            public Int16 FactorOutput8 { get; set; }
        }

        public enum Registers
        {
            FLAG_TEST = 0x2AF8,
            MODE_ANALOG_OUT = 0X5208,
            VALOR_ANALOG_ALL_OUT = 0X5014,
            FACTOR_CALIBRATION_CURRENT = 0X4E34,
            FACTOR_CALIBRATION_VOLTAGE = 0X4E3C,
            DEFAULTS_FACTORS_CALIBRATION = 0X0C1C
        }

        public enum RegistersFactorCalibrationCurrent
        {
            FACTOR_CALIBRATION_CURRENT_OUT1 = 0X4E34,
            FACTOR_CALIBRATION_CURRENT_OUT2 = 0X4E35,
            FACTOR_CALIBRATION_CURRENT_OUT3 = 0X4E36,
            FACTOR_CALIBRATION_CURRENT_OUT4 = 0X4E37,
            FACTOR_CALIBRATION_CURRENT_OUT5 = 0X4E38,
            FACTOR_CALIBRATION_CURRENT_OUT6 = 0X4E39,
            FACTOR_CALIBRATION_CURRENT_OUT7 = 0X4E3A,
            FACTOR_CALIBRATION_CURRENT_OUT8 = 0X4E3B
        }

        public enum RegistersFactorCalibrationVoltage
        {
            [Description("OUTPUT1")]
            FACTOR_CALIBRATION_VOLTAGE_OUT1 = 0X4E3C,
            [Description("OUTPUT2")]
            FACTOR_CALIBRATION_VOLTAGE_OUT2 = 0X4E3D,
            [Description("OUTPUT3")]
            FACTOR_CALIBRATION_VOLTAGE_OUT3 = 0X4E3E,
            [Description("OUTPUT4")]
            FACTOR_CALIBRATION_VOLTAGE_OUT4 = 0X4E3F,
            [Description("OUTPUT5")]
            FACTOR_CALIBRATION_VOLTAGE_OUT5 = 0X4E40,
            [Description("OUTPUT6")]
            FACTOR_CALIBRATION_VOLTAGE_OUT6 = 0X4E41,
            [Description("OUTPUT7")]
            FACTOR_CALIBRATION_VOLTAGE_OUT7 = 0X4E42,
            [Description("OUTPUT8")]
            FACTOR_CALIBRATION_VOLTAGE_OUT8 = 0X4E43
        }     

        public enum RelayAnalogOuts
        {
            OUTPUT1 = 19,
            OUTPUT2 = 20,
            OUTPUT3 = 21,
            OUTPUT4 = 22,
            OUTPUT5 = 17,
            OUTPUT6 = 43,
            OUTPUT7 = 0,//CHROMA
            OUTPUT8 = 28
        }

        public enum AnalogOutputValues
        {
            OUPUT1 = 0X5014,
            OUTPUT2 = 0X5016,
            OUTPUT3 = 0X5018,
            OUTPUT4 = 0X501A,
            OUTPUT5 = 0X501C,
            OUTPUT6 = 0X501E,
            OUTPUT7 = 0X5020,
            OUTPUT8 = 0X5022
        }

        public enum AnalogInputPoints
        {
            INPUT1 = 0X4FB0,
            INPUT2 = 0X4FB2,
            INPUT3 = 0X4FB4,
            INPUT4 = 0X4FB6,
        }

        public enum AnalogInputsPointsCalibrated
        {
            INPUT1 = 0X4F4C,
            INPUT2 = 0X4F4E,
            INPUT3 = 0X4F50,
            INPUT4 = 0X4F52
        }

        public enum RegistersPointsCalibrationInputs
        {
            POINTS_CALIBRATION_IN1 = 0X4FB0,
            POINTS_CALIBRATION_IN2 = 0X4FB2,
            POINTS_CALIBRATION_IN3 = 0X4FB4,
            POINTS_CALIBRATION_IN4 = 0X4FB6
        }

        public enum RegistersFactorCalibrationInputs
        {
            FACTOR_CALIBRATION_IN1 = 0X4E20,
            FACTOR_CALIBRATION_IN2 = 0X4E21,
            FACTOR_CALIBRATION_IN3 = 0X4E22,
            FACTOR_CALIBRATION_IN4 = 0X4E23
        }

        public enum AnalogModulesOuputsModes
        {
            OFF,
            CURRENT,
            VOLTAGE
        }
        #endregion
    }
}
