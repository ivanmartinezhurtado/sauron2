﻿using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.Test_Init
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(InitNumeroSerieOfflineActionDescription))]
   
    public class InitNumeroSerieOfflineAction : ActionBase
    {
        public string Prefix { get; set; }
        public int? MinLength { get; set; }
        public int? MaxLength { get; set; }
        public bool AssignBastidor { get; set; }

        public override void Execute()
        {
            var data = SequenceContext.Current.Services.Get<ITestContext>();
            if (data == null)
                throw new Exception("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            if (data.NumOrden != null && data.NumProducto == 0)
                throw new Exception("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test");

            if (string.IsNullOrEmpty(data.TestInfo.NumSerie))
            {
                data.TestInfo.NumSerie = GetIntroBastiorByUser(null);

                if (AssignBastidor)
                    data.TestInfo.NumBastidor = Convert.ToUInt64(data.TestInfo.NumSerie);

                SequenceContext.Current.Variables.AddOrUpdate(ConstantsParameters.TestInfo.NUM_SERIE, data.TestInfo.NumSerie);
            }
        }

        private string GetIntroBastiorByUser(string device)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            if (string.IsNullOrEmpty(device))
                device = " a testear";

            string msg = context.NumInstance > 1 || context.RunningInstances > 1 ?
                string.Format("ENTRADA DEL NUMERO DE SERIE DEL TEST {0}", context.NumInstance) :
                "ENTRADA DEL NUMERO DE SERIE";

            var numeroSerie = iShell.ShowDialog<string>(msg, () =>
            {
                return new InputKeyBoard(string.Format("Leer código de barras del numero de serie del equipo {0}", device));
            }
           , MessageBoxButtons.OKCancel, (c, d) =>
           {
               if (d == DialogResult.OK)
                   return ((InputKeyBoard)c).TextValue;

               return string.Empty;
           });

            if (string.IsNullOrEmpty(numeroSerie))
                throw new Exception("El usuario ha cancelado la entrada del numero de serie!");

            if (!string.IsNullOrEmpty(Prefix) && !numeroSerie.StartsWith(Prefix))
                throw new Exception("El numero de serie introducido no es válido");

            if (numeroSerie.Length < MinLength.GetValueOrDefault(0) || numeroSerie.Length > MaxLength.GetValueOrDefault(Int32.MaxValue))
                throw new Exception("La longitud del numero de serie introducido no es valida");

            return numeroSerie;
        }
    }

    public class InitNumeroSerieOfflineActionDescription : ActionDescription
    {
        public override string Name { get { return "InitNumeroSerieOfflineAction"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
    }
}
