﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Dezac.Data.ViewModels;
using System.Configuration;
using System.Diagnostics;

namespace Dezac.Tests.Laser.Controls
{
    public partial class LaserStep : UserControl
    {
        private static int Count = 0;

        public LaserStep()
        {
            InitializeComponent();
            
            BorrarFicherosTemp();
        }       

        public void Validate(LaserItemViewModel item)
        {
            lblItem.Text = item.CodigoBcn;
            lblDescripcion.Text = item.DescripcionArticulo;
            lblCodigoUtil.Text = item.Codigo;
            lblDescripcionUtil.Text = item.descripcion;           

            if (item.DocPdf != null)
                webBrowser1.Navigate("file:///" + CrearUrlFileInstruccion(item.DocPdf));

            //string name = string.Format("{0}.svg", item.CodigoBcn);

            //string fileName = Path.Combine(PathSVGFileLaser, name);

            //if (!File.Exists(fileName))
            //    throw new Exception(string.Format("Archivo {0} inexistente!", fileName));

            //webBrowserSVG.Navigate(fileName);
            //webBrowser1.Navigate(fileName);
            Application.DoEvents();
        }

        //private string PathSVGFileLaser
        //{
        //    get { return ConfigurationManager.AppSettings["PathFileSVGLaser"]; }
        //}

        public string PathFileInstrucionTemp
        {
            get { return ConfigurationManager.AppSettings["PathFileInstrucionTemp"]; }
        }

        public Image CrearArchivoImagen(byte[] imagen)
        {
            MemoryStream ms = new MemoryStream(imagen);

            Image returnImage = Image.FromStream(ms);

            return returnImage;
        }

        public string CrearUrlFileInstruccion(byte[] pdf)
        {
            Count++;

            string url = PathFileInstrucionTemp + "InstruccionTemp" + Count.ToString() + ".pdf";

            if (!Directory.Exists(PathFileInstrucionTemp))
                Directory.CreateDirectory(PathFileInstrucionTemp);

            File.WriteAllBytes(url, pdf);

            return url;
        }

        public void BorrarFicherosTemp()
        {
            Count = 0;

            string url = PathFileInstrucionTemp;

            if (!Directory.Exists(url))
                Directory.CreateDirectory(url);

            foreach (string fl in Directory.GetFiles(url))
                File.Delete(fl);
        }

    }
}
