﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Repository.Pattern.Infrastructure;

namespace Repository.Pattern.Ef5
{
    public abstract class FakeDbSet<TEntity> : /*DbSet<TEntity>,*/ IDbSet<TEntity> where TEntity : Entity, new()
    {
        #region Private Fields
        private readonly ObservableCollection<TEntity> _items = new ObservableCollection<TEntity>();
        private /*readonly*/ IQueryable _query;
        #endregion Private Fields

        //public void Init()
        //{
        //    _items = new ObservableCollection<TEntity>();
        //    _query = _items.AsQueryable();
        //}

        IEnumerator IEnumerable.GetEnumerator() { return _items.GetEnumerator(); }
        public IEnumerator<TEntity> GetEnumerator() { return _items.GetEnumerator(); }

        private IQueryable Query()
        {
            if (_query == null)
                _query = _items.AsQueryable();

            return _query;
        }

        public Expression Expression { get { return _query.Expression; } }

        public Type ElementType { get { return Query().ElementType; } }

        public IQueryProvider Provider { get { return _query.Provider; } }

        public /*override*/ TEntity Add(TEntity entity)
        {
            _items.Add(entity);
            return entity;
        }

        public /*override*/ TEntity Remove(TEntity entity)
        {
            _items.Remove(entity);
            return entity;
        }

        public /*override*/ TEntity Attach(TEntity entity)
        {
            switch (entity.ObjectState)
            {
                case ObjectState.Modified:
                    _items.Remove(entity);
                    _items.Add(entity);
                    break;
                
                case ObjectState.Deleted:
                    _items.Remove(entity);
                    break;
                
                case ObjectState.Unchanged:
                case ObjectState.Added:
                    _items.Add(entity);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return entity;
        }

        public /*override*/ TEntity Create() { return new TEntity(); }

        public /*override*/ TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity
        { 
            return Activator.CreateInstance<TDerivedEntity>(); 
        }

        public /*override*/ ObservableCollection<TEntity> Local { get { return _items; } }


        public TEntity Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }
    }
}