﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(LambdaDescription))]
    public partial class UC_LAMBDA : UserControl, IDisposable
    {
        private static readonly string TAG = "LAMBDA";

        private MyComputer computer=new MyComputer();

        private double Voltage;
        private double Current;

        private SynchronizationContext sc;

        private bool timerEnabled;
        private bool modeRead;
        private Lambda lmd;

        public UC_LAMBDA()
        {
            InitializeComponent();            
        }

        public Type InstrumentType
        {
            get
            {
                return typeof(Lambda);
            }
        }


        public Lambda Instrument
        {
            get
            {
                if (lmd == null)
                    lmd = new Lambda();

                return lmd;
            }
            set
            {
                if (lmd != null)
                {
                    lmd.Dispose();
                    lmd = null;
                }
                lmd = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;

            try
            {
                var ports = new MyComputer().SerialPortNames.ToArray();
                if (ports.Length == 0)
                {
                    errorProvider1.SetError(cmb_Port, "No se ha encontrado puertos en el PC");
                    return;
                }

                cmb_Port.Items.AddRange(ports);
                cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM12");
                if (cmb_Port.SelectedItem == null)
                    cmb_Port.SelectedIndex = 0;

                SetMode(false);
            }
            catch (Exception)
            {
                errorProvider1.SetError(cmb_Port, "No se ha encontrado la fuente DC LAMBDA");
            }
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            ToggleButtonsState((Button)sender);

            errorProvider1.Clear();
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
            else
            {
                try
                {
                    var port = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                    var task = Task.Factory.StartNew((Action)(() =>
                    {
                        this.Instrument.PortCom = port;
                    }));
                    LifeMethod("inicializando", task);
                }
                catch (Exception ex)
                {
                    Logger.Error("Error al inicializar el LAMBDA", ex);
                    ToggleButtonsState((Button)sender);
                    return;
                }
                try
                {
                    if (Instrument != null)
                        if (ValidatePresets() == true)
                        {
                            var task = Task.Factory.StartNew(() => { Instrument.ApplyPresetsAndWaitStabilisation(Voltage, Current); });
                            LifeMethod("Encendiendo", task);
                        }
                }
                catch (Exception ex)
                {
                    Logger.Error("Error al encender el LAMBDA", ex);
                }
                finally
                {
                    ToggleButtonsState((Button)sender);
                }
            }
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            try
            {
                ToggleButtonsState((Button)sender);

                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyOffAndWaitStabilisation(); }));

                LifeMethod("Apagando", task);
            }
            catch (Exception ex)
            {
                Logger.Error("Error al apagar el LAMBDA", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }

        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = modeRead ? Color.DimGray : Color.LightGreen;
            SetMode(!modeRead);
        }

        private bool ValidatePresets()
        {
            try
            {
                if (!ValidateVoltageFreq()) return false;
                if (!ValidateCurrent()) return false;

            }
            catch (Exception ex)
            {
                Logger.Error("Error al validar presets al LAMBDA", ex);
            }
            return true;
        }

        private bool ValidateVoltageFreq()
        {
            Voltage = GetAsDouble(txtV1);
            if (Voltage > 400)
            {
                errorProvider1.SetError(txtV1, "Voltage must lower 400V");
                return false;
            }
            return true;
        }

        private bool ValidateCurrent()
        {
            Current = GetAsDouble(txtI1);

            if (Current > 12)
            {
                errorProvider1.SetError(txtI1, "Current must lower 12A");
                return false;
            }
            return true;
        }

        private void SetMode(bool read)
        {
            modeRead = read;

            if (modeRead && !timerEnabled)
                EnableTimer();
            else
                timerEnabled = false;
        }

        private void EnableTimer()
        {
            timerEnabled = true;
            Task.Factory.StartNew(OnGraphTask);
        }

        private void OnGraphTask()
        {
            while (timerEnabled)
            {
                try
                {
                    UpdateGraphs();
                }
                catch { }
                Thread.Sleep(1000);
            }
        }

        private void UpdateGraphs()
        {
            double current = 0;
            double voltage = 0;

            if (modeRead)
                current = Instrument.ReadCurrent();

            if (modeRead)
                voltage = Instrument.ReadVoltage();

            sc.Post(_ =>
            {
                if (current != 0)
                    DataPointHub.Add(this, "I1", current, TAG);

                if (voltage != 0)
                    DataPointHub.Add(this, "V1", voltage, TAG);
            }, null);
        }

        private double GetAsDouble(MaskedTextBox txt)
        {
            if (string.IsNullOrEmpty(txt.Text))
                return 0;

            return Convert.ToDouble(txt.Text);
        }

        private void ToggleButtonsState(Button btn)
        {
            if (btn.Equals(btnOn))
                btnOn.Enabled = !btnOn.Enabled;
            if (btn.Equals(btnOff))
                btnOff.Enabled = !btnOff.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if(Instrument != null)
            Instrument.Dispose();

            base.Dispose(disposing);
        }
    }

    public class LambdaDescription : ControlDescription
    {
        public override string Name { get { return "LAMBDA"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_icon; } }
        public override Image IconBig { get { return Properties.Resources.LAMBDA_DC; } }
        public override Type SubcomponentType { get { return typeof(Lambda); } }
    }
}
