﻿namespace Dezac.Instruments.Controls
{
    partial class UC_CVMB150
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label factorPotenciaL1Label;
            System.Windows.Forms.Label factorPotenciaL2Label;
            System.Windows.Forms.Label factorPotenciaL3Label;
            System.Windows.Forms.Label facttorPotenciaIIILabel;
            System.Windows.Forms.Label potenciaActivaL1Label;
            System.Windows.Forms.Label potenciaActivaL2Label;
            System.Windows.Forms.Label potenciaActivaL3Label;
            System.Windows.Forms.Label potenciaActivaIIILabel;
            System.Windows.Forms.Label tensionL1Label;
            System.Windows.Forms.Label tensionL2Label;
            System.Windows.Forms.Label tensionL3Label;
            System.Windows.Forms.Label tensionFaseIIILabel;
            System.Windows.Forms.Label tensionNeutroLabel;
            System.Windows.Forms.Label corrienteL1Label;
            System.Windows.Forms.Label corrienteL2Label;
            System.Windows.Forms.Label corrienteL3Label;
            System.Windows.Forms.Label corrienteNeutroLabel;
            System.Windows.Forms.Label cosenoPhiL1Label;
            System.Windows.Forms.Label cosenoPhiL2Label;
            System.Windows.Forms.Label cosenoPhiL3Label;
            System.Windows.Forms.Label cosenoPhiIIILabel;
            System.Windows.Forms.Label potenciaAparenteL1Label;
            System.Windows.Forms.Label potenciaAparenteL2Label;
            System.Windows.Forms.Label potenciaAparenteL3Label;
            System.Windows.Forms.Label potenciaAparenteIIILabel;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL1Label;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL2Label;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL3Label;
            System.Windows.Forms.Label potenciaCapacitivaIIILabel;
            System.Windows.Forms.Label potenciaReactivaInductivaL1Label;
            System.Windows.Forms.Label potenciaReactivaInductivaL2Label;
            System.Windows.Forms.Label potenciaReactivaInductivaL3Label;
            System.Windows.Forms.Label potenciaInductivaIIILabel;
            System.Windows.Forms.Label corrienteIIILabel;
            System.Windows.Forms.Label tensionLineaL1L2Label;
            System.Windows.Forms.Label tensionLineaL2L3Label;
            System.Windows.Forms.Label tensionLineaL3L1Label;
            System.Windows.Forms.Label tensionLineaIIILabel;
            System.Windows.Forms.Label frecuenciaLabel;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            this.cosenoPhiIIITextBox = new System.Windows.Forms.TextBox();
            this.bsAllVars = new System.Windows.Forms.BindingSource(this.components);
            this.cosenoPhiL3TextBox = new System.Windows.Forms.TextBox();
            this.cosenoPhiL2TextBox = new System.Windows.Forms.TextBox();
            this.cosenoPhiL1TextBox = new System.Windows.Forms.TextBox();
            this.corrienteIIITextBox = new System.Windows.Forms.TextBox();
            this.corrienteNeutroTextBox = new System.Windows.Forms.TextBox();
            this.corrienteL3TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL2TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL1TextBox = new System.Windows.Forms.TextBox();
            this.tensionNeutroTextBox = new System.Windows.Forms.TextBox();
            this.tensionFaseIIITextBox = new System.Windows.Forms.TextBox();
            this.tensionL3TextBox = new System.Windows.Forms.TextBox();
            this.tensionL2TextBox = new System.Windows.Forms.TextBox();
            this.tensionL1TextBox = new System.Windows.Forms.TextBox();
            this.btnOn = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.facttorPotenciaIIITextBox = new System.Windows.Forms.TextBox();
            this.factorPotenciaL3TextBox = new System.Windows.Forms.TextBox();
            this.factorPotenciaL2TextBox = new System.Windows.Forms.TextBox();
            this.factorPotenciaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaCapacitivaIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaInductivaIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL1TextBox = new System.Windows.Forms.TextBox();
            this.frecuenciaTextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaIIITextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaL3L1TextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaL2L3TextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaL1L2TextBox = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmb_Periferic = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.comboBoxBaudRate = new System.Windows.Forms.ComboBox();
            factorPotenciaL1Label = new System.Windows.Forms.Label();
            factorPotenciaL2Label = new System.Windows.Forms.Label();
            factorPotenciaL3Label = new System.Windows.Forms.Label();
            facttorPotenciaIIILabel = new System.Windows.Forms.Label();
            potenciaActivaL1Label = new System.Windows.Forms.Label();
            potenciaActivaL2Label = new System.Windows.Forms.Label();
            potenciaActivaL3Label = new System.Windows.Forms.Label();
            potenciaActivaIIILabel = new System.Windows.Forms.Label();
            tensionL1Label = new System.Windows.Forms.Label();
            tensionL2Label = new System.Windows.Forms.Label();
            tensionL3Label = new System.Windows.Forms.Label();
            tensionFaseIIILabel = new System.Windows.Forms.Label();
            tensionNeutroLabel = new System.Windows.Forms.Label();
            corrienteL1Label = new System.Windows.Forms.Label();
            corrienteL2Label = new System.Windows.Forms.Label();
            corrienteL3Label = new System.Windows.Forms.Label();
            corrienteNeutroLabel = new System.Windows.Forms.Label();
            cosenoPhiL1Label = new System.Windows.Forms.Label();
            cosenoPhiL2Label = new System.Windows.Forms.Label();
            cosenoPhiL3Label = new System.Windows.Forms.Label();
            cosenoPhiIIILabel = new System.Windows.Forms.Label();
            potenciaAparenteL1Label = new System.Windows.Forms.Label();
            potenciaAparenteL2Label = new System.Windows.Forms.Label();
            potenciaAparenteL3Label = new System.Windows.Forms.Label();
            potenciaAparenteIIILabel = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL1Label = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL2Label = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL3Label = new System.Windows.Forms.Label();
            potenciaCapacitivaIIILabel = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL1Label = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL2Label = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL3Label = new System.Windows.Forms.Label();
            potenciaInductivaIIILabel = new System.Windows.Forms.Label();
            corrienteIIILabel = new System.Windows.Forms.Label();
            tensionLineaL1L2Label = new System.Windows.Forms.Label();
            tensionLineaL2L3Label = new System.Windows.Forms.Label();
            tensionLineaL3L1Label = new System.Windows.Forms.Label();
            tensionLineaIIILabel = new System.Windows.Forms.Label();
            frecuenciaLabel = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // factorPotenciaL1Label
            // 
            factorPotenciaL1Label.AutoSize = true;
            factorPotenciaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL1Label.Location = new System.Drawing.Point(18, 249);
            factorPotenciaL1Label.Name = "factorPotenciaL1Label";
            factorPotenciaL1Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL1Label.TabIndex = 0;
            factorPotenciaL1Label.Text = "PF L1:";
            // 
            // factorPotenciaL2Label
            // 
            factorPotenciaL2Label.AutoSize = true;
            factorPotenciaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL2Label.Location = new System.Drawing.Point(18, 275);
            factorPotenciaL2Label.Name = "factorPotenciaL2Label";
            factorPotenciaL2Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL2Label.TabIndex = 2;
            factorPotenciaL2Label.Text = "PF L2:";
            // 
            // factorPotenciaL3Label
            // 
            factorPotenciaL3Label.AutoSize = true;
            factorPotenciaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL3Label.Location = new System.Drawing.Point(18, 301);
            factorPotenciaL3Label.Name = "factorPotenciaL3Label";
            factorPotenciaL3Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL3Label.TabIndex = 4;
            factorPotenciaL3Label.Text = "PF L3:";
            // 
            // facttorPotenciaIIILabel
            // 
            facttorPotenciaIIILabel.AutoSize = true;
            facttorPotenciaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            facttorPotenciaIIILabel.Location = new System.Drawing.Point(20, 327);
            facttorPotenciaIIILabel.Name = "facttorPotenciaIIILabel";
            facttorPotenciaIIILabel.Size = new System.Drawing.Size(42, 13);
            facttorPotenciaIIILabel.TabIndex = 6;
            facttorPotenciaIIILabel.Text = "PF III:";
            // 
            // potenciaActivaL1Label
            // 
            potenciaActivaL1Label.AutoSize = true;
            potenciaActivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL1Label.Location = new System.Drawing.Point(158, 15);
            potenciaActivaL1Label.Name = "potenciaActivaL1Label";
            potenciaActivaL1Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL1Label.TabIndex = 0;
            potenciaActivaL1Label.Text = "Pot. Act L1:";
            // 
            // potenciaActivaL2Label
            // 
            potenciaActivaL2Label.AutoSize = true;
            potenciaActivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL2Label.Location = new System.Drawing.Point(158, 41);
            potenciaActivaL2Label.Name = "potenciaActivaL2Label";
            potenciaActivaL2Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL2Label.TabIndex = 2;
            potenciaActivaL2Label.Text = "Pot. Act L2:";
            // 
            // potenciaActivaL3Label
            // 
            potenciaActivaL3Label.AutoSize = true;
            potenciaActivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL3Label.Location = new System.Drawing.Point(158, 67);
            potenciaActivaL3Label.Name = "potenciaActivaL3Label";
            potenciaActivaL3Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL3Label.TabIndex = 4;
            potenciaActivaL3Label.Text = "Pot. Act L3:";
            // 
            // potenciaActivaIIILabel
            // 
            potenciaActivaIIILabel.AutoSize = true;
            potenciaActivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaIIILabel.Location = new System.Drawing.Point(160, 93);
            potenciaActivaIIILabel.Name = "potenciaActivaIIILabel";
            potenciaActivaIIILabel.Size = new System.Drawing.Size(73, 13);
            potenciaActivaIIILabel.TabIndex = 6;
            potenciaActivaIIILabel.Text = "Pot. Act III:";
            // 
            // tensionL1Label
            // 
            tensionL1Label.AutoSize = true;
            tensionL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL1Label.Location = new System.Drawing.Point(25, 15);
            tensionL1Label.Name = "tensionL1Label";
            tensionL1Label.Size = new System.Drawing.Size(37, 13);
            tensionL1Label.TabIndex = 0;
            tensionL1Label.Text = "V L1:";
            // 
            // tensionL2Label
            // 
            tensionL2Label.AutoSize = true;
            tensionL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL2Label.Location = new System.Drawing.Point(25, 41);
            tensionL2Label.Name = "tensionL2Label";
            tensionL2Label.Size = new System.Drawing.Size(37, 13);
            tensionL2Label.TabIndex = 2;
            tensionL2Label.Text = "V L2:";
            // 
            // tensionL3Label
            // 
            tensionL3Label.AutoSize = true;
            tensionL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL3Label.Location = new System.Drawing.Point(25, 67);
            tensionL3Label.Name = "tensionL3Label";
            tensionL3Label.Size = new System.Drawing.Size(37, 13);
            tensionL3Label.TabIndex = 4;
            tensionL3Label.Text = "V L3:";
            // 
            // tensionFaseIIILabel
            // 
            tensionFaseIIILabel.AutoSize = true;
            tensionFaseIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionFaseIIILabel.Location = new System.Drawing.Point(342, 327);
            tensionFaseIIILabel.Name = "tensionFaseIIILabel";
            tensionFaseIIILabel.Size = new System.Drawing.Size(66, 13);
            tensionFaseIIILabel.TabIndex = 6;
            tensionFaseIIILabel.Text = "V Fase III:";
            // 
            // tensionNeutroLabel
            // 
            tensionNeutroLabel.AutoSize = true;
            tensionNeutroLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionNeutroLabel.Location = new System.Drawing.Point(1, 93);
            tensionNeutroLabel.Name = "tensionNeutroLabel";
            tensionNeutroLabel.Size = new System.Drawing.Size(61, 13);
            tensionNeutroLabel.TabIndex = 8;
            tensionNeutroLabel.Text = "V Neutro:";
            // 
            // corrienteL1Label
            // 
            corrienteL1Label.AutoSize = true;
            corrienteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL1Label.Location = new System.Drawing.Point(29, 119);
            corrienteL1Label.Name = "corrienteL1Label";
            corrienteL1Label.Size = new System.Drawing.Size(33, 13);
            corrienteL1Label.TabIndex = 0;
            corrienteL1Label.Text = "I L1:";
            // 
            // corrienteL2Label
            // 
            corrienteL2Label.AutoSize = true;
            corrienteL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL2Label.Location = new System.Drawing.Point(29, 145);
            corrienteL2Label.Name = "corrienteL2Label";
            corrienteL2Label.Size = new System.Drawing.Size(33, 13);
            corrienteL2Label.TabIndex = 2;
            corrienteL2Label.Text = "I L2:";
            // 
            // corrienteL3Label
            // 
            corrienteL3Label.AutoSize = true;
            corrienteL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL3Label.Location = new System.Drawing.Point(29, 171);
            corrienteL3Label.Name = "corrienteL3Label";
            corrienteL3Label.Size = new System.Drawing.Size(33, 13);
            corrienteL3Label.TabIndex = 4;
            corrienteL3Label.Text = "I L3:";
            // 
            // corrienteNeutroLabel
            // 
            corrienteNeutroLabel.AutoSize = true;
            corrienteNeutroLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteNeutroLabel.Location = new System.Drawing.Point(5, 197);
            corrienteNeutroLabel.Name = "corrienteNeutroLabel";
            corrienteNeutroLabel.Size = new System.Drawing.Size(57, 13);
            corrienteNeutroLabel.TabIndex = 6;
            corrienteNeutroLabel.Text = "I Neutro:";
            // 
            // cosenoPhiL1Label
            // 
            cosenoPhiL1Label.AutoSize = true;
            cosenoPhiL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL1Label.Location = new System.Drawing.Point(336, 119);
            cosenoPhiL1Label.Name = "cosenoPhiL1Label";
            cosenoPhiL1Label.Size = new System.Drawing.Size(72, 13);
            cosenoPhiL1Label.TabIndex = 0;
            cosenoPhiL1Label.Text = "Cos Phi L1:";
            // 
            // cosenoPhiL2Label
            // 
            cosenoPhiL2Label.AutoSize = true;
            cosenoPhiL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL2Label.Location = new System.Drawing.Point(336, 145);
            cosenoPhiL2Label.Name = "cosenoPhiL2Label";
            cosenoPhiL2Label.Size = new System.Drawing.Size(72, 13);
            cosenoPhiL2Label.TabIndex = 2;
            cosenoPhiL2Label.Text = "Cos Phi L2:";
            // 
            // cosenoPhiL3Label
            // 
            cosenoPhiL3Label.AutoSize = true;
            cosenoPhiL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL3Label.Location = new System.Drawing.Point(336, 171);
            cosenoPhiL3Label.Name = "cosenoPhiL3Label";
            cosenoPhiL3Label.Size = new System.Drawing.Size(72, 13);
            cosenoPhiL3Label.TabIndex = 4;
            cosenoPhiL3Label.Text = "Cos Phi L3:";
            // 
            // cosenoPhiIIILabel
            // 
            cosenoPhiIIILabel.AutoSize = true;
            cosenoPhiIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiIIILabel.Location = new System.Drawing.Point(338, 197);
            cosenoPhiIIILabel.Name = "cosenoPhiIIILabel";
            cosenoPhiIIILabel.Size = new System.Drawing.Size(70, 13);
            cosenoPhiIIILabel.TabIndex = 6;
            cosenoPhiIIILabel.Text = "Cos Phi III:";
            // 
            // potenciaAparenteL1Label
            // 
            potenciaAparenteL1Label.AutoSize = true;
            potenciaAparenteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL1Label.Location = new System.Drawing.Point(326, 15);
            potenciaAparenteL1Label.Name = "potenciaAparenteL1Label";
            potenciaAparenteL1Label.Size = new System.Drawing.Size(82, 13);
            potenciaAparenteL1Label.TabIndex = 0;
            potenciaAparenteL1Label.Text = "Pot. Apar L1:";
            // 
            // potenciaAparenteL2Label
            // 
            potenciaAparenteL2Label.AutoSize = true;
            potenciaAparenteL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL2Label.Location = new System.Drawing.Point(326, 41);
            potenciaAparenteL2Label.Name = "potenciaAparenteL2Label";
            potenciaAparenteL2Label.Size = new System.Drawing.Size(82, 13);
            potenciaAparenteL2Label.TabIndex = 2;
            potenciaAparenteL2Label.Text = "Pot. Apar L2:";
            // 
            // potenciaAparenteL3Label
            // 
            potenciaAparenteL3Label.AutoSize = true;
            potenciaAparenteL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL3Label.Location = new System.Drawing.Point(326, 67);
            potenciaAparenteL3Label.Name = "potenciaAparenteL3Label";
            potenciaAparenteL3Label.Size = new System.Drawing.Size(82, 13);
            potenciaAparenteL3Label.TabIndex = 4;
            potenciaAparenteL3Label.Text = "Pot. Apar L3:";
            // 
            // potenciaAparenteIIILabel
            // 
            potenciaAparenteIIILabel.AutoSize = true;
            potenciaAparenteIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteIIILabel.Location = new System.Drawing.Point(328, 93);
            potenciaAparenteIIILabel.Name = "potenciaAparenteIIILabel";
            potenciaAparenteIIILabel.Size = new System.Drawing.Size(80, 13);
            potenciaAparenteIIILabel.TabIndex = 6;
            potenciaAparenteIIILabel.Text = "Pot. Apar III:";
            // 
            // potenciaReactivaCapicitivaL1Label
            // 
            potenciaReactivaCapicitivaL1Label.AutoSize = true;
            potenciaReactivaCapicitivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL1Label.Location = new System.Drawing.Point(155, 223);
            potenciaReactivaCapicitivaL1Label.Name = "potenciaReactivaCapicitivaL1Label";
            potenciaReactivaCapicitivaL1Label.Size = new System.Drawing.Size(78, 13);
            potenciaReactivaCapicitivaL1Label.TabIndex = 0;
            potenciaReactivaCapicitivaL1Label.Text = "Pot. Cap L1:";
            // 
            // potenciaReactivaCapicitivaL2Label
            // 
            potenciaReactivaCapicitivaL2Label.AutoSize = true;
            potenciaReactivaCapicitivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL2Label.Location = new System.Drawing.Point(155, 249);
            potenciaReactivaCapicitivaL2Label.Name = "potenciaReactivaCapicitivaL2Label";
            potenciaReactivaCapicitivaL2Label.Size = new System.Drawing.Size(78, 13);
            potenciaReactivaCapicitivaL2Label.TabIndex = 2;
            potenciaReactivaCapicitivaL2Label.Text = "Pot. Cap L2:";
            // 
            // potenciaReactivaCapicitivaL3Label
            // 
            potenciaReactivaCapicitivaL3Label.AutoSize = true;
            potenciaReactivaCapicitivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL3Label.Location = new System.Drawing.Point(155, 275);
            potenciaReactivaCapicitivaL3Label.Name = "potenciaReactivaCapicitivaL3Label";
            potenciaReactivaCapicitivaL3Label.Size = new System.Drawing.Size(78, 13);
            potenciaReactivaCapicitivaL3Label.TabIndex = 4;
            potenciaReactivaCapicitivaL3Label.Text = "Pot. Cap L3:";
            // 
            // potenciaCapacitivaIIILabel
            // 
            potenciaCapacitivaIIILabel.AutoSize = true;
            potenciaCapacitivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaCapacitivaIIILabel.Location = new System.Drawing.Point(157, 301);
            potenciaCapacitivaIIILabel.Name = "potenciaCapacitivaIIILabel";
            potenciaCapacitivaIIILabel.Size = new System.Drawing.Size(76, 13);
            potenciaCapacitivaIIILabel.TabIndex = 6;
            potenciaCapacitivaIIILabel.Text = "Pot. Cap III:";
            // 
            // potenciaReactivaInductivaL1Label
            // 
            potenciaReactivaInductivaL1Label.AutoSize = true;
            potenciaReactivaInductivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL1Label.Location = new System.Drawing.Point(159, 119);
            potenciaReactivaInductivaL1Label.Name = "potenciaReactivaInductivaL1Label";
            potenciaReactivaInductivaL1Label.Size = new System.Drawing.Size(74, 13);
            potenciaReactivaInductivaL1Label.TabIndex = 0;
            potenciaReactivaInductivaL1Label.Text = "Pot. Ind L1:";
            // 
            // potenciaReactivaInductivaL2Label
            // 
            potenciaReactivaInductivaL2Label.AutoSize = true;
            potenciaReactivaInductivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL2Label.Location = new System.Drawing.Point(159, 145);
            potenciaReactivaInductivaL2Label.Name = "potenciaReactivaInductivaL2Label";
            potenciaReactivaInductivaL2Label.Size = new System.Drawing.Size(74, 13);
            potenciaReactivaInductivaL2Label.TabIndex = 2;
            potenciaReactivaInductivaL2Label.Text = "Pot. Ind L2:";
            // 
            // potenciaReactivaInductivaL3Label
            // 
            potenciaReactivaInductivaL3Label.AutoSize = true;
            potenciaReactivaInductivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL3Label.Location = new System.Drawing.Point(159, 171);
            potenciaReactivaInductivaL3Label.Name = "potenciaReactivaInductivaL3Label";
            potenciaReactivaInductivaL3Label.Size = new System.Drawing.Size(74, 13);
            potenciaReactivaInductivaL3Label.TabIndex = 4;
            potenciaReactivaInductivaL3Label.Text = "Pot. Ind L3:";
            // 
            // potenciaInductivaIIILabel
            // 
            potenciaInductivaIIILabel.AutoSize = true;
            potenciaInductivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaInductivaIIILabel.Location = new System.Drawing.Point(161, 197);
            potenciaInductivaIIILabel.Name = "potenciaInductivaIIILabel";
            potenciaInductivaIIILabel.Size = new System.Drawing.Size(72, 13);
            potenciaInductivaIIILabel.TabIndex = 6;
            potenciaInductivaIIILabel.Text = "Pot. Ind III:";
            // 
            // corrienteIIILabel
            // 
            corrienteIIILabel.AutoSize = true;
            corrienteIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteIIILabel.Location = new System.Drawing.Point(31, 223);
            corrienteIIILabel.Name = "corrienteIIILabel";
            corrienteIIILabel.Size = new System.Drawing.Size(31, 13);
            corrienteIIILabel.TabIndex = 8;
            corrienteIIILabel.Text = "I III:";
            // 
            // tensionLineaL1L2Label
            // 
            tensionLineaL1L2Label.AutoSize = true;
            tensionLineaL1L2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaL1L2Label.Location = new System.Drawing.Point(357, 223);
            tensionLineaL1L2Label.Name = "tensionLineaL1L2Label";
            tensionLineaL1L2Label.Size = new System.Drawing.Size(51, 13);
            tensionLineaL1L2Label.TabIndex = 0;
            tensionLineaL1L2Label.Text = "V L1L2:";
            // 
            // tensionLineaL2L3Label
            // 
            tensionLineaL2L3Label.AutoSize = true;
            tensionLineaL2L3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaL2L3Label.Location = new System.Drawing.Point(357, 249);
            tensionLineaL2L3Label.Name = "tensionLineaL2L3Label";
            tensionLineaL2L3Label.Size = new System.Drawing.Size(51, 13);
            tensionLineaL2L3Label.TabIndex = 2;
            tensionLineaL2L3Label.Text = "V L2L3:";
            // 
            // tensionLineaL3L1Label
            // 
            tensionLineaL3L1Label.AutoSize = true;
            tensionLineaL3L1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaL3L1Label.Location = new System.Drawing.Point(357, 275);
            tensionLineaL3L1Label.Name = "tensionLineaL3L1Label";
            tensionLineaL3L1Label.Size = new System.Drawing.Size(51, 13);
            tensionLineaL3L1Label.TabIndex = 4;
            tensionLineaL3L1Label.Text = "V L3L1:";
            // 
            // tensionLineaIIILabel
            // 
            tensionLineaIIILabel.AutoSize = true;
            tensionLineaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaIIILabel.Location = new System.Drawing.Point(338, 301);
            tensionLineaIIILabel.Name = "tensionLineaIIILabel";
            tensionLineaIIILabel.Size = new System.Drawing.Size(70, 13);
            tensionLineaIIILabel.TabIndex = 6;
            tensionLineaIIILabel.Text = "V Linea III:";
            // 
            // frecuenciaLabel
            // 
            frecuenciaLabel.AutoSize = true;
            frecuenciaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            frecuenciaLabel.Location = new System.Drawing.Point(159, 327);
            frecuenciaLabel.Name = "frecuenciaLabel";
            frecuenciaLabel.Size = new System.Drawing.Size(74, 13);
            frecuenciaLabel.TabIndex = 0;
            frecuenciaLabel.Text = "Frecuencia:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.BackColor = System.Drawing.Color.Transparent;
            label8.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.ForeColor = System.Drawing.Color.Black;
            label8.Location = new System.Drawing.Point(5, 68);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(84, 19);
            label8.TabIndex = 54;
            label8.Text = "CVM-B150";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(12, 95);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(70, 13);
            label1.TabIndex = 55;
            label1.Text = "Serial Port:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(9, 275);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(77, 13);
            label2.TabIndex = 56;
            label2.Text = "Interval (ms)";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(18, 137);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(58, 13);
            label4.TabIndex = 58;
            label4.Text = "Periferic:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(18, 179);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(67, 13);
            label3.TabIndex = 62;
            label3.Text = "BaudRate:";
            // 
            // cosenoPhiIIITextBox
            // 
            this.cosenoPhiIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiIII", true));
            this.cosenoPhiIIITextBox.Location = new System.Drawing.Point(413, 193);
            this.cosenoPhiIIITextBox.Name = "cosenoPhiIIITextBox";
            this.cosenoPhiIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.cosenoPhiIIITextBox.TabIndex = 7;
            // 
            // bsAllVars
            // 
            this.bsAllVars.DataSource = typeof(Dezac.Instruments.Controls.UC_CVMB150.Vars);
            // 
            // cosenoPhiL3TextBox
            // 
            this.cosenoPhiL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL3", true));
            this.cosenoPhiL3TextBox.Location = new System.Drawing.Point(413, 167);
            this.cosenoPhiL3TextBox.Name = "cosenoPhiL3TextBox";
            this.cosenoPhiL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.cosenoPhiL3TextBox.TabIndex = 5;
            // 
            // cosenoPhiL2TextBox
            // 
            this.cosenoPhiL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL2", true));
            this.cosenoPhiL2TextBox.Location = new System.Drawing.Point(413, 141);
            this.cosenoPhiL2TextBox.Name = "cosenoPhiL2TextBox";
            this.cosenoPhiL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.cosenoPhiL2TextBox.TabIndex = 3;
            // 
            // cosenoPhiL1TextBox
            // 
            this.cosenoPhiL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL1", true));
            this.cosenoPhiL1TextBox.Location = new System.Drawing.Point(413, 115);
            this.cosenoPhiL1TextBox.Name = "cosenoPhiL1TextBox";
            this.cosenoPhiL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.cosenoPhiL1TextBox.TabIndex = 1;
            // 
            // corrienteIIITextBox
            // 
            this.corrienteIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteIII", true));
            this.corrienteIIITextBox.Location = new System.Drawing.Point(68, 219);
            this.corrienteIIITextBox.Name = "corrienteIIITextBox";
            this.corrienteIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteIIITextBox.TabIndex = 9;
            // 
            // corrienteNeutroTextBox
            // 
            this.corrienteNeutroTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteNeutroTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteNeutro", true));
            this.corrienteNeutroTextBox.Location = new System.Drawing.Point(68, 193);
            this.corrienteNeutroTextBox.Name = "corrienteNeutroTextBox";
            this.corrienteNeutroTextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteNeutroTextBox.TabIndex = 7;
            // 
            // corrienteL3TextBox
            // 
            this.corrienteL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL3", true));
            this.corrienteL3TextBox.Location = new System.Drawing.Point(68, 167);
            this.corrienteL3TextBox.Name = "corrienteL3TextBox";
            this.corrienteL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL3TextBox.TabIndex = 5;
            // 
            // corrienteL2TextBox
            // 
            this.corrienteL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL2", true));
            this.corrienteL2TextBox.Location = new System.Drawing.Point(68, 141);
            this.corrienteL2TextBox.Name = "corrienteL2TextBox";
            this.corrienteL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL2TextBox.TabIndex = 3;
            // 
            // corrienteL1TextBox
            // 
            this.corrienteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL1", true));
            this.corrienteL1TextBox.Location = new System.Drawing.Point(68, 115);
            this.corrienteL1TextBox.Name = "corrienteL1TextBox";
            this.corrienteL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL1TextBox.TabIndex = 1;
            // 
            // tensionNeutroTextBox
            // 
            this.tensionNeutroTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionNeutroTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionNeutro", true));
            this.tensionNeutroTextBox.Location = new System.Drawing.Point(68, 89);
            this.tensionNeutroTextBox.Name = "tensionNeutroTextBox";
            this.tensionNeutroTextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionNeutroTextBox.TabIndex = 9;
            // 
            // tensionFaseIIITextBox
            // 
            this.tensionFaseIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionFaseIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionFaseIII", true));
            this.tensionFaseIIITextBox.Location = new System.Drawing.Point(413, 323);
            this.tensionFaseIIITextBox.Name = "tensionFaseIIITextBox";
            this.tensionFaseIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionFaseIIITextBox.TabIndex = 7;
            // 
            // tensionL3TextBox
            // 
            this.tensionL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL3", true));
            this.tensionL3TextBox.Location = new System.Drawing.Point(68, 63);
            this.tensionL3TextBox.Name = "tensionL3TextBox";
            this.tensionL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL3TextBox.TabIndex = 5;
            // 
            // tensionL2TextBox
            // 
            this.tensionL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL2", true));
            this.tensionL2TextBox.Location = new System.Drawing.Point(68, 37);
            this.tensionL2TextBox.Name = "tensionL2TextBox";
            this.tensionL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL2TextBox.TabIndex = 3;
            // 
            // tensionL1TextBox
            // 
            this.tensionL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL1", true));
            this.tensionL1TextBox.Location = new System.Drawing.Point(68, 11);
            this.tensionL1TextBox.Name = "tensionL1TextBox";
            this.tensionL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL1TextBox.TabIndex = 1;
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(8, 231);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(79, 42);
            this.btnOn.TabIndex = 4;
            this.btnOn.Text = "Read";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(10, 111);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(75, 21);
            this.cmb_Port.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // facttorPotenciaIIITextBox
            // 
            this.facttorPotenciaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.facttorPotenciaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FacttorPotenciaIII", true));
            this.facttorPotenciaIIITextBox.Location = new System.Drawing.Point(68, 324);
            this.facttorPotenciaIIITextBox.Name = "facttorPotenciaIIITextBox";
            this.facttorPotenciaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.facttorPotenciaIIITextBox.TabIndex = 7;
            // 
            // factorPotenciaL3TextBox
            // 
            this.factorPotenciaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL3", true));
            this.factorPotenciaL3TextBox.Location = new System.Drawing.Point(68, 297);
            this.factorPotenciaL3TextBox.Name = "factorPotenciaL3TextBox";
            this.factorPotenciaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL3TextBox.TabIndex = 5;
            // 
            // factorPotenciaL2TextBox
            // 
            this.factorPotenciaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL2", true));
            this.factorPotenciaL2TextBox.Location = new System.Drawing.Point(68, 271);
            this.factorPotenciaL2TextBox.Name = "factorPotenciaL2TextBox";
            this.factorPotenciaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL2TextBox.TabIndex = 3;
            // 
            // factorPotenciaL1TextBox
            // 
            this.factorPotenciaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL1", true));
            this.factorPotenciaL1TextBox.Location = new System.Drawing.Point(68, 245);
            this.factorPotenciaL1TextBox.Name = "factorPotenciaL1TextBox";
            this.factorPotenciaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL1TextBox.TabIndex = 1;
            // 
            // potenciaActivaIIITextBox
            // 
            this.potenciaActivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaIII", true));
            this.potenciaActivaIIITextBox.Location = new System.Drawing.Point(238, 89);
            this.potenciaActivaIIITextBox.Name = "potenciaActivaIIITextBox";
            this.potenciaActivaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaIIITextBox.TabIndex = 7;
            // 
            // potenciaActivaL3TextBox
            // 
            this.potenciaActivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL3", true));
            this.potenciaActivaL3TextBox.Location = new System.Drawing.Point(238, 63);
            this.potenciaActivaL3TextBox.Name = "potenciaActivaL3TextBox";
            this.potenciaActivaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL3TextBox.TabIndex = 5;
            // 
            // potenciaActivaL2TextBox
            // 
            this.potenciaActivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL2", true));
            this.potenciaActivaL2TextBox.Location = new System.Drawing.Point(238, 37);
            this.potenciaActivaL2TextBox.Name = "potenciaActivaL2TextBox";
            this.potenciaActivaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL2TextBox.TabIndex = 3;
            // 
            // potenciaActivaL1TextBox
            // 
            this.potenciaActivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL1", true));
            this.potenciaActivaL1TextBox.Location = new System.Drawing.Point(238, 11);
            this.potenciaActivaL1TextBox.Name = "potenciaActivaL1TextBox";
            this.potenciaActivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL1TextBox.TabIndex = 1;
            // 
            // potenciaAparenteIIITextBox
            // 
            this.potenciaAparenteIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteIII", true));
            this.potenciaAparenteIIITextBox.Location = new System.Drawing.Point(413, 89);
            this.potenciaAparenteIIITextBox.Name = "potenciaAparenteIIITextBox";
            this.potenciaAparenteIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaAparenteIIITextBox.TabIndex = 7;
            // 
            // potenciaAparenteL3TextBox
            // 
            this.potenciaAparenteL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL3", true));
            this.potenciaAparenteL3TextBox.Location = new System.Drawing.Point(413, 63);
            this.potenciaAparenteL3TextBox.Name = "potenciaAparenteL3TextBox";
            this.potenciaAparenteL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaAparenteL3TextBox.TabIndex = 5;
            // 
            // potenciaAparenteL2TextBox
            // 
            this.potenciaAparenteL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL2", true));
            this.potenciaAparenteL2TextBox.Location = new System.Drawing.Point(413, 37);
            this.potenciaAparenteL2TextBox.Name = "potenciaAparenteL2TextBox";
            this.potenciaAparenteL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaAparenteL2TextBox.TabIndex = 3;
            // 
            // potenciaAparenteL1TextBox
            // 
            this.potenciaAparenteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL1", true));
            this.potenciaAparenteL1TextBox.Location = new System.Drawing.Point(413, 11);
            this.potenciaAparenteL1TextBox.Name = "potenciaAparenteL1TextBox";
            this.potenciaAparenteL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaAparenteL1TextBox.TabIndex = 1;
            // 
            // potenciaCapacitivaIIITextBox
            // 
            this.potenciaCapacitivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaCapacitivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaCapacitivaIII", true));
            this.potenciaCapacitivaIIITextBox.Location = new System.Drawing.Point(238, 297);
            this.potenciaCapacitivaIIITextBox.Name = "potenciaCapacitivaIIITextBox";
            this.potenciaCapacitivaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaCapacitivaIIITextBox.TabIndex = 7;
            // 
            // potenciaReactivaCapicitivaL3TextBox
            // 
            this.potenciaReactivaCapicitivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL3", true));
            this.potenciaReactivaCapicitivaL3TextBox.Location = new System.Drawing.Point(238, 271);
            this.potenciaReactivaCapicitivaL3TextBox.Name = "potenciaReactivaCapicitivaL3TextBox";
            this.potenciaReactivaCapicitivaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaCapicitivaL3TextBox.TabIndex = 5;
            // 
            // potenciaReactivaCapicitivaL2TextBox
            // 
            this.potenciaReactivaCapicitivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL2", true));
            this.potenciaReactivaCapicitivaL2TextBox.Location = new System.Drawing.Point(238, 245);
            this.potenciaReactivaCapicitivaL2TextBox.Name = "potenciaReactivaCapicitivaL2TextBox";
            this.potenciaReactivaCapicitivaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaCapicitivaL2TextBox.TabIndex = 3;
            // 
            // potenciaReactivaCapicitivaL1TextBox
            // 
            this.potenciaReactivaCapicitivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL1", true));
            this.potenciaReactivaCapicitivaL1TextBox.Location = new System.Drawing.Point(238, 219);
            this.potenciaReactivaCapicitivaL1TextBox.Name = "potenciaReactivaCapicitivaL1TextBox";
            this.potenciaReactivaCapicitivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaCapicitivaL1TextBox.TabIndex = 1;
            // 
            // potenciaInductivaIIITextBox
            // 
            this.potenciaInductivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaInductivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaInductivaIII", true));
            this.potenciaInductivaIIITextBox.Location = new System.Drawing.Point(238, 193);
            this.potenciaInductivaIIITextBox.Name = "potenciaInductivaIIITextBox";
            this.potenciaInductivaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaInductivaIIITextBox.TabIndex = 7;
            // 
            // potenciaReactivaInductivaL3TextBox
            // 
            this.potenciaReactivaInductivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaInductivaL3", true));
            this.potenciaReactivaInductivaL3TextBox.Location = new System.Drawing.Point(238, 167);
            this.potenciaReactivaInductivaL3TextBox.Name = "potenciaReactivaInductivaL3TextBox";
            this.potenciaReactivaInductivaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL3TextBox.TabIndex = 5;
            // 
            // potenciaReactivaInductivaL2TextBox
            // 
            this.potenciaReactivaInductivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaInductivaL2", true));
            this.potenciaReactivaInductivaL2TextBox.Location = new System.Drawing.Point(238, 141);
            this.potenciaReactivaInductivaL2TextBox.Name = "potenciaReactivaInductivaL2TextBox";
            this.potenciaReactivaInductivaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL2TextBox.TabIndex = 3;
            // 
            // potenciaReactivaInductivaL1TextBox
            // 
            this.potenciaReactivaInductivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaInductivaL1", true));
            this.potenciaReactivaInductivaL1TextBox.Location = new System.Drawing.Point(238, 115);
            this.potenciaReactivaInductivaL1TextBox.Name = "potenciaReactivaInductivaL1TextBox";
            this.potenciaReactivaInductivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL1TextBox.TabIndex = 1;
            // 
            // frecuenciaTextBox
            // 
            this.frecuenciaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frecuenciaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "Frecuencia", true));
            this.frecuenciaTextBox.Location = new System.Drawing.Point(238, 323);
            this.frecuenciaTextBox.Name = "frecuenciaTextBox";
            this.frecuenciaTextBox.Size = new System.Drawing.Size(79, 20);
            this.frecuenciaTextBox.TabIndex = 1;
            // 
            // tensionLineaIIITextBox
            // 
            this.tensionLineaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaIII", true));
            this.tensionLineaIIITextBox.Location = new System.Drawing.Point(413, 297);
            this.tensionLineaIIITextBox.Name = "tensionLineaIIITextBox";
            this.tensionLineaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaIIITextBox.TabIndex = 7;
            // 
            // tensionLineaL3L1TextBox
            // 
            this.tensionLineaL3L1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaL3L1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaL3L1", true));
            this.tensionLineaL3L1TextBox.Location = new System.Drawing.Point(413, 271);
            this.tensionLineaL3L1TextBox.Name = "tensionLineaL3L1TextBox";
            this.tensionLineaL3L1TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaL3L1TextBox.TabIndex = 5;
            // 
            // tensionLineaL2L3TextBox
            // 
            this.tensionLineaL2L3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaL2L3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaL2L3", true));
            this.tensionLineaL2L3TextBox.Location = new System.Drawing.Point(413, 245);
            this.tensionLineaL2L3TextBox.Name = "tensionLineaL2L3TextBox";
            this.tensionLineaL2L3TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaL2L3TextBox.TabIndex = 3;
            // 
            // tensionLineaL1L2TextBox
            // 
            this.tensionLineaL1L2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaL1L2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaL1L2", true));
            this.tensionLineaL1L2TextBox.Location = new System.Drawing.Point(413, 219);
            this.tensionLineaL1L2TextBox.Name = "tensionLineaL1L2TextBox";
            this.tensionLineaL1L2TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaL1L2TextBox.TabIndex = 1;
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.BackColor = System.Drawing.Color.DimGray;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.White;
            this.checkBox1.Location = new System.Drawing.Point(8, 315);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(79, 30);
            this.checkBox1.TabIndex = 45;
            this.checkBox1.Text = "Loop";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtInterval
            // 
            this.txtInterval.Location = new System.Drawing.Point(22, 292);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(50, 20);
            this.txtInterval.TabIndex = 46;
            this.txtInterval.Text = "1000";
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(95, 364);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(502, 13);
            this.pbStatus.TabIndex = 41;
            this.pbStatus.Visible = false;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(cosenoPhiIIILabel);
            this.panel1.Controls.Add(potenciaCapacitivaIIILabel);
            this.panel1.Controls.Add(potenciaAparenteIIILabel);
            this.panel1.Controls.Add(this.potenciaCapacitivaIIITextBox);
            this.panel1.Controls.Add(this.cosenoPhiIIITextBox);
            this.panel1.Controls.Add(facttorPotenciaIIILabel);
            this.panel1.Controls.Add(cosenoPhiL3Label);
            this.panel1.Controls.Add(this.corrienteIIITextBox);
            this.panel1.Controls.Add(corrienteL1Label);
            this.panel1.Controls.Add(potenciaReactivaCapicitivaL3Label);
            this.panel1.Controls.Add(this.cosenoPhiL3TextBox);
            this.panel1.Controls.Add(this.corrienteL2TextBox);
            this.panel1.Controls.Add(this.corrienteL1TextBox);
            this.panel1.Controls.Add(corrienteL2Label);
            this.panel1.Controls.Add(this.potenciaAparenteIIITextBox);
            this.panel1.Controls.Add(cosenoPhiL2Label);
            this.panel1.Controls.Add(this.corrienteL3TextBox);
            this.panel1.Controls.Add(this.cosenoPhiL1TextBox);
            this.panel1.Controls.Add(corrienteL3Label);
            this.panel1.Controls.Add(cosenoPhiL1Label);
            this.panel1.Controls.Add(this.corrienteNeutroTextBox);
            this.panel1.Controls.Add(this.cosenoPhiL2TextBox);
            this.panel1.Controls.Add(this.potenciaReactivaCapicitivaL3TextBox);
            this.panel1.Controls.Add(corrienteNeutroLabel);
            this.panel1.Controls.Add(potenciaAparenteL3Label);
            this.panel1.Controls.Add(corrienteIIILabel);
            this.panel1.Controls.Add(potenciaReactivaCapicitivaL2Label);
            this.panel1.Controls.Add(potenciaInductivaIIILabel);
            this.panel1.Controls.Add(this.potenciaReactivaCapicitivaL2TextBox);
            this.panel1.Controls.Add(this.potenciaAparenteL3TextBox);
            this.panel1.Controls.Add(potenciaReactivaCapicitivaL1Label);
            this.panel1.Controls.Add(this.potenciaReactivaCapicitivaL1TextBox);
            this.panel1.Controls.Add(this.facttorPotenciaIIITextBox);
            this.panel1.Controls.Add(potenciaAparenteL2Label);
            this.panel1.Controls.Add(potenciaActivaIIILabel);
            this.panel1.Controls.Add(this.potenciaAparenteL2TextBox);
            this.panel1.Controls.Add(factorPotenciaL3Label);
            this.panel1.Controls.Add(potenciaAparenteL1Label);
            this.panel1.Controls.Add(this.potenciaInductivaIIITextBox);
            this.panel1.Controls.Add(this.potenciaAparenteL1TextBox);
            this.panel1.Controls.Add(this.factorPotenciaL3TextBox);
            this.panel1.Controls.Add(factorPotenciaL2Label);
            this.panel1.Controls.Add(potenciaReactivaInductivaL3Label);
            this.panel1.Controls.Add(this.factorPotenciaL2TextBox);
            this.panel1.Controls.Add(this.potenciaActivaIIITextBox);
            this.panel1.Controls.Add(factorPotenciaL1Label);
            this.panel1.Controls.Add(this.factorPotenciaL1TextBox);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL3TextBox);
            this.panel1.Controls.Add(frecuenciaLabel);
            this.panel1.Controls.Add(potenciaReactivaInductivaL2Label);
            this.panel1.Controls.Add(potenciaActivaL3Label);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL2TextBox);
            this.panel1.Controls.Add(tensionNeutroLabel);
            this.panel1.Controls.Add(potenciaReactivaInductivaL1Label);
            this.panel1.Controls.Add(this.potenciaActivaL3TextBox);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL1TextBox);
            this.panel1.Controls.Add(potenciaActivaL2Label);
            this.panel1.Controls.Add(tensionLineaIIILabel);
            this.panel1.Controls.Add(this.potenciaActivaL2TextBox);
            this.panel1.Controls.Add(potenciaActivaL1Label);
            this.panel1.Controls.Add(this.potenciaActivaL1TextBox);
            this.panel1.Controls.Add(this.tensionNeutroTextBox);
            this.panel1.Controls.Add(this.frecuenciaTextBox);
            this.panel1.Controls.Add(tensionFaseIIILabel);
            this.panel1.Controls.Add(this.tensionLineaIIITextBox);
            this.panel1.Controls.Add(tensionLineaL3L1Label);
            this.panel1.Controls.Add(this.tensionFaseIIITextBox);
            this.panel1.Controls.Add(this.tensionLineaL3L1TextBox);
            this.panel1.Controls.Add(tensionL3Label);
            this.panel1.Controls.Add(tensionLineaL2L3Label);
            this.panel1.Controls.Add(tensionL1Label);
            this.panel1.Controls.Add(this.tensionLineaL1L2TextBox);
            this.panel1.Controls.Add(this.tensionLineaL2L3TextBox);
            this.panel1.Controls.Add(this.tensionL1TextBox);
            this.panel1.Controls.Add(tensionLineaL1L2Label);
            this.panel1.Controls.Add(this.tensionL3TextBox);
            this.panel1.Controls.Add(this.tensionL2TextBox);
            this.panel1.Controls.Add(tensionL2Label);
            this.panel1.Location = new System.Drawing.Point(93, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(507, 352);
            this.panel1.TabIndex = 50;
            // 
            // cmb_Periferic
            // 
            this.cmb_Periferic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Periferic.FormattingEnabled = true;
            this.cmb_Periferic.Location = new System.Drawing.Point(10, 153);
            this.cmb_Periferic.Name = "cmb_Periferic";
            this.cmb_Periferic.Size = new System.Drawing.Size(75, 21);
            this.cmb_Periferic.TabIndex = 57;
            this.cmb_Periferic.SelectedIndexChanged += new System.EventHandler(this.OnChangePeriferic);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.circutor1;
            this.pictureBox1.Location = new System.Drawing.Point(11, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(74, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 60;
            this.pictureBox1.TabStop = false;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.BackgroundImage = global::Dezac.Instruments.Properties.Resources.Actions_exit_icon;
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.Transparent;
            this.btnReset.Location = new System.Drawing.Point(30, 346);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(35, 36);
            this.btnReset.TabIndex = 6;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // comboBoxBaudRate
            // 
            this.comboBoxBaudRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBaudRate.FormattingEnabled = true;
            this.comboBoxBaudRate.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.comboBoxBaudRate.Location = new System.Drawing.Point(10, 195);
            this.comboBoxBaudRate.Name = "comboBoxBaudRate";
            this.comboBoxBaudRate.Size = new System.Drawing.Size(75, 21);
            this.comboBoxBaudRate.TabIndex = 61;
            // 
            // UC_CVMB150
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(label3);
            this.Controls.Add(this.comboBoxBaudRate);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(label4);
            this.Controls.Add(this.cmb_Periferic);
            this.Controls.Add(label2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(label1);
            this.Controls.Add(this.txtInterval);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(label8);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnOn);
            this.Controls.Add(this.panel1);
            this.Name = "UC_CVMB150";
            this.Size = new System.Drawing.Size(611, 385);
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.BindingSource bsAllVars;
        private System.Windows.Forms.TextBox tensionNeutroTextBox;
        private System.Windows.Forms.TextBox tensionFaseIIITextBox;
        private System.Windows.Forms.TextBox tensionL3TextBox;
        private System.Windows.Forms.TextBox tensionL2TextBox;
        private System.Windows.Forms.TextBox tensionL1TextBox;
        private System.Windows.Forms.TextBox potenciaActivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaActivaL3TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL1TextBox;
        private System.Windows.Forms.TextBox facttorPotenciaIIITextBox;
        private System.Windows.Forms.TextBox factorPotenciaL3TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL2TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL1TextBox;
        private System.Windows.Forms.TextBox cosenoPhiIIITextBox;
        private System.Windows.Forms.TextBox cosenoPhiL3TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL2TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL1TextBox;
        private System.Windows.Forms.TextBox corrienteIIITextBox;
        private System.Windows.Forms.TextBox corrienteNeutroTextBox;
        private System.Windows.Forms.TextBox corrienteL3TextBox;
        private System.Windows.Forms.TextBox corrienteL2TextBox;
        private System.Windows.Forms.TextBox corrienteL1TextBox;
        private System.Windows.Forms.TextBox frecuenciaTextBox;
        private System.Windows.Forms.TextBox tensionLineaIIITextBox;
        private System.Windows.Forms.TextBox tensionLineaL3L1TextBox;
        private System.Windows.Forms.TextBox tensionLineaL2L3TextBox;
        private System.Windows.Forms.TextBox tensionLineaL1L2TextBox;
        private System.Windows.Forms.TextBox potenciaInductivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL3TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL1TextBox;
        private System.Windows.Forms.TextBox potenciaCapacitivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL3TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL1TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteIIITextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL3TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL2TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL1TextBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmb_Periferic;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBoxBaudRate;
    }
}
