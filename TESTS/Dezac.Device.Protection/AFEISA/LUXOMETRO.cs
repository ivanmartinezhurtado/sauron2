﻿using Dezac.Core.Utility;
using System;
using System.Runtime.InteropServices;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.00)]
    public class LUXOMETRO : DeviceBase
    {
        public enum EscalaLux
        {
            Escala200,
            Escala2000,
            Escala8000,
            Ajuste,
        }

        public LUXOMETRO()
        {
        }

        public LUXOMETRO(int port = 5, byte perifericNumber = 1)
        {
            SetPort(port, perifericNumber);
        }

        public void SetPort(int port, byte perifericNumber, int baudRate = 38400)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

           Modbus.PerifericNumber = perifericNumber;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            LogMethod();
            Modbus.Dispose();
        }

        public ushort ReadTestCOM()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TEST_COM);
        }

        public double ReadMedidaLuxometro()
        {
            LogMethod();
            return (double)Modbus.ReadInt32((ushort)Registers.MEDIDA_LUXOMETRO) / 10D;
        }

        public uint ReadSerialNumber()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public ushort ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION);
        }

        public ushort ReadGainFactor(EscalaLux escala)
        {
            LogMethod();
            var register = escala == EscalaLux.Escala2000 ? Registers.FACTOR_CALIBRATION_VERIFICAR2 : escala == EscalaLux.Escala8000 ? Registers.FACTOR_CALIBRATION_VERIFICAR3 : Registers.FACTOR_CALIBRATION_VERIFICAR;
            return Modbus.ReadRegister((ushort)register);
        }

        public void WriteGainFactor(EscalaLux escala, ushort factor)
        {
            LogMethod();
            var register = escala == EscalaLux.Escala2000 ? Registers.FACTOR_CALIBRATION_VERIFICAR2 : escala == EscalaLux.Escala8000 ? Registers.FACTOR_CALIBRATION_VERIFICAR3 : Registers.FACTOR_CALIBRATION_VERIFICAR;
            Modbus.Write((ushort)register, factor);
        }

        public Tuple<bool, double> AdjustFactors(EscalaLux escala, int delFirst, int initCount, int samples, int interval, double luxReference, Func<double, bool> adjustValidation)
        {
            LogMethod();
            var gainFactor = ReadGainFactor(escala);
            var newGain = 0D;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 15,
                () =>
                {
                    var valor = ReadMedidaLuxometro();
                    return new double[] { valor };
                },
                (listValues) =>
                {
                    newGain =  (luxReference / listValues.Average(0)) * gainFactor;
                    return adjustValidation(newGain);
                });

            return Tuple.Create(adjustResult.Item1, newGain);
        }

        public void WriteValidationChange()
        {
            LogMethod();

            var value = "DE2ACFAB";
            var data = value.HexStringToBytes();

            Modbus.WriteString((ushort)Registers.VALIDATION_CHANGES, "DE2ACFAB");   
            Modbus.WriteBytes((ushort)Registers.VALIDATION_CHANGES, data);
        }

        //***************************************
        //******* GPS **************************

        public void WriteFase(uint valueFase)
        {
            //if fase == AJUSTAR fase = 0039  sino es null ???
            //If ComRS.ComRS("06002F" & fase, "06002F" & fase, RespCom) = False Then
            LogMethod();

            Modbus.UseFunction06 = true;
            Modbus.Write((ushort)Registers.WRITE_FASE, valueFase);
        }


        public void WriteSerialNumber(uint serialnumber)
        {
            LogMethod();
            Modbus.UseFunction06 = true;
            Modbus.Write((ushort)Registers.SERIAL_NUMBER, serialnumber);
        }


        public struct GPSInfo
        {
            public ushort status;
            public ushort latitud;
            public ushort longitud;
            public ushort StatlitsNumber;
            public ushort StalitsAverage;
            public DateTime datetime;
        }

        public GPSInfo ReadGPSInformation()
        {
            var info = new GPSInfo()
            {
                status = ReadStatusGPS(),
                latitud = ReadLatitudGPS(),
                longitud = ReadLongitudGPS(),
                StalitsAverage = ReadSatelisNumberAverage(),
                StatlitsNumber = ReadSatelisNumber(),
                datetime = ReadDateTime()
            };
            return info;
        }

        public ushort ReadStatusGPS()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.STATUS_GPS);
        }

        public ushort ReadLongitudGPS()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.LONGITUD_GPS);
        }

        public ushort ReadLatitudGPS()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.LATITUS_GPS);
        }

        public ushort ReadSatelisNumber()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.NUMERO_SATELITES);
        }

        public ushort ReadSatelisNumberAverage()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.MEDIA_SATELITES);
        }

        public DateTime ReadDateTime()
        {
            LogMethod();
            return Modbus.Read<FechaHora>((ushort)Registers.DATE_TIME).ToDateTime();
        }

        public void WriteDateTime()
        {
            LogMethod();
            Modbus.Write<FechaHora>((ushort)Registers.DATE_TIME, FechaHora.Create());
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FechaHora
        {
            public UInt16 Day;
            public UInt16 Month;
            public UInt16 Year;
            public UInt16 Hour;
            public UInt16 Minutes;
            public UInt16 Seconds;

            public static FechaHora Create(DateTime? date = null)
            {
                DateTime fecha = date.GetValueOrDefault(DateTime.Now);

                FechaHora value = new FechaHora
                {
                    Year = (ushort)fecha.Year,
                    Month = (ushort)fecha.Month,
                    Day = (ushort)fecha.Day,
                    Hour = (ushort)fecha.Hour,
                    Minutes = (ushort)fecha.Minute,
                    Seconds = (ushort)fecha.Second,
                };

                return value;
            }

            public DateTime ToDateTime()
            {
                return new DateTime(Year + 2000, Month, Day, Hour, Minutes, Seconds);
            }
        }

        public enum Registers
        {
            SERIAL_NUMBER = 0x0002,
            TEST_COM = 0x0001, //00010001
            MEDIDA_LUXOMETRO = 0x0008,
            FIRMWARE_VERSION = 0x0000,
            FACTOR_CALIBRATION_VERIFICAR = 0x0021,
            FACTOR_CALIBRATION_VERIFICAR2 = 0x0020,
            FACTOR_CALIBRATION_VERIFICAR3 = 0x001F,
            VALIDATION_CHANGES = 0x0028,
            WRITE_FASE = 0x002F,

            STATUS_GPS = 0x0011,
            LATITUS_GPS = 0x0012,
            LONGITUD_GPS = 0x0015,
            NUMERO_SATELITES = 0x001C,
            MEDIA_SATELITES = 0x001D,
            DATE_TIME = 0x000A
        }
    }
}
