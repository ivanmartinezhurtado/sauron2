﻿using Dezac.Tests.UserControls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Tests.Forms
{
    public partial class MsgBox : Form
    {
        private IDialogView dialogControl;

        public MsgBox()
        {
            InitializeComponent();
        }

        public MsgBox(Control control)
            : this(control.Text, control, MessageBoxButtons.OK)
        {
        }

        public MsgBox(string title, Control control, MessageBoxButtons? buttons, string description="", int waitTimeAccept = 2000)
            : this(title, control, buttons)
        {
            ChangeStatusButtons(false);

            if (!string.IsNullOrEmpty(description))
                lblDecripction.Text = description;

            timer1.Interval = waitTimeAccept;
            timer1.Enabled = true;
        }

        public MsgBox(string title, Control control, MessageBoxButtons? buttons)
            : this()
        {
            this.Text = title;

            this.Size = new Size(control.Width + 10, control.Height + footer.Height + 50);

            control.Dock = DockStyle.Fill;
            content.Controls.Add(control);

            dialogControl = control as IDialogView;
            if (dialogControl != null)
                dialogControl.CloseDialog += OnCloseView;

            if (buttons != null)
            {
                OK.Visible = buttons == MessageBoxButtons.OK || buttons == MessageBoxButtons.OKCancel;
                Yes.Visible = buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel;
                No.Visible = buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel;
                Cancel.Visible = buttons == MessageBoxButtons.OKCancel || buttons == MessageBoxButtons.YesNoCancel;

                if (buttons == MessageBoxButtons.OK || buttons == MessageBoxButtons.OKCancel)
                    AcceptButton = OK;
                else if (buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel)
                    AcceptButton = Yes;
            }else
            {
                OK.Visible = false;
                Yes.Visible = false;
                No.Visible = false;
                Cancel.Visible = false;
            }

            CancelButton = Cancel;
        }

        public static MsgBox Show(string title, Control control, MessageBoxButtons? buttons)
        {
            var form = new MsgBox(title, control, buttons);
            form.ActiveControl = control;
            form.Show();
            return form;
        }

        [Browsable(false)]
        public Control Content { get { return content; } }

        [Browsable(false)]
        public Button OK { get { return cmdOK; } }
        [Browsable(false)]
        public Button Yes { get { return cmdYes; } }
        [Browsable(false)]
        public Button No { get { return cmdNo; } }
        [Browsable(false)]
        public Button Cancel { get { return cmdCancel; } }

        public void OnButtonClicked(object sender, EventArgs e)
        {
            InternalClose(((Button)sender).DialogResult);
        }

        void OnCloseView(object sender, DialogResultEventArgs e)
        {
            InternalClose(e.Result);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (dialogControl != null)
                e.Cancel = !dialogControl.ValidateView(this.DialogResult);
        }

        private void InternalClose(DialogResult result)
        {
            this.DialogResult = result;
            Close();
        }

        private void ChangeStatusButtons(bool status)
        {
            OK.Enabled = status;
            Yes.Enabled = status;
            No.Enabled = status;
            Cancel.Enabled =status;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            ChangeStatusButtons(true);
        }
    }
}
