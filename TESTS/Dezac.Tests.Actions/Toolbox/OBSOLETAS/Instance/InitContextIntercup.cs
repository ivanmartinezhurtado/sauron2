﻿using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(InitContextIntercupDescription))]
    
    public class InitContextIntercup : ActionBase
    {
        public string Lote { get; set; }
        public string Codigo { get; set; }
        public string Empleado { get; set; }
        public int cantidad { get; set; }
        public string  cliente { get; set; }
        public override void Execute()
        {
            var context = SequenceContext.Current;

            if (context.RunMode == RunMode.Release)
                return;
            var data = new TestContextModel
            {
                IdEmpleado = Empleado != null ? Empleado : "999",
                IdOperario = Empleado != null ? Empleado : "999",
                modoPlayer = ModoTest.Normal,
                Cantidad = cantidad,
                Lote = Lote,
                Codigo = Codigo,
                Cliente = cliente,
                IdFase = "120"
            };

            Context.Services.Add<ITestContext>(data);
        }
    }

    public class InitContextIntercupDescription : ActionDescription
    {
        public override string Name { get { return "InitContextIntercup"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.kernel; } }
    }
}