﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using System;
using Dezac.Tests.Extensions;
using Instruments.Utility;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.09)]
    public class CVMB1XXTestModulosGenerico : TestBase
    {
        protected CVMB1XX cvm;
        public CVMB1XX CVMDummy
        {
            get { return cvm;  }
            set  {  cvm = value;  }
        }

        public virtual void TestDetectionModule()
        {
            if (CVMDummy == null)
            {
                var conversor = this.WMISerialPortInitialize("USB Serial Port");
                var Port = Convert.ToByte(conversor.Replace("COM", ""));

                CVMDummy = GetVariable<CVMB1XX>("UUT", cvm);

                if (CVMDummy == null)
                    CVMDummy = new CVMB1XX(Port);

                CVMDummy.Modbus.PortCom = Port;
                CVMDummy.Modbus.PerifericNumber = Comunicaciones.Periferico;
            }
            //Delay(4000, "Esperamos arranque del equipo");

            SamplerWithCancel((p) => { CVMDummy.ReadDetectionModulo(); return true; }, "Error de comunicaciones con el equipo, no hemos recibido respuesta", 15, 1000, 0, false, false);

            var fw = CVMDummy.ReadSoftwareVersion().CToNetString();
            Assert.AreEqual("VERSION_DUMMY", fw, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("La versión del Dummy es incorrecta"), ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { CVMDummy.ReadDetectionModulo(); return true; }, "Error de comunicaciones con el equipo, no hemos recibido respuesta", 3, 500, 0, false, false);

            var modulo = CVMDummy.ReadDetectionModulo();

            Assert.IsTrue(modulo.ToString() == Identificacion.MODELO.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE(string.Format("Error, modulo detectado {0} no corresponde con el esperado {1}", modulo, Identificacion.MODELO.Trim())));

            //CVMDummy.FlagTest();
            var boot = CVMDummy.ReadVersionBootModulo();

            var versionBootModulo = Identificacion.VERSION_BOOT_MODULO.Split('.');
            var versionBoot = Identificacion.VERSION_BOOT;

            Resultado.Set(ConstantsParameters.Identification.VERSION_BOOT_MODULO, string.Format("{0}/{1}.{2}.{3}", boot[0], boot[1], boot[2], boot[3]), ParamUnidad.SinUnidad,
                string.Format("{0}/{1}.{2}.{3}", versionBootModulo, versionBootModulo[0], versionBootModulo[1], versionBootModulo[2]));

            Assert.IsTrue(boot[0] == Convert.ToByte(versionBoot) && boot[1] == Convert.ToByte(versionBootModulo[0]) && boot[2] == Convert.ToByte(versionBootModulo[1]) && boot[3] == Convert.ToByte(versionBootModulo[2]),
            Error().UUT.CONFIGURACION.NO_COINCIDE(string.Format("Error, version boot detectado modulo:{0} version={1}.{2}.{3} incorrecto, se esperaba {4}.{5}.{6}", boot[0], boot[1], boot[2], boot[3], versionBootModulo[0], versionBootModulo[1], versionBootModulo[2])));
        }

        public virtual void TestFlashAndWriteSerielNumber()
        {         
            SamplerWithCancel((p) =>
                {
                    CVMDummy.ReadSoftwareVersion();
                    return true;
                }, "No se puede comunicar con el equipo", 10, 1500);

            //CVMDummy.Reset();
            //Delay(30000, "Esperamos al reinicio del equipo");

            //SamplerWithCancel((p) =>
            //{
            //    CVMDummy.ReadSoftwareVersion();
            //    return true;
            //}, "No se puede comunicar con el equipo", 10, 1500);
            
            CVMDummy.WriteNumSerieModulo(Convert.ToInt64(TestInfo.NumSerie));

            var ns = CVMDummy.ReadNumSerieModulo();
            logger.DebugFormat("numero serie leido despues de escribir --> {0}", ns);

            Delay(5000, "Esrcibimos Numero de Serie");
            
            CVMDummy.Reset();
            
            Delay(30000, "Esperamos al reinicio del equipo");

            ushort modulo = 0;

            SamplerWithCancel((p) => { modulo = CVMDummy.ReadDetectionModulo(); return true; }, "Error de comunicaciones con el equipo, no hemos recibido respuesta despues del reset", 5, 1000);

            Resultado.Set(ConstantsParameters.Identification.MODELO, modulo.ToString(), ParamUnidad.SinUnidad);

            Assert.IsTrue(modulo.ToString() == Identificacion.MODELO, Error().UUT.HARDWARE.NO_COINCIDE(string.Format("Error, modulo detectado {0} no corresponde con el esperado {1} después del Reset de Hardware", modulo, Identificacion.MODELO)));

            var numseriemodul = CVMDummy.ReadNumSerieModulo();

            Resultado.Set(ConstantsParameters.TestInfo.NUM_SERIE, numseriemodul, ParamUnidad.SinUnidad, TestInfo.NumSerie.ToString());

            Assert.AreEqual(Convert.ToInt64(TestInfo.NumSerie), Convert.ToInt64(numseriemodul), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error numero de serie leido del modulo conectado no corresponde con el grabado en la placa"));
        }

        public void TestIncrementDate()
        {
            SamplerWithCancel((p) =>
            {
                var dateIni = CVMDummy.Modbus.Read<CVMB1XX.FechaHora>((ushort)CVMB1XX.Registers.FECHA_MODULO);

                Delay(2500, "Esperamos para ver el incremento del reloj del modulo");

                var dateFin = CVMDummy.Modbus.Read<CVMB1XX.FechaHora>((ushort)CVMB1XX.Registers.FECHA_MODULO);

                Assert.IsTrue((dateFin.ToDateTime() - dateIni.ToDateTime()).Seconds > 0, Error().UUT.CONFIGURACION.SETUP("Error fecha leida del modulo incorrecta o no ha incrementado"));

                return true;
            }, "", 2, 1000, 0, false, false);
        }

        public void TestConnectors()
        {
            CVMDummy.FlagTest();

            SamplerWithCancel((p) =>
            {
                return CVMDummy.Modbus.Read<CVMB1XX.Conector>().IsValid;
            }, "Error en el conector del modulo al probar el test de final de linia", 5, 1000);
        }

        public override void Dispose()
        {
            if (CVMDummy != null)
                CVMDummy.Dispose();
        }
        
        public void TestDispose()
        {
            if (CVMDummy != null)
                CVMDummy.Dispose();
        }
    }
}
