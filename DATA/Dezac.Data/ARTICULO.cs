namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.ARTICULO")]
    public partial class ARTICULO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ARTICULO()
        {
            ARTICULOBLOQUEO = new HashSet<ARTICULOBLOQUEO>();
            ARTICULOCLIENTE = new HashSet<ARTICULOCLIENTE>();
            ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            ORDEN = new HashSet<ORDEN>();
            PRODUCTO = new HashSet<PRODUCTO>();
            PRODUCTOCOMPONENTE = new HashSet<PRODUCTOCOMPONENTE>();
            PROYECTO1 = new HashSet<PROYECTO>();
            T_IRISERRORPRODUCTOFASE = new HashSet<T_IRISERRORPRODUCTOFASE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMARTICULO { get; set; }

        [StringLength(20)]
        public string CLACODIGO { get; set; }

        [StringLength(50)]
        public string CLADESC { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [StringLength(20)]
        public string CODIGOBCN { get; set; }

        [StringLength(50)]
        public string DESCBCN { get; set; }

        [StringLength(20)]
        public string CODIGOTERRASSA { get; set; }

        [StringLength(50)]
        public string DESCTERRASSA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTIPOARTICULO { get; set; }

        public DateTime? FECHAALTA { get; set; }

        [StringLength(10)]
        public string IDIVA { get; set; }

        [StringLength(10)]
        public string IDPROVEEDOR { get; set; }

        [StringLength(10)]
        public string IDMARCA { get; set; }

        public int? NUMXREF { get; set; }

        [StringLength(10)]
        public string IDALMACENCALIDAD { get; set; }

        [Required]
        [StringLength(8)]
        public string CUENTACOMPRA { get; set; }

        [Required]
        [StringLength(8)]
        public string CUENTAVENTA { get; set; }

        [Required]
        [StringLength(8)]
        public string CUENTAFABRICACION { get; set; }

        [StringLength(20)]
        public string EXTXREF { get; set; }

        [StringLength(1)]
        public string INVENTARIABLE { get; set; }

        public decimal EXISTENCIA { get; set; }

        public decimal ORDENADO { get; set; }

        public decimal RESERVADO { get; set; }

        public decimal DISPONIBLE { get; set; }

        [Required]
        [StringLength(1)]
        public string CONTROLCALIDAD { get; set; }

        [StringLength(1)]
        public string CARGABLE { get; set; }

        public decimal? PRECIOSTANDARD { get; set; }

        public decimal? PRECIOMEDIO { get; set; }

        public decimal? PRECIOULTIMO { get; set; }

        public decimal? PRECIOTEORICO { get; set; }

        public decimal? PRECIOVENTA { get; set; }

        [Required]
        [StringLength(1)]
        public string ACTIVO { get; set; }

        [StringLength(200)]
        public string OBSERVING { get; set; }

        [StringLength(500)]
        public string OBSERVCOMPRA { get; set; }

        [StringLength(200)]
        public string OBSERVVENTA { get; set; }

        [StringLength(200)]
        public string OBSERVALMACEN { get; set; }

        [StringLength(12)]
        public string CODCLAPROV { get; set; }

        [Required]
        [StringLength(1)]
        public string UDSPEDMODREC_SN { get; set; }

        [Required]
        [StringLength(1)]
        public string DOCUMENTABLE { get; set; }

        [Required]
        [StringLength(1)]
        public string PREVISIONABLEVTA { get; set; }

        [Required]
        [StringLength(1)]
        public string DEPOSITOCLIENTE { get; set; }

        public decimal EXISTENCIADEPOSITOCLIENTE { get; set; }

        public int? NUMPROYECTO { get; set; }

        [StringLength(3)]
        public string IDFAMILIAPRODUCCION { get; set; }

        [StringLength(1)]
        public string ROHS { get; set; }

        public decimal? PESO { get; set; }

        public int? LEADTIME { get; set; }

        public int? UDSEMBALAJE { get; set; }

        public int? LOTEOPTIMO { get; set; }

        [Required]
        [StringLength(1)]
        public string CRITICO { get; set; }

        [StringLength(1)]
        public string TIPOABC_MA { get; set; }

        public int? DESTINOSFINALACTIVOS { get; set; }

        [StringLength(500)]
        public string FICHEROIMAGEN { get; set; }

        public int? IMAGEWIDTH { get; set; }

        public int? IMAGEHEIGHT { get; set; }

        [StringLength(1)]
        public string AVISOPPC { get; set; }

        [Required]
        [StringLength(1)]
        public string RETIRASTOCKBULTO { get; set; }

        [StringLength(500)]
        public string NOTACMC { get; set; }

        [StringLength(10)]
        public string TARICCOD { get; set; }

        public decimal? TARICPCT { get; set; }

        [StringLength(10)]
        public string IDGRUPO { get; set; }

        [StringLength(50)]
        public string DESCRIPCIONCOMERCIAL { get; set; }

        [StringLength(10)]
        public string IDMARCAVENTA { get; set; }

        [StringLength(10)]
        public string STATUS { get; set; }

        [StringLength(9)]
        public string NUMSUSTITUTO { get; set; }

        [StringLength(15)]
        public string REFVENTA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARTICULOBLOQUEO> ARTICULOBLOQUEO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARTICULOCLIENTE> ARTICULOCLIENTE { get; set; }

        public virtual PROYECTO PROYECTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDEN> ORDEN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTO> PRODUCTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTOCOMPONENTE> PRODUCTOCOMPONENTE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROYECTO> PROYECTO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_IRISERRORPRODUCTOFASE> T_IRISERRORPRODUCTOFASE { get; set; }
    }
}
