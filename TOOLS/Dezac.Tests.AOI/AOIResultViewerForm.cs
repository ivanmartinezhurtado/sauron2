﻿using Comunications.Ethernet;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.AOI.Models;
using HalconDotNet;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Dezac.Tests.AOI
{
    public partial class AOIResultViewerForm : RadForm
    {
        private static BindingList<AOIViewerResults> resultList = new BindingList<AOIViewerResults>();
        private List<int> notOKIndexes;
        private int index;
        private List<string> resultFiles;
        private AOIStatistics statistics;
        private SynchronizationContext sc;
        private string numOrden;
        private string orderPath;
        private string idFase;
        AOIViewerResults resultsToSave;

        protected static readonly ILog logger = LogManager.GetLogger("AOIResultViewer");

        public AOIResultViewerForm(string _numOrden, string _idFase)
        {
            InitializeComponent();

            bsList.DataSource = resultList;

            resultFiles = new List<string>();
            notOKIndexes = new List<int>();
            statistics = new AOIStatistics();

            sc = SynchronizationContext.Current;

            idFase = _idFase;
            numOrden = _numOrden;
            labelNumOrden.Text = numOrden;

            orderPath = ConfigurationManager.AppSettings["PathAOIResultsSave"] + numOrden + "\\";

            UpdateStatistics(false);

            BlockButtons();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (Screen.AllScreens.Length > 1)
            {
                var screen = Screen.AllScreens[0].WorkingArea;
                this.SetDesktopLocation(screen.Left, screen.Top);
                this.WindowState = FormWindowState.Maximized;
            }

            timer1.Enabled = true;
            timer1.Interval = 1000;
            btnBolaEstano.Enabled = false;
            btnCable.Enabled = false;
            btnCruce.Enabled = false;
            btnFalsoFallo.Enabled = false;
            btnFaltaEstano.Enabled = false;
            btnNext.Enabled = false;
            btnNoReparado.Enabled = false;
            btnOtroFallo.Enabled = false;
            btnPrevious.Enabled = false;
            btnSinComponente.Enabled = false;
        }

        private AOIViewerResults Current { get { return bsList.Current as AOIViewerResults; }}

        private void bsList_CurrentChanged(object sender, EventArgs e)
        {
            if (Current != null)
            {
                notOKIndexes = Enumerable.Range(0, Current.InspectionResults.WeldingsResult.Count)
                    .Where(i => Current.InspectionResults.WeldingsResult[i] != ResultEnum.OK).ToList();

                SetPosition(0);

                btnBolaEstano.Enabled = true;
                btnCable.Enabled = true;
                btnCruce.Enabled = true;
                btnFalsoFallo.Enabled = true;
                btnFaltaEstano.Enabled = true;
                btnNext.Enabled = true;
                btnNoReparado.Enabled = true;
                btnOtroFallo.Enabled = true;
                btnPrevious.Enabled = true;
                btnSinComponente.Enabled = true;
            }
            else
            {
                hWindow1.HalconWindow.ClearWindow();
                hWindow2.HalconWindow.ClearWindow();
                btnBolaEstano.Enabled = false;
                btnCable.Enabled = false;
                btnCruce.Enabled = false;
                btnFalsoFallo.Enabled = false;
                btnFaltaEstano.Enabled = false;
                btnNext.Enabled = false;
                btnNoReparado.Enabled = false;
                btnOtroFallo.Enabled = false;
                btnPrevious.Enabled = false;
                btnSinComponente.Enabled = false;              
            }
        }

        private void SetPosition(int newIndex)
        {
            BlockButtons();

            if (notOKIndexes.Count != 0)
            {
                if (newIndex < 0)
                    newIndex = notOKIndexes.Count - 1;

                if (newIndex > notOKIndexes.Count - 1)
                    newIndex = 0;

                index = newIndex;
            }
            else
                index = -1;

            UpdateUI();
            EnableButtons();
        }

        private void UpdateUI()
        { 
            var item = Current;

            if (item == null)
                return;

            bsList.ResetItem(bsList.Position);

            if (Current.RemainingFailures == 0)          
                GreenButton(btnFinishRepair);
            else
                GrayButton(btnFinishRepair);

            //if (index != -1 && Current.RemainingFailures == 0)
            //{
            //    FinishRepair();
            //}

            //Update Images
            hWindow2.HalconWindow.ClearWindow();
            hWindow1.HalconWindow.ClearWindow();

            Current.GenerateROIVisualHelpers();
            if (index != -1)
            {
                Current.GenerateDrawVisualHelpers(notOKIndexes[index]);

                Current.GetWindows2Image().DispObj(hWindow2.HalconWindow);
                hWindow2.HalconWindow.SetPart(0, 0, -2, -2);
            }

            Current.GetWindows1Image().DispObj(hWindow1.HalconWindow);
            hWindow1.HalconWindow.SetPart(0, 0, -2, -2);

            GC.Collect();
        }

        private void GreenButton(RadButton button)
        {
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor2 = Color.FromArgb(155, 248, 108);
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor3 = Color.FromArgb(94, 245, 18);
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor4 = Color.FromArgb(51, 114, 19);
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor = Color.FromArgb(183, 238, 156);
        }

        private void GrayButton(RadButton button)
        {
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor2 = Color.FromArgb(184, 184, 184);
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor3 = Color.FromArgb(140, 140, 140);
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor4 = Color.FromArgb(56, 56, 56);
            ((Telerik.WinControls.Primitives.FillPrimitive)(button.GetChildAt(0).GetChildAt(0))).BackColor = Color.FromArgb(219, 219, 219);
        }

        private void FinishRepair()
        {
            this.Cursor = Cursors.WaitCursor;
            this.Enabled = false;

            index = 0;
            resultsToSave = Current;

            Task.Factory.StartNew(SaveResultDataBase);
            Task.Factory.StartNew(SaveWeldingImages);
            UpdateStatistics(true);

            File.Delete(orderPath + "Iconic\\Image_" + Current.Barcodes + ".hobj");
            File.Delete(orderPath + "Iconic\\Weldings_" + Current.Barcodes + ".hobj");
            File.Delete(orderPath + "Iconic\\WBoards_" + Current.Barcodes + ".hobj");
            File.Delete(Current.JsonFile);

            resultFiles.Remove(Current.JsonFile);

            bsList.RemoveCurrent();
            bsList.MoveFirst();

            this.Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void SaveResultDataBase()
        {
            if (numOrden == "0")
                return;

            using (DezacService svc = new DezacService())
            {
                for (int i = 0; i < resultsToSave.InspectionResults.Barcodes.Length; i++)
                {
                    var bastidor = Int32.Parse(resultsToSave.InspectionResults.Barcodes[i]);

                    int? numFabricacion = null;
                    try
                    {
                        numFabricacion = svc.GetNumFabricacion(resultsToSave.InspectionResults.NumOrden, bastidor);
                    }
                    catch (Exception e)
                    {
                        logger.Warn("No se ha podido obtener numero de fabricación: " + e.Message);
                    }

                    var test = new T_TEST
                    {
                        NUMFABRICACION = numFabricacion,
                        NUMMATRICULA = bastidor,
                        NUMMATRICULATEST = bastidor,
                        FECHA = DateTime.Now,
                        NUMPRODUCTOTEST = resultsToSave.InspectionResults.NumProduct,
                        VERSIONPRODUCTOTEST = 1,
                        TIEMPO = Convert.ToInt32(0),
                        RESULTADO = "O",
                        NUMFAMILIA = resultsToSave.InspectionResults.NumFamilia,
                        VERSIONREPORT = 0
                    };

                    T_TESTFASE fase = new T_TESTFASE
                    {
                        T_TEST = test,
                        IDFASE = idFase,
                        IDOPERARIO = resultsToSave.InspectionResults.NumOperario,
                        FECHA = DateTime.Now,
                        TIEMPO = Convert.ToInt32(0),
                        RESULTADO = "O",
                        NOMBREPC = Dns.GetHostName()
                    };

                    resultsToSave.InspectionResults.Boards[i].Weldigns.ForEach((welding) =>
                    {
                        if (welding.Repair != RepairEnum.SIN_REPARACION)
                        {
                            var numParam = svc.CrearParametro(resultsToSave.InspectionResults.NumFamilia, idFase, 18, "BOARD_#" + i + "_WELDING_#" + welding.IDBoard + "_REPAIR", 1, 1, 26, "Vision");
                            fase.T_TESTVALOR.Add(new T_TESTVALOR { NUMPARAM = numParam, VALOR = welding.Repair.ToString(), VALMAX = null, VALMIN = null, EXPECTEDVALUE = null, STEPNAME = "WeldingReprocess", NUMUNIDAD = 1 });
                        }
                    });

                    fase.T_TESTSTEP.Add(new T_TESTSTEP
                    {
                        STEPNAME = "WeldingReprocess",
                        TIMESTART = DateTime.Now,
                        TIMEEND = DateTime.Now,
                        RESULT = "O",
                        EXCEPTION = null,
                    });

                    test.T_TESTFASE.Add(fase);

                    svc.GrabarDatos(test);
                }
            };
        }

        private void SaveWeldingImages()
        {
            HObject weldingImage = null;

            string baseFolder = ConfigurationManager.AppSettings["PathAOILabeledImages"];

            using (FTPClient client = new FTPClient(ConfigurationManager.AppSettings["VAFTPDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
            {
                foreach (string classificatorName in Enum.GetNames(typeof(ClassificatorsEnum)))
                foreach (string name in Enum.GetNames(typeof(RepairEnum)))
                    client.CreateDirectory($"{baseFolder}{classificatorName}\\{name}\\{resultsToSave.InspectionResults.NumProduct}");

                resultsToSave.ExtractWeldingImages();

                int i = 1;
                foreach (var board in resultsToSave.InspectionResults.Boards)
                foreach (var welding in board.Weldigns)
                {
                    HOperatorSet.SelectObj(resultsToSave.WeldingImages, out weldingImage, i);
                    HOperatorSet.Compose3(weldingImage, weldingImage, weldingImage, out HObject imageNew);
                    HOperatorSet.InterleaveChannels(imageNew, out imageNew, "argb", "match", 255);
                    HOperatorSet.GetImagePointer1(imageNew, out HTuple pointer, out HTuple type, out HTuple width, out HTuple height);

                    string imageName = $"{baseFolder}" +
                                       $"{((ClassificatorsEnum)welding.Classificator).ToString("G")}" +
                                       $"\\{welding.Repair.ToString()}" +
                                       $"\\{resultsToSave.InspectionResults.NumProduct}" +
                                       $"\\{board.Barcode}" +
                                       $"_{ DateTime.Now.ToShortDateString().Replace("/", "")}" +
                                       $"_{ DateTime.Now.ToLongTimeString().Replace(":", "")}" +
                                       $"_{welding.IDBoard}.png";

                    using (Bitmap bitmap = new Bitmap(width/4, height, width, PixelFormat.Format32bppArgb, pointer))
                    using (MemoryStream stream = new MemoryStream())
                    {
                        bitmap.Save(stream, ImageFormat.Png);
                        client.UploadOverwrite(stream, imageName, 3);
                        if (welding.MissClassified)
                        {
                            imageName = $"{baseFolder}" +
                                        $"{((ClassificatorsEnum)welding.Classificator).ToString("G")}" +
                                        $"\\MISSCLASSIFIED" +
                                        $"\\{resultsToSave.InspectionResults.NumProduct}" +
                                        $"\\{board.Barcode}" +
                                        $"_{ DateTime.Now.ToShortDateString().Replace("/", "")}" +
                                        $"_{ DateTime.Now.ToLongTimeString().Replace(":", "")}" +
                                        $"_{welding.IDBoard}.png";

                            client.UploadOverwrite(stream, imageName, 3);
                        }
                    }
                    i++;
                }
            }
        }

        private void UpdateStatistics(bool hasNewStatistics)
        {
            string filename = ConfigurationManager.AppSettings["PathAOIStatistics"] + numOrden + ".stats";

            using (FTPClient ftp = new FTPClient(ConfigurationManager.AppSettings["VAFTPDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
            {
                ftp.CreateDirectory(Path.GetDirectoryName(filename));
                if (ftp.FileExist(filename))
                {
                    using (Stream stream = new MemoryStream())
                    {
                        ftp.Download(stream, filename);
                        using (TextReader tr = new StreamReader(stream))
                        {
                            string json = tr.ReadToEnd();
                            statistics = JsonConvert.DeserializeObject<AOIStatistics>(json);
                        }
                    }

                    if (hasNewStatistics)
                        statistics.Add(Current.InspectionResults);

                    lblFalsosFallos.Text = statistics.FalsoFallo.ToString();
                    lblFaltasEstaño.Text = statistics.FaltaEstaño.ToString();
                    lblSoldadurasFriasBolas.Text = statistics.FormaDefectuosa.ToString();
                    lblCruces.Text = statistics.Cruce.ToString();
                    lblPatasLargas.Text = statistics.PataLarga.ToString();
                    lblFaltasComponentes.Text = statistics.FaltaComponente.ToString();
                    lblLevantado.Text = statistics.ComponenteLevantado.ToString();
                    lblOtrosFallos.Text = statistics.OtroFallo.ToString();
                    lblDPMO.Text = statistics.DPMO.ToString();

                    string jsonStatistics = JsonConvert.SerializeObject(statistics);

                    using (Stream stream = new MemoryStream())
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.Write(jsonStatistics);
                        writer.Flush();
                        stream.Position = 0;
                        ftp.UploadOverwrite(stream, filename, 3);
                    }
                }
            }
        }

        // Busca nuevos archivos
        private void timer1_Tick(object sender, EventArgs e) 
        {
            timer1.Enabled = false;
            if (!Directory.Exists(orderPath))
                Directory.CreateDirectory(orderPath);

            var actualResultFiles = Directory.GetFiles(orderPath).ToList();

            var newResultFiles = actualResultFiles.Except(resultFiles).ToList();

            if (newResultFiles.Count > 0)
                LoadResults(newResultFiles);
            timer1.Enabled = true;
        }

        private void LoadResults(List<string> newResultFiles)
        {
            Form form = null;
            RadProgressBar ProgressBar = new RadProgressBar();

            if (newResultFiles.Count > 5)
            {
                form = new Form();
                form.Size = new Size(300, 80);
                form.Text = "Cargando Resultados";
                form.StartPosition = FormStartPosition.CenterScreen;
                form.ControlBox = false;
                form.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                form.TopMost = true;

                ProgressBar.Location = new System.Drawing.Point(0, 0);
                ProgressBar.Name = "ProgressBar";
                ProgressBar.Size = new System.Drawing.Size(280, 60);
                ProgressBar.TabIndex = 0;
                ProgressBar.Text = "";
                ProgressBar.Dock = DockStyle.Fill;
                ProgressBar.Maximum = newResultFiles.Count;
                ProgressBar.Minimum = 0;

                form.Controls.Add(ProgressBar);

                sc.Send(_ =>
                {
                    form.Show();
                }, null);

                this.Cursor = Cursors.WaitCursor;
                this.Enabled = false;
            }

            Thread.Sleep(500); // Habia veces que no daba tiempo a guardarse los ficheros iconicos

            int i = 0;
            hWindow1.Visible = false;
            hWindow2.Visible = false;
            bsList.CurrentChanged -= bsList_CurrentChanged;
            var actualPosition = bsList.Position;
            var actualIndex = index;

            foreach (string file in newResultFiles)
            {
                string json = File.ReadAllText(file);

                var result = JsonConvert.DeserializeObject<HalconInspectionResults>(json);
                string jointBarcodes = string.Join("_", result.Barcodes);

                HObject image, weldings, boards;

                HOperatorSet.ReadImage(out image, orderPath + "Iconic\\Image_" + jointBarcodes + ".hobj");
                HOperatorSet.ReadRegion(out weldings, orderPath + "Iconic\\Weldings_" + jointBarcodes + ".hobj");
                HOperatorSet.ReadRegion(out boards, orderPath + "Iconic\\WBoards_" + jointBarcodes + ".hobj");

                bsList.Add(new AOIViewerResults(image, weldings, boards, result, hWindow1.Size, file));

                resultFiles.Add(file);

                i++;
                ProgressBar.Value1 = i;
                Application.DoEvents();
            }
            bsList.CurrentChanged += bsList_CurrentChanged;
            hWindow1.Visible = true;
            hWindow2.Visible = true;

            if (actualPosition == -1)
                bsList_CurrentChanged(null, null);
            else
                bsList.Position = actualPosition;

            if (actualIndex != 0)
            {
                index = actualIndex;
                SetPosition(index);
            }

            if (form != null)
            {
                this.Enabled = true;
                this.Cursor = Cursors.Default;

                form.Invoke((MethodInvoker)(() => form.Dispose()));
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            SetPosition(--index);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            SetPosition(++index);
        }

        private void btnFalsoFallo_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.FALSO_FALLO);
        }

        private void btnNoReparado_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.SIN_REPARACION);
        }

        private void btnFaltaEstano_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.FALTA_ESTAÑO);
        }

        private void btnSinComponente_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.FALTA_COMPONENTE);
        }

        private void btnCable_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.PATA_LARGA);
        }

        private void btnBolaEstano_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.FORMA_DEFECTUOSA);
        }

        private void btnCruce_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.CRUCE);
        }

        private void btnOtroFallo_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.OTRO_FALLO);
        }

        private void BtnComponenteLevantaado_Click(object sender, EventArgs e)
        {
            AddRepair(RepairEnum.COMPONENTE_LEVANTADO);
        }

        private void AddRepair(RepairEnum repair)
        {
            int weldingIndex = notOKIndexes[index];

            if (Current.InspectionResults.WeldingsRepair[weldingIndex] == RepairEnum.SIN_REPARACION)
                Current.InspectionResults.AddRepair(weldingIndex, repair);         
            else
            {
                var result = MessageBox.Show("La soldadura ya tiene una reparación. ¿Quiere reemplazar la reparación actual?", "ATENCION", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                    Current.InspectionResults.AddRepair(weldingIndex, repair);
            }

            SetPosition(++index);
        }

        private void btnFinishRepair_Click(object sender, EventArgs e)
        {
            if (Current.RemainingFailures != 0)
            {
                DialogResult result = MessageBox.Show("Hay errores sin tipificar ¿Esta seguro/a que desea finalizar el panel?", "Finalizar Panel", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    return;
            }
            FinishRepair();
            GrayButton(btnFinishRepair);
        }

        private void btnDescartarCircuito_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Esta seguro/a que desea descartar el circuito?", "Descartar Circuito", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;

            int board = Current.InspectionResults.GetBoardOfWelding(notOKIndexes[index]);
            Current.InspectionResults.Boards[board].Weldigns.ForEach(w => w.Repair = RepairEnum.DESCARTADO);

            int initIndex = index;
            do
            {
                index++;
                if (index == notOKIndexes.Count)
                    index = 0;
            } while (index != initIndex && Current.InspectionResults.WeldingsRepair[notOKIndexes[index]] != RepairEnum.SIN_REPARACION);

            SetPosition(index);
        }

        private void BlockButtons()
        {
            btnBolaEstano.Enabled = false;
            btnCable.Enabled = false;
            btnCruce.Enabled = false;
            btnFalsoFallo.Enabled = false;
            btnFaltaEstano.Enabled = false;
            btnNext.Enabled = false;
            btnNoReparado.Enabled = false;
            btnOtroFallo.Enabled = false;
            btnPrevious.Enabled = false;
            btnSinComponente.Enabled = false;
            btnComponenteLevantaado.Enabled = false;
            btnFinishRepair.Enabled = false;
            btnDescartarCircuito.Enabled = false;
        }

        private void EnableButtons()
        {
            btnBolaEstano.Enabled = true;
            btnCable.Enabled = true;
            btnCruce.Enabled = true;
            btnFalsoFallo.Enabled = true;
            btnFaltaEstano.Enabled = true;
            btnNext.Enabled = true;
            btnNoReparado.Enabled = true;
            btnOtroFallo.Enabled = true;
            btnPrevious.Enabled = true;
            btnSinComponente.Enabled = true;
            btnComponenteLevantaado.Enabled = true;
            btnFinishRepair.Enabled = true;
            btnDescartarCircuito.Enabled = true;
        }

        private void hWindow1_HMouseUp(object sender, HMouseEventArgs e)
        {
            if (Current == null)
                return;

            Current.GenerateDrawVisualHelpers((int)e.Y, (int)e.X, 0, 0, 0, 0);

            Current.GenerateROIVisualHelpers();

            Current.GetWindows1Image().DispObj(hWindow1.HalconWindow);
            hWindow1.HalconWindow.SetPart(0, 0, -2, -2);

            Current.GetWindows2Image().DispObj(hWindow2.HalconWindow);
            hWindow2.HalconWindow.SetPart(0, 0, -2, -2);

            GC.Collect();
        }

        private void hWindow2_HMouseUp(object sender, HMouseEventArgs e)
        {
            if (Current == null)
                return;

            var position = Current.DetermineSelectedWelding(e.X, e.Y);

            if (!position.HasValue)
                return;

            if (!notOKIndexes.Contains(position.Value))
            {
                index = index < 0 ? 0 : index;
                notOKIndexes.Insert(index, position.Value);
                Current.InspectionResults.AddError(position.Value);
            }
            else
                index = notOKIndexes.FindIndex(s => s == position.Value);

            UpdateUI();
        }
    }
}
