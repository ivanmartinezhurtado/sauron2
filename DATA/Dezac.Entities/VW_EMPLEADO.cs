namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class VW_EMPLEADO
    {
        public string IDEMPLEADO { get; set; }
        public string NOMBRE { get; set; }
        public string IDPUESTOTRABAJO { get; set; }
        public string IDOPERARIO { get; set; }
        public byte[] FOTOGRAFIA { get; set; }
        public string EMAIL { get; set; }
        public string USERNAME { get; set; }
        public string IDCALENDARIO { get; set; }
        public Nullable<long> NUMTARJETA { get; set; }
        public string EXTTLFNINTERNA { get; set; }
        public string DEPARTAMENTO { get; set; }
        public Nullable<System.DateTime> FECHABAJA { get; set; }
        public string DEPT { get; set; }
    }
}
