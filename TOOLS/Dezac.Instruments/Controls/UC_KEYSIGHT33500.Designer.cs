﻿namespace Dezac.Instruments.Controls
{
    partial class UC_KEYSIGHT33500
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSp;
            System.Windows.Forms.Label lblSignal;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label11;
            this.cmb_SignalForm = new System.Windows.Forms.ComboBox();
            this.cmb_Channel = new System.Windows.Forms.ComboBox();
            this.btnOff = new System.Windows.Forms.Button();
            this.btnOn = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.txtFrequency = new System.Windows.Forms.MaskedTextBox();
            this.txtVoffset = new System.Windows.Forms.MaskedTextBox();
            this.txtAmplitude = new System.Windows.Forms.MaskedTextBox();
            this.txtVmin = new System.Windows.Forms.MaskedTextBox();
            this.txtVmax = new System.Windows.Forms.MaskedTextBox();
            this.txtPhase = new System.Windows.Forms.MaskedTextBox();
            this.txtDutyCycle = new System.Windows.Forms.MaskedTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPhase = new System.Windows.Forms.Label();
            this.lblDuty = new System.Windows.Forms.Label();
            this.cmbVmax = new System.Windows.Forms.ComboBox();
            this.cmbVmin = new System.Windows.Forms.ComboBox();
            this.cmbAmplitude = new System.Windows.Forms.ComboBox();
            this.cmbOffset = new System.Windows.Forms.ComboBox();
            this.cmbFrec = new System.Windows.Forms.ComboBox();
            this.txtAddres = new System.Windows.Forms.TextBox();
            this.cmb_SendType = new System.Windows.Forms.ComboBox();
            this.cmbImpedance = new System.Windows.Forms.ComboBox();
            lblSp = new System.Windows.Forms.Label();
            lblSignal = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSp
            // 
            lblSp.AutoSize = true;
            lblSp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSp.Location = new System.Drawing.Point(12, 50);
            lblSp.Name = "lblSp";
            lblSp.Size = new System.Drawing.Size(77, 13);
            lblSp.TabIndex = 57;
            lblSp.Text = "Addres Port:";
            // 
            // lblSignal
            // 
            lblSignal.AutoSize = true;
            lblSignal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSignal.Location = new System.Drawing.Point(385, 85);
            lblSignal.Name = "lblSignal";
            lblSignal.Size = new System.Drawing.Size(77, 13);
            lblSignal.TabIndex = 59;
            lblSignal.Text = "Signal Form:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(221, 85);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(57, 13);
            label1.TabIndex = 61;
            label1.Text = "Channel:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(13, 13);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(70, 13);
            label2.TabIndex = 66;
            label2.Text = "Frequency:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(3, 46);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(92, 13);
            label3.TabIndex = 68;
            label3.Text = "Offset Voltage:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(17, 78);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(66, 13);
            label4.TabIndex = 70;
            label4.Text = "Amplitude:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(17, 110);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(78, 13);
            label5.TabIndex = 72;
            label5.Text = "Min Voltage:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(294, 31);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(81, 13);
            label6.TabIndex = 74;
            label6.Text = "Max Voltage:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.Location = new System.Drawing.Point(322, 64);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(46, 13);
            label7.TabIndex = 76;
            label7.Text = "Phase:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new System.Drawing.Point(303, 96);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(72, 13);
            label8.TabIndex = 78;
            label8.Text = "Duty Cycle:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.BackColor = System.Drawing.Color.Transparent;
            label10.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label10.ForeColor = System.Drawing.Color.Black;
            label10.Location = new System.Drawing.Point(6, 11);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(249, 22);
            label10.TabIndex = 96;
            label10.Text = "Key Sight Function Generator";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new System.Drawing.Point(7, 85);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(105, 13);
            label9.TabIndex = 99;
            label9.Text = "Send Input Type:";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(227, 48);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(115, 13);
            label11.TabIndex = 101;
            label11.Text = "Out Impedance(Ω):";
            // 
            // cmb_SignalForm
            // 
            this.cmb_SignalForm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SignalForm.FormattingEnabled = true;
            this.cmb_SignalForm.Location = new System.Drawing.Point(464, 82);
            this.cmb_SignalForm.Name = "cmb_SignalForm";
            this.cmb_SignalForm.Size = new System.Drawing.Size(93, 21);
            this.cmb_SignalForm.TabIndex = 58;
            this.cmb_SignalForm.SelectedIndexChanged += new System.EventHandler(this.selectionWaveform);
            // 
            // cmb_Channel
            // 
            this.cmb_Channel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Channel.FormattingEnabled = true;
            this.cmb_Channel.Location = new System.Drawing.Point(278, 82);
            this.cmb_Channel.Name = "cmb_Channel";
            this.cmb_Channel.Size = new System.Drawing.Size(93, 21);
            this.cmb_Channel.TabIndex = 60;
            // 
            // btnOff
            // 
            this.btnOff.BackColor = System.Drawing.Color.DimGray;
            this.btnOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOff.ForeColor = System.Drawing.Color.White;
            this.btnOff.Location = new System.Drawing.Point(152, 273);
            this.btnOff.Name = "btnOff";
            this.btnOff.Size = new System.Drawing.Size(125, 35);
            this.btnOff.TabIndex = 63;
            this.btnOff.Text = "OFF CHANNEL";
            this.btnOff.UseVisualStyleBackColor = false;
            this.btnOff.Click += new System.EventHandler(this.OffKeySight);
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(10, 273);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(117, 35);
            this.btnOn.TabIndex = 62;
            this.btnOn.Text = "ON CHANNEL";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.OnKeySight);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.DimGray;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(445, 273);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(148, 35);
            this.btnReset.TabIndex = 64;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.ResetKeySight);
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.DimGray;
            this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.ForeColor = System.Drawing.Color.White;
            this.btnApply.Location = new System.Drawing.Point(305, 273);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(117, 35);
            this.btnApply.TabIndex = 86;
            this.btnApply.Text = "APPLY";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.ApplySignal);
            // 
            // txtFrequency
            // 
            this.txtFrequency.Location = new System.Drawing.Point(93, 10);
            this.txtFrequency.Name = "txtFrequency";
            this.txtFrequency.Size = new System.Drawing.Size(100, 20);
            this.txtFrequency.TabIndex = 87;
            this.txtFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVoffset
            // 
            this.txtVoffset.Location = new System.Drawing.Point(93, 43);
            this.txtVoffset.Name = "txtVoffset";
            this.txtVoffset.Size = new System.Drawing.Size(100, 20);
            this.txtVoffset.TabIndex = 88;
            this.txtVoffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtAmplitude
            // 
            this.txtAmplitude.Location = new System.Drawing.Point(93, 75);
            this.txtAmplitude.Name = "txtAmplitude";
            this.txtAmplitude.Size = new System.Drawing.Size(100, 20);
            this.txtAmplitude.TabIndex = 89;
            this.txtAmplitude.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVmin
            // 
            this.txtVmin.Location = new System.Drawing.Point(93, 107);
            this.txtVmin.Name = "txtVmin";
            this.txtVmin.Size = new System.Drawing.Size(100, 20);
            this.txtVmin.TabIndex = 90;
            this.txtVmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVmax
            // 
            this.txtVmax.Location = new System.Drawing.Point(378, 28);
            this.txtVmax.Name = "txtVmax";
            this.txtVmax.Size = new System.Drawing.Size(100, 20);
            this.txtVmax.TabIndex = 91;
            this.txtVmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPhase
            // 
            this.txtPhase.Location = new System.Drawing.Point(378, 61);
            this.txtPhase.Name = "txtPhase";
            this.txtPhase.Size = new System.Drawing.Size(100, 20);
            this.txtPhase.TabIndex = 92;
            this.txtPhase.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDutyCycle
            // 
            this.txtDutyCycle.Location = new System.Drawing.Point(378, 93);
            this.txtDutyCycle.Name = "txtDutyCycle";
            this.txtDutyCycle.Size = new System.Drawing.Size(100, 20);
            this.txtDutyCycle.TabIndex = 93;
            this.txtDutyCycle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblPhase);
            this.panel1.Controls.Add(this.lblDuty);
            this.panel1.Controls.Add(this.cmbVmax);
            this.panel1.Controls.Add(this.cmbVmin);
            this.panel1.Controls.Add(this.cmbAmplitude);
            this.panel1.Controls.Add(this.cmbOffset);
            this.panel1.Controls.Add(this.cmbFrec);
            this.panel1.Controls.Add(this.txtDutyCycle);
            this.panel1.Controls.Add(this.txtPhase);
            this.panel1.Controls.Add(this.txtVmax);
            this.panel1.Controls.Add(this.txtVmin);
            this.panel1.Controls.Add(this.txtAmplitude);
            this.panel1.Controls.Add(this.txtVoffset);
            this.panel1.Controls.Add(label2);
            this.panel1.Controls.Add(this.txtFrequency);
            this.panel1.Controls.Add(label3);
            this.panel1.Controls.Add(label4);
            this.panel1.Controls.Add(label8);
            this.panel1.Controls.Add(label5);
            this.panel1.Controls.Add(label7);
            this.panel1.Controls.Add(label6);
            this.panel1.Location = new System.Drawing.Point(10, 115);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 143);
            this.panel1.TabIndex = 94;
            // 
            // lblPhase
            // 
            this.lblPhase.AutoSize = true;
            this.lblPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhase.Location = new System.Drawing.Point(480, 61);
            this.lblPhase.Name = "lblPhase";
            this.lblPhase.Size = new System.Drawing.Size(16, 20);
            this.lblPhase.TabIndex = 105;
            this.lblPhase.Text = "º";
            // 
            // lblDuty
            // 
            this.lblDuty.AutoSize = true;
            this.lblDuty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuty.Location = new System.Drawing.Point(478, 93);
            this.lblDuty.Name = "lblDuty";
            this.lblDuty.Size = new System.Drawing.Size(24, 20);
            this.lblDuty.TabIndex = 104;
            this.lblDuty.Text = "%";
            // 
            // cmbVmax
            // 
            this.cmbVmax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVmax.FormattingEnabled = true;
            this.cmbVmax.Location = new System.Drawing.Point(484, 27);
            this.cmbVmax.Name = "cmbVmax";
            this.cmbVmax.Size = new System.Drawing.Size(80, 21);
            this.cmbVmax.TabIndex = 102;
            // 
            // cmbVmin
            // 
            this.cmbVmin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVmin.FormattingEnabled = true;
            this.cmbVmin.Location = new System.Drawing.Point(199, 106);
            this.cmbVmin.Name = "cmbVmin";
            this.cmbVmin.Size = new System.Drawing.Size(45, 21);
            this.cmbVmin.TabIndex = 101;
            // 
            // cmbAmplitude
            // 
            this.cmbAmplitude.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAmplitude.FormattingEnabled = true;
            this.cmbAmplitude.Location = new System.Drawing.Point(199, 75);
            this.cmbAmplitude.Name = "cmbAmplitude";
            this.cmbAmplitude.Size = new System.Drawing.Size(45, 21);
            this.cmbAmplitude.TabIndex = 100;
            // 
            // cmbOffset
            // 
            this.cmbOffset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOffset.FormattingEnabled = true;
            this.cmbOffset.Location = new System.Drawing.Point(199, 43);
            this.cmbOffset.Name = "cmbOffset";
            this.cmbOffset.Size = new System.Drawing.Size(45, 21);
            this.cmbOffset.TabIndex = 99;
            // 
            // cmbFrec
            // 
            this.cmbFrec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFrec.FormattingEnabled = true;
            this.cmbFrec.Location = new System.Drawing.Point(199, 10);
            this.cmbFrec.Name = "cmbFrec";
            this.cmbFrec.Size = new System.Drawing.Size(45, 21);
            this.cmbFrec.TabIndex = 98;
            // 
            // txtAddres
            // 
            this.txtAddres.Location = new System.Drawing.Point(95, 46);
            this.txtAddres.Name = "txtAddres";
            this.txtAddres.ReadOnly = true;
            this.txtAddres.Size = new System.Drawing.Size(85, 20);
            this.txtAddres.TabIndex = 97;
            // 
            // cmb_SendType
            // 
            this.cmb_SendType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendType.FormattingEnabled = true;
            this.cmb_SendType.Location = new System.Drawing.Point(112, 82);
            this.cmb_SendType.Name = "cmb_SendType";
            this.cmb_SendType.Size = new System.Drawing.Size(93, 21);
            this.cmb_SendType.TabIndex = 98;
            this.cmb_SendType.SelectedIndexChanged += new System.EventHandler(this.selectionTypeInputVoltage);
            // 
            // cmbImpedance
            // 
            this.cmbImpedance.FormattingEnabled = true;
            this.cmbImpedance.Location = new System.Drawing.Point(341, 45);
            this.cmbImpedance.Name = "cmbImpedance";
            this.cmbImpedance.Size = new System.Drawing.Size(93, 21);
            this.cmbImpedance.TabIndex = 102;
            // 
            // UC_KEYSIGHT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.cmbImpedance);
            this.Controls.Add(label11);
            this.Controls.Add(label9);
            this.Controls.Add(this.cmb_SendType);
            this.Controls.Add(this.txtAddres);
            this.Controls.Add(label10);
            this.Controls.Add(lblSp);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.cmb_SignalForm);
            this.Controls.Add(this.btnOff);
            this.Controls.Add(this.btnOn);
            this.Controls.Add(lblSignal);
            this.Controls.Add(label1);
            this.Controls.Add(this.cmb_Channel);
            this.Name = "UC_KEYSIGHT";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Size = new System.Drawing.Size(611, 326);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmb_SignalForm;
        private System.Windows.Forms.ComboBox cmb_Channel;
        private System.Windows.Forms.Button btnOff;
        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.MaskedTextBox txtFrequency;
        private System.Windows.Forms.MaskedTextBox txtVoffset;
        private System.Windows.Forms.MaskedTextBox txtAmplitude;
        private System.Windows.Forms.MaskedTextBox txtVmin;
        private System.Windows.Forms.MaskedTextBox txtVmax;
        private System.Windows.Forms.MaskedTextBox txtPhase;
        private System.Windows.Forms.MaskedTextBox txtDutyCycle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtAddres;
        private System.Windows.Forms.ComboBox cmbVmax;
        private System.Windows.Forms.ComboBox cmbVmin;
        private System.Windows.Forms.ComboBox cmbAmplitude;
        private System.Windows.Forms.ComboBox cmbOffset;
        private System.Windows.Forms.ComboBox cmbFrec;
        private System.Windows.Forms.Label lblPhase;
        private System.Windows.Forms.Label lblDuty;
        private System.Windows.Forms.ComboBox cmb_SendType;
        private System.Windows.Forms.ComboBox cmbImpedance;
    }
}
