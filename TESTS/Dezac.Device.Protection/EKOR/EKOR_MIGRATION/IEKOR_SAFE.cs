﻿using System;
using System.Collections.Generic;
using static Dezac.Device.Protection.EKOR_SAFE_OLD;

namespace Dezac.Device.Protection
{
    public interface IEKOR_SAFE
    {
        CirbusDeviceSerialPort Cirbus { get; set; }

        void SetPort(int port, int baudRate = 9600, byte periferic = 1, int timeout = 3000, int retries = 3);
        void WriteSerialNumber(string sn);
        void WriteBastidor(string frame);
        void WriteDisplayTestSegmentsEven();
        void WriteDisplayTestSegmentsOdd();
        void WriteDateTime();
        void FATFormat();
        void FATResetDefaultFabrica();
        void RecoveryDefaultSettings(int port, int baudrate, byte periferico, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even);
        void WriteConfigurationDefaultFab(ModelHardware model, string subModel = "");
        void Trip();
        void CalibrationCurrents(ChannelCurrent channel, Scale scale, int current_mA);
        void CalibrationVoltage(ChannelVoltage channel, List<double> current_uA, Scale scale = Scale.Fina);
        void Dispose();
        void DesactivaLED(int led);
        void ActivaLED(int led);
        void DesactivaDigitalOutput(int output);
        void ActivaDigitalOutput(int output);
        void StartSignalClok(int secondDuration);
        string ReadPPP();
        void WriteEEPROM(int data);
        int ReadEEPROM();
        double ReadTemperatureI2C();
        StructTicks ReadCountTicksLastTest();
        T ReadCalibrationScale<T>(Scale scale) where T : struct;
        T ReadVoltageMeasureVerification<T>() where T : struct;
        MeasureStructVoltagePhase ReadVoltage();
        MeasureStructCurrenPhase ReadCurrentPhase();
        EKOR_SAFE_OLD.Version ReadVersion();
        EKOR_SAFE_OLD.Version ReadVersionDesarrollo();
        string ReadVersionBoot();
        typeMicro ReadMicrocontrolerType();
        FLASHstruct ReadStateFLASH();
        bool ReadPowerBitState(int bit);
        string ReadDeviceModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even);
        int ReadModelModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even);
        MeasureInstante ReadMeasure(Lines channel);
        bool ReadKeyState(int bit);
        bool ReadDigitalInput(int bit);
        DateTime ReadDateTime();
        string ReadBastidor();
        string ReadSerialNumber();
        double ReadBattery(double vRef);
        StructCalibrationClock CalibrationClock(double frequency, double temerature);
        StructCalibrationClock ReadCalibrationClock();
        void WriteFactorsCalibration<T>(Scale scale, T factors);
        void Reset();
        string ReadVoltageInstant();
        string ReadCRCBoot();
    }
}
