﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Renci.SshNet;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(SSHWriteCommandActionDescription))]
    public class SSHWriteCommand : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Comando que se ha de mandar")]
        public ActionVariable CommandToSend { get; set; } = new ActionVariable();

        [DefaultValue("SSH_Device")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var ssh = GetVariable(DeviceName.Replace('-', '_')) as SSHClient;

            if (ssh == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el SSHClient").Throw();

            testBase.SamplerWithCancel((p) =>
            {
                var send = VariablesTools.ResolveText(CommandToSend.Value.ToString());

                ssh.Shell.WriteLine(send);

                testBase.Logger.DebugFormat("TX : " + send);

                return true;

            }, "Error en las comunicaciones SSH", 5, 100, 500, false, false);
        }
    }

    public class SSHWriteCommandActionDescription : ActionDescription
    {
        public override string Name { get { return "SSHShellWriteCommand"; } }
        public override string Description { get { return "Envía el comando especificado por una tty de la sesión por SSH"; } }
        public override string Category { get { return "Tools/Ethernet/SSH"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
