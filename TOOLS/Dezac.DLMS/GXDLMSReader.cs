﻿using Gurux.Common;
using Gurux.DLMS;
using Gurux.DLMS.Enums;
using Gurux.DLMS.Objects;
using Gurux.Net;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Dezac.DLMS
{
    public class GXDLMSReader
    {
        protected static readonly ILog _logger = LogManager.GetLogger("GXDLMSReader");

        private IGXMedia Media;
        private TraceLevel Trace;
        private GXDLMSClient Client;
        private GXDLMSObjectCollection colectionObject = null;

        public int WaitTime { get; set; }

        public int RetryCount { get; set; }

        public GXDLMSObjectCollection ColectionObject
        {
            get
            {
                if (colectionObject == null)
                    colectionObject = ReadObjects();

                return colectionObject;
            }
        }

        public GXDLMSReader(GXDLMSClient client, IGXMedia media, TraceLevel trace)
        {
            Trace = trace;
            Media = media;
            Client = client;
            RetryCount = 3;
            WaitTime = 5000;
        }

        public void InitializeConnection()
        {
            GXReplyData reply = new GXReplyData();
            byte[] data;
            data = Client.SNRMRequest();
            if (data != null)
            {
                if (Trace > TraceLevel.Info)
                    _logger.DebugFormat("Send SNRM request." + GXCommon.ToHex(data, true));

                ReadDataBlock(data, reply);
                if (Trace == TraceLevel.Verbose)
                    _logger.DebugFormat("Parsing UA reply." + reply.ToString());

                //Has server accepted client.
                Client.ParseUAResponse(reply.Data);
                if (Trace > TraceLevel.Info)
                    _logger.DebugFormat("Parsing UA reply succeeded.");
            }

            //Generate AARQ request.
            //Split requests to multiple packets if needed.
            //If password is used all data might not fit to one packet.
            foreach (byte[] it in Client.AARQRequest())
            {
                if (Trace > TraceLevel.Info)
                    _logger.DebugFormat("Send AARQ request", GXCommon.ToHex(it, true));

                reply.Clear();
                ReadDataBlock(it, reply);
            }
            if (Trace > TraceLevel.Info)
                _logger.DebugFormat("Parsing AARE reply" + reply.ToString());

            //Parse reply.
            Client.ParseAAREResponse(reply.Data);
            reply.Clear();
            //Get challenge Is HLS authentication is used.
            if (Client.IsAuthenticationRequired)
            {
                foreach (byte[] it in Client.GetApplicationAssociationRequest())
                {
                    reply.Clear();
                    ReadDataBlock(it, reply);
                }
                Client.ParseApplicationAssociationResponse(reply.Data);
            }
            if (Trace > TraceLevel.Info)
                _logger.DebugFormat("Parsing AARE reply succeeded.");

            //if (!readAllObject)
            //    ReadObjects();
        }

        //****************************************************************************      

        public object Read(GXDLMSObject it, int attributeIndex)
        {
            GXReplyData reply = new GXReplyData();
            if (!ReadDataBlock(Client.Read(it, attributeIndex), reply))
            {
                if (reply.Error != (short)ErrorCode.Rejected)
                    throw new GXDLMSException(reply.Error);

                reply.Clear();
                Thread.Sleep(1000);
                if (!ReadDataBlock(Client.Read(it, attributeIndex), reply))
                    throw new GXDLMSException(reply.Error);
            }

            //Update data type.
            if (it.GetDataType(attributeIndex) == DataType.None)
                it.SetDataType(attributeIndex, reply.DataType);

            return Client.UpdateValue(it, attributeIndex, reply.Value);
        }

        public bool ReadDataBlock(byte[][] data, GXReplyData reply)
        {
            if (data == null)
                return true;

            foreach (byte[] it in data)
            {
                reply.Clear();
                ReadDataBlock(it, reply);
            }
            return reply.Error == 0;
        }

        public void ReadDataBlock(byte[] data, GXReplyData reply)
        {
            ReadDLMSPacket(data, reply);

            lock (Media.Synchronous)
            {
                while (reply.IsMoreData)
                {
                    if (reply.IsStreaming())
                        data = null;
                    else
                        data = Client.ReceiverReady(reply.MoreData);

                    ReadDLMSPacket(data, reply);

                    if (Trace > TraceLevel.Info)
                    {
                        //If data block is read.
                        if ((reply.MoreData & RequestTypes.Frame) == 0)
                            _logger.DebugFormat("Get Next Data block.");
                        else
                            _logger.DebugFormat("Get next frame.");
                    }
                }
            }
        }

        public void ReadDLMSPacket(byte[] data, GXReplyData reply)
        {
            if (data == null && !reply.IsStreaming())
                return;

            reply.Error = 0;
            object eop = (byte)0x7E;
            //In network connection terminator is not used.
            if (Client.InterfaceType == InterfaceType.WRAPPER && Media is GXNet)
                eop = null;

            int pos = 0;
            bool succeeded = false;
            ReceiveParameters<byte[]> p = new ReceiveParameters<byte[]>()
            {
                Eop = eop,
                Count = 5,
                WaitTime = WaitTime,
            };

            lock (Media.Synchronous)

            {
                while (!succeeded && pos != 3)
                {
                    if (!reply.IsStreaming())
                    {
                        _logger.DebugFormat("TX: " + GXCommon.ToHex(data, true));
                        Media.Send(data, null);
                    }
                    succeeded = Media.Receive(p);
                    if (!succeeded)
                    {
                        if (++pos >= RetryCount)
                            throw new Exception("Failed to receive reply from the device in given time.");

                        //If Eop is not set read one byte at time.
                        if (p.Eop == null)
                            p.Count = 1;

                        //Try to read again...
                        _logger.DebugFormat("Data send failed. Try to resend " + pos.ToString() + "/3");
                    }
                }
                try
                {
                    pos = 0;
                    //Loop until whole COSEM packet is received.
                    while (!Client.GetData(p.Reply, reply))
                    {
                        //If Eop is not set read one byte at time.
                        if (p.Eop == null)
                            p.Count = 1;

                        while (!Media.Receive(p))
                        {
                            if (++pos >= RetryCount)
                                throw new Exception("Failed to receive reply from the device in given time.");

                            //If echo.
                            if (p.Reply == null || p.Reply.Length == data.Length)
                                Media.Send(data, null);

                            //Try to read again...
                            _logger.DebugFormat("Data send failed. Try to resend " + pos.ToString() + "/3");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.DebugFormat("RX: " + GXCommon.ToHex(p.Reply, true));
                    throw ex;
                }
            }
            _logger.DebugFormat("RX: " + GXCommon.ToHex(p.Reply, true));
            if (reply.Error != 0)
            {
                if (reply.Error == (short)ErrorCode.Rejected)
                {
                    Thread.Sleep(1000);
                    ReadDLMSPacket(data, reply);
                }
                else
                    throw new GXDLMSException(reply.Error);
            }
        }


        //************************************************************************************

        public String CadenaHexArray(Byte[] cadena)
        {
            string StringHexa;

            StringHexa = ASCIIEncoding.ASCII.GetString(cadena);
            return StringHexa;
        }

        //************************************************************************************

        //************************************************************************************

        public void Write(GXDLMSObject it, int attributeIndex)
        {
            GXReplyData reply = new GXReplyData();
            ReadDataBlock(Client.Write(it, attributeIndex), reply);
        }

        //************************************************************************************

        public void ImageUpdate(GXDLMSImageTransfer target, string identification, byte[] data)
        {
            //Check that image transfer ia enabled.
            GXReplyData reply = new GXReplyData();
            ReadDataBlock(Client.Read(target, 5), reply);
            Client.UpdateValue(target, 5, reply.Value);
            if (!target.ImageTransferEnabled)
                throw new Exception("Image transfer is not enabled");

            //Step 1: Read image block size.
            ReadDataBlock(Client.Read(target, 2), reply);
            Client.UpdateValue(target, 2, reply.Value);

            // Step 2: Initiate the Image transfer process.
            ReadDataBlock(target.ImageTransferInitiate(Client, identification, data.Length), reply);

            // Step 3: Transfers ImageBlocks.
            int imageBlockCount;
            ReadDataBlock(target.ImageBlockTransfer(Client, data, out imageBlockCount), reply);

            //Step 4: Check the completeness of the Image.
            ReadDataBlock(Client.Read(target, 3), reply);
            Client.UpdateValue(target, 3, reply.Value);

            // Step 5: The Image is verified;
            ReadDataBlock(target.ImageVerify(Client), reply);
            // Step 6: Before activation, the Image is checked;

            //Get list to images to activate.
            ReadDataBlock(Client.Read(target, 7), reply);
            Client.UpdateValue(target, 7, reply.Value);
            bool bFound = false;
            foreach (GXDLMSImageActivateInfo it in target.ImageActivateInfo)
                if (CadenaHexArray(it.Identification) == identification)
                {
                    bFound = true;
                    break;
                }

            //Read image transfer status.
            ReadDataBlock(Client.Read(target, 6), reply);
            Client.UpdateValue(target, 6, reply.Value);
            if (target.ImageTransferStatus != Gurux.DLMS.Objects.Enums.ImageTransferStatus.VerificationSuccessful)
                throw new Exception("Image transfer status is " + target.ImageTransferStatus.ToString());

            if (!bFound)
                throw new Exception("Image not found.");

            //Step 7: Activate image.
            ReadDataBlock(target.ImageActivate(Client), reply);
        }

        public string GetCacheName()
        {
            return Media.ToString().Replace(":", "") + ".xml";
        }

        public GXDLMSObjectCollection ReadObjects()
        {
            _logger.DebugFormat("--- Collecting objects. ---");
            GXReplyData reply = new GXReplyData();
            try
            {
                ReadDataBlock(Client.GetObjectsRequest(), reply);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetObjects failed. " + Ex.Message);
            }
            GXDLMSObjectCollection objs = Client.ParseObjects(reply.Data, true);
            _logger.DebugFormat("--- Collecting " + objs.Count.ToString() + " objects. ---");

            return objs;
        }

        //***************************************************************

        public void ShowValue(object val, int pos)
        {
            //If trace is info.
            if (Trace > TraceLevel.Warning)
            {
                //If data is array.
                if (val is byte[])
                    val = GXCommon.ToHex((byte[])val, true);
                else if (val is Array)
                {
                    string str = "";
                    for (int pos2 = 0; pos2 != (val as Array).Length; ++pos2)
                    {
                        if (str != "")
                            str += ", ";

                        if ((val as Array).GetValue(pos2) is byte[])
                            str += GXCommon.ToHex((byte[])(val as Array).GetValue(pos2), true);
                        else
                            str += (val as Array).GetValue(pos2).ToString();
                    }
                    val = str;
                }
                else if (val is System.Collections.IList)
                {
                    string str = "[";
                    bool empty = true;
                    foreach (object it2 in val as System.Collections.IList)
                    {
                        if (!empty)
                            str += ", ";

                        empty = false;
                        if (it2 is byte[])
                            str += GXCommon.ToHex((byte[])it2, true);
                        else
                            str += it2.ToString();
                    }
                    str += "]";
                    val = str;
                }
                _logger.DebugFormat("Index: " + pos + " Value: " + val);
            }
        }

        public void GetReadOut()
        {

            bool all = true;
            foreach (GXDLMSObject it in Client.Objects)
            {
                // Profile generics are read later because they are special cases.
                // (There might be so lots of data and we so not want waste time to read all the data.)
                if (it is GXDLMSProfileGeneric)
                    continue;

                if (!(it is IGXDLMSBase))
                {
                    //If interface is not implemented.
                    //Example manufacturer spesific interface.
                    if (Trace > TraceLevel.Error)
                        _logger.DebugFormat("Unknown Interface: " + it.ObjectType.ToString());

                    continue;
                }

                if (Trace > TraceLevel.Warning)
                    _logger.DebugFormat("-------- Reading " + it.GetType().Name + " " + it.Name + " " + it.Description);

                foreach (int pos in (it as IGXDLMSBase).GetAttributeIndexToRead(all))
                    try
                    {
                        object val = Read(it, pos);
                        ShowValue(val, pos);
                    }
                    catch (Exception ex)
                    {
                        _logger.DebugFormat("Error! " + it.GetType().Name + " " + it.Name + "Index: " + pos + " " + ex.Message);
                        _logger.DebugFormat(ex.ToString());
                    }
            }
        }

        public void ReadList(List<KeyValuePair<GXDLMSObject, int>> list)
        {
            byte[][] data = Client.ReadList(list);
            GXReplyData reply = new GXReplyData();
            List<object> values = new List<object>();
            foreach (byte[] it in data)
            {
                ReadDataBlock(it, reply);
                if (list.Count != 1 && reply.Value is object[])
                    values.AddRange((object[])reply.Value);
                else if (reply.Value != null)
                    //Value is null if data is send in multiple frames.
                    values.Add(reply.Value);

                reply.Clear();
            }
            if (values.Count != list.Count)
                throw new Exception("Invalid reply. Read items count do not match.");

            Client.UpdateValues(list, values);
        }

        public void Method(GXDLMSObject it, int attributeIndex, object value, DataType type)
        {
            GXReplyData reply = new GXReplyData();
            ReadDataBlock(Client.Method(it, attributeIndex, value, type), reply);
        }

        //***************************************************************************

        public GXDLMSProfileGeneric ReadGenericProfile(string obis, DateTime? start, DateTime? end)
        {
            var genericProfile = GetProfileGenerics(obis) as GXDLMSProfileGeneric;
            if (genericProfile == null)
                throw new Exception("No se ha encontrado este codigo OBIS en el equipo");

            GetProfileGenericColumns(genericProfile);

            if (start.HasValue)
                genericProfile.From = new GXDateTime(start.Value);

            if (end.HasValue)
                genericProfile.To = new GXDateTime(end.Value);

            GXReplyData reply = new GXReplyData();

            if (!start.HasValue || !end.HasValue)
                ReadDataBlock(Client.Read(genericProfile, 2), reply);
            else
                ReadDataBlock(Client.ReadRowsByRange(genericProfile, start, end), reply);

            Client.UpdateValue(genericProfile, 2, reply.Value);

            return genericProfile;
        }

        private GXDLMSObject GetProfileGenerics(string obis)
        {
            return ColectionObject.FindByLN(ObjectType.ProfileGeneric, obis);
        }

        public void GetProfileGenericColumns(GXDLMSProfileGeneric item)
        {
            GXReplyData reply = new GXReplyData();
            ReadDataBlock(Client.Read(item, 3)[0], reply);
            Client.UpdateValue(item, 3, reply.Value);

            foreach (GXKeyValuePair<GXDLMSObject, GXDLMSCaptureObject> it in item.CaptureObjects)
            {
                if (it.Key is GXDLMSRegister)
                {
                    _logger.DebugFormat(it.Key.Name.ToString());
                    Read(it.Key, 3);
                }
                if (it.Key is GXDLMSDemandRegister)
                {
                    _logger.DebugFormat(it.Key.Name.ToString());
                    Read(it.Key, 4);
                }
            }
        }

        public object[] ReadRowsByEntry(GXDLMSProfileGeneric it, uint index, uint count)
        {
            GXReplyData reply = new GXReplyData();
            ReadDataBlock(Client.ReadRowsByEntry(it, index, count), reply);
            return (object[])Client.UpdateValue(it, 2, reply.Value);
        }

        public object[] ReadRowsByRange(GXDLMSProfileGeneric it, DateTime start, DateTime end)
        {
            GXReplyData reply = new GXReplyData();
            ReadDataBlock(Client.ReadRowsByRange(it, start, end), reply);
            return (object[])Client.UpdateValue(it, 2, reply.Value);
        }

        //*******************************************************************************************************************

        public void Disconnect()
        {
            if (Media != null && Client != null)
            try
            {
                if (Trace > TraceLevel.Info)
                    _logger.DebugFormat("Disconnecting from the meter.");

                GXReplyData reply = new GXReplyData();
                ReadDLMSPacket(Client.DisconnectRequest(), reply);
                Media.Close();
            }
            catch
            {

            }
            //Media = null;
            //Client = null;
        }
    }
}
