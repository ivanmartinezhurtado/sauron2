﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Extensions
{
    public interface IRunnerExtensions
    {
        void SaveFileNumTestFase(int numTestFase, string name, string tipo, byte[] data);
        void SaveFileVANumTestFase(int numTestFase, string name, string tipo, byte[] data);
        void SaveProcessFaseFile(int numRegister, byte[] data);
    }
}
