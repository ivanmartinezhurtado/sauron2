﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(ScpClientInitializationDescription))]
    public class ScpClientInitialization : ActionBase
    {       
        /// <summary>
        /// User name.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Nombre de usuario")]
        public string User { get; set; }
        /// <summary>
        /// User passsword.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Contraseña de usuario")]
        public string Password { get; set; }
        /// <summary>
        /// User passsword.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Fichero de clave privada de usuario")]
        public string KeyFile { get; set; }
        /// <summary>
        /// The server host.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Dirección IP del servidor")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable IpServer { get; set; } = new ActionVariable();
        /// <summary>
        /// Instance of object SCPClient.
        /// </summary>
        [DefaultValue("SCPClient")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SCPClient")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            try
            {
                SCPClient scp;
                string host = IpServer.Value.ToString();
                scp = new SCPClient(host, User, Password, KeyFile);
                SetVariable(DeviceName, scp);
            }
            catch (Exception ex)
            {
                Error().PROCESO.ACTION_EXECUTE.EXCEPTION_NOT_CONTROLLED(ex.Message).Throw();
            }
        }
    }
    class ScpClientInitializationDescription : ActionDescription
    {
        public override string Name { get { return "SCPClientInitialization"; } }
        public override string Category { get { return "Tools/Ethernet/SCP"; } }
        public override string Description { get { return "Inicializa la instancia de un cliente SCP"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "ScpClientFinish:2"; } }
    }
}
