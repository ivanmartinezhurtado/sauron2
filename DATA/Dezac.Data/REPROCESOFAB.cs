namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.REPROCESOFAB")]
    public partial class REPROCESOFAB
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public REPROCESOFAB()
        {
            ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            RF_ACTUACION = new HashSet<RF_ACTUACION>();
            RF_ANOMALIA = new HashSet<RF_ANOMALIA>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMREPROCESO { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        public int? NUMFABRICACION { get; set; }

        public int? NUMPRODUCTO { get; set; }

        public int? VERSION { get; set; }

        [StringLength(500)]
        public string NOTAS { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACION { get; set; }

        public int? NUMMATRICULA { get; set; }

        public int? NUMORDEN { get; set; }

        public int? NUMTESTFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }

        public virtual MATRICULA MATRICULA { get; set; }

        public virtual ORDEN ORDEN { get; set; }

        public virtual ORDENPRODUCTO ORDENPRODUCTO { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }

        public virtual T_TESTFASE T_TESTFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RF_ACTUACION> RF_ACTUACION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RF_ANOMALIA> RF_ANOMALIA { get; set; }
    }
}
