﻿using System.Collections.Generic;

namespace Dezac.Tests.Reports.Reports
{
    public class TestReportModel
    {
        public int NUMTEST { get; set; }
        public int NUMTESTFASE { get; set; }
        public int NUMMATRICULATEST { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public string DESCRIPCION { get; set; }
        public string NROSERIE { get; set; }

        public int NUMORDEN { get; set; }
        public System.DateTime FECHA { get; set; }

        public string NUM_PC { get; set; }
        public string IDOPERARIO { get; set; }

        public string UUTFIRMWARE { get; set; }
        public string TESTVERSION { get; set; }

        public List<TestReportValueModel> Values { get; set; }
        public List<TestReportFunctionModel> Functions { get; set; }
    }

    public class TestReportValueModel
    {
        public int NUMMATRICULATEST { get; set; }
        public string TestPoint { get; set; }
        public string Value { get; set; }
        public string Tolerance { get; set; }
    }

    public class TestReportFunctionModel
    {
        public int NUMMATRICULATEST { get; set; }
        public string Function { get; set; }
        public string Result { get; set; }
    }
}
