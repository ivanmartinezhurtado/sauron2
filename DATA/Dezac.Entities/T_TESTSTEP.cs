namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTSTEP
    {
        public int NUMTESTFASE { get; set; }
        public string STEPNAME { get; set; }
        public System.DateTime TIMESTART { get; set; }
        public System.DateTime TIMEEND { get; set; }
        public string RESULT { get; set; }
        public string EXCEPTION { get; set; }
        public string GROUPNAME { get; set; }
    
        public virtual T_TESTFASE T_TESTFASE { get; set; }
    }
}
