﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.43)]
    public class EKOR_SAFE : DeviceBase
    {
        #region Comandos comunicaciones
        protected const string READVERSION = "VER";
        protected const string READVERSIONDESARROLLO = "VDP";
        protected const string READVERSIONBOOT = "VER BOOT";
        protected const string READCRCBOOT = "CRC BOOT";
        protected const string READBASTIDOR = "LID FN";
        protected const string WRITEBASTIDOR = "EID FN {0:00000000}";
        protected const string WRITESERIALNUMBER = "EID SN {0:00000000000000}";
        protected const string READSERIALNUMBER = "LID SN";
        protected const string READCALIBRATIONCHANNEL = "CAL C {0:00}";
        protected const string READCALIBRATIONSCALE = "CAL E {0:00}";
        protected const string AUTOCALIBRATION = "CAL A {0:0000} {1} {2}";
        protected const string AUTOCALIBRATIONVOLTAGE = "CAL A {0:0000} {1} {2} {3} {4}";
        protected const string READAUTOCALIBRATION = "CAL A";
        protected const string WRITEFACTORSCALIBRATION = "CAL EW {0:00} {1}";
        protected const string WRITESCALE = "TST SCALE {0}";
        protected const string STARTSIGNALCLOK = "CLK DE2ACFAB S{0:000}";
        protected const string CALIBRATIONCLOCK = "CLK F{0:000.000000} T{1:00.0}";
        protected const string READCALIBRATIONCLOCK = "CLK I";
        protected const string READTEMPERATURE = "CLK T";
        protected const string READCOUNTTICKSLASTTEST = "CLK N";
        protected const string WRITEDISPLAY = "PRT {0}";
        protected const string WRITEDISPLAYTEST = "PRT S:{0}";
        protected const string LEERESTADOENTRADADIGITAL = "CHK DIN {0:0000}";
        protected const string LEERESTADOENTRADASDIGITALES = "CHK DIN {0:0000} {1:0000}";
        protected const string ACTIVASALIDADIGITAL = "CHK SET DOUT {0:0000}";
        protected const string DESACTIVASALIDADIGITAL = "CHK CLR DOUT {0:0000}";
        protected const string LEERESTADOTECLA = "CHK KBD {0:0000}";
        protected const string LEERESTADOTECLAS = "CHK KBD {0:0000} {1:0000}";
        protected const string ACTIVALED = "CHK SET LED {0:0000}";
        protected const string DESACTIVALED = "CHK CLR LED {0:0000}";
        protected const string LEERESTADOALIMENTACION = "CHK POWER {0:0000}";
        protected const string LEERESTADOALIMENTACIONES = "CHK POWER {0:0000} {1:0000}";
        protected const string WRITEDATETIME = "ERL{0}";
        protected const string READDATETIME = "LRL";
        protected const string TRIP = "TRP";
        protected const string CAPTUREFILEMEASURE = "CAP {0}";
        protected const string READCURRENTINSTANT = "LII";
        protected const string READCURRENTFUNDAMENTAL = "LIF {0}";
        protected const string READVOLTAGEINSTANT = "LVI";
        protected const string READVOLTAGEFUNFAMENTAL = "LVF {0}";
        protected const string READSTATUSTEST = "TST";
        protected const string READTEMPERATUREI2C = "I2C TMP";
        protected const string READPARAMETERSMEASURE = "VIP {0}";
        protected const string READVOLTAGESMEASUREVERIFICATION = "LEV F";
        protected const string IMPXML = "XML IMP {0}";
        protected const string EXPXML = "XML EXP {0}";
        protected const string READFLASHSTATUS = "FAT I";
        protected const string FATFORMAT = "FAT FORMAT";
        protected const string FATERASE = "FAT UNFORMAT";
        protected const string FATRESETFAB = "FAT RST PRG";
        protected const string READMICROCONTROLERTYPE = "VDP CPU";
        protected const string WRITECONFIGURATIONDEFAULT = "STP HWR {0:00} {1:00}";
        protected const string WRITECONFIGURATION = "STP HWR {0:00}";
        protected const string READBATTERYLEVEL = "BAT {0}";
        protected const string CRH_START = "CRH START";
        protected const string CRH = "CRH";
        protected const string CPF = "CPF {0:000} {1}";
        protected const string READEEPROM = "EEP READ 0F00 0001";
        protected const string WRITEEEPROM = "EEP WRITE 0F00 {0}";
        protected const string MEDIDA_FREQUENIA = "LFR TST";
        protected const string READPPP = "PPP DE2ACFAB";
        protected const string WRITE_FACTORES_CVALIBRACION_DEFECTO = "CAL 0";
        protected const string READ_KEYBOARD = "KPD";
        protected const string AUTOCALIBRATION_NEW = "CAL A {0:0000} {1} {2} C={3}";
        protected const string SET_FRECUENCY_CIRCUIT_MEASURE = "STP VER 01";
        protected const string GET_FRECUENCY_CIRCUIT_MEASURE = "STP VER";

        #endregion

        #region Enumerados

        [Flags]
        public enum enumPowerStates
        {
            VUSB_OK,
            VCC_OK,
            VAUX_OK,
            VGEN_OK,
            VTRP_OK,       
        }

        [Flags]
        public enum Leds
        {
            LED_FASE = 1,
            LED_NEUTRO = 2,   
            LED_EXT = 3
        }

        [Flags]
        public enum enumKeyboard
        {
            SET ,
            UP ,
            ESC ,
            DOWN ,
            RIGHT ,
            LEFT,
            SIDE 
        }

        [Flags]
        public enum enumOutputs
        {
            RELE1,
            RELE2,
            RELE3,
            RELE4
        }

        public enum FileExportImport
        {
            HWR,
            MOD,
            CFG,
            INS,
            CAX,
            USR,
            CAL,
            RTC,
            ALL
        }

        public enum ModelHardware
        {
            WTP10 = 0,
            WTP100 = 1,
            WTP200 = 2,
            RPA100 = 3,
            MVE = 4,
            SAFE = 5,
            FABSAFE = 6,
            FABPLUS = 7,
            FAB_DIDO = 9,
            DIDO = 10,
            RPG_CI_RTU = 11,
            RPA200_C = 12,
            FAB_ENERGY_C = 13,
            RPA200_R = 14,
            FAB_ENERGY_R = 15,
            RPA200_T = 16,
            FAB_ENERGY_T = 17,
            LVM_100 = 18,
            RPA200_RPPL = 19,
            FABMOTOR = 20,
            TRAFO = 21,
            MVE_CPU = 99
        }
 
        public enum RegistersModbus
        {
            KEY_PSWI = 0X0501,
            SET_COM_TO_FAB = 0x0550,
            SERIAL_NUMBER = 0x0232,
            MODEL = 0x0108,
            DEVICE = 0x0239
        }
        
        public enum stateFLAASH
        {
            NOT_INIALIZE,        //0       ,     No inicializada.  
            READY,               //1       ,     Funcionando -> FileSystem activo.
            BUSY,                //2       ,     Busy -> FileSystem ocupado por otra tarea.
            ERROR_HARDWARE_CPU,  //3       ,     Error al inicializar el hardware de la CPU.
            ERROR_CPI,           //4       ,     Error al inicializar el acceso SPI. 
            ERROR_DETECTION,     //5       ,     Error al detectar la memoria.         
            ERROR_FORMAT         //6       ,     Error en el formato de la memoria.         
        }

        #endregion

        #region Estructuras
        public struct Version
        {
            public string model;
            public string version;
            public byte versionMajor;
            public byte versionMinor;
            public byte versionRevision;
            public DateTime fechaDesarrollo;
        }
        public struct StructCalibrationClock
        {
            public int Arr_50Hz_TM1;
            public int Arr_60Hz_TM1;
            public int Po;
            public int Err25;
            public int RTC_PRS;
        }
        public struct StructTemperature
        {
            public int temperatura;
            public int MSRPointsTemp;
            public string lastValueRTC;

            public int Temperatura { get { return temperatura / 10; } }
            public int MSR_PointsTemp { get { return MSRPointsTemp / 256; } }
            public int LastValueRTC { get { return int.Parse(lastValueRTC.Replace("0x", ""), NumberStyles.HexNumber); } }
        }
        public struct StructTicks
        {
            public int MaxTick;
            public int AvgTick;
            public int MinTick;
            public int TimeTick;
        }
        public struct FLASHstruct
        {
            public stateFLAASH state;
            public string ID;
        }
        public struct typeMicro
        {
            public string cpu;
            public string revision;
        }

        #endregion

        #region Constructor y Disposed
        public EKOR_SAFE()
        {
        }

        public EKOR_SAFE(int port)
        {
            SetPort(port);
        }

        public CirbusDeviceSerialPort Cirbus { get; set; }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1, int timeout = 3000, int retries = 3)
        {
            if (Cirbus == null)
                Cirbus = new CirbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Cirbus.PortCom = port;

            Cirbus.PerifericNumber = periferic;
            Cirbus.TimeOut = timeout;
            Cirbus.Retries = retries;
        }

        public override void Dispose()
        {
            if (Cirbus != null)
            {
                Cirbus.Dispose();
                Cirbus = null;
            }
        }

        #endregion

        #region Metodos SETUP y TEST
        public void WriteConfigurationDefaultFab(ModelHardware modelo, string subModel = "")
        {
            LogMethod();
            var modelString = ((byte)modelo);

            var retires = Cirbus.Retries;
            var timeout = Cirbus.TimeOut;
            Cirbus.Retries = 1;
            Cirbus.TimeOut = 6000;

            if (!string.IsNullOrEmpty(subModel))
                Cirbus.Write(WRITECONFIGURATIONDEFAULT, modelString.ToString("X2"), subModel);
            else
                Cirbus.Write(WRITECONFIGURATION, modelString.ToString("X2"));

            Cirbus.TimeOut = timeout;
            Cirbus.Retries = retires;
        }

        public void WriteFactorsCalibrationDefault()
        {
            LogMethod();
            Cirbus.Write(WRITE_FACTORES_CVALIBRACION_DEFECTO, listErrorCirbus);
        }

        public string ReadBastidor()
        {
           LogMethod();
            return Cirbus.Read(READBASTIDOR, listErrorCirbus);
        }

        public void WriteBastidor(string bastidor)
        {
            LogMethod();
            Cirbus.Write(WRITEBASTIDOR, listErrorCirbus, bastidor.PadLeft(8, '0'));
        }

        public void WriteSerialNumber(string numeroserie)
        {
            LogMethod();
            Cirbus.Write(WRITESERIALNUMBER, listErrorCirbus, numeroserie.PadLeft(14, '0'));
        }

        public string ReadSerialNumber()
        {
            LogMethod();
            return Cirbus.Read(READSERIALNUMBER, listErrorCirbus); 
        }

        public Version ReadVersion()
        {
            LogMethod();
            
            return internalReadVersion(Cirbus.Read(READVERSION));     
        }

        public Version ReadVersionDesarrollo()
        {
            LogMethod();
            return internalReadVersion(Cirbus.Read(READVERSIONDESARROLLO));
        }

        public string ReadVersionBoot()
        {
            LogMethod();
            var boot = Cirbus.Read(READVERSIONBOOT);
            return boot;
        }

        public string ReadCRCBoot()
        {
            LogMethod();
            var crc = Cirbus.Read(READCRCBOOT, true).TrimStart();
            return crc;
        }

        private Version internalReadVersion(string resp)
        {
            Version version = new Version();
            var split = resp.Split(' ');

            if (split.Length < 3)
                throw new Exception("Error de longitud de trama en la respuesta de la version");

            version.model = split[1];
            version.version = split[2];

            var versionNumber = split[2].Split('.');
            if (versionNumber.Length < 4)
                throw new Exception("Error de longitud numerica de la de trama en la respuesta de la version");

            version.versionMajor = Convert.ToByte(versionNumber[1]);
            version.versionMinor = Convert.ToByte(versionNumber[2]);
            version.versionRevision = Convert.ToByte(versionNumber[3]);

            if (split.Length > 4)
                version.fechaDesarrollo = Convert.ToDateTime(split[3]);

            return version;
        }        

        public typeMicro ReadMicrocontrolerType()
        {
            LogMethod();
            var version = Cirbus.Read(READMICROCONTROLERTYPE).Split(':');
            var cpu = version[1].Substring(version[1].IndexOf("(") + 1, version[1].IndexOf(")") - 2);
            var revision =  version[2].Substring(version[2].IndexOf("(") + 1, version[2].IndexOf(")") - 2);

            return new typeMicro() { cpu = cpu, revision = revision };
        }

        public string ReadStatusTest()
        {
            LogMethod();
            return Cirbus.Read(READSTATUSTEST);
        }

        public void WriteCPF_RTU(bool activated)
        {
            LogMethod();
            Cirbus.Write(CPF, "RTU", activated ? "0" : "1"); //la logica va al reves 
        }

        public string ReadPPP()
        {
            LogMethod();
            try
            {
                Cirbus.Read(READPPP, listErrorPPP);
                return "PPP OK";
            }
            catch (Exception ex)
            {
                TestException.Create().UUT.HARDWARE.NO_COMUNICA("PUERTO PPP").Throw();
                return "";
            }
        }

        public double ReadTemperatureI2C()
        {
            LogMethod();

            return Cirbus.Read<double>(READTEMPERATUREI2C, listErrorCirbus) / 100;
        }

        public void Reset()
        {
            LogMethod();

            Cirbus.Retries = 0;
            Cirbus.WriteWhitoutResponse(CRH_START);
            Cirbus.Retries = 3;
            Thread.Sleep(1500);
        }

        [Browsable(false)]
        public string ReadCrash()
        {
            LogMethod();
            return Cirbus.Read(CRH);
        }

        public bool ReadCalibrationFinished()
        {
            LogMethod();
            var x = Cirbus.Read(READAUTOCALIBRATION, true);
            _logger.DebugFormat(string.Format("response CAL : {0}", x));
            if (x.Equals(string.Empty))
                return true;

            return false;

        }

        public void WriteAutoScale()
        {
            LogMethod();
            Cirbus.Write(WRITESCALE, listErrorCirbus, "A");
        }

        #endregion

        #region Metodos Reloj  

        public void StartSignalClok(int secondDuration)
        {
            LogMethod();
            Cirbus.Retries = 0;
            Cirbus.WithTimeOut((p) => { p.Write(STARTSIGNALCLOK, listErrorClock, secondDuration); });
            Cirbus.Retries = 3;
        }

        public StructCalibrationClock CalibrationClock(double frequency, double temerature)
        {
            LogMethod();
            return Cirbus.Read<StructCalibrationClock>(CALIBRATIONCLOCK, listErrorClock, frequency, temerature);
        }

        public StructCalibrationClock ReadCalibrationClock()
        {
            LogMethod();
            return Cirbus.Read <StructCalibrationClock>(READCALIBRATIONCLOCK, listErrorClock);
        }

        [Browsable(false)]
        public StructTemperature ReadTemperature()
        {
            LogMethod();
            return Cirbus.Read <StructTemperature>(READTEMPERATURE, listErrorClock);
        }

        public StructTicks ReadCountTicksLastTest()
        {
            LogMethod();
            return Cirbus.Read<StructTicks>(READCOUNTTICKSLASTTEST, listErrorClock);
        }

        #endregion

        #region Metodos FechaTiempo

        public void WriteDateTime()
        {
            LogMethod();
            var datetime = DateTime.Now.ToString("dd/MM/yy HH:mm:ss,fff"); //20/01/00 21:41:50,367
            Cirbus.Write(WRITEDATETIME, datetime);
        }

        public DateTime ReadDateTime()
        {
            LogMethod();
            var time = Cirbus.Read(READDATETIME, listErrorBit);
            IFormatProvider culture = new CultureInfo("es-ES", true);
            DateTime dateVal = DateTime.ParseExact(time, "dd/MM/yy HH:mm:ss,fff", culture);
            return dateVal;
        }

        #endregion

        #region Metodos Bit (Entradas Salidas Digitales)

        public void ActivaDigitalOutput(int bit)
        {
            LogMethod();
            Cirbus.Write(ACTIVASALIDADIGITAL, listErrorBit, bit.ToString("X4"));
        }

        public void DesactivaDigitalOutput(int bit)
        {
            LogMethod();
            Cirbus.Write(DESACTIVASALIDADIGITAL, listErrorBit, bit.ToString("X4"));
        }

        public void ActivaLED(int bit)
        {
            LogMethod();
            Cirbus.Write(ACTIVALED, listErrorBit, bit.ToString("X4"));
        }

        public void DesactivaLED(int bit)
        {
            LogMethod();
            Cirbus.Write(DESACTIVALED, listErrorBit, bit.ToString("X4"));
        }

        public bool ReadDigitalInput(int bit)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOENTRADADIGITAL, listErrorBit, bit.ToString("X4"));
            return resp == "1" ? true : false;
        }

        [Browsable(false)]
        public string ReadDigitalInputs(int bit, byte qtyBits)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOENTRADASDIGITALES, listErrorBit, bit.ToString("X4"), qtyBits.ToString("X4"));
            return resp;
        }

        public bool ReadKeyState(int bit)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOTECLA, listErrorBit, bit.ToString("X4"));
            return resp == "1" ? true : false;
        }

        [Browsable(false)]
        public string ReadKeysStates(int bit, byte qtyBits)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOTECLAS, listErrorBit, bit.ToString("X4"), qtyBits.ToString("X4"));
            return resp;
        }

        public bool ReadPowerBitState(int bit)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOALIMENTACION, listErrorBit, bit.ToString("X4"));
            return resp == "1" ? true : false;
        }

        #endregion

        #region Metodos Teclado
        public Keyborad ReadKeyborad()
        {
            LogMethod();
            var resp = Cirbus.Read<Keyborad>(READ_KEYBOARD, false);
            return resp;
        }

        public struct Keyborad
        {
            private string aa;
            private string keyboard;
            private string nnnn;

            public string KEYBOARD { get { return keyboard.ToUpper().Replace("0X",""); } }
        }

        #endregion

        #region Metodos Display

        [Browsable(false)]
        public void WriteDisplay(byte type)
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAY, listErrorBit, type);
        }

        public void WriteDisplayTestSegmentsOdd()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFFA00AAA0000AA00A0AAA0AAA00AA000A0");
        }

        public void WriteDisplayTestSegmentsEven()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFF05500055550055050005000550055500");
        }

        public void WriteDisplayTestSegmentsAll()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
        }

        public void WriteDisplayTestSegmentsNone()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFF00000000000000000000000000000000");
        }

        #endregion

        #region Metodos TRIP (Disparo)

        [Browsable(false)]
        public void ActivateTPR()
        {
            LogMethod();
            Cirbus.Write(TRIP);
        }

        public void Trip()
        {
            LogMethod();
            try
            {
                Cirbus.Write(TRIP, listErrorTrip);
            }
            catch (Exception ex)
            {
                _logger.Warn(ex.Message);
                if (!ex.Message.Contains(listErrorTrip[1]))
                    throw;
            }
        }

        #endregion

        #region Metodos FLASH

        public FLASHstruct ReadStateFLASH()
        {
            LogMethod();
            var resp = Cirbus.Read(READFLASHSTATUS, listErrorFlash).Split(' ');

            if (resp.Count() == 0)
                throw new Exception("Error no se ha podido leer el estado de la Flash");

            var flashInfo = new FLASHstruct()
            {
                 ID=resp[1],
                 state = (stateFLAASH)Convert.ToByte(resp[0]),
            };
            return flashInfo;
        }

        public void FATFormat()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FATFORMAT);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public void FATResetDefaultFabrica()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FATRESETFAB);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public void FATErase()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FATERASE);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public void WriteEEPROM(int data)
        {
            LogMethod();
            Cirbus.Write(WRITEEEPROM, data.ToString("X2"));
        }

        public double ReadBattery(double vRef)
        {
            LogMethod();
            return Cirbus.Read<double>(READBATTERYLEVEL, listErrorCirbus, (int)(vRef * 1000)) / 1000;
        }

        public int ReadEEPROM()
        {
            LogMethod();       
            return Convert.ToInt32(Cirbus.Read(READEEPROM, listErrorCirbus), 16);
        }

        #endregion

        #region Metodos Frequencia
        public Frequency ReadMEDIDA_FREQ()
        {
            LogMethod();
            var result = Cirbus.Read<Frequency>(MEDIDA_FREQUENIA, listErrorCirbus);
            return result;
        }

        public struct Frequency
        {
            private int FreqV2;
            private int FreqVSync;

            public double FREQ_V2 { get { return (FreqV2 * 50D) / 128D; } }
            public double FREQ_VSYNC { get { return (FreqVSync * 50D) / 128D; } }
        }

        public void WriteCircuitMeasureFreq(bool hasCiruitMeasureFreq)
        {
            LogMethod();
            Cirbus.Write(SET_FRECUENCY_CIRCUIT_MEASURE, hasCiruitMeasureFreq ? "01" : "00");
        }

        public bool ReadCircuitMeasureFreq()
        {
            LogMethod();
            var result = Cirbus.Read(GET_FRECUENCY_CIRCUIT_MEASURE, true).TrimStart();
            return result.Left(2) == "01";
        }

        #endregion

        #region Metodos MODBUS
        public void RecoveryDefaultSettings(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            Cirbus.ClosePort();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.UseFunction06 = true;
                Modbus.PerifericNumber = periferico;
                Modbus.TimeOut = 2000;
                Modbus.Write((ushort)RegistersModbus.SET_COM_TO_FAB, (ushort)0xC0FA);
            };

            Cirbus.OpenPort();
        }      

        public int ReadModelModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            if (Cirbus.IsOpen)
                Cirbus.ClosePort();

            ushort modelCode;            
            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.PerifericNumber = periferico;
                modelCode = Modbus.ReadHoldingRegister((ushort)RegistersModbus.MODEL);
                modelCode >>= 8;
            };

            var model =  RelationCodeModel.Where(k => k.Key == modelCode).Select(v => v.Value).FirstOrDefault();
            return model;
        }

        public string ReadDeviceModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            ushort modelRead;
            if (Cirbus.IsOpen)
                Cirbus.ClosePort();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.PerifericNumber = periferico;
                modelRead = Modbus.ReadHoldingRegister((ushort)RegistersModbus.DEVICE);
            };

            return ((ModelHardware)modelRead).ToString();
        }

        public string ReadSerialNumberModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            Cirbus.ClosePort();

            string sn = string.Empty;

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.PerifericNumber = 1;
                sn = Modbus.ReadHexStringHoldingRegister((ushort)RegistersModbus.SERIAL_NUMBER, 4);
            };

            Cirbus.OpenPort();
            return sn;
        }

        #endregion

        #region Metodos FILE y XML

        [Browsable(false)]
        public void StartGenerationFileCapture(byte ciclyNumber)
        {
            LogMethod();
            Cirbus.Write(CAPTUREFILEMEASURE, ciclyNumber);
        }

        [Browsable(false)]
        public string ReadStatusFileCapture()
        {
            LogMethod();
            return Cirbus.Read(CAPTUREFILEMEASURE,"");
        }

        [Browsable(false)]
        public string ImportXML(FileExportImport file)
        {
            LogMethod();
            return Cirbus.Read(IMPXML, listErrorFileExport, file.ToString());
        }

        [Browsable(false)]
        public string ExportXML(FileExportImport file)
        {
            LogMethod();
            return Cirbus.Read(EXPXML, listErrorFileExport, file.ToString());
        }

        #endregion    

        #region Dictionary

        private Dictionary<byte, string> listErrorFileExport = new Dictionary<byte, string>(){
             { 3, "Error al importat/Exportar fichero" },
             { 4, "Error al crear XSD" },
             { 6, "Fallo en la sintaxis del comando" }
        };

        private Dictionary<byte, string> listErrorTrip = new Dictionary<byte, string>(){
             { 1, "Error TRIP ocupado" },
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        protected Dictionary<byte, string> listErrorCirbus = new Dictionary<byte, string>(){
             { 1, "Error comando Cirbus" },
             { 2, "Error comando Cirbus" },
             { 6, "Fallo en la sintaxis del comando" },
             { 7, "Fallo cheksum del comando" }
        };

        private Dictionary<byte, string> listErrorMeasure = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorClock = new Dictionary<byte, string>(){
             { 1, "Falla EEP 1º Pagina" },
             { 2, "Falla EEP 2º Pagina" },
             { 3, "Falla EEP 1º y 2º Pagina" },
             { 4, "Fallo indeterminado" },
             { 5, "Comando calibración sin haber echo el comando Start Test" },
             { 6, "Error sintaxis del comando CLK" },
             { 7, "Error ChekSum del comando CLK" }
        };

        protected Dictionary<byte, string> listErrorCalibration = new Dictionary<byte, string>(){
             { 2, "Error parametro incorrecto" },
             { 3, "Ha fallado la grabación de la primera copia en EEP" },
             { 4, "Ha fallado la grabación de la segunda copia en EEP" },
             { 5, "Ha fallado la grabación de las dos copias en EEP" },
             { 6, "Fallo en la sintaxis del comando CAL" },
             { 7, "Fallo al iniciar test" }
        };

        protected Dictionary<byte, string> listErrorPPP = new Dictionary<byte, string>()
        {
             { 1, "Falla password" },
             { 2, "Falla recepción COMAux" },
             { 3, "Falla cerrojo flash" },
             { 4, "Fallo indeterminado" },
             { 5, "Fallo comando Cirbus" }
        };

        protected Dictionary<byte, string> listErrorFlash = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorBit = new Dictionary<byte, string>(){
             { 2, "Error direccion fuera de limites" },
             { 6, "Fallo en la sintaxis del comando CHR" },
        };

        private Dictionary<byte, string> listErrorDisplay = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<ushort, int> RelationCodeModel = new Dictionary<ushort, int>
        {
            {0, 100}, {1, 101}, {2, 102}, {3, 103}, {4, 202}, {5, 301}, {6, 302}, {7, 1000},
            {8, 1001}, {9, 1002}, {10, 2001}, {11, 2002}, {12, 3001}, {13, 3002}, {14, 1010}, {15, 1011},
            {16, 1012}, {17, 2011}, {18, 2012}, {19, 3011}, {20, 3012}, {21, 1020}, {22, 1021}, {23, 1022},
            {24, 2021}, {25, 2022}, {26, 3021}, {27, 3022}, {28, 1030}, {29, 1031}, {30, 1032}, {31, 2031},
            {32, 2032}, {33, 3031}, {34, 3032}, {35, 5020}, {36, 5022}, {37, 5030}, {38, 5032}, {39, 103},
            {40, 2024}, {41, 3024}
        };

        #endregion

        #region PasaTapas 

        public struct PasaTapas
        {
            public double L1_FactorAjuste { get; set; }
            public double L1_FactorCalibracion { get; set; }
            public double L2_FactorAjuste { get; set; }
            public double L2_FactorCalibracion { get; set; }
            public double L3_FactorAjuste { get; set; }
            public double L3_FactorCalibracion { get; set; }
            public double L4_FactorAjuste { get; set; }
            public double L4_FactorCalibracion { get; set; }
            public double L5_FactorAjuste { get; set; }
            public double L5_FactorCalibracion { get; set; }
            public double L6_FactorAjuste { get; set; }
            public double L6_FactorCalibracion { get; set; }
        }

        public PasaTapas FactorPasaTapas { get; set; }

        public void Factor_Pasa_Tapas(PasaTapas factorPasaTapas)
        {
            FactorPasaTapas = factorPasaTapas;
        }

        #endregion

        #region Relacion Transformacion

        public double Rel_Voltage_Measure { get; set; }

        public double Rel_Voltage_Vn_Measure { get; set; }

        public double Rel_Current_Measure { get; set; }

        public void Relacion_Voltage_Medida(double rel_Voltage_Measure)
        {
            Rel_Voltage_Measure = rel_Voltage_Measure;
        }
        public void Relacion_Voltage_Vn_Medida(double rel_Voltage_Vn_Measure)
        {
            Rel_Voltage_Vn_Measure = rel_Voltage_Vn_Measure;
        }

        public void Relacion_Corriente_Medida(double rel_Current_Measure)
        {
            Rel_Current_Measure = rel_Current_Measure;
        }
 
        #endregion
    }
}
