﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace WinTaskRunner.Models
{
    public class ComponentsViewItem
    {
        public string FileName { get; set; }
        public string Name { get; set; }
        public string FullName { get { return Path.GetFileName(FileName); } }
        public bool IsDirectory { get; set; }
        public bool IsFolder { get; set; }
        public bool IsDll { get; set; }
        public bool IsLazy { get; set; }
        public string ImageKey { get; set; }

        public Func<string, bool> Filter { get; set; }

        [JsonIgnore]
        public bool IsLoaded { get; set; }
    }
}
