﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.Styles;
using Telerik.WinControls;

namespace WinTestPlayer.Controls.Expanders
{
    class SettingsButtonStateManager : ItemStateManagerFactory
    {
        protected override StateNodeBase CreateSpecificStates()
        {
            CompositeStateNode baseStates = new CompositeStateNode("States");

            StateNodeBase expander = new StateNodeWithCondition("Selected", new SimpleCondition(ChevronButton.IsExpandedProperty, true));

            baseStates.AddState(expander);

            return baseStates;
        }


    }
}
