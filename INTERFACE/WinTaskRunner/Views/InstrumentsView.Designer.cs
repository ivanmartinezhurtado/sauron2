﻿using System.Windows.Forms;

namespace WinTaskRunner.Views
{
    partial class InstrumentsView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.radSplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.commandBarStrip = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripPowerSource = new Telerik.WinControls.UI.CommandBarStripElement();
            this.radDockInstruments = new Telerik.WinControls.UI.Docking.RadDock();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.Panel1.SuspendLayout();
            this.radSplitContainer1.Panel2.SuspendLayout();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDockInstruments)).BeginInit();
            this.radDockInstruments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // radSplitContainer1.Panel1
            // 
            this.radSplitContainer1.Panel1.Controls.Add(this.commandBarStrip);
            this.radSplitContainer1.Panel1MinSize = 48;
            // 
            // radSplitContainer1.Panel2
            // 
            this.radSplitContainer1.Panel2.AutoScroll = true;
            this.radSplitContainer1.Panel2.Controls.Add(this.radDockInstruments);
            this.radSplitContainer1.Panel2MinSize = 48;
            this.radSplitContainer1.Size = new System.Drawing.Size(431, 613);
            this.radSplitContainer1.SplitterDistance = 55;
            this.radSplitContainer1.SplitterWidth = 1;
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // commandBarStrip
            // 
            this.commandBarStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commandBarStrip.Location = new System.Drawing.Point(0, 0);
            this.commandBarStrip.Name = "commandBarStrip";
            this.commandBarStrip.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.commandBarStrip.Size = new System.Drawing.Size(431, 55);
            this.commandBarStrip.TabIndex = 0;
            this.commandBarStrip.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripPowerSource});
            // 
            // commandBarStripPowerSource
            // 
            this.commandBarStripPowerSource.DisplayName = "commandBarStripElement1";
            this.commandBarStripPowerSource.Name = "commandBarStripPowerSource";
            // 
            // radDockInstruments
            // 
            this.radDockInstruments.Controls.Add(this.documentContainer1);
            this.radDockInstruments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDockInstruments.IsCleanUpTarget = true;
            this.radDockInstruments.Location = new System.Drawing.Point(0, 0);
            this.radDockInstruments.MainDocumentContainer = this.documentContainer1;
            this.radDockInstruments.Name = "radDockInstruments";
            // 
            // 
            // 
            this.radDockInstruments.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radDockInstruments.Size = new System.Drawing.Size(431, 557);
            this.radDockInstruments.TabIndex = 0;
            this.radDockInstruments.TabStop = false;
            this.radDockInstruments.Text = "radDock1";
            // 
            // documentContainer1
            // 
            this.documentContainer1.AutoScroll = true;
            this.documentContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.SplitterWidth = 1;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(611, 42);
            this.splitPanel1.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 55);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.443662F);
            this.splitPanel1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -757);
            this.splitPanel1.TabIndex = 1;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPanel2.Location = new System.Drawing.Point(0, 43);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(611, 957);
            this.splitPanel2.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 957);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.443662F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 889);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // InstrumentsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "InstrumentsView";
            this.Size = new System.Drawing.Size(431, 613);
            this.radSplitContainer1.Panel1.ResumeLayout(false);
            this.radSplitContainer1.Panel1.PerformLayout();
            this.radSplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.commandBarStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDockInstruments)).EndInit();
            this.radDockInstruments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private SplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadCommandBar commandBarStrip;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripPowerSource;
        private Telerik.WinControls.UI.Docking.RadDock radDockInstruments;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
    }
}
