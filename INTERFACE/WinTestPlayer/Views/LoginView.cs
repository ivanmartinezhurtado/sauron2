﻿using System.Windows.Forms;
using Dezac.Tests.UserControls;
using WinTestPlayer.Services;
using Dezac.Services;
using Dezac.Data;

namespace WinTestPlayer.Views
{
    public partial class LoginView : DialogViewBase
    {
        private string roles = "";

        public LoginView()
        {
            InitializeComponent();
        }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            string userName = UserName;
            string password = Password;

            if (string.IsNullOrEmpty(userName))
                return false;

            if (string.IsNullOrEmpty(password))
                return false;

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                empleado = svc.Login(userName, password);
                roles = svc.GetUserParam("WebTSD", UserName, "Role");
            }

            if (empleado == null)
            {
                lblError.Text = "Usuario inexistente o password incorrecto";
                return false;
            }
            if (roles == null)
            {
                lblError.Text = "Usuario sin roles asignados";
                return false;
            }

            lblError.Text = "";
            return true;
        }

        public string Roles
        {
            get { return roles; }
        }

        public string UserName
        {
            get { return txtUserName.Text; }
            set { txtUserName.Text = value; }
        }

        public string Password
        {
            get { return txtPassword.Text; }
            set { txtPassword.Text = value; }
        }

        public bool PostVenta
        {
            get { return rbPostVenta.IsChecked; }
            set { rbPostVenta.IsChecked = value; }
        }

        public bool SAT
        {
            get { return rbSAT.IsChecked; }
            set { rbSAT.IsChecked = value; }
        }

        public bool Laser
        {
            get { return rbLaser.IsChecked; }
            set { rbLaser.IsChecked = value; }
        }

        public bool TIC
        {
            get { return rdTIC.IsChecked; }
            set { rdTIC.IsChecked = value; }
        }

        public bool AutoTest
        {
            get { return rbAutotest.IsChecked; }
            set { rbAutotest.IsChecked = value; }
        }

        public bool PrintLabel
        {
            get { return printLabel.IsChecked; }
            set { printLabel.IsChecked = value; }
        }


        public Model.ModoPlayer ModoTest
        {
            get
            {
                if (SAT)
                    if (roles.ToUpper().Contains("SAT"))
                        return Model.ModoPlayer.SAT;

                if (TIC)
                    if (roles.ToUpper().Contains("TIC"))
                        return Model.ModoPlayer.TIC;

                if (PostVenta)
                    if (roles.ToUpper().Contains("POSTVENTA"))
                        return Model.ModoPlayer.PostVenta;

                if (Laser)
                    if (roles.ToUpper().Contains("PRODADMIN"))
                        return Model.ModoPlayer.Laser;

                if (PrintLabel)
                    if (roles.ToUpper().Contains("PRINT"))
                        return Model.ModoPlayer.PrintLabel;

                if (AutoTest)
                    return Model.ModoPlayer.AutoTest;

                return Model.ModoPlayer.SinPermisos;
            }
        }

        public VW_EMPLEADO empleado { get; set; }
    }
}
