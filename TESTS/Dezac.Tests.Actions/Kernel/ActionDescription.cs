﻿using System.Drawing;

namespace Dezac.Tests.Actions
{
    public class ActionDescription
    {
        public virtual string Name { get { return null; } }
        public virtual string Description { get { return null; } }
        public virtual string Category { get { return null; } }
        public virtual Image Icon { get { return Properties.Resources.Action; } }
        public virtual string Dependencies { get { return null; } }
    }
}