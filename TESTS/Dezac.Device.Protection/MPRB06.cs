﻿using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class MPRB06 : DeviceBase
    {

        public MidabusDeviceSerialPort Midabus { get; internal set; }

        public MPRB06()
        {
        }

        public MPRB06(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 38400, byte periferic = 1)
        {
            if (Midabus == null)
                Midabus = new MidabusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, 8, _logger);
            else
                Midabus.PortCom = port;

            Midabus.PerifericNumber = periferic;
        }

        public override void Dispose()
        {
            if (Midabus != null)
                Midabus.Dispose();
        }
       
        private const string READ_VERSION = "10";
        private const string READ_FACTORY_STATE = "40";
        private const string INIT_CODE_ERROR = "C0FF";
        private const string RECORD_FABRICATION_STATE = "C09F";
        private const string READ_FABRICATION_STATE = "40";
        private const string TAKE_MEASURES = "AE00";
        private const string READ_CURRENT_POINTS = "2E";
        private const string READ_KEYBOARD = "17000E01";
        private const string DISPLAY = "98";
        private const string GAINS = "AD";
        private const string READ_GAINS = "2D";
        private const string PARAMETRIZATION = "93";
        private const string READ_PARAMETRIZATION = "13";
        private const string BASTIDOR = "99DE2ACFAB";
        private const string READ_BASTIDOR = "19";
        private const string RESET = "9C00";
        private const string READ_ERROR_CODE = "C0";

        //Read
        public string ReadVersion()
        {
            LogMethod();
            var result = Midabus.Read(READ_VERSION, "9005");
            var version = Convert.ToInt32(result.Substring(4, result.Length - 4)) / 1000d;
            return version.ToString();
        }

        public string ReadFabricationState()
        {
            LogMethod();
            return Midabus.Read(READ_FABRICATION_STATE, "C09F");
        }

        public CurrentsInAmpers ReadCurrent(Scales scale)
        {
            LogMethod();
            WriteStartMeasures(scale, 16);
            Thread.Sleep(1000);
            var currentPoints = ReadCurrentPoints();
            var gains = ReadGains();
            return new CurrentsInAmpers(scale, currentPoints, gains);
        }

        public Currents ReadCurrentPoints()
        {
            LogMethod();
            var reading =  Midabus.Read(READ_CURRENT_POINTS, "AE");
            return new Currents(reading);
        }

        public Gains ReadGains()
        {
            LogMethod();
            var reading = Midabus.Read(READ_GAINS, "AD");
            return new Gains(reading);
        }

        public Keys ReadKeyboard()
        {
            LogMethod();
            var key = Midabus.Read(READ_KEYBOARD, "97000E00").Replace("97000E00", "").Substring(0,2);
            return (Keys)(Convert.ToByte(key));
        }

        public Parametrization ReadParametrizacion()
        {
            LogMethod();
            var response = Midabus.Read(READ_PARAMETRIZATION, "93");
            return new Parametrization(response);
        }

        public string ReadBastidor()
        {
            LogMethod();
            return Midabus.Read(READ_BASTIDOR, "99");
        }

        public string ReadErrorCode()
        {
            LogMethod();
            return Midabus.Read(READ_ERROR_CODE, "00");
        }

        //Write
        public void WriteInitCodeError()
        {
            LogMethod();
            Midabus.Write(INIT_CODE_ERROR, "010000");
        }

        public void WriteParametrization(Parametrization parameters)
        {
            LogMethod();
            Midabus.Write(PARAMETRIZATION + parameters.ToFrame(), "00");
        }

        public void WriteFabricationState()
        {
            LogMethod();
            Midabus.Write(RECORD_FABRICATION_STATE, "0000");
        }

        public void WriteStartMeasures(Scales scale, int NumMuestras = 64)
        {
            LogMethod();
            Midabus.Read(string.Format(TAKE_MEASURES + "{0:00}{1:00}", NumMuestras.ToBytes().BytesToHexString().TrimStart('0'),(ushort)scale), "00");
        }

        public void WriteDisplay(DisplayState displayState)
        {
            LogMethod();
            var segments = displayState == DisplayState.NUMERIC ? "00EFEFEFEFEFEF00FFE7FFE7FFE7" : displayState == DisplayState.SYMBOLS ? "FF101010101010FF001800180018" : string.Empty;
            
            Midabus.Write(DISPLAY + segments, "00");
        }

        public void WriteGains(Gains gains)
        {
            LogMethod();
            Midabus.Write(GAINS + gains.ToFrame(), "0000");
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();
            Midabus.Write(string.Format(BASTIDOR + "{0:00000000}", bastidor), "00");
        }

        public void Reset()
        {
            LogMethod();
            Midabus.Write(RESET, "0000");
        }

        //Calibration

        public TriLineValue CalibrationPhaseCurrents(double currentConsigned, Scales scale, ConfigurationSampler config)
        {
            TriLineValue gains = new TriLineValue(); //Gains = ReadGains()  TODO

            double power = scale == Scales.SHORTCIRCUIT ? 16 : 24;

            var result = AdjustBase(0, config.SampleNumber, config.SampleMaxNumber, config.DelayBetweenSamples, 0,
            () =>
            {
                var res = ReadCurrentPoints();
                var points = new List<double>()
                    {
                        res.L1, res.L2, res.L3
                    };
                return points.ToArray();
            },
            (listValues) =>
            {
                gains.L1 = (long)(Math.Pow(2, power) * Math.Pow(10, 10) * (Math.Pow(currentConsigned, 2) * 64 / listValues.Average(0)));
                gains.L2 = (long)(Math.Pow(2, power) * Math.Pow(10, 10) * (Math.Pow(currentConsigned, 2) * 64 / listValues.Average(1)));
                gains.L3 = (long)(Math.Pow(2, power) * Math.Pow(10, 10) * (Math.Pow(currentConsigned, 2) * 64 / listValues.Average(2)));

                return true;               
            });

            return gains;
        }

        public double CalibrationHomopolarCurrents(double currentConsigned, Scales scale, ConfigurationSampler config)
        {
            double gain = 0;

            double power = scale == Scales.SHORTCIRCUIT ? 16 : 24;

            var result = AdjustBase(0, config.SampleNumber, config.SampleMaxNumber, config.DelayBetweenSamples, 0,
            () =>
            {
                var res = ReadCurrentPoints();
                var points = new List<double>()
                    {
                        res.LH
                    };
                return points.ToArray();
            },
            (listValues) =>
            {
                gain = (int)(Math.Pow(2, power) * Math.Pow(10, 10) * (Math.Pow(currentConsigned, 2) * 64 / listValues.Average(0)));             
                return true;
            });

            return gain;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Parametrization
        {
            public string NumVSecondary { get; set; }
            public string IdCurvaPhase { get; set; }
            public string IdCurvaT { get; set; }
            public string IPhase { get; set; }
            public string INominal { get; set; }
            public string TiPhase { get; set; }
            public string TiHomopolar { get; set; }
            public string IHomopolar { get; set; }
            public string Ie { get; set; }
            public string Tie { get; set; }

            public Parametrization(string frame)
            {
                NumVSecondary = frame.Substring(2, 2);
                IdCurvaPhase = frame.Substring(4, 2);
                IdCurvaT = frame.Substring(6, 2);
                IPhase = frame.Substring(8, 2);
                INominal = frame.Substring(10, 4);
                TiPhase = frame.Substring(14, 4);
                TiHomopolar = frame.Substring(18, 4);
                IHomopolar = frame.Substring(22, 2);
                Ie = frame.Substring(24, 2);
                Tie = frame.Substring(26, 4);
            }

            public string ToFrame()
            {
                return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}",
                    NumVSecondary, IdCurvaPhase, IdCurvaT, IPhase, INominal, TiPhase, TiHomopolar,IHomopolar,Ie, Tie);
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Gains
        {

            public int GL1ShortCircuit;
            public int GL2ShortCircuit;
            public int GL3ShortCircuit;
            public int GLHShortCircuit;

            public int GL1Precision;
            public int GL2Precision;
            public int GL3Precision;
            public int GLHPrecision;


            public Gains(string frame)
            {
                GL1ShortCircuit = Convert.ToInt32(frame.Substring(2, 8), 16);
                GL2ShortCircuit = Convert.ToInt32(frame.Substring(10, 8), 16);
                GL3ShortCircuit = Convert.ToInt32(frame.Substring(18, 8), 16);
                GLHShortCircuit = Convert.ToInt32(frame.Substring(26, 8), 16);

                GL1Precision = Convert.ToInt32(frame.Substring(34, 8), 16);
                GL2Precision = Convert.ToInt32(frame.Substring(42, 8), 16);
                GL3Precision = Convert.ToInt32(frame.Substring(50, 8), 16);
                GLHPrecision = Convert.ToInt32(frame.Substring(58, 8), 16);
            }

            public string ToFrame()
            {
                return string.Format("{0:00000000}{1:00000000}{2:00000000}{3:00000000}{4:00000000}{5:00000000}{6:00000000}{7:00000000}",
                    GL1ShortCircuit.ToBytes().BytesToHexString(), GL2ShortCircuit.ToBytes().BytesToHexString(), GL3ShortCircuit.ToBytes().BytesToHexString() , GLHShortCircuit.ToBytes().BytesToHexString(),
                    GL1Precision.ToBytes().BytesToHexString(), GL2Precision.ToBytes().BytesToHexString(), GL3Precision.ToBytes().BytesToHexString(), GLHPrecision.ToBytes().BytesToHexString());
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Currents
        {
            public long L1;
            public long L2;
            public long L3;
            public long LH;

            public Currents(string frame)
            {
                L1 = Convert.ToInt64(frame.Substring(2, 12), 16);
                L2 = Convert.ToInt64(frame.Substring(14, 12), 16);
                L3 = Convert.ToInt64(frame.Substring(26, 12), 16);
                LH = Convert.ToInt64(frame.Substring(38, 12), 16);
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CurrentsInAmpers
        {
            public double L1;
            public double L2;
            public double L3;
            public double LH;   
            
            public CurrentsInAmpers(Scales scale, Currents currentPoints, Gains gains)
            {
                double power = scale == Scales.SHORTCIRCUIT ? 16D : 24D;
                double gainL1 = scale == Scales.SHORTCIRCUIT ? gains.GL1ShortCircuit : gains.GL1Precision;
                double gainL2 = scale == Scales.SHORTCIRCUIT ? gains.GL2ShortCircuit : gains.GL2Precision;
                double gainL3 = scale == Scales.SHORTCIRCUIT ? gains.GL3ShortCircuit : gains.GL3Precision;
                double gainLH = scale == Scales.SHORTCIRCUIT ? gains.GLHShortCircuit : gains.GLHPrecision;

                L1 = Math.Sqrt((Convert.ToDouble(currentPoints.L1) * gainL1) / (Math.Pow(2, power) * Math.Pow(10, 10) * 16D));
                L2 = Math.Sqrt((Convert.ToDouble(currentPoints.L2) * gainL2) / (Math.Pow(2, power) * Math.Pow(10, 10) * 16D));
                L3 = Math.Sqrt((Convert.ToDouble(currentPoints.L3) * gainL3) / (Math.Pow(2, power) * Math.Pow(10, 10) * 16D));
                LH = Math.Sqrt((Convert.ToDouble(currentPoints.LH) * gainLH) / (Math.Pow(2, power) * Math.Pow(10, 10) * 16D));
            }
            

        }

        public enum Scales
        {
            SHORTCIRCUIT,
            PRECISION          
        }

        public enum Keys
        {
            NO_KEY = 0,
            FAULT_KEY = 1,
            LESS_KEY = 2,
            PLUS_KEY = 4,
            PAGE_KEY = 8,
            PROG_KEY = 10
        }

        public enum DisplayState
        {
            NUMERIC,
            SYMBOLS
        }

    }

    
}
