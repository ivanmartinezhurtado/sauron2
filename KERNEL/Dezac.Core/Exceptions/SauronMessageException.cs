﻿using System;

namespace Dezac.Core.Exceptions
{
    public class SauronMessageException
    {
        public string[] Category { get; set; }

        public string[] SubCategory { get; set; }

        public string[] Sintoma { get; set; }

        public string Parameter { get; set; }

        public string ClassName { get; set; }

        public string MessageError { get { return string.Format("Excepción de {0} {1} por {2} {3}", Category[1], SubCategory[1], Sintoma[1], Parameter); } }

        public string CodeError { get {  return string.Format("{0}{1}{2}{3}",  Category[0], SubCategory[0], Sintoma[0], GetParameterCode());  } }

        public SauronException Exception()
        {
            return new SauronException(MessageError, CodeError);
        }

        private string GetParameterCode()
        {
            var code = (Parameter ?? string.Empty).GetHashCode();

            return code.ToString("X8");
        }

        public static SauronMessageException Create(string[] category, string[] subCategory, string[] sintoma, string parameter)
        {
            var psl = new SauronMessageException();
            psl.Parameter = parameter;
            psl.Category = category;
            psl.SubCategory = subCategory;
            psl.Sintoma = sintoma;

            return psl;
        }
    }
}
