namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class FASE
    {
        public const string VERIFICAR = "120";

        public FASE()
        {
            this.DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
            this.PRODUCTOFASE = new HashSet<PRODUCTOFASE>();
            this.T_FAMILIAPARAM = new HashSet<T_FAMILIAPARAM>();
            this.T_GRUPOPARAMETRIZACION = new HashSet<T_GRUPOPARAMETRIZACION>();
            this.T_TESTFASE = new HashSet<T_TESTFASE>();
            this.T_TESTFASES = new HashSet<T_TESTFASES>();
            this.T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
        }
    
        public string IDFASE { get; set; }
        public string DESCRIPCION { get; set; }
        public string AVISOMARPTEVALID { get; set; }
        public string FASETEST { get; set; }
    
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }
        public virtual ICollection<PRODUCTOFASE> PRODUCTOFASE { get; set; }
        public virtual ICollection<T_FAMILIAPARAM> T_FAMILIAPARAM { get; set; }
        public virtual ICollection<T_GRUPOPARAMETRIZACION> T_GRUPOPARAMETRIZACION { get; set; }
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }
        public virtual ICollection<T_TESTFASES> T_TESTFASES { get; set; }
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }
    }
}
