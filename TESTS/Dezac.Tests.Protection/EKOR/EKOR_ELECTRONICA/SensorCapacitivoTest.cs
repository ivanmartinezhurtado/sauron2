﻿using Dezac.Tests.Model;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Auxiliars;
using Instruments.Measure;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using Instruments.IO;
using Instruments.Utility;

namespace Dezac.Tests.Protection
{

    [TestVersion(1.20)]
    public class SensorCapacitivoTest : TestBase
    {
        #region Variables

        private string PATH_IMAGE = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\SENSOR_CAPACITIVO\";

        private byte OUT_MUX_V_FREQ = 2;
        private byte OUT_CURRENT_MEASURE = 28;
        private byte OUT_VOLTAGE_MEASURE = 29;
        private byte OUT_MODELO_MEASURE = 13;
        private byte[] OUT_IJP = { 17, 18, 19, 20, 21, 22, 41, 42 };
        private byte[] OUT_VJP = { 0, 1, 2, 3, 40, 30 };
        private byte DESACTIVA_IN_FREQ = 15;

        private int Channel = 2;
        #endregion

        private Tower1 tower;

        public void TestInitialization()
        {
            tower = new Tower1();

            if (Shell.ShowDialog("PREPARACIÓN TEST", () => new ImageView("Quitar el cable del conector BNC del Yokogawa, coloque el interruptor en tensión tal y como muestra la imagen y pulse ACEPTAR", PATH_IMAGE + "BNC_DISCONNECT_CONTACT_TENSION.jpg"), System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                throw new Exception("Error, se ha cancelado la desconexión del BNC del Yokogawa a IN FREQ.");

            tower.Active24VDC();

            Channel=(int)Configuracion.GetDouble("CHANNEL_SIGNAL_GENERATOR",2,  ParamUnidad.SinUnidad);

            tower.IO.DO.On(DESACTIVA_IN_FREQ);
        }

        public void AsignPortMTX(string portMTX)
        {
            tower.Metrix3293.PortCom = Convert.ToByte(GetVariable<string>(portMTX, portMTX).Replace("COM", ""));
            tower.Metrix3293.SetAutoRange();
        }

        public void TestMeasureCapacitorsInputs(int waitTimeTester = 4000)
        {
            DeasctiveOuputs();

            tower.PTE.ApplyOff();

            tower.LAMBDA.Initialize();
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            var voltageCapacitor = new AdjustValueDef(Params.V.Null.TestPoint("INPUT_CAPACITOR").Name, 0, Params.V.Null.TestPoint("INPUT_CAPACITOR").Min(), Params.V.Null.TestPoint("INPUT_CAPACITOR").Max(), 0, 0, ParamUnidad.V);
            
            tower.IO.DO.On(13);

            Delay(500, "Espera apagado fuentes e Inputs");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V_DC.Null.TestPoint("INPUT_CAPACITOR").Name,12, ParamUnidad.V), 0.1);

            tower.PTE.ApplyPresets(Consignas.GetDouble(Params.V_AC.Null.TestPoint("INPUT_CAPACITOR").Name, 100, ParamUnidad.V), 0, 0);

            Delay(500, "Espera estabilización fuente");         

            Delay(waitTimeTester, "Tiempo de espera de lectura del tester");

            TestMeasureBase(voltageCapacitor, (step) =>
            {
                return tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
            }, 1, 4, 500);

            tower.IO.DO.Off(13);

            tower.PTE.ApplyOff();

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(500, "Espera apagado fuentes e Inputs");

        }

        public void TestMeasureVoltage(int waitTimeTester = 4000)
        {
            DeasctiveOuputs();

            tower.PTE.ApplyPresetsAndWaitStabilisation(68, 0, 0);

            var voltageJP = new Dictionary<AdjustValueDef, Byte>();
            var voltageJPvacio = new AdjustValueDef(Params.V.Null.EnVacio.Name, 0, Params.V.Null.EnVacio.Min(), Params.V.Null.EnVacio.Max(), 0, 0, ParamUnidad.V);

            string name = "";

            for (byte i = 0; i < OUT_VJP.Length; i++)
            {
                if (i == OUT_VJP.Length - 1)
                    name = "36KW";
                else
                    name = (i + 9).ToString("00");

                voltageJP.Add(new AdjustValueDef(Params.V.Null.TestPoint("JP" + name).Name, 0, Params.V.Null.TestPoint("JP" + name).Min(), Params.V.Null.TestPoint("JP" + name).Max(), 0, 0, ParamUnidad.V), OUT_VJP[i]);
            }

            tower.IO.DO.On(OUT_VOLTAGE_MEASURE);         

            Delay(waitTimeTester, "Tiempo de espera de lectura del tester");

            TestMeasureBase(voltageJPvacio, (step) =>
            {
                return tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
            }, 1, 4, 500);

            var referenceMeasure = voltageJPvacio.Value;

            foreach (KeyValuePair<AdjustValueDef, Byte> jp in voltageJP)
            {
                tower.IO.DO.On(jp.Value);

                Delay(waitTimeTester, "Tiempo de espera de lectura del tester");

                TestMeasureBase(jp.Key, (step) =>
                {
                    var measure = tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
                    return measure - referenceMeasure;
                }, 1, 4, 500);

                tower.IO.DO.Off(jp.Value);
            }
        }

        public void TestMeasureCurrent(int waitTimeTester = 4000)
        {
            DeasctiveOuputs();

            if (Shell.ShowDialog("CONECTAR CORRIENTE", () => new ImageView("Conecte el interruptor en Corriente y pulse ACEPTAR", PATH_IMAGE + "Corriente.jpg"), System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                throw new Exception("Error. se ha cancelado el conexiónado del interruptor en corriente.");

            tower.PTE.ApplyPresetsAndWaitStabilisation(400, 0, 0);

            var currentJP = new Dictionary<AdjustValueDef, Byte>();
            var currentJPvacio = new AdjustValueDef(Params.I.Null.EnVacio.Name, 0, Params.I.Null.EnVacio.Min(), Params.I.Null.EnVacio.Max(), 0, 0, ParamUnidad.V);

            string name = "";

            for (byte i = 0; i < OUT_IJP.Length; i++)
            {
                name = (i + 1).ToString("00");
                currentJP.Add(new AdjustValueDef(Params.I.Null.TestPoint("JP" + name).Name, 0, Params.I.Null.TestPoint("JP" + name).Min(), Params.I.Null.TestPoint("JP" + name).Max(), 0, 0, ParamUnidad.V), OUT_IJP[i]); //Vacio
            }

            tower.IO.DO.On(OUT_CURRENT_MEASURE);

            Delay(waitTimeTester, "Tiempo de espera de lectura del tester");

            TestMeasureBase(currentJPvacio, (step) =>
            {
                return tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
            }, 1, 4, 500);

            var referenceMeasure = currentJPvacio.Value;

            foreach (KeyValuePair<AdjustValueDef, Byte> jp in currentJP)
            {
                tower.IO.DO.On(jp.Value);

                Delay(waitTimeTester, "Tiempo de espera de lectura del tester");

                TestMeasureBase(jp.Key, (step) =>
                {
                    var measure = tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
                    return measure - referenceMeasure;
                }, 1, 4, 500);

                tower.IO.DO.Off(jp.Value);
            }

            tower.IO.DO.OffWait(500, OUT_CURRENT_MEASURE);
            tower.IO.DO.On(OUT_VOLTAGE_MEASURE);

            DeasctiveOuputs();

            var modeloDef = new AdjustValueDef(Params.I.Null.TestPoint("MODELO").Name, 0, Params.I.Null.TestPoint("MODELO").Min(), Params.I.Null.TestPoint("MODELO").Max(), 0, 0, ParamUnidad.V);

            tower.IO.DO.On(OUT_MODELO_MEASURE);

            Delay(waitTimeTester, "Tiempo de espera de lectura del tester");

            TestMeasureBase(modeloDef, (step) =>
            {
                return tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
            }, 1, 4, 500);

        }

        public void TestFrecuency()
        {
            DeasctiveOuputs();

            tower.PTE.ApplyOffAllAndWaitStabilisation();

            if (Shell.ShowDialog("CONECTAR CABLE BNC PARA MEDIDA DE FRECUENCIA", () => new ImageView("Conecte el conector BNC del Yokogawa a IN FREQ. del utillaje y pulse ACEPTAR", PATH_IMAGE + "BNC_CONNECT.jpg"), System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                throw new Exception("Error, se ha cancelado la conexión del conector BNC del Yokogawa a IN FREQ.");

            tower.IO.DO.Off(DESACTIVA_IN_FREQ);

            tower.SignalGenerator.SetSignal(new SignalInfo
            {
                Channel = Channel,
                Function = SignalFunction.SINUSOIDAL,
                Frequency = 50,
                Amplitude = 3,
                VoltageOffset = 0,
                Phase = 0,
            }, false);

            tower.SignalGenerator.On(Channel);
            Delay(1000, "Generando señal");

            var result = tower.MeasureMultimeter((InputMuxEnum)OUT_MUX_V_FREQ, MagnitudsMultimeter.VoltAC, 3000); //Medida 50Hz

            tower.SignalGenerator.Off(Channel);

            Assert.AreBetween(Params.V.Null.TestPoint("50Hz").Name, result, Params.V.Null.TestPoint("50Hz").Min(), Params.V.Null.TestPoint("50Hz").Max(), Error().UUT.MEDIDA.MARGENES("Error medida de 50Hz Tension fuera de margenes"), ParamUnidad.V);

            tower.MUX.Connect(InputMuxEnum.IN2, Outputs.FreCH1);

            tower.SignalGenerator.SetSignal(new SignalInfo
            {
                Channel = Channel,
                Function = SignalFunction.SINUSOIDAL,
                Frequency = 2000000,
                Amplitude = 3,
                VoltageOffset = 0,
                Phase = 0,
            }, true);
                       

            var voltPeak = tower.HP53131A.RunMeasure(HP53131A.Variables.VOLT_MAX, 3000);            

            Assert.AreBetween(Params.V.Null.TestPoint("2MHz").Name, voltPeak.Value, Params.V.Null.TestPoint("2MHz").Min(), Params.V.Null.TestPoint("2MHz").Max(), Error().UUT.MEDIDA.MARGENES("Error medida de 2MHz Tension fuera de margenes"), ParamUnidad.V);

            var freq = tower.HP53131A.RunMeasure(HP53131A.Variables.FREQ, 3000);

            tower.SignalGenerator.Off(Channel);

            Assert.AreBetween(Params.FREQ.Null.TestPoint("2MHz").Name, freq.Value, Params.FREQ.Null.TestPoint("2MHz").Min(), Params.FREQ.Null.TestPoint("2MHz").Max(), Error().UUT.MEDIDA.MARGENES("Error medida de 2MHz Frecuencia fuera de margenes"), ParamUnidad.V);

            tower.MUX.Reset();

        }

        public void TestFinish()
        {
            if (tower == null)
                return;

            tower.IO.DO.Off(13);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.PTE.ApplyOff();

            DeasctiveOuputs();

            tower.SignalGenerator.Off(Channel);

            tower.Dispose();
        }

        private void DeasctiveOuputs()
        {
            tower.IO.DO.Off(OUT_CURRENT_MEASURE);

            tower.IO.DO.Off(OUT_VOLTAGE_MEASURE);

            tower.IO.DO.Off(OUT_MODELO_MEASURE);

            for (byte i = 0; i < OUT_IJP.Length; i++)
                tower.IO.DO.Off(OUT_IJP[i]);
            for (byte i = 0; i < OUT_VJP.Length; i++)
                tower.IO.DO.Off(OUT_VJP[i]);
        }
    }
}
