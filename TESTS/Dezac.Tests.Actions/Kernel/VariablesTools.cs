﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace Dezac.Tests.Actions.Kernel
{
    public static class VariablesTools
    {
        private static readonly ILog logger = LogManager.GetLogger("VariablesTools");

        public static object ResolveValue(string name, string defValue = null, ParamUnidad? unidades = null, bool resolveResults = false, bool throwExceptionIfNotExists = true, bool? isSharedVariable = null)
        {
            if (name == null)
                return name;

            if (name.StartsWith("~"))
                return name.Remove(0, 1);

            if (!name.StartsWith("$"))
                return name;

            if (SequenceContext.Current == null)
                return null;

            var key = name.Trim().Substring(1);
            object variable = null;
            if (isSharedVariable == null || !isSharedVariable.Value) 
                variable = SequenceContext.Current.Variables.Get(key);
            if (variable != null)
                return variable;
            if (isSharedVariable == null || isSharedVariable.Value)
                variable = SequenceContext.Current.SharedVariables.Get(key);
            if (variable != null)
                return variable;

            key = key.Replace("-", ".");

            //Test Results
            var model = SequenceContext.Current.Services.Get<ITestContext>();
            if (!key.Contains("."))
            {
                if (resolveResults && SequenceContext.Current.ResultList.Any(p => p.Name == key))
                {
                    var resultItem = SequenceContext.Current.ResultList.Find(p => p.Name == key);
                    return resultItem.Value is ParamValue ? ((ParamValue)resultItem.Value).Valor : resultItem.Value?.ToString();
                }
                if (throwExceptionIfNotExists)
                    TestException.Create().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA($"\"{key}\"").Throw();
                return defValue;
            }

            var tokens = key.Split('.');
            var firstToken = tokens[0];
            object value = null;

            //Context parametrization
            if (model != null)
            {
                var valueList = VariablesResolver.GetPropertyValue(model, firstToken);
                ParamValueCollection paramsList = valueList as ParamValueCollection;
                if (paramsList != null)
                {
                    if (unidades.HasValue)
                        return paramsList.GetString(tokens[1], defValue, unidades.Value);
                    else
                        return paramsList.GetString(tokens[1], defValue, true);
                }
                ConfigurationFilesCollection configFilesList = valueList as ConfigurationFilesCollection;
                if (configFilesList != null)
                    return configFilesList.GetData(tokens[1]);

                if (firstToken.ToLower() == "testinfo")
                    value = model.TestInfo;
                else if (firstToken.ToLower() == "testcontext")
                    value = model;

                if (value != null)
                    for (int i = 1; i < tokens.Length; i++)
                        value = VariablesResolver.GetPropertyValue(value, tokens[i]);
            }

            if (value == null)
            {
                if (throwExceptionIfNotExists)
                    TestException.Create().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA($"\"{key}\"").Throw();
                return defValue;
            }

            return value ?? defValue;
        }

        public static object ResolveValue(RunMethodAction.Argument arg)
        {
            if (!string.IsNullOrEmpty(arg.Value) && arg.IsJSON)
            {
                var type = arg.ResolveType();

                if (type.IsArray && type.GetElementType().IsPrimitive)
                    return JsonConvert.DeserializeObject(arg.Value, type);

                var value = Activator.CreateInstance(type);

                JsonConvert.DeserializeObject<List<NameValuePair>>(arg.Value)
                    .ForEach(p =>
                    {
                        var argValue = VariablesTools.ResolveValue(p.Value);

                        if (string.IsNullOrEmpty(p.Name))
                            value = argValue;
                        else
                        {
                            var paths = p.Name.Split('.');
                            var current = value;

                            FieldInfo field = null;
                            PropertyInfo prop = null;

                            for (int i = 0; i < paths.Length; i++)
                            {
                                var last = i == paths.Length - 1;

                                field = current.GetType().GetField(paths[i]);
                                if (field != null)
                                {
                                    if (last)
                                        field.SetValue(current, Convert.ChangeType(argValue, field.FieldType));
                                    else
                                        current = field.GetValue(current);
                                }
                                else
                                {
                                    prop = current.GetType().GetProperty(paths[i]);
                                    if (prop != null)
                                    {
                                        if (last)
                                        {
                                            if (prop.PropertyType.IsEnum)
                                                prop.SetValue(current, RunnerHelper.ChangeType(Enum.Parse(prop.PropertyType, argValue.ToString()), prop.PropertyType));
                                            else
                                                prop.SetValue(current, RunnerHelper.ChangeType(argValue, prop.PropertyType));
                                        }
                                        else
                                            current = prop.GetValue(current);
                                    }
                                }
                            }
                        }
                    });

                return value;
            }

            return VariablesTools.ResolveValue(arg.Value, arg.DefaultValue, arg.Unidades);
        }

        public static string ResolveText(string text)
        {
            if (text == null || !text.Contains("$"))
                return text;

            var matches = new Regex(@"([~]*\$[a-zA-Z][\w|.]*)").Matches(text);

            foreach (Match match in matches)
            {
                if (match.Value.StartsWith("~"))
                {
                    var value = match.Value.Remove(0,1);
                    text = text.Replace(match.Value, value);
                }
                else
                {
                    var value = VariablesTools.ResolveValue(match.Value).ToString();
                    logger.Info($"Variable {match.Value} with value {value} Found");
                    value = ResolveText(value);
                    text = text.Replace(match.Value, value);
                }
            }
            logger.Info($"Text Resolved: \"{text}\"");
            return text;
        }

        public static double ResolveEquation(string equation)
        {
            var variables = Regex.Matches(equation, @"\B\$[\w\.]+");

            foreach (Match variable in variables)
            {
                var value = VariablesTools.ResolveValue(variable.Value);
                logger.Info($"Variable {variable.Value} with value {value} Found");
                equation = Regex.Replace(equation, $@"\B\${variable.ToString().Replace("$", "")}(?![\w\d])", value.ToString().Replace(",","."));
            }
            logger.Info($"Equation resolved: \"{equation}\"");

            equation = Regex.Replace(equation, @"\B\$[\w\.]+", "");

            var matches = Regex.Matches(equation, @"\b[A-Z][\w.]+");

            foreach (Match match in matches)
            {
                logger.Info($"System.Math library reference found: {match.Value}");
                equation = equation.Replace(match.Value, "System.Math." + match);
            }
            logger.Info($"Equation with references: \"{equation}\"");

            var ScriptService = new ScriptService();
            double result = Convert.ToDouble(ScriptService.Evaluate(equation));
            logger.Info($"Equation Result: {equation} = {result}");

            return result;
        }

        public static List<object> GetVariablesValuesFromExpresion(string expresion)
        {
            var values = new List<object>();
            if (expresion == null || !expresion.Contains("$"))
                return values;

            var matches = new Regex(@"(\$[a-zA-Z][\w|.]*)").Matches(expresion);
            foreach (Match match in matches)
                values.Add(VariablesTools.ResolveValue(match.Value));

            return values;
        }

        public static List<string> GetVariablesKeysFromExpresion(string expresion)
        {
            var keys = new List<string>();
            if (expresion == null || !expresion.Contains("$"))
                return keys;

            var matches = new Regex(@"(\$[a-zA-Z][\w|.]*)").Matches(expresion);
            foreach (Match match in matches)
                keys.Add(match.Value);

            return keys;
        }

        public static void SetVariable(string key, object value, bool? isSharedVariable)
        {
            if (SequenceContext.Current == null)
                return;

            if (key == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA().Throw();

            key = key.Replace("$", "");

            if (key.Contains("."))
            {
                var tokens = key.Split('.');
                var model = SequenceContext.Current.Services.Get<ITestContext>();
                if (model != null && tokens[0].ToLower() == "testinfo")
                {
                    VariablesResolver.SetVariableProperty(model.TestInfo, key, value);
                    return;
                }
                else if (model != null && tokens[0].ToLower() == "testcontext")
                {
                    VariablesResolver.SetVariableProperty(model, key, value);
                    return;
                }
            }

            if (isSharedVariable == null || !isSharedVariable.Value)
                SequenceContext.Current.Variables.AddOrUpdate(key, value);
            else
                SequenceContext.Current.SharedVariables.AddOrUpdate(key, value);
        }
    }
}
