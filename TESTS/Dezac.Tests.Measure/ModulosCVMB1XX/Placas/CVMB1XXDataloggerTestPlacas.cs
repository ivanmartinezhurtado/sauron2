﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Instruments.Router;
using Instruments.Towers;
using System;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.11)]
    public class CVMB1XXDataloggerTestPlacas : TestBase
    {
        TowerBoard tower;
        CVMB1XXModuloDatalogger cvm;
        public CVMB1XXModuloDatalogger Cvm
        {
            get
            {
                if (cvm != null)
                    return cvm;
                return null;
            }
            set
            {
                cvm = value;
            }
        }

        CVMB1XX cvmDummy;
        public CVMB1XX CvmDummy
        {
            get
            {
                if (cvmDummy != null)
                    return cvmDummy;
                return null;
            }
            set
            {
                cvmDummy = value;
            }
        }

        private byte POWER_SUPPLY_A = 17;
        private byte POWER_SUPPLY_B = 18;

        private bool IstypeA;

        private const byte PILOTO_ERROR = 43;

        public void TestInitialization(byte port = 5, string ip = "100.0.0.1")
        {
            cvm = AddInstanceVar(new CVMB1XXModuloDatalogger(port), "UUT");
            cvm.Modbus.PerifericNumber = 100;
            cvm.Modbus.TimeOut = 200;
            cvm.Modbus.RtsEnable = true;
            cvm.Modbus.DtrEnable = true;
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", cvm.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", cvm.GetType().Name), ParamUnidad.SinUnidad);

            CvmDummy = new CVMB1XX(7);

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
            tower.Active24VDC();
            tower.IO.DO.Off(PILOTO_ERROR);

            IstypeA = Configuracion.GetString("CVM_TYPE", "A", ParamUnidad.SinUnidad) == "A" ? true : false;

            if (IstypeA)
                tower.IO.DO.On(POWER_SUPPLY_A);
            else
                tower.IO.DO.On(POWER_SUPPLY_B);

            SetVariable("deviceIP", ip); 
        }

        public void WriteDateTime()
        {
            CvmDummy.FlagTest();

            cvm.WriteFlagTest();

            DateTime fecha = DateTime.Now;

            CvmDummy.WriteDateTime(fecha);

            Delay(4000, "Espera arrancar el equipo");

            CvmDummy.FlagTest(false);
        }

        public void HttpSendHttpLockSession()
        {
            var IP_asigned = GetVariable("IP_DATALOGGER");
            cvm.Host = IP_asigned.ToString();

            logger.InfoFormat("IP Asigned --> {0}", cvm.Host);

            cvm.SendHttpLockSession();

            logger.InfoFormat("SendHttpLockSession --> OK");
        }

        public void HttpSendHttpXmlConfiguration()
        {
            
            var driver = Parametrizacion.GetString("DRIVER", "CVMA1500CONTAINER", ParamUnidad.SinUnidad);
            var name = Parametrizacion.GetString("NAME", "CVM-A1500", ParamUnidad.SinUnidad);

            Delay(4000, "Start SendHttpXmlConfiguration");

            cvm.SendHttpXmlConfiguration(driver, name);

            logger.InfoFormat("SendHttpXmlConfiguration --> OK");
        }

        public void HttpSendHttpXmlConfiguration(string driver, string name)
        {           

            Delay(4000, "Start SendHttpXmlConfiguration");

            cvm.SendHttpXmlConfiguration(driver, name);

            logger.InfoFormat("SendHttpXmlConfiguration --> OK");
        }

        public void HttpSendHttpSynchronize()
        {
            Delay(6000, "Start SendHttpSynchronize");

            cvm.SendHttpSynchronize();

            logger.InfoFormat("SendHttpSynchronize --> OK");
        }

        public void HttpSendHttpUnLockSession()
        {
            Delay(6000, "Start SendHttpUnLockSession");

            cvm.SendHttpUnLockSession();

            logger.InfoFormat("SendHttpUnLockSession --> OK");

        }

        public void TestCommunicationEmbedded()
        {            

            SamplerWithCancel((p) => {               

                var softwareVersion = CvmDummy.ReadKernelVersion().CToNetString();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_EMBEDDED, softwareVersion, Identificacion.VERSION_EMBEDDED, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. La versión del embedded es incorrecta"), ParamUnidad.SinUnidad);

                Delay(2000, "Espera PSS");

                var powerStudioVersion = CvmDummy.ReadPowerStudioVersion().CToNetString();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_POWER_STUDIO, powerStudioVersion, Identificacion.VERSION_POWER_STUDIO, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. La versión del PowerStudioScada es incorrecta"), ParamUnidad.SinUnidad);

                return true;

            }, "", 25, 2500, 2000, false, false);

        }

        public void TestFinish()
        {
            if (cvm != null)
                cvm.Dispose();

            if (CvmDummy != null)
                CvmDummy.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(14, POWER_SUPPLY_A, POWER_SUPPLY_B);
                tower.ShutdownSources();
                tower.Dispose();
            }
        }
    }
}
