﻿using Dezac.Core.Utility;
using Dezac.Tests.Services;
using Microsoft.CSharp;
using Newtonsoft.Json;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using WinTaskRunner.Services;
using WinTaskRunner.Utils;

namespace WinTaskRunner.Views
{
    public partial class ModbusRegisterView : UserControl
    {
        protected readonly IShell shell;

        private BindingList<ModbusRegisterItemVarModel> variables = new BindingList<ModbusRegisterItemVarModel>();
        private ModbusConnectionsService modbusConnectionsService = new ModbusConnectionsService();
        private ModbusConnection ModbusConnection { get; set; }

        int num = 0;

        public ModbusRegisterView()
        {
            InitializeComponent();

            ((GridViewComboBoxColumn)grid.Columns["Function"]).DataSource = new string[] { "COIL", "DI", "IR", "HR" };
            ((GridViewComboBoxColumn)grid.Columns["Access"]).DataSource = new string[] { "RD", "WR", "RD_WR" };
            ((GridViewComboBoxColumn)grid.Columns["Type"]).DataSource = new string[] { "bool", "byte", "uint16", "int16", "uint32", "int32", "uint64", "int64", "float16", "float32" };

            ModbusConnection = modbusConnectionsService.Add(ModbusConnection.ModbusConnectionType.Serial);
            modbusProps.SelectedObject = ModbusConnection;

            bsList.DataSource = variables;
            grid.DataSource = bsList;
        }

        public ModbusRegisterView(IShell shell)
            : this()
        {
            this.shell = shell;
        }

        private void UpdateNodes()
        {
            var groups = variables.Select(p => p.Group).Distinct().ToList();

            if (tree.Nodes.Count != groups.Count)
            {
                tree.Nodes.Clear();
                groups.ForEach(p => tree.Nodes.Add(new RadTreeNode { Text = p, Checked = true }));
            }
            else
                for (int i = 0; i < groups.Count; i++)
                    tree.Nodes[i].Text = groups[i];
        }

        private void cmdOpen_Click(object sender, EventArgs e)
        {

            var ofd = new OpenFileDialog();
            ofd.Filter = "Modbus CSV files (*.csv)|*.csv|JSON files (*.json)|*.json|Text files (*.txt)|*.txt|All files (*.*)|*.*";

            string currentPath = ConfigurationManager.AppSettings["PathDeviceLibrary"];

            if (!string.IsNullOrEmpty(currentPath))
                ofd.InitialDirectory = currentPath;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Clear();

                tree.Enabled = false;
                grid.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;

                try
                {
                    List<ModbusRegisterItemVarModel> items = null;

                    if (Path.GetExtension(ofd.FileName) == ".json")
                        items = AppProfile.DeserializeFile<List<ModbusRegisterItemVarModel>>(ofd.FileName);
                    else
                    {
                        items = new List<ModbusRegisterItemVarModel>();
                        string content = File.ReadAllText(ofd.FileName);

                        var sr = new StringReader(content);
                        string line = null;
                        int nextAddress = -1;
                        char delimiter = ';';
                        string currentFunction = null;

                        while ((line = sr.ReadLine()) != null)
                        {
                            if (string.IsNullOrEmpty(line))
                                continue;

                            bool separate = line.Split(delimiter).Count() > 12;
                            if (!separate)
                                continue;

                            string[] tokens = line.Split(delimiter);

                            if (string.IsNullOrEmpty(tokens[1]))
                                continue;

                            if (string.IsNullOrEmpty(tokens[2]) && currentFunction == null)
                                continue;

                            if (string.IsNullOrEmpty(tokens[3]))
                                continue;


                            if (!int.TryParse(tokens[4], out int numRegister))
                                continue;

                            if (string.IsNullOrEmpty(tokens[5]))
                                continue;

                            if (string.IsNullOrEmpty(tokens[6]))
                                continue;

                            try
                            {
                                int address = Convert.ToInt32(tokens[0]);
                                var value = new ModbusRegisterItemVarModel();
                                value.Address = address.ToString("X4");
                                value.Function = string.IsNullOrEmpty(tokens[2]) ? currentFunction : tokens[2];
                                value.Name = tokens[3];
                                value.NumRegisters = numRegister;
                                value.Type = value.Function == "COIL" ? "bool" : tokens[5];
                                value.Access = tokens[6];
                                value.DefaultValue = tokens[7];
                                value.Minimum = tokens[8];
                                value.Maximum = tokens[9];
                                value.Unit = tokens[10];
                                value.Scale = tokens[11];
                                value.Description = tokens.Length > 12 ? tokens[12] : null;
                                value.Group = tokens.Length > 13 ? tokens[13] : null;

                                nextAddress = address + value.NumRegisters;
                                currentFunction = value.Function;
                                items.Add(value);
                            }
                            catch (Exception ex)
                            {
                                shell.MsgBox(string.Format("Address:{0} Function:{1} Description:{2}  Exception:{3}", tokens[1], tokens[2], tokens[3], ex.Message), "ERROR CARGAR CSV");
                                continue;
                            }
                        }
                    }

                    items.ForEach(p => variables.Add(p));
                }
                catch (Exception ex)
                {
                    shell.MsgBox(ex.Message, "ERROR");
                }
                finally
                {
                    tree.Enabled = true;
                    grid.Enabled = true;
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void Clear()
        {
            variables.Clear();
            tree.Nodes.Clear();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.DefaultExt = "csv";
            sfd.Filter = "Modbus CSV files (*.csv)|*.csv|JSON files (*.json)|*.json|Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (Path.GetExtension(sfd.FileName) == ".json")
                    AppProfile.SerializeFile(sfd.FileName, variables);
                else
                {
                    var sb = new StringBuilder();
                    sb.AppendLine("MODBUS MAPPING REGISTERS");
                    sb.AppendFormat("DEVICE ", txtDeviceName.Text);
                    sb.AppendFormat("DATE {0:dd/MM/yyyy}", DateTime.Now);
                    sb.AppendLine("Address;Hex add.;Block;Name;N. Reg.;Type;Access;Default value;Minimum;Maximum;Units;Scale;Description;Group");
                    variables.ToList().ForEach(p => sb.AppendLine(p.ToString()));
                    File.WriteAllText(sfd.FileName, sb.ToString());
                }
            }
        }

        //private void Run()
        //{
        //    num++;

        //    var enabled = tree.Nodes.Where(p => p.Checked).Select(p => p.Text);

        //    variables
        //        .Where(p => enabled.Contains(p.Group))
        //        .ToList()
        //        .ForEach(p => RunItem(p.Group));
        //}

        //private void RunItem(string group)
        //{
        //    try
        //    {
        //        var vars = variables.Where(p => p.Group == group).ToList();

        //        var first = vars.FirstOrDefault();
        //        var address = Convert.ToUInt16(first.Address, 16);

        //        if (vars.Count == 1)
        //            ReadModbusItemValue(first);
        //        else
        //        {
        //            if (deviceType == null)
        //                Build();

        //            var structType = deviceType
        //                .Assembly
        //                .GetExportedTypes()
        //                .Where(p => p.Name == group && p.IsValueType && !p.IsPrimitive)
        //                .FirstOrDefault();

        //            var result = ModbusConnection.Device.ReadStruct(structType, address);

        //            foreach (var v in vars)
        //            {
        //                var fld = structType.GetField(v.Valor.Replace(" ", "_"));
        //                v.Data = fld.GetValue(result);

        //                var value = 0D;
        //                if (double.TryParse(v.Valor, out value))
        //                    DataPointHub.Add(this, v.Name, value, "ModbusRegisterView");
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        private void ReadModbusItemValue(ModbusRegisterItemVarModel item)
        {
            if (item == null)
                return;

            tree.Enabled = false;
            grid.Enabled = false;

            try
            {
                ModbusConnection.Open();

                ushort address = Convert.ToUInt16(item.Address, 16);
                ushort length = (ushort)item.NumRegisters;
                string type = item.Type.ToUpper().Trim();
                string scale = item.Scale.ToUpper().Trim();

                if (item.Function == "COIL")
                    item.Data = ModbusConnection.Device.ReadCoil(address);
                else if (item.Function == "DI")
                    item.Data = ModbusConnection.Device.ReadSinlgeInputs(address);
                else if (item.Function == "IR")
                {
                    if (scale.Contains("EPOCH"))
                    {
                        if (length == 2)
                            item.Data = ModbusConnection.Device.ReadInt32Epoch(address);
                        else if (length == 4)
                            item.Data = ModbusConnection.Device.ReadInt64Epoch(address);
                    }
                    else if (length == 1 && type.Contains("UINT16"))
                        item.Data = ModbusConnection.Device.ReadRegister(address);
                    else if (length == 1 && type.Contains("INT16"))
                        item.Data = (short)(ModbusConnection.Device.ReadRegister(address));
                    else if (length == 2 && type.Contains("UINT32"))
                        item.Data = (uint)ModbusConnection.Device.ReadInt32(address);
                    else if (length == 2 && type.Contains("INT32"))
                        item.Data = ModbusConnection.Device.ReadInt32(address);
                    else if (length == 4 && type.Contains("UINT64"))
                        item.Data = (ulong)(ModbusConnection.Device.ReadInt64(address));
                    else if (length == 4 && type.Contains("INT64"))
                        item.Data = ModbusConnection.Device.ReadInt64(address);
                    else if (length == 1 && type.Contains("BYTE"))
                        item.Data = ModbusConnection.Device.ReadRegister(address);
                    else if (type.Contains("STRING"))
                        item.Data = ModbusConnection.Device.ReadString(address, length);
                    else if (length == 2 && type.Contains("FLOAT"))
                        item.Data = ModbusConnection.Device.ReadFloat32(address);
                    else if (length == 1 && type.Contains("FLOAT"))
                        item.Data = ModbusConnection.Device.ReadFloat16(address);
                    else
                        throw new Exception("Registro mal implementado, la longitud y el tipo no son coherentes");
                }
                else if (item.Function == "HR")
                {
                    if (scale.Contains("EPOCH"))
                    {
                        if (length == 2)
                            item.Data = ModbusConnection.Device.ReadInt32EpochHoldingRegister(address);
                        else if (length == 4)
                            item.Data = ModbusConnection.Device.ReadInt64EpochHoldingRegister(address);
                    }
                    else if (length == 1 && type.Contains("UINT16"))
                        item.Data = ModbusConnection.Device.ReadHoldingRegister(address);
                    else if (length == 1 && type.Contains("INT16"))
                        item.Data = (short)(ModbusConnection.Device.ReadHoldingRegister(address));
                    else if (length == 2 && type.Contains("UINT32"))
                        item.Data = (uint)(ModbusConnection.Device.ReadInt32HoldingRegisters(address));
                    else if (length == 2 && type.Contains("INT32"))
                        item.Data = ModbusConnection.Device.ReadInt32HoldingRegisters(address);
                    else if (length == 4 && type.Contains("UINT64"))
                        item.Data = (ulong)(ModbusConnection.Device.ReadInt64HoldingRegisters(address));
                    else if (length == 4 && type.Contains("INT64"))
                        item.Data = ModbusConnection.Device.ReadInt64HoldingRegisters(address);
                    else if (type.Contains("STRING"))
                        item.Data = ModbusConnection.Device.ReadStringHoldingRegister(address, length);
                    else if (length == 1 && type.Contains("BYTE"))
                        item.Data = ModbusConnection.Device.ReadHoldingRegister(address);
                    else if (length == 2 && type.Contains("FLOAT"))
                        item.Data = ModbusConnection.Device.ReadFloat32HoldingRegister(address);
                    else if (length == 1 && type.Contains("FLOAT"))
                        item.Data = ModbusConnection.Device.ReadFloat16HoldingRegister(address);
                    else
                        throw new Exception("Registro mal implementado, la longitud y el tipo no son coherentes");
                }

                if (double.TryParse(item.Valor, out double value))
                    DataPointHub.Add(this, item.Name, value, "ModbusRegisterView");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de comunicaciones por " + ex.Message);
            }
            finally
            {
                ModbusConnection.Close();

                tree.Enabled = true;
                grid.Enabled = true;
            }
        }

        private void WriteModbusItemValue(ModbusRegisterItemVarModel item)
        {
            if (item == null)
                return;

            tree.Enabled = false;
            grid.Enabled = false;

            try
            {
                ModbusConnection.Open();

                ushort address = Convert.ToUInt16(item.Address, 16);
                ushort length = (ushort)item.NumRegisters;
                string type = item.Type.ToUpper().Trim();
                string scale = item.Scale.ToUpper().Trim();

                if (item.Function == "COIL")
                {
                    bool data = false;
                    if (byte.TryParse(item.Parameter, out byte value))
                        data = value == 1;
                    else
                        data = item.Parameter.ToUpper().Trim().Contains("TRUE");

                    ModbusConnection.Device.WriteSingleCoil(address, data);
                }
                else if ((item.Function == "IR") || (item.Function == "HR"))
                {
                    if (scale.Contains("EPOCH"))
                    {
                        if (length == 2)
                            ModbusConnection.Device.WriteInt32Epoch(address, Convert.ToDateTime(item.Parameter));
                        else if (length == 4)
                            ModbusConnection.Device.WriteInt64Epoch(address, Convert.ToDateTime(item.Parameter));
                    }
                    if (length == 1 && type.Contains("UINT16"))
                        ModbusConnection.Device.Write(address, Convert.ToUInt16(item.Parameter));
                    else if (length == 1 && type.Contains("INT16"))
                        ModbusConnection.Device.Write(address, Convert.ToInt16(item.Parameter));
                    else if (length == 2 && type.Contains("UINT32"))
                        ModbusConnection.Device.WriteInt32(address, Convert.ToInt32(item.Parameter));
                    else if (length == 2 && type.Contains("INT32"))
                        ModbusConnection.Device.WriteInt32(address, Convert.ToInt32(item.Parameter));
                    else if (length == 4 && type.Contains("UINT64"))
                        ModbusConnection.Device.WriteInt64(address, Convert.ToInt64(item.Parameter));
                    else if (length == 4 && type.Contains("INT64"))
                        ModbusConnection.Device.WriteInt64(address, Convert.ToInt64(item.Parameter));
                    else if (type.Contains("STRING"))
                        ModbusConnection.Device.WriteString(address, item.Parameter);
                    else if (length == 2 && type.Contains("FLOAT"))
                        ModbusConnection.Device.WriteFloat32(address, (float)Convert.ToDouble(item.Parameter));
                    else if (length == 1 && type.Contains("FLOAT"))
                        ModbusConnection.Device.WriteFloat16(address, (float16)Convert.ToDouble(item.Parameter));
                    else
                        throw new Exception("Registro mal implementado, la longitud y el tipo no son coherentes");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de comunicaciones por " + ex.Message);
            }
            finally
            {
                ModbusConnection.Close();

                tree.Enabled = true;
                grid.Enabled = true;
            }
        }

        private string GenerateDeviceCode(string deviceName, string division)
        {
            byte[] fileName = Properties.Resources.ModbusDevice; // Path.Combine(ConfigurationManager.AppSettings["PathRazorTemplates"], "ModbusDevice.cshtml");

            string template = System.Text.Encoding.UTF8.GetString(fileName);

            var model = new GenerateDeviceCodeModel
            {
                Name = deviceName,
                Division = division,
                Variables = variables.ToList()
            };

            string dc = DateTime.Now.Ticks.ToString();

            try
            {
                string result = Engine.Razor.RunCompile(template, fileName + dc, model.GetType(), model);

                return result;
            }
            catch (TemplateCompilationException ex)
            {
                MessageBox.Show(ex.ToString());

                return ex.ToString();
            }
        }

        private void Build(string DeviceName, string Division)
        {
            string code = GenerateDeviceCode(DeviceName, Division);
            Build(code, DeviceName, Division);
        }

        private void Build(string code, string DeviceName, string Division)
        {
            var csc = new CSharpCodeProvider();
            var parameters = new CompilerParameters();
            parameters.GenerateExecutable = false;
            parameters.GenerateInMemory = true;
            string path = ConfigurationManager.AppSettings["PathDeviceLibrary"];
            Directory.CreateDirectory(path);
            parameters.OutputAssembly = string.Format("{0}\\Device.Generated.{1}.{2}.dll", path, Division, DeviceName);

            parameters.ReferencedAssemblies.Add(Path.Combine(AppProfile.ProgramPath, "Comunications.dll"));
            parameters.ReferencedAssemblies.Add(Path.Combine(AppProfile.ProgramPath, "Dezac.Core.dll"));
            parameters.ReferencedAssemblies.Add(Path.Combine(AppProfile.ProgramPath, "Dezac.Device.dll"));
            parameters.ReferencedAssemblies.Add(Path.Combine(AppProfile.ProgramPath, "log4net.dll"));

            IEnumerable<IGrouping<string, System.Reflection.Assembly>> assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(p => !p.IsDynamic && p.Location.EndsWith(".dll"))
                .GroupBy(p => p.FullName);

            foreach (IGrouping<string, System.Reflection.Assembly> asm in assemblies)
                parameters.ReferencedAssemblies.Add(asm.First().Location);

            var sb = new StringBuilder();

            CompilerResults results = csc.CompileAssemblyFromSource(parameters, code);
            results.Errors.Cast<CompilerError>()
                .ToList()
                .ForEach(error => sb.Append(error.ErrorText));

            if (sb.Length > 0)
                shell.MsgBox(sb.ToString(), "Atención");
            else
                shell.MsgBox(string.Format("Device.Generated.{0}.{1}.dll se ha creado correctamente en el path {2}", Division, DeviceName, path), "Succes generate device library");
        }

        private void grid_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column?.Name == "Read")
            {
                var item = (ModbusRegisterItemVarModel)e.Row.DataBoundItem;
                if (item.Access.Contains("RD"))
                    ReadModbusItemValue(item);
            }

            if (e.Column?.Name == "Write")
            {
                var item = (ModbusRegisterItemVarModel)e.Row.DataBoundItem;
                if (item.Access.Contains("WR") && item.Parameter != null)
                    WriteModbusItemValue(item);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var value = new ModbusRegisterItemVarModel
            {
                NumRegisters = 1,
                Access = "RD",
                DefaultValue = "NA",
                Minimum = "NA",
                Maximum = "NA",
                Unit = "NA",
                Scale = "NA",
            };

            ModbusRegisterItemVarModel last = variables.LastOrDefault();
            if (last != null)
            {
                int adress = Convert.ToInt32(last.Address, 16) + last.NumRegisters;

                value.Group = last.Group;
                value.Address = adress.ToString("X4");
                value.Function = last.Function;
                value.Name = last.Name;
                value.NumRegisters = last.NumRegisters;
                value.Type = last.Type;
                value.Access = last.Access;
                value.Minimum = last.Minimum;
                value.Maximum = last.Maximum;
                value.Scale = last.Scale;
                value.Unit = last.Unit;
            }

            bsList.Add(value);
        }

        private void bsList_ListChanged(object sender, ListChangedEventArgs e)
        {
            UpdateNodes();
        }

        private void tree_SelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            grid.MasterTemplate.Refresh();
        }

        private void tree_Edited(object sender, TreeNodeEditedEventArgs e)
        {
            if (e.Node == null || e.Node.Tag == null)
                return;

            string oldText = e.Node.Tag.ToString();

            if (oldText != e.Node.Text)
            {
                bsList.RaiseListChangedEvents = false;
                variables.Where(p => p.Group == oldText)
                    .ToList()
                    .ForEach(p => p.Group = e.Node.Text);
                bsList.RaiseListChangedEvents = true;
                grid.MasterTemplate.Refresh();
            }
        }

        private void tree_Editing(object sender, TreeNodeEditingEventArgs e)
        {
            if (e.Node == null || e.Node.Text == "")
                return;

            e.Node.Tag = e.Node.Text;

            e.Editor.Value = e.Node.Text;
            e.Node.Value = e.Node.Text;
        }

        private void MasterTemplate_RowFormatting(object sender, Telerik.WinControls.UI.RowFormattingEventArgs e)
        {
            var item = e.RowElement.RowInfo.DataBoundItem as ModbusRegisterItemVarModel;

            if (tree.SelectedNode != null && tree.SelectedNode.Text == item.Group)
            {
                e.RowElement.DrawFill = true;
                e.RowElement.GradientStyle = GradientStyles.Solid;
                e.RowElement.BackColor = Color.Orange;
            }
            else
            {
                e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
                e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local);
                e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);
            }
        }

        private void CommandBarButton1_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.DefaultExt = "cs";
            sfd.Filter = "C# files (*.cs)|*.cs";

            if (string.IsNullOrEmpty(txtDeviceName.Text))
            {
                shell.MsgBox("Falta rellenar el Device name para generar la libreria", "Atención");
                return;
            }

            if (string.IsNullOrEmpty(cmbDivision.SelectedItem.ToString()))
            {
                shell.MsgBox("Falta seleccionar una division del Device para generar la libreria", "Atención");
                return;
            }

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string code = GenerateDeviceCode(txtDeviceName.Text, cmbDivision.SelectedItem.ToString());
                File.WriteAllText(sfd.FileName, code);
            }
        }

        private void RadButtonRun_Click(object sender, EventArgs e)
        {

        }

        private void CommandBarButtonBuild_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDeviceName.Text))
            {
                shell.MsgBox("Falta rellenar el Device name para generar la libreria", "Atención");
                return;
            }

            if (string.IsNullOrEmpty(cmbDivision.SelectedItem.ToString()))
            {
                shell.MsgBox("Falta seleccionar una division del Device para generar la libreria", "Atención");
                return;
            }

            Build(txtDeviceName.Text, cmbDivision.SelectedItem.ToString());
        }
    }

    public class ModbusRegisterItemVarModel : INotifyPropertyChanged
    {
        // Address,Hex add., Block, Name, N.Reg., Type, Access, Default value, Minimum, Maximum, Units, Scale, Description
        // 36000,8CA0,DI,REC_MODE,1,bool,RD,NA,NA,NA,NA,NA,Input status switch activated REC_MODE
        private string group;
        private string address;
        private string function;
        private string name;
        private int numRegisters;
        private string type;
        private string access;
        private string defaultValue;
        private string minimum;
        private string maximum;
        private string unit;
        private string scale;
        private string description;
        private string parameter;
        private string valor;
        private object data;

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"{Convert.ToInt32(Address, 16)};{Address};{Function};{Name};{NumRegisters};{Type};{Access};{DefaultValue};{Minimum};{Maximum};{Unit};{Scale};{Description};{Group}";
        }

        public string Group
        {
            get { return group; }
            set
            {
                if (value == group)
                    return;

                group = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                if (value != address)
                {
                    address = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Function
        {
            get { return function; }
            set
            {
                if (value != function)
                {
                    function = value;
                    OnPropertyChanged();

                    if (function == "DI")
                    {
                        Type = "bool";
                        NumRegisters = 1;
                    }
                }
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }

        public int NumRegisters
        {
            get { return numRegisters; }
            set
            {
                if (value != numRegisters)
                {
                    numRegisters = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Type
        {
            get { return type; }
            set
            {
                if (value != type)
                {
                    type = value;
                    OnPropertyChanged();

                    if (type == "bool" || type == "uint16" || type == "int16")
                        NumRegisters = 1;
                    else if (type == "uint32" || type == "int32")
                        NumRegisters = 2;
                }
            }
        }

        public string NetType
        {
            get
            {
                if (type.Contains("float"))
                    return "float";

                return type.Replace("ui", "UI").Replace("int", "Int");
            }
        }

        public string Access
        {
            get { return access; }
            set
            {
                if (value != access)
                {
                    access = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DefaultValue
        {
            get { return defaultValue; }
            set
            {
                if (value != defaultValue)
                {
                    defaultValue = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Minimum
        {
            get { return minimum; }
            set
            {
                if (value != minimum)
                {
                    minimum = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Maximum
        {
            get { return maximum; }
            set
            {
                if (value != maximum)
                {
                    maximum = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Unit
        {
            get { return unit; }
            set
            {
                if (value != unit)
                {
                    unit = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Scale
        {
            get { return scale; }
            set
            {
                if (value != scale)
                {
                    scale = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Description
        {
            get { return description; }
            set
            {
                if (value != description)
                {
                    description = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public string Valor
        {
            get { return valor; }
            set
            {
                if (value != valor)
                {
                    valor = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public object Data
        {
            get
            {
                return data;
            }
            set
            {
                if (data != value)
                {
                    data = value;
                    OnPropertyChanged("Data");

                    if (data != null)
                    {
                        if (data is byte[])
                            Valor = ASCIIEncoding.ASCII.GetString((byte[])data);
                        else
                            Valor = data.ToString();
                    }
                    else
                        Valor = null;
                }
            }
        }

        [JsonIgnore]
        public string Parameter
        {
            get { return parameter; }
            set
            {
                if (value != parameter)
                {
                    parameter = value;
                    OnPropertyChanged();
                }
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool CanRead
        {
            get
            {
                return access != null && access.IndexOf("RD") >= 0;
            }
        }

        public bool CanWrite
        {
            get
            {
                return access != null && access.IndexOf("WR") >= 0;
            }
        }
    }

    public class GenerateDeviceCodeModel
    {
        public string Name { get; set; }
        public string Division { get; set; }
        public List<ModbusRegisterItemVarModel> Variables { get; set; }
    }
}
