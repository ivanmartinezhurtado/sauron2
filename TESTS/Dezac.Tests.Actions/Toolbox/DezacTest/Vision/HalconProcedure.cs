﻿using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(HalconProcedureDescription))]
    [Obsolete("Usar la action de HalconProcedureAsync")]
    public class HalconProcedure : ActionBase
    {
        [Category("1.- Test Configuration")]
        [Description("Halcon test Name")]
        [DefaultValue("RGU10_LED_VERDE")]
        public string testName { get; set; }

        [Category("1.- Test Configuration")]
        [Description("Image save name")]
        [DefaultValue("LED_VERDE")]
        public string imageSaveName { get; set; }

        [Category("1.- Test Configuration")]
        [Description("Test device Name")]
        [DefaultValue("RGU10")]
        public string deviceName { get; set; }

        [Category("1.- Test Configuration")]
        [Description("Number of captures (retries)")]
        [DefaultValue((byte)1)]
        public byte retry { get; set; }

        [Category("1.- Test Configuration")]
        [Description("Type of procedure the action must perform")]
        [DefaultValue(TestHalconExtension.TypeProcedure.ReadBarCode)]
        public TestHalconExtension.TypeProcedure Type { get; set; }

        [Category("2.- Camera Selection")]
        [Description("Camera Name if has been defined as variable with AddVariable Action")]
        [DefaultValue("")]
        public string VariableName { get; set; }

        [Category("2.- Camera Selection")]
        [Description("Camera Serial Number (not required if has Variable Name)")]
        [DefaultValue("")]
        public string serialNumber { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Row1 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Row1 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Column1 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Column1 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Row2 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Row2 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Column2 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Column2 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Resize factor for lower image resolution (1 means no resolution reduction)")]
        [DefaultValue(0)]
        public double ResizeFactor { get; set; }

        [Category("4.- Output variables")]
        [Description("Output variable with the read barcode")]
        [DefaultValue("")]
        public string BarcodeRead { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del TestBase");

            if (!string.IsNullOrEmpty(VariableName))
                serialNumber = GetVariable<string>(VariableName);

            switch (Type)
            {
                case TestHalconExtension.TypeProcedure.PaternMatching:
                    testBase.TestHalconMatchingProcedure(serialNumber, testName, imageSaveName, deviceName, retry, null);
                    break;
                case TestHalconExtension.TypeProcedure.FindLeds:
                    testBase.TestHalconFindLedsProcedure(serialNumber, testName, imageSaveName, deviceName, retry, null);
                    break;
                case TestHalconExtension.TypeProcedure.ReadBarCode:
                    TestHalconExtension.TypeBarcode type = TestHalconExtension.TypeBarcode.None;
                    Enum.TryParse(testName, out type);
                    var readCodeBar = testBase.TestHalconReadBarCodeProcedure(serialNumber, type, imageSaveName, deviceName, retry, null, Row1, Column1, Row2, Column2,0);
                    Context.Variables.AddOrUpdate(BarcodeRead, readCodeBar);
                    break;
                case TestHalconExtension.TypeProcedure.OCR:
                    throw new Exception("It is not posible to make an OCR with this action, please use HalconOCR action");
            }
        }
    }

    public class HalconProcedureDescription : ActionDescription
    {
        public override string Name { get { return "HalconProcedure"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
