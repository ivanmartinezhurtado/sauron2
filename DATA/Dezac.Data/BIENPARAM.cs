namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.BIENPARAM")]
    public partial class BIENPARAM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARAM { get; set; }

        public int NUMBIEN { get; set; }

        [Required]
        [StringLength(10)]
        public string IDPARAM { get; set; }

        [StringLength(20)]
        public string PARAM { get; set; }

        public virtual BIEN BIEN { get; set; }
    }
}
