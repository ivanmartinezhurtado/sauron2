﻿
using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.04)]
    public class CIRCAMPRepeaterTest : TestBase
    {
        #region Definition Inputs and Outputs

        private const byte IN_DEVICE_PRESENCE = 9;
        private const byte IN_BLOQUE_SEGURIDAD = 23;
     
        private int TIME_WAIT_IN_DETECTION = 2500;

        #endregion

        private TowerBoard tower;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.IO.DO.OnWait(500, 14);
            tower.IO.DO.Off(43);

            if (!tower.IO.DI.WaitOn(IN_BLOQUE_SEGURIDAD, TIME_WAIT_IN_DETECTION))
                Shell.MsgBox("NO SE HA DETECTADO BLOQUE SEGURIDAD", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, TIME_WAIT_IN_DETECTION))
                Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tower.ActiveVoltageCircuit(false, false, true, true);

            var tension = TriLineValue.Create(0, 0, 230);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(tension, 0, 50, 0, 0);

            if (Identificacion.MODELO.Trim().ToUpper() == "CIRWATT")
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(24, 0.2);
            else
                tower.LAMBDA.ApplyOff();

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);
        }

        public void TestPLC()
        {
            var compacdc = AddInstanceVar(new COMPACT_DC("3.1.c"), "UUT");

            var hasKeyFile = Configuracion.GetString("HAS_KEY_FILE_SSH", "NO", ParamUnidad.SinUnidad) == "SI" ? true : false;
            if (hasKeyFile)
            {
                var dirPath = ConfigurationManager.AppSettings["PathCustomizationFile"];
                var keyFile = Path.Combine(dirPath, "CDC", "id_rsa");
                compacdc.PrivateKeyFile = keyFile;
            }

            var timeCurrentStart = DateTime.Now;
            byte retries = 20;

            SamplerWithCancel((p) =>
            {
                var diffCurrentTime = DateTime.Now.Subtract(timeCurrentStart).TotalSeconds;
                logger.InfoFormat("Tiempo en (s) transcurrido: {0}", diffCurrentTime);

                TestInfo.NumMAC_PLC = compacdc.ReadMac();
                Resultado.Set("MAC_PLC", TestInfo.NumMAC_PLC, ParamUnidad.SinUnidad);

                logger.InfoFormat("Reading PLC Communication Quality -- Retry {0} to {1}",  p, retries);
                var listSniffer = compacdc.ReadPLCCommunicationsQuality();
               
                if (listSniffer != null && listSniffer.Count != 0)
                {
                    logger.InfoFormat("Respuesta PLC count: {0}", listSniffer.LastOrDefault());

                    var readingPLCQuality = Convert.ToInt32(listSniffer.LastOrDefault().PHYLevel);
                    var readingPLCNoise = Convert.ToInt32(listSniffer.LastOrDefault().PHYSNR);

                    var qualityGood = Assert.GetIsValidMaxMin(Params.PARAMETER("QUALITY_LEVEL_PLC", ParamUnidad.SinUnidad).Null.Name, readingPLCQuality, ParamUnidad.SinUnidad,
                     Params.PARAMETER("QUALITY_LEVEL_PLC", ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                     Params.PARAMETER("QUALITY_LEVEL_PLC", ParamUnidad.SinUnidad).Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES("Nivel calidad del PLC"));

                    var snrGood = Assert.GetIsValidMaxMin(Params.PARAMETER("SNR_LEVEL_PLC", ParamUnidad.SinUnidad).Null.Name, readingPLCNoise, ParamUnidad.SinUnidad,
                     Params.PARAMETER("SNR_LEVEL_PLC", ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                     Params.PARAMETER("SNR_LEVEL_PLC", ParamUnidad.SinUnidad).Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES("Nivel SNR del PLC"));

                    logger.InfoFormat("Tiempo en (s) PLC Meter OK: {0}", diffCurrentTime);
                    return true;
                }

                return false;
            }, "No se ha leido la trama del sniffer despues del tiempo indicado", retries, 5000, 15000, false, false);                        
        }

        public void TestFinish()
        {
            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                    tower.IO.DO.PulseOn(5000, 15);
            }

            tower.Dispose();

            if (IsTestError)
                tower.IO.DO.On(14, 43);

        }   
    }
}
