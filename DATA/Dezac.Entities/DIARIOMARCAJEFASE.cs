namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class DIARIOMARCAJEFASE
    {
        public DIARIOMARCAJEFASE()
        {
            this.DIARIOMARCAJEFASE1 = new HashSet<DIARIOMARCAJEFASE>();
            this.T_TESTFASE = new HashSet<T_TESTFASE>();
        }
    
        public int NUMMARCAJE { get; set; }
        public string IDOPERARIO { get; set; }
        public Nullable<int> NUMHOJA { get; set; }
        public System.DateTime FECHAREGISTRO { get; set; }
        public System.DateTime FECHA { get; set; }
        public string TIPO { get; set; }
        public string IDFASE { get; set; }
        public string INICIOFIN { get; set; }
        public Nullable<int> NUMMARCAJEINICIO { get; set; }
        public Nullable<int> CANTIDAD { get; set; }
        public Nullable<int> NUMMARCAJEACTIVIDAD { get; set; }
        public string HORAINCORRECTA { get; set; }
        public string NOTASHORAINCORRECTA { get; set; }
        public string INSERCIONPAREJA { get; set; }
        public Nullable<System.DateTime> TIME_ID { get; set; }
        public string NOTAS { get; set; }
        public string INICIOSESION { get; set; }
        public string HOST { get; set; }
    
        public virtual DIARIOMARCAJE DIARIOMARCAJE { get; set; }
        public virtual HOJAMARCAJEFASE HOJAMARCAJEFASE { get; set; }
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE1 { get; set; }
        public virtual DIARIOMARCAJEFASE DIARIOMARCAJEFASE2 { get; set; }
        public virtual FASE FASE { get; set; }
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }
    }
}
