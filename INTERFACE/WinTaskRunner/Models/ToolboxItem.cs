﻿using System;
using System.Drawing;

namespace WinTaskRunner.Models
{
    public class ToolboxItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public Image IconAction { get; set; }
        public Image IconCategory { get; set; }
        public Type Type { get; set; }
        public bool Obsolete { get; set; }
        public bool NoVisible { get; set; }
    }
}
