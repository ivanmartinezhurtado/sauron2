﻿using Comunications.Ethernet;
using Dezac.Tests.AOI.Models;
using HalconDotNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.AOI
{
    public partial class AOIStatisticsForm : Form
    {
        List<AOIStatistics> ListOfStatistics;
        AOIStatistics statistics;
        private List<Point> weldingPositions;
        private int? weldingSelected = null;
        private List<HObject> boardImage;
        private List<HObject> weldingsROI;
        private HObject RGBImage;
        private Dictionary<int, int> weldingPareto = new Dictionary<int, int>();
        
        public AOIStatisticsForm()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;

            hWindow1.Enabled = false;
            chart1.Enabled = false;
            comboBoxTipoFallo.DataSource = Enum
                    .GetValues(typeof(RepairEnum))
                    .Cast<RepairEnum>()
                    .ToList();

            using (FTPClient ftp = new FTPClient())
            {
                var files = ftp.ListFilesDirectory(ConfigurationManager.AppSettings["PathAOIStatistics"]);
                ListOfStatistics = new List<AOIStatistics>();

                foreach (var file in files)
                {
                    if (Path.GetExtension(file.FullName) == ".stats")
                        continue;
                    using (Stream stream = new MemoryStream())
                    {
                        ftp.Download(stream, file.FullName);
                        using (TextReader tr = new StreamReader(stream))
                        {
                            string json = tr.ReadToEnd();
                            AOIStatistics statistics = JsonConvert.DeserializeObject<AOIStatistics>(json);
                            if (statistics.Total != 0)
                                ListOfStatistics.Add(statistics);
                        }
                    }
                }
            }

            statistics = new AOIStatistics();
        }

        private void InitializeImages()
        {
            if (statistics.NumProductList.Count != 1 || statistics.IdFaseList.Count != 1) //Imagen solo cuando hay un unico producto y fase
                return;

            //Extract image transformed
            int i = 0;
            HObject imageRotate, imageMirror = new HObject();
            List<HTuple> imageWidth = new List<HTuple>();
            List<HTuple> imageHeight = new List<HTuple>();

            boardImage = new List<HObject>();
            weldingsROI = new List<HObject>();
            weldingPositions = new List<Point>();

            while (File.Exists($"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_{i}.hobj") || File.Exists($"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_0{i}.hobj"))
            {
                HObject boardImageAux;
                if (File.Exists($"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_{i}.hobj"))
                    HOperatorSet.ReadImage(out boardImageAux, $"W:\\_AOI\\ROI\\IMAGE_MODEL_RECTIFIED_{statistics.NumProductList[0]}_{i}.png");
                else
                    HOperatorSet.ReadImage(out boardImageAux, $"W:\\_AOI\\ROI\\IMAGE_MODEL_RECTIFIED_{statistics.NumProductList[0]}_0{i}.png");
                HOperatorSet.RotateImage(boardImageAux, out imageRotate, 90, "constant");
                HOperatorSet.MirrorImage(imageRotate, out imageMirror, "column");
                HOperatorSet.GetImageSize(imageMirror, out HTuple imageWidthAux, out HTuple imageHeightAux);
                imageWidth.Add(imageWidthAux);
                imageHeight.Add(imageHeightAux);
                boardImage.Add(imageMirror);
                i++;
            }

            //Extract regions transformed
            HOperatorSet.SetSystem("clip_region", "false");
            HOperatorSet.HomMat2dIdentity(out HTuple homMat2DIdentity);
            HOperatorSet.HomMat2dRotate(homMat2DIdentity, 3.1416 / 2, imageWidth[0] / 2, imageHeight[0] / 2, out HTuple homMat2DRotate);
            HOperatorSet.HomMat2dTranslate(homMat2DRotate, (imageHeight[0] / 2) - (imageWidth[0] / 2), (imageWidth[0] / 2) - (imageHeight[0] / 2), out HTuple homMat2DTranslate);
            HOperatorSet.HomMat2dReflect(homMat2DTranslate, 0, imageWidth[0] / 2, imageHeight[0], imageWidth[0] / 2, out HTuple homMat2DReflect);
            i = 0;
            while (File.Exists($"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_{i}.hobj") || File.Exists($"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_0{i}.hobj"))
            {
                HObject weldingsROIAux;
                if (File.Exists($"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_{i}.hobj"))
                    HOperatorSet.ReadRegion(out weldingsROIAux, $"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_{i}.hobj");
                else
                    HOperatorSet.ReadRegion(out weldingsROIAux, $"W:\\_AOI\\ROI\\ROI_MODEL_{statistics.NumProductList[0]}_0{i}.hobj");

                HOperatorSet.AffineTransRegion(weldingsROIAux, out HObject weldingsTrans, homMat2DReflect, "nearest_neighbor");
                weldingsROI.Add(weldingsTrans);
                i++;
            }
            HOperatorSet.SetSystem("clip_region", "true");

            //Extract Position
            for (i = 0; i < statistics.WeldingFailureCount.Count; i++)
            {
                HOperatorSet.SelectObj(weldingsROI[0], out HObject roiWelding, i + 1);
                HOperatorSet.SmallestRectangle1(roiWelding, out HTuple row1, out HTuple column1, out HTuple row2, out HTuple column2);
                weldingPositions.Add(new Point((int)(column1 + (column2 - column1) / 2), (int)(row1 + (row2 - row1) / 2)));
            }
            
        }

        private void InitializeChart()
        {
            if (statistics == null || statistics.NumProductList.Count != 1 || statistics.IdFaseList.Count != 1)
            {
                chart1.Series["Weldings"].Points.Clear();
                chart1.Series["Percentage"].Points.Clear();
                return;
            }

            chart1.ChartAreas["ChartArea1"].AxisY2.Maximum = 100;
            chart1.ChartAreas["ChartArea1"].AxisY2.Minimum = 0;

            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;

            chart1.ChartAreas["ChartArea1"].AxisY.Minimum = 0;
            chart1.ChartAreas["ChartArea1"].AxisY.Maximum = statistics.WeldingFailureCount.Max();

            if ((RepairEnum)comboBoxTipoFallo.SelectedItem == RepairEnum.SIN_REPARACION)
            {
                weldingPareto = statistics.WeldingFailureCount
                    .Select((s, i) => new { s, i })
                    .ToDictionary(x => x.i, x => x.s)
                    .OrderByDescending(x => x.Value)
                    .ToDictionary(x => x.Key, x => x.Value);

                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = statistics.WeldingFailureCount.Max();
            }
            else
            {
                weldingPareto = statistics.WeldingFailureCountByType
                    .Select((dict) => dict[(RepairEnum)comboBoxTipoFallo.SelectedItem])
                    .Select((s, i) => new { s, i })
                    .ToDictionary(x => x.i, x => x.s)
                    .OrderByDescending(x => x.Value)
                    .ToDictionary(x => x.Key, x => x.Value);

                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = statistics.WeldingFailureCountByType
                    .Select((dict) => dict[(RepairEnum)comboBoxTipoFallo.SelectedItem]).Max();
            }

            chart1.Series["Weldings"].Points.Clear();
            chart1.Series["Percentage"].Points.Clear();

            int sum = weldingPareto.Values.Sum();
            double percentge = 0;
            for (int i = 0; i < weldingPareto.Values.Count; i++)
            {
                chart1.Series["Weldings"].Points.AddXY(i + 1, weldingPareto.Values.ToArray()[i]);
                percentge += (double)weldingPareto.Values.ToArray()[i] / sum;
                chart1.Series["Percentage"].Points.AddXY(i + 1, percentge * 100);
                if (percentge > 0.8)
                    break;
            }
            chart1.Series["Weldings"].Color = Color.DeepSkyBlue;
            chart1.Series["Percentage"].Color = Color.Gold;

        }

        private void UpdateStatistics()
        {
            if (weldingSelected == null)
                return;
            labelCruces.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.CRUCE].ToString();
            labelFalsosFallos.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.FALSO_FALLO].ToString();
            labelFaltasComponentes.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.FALTA_COMPONENTE].ToString();
            labelFaltasEstaño.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.FALTA_ESTAÑO].ToString();
            labelLevantado.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.COMPONENTE_LEVANTADO].ToString();
            labelOtrosFallos.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.OTRO_FALLO].ToString();
            labelPatasLargas.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.PATA_LARGA].ToString();
            labelFormaDefectuosa.Text = statistics.WeldingFailureCountByType[weldingSelected.Value][Models.RepairEnum.FORMA_DEFECTUOSA].ToString();
        }

        private void PaintWeldings()
        {
            if (boardImage == null || statistics.NumProductList.Count > 1)
            {
                hWindow1.HalconWindow.ClearWindow();
                return;
            }
            // Paints Weldings
            HObject imageChannelG = boardImage[0].Clone();
            HObject imageChannelR = boardImage[0].Clone();
            HObject imageChannelB = boardImage[0].Clone();

            int max = ((RepairEnum)comboBoxTipoFallo.SelectedItem == RepairEnum.SIN_REPARACION) ?
                statistics.WeldingFailureCount.Max() :
                statistics.WeldingFailureCountByType.Select((dict) => dict[(RepairEnum)comboBoxTipoFallo.SelectedItem]).Max();

            for (int i = 0; i < statistics.WeldingFailureCountByType.Count; i++)
            {
                HOperatorSet.SelectObj(weldingsROI[0], out HObject roiWelding, i + 1);

                HOperatorSet.Boundary(roiWelding, out HObject border, "inner");
                HOperatorSet.DilationCircle(border, out HObject regionDilation, 5);
                HOperatorSet.ErosionCircle(roiWelding, out HObject regionErosion, 5);
                HOperatorSet.Difference(regionDilation, regionErosion, out HObject regionDifference);

                int weldingErrors = ((RepairEnum)comboBoxTipoFallo.SelectedItem == RepairEnum.SIN_REPARACION) ?
                    statistics.WeldingFailureCount[i]:
                    statistics.WeldingFailureCountByType[i][(RepairEnum)comboBoxTipoFallo.SelectedItem];

                int relative = (int)((double)weldingErrors / ((double)max) * 255);
                if (relative == 0 || max == 0)
                {
                    HOperatorSet.OverpaintRegion(imageChannelR, regionDifference, 0, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelG, regionDifference, 130, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelB, regionDifference, 0, "fill");
                }
                else
                {
                    HOperatorSet.OverpaintRegion(imageChannelR, regionDifference, 255, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelG, regionDifference, 255 - relative, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelB, regionDifference, 0, "fill");
                }


            }
            HOperatorSet.Compose3(imageChannelR, imageChannelG, imageChannelB, out RGBImage);

            //Display Image
            hWindow1.HalconWindow.ClearWindow();
            RGBImage.DispObj(hWindow1.HalconWindow);
            hWindow1.HalconWindow.SetPart(0, 0, -2, -2);
        }

        private void PaintSelectedWelding(int? previousSelected)
        {
            // Paints Weldings

            HOperatorSet.Decompose3(RGBImage, out HObject imageChannelR, out HObject imageChannelG, out HObject imageChannelB);

            //Paint Selected in White
            HOperatorSet.SelectObj(weldingsROI[0], out HObject roiWelding, weldingSelected + 1);
            HOperatorSet.Boundary(roiWelding, out HObject border, "inner");
            HOperatorSet.DilationCircle(border, out HObject regionDilation, 5);
            HOperatorSet.ErosionCircle(roiWelding, out HObject regionErosion, 5);
            HOperatorSet.Difference(regionDilation, regionErosion, out HObject regionDifference);

            HOperatorSet.OverpaintRegion(imageChannelR, regionDifference, 255, "fill");
            HOperatorSet.OverpaintRegion(imageChannelG, regionDifference, 255, "fill");
            HOperatorSet.OverpaintRegion(imageChannelB, regionDifference, 255, "fill");

            if (previousSelected != null)
            {
                //Paint With previous color the previous selected welding
                HOperatorSet.SelectObj(weldingsROI[0], out roiWelding, previousSelected + 1);
                HOperatorSet.Boundary(roiWelding, out border, "inner");
                HOperatorSet.DilationCircle(border, out regionDilation, 5);
                HOperatorSet.ErosionCircle(roiWelding, out regionErosion, 5);
                HOperatorSet.Difference(regionDilation, regionErosion, out regionDifference);

                int relative = (int)(((double)statistics.WeldingFailureCount[previousSelected.Value] / (double)statistics.WeldingFailureCount.Max()) * 255);
                if (relative == 0)
                {
                    HOperatorSet.OverpaintRegion(imageChannelR, regionDifference, 0, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelG, regionDifference, 130, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelB, regionDifference, 0, "fill");
                }
                else
                {
                    HOperatorSet.OverpaintRegion(imageChannelR, regionDifference, 255, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelG, regionDifference, 255 - relative, "fill");
                    HOperatorSet.OverpaintRegion(imageChannelB, regionDifference, 0, "fill");
                }
            }
            HOperatorSet.Compose3(imageChannelR, imageChannelG, imageChannelB, out RGBImage);

            //Display Image
            hWindow1.HalconWindow.ClearWindow();
            RGBImage.DispObj(hWindow1.HalconWindow);
            hWindow1.HalconWindow.SetPart(0, 0, -2, -2);
        }

        private void HWindow1_Load(object sender, EventArgs e)
        {
            this.MouseWheel += my_MouseWheel;
        }

        private void my_MouseWheel(object sender, MouseEventArgs e)
        {
            Point pt = hWindow1.Location;
            MouseEventArgs newe = new MouseEventArgs(e.Button, e.Clicks, e.X - pt.X, e.Y - pt.Y, e.Delta);
            hWindow1.HSmartWindowControl_MouseWheel(sender, newe);
        }

        private void HWindow1_HMouseUp(object sender, HMouseEventArgs e)
        {
            int? previous = weldingSelected;

            Point pt = hWindow1.Location;
            var x = e.X - pt.X;
            var y = e.Y - pt.Y;

            hWindow1.HalconWindow.ConvertCoordinatesWindowToImage(y, x, out double rowImage, out double columnImage);

            double bestDistance = 100;

            for (int i = 0; i < weldingPositions.Count; i++)
            {
                var distance = Math.Sqrt(Math.Pow((weldingPositions[i].X - x), 2) + Math.Pow((weldingPositions[i].Y - y), 2));
                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    weldingSelected = i;
                }
            }

            if (weldingSelected == previous)
                return;

            if (weldingSelected != null)
            {
                var pointIndex = weldingPareto.Keys.ToList().FindIndex((s) => s == weldingSelected.Value);

                chart1.Series["Weldings"].Points.ToList().ForEach(point => point.Color = Color.DeepSkyBlue);
                if (pointIndex < chart1.Series["Weldings"].Points.Count)
                    chart1.Series["Weldings"].Points[pointIndex].Color = Color.DarkRed;
            }
            
            UpdateStatistics();
            PaintSelectedWelding(previous);
        }

        private void Chart1_MouseUp(object sender, MouseEventArgs e)
        {
            var b = chart1.HitTest(e.X, e.Y);
            if (b.PointIndex != -1)
            {
                int? previous = weldingSelected;
                weldingSelected = weldingPareto.ElementAt(b.PointIndex).Key;
                UpdateStatistics();

                PaintSelectedWelding(previous);
                chart1.Series["Weldings"].Points.ToList().ForEach(point => point.Color = Color.DeepSkyBlue);
                chart1.Series["Weldings"].Points[b.PointIndex].Color = Color.DarkRed;
            }
        }

        private void ButtonLoad_Click(object sender, EventArgs e)
        {
            AOILoadForm aOILoadForm = new AOILoadForm(ListOfStatistics);

            Cursor.Current = Cursors.WaitCursor;
            this.Enabled = false;
            if (aOILoadForm.ShowDialog() == DialogResult.OK)
            {
                statistics = new AOIStatistics();
                foreach (var stats in aOILoadForm.StatisticsList)
                    statistics.JointStatistics(stats);

                //TODO, falta hay que poner todas las OF
                labelOrdenProducto.Text = $"Nº Orden: {string.Join(",",statistics.NumOrdenList.ConvertAll<string>(s => s.ToString()).ToArray())} - Nº Producto: {string.Join(",", statistics.NumProductList.ConvertAll<string>(s => s.ToString()).ToArray())}";

                InitializeImages();
                PaintWeldings();
                InitializeChart();
                hWindow1.Enabled = true;
                chart1.Enabled = true;

                labelTotalCircuitos.Text = statistics.NumCircuitos.ToString();
                labelTotalCruces.Text = statistics.Cruce.ToString();
                labelTotalFallos.Text = statistics.OK.ToString();
                labelTotalFalsosFallos.Text = statistics.FalsoFallo.ToString();
                labelTotalFaltaComponente.Text = statistics.FaltaComponente.ToString();
                labelTotalFaltaEstano.Text = statistics.FaltaEstaño.ToString();
                labelTotalFormaDefectuosa.Text = statistics.FormaDefectuosa.ToString();
                labelTotalLevantado.Text = statistics.ComponenteLevantado.ToString();
                labelTotalOtros.Text = statistics.OtroFallo.ToString();
                labelTotalPatasLargas.Text = statistics.PataLarga.ToString();
                labelTotalSoldaduras.Text = statistics.Total.ToString();
                labelDPMO.Text = statistics.DPMO.ToString("#.00");

                weldingSelected = null;

                var csv = new StringBuilder();
                string newline = "Date;OK;KO;Levantado;Cruce;FalsoFallo;FaltaComponente;FaltaEstaño;OtroFallo;PataLarga";
                csv.AppendLine(newline);
                for (int i = 0; i < statistics.KOTimeSerie.Count; i++)
                {
                    newline = $"{statistics.KOTimeSerie [i].Key};";
                    newline += $"{statistics.OKTimeSerie[i].Value};";
                    newline += $"{statistics.KOTimeSerie[i].Value};";
                    newline += $"{statistics.ComponenteLevantadoTimeSerie[i].Value};";
                    newline += $"{statistics.CruceTimeSerie[i].Value};";
                    newline += $"{statistics.FalsoFalloTimeSerie[i].Value};";
                    newline += $"{statistics.FaltaComponenteTimeSerie[i].Value};";
                    newline += $"{statistics.FaltaEstañoTimeSerie[i].Value};";
                    newline += $"{statistics.OtroFalloTimeSerie[i].Value};";
                    newline += $"{statistics.PataLargaTimeSerie[i].Value};";
                    csv.AppendLine(newline);
                }
                File.WriteAllText(@"C:\Users\admva\Documents\GitLab\SAURON\TOOLS\Dezac.Tests.AOI\TimeSerie.csv", csv.ToString());
            }

            Cursor.Current = Cursors.Default;
            this.Enabled = true;

            return;
        }

        private void ComboBoxTipoFallo_SelectedValueChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            PaintWeldings();
            InitializeChart();
            Cursor.Current = Cursors.Default;
        }
    }
}
