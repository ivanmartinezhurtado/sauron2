﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Dezac.Tests.Actions.Views
{
    public partial class MethodSelectorView : Form
    {
        public RunMethodAction Value { get; set; }

        public MethodSelectorView()
        {
            InitializeComponent();
            list.SelectedNodeChanged += OnSelectedNodeChanged;

            this.Icon = Application.OpenForms[0].Icon;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            list.LoadDevices();
            list.LoadInstruments(false);

            if (Value != null)
            {
                props.SelectedObject = Value;

                var node = list.FindNodeRecursive(p => p.Tag is MethodInfo && 
                    string.Format("({0}.{1})", ((MethodInfo)p.Tag).ReflectedType.FullName, ((MethodInfo)p.Tag).Name) == Value.ToString());
                if (node != null)
                {
                    node.Tag = Value;
                    node.Selected = true;
                    node.EnsureVisible();
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SelectNodo(list.SelectedNode);
        }

        private void SelectNodo(RadTreeNode node)
        {
            if (node == null || (!(node.Tag is MethodInfo) && !(node.Tag is RunMethodAction)))
            {
                MessageBox.Show("Debe seleccionar un nodo de un método!");
                return;
            }

            Value = props.SelectedObject as RunMethodAction;
        }

        private RunMethodAction CreateAction(MethodInfo method)
        {
            if (method == null)
                return null;

            var action = new RunMethodAction();
            action.AssemblyFile = method.ReflectedType.Assembly.Location;
            action.TypeName = method.ReflectedType.FullName;
            action.Method = method.Name;

            method.GetParameters()
                .ToList()
                .ForEach(p =>
                {
                    var value = new RunMethodAction.Argument { Name = p.Name, Type = p.ParameterType.FullName };

                    var description = p.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (description != null)
                        value.Description = description.Description;

                    if (p.DefaultValue != null && p.HasDefaultValue)
                        value.Value = p.ParameterType.IsEnum ? ((int)p.DefaultValue).ToString() : p.DefaultValue.ToString();

                    action.Args.Add(value);
                });

            return action;
        }

        private void OnSelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            if (e.Node.Tag is RunMethodAction)
                props.SelectedObject = e.Node.Tag;
            else
                props.SelectedObject = CreateAction(e.Node.Tag as MethodInfo);
        }

        private void rdButtonRemove_Click(object sender, EventArgs e)
        {
            Value = null;
        }
    }
}
