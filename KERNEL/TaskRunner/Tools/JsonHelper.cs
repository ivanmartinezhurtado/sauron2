﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace TaskRunner.Tools
{
    public static class JsonHelper
    {
        public static List<NameValuePair<object>> DeserializeObjectAndFlattenList(object value)
        {
            string json = JsonConvert.SerializeObject(value);
            List<NameValuePair<object>> props = JsonHelper.DeserializeAndFlattenList(json);

            return props;
        }

        public static List<NameValuePair<object>> DeserializeAndFlattenList(string json)
        {
            List<NameValuePair<object>> result = new List<NameValuePair<object>>();
            Dictionary<string, object> dict = DeserializeAndFlatten(json);

            foreach (KeyValuePair<string, object> pair in dict)
                result.Add(new NameValuePair<object> { Name = pair.Key, Value = pair.Value });

            return result;
        }

        public static Dictionary<string, object> DeserializeObjectAndFlatten(object value)
        {
            string json = JsonConvert.SerializeObject(value);
            Dictionary<string, object> props = JsonHelper.DeserializeAndFlatten(json);
            return props;
        }

        public static Dictionary<string, object> DeserializeAndFlatten(string json)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();

            if (string.IsNullOrEmpty(json))
                return dict;

            JToken token = JToken.Parse(json);
            FillDictionaryFromJToken(dict, token, string.Empty);

            return dict;
        }

        private static void FillDictionaryFromJToken(Dictionary<string, object> dict, JToken token, string prefix)
        {
            switch (token.Type)
            {
                case JTokenType.Object:
                    foreach (JProperty prop in token.Children<JProperty>())
                        FillDictionaryFromJToken(dict, prop.Value, string.IsNullOrEmpty(prefix) ? $"{prop.Name}" : $"{prefix}.{prop.Name}");

                    break;
                case JTokenType.Array:
                    int index = 0;
                    foreach (JToken value in token.Children())
                    {
                        FillDictionaryFromJToken(dict, value, string.IsNullOrEmpty(prefix) ? $"[{index}]" : $"{prefix}[{index}]");
                        index++;
                    }
                    break;
                default:
                    dict.Add(prefix, ((JValue)token).Value);
                    break;
            }
        }
    }
}
