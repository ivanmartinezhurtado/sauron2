﻿namespace Dezac.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DezacContext : DbContext
    {
        public DezacContext()
            : base("name=DezacContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<ACTUACIONREPFAB> ACTUACIONREPFAB { get; set; }
        public virtual DbSet<ANOMALIAREPFAB> ANOMALIAREPFAB { get; set; }
        public virtual DbSet<ARTICULO> ARTICULO { get; set; }
        public virtual DbSet<ARTICULOBLOQUEO> ARTICULOBLOQUEO { get; set; }
        public virtual DbSet<ARTICULOCLIENTE> ARTICULOCLIENTE { get; set; }
        public virtual DbSet<ARTICULOCLIENTEDESCRIPCION> ARTICULOCLIENTEDESCRIPCION { get; set; }
        public virtual DbSet<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }
        public virtual DbSet<BIEN> BIEN { get; set; }
        public virtual DbSet<BIENASISTENCIA> BIENASISTENCIA { get; set; }
        public virtual DbSet<BIENPROGRAMACIONASISTENCIA> BIENPROGRAMACIONASISTENCIA { get; set; }
        public virtual DbSet<BIENPARAM> BIENPARAM { get; set; }
        public virtual DbSet<CAJAEQUIPO> CAJAEQUIPO { get; set; } 
        public virtual DbSet<DIARIOMARCAJE> DIARIOMARCAJE { get; set; }
        public virtual DbSet<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }
        public virtual DbSet<ESTRUCTURAMATRICULA> ESTRUCTURAMATRICULA { get; set; }
        public virtual DbSet<FASE> FASE { get; set; }
        public virtual DbSet<HOJAMARCAJE> HOJAMARCAJE { get; set; }
        public virtual DbSet<HOJAMARCAJEFASE> HOJAMARCAJEFASE { get; set; }
        public virtual DbSet<INSTRUCCIONTECNICA> INSTRUCCIONTECNICA { get; set; }
        public virtual DbSet<MATRICULA> MATRICULA { get; set; }
        public virtual DbSet<OPE_ERROR> OPE_ERROR { get; set; }
        public virtual DbSet<OPERARIO> OPERARIO { get; set; }
        public virtual DbSet<ORDEN> ORDEN { get; set; }
        public virtual DbSet<ORDENCAJA> ORDENCAJA { get; set; }
        public virtual DbSet<ORDENFASETEST> ORDENFASETEST { get; set; }
        public virtual DbSet<ORDENPROCESOFASE> ORDENPROCESOFASE { get; set; }
        public virtual DbSet<ORDENPROCESOFASEFILE> ORDENPROCESOFASEFILE { get; set; }
        public virtual DbSet<ORDENPRODUCTO> ORDENPRODUCTO { get; set; }
        public virtual DbSet<ORDENPRODUCTOERROR> ORDENPRODUCTOERROR { get; set; }
        public virtual DbSet<PARTEORDENEQUIPO> PARTEORDENEQUIPO { get; set; }
        public virtual DbSet<PRODUCTO> PRODUCTO { get; set; }
        public virtual DbSet<PRODUCTOBINARIO> PRODUCTOBINARIO { get; set; }
        public virtual DbSet<PRODUCTOCOMPONENTE> PRODUCTOCOMPONENTE { get; set; }
        public virtual DbSet<PRODUCTOFASE> PRODUCTOFASE { get; set; }
        public virtual DbSet<PROYECTO> PROYECTO { get; set; }
        public virtual DbSet<PROYECTOTAREA> PROYECTOTAREA { get; set; }
        public virtual DbSet<REPROCESOFAB> REPROCESOFAB { get; set; }
        public virtual DbSet<RF_ACTUACION> RF_ACTUACION { get; set; }
        public virtual DbSet<RF_ANOMALIA> RF_ANOMALIA { get; set; }
        public virtual DbSet<T_FAMILIA> T_FAMILIA { get; set; }
        public virtual DbSet<T_FAMILIAERROR> T_FAMILIAERROR { get; set; }
        public virtual DbSet<T_FAMILIAPARAM> T_FAMILIAPARAM { get; set; }
        public virtual DbSet<T_GRUPOPARAMETRIZACION> T_GRUPOPARAMETRIZACION { get; set; }
        public virtual DbSet<T_IRISERROR> T_IRISERROR { get; set; }
        public virtual DbSet<T_IRISERRORFAMILIAFASE> T_IRISERRORFAMILIAFASE { get; set; }
        public virtual DbSet<T_IRISERRORPRODUCTOFASE> T_IRISERRORPRODUCTOFASE { get; set; }
        public virtual DbSet<T_LOG> T_LOG { get; set; }
        public virtual DbSet<T_PARAMETRO> T_PARAMETRO { get; set; }
        public virtual DbSet<T_TEST> T_TEST { get; set; }
        public virtual DbSet<T_TESTERROR> T_TESTERROR { get; set; }
        public virtual DbSet<T_TESTFASE> T_TESTFASE { get; set; }
        public virtual DbSet<T_TESTFASEFILE> T_TESTFASEFILE { get; set; }
        public virtual DbSet<T_TESTFASES> T_TESTFASES { get; set; }
        public virtual DbSet<T_TESTREPORT> T_TESTREPORT { get; set; }
        public virtual DbSet<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }
        public virtual DbSet<T_TESTSEQUENCEFILE> T_TESTSEQUENCEFILE { get; set; }
        public virtual DbSet<T_TESTSEQUENCEFILEPRODUCTO> T_TESTSEQUENCEFILEPRODUCTO { get; set; }
        public virtual DbSet<T_TESTSEQUENCEFILEVERSION> T_TESTSEQUENCEFILEVERSION { get; set; }
        public virtual DbSet<T_TESTCONFIGFILE> T_TESTCONFIGFILE { get; set; }
        public virtual DbSet<T_TESTCONFIGFILEPRODUCTO> T_TESTCONFIGFILEPRODUCTO { get; set; }
        public virtual DbSet<T_TESTCONFIGFILEVERSION> T_TESTCONFIGFILEVERSION { get; set; }
        public virtual DbSet<T_TESTSTEP> T_TESTSTEP { get; set; }
        public virtual DbSet<T_TESTVALOR> T_TESTVALOR { get; set; }
        public virtual DbSet<T_TIPOEVENTO> T_TIPOEVENTO { get; set; }
        public virtual DbSet<T_TIPOGRUPOPARAMETRIZACION> T_TIPOGRUPOPARAMETRIZACION { get; set; }
        public virtual DbSet<T_TIPOPARAMETRO> T_TIPOPARAMETRO { get; set; }
        public virtual DbSet<T_TIPOVALORPARAMETRO> T_TIPOVALORPARAMETRO { get; set; }
        public virtual DbSet<T_UNIDAD> T_UNIDAD { get; set; }
        public virtual DbSet<T_VALORPARAMETRO> T_VALORPARAMETRO { get; set; }
        public virtual DbSet<T_VALORPARAMETROPRODUCTO> T_VALORPARAMETROPRODUCTO { get; set; }
        //public virtual DbSet<T_VALORPARAMETROTOL> T_VALORPARAMETROTOL { get; set; }
        //public virtual DbSet<T_VALORPARAMETROTOLPROD> T_VALORPARAMETROTOLPROD { get; set; }
        public virtual DbSet<WEBUSERPARAM> WEBUSERPARAM { get; set; }
        public virtual DbSet<T_VALORPARAMETROLOG> T_VALORPARAMETROLOG { get; set; }
        public virtual DbSet<VW_EMPLEADO> VW_EMPLEADO { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACTUACIONREPFAB>()
                .Property(e => e.IDACTUACION)
                .IsUnicode(false);

            modelBuilder.Entity<ACTUACIONREPFAB>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<ACTUACIONREPFAB>()
                .HasMany(e => e.RF_ACTUACION)
                .WithRequired(e => e.ACTUACIONREPFAB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.IDANOMALIA)
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.POSTVENTASINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.FABRICACIONSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.ACTUACIONSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.COMPONENTESINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .Property(e => e.IDACTUACION)
                .IsUnicode(false);

            modelBuilder.Entity<ANOMALIAREPFAB>()
                .HasMany(e => e.RF_ANOMALIA)
                .WithRequired(e => e.ANOMALIAREPFAB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CLACODIGO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CLADESC)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CODIGOBCN)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.DESCBCN)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CODIGOTERRASSA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.DESCTERRASSA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDTIPOARTICULO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDIVA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDPROVEEDOR)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDMARCA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDALMACENCALIDAD)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CUENTACOMPRA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CUENTAVENTA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CUENTAFABRICACION)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.EXTXREF)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.INVENTARIABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.EXISTENCIA)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.ORDENADO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.RESERVADO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.DISPONIBLE)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CONTROLCALIDAD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CARGABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PRECIOSTANDARD)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PRECIOMEDIO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PRECIOULTIMO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PRECIOTEORICO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PRECIOVENTA)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.ACTIVO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.OBSERVING)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.OBSERVCOMPRA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.OBSERVVENTA)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.OBSERVALMACEN)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CODCLAPROV)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.UDSPEDMODREC_SN)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.DOCUMENTABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PREVISIONABLEVTA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.DEPOSITOCLIENTE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.EXISTENCIADEPOSITOCLIENTE)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDFAMILIAPRODUCCION)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.ROHS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.PESO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.CRITICO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.TIPOABC_MA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.FICHEROIMAGEN)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.AVISOPPC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.RETIRASTOCKBULTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.NOTACMC)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.TARICCOD)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.TARICPCT)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULO>()
                .Property(e => e.IDGRUPO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.ARTICULOBLOQUEO)
                .WithRequired(e => e.ARTICULO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.ARTICULOCLIENTE)
                .WithRequired(e => e.ARTICULO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.ASISTENCIATECNICA)
                .WithRequired(e => e.ARTICULO)
                .HasForeignKey(e => e.NUMPRODUCTO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.ORDEN)
                .WithRequired(e => e.ARTICULO)
                .HasForeignKey(e => e.NUMPRODUCTO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.PRODUCTO)
                .WithRequired(e => e.ARTICULO)
                .HasForeignKey(e => e.NUMPRODUCTO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.PRODUCTOCOMPONENTE)
                .WithRequired(e => e.ARTICULO)
                .HasForeignKey(e => e.NUMCOMPONENTE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.PROYECTO1)
                .WithOptional(e => e.ARTICULO1)
                .HasForeignKey(e => e.NUMARTICULO);

            modelBuilder.Entity<ARTICULO>()
                .HasMany(e => e.T_IRISERRORPRODUCTOFASE)
                .WithRequired(e => e.ARTICULO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ARTICULOBLOQUEO>()
                .Property(e => e.PROCESO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOBLOQUEO>()
                .Property(e => e.USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOBLOQUEO>()
                .Property(e => e.MOTIVO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOBLOQUEO>()
                .Property(e => e.IDMOTIVO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.IDCLIENTE)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.REFCLIENTE)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.REFCLIENTE2)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.PRECIO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.ATTRIB1)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.ATTRIB2)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.ATTRIB3)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.ATTRIB4)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.ATTRIB5)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.INACTIVO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.PRECIOBORRADOR)
                .HasPrecision(20, 5);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.OBSOLETO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.NOTASOBSOLETO)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.OBSERVACIONESBORRADOR)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.EAN13)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.IDCLIENTEFINAL)
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.VALIDARVERSION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ARTICULOCLIENTE>()
                .Property(e => e.EAN13MARCA)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.NUMEROSERIE)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.ABIERTA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.GARANTIA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.CARGO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.CAUSA_1)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.CAUSA_2)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.TRABAJO_1)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.TRABAJO_2)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.HORAS)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.PRECIO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.AVERIACOMUNICADA)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.OFERTA_SINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.ESTADOEQUIPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.DEFECTUOSOEN)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.IDDEFECTO)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.NROREPARACION)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.UBICACION)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.ESTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.NUEVONROSERIE)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.NOTASLOG)
                .IsUnicode(false);

            modelBuilder.Entity<ASISTENCIATECNICA>()
                .Property(e => e.RESPONSABLE)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.CODIGO)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.NROSERIE)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDPROVEEDOR)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.VALORADQUISICION)
                .HasPrecision(15, 5);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.DOCCONTABLE)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.AMORTIZABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.USADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.UBICACION)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDFAMILIA)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.CAUSABAJA)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDSECCION)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.SITUACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDTIPO)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDPROPIETARIO)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.IDDEPOSITARIO)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.REFPROVEEDOR)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .Property(e => e.UBICACIONPROVISIONAL)
                .IsUnicode(false);

            modelBuilder.Entity<BIEN>()
                .HasMany(e => e.BIEN1)
                .WithOptional(e => e.BIEN2)
                .HasForeignKey(e => e.NUMBIENPADRE);

            modelBuilder.Entity<BIEN>()
                .HasMany(e => e.BIENPARAM)
                .WithRequired(e => e.BIEN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BIEN>()
                .HasMany(e => e.T_LOG)
                .WithOptional(e => e.BIEN)
                .HasForeignKey(e => e.IDTORRE);

            modelBuilder.Entity<BIEN>()
                .HasMany(e => e.T_LOG1)
                .WithOptional(e => e.BIEN1)
                .HasForeignKey(e => e.IDDISPOSITIVO);

            modelBuilder.Entity<BIEN>()
                .HasMany(e => e.T_LOG2)
                .WithOptional(e => e.BIEN2)
                .HasForeignKey(e => e.IDSOFTWARE);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.IDGFH)
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.ABIERTA_SN)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.TIPOASISTENCIA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.REPARACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.CALIBRACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIENASISTENCIA>()
                .Property(e => e.PRESERIE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BIENPARAM>()
                .Property(e => e.IDPARAM)
                .IsUnicode(false);

            modelBuilder.Entity<BIENPARAM>()
                .Property(e => e.PARAM)
                .IsUnicode(false);

            modelBuilder.Entity<CAJAEQUIPO>()
                .Property(e => e.NUMCAJA)
                .IsRequired();

            modelBuilder.Entity<CAJAEQUIPO>()
                .Property(e => e.NUMEQUIPO)
                .IsRequired();

            modelBuilder.Entity<CAJAEQUIPO>()
                .Property(e => e.NROSERIE)
                .IsRequired();

            modelBuilder.Entity<CAJAEQUIPO>()
                .Property(e => e.NROSERIEORIGINAL)
                .IsRequired();

            modelBuilder.Entity<DIARIOMARCAJE>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJE>()
                .Property(e => e.PRECIO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<DIARIOMARCAJE>()
                .Property(e => e.USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJE>()
                .Property(e => e.IDOPERARIOCONTROL)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJE>()
                .Property(e => e.COMPUTERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJE>()
                .Property(e => e.CARA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJE>()
                .HasMany(e => e.DIARIOMARCAJEFASE)
                .WithOptional(e => e.DIARIOMARCAJE)
                .HasForeignKey(e => e.NUMMARCAJEACTIVIDAD);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.TIPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.INICIOFIN)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.HORAINCORRECTA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.NOTASHORAINCORRECTA)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.INSERCIONPAREJA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.INICIOSESION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .Property(e => e.HOST)
                .IsUnicode(false);

            modelBuilder.Entity<DIARIOMARCAJEFASE>()
                .HasMany(e => e.DIARIOMARCAJEFASE1)
                .WithOptional(e => e.DIARIOMARCAJEFASE2)
                .HasForeignKey(e => e.NUMMARCAJEINICIO);

            modelBuilder.Entity<ESTRUCTURAMATRICULA>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .Property(e => e.AVISOMARPTEVALID)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .Property(e => e.FASETEST)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .Property(e => e.DOCREQECP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .Property(e => e.CODIGOGRUPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.ORDENPROCESOFASE)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.PRODUCTOFASE)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.T_GRUPOPARAMETRIZACION)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.T_IRISERRORPRODUCTOFASE)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.T_IRISERRORFAMILIAFASE)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.T_TESTFASE)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.T_TESTFASES)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FASE>()
                .HasMany(e => e.T_TESTREPORTVALOR)
                .WithRequired(e => e.FASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HOJAMARCAJE>()
                .Property(e => e.IDACTIVIDAD)
                .IsUnicode(false);

            modelBuilder.Entity<HOJAMARCAJE>()
                .Property(e => e.IDACTIVIDADREPROCESO)
                .IsUnicode(false);

            modelBuilder.Entity<HOJAMARCAJE>()
                .Property(e => e.IDCAUSANTEREPROCESO)
                .IsUnicode(false);

            modelBuilder.Entity<HOJAMARCAJE>()
                .Property(e => e.MODIFICACIONSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HOJAMARCAJE>()
                .Property(e => e.PREPARACIONSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HOJAMARCAJE>()
                .HasMany(e => e.DIARIOMARCAJE)
                .WithRequired(e => e.HOJAMARCAJE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HOJAMARCAJEFASE>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<INSTRUCCIONTECNICA>()
                .Property(e => e.REFINSTRUCCION)
                .IsUnicode(false);

            modelBuilder.Entity<INSTRUCCIONTECNICA>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<MATRICULA>()
                .Property(e => e.USUARIO)
                .IsUnicode(false);

            modelBuilder.Entity<MATRICULA>()
                .Property(e => e.SITUACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<MATRICULA>()
                .HasMany(e => e.ESTRUCTURAMATRICULA)
                .WithRequired(e => e.MATRICULA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OPE_ERROR>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<OPE_ERROR>()
                .HasMany(e => e.ORDENPRODUCTOERROR)
                .WithRequired(e => e.OPE_ERROR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OPE_ERROR>()
                .HasMany(e => e.T_TESTERROR)
                .WithRequired(e => e.OPE_ERROR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OPERARIO>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<OPERARIO>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<OPERARIO>()
                .Property(e => e.IDCATEGORIA)
                .IsUnicode(false);

            modelBuilder.Entity<OPERARIO>()
                .Property(e => e.PRECIO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<OPERARIO>()
                .Property(e => e.VARIASORDENES)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OPERARIO>()
                .Property(e => e.PRODUCTIVIDAD)
                .HasPrecision(15, 5);

            modelBuilder.Entity<OPERARIO>()
                .HasMany(e => e.DIARIOMARCAJE)
                .WithOptional(e => e.OPERARIO)
                .HasForeignKey(e => e.IDOPERARIOCONTROL);

            modelBuilder.Entity<OPERARIO>()
                .HasMany(e => e.DIARIOMARCAJE1)
                .WithRequired(e => e.OPERARIO1)
                .HasForeignKey(e => e.IDOPERARIO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OPERARIO>()
                .HasMany(e => e.DIARIOMARCAJEFASE)
                .WithRequired(e => e.OPERARIO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OPERARIO>()
                .HasMany(e => e.ESTRUCTURAMATRICULA)
                .WithRequired(e => e.OPERARIO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OPERARIO>()
                .HasMany(e => e.T_TESTFASE)
                .WithRequired(e => e.OPERARIO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.ABIERTA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.CANTIDAD)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PENDIENTE)
                .HasPrecision(15, 5);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.IDALMACEN)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.UBICACION)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROC_DBMS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROC_TIPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROC_SERIE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROC_NUMERO)
                .HasPrecision(38, 0);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROC_CANTIDAD)
                .HasPrecision(38, 0);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.CONMODIFICACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.DETRANSFORMACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.NOTASMODIFICACION)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.TRAPARTESINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.USERNAMEALTA)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.USERNAMEMODIFICACION)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.CONFIRMACIONENTREGA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.CONFIRMACIONENTREGAIE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.FABRICARPORFASES)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.REFERENCIATIEMPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.NOTASIDP)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PRODUCCIONSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROTOTIPOSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.BLOQUEOSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.NOTASBLOQUEO)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PRESERIESINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.REPROCESO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.IDTIPO)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.STOCKSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.IDLINEAENTREGA)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.MRP_CODI)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.BLOQUEOMARCAJE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.NIVELCONTROLTEST)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.EXTERNOSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.CALCULARINDICADOR)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.UBICACION2)
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.ENSAYO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .Property(e => e.PROSERIESINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDEN>()
                .HasMany(e => e.HOJAMARCAJEFASE)
                .WithRequired(e => e.ORDEN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDEN>()
                .HasMany(e => e.ORDENCAJA)
                .WithRequired(e => e.ORDEN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDEN>()
                .HasMany(e => e.ORDENPRODUCTO)
                .WithRequired(e => e.ORDEN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDEN>()
                .HasMany(e => e.ORDENPROCESOFASE)
                .WithRequired(e => e.ORDEN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDENFASETEST>()
                .Property(e => e.NUMORDEN)
                .IsRequired();

            modelBuilder.Entity<ORDENFASETEST>()
                .Property(e => e.IDFASE)
                .IsRequired();

            modelBuilder.Entity<ORDENFASETEST>()
                .Property(e => e.SECUENCIA)
                .IsRequired();

            modelBuilder.Entity<ORDENPROCESOFASE>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPROCESOFASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPROCESOFASE>()
                .Property(e => e.CODIGOGRUPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPROCESOFASE>()
                .Property(e => e.RESULTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPROCESOFASE>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPRODUCTO>()
                .Property(e => e.NROSERIE)
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPRODUCTO>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPRODUCTO>()
                .Property(e => e.PRODUCTOVALIDO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPRODUCTO>()
                .Property(e => e.SITUACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPRODUCTO>()
                .HasMany(e => e.ORDENPRODUCTOERROR)
                .WithRequired(e => e.ORDENPRODUCTO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDENPRODUCTOERROR>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<ORDENPRODUCTOERROR>()
                .Property(e => e.OBSERVACIONES)
                .IsUnicode(false);

            modelBuilder.Entity<PARTEORDENEQUIPO>()
                .Property(e => e.NROSERIE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.SITUACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.ACTIVO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.IDALMACENPRODUCCION)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.NOTASVERSION)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.IDGFH)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.TIEMPOFABRICACION)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.PRESERIE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.AVISOMARCAJEFASE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.AUTOPARTE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.PRODUCTODECOMPRA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.ENTRAMADO)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.TRAZABILIDAD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.IDFAMILIA)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.LISTADISTRIBAVISOMARC)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.AVISOPREPROD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.MATRICULASHIJAS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.EMPLEADOVALIDAPRODUC)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.NOTASVALIDAPRODUC)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.EMPLEADORESPONSABLE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.EMPLEADOVALIDAID)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.NOTASVALIDAID)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.IDTEST)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.ESPEJOSMD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .Property(e => e.CARASSINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.PRODUCTOFASE)
                .WithRequired(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSION })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.PRODUCTOBINARIO)
                .WithRequired(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSION })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.PRODUCTOCOMPONENTE)
                .WithRequired(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSIONPRODUCTO })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.REPROCESOFAB)
                .WithOptional(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSION });

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.T_TEST)
                .WithOptional(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTOTEST, e.VERSIONPRODUCTOTEST });

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.T_TESTREPORTVALOR)
                .WithOptional(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSION });

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.T_TESTSEQUENCEFILE)
                .WithOptional(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSION });

            modelBuilder.Entity<PRODUCTO>()
                .HasMany(e => e.T_VALORPARAMETROPRODUCTO)
                .WithRequired(e => e.PRODUCTO)
                .HasForeignKey(e => new { e.NUMPRODUCTO, e.VERSION })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PRODUCTOBINARIO>()
                .Property(e => e.BINARIO)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOBINARIO>()
                .Property(e => e.VERBINARIO)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOBINARIO>()
                .Property(e => e.ACTIVO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOCOMPONENTE>()
                .Property(e => e.CANTIDAD)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PRODUCTOCOMPONENTE>()
                .Property(e => e.ORDENMONTAJE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.TIEMPOTEORICO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.TIEMPOSTANDARD)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.TIEMPOPREPARACION)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.FASETEST)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.TACKTIME)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PRODUCTOFASE>()
                .Property(e => e.CODIGOGRUPO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.CODIGO)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.ABIERTO_SN)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IDSITUACION)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IDGFH)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.DESCRIPCIONDETALLADA)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.COSTEPREVISTO)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.HORASPREVISTAS)
                .HasPrecision(15, 5);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IDCLIENTE)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.MAHORASPREVISTAS)
                .HasPrecision(15, 2);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IMPORTEVENTA)
                .HasPrecision(15, 2);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.USUARIO)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.AMBITO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IDTIPO)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.IDCLASE)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.DESCRIPCIONCLIENTE)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .Property(e => e.REFCLIENTE)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTO>()
                .HasMany(e => e.ARTICULO)
                .WithOptional(e => e.PROYECTO)
                .HasForeignKey(e => e.NUMPROYECTO);

            modelBuilder.Entity<PROYECTO>()
                .HasMany(e => e.PROYECTO1)
                .WithOptional(e => e.PROYECTO2)
                .HasForeignKey(e => e.NUMPROYECTOPADRE);

            modelBuilder.Entity<PROYECTOTAREA>()
                .Property(e => e.IDTAREA)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTOTAREA>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTOTAREA>()
                .Property(e => e.DESCRIPCIONDETALLADA)
                .IsUnicode(false);

            modelBuilder.Entity<PROYECTOTAREA>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<REPROCESOFAB>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<REPROCESOFAB>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<REPROCESOFAB>()
                .Property(e => e.SITUACION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<REPROCESOFAB>()
                .HasMany(e => e.RF_ACTUACION)
                .WithRequired(e => e.REPROCESOFAB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<REPROCESOFAB>()
                .HasMany(e => e.RF_ANOMALIA)
                .WithRequired(e => e.REPROCESOFAB)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RF_ACTUACION>()
                .Property(e => e.IDACTUACION)
                .IsUnicode(false);

            modelBuilder.Entity<RF_ACTUACION>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<RF_ACTUACION>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<RF_ANOMALIA>()
                .Property(e => e.IDANOMALIA)
                .IsUnicode(false);

            modelBuilder.Entity<RF_ANOMALIA>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIA>()
                .Property(e => e.FAMILIA)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIA>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIA>()
                .Property(e => e.BLOQUEOTEST)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIA>()
                .Property(e => e.PLATAFORMA)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIA>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.T_FAMILIAPARAM)
                .WithRequired(e => e.T_FAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.T_GRUPOPARAMETRIZACION)
                .WithRequired(e => e.T_FAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.T_IRISERRORFAMILIAFASE)
                .WithRequired(e => e.T_FAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.T_TESTREPORTVALOR)
                .WithRequired(e => e.T_FAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.T_TESTSEQUENCEFILE)
                .WithRequired(e => e.T_FAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.T_TESTCONFIGFILE)
                .WithRequired(e => e.T_FAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIA>()
                .HasMany(e => e.PRODUCTO)
                .WithRequired(e => e.T_FAMILIA)
                .HasForeignKey(e => e.NUMFAMILIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIAERROR>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIAERROR>()
                .HasMany(e => e.OPE_ERROR)
                .WithRequired(e => e.T_FAMILIAERROR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_FAMILIAPARAM>()
                .Property(e => e.IDPARAM)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIAPARAM>()
                .Property(e => e.PARAM)
                .IsUnicode(false);

            modelBuilder.Entity<T_FAMILIAPARAM>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_GRUPOPARAMETRIZACION>()
                .Property(e => e.ALIAS)
                .IsUnicode(false);

            modelBuilder.Entity<T_GRUPOPARAMETRIZACION>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_GRUPOPARAMETRIZACION>()
                .Property(e => e.INOUT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_GRUPOPARAMETRIZACION>()
                .HasMany(e => e.T_VALORPARAMETRO)
                .WithRequired(e => e.T_GRUPOPARAMETRIZACION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_IRISERROR>()
                .Property(e => e.ERRORCODE)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERROR>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERROR>()
                .Property(e => e.DESCRIPCIONML)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERROR>()
                .Property(e => e.DIAGNOSTICOML)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERROR>()
                .HasMany(e => e.T_IRISERRORPRODUCTOFASE)
                .WithRequired(e => e.T_IRISERROR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_IRISERROR>()
                .HasMany(e => e.T_IRISERRORFAMILIAFASE)
                .WithRequired(e => e.T_IRISERROR)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_IRISERRORFAMILIAFASE>()
                .Property(e => e.ERRORCODE)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORFAMILIAFASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORFAMILIAFASE>()
                .Property(e => e.DESCRIPCIONML)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORFAMILIAFASE>()
                .Property(e => e.DIAGNOSTICOML)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORPRODUCTOFASE>()
                .Property(e => e.ERRORCODE)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORPRODUCTOFASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORPRODUCTOFASE>()
                .Property(e => e.DESCRIPCIONML)
                .IsUnicode(false);

            modelBuilder.Entity<T_IRISERRORPRODUCTOFASE>()
                .Property(e => e.DIAGNOSTICOML)
                .IsUnicode(false);

            modelBuilder.Entity<T_LOG>()
                .Property(e => e.IDTIPOEVENTO)
                .IsUnicode(false);

            modelBuilder.Entity<T_LOG>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<T_PARAMETRO>()
                .Property(e => e.PARAM)
                .IsUnicode(false);

            modelBuilder.Entity<T_PARAMETRO>()
                .Property(e => e.METROLOGIASINO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_PARAMETRO>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_PARAMETRO>()
                .HasMany(e => e.T_TESTREPORTVALOR)
                .WithOptional(e => e.T_PARAMETRO)
                .HasForeignKey(e => e.NUMPARAM);

            modelBuilder.Entity<T_PARAMETRO>()
                .HasMany(e => e.T_TESTREPORTVALOR1)
                .WithOptional(e => e.T_PARAMETRO1)
                .HasForeignKey(e => e.NUMPARAM2);

            modelBuilder.Entity<T_PARAMETRO>()
                .HasMany(e => e.T_TESTVALOR)
                .WithRequired(e => e.T_PARAMETRO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_PARAMETRO>()
                .HasMany(e => e.T_VALORPARAMETRO)
                .WithRequired(e => e.T_PARAMETRO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TEST>()
                .Property(e => e.RESULTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_TEST>()
                .Property(e => e.NROSERIE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TEST>()
                .HasMany(e => e.T_TESTFASE)
                .WithRequired(e => e.T_TEST)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TEST>()
                .HasMany(e => e.T_TESTFASES)
                .WithRequired(e => e.T_TEST)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TEST>()
                .HasOptional(e => e.T_TESTREPORT)
                .WithRequired(e => e.T_TEST);

            modelBuilder.Entity<T_TESTERROR>()
                .Property(e => e.PUNTOERROR)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTERROR>()
                .Property(e => e.EXTENSION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTERROR>()
                .Property(e => e.ERRORCODE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASE>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASE>()
                .Property(e => e.RESULTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASE>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASE>()
                .Property(e => e.NOMBREPC)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASE>()
                .HasMany(e => e.T_TESTFASEFILE)
                .WithRequired(e => e.T_TESTFASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TESTFASE>()
                .HasMany(e => e.T_TESTSTEP)
                .WithRequired(e => e.T_TESTFASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TESTFASE>()
                .HasMany(e => e.T_TESTVALOR)
                .WithRequired(e => e.T_TESTFASE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TESTFASEFILE>()
                .Property(e => e.IDTIPODOC)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASEFILE>()
                .Property(e => e.NOMBRE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTFASES>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTREPORTVALOR>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTREPORTVALOR>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTREPORTVALOR>()
                .Property(e => e.VALORAPLICADO)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTREPORTVALOR>()
                .Property(e => e.TOLERANCIA)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTREPORTVALOR>()
                .Property(e => e.VISIBLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTREPORTVALOR>()
                .Property(e => e.UNIDAD)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .Property(e => e.NOMBREFICHERO)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .Property(e => e.USUARIOALTA)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .Property(e => e.USUARIOVALIDACION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .Property(e => e.IDFASE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .HasMany(e => e.T_TESTSEQUENCEFILEVERSION)
                .WithRequired(e => e.T_TESTSEQUENCEFILE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILE>()
                .HasMany(e => e.T_TESTSEQUENCEFILEPRODUCTO)
                .WithRequired(e => e.T_TESTSEQUENCEFILE)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<T_TESTCONFIGFILE>()
                .Property(e => e.NOMBREFICHERO)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTCONFIGFILE>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTCONFIGFILE>()
                .Property(e => e.USUARIOALTA)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTCONFIGFILE>()
                .HasMany(e => e.T_TESTCONFIGFILEVERSION)
                .WithRequired(e => e.T_TESTCONFIGFILE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TESTCONFIGFILE>()
                .HasMany(e => e.T_TESTCONFIGFILEPRODUCTO)
                .WithRequired(e => e.T_TESTCONFIGFILE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILEVERSION>()
                .Property(e => e.USUARIOALTA)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILEVERSION>()
                .Property(e => e.USUARIOVALIDACION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSEQUENCEFILEVERSION>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTCONFIGFILEVERSION>()
                .Property(e => e.USUARIOALTA)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTCONFIGFILEVERSION>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSTEP>()
                .Property(e => e.STEPNAME)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSTEP>()
                .Property(e => e.RESULT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSTEP>()
                .Property(e => e.EXCEPTION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTSTEP>()
                .Property(e => e.GROUPNAME)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTVALOR>()
                .Property(e => e.VALOR)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTVALOR>()
                .Property(e => e.STEPNAME)
                .IsUnicode(false);

            modelBuilder.Entity<T_TESTVALOR>()
                .Property(e => e.VALMAX)
                .HasPrecision(15, 5);

            modelBuilder.Entity<T_TESTVALOR>()
                .Property(e => e.VALMIN)
                .HasPrecision(15, 5);

            modelBuilder.Entity<T_TESTVALOR>()
                .Property(e => e.EXPECTEDVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOEVENTO>()
                .Property(e => e.IDTIPO)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOEVENTO>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOEVENTO>()
                .HasMany(e => e.T_LOG)
                .WithRequired(e => e.T_TIPOEVENTO)
                .HasForeignKey(e => e.IDTIPOEVENTO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TIPOGRUPOPARAMETRIZACION>()
                .Property(e => e.TIPO)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOGRUPOPARAMETRIZACION>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOGRUPOPARAMETRIZACION>()
                .HasMany(e => e.T_GRUPOPARAMETRIZACION)
                .WithRequired(e => e.T_TIPOGRUPOPARAMETRIZACION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TIPOPARAMETRO>()
                .Property(e => e.TIPOPARAM)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOPARAMETRO>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOPARAMETRO>()
                .HasMany(e => e.T_PARAMETRO)
                .WithRequired(e => e.T_TIPOPARAMETRO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_TIPOVALORPARAMETRO>()
                .Property(e => e.TIPOVALOR)
                .IsUnicode(false);

            modelBuilder.Entity<T_TIPOVALORPARAMETRO>()
                .HasMany(e => e.T_PARAMETRO)
                .WithRequired(e => e.T_TIPOVALORPARAMETRO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_UNIDAD>()
                .Property(e => e.UNIDAD)
                .IsUnicode(false);

            modelBuilder.Entity<T_UNIDAD>()
                .HasMany(e => e.T_PARAMETRO)
                .WithRequired(e => e.T_UNIDAD)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_VALORPARAMETRO>()
                .Property(e => e.VALOR)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETRO>()
                .Property(e => e.VALORINICIO)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETRO>()
                .Property(e => e.IDCATEGORIA)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETRO>()
                .HasMany(e => e.T_VALORPARAMETROPRODUCTO)
                .WithRequired(e => e.T_VALORPARAMETRO)
                .HasForeignKey(e => new { e.NUMGRUPO, e.NUMPARAM })
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<T_VALORPARAMETRO>()
            //    .HasOptional(e => e.T_VALORPARAMETROTOL)
            //    .WithRequired(e => e.T_VALORPARAMETRO);

            //modelBuilder.Entity<T_VALORPARAMETRO>()
            //    .HasMany(e => e.T_VALORPARAMETROTOL1)
            //    .WithRequired(e => e.T_VALORPARAMETRO1)
            //    .HasForeignKey(e => new { e.NUMGRUPO2, e.NUMPARAM2 })
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<T_VALORPARAMETRO>()
            //    .HasMany(e => e.T_VALORPARAMETROTOL2)
            //    .WithRequired(e => e.T_VALORPARAMETRO2)
            //    .HasForeignKey(e => new { e.NUMGRUPO1, e.NUMPARAM1 })
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<T_VALORPARAMETROPRODUCTO>()
                .Property(e => e.VALOR)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETROPRODUCTO>()
                .Property(e => e.VALORINICIO)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETROPRODUCTO>()
                .Property(e => e.IDCATEGORIA)
                .IsUnicode(false);

            //modelBuilder.Entity<T_VALORPARAMETROPRODUCTO>()
            //    .HasOptional(e => e.T_VALORPARAMETROTOLPROD)
            //    .WithRequired(e => e.T_VALORPARAMETROPRODUCTO);

            //modelBuilder.Entity<T_VALORPARAMETROPRODUCTO>()
            //    .HasMany(e => e.T_VALORPARAMETROTOLPROD1)
            //    .WithRequired(e => e.T_VALORPARAMETROPRODUCTO1)
            //    .HasForeignKey(e => new { e.NUMGRUPO1, e.NUMPARAM1, e.NUMPRODUCTO, e.VERSION })
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<T_VALORPARAMETROPRODUCTO>()
            //    .HasMany(e => e.T_VALORPARAMETROTOLPROD2)
            //    .WithRequired(e => e.T_VALORPARAMETROPRODUCTO2)
            //    .HasForeignKey(e => new { e.NUMGRUPO2, e.NUMPARAM2, e.NUMPRODUCTO, e.VERSION })
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<T_VALORPARAMETROTOL>()
            //    .Property(e => e.TIPOTOL)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //modelBuilder.Entity<T_VALORPARAMETROTOLPROD>()
            //    .Property(e => e.TIPOTOL)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            modelBuilder.Entity<WEBUSERPARAM>()
                .Property(e => e.IDUSER)
                .IsUnicode(false);

            modelBuilder.Entity<WEBUSERPARAM>()
                .Property(e => e.APPLICATION)
                .IsUnicode(false);

            modelBuilder.Entity<WEBUSERPARAM>()
                .Property(e => e.PARAM)
                .IsUnicode(false);

            modelBuilder.Entity<WEBUSERPARAM>()
                .Property(e => e.VALUE)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETROLOG>()
                .Property(e => e.OLDVALOR)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETROLOG>()
                .Property(e => e.NEWVALOR)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETROLOG>()
                .Property(e => e.USUARIO)
                .IsUnicode(false);

            modelBuilder.Entity<T_VALORPARAMETROLOG>()
                .Property(e => e.NOTAS)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.IDEMPLEADO)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.NOMBRE)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.IDPUESTOTRABAJO)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.IDOPERARIO)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.IDCALENDARIO)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.EXTTLFNINTERNA)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.DEPARTAMENTO)
                .IsUnicode(false);

            modelBuilder.Entity<VW_EMPLEADO>()
                .Property(e => e.DEPT)
                .IsUnicode(false);
        }

        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            return this.Database.SqlQuery<TElement>(sql, parameters);
        }

        private ObjectContext ObjectContext
        {
            get { return ((IObjectContextAdapter)this).ObjectContext; }
        }

        public bool UseProxy
        {
            get { return this.Configuration.ProxyCreationEnabled; }
            set { this.Configuration.ProxyCreationEnabled = value; }
        }

        public void ChangeState<T>(T entity, EntityState state) where T : class
        {
            //if (ObjectContext.IsAttachedTo2(entity))
            //  Detach(entity);

            this.Entry<T>(entity).State = state;
        }

        public void Attach<T>(T entity) where T : class
        {
            if (ObjectContext.IsAttachedTo(entity))
                Detach(entity);

            this.Set<T>().Attach(entity);
        }

        public void Detach<T>(T entity) where T : class
        {
            this.Entry<T>(entity).State = EntityState.Detached;
        }

        public void SetValues<T>(T entity, T fromEntity) where T : class
        {
            this.Entry<T>(entity).CurrentValues.SetValues(fromEntity);
        }


        public void LoadProperty(object entity, string navigationProperty)
        {
            this.ObjectContext.LoadProperty(entity, navigationProperty);
        }

        public int GetNextSerie(string serie)
        {
            return this.Database.SqlQuery<int>(string.Format("SELECT app.{0}.nextval from dual", serie)).FirstOrDefault();
        }

    }
}
