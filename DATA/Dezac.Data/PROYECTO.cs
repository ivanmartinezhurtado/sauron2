namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PROYECTO")]
    public partial class PROYECTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PROYECTO()
        {
            ARTICULO = new HashSet<ARTICULO>();
            BIEN = new HashSet<BIEN>();
            ORDEN = new HashSet<ORDEN>();
            PROYECTO1 = new HashSet<PROYECTO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPROYECTO { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [Required]
        [StringLength(20)]
        public string CODIGO { get; set; }

        [Required]
        [StringLength(1)]
        public string ABIERTO_SN { get; set; }

        [Required]
        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDSITUACION { get; set; }

        [Required]
        [StringLength(10)]
        public string IDGFH { get; set; }

        [StringLength(800)]
        public string DESCRIPCIONDETALLADA { get; set; }

        public DateTime? FECHACIERRE { get; set; }

        public DateTime FECHAFINPREVISTA { get; set; }

        public decimal COSTEPREVISTO { get; set; }

        public decimal HORASPREVISTAS { get; set; }

        [Required]
        [StringLength(10)]
        public string IDCLIENTE { get; set; }

        public decimal? MAHORASPREVISTAS { get; set; }

        public decimal? IMPORTEVENTA { get; set; }

        public int? NUMARTICULO { get; set; }

        [Required]
        [StringLength(15)]
        public string USUARIO { get; set; }

        [Required]
        [StringLength(1)]
        public string AMBITO { get; set; }

        public int? NUMPROYECTOPADRE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTIPO { get; set; }

        [StringLength(10)]
        public string IDCLASE { get; set; }

        [StringLength(100)]
        public string DESCRIPCIONCLIENTE { get; set; }

        [StringLength(20)]
        public string REFCLIENTE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARTICULO> ARTICULO { get; set; }

        public virtual ARTICULO ARTICULO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BIEN> BIEN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDEN> ORDEN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROYECTO> PROYECTO1 { get; set; }

        public virtual PROYECTO PROYECTO2 { get; set; }
    }
}
