﻿namespace WinTestPlayer.Views
{
    partial class ResultsParamView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn9 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn10 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn11 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn12 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn13 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn14 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.enumBinder1 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder2 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder3 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder4 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder5 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder6 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder7 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder8 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder9 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder10 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder11 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder12 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder13 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder14 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.bsList = new System.Windows.Forms.BindingSource(this.components);
            this.radGridView1 = new System.Windows.Forms.DataGridView();
            this.numInstanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorEsperadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // enumBinder1
            // 
            this.enumBinder1.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn1.DataSource = this.enumBinder1;
            gridViewComboBoxColumn1.DisplayMember = "Description";
            gridViewComboBoxColumn1.ValueMember = "Value";
            this.enumBinder1.Target = gridViewComboBoxColumn1;
            // 
            // enumBinder2
            // 
            this.enumBinder2.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn2.DataSource = this.enumBinder2;
            gridViewComboBoxColumn2.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn2.DisplayMember = "Description";
            gridViewComboBoxColumn2.FieldName = "IdUnidad";
            gridViewComboBoxColumn2.HeaderText = "IdUnidad";
            gridViewComboBoxColumn2.IsAutoGenerated = true;
            gridViewComboBoxColumn2.Name = "IdUnidad";
            gridViewComboBoxColumn2.ValueMember = "Value";
            this.enumBinder2.Target = gridViewComboBoxColumn2;
            // 
            // enumBinder3
            // 
            this.enumBinder3.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn3.DataSource = this.enumBinder3;
            gridViewComboBoxColumn3.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn3.DisplayMember = "Description";
            gridViewComboBoxColumn3.FieldName = "IdUnidad";
            gridViewComboBoxColumn3.HeaderText = "IdUnidad";
            gridViewComboBoxColumn3.IsAutoGenerated = true;
            gridViewComboBoxColumn3.Name = "IdUnidad";
            gridViewComboBoxColumn3.ValueMember = "Value";
            this.enumBinder3.Target = gridViewComboBoxColumn3;
            // 
            // enumBinder4
            // 
            this.enumBinder4.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn4.DataSource = this.enumBinder4;
            gridViewComboBoxColumn4.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn4.DisplayMember = "Description";
            gridViewComboBoxColumn4.FieldName = "IdUnidad";
            gridViewComboBoxColumn4.HeaderText = "IdUnidad";
            gridViewComboBoxColumn4.IsAutoGenerated = true;
            gridViewComboBoxColumn4.Name = "IdUnidad";
            gridViewComboBoxColumn4.ValueMember = "Value";
            this.enumBinder4.Target = gridViewComboBoxColumn4;
            // 
            // enumBinder5
            // 
            this.enumBinder5.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn5.DataSource = this.enumBinder5;
            gridViewComboBoxColumn5.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn5.DisplayMember = "Description";
            gridViewComboBoxColumn5.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn5.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn5.IsAutoGenerated = true;
            gridViewComboBoxColumn5.Name = "IdTipoGrupo";
            gridViewComboBoxColumn5.ValueMember = "Value";
            this.enumBinder5.Target = gridViewComboBoxColumn5;
            // 
            // enumBinder6
            // 
            this.enumBinder6.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn6.DataSource = this.enumBinder6;
            gridViewComboBoxColumn6.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn6.DisplayMember = "Description";
            gridViewComboBoxColumn6.FieldName = "IdUnidad";
            gridViewComboBoxColumn6.HeaderText = "IdUnidad";
            gridViewComboBoxColumn6.IsAutoGenerated = true;
            gridViewComboBoxColumn6.Name = "IdUnidad";
            gridViewComboBoxColumn6.ValueMember = "Value";
            this.enumBinder6.Target = gridViewComboBoxColumn6;
            // 
            // enumBinder7
            // 
            this.enumBinder7.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn7.DataSource = this.enumBinder7;
            gridViewComboBoxColumn7.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn7.DisplayMember = "Description";
            gridViewComboBoxColumn7.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn7.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn7.IsAutoGenerated = true;
            gridViewComboBoxColumn7.Name = "IdTipoGrupo";
            gridViewComboBoxColumn7.ValueMember = "Value";
            this.enumBinder7.Target = gridViewComboBoxColumn7;
            // 
            // enumBinder8
            // 
            this.enumBinder8.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn8.DataSource = this.enumBinder8;
            gridViewComboBoxColumn8.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn8.DisplayMember = "Description";
            gridViewComboBoxColumn8.FieldName = "IdUnidad";
            gridViewComboBoxColumn8.HeaderText = "IdUnidad";
            gridViewComboBoxColumn8.IsAutoGenerated = true;
            gridViewComboBoxColumn8.Name = "IdUnidad";
            gridViewComboBoxColumn8.ValueMember = "Value";
            this.enumBinder8.Target = gridViewComboBoxColumn8;
            // 
            // enumBinder9
            // 
            this.enumBinder9.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn9.DataSource = this.enumBinder9;
            gridViewComboBoxColumn9.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn9.DisplayMember = "Description";
            gridViewComboBoxColumn9.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn9.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn9.IsAutoGenerated = true;
            gridViewComboBoxColumn9.Name = "IdTipoGrupo";
            gridViewComboBoxColumn9.ValueMember = "Value";
            this.enumBinder9.Target = gridViewComboBoxColumn9;
            // 
            // enumBinder10
            // 
            this.enumBinder10.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn10.DataSource = this.enumBinder10;
            gridViewComboBoxColumn10.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn10.DisplayMember = "Description";
            gridViewComboBoxColumn10.FieldName = "IdUnidad";
            gridViewComboBoxColumn10.HeaderText = "IdUnidad";
            gridViewComboBoxColumn10.IsAutoGenerated = true;
            gridViewComboBoxColumn10.Name = "IdUnidad";
            gridViewComboBoxColumn10.ValueMember = "Value";
            this.enumBinder10.Target = gridViewComboBoxColumn10;
            // 
            // enumBinder11
            // 
            this.enumBinder11.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn11.DataSource = this.enumBinder11;
            gridViewComboBoxColumn11.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn11.DisplayMember = "Description";
            gridViewComboBoxColumn11.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn11.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn11.IsAutoGenerated = true;
            gridViewComboBoxColumn11.Name = "IdTipoGrupo";
            gridViewComboBoxColumn11.ValueMember = "Value";
            this.enumBinder11.Target = gridViewComboBoxColumn11;
            // 
            // enumBinder12
            // 
            this.enumBinder12.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn12.DataSource = this.enumBinder12;
            gridViewComboBoxColumn12.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn12.DisplayMember = "Description";
            gridViewComboBoxColumn12.FieldName = "IdUnidad";
            gridViewComboBoxColumn12.HeaderText = "IdUnidad";
            gridViewComboBoxColumn12.IsAutoGenerated = true;
            gridViewComboBoxColumn12.Name = "IdUnidad";
            gridViewComboBoxColumn12.ValueMember = "Value";
            this.enumBinder12.Target = gridViewComboBoxColumn12;
            // 
            // enumBinder13
            // 
            this.enumBinder13.Source = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn13.DataSource = this.enumBinder13;
            gridViewComboBoxColumn13.DataType = typeof(Dezac.Tests.Model.TipoGrupo);
            gridViewComboBoxColumn13.DisplayMember = "Description";
            gridViewComboBoxColumn13.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn13.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn13.IsAutoGenerated = true;
            gridViewComboBoxColumn13.Name = "IdTipoGrupo";
            gridViewComboBoxColumn13.ValueMember = "Value";
            this.enumBinder13.Target = gridViewComboBoxColumn13;
            // 
            // enumBinder14
            // 
            this.enumBinder14.Source = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn14.DataSource = this.enumBinder14;
            gridViewComboBoxColumn14.DataType = typeof(Dezac.Tests.Model.ParamUnidad);
            gridViewComboBoxColumn14.DisplayMember = "Description";
            gridViewComboBoxColumn14.FieldName = "IdUnidad";
            gridViewComboBoxColumn14.HeaderText = "IdUnidad";
            gridViewComboBoxColumn14.IsAutoGenerated = true;
            gridViewComboBoxColumn14.Name = "IdUnidad";
            gridViewComboBoxColumn14.ValueMember = "Value";
            this.enumBinder14.Target = gridViewComboBoxColumn14;
            // 
            // bsList
            // 
            this.bsList.DataSource = typeof(Dezac.Tests.Model.ParamValue);
            // 
            // radGridView1
            // 
            this.radGridView1.AllowUserToAddRows = false;
            this.radGridView1.AllowUserToDeleteRows = false;
            this.radGridView1.AllowUserToOrderColumns = true;
            this.radGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.radGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.radGridView1.AutoGenerateColumns = false;
            this.radGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.radGridView1.BackgroundColor = System.Drawing.Color.White;
            this.radGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.radGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.radGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.radGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.radGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numInstanceDataGridViewTextBoxColumn,
            this.stepNameDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.minDataGridViewTextBoxColumn,
            this.valorDataGridViewTextBoxColumn,
            this.maxDataGridViewTextBoxColumn,
            this.unidadDataGridViewTextBoxColumn,
            this.valorEsperadoDataGridViewTextBoxColumn});
            this.radGridView1.DataSource = this.bsList;
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.radGridView1.EnableHeadersVisualStyles = false;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            this.radGridView1.MultiSelect = false;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.radGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.radGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.radGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.radGridView1.Size = new System.Drawing.Size(913, 334);
            this.radGridView1.TabIndex = 1;
            // 
            // numInstanceDataGridViewTextBoxColumn
            // 
            this.numInstanceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.numInstanceDataGridViewTextBoxColumn.DataPropertyName = "NumInstance";
            this.numInstanceDataGridViewTextBoxColumn.HeaderText = "Equipo";
            this.numInstanceDataGridViewTextBoxColumn.Name = "numInstanceDataGridViewTextBoxColumn";
            this.numInstanceDataGridViewTextBoxColumn.ReadOnly = true;
            this.numInstanceDataGridViewTextBoxColumn.Width = 68;
            // 
            // stepNameDataGridViewTextBoxColumn
            // 
            this.stepNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.stepNameDataGridViewTextBoxColumn.DataPropertyName = "StepName";
            this.stepNameDataGridViewTextBoxColumn.HeaderText = "StepName";
            this.stepNameDataGridViewTextBoxColumn.Name = "stepNameDataGridViewTextBoxColumn";
            this.stepNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // minDataGridViewTextBoxColumn
            // 
            this.minDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.minDataGridViewTextBoxColumn.DataPropertyName = "Min";
            this.minDataGridViewTextBoxColumn.HeaderText = "Min";
            this.minDataGridViewTextBoxColumn.Name = "minDataGridViewTextBoxColumn";
            this.minDataGridViewTextBoxColumn.ReadOnly = true;
            this.minDataGridViewTextBoxColumn.Width = 51;
            // 
            // valorDataGridViewTextBoxColumn
            // 
            this.valorDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valorDataGridViewTextBoxColumn.DataPropertyName = "Valor";
            this.valorDataGridViewTextBoxColumn.HeaderText = "Valor";
            this.valorDataGridViewTextBoxColumn.Name = "valorDataGridViewTextBoxColumn";
            this.valorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maxDataGridViewTextBoxColumn
            // 
            this.maxDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.maxDataGridViewTextBoxColumn.DataPropertyName = "Max";
            this.maxDataGridViewTextBoxColumn.HeaderText = "Max";
            this.maxDataGridViewTextBoxColumn.Name = "maxDataGridViewTextBoxColumn";
            this.maxDataGridViewTextBoxColumn.ReadOnly = true;
            this.maxDataGridViewTextBoxColumn.Width = 52;
            // 
            // unidadDataGridViewTextBoxColumn
            // 
            this.unidadDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.unidadDataGridViewTextBoxColumn.DataPropertyName = "Unidad";
            this.unidadDataGridViewTextBoxColumn.HeaderText = "Unidad";
            this.unidadDataGridViewTextBoxColumn.Name = "unidadDataGridViewTextBoxColumn";
            this.unidadDataGridViewTextBoxColumn.ReadOnly = true;
            this.unidadDataGridViewTextBoxColumn.Width = 69;
            // 
            // valorEsperadoDataGridViewTextBoxColumn
            // 
            this.valorEsperadoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valorEsperadoDataGridViewTextBoxColumn.DataPropertyName = "ValorEsperado";
            this.valorEsperadoDataGridViewTextBoxColumn.HeaderText = "ValorEsperado";
            this.valorEsperadoDataGridViewTextBoxColumn.Name = "valorEsperadoDataGridViewTextBoxColumn";
            this.valorEsperadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ResultsParamView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.radGridView1);
            this.Name = "ResultsParamView";
            this.Size = new System.Drawing.Size(913, 334);
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource bsList;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder1;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder2;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder3;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder4;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder5;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder6;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder7;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder8;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder9;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder10;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder11;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder12;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder13;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder14;
        private System.Windows.Forms.DataGridView radGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn numInstanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stepNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorEsperadoDataGridViewTextBoxColumn;
    }
}