﻿using Comunications.Ethernet;
using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Dezac.Device.Metering.SOAP;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.01)]
    public class PLC1000 : DeviceBase
    {
        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private CompactDctSoapService _soap;

        private SSHClient _ssh;

        private Ports puertos;

        private UART uart;

        private SerialPortAdapter sp;

        public string HostName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public byte PortCom { get; set; }

        public SerialPortAdapter SP
        {
          get
            {
                if (sp == null)
                {
                    var serialPort = new SerialPort("COM" + PortCom, 38400, Parity.None, 8, StopBits.One);
                    serialPort.ReadTimeout = 4000;
                     sp = new SerialPortAdapter(serialPort, "\n");
                }

                return sp;
            }
        }

        public SSHClient Ssh
        {
            get
            {
                if (_ssh == null)
                    _ssh = new SSHClient(HostName, UserName, Password, _logger);

                return _ssh;
            }
        }

        public CompactDctSoapService Soap
        {
            get
            {
                if (_soap == null)
                    _soap = new CompactDctSoapService("3.1.c");

                return _soap;
            }
        }

        public Ports Puertos
        {
            get
            {
                if (puertos == null)
                    puertos = new Ports(Ssh);

                return puertos;
            }
        }

        public UART Uart
        {
            get
            {
                if (uart == null)
                    uart = new UART(Ssh);

                return uart;
            }
        }


        public PLC1000()
        {
            this.HostName = "100.0.0.1";
            this.UserName = "root";
            this.Password = "j7awg54q";
        }

        public PLC1000(string hostname, string username, string password)
        {
            this.HostName = hostname;
            this.UserName = username;
            this.Password = password;
        }

        public void KillConcentratorAplication()
        {
            LogMethod();
            Ssh.Run("killall concentrator");
        }

        public void RunConcentratorAplication()
        {
            LogMethod();
            Ssh.Run("concentrator &");
        }

        public enum typePort
        {
            Input,
            Output
        }

        public enum statusPort
        {
            On,
            Off
        }

        public statusPort ReadStatusResetButton()
        {
            LogMethod();
            return Puertos.ReadInputsStatus(Ports.DefinePortsInputs.IN_SOFT_RESET);
        }

        public class Ifconfig
        {
            public string IP { get; set; }
            public string Broadcast { get; set; }
            public string Netmask { get; set; }
        }

        public class Memory
        {
            public string Filesystem { get; set; }
            public string Size { get; set; }
            public string Used { get; set; }
        }

        public List<Memory> StatusInternalMemory()
        {
            LogMethod();
            var memory = Ssh.Run("df -h").Split('\n'); //.Where((p) => p != "").ToArray();
            var memoryList = new List<Memory>();      
            foreach (string row in memory)
            {
                var file = row.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);

                if (file.Count() != 0)
                    memoryList.Add(new Memory()
                    {
                        Filesystem = file[0],
                        Size = file[1],
                        Used = file[2],
                    });
            }
            return memoryList;
        }

        public void FomatearSD()
        {
            LogMethod();
            Ssh.Run("mmc-umount",false);
            System.Threading.Thread.Sleep(1000);
            Ssh.Run("mkfs.ext3 -L Concentrator /dev/mmcblk0p1", false);
            System.Threading.Thread.Sleep(1000);
            Ssh.Run("mmc-mount", false);
            System.Threading.Thread.Sleep(1000);
        }

        public void BorrarMemoria()
        {
            try
            {
                LogMethod();
                Ssh.Run("rm -R /data/*");
                System.Threading.Thread.Sleep(1000);
            }
            catch { }
        }

        public int GetSizeMemoryRAM()
        {
            LogMethod();
            return Convert.ToInt32(Ssh.Run("free | grep Mem: | awk '{print $2}'"));
        }

        public bool DetectionSD()
        {
            LogMethod();
            var resp = Ssh.Run("mount | grep mmcblk0p1");

            if (string.IsNullOrEmpty(resp))
                throw new Exception("Error no se ha leido leer comando de deteccion SD'");

            var sd= resp.Replace("\n", "");

            return sd.Contains("type ext3");         
        }

        public double VerificationTemperature()
        {
            LogMethod();
            var result = Ssh.Run("head -c 2 /dev/adc/0 | od -d | grep 0000000 | awk '{ print $2; }'");

            if (string.IsNullOrEmpty(result))
                throw new Exception("Error no se ha leido nada del comando para obtener la temperatura");

            var adcval = Convert.ToDouble(result.Replace("\n", ""));

            double temperature = 0;

            if (adcval < 1900) 
                temperature = (0.0228 * adcval) - 21.072;
            else if (adcval < 2950) 
                temperature = (0.0242 * adcval) - 24.899;
            else if (adcval < 3550)
                temperature = (0.0417 * adcval) - 77.277;
            else 
                temperature = (0.0714 * adcval) - 181.42;

            return temperature;
        }

        public string StatusExternalMemory()
        {
            LogMethod();
            var result = Ssh.Run("cat /tmp/log.txt | grep \"Last shut down\"");
            var sentence = "Last shut down at";

            if (string.IsNullOrEmpty(result))
                throw new Exception("Error no se ha leido nada del log.txt con el grep 'Last shut down'");
                
            var index = result.LastIndexOf(sentence);

            result = result.Substring(index).Replace(sentence, "");

            return result.Replace("\n","");
        }

        public void ActiveMagneticPulse()
        {
            LogMethod();

            Ssh.Run("echo 55 > /sys/class/gpio/export");
        }

        public byte VerificationMagneticPulse()
        {
            LogMethod();

            var result = Ssh.Run("cat /sys/devices/virtual/gpio/gpio55/value");

            if (string.IsNullOrEmpty(result))
                throw new Exception("Error no se ha leido el estado del pulsador magnetico");

            var pulseDetection = Convert.ToByte(result.Replace("\n", ""));

            return pulseDetection;
        }

        public void DeleteListNodes()
        {
            LogMethod();
            Ssh.Run("rm /media/mmcblk0p1/data/nodes.sqlite3");
            Ssh.Run("sync");
        }

        public double ReadPLCCommunicationsQuality()
        {
            LogMethod();

            // FileHelper es un paquete de nugget que permite procesar facilmente archivos CSV
            var csvReader = new FileHelperEngine<PLCCommunicationsQuality>();
            // Configuro para que en caso de que de un error porque la longitud de datos o el formato de los mismos sea incorrecto, lo ignore y siga adelante
            csvReader.ErrorMode = ErrorMode.IgnoreAndContinue;

            // No uso un Statisctical List porque me da un error al intentar hacer un add, en la excepción, lo unico que leo es "error", por lo que no puedo saber que esta pasando
            var outgoingCommunications = new List<PLCCommunicationsQuality>();

            var amountOfColumns = Ssh.Run("head /tmp/sniffer.csv -n 1").Replace("\r", string.Empty).Split('\n').First().Split(',').Count();
            var communications = Ssh.Run("tail /tmp/sniffer.csv -n 20").Replace(",\r", string.Empty).Split('\n');
         
            var communicationEvents = communications.Where(val => !val.Contains("-,-,-")).ToArray();

            foreach (string currentEvent in communicationEvents)
                if (currentEvent.Split(',').Count().Equals(amountOfColumns))
                    outgoingCommunications.Add(csvReader.ReadString(currentEvent).First());

            return outgoingCommunications.Average((p) => Convert.ToDouble(p.PHYLevel, new CultureInfo("en-US")));
        }

        public Ifconfig IfConfig()
        {
            LogMethod();

            var ifconfig = new Ifconfig();
            
            var response = Ssh.Run("ifconfig").Split('\n');
            var ifconfigArray = response[1].Split(':');

            ifconfig.IP = ifconfigArray[1].Remove(ifconfigArray[1].IndexOf(' '));
            ifconfig.Broadcast = ifconfigArray[2].Remove(ifconfigArray[2].IndexOf(' '));
            ifconfig.Netmask = ifconfigArray[3];

            return ifconfig;
        }

        [DelimitedRecord(",")]
        public class PLCCommunicationsQuality
        {
            public string PCTime;
            
            public string Time;
            
            public string DeltaTime;
            
            public string Htype;
            
            public string DO;
            
            public string LEVEL;
            
            public string HCS;
            
            public string SID;
            
            public string LNID;
            
            public string PRIO;
            
            public string Len;
            
            public string CRC;
            
            public string Txt;
            
            public string Scheme;
            
            public string PHYLevel;
            
            public string PHYSNR;
            
            public string PHYRQ;
        }
        
        public class Ports
        {
            private SSHClient Ssh;

            public enum DefinePortsOutputs
            {
                OUT_NOT_USED1 = 81,      // Not used         (PIN 1 - GPIO2_17)
                OUT_NOT_USED2 = 8,       // Not used         (PIN 2 - GPIO0_8)
                OUT_LED_PLC_LINK = 9,    // LED PLC link     (PIN 3 - GPIO0_9)
                OUT_LED_POWER = 10,      // LED power        (PIN 4 - GPIO0_10)
                OUT_LED_SBT_LINK = 11,   // LED SBT link     (PIN 5 - GPIO0_11)
                OUT_WATCHDOG = 75,       // Watchdog         (PIN 6 - GPIO2_11)
                OUT_LED_MODEM_LINK = 78, // LED modem link   (PIN 11 - GPIO2_14)
                OUT_NOT_USED3 = 79,      // Not used         (PIN 12 - GPIO2_15)
                OUT_RS485_ENTX = 80,     // OUT_NOT_USED4    (PIN 13 - GPIO2_16)
                OUT_GLOBAL_RESET = 71,   // Global reset     (PIN 14 - GPIO2_7)
                OUT_GLOBAL_SUPPLY = 70,  // Global supply    (PIN 15 - GPIO2_6)
                //OUT_NOT_USED5 = 73,      // Not used         (PIN 16 - GPIO2_9)
                //OUT_NOT_USED6 = 72,      // Not used         (PIN 17 - GPIO2_8)
                //OUT_NOT_USED7 = 74,      // Not used         (PIN 19 - GPIO2_10)

                OUT_SBT_SWITCH = 65,     // Select SBT       (PIN 76 - GPIO2_1)
                //OUT_NOT_USED8 = 45,      // Not used         (PIN 85 - GPIO1_13)
                //OUT_NOT_USED9 = 44,      // Not used         (PIN 86 - GPIO1_12)
                //OUT_NOT_USED10 = 103,    // Not used         (PIN 111 - GPIO3_7)
                //OUT_NOT_USED11 = 20,     // Not used         (PIN 135 - GPIO0_20)
                //OUT_NOT_USED12 = 6,      // Not used         (PIN 171 - GPIO0_6)
                //OUT_NOT_USED13 = 7,      // Not used         (PIN 173 - GPIO0_7)
            }

            public enum DefinePortsInputs
            {
                IN_SLOT_ID0 = 87,        // Slot ID0         (PIN 20 - GPIO2_23)
                IN_SLOT_ID1 = 89,        // Slot ID1         (PIN 21 - GPIO2_25)
                IN_SLOT_ID2 = 88,        // Slot ID2         (PIN 22 - GPIO2_24)
                IN_SLOT_ID3 = 64,        // Slot ID3         (PIN 47 - GPIO2_0)
                IN_PG = 100,             // Power good       (PIN 72 - GPIO3_4)
                IN_INTERRUPT = 47,       // Interrupt        (PIN 87 - GPIO1_15)
                IN_SOFT_RESET = 46,      // Soft reset       (PIN 88 - GPIO1_14)
                IN_PLC_MONITOR = 104,    // PLC monitor      (PIN 160 - GPIO3_8)
                IN_PLC_SECONDARY = 102,  // PLC secondary    (PIN 172 - GPIO3_6)
            }

            public Ports(SSHClient ssh)
            {
                Ssh = ssh;
            }

            public void ConfigurationAllPorts()
            {
                foreach (DefinePortsInputs port in Enum.GetValues(typeof(DefinePortsInputs)))
                {
                    ActivePort((byte)port);
                    ConfigPort(typePort.Input, (byte)port);

                    if (!VerificationTypePort(typePort.Input, (byte)port))
                        throw new Exception(string.Format("Error no se ha podido configurar el Puerto {0} como entrada", port.ToString()));

                }

                foreach (DefinePortsOutputs port in Enum.GetValues(typeof(DefinePortsOutputs)))
                {
                    ActivePort((byte)port);
                    ConfigPort(typePort.Output,(byte)port);

                    if (!VerificationTypePort(typePort.Output, (byte)port))
                        throw new Exception(string.Format("Error no se ha podido configurar el Puerto {0} como salida", (byte)port));
                }
            }

            public void ActivePort(byte port)
            {
                Ssh.Run("echo " + port + " > /sys/class/gpio/export");
            }

            public void ConfigPort(typePort type, byte port)
            {
                var function = type == typePort.Output ? "out" : "in";

                Ssh.Run("echo " + function + " > /sys/class/gpio/gpio" + port + "/direction");
            }

            public bool VerificationTypePort(typePort type, byte port)
            {
                var valueType = Ssh.Run("cat /sys/class/gpio/gpio" + port + "/direction").CRoNetString();

                typePort typePort = valueType == "in" ? PLC1000.typePort.Input : PLC1000.typePort.Output;

                if (typePort == type)
                    return true;

                return false;
            }

            public void Off(DefinePortsOutputs port)
            {
                Ssh.Run("echo 0 > /sys/class/gpio/gpio" + (byte)port + "/value");
            }

            public void On(DefinePortsOutputs port)
            {
                Ssh.Run("echo 1 > /sys/class/gpio/gpio" + (byte)port + "/value");
            }

            public bool VerificationOutputStatus(statusPort status, DefinePortsOutputs port)
            {
                var value = Ssh.Run("cat /sys/class/gpio/gpio" + (byte)port + "/value").CRoNetString();

                bool Status = status == statusPort.On ? true : false;

                bool statusRead = value.Trim() == "1" ? true : false;

                return Status == statusRead;
            }

            public bool VerificationInputsStatus(statusPort status, DefinePortsInputs port)
            {
                var value = Ssh.Run("cat /sys/class/gpio/gpio" + (byte)port + "/value").CRoNetString();

                bool Status = status == statusPort.On ? true : false;

                bool statusRead = value.Trim() == "1" ? true : false;

                return Status == statusRead;
            }

            public statusPort ReadInputsStatus(DefinePortsInputs port)
            {
                var value = Ssh.Run("cat /sys/class/gpio/gpio" + (byte)port + "/value").CRoNetString();

               statusPort status;

               status = value.Trim() == "1" ? statusPort.On : statusPort.Off;

               return status;
            }
        }

        public class UART
        {
            private SSHClient Ssh;

            protected MessageTransport transport;

            protected IProtocol protocol;

            private SerialPort sp;

            private byte _port = 12;

            public enum defineUART
            {
                UART1_SBT,
                UART2_PLC,
                UART4_RS485,
                USB1
            }

            public UART(SSHClient ssh)
            {
                Ssh = ssh;
            }

            public void Active_UART(defineUART uart)
            {
                switch (uart)
                {
                    case defineUART.UART1_SBT:
                        Ssh.Run("~# microcom /dev/ttyO1");
                        break;
                    case defineUART.UART2_PLC:
                        Ssh.Run("~# microcom /dev/ttyO2");
                        break;
                    case defineUART.UART4_RS485:
                        Ssh.Run("~# microcom /dev/ttyO4");
                        break;
                    case defineUART.USB1:
                        Ssh.Run("~# microcom /dev/ttyUSB1");
                        break;
                }
            }

            public void Desactive_UART()
            {
                Ssh.Run("~# microcom exit");
            }

            public void SerialPortConfigure()
            {
                if (sp == null)
                    sp = new SerialPort();

                //sets the read and write timeout to 3 seconds
                sp.WriteTimeout = sp.ReadTimeout = 3000;
                sp.BaudRate = 9600;
                sp.StopBits = StopBits.One;
                sp.Parity = Parity.None;
                sp.DataBits = 8;
                sp.PortName = "COM" + _port.ToString();

                if (protocol == null)
                    protocol = new ASCIIProtocol() { CaracterFinTx = "\r", CaracterFinRx = "\r" };

                if (transport == null)
                    transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
            }

            public byte SerialPort
            {
                get { return _port; }
                set
                {
                    if (_port == value)
                        return;

                    _port = value;
                    if (sp.IsOpen)
                    {
                        sp.Close();
                        sp.PortName = "COM" + _port;
                        sp.Open();
                    }
                    else
                        sp.PortName = "COM" + _port;
                }
            }

            public void ExecuteCommand(string command, int numRetries)
            {
                bool error = false;
                StringMessage message;
                do
                {
                    try
                    {
                        _logger.InfoFormat("Command {0}", command);
                        error = false;
                        message = StringMessage.Create(enUS, command);
                        message.Validator = (m) => { return m.Text == command; };
                        var response = transport.SendMessage<StringMessage>(message);
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        numRetries--;

                        _logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                        if (numRetries <= 0 || ex is NotSupportedException)
                            throw;
                    }
                } while (error);
            }

            public void Dispose()
            {
                if (sp != null)
                    sp.Dispose();
            }
        }

        public void CloseConectionSsH()
        {
            LogMethod();

            if (_ssh != null)
            {
                _ssh.Close();
                _ssh.Dispose();
            }
        }

        public override void Dispose()
        {
            CloseConectionSsH();

            if (sp != null)
                sp.Dispose();
        }

    }
}
