﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Reports.Reports
{
  
    public class BoxReportAdpater : ITestReportAdapter
    {
        public object GetReport(object testResult)
        {
            BoxReport br = new BoxReport();

            var brtRow = br.BoxReportTable.NewBoxReportTableRow();

            brtRow.OrdenFabricacion = "";
            brtRow.NumeroFabricacion = "";
            brtRow.Version = "";
            brtRow.Descripcion = "";
            brtRow.Cantidad = "";

            br.BoxReportTable.AddBoxReportTableRow(brtRow);

            return br;
        }
    }
}
