﻿using Microsoft.VisualBasic.Devices;
using System.Collections.Generic;
using System.Linq;

namespace Instruments.Utility
{
    public class MyComputer
    {
        private Computer myComputer = new Computer();
        private List<string> serialPortNames = new List<string>();

        public List<string> SerialPortNames { get { return serialPortNames; } }

        public  string Name { get; private set; }

        public MyComputer()
        {
            serialPortNames.Clear();
            Name = myComputer.Name;
            serialPortNames = myComputer.Ports.SerialPortNames.ToList();
        }
    }
}
