﻿using Elnec.Pg4uw.RemotelbNET;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Tests.Dataman.Controls
{
    public partial class DataManControlSite : UserControl
    {
        private int SiteIndex { get; set; }
        private DatamanManager Manager { get; set; }

        private static string[] OPERATIONS = new string[]
        {
            "", "App Running", "Blank Check", "Reading", "Verifying", "Programming", "Erasing", "Load Project", "Stop", "Self Test", "Self Test Plus"
        };

        public DataManControlSite()
        {
            InitializeComponent();
        }

        public DataManControlSite(DatamanManager manager, int siteIndex)
            : this()
        {
            this.Manager = manager;
            this.SiteIndex = siteIndex;

            lblSite.Text = string.Format("Site #{0}", siteIndex);
        }

        private DatamanManager.DeviceProgressInfo GetSite()
        {
            return Manager.Sites.Find(p => p.Site == SiteIndex);
        }

        public void UpdateUI()
        {
            var site = GetSite();
            bool busy = site.Busy;

            if (busy)
                SetDeviceOpResult("Busy", this.busyLed.ColorOn);

            UpdateLeds(site);

            lblChecksum.Text = site.Checksum;
            lblDevice.Text = site.SelectedDevice;

            if (busy)
                SetProgrammerState("BUSY", System.Drawing.Color.Red);
            else if (Manager.IsOperationActive(site.OpCode))
                SetProgrammerState("WORKING", System.Drawing.Color.Blue);
            else
                SetProgrammerState("IDLE", System.Drawing.Color.Green);

            SetProgressbarValue(site.Progress, Manager.IsOperationActive(site.OpCode));

            RefreshDeviceStatusLabel(site);
        }

        private void UpdateLeds(DatamanManager.DeviceProgressInfo site)
        {
            bool busy = site.Busy;
            this.busyLed.IsOn = busy;

            var deviceOperationColor = SystemColors.Control;

            powerLed.IsOn = site.ProgrammerDetected;

            switch (site.DeviceOpResult)
            {
                case Remotelb.TOpResultForRemote.oprGood:
                    goodLed.IsOn = true;
                    errorLed.IsOn = false;

                    if (!busy)
                        SetDeviceOpResult("GOOD", goodLed.ColorOn);
                    break;
                case Remotelb.TOpResultForRemote.oprFail:
                case Remotelb.TOpResultForRemote.oprHWError:
                    goodLed.IsOn = false;
                    errorLed.IsOn = true;

                    deviceOperationColor = errorLed.ColorOn;
                    if (!busy)
                        SetDeviceOpResult("FAILED", errorLed.ColorOn);
                    break;
                default:
                    goodLed.IsOn = false;
                    errorLed.IsOn = false;

                    if (!busy)
                        SetDeviceOpResult(string.Empty, SystemColors.ControlLight);

                    break;
            }
        }

        private void SetDeviceOpResult(string text, Color color)
        {
            lblOpResult.Text = text;
            lblOpResult.BackColor = color;
        }

        private void SetProgrammerState(string text, Color color)
        {
            lblPrgState.Text = text;
            lblPrgState.BackColor = color;
        }

        private void SetProgressbarValue(int value, bool running)
        {
            progressBar.Value = value;
            progressBar.Visible = running;
        }

        private void RefreshDeviceStatusLabel(DatamanManager.DeviceProgressInfo site)
        {
            string text = null;

            var curOp = site.OpCode;
            bool curBusy = false;

            if (curOp != DatamanManager.OperationType.None)
                curBusy = site.Busy;

            if (!site.ProgrammerDetected)
                SetProgrammerState("Not present", Color.Gray);

            if (!string.IsNullOrEmpty(site.InfoLine))
                text = site.InfoLine;
            else
                text = GetOperationName(curOp);

            if (curBusy)
                text += string.Format(" {0}%", site.Progress);
            else if (!site.ProgrammerDetected && Manager.IsOperationActive(curOp))
                text = "Communication with programmer is lost.";

            lblOp.Text = text;
        }

        private string GetOperationName(DatamanManager.OperationType op)
        {
            switch (op)
            {
                case DatamanManager.OperationType.None:
                    return "None";
                case DatamanManager.OperationType.RunApp:
                    return "Run";
                case DatamanManager.OperationType.BlankCheck:
                    return "Blank check";
                case DatamanManager.OperationType.ReadDevice:
                    return "Read";
                case DatamanManager.OperationType.VerifyDevice:
                    return "Verify";
                case DatamanManager.OperationType.ProgramDevice:
                    return "Program";
                case DatamanManager.OperationType.EraseDevice:
                    return "Erase";
                case DatamanManager.OperationType.LoadProject:
                    return "Load project";
                case DatamanManager.OperationType.Stop:
                    return "Stop";
                default:
                    return "Unknown";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }
    }
}
