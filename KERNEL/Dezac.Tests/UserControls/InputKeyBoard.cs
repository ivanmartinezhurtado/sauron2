﻿using System;
using System.Windows.Forms;

namespace Dezac.Tests.UserControls
{
    public partial class InputKeyBoard : DialogViewBase, IFocusable
    {
        public InputKeyBoard()
        {
            InitializeComponent();
        }

        public InputKeyBoard(string label)
            : this()
        {
            Label = label;
        }

        public InputKeyBoard(string label, string mask)
            : this(label)
        {
            Mask = mask;
        }

        public InputKeyBoard(string label, string mask, string initValue)
            : this(label)
        {
            Mask = mask;
            TextValue = initValue;
        }

        public InputKeyBoard(string label, string mask, Func<bool> validator)
            : this(label, mask)
        {
            Validator = validator;            
        }

        public Func<bool> Validator { get; set; }

        public string Label
        {
            get { return lblCommand.Text;  }
            set { lblCommand.Text = value; }
        }

        public string Mask
        {
            get { return txtResult.Mask; }
            set { txtResult.Mask = value; }
        }

        public string TextValue
        {
            get { return txtResult.Text; }
            set { txtResult.Text = value; }
        }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            if (Validator != null)
                return Validator();

            if (!string.IsNullOrEmpty(Mask))
                return txtResult.MaskFull;

            return true;
        }
    }
}
