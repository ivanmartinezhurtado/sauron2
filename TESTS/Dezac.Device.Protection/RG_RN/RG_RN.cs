﻿using System;
using System.ComponentModel;
using Comunications.Protocolo;
using Comunications.Message;
using System.IO.Ports;
using System.Globalization;
using System.Collections.Generic;
using Dezac.Core.Utility;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.04)]
    public class RG_RN : DeviceBase
    {
        #region Instanciacion y destrucción de la clase del equipo

        public MessageTransport Transport { get; set; }
        public RGRGProtocol Protocol { get; set; }
        public SerialPort Sp { get; set; }
        public byte PortCom { get; set; }

        private RGRNMessage tx, rx;
        private CultureInfo enUS = new CultureInfo("en-US");

        public RG_RN()
        {
        }

        public RG_RN(byte port)
        {
            SetPort(port);
        }

        public void SetPort(byte port)
        {
            Sp = new SerialPort();
            Sp.WriteTimeout = Sp.ReadTimeout = 500;
            Sp.BaudRate = 9600;
            Sp.StopBits = StopBits.One;
            Sp.Parity = Parity.None;
            Sp.DataBits = 8;
            Sp.PortName = "COM" + port.ToString();
            Sp.DtrEnable = true;

            Protocol = new RGRGProtocol();
            Protocol.CaracterFinRx = string.Empty;
            Protocol.CaracterFinTx = string.Empty;
            Transport = new MessageTransport(Protocol, new SerialPortAdapter(Sp), _logger);
            Transport.LogMessage = false;
            Transport.LogRawMessage = true;
            PortCom = port;

            Sp.Open();
        }

        public override void Dispose()
        {
            if (Sp != null)
                Sp.Dispose();
        }

        #endregion

        #region Operaciones de lectura

        public string ReadFirmwareVersion()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.READ_TIME], "");
            tx.HasResponse = true;

            rx = Transport.SendMessage<RGRNMessage>(tx);

            return rx.MessageFrame[3].ToString("N0").Insert(1, ".");
        }

        public ushort ReadCurrentGain()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.READ_GAIN_HI], "");
            tx.HasResponse = true;

            rx = Transport.SendMessage<RGRNMessage>(tx);
            ushort rx_aux = (ushort)((ushort)(rx.MessageFrame[2] << 8) + (ushort)(rx.MessageFrame[3]));
            return rx_aux;
        }

        public uint ReadCurrent()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.READ_CURRENT], "");
            tx.HasResponse = true;

            rx = Transport.SendMessage<RGRNMessage>(tx);
            uint rx_aux = (uint)(rx.MessageFrame[2] << 24) + (uint)(rx.MessageFrame[3] << 16) + (uint)(rx.MessageFrame[4] << 8) + (uint)rx.MessageFrame[5];
            return rx_aux;
        }

        public double ReadCurrent(double escala, ushort? gain = null)
        {
            var currentPoints = ReadCurrent();

            if (gain == null)
                gain = ReadCurrentGain();

            var current = Math.Sqrt(currentPoints / Math.Pow((double)gain, 2)) * escala;

            return current;
        }

        public ushort ReadTime()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.READ_TIME], "");
            tx.HasResponse = true;

            rx = Transport.SendMessage<RGRNMessage>(tx);
            ushort rx_aux = rx.MessageFrame[2];
            return rx_aux;
        }

        #endregion

        #region Operaciones de escritura

        public void WriteFlagTest()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.TEST_MODE], "");
            tx.HasResponse = true;
            
            Transport.SendMessage<RGRNMessage>(tx);
        }

        public uint WriteComputeGain()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.START_CALIBRATION], "");
            tx.HasResponse = true;
            tx.HasValidator = false;

            var timeout = Transport.ReadTimeout;
            Transport.ReadTimeout = 5000;

            rx = Transport.SendMessage<RGRNMessage>(tx);

            uint response = (uint)(rx.MessageFrame[1] << 24) + (uint)(rx.MessageFrame[2] << 16) + (uint)(rx.MessageFrame[3] << 8) + (uint)rx.MessageFrame[4];
            //ushort response = (ushort)((ushort)(rx.MessageFrame[0] << 8) + rx.MessageFrame[1]);

            Transport.ReadTimeout = timeout;

            return response;
        }

        public void WriteCurrentGain(ushort gain)
        {
            LogMethod();
            byte hi = (byte)(gain / 256);
            byte low = (byte)(gain % 256);

            tx = RGRNMessage.Create(RegistersDictionary[Registers.WRITE_GAIN_HI] + (new byte[] { hi, low }).BytesToHexString(), "");
            tx.HasResponse = true;
            Transport.SendMessage<RGRNMessage>(tx);
        }

        public void WriteNormalMode()
        {
            LogMethod();

            tx = RGRNMessage.Create(RegistersDictionary[Registers.END_CALIBRATION], "");
            tx.HasResponse = true;

            Transport.SendMessage<RGRNMessage>(tx);
        }

        #endregion

        #region Enums

        Dictionary<Registers, string> RegistersDictionary = new Dictionary<Registers, string> {
            { Registers.TEST_MODE, "01"},
            { Registers.START_CALIBRATION, "02"},
            { Registers.WRITE_GAIN_HI, "03"},
            { Registers.END_CALIBRATION, "05"},
            { Registers.READ_GAIN_HI, "06"},
            { Registers.READ_CURRENT, "08"},
            { Registers.READ_TIME, "09"}
        };

        public enum Registers
        {
            TEST_MODE,
            START_CALIBRATION,
            WRITE_GAIN_HI,
            END_CALIBRATION,
            READ_GAIN_HI,
            READ_CURRENT,
            READ_TIME,         
        }

        #endregion

        #region Private functions

        public ushort CumputeCurrentGain(uint readCurrent, double escala, double gainFactor )
        {
            var result = Math.Sqrt(readCurrent / 250) / gainFactor;
            return (ushort)result;
        }

        #endregion

    }
}
