﻿namespace WinTestPlayer.Views
{
    partial class TestView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.sequenceView1 = new WinTestPlayer.Views.SequenceView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.resultsParamView1 = new WinTestPlayer.Views.ResultsParamView();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.logViewer1 = new WinTestPlayer.Views.LogViewer();
            this.indicatorsView1 = new WinTestPlayer.Views.IndicatorsView();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.splitPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel1);
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer2.Size = new System.Drawing.Size(745, 893);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            this.radSplitContainer2.Text = "radSplitContainer2";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(745, 660);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2426901F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 125);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel4);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(745, 660);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer3);
            this.splitPanel2.Location = new System.Drawing.Point(0, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(213, 660);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2125506F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(-163, 0);
            this.splitPanel2.TabIndex = 0;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel6);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            this.radSplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer3.Size = new System.Drawing.Size(213, 660);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            this.radSplitContainer3.Text = "radSplitContainer3";
            // 
            // splitPanel6
            // 
            this.splitPanel6.Controls.Add(this.sequenceView1);
            this.splitPanel6.Location = new System.Drawing.Point(0, 0);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel6.Size = new System.Drawing.Size(213, 660);
            this.splitPanel6.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.294686F);
            this.splitPanel6.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 122);
            this.splitPanel6.TabIndex = 0;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // sequenceView1
            // 
            this.sequenceView1.BackColor = System.Drawing.Color.White;
            this.sequenceView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sequenceView1.Location = new System.Drawing.Point(0, 0);
            this.sequenceView1.Name = "sequenceView1";
            this.sequenceView1.Size = new System.Drawing.Size(213, 660);
            this.sequenceView1.TabIndex = 0;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.resultsParamView1);
            this.splitPanel4.Location = new System.Drawing.Point(217, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel4.Size = new System.Drawing.Size(528, 660);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2125506F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(309, 0);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // resultsParamView1
            // 
            this.resultsParamView1.BackColor = System.Drawing.Color.White;
            this.resultsParamView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsParamView1.Location = new System.Drawing.Point(0, 0);
            this.resultsParamView1.Name = "resultsParamView1";
            this.resultsParamView1.Size = new System.Drawing.Size(528, 660);
            this.resultsParamView1.TabIndex = 0;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.logViewer1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 664);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(745, 229);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2426901F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -125);
            this.splitPanel3.TabIndex = 1;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // logViewer1
            // 
            this.logViewer1.BackColor = System.Drawing.Color.White;
            this.logViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logViewer1.Location = new System.Drawing.Point(0, 0);
            this.logViewer1.Name = "logViewer1";
            this.logViewer1.Size = new System.Drawing.Size(745, 229);
            this.logViewer1.TabIndex = 0;
            // 
            // indicatorsView1
            // 
            this.indicatorsView1.BackColor = System.Drawing.Color.White;
            this.indicatorsView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.indicatorsView1.Location = new System.Drawing.Point(745, 0);
            this.indicatorsView1.Name = "indicatorsView1";
            this.indicatorsView1.Size = new System.Drawing.Size(162, 893);
            this.indicatorsView1.TabIndex = 0;
            // 
            // TestView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radSplitContainer2);
            this.Controls.Add(this.indicatorsView1);
            this.Name = "TestView";
            this.Size = new System.Drawing.Size(907, 893);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.splitPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private LogViewer logViewer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private ResultsParamView resultsParamView1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private SequenceView sequenceView1;
        private IndicatorsView indicatorsView1;
    }
}
