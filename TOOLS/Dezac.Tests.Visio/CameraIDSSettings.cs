﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Visio
{
    public class CameraIDSSettings
    {
        public CameraModel Model { get; set; }
        public string SerialNumber { get; set; }
        public double ExposureTime { get; set; }
        public int Saturation { get; set; }
        public double Zoom { get; set; }
        public bool AutoFocusEnable { get; set; }
        public UInt32 FocusManual { get; set; }
        public bool AutoGain { get; set; }
        public int Gain { get; set; }
        public int BlackLevel { get; set; }
        public bool AutoWhiteBalance { get; set; }
        public int RedOffset { get; set; }
        public int BlueOffset { get; set; }
        public double Framerate { get; set; }
        public int PixelClock { get; set; }
        public int ImageFormat { get; set; }
        public bool StartCapture { get; set; }
        public bool AutoGainShutter { get; set; }
        public int Sharpness { get; set; }
        public IDSColor ColorMode { get; set; }
        public int SensorGain { get; set; }
        public Dictionary<IDSColor, uEye.Defines.ColorMode> DictionaryColorMode = new Dictionary<IDSColor, uEye.Defines.ColorMode>
        {
            {IDSColor.RGB,uEye.Defines.ColorMode.BGR8Packed},
            {IDSColor.Mono,uEye.Defines.ColorMode.Mono8},
        };
    }

    public enum IDSFormats
    {
        _6M = 36,
        _5M = 4,
        _3M = 5,
        FULL_HD_16_9 = 6,
        HD_16_9 = 9,
        VGA = 13
    }

    public enum IDSColor
    {
        RGB,
        Mono
    }

    public enum CameraModel
    {
        XS,
        UI_388xLE,
    }
}
