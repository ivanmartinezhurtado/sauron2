﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Utils;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(2.04)]
    [DesignerAction(Type = typeof(InitBastidorMultipleActionDescription))]
    public class InitBastidorMultipleAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Filas que tiene el bastidor")]
        [DefaultValue(1)]
        public int Rows { get; set; }

        [Category("3. Configuration")]
        [Description("Columnas que tiene el bastidor")]
        [DefaultValue(1)]
        public int Columns { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para saber la vida del equipo del que estamos haciendo la secuencia")]
        public bool Traceability { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para ordenar los componentes que tiene el equipo")]
        public bool SortIntroComponents { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para añadir los subconjuntos sin registrar")]
        public bool AddSubsetsWithoutRegistering { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para buscar los números de serie que ha tenido anteriormente el equipo")]
        public bool SearchNumSerieTestFaseBefore { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Verdadero si el código de barras va a ser leído autimáticamente")]
        [DefaultValue(false)]
        public bool FrameAutoReader { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Número de serie de la cámara (No necesario si tiene DataInput)")]
        [DefaultValue(null)]
        public string SerialNumber { get; set; }

        [Category("3.2 Camera Selection")]
        [Description("Nombre de la Cámara si se ha definido como variable con el Action AddVariable")]
        [DefaultValue("")]
        public string DataInput { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Sistema de codificación del código de barras")]
        [DefaultValue(TestHalconExtension.TypeBarcode.Code_128)]
        public TestHalconExtension.TypeBarcode BarCodeType { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Torre de salida que enciende los LEDs para el código de barras")]
        [DefaultValue(null)]
        public byte? LedOutput { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Repeticiones del número de capturas")]
        [DefaultValue((byte)3)]
        public byte Retries { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Fila1 para la especificación del area buscada (0 significa buscar en toda la imagen)")]
        [DefaultValue(0)]
        public int Row1 { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Columna1 para la especificación del area buscada (0 significa buscar en toda la imagen)")]
        [DefaultValue(0)]
        public int Column1 { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Fila2 para la especificación del area buscada (0 significa buscar en toda la imagen)")]
        [DefaultValue(0)]
        public int Row2 { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Columna2 para la especificación del area buscada (0 significa buscar en toda la imagen)")]
        [DefaultValue(0)]
        public int Column2 { get; set; }

        [Category("3.1 AutoReader")]
        [Description("Factor de redimensión usado para reducir la resolución de la imagen(1 significa que no hay redimensión)")]
        [DefaultValue(1)]
        public int ResizeFactor { get; set; }

        private static object _obj = new object();

        public override void Execute()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                WaitAllTestsAndRunOnlyOnce("_INITBASTIDORES_", InternalExecute);
            else
                InternalExecute();

            lock (_obj)
            {
                BastidorService.BastidorValidation(Traceability, SortIntroComponents, SearchNumSerieTestFaseBefore, AddSubsetsWithoutRegistering);
                System.Threading.Thread.Sleep(500);
            }
        }

        private void InternalExecute()
        {
            Retries = Retries != (byte)0 ? Retries : (byte)3; 

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            if (!string.IsNullOrEmpty(DataInput))
                SerialNumber = GetVariable<string>(DataInput);

            SetSharedVariable("ROWS", Rows);
            SetSharedVariable("COLUMNS", Columns);

            if (FrameAutoReader)
                BastidorService.BastidorAutoReader(testBase, LedOutput, SerialNumber, TestHalconExtension.TypeBarcode.Code_128, Retries, Row1, Column1, Row2, Column2, ResizeFactor);

            BastidorService.BastidorController(testBase, Rows, Columns);
        }
        public override void PostExecute(StepResult stepResult)
        {
            if (stepResult.ExecutionResult == StepExecutionResult.Failed)
                stepResult.ExecutionResult = StepExecutionResult.Aborted;
        }

    }

    public class InitBastidorMultipleActionDescription : ActionDescription
    {
        public override string Name { get { return "FrameRegister"; } }
        public override string Category { get { return "DezacTest/Trazabilidad"; } }
        public override string Description { get { return "Action que sirve para leer el bastidor del equipo y mira si el bastidor ya ha sido fabricado"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
        public override string Dependencies { get{ return "InitTestBase"; } }
    }
}
