﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Utils
{
    public enum BaudRateEnum
    {
        Bps9600 = 9600,
        Bps19200 = 19200,
        Bps38400 = 38400,
        Bps57600 = 57600,
        Bps115200 = 115200,
        Bps230400 = 230400,
        Bps460800 = 460800,
        Bps921600 = 921600,
        Bps2000000 = 2000000,
        Bps4000000 = 4000000
    }
}
