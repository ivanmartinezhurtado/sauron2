﻿using Dezac.Core.Exceptions;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(XmlFilterNodeDescription))]
    public class XmlFilterNode: ActionBase
    {
        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Path del Nodo que se quiere filtrar  Ejemplo:/root/nodo1/nodoChild")]
        public string FilterNode { get; set; }

        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Atributo que se quiere filtrar")]
        public string FilterAttribute { get; set; }

        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Valor del Atributo que se quiere filtrar")]
        public string FilterAttributeValue { get; set; }

        [DefaultValue("")]
        [Category("3. Configuration")]
        [Description("Path del fichero xml o variable con los datos XML")]
        public string DataInput { get; set; }

        [Category("4. Results")]
        [Description("Fichero xml o variable con los datos XML de salida")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            if (string.IsNullOrEmpty(DataOutput))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataOutput").Throw();

            if (string.IsNullOrEmpty(DataInput))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataInput no pude ser nula").Throw();

            if (string.IsNullOrEmpty(FilterNode))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("Node").Throw();

            var docXml = XmlDocumentResolve(DataInput);
            if (docXml == null)
                Error().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE("DataInput Fichero XML no encontrdo o no es un XML").Throw();

            SearchNode(docXml);
        }

        private XDocument XmlDocumentResolve(string value)
        {
            XDocument docNav;
            var xml = VariablesTools.ResolveValue(value);
            if (xml is string)
            {
                if (!xml.ToString().StartsWith("<"))
                {
                    var initxml = xml.ToString().IndexOf('<');
                    xml = xml.ToString().Substring(initxml);
                } 

                if (File.Exists(xml.ToString()))
                {
                    Logger.InfoFormat("xml is string -> Path file Exist");
                    docNav = new XDocument((string)xml);
                }
                else
                {
                    Logger.InfoFormat("xml is string -> No path file Exist -> xml to StringReader");
                    docNav = XDocument.Parse(xml.ToString());
                }
            }
            else
            {
                Logger.InfoFormat("xml is no string -> Type {0}", xml.GetType().Name);
                var sxml = xml as XElement;
                docNav = new XDocument(sxml.ToString());
            }

            Logger.InfoFormat("XDocument create succes");
            return docNav;
        }

        private void SearchNode(XDocument xDoc)
        {
            var xpath = xDoc.XPathSelectElements(FilterNode);
            if (xpath != null)
            {
                Logger.InfoFormat("Selected node -> {0}", FilterNode);
                var i = 1;

                foreach (XElement xl in xpath)
                {
                    var register = false;

                    Logger.InfoFormat("Node {0}  pos. {1}", xl.Name, i);

                    if (!string.IsNullOrEmpty(FilterAttribute))
                    {
                        if (xl.HasAttributes)
                            register = NodeFilterAttribute(xl);
                        else
                            Error().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE("Nodo XML sin atributos no se puede aplicar FilterAttribute").Throw();
                    }
                    else
                        register = true;

                    if (register)
                    {
                        var nameNode = string.Format("{0}", DataOutput);

                        if (Context.Variables.ContainsKey(nameNode))
                            nameNode = string.Format("{0}_{1}", DataOutput, i);

                        NodeRegisterAllAtributes(xl, nameNode);

                        if (!string.IsNullOrEmpty(xl.Value))
                        {
                            Context.Variables.AddOrUpdate(string.Format("{0}_Value", nameNode), xl.Value);
                            Context.ResultList.Add(string.Format("{0}_Value", nameNode), xl.Value);
                        }

                        Context.Variables.AddOrUpdate(nameNode, xl);
                        Context.ResultList.Add(nameNode, xl);
                    }

                    i++;
                }
            }
            else
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML Node {0} no encontrado", FilterNode)).Throw();
        }

        private bool NodeFilterAttribute(XElement xl)
        {
            var att = xl.Attribute(FilterAttribute);
            if (att != null)
            {
                if (!string.IsNullOrEmpty(FilterAttributeValue))
                {
                    if (att.Value == FilterAttributeValue)
                        return true;
                }
                else
                    return true;
            }
            else
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0} con atributo {1} no encontrado", FilterNode, FilterAttribute)).Throw();

            return false;
        }  

        private void NodeRegisterAllAtributes(XElement xl, string nameNode)
        {
            var attNode = xl.Attributes();
            if (attNode != null)
                foreach (var att in attNode)
                {
                    var nameAttrbs = string.Format("{0}_{1}", nameNode, att.Name);
                    Context.Variables.AddOrUpdate(nameAttrbs, att.Value);
                    Context.ResultList.Add(nameAttrbs, att.Value);
                }
            else
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0} con atributos no encontrado", FilterNode)).Throw();
        }
    }

    public class XmlFilterNodeDescription : ActionDescription
    { 
        public override string Name { get { return "XmlFilterNode"; }}
        public override string Description { get { return "Action que sirve para filtrar un nodo XML"; } }
        public override string Category { get { return "Actions/Variables/XML"; } }
        public override Image Icon { get { return Properties.Resources.Other_xml_icon; } }
    }
}
