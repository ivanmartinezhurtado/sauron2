﻿using Dezac.Core.Exceptions;
using Instruments.Utility;
using log4net;
using System;

namespace Instruments.PowerSource
{
    public abstract class PowerSourceACI : PowerSourceACBase<double>
    {
        public PowerSourceACI(ILog logger) : base(0D, logger)
        {
        }

        public double ReadVoltage()
        {
            var cmd = new Command<Commands>(Commands.READ_VOLTAGE, true);

            if (SendCommand(cmd))
                return DeviceData.Voltage;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        public double ReadCurrent()
        {
            var cmd = new Command<Commands>(Commands.READ_CURRENT, true);

            if (SendCommand(cmd))
                return DeviceData.Current;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        public double ReadFreq()
        {
            var cmd = new Command<Commands>(Commands.READ_FREQ, true);

            if (SendCommand(cmd))
                return DeviceData.Freq;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        public double ReadPower()
        {
            var cmd = new Command<Commands>(Commands.READ_POWER, true);

            if (SendCommand(cmd))
                return DeviceData.Power;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        public double ReadPowerFactor()
        {
            var cmd = new Command<Commands>(Commands.READ_POWERFACTOR, true);

            if (SendCommand(cmd))
                return DeviceData.PF;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        //********************************************************************

        public bool ApplyAndWaitStabilisation(bool forceApply = false)
        {
            return base.ApplyAndWaitStabilisationBase(forceApply);
        }

        public bool ApplyPresetsAndWaitStabilisation(double voltage, double current, double freq, bool forceApply = false)
        {
            return base.ApplyAndWaitStabilisationBase(voltage, current, freq, forceApply);
        }

        public void ApplyPresets(double voltage, double current, double freq, bool forceApply = false)
        {
            base.ApplyPresetsBase(voltage, current, freq, forceApply);
        }

        public bool ApplyPresetsAndWaitStabilisationWithValidator(double voltage, double current, double freq, bool forceApply = false, Func<bool> validator = null)
        {
            return base.ApplyAndWaitStabilisationBase(voltage, current, freq, forceApply, validator);
        }

        protected override void ResetPresets()
        {
            this.DevicePreset.Voltage = 0;
            this.DevicePreset.Current = 0;
            this.DevicePreset.Freq = 0;
            this.DevicePreset.PF = 0;
            this.DevicePreset.Desfase = 0;
        }
    }
}
