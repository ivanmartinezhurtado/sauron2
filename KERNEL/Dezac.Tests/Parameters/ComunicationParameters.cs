﻿using Dezac.Tests.Model;
using System;

namespace Dezac.Tests.Parameters
{
    public class ComunicationParameters
    {
        ParamValueCollection com;

        public byte SerialPort { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SERIALPORT, 1, ParamUnidad.SinUnidad)); } }
        public byte Periferico { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.PERIFERICO, 1, ParamUnidad.SinUnidad)); } }
        public byte SerialPort2 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SERIALPORT2, 2, ParamUnidad.SinUnidad)); } }
        public byte Periferico2 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.PERIFERICO2, 1, ParamUnidad.SinUnidad)); } }
        public byte SerialPort3 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SERIALPORT3, 3, ParamUnidad.SinUnidad)); } }
        public byte Periferico3 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.PERIFERICO3, 1, ParamUnidad.SinUnidad)); } }
        public int BaudRate { get { return Convert.ToInt32(com.GetDouble(ConstantsParameters.Comunications.BAUDRATE, 9600, ParamUnidad.bps)); } }
        public int BitsStop { get { return Convert.ToInt32(com.GetDouble(ConstantsParameters.Comunications.BITSSTOP, 1, ParamUnidad.SinUnidad)); } }
        public string Paridad { get { return com.GetString(ConstantsParameters.Comunications.PARIDAD, "NO", ParamUnidad.SinUnidad); } }
        public byte SerialPortModulo { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SERIALPORT_MODULO, 6, ParamUnidad.SinUnidad)); } }
        public byte PerifericoModulo { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.PERIFERICO_MODULO, 255, ParamUnidad.SinUnidad)); } }
        public byte SerialPortInstruments { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SERIALPORT_INSTRUMENT, 10, ParamUnidad.SinUnidad)); } }
        public byte SerialPortDevice { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SERIALPORT_DEVICE, 5, ParamUnidad.SinUnidad)); } }
        public byte PerifericoDevice { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.PERIFERICO_DEVICE, 10, ParamUnidad.SinUnidad)); } }
        public byte SP0 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SP0, 5, ParamUnidad.SinUnidad)); } }
        public byte SP1 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SP1, 6, ParamUnidad.SinUnidad)); } }
        public byte SP2 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SP2, 7, ParamUnidad.SinUnidad)); } }
        public byte SP3 { get { return Convert.ToByte(com.GetDouble(ConstantsParameters.Comunications.SP3, 8, ParamUnidad.SinUnidad)); } }
        public string Protocolo { get { return com.GetString(ConstantsParameters.Comunications.PROTOCOLO, "MODBUS", ParamUnidad.SinUnidad); } }
        public string Tipo { get { return com.GetString(ConstantsParameters.Comunications.TIPO, "OPTICO", ParamUnidad.SinUnidad); } }
        public string IP { get { return com.GetString(ConstantsParameters.Comunications.IP, "0.0.0.0", ParamUnidad.SinUnidad); } }

        public ComunicationParameters(ParamValueCollection comunicaciones)
        {
            com = comunicaciones;
        }
    }
}
