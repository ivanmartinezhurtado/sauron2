﻿using Dezac.Core.Utility;
using Dezac.Tests.Services;
using Instruments.Router;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(GetIPClientByPositionDescription))]
    
    public class GetIPClientByPosition : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del usuario en el router")]
        [DefaultValue("root")]
        public string UserName { get; set; }

        [Category("3. Configuration")]
        [Description("Contraseña")]
        [DefaultValue("dezac2309")]
        public string Password { get; set; }

        [Category("3. Configuration")]
        [Description("Posición del dispositivo en la tabla arp del router del que vamos a obtener la IP")]
        public int PositionIPIntoRouter { get; set; }

        public override void Execute()
        {
            Router router = Router.Default();

            if (!string.IsNullOrEmpty(UserName))
            {
                router.UserName = UserName;
                router.Password = Password;
            }

            var data = SequenceContext.Current.Services.Get<ITestContext>();

            string myMAC = string.Empty;
            string deviceIP = string.Empty;

            var sampler = Sampler.Run(
                () => { return new SamplerConfig { NumIterations = 5, Interval = 3500, CancelOnException = true }; },
                (step) =>
                {
                    try
                    {
                        var clients = router.GetConnectedClientsFromWeb(true);
                        if (clients.Count == 0)
                            return;
                        myMAC = clients[PositionIPIntoRouter].MAC;
                        deviceIP = clients[PositionIPIntoRouter].IP;

                        if (SequenceContext.Current.Variables.ContainsKey("LastDevice_MAC"))
                            SequenceContext.Current.Variables.AddOrUpdate("LastDevice_MAC", myMAC);
                        else
                            SequenceContext.Current.Variables.AddOrUpdate("LastDevice_MAC", myMAC);

                        SequenceContext.Current.ResultList.Add("LastDevice_MAC" + myMAC);


                        if (SequenceContext.Current.Variables.ContainsKey("LastDevice_IP"))
                            SequenceContext.Current.Variables.AddOrUpdate("LastDevice_IP", deviceIP);
                        else
                            SequenceContext.Current.Variables.AddOrUpdate("LastDevice_IP", deviceIP);

                        SequenceContext.Current.ResultList.Add("LastDevice_IP" + deviceIP);


                        if (myMAC != "00:01:02:03:04:05")
                            if (data != null)
                                data.TestInfo.NumMAC = myMAC;

                        step.Cancel = true;
                    }
                    catch (TimeoutException)
                    {
                        Thread.Sleep(500);
                    }
                });

            sampler.ThrowExceptionIfExists();


            Assert.IsTrue(sampler.Canceled, Error().UUT.COMUNICACIONES.NO_COMUNICA("No se ha detectado ningún equipo conectado al router"));
        }

    }

    public class GetIPClientByPositionDescription : ActionDescription
    {
        public override string Name { get { return "GetDeviceIPByPosition"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override string Description { get { return "Action para coger el IP del equipo dependiendo de la posición"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
