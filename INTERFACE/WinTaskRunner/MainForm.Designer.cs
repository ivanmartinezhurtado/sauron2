﻿namespace WinTaskRunner
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.toolWindow4 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.toolTabStrip3 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolWindow3 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.toolTabStrip2 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.documentWindow1 = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.toolTabStrip1 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolWindow1 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.toolWindow2 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.commandBarTextBox1 = new Telerik.WinControls.UI.CommandBarTextBox();
            this.commandBarDropDownList1 = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.radDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.cmdActions = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarButton1 = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarGuardar = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.cmdNumTestsLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.cmdNumTests = new Telerik.WinControls.UI.CommandBarTextBox();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.cmdNumTestsP = new Telerik.WinControls.UI.CommandBarTextBox();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.cmdPlay = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarDropDownTypeTest = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.cmdPlayStep = new Telerik.WinControls.UI.CommandBarButton();
            this.cmdPause = new Telerik.WinControls.UI.CommandBarButton();
            this.cmdStop = new Telerik.WinControls.UI.CommandBarButton();
            this.cmdSeparator = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarToggleButtonDiseño = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarToggleButtonVistaMixto = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarToggleButtonVistaRun = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarToggleButtonInstrumntos = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarToggleButtonPowerSource = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarToggleButtonInOut = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarToggleButtonInstrumentDevice = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarToggleButtonDevice = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarToggleButtonModbus = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.cmdPlot = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.cmdNumEnsayoLabel = new Telerik.WinControls.UI.CommandBarLabel();
            this.cmdNumEnsayo = new Telerik.WinControls.UI.CommandBarTextBox();
            this.cmdSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenu1NuevoEnsayo = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenu1AbrirEnsayo = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenu1DescargarEnsayo = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenu1Guardar = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenu1SaveAs = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenuSubir = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenu3NewOF = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radMenu3FindOF = new Telerik.WinControls.UI.RadMenuButtonItem();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6 = new Telerik.WinControls.UI.RadMenuItem();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.commandBarToggleButtonInstruments = new Telerik.WinControls.UI.CommandBarToggleButton();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).BeginInit();
            this.toolTabStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).BeginInit();
            this.toolTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock)).BeginInit();
            this.radDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // toolWindow4
            // 
            this.toolWindow4.Caption = null;
            this.toolWindow4.DefaultFloatingSize = new System.Drawing.Size(300, 200);
            this.toolWindow4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow4.Location = new System.Drawing.Point(1, 24);
            this.toolWindow4.Name = "toolWindow4";
            this.toolWindow4.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow4.Size = new System.Drawing.Size(230, 408);
            this.toolWindow4.Text = "toolWindow4";
            // 
            // toolTabStrip3
            // 
            this.toolTabStrip3.CanUpdateChildIndex = true;
            this.toolTabStrip3.Controls.Add(this.toolWindow3);
            this.toolTabStrip3.Location = new System.Drawing.Point(0, 234);
            this.toolTabStrip3.Name = "toolTabStrip3";
            // 
            // 
            // 
            this.toolTabStrip3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip3.SelectedIndex = 0;
            this.toolTabStrip3.Size = new System.Drawing.Size(646, 200);
            this.toolTabStrip3.TabIndex = 1;
            this.toolTabStrip3.TabStop = false;
            // 
            // toolWindow3
            // 
            this.toolWindow3.Caption = null;
            this.toolWindow3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow3.Location = new System.Drawing.Point(4, 4);
            this.toolWindow3.Name = "toolWindow3";
            this.toolWindow3.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow3.Size = new System.Drawing.Size(638, 192);
            this.toolWindow3.Text = "toolWindow3";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.toolTabStrip2);
            this.radSplitContainer2.Controls.Add(this.radSplitContainer1);
            this.radSplitContainer2.IsCleanUpTarget = true;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(646, 230);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // toolTabStrip2
            // 
            this.toolTabStrip2.CanUpdateChildIndex = true;
            this.toolTabStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip2.Name = "toolTabStrip2";
            // 
            // 
            // 
            this.toolTabStrip2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip2.SelectedIndex = 0;
            this.toolTabStrip2.Size = new System.Drawing.Size(321, 230);
            this.toolTabStrip2.TabIndex = 1;
            this.toolTabStrip2.TabStop = false;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.IsCleanUpTarget = true;
            this.radSplitContainer1.Location = new System.Drawing.Point(325, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(321, 230);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // documentWindow1
            // 
            this.documentWindow1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.documentWindow1.Location = new System.Drawing.Point(6, 29);
            this.documentWindow1.Name = "documentWindow1";
            this.documentWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.documentWindow1.Size = new System.Drawing.Size(430, 0);
            this.documentWindow1.Text = "documentWindow1";
            // 
            // toolTabStrip1
            // 
            this.toolTabStrip1.CanUpdateChildIndex = true;
            this.toolTabStrip1.Controls.Add(this.toolWindow1);
            this.toolTabStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip1.Name = "toolTabStrip1";
            // 
            // 
            // 
            this.toolTabStrip1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip1.SelectedIndex = 0;
            this.toolTabStrip1.Size = new System.Drawing.Size(442, 200);
            this.toolTabStrip1.TabIndex = 1;
            this.toolTabStrip1.TabStop = false;
            // 
            // toolWindow1
            // 
            this.toolWindow1.Caption = null;
            this.toolWindow1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow1.Location = new System.Drawing.Point(4, 4);
            this.toolWindow1.Name = "toolWindow1";
            this.toolWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow1.Size = new System.Drawing.Size(434, 192);
            this.toolWindow1.Text = "toolWindow1";
            // 
            // toolWindow2
            // 
            this.toolWindow2.Caption = null;
            this.toolWindow2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow2.Location = new System.Drawing.Point(1, 24);
            this.toolWindow2.Name = "toolWindow2";
            this.toolWindow2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow2.Size = new System.Drawing.Size(198, 204);
            this.toolWindow2.Text = "toolWindow2";
            // 
            // commandBarTextBox1
            // 
            this.commandBarTextBox1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarTextBox1.DisplayName = "commandBarTextBox1";
            this.commandBarTextBox1.Name = "commandBarTextBox1";
            this.commandBarTextBox1.Text = "1";
            this.commandBarTextBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.commandBarTextBox1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarDropDownList1
            // 
            this.commandBarDropDownList1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarDropDownList1.DisplayName = "commandBarDropDownList1";
            this.commandBarDropDownList1.DropDownAnimationEnabled = true;
            this.commandBarDropDownList1.MaxDropDownItems = 0;
            this.commandBarDropDownList1.Name = "commandBarDropDownList1";
            this.commandBarDropDownList1.Text = "commandBarDropDownList1";
            this.commandBarDropDownList1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // documentContainer1
            // 
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // radDock
            // 
            this.radDock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.radDock.Controls.Add(this.documentContainer1);
            this.radDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock.IsCleanUpTarget = true;
            this.radDock.Location = new System.Drawing.Point(0, 0);
            this.radDock.MainDocumentContainer = this.documentContainer1;
            this.radDock.Name = "radDock";
            this.radDock.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radDock.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radDock.Size = new System.Drawing.Size(1280, 942);
            this.radDock.TabIndex = 22;
            this.radDock.TabStop = false;
            this.radDock.Text = "radDock";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.commandBarRowElement1.AutoSize = true;
            this.commandBarRowElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarRowElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.cmdActions});
            this.commandBarRowElement1.Text = "";
            this.commandBarRowElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.commandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // cmdActions
            // 
            this.cmdActions.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdActions.AutoSize = true;
            this.cmdActions.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cmdActions.AutoToolTip = true;
            this.cmdActions.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdActions.DisplayName = "Actions";
            // 
            // 
            // 
            this.cmdActions.Grip.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdActions.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.commandBarButton1,
            this.commandBarGuardar,
            this.commandBarSeparator3,
            this.cmdNumTestsLabel,
            this.cmdNumTests,
            this.commandBarLabel1,
            this.cmdNumTestsP,
            this.commandBarSeparator2,
            this.cmdPlay,
            this.commandBarDropDownTypeTest,
            this.cmdPlayStep,
            this.cmdPause,
            this.cmdStop,
            this.cmdSeparator,
            this.commandBarToggleButtonDiseño,
            this.commandBarToggleButtonVistaMixto,
            this.commandBarToggleButtonVistaRun,
            this.commandBarSeparator5,
            this.commandBarToggleButtonInstrumntos,
            this.commandBarToggleButtonPowerSource,
            this.commandBarToggleButtonInOut,
            this.commandBarToggleButtonInstrumentDevice,
            this.commandBarSeparator4,
            this.commandBarToggleButtonDevice,
            this.commandBarToggleButtonModbus,
            this.commandBarSeparator6,
            this.cmdPlot,
            this.commandBarSeparator7,
            this.cmdNumEnsayoLabel,
            this.cmdNumEnsayo,
            this.cmdSeparator2});
            this.cmdActions.Name = "commandBarStripElement1";
            // 
            // 
            // 
            this.cmdActions.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.cmdActions.StretchHorizontally = true;
            this.cmdActions.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.cmdActions.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdActions.TextWrap = false;
            ((Telerik.WinControls.UI.RadCommandBarGrip)(this.cmdActions.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.cmdActions.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // commandBarButton1
            // 
            this.commandBarButton1.AutoToolTip = true;
            this.commandBarButton1.DisplayName = "commandBarButton1";
            this.commandBarButton1.Image = global::WinTaskRunner.Properties.Resources.Files_New_File_icon__2_;
            this.commandBarButton1.Name = "commandBarButton1";
            this.commandBarButton1.Text = "commandBarButtonNewSeq";
            this.commandBarButton1.ToolTipText = "Nuva sequencia";
            this.commandBarButton1.Click += new System.EventHandler(this.CommandBarButton1_Click);
            // 
            // commandBarGuardar
            // 
            this.commandBarGuardar.AutoSize = false;
            this.commandBarGuardar.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.commandBarGuardar.AutoToolTip = true;
            this.commandBarGuardar.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarGuardar.DisplayName = "commandBarButton1";
            this.commandBarGuardar.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentPadding;
            this.commandBarGuardar.Image = global::WinTaskRunner.Properties.Resources.Save_icon;
            this.commandBarGuardar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.commandBarGuardar.Name = "commandBarGuardar";
            this.commandBarGuardar.Text = "Guardar";
            this.commandBarGuardar.ToolTipText = "Guardar";
            this.commandBarGuardar.Click += new System.EventHandler(this.commandBarGuardar_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.AutoSize = true;
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // cmdNumTestsLabel
            // 
            this.cmdNumTestsLabel.AutoSize = true;
            this.cmdNumTestsLabel.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cmdNumTestsLabel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdNumTestsLabel.DisplayName = "commandBarLabel1";
            this.cmdNumTestsLabel.Name = "cmdNumTestsLabel";
            this.cmdNumTestsLabel.Text = "Repeticiones:";
            this.cmdNumTestsLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // cmdNumTests
            // 
            this.cmdNumTests.AccessibleRole = System.Windows.Forms.AccessibleRole.Indicator;
            this.cmdNumTests.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdNumTests.AutoSize = true;
            this.cmdNumTests.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.cmdNumTests.DefaultSize = new System.Drawing.Size(0, 0);
            this.cmdNumTests.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdNumTests.DisplayName = "commandBarTextBox1";
            this.cmdNumTests.Name = "cmdNumTests";
            this.cmdNumTests.ShowHorizontalLine = true;
            this.cmdNumTests.StretchHorizontally = false;
            this.cmdNumTests.StretchVertically = false;
            this.cmdNumTests.Text = "1";
            this.cmdNumTests.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdNumTests.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            this.cmdNumTests.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdNumTests.TextWrap = true;
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.AutoSize = true;
            this.commandBarLabel1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.commandBarLabel1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "Equipos a la vez:";
            this.commandBarLabel1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // cmdNumTestsP
            // 
            this.cmdNumTestsP.AutoSize = true;
            this.cmdNumTestsP.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.cmdNumTestsP.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdNumTestsP.Name = "cmdNumTestsP";
            this.cmdNumTestsP.Text = "1";
            this.cmdNumTestsP.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdNumTestsP.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.AutoSize = true;
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // cmdPlay
            // 
            this.cmdPlay.AutoSize = false;
            this.cmdPlay.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cmdPlay.AutoToolTip = true;
            this.cmdPlay.Bounds = new System.Drawing.Rectangle(0, 0, 36, 36);
            this.cmdPlay.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPlay.DisplayName = "Play Test";
            this.cmdPlay.Image = global::WinTaskRunner.Properties.Resources.Play_Normal_icon;
            this.cmdPlay.Name = "cmdPlay";
            this.cmdPlay.Text = "Play Test (&F5)";
            this.cmdPlay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdPlay.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPlay.ToolTipText = "Run Test (F5)";
            this.cmdPlay.Click += new System.EventHandler(this.cmdPlay_Click);
            // 
            // commandBarDropDownTypeTest
            // 
            this.commandBarDropDownTypeTest.AutoSizeItems = true;
            this.commandBarDropDownTypeTest.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.commandBarDropDownTypeTest.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarDropDownTypeTest.DisplayName = "commandBarDropDownList2";
            this.commandBarDropDownTypeTest.DropDownAnimationEnabled = true;
            this.commandBarDropDownTypeTest.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.commandBarDropDownTypeTest.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem1.Text = "Debug";
            radListDataItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            radListDataItem2.Text = "Testing";
            radListDataItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.commandBarDropDownTypeTest.Items.Add(radListDataItem1);
            this.commandBarDropDownTypeTest.Items.Add(radListDataItem2);
            this.commandBarDropDownTypeTest.MaxDropDownItems = 0;
            this.commandBarDropDownTypeTest.Name = "commandBarDropDownTypeTest";
            this.commandBarDropDownTypeTest.Text = "";
            this.commandBarDropDownTypeTest.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // cmdPlayStep
            // 
            this.cmdPlayStep.AutoSize = false;
            this.cmdPlayStep.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cmdPlayStep.AutoToolTip = true;
            this.cmdPlayStep.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.cmdPlayStep.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPlayStep.DisplayName = "Run Step";
            this.cmdPlayStep.Image = global::WinTaskRunner.Properties.Resources.Play_icon__1_;
            this.cmdPlayStep.Name = "cmdPlayStep";
            this.cmdPlayStep.Text = "Run Step";
            this.cmdPlayStep.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPlayStep.ToolTipText = "Run Step (F8)";
            this.cmdPlayStep.Click += new System.EventHandler(this.cmdPlayStep_Click);
            // 
            // cmdPause
            // 
            this.cmdPause.AutoSize = false;
            this.cmdPause.AutoToolTip = true;
            this.cmdPause.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.cmdPause.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPause.DisplayName = "Pause Test";
            this.cmdPause.Image = global::WinTaskRunner.Properties.Resources.Pause_icon__2_;
            this.cmdPause.Name = "cmdPause";
            this.cmdPause.Text = "commandBarButton1";
            this.cmdPause.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPause.ToolTipText = "Pause";
            this.cmdPause.Click += new System.EventHandler(this.cmdPause_Click);
            // 
            // cmdStop
            // 
            this.cmdStop.AutoSize = false;
            this.cmdStop.AutoToolTip = true;
            this.cmdStop.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.cmdStop.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdStop.DisplayName = "Stop Test";
            this.cmdStop.Image = global::WinTaskRunner.Properties.Resources.Stop_Normal_Red_icon__1_;
            this.cmdStop.Name = "cmdStop";
            this.cmdStop.Text = "Stop Test";
            this.cmdStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdStop.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdStop.ToolTipText = "Stop Test";
            this.cmdStop.Click += new System.EventHandler(this.cmdStop_Click);
            // 
            // cmdSeparator
            // 
            this.cmdSeparator.AccessibleDescription = "commandBarSeparator2";
            this.cmdSeparator.AccessibleName = "commandBarSeparator2";
            this.cmdSeparator.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdSeparator.DisplayName = "commandBarSeparator2";
            this.cmdSeparator.Name = "cmdSeparator";
            this.cmdSeparator.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdSeparator.VisibleInOverflowMenu = false;
            // 
            // commandBarToggleButtonDiseño
            // 
            this.commandBarToggleButtonDiseño.AutoSize = false;
            this.commandBarToggleButtonDiseño.AutoToolTip = true;
            this.commandBarToggleButtonDiseño.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarToggleButtonDiseño.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonDiseño.Image = global::WinTaskRunner.Properties.Resources.Draw;
            this.commandBarToggleButtonDiseño.Name = "commandBarToggleButtonDiseño";
            this.commandBarToggleButtonDiseño.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonDiseño.ToolTipText = "Vista Diseño";
            this.commandBarToggleButtonDiseño.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonDiseño_ToggleStateChanged);
            this.commandBarToggleButtonDiseño.Click += new System.EventHandler(this.commandBarToggleButtonDiseño_Click);
            // 
            // commandBarToggleButtonVistaMixto
            // 
            this.commandBarToggleButtonVistaMixto.AutoSize = false;
            this.commandBarToggleButtonVistaMixto.AutoToolTip = true;
            this.commandBarToggleButtonVistaMixto.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarToggleButtonVistaMixto.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonVistaMixto.Image = global::WinTaskRunner.Properties.Resources.ChangeView;
            this.commandBarToggleButtonVistaMixto.Name = "commandBarToggleButtonVistaMixto";
            this.commandBarToggleButtonVistaMixto.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonVistaMixto.ToolTipText = "Vista Mixto";
            this.commandBarToggleButtonVistaMixto.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonVistaMixto_ToggleStateChanged);
            this.commandBarToggleButtonVistaMixto.Click += new System.EventHandler(this.commandBarToggleButtonVistaMixto_Click);
            // 
            // commandBarToggleButtonVistaRun
            // 
            this.commandBarToggleButtonVistaRun.AutoSize = false;
            this.commandBarToggleButtonVistaRun.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.commandBarToggleButtonVistaRun.AutoToolTip = true;
            this.commandBarToggleButtonVistaRun.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarToggleButtonVistaRun.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonVistaRun.Image = global::WinTaskRunner.Properties.Resources.Table_icon;
            this.commandBarToggleButtonVistaRun.Name = "commandBarToggleButtonVistaRun";
            this.commandBarToggleButtonVistaRun.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonVistaRun.ToolTipText = "Vista Run Test";
            this.commandBarToggleButtonVistaRun.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonVistaRun_ToggleStateChanged);
            this.commandBarToggleButtonVistaRun.Click += new System.EventHandler(this.commandBarToggleButtonVistaRun_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.DisplayName = "commandBarSeparator5";
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // commandBarToggleButtonInstrumntos
            // 
            this.commandBarToggleButtonInstrumntos.AutoSize = false;
            this.commandBarToggleButtonInstrumntos.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.commandBarToggleButtonInstrumntos.AutoToolTip = true;
            this.commandBarToggleButtonInstrumntos.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarToggleButtonInstrumntos.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonInstrumntos.Image = global::WinTaskRunner.Properties.Resources.preferences_system_power_icon;
            this.commandBarToggleButtonInstrumntos.Name = "commandBarToggleButtonInstrumntos";
            this.commandBarToggleButtonInstrumntos.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonInstrumntos.ToolTipText = "Vista Power Source | Inputs & Outputs | Instrumentos";
            this.commandBarToggleButtonInstrumntos.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonInstrumntos_ToggleStateChanged);
            // 
            // commandBarToggleButtonPowerSource
            // 
            this.commandBarToggleButtonPowerSource.AutoSize = false;
            this.commandBarToggleButtonPowerSource.AutoToolTip = true;
            this.commandBarToggleButtonPowerSource.Bounds = new System.Drawing.Rectangle(0, 0, 36, 36);
            this.commandBarToggleButtonPowerSource.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonPowerSource.Image = ((System.Drawing.Image)(resources.GetObject("commandBarToggleButtonPowerSource.Image")));
            this.commandBarToggleButtonPowerSource.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.commandBarToggleButtonPowerSource.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.commandBarToggleButtonPowerSource.Name = "commandBarToggleButtonPowerSource";
            this.commandBarToggleButtonPowerSource.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonPowerSource.ToolTipText = "Vista Power Source";
            this.commandBarToggleButtonPowerSource.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonPowerSource_ToggleStateChanged);
            // 
            // commandBarToggleButtonInOut
            // 
            this.commandBarToggleButtonInOut.AutoSize = false;
            this.commandBarToggleButtonInOut.AutoToolTip = true;
            this.commandBarToggleButtonInOut.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarToggleButtonInOut.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonInOut.Image = ((System.Drawing.Image)(resources.GetObject("commandBarToggleButtonInOut.Image")));
            this.commandBarToggleButtonInOut.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.commandBarToggleButtonInOut.Name = "commandBarToggleButtonInOut";
            this.commandBarToggleButtonInOut.StretchHorizontally = false;
            this.commandBarToggleButtonInOut.StretchVertically = false;
            this.commandBarToggleButtonInOut.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonInOut.ToolTipText = "Vista Inputs & Outputs";
            this.commandBarToggleButtonInOut.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonInOut_ToggleStateChanged);
            // 
            // commandBarToggleButtonInstrumentDevice
            // 
            this.commandBarToggleButtonInstrumentDevice.AutoSize = false;
            this.commandBarToggleButtonInstrumentDevice.AutoToolTip = true;
            this.commandBarToggleButtonInstrumentDevice.Bounds = new System.Drawing.Rectangle(0, 0, 36, 36);
            this.commandBarToggleButtonInstrumentDevice.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonInstrumentDevice.Image = ((System.Drawing.Image)(resources.GetObject("commandBarToggleButtonInstrumentDevice.Image")));
            this.commandBarToggleButtonInstrumentDevice.Name = "commandBarToggleButtonInstrumentDevice";
            this.commandBarToggleButtonInstrumentDevice.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonInstrumentDevice.ToolTipText = "Vista Instruments";
            this.commandBarToggleButtonInstrumentDevice.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonInstrumentDevice_ToggleStateChanged);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisplayName = "commandBarSeparator4";
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // commandBarToggleButtonDevice
            // 
            this.commandBarToggleButtonDevice.AutoToolTip = true;
            this.commandBarToggleButtonDevice.DisplayName = "comandToggleButDevice";
            this.commandBarToggleButtonDevice.Image = ((System.Drawing.Image)(resources.GetObject("commandBarToggleButtonDevice.Image")));
            this.commandBarToggleButtonDevice.Name = "commandBarToggleButtonDevice";
            this.commandBarToggleButtonDevice.Text = "Device";
            this.commandBarToggleButtonDevice.ToolTipText = "Vista Inspector Device";
            this.commandBarToggleButtonDevice.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.CommandBarToggleButton1_ToggleStateChanged);
            // 
            // commandBarToggleButtonModbus
            // 
            this.commandBarToggleButtonModbus.AutoSize = false;
            this.commandBarToggleButtonModbus.AutoToolTip = true;
            this.commandBarToggleButtonModbus.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.commandBarToggleButtonModbus.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonModbus.Image = global::WinTaskRunner.Properties.Resources.connect_icon;
            this.commandBarToggleButtonModbus.Name = "commandBarToggleButtonModbus";
            this.commandBarToggleButtonModbus.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonModbus.ToolTipText = "Vista Modbus Resgister";
            this.commandBarToggleButtonModbus.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.commandBarToggleButtonModbus_ToggleStateChanged);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.DisplayName = "commandBarSeparator6";
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // cmdPlot
            // 
            this.cmdPlot.AutoSize = false;
            this.cmdPlot.AutoToolTip = true;
            this.cmdPlot.Bounds = new System.Drawing.Rectangle(0, 0, 28, 36);
            this.cmdPlot.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPlot.DisplayName = "Plot View";
            this.cmdPlot.Image = global::WinTaskRunner.Properties.Resources.Line_chart_icon;
            this.cmdPlot.Name = "cmdPlot";
            this.cmdPlot.Text = "commandBarButton1";
            this.cmdPlot.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdPlot.ToolTipText = "Vista Graficas";
            this.cmdPlot.Click += new System.EventHandler(this.cmdPlot_Click);
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.DisplayName = "commandBarSeparator7";
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // cmdNumEnsayoLabel
            // 
            this.cmdNumEnsayoLabel.AutoSize = true;
            this.cmdNumEnsayoLabel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdNumEnsayoLabel.DisplayName = "commandBarLabel1";
            this.cmdNumEnsayoLabel.Name = "cmdNumEnsayoLabel";
            this.cmdNumEnsayoLabel.Text = "O.F.";
            this.cmdNumEnsayoLabel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // cmdNumEnsayo
            // 
            this.cmdNumEnsayo.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdNumEnsayo.DisplayName = "commandBarTextBox2";
            this.cmdNumEnsayo.Name = "cmdNumEnsayo";
            this.cmdNumEnsayo.Text = "";
            this.cmdNumEnsayo.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // cmdSeparator2
            // 
            this.cmdSeparator2.AccessibleDescription = "commandBarSeparator2";
            this.cmdSeparator2.AccessibleName = "commandBarSeparator2";
            this.cmdSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdSeparator2.DisplayName = "commandBarSeparator2";
            this.cmdSeparator2.Name = "cmdSeparator2";
            this.cmdSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.cmdSeparator2.VisibleInOverflowMenu = false;
            // 
            // radCommandBar
            // 
            this.radCommandBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCommandBar.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar.Name = "radCommandBar";
            this.radCommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar.Size = new System.Drawing.Size(1280, 65);
            this.radCommandBar.TabIndex = 19;
            this.radCommandBar.Text = "radCommandBar1";
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AutoSize = true;
            this.radMenuItem1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenu1NuevoEnsayo,
            this.radMenu1AbrirEnsayo,
            this.radMenu1DescargarEnsayo,
            this.radMenuSeparatorItem1,
            this.radMenu1Guardar,
            this.radMenu1SaveAs,
            this.radMenuSubir});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "Archivo";
            // 
            // radMenu1NuevoEnsayo
            // 
            this.radMenu1NuevoEnsayo.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radMenu1NuevoEnsayo.AutoSize = false;
            this.radMenu1NuevoEnsayo.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenu1NuevoEnsayo.Bounds = new System.Drawing.Rectangle(0, 0, 170, 35);
            this.radMenu1NuevoEnsayo.Image = global::WinTaskRunner.Properties.Resources.Files_New_File_icon__2_;
            this.radMenu1NuevoEnsayo.Name = "radMenu1NuevoEnsayo";
            this.radMenu1NuevoEnsayo.Text = "Nuevo ensayo";
            this.radMenu1NuevoEnsayo.Click += new System.EventHandler(this.radMenu1NuevoEnsayo_Click);
            // 
            // radMenu1AbrirEnsayo
            // 
            this.radMenu1AbrirEnsayo.AutoSize = false;
            this.radMenu1AbrirEnsayo.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenu1AbrirEnsayo.Bounds = new System.Drawing.Rectangle(0, 0, 170, 34);
            this.radMenu1AbrirEnsayo.Image = global::WinTaskRunner.Properties.Resources.open_file_icon;
            this.radMenu1AbrirEnsayo.Name = "radMenu1AbrirEnsayo";
            this.radMenu1AbrirEnsayo.Text = "Abrir ensayo";
            this.radMenu1AbrirEnsayo.Click += new System.EventHandler(this.radMenu1AbrirEnsayo_Click);
            // 
            // radMenu1DescargarEnsayo
            // 
            this.radMenu1DescargarEnsayo.AutoSize = false;
            this.radMenu1DescargarEnsayo.Bounds = new System.Drawing.Rectangle(0, 0, 170, 45);
            this.radMenu1DescargarEnsayo.Image = global::WinTaskRunner.Properties.Resources.Action_db_update_icon1;
            this.radMenu1DescargarEnsayo.Name = "radMenu1DescargarEnsayo";
            this.radMenu1DescargarEnsayo.Text = "Descargar ensayo";
            this.radMenu1DescargarEnsayo.Click += new System.EventHandler(this.radMenu1DescargarEnsayo_Click);
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radMenu1Guardar
            // 
            this.radMenu1Guardar.AutoSize = false;
            this.radMenu1Guardar.Bounds = new System.Drawing.Rectangle(0, 0, 170, 34);
            this.radMenu1Guardar.Image = global::WinTaskRunner.Properties.Resources.Save_icon;
            this.radMenu1Guardar.Name = "radMenu1Guardar";
            this.radMenu1Guardar.Text = "Guardar";
            this.radMenu1Guardar.Click += new System.EventHandler(this.radMenu1Guardar_Click);
            // 
            // radMenu1SaveAs
            // 
            this.radMenu1SaveAs.AutoSize = false;
            this.radMenu1SaveAs.Bounds = new System.Drawing.Rectangle(0, 0, 170, 34);
            this.radMenu1SaveAs.Image = global::WinTaskRunner.Properties.Resources.SaveAs2_icon;
            this.radMenu1SaveAs.Name = "radMenu1SaveAs";
            this.radMenu1SaveAs.Text = "Guardar como";
            this.radMenu1SaveAs.Click += new System.EventHandler(this.radMenu1SaveAs_Click);
            // 
            // radMenuSubir
            // 
            this.radMenuSubir.AutoSize = false;
            this.radMenuSubir.Bounds = new System.Drawing.Rectangle(0, 0, 170, 45);
            this.radMenuSubir.Image = global::WinTaskRunner.Properties.Resources.Action_db_commit_icon;
            this.radMenuSubir.Name = "radMenuSubir";
            this.radMenuSubir.Text = "Subir ensayo";
            this.radMenuSubir.Click += new System.EventHandler(this.radMenuSubir_Click);
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenu3NewOF,
            this.radMenu3FindOF});
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "Preserie";
            // 
            // radMenu3NewOF
            // 
            this.radMenu3NewOF.AutoSize = false;
            this.radMenu3NewOF.Bounds = new System.Drawing.Rectangle(0, 0, 170, 35);
            this.radMenu3NewOF.Image = global::WinTaskRunner.Properties.Resources._1478532846_report_user_2;
            this.radMenu3NewOF.Name = "radMenu3NewOF";
            this.radMenu3NewOF.Text = "Nueva Orden Fab.";
            this.radMenu3NewOF.Click += new System.EventHandler(this.radMenu3NewOF_Click);
            // 
            // radMenu3FindOF
            // 
            this.radMenu3FindOF.AutoSize = false;
            this.radMenu3FindOF.Bounds = new System.Drawing.Rectangle(0, 0, 170, 35);
            this.radMenu3FindOF.Image = global::WinTaskRunner.Properties.Resources._1478532766_report_magnify_2;
            this.radMenu3FindOF.Name = "radMenu3FindOF";
            this.radMenu3FindOF.Text = "Buscar Orden Fab.";
            this.radMenu3FindOF.Click += new System.EventHandler(this.radMenu3FindOF_Click);
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel1);
            this.radSplitContainer3.Controls.Add(this.splitPanel2);
            this.radSplitContainer3.Controls.Add(this.splitPanel3);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            this.radSplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer3.Size = new System.Drawing.Size(1280, 1003);
            this.radSplitContainer3.SplitterWidth = 0;
            this.radSplitContainer3.TabIndex = 29;
            this.radSplitContainer3.TabStop = false;
            this.radSplitContainer3.Text = "radSplitContainer3";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radMenu1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(1280, 19);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.3142379F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -131);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radMenu1
            // 
            this.radMenu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem3,
            this.radMenuItem4});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(1280, 20);
            this.radMenu1.TabIndex = 26;
            this.radMenu1.Text = "radMenu1";
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem5,
            this.radMenuItem6});
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "Ayuda";
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "Ayuda online";
            // 
            // radMenuItem6
            // 
            this.radMenuItem6.Name = "radMenuItem6";
            this.radMenuItem6.Text = "Acerca de";
            this.radMenuItem6.Click += new System.EventHandler(this.RadMenuItem6_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radCommandBar);
            this.splitPanel2.Location = new System.Drawing.Point(0, 19);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(1280, 42);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2911223F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -164);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.radDock);
            this.splitPanel3.Location = new System.Drawing.Point(0, 61);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(1280, 942);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.6053602F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 295);
            this.splitPanel3.TabIndex = 2;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // commandBarToggleButtonInstruments
            // 
            this.commandBarToggleButtonInstruments.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarToggleButtonInstruments.DisplayName = "commandBarToggleButton1";
            this.commandBarToggleButtonInstruments.Image = global::WinTaskRunner.Properties.Resources.preferences_system_power_icon;
            this.commandBarToggleButtonInstruments.Name = "commandBarToggleButtonInstruments";
            this.commandBarToggleButtonInstruments.Text = "commandBarToggleButton1";
            this.commandBarToggleButtonInstruments.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarToggleButtonInstruments.ToolTipText = "Instruments";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1280, 1003);
            this.Controls.Add(this.radSplitContainer3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WinTaskRunner";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).EndInit();
            this.toolTabStrip3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).EndInit();
            this.toolTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock)).EndInit();
            this.radDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            this.splitPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow4;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip3;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow3;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.Docking.DocumentWindow documentWindow1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow2;
        private Telerik.WinControls.UI.CommandBarTextBox commandBarTextBox1;
        private Telerik.WinControls.UI.CommandBarDropDownList commandBarDropDownList1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.RadDock radDock;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement cmdActions;
        private Telerik.WinControls.UI.CommandBarButton cmdPlay;
        private Telerik.WinControls.UI.CommandBarDropDownList commandBarDropDownTypeTest;
        private Telerik.WinControls.UI.CommandBarButton cmdPlayStep;
        private Telerik.WinControls.UI.CommandBarButton cmdPause;
        private Telerik.WinControls.UI.CommandBarButton cmdStop;
        private Telerik.WinControls.UI.CommandBarSeparator cmdSeparator;
        private Telerik.WinControls.UI.CommandBarLabel cmdNumTestsLabel;
        private Telerik.WinControls.UI.CommandBarTextBox cmdNumTests;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarTextBox cmdNumTestsP;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarLabel cmdNumEnsayoLabel;
        private Telerik.WinControls.UI.CommandBarTextBox cmdNumEnsayo;
        private Telerik.WinControls.UI.CommandBarSeparator cmdSeparator2;
        private Telerik.WinControls.UI.CommandBarButton cmdPlot;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu1NuevoEnsayo;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu1AbrirEnsayo;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu1DescargarEnsayo;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu1Guardar;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu1SaveAs;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenuSubir;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu3NewOF;
        private Telerik.WinControls.UI.RadMenuButtonItem radMenu3FindOF;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6;
        private Telerik.WinControls.UI.CommandBarButton commandBarGuardar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonVistaMixto;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonVistaRun;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonDiseño;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonInstrumntos;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonInstruments;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonModbus;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonPowerSource;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonInOut;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonInstrumentDevice;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.CommandBarButton commandBarButton1;
        private Telerik.WinControls.UI.CommandBarToggleButton commandBarToggleButtonDevice;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private Telerik.WinControls.UI.RadMenu radMenu1;
    }
}

