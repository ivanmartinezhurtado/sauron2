﻿using Elnec.Pg4uw.RemotelbNET;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Dezac.Tests.Dataman
{
    public class DatamanManager : IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(DatamanManager));

        protected static readonly string CREDIT_BOX_WARNING = "CREDIT BOX WITH SUFFICIENT AMOUNT";
        protected static readonly string CREDIT_BOX_ERROR = "CREDIT BOX ERROR";

        public enum DetailedOpResult
        {
            Unavailable = 0,
            Good = 1,
            AdapterTestFailure = 2,
            InsertionTestFailure = 3,
            IDCheckFailure = 4,
            AccessCanceledByUserFailure = 5,
            OtherFailure = 6,
            OperationalFailure = 999,

            NotReady = -1,
            NoCreditBox = -2,
            TimeOut = -3
        }

        public enum OperationType
        {
            None = 0,
            RunApp,
            BlankCheck,
            ReadDevice,
            VerifyDevice,
            ProgramDevice,
            EraseDevice,
            LoadProject,
            Stop,
            SelfTest,
            SelfTestPlus,
        }

        public enum ProgrammerStates
        {
            Ready = 1,
            NotReady = 2,
            Busy = 3,
        }

        public enum FileResult
        {
            Good = 11,
            Error = 12,
            Cancelled = 13,
            None = 99
        }

        public class DeviceProgressInfo
        {
            public DeviceProgressInfo()
            {
                OpActionsDone = new List<OpActionsInfo>();
            }

            public int Site { get; set; }
            public int? PIDPguw { get; set; }
            public string SerialNumber { get; set; }
            public string ProjectFileName { get; set; }
            public string BinaryFileName { get; set; }
            public int instance { get; set; }
            public int TimeOut { get; set; }

            public string SelectedDevice { get; set; }
            public int Progress { get; internal set; }
            public OperationType OpCode { get; internal set; }
            public string InfoLine { get; internal set; }
            public bool Busy { get; internal set; }
            public bool ProgrammerDetected { get; internal set; }
            public ProgrammerStates ProgrammerReady { get; internal set; }
            public FileResult LoadProjectResult { get; internal set; }
            public DetailedOpResult OpResult { get; set; }
            public List<OpActionsInfo> OpActionsDone { get; internal set; }
            public DateTime? StartOp { get; set; }
            public DateTime? EndOp { get; set; }
            public Remotelb.TOpResultForRemote DeviceOpResult { get; set; }
            public string Checksum { get; set; }
            public OperationType LastOperation { get; set; }
            public bool Connected { get; set; }
            public bool NextSiteStarted { get; set; }
            public DateTime LoadProjectTime { get; set; }
        }

        public class StatisticsInfo
        {
            public int Good { get; set; }
            public int Fail { get; set; }
            public int Total { get { return Good + Fail; } }
        }

        public class OpActionsInfo
        {
            public DateTime Start { get; set; }
            public DateTime? End { get; set; }
            public OperationType OpCode { get; set; }
            public bool Error { get; set; }

            public double TotalSeconds {
                get
                {
                    return (End.GetValueOrDefault(Start) - Start).TotalSeconds;
                }
            }
        }

        public string DatamanPath { get; set; }
        public bool ViewLog { get; set; }

        public OperationType AutoRunOp { get; set; }

        private Remotelb remotelb;

        public DeviceProgressInfo DefaultSite { get { return Sites.FirstOrDefault(); } }

        public StatisticsInfo Stats { get; private set; }

        public event EventHandler StateChanged;
        public event EventHandler OperationCompleted;
        public event EventHandler OnConnected;

        public bool Verbose { get; set; }

        public bool HasClientsConnected { get; private set; }

        public bool MultiSite { get;  set; }

        private Timer timer;

        public List<DeviceProgressInfo> Sites { get; private set; }

        public const string TCP_CMD_GETDETAILEDOPRESULT_RESULT = "detailedopresult:";
        public const string TCP_CMD_SERVER_CAN_RUN_ANOTHER_SITE = "canrunanothersite";

        public string Log { get; private set; }

        public DatamanManager()
        {
            Stats = new StatisticsInfo();
            Sites = new List<DeviceProgressInfo>();
        }

        public void FirstConnect(List<PositionSite> sitesPosition)
        {
            if (remotelb == null)
            {
                var procs = Process.GetProcessesByName("pg4uw");
                foreach (var p in procs)
                    p.Kill();

                remotelb = new Remotelb();

                remotelb.OnClientConnect += Remotelb_OnClientConnect;
                remotelb.onClientDisconnect += Remotelb_onClientDisconnect;
                remotelb.OnWriteLineToLogWindow += Remotelb_OnWriteLineToLogWindow;
                remotelb.OnServerProcess += Remotelb_OnServerProcess;

                if (MultiSite)
                    remotelb.CreateServerAndMakeListenToMultiClients("telnet");
                else
                    remotelb.CreateServerAndMakeListenToClients("telnet");
            }

            Sites.Clear();

            foreach (var pos in sitesPosition)
            {
                var site = new DeviceProgressInfo
                {
                    Site = pos.position,
                    SerialNumber = pos.serialNumber,
                    BinaryFileName = pos.binaryFile,
                    ProjectFileName = pos.ProjectFile,
                    instance = pos.instance,
                    LoadProjectTime = DateTime.Now,
                    TimeOut = pos.timeoutGrabador,
                };

                LoadProject(site);
                Sites.Add(site);
            }
        }

        public void Connect(OperationType autoRunOp = OperationType.None)
        {
            inTimer = false;

            AutoRunOp = autoRunOp;

            Sites.ForEach(site =>
            {
                site.OpCode = OperationType.None;
                site.OpResult = DetailedOpResult.Unavailable;
                site.StartOp = null;
                site.EndOp = null;
                site.LoadProjectTime = DateTime.Now;
                site.OpActionsDone.Clear();
            });
            
            if (OnConnected != null)
                OnConnected(this, EventArgs.Empty);

            StartNextSite();

            timer = new Timer(OnTimerTick, this,250, 500);     
        }

        private void StartNextSite()
        {
            var site = Sites.Find(p => !p.PIDPguw.HasValue);
            if (site == null)
                return;

            StartNextSite(site);
        }

        private void StartNextSite(DeviceProgressInfo site)
        { 
            string args = " /enableremote:autoanswer /remoteport:telnet /remoteaddr:localhost "; //"/startvisible";

            if (ViewLog)
                args += " /copylogwintoremote";

            if (!string.IsNullOrEmpty(site.SerialNumber))
                args += string.Format(" /usb:{0}:{1}", site.Site, site.SerialNumber);

            if (!string.IsNullOrEmpty(site.ProjectFileName))
            {
                if (!File.Exists(site.ProjectFileName))
                    throw new Exception(string.Format("El archivo del proyecto {0} no existe!", site.ProjectFileName));

                args += " /prj:\"" + site.ProjectFileName + "\"";
            }

            if (!string.IsNullOrEmpty(site.BinaryFileName))
            {
                if (!File.Exists(site.BinaryFileName))
                    throw new Exception(string.Format("El archivo binario {0} no existe!", site.BinaryFileName));

                args += " /loadfile:\"" + site.BinaryFileName + "\"";
            }

            string appPath = Path.Combine(DatamanPath ?? "", "Pg4uw.exe");

            AddLog(string.Format("Posicion {0} -> Run DATAMAN -> {1}", site.Site, appPath));
            AddLog(string.Format("Posicion {0} -> Args -> {1}", site.Site, args));
            
            site.PIDPguw = Process.Start(appPath, args).Id;
            site.LoadProjectTime = DateTime.Now;
            site.ProgrammerReady = ProgrammerStates.NotReady;
        }

        //-----------------------------------------------------------

        private void StartNextSiteBySite()
        {
            Sites.ForEach(p =>
            {
                if (!p.PIDPguw.HasValue)
                {
                    StartNextSite(p);
                    do 
                    {
                        DoOperationBySite(p);
                    }
                    while (p.LastOperation == OperationType.ProgramDevice);
                }
            });
        }

        private void DoOperationBySite(DeviceProgressInfo p)
        {
            if (Remotelb.GetIfClientAppWithOrdNumExists(p.Site))
            {
                if (!p.Connected)
                {
                    p.Connected = true;
                    Remotelb.SEND_CMD_GetProgrammerSiteNameAndSN_CINDEX(p.Site);
                }

                Remotelb.SEND_CMD_GetProgStatus_CINDEX(p.Site);

                if (AutoRunOp != OperationType.None && !p.StartOp.HasValue && p.ProgrammerReady == DatamanManager.ProgrammerStates.Ready && p.LoadProjectResult == FileResult.Good)
                    DoOperation(AutoRunOp, p);
            }
        }

        //------------------------------------------------------------

        private void LoadProject(DeviceProgressInfo site)
        {
            site.LoadProjectTime = DateTime.Now;

            site.LoadProjectResult = FileResult.None;
            var projectFileName = site.ProjectFileName;

            if (remotelb == null)
                return;

            if (site.Site == 0)
                Remotelb.SEND_CMD_Stop();
            else
                Remotelb.SEND_CMD_Stop_CINDEX(site.Site);

            System.Windows.Forms.Application.DoEvents();
            Thread.Sleep(80);

            if (site.Site == 0)
                Remotelb.SEND_CMD_LoadProject(ref projectFileName);
            else
                Remotelb.SEND_CMD_LoadProject_CINDEX(site.Site, ref projectFileName);
        }

        private static bool inTimer = false;

        protected void OnTimerTick(object state)
        {
            if (inTimer)
                return;

            try
            {
                inTimer = true;

                HasClientsConnected = Remotelb.ServerHasConnectedClient();

                if (MultiSite)
                    Sites.ForEach(p =>
                    {
                        if (Remotelb.GetIfClientAppWithOrdNumExists(p.Site))
                        {
                            if (!p.Connected)
                            {
                                p.Connected = true;
                                Remotelb.SEND_CMD_GetProgrammerSiteNameAndSN_CINDEX(p.Site);
                            }

                            Remotelb.SEND_CMD_GetProgStatus_CINDEX(p.Site);

                            if (AutoRunOp != OperationType.None && !p.StartOp.HasValue && p.ProgrammerReady == DatamanManager.ProgrammerStates.Ready && p.LoadProjectResult == FileResult.Good)
                                DoOperation(AutoRunOp, p);
                        }

                        CheckTimeOut(p);
                    });
                else if (HasClientsConnected)
                {
                    Remotelb.SEND_CMD_GetProgStatus();

                    if (AutoRunOp != OperationType.None && !DefaultSite.StartOp.HasValue && DefaultSite.ProgrammerReady == DatamanManager.ProgrammerStates.Ready && DefaultSite.LoadProjectResult == FileResult.Good)
                        DoOperation(AutoRunOp, DefaultSite);

                    CheckTimeOut();
                }
            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }

            //queryStatus = !queryStatus;
            inTimer = false;
        }

        private void CheckTimeOut()
        {
                if (!DefaultSite.ProgrammerDetected)
                {
                    if ((DateTime.Now - DefaultSite.LoadProjectTime).TotalSeconds > DefaultSite.TimeOut)
                    {
                        DefaultSite.OpResult = DetailedOpResult.TimeOut;
                        OnOperationCompleted(DefaultSite);
                    }
                    return;
                }
                if (!DefaultSite.StartOp.HasValue ||
                  (DateTime.Now - DefaultSite.StartOp.Value).TotalSeconds < DefaultSite.TimeOut)
                    return;

                DefaultSite.OpResult = DetailedOpResult.TimeOut;
            OnOperationCompleted(DefaultSite);
        }

        private void CheckTimeOut(DeviceProgressInfo site)
        {
      
                if (!site.ProgrammerDetected)
                {
                    if ((DateTime.Now - site.LoadProjectTime).TotalSeconds > site.TimeOut)
                    {
                        site.OpResult = DetailedOpResult.TimeOut;
                        OnOperationCompleted(DefaultSite);
                    }
                    return;
                }
                if (!site.StartOp.HasValue ||
                  (site.EndOp.HasValue && site.OpResult != DetailedOpResult.Unavailable) ||
                  (DateTime.Now - site.StartOp.Value).TotalSeconds < site.TimeOut)
                    return;
                         

            site.OpResult = DetailedOpResult.TimeOut;
            OnOperationCompleted(site);
        }

        public bool CheckDeviceConnected()
        {
            return Remotelb.GetProgrammerConnectedStatus();
        }

        public bool CheckDeviceConnected(int numSite)
        {
            return Remotelb.GetProgrammerConnectedStatus_CINDEX(numSite);
        }

        private void Remotelb_OnWriteLineToLogWindow(string line)
        {
            if (Verbose)
                AddLog(line);
        }

        private void Remotelb_onClientDisconnect()
        {
            AddLog("Cliente se ha desconectado");
        }

        private void Remotelb_OnClientConnect()
        {
            AddLog("Cliente se ha conectado");
        }

        private object sync = new object();

        private void Remotelb_OnServerProcess(string cmdLine)
        {
            if (string.IsNullOrEmpty(cmdLine))
                return;

            lock (sync)
            {
                using (var reader = new StringReader(cmdLine))
                {
                    string line = null;

                    while ((line = reader.ReadLine()) != null)
                        ProcessCmdLine(line);
                }
            }
        }

        #region PROCESS CMD LINE
        private void ProcessCmdLine(string cmd)
        {
            var line = cmd.Trim();

            var site = DefaultSite;

            // now try to obtain client index from CMDline (not used in single programming mode)
            // it is stored in "index" variable
            if (cmd.IndexOf(Remotelb.TCP_KEYWORD_CINDEX) >= 0)
            {
                var tmp = cmd.Substring(Remotelb.TCP_KEYWORD_CINDEX.Length).Trim();
                tmp = tmp.Substring(0, tmp.IndexOf("|")).Trim();

                int res = 0;

                if (int.TryParse(tmp, out res))
                {
                    site = Sites.Where(p => p.Site == res).FirstOrDefault();

                    var q = cmd.IndexOf("|");
                    if (q > 0)
                        cmd = cmd.Substring(q + 1).Trim();
                    else
                        cmd = string.Empty;
                }
            }

            if (cmd.IndexOf(Remotelb.TCP_KEYWORD_OPTYPE) < 0 && cmd.IndexOf(Remotelb.TCP_CMD_INFO_LINE) < 0)
                AddLog(cmd);

            if (cmd == Remotelb.TCP_CMD_PROG_IS_BUSY)
                Process_CMD_PROG_IS_BUSY(site);
            else if (cmd.IndexOf(Remotelb.TCP_CMD_OPRESULT) == 0)
                Process_CMD_OPRESULT(cmd, site);                //CMD operation result
            else if (cmd.IndexOf(Remotelb.TCP_KEYWORD_OPTYPE) == 0)
                Process_KEYWORD_OPTYPE(cmd, site);               // CMD operation progress (status)
            else if (cmd.IndexOf(Remotelb.TCP_CMD_LOAD_FILE_PRJ_RESULT) == 0)
                Process_CMD_LOAD_FILE_PRJ_RESULT(cmd, site);     // CMD operation load file/project result
            else if (cmd.IndexOf(Remotelb.TCP_CMD_CLIENT_READY_ANSWER) == 0)
                Process_CMD_CLIENT_READY_ANSWER(cmd, site);      // CMD ready result
            else if (cmd.IndexOf(Remotelb.TCP_CMD_LOG_LINE) == 0)
                Process_CMD_LOG_LINE(cmd, site);                 // CMD log line is comming
            else if (cmd.IndexOf(Remotelb.TCP_CMD_INFO_LINE) == 0)
                Process_CMD_INFO_LINE(cmd, site);                  // CMD info line is comming
            else if (cmd.IndexOf(Remotelb.TCP_CMD_CUR_DEVICE) == 0)
                Process_CMD_CUR_DEVICE_NAME(cmd, site);          // CMD device name is comming
            else if (cmd.IndexOf(Remotelb.TCP_CMD_GETDEVCHECKSUM_RESULT) == 0)
                Process_CMD_GETDEVCHECKSUM_RESULT(cmd, site);    // CMD device (buffer) checksum is comming
            else if (cmd.IndexOf(Remotelb.TCP_CMD_GETDEVSPECIALCHECKSUM_RESULT) == 0)
                Process_CMD_GETDEVSPECIALCHECKSUM_RESULT(cmd, site);    // Checksum especial para algunos micros
            else if (cmd.IndexOf(TCP_CMD_GETDETAILEDOPRESULT_RESULT) == 0)
                Process_CMD_GETDETAILEDOPRESULT_RESULT(cmd, site);
            else if (cmd.IndexOf(TCP_CMD_SERVER_CAN_RUN_ANOTHER_SITE) == 0 && !site.NextSiteStarted)
            {
                site.NextSiteStarted = true;
                StartNextSite();
            }
            else
                AddLog("Recibido del cliente:\n    \"" + cmd + "\"");

            OnStateChanged();
        }

        private void Process_CMD_PROG_IS_BUSY(DeviceProgressInfo site)
        {
            site.DeviceOpResult = Remotelb.TOpResultForRemote.oprNone;

            site.Busy = true;
            site.Progress = 0;

            Remotelb.EnableWriteEventsToLog(false);

            site.ProgrammerReady = ProgrammerStates.Busy;
        }

        private void Process_CMD_OPRESULT(string cmdLine, DeviceProgressInfo site)
        {
            Remotelb.EnableWriteEventsToLog(true);

            var keyword = cmdLine.Substring(Remotelb.TCP_CMD_OPRESULT.Length);

            switch (keyword)
            {
                case Remotelb.TCP_KEYWORD_OPRESULT_GOOD:
                    site.DeviceOpResult = Remotelb.TOpResultForRemote.oprGood;
                    Stats.Good++;
                    break;
                case Remotelb.TCP_KEYWORD_OPRESULT_FAIL:
                    site.DeviceOpResult = Remotelb.TOpResultForRemote.oprFail;
                    Stats.Fail++;
                    break;
                case Remotelb.TCP_KEYWORD_OPRESULT_HWERR:
                    site.DeviceOpResult = Remotelb.TOpResultForRemote.oprHWError;
                    Stats.Fail++;
                    break;
                default:
                    site.DeviceOpResult = Remotelb.TOpResultForRemote.oprNone;
                    break;
            }

            site.Busy = false;
            site.ProgrammerReady = site.ProgrammerDetected ? ProgrammerStates.Ready : ProgrammerStates.NotReady;
            site.EndOp = DateTime.Now;

            var last = site.OpActionsDone.LastOrDefault();
            if (last != null && !last.End.HasValue)
            {
                last.End = site.EndOp;
                if (site.DeviceOpResult != Remotelb.TOpResultForRemote.oprGood)
                    last.Error = true;
            }
        }

        private void Process_KEYWORD_OPTYPE(string cmdLine, DeviceProgressInfo site)
        {
            int value;
            string tmp = cmdLine.Substring(Remotelb.TCP_KEYWORD_OPTYPE.Length + 1);
            int q = tmp.IndexOf(" ");

            if (q > 0)
                tmp = tmp.Substring(0, q).Trim();
            else
                tmp = "unknown";

            if (int.TryParse(tmp, out value))
                site.OpCode = (OperationType)value;
            else
                site.OpCode = OperationType.None;

            q = cmdLine.IndexOf(Remotelb.TCP_KEYWORD_PROGRESS);
            if (q > 0)
            {
                tmp = cmdLine.Substring(q + Remotelb.TCP_KEYWORD_PROGRESS.Length + 1);
                q = tmp.IndexOf(" ");
                if (q > 0)
                    tmp = tmp.Substring(0, q).Trim();
                else
                    tmp = "unknown";

                if (!int.TryParse(tmp, out value))
                    value = 0;

                site.Progress = value;
            }

            q = cmdLine.IndexOf(Remotelb.TCP_CMD_PROGRAMMER_READY_STATUS);
            if (q > 0)
            {
                tmp = cmdLine.Substring(q + Remotelb.TCP_CMD_PROGRAMMER_READY_STATUS.Length);
                q = tmp.IndexOf(" ");
                if (q > 0)
                    tmp = tmp.Substring(0, q - 1).Trim();
                else
                    tmp = tmp.Trim();

                if (tmp == Remotelb.TCP_KEY_PROGRAMMER_READY)
                {
                    site.ProgrammerDetected = true;
                    if (site.ProgrammerReady != ProgrammerStates.Busy)
                        site.ProgrammerReady = ProgrammerStates.Ready;
                }
                else
                {
                    site.ProgrammerDetected = false;
                    site.ProgrammerReady = ProgrammerStates.NotReady;
                }
            }

            if (!IsOperationActive(site.OpCode) && !site.Busy)
                site.InfoLine = string.Empty;
        }

        private void Process_CMD_LOAD_FILE_PRJ_RESULT(string cmdLine, DeviceProgressInfo site)
        {
            var res = cmdLine.Substring(Remotelb.TCP_CMD_LOAD_FILE_PRJ_RESULT.Length);

            switch (res)
            {
                case Remotelb.TCP_FILE_LOAD_GOOD:
                    site.LoadProjectResult = FileResult.Good;
                    break;
                case Remotelb.TCP_FILE_LOAD_CANCELLED:
                    site.LoadProjectResult = FileResult.Cancelled;
                    break;
                default:
                    site.LoadProjectResult = FileResult.Error;
                    break;
            }
        }

        private void Process_CMD_CLIENT_READY_ANSWER(string cmdLine, DeviceProgressInfo site)
        {
            Remotelb.EnableWriteEventsToLog(true);

            var res = cmdLine.Substring(Remotelb.TCP_CMD_CLIENT_READY_ANSWER.Length);
            if (res == Remotelb.TCP_KEY_CLIENT_READY_YES)
            {
                Process_CMD_LOG_LINE(string.Format("{0} >> {1:yy:MM:ss}{2}Program is connected and waiting for programmer.", Remotelb.TCP_CMD_LOG_LINE, DateTime.Now, Remotelb.LOG_LINES_DELIMITER), site);
                HideDatamanWindow();
            }
            else
            {
                site.ProgrammerReady = ProgrammerStates.NotReady;
                Process_CMD_LOG_LINE(string.Format("{0} >> {1:yy:MM:ss}{2}Programmer is connected but not ready.", Remotelb.TCP_CMD_LOG_LINE, DateTime.Now, Remotelb.LOG_LINES_DELIMITER), site);
            }
        }

        private void Process_CMD_LOG_LINE(string cmdLine, DeviceProgressInfo site)
        {
            cmdLine = cmdLine.Substring(Remotelb.TCP_CMD_LOG_LINE.Length)
                .Replace(Remotelb.LOG_LINES_DELIMITER, Environment.NewLine)
                .Trim();

            AddLog(cmdLine);

            if (site.ProgrammerReady == ProgrammerStates.Ready &&
                site.LoadProjectResult == FileResult.Good && 
                (cmdLine.ToUpper().IndexOf(CREDIT_BOX_WARNING) >= 0 || cmdLine.ToUpper().IndexOf(CREDIT_BOX_ERROR) >= 0))
            {
                site.OpResult = DetailedOpResult.NoCreditBox;
                OnOperationCompleted(site);
            }

            Remotelb.EnableWriteEventsToLog(true);
        }

        private void Process_CMD_INFO_LINE(string cmdLine, DeviceProgressInfo site)
        {
            var text = cmdLine.Substring(Remotelb.TCP_CMD_INFO_LINE.Length).Trim();

            site.InfoLine = text;

            if (string.IsNullOrEmpty(text))
                return;

            if (site.Site > 0)
                logger.InfoFormat("{0} (#{1})", text, site.Site);

            OpActionsInfo action = null;

            if (text.StartsWith("Erasing ") && !site.OpActionsDone.Any(p => p.OpCode == OperationType.EraseDevice))
                action = new OpActionsInfo { OpCode = OperationType.EraseDevice, Start = DateTime.Now };

            if (text.StartsWith("Device blank checking ") && !site.OpActionsDone.Any(p => p.OpCode == OperationType.BlankCheck))
                action = new OpActionsInfo { OpCode = OperationType.BlankCheck, Start = DateTime.Now };

            if (text.StartsWith("Programming ") && !site.OpActionsDone.Any(p => p.OpCode == OperationType.ProgramDevice))
                action = new OpActionsInfo { OpCode = OperationType.ProgramDevice, Start = DateTime.Now };

            if (text.StartsWith("Verifying ") && 
                (site.LastOperation != OperationType.ProgramDevice || site.OpActionsDone.Any(p => p.OpCode == OperationType.ProgramDevice)) &&
                !site.OpActionsDone.Any(p => p.OpCode == OperationType.VerifyDevice))
                action = new OpActionsInfo { OpCode = OperationType.VerifyDevice, Start = DateTime.Now };

            if (action != null)
            {
                var last = site.OpActionsDone.LastOrDefault();
                if (last != null)
                    last.End = action.Start;

                site.OpActionsDone.Add(action);
            }
        }

        private void Process_CMD_CUR_DEVICE_NAME(string cmdLine, DeviceProgressInfo site)
        {
            site.SelectedDevice = cmdLine.Substring(Remotelb.TCP_CMD_CUR_DEVICE.Length).Trim();
        }

        private void Process_CMD_GETDEVCHECKSUM_RESULT(string cmdLine, DeviceProgressInfo site)
        {
            site.Checksum = cmdLine.Trim().ToUpper().Substring(Remotelb.TCP_CMD_GETDEVCHECKSUM_RESULT.Length);
        }

        private void Process_CMD_GETDEVSPECIALCHECKSUM_RESULT(string cmdLine, DeviceProgressInfo site)
        {
            site.Checksum = cmdLine.Trim().ToUpper().Substring(Remotelb.TCP_CMD_GETDEVSPECIALCHECKSUM_RESULT.Length);
        }

        private void Process_CMD_GETDETAILEDOPRESULT_RESULT(string cmdLine, DeviceProgressInfo site)
        {
            var keyword = cmdLine.Substring(TCP_CMD_GETDETAILEDOPRESULT_RESULT.Length);

            int result = -1;
            if (int.TryParse(keyword, out result))
                site.OpResult = (DetailedOpResult)result;
            else
                site.OpResult = DetailedOpResult.OtherFailure;

            OnOperationCompleted(site);
        }

        #endregion

        public void Disconnect()
        {
            try
            {
                if (MultiSite)
                {
                    foreach (var site in Sites)
                        if (Remotelb.GetIfClientAppWithOrdNumExists(site.Site))
                        {
                            if (site.Busy)
                                //Se debe cancelar la operación en curso
                                continue;
                            else
                            {
                                Remotelb.SEND_CMD_Stop_CINDEX(site.Site);
                                Thread.Sleep(90);
                                System.Windows.Forms.Application.DoEvents();
                                Thread.Sleep(90);
                                Remotelb.SEND_CMD_Stop_CINDEX(site.Site);
                                Thread.Sleep(90);
                                System.Windows.Forms.Application.DoEvents();
                                Thread.Sleep(150);
                                Remotelb.SEND_CMD_CloseApp_CINDEX(site.Site);
                                Thread.Sleep(80);
                                System.Windows.Forms.Application.DoEvents();
                                Thread.Sleep(80);
                            }
                        }
                }
                else if (Remotelb.ServerHasConnectedClient())
                {
                    if (DefaultSite.Busy)
                    //Se debe cancelar la operación en curso
                        return;
                    else
                    {
                        Remotelb.SEND_CMD_Stop();
                        Thread.Sleep(90);
                        System.Windows.Forms.Application.DoEvents();
                        Thread.Sleep(90);
                        Remotelb.SEND_CMD_Stop();
                        Thread.Sleep(90);
                        System.Windows.Forms.Application.DoEvents();
                        Thread.Sleep(150);
                        Remotelb.SEND_CMD_CloseApp();
                        Thread.Sleep(80);
                        System.Windows.Forms.Application.DoEvents();
                        Thread.Sleep(80);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public void Dispose()
        {
            inTimer = true;

            if (timer != null)
                timer.Dispose();

            timer = null;

            Disconnect();

            try
            {
                Remotelb.MakeClientServertCloseConnectionAndFree();
                if (!MultiSite)
                    Remotelb.SEND_CMD_CloseApp();
            }
            catch { }
        }

        protected void AddLog(string line)
        {
            //if (Log != null && !string.IsNullOrEmpty(line))
            //    Log(this, line);

            Log += line + Environment.NewLine;
            logger.InfoFormat("DATAMAN -> {0}", line);
        }

        protected void OnStateChanged()
        {
            if (StateChanged != null)
                StateChanged(this, EventArgs.Empty);
        }

        public bool IsOperationActive(DatamanManager.OperationType opcode)
        {
            return opcode == DatamanManager.OperationType.BlankCheck ||
                opcode == DatamanManager.OperationType.EraseDevice ||
                opcode == DatamanManager.OperationType.ReadDevice ||
                opcode == DatamanManager.OperationType.VerifyDevice ||
                opcode == DatamanManager.OperationType.ProgramDevice;
        }

        public void Erase()
        {
            DoOperation(OperationType.EraseDevice);
        }

        public void Flash()
        {
            DoOperation(OperationType.ProgramDevice);
        }

        public void Verify()
        {
            DoOperation(OperationType.VerifyDevice);
        }

        public void Read()
        {
            DoOperation(OperationType.ReadDevice);
        }

        public void HideDatamanWindow()
        {
            Remotelb.SEND_CMD_HideMainForm();
        }

        public void HideDatamanWindow(int site)
        {
            if (site == 0)
                Remotelb.SEND_CMD_HideMainForm();
            else
                Remotelb.SEND_CMD_HideMainForm_CINDEX(site);
        }

        private void DoOperation(OperationType operation)
        {
            Sites.ForEach(p => DoOperation(operation, p));
        }

        private void DoOperation(OperationType operation, DeviceProgressInfo site)
        {
            HideDatamanWindow(site.Site);

            if (site.Site == 0 && !Remotelb.ServerHasConnectedClient())
                throw new Exception("No hay ningún cliente conectado!");

            if (site.Site > 0 && !Remotelb.GetIfClientAppWithOrdNumExists(site.Site))
                throw new Exception(string.Format("No hay ningún cliente conectado para el programador nº {0}!", site.Site));

            if (site.LoadProjectResult != FileResult.Good)
                throw new Exception("No hay ningún proyecto cargado!");

            if (operation != site.LastOperation)
            {
                if (site.LastOperation != OperationType.None)
                {
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_Stop();
                    else
                        Remotelb.SEND_CMD_Stop_CINDEX(site.Site);
                }

                site.LastOperation = operation;
            }

            site.StartOp = DateTime.Now;
            site.EndOp = null;
            site.OpActionsDone.Clear();

            switch (operation)
            {
                case OperationType.None:
                    break;
                case OperationType.RunApp:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_BringToFront();
                    else
                        Remotelb.SEND_CMD_BringToFront_CINDEX(site.Site);
                    break;
                case OperationType.BlankCheck:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_BlankCheckDevice();
                    else
                        Remotelb.SEND_CMD_BlankCheckDevice_CINDEX(site.Site);
                    break;
                case OperationType.ReadDevice:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_ReadDevice();
                    else
                        Remotelb.SEND_CMD_ReadDevice_CINDEX(site.Site);
                    break;
                case OperationType.VerifyDevice:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_VerifyDevice();
                    else
                        Remotelb.SEND_CMD_VerifyDevice_CINDEX(site.Site);
                    break;
                case OperationType.ProgramDevice:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_ProgramDevice();
                    else
                        Remotelb.SEND_CMD_ProgramDevice_CINDEX(site.Site);
                    break;
                case OperationType.EraseDevice:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_EraseDevice();
                    else
                        Remotelb.SEND_CMD_EraseDevice_CINDEX(site.Site);
                    break;
                case OperationType.Stop:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_Stop();
                    else
                        Remotelb.SEND_CMD_Stop_CINDEX(site.Site);
                    break;
                default:
                    if (site.Site == 0)
                        Remotelb.SEND_CMD_BringToFront(); // unknown operation (do Bring to front only)
                    else
                        Remotelb.SEND_CMD_BringToFront_CINDEX(site.Site);
                    break;
            }
        }

        private void OnOperationCompleted(DeviceProgressInfo site)
        {
            if (HasFinishedByError() || HasOperationsFinished())
                if (OperationCompleted != null)
                    OperationCompleted(this, EventArgs.Empty);
        }

        public bool IsProgrammerConnected()
        {
            return Remotelb.GetProgrammerConnectedStatus();
        }

        public bool HasOperationsFinished()
        {
            return Sites.All(p => p.EndOp.HasValue && p.OpResult != DetailedOpResult.Unavailable);
        }

        public bool HasFinishedByError()
        {
            return Sites.Any(p => p.OpResult == DetailedOpResult.TimeOut || p.OpResult == DetailedOpResult.NoCreditBox);
        }
    }

    public class PositionSite
    {
        public int position;
        public int instance;
        public string serialNumber;
        public string binaryFile;
        public string ProjectFile;
        public int timeoutGrabador;
    };
}
