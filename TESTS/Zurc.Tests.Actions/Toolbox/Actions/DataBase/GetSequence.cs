﻿using Dezac.Core.Exceptions;
using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;
using Zurc.Data;
using Dezac.Tests.Actions.Kernel;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(GetSequenceDescription))]
    public class GetSequence : ActionBase
    {
        [Description("Nombre de la secuencia")]
        public string SequenceName { get; set; }
        [Description("Añadir prefijo al valor de la secuencia")]
        public string Prefix { get; set; }
        [Description("Compensar con n 0 delante")]
        public int? ZeroPad { get; set; }

        [Description("Nombre de la variable donde guardaremos el valor de la secuencia, si no se informa se usa el de la secuencia")]
        public string VariableName { get; set; }
        [Description("Flag para activar la grabacion del valor y sus unidades como resultados del test")]
        public bool AddToResult { get; set; }
        [Description("Si se activa intenta buscar el valor en tests anteriores de la misma matricual")]
        public bool ReusePreviousTestValue { get; set; }

        public override void Execute()
        {
            InternalExecute();
        }

        private void InternalExecute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            var sequenceName = (VariablesTools.ResolveValue(SequenceName) ?? string.Empty).ToString();
            string nextval = null;

            if (string.IsNullOrEmpty(sequenceName))
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("FALTA INDICAR EL NOMBRE DE LA SECUENCIA").Throw();

            var variableName = string.IsNullOrEmpty(VariableName) ? sequenceName : VariableName;

            using (var svc = new ZurcService())
            {
                if (ReusePreviousTestValue && data != null && data.TestInfo != null)
                {
                    if (string.IsNullOrEmpty(variableName))
                        TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("FALTA INDICAR EL VALOR DE LA VARIABLE A REUSAR").Throw();

                    if (data.TestInfo.NumBastidor.GetValueOrDefault() > 0)
                        nextval = svc.GetPreviousTestValue(data.NumProducto, data.Version, (long)data.TestInfo.NumBastidor.Value, data.IdFase, variableName);

                    Logger.Info($"Buscando secuencia anterio de {data.NumProducto}/{data.Version} {data.TestInfo.NumBastidor} {data.IdFase} {variableName} -> {nextval}");
                }

                if (string.IsNullOrEmpty(nextval))
                {
                    nextval = svc.GetNextVal(sequenceName).ToString();

                    if (ZeroPad.GetValueOrDefault() > 0)
                        nextval = nextval.PadLeft(ZeroPad.GetValueOrDefault(), '0');

                    if (!string.IsNullOrEmpty(Prefix))
                        nextval = string.Format("{0}{1}", VariablesTools.ResolveValue(Prefix), nextval);
                }

                SetVariable(variableName, nextval);
                
                if (AddToResult)
                {
                    if (data != null)
                        data.Resultados.Set(variableName, nextval, Dezac.Tests.Model.ParamUnidad.SinUnidad);
                    else
                        context.ResultList.Add($"{variableName} = {nextval}");
                }
            }

            Logger.InfoFormat("Sequence Value {0} -> {1} ({2})", sequenceName, nextval, variableName);
        }
    }

    public class GetSequenceDescription : ActionDescription
    {
        public override string Description { get { return "Action to get sequence value"; } }
        public override string Name { get { return "GetSequence"; } }
        public override string Category { get { return "ZurcTest/Trazabilidad"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
