﻿using Dezac.Core.Utility;
using System;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class CVMMINI_Cirbus : DeviceBase
    {
        public CirbusDevice Cirbus { get; internal set; }

        #region Instanciacion y destruccion Device

        public CVMMINI_Cirbus()
        {
        }

        public CVMMINI_Cirbus(int port)
        {
            SetPort(port, 1, 9600);
        }

        public CVMMINI_Cirbus(int port, byte periferico, int baudrate)
        {
            SetPort(port, periferico, baudrate);
        }

        public void SetPort(int port, byte periferico, int baudrate)
        {
            Cirbus = new CirbusDeviceSerialPort(port, baudrate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Cirbus.PerifericNumber = periferico;
            Cirbus.TimeOut = 2000;
        }

        public override void Dispose()
        {
            Cirbus.Dispose();
        }

        #endregion


        #region Operaciones Modbus de lectura
       
        public string ReadVersion()
        {
            LogMethod();
            return Cirbus.Read(READ_VERSION);
        }

        public bool ReadFlashFormat()
        {
            LogMethod();

            var response = Cirbus.Read(READ_FLASH_FORMAT);
            var split = response.Split(' ');

            if (split[1] == "1")
                return true;
            if (split[1] == "6")
                return false;
            throw new Exception("Error al leer el formato de la flash. Valores esperados: 1 o 6. Recibido: " + split[1]); 
        }

        public string ReadMAC()
        {
            LogMethod();

            var response = Cirbus.Read(READ_MAC);
            return response;
        }


        #endregion

        #region Operaciones de escritura 

        public void WriteDisableComunications()
        {
            LogMethod();
            Cirbus.Write(WRITE_DISABLE_COM);
        }

        public void WriteToFormatFlash()
        {
            LogMethod();
            var timeout = Cirbus.TimeOut;
            Cirbus.TimeOut = 5000;
            Cirbus.Write(WRITE_FORMAT_FLASH);
            Cirbus.TimeOut = timeout;  
        }
 
        public void WriteExportXML()
        {
            LogMethod();
            Cirbus.Write(WRITE_EXPORT_XML);
        }
        public void WriteMAC(string mac)
        {
            LogMethod();
            Cirbus.Write(WRITE_MAC, mac);
        }

        #endregion

        #region Structures
        
        #endregion

        #region Vector de Wardware

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele2 { get; set; }
            public bool Rele1 { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Cuadrantes { get; set; }
            public bool Shunt { get; set; }
            public bool CalculoHarmonicos { get; set; }
            public bool Johnson { get; set; }
            public bool _1Fase { get; set; }

            public string Rele1String { get { return Rele1 == true ? "SI" : "NO"; } }
            public string Rele2String { get { return Rele2 == true ? "SI" : "NO"; } }
            public string Rs232String { get { return Rs232 == true ? "SI" : "NO"; } }
            public string ComunicationString { get { return Comunication == true ? "SI" : "NO"; } }
            public string ShuntString { get { return Shunt == true ? "SI" : "NO"; } }
            public string CalculoHarmonicosString { get { return CalculoHarmonicos == true ? "SI" : "NO"; } }
            public string CuadrantesString { get { return Cuadrantes == true ? "2_CUADRANTES" : "4_CUADRANTES"; } }
            public string JohnsonString { get { return Johnson == true ? "SI" : "NO"; } }
            public string _1FaseString { get { return _1Fase == true ? "1" : "3"; } }

            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value.ToUpper().Trim();

                    if (powerSupplyString.Contains("230VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._230Vac;
                        return;
                    }
                    else if(powerSupplyString.Contains("400VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._400Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("480VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._480Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("80-265VAC"))
                    {
                        PowerSupply = VectorHardwareSupply._80_265Vac;
                        return;
                    }
                    else if (powerSupplyString.Contains("21-51VDC"))
                    {
                        PowerSupply = VectorHardwareSupply._21_51Vdc;
                        return;
                    }
                    else
                        throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware, Valores permitidos (230VAC, 400VAC, 480VAC, 80-265VAC o 21-51VDC)");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else
                    {
                        if (inputVoltageString.Contains("110"))
                        {
                            InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                            return;
                        }
                        else
                        {
                            if (inputVoltageString.Contains("500"))
                            {
                                InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                                return;
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("5"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                        return;
                    }
                    else
                    {
                        if (inputCurrentString.Contains("2"))
                        {
                            InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                            return;
                        }
                        else
                        {
                            if (inputCurrentString.Contains("1"))
                            {
                                InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                                return;
                            }
                            else
                            {
                                if (inputCurrentString.ToUpper().Contains("ITF"))
                                {
                                    InputCurrent = VectorHardwareCurrentInputs.ITFext;
                                    return;
                                }
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString == "CVM_MINI")
                        Modelo = VectorHardwareModelo.CVM_MINI;
                    else if (modeloString == "CVM_MINI_ETH")
                        Modelo = VectorHardwareModelo.CVM_MINI_ETH;
                    else       
                      throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele1 = (vectorHardware[0] & 0x10) == 0x10;
                Rele2 = (vectorHardware[0] & 0x20) == 0x20;
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                Johnson = (vectorHardware[8] & 0x02) == 0x02;
                _1Fase = (vectorHardware[10] & 0x04) == 0x04;
                Cuadrantes = (vectorHardware[10] & 0x02) == 0x02;
                CalculoHarmonicos = (vectorHardware[10] & 0x01) == 0x01;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele1;
                vector[5] = Rele2;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;
                vector[11] = Shunt;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[65] = Johnson;

                vector[81] = Cuadrantes;
                vector[80] = CalculoHarmonicos;
                vector[82] = _1Fase;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2,
            ITFext = 7,
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _21_51Vdc = 3,
            _80_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            CVM_MINI = 1,
            CVM_MINI_ETH = 2         
        }

        #endregion

        #region Enumerados y Diccionarios

        private const string READ_VERSION = "VER";
        private const string READ_FLASH_FORMAT = "FAT I";
        private const string WRITE_FORMAT_FLASH = "FAT FORMAT";
        private const string READ_IP = "IPR";
        private const string WRITE_IP = "IPW {0} {1} {2} {3}";
        private const string WRITE_EXPORT_XML = "XML EXP NET";
        private const string READ_MAC = "MAC";
        private const string WRITE_MAC = "MAC WRITE {0}";
        private const string WRITE_DISABLE_COM = "UAR D";

        #endregion
    }
}
