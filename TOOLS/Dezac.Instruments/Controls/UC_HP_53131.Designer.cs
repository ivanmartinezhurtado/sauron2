﻿namespace Dezac.Instruments.Controls
{
    partial class UC_HP_53131
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnReset = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lbl_Vers_tester = new System.Windows.Forms.Label();
            this.lbl_Mesur_Mult = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.chkCh1 = new System.Windows.Forms.CheckBox();
            this.ValorLevel1 = new System.Windows.Forms.TextBox();
            this.Label81 = new System.Windows.Forms.Label();
            this.Filtro1 = new System.Windows.Forms.ComboBox();
            this.Label83 = new System.Windows.Forms.Label();
            this.Atenua1 = new System.Windows.Forms.ComboBox();
            this.Label84 = new System.Windows.Forms.Label();
            this.Acoplo1 = new System.Windows.Forms.ComboBox();
            this.Label85 = new System.Windows.Forms.Label();
            this.Imped1 = new System.Windows.Forms.ComboBox();
            this.Label80 = new System.Windows.Forms.Label();
            this.Label75 = new System.Windows.Forms.Label();
            this.Level1 = new System.Windows.Forms.ComboBox();
            this.chkCh2 = new System.Windows.Forms.CheckBox();
            this.ValorLevel2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Filtro2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Atenua2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Acoplo2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Imped2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Level2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Sens2 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Pend2 = new System.Windows.Forms.ComboBox();
            this.Label73 = new System.Windows.Forms.Label();
            this.Sens1 = new System.Windows.Forms.ComboBox();
            this.Label71 = new System.Windows.Forms.Label();
            this.Pend1 = new System.Windows.Forms.ComboBox();
            this.chkGate = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.StopGate = new System.Windows.Forms.ComboBox();
            this.Label88 = new System.Windows.Forms.Label();
            this.ValorGateStop = new System.Windows.Forms.TextBox();
            this.Label92 = new System.Windows.Forms.Label();
            this.StopPend = new System.Windows.Forms.ComboBox();
            this.Label89 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.StartPend = new System.Windows.Forms.ComboBox();
            this.Label87 = new System.Windows.Forms.Label();
            this.StartGate = new System.Windows.Forms.ComboBox();
            this.Label86 = new System.Windows.Forms.Label();
            this.checStats = new System.Windows.Forms.CheckBox();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.RadioButton2 = new System.Windows.Forms.RadioButton();
            this.RadioButton1 = new System.Windows.Forms.RadioButton();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.RadioButton3 = new System.Windows.Forms.RadioButton();
            this.RadioButton4 = new System.Windows.Forms.RadioButton();
            this.Label94 = new System.Windows.Forms.Label();
            this.ValorN = new System.Windows.Forms.TextBox();
            this.Label93 = new System.Windows.Forms.Label();
            this.Meas = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.chkFRECCH1 = new System.Windows.Forms.CheckBox();
            this.chkPERCH1 = new System.Windows.Forms.CheckBox();
            this.chkTINT = new System.Windows.Forms.CheckBox();
            this.chkFRECCH2 = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Channel1 = new System.Windows.Forms.TabPage();
            this.Channel2 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Channel1.SuspendLayout();
            this.Channel2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.DimGray;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(7, 36);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(61, 25);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.BackColor = System.Drawing.Color.Black;
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.ForeColor = System.Drawing.Color.PowderBlue;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(361, 11);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(72, 21);
            this.cmb_Port.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lbl_Vers_tester
            // 
            this.lbl_Vers_tester.AutoSize = true;
            this.lbl_Vers_tester.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Vers_tester.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Vers_tester.ForeColor = System.Drawing.Color.Black;
            this.lbl_Vers_tester.Location = new System.Drawing.Point(135, 10);
            this.lbl_Vers_tester.Name = "lbl_Vers_tester";
            this.lbl_Vers_tester.Size = new System.Drawing.Size(51, 13);
            this.lbl_Vers_tester.TabIndex = 154;
            this.lbl_Vers_tester.Text = "Version:";
            this.lbl_Vers_tester.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_Mesur_Mult
            // 
            this.lbl_Mesur_Mult.BackColor = System.Drawing.Color.Black;
            this.lbl_Mesur_Mult.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbl_Mesur_Mult.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Mesur_Mult.ForeColor = System.Drawing.Color.PowderBlue;
            this.lbl_Mesur_Mult.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_Mesur_Mult.Location = new System.Drawing.Point(12, 39);
            this.lbl_Mesur_Mult.Name = "lbl_Mesur_Mult";
            this.lbl_Mesur_Mult.Size = new System.Drawing.Size(423, 48);
            this.lbl_Mesur_Mult.TabIndex = 132;
            this.lbl_Mesur_Mult.Text = "0.0000000,00";
            this.lbl_Mesur_Mult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.DimGray;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(7, 9);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(61, 25);
            this.btnClear.TabIndex = 170;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chkCh1
            // 
            this.chkCh1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkCh1.AutoSize = true;
            this.chkCh1.BackColor = System.Drawing.Color.DimGray;
            this.chkCh1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCh1.ForeColor = System.Drawing.Color.White;
            this.chkCh1.Location = new System.Drawing.Point(384, 41);
            this.chkCh1.Name = "chkCh1";
            this.chkCh1.Size = new System.Drawing.Size(36, 25);
            this.chkCh1.TabIndex = 22;
            this.chkCh1.Text = "ON";
            this.chkCh1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCh1.UseVisualStyleBackColor = false;
            this.chkCh1.CheckedChanged += new System.EventHandler(this.chkCh1_CheckedChanged);
            // 
            // ValorLevel1
            // 
            this.ValorLevel1.Location = new System.Drawing.Point(303, 26);
            this.ValorLevel1.Name = "ValorLevel1";
            this.ValorLevel1.Size = new System.Drawing.Size(68, 20);
            this.ValorLevel1.TabIndex = 17;
            // 
            // Label81
            // 
            this.Label81.AutoSize = true;
            this.Label81.BackColor = System.Drawing.Color.Transparent;
            this.Label81.Location = new System.Drawing.Point(323, 52);
            this.Label81.Name = "Label81";
            this.Label81.Size = new System.Drawing.Size(29, 13);
            this.Label81.TabIndex = 15;
            this.Label81.Text = "Filtro";
            // 
            // Filtro1
            // 
            this.Filtro1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filtro1.FormattingEnabled = true;
            this.Filtro1.Items.AddRange(new object[] {
            "ON",
            "OFF"});
            this.Filtro1.Location = new System.Drawing.Point(303, 67);
            this.Filtro1.Name = "Filtro1";
            this.Filtro1.Size = new System.Drawing.Size(68, 21);
            this.Filtro1.TabIndex = 14;
            // 
            // Label83
            // 
            this.Label83.AutoSize = true;
            this.Label83.BackColor = System.Drawing.Color.Transparent;
            this.Label83.Location = new System.Drawing.Point(211, 52);
            this.Label83.Name = "Label83";
            this.Label83.Size = new System.Drawing.Size(61, 13);
            this.Label83.TabIndex = 13;
            this.Label83.Text = "Atenuación";
            // 
            // Atenua1
            // 
            this.Atenua1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Atenua1.FormattingEnabled = true;
            this.Atenua1.Items.AddRange(new object[] {
            "x1",
            "x10"});
            this.Atenua1.Location = new System.Drawing.Point(207, 67);
            this.Atenua1.Name = "Atenua1";
            this.Atenua1.Size = new System.Drawing.Size(68, 21);
            this.Atenua1.TabIndex = 12;
            // 
            // Label84
            // 
            this.Label84.AutoSize = true;
            this.Label84.BackColor = System.Drawing.Color.Transparent;
            this.Label84.Location = new System.Drawing.Point(124, 52);
            this.Label84.Name = "Label84";
            this.Label84.Size = new System.Drawing.Size(43, 13);
            this.Label84.TabIndex = 11;
            this.Label84.Text = "Acoplo:";
            // 
            // Acoplo1
            // 
            this.Acoplo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Acoplo1.FormattingEnabled = true;
            this.Acoplo1.Items.AddRange(new object[] {
            "AC",
            "DC"});
            this.Acoplo1.Location = new System.Drawing.Point(111, 67);
            this.Acoplo1.Name = "Acoplo1";
            this.Acoplo1.Size = new System.Drawing.Size(68, 21);
            this.Acoplo1.TabIndex = 10;
            // 
            // Label85
            // 
            this.Label85.AutoSize = true;
            this.Label85.BackColor = System.Drawing.Color.Transparent;
            this.Label85.Location = new System.Drawing.Point(17, 52);
            this.Label85.Name = "Label85";
            this.Label85.Size = new System.Drawing.Size(65, 13);
            this.Label85.TabIndex = 9;
            this.Label85.Text = "Impedancia:";
            // 
            // Imped1
            // 
            this.Imped1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Imped1.FormattingEnabled = true;
            this.Imped1.Items.AddRange(new object[] {
            "50",
            "1M"});
            this.Imped1.Location = new System.Drawing.Point(15, 67);
            this.Imped1.Name = "Imped1";
            this.Imped1.Size = new System.Drawing.Size(68, 21);
            this.Imped1.TabIndex = 8;
            // 
            // Label80
            // 
            this.Label80.AutoSize = true;
            this.Label80.BackColor = System.Drawing.Color.Transparent;
            this.Label80.Location = new System.Drawing.Point(304, 10);
            this.Label80.Name = "Label80";
            this.Label80.Size = new System.Drawing.Size(66, 13);
            this.Label80.TabIndex = 7;
            this.Label80.Text = "Valor Medio:";
            // 
            // Label75
            // 
            this.Label75.AutoSize = true;
            this.Label75.BackColor = System.Drawing.Color.Transparent;
            this.Label75.Location = new System.Drawing.Point(224, 10);
            this.Label75.Name = "Label75";
            this.Label75.Size = new System.Drawing.Size(34, 13);
            this.Label75.TabIndex = 5;
            this.Label75.Text = "Nivel:";
            // 
            // Level1
            // 
            this.Level1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Level1.FormattingEnabled = true;
            this.Level1.Items.AddRange(new object[] {
            "AUTO ON",
            "LEVEL V",
            "LEVEL %"});
            this.Level1.Location = new System.Drawing.Point(207, 26);
            this.Level1.Name = "Level1";
            this.Level1.Size = new System.Drawing.Size(68, 21);
            this.Level1.TabIndex = 4;
            // 
            // chkCh2
            // 
            this.chkCh2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkCh2.AutoSize = true;
            this.chkCh2.BackColor = System.Drawing.Color.DimGray;
            this.chkCh2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCh2.ForeColor = System.Drawing.Color.White;
            this.chkCh2.Location = new System.Drawing.Point(392, 43);
            this.chkCh2.Name = "chkCh2";
            this.chkCh2.Size = new System.Drawing.Size(36, 25);
            this.chkCh2.TabIndex = 22;
            this.chkCh2.Text = "ON";
            this.chkCh2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCh2.UseVisualStyleBackColor = false;
            this.chkCh2.CheckedChanged += new System.EventHandler(this.chkCh2_CheckedChanged);
            // 
            // ValorLevel2
            // 
            this.ValorLevel2.Location = new System.Drawing.Point(311, 26);
            this.ValorLevel2.Name = "ValorLevel2";
            this.ValorLevel2.Size = new System.Drawing.Size(68, 20);
            this.ValorLevel2.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(331, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Filtro";
            // 
            // Filtro2
            // 
            this.Filtro2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filtro2.FormattingEnabled = true;
            this.Filtro2.Items.AddRange(new object[] {
            "ON",
            "OFF"});
            this.Filtro2.Location = new System.Drawing.Point(311, 67);
            this.Filtro2.Name = "Filtro2";
            this.Filtro2.Size = new System.Drawing.Size(68, 21);
            this.Filtro2.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(215, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Atenuación";
            // 
            // Atenua2
            // 
            this.Atenua2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Atenua2.FormattingEnabled = true;
            this.Atenua2.Items.AddRange(new object[] {
            "x1",
            "x10"});
            this.Atenua2.Location = new System.Drawing.Point(211, 67);
            this.Atenua2.Name = "Atenua2";
            this.Atenua2.Size = new System.Drawing.Size(68, 21);
            this.Atenua2.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(125, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Acoplo:";
            // 
            // Acoplo2
            // 
            this.Acoplo2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Acoplo2.FormattingEnabled = true;
            this.Acoplo2.Items.AddRange(new object[] {
            "AC",
            "DC"});
            this.Acoplo2.Location = new System.Drawing.Point(112, 67);
            this.Acoplo2.Name = "Acoplo2";
            this.Acoplo2.Size = new System.Drawing.Size(68, 21);
            this.Acoplo2.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(20, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Impedancia:";
            // 
            // Imped2
            // 
            this.Imped2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Imped2.FormattingEnabled = true;
            this.Imped2.Items.AddRange(new object[] {
            "50",
            "1M"});
            this.Imped2.Location = new System.Drawing.Point(18, 67);
            this.Imped2.Name = "Imped2";
            this.Imped2.Size = new System.Drawing.Size(68, 21);
            this.Imped2.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(312, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Valor Medio:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(228, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Nivel:";
            // 
            // Level2
            // 
            this.Level2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Level2.FormattingEnabled = true;
            this.Level2.Items.AddRange(new object[] {
            "AUTO ON",
            "LEVEL V",
            "LEVEL %"});
            this.Level2.Location = new System.Drawing.Point(211, 26);
            this.Level2.Name = "Level2";
            this.Level2.Size = new System.Drawing.Size(68, 21);
            this.Level2.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(113, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Sensibilidad:";
            // 
            // Sens2
            // 
            this.Sens2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Sens2.FormattingEnabled = true;
            this.Sens2.Items.AddRange(new object[] {
            "HIGH",
            "MED",
            "LOW"});
            this.Sens2.Location = new System.Drawing.Point(112, 26);
            this.Sens2.Name = "Sens2";
            this.Sens2.Size = new System.Drawing.Size(68, 21);
            this.Sens2.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(23, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Pendiente:";
            // 
            // Pend2
            // 
            this.Pend2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Pend2.FormattingEnabled = true;
            this.Pend2.Items.AddRange(new object[] {
            "POSITIVO",
            "NEGATIVO"});
            this.Pend2.Location = new System.Drawing.Point(18, 26);
            this.Pend2.Name = "Pend2";
            this.Pend2.Size = new System.Drawing.Size(68, 21);
            this.Pend2.TabIndex = 18;
            // 
            // Label73
            // 
            this.Label73.AutoSize = true;
            this.Label73.BackColor = System.Drawing.Color.Transparent;
            this.Label73.Location = new System.Drawing.Point(112, 9);
            this.Label73.Name = "Label73";
            this.Label73.Size = new System.Drawing.Size(66, 13);
            this.Label73.TabIndex = 3;
            this.Label73.Text = "Sensibilidad:";
            // 
            // Sens1
            // 
            this.Sens1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Sens1.FormattingEnabled = true;
            this.Sens1.Items.AddRange(new object[] {
            "HIGH",
            "MED",
            "LOW"});
            this.Sens1.Location = new System.Drawing.Point(111, 26);
            this.Sens1.Name = "Sens1";
            this.Sens1.Size = new System.Drawing.Size(68, 21);
            this.Sens1.TabIndex = 2;
            // 
            // Label71
            // 
            this.Label71.AutoSize = true;
            this.Label71.BackColor = System.Drawing.Color.Transparent;
            this.Label71.Location = new System.Drawing.Point(20, 9);
            this.Label71.Name = "Label71";
            this.Label71.Size = new System.Drawing.Size(58, 13);
            this.Label71.TabIndex = 1;
            this.Label71.Text = "Pendiente:";
            // 
            // Pend1
            // 
            this.Pend1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Pend1.FormattingEnabled = true;
            this.Pend1.Items.AddRange(new object[] {
            "POSITIVO",
            "NEGATIVO"});
            this.Pend1.Location = new System.Drawing.Point(15, 26);
            this.Pend1.Name = "Pend1";
            this.Pend1.Size = new System.Drawing.Size(68, 21);
            this.Pend1.TabIndex = 0;
            // 
            // chkGate
            // 
            this.chkGate.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkGate.AutoSize = true;
            this.chkGate.BackColor = System.Drawing.Color.DimGray;
            this.chkGate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGate.ForeColor = System.Drawing.Color.White;
            this.chkGate.Location = new System.Drawing.Point(389, 40);
            this.chkGate.Name = "chkGate";
            this.chkGate.Size = new System.Drawing.Size(36, 25);
            this.chkGate.TabIndex = 22;
            this.chkGate.Text = "ON";
            this.chkGate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkGate.UseVisualStyleBackColor = false;
            this.chkGate.CheckedChanged += new System.EventHandler(this.chkGate_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.StopGate);
            this.groupBox7.Controls.Add(this.Label88);
            this.groupBox7.Controls.Add(this.ValorGateStop);
            this.groupBox7.Controls.Add(this.Label92);
            this.groupBox7.Controls.Add(this.StopPend);
            this.groupBox7.Controls.Add(this.Label89);
            this.groupBox7.Location = new System.Drawing.Point(203, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox7.Size = new System.Drawing.Size(177, 89);
            this.groupBox7.TabIndex = 126;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Stop";
            // 
            // StopGate
            // 
            this.StopGate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StopGate.FormattingEnabled = true;
            this.StopGate.Items.AddRange(new object[] {
            "AUTO",
            "TIME",
            "DIGIT",
            "EXT"});
            this.StopGate.Location = new System.Drawing.Point(67, 12);
            this.StopGate.Name = "StopGate";
            this.StopGate.Size = new System.Drawing.Size(97, 21);
            this.StopGate.TabIndex = 10;
            // 
            // Label88
            // 
            this.Label88.AutoSize = true;
            this.Label88.BackColor = System.Drawing.Color.Transparent;
            this.Label88.Location = new System.Drawing.Point(35, 16);
            this.Label88.Name = "Label88";
            this.Label88.Size = new System.Drawing.Size(33, 13);
            this.Label88.TabIndex = 11;
            this.Label88.Text = "Gate:";
            // 
            // ValorGateStop
            // 
            this.ValorGateStop.Location = new System.Drawing.Point(67, 62);
            this.ValorGateStop.Name = "ValorGateStop";
            this.ValorGateStop.Size = new System.Drawing.Size(97, 20);
            this.ValorGateStop.TabIndex = 14;
            // 
            // Label92
            // 
            this.Label92.AutoSize = true;
            this.Label92.BackColor = System.Drawing.Color.Transparent;
            this.Label92.Location = new System.Drawing.Point(35, 66);
            this.Label92.Name = "Label92";
            this.Label92.Size = new System.Drawing.Size(34, 13);
            this.Label92.TabIndex = 15;
            this.Label92.Text = "Valor:";
            // 
            // StopPend
            // 
            this.StopPend.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StopPend.FormattingEnabled = true;
            this.StopPend.Items.AddRange(new object[] {
            "POS",
            "NEG"});
            this.StopPend.Location = new System.Drawing.Point(67, 37);
            this.StopPend.Name = "StopPend";
            this.StopPend.Size = new System.Drawing.Size(97, 21);
            this.StopPend.TabIndex = 8;
            // 
            // Label89
            // 
            this.Label89.AutoSize = true;
            this.Label89.BackColor = System.Drawing.Color.Transparent;
            this.Label89.Location = new System.Drawing.Point(12, 41);
            this.Label89.Name = "Label89";
            this.Label89.Size = new System.Drawing.Size(58, 13);
            this.Label89.TabIndex = 9;
            this.Label89.Text = "Pendiente:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.StartPend);
            this.groupBox5.Controls.Add(this.Label87);
            this.groupBox5.Controls.Add(this.StartGate);
            this.groupBox5.Controls.Add(this.Label86);
            this.groupBox5.Location = new System.Drawing.Point(17, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox5.Size = new System.Drawing.Size(170, 89);
            this.groupBox5.TabIndex = 125;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Start";
            // 
            // StartPend
            // 
            this.StartPend.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartPend.FormattingEnabled = true;
            this.StartPend.Items.AddRange(new object[] {
            "POS",
            "NEG"});
            this.StartPend.Location = new System.Drawing.Point(67, 37);
            this.StartPend.Name = "StartPend";
            this.StartPend.Size = new System.Drawing.Size(97, 21);
            this.StartPend.TabIndex = 4;
            // 
            // Label87
            // 
            this.Label87.AutoSize = true;
            this.Label87.BackColor = System.Drawing.Color.Transparent;
            this.Label87.Location = new System.Drawing.Point(6, 41);
            this.Label87.Name = "Label87";
            this.Label87.Size = new System.Drawing.Size(58, 13);
            this.Label87.TabIndex = 5;
            this.Label87.Text = "Pendiente:";
            // 
            // StartGate
            // 
            this.StartGate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartGate.FormattingEnabled = true;
            this.StartGate.Items.AddRange(new object[] {
            "AUTO",
            "EXT"});
            this.StartGate.Location = new System.Drawing.Point(67, 12);
            this.StartGate.Name = "StartGate";
            this.StartGate.Size = new System.Drawing.Size(97, 21);
            this.StartGate.TabIndex = 6;
            // 
            // Label86
            // 
            this.Label86.AutoSize = true;
            this.Label86.BackColor = System.Drawing.Color.Transparent;
            this.Label86.Location = new System.Drawing.Point(31, 16);
            this.Label86.Name = "Label86";
            this.Label86.Size = new System.Drawing.Size(33, 13);
            this.Label86.TabIndex = 7;
            this.Label86.Text = "Gate:";
            // 
            // checStats
            // 
            this.checStats.Appearance = System.Windows.Forms.Appearance.Button;
            this.checStats.AutoSize = true;
            this.checStats.BackColor = System.Drawing.Color.DimGray;
            this.checStats.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checStats.ForeColor = System.Drawing.Color.White;
            this.checStats.Location = new System.Drawing.Point(383, 44);
            this.checStats.Name = "checStats";
            this.checStats.Size = new System.Drawing.Size(36, 25);
            this.checStats.TabIndex = 22;
            this.checStats.Text = "ON";
            this.checStats.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checStats.UseVisualStyleBackColor = false;
            this.checStats.CheckedChanged += new System.EventHandler(this.checStats_CheckedChanged);
            // 
            // GroupBox2
            // 
            this.GroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.GroupBox2.Controls.Add(this.RadioButton2);
            this.GroupBox2.Controls.Add(this.RadioButton1);
            this.GroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox2.Location = new System.Drawing.Point(24, 44);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(149, 38);
            this.GroupBox2.TabIndex = 125;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Use:";
            // 
            // RadioButton2
            // 
            this.RadioButton2.AutoSize = true;
            this.RadioButton2.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton2.Checked = true;
            this.RadioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton2.Location = new System.Drawing.Point(6, 15);
            this.RadioButton2.Name = "RadioButton2";
            this.RadioButton2.Size = new System.Drawing.Size(65, 17);
            this.RadioButton2.TabIndex = 21;
            this.RadioButton2.TabStop = true;
            this.RadioButton2.Text = "All Meas";
            this.RadioButton2.UseVisualStyleBackColor = false;
            // 
            // RadioButton1
            // 
            this.RadioButton1.AutoSize = true;
            this.RadioButton1.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton1.Location = new System.Drawing.Point(77, 15);
            this.RadioButton1.Name = "RadioButton1";
            this.RadioButton1.Size = new System.Drawing.Size(61, 17);
            this.RadioButton1.TabIndex = 20;
            this.RadioButton1.TabStop = true;
            this.RadioButton1.Text = " In Limit";
            this.RadioButton1.UseVisualStyleBackColor = false;
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.GroupBox1.Controls.Add(this.RadioButton3);
            this.GroupBox1.Controls.Add(this.RadioButton4);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(225, 44);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(100, 38);
            this.GroupBox1.TabIndex = 124;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "On Single";
            // 
            // RadioButton3
            // 
            this.RadioButton3.AutoSize = true;
            this.RadioButton3.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton3.Checked = true;
            this.RadioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton3.Location = new System.Drawing.Point(12, 15);
            this.RadioButton3.Name = "RadioButton3";
            this.RadioButton3.Size = new System.Drawing.Size(31, 17);
            this.RadioButton3.TabIndex = 23;
            this.RadioButton3.TabStop = true;
            this.RadioButton3.Text = "1";
            this.RadioButton3.UseVisualStyleBackColor = false;
            // 
            // RadioButton4
            // 
            this.RadioButton4.AutoSize = true;
            this.RadioButton4.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton4.Location = new System.Drawing.Point(53, 15);
            this.RadioButton4.Name = "RadioButton4";
            this.RadioButton4.Size = new System.Drawing.Size(36, 17);
            this.RadioButton4.TabIndex = 22;
            this.RadioButton4.TabStop = true;
            this.RadioButton4.Text = " N";
            this.RadioButton4.UseVisualStyleBackColor = false;
            // 
            // Label94
            // 
            this.Label94.AutoSize = true;
            this.Label94.BackColor = System.Drawing.Color.Transparent;
            this.Label94.Location = new System.Drawing.Point(216, 15);
            this.Label94.Name = "Label94";
            this.Label94.Size = new System.Drawing.Size(53, 13);
            this.Label94.TabIndex = 17;
            this.Label94.Text = "Muestras:";
            // 
            // ValorN
            // 
            this.ValorN.Location = new System.Drawing.Point(272, 11);
            this.ValorN.Name = "ValorN";
            this.ValorN.Size = new System.Drawing.Size(53, 20);
            this.ValorN.TabIndex = 16;
            // 
            // Label93
            // 
            this.Label93.AutoSize = true;
            this.Label93.BackColor = System.Drawing.Color.Transparent;
            this.Label93.Location = new System.Drawing.Point(21, 15);
            this.Label93.Name = "Label93";
            this.Label93.Size = new System.Drawing.Size(45, 13);
            this.Label93.TabIndex = 11;
            this.Label93.Text = "Medida:";
            // 
            // Meas
            // 
            this.Meas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Meas.FormattingEnabled = true;
            this.Meas.Items.AddRange(new object[] {
            "MEAS",
            "STD DEV",
            "MEAN",
            "MAX",
            "MIN"});
            this.Meas.Location = new System.Drawing.Point(69, 11);
            this.Meas.Name = "Meas";
            this.Meas.Size = new System.Drawing.Size(91, 21);
            this.Meas.TabIndex = 10;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnReset);
            this.groupBox3.Controls.Add(this.btnClear);
            this.groupBox3.Controls.Add(this.btnRun);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(444, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(74, 99);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.DimGray;
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.ForeColor = System.Drawing.Color.White;
            this.btnRun.Location = new System.Drawing.Point(7, 63);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(61, 33);
            this.btnRun.TabIndex = 178;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // chkFRECCH1
            // 
            this.chkFRECCH1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkFRECCH1.BackColor = System.Drawing.Color.DimGray;
            this.chkFRECCH1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkFRECCH1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFRECCH1.ForeColor = System.Drawing.Color.White;
            this.chkFRECCH1.Location = new System.Drawing.Point(5, 12);
            this.chkFRECCH1.Name = "chkFRECCH1";
            this.chkFRECCH1.Size = new System.Drawing.Size(61, 25);
            this.chkFRECCH1.TabIndex = 139;
            this.chkFRECCH1.Text = "Freq CH1";
            this.chkFRECCH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkFRECCH1.UseVisualStyleBackColor = false;
            this.chkFRECCH1.CheckedChanged += new System.EventHandler(this.chkFRECCH1_CheckedChanged);
            // 
            // chkPERCH1
            // 
            this.chkPERCH1.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkPERCH1.BackColor = System.Drawing.Color.DimGray;
            this.chkPERCH1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkPERCH1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPERCH1.ForeColor = System.Drawing.Color.White;
            this.chkPERCH1.Location = new System.Drawing.Point(5, 39);
            this.chkPERCH1.Name = "chkPERCH1";
            this.chkPERCH1.Size = new System.Drawing.Size(61, 25);
            this.chkPERCH1.TabIndex = 141;
            this.chkPERCH1.Text = "Per CH1";
            this.chkPERCH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkPERCH1.UseVisualStyleBackColor = false;
            this.chkPERCH1.CheckedChanged += new System.EventHandler(this.chkPERCH1_CheckedChanged);
            // 
            // chkTINT
            // 
            this.chkTINT.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkTINT.BackColor = System.Drawing.Color.DimGray;
            this.chkTINT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkTINT.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTINT.ForeColor = System.Drawing.Color.White;
            this.chkTINT.Location = new System.Drawing.Point(5, 93);
            this.chkTINT.Name = "chkTINT";
            this.chkTINT.Size = new System.Drawing.Size(61, 25);
            this.chkTINT.TabIndex = 142;
            this.chkTINT.Text = "T1 to T2";
            this.chkTINT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkTINT.UseVisualStyleBackColor = false;
            this.chkTINT.CheckedChanged += new System.EventHandler(this.chkTINT_CheckedChanged);
            // 
            // chkFRECCH2
            // 
            this.chkFRECCH2.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkFRECCH2.BackColor = System.Drawing.Color.DimGray;
            this.chkFRECCH2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkFRECCH2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFRECCH2.ForeColor = System.Drawing.Color.White;
            this.chkFRECCH2.Location = new System.Drawing.Point(5, 66);
            this.chkFRECCH2.Name = "chkFRECCH2";
            this.chkFRECCH2.Size = new System.Drawing.Size(61, 25);
            this.chkFRECCH2.TabIndex = 140;
            this.chkFRECCH2.Text = "Freq CH2";
            this.chkFRECCH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkFRECCH2.UseVisualStyleBackColor = false;
            this.chkFRECCH2.CheckedChanged += new System.EventHandler(this.chkFRECCH2_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Channel1);
            this.tabControl1.Controls.Add(this.Channel2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(438, 127);
            this.tabControl1.TabIndex = 182;
            // 
            // Channel1
            // 
            this.Channel1.BackColor = System.Drawing.Color.LightGray;
            this.Channel1.Controls.Add(this.chkCh1);
            this.Channel1.Controls.Add(this.Pend1);
            this.Channel1.Controls.Add(this.Label81);
            this.Channel1.Controls.Add(this.Imped1);
            this.Channel1.Controls.Add(this.Filtro1);
            this.Channel1.Controls.Add(this.Label71);
            this.Channel1.Controls.Add(this.Label83);
            this.Channel1.Controls.Add(this.Acoplo1);
            this.Channel1.Controls.Add(this.Atenua1);
            this.Channel1.Controls.Add(this.Label85);
            this.Channel1.Controls.Add(this.Label84);
            this.Channel1.Controls.Add(this.Sens1);
            this.Channel1.Controls.Add(this.Label80);
            this.Channel1.Controls.Add(this.Label73);
            this.Channel1.Controls.Add(this.ValorLevel1);
            this.Channel1.Controls.Add(this.Level1);
            this.Channel1.Controls.Add(this.Label75);
            this.Channel1.Location = new System.Drawing.Point(4, 22);
            this.Channel1.Name = "Channel1";
            this.Channel1.Padding = new System.Windows.Forms.Padding(3);
            this.Channel1.Size = new System.Drawing.Size(430, 101);
            this.Channel1.TabIndex = 2;
            this.Channel1.Text = "Channel1";
            // 
            // Channel2
            // 
            this.Channel2.BackColor = System.Drawing.Color.LightGray;
            this.Channel2.Controls.Add(this.chkCh2);
            this.Channel2.Controls.Add(this.Pend2);
            this.Channel2.Controls.Add(this.label1);
            this.Channel2.Controls.Add(this.Imped2);
            this.Channel2.Controls.Add(this.Filtro2);
            this.Channel2.Controls.Add(this.label8);
            this.Channel2.Controls.Add(this.label2);
            this.Channel2.Controls.Add(this.label4);
            this.Channel2.Controls.Add(this.Atenua2);
            this.Channel2.Controls.Add(this.label7);
            this.Channel2.Controls.Add(this.label5);
            this.Channel2.Controls.Add(this.Acoplo2);
            this.Channel2.Controls.Add(this.Sens2);
            this.Channel2.Controls.Add(this.ValorLevel2);
            this.Channel2.Controls.Add(this.label3);
            this.Channel2.Controls.Add(this.Level2);
            this.Channel2.Controls.Add(this.label6);
            this.Channel2.Location = new System.Drawing.Point(4, 22);
            this.Channel2.Name = "Channel2";
            this.Channel2.Padding = new System.Windows.Forms.Padding(3);
            this.Channel2.Size = new System.Drawing.Size(430, 101);
            this.Channel2.TabIndex = 3;
            this.Channel2.Text = "Channel2";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.chkGate);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabPage1.Size = new System.Drawing.Size(430, 101);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Gate";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.GroupBox2);
            this.tabPage2.Controls.Add(this.GroupBox1);
            this.tabPage2.Controls.Add(this.checStats);
            this.tabPage2.Controls.Add(this.Label93);
            this.tabPage2.Controls.Add(this.Meas);
            this.tabPage2.Controls.Add(this.Label94);
            this.tabPage2.Controls.Add(this.ValorN);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(430, 101);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Statistics";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkFRECCH1);
            this.groupBox4.Controls.Add(this.chkFRECCH2);
            this.groupBox4.Controls.Add(this.chkPERCH1);
            this.groupBox4.Controls.Add(this.chkTINT);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox4.Location = new System.Drawing.Point(444, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(74, 127);
            this.groupBox4.TabIndex = 183;
            this.groupBox4.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 99);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 127);
            this.panel1.TabIndex = 184;
            // 
            // UC_HP_53131
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BackgroundImage = global::Dezac.Instruments.Properties.Resources._53131A2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbl_Mesur_Mult);
            this.Controls.Add(this.lbl_Vers_tester);
            this.DoubleBuffered = true;
            this.Name = "UC_HP_53131";
            this.Size = new System.Drawing.Size(518, 226);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.Channel1.ResumeLayout(false);
            this.Channel1.PerformLayout();
            this.Channel2.ResumeLayout(false);
            this.Channel2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        internal System.Windows.Forms.Label lbl_Vers_tester;
        internal System.Windows.Forms.Label lbl_Mesur_Mult;
        private System.Windows.Forms.Button btnClear;
        internal System.Windows.Forms.TextBox ValorLevel1;
        internal System.Windows.Forms.Label Label81;
        internal System.Windows.Forms.ComboBox Filtro1;
        internal System.Windows.Forms.Label Label83;
        internal System.Windows.Forms.ComboBox Atenua1;
        internal System.Windows.Forms.Label Label84;
        internal System.Windows.Forms.ComboBox Acoplo1;
        internal System.Windows.Forms.Label Label85;
        internal System.Windows.Forms.ComboBox Imped1;
        internal System.Windows.Forms.Label Label80;
        internal System.Windows.Forms.Label Label75;
        internal System.Windows.Forms.ComboBox Level1;
        internal System.Windows.Forms.Label Label73;
        internal System.Windows.Forms.ComboBox Sens1;
        internal System.Windows.Forms.Label Label71;
        internal System.Windows.Forms.ComboBox Pend1;
        internal System.Windows.Forms.Label Label92;
        internal System.Windows.Forms.TextBox ValorGateStop;
        internal System.Windows.Forms.Label Label88;
        internal System.Windows.Forms.ComboBox StopGate;
        internal System.Windows.Forms.Label Label89;
        internal System.Windows.Forms.ComboBox StopPend;
        internal System.Windows.Forms.Label Label86;
        internal System.Windows.Forms.ComboBox StartGate;
        internal System.Windows.Forms.Label Label87;
        internal System.Windows.Forms.ComboBox StartPend;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.RadioButton RadioButton2;
        internal System.Windows.Forms.RadioButton RadioButton1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.RadioButton RadioButton3;
        internal System.Windows.Forms.RadioButton RadioButton4;
        internal System.Windows.Forms.CheckBox checStats;
        internal System.Windows.Forms.Label Label94;
        internal System.Windows.Forms.TextBox ValorN;
        internal System.Windows.Forms.Label Label93;
        internal System.Windows.Forms.ComboBox Meas;
        internal System.Windows.Forms.TextBox ValorLevel2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox Filtro2;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ComboBox Atenua2;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.ComboBox Acoplo2;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.ComboBox Imped2;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.ComboBox Level2;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.ComboBox Sens2;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.ComboBox Pend2;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.CheckBox chkFRECCH1;
        internal System.Windows.Forms.CheckBox chkPERCH1;
        internal System.Windows.Forms.CheckBox chkTINT;
        internal System.Windows.Forms.CheckBox chkFRECCH2;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.CheckBox chkGate;
        internal System.Windows.Forms.CheckBox chkCh1;
        internal System.Windows.Forms.CheckBox chkCh2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage Channel1;
        private System.Windows.Forms.TabPage Channel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}
