﻿using log4net;
using Seagull.BarTender.Print;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dezac.Labels
{
    public class BarTenderService : IDisposable
    {
        private static readonly ILog logger = LogManager.GetLogger("BarTenderReportService");

        private Engine engine = null;
        private LabelFormatDocument format = null;

        public BarTenderService()
        {
            LicenseServer = new BarTenderLicenseServerInfo { Timeout = 10, Retries = 1 };
        }

        //public BarTenderService(string licenseServerAddress, int port = 5160)
        //    : this()
        //{
        //    LicenseServer.Address = licenseServerAddress;
        //    LicenseServer.Port = port;
        //}

        public BarTenderService(string fileName)
            : this()
        {
            Open(fileName);
        }

        private Engine Engine
        {
            get
            {
                if (engine == null)
                {
                    engine = new Engine(true);                    
                    logger.Info("Instanciamos correctamente BarTender Engine");
                }

                return engine;
            }
        }

        public string PreviewRange(string previewPath, string range = "1")
        {
            EnsurePrinterSelected();

            format.PrintSetup.PrinterName = PrinterName;

            int width = Convert.ToInt32(format.PageSetup.LabelWidth * 7);
            int height = Convert.ToInt32(format.PageSetup.LabelHeight * 7);

            Messages messages;

            if (!Directory.Exists(previewPath))
                Directory.CreateDirectory(previewPath);

            var name = "BT_Preview_" + Path.GetFileNameWithoutExtension(format.FileName) + ".jpg";
            var imageFileName = Path.Combine(previewPath, name);

            if (File.Exists(imageFileName))
                File.Delete(imageFileName);

            var result = format.ExportPrintPreviewRangeToFile(range, previewPath, name, ImageType.JPEG, ColorDepth.ColorDepth24bit, new Resolution(width, height), System.Drawing.Color.White, OverwriteOptions.Overwrite, true, true, out messages);
            imageFileName = imageFileName.Replace(".jpg", "1.jpg");

            if (result == Result.Success && File.Exists(imageFileName))
                return imageFileName;

            return null;
        }

        public string ExportImage(string previewPath)
        {
            EnsurePrinterSelected();

            format.PrintSetup.PrinterName = PrinterName;

            int width = Convert.ToInt32(format.PageSetup.LabelWidth * 100);
            int height = Convert.ToInt32(format.PageSetup.LabelHeight * 100);

            if (!Directory.Exists(previewPath))
                Directory.CreateDirectory(previewPath);

            var name = "BT_Preview_" + Path.GetFileNameWithoutExtension(format.FileName) + ".jpg";
            var imageFileName = Path.Combine(previewPath, name);

            if (File.Exists(imageFileName))
                File.Delete(imageFileName);

            format.ExportImageToFile(imageFileName, ImageType.JPEG, ColorDepth.ColorDepth24bit, new Resolution(width, height), OverwriteOptions.Overwrite);

            return imageFileName;
        }

        public BarTenderLicenseServerInfo LicenseServer { get; private set; }
        public string PrinterName { get; set; }
        public string PathPreviews { get; set; }

        private void EnsurePrinterSelected()
        {
            if (PrinterName != null)
                return;

            PrinterName = GetPrinterNames().FirstOrDefault();
        }

        public List<string> GetPrinterNames()
        {
            return new Printers().Select(p => p.PrinterName).ToList();
        }

        public List<BarTenderNamedItem> GetFields()
        {
            if (format == null)
                return new List<BarTenderNamedItem>();

            var result = format.SubStrings
                .Select(p => new BarTenderNamedItem { Name = p.Name, Value = p.Value, Type = (int)p.Type })
                .ToList();

            return result;
        }

        public void SetFields(List<BarTenderNamedItem> items)
        {
            if (format == null)
                return;

            items.ForEach(p => SetField(p.Name, p.Value));
        }

        public void SetField(string name, string value)
        {
            if (format == null)
                return;

            try
            {
                logger.InfoFormat("BarTender SetField name = {0} -> value = {1} ", name, value);
                var item = format.SubStrings.Where(p => p.Name == name).FirstOrDefault();
                if (item != null)
                    item.Value = value;
                else
                    logger.InfoFormat("BarTender SetField name = {0}  no exist !!!", name);
            }catch(Exception ex)
            {
                logger.WarnFormat("BarTender SetField name = {0}  Exception {1}", ex.Message);
            }
        }

        public void FillFields(Func<BarTenderNamedItem, bool> resolver)
        {
            GetFields()
                .ForEach(p =>
                {
                    if (resolver(p))
                        SetField(p.Name, p.Value);
                });
        }

        public void Print(int numCopies = 1)
        {
            if (format == null)
                throw new Exception("No se ha podido imprimir, porque no hay un formato de etiqueta cargado");

            format.PrintSetup.IdenticalCopiesOfLabel = numCopies;

            format.PrintSetup.PrinterName = PrinterName;
            //var result = format.Print(string.Empty, 15 * 1000);

            Messages messages;

            logger.Info("BarTender Engine format.Print");

            var result = format.Print(string.Empty, out messages); 
            if(result != Result.Success)
                throw new Exception(messages.FirstOrDefault().Text);        
        }

        public void Open(string fileName)
        {
            Close();

            format = Engine.Documents.Open(fileName);
        }

        public void Close()
        {
            if (format != null)
                format.Close(SaveOptions.DoNotSaveChanges);

            format = null;
        }

        public void Dispose()
        {
            Close();

            if (engine != null)
                engine.Stop(SaveOptions.DoNotSaveChanges);

            engine = null;
        }
    }

    public class BarTenderLicenseServerInfo
    {
        public string Address { get; set; }
        public int Port { get; set; }
        public int Retries { get; set; }
        public int Timeout { get; set; }
    }

    public class BarTenderNamedItem
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }
}
