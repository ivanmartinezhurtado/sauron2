﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using System.ComponentModel;
using System.Drawing;
using System.Text;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.06)]
    [DesignerAction(Type = typeof(ChekSumActionDescription))]
    public class ChekSumAction : ActionBase
    {
        [Description("Nombre del dato a calcular el Checksum")]
        [Category("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Campo para decir si es Hexadecimal el valor del dato a calcular ")]
        [Category("3. Configuration")]
        [DefaultValue(false)]
        public bool HexString { get; set; }

        [Description("")]
        [Category("3. Configuration")]
        [DefaultValue(ChecksumTypeEnum.Decimal_Mod100)]
        public ChecksumTypeEnum ChecksumType { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            int cheksum = 0;

            var var = DataInput.Value.ToString();
                
            byte[] byteToCalculate = !HexString ? Encoding.ASCII.GetBytes(var) : var.HexStringToBytes();

            foreach (byte b in byteToCalculate)
                cheksum += b;

            string cheksumResult ="";
            switch (ChecksumType)
            {
                case ChecksumTypeEnum.Decimal_Mod100:
                    cheksumResult = ((byte)(cheksum % 100)).ToString("D2");
                    break;
                case ChecksumTypeEnum.Hexadecimal_Mod256:
                    cheksumResult = ((byte)(cheksum % 256)).ToString("X2");
                    break;
                case ChecksumTypeEnum.NotDefined:
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("ChecksumType sin configurar");
                    break;
            }

            Logger.InfoFormat($"Sum = {cheksum}");
            Logger.InfoFormat($"CheckSum = {cheksumResult}");

            DataOutput.Value = cheksumResult;
        }
    }

    public class ChekSumActionDescription : ActionDescription
    {
        public override string Name { get { return "CheckSum"; } }
        public override string Description { get { return "Calcula el CheckSum del dato de entrada"; } }
        public override string Category { get { return "Actions/Math"; } }
        public override Image Icon { get { return Properties.Resources.Math; } }
    }
}
