﻿using System.Linq;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.01)]
    public class CNET_RS : DeviceBase
    {
        public CirbusDevice Cirbus { get; internal set; }

        #region Instanciacion y destruccion Device

        public CNET_RS()
        {
        }

        public CNET_RS(int port, byte periferico, int baudrate)
        {
            SetPort(port, periferico, baudrate);
        }

        public void SetPort(int port, byte periferico, int baudrate)
        {
            Cirbus = new CirbusDeviceSerialPort(port, baudrate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Cirbus.PerifericNumber = periferico;
            Cirbus.TimeOut = 2000;
        }

        public override void Dispose()
        {
            Cirbus.Dispose();
        }

        #endregion

        #region Operaciones lectura

        public string ReadVersion()
        {
            LogMethod();

            string version = Cirbus.Read(READ_VERSION).Split(' ').LastOrDefault();

            return version;
        }

        public string ReadIP()
        {
            LogMethod();
            return Cirbus.Read(READ_IP).Remove(0, 4);
        }

        public string ReadMAC()
        {
            LogMethod();
            return Cirbus.Read(READ_MAC).Split(' ').LastOrDefault();
        }

        public string ReadFlash()
        {
            LogMethod();
            return Cirbus.Read(READ_FLASH_INFO).Split(' ').LastOrDefault();
        }
       

        #endregion

        #region Operaciones Escritura

        public void WriteIP(string IP)
        {
            LogMethod();
            Cirbus.Write(WRITE_IP, IP);
        }

        public void WriteSaveConfig()
        {
            LogMethod();
            Cirbus.Write(WRITE_SAVE_CONFIG);
        }

        public void WriteMAC(string mac)
        {
            LogMethod();
            Cirbus.Write(WRITE_MAC, mac);
        }

        public void WriteFlashFormat()
        {
            LogMethod();
            Cirbus.Write(WRITE_FLASH_FORMAT);
        }

        #endregion

        #region Enum and Dict

        private const string READ_VERSION = "VER";
        private const string READ_IP = "IPR ETH";
        private const string WRITE_IP = "IPW ETH {0}";
        private const string WRITE_SAVE_CONFIG = "XML EXP NET"; 
        private const string READ_MAC = "MAC";
        private const string WRITE_MAC = "MAC WRITE {0}";
        private const string READ_FLASH_INFO = "FAT I";
        private const string WRITE_FLASH_FORMAT = "FAT FORMAT";

        #endregion
    }

}
