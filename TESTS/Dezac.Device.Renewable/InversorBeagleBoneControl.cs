﻿using Comunications.Utility;
using System.Runtime.InteropServices;

namespace Dezac.Device.Renewable
{
    [DeviceVersion(1.00)]
    public class InversorBeagleBoneControl : DeviceBase
    {
        #region Instanciación del equipo

        public InversorBeagleBoneControl()
        {
        }

        public InversorBeagleBoneControl(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port = 3, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.Two, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }
        
        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #endregion

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        #region Operaciones de lectura

        public ushort ReadDigitalInput(DigitalInputs input)
        {
            return Modbus.ReadMultipleHoldingRegister((ushort)input)[0];
        }

        public double ReadAnalogInput(AnalogInputs input)
        {
            return Modbus.ReadMultipleHoldingRegister((ushort)input)[0] / 100D;
        }


        #endregion

        #region Operaciones de escritura

        public void WriteOutputs(REOutputs output)
        {
            Modbus.Write(output);
        }

        public void WriteOutputs(RESETAndPWMOutputs output)
        {
            Modbus.Write(output);
        }


        #endregion

        #region Registers and structures

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Outputs.RE1)]
        public struct REOutputs
        {
            private ushort _re1;

            public bool Re1
            {
                get { return _re1 == 0x0001; }
                set { _re1 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re2;

            public bool Re2
            {
                get { return _re2 == 0x0001; }
                set { _re2 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re3;

            public bool Re3
            {
                get { return _re3 == 0x0001; }
                set { _re3 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re4;

            public bool Re4
            {
                get { return _re4 == 0x0001; }
                set { _re4 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re5;

            public bool Re5
            {
                get { return _re5 == 0x0001; }
                set { _re5 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re6;

            public bool Re6
            {
                get { return _re6 == 0x0001; }
                set { _re6 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }
            private ushort _re7;

            public bool Re7
            {
                get { return _re7 == 0x0001; }
                set { _re7 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re8;

            public bool Re8
            {
                get { return _re8 == 0x0001; }
                set { _re8 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re9;

            public bool Re9
            {
                get { return _re9 == 0x0001; }
                set { _re9 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re10;

            public bool Re10
            {
                get { return _re10 == 0x0001; }
                set { _re10 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re11;

            public bool Re11
            {
                get { return _re11 == 0x0001; }
                set { _re11 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _re12;

            public bool Re12
            {
                get { return _re12 == 0x0001; }
                set { _re12 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Outputs.RESET1)]
        public struct RESETAndPWMOutputs
        {
            private ushort _reset1;

            public bool Reset1
            {
                get { return _reset1 == 0x0001; }
                set { _reset1 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _reset2;

            public bool Reset2
            {
                get { return _reset2 == 0x0001; }
                set { _reset2 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _reset3;

            public bool Reset3
            {
                get { return _reset3 == 0x0001; }
                set { _reset3 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _reset4;

            public bool Reset4
            {
                get { return _reset4 == 0x0001; }
                set { _reset4 = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _led1;

            private ushort _led2;

            private ushort _pwm1_top;

            public bool Pwm1_top
            {
                get { return _pwm1_top == 0x0001; }
                set { _pwm1_top = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm1_bot;

            public bool Pwm1_bot
            {
                get { return _pwm1_bot == 0x0001; }
                set { _pwm1_bot = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm2_top;

            public bool Pwm2_top
            {
                get { return _pwm2_top == 0x0001; }
                set { _pwm2_top = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm2_bot;

            public bool Pwm2_bot
            {
                get { return _pwm2_bot == 0x0001; }
                set { _pwm2_bot = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm3_top;

            public bool Pwm3_top
            {
                get { return _pwm3_top == 0x0001; }
                set { _pwm3_top = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm3_bot;

            public bool Pwm3_bot
            {
                get { return _pwm3_bot == 0x0001; }
                set { _pwm3_bot = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm4_top;

            public bool Pwm4_top
            {
                get { return _pwm4_top == 0x0001; }
                set { _pwm4_top = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm4_bot;

            public bool Pwm4_bot
            {
                get { return _pwm4_bot == 0x0001; }
                set { _pwm4_bot = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm5_top;

            public bool Pwm5_top
            {
                get { return _pwm5_top == 0x0001; }
                set { _pwm5_top = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm5_bot;

            public bool Pwm5_bot
            {
                get { return _pwm5_bot == 0x0001; }
                set { _pwm5_bot = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm6_top;

            public bool Pwm6_top
            {
                get { return _pwm6_top == 0x0001; }
                set { _pwm6_top = (ushort)(value == true ? 0x0001 : 0x0000); }
            }

            private ushort _pwm6_bot;

            public bool Pwm6_bot
            {
                get { return _pwm6_bot == 0x0001; }
                set { _pwm6_bot = (ushort)(value == true ? 0x0001 : 0x0000); }
            }
        }

        public enum Outputs
        {
            RE1 = 0x0001,
            RE2 = 0x0002,
            RE3 = 0x0003,
            RE4 = 0x0004,
            RE5 = 0x0005,
            RE6 = 0x0006,
            RE7 = 0x0007,
            RE8 = 0x0008,
            RE9 = 0x0009,
            RE10 = 0x000A,
            RE11 = 0x000B,
            RE12 = 0x000C,
            RESET1 = 0x000D,
            RESET2 = 0x000E,
            RESET3 = 0x000F,
            RESET4 = 0x0010,
            LED1 = 0x0011,
            LED2 = 0x0012,
            PWM1_TOP = 0x0013,
            PWM1_BOT = 0x0014,
            PWM2_TOP = 0x0015,
            PWM2_BOT = 0x0016,
            PWM3_TOP = 0x0017,
            PWM3_BOT = 0x0018,
            PWM4_TOP = 0x0019,
            PWM4_BOT = 0x001A,
            PWM5_TOP = 0x001B,
            PWM5_BOT = 0x001C,
            PWM6_TOP = 0x001D,
            PWM6_BOT = 0x001E,
        }

        public enum DigitalInputs
        {
            ERROR1 = 0x001F,
            ERROR2 = 0x0020,
            ERROR3 = 0x0021,
            ERROR4 = 0x0022,
            ERROR5 = 0x0023,
            ERROR = 0x0024,
            INPUT_1 = 0x0025,
            INPUT_2 = 0x0026,
            INPUT_3 = 0x0027,
            INPUT_4 = 0x0028,
        } 

        public enum AnalogInputs
        {
            ADC_A0 = 0x0029,
            ADC_A1 = 0x002A,
            ADC_A2 = 0x002B,
            ADC_A3 = 0x002C,
            ADC_A4 = 0x002D,
            ADC_A5 = 0x002E,
            ADC_A6 = 0x002F,
            ADC_A7 = 0x0030,
            ADC_B0 = 0x0031,
            ADC_B1 = 0x0032,
            ADC_B2 = 0x0033,
            ADC_B3 = 0x0034,
            ADC_B4 = 0x0035,
            ADC_B5 = 0x0036,
            ADC_B6 = 0x0037,
            ADC_B7 = 0x0038,
        }

        #endregion

    }
}
