namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ASISTENCIATECNICA")]
    public partial class ASISTENCIATECNICA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMASISTENCIA { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPRODUCTO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMLIN { get; set; }

        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public int? NUMPEDIDO { get; set; }

        public int? NUMLINPEDIDO { get; set; }

        [StringLength(20)]
        public string NUMEROSERIE { get; set; }

        public DateTime FECHAALTA { get; set; }

        public DateTime? FECHAFIN { get; set; }

        public DateTime? FECHAFINPREV { get; set; }

        [Required]
        [StringLength(1)]
        public string ABIERTA { get; set; }

        [StringLength(1)]
        public string GARANTIA { get; set; }

        [StringLength(1)]
        public string CARGO { get; set; }

        [StringLength(500)]
        public string CAUSA_1 { get; set; }

        [StringLength(500)]
        public string CAUSA_2 { get; set; }

        [StringLength(500)]
        public string TRABAJO_1 { get; set; }

        [StringLength(500)]
        public string TRABAJO_2 { get; set; }

        public decimal? HORAS { get; set; }

        public decimal? PRECIO { get; set; }

        [StringLength(500)]
        public string NOTAS { get; set; }

        [StringLength(500)]
        public string AVERIACOMUNICADA { get; set; }

        public DateTime? TIME_ID { get; set; }

        [Required]
        [StringLength(1)]
        public string OFERTA_SINO { get; set; }

        public DateTime? FECHAOFERTA { get; set; }

        public int? NUMDOC { get; set; }

        [StringLength(1)]
        public string ESTADOEQUIPO { get; set; }

        [StringLength(1)]
        public string DEFECTUOSOEN { get; set; }

        [StringLength(10)]
        public string IDDEFECTO { get; set; }

        public int? NUMMATRICULA { get; set; }

        public int? NUMTEST { get; set; }

        [StringLength(20)]
        public string NROREPARACION { get; set; }

        public int? NS_ALBARAN { get; set; }

        public int? NS_PEDIDO { get; set; }

        public int? NS_LINEA { get; set; }

        public int? NS_PARTE { get; set; }

        public int? NS_ORDEN { get; set; }

        public int? NS_NUMFABRICACION { get; set; }

        [StringLength(20)]
        public string UBICACION { get; set; }

        [StringLength(1)]
        public string ESTADO { get; set; }

        [StringLength(20)]
        public string NUEVONROSERIE { get; set; }

        [StringLength(50)]
        public string NOTASLOG { get; set; }

        [StringLength(20)]
        public string RESPONSABLE { get; set; }

        public int? NUMREPROCESO { get; set; }

        public DateTime? FECHAFINREAL { get; set; }

        public int? NUMLOADPVC { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        public virtual MATRICULA MATRICULA { get; set; }

        public virtual T_TEST T_TEST { get; set; }

        public virtual REPROCESOFAB REPROCESOFAB { get; set; }
    }
}
