﻿using Dezac.Labels;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(PrintLabelOfflineDescription))]
    public class PrintLabelOffline : ActionBase
    {
        [DefaultValue(false)]
        [Description("Parámetro para activar si abrir el BarTender en cada impresión")]
        [Category("3. Configuration")]
        public bool BarTenderVisible { get; set; }

        [Description("Path donde esta la etiqueta a imprimir")]
        [Category("3. Configuration")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PathBarTenderLabels { get; set; }

        [Description("Nombre de la impresora a utilizar, sino coge la que tenga el BarTender o la predeterminada")]
        [Category("3. Configuration")]
        public string  printerName { get; set; }

        [Description("Lista de las variables a utilizar en el test para asignar a las variables del BarTender e imprimir")]
        [Category("3. Configuration")]
        public TextValuePair[] DataInput { get; set; }

        [DefaultValue(1)]
        [Description("Número de etiquetas que se quiere imprimir")]
        [Category("4. Results")]
        public int NumberCopies { get; set; }

        public override void Execute()
        {
            if (string.IsNullOrEmpty(PathBarTenderLabels))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("PathBarTenderLabels").Throw();

            var ns = new Dictionary<string, object>();

            if (DataInput != null)
                foreach (TextValuePair var in DataInput)
                    ns.Add(var.Key, VariablesTools.ResolveValue(var.Value));

            using (var btr = new BarTenderReport())
            {
                btr.LabelFileName = PathBarTenderLabels;
                btr.PrinterName = printerName; 
                btr.IdenticalCopiesOfLabel = NumberCopies;
                btr.Visible = BarTenderVisible;
                btr.PrintTest(ns);
            }
        }
    }

    public class PrintLabelOfflineDescription : ActionDescription
    {
        public override string Name { get { return "PrintLabelOffline"; } }
        public override string Category { get { return "Tools/Labels"; } }
        public override string Description { get { return "Action para utilizar la impresora de manera offline"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
