﻿namespace WinTaskRunner.Views
{
    partial class EditorView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuNodo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdUp = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdDown = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdCut = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdCollapse = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdExpand = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdBreakpoint = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDropNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdMoveAsChild = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdMoveAsPrevious = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdMoveAsNext = new System.Windows.Forms.ToolStripMenuItem();
            this.searchBox = new Telerik.WinControls.UI.RadTextBox();
            this.treeView = new Telerik.WinControls.UI.RadTreeView();
            this.mnuNodo.SuspendLayout();
            this.mnuDropNode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuNodo
            // 
            this.mnuNodo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdUp,
            this.cmdDown,
            this.cmdSeparator1,
            this.cmdRemove,
            this.cmdSeparator2,
            this.cmdCut,
            this.cmdCopy,
            this.cmdPaste,
            this.toolStripMenuItem1,
            this.cmdCollapse,
            this.cmdExpand,
            this.toolStripMenuItem2,
            this.cmdBreakpoint});
            this.mnuNodo.Name = "mnuNodo";
            this.mnuNodo.Size = new System.Drawing.Size(208, 226);
            this.mnuNodo.Opening += new System.ComponentModel.CancelEventHandler(this.mnuNodo_Opening);
            // 
            // cmdUp
            // 
            this.cmdUp.Name = "cmdUp";
            this.cmdUp.Size = new System.Drawing.Size(207, 22);
            this.cmdUp.Text = "Subir";
            this.cmdUp.Click += new System.EventHandler(this.cmdUp_Click);
            // 
            // cmdDown
            // 
            this.cmdDown.Name = "cmdDown";
            this.cmdDown.Size = new System.Drawing.Size(207, 22);
            this.cmdDown.Text = "Bajar";
            this.cmdDown.Click += new System.EventHandler(this.cmdDown_Click);
            // 
            // cmdSeparator1
            // 
            this.cmdSeparator1.Name = "cmdSeparator1";
            this.cmdSeparator1.Size = new System.Drawing.Size(204, 6);
            // 
            // cmdRemove
            // 
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(207, 22);
            this.cmdRemove.Text = "Quitar";
            this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
            // 
            // cmdSeparator2
            // 
            this.cmdSeparator2.Name = "cmdSeparator2";
            this.cmdSeparator2.Size = new System.Drawing.Size(204, 6);
            // 
            // cmdCut
            // 
            this.cmdCut.Name = "cmdCut";
            this.cmdCut.Size = new System.Drawing.Size(207, 22);
            this.cmdCut.Text = "Cortar";
            this.cmdCut.Click += new System.EventHandler(this.cmdCopy_Click);
            // 
            // cmdCopy
            // 
            this.cmdCopy.Name = "cmdCopy";
            this.cmdCopy.Size = new System.Drawing.Size(207, 22);
            this.cmdCopy.Text = "Copiar";
            this.cmdCopy.Click += new System.EventHandler(this.cmdCopy_Click);
            // 
            // cmdPaste
            // 
            this.cmdPaste.Name = "cmdPaste";
            this.cmdPaste.Size = new System.Drawing.Size(207, 22);
            this.cmdPaste.Text = "Pegar";
            this.cmdPaste.Click += new System.EventHandler(this.cmdPaste_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(204, 6);
            // 
            // cmdCollapse
            // 
            this.cmdCollapse.Name = "cmdCollapse";
            this.cmdCollapse.Size = new System.Drawing.Size(207, 22);
            this.cmdCollapse.Text = "Colapsar";
            this.cmdCollapse.Click += new System.EventHandler(this.cmdCollapse_Click);
            // 
            // cmdExpand
            // 
            this.cmdExpand.Name = "cmdExpand";
            this.cmdExpand.Size = new System.Drawing.Size(207, 22);
            this.cmdExpand.Text = "Expandir";
            this.cmdExpand.Click += new System.EventHandler(this.cmdExpand_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(204, 6);
            // 
            // cmdBreakpoint
            // 
            this.cmdBreakpoint.Name = "cmdBreakpoint";
            this.cmdBreakpoint.Size = new System.Drawing.Size(207, 22);
            this.cmdBreakpoint.Text = "Añadir/Quitar Breakpoint";
            this.cmdBreakpoint.Click += new System.EventHandler(this.cmdBreakpoint_Click);
            // 
            // mnuDropNode
            // 
            this.mnuDropNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdMoveAsChild,
            this.cmdMoveAsPrevious,
            this.cmdMoveAsNext});
            this.mnuDropNode.Name = "mnuNodo";
            this.mnuDropNode.Size = new System.Drawing.Size(68, 70);
            // 
            // cmdMoveAsChild
            // 
            this.cmdMoveAsChild.Name = "cmdMoveAsChild";
            this.cmdMoveAsChild.Size = new System.Drawing.Size(67, 22);
            // 
            // cmdMoveAsPrevious
            // 
            this.cmdMoveAsPrevious.Name = "cmdMoveAsPrevious";
            this.cmdMoveAsPrevious.Size = new System.Drawing.Size(67, 22);
            // 
            // cmdMoveAsNext
            // 
            this.cmdMoveAsNext.Name = "cmdMoveAsNext";
            this.cmdMoveAsNext.Size = new System.Drawing.Size(67, 22);
            // 
            // searchBox
            // 
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox.Location = new System.Drawing.Point(0, 0);
            this.searchBox.Name = "searchBox";
            this.searchBox.NullText = "Buscar...";
            this.searchBox.Size = new System.Drawing.Size(500, 20);
            this.searchBox.TabIndex = 6;
            this.searchBox.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
            // 
            // treeView
            // 
            this.treeView.AllowDragDrop = true;
            this.treeView.AllowDrop = true;
            this.treeView.AllowEdit = true;
            this.treeView.CheckBoxes = true;
            this.treeView.ContextMenuStrip = this.mnuNodo;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.EnableKineticScrolling = true;
            this.treeView.FullRowSelect = false;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(0, 20);
            this.treeView.Margin = new System.Windows.Forms.Padding(0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(500, 649);
            this.treeView.SpacingBetweenNodes = -1;
            this.treeView.TabIndex = 7;
            this.treeView.Editing += new Telerik.WinControls.UI.TreeNodeEditingEventHandler(this.TreeView_Editing);
            this.treeView.Edited += new Telerik.WinControls.UI.TreeNodeEditedEventHandler(this.TreeView_Edited);
            this.treeView.ValueValidating += new Telerik.WinControls.UI.TreeNodeValidatingEventHandler(this.TreeView_ValueValidating);
            this.treeView.DragEnding += new Telerik.WinControls.UI.RadTreeView.DragEndingHandler(this.TreeView_DragEnding);
            this.treeView.DragEnded += new Telerik.WinControls.UI.RadTreeView.DragEndedHandler(this.TreeView_DragEnded);
            this.treeView.SelectedNodeChanged += new Telerik.WinControls.UI.RadTreeView.RadTreeViewEventHandler(this.TreeView_SelectedNodeChanged);
            this.treeView.NodeMouseDoubleClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.TreeView_NodeMouseDoubleClick);
            this.treeView.NodeCheckedChanged += new Telerik.WinControls.UI.TreeNodeCheckedEventHandler(this.TreeView_NodeCheckedChanged);
            this.treeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView_DragDrop);
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView_DragEnter);
            this.treeView.DragOver += new System.Windows.Forms.DragEventHandler(this.treeView_DragOver);
            this.treeView.NodeExpandedChanged += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.TreeView_NodeExpandedChanged);

            // 
            // EditorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.searchBox);
            this.Name = "EditorView";
            this.Size = new System.Drawing.Size(500, 669);
            this.mnuNodo.ResumeLayout(false);
            this.mnuDropNode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip mnuNodo;
        private System.Windows.Forms.ToolStripMenuItem cmdUp;
        private System.Windows.Forms.ToolStripMenuItem cmdDown;
        private System.Windows.Forms.ToolStripSeparator cmdSeparator1;
        private System.Windows.Forms.ToolStripMenuItem cmdRemove;
        private System.Windows.Forms.ToolStripSeparator cmdSeparator2;
        private System.Windows.Forms.ToolStripMenuItem cmdCut;
        private System.Windows.Forms.ToolStripMenuItem cmdCopy;
        private System.Windows.Forms.ToolStripMenuItem cmdPaste;
        private System.Windows.Forms.ContextMenuStrip mnuDropNode;
        private System.Windows.Forms.ToolStripMenuItem cmdMoveAsChild;
        private System.Windows.Forms.ToolStripMenuItem cmdMoveAsPrevious;
        private System.Windows.Forms.ToolStripMenuItem cmdMoveAsNext;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cmdCollapse;
        private System.Windows.Forms.ToolStripMenuItem cmdExpand;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cmdBreakpoint;
        private Telerik.WinControls.UI.RadTextBox searchBox;
        private Telerik.WinControls.UI.RadTreeView treeView;
    }
}
