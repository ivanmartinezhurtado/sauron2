﻿using Comunications.Ethernet;
using Dezac.Core.Utility;
using Dezac.Tests.Parameters;
using log4net;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(TelnetInitializationActionDescription))]
    public class TelnetInitialization : ActionBase
    {
        [Category("3.1 Instance")]
        [Description("Nombre del equipo")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre de usuario")]
        [DefaultValue("root")]
        public string UserName { get; set; }

        [Category("3. Configuration")]
        [Description("Contraseña")]
        [DefaultValue("root")]
        public string Password { get; set; }

        [Category("3. Configuration")]
        [Description("Número IP del Host")]
        [DefaultValue("100.0.0.1")]
        public string HostName { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para hacer que el Host sea fijo")]
        [DefaultValue(false)]
        public bool FixHostName { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            if (Context.Variables.ContainsKey(ConstantsParameters.TestInfo.DEVICE_IP) || !FixHostName )
                HostName = Context.Variables.Get(ConstantsParameters.TestInfo.DEVICE_IP).ToString();

            TelnetConnection telnet = null;
            testBase.SamplerWithCancel((p) =>
            {
                Logger.InfoFormat("Conectamos por Telnet a la IP {0}:23", HostName);
                telnet = new TelnetConnection(HostName, 23);
                return true;
            }, string.Format("Error no se ha podido conectar con el equipo a la IP {0}", HostName), 3, 500, 1000, false, false);

            telnet.WriteLineString(" ");

            testBase.SamplerWithCancel((p) =>
            {
                Logger.InfoFormat("Nos logeamos en el equipo por Telnet (root)");
                telnet.Login(UserName, Password, 3000);
                return true;

            }, "Error no se ha podido validar el login del equipo", 15, 4000, 1000, false, false);

            Context.Variables.AddOrUpdate(DeviceName, telnet);
        }
    }

    public class TelnetInitializationActionDescription : ActionDescription
    {
        public override string Name { get { return "TelnetInitialization"; } }
        public override string Description { get { return "Inicializa una instancia de las comunicaciones Telnet"; } }
        public override string Category { get { return "Tools/Ethernet/Telnet"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
