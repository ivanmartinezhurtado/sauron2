﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ValidateMeasuresAvgTolDescription))]
    public class ValidateMeasuresAvgTol : ActionBase
    {
        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInput { get; set; }

        [Category("4. Results")]
        [Description("Descripción de las unidades de la variable a obtener")]
        public ParamUnidad Unit { get; set; }

        [Category("3.1 Margins")]
        [Description("Margen de media de la medida en hacer el action")]
        public ActionVariable Average { get; set; } = new ActionVariable();

        [Category("3.1 Margins")]
        [Description("Margen de tolerancia de la medida en hacer el action")]
        public ActionVariable Tolerance { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            double variable = 0;

            if (Unit != 0)
                variable = Convert.ToDouble(VariablesTools.ResolveValue(DataInput, null, Unit));
            else
                variable = Convert.ToDouble(VariablesTools.ResolveValue(DataInput));

            var name = DataInput.Trim().Replace("$", "");

            if (Average == null || Average.IsKeyNullOrEmpty)
            {
                if (Unit == 0)
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("FALTA UNIDAD PARA CREAR PARAMETRO EN BBDD").Throw();
                testBase.Margenes.GetDouble(string.Format("{0}_AVG", name), variable, Unit);

                Average = string.Format("$Margenes.{0}_AVG", name);
            }
            if (Tolerance == null || Tolerance.IsKeyNullOrEmpty)
            {
                if (Unit == 0)
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("FALTA UNIDAD PARA CREAR PARAMETRO EN BBDD").Throw();
                testBase.Margenes.GetDouble(string.Format("{0}_TOL", name), 5, ParamUnidad.PorCentage);

                Tolerance = string.Format("$Margenes.{0}_TOL", name);
            }

            var average = Convert.ToDouble(Average.Value);
            var tolerancia = Convert.ToDouble(Tolerance.Value);
            var adjustValueTestPoint = new AdjustValueDef(name, average, tolerancia, 0, Unit);
            adjustValueTestPoint.Value = variable;

            testBase.Resultado.Set(adjustValueTestPoint, true, false);

            if (!adjustValueTestPoint.IsCorrect())
                Error().UUT.MEDIDA.MARGENES(adjustValueTestPoint.Name).Throw();                     
        }
    }

    public class ValidateMeasuresAvgTolDescription : ActionDescription
    {
        public override string Name { get { return "ValidateMeasuresAvgTol"; } }
        public override string Description { get { return "Acción genérica para comparar variables o resultados con unos márgenes de Average y Tolerancia"; } }
        public override string Category { get { return "Actions/Variables/Validate"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
        public override string Dependencies { get { return "InitTestBase;InitProductAction;InitBastidorMultipleAction"; } }
    }
}
