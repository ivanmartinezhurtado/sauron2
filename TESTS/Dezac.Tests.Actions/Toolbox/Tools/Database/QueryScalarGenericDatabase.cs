﻿using log4net;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Drawing;
using TaskRunner;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(QueryScalarGenericDatabaseDescription))]
    public class QueryScalarGenericDatabase : ActionBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("SaveGenericDatabaseAction");

        public enum DatabaseTypeEnum
        {
            SQLServer,
            MySql,
            SqLite,
            Oracle,
            Oledb,
            Csv
        }

        [Category("3.1 Query")]
        [Description("Consulta en leguaje SQL")]
        public ActionVariable CommandSQL { get; set; } = new ActionVariable();

        [Category("3.2 Database Provider")]
        [Description("Tipo de base de datos")]
        [DefaultValue(DatabaseTypeEnum.SQLServer)]
        public DatabaseTypeEnum DatabaseType { get; set; }

        [Category("3.2 Database Provider")]
        [Description("Cadena de conexión desde el fichero de configuración o toda la cadena de conexión")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ConnectionString { get; set; }

        [DefaultValue("")]
        [Category("3. Configuration")]
        [Description("Variable donde se guradará el valor")]
        public string DataInput { get; set; }

        public override void Execute()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"]?.ConnectionString ?? ConnectionString;

            DbProviderFactory db = DbProviderFactories.GetFactory(GetProviderName());

            var context = SequenceContext.Current;

            using (var cn = db.CreateConnection())
            {
                cn.ConnectionString = connectionString;
                cn.Open();

                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = VariablesTools.ResolveText(CommandSQL.Value.ToString());
                    var result = cmd.ExecuteScalar();
                    logger.InfoFormat("result: {0}", result);

                    Variables.AddOrUpdate(DataInput, result);
                }
            }
        }

        private string GetProviderName()
        {
            string providerName;

            switch (DatabaseType)
            {
                case DatabaseTypeEnum.Oracle:
                    providerName = "System.Data.OracleClient";
                    break;
                case DatabaseTypeEnum.MySql:
                    providerName = "System.Data.MySql";
                    break;
                case DatabaseTypeEnum.SqLite:
                    providerName = "System.Data.SqLite";
                    break;
                case DatabaseTypeEnum.Oledb:
                    providerName = "System.Data.OleDb";
                    break;
                default:
                    providerName = "System.Data.SqlClient";
                    break;
            }

            return providerName;
        }

        public class QueryScalarGenericDatabaseDescription : ActionDescription
        {
            public override string Name { get { return "QueryScalarGenericDatabase"; } }
            public override string Category { get { return "Tools/Database"; } }
            public override string Description { get { return "Consulta genérica a una base de datos para que devuelva un valor escalar"; } }
            public override Image Icon { get { return Properties.Resources.Database; } }
        }

    }
}