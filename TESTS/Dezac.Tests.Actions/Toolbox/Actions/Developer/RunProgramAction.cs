﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(RunProgramActionDescription))]
    public class RunProgramAction : ActionBase
    {
        [Category("3.1. Program")]
        [Description("Nombre o ruta del programa a ejecutar")]
        public ActionVariable Program { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Parámetros de entrada que necesita el método a ejecutar")]
        public Args[] Arguments { get; set; }

        [Category("3. Configuration")]
        [Description("Directorio donde se debe ejecutar el programa(si es necesario)")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable WorkingDirectory { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Esperar a que finalice la ejecución del programa")]
        public bool WaitToEndPogram { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del subprograma a esperar que finalice")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string WaitToEndSubProgramName { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo mínimo de la ejecución del programa en ms")]
        public double? ExecutionMinTime { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo máximo de la ejecución del programa en ms")]
        public double? ExecutionMaxTime { get; set; }

        [Category("3. Configuration")]
        [Description("Visualiza la pantalla del shell de windows")]
        public bool UseShellExecute { get; set; }

        public override void Execute()
        {
            Process ps, subPs;
            double timePs, timeSubPs = 0;

            var pathProgram = Program.Value.ToString();
            var workingdDir = WorkingDirectory.Value.ToString();
            var args = "";

            if (string.IsNullOrEmpty(pathProgram))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("Program").Throw();

            if (Arguments != null && Arguments.Count() > 0)
            {
                var argsList = Arguments.Select(p => VariablesTools.ResolveText(p.Value)).ToList();
                argsList.ForEach(p => args += p + " ");
            }

            if (WaitToEndPogram == false)
                ps = Process.Start(pathProgram, args);
            else
            {
                var psi = new ProcessStartInfo(pathProgram, args);
                psi.UseShellExecute = UseShellExecute;
                psi.CreateNoWindow = true;
                psi.WorkingDirectory = workingdDir;

                ps = new Process();
                ps.StartInfo = psi;
                ps.EnableRaisingEvents = true;
                ps.Start();
                ps.WaitForExit();

                if (!string.IsNullOrEmpty(WaitToEndSubProgramName))
                {
                    Thread.Sleep(500);
                    subPs = Process.GetProcessesByName(WaitToEndSubProgramName).FirstOrDefault();

                    if (subPs == null)
                    Error().PROCESO.ACTION_EXECUTE.ERROR_SUBPROCESO(string.Format("No se ha ejecutado el Subproceso especificado : {0}", WaitToEndSubProgramName)).Throw();

                    subPs.EnableRaisingEvents = true;
                    subPs.WaitForExit();
                    timeSubPs = Math.Round((subPs.ExitTime - subPs.StartTime).TotalMilliseconds);
                    subPs.Dispose();
                }
            }
            
            timePs = Math.Round((ps.ExitTime - ps.StartTime).TotalMilliseconds);            
            var totalTime = timePs + timeSubPs;
            ps.Dispose();

            if (ExecutionMinTime.HasValue)
                Assert.AreGreater(totalTime, ExecutionMinTime.Value, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Tiempo de ejecución menor que el especificado"));
            if (ExecutionMaxTime.HasValue)
                Assert.AreLesser(totalTime, ExecutionMaxTime.Value, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Tiempo de ejecución mayor que el especificado"));
        }
        
       
        public class Args
        {
            [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
            public string Value { get; set; }
        }
    }

    public class RunProgramActionDescription : ActionDescription
    {
        public override string Name { get { return "RunProgram"; } }
        public override string Category { get { return "Actions/Developer"; } }
        public override string Description { get { return "Este action ejecuta un programa en Windows"; } }
        public override Image Icon { get { return Properties.Resources.Other_xml_icon; } }
    }
}
