namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PRODUCTOBINARIO")]
    public partial class PRODUCTOBINARIO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPRODUCTO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VERSION { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime FECHA { get; set; }

        [StringLength(20)]
        public string BINARIO { get; set; }

        [StringLength(20)]
        public string VERBINARIO { get; set; }

        [Required]
        [StringLength(1)]
        public string ACTIVO { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }
    }
}
