﻿namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.CAJAEQUIPO")]
    public partial class CAJAEQUIPO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMCAJA { get; set; }

        [Key]
        [Column(Order = 1)]
        [Required]
        public int NUMEQUIPO { get; set; }

        [Required]
        [StringLength(20)]
        public string NROSERIE { get; set; }

        public char SITUACION { get; set; }

        [Required]
        [StringLength(20)]
        public string NROSERIEORIGINAL { get; set; }
    }
}