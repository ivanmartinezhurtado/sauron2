﻿namespace Dezac.Core.Exceptions
{
    public static class ConstantsExceptions
    {
        public static class Categoria
        {
            public static string[] HARDWARE = new string[] { "01", "HARDWARE" };
            public static string[] SOFTWARE = new string[] { "02", "SOFTWARE" };
            public static string[] UUT = new string[] { "03", "EQUIPO EN TEST" };
            public static string[] PROCESO = new string[] { "04", "PROCESO" };
        }

        public static class HardwareLevel0
        {
            public static string[] TORRE = new string[] { "01", "TORRE" };
            public static string[] MANGUERA = new string[] { "02", "MANGUERA" };
            public static string[] UTILLAJE = new string[] { "03", "UTILLAJE" };
            public static string[] INSTRUMENTOS = new string[] { "04", "INSTRUMENTOS" };
            public static string[] PC = new string[] { "05", "PC" };
            public static string[] CONFIGURACION = new string[] { "06", "CONFIGURACION" };
        }

        public static class HardwareLevel1
        {
            public static string[] COMUNICACIONES = new string[] { "01", "COMUNICACIONES" };
            public static string[] ELECTRICO = new string[] { "02", "ELECTRICO" };
            public static string[] NEUMATICO = new string[] { "03", "NEUMATICO" };
            public static string[] MECANICO = new string[] { "04", "MECANICO" };
            public static string[] BLOQUE_SEGURIDAD = new string[] { "05", "BLOQUE DE SEGURIDAD" };
            public static string[] RDNI_INCORRECTO = new string[] { "06", "RDNI INCORRECTO" };
            public static string[] ENTRADAS_DIGITALES = new string[] { "07", "ENTRADAS DIGITALES" };
            public static string[] SALIDAS_DIGITALES = new string[] { "08", "SALIDAS DIGITALES" };
            public static string[] DETECCION_INSTRUMENTO = new string[] { "09", "DETECCION INSTRUMENTO" };
            public static string[] CONSIGNAR_FUENTE = new string[] { "10", "CONSIGNAR LA FUENTE" };
            public static string[] CONFIGURACION_INSTRUMENTO = new string[] { "11", "CONFIGURACION DEL INSTRUMENTO" };
            public static string[] MEDIDA_INSTRUMENTO = new string[] { "12", "MEDIDA INSTRUMENTO" };
            public static string[] PNEUMATICA = new string[] { "13", "PISTONES PNEUMATICOS" };
            public static string[] DETECCION_EQUIPO = new string[] { "14", "DETECCION EQUIPO" };
            public static string[] FUNCION_PTE = new string[] { "15", "FUNCION PTE" };
            public static string[] LASER = new string[] { "16", "LASER" };
            public static string[] LECTURA_FUENTE = new string[] { "17", "LECTURA_FUENTE" };
            public static string[] CALIBRACION_CADUCADA = new string[] { "18", "CALIBRACION_CADUCADA" };
            public static string[] ASISTENCIA_CADUCADA = new string[] {"19", "ASISTENCIA_CADUCADA" };
            public static string[] ROUTER = new string[] { "20", "ROUTER" };
            public static string[] ADAPTADOR_DE_RED_ETHERNET = new string[] { "21", "ADAPTADOR_DE_RED_ETHERNET" };
            public static string[] ADAPTADOR_DE_RED_WIFI = new string[] { "22", "ADAPTADOR_DE_RED_WIFI" };
            public static string[] VECTOR_HARDWARE_GRABADO = new string[] { "23", "VECTOR_HARDWARE_GRABADO" };
            public static string[] BORNE = new string[] { "24", "BORNE" };
        }

        public static class SoftwareLevel0
        {
            public static string[] NO_CONTROLADO = new string[] { "01", "NO CONTROLADO" };
            public static string[] BBDD = new string[] { "02", "BASE DE DATOS" };
            public static string[] FALLO_RED = new string[] { "03", "CONEXION_ETHERNET" };
            public static string[] BARTENDER = new string[] { "04", "BARTENDER" };
            public static string[] HALCON = new string[] { "05", "HALCON" };
            public static string[] SEQCUENCIA_TEST = new string[] { "06", "SEQUENCIA DE TEST" };
            public static string[] DRIVER = new string[] { "07", "DRIVER" };
            public static string[] SAURON = new string[] { "08", "SAURON SYSTEM" };
            public static string[] SISMA = new string[] { "09", "SISMA" };
            public static string[] STM32_FLASH_LOADER = new string[] { "10", "STM32_FLASH_LOADER" };
            public static string[] WEB_SERVICE = new string[] { "11", "WEB_SERVICE" };
            public static string[] ERROR_TRANSFERENCIA = new string[] { "12", "ERROR TRANSFERENCIA" };
            public static string[] FIRMWARE = new string[] { "13", "FIRMWARE" };
            public static string[] IP = new string[] { "14", "IP" };
            public static string[] CPU = new string[] { "15", "CPU" };
            public static string[] ESP32_FLASH_LOADER = new string[] { "16", "STM32_FLASH_LOADER" };
        }

        public static class SoftwareLevel1
        {
            public static string[] TORRE = new string[] { "01", "TORRE" };
            public static string[] INSTRUMENTOS = new string[] { "02", "INSTRUMENTOS" };
            public static string[] CAMARA = new string[] { "03", "CAMARA" };
            public static string[] IMPRESORAS = new string[] { "04", "IMPRESORAS" };
            public static string[] ROUTER = new string[] { "05", "ROUTER" };
            public static string[] INSTANCIA_CLASE = new string[] { "06", "INSTANCIA DE LA CLASE" };
            public static string[] SAMPLER_WITH_CANCEL_MAL_IMPLEMENTADO = new string[] { "07", "SAMPLER MAL IMPLEMENTADO" };
            public static string[] CONFIGURACION_INCORRECTA = new string[] { "08", "CONFIGURACION INCORRECTA" };
            public static string[] VARIABLE_NO_ENCONTRADA = new string[] { "09", "VARIABLE NO ENCONTRADA" };
            public static string[] VARIABLE_ES_NULA = new string[] { "10", "VARIABLE NO PUEDE SER NULLA" };
            public static string[] CONSULTA_PGI = new string[] { "11", "CONSULTA DEL PGI" };
            public static string[] PARALELISMO_CANCELADO = new string[] { "11", "PARALELISMO CANCELADO" };
            public static string[] NO_INSTALADO = new string[] { "12", "NO INSTALADO" };
            public static string[] FICHERO_NO_ENCONTRADO = new string[] { "13", "FICHERO_NO_ENCONTRADO" };
            public static string[] ERROR_GRABACION = new string[] { "14", "ERROR EN LA GRABACION" };
            public static string[] CARGA_SERVICIO = new string[] {"15" ,"CARGA DEL SERVICIO" };
            public static string[] ACTION_MAL_CONFIGURADO = new string[] { "16", "ACTION MAL CONFIGURADO" };
            public static string[] NOMBRE_DE_LA_VARIABLE_NULO = new string[] { "17", "EL NOMBRE DE UNA VARIABLE NO PUEDE SER NULO" };
            public static string[] CREACION_O_MODIFICACION_DE_VARIABLE = new string[] { "18", "CREACION O MODIFICACION DE VARIABLE" };
            public static string[] FALTA_TEST_BASE = new string[] { "19", "FALTA TEST BASE" };
            public static string[] VERSION_INCORRECTA = new string[] { "20", "VERSION INCORRECTA" };
            public static string[] WIFI = new string[] { "21", "WIFI" };
            public static string[] TRESG = new string[] { "22", "3G" };
            public static string[] DETECCION = new string[] { "23", "DETECCION" };
        }

        public static class UUTLevel0
        {
            public static string[] FIRMWARE = new string[] { "01", "FIRMWARE" };
            public static string[] MEDIDA = new string[] { "02", "MEDIDA" };
            public static string[] CONSUMO = new string[] { "03", "CONSUMO" };
            public static string[] COMUNICACIONES = new string[] { "04", "COMUNICACIONES" };
            public static string[] HARDWARE = new string[] { "05", "HARDWARE" };
            public static string[] CALIBRACION = new string[] { "06", "CALIBRACION" };
            public static string[] AJUSTE = new string[] { "07", "AJUSTE" };
            public static string[] DISPARO = new string[] { "08", "VERIFICACION DISPARO" };
            public static string[] CONFIGURACION = new string[] { "09", "CONFIGURACION" };
            public static string[] EEPROM = new string[] { "10", "EEPROM" };
            public static string[] NUMERO_DE_SERIE = new string[] { "11", "NUMERO DE SERIE" };
            public static string[] NUMERO_DE_BASTIDOR = new string[] { "12", "NUMERO DE BASTIDOR" };
            public static string[] MEMORIA = new string[] { "13", "MEMORIA" };
            public static string[] SIM = new string[] { "14", "SIM" };
        }

        public static class UUTLevel1
        {
            public static string[] NO_DISPARA = new string[] { "01", "NO HA DISPARADO" };
            public static string[] MARGENES = new string[] { "02", "FUERA DE MARGENES" };
            public static string[] SETUP = new string[] { "03", "NO SE HA GRABADO SETUP" };
            public static string[] YA_DISPARADO = new string[] { "04", "YA ESTABA DISPARADO" };
            public static string[] MARGENES_INFERIORES = new string[] { "05", "FUERA DE MARGENES INFERIORES" };
            public static string[] MARGENES_SUPERIORES = new string[] { "06", "FUERA DE MARGENES SUPERIORES" };
            public static string[] FLAG_TEST = new string[] { "07", "FLAG TEST" };
            public static string[] INICIO_SESION = new string[] { "08", "INICIO DE SESION" };
            public static string[] TIMEOUT = new string[] { "09", "TIMEOUT" };
            public static string[] INICIALIZACION = new string[] { "10", "INICIALIZACION" };
            public static string[] RECUPERACION_MODO_FABRICA = new string[] { "11", "RECUPERACION MODO FABRICA" };
            public static string[] KEYBOARD = new string[] { "12", "TECLADO" };
            public static string[] LEDS = new string[] { "13", "LEDS" };
            public static string[] RELE = new string[] { "14", "RELES" };
            public static string[] DIGITAL_INPUTS = new string[] { "15", "ENTRADAS DIGITALES" };
            public static string[] DIGITAL_OUTPUTS = new string[] { "16", "SALIDAS DIGITALES" };
            public static string[] TOROIDAL = new string[] { "17", "TOROIDAL" };
            public static string[] USB = new string[] { "18", "USB" };
            public static string[] RTC = new string[] { "19", "RTC" };
            public static string[] EXTERNAL_MEMORY = new string[] { "20", "MEMORIA EXTERNA" };
            public static string[] ETHERNET = new string[] { "21", "ETHERNET" };
            public static string[] BORRADO = new string[] { "22", "BORRADO INCORRECTO" };
            public static string[] ALARMA = new string[] { "23", "ALARMA" };
            public static string[] SOAP_SERVICE = new string[] { "24", "SOAP_SERVICE" };
            public static string[] VECTOR_HARDWARE = new string[] { "25", "VECTOR DE HARDWARE" };
            public static string[] PARAMETROS_NO_GRABADOS = new string[] { "26", "PARAMETROS_NO_GRABADOS" };
            public static string[] DISPLAY = new string[] { "27", "DISPLAY" };
            public static string[] NO_COMUNICA = new string[] { "28", "NO_HAY_COMUNICACION" };
            public static string[] NO_ENCIENDE = new string[] { "29", "NO_ENCIENDE" };
            public static string[] UPGRADE = new string[] { "30", "UPGRADE" };
            public static string[] ERROR_UPLOAD = new string[] { "31", "ERROR UPLOAD" };
            public static string[] ERROR_DOWNLOAD = new string[] { "32", "ERROR DOWNLOAD" };
            public static string[] NO_CORRESPONDE_CON_ENVIO = new string[] { "33", "NO CORRESPONDE CON ENVIO" };
            public static string[] BLUETOOTH = new string[] { "34", "BLUETOOTH" };
            public static string[] GRABACION_SINCRONISMO = new string[] { "35", "GRABACION SINCRONISMO" };
            public static string[] BAUDRATE = new string[] { "36", "BAUDRATE" };
            public static string[] NO_COINCIDE = new string[] { "37", "NO COINCIDE" };
            public static string[] DESCRIPCION_INCORRECTA = new string[] { "38", "DESCRIPCION INCORRECTA" };
            public static string[] NO_GRABADO = new string[] { "39", "NO GRABADO" };
            public static string[] GPS = new string[] { "40", "GPS" };
            public static string[] RELOJ = new string[] { "40", "RELOJ" };
            public static string[] CANCELADO_POR_USUARIO = new string[] { "41", "CANCELADO POR USUARIO" };
        }

        public static class PROCESLevel0
        {
            public static string[] PARAMETROS_ERROR = new string[] { "01", "PARAMETROS ERROR" };
            public static string[] OPERARIO = new string[] { "02", "OPERARIO" };
            public static string[] ACTION_EJECUTANDO = new string[] { "03", "ACTION EJECUTANDO" };
            public static string[] MATERIAL = new string[] { "04", "FALLO DE MATERIAL" };
            public static string[] FALTA_PROCESO = new string[] { "05", "FALTA PROCESO" };
            public static string[] ACTION_EXECUTE = new string[] { "06", "ACTION EXECUTE" };
            public static string[] TRAZABILIDAD = new string[] { "07", "TRAZABILIDAD" };
            public static string[] PRODUCTO = new string[] { "08", "PRODUCTO" };
            public static string[] FAMILIA = new string[] { "09", "FAMILIA" };
            public static string[] LINEA = new string[] { "10", "LINEA" };
            public static string[] FICHERO_NO_ENCONTRADO = new string[] { "11", "FICHERO NO ENCONTRADO" };
        }

        public static class PROCESLevel1
        {
            public static string[] VERSION_FIRMWARE = new string[] { "01", "VERSION FIRMWARE" };
            public static string[] TEST_CANCELADO = new string[] { "02", "TEST CANCELADO" };
            public static string[] PARAMETRO_INCOHERENTE = new string[] { "03", "PARAMETRO INCOHERENTE" };
            public static string[] FALTA_VALOR_PARAMETRO = new string[] { "04", "FALTA VALOR DE UN PARAMETRO" };
            public static string[] FALTA_ANCLA = new string[] { "05", "EL EQUIPO NO LLEVA EL ANCLA" };
            public static string[] COLOCACION_INCORRECTA_EQUIPO = new string[] { "06", "COLOCACION_INCORRECTA_EQUIPO" };
            public static string[] ETIQUETA_INCORRECTA = new string[] { "07", " ETIQUETA INCORRECTA" };
            public static string[] FALTA_TAPA = new string[] { "08", "EL EQUIPO NO LLEVA TAPA" };
            public static string[] EMBALAJE_INDIVIDUAL_INCORRECTO = new string[] { "09", "LA ETIQUETA DE EMBALAJE NO ES CORRECTA" };
            public static string[] TEST_PLACAS = new string[] { "10", "TEST DE PLACAS" };
            public static string[] DISPOSITIVO_NO_ENCONTRADO = new string[] { "11", "DISPOSITIVO NO ENCONTRADO" };
            public static string[] DISPOSITIVO_REPETIDO = new string[] { "12", "DISPOSITIVO REPETIDO" };
            public static string[] PRODUCTO_INCORRECTO = new string[] { "13", "PRODUCTO INCORRECTO" };
            public static string[] FASES_TEST_INCORRECTAS = new string[] { "14"," FASES DE TEST INCORRECTAS" };
            public static string[] ESTRUCTURA_COMPONENTES = new string[] { "15", "ESTRUCTURA COMPONENTES" };
            public static string[] PLANTILLAS_LASER = new string[] { "16", "PLANTILLAS LASER" };
            public static string[] INSTRUCCIONES = new string[] { "17", "INSTRUCCIONES" };
            public static string[] BINARIOS = new string[] { "18", "BINARIOS" };
            public static string[] NO_VALIDADO_IDP = new string[] { "19", "NO VALIDADO IDP" };
            public static string[] BLOQUEADO = new string[] { "20", "BLOQUEADO" };
            public static string[] VALOR_INCORRECTO = new string[] { "21", "VALOR INCORRECTO" };
            public static string[] EXCEPTION_NOT_CONTROLLED = new string[] { "22", "EXCEPCION NO CONTROLADA" };
            public static string[] FALTA_ARCHIVO_ETIQUETA = new string[] { "23", "FALTA ARCHIVO ETIQUETA" };
            public static string[] ERROR_SUBPROCESO = new string[] { "24", "ERROR SUBPROCESO" };
            public static string[] NO_ORDEN_FABRICACION_NO_PRODUCTO = new string[] { "25", "NO ORDEN FABRICACION NO PRODUCTO" };
            public static string[] FALTA_ARCHIVO = new string[] { "26", "FALTA ARCHIVO" };
            public static string[] NO_COINCIDE = new string[] { "27", "NO COINCIDE" };
            public static string[] BIEN = new string[] { "28", "BIEN" };
            public static string[] LED = new string[] { "29", "LED" };
        }
    }
}
