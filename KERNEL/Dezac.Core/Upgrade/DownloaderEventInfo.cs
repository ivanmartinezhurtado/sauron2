﻿using System;

namespace Dezac.Core.Upgrade
{
    public class DownloadEventInfo : EventArgs
    {
        public DownloadEventType EventType { get; set; }
        public Exception Error { get; internal set; }
        public string Message { get; set; }
        public int? CurrentRecord { get; set; }
        public int? MaxRecords { get; set; }

        public DownloadEventInfo()
        {
        }

        public DownloadEventInfo(string message)
        {
            EventType = DownloadEventType.Info;
            Message = message;
        }

        public DownloadEventInfo(string message, int? record = null, int? maxRecords = null)
        {
            EventType = DownloadEventType.Info;
            Message = message;
            CurrentRecord = record;
            MaxRecords = maxRecords;
        }
    }

    public enum DownloadEventType
    {
        Info,
        Completed,
        Aborted,
        Exception
    }
}
