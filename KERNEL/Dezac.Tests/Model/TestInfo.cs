﻿using Dezac.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace Dezac.Tests.Model
{
    public class TestInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string numSerie;
        private string numSerieInterno;
        private string numSerieFamilia;
        private int? numFabricacion;
        private UInt64? numBastidor;
        private string numMAC;
        private string numMAC_PLC;
        private string numMAC_WiFi;
        private string numMAC_3G;
        private int? bacNetID;
        private int? numTest;
        private int? numTestFase;
        private string codCircutor;
        private string costumerCode2;
        private string deviceID;
        private string perifericNumber;
        private string labelTest;
        private int? reportVersion;
        private labels typeLabel;
        private string printerLabel;
        private string printerCharacteristics;
        private string printerIndividualPackage;
        private string printerPackingPackage;
        private string printerReport;
        private string reportName;
        private string sSID;
        private string pSW;
        private string producto;
        private string productoVersion;
        private string firmwareVersion;
        private string hardwareVersion;
        private string fechaFabricacion;
        private string factoryIP;
        private int identicalCopiesOfLabel;
        private Dictionary<string, string> numSerieSubconjunto;
        private string imei;
        private List<ComponentsEstructureInProduct> listComponents;
        private Dictionary<string, string> listCameras;
        private string idActuacionReparacion;
        private bool saveBBDD;
        private bool numSerieAssigned;
        private string guid;
        private string descCom;
        private string refVenta;
        private string codigoBcn;

        public string TestVersion { get; set; }
        public DateTime StartTime { get; set; }
        public TimeSpan Elapsed { get { return DateTime.Now - StartTime; } }
        public int NumInstance { get; set; }
        public bool IsRetry { get; set; }

        public bool NumSerieAssigned
        {
            get { return numSerieAssigned; }
            set { numSerieAssigned = value; OnPropertyChanged("NumSerieAsigned"); }
        }

        public string NumSerie
        {
            get { return numSerie; }
            set { numSerie = value; OnPropertyChanged("NumSerie"); }
        }

        public string NumSerieInterno
        {
            get { return numSerieInterno; }
            set { numSerieInterno = value; OnPropertyChanged("NumSerieInterno"); }
        }

        public string NumSerieFamilia
        {
            get { return numSerieFamilia; }
            set { numSerieFamilia = value; OnPropertyChanged("NumSerieFamilia"); }
        }

        public int? NumFabricacion
        {
            get { return numFabricacion; }
            set { numFabricacion = value; OnPropertyChanged("NumFabricacion"); }
        }

        public UInt64? NumBastidor
        {
            get { return numBastidor; }
            set { numBastidor = value; OnPropertyChanged("NumBastidor"); }
        }

        public string NumMAC
        {
            get { return numMAC; }
            set { numMAC = value; OnPropertyChanged("NumMAC"); }
        }

        public string NumMACWifi
        {
            get { return numMAC_WiFi; }
            set { numMAC_WiFi = value; OnPropertyChanged("NumMAC_WIFI"); }
        }

        public string NumMAC3G
        {
            get { return numMAC_3G; }
            set { numMAC_3G = value; OnPropertyChanged("NumMAC_3G"); }
        }

        public string NumMAC_PLC
        {
            get { return numMAC_PLC; }
            set { numMAC_PLC = value; OnPropertyChanged("NumMAC_PLC"); }
        }

        public string SSID
        {
            get { return sSID; }
            set { sSID = value; OnPropertyChanged("SSID"); }
        }

        public string PSW
        {
            get { return pSW; }
            set { pSW = value; OnPropertyChanged("PSW"); }
        }

        public int? NumTest
        {
            get { return numTest; }
            set { numTest = value; OnPropertyChanged("NumTest"); }
        }

        public int? BacNetID
        {
            get { return bacNetID; }
            set { bacNetID = value; OnPropertyChanged("BacNetID"); }
        }

        public int? NumTestFase
        {
            get { return numTestFase; }
            set { numTestFase = value; OnPropertyChanged("NumTestFase"); }
        }

        public string CodCircutor
        {
            get { return codCircutor; }
            set { codCircutor = value; OnPropertyChanged("CodCircutor"); }
        }

        public string CostumerCode2
        {
            get { return costumerCode2; }
            set { costumerCode2 = value; OnPropertyChanged("CostumerCode2"); }
        }

        public string DeviceID
        {
            get { return deviceID; }
            set { deviceID = value; OnPropertyChanged("DeviceID"); }
        }

        public string PerifericNumber
        {
            get { return perifericNumber; }
            set { perifericNumber = value; OnPropertyChanged("PerifericNumber"); }
        }

        public string FirmwareVersion
        {
            get { return firmwareVersion; }
            set { firmwareVersion = value; OnPropertyChanged("FirmwareVersion"); }
        }

        public string HardwareVersion
        {
            get { return hardwareVersion; }
            set { hardwareVersion = value; OnPropertyChanged("HardwareVersion"); }
        }

        public string ProductoVersion
        {
            get { return productoVersion; }
            set { productoVersion = value; OnPropertyChanged("ProductoVersion"); }
        }

        public string Producto
        {
            get { return producto; }
            set { producto = value; OnPropertyChanged("Producto"); }
        }

        public string LabelTest
        {
            get { return reportName; }
            set { reportName = value; OnPropertyChanged("LabelTest"); }
        }

        public int IdenticalCopiesOfLabel
        {
            get { return identicalCopiesOfLabel; }
            set { identicalCopiesOfLabel = value; OnPropertyChanged("IdenticalCopiesOfLabel"); }
        }

        public string ReportName
        {
            get { return labelTest; }
            set { labelTest = value; OnPropertyChanged("ReportName"); }
        }

        public int? ReportVersion
        {
            get { return reportVersion; }
            set { reportVersion = value; OnPropertyChanged("ReportVersion"); }
        }

        public string PrinterLabel
        {
            get { return printerLabel; }
            set { printerLabel = value; OnPropertyChanged("PrinterLabel"); }
        }

        public string PrinterCharacteristics
        {
            get { return printerCharacteristics; }
            set { printerCharacteristics = value; OnPropertyChanged("PrinterIndividualPackage"); }
        }

        public string PrinterIndividualPackage
        {
            get { return printerIndividualPackage; }
            set { printerIndividualPackage = value; OnPropertyChanged("PrinterIndividualPackage"); }
        }

        public string PrinterPackingPackage
        {
            get { return printerPackingPackage; }
            set { printerPackingPackage = value; OnPropertyChanged("PrinterPackingPackage"); }
        }

        public string PrinterReport
        {
            get { return printerReport; }
            set { printerReport = value; OnPropertyChanged("PrinterReport"); }
        }

        public string FechaFabricacion
        {
            get { return fechaFabricacion; }
            set { fechaFabricacion = value; OnPropertyChanged("FechaFabricacion"); }
        }

        public string FactoryIP
        {
            get { return factoryIP; }
            set { factoryIP = value; OnPropertyChanged("FactoryIP"); }
        }

        public Dictionary<string, string> NumSerieSubconjunto
        {
            get { return numSerieSubconjunto; }
            set { numSerieSubconjunto = value; OnPropertyChanged("NumSerieSubconjunto"); }
        }

        public string IMEI
        {
            get { return imei; }
            set { imei = value; OnPropertyChanged("IMEI"); }
        }

        public string GUID
        {
            get { return guid; }
            set { guid = value; OnPropertyChanged("GUID"); }
        }

        public string DescripcionComercial
        {
            get { return descCom; }
            set { descCom = value; OnPropertyChanged("DescripcionComercial"); }
        }

        public string RefVenta
        {
            get { return refVenta; }
            set { refVenta = value; OnPropertyChanged("RefVenta"); }
        }

        public string CodigoBcn
        {
            get { return codigoBcn; }
            set { codigoBcn = value; OnPropertyChanged("CodigoBcn"); }
        }

        public List<ComponentsEstructureInProduct> ListComponents
        {
            get { return listComponents; }
            set { listComponents = value; OnPropertyChanged("ListComponents"); }
        }

        public labels TypeLabel
        {
            get { return typeLabel; }
            set { typeLabel = value; OnPropertyChanged("TypeLabel"); }
        }

        public string IdActuacionReparacion
        {
            get { return idActuacionReparacion; }
            set { idActuacionReparacion = value; OnPropertyChanged("IdActuacionReparacion"); }
        }

        public bool SaveBBDD
        {
            get { return saveBBDD; }
            set { saveBBDD = value; OnPropertyChanged("SaveBBDD"); }
        }

        public SynchronizationContext SC { get; set; }

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null && SC != null)
                SC.Post(_ => { PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); }, null);
        }

        public enum labels
        {
            Numero_Serie,
            Advertencia,
            Embalaje_Individual,
            Normativa_Seguridad,
            Test,
            Numero_Serie_Subconjunto,
            Otros
        }

        public enum FasesEnum
        {
            Ajustar_100 = 100,
            Verificar_120 = 120,
            Verificar2_121 = 121,
            Verificar3_122 = 122,
            VerificarMetering_129 = 129,
            Testear_080 = 80,
            Testear_2_081 = 81,
            Ensamblar_090 = 90,
            Lasear_150 = 150,
            Dielectrica_180 = 180,
            Grabar_070 = 70,
            Grabar2_071 = 71,
            Grabar_Despanelar_073 = 73,
            Grabar_Verificar_074 = 74,
            Grabar_Testear_075 = 75,
            Embalar_130 = 130,
            AOI_038 = 38,
            AOI2_039 = 39,
            Pametrizar_170 = 170,
            ProcesoGrabar_505 = 505,
            Reproceso_990 = 990
        }

        public enum TipoDocs
        {
            ProjectoDataman = 706,
            Hex = 120,
        }
    }
}
