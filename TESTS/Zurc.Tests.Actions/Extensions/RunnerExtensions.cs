﻿using Dezac.Tests.Extensions;
using Zurc.Data;

namespace Zurc.Tests.Actions.Extensions
{
    public class RunnerExtensions : IRunnerExtensions
    {
        public void SaveFileNumTestFase(int numTestFase, string name, string tipo, byte[] data)
        {
            using(var db = new ZurcService())
            {
                db.AddTestFaseFile(numTestFase, name, data, tipo);
            }
        }

        public void SaveFileVANumTestFase(int numTestFase, string name, string tipo, byte[] data)
        {
        }

        public void SaveProcessFaseFile(int numRegister, byte[] data)
        {
        }
    }
}
