namespace Dezac.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_VALORPARAMETRO")]
    public partial class T_VALORPARAMETRO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_VALORPARAMETRO()
        {
            T_VALORPARAMETROPRODUCTO = new HashSet<T_VALORPARAMETROPRODUCTO>();
            //T_VALORPARAMETROTOL1 = new HashSet<T_VALORPARAMETROTOL>();
            //T_VALORPARAMETROTOL2 = new HashSet<T_VALORPARAMETROTOL>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMGRUPO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARAM { get; set; }

        [Required]
        [StringLength(200)]
        public string VALOR { get; set; }

        [StringLength(200)]
        public string VALORINICIO { get; set; }

        public DateTime? FECHAALTA { get; set; }

        [StringLength(10)]
        public string IDCATEGORIA { get; set; }
        [JsonIgnore]
        public virtual T_GRUPOPARAMETRIZACION T_GRUPOPARAMETRIZACION { get; set; }

        public virtual T_PARAMETRO T_PARAMETRO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_VALORPARAMETROPRODUCTO> T_VALORPARAMETROPRODUCTO { get; set; }
        //[JsonIgnore]
        //public virtual T_VALORPARAMETROTOL T_VALORPARAMETROTOL { get; set; }
        //[JsonIgnore]
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<T_VALORPARAMETROTOL> T_VALORPARAMETROTOL1 { get; set; }
        //[JsonIgnore]
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<T_VALORPARAMETROTOL> T_VALORPARAMETROTOL2 { get; set; }

        /// <summary>
        /// Propiedades no mapeadas
        /// </summary>
        /// 
        private string valorFamilia { get; set; }

        [NotMapped]
        public bool DeProducto { get; set; }

        [NotMapped]
        [JsonIgnore]
        public string ValorProducto
        {
            get { return DeProducto ? VALOR : null; }
            set
            {
                DeProducto = true;
                valorFamilia = value;
                VALOR = value;
            }
        }

        [NotMapped]
        public string ValorFamilia
        {
            get { return DeProducto ? valorFamilia : VALOR; }
        }
    }
}
