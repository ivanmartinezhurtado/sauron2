﻿using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Forms;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.13)]
    [DesignerAction(Type = typeof(InitProductActionDescription))]
    

    public class InitProductAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número del producto")]
        public int NumProduct { get; set; }

        [Category("3. Configuration")]
        [Description("Versión del producto")]
        public int Version { get; set; }

        [Category("3. Configuration")]
        [Description("Fase")]
        [DefaultValue(TestInfo.FasesEnum.Verificar_120)]
        public TestInfo.FasesEnum Phase { get; set; }

        [Category("3. Configuration")]
        [Description("Número de identificación del empleado")]
        public string IdEmployee { get; set; }

        [Category("3. Configuration")]
        [Description("Modo del test que se va a ejecutar")]
        [DefaultValue(ModoTest.SAT)]
        public ModoTest TestMode { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;

            if (context.RunMode == RunMode.Release)
                return;
            if (context.Services.Contains<ITestContext>())
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("El Action InitProduct debe ir antes del action InitTestBase").Throw();

            NumProduct = GetVariable<int>("NumProducto", NumProduct);

            Version = GetVariable<int>("Version", Version);

            var value = GetVariable("NumFase");
            if (value != null)
                Phase = (TestInfo.FasesEnum)Convert.ToInt32(value);

            int? numOrden = null;

            value = GetVariable("NumOrden");
            if (value != null)
                numOrden = Convert.ToInt32(value);

            PRODUCTO producto;
            VW_EMPLEADO operario;
            ORDEN orden = null;
            List<T_GRUPOPARAMETRIZACION> parametros;
            string fase = ((int)Phase).ToString("000");
            List<string> direcciones = null;
            List<string> Atributos = null;
            string Ean = string.Empty;
            string EanMarca = string.Empty;
            Dictionary<string, string> DescripcionLenguagesCliente = null;
            string codigoCircutor = null;
            string costumerCode2 = null;
            string TestLocationDescription = null;
            int? versionReport = null;

            using (DezacService svc = new DezacService())
            {
                if (numOrden.HasValue)
                    orden = svc.GetOrden(numOrden.Value);

                if (!string.IsNullOrEmpty(IdEmployee))
                {
                    operario = svc.GetOperarioByIdEmpleado(IdEmployee);
                    if (operario == null)
                        Error().PROCESO.OPERARIO.VALOR_INCORRECTO("Operario no encontrado").Throw();

                    if (orden == null)
                    {
                        var dmf = svc.GetLastDiarioMarcaje(operario.IDEMPLEADO);
                        if (dmf != null && dmf.HOJAMARCAJEFASE != null)
                        {
                            orden = svc.GetOrden(dmf.HOJAMARCAJEFASE.NUMORDEN);
                            if (orden == null)
                                Error().PROCESO.FICHERO_NO_ENCONTRADO.NO_ORDEN_FABRICACION_NO_PRODUCTO("Orden inexistente").Throw();

                            fase = dmf.IDFASE;
                        }
                    }
                }
                else
                    operario = null;

                if (orden != null)
                {
                    NumProduct = orden.NUMPRODUCTO;
                    Version = orden.VERSION;
                }

                producto = svc.GetProductoByIOrdenFab(NumProduct, Version);
                if (producto == null)
                    Error().PROCESO.FICHERO_NO_ENCONTRADO.NO_ORDEN_FABRICACION_NO_PRODUCTO("Producto inexistente").Throw();

                if (producto.T_FAMILIA == null)
                    Error().PROCESO.PRODUCTO.PRODUCTO_INCORRECTO("Producto incorrecto o no tiene familia asignada").Throw();

                parametros = svc.GetGruposParametrizacion(producto, fase.ToString());
                if (parametros == null)
                    Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Grupos de parametrización inexistentes").Throw();

                if (producto.NUMDIRECCION.HasValue)
                    direcciones = svc.GetDirecciones(producto.NUMDIRECCION.Value);

                codigoCircutor = svc.GetCodigoCircutor(producto.NUMPRODUCTO);
                costumerCode2 = svc.GetCostumerCode2(producto.NUMPRODUCTO);
                Atributos = svc.GetAtributosCliente(producto.NUMPRODUCTO, codigoCircutor);
                Ean = svc.GetEANCode(producto.NUMPRODUCTO, codigoCircutor);
                EanMarca = svc.GetEANMarcaCode(producto.NUMPRODUCTO, codigoCircutor);
                DescripcionLenguagesCliente = svc.GetDescripcionLenguagesCliente(producto.NUMPRODUCTO, codigoCircutor);

                var lastTest = svc.GetLastAutoTest(Dns.GetHostName());
                if (lastTest != null)       
                    TestLocationDescription = lastTest.Item2;

                versionReport = svc.GetVersionTestReport(producto.T_FAMILIA.NUMFAMILIA);
                var configuracion = parametros.Where(p => p.NUMTIPOGRUPO == 5).FirstOrDefault();
                if (configuracion != null)
                {
                    var versionReportProduct = configuracion.T_VALORPARAMETRO.Where(p => p.T_PARAMETRO.PARAM == "TEST_REPORT_VERSION").FirstOrDefault();
                    if (versionReportProduct != null)
                        versionReport = Convert.ToInt32(versionReportProduct.VALOR);
                }
                if (versionReport == null)
                    versionReport = 1;
            }

            if (NumProduct > 0 && NumProduct != producto.NUMPRODUCTO)
                Error().PROCESO.PRODUCTO.NO_COINCIDE("El producto configurado en la acción no coincide con el de la orden").Throw();

            if (Version > 0 && Version != producto.VERSION)
                Error().PROCESO.TRAZABILIDAD.NO_COINCIDE("La versión del producto configurado en la acción no coincide con el de la orden").Throw();

            var sequenceName = GetVariable("SequenceName");
            var sequenceVersion = GetVariable("SequenceVersion");

            var data = new TestContextModel
            {
                IdEmpleado = operario != null ? operario.IDEMPLEADO : "230",
                IdOperario = operario != null ? operario.IDOPERARIO : "230",
                IdFamilia = producto.IDFAMILIA,
                NumFamilia = producto.NUMFAMILIA,
                NumProducto = producto.NUMPRODUCTO,
                ModelLeader = producto.T_FAMILIA.IDEMPLEADO,
                Version = producto.VERSION,
                IdFase = fase,
                modoPlayer = TestMode,
                Direcciones = direcciones,
                Atributos = Atributos,
                DescripcionLenguagesCliente = DescripcionLenguagesCliente,
                PC = Dns.GetHostName(),
                LocationTest = TestLocationDescription,
                DescFamilia = producto.ARTICULO.DESCRIPCION,
                EAN = Ean,
                EAN_MARCA = EanMarca,
                SequenceName = sequenceName != null ? sequenceName.ToString() : string.Format("{0}_{1}_{2}", producto.NUMPRODUCTO, producto.VERSION, fase),
                SequenceVersion = sequenceVersion != null ? sequenceVersion.ToString() : "1",
                SauronInterface = Application.ProductName,
                SauronVersion = Application.ProductVersion,            
            };

            data.TestInfo.CodCircutor = codigoCircutor;
            data.TestInfo.CostumerCode2 = costumerCode2;
            data.TestInfo.ReportVersion = versionReport.Value;

            if (orden != null)
            {
                data.NumOrden = orden.NUMORDEN;
                data.Cantidad = (int)orden.CANTIDAD;
                data.CantidadPendiente = orden.PENDIENTE.HasValue ? (int)orden.PENDIENTE.Value : 0;
            }
            else
                data.NumOrden = 0;

            parametros.ForEach(p =>
            {
                ParamValueCollection list = data.GetGroupList(p.NUMTIPOGRUPO);
                if (list != null)
                    foreach (var vp in p.T_VALORPARAMETRO)
                    {
                        ParamValue pv = new ParamValue
                        {
                            IdGrupo = vp.NUMGRUPO,
                            IdTipoGrupo = (TipoGrupo)p.NUMTIPOGRUPO,
                            Grupo = list.Name,
                            IdParam = vp.NUMPARAM,
                            ValorInicio = vp.VALORINICIO,
                            Valor = vp.VALOR,
                            IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                            IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                            Unidad = vp.T_PARAMETRO.T_UNIDAD.UNIDAD,
                            Name = vp.T_PARAMETRO.PARAM,
                            IdCategoria = vp.IDCATEGORIA
                        };

                        list.Add(pv);
                    }
            });

            data.ConfigurationFiles.LoadAllFilesFromBBDD(producto);

            var numBastidor = GetVariable(ConstantsParameters.TestInfo.NUM_BASTIDOR);
            if (numBastidor != null && !string.IsNullOrEmpty(numBastidor.ToString()))
                data.TestInfo.NumBastidor = Convert.ToUInt64(numBastidor);

            context.Services.Add<ITestContext>(data);
        }

        private void ControlBienByProducto(T_GRUPOPARAMETRIZACION parametros, TestContextModel Model)
        {
            var context = SequenceContext.Current;

            if (parametros != null)
            {
                var hasUtil = parametros.T_VALORPARAMETRO.Where(p => p.T_PARAMETRO.PARAM == "HAS_UTIL").FirstOrDefault();
                if (hasUtil != null)
                    if (hasUtil.VALOR == "NO")
                        return;
            }

            var cacheSvc = context.Services.Get<ICacheService>();

            var utillaje = -1;
            var hasValue = cacheSvc.TryGet("NUMERO_UTILLAJE", out utillaje);

            if (hasValue)
                Model.NumUtilBien = Convert.ToInt32(utillaje);
            else
            {
                var iShell = context.Services.Get<IShell>();

                DeviceUtilView ctrl = new DeviceUtilView(Model, context.RunMode == RunMode.Debug);

                if (iShell.ShowDialog("CONTROL DE TRAZABILIDAD DE LOS UTILLAJES", ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
                    Error().PROCESO.TRAZABILIDAD.BIEN("ERROR, NO SE HA INTRODUCIDO CORRECTAMENTE EL BIEN PARA EL CONTROL DE TRAZABILIDAD DE LOS UTILLAJES").Throw();

                if (ctrl.Utillaje.HasValue)
                    cacheSvc.Add("NUMERO_UTILLAJE", ctrl.Utillaje.Value);

                if (ctrl.Manguera.HasValue)
                    cacheSvc.Add("NUMERO_MANGUERA", ctrl.Manguera.Value);
            }
        }
    }

    public class InitProductActionDescription : ActionDescription
    {
        public override string Name { get { return "InitProduct"; } }
        public override string Category { get { return "DezacTest/Oracle/Parameters"; } }
        public override string Description { get { return ""; } }
        public override Image Icon { get { return Properties.Resources.DatabaseParameters; } }
    }
}