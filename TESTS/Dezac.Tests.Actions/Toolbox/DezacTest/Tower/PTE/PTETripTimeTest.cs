﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.Tower.PowerSource
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(PTETripTimeTestDescription))]
    [Browsable(true)]
    public class PTETripTimeTestFunction : ActionBase
    {
        public int Current { get; set; }
        public double MultiplyingFactor { get; set; }
        public string TestName { get; set; }
        [DefaultValue(null)]
        public int? RelayInput { get; set; }
        [DefaultValue(RelayLogicEnum.NO)]
        public RelayLogicEnum RelayLogic { get; set; }

        public enum RelayLogicEnum
        {
            NO,
            NC
        }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            if (!string.IsNullOrEmpty(TestName))
                TestName = "_" + TestName.ToUpper().Trim();

            var verificationLevel = Current != 0 ? Current : testBase.Consignas.GetDouble(String.Format("PTE_LEVEL{0}", TestName), 0.001, ParamUnidad.A);

            var logic = RelayLogic == RelayLogicEnum.NO ? false : true;
            if ((RelayInput != null) && tower.IO.DI[(int)RelayInput] != logic)
                Error().UUT.DISPARO.YA_DISPARADO();

            tower.PTE.ResetTimerTripMonitor();

            tower.PTE.ApplyConfigMonitor(3, PTE.Monitor.EventType.TRIP, PTE.Monitor.EventTrigger.ActivarseDesactivarse);

            tower.PTE.DesActivaTimer(108);

            tower.PTE.ApplyCurrentAndStartTimer(verificationLevel);

            testBase.Delay(300, "Esperando el disparo del equipo");

            try
            {
                if ((RelayInput != null) && tower.IO.DI[(int)RelayInput] == logic)
                    Error().UUT.DISPARO.NO_DISPARA();

                var min = testBase.Margenes.GetDouble(string.Format("TRIGGER_TIME{0}_MIN", TestName), 0, ParamUnidad.ms);
                var max = testBase.Margenes.GetDouble(string.Format("TRIGGER_TIME{0}_MAX", TestName), 9999, ParamUnidad.ms);
                var def = new AdjustValueDef(string.Format("TRIGGER_TIME{0}", TestName), 0, min, max, 0, 0, ParamUnidad.ms);

                var timeResult = testBase.TestMeasureBase(def, (s) =>
                {
                    var time = tower.PTE.ReadTime();
                    return time;
                }, 0, 1, 1000);
            }
            finally
            {
                tower.PTE.ApplyOff();
            }

        }
    }

    public class PTETripTimeTestDescription : ActionDescription
    {
        public override string Name { get { return "PTETripTimeTest"; } }
        public override string Category { get { return "DezacTest/Tower/TowerI"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
    }
}
