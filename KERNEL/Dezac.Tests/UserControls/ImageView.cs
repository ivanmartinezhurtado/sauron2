﻿using System.Drawing;

namespace Dezac.Tests.UserControls
{
    public partial class ImageView : DialogViewBase
    {
        private string imagePath;

        public ImageView()
        {
            InitializeComponent();
        }

        public ImageView(string titulo, string rutaImagen)
            : this()
        {
            Titulo = titulo;
            ImagePath = rutaImagen;
        }

        public ImageView(string rutaImagen)
            : this()
        {
            ImagePath = rutaImagen;
        }

        public ImageView(string titulo, Image imagen)
            : this()
        {
            Titulo = titulo;
            Imagen = imagen;
        }


        public string Titulo
        {
            get { return lblCaption.Text; }
            set { lblCaption.Text = value; }
        }

        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                if (imagePath != value)
                {
                    imagePath = value;
                    Imagen = Image.FromFile(imagePath);
                }
            }
        }

        public Image Imagen
        {
            get { return PictureImage.Image; }
            set { PictureImage.Image = value; }
        }

        protected override void Dispose(bool disposing)
        {
            if (Imagen != null) 
                Imagen.Dispose();
            base.Dispose(disposing);
        }
    }
}
