﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.13)]
    public class RGU10_Reconexion_Test : RGU10TestBase
    {
        public override void TestRelays()
        {
            Rgu.WriteFlagTest();

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

            var cantidadReles = (int)VectorHardware.GetDouble("NUMERO_RELES", 2, ParamUnidad.SinUnidad);

            switch (cantidadReles)
            {
                case 2:
                    SamplerWithCancel((p) =>
                    {
                        Rgu.WriteRelays(RGU10.Relays.PREALARM_RELAY);

                        return Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && !Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;
                    }, "Error, No se detecta la activación del relé de prealarma", 10, 1000, 500);

                    Resultado.Set("PREALARM_RELAY", "OK", ParamUnidad.SinUnidad);

                    SamplerWithCancel((p) =>
                    {
                        Rgu.WriteRelays(RGU10.Relays.TRIGGER_RELAY); 

                        Delay(200, "Espera activación rele");

                        bool result = Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && !Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];

                        return result;
                    }, "Error, No se detecta la activación del relé de disparo", 5, 1000, 500);

                    Resultado.Set("TRIGGER_RELAY", "OK", ParamUnidad.SinUnidad);

                    break;

                case 4:
                    SamplerWithCancel((p) =>
                    {
                        Rgu.WriteRelays(RGU10.Relays.PREALARM_RELAY);

                        return Tower.IO.DI.Read(INPUT_NO_PREALARM_MT_RELAY).Value && !Tower.IO.DI.Read(INPUT_NC_PREALARM_MT_RELAY).Value;
                    }, "Error, No se detecta la activación del relé de prealarma", 10, 1000, 500);

                    Resultado.Set("PREALARM_RELAY", "OK", ParamUnidad.SinUnidad);

                    SamplerWithCancel((p) =>
                    {
                        Rgu.WriteRelays(RGU10.Relays.TRIGGER_RELAY);

                        bool result = Tower.IO.DI.Read(INPUT_NC_TRIGGER_MT_RELAY).Value;

                        return result;
                    }, "Error, No se detecta la activación del rele de disparo.", 5, 1000, 500);

                    Resultado.Set("TRIGGER_RELAY", "OK", ParamUnidad.SinUnidad);

                    SamplerWithCancel((p) =>
                    {
                        Rgu.WriteRelays(RGU10.Relays.RELAY_ENGINE_OFF);

                        return Tower.IO.DI.Read(INPUT_OFF_MOTOR_MT_RELAY).Value;
                    }, "Error, No se detecta la activación del rele de activación de motor.", 5, 1000, 500);

                    Resultado.Set("ENGINE_ON_RELAY", "OK", ParamUnidad.SinUnidad);

                    SamplerWithCancel((p) =>
                    {
                        Rgu.WriteRelays(RGU10.Relays.RELAY_ENGINE_ON);

                        return Tower.IO.DI.Read(INPUT_ON_MOTOR_MT_RELAY).Value;
                    }, "Error, No se detecta la activación del rele de desactivación de motor.", 5, 1000, 500);

                    Resultado.Set("ENGINE_OFF_RELAY", "OK", ParamUnidad.SinUnidad);
                    break;

                default:
                    throw new Exception("Parametrizacion de reles incorrecta");
            }

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);
        }

        public void TestRelaysMocuel()
        {
            Rgu.WriteFlagTest();

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

            SamplerWithCancel((p) =>
            {
                Rgu.WriteRelays(RGU10.Relays.PREALARM_RELAY);

                return Tower.IO.DI.Read(INPUT_NO_PREALARM_MT_RELAY).Value && !Tower.IO.DI.Read(INPUT_NC_PREALARM_MT_RELAY).Value;
            }, "Error, No se detecta la activación del relé de prealarma", 10, 1000, 500);

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

            SamplerWithCancel((p) =>
            {
                Rgu.WriteRelays(RGU10.Relays.TRIGGER_RELAY);

                bool result = Tower.IO.DI.Read(INPUT_NC_TRIGGER_MT_RELAY).Value;

                return result;
            }, "Error, No se detecta la activación del rele de disparo.", 5, 1000, 500);

            Resultado.Set("TRIGGER_RELAY", "OK", ParamUnidad.SinUnidad);

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);
        }

        public override void TestSetupCustomer()
        {
            Rgu.WriteFlagTest();

            Rgu.ClearCurrentTimeTables();

            Delay(5000, "Espera para el borrado de las tablas");

            WriteCostumerTableCurrentTime();

            WriteCustomizeLimits();

            WriteCustomizeLockingConfiguration();

            WriteTypeCutt();

            WriteCustomizeReconectionTable();

            WriteCustomizeFrequency();

            WriteCustomizeRelaySecurity();

            WriteMessageInitial();      

            Rgu.WriteErrorCode(0);

            WriteCustomizePerifericNumber();
        }

        public override void TestExternalInput()
        {
            Rgu.WriteFlagTest();

            if ((int)VectorHardware.GetDouble("NUMERO_RELES", 2, ParamUnidad.SinUnidad) == 4)
                if (UsbIO.DI.Read(OUT_ADVANTECH_MODELO_MT).Value == false)
                    throw new Exception("No se ha activado el modulo USB que commuta a modelo RAL/MT");

            var cantidadReles = (int)VectorHardware.GetDouble("NUMERO_RELES", 2, ParamUnidad.SinUnidad);
            Delay(1000, "");
            if (cantidadReles == 2)
                Tower.IO.DO.On(OUT_EXTERNAL_TRIGGER);
            if (cantidadReles == 4)
                Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);          

            SamplerWithCancel((p) =>
            {
                var inputs = Rgu.ReadInputs();
                return inputs.HasFlag(RGU10.Inputs.EXTERNAL_INPUT);

            }, "Error. No se detecta correctamente la señal externa.", 5, 1000, 1000, false, false);

            if (cantidadReles == 2)
                Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);
            if (cantidadReles == 4)
                Tower.IO.DO.On(OUT_EXTERNAL_TRIGGER);

            SamplerWithCancel((p) =>
            {
                var inputs = Rgu.ReadInputs();
                return !inputs.HasFlag(RGU10.Inputs.EXTERNAL_INPUT);

            }, "Error. la señal externa sigue activada", 5, 1000, 1000, false, false);

            Resultado.Set("EXTERNA_INPUT", "OK", ParamUnidad.SinUnidad);
        }

        public void TestExternalInputTelefonica()
        {
            Rgu.WriteNormalMode();
            Delay(1000, "");
            Rgu.WriteNormalMode();

            Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);

            Delay(2000, "");

            Tower.IO.DO.On(OUT_EXTERNAL_TRIGGER);

            SamplerWithCancel((p) =>
            {
                var inputs = Rgu.ReadInputs();
                return inputs.HasFlag(RGU10.Inputs.EXTERNAL_INPUT);

            }, "Error. No se detecta correctamente la señal externa.", 5, 1000, 1000, false, false);


            Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);

            SamplerWithCancel((p) =>
            {
                var inputs = Rgu.ReadInputs();
                return !inputs.HasFlag(RGU10.Inputs.EXTERNAL_INPUT);

            }, "Error. la señal externa sigue activada", 5, 1000, 1000, false, false);

            Rgu.WriteFlagTest();

            Resultado.Set("EXTERNA_INPUT", "OK", ParamUnidad.SinUnidad);
        }

        public void TestPulseInput()
        {
            Rgu.WriteFlagTest();

            Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);

            Tower.IO.DO.On(21, 43);  //21 Manguera 4 Reles conecta Negativo de la LAMBDA
                                     //43 Manguera positivo de la LAMBDA
            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(2, 1);

            SamplerWithCancel((Func<int, bool>)((p) =>
            {
                var inputs = Rgu.ReadInputs();

                var result = inputs != RGU10.Inputs.EXTERNAL_INPUT;

                if ((p == 3) || result)
                {
                    Tower.LAMBDA.ApplyOffAndWaitStabilisation();
                    Tower.IO.DO.Off(21, 43);
                    Tower.IO.DO.On((int)RGU10TestBase.OUT_EXTERNAL_TRIGGER);
                }
                return result;

            }), "Error. No se detecta correctamente la señal externa.", 3, 1000, 1000, false, false);

            Tower.IO.DO.On(OUT_EXTERNAL_TRIGGER);
            Resultado.Set("PULSE_INPUT", "OK", ParamUnidad.SinUnidad);
        }

        public void TestAdjustWithoutScale1()
        {
            var currentScala1 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_1.Name, 0, ParamUnidad.A), 0 };
            var currentScala2 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_2.Name, 3, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.Ajuste.esc_2.Name, 0, ParamUnidad.A) };
            var currentScala3 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_3.Name, 0, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.Ajuste.esc_3.Name, 1.5, ParamUnidad.A) };
            var currentScala4 = new double[] { 0, Consignas.GetDouble(Params.I.L2.Ajuste.esc_4.Name, 10, ParamUnidad.A) };

            var currentDictionaryTemporal = new Dictionary<RGU10.ScaleCalibration, double[]>()
            {
                //{RGU10.ScaleCalibration.SCALE_1,currentScala1},
                {RGU10.ScaleCalibration.SCALE_2,currentScala2},
                {RGU10.ScaleCalibration.SCALE_3,currentScala3},
                {RGU10.ScaleCalibration.SCALE_4,currentScala4},
            };

            InternalAdjust(currentDictionaryTemporal);
        }

        public override void TestVerification([Description("Posicion de la corriente que se seleccionar para probar el disparo")]byte positionCurrent = 0, [Description("Posicion del tiempo que se seleccionar para probar el disparo")]byte positionTime = 2)
        {
            var currentTrigger = new double[] { Consignas.GetDouble(Params.I.L1.TRIGGER.esc_1.Name, 0.06, ParamUnidad.mA), Consignas.GetDouble(Params.I.L2.TRIGGER.esc_1.Name, 0, ParamUnidad.mA) };
            bool parallelReset = Parametrizacion.GetString("PARALLEL_RESET", "NO", ParamUnidad.SinUnidad) == "SI";

            Tower.IO.DO.On(OUT_CORRIENTE_L1);

            this.InParellel(() =>
            {            
                Rgu.WriteNormalMode();
                Rgu.WriteAllowParametersWriting();
                Rgu.WriteTriggerCurrentSelection(CurrentPosition);
                Rgu.WriteTriggerTimeSelection(TimePosition);
                Rgu.WriteSetupWriting();

                PowerSourceResetAndApplyPrsetes(false);
            },
            () =>
            {
                Tower.MUX.Reset();
                var InputTriggerMux = (InputMuxEnum)Configuracion.GetDouble("ENTRADA_MUX_DISPARO", 2, ParamUnidad.SinUnidad);
                Tower.MUX.Connect2((int)TOROIDAL_COMMON, Outputs.FreCH1, (int)InputTriggerMux, Outputs.FreCH2);

                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.2);

                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);

                if (Parametrizacion.GetString("SEGURIDAD_RELE_TRIP_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
                else
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.1);
            },
            () =>
            {
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(currentTrigger[0], currentTrigger[1], 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
            });

            Delay(3000, "Espera disparo");

            var adjustValue = new AdjustValueDef(Params.TIME.Other("TRIGGER").Verificacion.esc_1, ParamUnidad.s);
            var adjustResult = TestMeasureBase(adjustValue, (Func<int, double>)((s) =>
            {
                var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 3000, HP53131A.Channels.CH1, (Action)(() =>
                {
                    Delay(500, "Espera disparo");
                    Tower.IO.DO.Off((int)RGU10TestBase.OUT_CORRIENTE_L1);
                }));
                return (timeResult.Value * 1000);
            }), 0, 1, 1000);

            var adjustValueCurrent = new AdjustValueDef(Params.I.Other("TRIGGER").Verificacion.esc_1, ParamUnidad.mA);

            this.InParellel(() =>
                {
                    PowerSourceResetAndApplyPrsetes(false);
                }, (Action)(() =>
                 {
                     Delay(4000, "Espera apagado del equipo");
                     adjustResult = TestMeasureBase(adjustValueCurrent, (s) =>
                     {
                         var currentReading = Rgu.ReadCurrent();
                         return currentReading;
                     }, 0, 20, 500);

                     Tower.IO.DO.On((int)RGU10TestBase.OUT_CORRIENTE_L1);
                 }));

            PowerSourceResetAndApplyPrsetes(parallelReset);
        }

        public override void TestCustomization()
        {
            Delay(5000, "");

            base.TestCustomization();

            Delay(1000, "");

            ReadAndSaveResultsCurrentTimeTables();

            ReadAndSaveResultsLimits();

            ReadAndSaveResultsRegisterLocking();

            ReadAndSaveResultsReconectionTables();

            ReadAndSaveResultsInitialMesage();
        }

        public void TestCustomizationTelefonica()
        {
            base.TestCustomization();

            Delay(1000, "");

            ReadAndSaveResultsCurrentTimeTables();
        }

        // Override porque en RAL-MQ siempre hay que borrar las tablas
        protected override void WriteCostumerTableCurrentTime()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad);

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad);

            var times = GetTimeScaleTable(timeTableElements);

            Rgu.WriteScaleTable(currents, RGU10.TableKind.CURRENT);

            Rgu.WriteScaleTable(times, RGU10.TableKind.TIME);

            //*******************************************************************

            Rgu.WriteAllowParametersWriting();

            var current = Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 20, ParamUnidad.mA);

            var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)current);

            if (posCurrent.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la I_DISPARO_DEFECTO no existe en la tabla de corrientes");

            CurrentPosition = posCurrent.FirstOrDefault().Position;

            Resultado.Set("POSICION_I_DISPARO_DEFECTO", CurrentPosition.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteTriggerCurrentSelection(posCurrent.FirstOrDefault().Position);

            var tiempo = Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 100, ParamUnidad.ms);

            var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)tiempo);

            if (posTime.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la T_RETARDO_DEFECTO no existe en la tabla de tiempos");

            TimePosition = posTime.FirstOrDefault().Position;

            Resultado.Set("POSICION_T_RETARDO_DEFECTO", TimePosition.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteTriggerTimeSelection(posTime.FirstOrDefault().Position);

            Rgu.WriteSetupWriting();
        }
    }
}
