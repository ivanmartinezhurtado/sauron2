﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions.Views
{
    public class UIMethodEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            RunMethodAction result = value as RunMethodAction;

            using (var editor = new MethodSelectorView())
            {
                editor.Value = result;

                var resp = editor.ShowDialog();

                if (resp == System.Windows.Forms.DialogResult.OK)
                    result = editor.Value;
                else if (resp == System.Windows.Forms.DialogResult.Abort)
                    result = null;
            }

            return result;
        }
    }
}
