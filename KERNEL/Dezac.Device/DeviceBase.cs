﻿using Dezac.Core.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Dezac.Device
{
    public class DeviceBase : IDisposable
    {
        protected static readonly ILog _logger = LogManager.GetLogger("UUT");

        public virtual void Dispose()
        {
        }

        protected void LogMethod([CallerMemberName] string name = "")
        {
            _logger.InfoFormat(name);
        }

        public string GetDeviceVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(DeviceVersionAttribute), true).FirstOrDefault() as DeviceVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }

        protected Tuple<bool, StatisticalList, string> AdjustBase(int delFirst, int initCount, int numMuestras, int intervalo, int diffMaxRangeValid, Func<double[]> funcVariables, Func<StatisticalList, bool> calculoAjuste, Func<double[], bool> validacionMuestra = null)
        {
            StatisticalList list = null;

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var value = funcVariables();

                    if (list == null)
                        list = new StatisticalList(value.Length);

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null && !validacionMuestra(value))
                        return;

                    list.Add(value);

                    var range = list.MaxRange();
                    if (range > diffMaxRangeValid && diffMaxRangeValid != 0)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, diffMaxRangeValid);
                        list.Clear(); //TODO ha tener en cuenta
                    }

                    if (list.Count(0) >= initCount)
                        step.Cancel = calculoAjuste(list);               
                });

            return Tuple.Create(sampler.Canceled, list, sampler.Exceptions != null ? sampler.Exceptions.Message : "");
        }

        protected List<S> GetEnumItemsFromUshort<S>(ushort input) where S : struct, IComparable, IConvertible, IFormattable
        {
            const int bitsInUInt16 = sizeof(ushort) * 8;
            var enumType = Enum.GetUnderlyingType(typeof(S));

            return Enumerable.Range(0, bitsInUInt16 + 1).Select(x => (1 << x) & UInt16.MaxValue)
                    .Where(x => (input & (ushort)x) == (ushort)x && Enum.IsDefined(typeof(S), x))
                    .Select(x => (S)Enum.Parse(typeof(S), Enum.GetName(typeof(S), x), true))
                    .ToList();
        }
    }
}
