﻿/***************************************************************************************
 ***                                                                                 ***
 ***  Copyright (c) 2019, Lucid Vision Labs, Inc.                                    ***
 ***                                                                                 ***
 ***  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     ***
 ***  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       ***
 ***  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    ***
 ***  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         ***
 ***  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  ***
 ***  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  ***
 ***  SOFTWARE.                                                                      ***
 ***                                                                                 ***
 ***************************************************************************************/
using System;

namespace Cs_Acquisition
{
    class Cs_Acquisition
    {
        const String TAB1 = "  ";
        const String TAB2 = "    ";

        // Acquisition: Introduction
        //    This example introduces the basics of image acquisition. This
        //    includes setting image acquisition and buffer handling modes,
        //    setting the device to automatically negotiate packet size, and
        //    setting the stream packet resend node before starting the image
        //    stream. The example then starts acquiring images by grabbing and
        //    requeuing buffers, and retrieving data on images before finally
        //    stopping the image stream.

        // =-=-=-=-=-=-=-=-=-
        // =-=- SETTINGS =-=-
        // =-=-=-=-=-=-=-=-=-

        // Image timeout
        //    Timeout for grabbing images (in milliseconds). If no image is
        //    available at the end of the timeout, an exception is thrown. The
        //    timeout is the maximum time to wait for an image; however, getting
        //    an image will return as soon as an image is available, not waiting
        //    the full extent of the timeout.
        const UInt32 TIMEOUT = 2000;

        // number of images to grab
        const UInt32 NUM_IMAGES = 25;

        // =-=-=-=-=-=-=-=-=-
        // =-=- EXAMPLE -=-=-
        // =-=-=-=-=-=-=-=-=-

        // demonstrates acquisition
        // (1) sets acquisition mode
        // (2) sets buffer handling mode
        // (3) enables auto negotiate packet size
        // (4) enables packet resend
        // (5) starts the stream
        // (6) gets a number of images
        // (7) prints information from images
        // (8) requeues buffers
        // (9) stops the stream
        static void AcquireImages(ArenaNET.IDevice device)
        {
            // get node values that will be changed in order to return their
            // values at the end of the example
            var acquisitionModeNode = (ArenaNET.IEnumeration)device.NodeMap.GetNode("AcquisitionMode");
            String acquisitionModeInitial = acquisitionModeNode.Entry.Symbolic;

            // Set acquisition mode
            //    Set acquisition mode before starting the stream. Starting the
            //    stream requires the acquisition mode to be set beforehand. The
            //    acquisition mode controls the number of images a device
            //    acquires once the stream has been started. Setting the
            //    acquisition mode to 'Continuous' keeps the stream from
            //    stopping. This example returns the camera to its initial
            //    acquisition mode near the end of the example.
            Console.WriteLine("{0}Set acquisition mode to 'Continuous'", TAB1);

            acquisitionModeNode.FromString("Continuous");

            // Set buffer handling mode
            //    Set buffer handling mode before starting the stream. Starting
            //    the stream requires the buffer handling mode to be set
            //    beforehand. The buffer handling mode determines the order and
            //    behavior of buffers in the underlying stream engine. Setting
            //    the buffer handling mode to 'NewestOnly' ensures the most
            //    recent image is delivered, even if it means skipping frames.
            Console.WriteLine("{0}Set buffer handling mode to 'NewestOnly'", TAB1);


            var streamBufferHandlingModeNode = (ArenaNET.IEnumeration)device.TLStreamNodeMap.GetNode("StreamBufferHandlingMode");
            streamBufferHandlingModeNode.FromString("NewestOnly");

            Console.WriteLine("{0}Enable stream to auto negotiate packet size", TAB1);

            // Enable stream auto negotiate packet size
            //    Setting the stream packet size is done before starting the
            //    stream. Setting the stream to automatically negotiate packet
            //    size instructs the camera to receive the largest packet size
            //    that the system will allow. This generally increases frame rate
            //    and results in fewer interrupts per image, thereby reducing CPU
            //    load on the host system. Ethernet settings may also be manually
            //    changed to allow for a larger packet size.
            var streamAutoNegotiatePacketSizeNode = (ArenaNET.IBoolean)device.TLStreamNodeMap.GetNode("StreamAutoNegotiatePacketSize");
            streamAutoNegotiatePacketSizeNode.Value = true;

            // Enable stream packet resend
            //    Enable stream packet resend before starting the stream. Images
            //    are sent from the camera to the host in packets using UDP
            //    protocol, which includes a header image number, packet number,
            //    and timestamp information. If a packet is missed while
            //    receiving an image, a packet resend is requested and this
            //    information is used retrieve and redeliver the missing packet
            //    in the correct order.
            Console.WriteLine("{0}Enable stream packet resend", TAB1);

            var streamPacketResendEnableNode = (ArenaNET.IBoolean)device.TLStreamNodeMap.GetNode("StreamPacketResendEnable");
            streamPacketResendEnableNode.Value = true;

            // Start stream
            //    Start the stream before grabbing any images. Starting the
            //    stream allocates buffers, which can be passed in as an argument
            //    (default: 10), and begins filling them with data. Starting the
            //    stream blocks write access to many features such as width,
            //    height, and pixel format, as well as acquisition and buffer
            //    handling modes, among others. The stream needs to be stopped
            //    later.
            Console.WriteLine("{0}Start stream", TAB1);

            device.StartStream();

            // get images
            Console.WriteLine("{0}Getting {1} images", TAB1, NUM_IMAGES);

            for (UInt32 i = 0; i < NUM_IMAGES; i++)
            {
                // Get image
                //    Retrieve images after the stream has started. If the
                //    timeout expires before an image is retrieved, example will
                //    throw. Because of this, the timeout should be at least a
                //    bit larger than the exposure time.
                Console.Write("{0}Get image {1}", TAB2, i);

                ArenaNET.IImage image = device.GetImage(TIMEOUT);

                // Get image information
                //    Images have information available from them beyond the
                //    basics including information related to data type, image
                //    parameters, and error handling.
                Console.Write(" ({0} bytes; {1}x{2}; {3}; timestamp (ns): {4})",
                    image.SizeFilled,
                    image.Width,
                    image.Height,
                    image.PixelFormat.ToString(),
                    image.TimestampNs);

                // Requeue image buffer
                //    Requeue an image buffer when access to it is no longer
                //    needed. Notice that failing to requeue buffers may cause
                //    memory to leak and may also result in the stream engine
                //    being starved due to there being no available buffers.
                Console.WriteLine(" and requeue");

                device.RequeueBuffer(image);
            }

            // Stop stream
            //    Stop the stream after all images have been requeued. Failing to
            //    stop the stream will leak memory.
            Console.WriteLine("{0}Stop stream", TAB1);

            device.StopStream();

            // return nodes to their initial values
            acquisitionModeNode.FromString(acquisitionModeInitial);

        }

        // =-=-=-=-=-=-=-=-=-
        // =- PREPARATION -=-
        // =- & CLEAN UP =-=-
        // =-=-=-=-=-=-=-=-=-

        static void Main(string[] args)
        {
            Console.WriteLine("Cs_Acquisition");

            try
            {
                // prepare example
                ArenaNET.ISystem system = ArenaNET.Arena.OpenSystem();
                system.UpdateDevices(100);
                if (system.Devices.Count == 0)
                {
                    Console.WriteLine("\nNo camera connected\nPress enter to complete");
                    Console.Read();
                    return;
                }
                ArenaNET.IDevice device = system.CreateDevice(system.Devices[0]);

                // run example
                Console.WriteLine("Commence example\n");
                AcquireImages(device);
                Console.WriteLine("\nExample complete");

                // clean up example
                system.DestroyDevice(device);
                ArenaNET.Arena.CloseSystem(system);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception thrown: {0}", ex.Message);
            }

            Console.WriteLine("Press enter to complete");
            Console.Read();
        }
    }
}