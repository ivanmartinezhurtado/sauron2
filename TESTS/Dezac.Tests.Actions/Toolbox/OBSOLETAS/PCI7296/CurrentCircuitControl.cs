﻿using Dezac.Tests.Services;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(CurrentCircuitControlActionDescription))]
    
    public class CurrentCircuitControl : ActionBase
    {
        [DefaultValue(false)]
        public bool L1Control { get; set; }

        [DefaultValue(false)]
        public bool L2Control { get; set; }

        [DefaultValue(false)]
        public bool L3Control { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            tower.ActiveCurrentCircuit(L1Control, L2Control, L3Control);
        }

    }

    public class CurrentCircuitControlActionDescription : ActionDescription
    {
        public override string Name { get { return "ControlCurrentCircuit"; } }
        public override string Description { get { return "Abre o cierra el circuito de corriente de la fuente MTE"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.output; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }
}
