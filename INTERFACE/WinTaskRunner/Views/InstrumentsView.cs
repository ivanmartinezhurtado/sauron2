﻿using Dezac.Core.Utility;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using WinTaskRunner.Services;

namespace WinTaskRunner.Views
{
    public partial class InstrumentsView : UserControl
    {
        protected readonly AppService app;
        //protected readonly AssemblyResolverService assemblyResolver;
        protected readonly IInstanceResolveService instanceResolveService;
        protected readonly IShell shell;

        private SynchronizationContext sc;

        public string Category { get; set; }

        private SequenceContext lastContext;

        private InstrumentsView()
        {
            InitializeComponent();
        }

        public InstrumentsView(AppService appService
            , InstanceResolveService instanceResolveService
            , IShell shell)
            : this()
        {
            app = appService;
            //this.assemblyResolver = assemblyResolver;
            this.shell = shell;
            this.instanceResolveService = instanceResolveService;
            sc = SynchronizationContext.Current;

            app.Runner.TestStart += (s, e) =>
            {
                lastContext = SequenceContext.Current;
            };
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!DesignMode)
                LoadAssembly();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }


        private void LoadAssembly()
        {
            var assembly = Assembly.LoadFrom("Dezac.Instruments.dll");
            if (assembly != null)
                try
                {
                    System.Collections.Generic.IEnumerable<Type> types = from t in assembly.GetTypes()
                        where t.IsClass && t.IsPublic && !t.IsAbstract
                        select t;

                    foreach (Type type in types)
                    {
                        bool isToolboxItem = type.GetCustomAttributes<ToolboxItemAttribute>().Any(p => p.ToolboxItemType != null);
                        if (isToolboxItem)
                        {
                            System.Collections.Generic.IEnumerable<Attribute> descriptionClass = type.GetCustomAttributes(typeof(DesignerControlAttribute));
                            var att = new { Type = type, Design = descriptionClass.Cast<DesignerControlAttribute>().FirstOrDefault() };
                            var instance = Activator.CreateInstance(att.Design.Type) as ControlDescription;
                            string categoria = instance.Category ?? "Others";

                            if (Category == categoria && instance.Visible && instance.IconBig != null)
                            {
                                var child = new CommandBarToggleButton();
                                child.Text = instance.Name;
                                child.Name = instance.Name;
                                child.Tag = type;
                                child.ToolTipText = instance.Name;
                                Image image = instance.IconBig;
                                child.Image = image;
                                child.ImageLayout = ImageLayout.Stretch;
                                child.AutoSize = false;
                                child.Size = new Size(82, 48);
                                child.ToggleStateChanged += Child_ToggleStateChanged;
                                commandBarStripPowerSource.Items.Add(child);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Ensamblado inválido!");
                }
        }

        private void Child_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            var button = sender as CommandBarToggleButton;
            if (button != null)
            {
                if (button.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    createControlAddWindow(button.Tag as Type);
                else
                    disposedConstrolWindow(button.Tag as Type);
            }
        }

        private DockWindow FindDockWindow(string title)
        {
            DockWindow tw = radDockInstruments.DockWindows.ToolWindows.Where(p => p.Text == title).FirstOrDefault();
            if (tw != null)
                return tw;
            else
                return null;
        }

        private void createControlAddWindow(Type control)
        {
            DockWindow windowFind = FindDockWindow(control.Name);
            if (windowFind == null)
            {
                var ctrl = Activator.CreateInstance(control) as Control;
                if (ctrl != null)
                {
                    if (instanceResolveService != null)
                    {
                        Type ctrlType = ctrl.GetType();
                        PropertyInfo prop = ctrlType.GetProperty("InstrumentType");
                        var typeInstr = (Type)prop.GetValue(ctrl);

                        object target = instanceResolveService.Resolve(typeInstr, lastContext);
                        if (target != null)
                        {
                            PropertyInfo prop2 = ctrlType.GetProperty("Instrument");
                            prop2.SetValue(ctrl, target);
                        }
                    }

                    AddToolWindow(ctrl, DockPosition.Top, control.Name, ctrl.Size.Width, ctrl.Height);
                }
            }
        }

        private void AddToolWindow(Control control, DockPosition position, string title = null, int width = 0, int height = 0)
        {
            var win = new ToolWindow(title);
            win.CloseAction = DockWindowCloseAction.Close;
            win.ToolCaptionButtons = ToolStripCaptionButtons.None;
            win.DockState = DockState.Floating;
            win.Controls.Add(control);
            var size = new Size(width + 10, height + 25);
            win.DefaultFloatingSize = size;
            radDockInstruments.DockWindow(win, position);
            win.TabStrip.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
            win.TabStrip.SizeInfo.AbsoluteSize = size;
        }

        private void disposedConstrolWindow(Type control)
        {
            DockWindow windowFind = FindDockWindow(control.Name);
            if (windowFind != null)
            {
                windowFind.Close();
                windowFind.Dispose();
                radDockInstruments.RemoveWindow(windowFind);
            }
        }
    }
}
