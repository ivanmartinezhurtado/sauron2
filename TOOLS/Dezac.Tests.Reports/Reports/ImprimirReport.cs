﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Reports.Reports
{
    public partial class ImprimirReport : Form
    {
        private ReportDocument report = null;

        private string reportName;

        public ImprimirReport()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (report != null)
                viewer.ReportSource = report;

            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (report != null)
                report.Dispose();

            report = null;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool LoadReport(string reportName, object data, ITestReportAdapter adapter = null)
        {
            if (report != null)
                report.Dispose();

            this.reportName = reportName;

            try
            {
                if (adapter != null)
                    data = adapter.GetReport(data);

                report = new ReportDocument();
                report.Load(reportName);
                report.ReportOptions.EnableSaveDataWithReport = false;
                report.SetDataSource(data);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public byte[] ExportPdf()
        {
            if (report == null)
                return null;

            using (var ms = report.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                byte[] bytes = new byte[ms.Length];
                ms.Read(bytes, 0, Convert.ToInt32(ms.Length));

                return bytes;
            }
        }

        public void Print(string printerName)
        {
            if (report != null)
            {
                try
                {
                    PaperSize(printerName,"Report");
                    report.PrintToPrinter(1, false, 0, 0);
                
                }catch(Exception ex)
                {
                    throw new Exception("Error al imprimir el report por " + ex.Message);
                }
            }
        }

        private void PaperSize(string printerName, string PaperSize)
        {
            report.PrintOptions.PrinterName = printerName;

            var doctoprint = new System.Drawing.Printing.PrintDocument();
            doctoprint.PrinterSettings.PrinterName = printerName;
            for (byte i = 0; i < doctoprint.PrinterSettings.PaperSizes.Count - 1; i++)
            {
                if (doctoprint.PrinterSettings.PaperSizes[i].PaperName.ToUpper() == PaperSize.ToUpper())
                {
                        var sizesetting = doctoprint.PrinterSettings.PaperSizes[i].RawKind;
                        report.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)sizesetting;
                        return;
                }
            }
        }
    }
}
