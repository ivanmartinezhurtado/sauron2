﻿using System.Collections.Generic;

namespace Dezac.Core.Utility
{
    public struct FourLineValue
    {
        public readonly static FourLineValue Zero = new FourLineValue();

        public TriLineValue LINES { get; set; }
        public double LN { get; set; }

        public static FourLineValue Create(TriLineValue triLines, double vN)
        {
            return new FourLineValue { LINES = triLines, LN = vN };
        }
        public static FourLineValue Create(double v1, double v2, double v3, double vN)
        {
            return new FourLineValue { LINES = TriLineValue.Create(v1, v2, v3), LN = vN };
        }
        public static FourLineValue Create(double v)
        {
            return new FourLineValue { LINES = TriLineValue.Create(v), LN = v };
        }

        public static bool operator ==(FourLineValue x, FourLineValue y)
        {
            return x.LINES == y.LINES && x.LN == y.LN;
        }

        public static bool operator !=(FourLineValue x, FourLineValue y)
        {
            return x.LINES != y.LINES || x.LN != y.LN;
        }

        public override bool Equals(object obj)
        {
            return (LINES == ((FourLineValue)obj).LINES) && (LN == ((FourLineValue)obj).LN);
        }
        public override int GetHashCode()
        {
            return 0;
        }

        public static FourLineValue operator *(double x, FourLineValue y)
        {
            y.LINES *= TriLineValue.Create(x);
            y.LN *= x;
            return y;
        }

        public static FourLineValue operator *(FourLineValue y, double x)
        {
            y.LINES *= TriLineValue.Create(x);
            y.LN *= x;
            return y;
        }

        public static FourLineValue operator /(FourLineValue y, double x)
        {
            y.LINES /= x;
            y.LN /= x;
            return y;
        }

        public static FourLineValue operator -(FourLineValue x, FourLineValue y)
        {
            x.LINES -= y.LINES;
            x.LN -= y.LN;
            return x;
        }

        public static implicit operator FourLineValue(double value)
        {
            return new FourLineValue {  LINES= TriLineValue.Create(value), LN = value };
        }

        public override string ToString()
        {
            return string.Format("{0}, LN={1}", LINES.ToString(), LN);
        }

        public double[] ToArray()
        {
            var list = new List<double>();
            list.AddRange(TriLineValue.ToArray(LINES));
            list.Add(LN);
            return list.ToArray();
        }

        public static double[] ToArray(FourLineValue value)
        {
            var list = new List<double>();
            list.AddRange(TriLineValue.ToArray(value.LINES));
            list.Add(value.LN);
            return list.ToArray();
        }

        public static bool isNotZero(FourLineValue x)
        {
            return TriLineValue.isNotZero(x.LINES) && x.LN != 0;
        }
    }
}
