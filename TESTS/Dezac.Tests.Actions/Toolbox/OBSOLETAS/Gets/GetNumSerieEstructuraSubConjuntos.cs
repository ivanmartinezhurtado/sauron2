﻿using Dezac.Core.Exceptions;
using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(GetNumSerieEstructuraSubConjuntosDescription))]
    
    public class GetNumSerieEstructuraSubConjuntos : ActionBase
    {
        public override void Execute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            Dictionary<string, string> NumSerieSubconjunto = new Dictionary<string, string>();

            if (data != null && data.TestInfo != null)
            {
                var producto = data.Identificacion.GetString(ConstantsParameters.Identification.ESTRUCTURA_SUBCONJUNTOS, "0", ParamUnidad.SinUnidad);
                if (string.IsNullOrEmpty(producto) || producto == "0")
                    throw new Exception("Error no se a creado parametro ESTRUCTURA_SUBCONJUNTOS en los parametros de Identificacion de la familia del producto");

                var productos = producto.Split(';');
                foreach (string prod in productos)
                {
                    var descNumroducto = prod.Split('=');
                    if (descNumroducto.Count() <= 1)
                        throw new Exception("Error parametro ESTRUCTURA_SUBCONJUNTOS no esta bien definido, 'descripcion=numproducto;' ej: 'PLC1000=102504;'");

                    var numproducto = descNumroducto[1].Split('/');
                    NumSerieSubconjunto = AddNumSerieSubconjuntoInComponent(data, Convert.ToInt32(numproducto[0]), Convert.ToInt32(numproducto[1]), data.TestInfo.ListComponents, NumSerieSubconjunto);
                }

                data.TestInfo.NumSerieSubconjunto = NumSerieSubconjunto;
            }
        }

        private Dictionary<string, string> AddNumSerieSubconjuntoInComponent(ITestContext data, int prod, int version, List<ComponentsEstructureInProduct> ListComponents, Dictionary<string, string> NumSerieSubconjunto)
        {

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();;

            foreach (ComponentsEstructureInProduct component in ListComponents)
            {
                string numSerie = null;

                if (component.numproducto == Convert.ToInt32(prod))
                {
                    if (data.modoPlayer == ModoTest.PostVenta)
                    {
                        var shell = SequenceContext.Current.Services.Get<IShell>();

                        var msg = string.Format("ASIGNAR AL EQUIPO {0} /0 DE NUMERO SERIE AUTOMATICO (SI) O MANUAL (NO)", component.descripcion);
                        var result = shell.MsgBox(msg, "ENTRADA NUMERO SERIE POSTVENTA", System.Windows.Forms.MessageBoxButtons.YesNo);

                        if (result == System.Windows.Forms.DialogResult.No)
                            numSerie = testBase.GetIntroNumSerieByUser();
                    }

                    if (string.IsNullOrEmpty(numSerie))
                    {
                        using (DezacService svc = new DezacService())
                        {
                            if (!data.NumOrden.HasValue && component.numproducto != 0)
                                numSerie = svc.GetNumSerie(component.numproducto, component.version, 0);
                            else
                                numSerie = svc.GetNumSerie(component.numproducto, component.version, data.NumOrden.Value);
                        }

                        if (string.IsNullOrEmpty(numSerie))
                            throw new Exception("Numero de Serie inexistente para esta Orden o número de producto");
                    }

                    NumSerieSubconjunto.Add(component.descripcion, numSerie.Trim());
                }
            }
            return NumSerieSubconjunto;
        }
    }

    public class GetNumSerieEstructuraSubConjuntosDescription : ActionDescription
    {
        public override string Name { get { return "GetNumSerieEstructuraSubConjuntos"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
