﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(HttpPutWithoutResponseActionDescription))]
    public class HttpPutWithoutResponse : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre o dirección del servidor")]
        public ActionVariable Host { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Dirección URL/URI")]
        public ActionVariable URL { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Contenido HTML a enviar")]
        public ActionVariable Content { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var url = VariablesTools.ResolveText(URL.Value.ToString());
            var host = Host.Value.ToString();
            string content = string.Empty;
            if (Content != null && !Content.IsKeyNullOrEmpty)
                content = Content.Value.ToString();

            bool done = ComunicationHttpClient.HttpPutContentWithoutResponse(host, url, content);

            if (!done)
                Error().SOFTWARE.WEB_SERVICE.CONFIGURACION_INCORRECTA("No se ha podido completar la petición web").Throw();
        }
    }

    public class HttpPutWithoutResponseActionDescription : ActionDescription
    {
        public override string Name { get { return "HttpPutWithoutResponse"; } }
        public override string Description { get { return "Pedir al servidor que haga accesible el documento que se le envía en una URL determinada sin esperar respuesta"; } }
        public override string Category { get { return "Tools/Ethernet/Http"; } }
        public override Image Icon { get { return Properties.Resources.HTTP; } }
    }
}
