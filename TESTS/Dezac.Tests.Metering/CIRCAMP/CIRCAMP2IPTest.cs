﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.05)]
    public class CIRCAMP2IPTes : CIRCAMPTestBase
    {
        #region Definition Inputs and Outputs

        private const byte IN_DEVICE_PRESENCE = 24;
        private const byte IN_BLOQUE_SEGURIDAD = 23;
        private const byte IN_DETECTA_ANCLA_1 = 25;
        private const byte IN_DETECTA_ANCLA_2 = 26;

        private const byte IN_PISTON_OPTICO = 30;
        private const byte IN_PISTON_BORNES_1_N = 31;
        private const byte IN_PISTON_BORNES_7_12 = 32;
        private const byte IN_PISTON_BORNES_21_28 = 33;
        private const byte IN_PISTON_BORNES_31_40 = 34;

        private const byte IN_CH1_LOAD = 11;
        private const byte IN_CH2_LOAD = 12;

        private const byte IN_CH1_OUT_IMPULSE = 9;
        private const byte IN_CH2_OUT_IMPULSE = 10;

        private const byte OUT_FIJACION_EQUIPO = 11;
        private const byte OUT_PISTON_BORNES = 5;

        private const byte OUT_ACTIVE_IN_CH1 = 0;
        private const byte OUT_ACTIVE_IN_CH2 = 1;

        private const byte OUT_ACTIVE_IN_RECLOSE = 41;

        #endregion

        public override void TestInitialization()
        {
            CirwattDlms.enumFases fasesSelected = CirwattDlms.enumFases.SIXFASICO;
            Fases = fasesSelected;

            base.TestInitialization();

            OUT_PISTON_OPTICO = 6;
            OUT_PISTON_TECLA1 = 10;
            OUT_PISTON_TECLA2 = 9;
            OUT_ACTIVE_NEUTRO_RELAY_CONTROL = 19;
            OUT_ACTIVE_230V_CDC = 15;
            OUT_ACTIVE_VL2_TO_CDC = 43;

            tower.IO.DO.On(OUT_FIJACION_EQUIPO);

            tower.IO.DO.OnWait(500, OUT_PISTON_OPTICO, OUT_PISTON_BORNES);

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_1, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL ANCLA 1 COLOCADO EN EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_2, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL ANCLA 2 COLOCADO EN EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_PISTON_OPTICO, 2000))
                throw new Exception("Error no se detecta piston optico");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_1_N, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 1 - 4 - N");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_7_12, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 7 - 12");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_21_28, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 21 - 28");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_31_40, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 31 - 40");

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                throw new Exception("Error no se el equipo en el util");

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_1, 2000))
                throw new Exception("Error no se detecta el ancla 1 en el equipo");

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_2, 2000))
                throw new Exception("Error no se detecta el ancla 2 en el equipo");
        }

        public void TestConfiguracionInicial()
        {
            SBT.IniciarSesion();

            var mac = SBT.LeerMACPLC();
            Resultado.Set("MAC_PLC", mac, ParamUnidad.SinUnidad);
            TestInfo.NumMAC_PLC = mac;

            SBT.EscribirNumeroFabricacion((uint)TestInfo.NumBastidor);

            SBT.EscribirNumeroSerie(1000000);

            SBT.EscribirFechaFabricacion(DateTime.Now);

            if (Configuracion.GetString("GRABAR_MODELO", "SI", ParamUnidad.SinUnidad).Trim() == "SI")
                SBT.EscribirModelo(Identificacion.MODELO);
            else
            {
                var modelo = SBT.LeerModelo();
                Resultado.Set(ConstantsParameters.Identification.MODELO, modelo, ParamUnidad.SinUnidad, Identificacion.MODELO);
                Assert.IsTrue(modelo == Identificacion.MODELO, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Modelo del equipo incorrecto"));
            }

            SBT.EscribirEstadoFabricacion(999);

            SBT.WriteZeroOffsetActiveGain(CirwattDlms.enumFases.QUATRIFASICO);

            SBT.WriteVoltageGain(CirwattDlms.enumFases.SIXFASICO, new List<ushort>() { 3018, 3018, 3018, 3018, 3018, 3018 });

            SBT.WriteGapGain(CirwattDlms.enumFases.SIXFASICO, TypeMeterConection.frequency._50Hz, new List<short>() { 0, 0, 0, 0, 0, 0 });

            SBT.WritePowerActiveGain(CirwattDlms.enumFases.SIXFASICO, TypeMeterConection.frequency._50Hz, new List<ushort>() { 35594, 35594, 35594, 35594, 35594, 35594 });

            SBT.EscribirFechaHoraActual();

            SBT.EscribirFechaVerificacion(DateTime.Now);

            //var Crc_Binario = SBT.LeerCrcBinario();
            //Assert.AreEqual(ConstantsParameters.Identification.CRC_FIRMWARE, Crc_Binario.ToString(), Identificacion.CRC_FIRMWARE.ToString(), "Error. el CRC de integridad del CIRCAMP es incorrecto.", ParamUnidad.SinUnidad);

            SBT.CerrarSesion(false);
        }

        public void TestRelesControlEnergia()
        {
            var ouputsValue = new List<KeyValuePair<byte, byte>>();
            ouputsValue.Add(new KeyValuePair<byte, byte>(IN_CH1_LOAD, 1));
            ouputsValue.Add(new KeyValuePair<byte, byte>(IN_CH2_LOAD, 2));

            internalTestRelesControlEnergia(ouputsValue);
        }

        public void TestRelesControImpulsos()
        {
            var ouputsValue = new List<KeyValuePair<byte, byte>>();
            ouputsValue.Add(new KeyValuePair<byte, byte>(IN_CH1_OUT_IMPULSE, 1));
            ouputsValue.Add(new KeyValuePair<byte, byte>(IN_CH2_OUT_IMPULSE, 2));

            internalTestRelesControImpulsos(ouputsValue);
        }

        public void TestEntradaImpulsos()
        {
            var inputsValue = new List<KeyValuePair<byte, CirwattDlms.enumKeyInputs>>();
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs> (OUT_ACTIVE_IN_CH1, CirwattDlms.enumKeyInputs.IN1));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs> (OUT_ACTIVE_IN_CH2, CirwattDlms.enumKeyInputs.IN2));

            internalTestEntradaImpulsos(inputsValue);
        }

        public void TestAjusteVoltage()
        {
            tower.IO.DO.On(19);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            TestAjusteVoltage(adjustVoltage);
        }

        public void TestAjusteDesfase()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 60, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage,  adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            TestAjusteDesfase(powerFactor);
        }

        public void TestAjustePotencia()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);
            var powerReference = adjustVoltage * adjustCurrent * Math.Cos(powerFactor.ToRadians());

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage,  adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            TestAjustePotencia(powerReference);
        }

        public override void TestFinish()
        {
            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(500, OUT_PISTON_OPTICO);
                    tower.IO.DO.OffWait(500, OUT_PISTON_BORNES);
                    tower.IO.DO.OffWait(500, OUT_FIJACION_EQUIPO);
                    tower.IO.DO.Off(OUT_ACTIVE_230V_CDC, OUT_ACTIVE_VL2_TO_CDC, OUT_ACTIVE_NEUTRO_RELAY_CONTROL);
                }
            }

            base.TestFinish();
        }
    }
}
