﻿using Dezac.Tests.Model;
using Dezac.Tests.Protection;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dezac.Tests.Protection
{

    public class UtillajeEkorSafe : IUtillajeEkor
    {
        private Tower3 tower;
        private TestBase testBase;

        private const int OUT_KEY_SIDE = 54;
        private const int OUT_KEY_SET = 7;
        private const int OUT_KEY_ESC = 8;
        private const int OUT_KEY_PLUS = 10;
        private const int OUT_KEY_MIN = 9;
        private const int OUT_KEY_LEFT = 11;
        private const int OUT_KEY_RIGHT = 12;

        private const byte IN_OUT1 = 10;
        private const byte IN_OUT2 = 11;
        private const byte IN_OUT3 = 13;
        private const byte IN_OUT4 = 14;
        private const byte IN_TRIGGER_RELAY = 12;

        private const int OUT_DI_1 = 0;
        private const int OUT_DI_2 = 1;
        private const int OUT_DI_3 = 0;
        private const int OUT_DI_4 = 1;
        private const int OUT_DI_5 = 0;
        private const int OUT_DI_6 = 1;
        private const int OUT_DI_7 = 0;
        private const int OUT_DI_8 = 1;

        private const int OUT_ACTIVE_VL = 20;
        private const int OUT_ACTIVE_VF = 21;

        internal const int OUT_ACTIVE_IN_SAFE1 = 19;
        internal const int OUT_ACTIVE_IN_SAFE2 = 18;
        internal const int OUT_ACTIVE_IN_1357 = 0;
        internal const int OUT_ACTIVE_IN_2468 = 1;
        internal const int OUT_ACTIVE_CRGA_33R = 22;
        internal const int OUT_ACTIVE_RS485_B = 17;
        internal const int OUT_ACTIVE_RS485_EMBEDDED = 43;

        List<KeyValuePair<byte, byte>> outputsValue = new List<KeyValuePair<byte, byte>>
        {
            new KeyValuePair<byte, byte>(IN_OUT1, 0),
            new KeyValuePair<byte, byte>(IN_OUT2, 1),
            new KeyValuePair<byte, byte>(IN_OUT3, 2),
            new KeyValuePair<byte, byte>(IN_OUT4, 3)
        };

        List<KeyValuePair<byte, byte>> inputsValue = new List<KeyValuePair<byte, byte>>
        {
            new KeyValuePair<byte, byte>(OUT_ACTIVE_IN_SAFE1, 0),
            new KeyValuePair<byte, byte>(OUT_ACTIVE_IN_SAFE2, 1),
            new KeyValuePair<byte, byte>(OUT_DI_1, 2),
            new KeyValuePair<byte, byte>(OUT_DI_2, 3),
            new KeyValuePair<byte, byte>(OUT_DI_3, 4),
            new KeyValuePair<byte, byte>(OUT_DI_4, 5),
            new KeyValuePair<byte, byte>(OUT_DI_5, 6),
            new KeyValuePair<byte, byte>(OUT_DI_6, 7),
            new KeyValuePair<byte, byte>(OUT_DI_7, 8),
            new KeyValuePair<byte, byte>(OUT_DI_8, 9)
        };

        List<KeyValuePair<byte, byte>> keyBoardValue = new List<KeyValuePair<byte, byte>>
        {
                new KeyValuePair<byte, byte>(OUT_KEY_SET, 0),
                new KeyValuePair<byte, byte>(OUT_KEY_ESC, 2),
                new KeyValuePair<byte, byte>(OUT_KEY_PLUS, 1),
                new  KeyValuePair<byte, byte>(OUT_KEY_MIN, 3),
                new KeyValuePair<byte, byte>(OUT_KEY_LEFT, 5),
                new KeyValuePair<byte, byte>(OUT_KEY_RIGHT, 4),
                new KeyValuePair<byte, byte>(OUT_KEY_SIDE, 6)
        };

        public UtillajeEkorSafe(TestBase testBase, ITower tower)
        {
            this.tower = (Tower3)tower;
            this.testBase = testBase;
        }

        public void ActivateMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public void ActivatePistonRS232()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public void DesactivatePistonRS232()
        {
            return;
        }

        public int GetInputDetectionTrigger()
        {
            return IN_TRIGGER_RELAY;
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairInputs()
        {
            return inputsValue;
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairKeyBoard()
        {
            return keyBoardValue;
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairOutputs()
        {
            return outputsValue;
        }

        public byte GetOutActivateVF()
        {
            return OUT_ACTIVE_VF;
        }

        public byte GetOutActivateVL()
        {
            return OUT_ACTIVE_VL;
        }

        public int GetOutCLK()
        {
            throw new NotImplementedException();
        }

        public void ResetUSB()
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Thread.Sleep(2500);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(testBase.Consignas.GetDouble(testBase.Params.V.Null.TestPoint("USB_CARGA").Vnom.Name, 5, ParamUnidad.V), testBase.Consignas.GetDouble(testBase.Params.I.Null.TestPoint("USB_CARGA").Vnom.Name, 1, ParamUnidad.A));

            Thread.Sleep(4000);
        }

        public void SupplyUSB(bool state = true)
        {
            throw new NotImplementedException();
        }

        public void TestDeviceConnection()
        {
            throw new NotImplementedException();
        }

        public void TestDeviceDisconnection()
        {
            tower.IO.DO.OffWait(300, OUT_ACTIVE_IN_SAFE1, OUT_ACTIVE_IN_SAFE2);
            tower.IO.DO.OffWait(300, OUT_ACTIVE_IN_1357, OUT_ACTIVE_IN_2468);
            tower.IO.DO.OffWait(300, OUT_ACTIVE_CRGA_33R, OUT_ACTIVE_VL, OUT_ACTIVE_VF);
            tower.IO.DO.OffWait(300, OUT_ACTIVE_RS485_B, OUT_ACTIVE_RS485_EMBEDDED);
            tower.IO.DO.OffWait(300, 58, 6);
            tower.IO.DO.OffWait(300, 5);
            tower.IO.DO.OffWait(300, 4);
        }

        public void ActivatePistonTestKeyboard()
        {
        }

        public void DesactivatePistonTestKeyboard()
        {
        }
    }
}
