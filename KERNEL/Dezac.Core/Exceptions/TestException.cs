﻿namespace Dezac.Core.Exceptions
{
    public class TestException
    {
        public TestExceptionHardwareLevel0 HARDWARE
        {
            get { return new TestExceptionHardwareLevel0() { Category = ConstantsExceptions.Categoria.HARDWARE }; }
        }

        public TestExceptionSoftwareLevel0 SOFTWARE
        {
            get { return new TestExceptionSoftwareLevel0() { Category = ConstantsExceptions.Categoria.SOFTWARE }; }
        }

        public TestExceptionUUTLevel0 UUT
        {
            get { return new TestExceptionUUTLevel0() { Category = ConstantsExceptions.Categoria.UUT }; }
        }

        public TestExceptionProcesLevel0 PROCESO
        {
            get { return new TestExceptionProcesLevel0() { Category = ConstantsExceptions.Categoria.PROCESO }; }
        }

        public static TestException Create()
        {
            return new TestException();
        }
    }
}
