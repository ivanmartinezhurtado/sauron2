﻿using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Dezac.Tests.Services;
using Dezac.Core.Utility;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.19)]
    public class EKOR_SAFETestPlacas : TestBase
    {
        #region Veriables In/ Out
        private const int OUT_VENTILADOR = 40;
        private const int OUT_AISLADOR_USB = 15;
        private const int OUT_CLK = 17;
       
        private const byte IN_DEVICE_PRESENCE = 9;
        private const byte IN_BLOQUE_SEGURIDAD = 23;
        private const byte IN_DETECTA_ANCLA_1 = 25;
        private const byte IN_DETECTA_ANCLA_2 = 26;

        private const byte IN_VAUX_OK = 15;
        private const byte IN_GEN_OK = 16;
        private const byte IN_USB_OK = 17;
        private const byte IN_VCC_OK = 18;
        private const byte IN_VTPR_OK = 19;

        private const byte IN_TRIGGER_RELAY = 12;
        private const byte IN_OUT1 = 10;
        private const byte IN_OUT2 = 11;
        private const byte IN_OUT3 = 13;
        private const byte IN_OUT4 = 14;

        private const int OUT_ACTIVE_IN_SAFE1 = 19;
        private const int OUT_ACTIVE_IN_SAFE2 = 18;

        private const int OUT_ACTIVE_WTP200_INPAR = 0;
        private const int OUT_ACTIVE_WTP200_PAR = 1;

        private const int OUT_ACTIVE_IN_1357 = 0;
        private const int OUT_ACTIVE_IN_2468 = 1;

        private const int OUT_ACTIVE_CRGA_33R = 22;

        private const int OUT_ACTIVE_VL = 20;
        private const int OUT_ACTIVE_VF = 21;

        private const int OUT_ACTIVE_RS485_B = 17;
        private const int OUT_ACTIVE_RS485_EMBEDDED = 43;
        #endregion

        protected struct StructCurrentsInVoltageChannel
        {
            public TriLineValue currentVL { get; set; }
            public TriLineValue currentVF { get; set; }
            public TriLineValue resistenceVL { get; set; }
            public TriLineValue resistenceVF { get; set; }

            public TriLineValue CurrentsVL
            {
                get
                {
                    var values = (currentVL / resistenceVL) * 1000000;
                    return values;
                }
            }
            public TriLineValue CurrentsVF
            {
                get
                {
                    var values = (currentVF / resistenceVF) * 1000000;
                    return values;
                }
            }

            public List<double> AllCurrents
            {
                get
                {
                    var value = new List<double>();
                    value.AddRange(CurrentsVL.ToArray());
                    value.AddRange(CurrentsVF.ToArray());
                    return value;
                }
            }
        }

        protected StructCurrentsInVoltageChannel measureValue;

        public enum modeloEkorWTP
        {
            WTP100,
            WTP10,
            WTP200,
            MVE,
            MVE_CPU
        }
        [JsonIgnore]
        public modeloEkorWTP ModeloEKOR
        {
            get
            {
                modeloEkorWTP modelo = modeloEkorWTP.WTP100;
                switch (Identificacion.MODELO.ToUpper().Trim())
                {
                    case "WTP100":
                        modelo = modeloEkorWTP.WTP100;
                        break;
                    case "WTP10":
                        modelo = modeloEkorWTP.WTP10;
                        break;
                    case "WTP200":
                        modelo = modeloEkorWTP.WTP200;
                        break;
                    case "MVE":
                        modelo = modeloEkorWTP.MVE;
                        break;
                    case "MVE_CPU":
                        modelo = modeloEkorWTP.MVE_CPU;
                        break;
                }
                return modelo;
            }
        }

        private bool TestPlacas = false;
        private EKOR_WTP ekor;
        private TowerBoard tower;

        public void TestInitialization(int portEkor = 5)
        {
            if (portEkor == 0)
                portEkor = Comunicaciones.SerialPort;

            ekor = new EKOR_WTP(portEkor);
            ekor.Cirbus.PerifericNumber = 1;

            this.TestPlacas = true;

            var towerBoard = tower as TowerBoard;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out towerBoard))
            {
                towerBoard = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", towerBoard);
            }
            if (towerBoard == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower = towerBoard;

            tower.IO.DO.On(14);
            tower.ActiveVoltageCircuit();

            tower.LAMBDA.Initialize();
            tower.MTEPS.Output120A = false;

            if (!tower.IO.DI.WaitOn(IN_BLOQUE_SEGURIDAD, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL BLOQUE DE SEGURIDAD CONECTADO AL UTIL", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_BLOQUE_SEGURIDAD, 2000))
                throw new Exception("Error no se el equipo en el util");

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                throw new Exception("Error no se el equipo en el util");
        }

        public void TestDetectionInputsVmin()
        {
            var inputsValue = new Dictionary<EKOR_WTP.enumPowerStates, bool>();
            inputsValue.Add(EKOR_WTP.enumPowerStates.VCC_OK, false);
            inputsValue.Add(EKOR_WTP.enumPowerStates.VAUX_OK, false);
            inputsValue.Add(EKOR_WTP.enumPowerStates.VUSB_OK, false);
            inputsValue.Add(EKOR_WTP.enumPowerStates.VGEN_OK, false);

            foreach (KeyValuePair<EKOR_WTP.enumPowerStates, bool> input in inputsValue)
            {
                logger.InfoFormat("Test signal control {0}", input.ToString());

                SamplerWithCancel((p) =>
                {
                    var result = ekor.ReadPowerBitState((int)input.Key);

                    Resultado.Set(string.Format("{0}_{1}","VAUXmin",input.Key.ToString()), result.ToString(), ParamUnidad.SinUnidad);

                    return result == input.Value;

                }, string.Format("Error. no se detecta la entrada {0} en el estado esperado {1}", input.ToString(), input.Value.ToString()), 2, 500, 100, false, false);
            }
        }

        public void TestDetectionInputsVnom()
        {
            var inputsValue = new Dictionary<EKOR_WTP.enumPowerStates, bool>();
            inputsValue.Add(EKOR_WTP.enumPowerStates.VCC_OK, true);
            inputsValue.Add(EKOR_WTP.enumPowerStates.VAUX_OK, true);
            inputsValue.Add(EKOR_WTP.enumPowerStates.VUSB_OK, false);
            inputsValue.Add(EKOR_WTP.enumPowerStates.VGEN_OK, false);

            foreach (KeyValuePair<EKOR_WTP.enumPowerStates, bool> input in inputsValue)
            {
                logger.InfoFormat("Test signal control {0}", input.ToString());

                SamplerWithCancel((p) =>
                {
                    var result = ekor.ReadPowerBitState((int)input.Key);

                    Resultado.Set(string.Format("{0}_{1}", "VAUXnom", input.Key.ToString()), result.ToString(), ParamUnidad.SinUnidad);

                    return result == input.Value;

                }, string.Format("Error. no se detecta la entrada {0} en el estado esperado {1}", input.Key.ToString(), input.Value.ToString()), 2, 500, 100, false, false);
            }
        }

        public void TestComunication()
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(2000, "Espera reset hardware");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(15, 0.1);

            SamplerWithCancel((p) =>
            {
                var versionEkor = ekor.ReadVersion();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionEkor.version, Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del eKOR no es correcta"), ParamUnidad.SinUnidad);

                var versionDesarrollo = ekor.ReadVersionDesarrollo();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_DEVELOPER, versionDesarrollo.version, Identificacion.VERSION_DEVELOPER.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del eKOR no es correcta"), ParamUnidad.SinUnidad);

                var versionMicro = ekor.ReadMicrocontrolerType();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, versionMicro.cpu, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.COMUNICACIONES.NO_COINCIDE("Error, la  tipo de microcontrolador no es el correcto"), ParamUnidad.SinUnidad);

                Resultado.Set("CPU REVISION", versionMicro.revision, ParamUnidad.SinUnidad);

                return true;
            }, "Error de comunicaciones por el puerto RS232 frontal al preguntar Version firmware el eKOR", 2, 100, 1000, true, true);

            var stateFlash = ekor.ReadStateFLASH();

            Resultado.Set("FLAH_ID", stateFlash.ID, ParamUnidad.SinUnidad);
            Resultado.Set("FLAH_STATUS", stateFlash.state.ToString(), ParamUnidad.SinUnidad);

            Assert.IsTrue(stateFlash.state == EKOR_WTP.stateFLAASH.READY, Error().UUT.MEMORIA.SETUP(string.Format("Error, estado de la memoria flash en {0}", stateFlash.state.ToString())));

            ekor.FATFormat();
        }

        public void TestCalibrationClock(string portOptris)
        {
            tower.HP53131A
                .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
                .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 1.5)
                .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            tower.IO.DO.On(OUT_CLK);

            double temperature=0;

            if (string.IsNullOrEmpty(portOptris))
                throw new Exception("No se ha pasado el puerto del Thermometro Optris");

            var optrisPort = Convert.ToByte(GetVariable<string>(portOptris, portOptris).Replace("COM", ""));

            TestMeasureBase(defs,
            (step) =>
            {
                ekor.StartSignalClok(020);

                Delay(500, "");

                var medidaFreqMed = tower.HP53131A.RunMeasure(HP53131A.Variables.FREQ, 8000, HP53131A.Channels.CH1);

                SamplerWithCancel((p) =>
                {
                    ekor.ReadVersion();
                    return true;
                }, "Error de comunicaciones despues de esperar 10seg. al enviar CLK_DE2ZACFAB", 8, 1000, 2000, true, true);

                using (var thermometer = new Instruments.Measure.OPTRIS_CS(optrisPort))
                {
                    var defTemp = new AdjustValueDef() { Name = Params.TEMP.Null.Ajuste.TestPoint("CLK").Name, Min = Params.TEMP.Null.Ajuste.TestPoint("CLK").Min(), Max = Params.TEMP.Null.Ajuste.TestPoint("CLK").Max(), Unidad = ParamUnidad.SinUnidad };
                    thermometer.Initialization();
                    TestMeasureBase(defTemp, 
                    (p) =>
                    {
                        return temperature = thermometer.ReadTemperature();
                    }, 0, 3, 500, true);                    
                }
                
                var calibrationClk = ekor.CalibrationClock(medidaFreqMed.Value, temperature);

                return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1, 
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
            }, 0, 2, 1000);

            Delay(2000, "Wait grabacion reloj");

            tower.IO.DO.Off(OUT_CLK);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(2000, "Wait reset hardware");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(15, 0.5);
        }

        public void TestUSB()
        {
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Other("USB").ConCarga.Vnom.Name, 5.3, ParamUnidad.V), Consignas.GetDouble(Params.I.Other("USB").ConCarga.Vnom.Name, 0.5, ParamUnidad.A));

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            tower.IO.DO.On(OUT_AISLADOR_USB);

            DriveInfo usbDrive = null;

            SamplerWithCancel((p) =>
            {
                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME","EKOR_DISK",ParamUnidad.SinUnidad).Trim())
                    {
                        usbDrive = drive;
                        break;
                    }

                if (usbDrive==null)
                    throw new Exception("Error no se detecta la unidad USB en el S.O.");

                return !string.IsNullOrEmpty(usbDrive.RootDirectory.Name);

            }, "Error No se ha encontrado el dispositivo USB del EKOR", 20, 1000, 2000, false, false);

            if (tower.IO.DI[IN_USB_OK])
                throw new Exception(string.Format("Error no se detecta la entrada {0}", IN_USB_OK.ToString()));

            SamplerWithCancel((p) =>
            {
                var result = ekor.ReadPowerBitState((int)EKOR_WTP.enumPowerStates.VUSB_OK);

                Resultado.Set(EKOR_WTP.enumPowerStates.VUSB_OK.ToString(), result.ToString(), ParamUnidad.SinUnidad);

                return result == true;

            }, string.Format("Error. no se detecta la entrada {0} en el estado esperado {1}", EKOR_WTP.enumPowerStates.VUSB_OK.ToString(), "True"), 2, 500, 100, false, false);

            Resultado.Set("USB", "OK", ParamUnidad.SinUnidad);


            var consumptionMargins = new AdjustValueDef(Params.I_DC.Null.TestPoint("USB").Name, 0, Params.I_DC.Null.TestPoint("USB").Min(), Params.I_DC.Null.TestPoint("USB").Max(), 0, 0, ParamUnidad.A);

            TestMeasureBase(consumptionMargins, (p) =>
            {
                var consumo = tower.LAMBDA.ReadCurrent();
                return consumo;
            }, 0, 3, 500);


            tower.IO.DO.Off(OUT_AISLADOR_USB);

            tower.Chroma.ApplyOff();

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(15, 0.5);
        }

        public void TestVGEN()
        {
            tower.ShutdownSources();

            logger.InfoFormat("Test signal control {0}", EKOR_WTP.enumPowerStates.VGEN_OK.ToString());

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            var configuration = new TestPointConfiguration()
            {
                Current = 0.25,
                Frecuency = 0,
                Iteraciones = 3,
                ResultName = "VGEN",
                TiempoIntervalo = 1000,
                typePowerSource = TypePowerSource.LAMBDA,
                typeTestExecute = TypeTestExecute.OnlyActive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = 15,
                WaitTimeReadMeasure = 500
            };

            this.TestConsumo(tower, configuration);

            SamplerWithCancel((p) =>
            {
                var result = ekor.ReadPowerBitState((int)EKOR_WTP.enumPowerStates.VGEN_OK);

                Resultado.Set("VGEN_OK", result.ToString(), ParamUnidad.SinUnidad);

                return result;
            }, string.Format("Error. no se detecta la entrada {0} activada", EKOR_WTP.enumPowerStates.VGEN_OK.ToString()), 2, 500, 100, false, false);


            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.Chroma.ApplyOff();

        }

        public void TestDetectionInvertedPhase()
        {
            tower.MTEPS.ApplyPresetsAndWaitStabilisation(230, 0.08, 0, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            tower.IO.DO.On(OUT_ACTIVE_VL, OUT_ACTIVE_VF);
            Delay(2000, "Espera estabilizacion");          

            SamplerWithCancel((p) => {

                var voltage = ekor.ReadVoltage();
                this.Logger.InfoFormat("VOLTAGE:{0}", voltage);

                var measure = ekor.ReadCurrentPhase();

                double resultPhaseL1 = measure.PH1;
                if (measure.PH1 > 300)
                    resultPhaseL1 = measure.PH1 - 360;

                Assert.AreBetween(Params.PHASE.L1.Verificacion.Name, resultPhaseL1, Params.PHASE.L1.Verificacion.Min(), Params.PHASE.L1.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES("Error angulo de fase L1 fuera de margenes"), ParamUnidad.Grados);

                Assert.AreBetween(Params.PHASE.L2.Verificacion.Name, measure.PH2, Params.PHASE.L2.Verificacion.Min(), Params.PHASE.L2.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES("Error angulo de fase L2 fuera de margenes"), ParamUnidad.Grados);

                Assert.AreBetween(Params.PHASE.L3.Verificacion.Name, measure.PH3, Params.PHASE.L3.Verificacion.Min(), Params.PHASE.L3.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES("Error angulo de fase L3 fuera de margenes"), ParamUnidad.Grados);

                return true;

            }, "", 5, 1500, 0, false, false);

        }

        public void TestInputs()
        {
            var inputsValue = new List<KeyValuePair<byte, EKOR_WTP.enumInputs>>();
            inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_IN_SAFE1, EKOR_WTP.enumInputs.SAFE1));
            inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_IN_SAFE2, EKOR_WTP.enumInputs.SAFE2));

            if (ModeloEKOR != modeloEkorWTP.WTP100 && ModeloEKOR != modeloEkorWTP.MVE_CPU)
            {
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_INPAR, EKOR_WTP.enumInputs.IN1));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_PAR, EKOR_WTP.enumInputs.IN2));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_INPAR, EKOR_WTP.enumInputs.IN3));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_PAR, EKOR_WTP.enumInputs.IN4));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_INPAR, EKOR_WTP.enumInputs.IN5));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_PAR, EKOR_WTP.enumInputs.IN6));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_INPAR, EKOR_WTP.enumInputs.IN7));
                inputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumInputs>(OUT_ACTIVE_WTP200_PAR, EKOR_WTP.enumInputs.IN8));
            }

            foreach (KeyValuePair<byte, EKOR_WTP.enumInputs> input in inputsValue)
            {
                this.Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    return ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 3, 500, 200, false, false);

                if (input.Value == EKOR_WTP.enumInputs.SAFE1 || input.Value == EKOR_WTP.enumInputs.SAFE2)
                    if (!tower.IO.DI.WaitOn(IN_TRIGGER_RELAY, 5000))
                        throw new Exception("Error. no se detecta la IN12 (Rele disparo del equipo) activada");

                tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    return !ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 3, 500, 200, false, false);

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);

                if (input.Value == EKOR_WTP.enumInputs.SAFE1 || input.Value == EKOR_WTP.enumInputs.SAFE2)
                {
                    if (!tower.IO.DI.WaitOff(IN_TRIGGER_RELAY, 5000))
                        throw new Exception("Error. no se detecta la IN12 (Rele disparo del equipo) desactivada");

                    Resultado.Set("INPUT_TRIGGER", "OK", ParamUnidad.SinUnidad);
                }
            }
        }

        public void TestOutputs()
        {
            var ouputsValue = new List<KeyValuePair<byte, EKOR_WTP.enumOutputs>>();
            ouputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumOutputs>(IN_OUT1, EKOR_WTP.enumOutputs.RELE1));
            ouputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumOutputs>(IN_OUT2, EKOR_WTP.enumOutputs.RELE2));

            if (ModeloEKOR != modeloEkorWTP.WTP100 && ModeloEKOR != modeloEkorWTP.MVE_CPU)
            {
                ouputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumOutputs>(IN_OUT3, EKOR_WTP.enumOutputs.RELE3));
                ouputsValue.Add(new KeyValuePair<byte, EKOR_WTP.enumOutputs>(IN_OUT4, EKOR_WTP.enumOutputs.RELE4));
            }

            foreach (KeyValuePair<byte, EKOR_WTP.enumOutputs> output in ouputsValue)
            {
                this.Logger.InfoFormat("{0}", output.Value.ToString());

                SamplerWithCancel((p) =>
                {
                    ekor.ActivaDigitalOutput((int)output.Value);
                    Delay(200, "");
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 5, 200, 200, false, false);

                SamplerWithCancel((p) =>
                {
                    ekor.DesactivaDigitalOutput((int)output.Value);
                    Delay(2000, "");
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} desactivada", output.Value.ToString()), 5, 200, 200, false, false);

                Resultado.Set(string.Format("{0}", output.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestTrip()
        {
            int max = 50;
            do
            {
                ekor.Trip();
            } while (!tower.IO.DI.WaitOn(IN_TRIGGER_RELAY, 100) && max-- != 0);

            if (max == 0)
                throw new Exception("Error no se ha detectado la señal de TRIP por software");

            Resultado.Set("TRIP", "OK", ParamUnidad.s);
        }

        public void TestAutoAlimentacion()
        {
            tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(2000, "Wait reset hardware");

            double current = Consignas.GetDouble(Params.I.Null.TestPoint("AUTOSUPPLY").Name, 0.8, ParamUnidad.A);

            tower.MTEPS.ApplyPresetsAndWaitStabilisation(0, current, 0, 0, 0);

            tower.ActiveCurrentCircuit(false, true, true);

            internalTestAutoConsumo("L1");

            tower.ActiveCurrentCircuit(true, false, true);

            Delay(500, "espera cambio L2");

            internalTestAutoConsumo("L2");

            tower.ActiveCurrentCircuit(true, true, false);

            Delay(500, "espera cambio L3");

            internalTestAutoConsumo("L3");

            tower.ActiveCurrentCircuit(true, true, true);

            Delay(500, "espera cambio L3");

            tower.MTEPS.ApplyOffAndWaitStabilisation();

            tower.ActiveCurrentCircuit(false, false, false);

        }

        public void MeasureCurrentApplyVoltageChannel()
        {
            measureValue = new StructCurrentsInVoltageChannel();

            measureValue.resistenceVL = Configuracion.GetDouble(Params.RES.Other("VL").TESTER.Name, 10000, ParamUnidad.Ohms);
            measureValue.resistenceVF = Configuracion.GetDouble(Params.RES.Other("VF").TESTER.Name, 10000, ParamUnidad.Ohms);

            tower.MUX.Reset();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.I.L1.TESTER.modo("VL").Name, Params.I.Null.TESTER.modo("VL").Min(), Params.I.Null.TESTER.modo("VL").Max(), 0, 0, ParamUnidad.uA));
            defs.Add(new TriAdjustValueDef(Params.I.L1.TESTER.modo("VF").Name, Params.I.Null.TESTER.modo("VF").Min(), Params.I.Null.TESTER.modo("VF").Max(), 0, 0, ParamUnidad.uA));

            var measureVL = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltAC,
                NumMuestras = 3,
                Rango = 20,
                DigitosPrecision = 4,
                NPLC = 8
            };

            var measureVF = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltAC,
                NumMuestras = 3,
                Rango = 20,
                DigitosPrecision = 4,
                NPLC = 8
            };

            TestMeasureBase(defs,
                (step) =>
                {
                    CheckCancellationRequested();

                    Delay(2000, "");

                    var VL1 = tower.MeasureMultimeter(InputMuxEnum.IN2, measureVL, 5000);
                    tower.MUX.Reset();

                    //tower.MUX.Connect((int)InputMuxEnum.IN3_AuxMedida2, Outputs.Multimetro);
                    var VL2 = tower.MeasureMultimeter(InputMuxEnum.IN3, measureVL, 5000);
                    tower.MUX.Reset();

                    //tower.MUX.Connect((int)InputMuxEnum.IN4_AuxMedida3, Outputs.Multimetro);
                    var VL3 = tower.MeasureMultimeter(InputMuxEnum.IN4, measureVL, 5000);

                    measureValue.currentVL = TriLineValue.Create(VL1, VL2, VL3);

                    CheckCancellationRequested();

                    Delay(2000, "");

                    var VF1 = tower.MeasureMultimeter(InputMuxEnum.IN5, measureVF, 5000);
                    tower.MUX.Reset();

                    var VF2 = tower.MeasureMultimeter(InputMuxEnum.IN6, measureVF, 5000);
                    tower.MUX.Reset();

                    var VF3 = tower.MeasureMultimeter(InputMuxEnum.IN7, measureVF, 5000);
                    tower.MUX.Reset();

                    measureValue.currentVF = TriLineValue.Create(VF1, VF2, VF3);

                    return measureValue.AllCurrents.ToArray();

                }, 0, 2, 500);
        }

        public void TestCustomize()
        {
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(15, 0.1);

            SamplerWithCancel((p) =>
            {
                var versionEkor = ekor.ReadVersion();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionEkor.version, Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del eKOR no es correcta"), ParamUnidad.SinUnidad);

                return true;
            }, "Error de comunicaciones por el puerto RS232 frontal al preguntar Version firmware el eKOR", 2, 100, 1000, true, true);

            ekor.WriteDateTime();

            ekor.WriteBastidor(TestInfo.NumBastidor.Value.ToString());

            Delay(2000, "Espera ...");

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void TestFinish()
        {
            if (ekor != null)
                ekor.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                Delay(2000, "Espera apagar MTE");
                interalTestFinish();
                if (tower.IO != null)
                {
                    tower.IO.DO.Off(OUT_VENTILADOR);
                    tower.IO.DO.Off(OUT_CLK);
                    tower.IO.DO.Off(14);
                }
                tower.Dispose();
            }
        }

        private void internalTestAutoConsumo(string line)
        {
            if (ModeloEKOR == modeloEkorWTP.MVE_CPU)
                try
                {
                    ekor.ReadPowerBitState((int)EKOR_WTP.enumPowerStates.VCC_OK);
                    throw new Exception("Error modelo MVE este equipo no debe autoalimentarse");
                }
                catch (Exception)
                {
                    return;
                }
            else
            {
                var inputsValue = new Dictionary<EKOR_WTP.enumPowerStates, bool>();
                inputsValue.Add(EKOR_WTP.enumPowerStates.VCC_OK, ModeloEKOR == modeloEkorWTP.MVE ? false : true);
                inputsValue.Add(EKOR_WTP.enumPowerStates.VAUX_OK, false);
                inputsValue.Add(EKOR_WTP.enumPowerStates.VUSB_OK, TestPlacas ? false : true);
                inputsValue.Add(EKOR_WTP.enumPowerStates.VGEN_OK, false);

                Delay(1000, "Espera temps canvi linea");

                foreach (KeyValuePair<EKOR_WTP.enumPowerStates, bool> input in inputsValue)
                {
                    this.Logger.InfoFormat("Test signal control {0}", input.ToString());

                    SamplerWithCancel((p) =>
                    {

                        var result = ekor.ReadPowerBitState((int)input.Key);
                        return result == input.Value;

                    }, string.Format("Error. no se detecta la entrada {0} en el estado esperado {1}", input.Key.ToString(), input.Value.ToString()), 2, 500, 100, false, false);
                }
            }
            Resultado.Set(string.Format("AUTOSUPPLY_{0}", line), "OK", ParamUnidad.SinUnidad);
        }

        private void interalTestFinish()
        {
            try
            {
                if (ekor != null)
                    ekor.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (tower != null)
            {
                tower.IO.DO.OffWait(300, OUT_ACTIVE_IN_SAFE1, OUT_ACTIVE_IN_SAFE2);
                tower.IO.DO.OffWait(300, OUT_ACTIVE_IN_1357, OUT_ACTIVE_IN_2468);
                tower.IO.DO.OffWait(300, OUT_ACTIVE_CRGA_33R, OUT_ACTIVE_VL, OUT_ACTIVE_VF);
                tower.IO.DO.OffWait(300, OUT_ACTIVE_RS485_B, OUT_ACTIVE_RS485_EMBEDDED);
                tower.IO.DO.OffWait(300, 58, 6);
                tower.IO.DO.OffWait(300, 5);
                tower.IO.DO.OffWait(300, 4);
            }
        }

        private void ResetUSB(Action func = null)
        {
            try
            {
                ekor.Cirbus.ClosePort();
            }
            catch (Exception)
            {

            }

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(2500, "");

            if (func != null)
                func();

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.TestPoint("USB_CARGA").Vnom.Name, 5, ParamUnidad.V), Consignas.GetDouble(Params.I.Null.TestPoint("USB_CARGA").Vnom.Name, 1, ParamUnidad.A));

            Delay(8000, "");
        }
    }
}
