﻿using Dezac.Core.Utility;

namespace Instruments.IO
{
    public interface IIOOutput
    {
        Result<bool> On(int num, bool? throwException = null);
        bool On(params int[] nums);
        bool OnRange(int start, int end);
        Result<bool> Off(int num, bool? throwException = null);
        bool Off(params int[] nums);
        bool OffRange(int start, int end);
        bool PulseOn(int milliseconds, params int[] num);
        bool PulseOff(int milliseconds, params int[] num);
    }
}
