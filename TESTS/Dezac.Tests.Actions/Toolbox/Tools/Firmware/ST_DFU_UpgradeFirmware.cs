﻿using Comunications.ST_DFU;
using Dezac.Data.ViewModels;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ST_DFU_UpgradeFirmwareDescription))]
    public class ST_DFU_UpgradeFirmware : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Búsqueda del manual del binario")]
        [DefaultValue(false)]
        public bool SearchManualBinary { get; set; }

        [Category("3. Configuration")]
        [Description("Fichero de extensión del binario")]
        [DefaultValue("DFU")]
        public string ExtensionFileBinary { get; set; }

        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            string fileName = null;

            if (SearchManualBinary && SequenceContext.Current.RunMode == RunMode.Debug)
                iShell.RunSync(() =>
                {
                    var dlgOpenFile = new OpenFileDialog { Title = "Seleccionar Firmware" };
                    if (dlgOpenFile.ShowDialog() != DialogResult.OK)
                        throw new Exception("Upgrade firmware process cancelled!");

                    fileName = dlgOpenFile.FileName;
                });
            else
            {
                var data = SequenceContext.Current.Services.Get<ITestContext>();

                if (data == null)
                    Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("Falta iniciar una función de carga de datos de contexto del test (InitProduct)").Throw();

                if (data.NumOrden != null && data.NumProducto == 0)
                    Error().PROCESO.PARAMETROS_ERROR.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test").Throw();

                List<BinaryItemViewModel> binaryItem = new List<BinaryItemViewModel>();

                if (SequenceContext.Current.Variables.Get("BinaryItemViewModel") is List<BinaryItemViewModel>)
                    binaryItem = (List<BinaryItemViewModel>)SequenceContext.Current.Variables.Get("BinaryItemViewModel");
                else
                    binaryItem.Add(SequenceContext.Current.Variables.Get("BinaryItemViewModel") as BinaryItemViewModel);
                
                if (binaryItem == null)
                    throw new Exception("No se ha podido obtener de las variables de contexto ningun BinaryItemViewModel");

                var binary = binaryItem.Where(p => p.NombreFichero.ToUpper().Contains(ExtensionFileBinary)).FirstOrDefault();
                if (binary == null)
                    throw new Exception("No se ha encontrado en la collección ningun binario con la extensión deseada");

                fileName = binary.PathFicheroTest;
            }
            
            UInt16 VID;
            UInt16 PID;
            UInt16 Version;
            string errMsg = null;

            var firmwareUpdate = new ST_DFU_FirmwareUpdate();

            firmwareUpdate.MassErase();

            firmwareUpdate.ParseDFU_File(fileName, out VID, out PID, out Version);

            var progress = iShell.ShowDialog("Upgrade Firmware in ST DFU USB",
                () =>
                {
                    var c = new ProgressBarView();

                    firmwareUpdate.OnFirmwareUpdateProgress += (sender, evf) =>
                    {
                        iShell.Run(() =>
                        {
                            c.SetValue((int)evf.Percentage, evf.Message);
                            if (evf.Percentage == 100)
                            {
                                errMsg = evf.Message;
                                c.CloseView(evf.Failed ? DialogResult.Abort : DialogResult.OK);
                            }
                        });
                    };

                    c.Load += (s, ev) => { firmwareUpdate.UpdateFirmware(fileName, false); };

                    return c;
                },
                MessageBoxButtons.RetryCancel);

            if (progress != DialogResult.OK)
                throw new Exception(string.Format("Error upgrading firmware in ST DFU USB: {0}", errMsg));
        }
       
        public class ST_DFU_UpgradeFirmwareDescription : ActionDescription
        {
            public override string Name { get { return "ST_DFU"; } }
            public override string Category { get { return "Tools/Firmware"; } }
            public override string Description { get { return "Action para actualizar el Firmware del equipo"; } }
            public override Image Icon { get { return Properties.Resources.stmicroelectronics; } }
        }
    }
}
