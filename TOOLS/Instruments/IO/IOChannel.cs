﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.IO
{
    public class IOChannel
    {
        public int NumChannel { get; internal set; }

        public List<IOPort> Ports { get; set; }

        public IOChannel(int numChannel, int numPorts)
        {
            Ports = new List<IOPort>(numPorts);
            NumChannel = numChannel;

            for (int i = 0; i < numPorts; i++)
                Ports.Add(new IOPort(this));
        }

        public IOPort this[int index]
        {
            get { return Ports[index]; }
        }

        public IOType IOType
        {
            set
            {
                foreach (var port in Ports)
                    port.IOType = value;
            }
        }

        public int NumBits
        {
            get
            {
                int num = 0;

                foreach (IOPort port in Ports)
                    num += port.BitsPerPort;

                return num;
            }
        }
    }
}
