namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class BIEN
    {
        public BIEN()
        {
            this.BIEN1 = new HashSet<BIEN>();
            this.BIENASISTENCIA = new HashSet<BIENASISTENCIA>();
            this.T_LOG = new HashSet<T_LOG>();
            this.T_LOG1 = new HashSet<T_LOG>();
            this.T_LOG2 = new HashSet<T_LOG>();
            this.PRODUCTOFASE = new HashSet<PRODUCTOFASE>();
            this.BIENPARAM = new HashSet<BIENPARAM>();
        }
    
        public int NUMBIEN { get; set; }
        public string CODIGO { get; set; }
        public string NROSERIE { get; set; }
        public string DESCRIPCION { get; set; }
        public string IDPROVEEDOR { get; set; }
        public Nullable<decimal> VALORADQUISICION { get; set; }
        public Nullable<System.DateTime> FECHAADQUISICION { get; set; }
        public Nullable<System.DateTime> FECHABAJA { get; set; }
        public string DOCCONTABLE { get; set; }
        public string AMORTIZABLE { get; set; }
        public string USADO { get; set; }
        public string UBICACION { get; set; }
        public string IDFAMILIA { get; set; }
        public string CAUSABAJA { get; set; }
        public string IDSECCION { get; set; }
        public Nullable<int> NUMBIENPADRE { get; set; }
        public string NOTAS { get; set; }
        public byte[] IMAGEN { get; set; }
        public string SITUACION { get; set; }
        public Nullable<int> IDENTIFICACIONTEST { get; set; }
        public string IDEMPLEADO { get; set; }
        public string IDTIPO { get; set; }
        public string IDPROPIETARIO { get; set; }
        public string IDDEPOSITARIO { get; set; }
        public System.DateTime FECHAALTA { get; set; }
        public Nullable<int> NUMPROYECTO { get; set; }
        public string REFPROVEEDOR { get; set; }
    
        public virtual PROYECTO PROYECTO { get; set; }
        public virtual ICollection<BIEN> BIEN1 { get; set; }
        public virtual BIEN BIEN2 { get; set; }
        public virtual ICollection<BIENASISTENCIA> BIENASISTENCIA { get; set; }
        public virtual ICollection<T_LOG> T_LOG { get; set; }
        public virtual ICollection<T_LOG> T_LOG1 { get; set; }
        public virtual ICollection<T_LOG> T_LOG2 { get; set; }
        public virtual ICollection<PRODUCTOFASE> PRODUCTOFASE { get; set; }
        public virtual ICollection<BIENPARAM> BIENPARAM { get; set; }
    }
}
