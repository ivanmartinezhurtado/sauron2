﻿using Dezac.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.42)]
    public class EKOR_SAFE_OLD : DeviceBase, IEKOR_SAFE
    {
        private const string READVERSION = "VER";
        private const string READVERSIONDESARROLLO = "VDP";
        private const string READVERSIONBOOT = "VER BOOT";
        private const string READCRCBOOT = "CRC BOOT";

        private const string READBASTIDOR = "LID FN";
        private const string WRITEBASTIDOR = "EID FN {0:00000000}";
        private const string WRITESERIALNUMBER = "EID SN {0:00000000000000}";
        private const string READSERIALNUMBER = "LID SN";

        private const string READCALIBRATIONCHANNEL = "CAL C {0:00}";
        protected const string READCALIBRATIONSCALE = "CAL E {0:00}";
        private const string AUTOCALIBRATION = "CAL A {0:0000} {1} {2}";
        private const string AUTOCALIBRATIONVOLTAGE = "CAL A {0:0000} {1} {2} {3} {4}";
        private const string READAUTOCALIBRATION = "CAL A";
        protected const string WRITEFACTORSCALIBRATION = "CAL EW {0:00} {1}";
        private const string WRITESCALE = "TST SCALE {0}";

        private const string STARTSIGNALCLOK = "CLK DE2ACFAB S{0:000}";
        private const string CALIBRATIONCLOCK = "CLK F{0:000.000000} T{1:00.0}";
        private const string READCALIBRATIONCLOCK = "CLK I";
        private const string READTEMPERATURE = "CLK T";
        private const string READCOUNTTICKSLASTTEST = "CLK N";

        private const string WRITEDISPLAY = "PRT {0}";
        private const string WRITEDISPLAYTEST = "PRT S:{0}";

        private const string LEERESTADOENTRADADIGITAL = "CHK DIN {0:0000}";
        private const string LEERESTADOENTRADASDIGITALES = "CHK DIN {0:0000} {1:0000}";

        private const string ACTIVASALIDADIGITAL = "CHK SET DOUT {0:0000}";
        private const string DESACTIVASALIDADIGITAL = "CHK CLR DOUT {0:0000}";

        private const string LEERESTADOTECLA = "CHK KBD {0:0000}";
        private const string LEERESTADOTECLAS = "CHK KBD {0:0000} {1:0000}";

        private const string ACTIVALED = "CHK SET LED {0:0000}";
        private const string DESACTIVALED = "CHK CLR LED {0:0000}";

        private const string LEERESTADOALIMENTACION = "CHK POWER {0:0000}";
        private const string LEERESTADOALIMENTACIONES = "CHK POWER {0:0000} {1:0000}";

        private const string WRITEDATETIME = "ERL{0}";
        private const string READDATETIME = "LRL";

        private const string TRIP = "TRP";
        private const string CAPTUREFILEMEASURE = "CAP {0}";

        private const string READCURRENTINSTANT = "LII";
        protected const string READCURRENTFUNDAMENTAL = "LIF {0}";
        private const string READVOLTAGEINSTANT = "LVI";
        protected const string READVOLTAGEFUNFAMENTAL = "LVF {0}";
        private const string READSTATUSTEST = "TST";
        private const string READTEMPERATUREI2C = "I2C TMP";
        protected const string READPARAMETERSMEASURE = "VIP {0}";
        private const string READVOLTAGESMEASUREVERIFICATION = "LEV F";
        private const string IMPXML = "XML IMP {0}";
        private const string EXPXML = "XML EXP {0}";

        private const string READFLASHSTATUS = "FAT I";
        private const string FATFORMAT = "FAT FORMAT";
        private const string FATERASE = "FAT UNFORMAT";
        private const string FATRESETFAB = "FAT RST PRG";

        private const string READMICROCONTROLERTYPE = "VDP CPU";

        private const string WRITECONFIGURATIONDEFAULT = "STP HWR {0:00} {1:00}";
        private const string WRITECONFIGURATION = "STP HWR {0:00}";

        private const string READBATTERYLEVEL = "BAT {0}";

        private const string CRH_START = "CRH START";

        private const string CRH = "CRH";
        private const string CPF = "CPF {0:000} {1}";


        private const string READEEPROM = "EEP READ 0F00 0001";
        private const string WRITEEEPROM = "EEP WRITE 0F00 {0}";

        private const string MEDIDA_FREQUENIA = "LFR TST";

        private const string READPPP = "PPP DE2ACFAB";

        private const string WRITE_FACTORES_CVALIBRACION_DEFECTO = "CAL 0";

        private const string READ_KEYBOARD = "KPD";

        private const string SET_CIRCUIT_MEASURE_FREQ = "STP VER {0:00}";

        private const string GET_CIRCUIT_MEASURE_FREQ = "STP VER";

        [Flags]
        public enum enumInputs
        {
            SAFE1,
            SAFE2,
            IN1,
            IN2, 
            IN3,
            IN4,
            IN5,
            IN6,
            IN7,
            IN8 
        }       

        public enum enumInputsDIDO
        {
            [Description("Entrada digital 1")]
            IN1 = 0,
            [Description("Entrada digital 2")]
            IN2 = 1,
            [Description("Entrada digital 3")]
            IN3 = 2,
            [Description("Entrada digital 4")]
            IN4 = 3,
            [Description("Entrada digital 5")]
            IN5 = 4,
            [Description("Entrada digital 6")]
            IN6 = 5,
            [Description("Entrada digital 7")]
            IN7 = 6,
            [Description("Entrada digital 8")]
            IN8 = 7,
            [Description("Entrada digital 9")]
            IN9 = 8,
            [Description("Entrada digital 10")]
            IN10 = 9,
        }

        public enum enumLEDSDIDO
        {
            [Description("LED COM")]
            COM = 1,
            [Description("LED IN1")]
            IN1 = 2,
            [Description("LED IN2")]
            IN2 = 3,
            [Description("LED IN3")]
            IN3 = 4,
            [Description("LED IN4")]
            IN4 = 5,
            [Description("LED IN5")]
            IN5 = 6,
            [Description("LED IN6")]
            IN6 = 7,
            [Description("LED IN7")]
            IN7 = 8,
            [Description("LED IN8")]
            IN8 = 9,
            [Description("LED IN9")]
            IN9 = 10,
            [Description("LED IN10")]
            IN10 = 11,
            [Description("LED OUT1")]
            OUT1 = 12,
            [Description("LED OUT2")]
            OUT2 = 13,
            [Description("LED OUT3")]
            OUT3 = 14,
            [Description("LED OUT4")]
            OUT4 = 15,
            [Description("LED ALARM1")]
            ALARM1 = 16,
            [Description("LED ALARM2")]
            ALARM2 = 17,
            [Description("LED ALARM3")]
            ALARM3 = 18,
            [Description("LED ALARM4")]
            ALARM4 = 19
        }

        [Flags]
        public enum enumPowerStates
        {
            VUSB_OK,
            VCC_OK,
            VAUX_OK,
            VGEN_OK,
            VTRP_OK,       
        }

        [Flags]
        public enum Leds
        {
            LED_FASE = 1,
            LED_NEUTRO = 2,   
            LED_EXT = 3
        }

        [Flags]
        public enum enumKeyboard
        {
            SET ,
            UP ,
            ESC ,
            DOWN ,
            RIGHT ,
            LEFT,
            SIDE 
        }

        [Flags]
        public enum enumOutputs
        {
            RELE1,
            RELE2,
            RELE3,
            RELE4
        }

        public enum Scale
        {
            Fina = 0,
            Gruesa = 1,
            Cortoc = 2,
            Fondo = 3
        }

        public enum FileExportImport
        {
            HWR,
            MOD,
            CFG,
            INS,
            CAX,
            USR,
            CAL,
            RTC,
            ALL
        }

        [Flags]
        public enum ChannelCurrent
        {
            IL1 = (1 << 0),
            IL2 = (1 << 1),
            IL3 = (1 << 2),
            IH = (1 << 3),
            IL123 = IL1 | IL2 | IL3,
        }

        [Flags]
        public enum ChannelVoltage
        {
            VL1 = (1 << 4),
            VL2 = (1 << 5),
            VL3 = (1 << 6),
            VF1 = (1 << 7),
            VF2 = (1 << 8),
            VF3 = (1 << 9),
            VL123 = VL1 | VL2 | VL3,
            VF123 = VF1 | VF2 | VF3,
            VL123VF123 = VL1 | VL2 | VL3 | VF1 | VF2 | VF3,
        }

        public enum ModelHardware
        {
            WTP10 = 0,
            WTP100 = 1,
            WTP200 = 2,
            RPA100 = 3,
            MVE = 4,
            SAFE = 5,
            FABSAFE = 6,
            FABPLUS = 7,
            FAB_DIDO = 9,
            DIDO = 10,
            RPG_CI_RTU = 11,
            RPA200_C = 12,
            FAB_ENERGY_C = 13,
            RPA200_R = 14,
            FAB_ENERGY_R = 15,
            RPA200_T = 16,
            FAB_ENERGY_T = 17,
            LVM_100 = 18,
            RPA200_RPPL = 19,
            FABMOTOR = 20,
            TRAFO = 21,
            MVE_CPU = 99
        }

        public enum Lines
        {
            L1 = 1,
            L2 = 2,
            L3 = 3,
            LH = 5,
            LS = 6
        }

        public enum RegistersModbus
        {
            KEY_PSWI = 0X0501,
            SET_COM_TO_FAB = 0x0550,
            SERIAL_NUMBER = 0x0232,
            MODEL = 0x0108,
            DEVICE = 0x0239
        }
        
        public enum ModesAutoCalibration
        {
            ALL = 0x02FF,
            NEUTRAL = 0x0100
        }

        public enum stateFLAASH
        {
            NOT_INIALIZE,        //0       ,     No inicializada.  
            READY,               //1       ,     Funcionando -> FileSystem activo.
            BUSY,                //2       ,     Busy -> FileSystem ocupado por otra tarea.
            ERROR_HARDWARE_CPU,  //3       ,     Error al inicializar el hardware de la CPU.
            ERROR_CPI,           //4       ,     Error al inicializar el acceso SPI. 
            ERROR_DETECTION,     //5       ,     Error al detectar la memoria.         
            ERROR_FORMAT         //6       ,     Error en el formato de la memoria.         
        }

        public struct Version
        {
            public string model;
            public string version;
            public byte versionMajor;
            public byte versionMinor;
            public byte versionRevision;
            public DateTime fechaDesarrollo;
        }

        public struct StructCalibrationClock
        {
            public int Arr_50Hz_TM1;
            public int Arr_60Hz_TM1;
            public int Po;
            public int Err25;
            public int RTC_PRS;
        }

        public struct StructTemperature
        {
            public int temperatura;
            public int MSRPointsTemp;
            public string lastValueRTC;

            public int Temperatura { get { return temperatura / 10; } }
            public int MSR_PointsTemp { get { return MSRPointsTemp / 256; } }
            public int LastValueRTC { get { return int.Parse(lastValueRTC.Replace("0x", ""), NumberStyles.HexNumber); } }
        }

        public struct StructTicks
        {
            public int MaxTick;
            public int AvgTick;
            public int MinTick;
            public int TimeTick;
        }

        public struct StructCalibrationMeasure
        {
            public string resp;
            public int factor_IL1;
            public int factor_IL2;
            public int factor_IL3;
            public int factor_IH;
            public int factor_VL1;
            public int factor_VL2;
            public int factor_VL3;
            public int factor_VF1;
            public int factor_VF2;
            public int factor_VF3;
        }

        public struct VoltageMeasureVerification
        {
            public string RESP;
            public double VL1;
            public double VL2;
            public double VL3;
            public double VN;
            public double VLK;
            public double VL1Aux;
            public double VL2Aux;
            public double VL3Aux;
            public double VNAux;

            public double VoltageL1 { get { return VL1 / 100; } }
            public double VoltageL2 { get { return VL2 / 100; } } //mA
            public double VoltageL3 { get { return VL3 / 100; } } //mA

            public double VoltageL1Aux { get { return VL1Aux / 100; } }
            public double VoltageL2Aux { get { return VL2Aux / 100; } } //mA
            public double VoltageL3Aux { get { return VL3Aux / 100; } } //mA
        }

        public struct FLASHstruct
        {
            public stateFLAASH state;
            public string ID;
        }

        public struct MeasureInstante
        {
            public string RESP;
            public string V;
            public string I;
            public string P;
            public string Q;
            public string S;
            public string COS;
            public string PHI;

            public double Voltage
            {
                get
                {
                    var resp = V.Replace("V:", "");
                    return Convert.ToDouble(resp) / 100;
                }
            }
            public double Current
            {
                get
                {
                    var resp = I.Replace("I:", "");
                    return Convert.ToDouble(resp) / 300;
                }
            }
            public double Angle
            {
                get
                {
                    var resp = PHI.Replace("Phi:", "");
                    return Convert.ToDouble(resp) / 100D;
                }
            }
        }

        public struct MeasureStructVoltagePhase
        {
            public double V1;
            public double V2;
            public double V3;
            public double VN;
            public double VNS;
            public double VF1;
            public double VF2;
            public double VF3;
            public double VFN;
            public string signe;
            public double PH1;
            public double PH2;
            public double PH3;
            public double PHN;
            public double PHNS;
            public double PHF1;
            public double PHF2;
            public double PHF3;
            public double PHFN;

            public double phase1 { get { return PH1 / 100; } }
            public double phase2 { get { return PH2 / 100; } }
            public double phase3 { get { return PH3 / 100; } }
            public double phaseN { get { return PHN / 100; } }
            public double phaseFuse1 { get { return PHF1 / 100; } }
            public double phaseFuse2 { get { return PHF2 / 100; } }
            public double phaseFuse3 { get { return PHF3 / 100; } }
            public double phaseFuseN { get { return PHFN / 100; } }
        }

        public struct MeasureStructCurrenPhase
        {
            public double I1;
            public double I2;
            public double I3;
            public double IN;
            public double INS;
            public string signe;
            public double PH1;
            public double PH2;
            public double PH3;
            public double PHN;
            public double PHS;

            public double phase1 { get { return PH1 / 100; } }
            public double phase2 { get { return PH2 / 100; } }
            public double phase3 { get { return PH3 / 100; } }
            public double phaseN { get { return PHN / 100; } }
            public double phaseS { get { return PHS / 100; } }
        }

        public struct typeMicro
        {
            public string cpu;
            public string revision;
        }

        public CirbusDeviceSerialPort Cirbus { get; set; }

        public EKOR_SAFE_OLD()
        {
        }

        public EKOR_SAFE_OLD(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1, int timeout = 3000, int retries = 3)
        {
            if (Cirbus == null)
                Cirbus = new CirbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Cirbus.PortCom = port;

            Cirbus.PerifericNumber = periferic;
            Cirbus.TimeOut = timeout;
            Cirbus.Retries = retries;
        }

        public void WriteConfigurationDefaultFab(ModelHardware modelo, string subModel = "")
        {
            LogMethod();
            var modelString = ((byte)modelo);

            var retires = Cirbus.Retries;
            var timeout = Cirbus.TimeOut;
            Cirbus.Retries = 1;
            Cirbus.TimeOut = 6000;

            if (!string.IsNullOrEmpty(subModel))
                Cirbus.Write(WRITECONFIGURATIONDEFAULT, modelString.ToString("X2"), subModel);
            else
                Cirbus.Write(WRITECONFIGURATION, modelString.ToString("X2"));

            Cirbus.TimeOut = timeout;
            Cirbus.Retries = retires;
        }

        public string ReadBastidor()
        {
           LogMethod();
            return Cirbus.Read(READBASTIDOR, listErrorCirbus);
        }

        public void WriteBastidor(string bastidor)
        {
            LogMethod();
            Cirbus.Write(WRITEBASTIDOR, listErrorCirbus, bastidor.PadLeft(8, '0'));
        }

        public void WriteSerialNumber(string numeroserie)
        {
            LogMethod();
            Cirbus.Write(WRITESERIALNUMBER, listErrorCirbus, numeroserie.PadLeft(14, '0'));
        }

        public string ReadSerialNumber()
        {
            LogMethod();
            return Cirbus.Read(READSERIALNUMBER, listErrorCirbus); 
        }

        public Version ReadVersion()
        {
            LogMethod();
            
            return internalReadVersion(Cirbus.Read(READVERSION));     
        }

        public Version ReadVersionDesarrollo()
        {
            LogMethod();
            return internalReadVersion(Cirbus.Read(READVERSIONDESARROLLO));
        }

        public string ReadVersionBoot()
        {
            LogMethod();
            var boot = Cirbus.Read(READVERSIONBOOT);
            return boot;
        }

        public string ReadCRCBoot()
        {
            LogMethod();
            var crc = Cirbus.Read(READCRCBOOT, true).TrimStart();
            return crc;
        }

        private Version internalReadVersion(string resp)
        {
            Version version = new Version();
            var split = resp.Split(' ');

            if (split.Length < 3)
                throw new Exception("Error de longitud de trama en la respuesta de la version");

            version.model = split[1];
            version.version = split[2];

            var versionNumber = split[2].Split('.');
            if (versionNumber.Length < 4)
                throw new Exception("Error de longitud numerica de la de trama en la respuesta de la version");

            version.versionMajor = Convert.ToByte(versionNumber[1]);
            version.versionMinor = Convert.ToByte(versionNumber[2]);
            version.versionRevision = Convert.ToByte(versionNumber[3]);

            if (split.Length > 4)
                version.fechaDesarrollo = Convert.ToDateTime(split[3]);

            return version;
        }        

        public typeMicro ReadMicrocontrolerType()
        {
            LogMethod();
            var version = Cirbus.Read(READMICROCONTROLERTYPE).Split(':');
            var cpu = version[1].Substring(version[1].IndexOf("(") + 1, version[1].IndexOf(")") - 2);
            var revision =  version[2].Substring(version[2].IndexOf("(") + 1, version[2].IndexOf(")") - 2);

            return new typeMicro() { cpu = cpu, revision = revision };
        }

        //*****************************************************
        //*****************************************************
        //                    CALIBRATION
        //*****************************************************
        //*****************************************************

        public void WriteFactorsCalibrationDefault()
        {
            LogMethod();
            Cirbus.Write(WRITE_FACTORES_CVALIBRACION_DEFECTO, listErrorCirbus);
        }

        [Browsable(false)]
        public string ReadCalibrationChannelCurrent(ChannelCurrent channel)
        {
            LogMethod();
            return Cirbus.Read(READCALIBRATIONCHANNEL, listErrorCalibration, channel);
        }

        [Browsable(false)]
        public string ReadCalibrationChannelVolatge(ChannelVoltage channel)
        {
            LogMethod();
            return Cirbus.Read(READCALIBRATIONCHANNEL, listErrorCalibration, (byte)channel);
        }

        public virtual T ReadCalibrationScale<T>(Scale scale) where T : struct
        {
            LogMethod();
            return Cirbus.Read<T>(READCALIBRATIONSCALE, listErrorCalibration, (byte)scale);
        }

        public string ReadPPP()
        {
            LogMethod();
            try
            {
                Cirbus.Read(READPPP, listErrorPPP);
                return "PPP OK";
            }catch(Exception ex)
            {
                TestException.Create().UUT.HARDWARE.NO_COMUNICA("PUERTO PPP").Throw();
                return "";
            }
        }

        public double ReadTemperatureI2C()
        {
            LogMethod();

            return Cirbus.Read<double>(READTEMPERATUREI2C, listErrorCirbus) / 100;
        }

        public StructCalibrationMeasure ReadCalibrationScale(Scale scale)
        {
            LogMethod();
            return Cirbus.Read<StructCalibrationMeasure>(READCALIBRATIONSCALE, listErrorCalibration, (byte)scale);
        }

        public void WriteScale(Scale scale)
        {
            LogMethod();

            Cirbus.Write(WRITESCALE, listErrorCirbus, (ushort)scale + 1);
        }

        public void WriteAutoScale()
        {
            LogMethod();

            Cirbus.Write(WRITESCALE, listErrorCirbus, "A");
        }

        public void CalibrationCurrents(ChannelCurrent channel, Scale scale, int current_mA)
        {
            LogMethod();
            var valueChannel =  (ushort)channel;
            Cirbus.Write(AUTOCALIBRATION, listErrorCalibration, valueChannel.ToString("X4"), (byte)scale, current_mA * 1000);
        }

        public virtual void CalibrationVoltage(ChannelVoltage channel, List<double> current_uA, Scale scale = Scale.Fina)
        {
            LogMethod();
            var valueChannel = (ushort)channel;
            var param = new List<string>();
            param.Add(valueChannel.ToString("X4"));
            param.Add(((ushort)scale).ToString());
            param.AddRange(current_uA.ConvertAll((p) => Math.Round(p * 1000).ToString()));

            Cirbus.Write(AUTOCALIBRATIONVOLTAGE, listErrorCalibration, param.ToArray());
        }

        public bool ReadCalibrationFinished()
        {
            LogMethod();
            var x = Cirbus.Read(READAUTOCALIBRATION, true);
            _logger.DebugFormat(string.Format("response CAL : {0}", x));
            if (x.Equals(string.Empty))
                return true;

            return false;

        }

        [Browsable(false)]
        public void WriteFactorsCalibration<T>(Scale scale, T factors)
        {
            LogMethod();

            Cirbus.Write(WRITEFACTORSCALIBRATION, listErrorCalibration, (int)scale, factors.ToString());

            Thread.Sleep(2000);
        }

        public void Reset()
        {
            LogMethod();

            Cirbus.Retries = 0;
            Cirbus.WriteWhitoutResponse(CRH_START);
            Cirbus.Retries = 3;
            Thread.Sleep(1500);
        }

        [Browsable(false)]
        public string ReadCrash()
        {
            LogMethod();
            return Cirbus.Read(CRH);
        }

        //*****************************************************
        //*****************************************************
        //                  CLOCK COMMANDS
        //*****************************************************
        //*****************************************************        

        public void StartSignalClok(int secondDuration)
        {
            LogMethod();
            Cirbus.Retries = 0;
            Cirbus.WithTimeOut((p) => { p.Write(STARTSIGNALCLOK, listErrorClock, secondDuration); });
            Cirbus.Retries = 3;
        }

        public StructCalibrationClock CalibrationClock(double frequency, double temerature)
        {
            LogMethod();
            return Cirbus.Read<StructCalibrationClock>(CALIBRATIONCLOCK, listErrorClock, frequency, temerature);
        }

        public StructCalibrationClock ReadCalibrationClock()
        {
            LogMethod();
            return Cirbus.Read <StructCalibrationClock>(READCALIBRATIONCLOCK, listErrorClock);
        }

        [Browsable(false)]
        public StructTemperature ReadTemperature()
        {
            LogMethod();
            return Cirbus.Read <StructTemperature>(READTEMPERATURE, listErrorClock);
        }

        public StructTicks ReadCountTicksLastTest()
        {
            LogMethod();
            return Cirbus.Read<StructTicks>(READCOUNTTICKSLASTTEST, listErrorClock);
        }

        //*****************************************************
        //*****************************************************
        //                    DATETIME COMMANDS
        //*****************************************************
        //*****************************************************

        public void WriteDateTime()
        {
            LogMethod();
            var datetime = DateTime.Now.ToString("dd/MM/yy HH:mm:ss,fff"); //20/01/00 21:41:50,367
            Cirbus.Write(WRITEDATETIME, datetime);
        }

        public DateTime ReadDateTime()
        {
            LogMethod();
            var time = Cirbus.Read(READDATETIME, listErrorBit);
            IFormatProvider culture = new CultureInfo("es-ES", true);
            DateTime dateVal = DateTime.ParseExact(time, "dd/MM/yy HH:mm:ss,fff", culture);
            return dateVal;
        }

        //*****************************************************
        //*****************************************************
        //                    BIT COMMANDS
        //*****************************************************
        //*****************************************************       

        public void ActivaDigitalOutput(int bit)
        {
            LogMethod();
            Cirbus.Write(ACTIVASALIDADIGITAL, listErrorBit, bit.ToString("X4"));
        }

        public void DesactivaDigitalOutput(int bit)
        {
            LogMethod();
            Cirbus.Write(DESACTIVASALIDADIGITAL, listErrorBit, bit.ToString("X4"));
        }

        public void ActivaLED(int bit)
        {
            LogMethod();
            Cirbus.Write(ACTIVALED, listErrorBit, bit.ToString("X4"));
        }

        public void DesactivaLED(int bit)
        {
            LogMethod();
            Cirbus.Write(DESACTIVALED, listErrorBit, bit.ToString("X4"));
        }

        public bool ReadDigitalInput(int bit)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOENTRADADIGITAL, listErrorBit, bit.ToString("X4"));
            return resp == "1" ? true : false;
        }

        [Browsable(false)]
        public string ReadDigitalInputs(int bit, byte qtyBits)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOENTRADASDIGITALES, listErrorBit, bit.ToString("X4"), qtyBits.ToString("X4"));
            return resp;
        }

        public bool ReadKeyState(int bit)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOTECLA, listErrorBit, bit.ToString("X4"));
            return resp == "1" ? true : false;
        }

        [Browsable(false)]
        public string ReadKeysStates(int bit, byte qtyBits)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOTECLAS, listErrorBit, bit.ToString("X4"), qtyBits.ToString("X4"));
            return resp;
        }

        public bool ReadPowerBitState(int bit)
        {
            LogMethod();
            var resp = Cirbus.Read(LEERESTADOALIMENTACION, listErrorBit, bit.ToString("X4"));
            return resp == "1" ? true : false;
        }

        //****************************************************
        //**************************************************
        public Keyborad ReadKeyborad()
        {
            LogMethod();
            var resp = Cirbus.Read<Keyborad>(READ_KEYBOARD, false);
            return resp;
        }

        public struct Keyborad
        {
            private string aa;
            private string keyboard;
            private string nnnn;

            public string KEYBOARD { get { return keyboard.ToUpper().Replace("0X",""); } }
        }

        //*****************************************************
        //*****************************************************
        //     DISPLAY
        //*****************************************************
        //*****************************************************       

        [Browsable(false)]
        public void WriteDisplay(byte type)
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAY, listErrorBit, type);
        }

        public void WriteDisplayTestSegmentsOdd()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFFA00AAA0000AA00A0AAA0AAA00AA000A0");
        }

        public void WriteDisplayTestSegmentsEven()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFF05500055550055050005000550055500");
        }

        public void WriteDisplayTestSegmentsAll()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
        }

        public void WriteDisplayTestSegmentsNone()
        {
            LogMethod();
            Cirbus.Write(WRITEDISPLAYTEST, listErrorBit, "FFFFFFFFFFFF00000000000000000000000000000000");
        }

        //*****************************************************
        //*****************************************************
        //    TRIP (DISPARO)
        //*****************************************************
        //*****************************************************

        [Browsable(false)]
        public void ActivateTPR()
        {
            LogMethod();
            Cirbus.Write(TRIP);
        }

        //*****************************************************
        //*****************************************************
        //    FLASH 
        //*****************************************************
        //*****************************************************
      
        public FLASHstruct ReadStateFLASH()
        {
            LogMethod();
            var resp = Cirbus.Read(READFLASHSTATUS, listErrorFlash).Split(' ');

            if (resp.Count() == 0)
                throw new Exception("Error no se ha podido leer el estado de la Flash");

            var flashInfo = new FLASHstruct()
            {
                 ID=resp[1],
                 state = (stateFLAASH)Convert.ToByte(resp[0]),
            };
            return flashInfo;
        }

        public void FATFormat()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FATFORMAT);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public void FATResetDefaultFabrica()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FATRESETFAB);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public void FATErase()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FATERASE);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public void WriteEEPROM(int data)
        {
            LogMethod();
            Cirbus.Write(WRITEEEPROM, data.ToString("X2"));
        }

        public double ReadBattery(double vRef)
        {
            LogMethod();
            return Cirbus.Read<double>(READBATTERYLEVEL, listErrorCirbus, (int)(vRef * 1000)) / 1000;
        }

        public int ReadEEPROM()
        {
            LogMethod();       
            return Convert.ToInt32(Cirbus.Read(READEEPROM, listErrorCirbus), 16);
        }

        public Frequency ReadMEDIDA_FREQ()
        {
            LogMethod();
            var result = Cirbus.Read<Frequency>(MEDIDA_FREQUENIA, listErrorCirbus);
            return result;
        }

        public void WriteCircuitMeasureFreq(bool value)
        {
            LogMethod();
            Cirbus.Write(SET_CIRCUIT_MEASURE_FREQ, value == true ? "01" : "00");       
        }

        public bool ReadCircuitMeasureFreq()
        {
            LogMethod();
            var result = Cirbus.Read(GET_CIRCUIT_MEASURE_FREQ);
            return result.Trim() == "01";
        }

        public struct Frequency
        {
            private int FreqV2;
            private int FreqVSync;

            public double FREQ_V2 { get { return (FreqV2 * 50D) / 128D; } }
            public double FREQ_VSYNC { get { return (FreqVSync * 50D) / 128D; } }
        }

        /*************** MODBUS PROTOCOL ******************/
        public void RecoveryDefaultSettings(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            Cirbus.ClosePort();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.UseFunction06 = true;
                Modbus.PerifericNumber = periferico;
                Modbus.TimeOut = 2000;
                Modbus.Write((ushort)RegistersModbus.SET_COM_TO_FAB, (ushort)0xC0FA);
            };

            Cirbus.OpenPort();
        }      

        public int ReadModelModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            if (Cirbus.IsOpen)
                Cirbus.ClosePort();

            ushort modelCode;            
            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.PerifericNumber = periferico;
                modelCode = Modbus.ReadHoldingRegister((ushort)RegistersModbus.MODEL);
                modelCode >>= 8;
            };

            var model =  RelationCodeModel.Where(k => k.Key == modelCode).Select(v => v.Value).FirstOrDefault();
            return model;
        }

        public string ReadDeviceModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            ushort modelRead;
            if (Cirbus.IsOpen)
                Cirbus.ClosePort();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.PerifericNumber = periferico;
                modelRead = Modbus.ReadHoldingRegister((ushort)RegistersModbus.DEVICE);
            };

            return ((ModelHardware)modelRead).ToString();
        }

        public string ReadSerialNumberModbus(int port, int baudrate = 9600, byte periferico = 1, System.IO.Ports.Parity parity = System.IO.Ports.Parity.Even)
        {
            LogMethod();

            Cirbus.ClosePort();

            string sn = string.Empty;

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.PerifericNumber = 1;
                sn = Modbus.ReadHexStringHoldingRegister((ushort)RegistersModbus.SERIAL_NUMBER, 4);
            };

            Cirbus.OpenPort();
            return sn;
        }

        //*****************************************************
        //*****************************************************
        //    MEASURE 
        //*****************************************************
        //*****************************************************      

        [Browsable(false)]
        public MeasureStructCurrenPhase ReadCurrentPhase()
        {
            LogMethod();
            var resp = Cirbus.Read <MeasureStructCurrenPhase>(READCURRENTFUNDAMENTAL, listErrorFlash, "P1");
            return resp;
        }              

        [Browsable(false)]
        public MeasureStructVoltagePhase ReadVoltage()
        {
            LogMethod();
            var resp = Cirbus.Read<MeasureStructVoltagePhase>(READVOLTAGEFUNFAMENTAL, listErrorFlash, "P1");
            return resp;
        }

        public MeasureInstante ReadMeasure(Lines canal)
        {
            LogMethod();
            return Cirbus.Read<MeasureInstante>(string.Format(READPARAMETERSMEASURE, (byte)canal), false);
        }

        [Browsable(false)]
        public VoltageMeasureVerification ReadVoltageMeasureVerification()
        {
            LogMethod();
            return Cirbus.Read<VoltageMeasureVerification>(READVOLTAGESMEASUREVERIFICATION);
        }

        public T ReadVoltageMeasureVerification<T>() where T : struct
        {
            LogMethod();
            return Cirbus.Read<T>(READVOLTAGESMEASUREVERIFICATION);
        }      
      
        public void Trip()
        {
            LogMethod();
            try
            {
                Cirbus.Write(TRIP, listErrorTrip);
            }catch(Exception ex)
            {
                _logger.Warn(ex.Message);
                if (!ex.Message.Contains(listErrorTrip[1]))
                    throw;
            }
        }

        [Browsable(false)]
        public void StartGenerationFileCapture(byte ciclyNumber)
        {
            LogMethod();
            Cirbus.Write(CAPTUREFILEMEASURE, ciclyNumber);
        }

        [Browsable(false)]
        public string ReadStatusFileCapture()
        {
            LogMethod();
            return Cirbus.Read(CAPTUREFILEMEASURE,"");
        }

        [Browsable(false)]
        public string ReadCurrentInstant()
         {
             LogMethod();
             return Cirbus.Read(READCURRENTINSTANT);
         }

        [Browsable(false)]
        public string ReadCurrentFundamental()
         {
             LogMethod();
             return Cirbus.Read(READCURRENTFUNDAMENTAL);
         }

        [Browsable(false)]
        public string ReadVoltageFundamental()
         {
             LogMethod();
             return Cirbus.Read(READVOLTAGEFUNFAMENTAL);
         }

        public string ReadVoltageInstant()
         {
             LogMethod();
             return Cirbus.Read(READVOLTAGEINSTANT);
         }

        public string ReadStatusTest()
         {
             LogMethod();
             return  Cirbus.Read(READSTATUSTEST);
         }

        [Browsable(false)]
        public string ImportXML(FileExportImport file)
        {
            LogMethod();
            return Cirbus.Read(IMPXML, listErrorFileExport, file.ToString());
        }

        [Browsable(false)]
        public string ExportXML(FileExportImport file)
        {
            LogMethod();
            return Cirbus.Read(EXPXML, listErrorFileExport, file.ToString());
        }

        public override void Dispose()
        {
            if (Cirbus != null)
            {
                Cirbus.Dispose();
                Cirbus = null;
            }
        }

        public void WriteCPF_RTU(bool activated)
        {
            LogMethod();
            Cirbus.Write(CPF, "RTU", activated ? "0" : "1"); //la logica va al reves 
        }

        //****************************************************

        private Dictionary<byte, string> listErrorFileExport = new Dictionary<byte, string>(){
             { 3, "Error al importat/Exportar fichero" },
             { 4, "Error al crear XSD" },
             { 6, "Fallo en la sintaxis del comando" }
        };

        private Dictionary<byte, string> listErrorTrip = new Dictionary<byte, string>(){
             { 1, "Error TRIP ocupado" },
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        protected Dictionary<byte, string> listErrorCirbus = new Dictionary<byte, string>(){
             { 1, "Error comando Cirbus" },
             { 2, "Error comando Cirbus" },
             { 6, "Fallo en la sintaxis del comando" },
             { 7, "Fallo cheksum del comando" }
        };

        private Dictionary<byte, string> listErrorMeasure = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorClock = new Dictionary<byte, string>(){
             { 1, "Falla EEP 1º Pagina" },
             { 2, "Falla EEP 2º Pagina" },
             { 3, "Falla EEP 1º y 2º Pagina" },
             { 4, "Fallo indeterminado" },
             { 5, "Comando calibración sin haber echo el comando Start Test" },
             { 6, "Error sintaxis del comando CLK" },
             { 7, "Error ChekSum del comando CLK" }
        };

        protected Dictionary<byte, string> listErrorCalibration = new Dictionary<byte, string>(){
             { 2, "Error parametro incorrecto" },
             { 3, "Ha fallado la grabación de la primera copia en EEP" },
             { 4, "Ha fallado la grabación de la segunda copia en EEP" },
             { 5, "Ha fallado la grabación de las dos copias en EEP" },
             { 6, "Fallo en la sintaxis del comando CAL" },
             { 7, "Fallo al iniciar test" }
        };

        protected Dictionary<byte, string> listErrorPPP = new Dictionary<byte, string>()
        {
             { 1, "Falla password" },
             { 2, "Falla recepción COMAux" },
             { 3, "Falla cerrojo flash" },
             { 4, "Fallo indeterminado" },
             { 5, "Fallo comando Cirbus" }
        };

        protected Dictionary<byte, string> listErrorFlash = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorBit = new Dictionary<byte, string>(){
             { 2, "Error direccion fuera de limites" },
             { 6, "Fallo en la sintaxis del comando CHR" },
        };

        private Dictionary<byte, string> listErrorDisplay = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        private Dictionary<ushort, int> RelationCodeModel = new Dictionary<ushort, int>
        {
            {0, 100}, {1, 101}, {2, 102}, {3, 103}, {4, 202}, {5, 301}, {6, 302}, {7, 1000},
            {8, 1001}, {9, 1002}, {10, 2001}, {11, 2002}, {12, 3001}, {13, 3002}, {14, 1010}, {15, 1011},
            {16, 1012}, {17, 2011}, {18, 2012}, {19, 3011}, {20, 3012}, {21, 1020}, {22, 1021}, {23, 1022},
            {24, 2021}, {25, 2022}, {26, 3021}, {27, 3022}, {28, 1030}, {29, 1031}, {30, 1032}, {31, 2031},
            {32, 2032}, {33, 3031}, {34, 3032}, {35, 5020}, {36, 5022}, {37, 5030}, {38, 5032}, {39, 103},
            {40, 2024}, {41, 3024}
        };
    }
}
