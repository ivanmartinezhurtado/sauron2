﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Auxiliars;
using Instruments.Measure;
using Instruments.Towers;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.04)]
    public class EKOR_DTC2_CPUTestPlacas : TestBase
    {

        EKOR_DTC2 ekor;
        MTX3293 metrix;
        KeySight33500 generator;
        Tower3 tower;

        string IMAGES_PATH;

        public void TestInitialization(int port)
        {
            if (port == 0)
                port = Comunicaciones.SerialPort;

            ekor = new EKOR_DTC2(port);
            ekor.Cirbus.PerifericNumber = 0;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            IMAGES_PATH = this.PathCameraImages + @"\EKOR_DTC2\";

        }

        public void InitMetrix(string nameDevicePortVirtual)
        {
            var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            metrix = new MTX3293();
            metrix.PortCom = portMetrix;
        }

        public void TestCommunications()
        {
            SamplerWithCancel(
            (p) =>
            {
                ekor.ReadFirmware();
                return true;
            }, "Error no se ha podido comunicar con el equipo", 5, 500, 2000, false, false);

            var fw = ekor.ReadFirmware();
            var fwBBDD = Identificacion.VERSION_FIRMWARE;
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, fw, fwBBDD, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, versión de firmware incorrecta"), ParamUnidad.SinUnidad);

            var currentP = ekor.ReadMeasurementsCurrentDisplay().ScalePrecision;
            var currentCC = ekor.ReadMeasurementsCurrentDisplay().ScaleCC;
        }

        public void TestDisplay()
        {
            var frm = this.ShowForm(() => new ImageView("TEST DISPLAY y LED", IMAGES_PATH + "\\DISPLAY.jpg"), MessageBoxButtons.OKCancel, "Se ve el display y led como en la imagen?" );
            try
            {
                SamplerWithCancel((step) =>
                {
                    if (step % 5 == 0)
                        ekor.ReadyDisplay();

                    if (frm.DialogResult == DialogResult.OK)
                        return true;

                    else
                    {
                        if (frm.DialogResult == DialogResult.Cancel)
                            throw new Exception("Error display incorrecto");
                        else
                            return false;
                    }                    
                }, "", 50, 1500, 200, true, true, (e) => Error().UUT.HARDWARE.DISPLAY("INCORRECTO").Throw());
            }
            finally
            {
                this.CloseForm(frm);
            }
        }

        public void TestKeyboard()
        {
            foreach (EKOR_DTC2.Keyboard key in Enum.GetValues(typeof(EKOR_DTC2.Keyboard)))
                if (key != EKOR_DTC2.Keyboard.NO_BUTTON_OR_INPUT && key != EKOR_DTC2.Keyboard.UP_DOWN_BUTTON)
                {
                    var frm = this.ShowForm(() => new ImageView("Test Teclado", IMAGES_PATH + key.ToString() + ".jpg"), MessageBoxButtons.AbortRetryIgnore, "Pulse la tecla mostrada en la imagen");

                    try
                    {
                        SamplerWithCancel((p) =>
                            {
                                if (ekor.ReadKeyboard() == key)
                                    return true;
                                return false;
                            }
                            , string.Format("No se detecta la tecla {0}", key.GetDescription().Replace("_", " ")), 40, 500, 0, true, true, (e) => Error().UUT.HARDWARE.KEYBOARD(e).Throw());
                    }
                    finally
                    {
                        this.CloseForm(frm);
                    }

                    Resultado.Set(key.ToString(), "OK", ParamUnidad.SinUnidad);
                }

            var errorKey = string.Empty;
            SamplerWithCancel((step) =>
            {
                var key = ekor.ReadKeyboard();
                if (key == EKOR_DTC2.Keyboard.NO_BUTTON_OR_INPUT)
                    return true;

                errorKey =  key.GetDescription();
                return false;
            }, string.Format("Se ha detectado la tecla {0} activada cuando deberían estar todas desactivadas", errorKey), 10, 500, 200, false, false);
        }

        public void TestDateTime()
        {
            SamplerWithCancel(
            (p) =>
            {
                var currentDate = DateTime.Now;
                ekor.WriteDate(currentDate);
                return ekor.ReadDateTime().Year == currentDate.Year;
            }, "Error al activar el reloj de la placa", 3, 1000, 2000);           
        }

        public void TestFrequency()
        {
            try
            {
                var channel = (HP53131A.Channels)Consignas.GetDouble("CHANNEL_FRECUENCYMETER", 1, ParamUnidad.SinUnidad);
            }
            catch (InvalidCastException)
            {
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("CHANNEL_FRECUENCYMETER ha de ser 1 o 2").Throw();
            }

            tower.HP53131A
                .PresetAndConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.AC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
                .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI)
                .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.AutoStop, HP53131A.Gate.SlopeEnum.POS);

            var freq = new AdjustValueDef { Name = Params.FREQ.Null.TestPoint("OSC").Name, Min = Params.FREQ.Null.TestPoint("OSC").Min(), Max = Params.FREQ.Null.TestPoint("OSC").Max(), Unidad = ParamUnidad.mA };

            var frm = this.ShowForm(() => new ImageView("Test Frecuencia Oscilador", IMAGES_PATH + @"\FREQ.jpg"), MessageBoxButtons.AbortRetryIgnore, "Colocar la Sonda en los TP indicados");
            try
            {
                TestMeasureBase(freq,
                (step) =>
                {
                    return tower.MeasureWithFrecuency(60000);
                }, 0, 5, 500);
            }
            finally
            {
                this.CloseForm(frm);
            }
        }

        public void TestConsumo(ModeMeasureConsumption mode)
        {
            ekor.Cirbus.DtrEnable = false;
            ekor.Cirbus.RtsEnable = false;

            if (mode == ModeMeasureConsumption.STANDBYE)
                Delay(20000, "Esperando que el equipo entre en modo STANDBYE");

            var consumo = new AdjustValueDef { Name = Params.I_DC.Null.TestPoint(mode.ToString()).Name, Min = Params.I_DC.Null.TestPoint(mode.ToString()).Min(), Max = Params.I_DC.Null.TestPoint(mode.ToString()).Max(), Unidad = ParamUnidad.mA };

            TestMeasureBase(consumo,
            (step) =>
            {
                return Math.Abs(metrix.ReadCurrent(MTX3293.measureTypes.DC)) * 1000;
            }, 0, 10, 1000);

            ekor.Cirbus.DtrEnable = true;
            ekor.Cirbus.RtsEnable = true;
        }        

        public void TestEmptyCurrentChannels()
        {
            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.I.L1.EnVacio.TestPoint("PRECISION"), ParamUnidad.A), true);
            defs.Neutro.Name.Replace("LN", "LH");

            TestMeasureBase(defs,
                (step) =>
                {
                    var readed = ekor.ReadMeasurementsCurrentDisplay().ScalePrecision;
                    return new double[] { readed.L1, readed.L2, readed.L3, readed.LH };
                }, 0, 10, 2000);

            var defs2 = new TriAdjustValueDef(new AdjustValueDef(Params.I.L1.EnVacio.TestPoint("CC"), ParamUnidad.A), true);
            defs.Neutro.Name.Replace("LN", "LH");

            TestMeasureBase(defs2,
                (step) =>
                {
                    var readed = ekor.ReadMeasurementsCurrentDisplay().ScaleCC;
                    return new double[] { readed.L1, readed.L2, readed.L3, readed.LH };
                }, 0, 10, 2000);
        }

        public void TestFinish()
        {
            if (ekor != null)
                ekor.Dispose();

            if (tower != null)
                tower.Dispose(true);

            if (generator != null)
            {
                generator.Reset();
                generator.Dispose();
            }

            if (metrix != null)
                metrix.Dispose();
        }


        #region
        public enum ModeMeasureConsumption
        {
            NORMAL,
            STANDBYE
        }
        #endregion
    }
}
