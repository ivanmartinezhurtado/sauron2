﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(PM100Description))]
    public partial class UC_PM100 : UserControl, IDisposable
    {
        private MyComputer computer = new MyComputer();
        private PM100 watt;

        public Type InstrumentType
        {
            get
            {
                return typeof(PM100);
            }
        }

        public PM100 Instrument
        {
            get
            {
                if (watt == null)
                    watt = new PM100();

                return watt;
            }
            set
            {
                if (watt != null)
                {
                    watt.Dispose();
                    watt = null;
                }
                watt = value;
            }
        }

        public UC_PM100()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            try
            {

                var ports = new MyComputer().SerialPortNames.ToArray();
                if (ports.Length == 0)
                {
                    errorProvider1.SetError(cmb_Port, "No se ha encontrado puertos en el PC");
                    return;
                }

                cmb_Port.Items.AddRange(ports);
                cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM1");
                if (cmb_Port.SelectedItem == null)
                    cmb_Port.SelectedIndex = 0;
            }
            catch (Exception)
            {
                errorProvider1.SetError(cmb_Port, "No se ha encontrado el PM100");
            }

        }

        private void btnIDN_Click(object sender, EventArgs e)
        {
            var idn = Instrument.GetIdentification();
            //ActionClick("Reseteando", Instrument.GetIdentification, sender);
            txtIDN.Text = Instrument.Idn;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ActionClick("Reseteando", this.Instrument.Reset, sender);
        }

        private void btmSetConfigure_Click(object sender, EventArgs e)
        {
            this.Instrument.SetConfigure(35,1);
        }

        private void btnAVG_Click(object sender, EventArgs e)
        {
            this.Instrument.SetAverageFix(1);
        }

        private void btnShunt_Click(object sender, EventArgs e)
        {
            this.Instrument.SetShunt(PM100.Shunt.EXT);
        }

        private void btnClearSel_Click(object sender, EventArgs e)
        {
            this.Instrument.SetSelectionFunction(PM100.SelectFunction.CLR);
        }
        private void btnSelection_Click(object sender, EventArgs e)
        {
            this.Instrument.SetSelectionFunction(PM100.SelectFunction.VAS);
            this.Instrument.SetSelectionFunction(PM100.SelectFunction.AMP);
            this.Instrument.SetSelectionFunction(PM100.SelectFunction.PWF);
        }

        private void btnRead_Click(object sender, EventArgs e)
        { 
            this.Instrument.ReadForeGroundData();
            this.Instrument.ReadConfiguration(35);
            this.Instrument.ReadFundamentalData(PM100.FundamentalData.VAS);
            this.Instrument.ReadFunctionData(PM100.FuncOptions.AMP);
        }

        #region private functions

        private void ActionClick(string metodo, Action action, object sender)
        {
            errorProvider1.Clear();
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
            else
            {
                ToggleButtonsState((Button)sender);
                try
                {
                    var task = Task.Factory.StartNew((Action)(() => { action(); }));
                    LifeMethod(metodo, task);
                }
                catch (Exception ex)
                {
                    Logger.Error("Error por", ex);
                }
                ToggleButtonsState((Button)sender);
            }
        }

        //private T FunctionClick<T>(string metodo, Func<object,T> func, object sender, object arg)
        //{
        //    var Result TypeOf<T>;

        //    errorProvider1.Clear();
        //    if (cmb_Port.SelectedItem == null)
        //        errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
        //    else
        //    {
        //        ToggleButtonsState((Button)sender);
        //        try
        //        { 
        //            var task = Task.Factory.StartNew<T>((Func<T>)(() => { return func(arg); }));
        //            LifeMethod(metodo, task);
        //            Result = task.Result;
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Error("Error por", ex);
        //        }
        //        ToggleButtonsState((Button)sender);

        //        return Result;
        //    }
        //}

        private void ToggleButtonsState(Button btn)
        {
            if (btn.Equals(btnReset))
                btnReset.Enabled = !btnReset.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;

        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        public void Close()
        {
            Instrument.Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (watt != null)
                watt.Dispose();

            base.Dispose(disposing);
        }

        private void cmb_Port_SelectedIndexChanged(object sender, EventArgs e)
        {
            Instrument.SetPort(cmb_Port.SelectedItem.ToString());
        }

        #endregion


    }

    public class PM100Description : ControlDescription
    {
        public override string Name { get { return "PM100"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Line_chart_icon; } }
        public override Image IconBig { get { return Properties.Resources.PRS_400_3_1; } }
        public override Type SubcomponentType { get { return typeof(PM100); } }
    }
}
