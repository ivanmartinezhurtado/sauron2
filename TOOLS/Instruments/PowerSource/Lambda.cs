﻿using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using log4net;
using System;
using System.Globalization;
using System.IO.Ports;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.02)]
    public class Lambda : PowerSourceDCI, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Lambda));

        public enum PrivateCommands
        {
            SET_INIT = Commands.USER_DEFINED_COMMANDS + 1,
            READ_SN = Commands.USER_DEFINED_COMMANDS + 2
        };

        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private SerialPort sp;
        private byte _port = 12;
        private string idn = "";
        private string serialNumber = "";
        private bool IsInitialized = false;

        private const string PRESET_CONFIG_VOLTAGE_COMMAND = "PV {0:0.###}";
        private const string PRESET_CONFIG_CURRENT_COMMAND = "PC {0:0.###}";
        private const string APPLY_CONFIG_COMMAND = "OUT 1";
        private const string APPLY_OFF_COMMAND = "OUT 0";
        private const string RESET_COMMAND = "RST";
        private const string READ_VOLTAGE_COMMAND = "PV?";
        private const string READ_CURRENT_COMMAND = "MC?";
        private const string READ_STATUS_COMMAND = "STT?";
        private const string SET_INIT_COMMAND = "ADR {0:00}";
        private const string READ_IDN_COMMAND = "IDN?";
        private const string READ_SERIALNUMBER_COMMAND = "SN?";

        public Lambda()
            : base(logger)
        {
            InitializeComponent();

            protocol = new ASCIIProtocol() { CaracterFinTx = "\r", CaracterFinRx = "\r" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), logger);
            transport.Retries = 1;
            Tolerance = 1;
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public void Initialize(int address = 6)
        {
            IsInitialized = true;
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_INIT, true, address));         

            Presets.Current = 1;
            SendCommand(new Command<Commands>(Commands.SET_CURRENT, true));
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = 1000;
            sp.ReadTimeout = 1000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
        }

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.READ_SN, true));
            return string.Format("{0}, SN={1}", idn, serialNumber);
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            if (!sp.IsOpen)
                sp.Open();

            if (!IsInitialized)
                Initialize();

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_VOLTAGE_COMMAND, Presets.Voltage);
                    message.Validator = (m) => { return m.Text.Contains("OK"); };
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Voltage = Presets.Voltage;
                    break;
                case (int)Commands.SET_CURRENT:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_CURRENT_COMMAND, Presets.Current);
                    message.Validator = (m) => { return m.Text.Contains("OK"); };
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    break;
                case (int)Commands.APPLY_CONFIG:
                    message = new StringMessage { Text = APPLY_CONFIG_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("OK"); };
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)Commands.APPLY_OFF:
                    message = new StringMessage { Text = APPLY_OFF_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("OK"); };
                    transport.SendMessage<StringMessage>(message);
                     DevicePreset.Voltage = 0;
                     DevicePreset.Current = 0;
                    break;
                case (int)Commands.RESET:
                    message = new StringMessage { Text = RESET_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("OK"); };
                    transport.SendMessage<StringMessage>(message);
                     DevicePreset.Voltage = 0;
                     DevicePreset.Current = 0;
                    break;
                case (int)Commands.READ_VOLTAGE:
                case (int)Commands.READ_CURRENT:
                case (int)Commands.READ_STATUS:
                    message = new StringMessage { Text = READ_STATUS_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);

                    string[] tokens = response.Text.Split(',');

                    DeviceData.Voltage = Convert.ToDouble(tokens[0].Substring(3, tokens[0].Length - 4), enUS);
                    DeviceData.Current = Convert.ToDouble(tokens[2].Substring(3, tokens[2].Length - 4), enUS);

                    byte status = byte.Parse(tokens[4].Substring(3, tokens[4].Length - 4));
                    byte fault = byte.Parse(tokens[5].Substring(3, tokens[5].Length - 4));

                    Status.IsBusy = false;
                    Status.HasError = (status & 0x80) > 0;
                    Status.HasWarning = false;

                    // TODO: Codificar excepciones
                    if (Status.HasError)
                        throw new InstrumentException(1);
                    
                    break;

                case (int) PrivateCommands.SET_INIT:
                    message = StringMessage.Create(enUS, SET_INIT_COMMAND, command.Args);
                    message.Validator = (m) => { return m.Text.Contains("OK"); };
                    response = transport.SendMessage<StringMessage>(message);
                    break;

                case (int)Commands.READ_IDN:
                    message = StringMessage.Create(enUS, READ_IDN_COMMAND);
                   // message.Validator = (m) => { return m.Text.Contains("OK"); };
                    response = transport.SendMessage<StringMessage>(message);
                    idn = response.Text;
                    break;

                case (int)PrivateCommands.READ_SN:
                    message = StringMessage.Create(enUS, READ_SERIALNUMBER_COMMAND, command.Args);
                    //message.Validator = (m) => { return m.Text.Contains("OK"); };
                    response = transport.SendMessage<StringMessage>(message);
                    serialNumber = response.Text;
                    break;
                default:
                    logger.WarnFormat("COMMAND LAMBDA {0} NOT IMPLEMENTED", command.Commands);
                    break;
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }

        protected override bool ValidateEstabilization(double tolerance)
        {
            ReadVoltage();

            if (DevicePreset.Voltage == 0)
                return DeviceData.Voltage < VoltageOffMin;

            var Max = Math.Round(DevicePreset.Voltage + (DevicePreset.Voltage * (tolerance / 100)), 12);

            var Min = Math.Round(DevicePreset.Voltage - (DevicePreset.Voltage * (tolerance / 100)), 12);

            return DeviceData.Voltage > Min && (DeviceData.Voltage < Max);
        }
    }
}
