﻿using System;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    public partial class InputControl : UserControl
    {
        private int numIO;
        public String text = null;

        public InputControl()
        {
            InitializeComponent();
            //toolTip1.Active = true;
            //toolTip1.UseAnimation = true;
            //toolTip1.ToolTipIcon = ToolTipIcon.Info;
            //toolTip1.ToolTipTitle = "TEST";
            //toolTip1.Tag = btState;
        }

        public int NumIO
        { 
            get { return numIO; }
            set 
            {
                numIO = value;
                btState.Text = text == null ? numIO.ToString() : text;
            }
        }

        public void UpdateUI(bool state)
        {
            btState.ImageIndex = state ? 1 : 0;
        }
    }
}
