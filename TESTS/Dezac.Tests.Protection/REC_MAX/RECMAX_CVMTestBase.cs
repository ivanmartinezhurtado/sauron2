﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using FileHelpers;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using Dezac.Tests.Services;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.36)]
    public class RECMAX_CVMTestBase : TestBase
    {
        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        protected string CAMARA_DISPLAY { get; set; }
        protected string CAMARA_DIFERENCIAL { get; set; }

        private RECMAX rec;
        public RECMAX Rec
        {
            get
            {
                if (rec == null)
                    rec = new RECMAX(Comunicaciones.SerialPort2);

                return rec;
            }

        }

        private CVMC10 cvm;
        public CVMC10 Cvm
        {
            get
            {
                if (cvm == null)
                    cvm = new CVMC10(Comunicaciones.SerialPort);

                return cvm;
            }
        }

        protected double transformationRelation;
        protected double current;
        protected TriLineValue DesfaseValue;
        protected CVMC10.TipoInstalacion typeInstalacion;

        protected const byte BLOCK_SECURITY = 23;
        protected const byte DEVICE_PRESENCE = 19;
        protected const byte DETECTION_COMS_CVM = 33;
        protected const byte DETECTION_COMS = 34;
        protected const byte DETECTION_PISTON_SUPERIOR_CVM = 28;
        protected const byte DETECTION_PISTON_SUPERIOR = 29;
        protected const byte DETECTION_FIJACION_EQUIPO = 24;
        protected const byte DETECTION_PISTON_DESBLOQUEO_EQUIPO = 25;
        protected const byte DETECTION_PALANCA_MOTOR_ON = 16;
        protected const byte DETECTION_PALANCA_MOTOR_OFF = 17;
        protected const byte DETECTION_BLOQUEO_EQUIPO = 18;
        protected const byte DETECTION_PUNTAS_INFERIORES = 31;
        protected const byte DETECTION_PUNTAS_SUPERIORES = 30;
        protected const byte DETECTION_ANCLA = 15;
        protected const byte DETECTION_PINS_COMS = 35;
        protected const byte DETECTION_OUT_AUX = 10;
        protected const byte DETECTION_OUT_TRIP = 11;
        protected const byte DETECTION_OUT_ALARM = 14;
        protected const byte DETECTION_OUT_BREAK_NO = 12;
        protected const byte DETECTION_OUT_BREAK_NC = 13;
        protected const byte DETECTION_MCB_L1 = 6;
        protected const byte DETECTION_MCB_L2 = 7;
        protected const byte DETECTION_MCB_L3 = 8;

        protected const byte EVs = 5;
        protected const byte FIJACION_CARRO = 8;
        protected const byte FIJACION_EQUIPO = 55;
        protected const byte UNLOCK_DEVICE = 7;
        protected const byte SWITCH_PISTON_COMS = 53;
        protected const byte PISTON_COMS = 54;
        protected const byte SWITCH_PISTON_PUNTAS = 49;
        protected const byte PUNTAS_INFERIORES = 50;
        protected const byte PUNTAS_SUPERIORES = 51;
        protected const byte PISTON_BLOQUEO = 6;
        protected const byte RESET_KEY = 9;
        protected const byte TEST_KEY = 10;
        protected const byte PROG_KEY = 11;
        protected const byte MODELO_CIEGO_KEY = 12;
        protected const byte DISCONECT_TOROIDAL = 1;
        protected const byte EXTERNAL_TRIGGER = 17;
        protected const byte EXTERNAL_LOCKED = 18;
        protected const byte PILOTO_ERROR = 56;
        protected const byte CORRIENTE_DIRECTA = 41;
        protected const byte PUERTA_ABIERTA = 20;

        protected const int VUELTAS_TORO_UTIL = 20;

        public void DisposeDevice()
        {
            if (Cvm != null)
                Cvm.Dispose();
            if (Rec != null)
                Rec.Dispose();
        }

        public void TestSensorMagneto()
        {
            Rec.WriteOperatingMode(RECMAX.OperatingMode.TEST);
            var cont = 20;
            do
            {
                Rec.WriteOutputs(RECMAX.Outputs.TRIGGER_INDUCTOR, RECMAX.Outputs.DISPLAY_ACTIVATION);
                SamplerWithCancel((p) =>
                {
                    return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.MAGNETOTHERMIC);
                }, "Error, no se ha detectado el disparo de bobina", 5, 100, 500, false, false);

                SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "Error, no se ha detectado la palanca en la posición OFF", 5, 100, 0, false, false);

                Resultado.Set(string.Format("TRIGGER_INDUCTOR_{0}", cont), "OK", ParamUnidad.SinUnidad);

                Delay(1000, "Reposo de motor entre rearmes");

                Rec.WriteOutputs(RECMAX.Outputs.REARM, RECMAX.Outputs.DISPLAY_ACTIVATION);
                SamplerWithCancel((p) =>
                {
                    return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.MAGNETOTHERMIC);
                }, string.Format("Error, no se ha detectado la activacion del Magnetotermico"), 10, 100, 1500, false, false);

                SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NC] && !Tower.IO.DI[DETECTION_OUT_BREAK_NO]; }, "Error, no se ha detectado la palanca en la posición ON", 10, 100, 500, false, false);

                Resultado.Set(string.Format("REARM_{0}", cont), "OK", ParamUnidad.SinUnidad);

                Delay(2000, "Esperando al motor");

                cont--;
            } while (cont > 0);
        }

        public void TestSensorBloqueo()
        {
            rec.WriteOperatingMode(RECMAX.OperatingMode.TEST);
            var cont = 20;
            do
            {
                Tower.IO.DO.On(UNLOCK_DEVICE);
                SamplerWithCancel((p) => { return tower.IO.DI[DETECTION_PISTON_DESBLOQUEO_EQUIPO]; }, "No se ha detectado el bloque de deteccion del bloqueo amarillo llegar a su fin de carrera", 5, 500, 250, false, false);

                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[DETECTION_BLOQUEO_EQUIPO];
                }, "Error, no se ha detectado el bloqueo del equipo en la posición correcta", 5, 100, 500, false, false);


                Resultado.Set(string.Format("LOCK_DEVICE_{0}", cont), "OK", ParamUnidad.SinUnidad);

                Tower.IO.DO.OffWait(750, UNLOCK_DEVICE);

                cont--;
            } while (cont > 0);
        }

        protected virtual void DeviceConnection()
        {
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();

            SamplerWithCancel((p) => { return Tower.IO.DI[BLOCK_SECURITY]; }, "No se ha detectado el bloque de seguridad", 10, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[DEVICE_PRESENCE]; }, "No se ha detectado la presencia del equipo", 5, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[PUERTA_ABIERTA]; }, "No se ha detectado la puerta del útil cerrada", 10, 500);

            Tower.IO.DO.On(EVs);

            //PREPARACION RECMAX O RECMAXCVM
            Tower.IO.DO.On(FIJACION_CARRO);
            Tower.IO.DO.On(SWITCH_PISTON_PUNTAS);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PISTON_SUPERIOR_CVM] && !Tower.IO.DI[DETECTION_PISTON_SUPERIOR]; }, "No se ha detectado la posición corRecta del utillaje en los pistones superiores para el RECMAXCVM", 10, 500);
            Tower.IO.DO.On(SWITCH_PISTON_COMS);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_COMS_CVM] && !Tower.IO.DI[DETECTION_COMS]; }, "No se ha detectado la posición corRecta del utillaje en los pistones de comunicación para el RECMAXCVM", 10, 500);

            Tower.IO.DO.On(FIJACION_EQUIPO);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_FIJACION_EQUIPO]; }, "No se ha detectado la fijación del equipo", 5, 500);

            Tower.IO.DO.On(PUNTAS_INFERIORES, PUNTAS_SUPERIORES);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PUNTAS_INFERIORES]; }, "No se ha detectado la entrada de los pistones inferiores en el equipo", 10, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PUNTAS_SUPERIORES]; }, "No se ha detectado la entrada de los pistones superiores en el equipo", 10, 500);

            Tower.IO.DO.On(PISTON_COMS);
            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PINS_COMS]; }, "No se ha detectado la entrada del piston de comunicaciones TTL", 10, 500);

            Tower.IO.DO.OffWait(500, UNLOCK_DEVICE);
        }

        public virtual void CheckDeviceConnection()
        {

            SamplerWithCancel((p) => { return Tower.IO.DI[BLOCK_SECURITY]; }, "No se ha detectado el bloque de seguridad", 10, 500);
            SamplerWithCancel((p) => { return Tower.IO.DI[DEVICE_PRESENCE]; }, "No se ha detectado la presencia del equipo", 5, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PISTON_SUPERIOR_CVM] && !Tower.IO.DI[DETECTION_PISTON_SUPERIOR]; }, "No se ha detectado la posición corRecta del utillaje en los pistones superiores para el RECMAXCVM", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_COMS_CVM] && !Tower.IO.DI[DETECTION_COMS]; }, "No se ha detectado la posición corRecta del utillaje en los pistones de comunicación para el RECMAXCVM", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_FIJACION_EQUIPO]; }, "No se ha detectado la fijación del equipo", 5, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PUNTAS_INFERIORES] && Tower.IO.DI[DETECTION_PUNTAS_SUPERIORES]; }, "No se ha detectado la entrada de los pistones laterales en el equipo", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] || Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "No se ha detectado la palanca en ninguna de las dos posiciones (ON/OFF) --> SENSOR KO", 10, 500);

            string posicionPalanca = string.Empty;

            if (Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC])
                posicionPalanca = "OFF";
            else
                posicionPalanca = "ON";

            logger.InfoFormat("SENSOR OK --> Ha detectado la palanca en " + posicionPalanca);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO]; }, "No se ha detectado la palanca en la posicion OFF", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_ANCLA]; }, "No se ha detectado el ancla del equipo", 10, 500);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_PINS_COMS]; }, "No se ha detectado la entrada del piston de comunicaciones TTL", 10, 500);

            Tower.IO.DO.OffWait(500, UNLOCK_DEVICE);
        }

        protected virtual void DeviceDisConnection()
        {
            Tower.IO.DO.Off(PISTON_COMS);
            SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[DETECTION_PINS_COMS]; }, "No se ha detectado la salida del piston de comunicaciones TTL", 5, 500, 0, false, false);

            Tower.IO.DO.Off(PUNTAS_INFERIORES, PUNTAS_SUPERIORES);
            SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[DETECTION_PUNTAS_INFERIORES] && !tower.IO.DI[DETECTION_PUNTAS_SUPERIORES]; }, "No se ha detectado la entrada de los pistones laterales en el equipo", 5, 500, 0, false, false);

            Tower.IO.DO.Off(PISTON_BLOQUEO);
            Tower.IO.DO.Off(UNLOCK_DEVICE);

            SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[DETECTION_PISTON_DESBLOQUEO_EQUIPO]; }, "No se ha detectado el desbloqueo del equipo", 5, 500, 0, false, false);
            Tower.IO.DO.Off(FIJACION_EQUIPO);
            SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[DETECTION_FIJACION_EQUIPO]; }, "No se ha detectado la desconexión de la fijación del equipo", 5, 500, 0, false, false);

            Tower.IO.DO.Off(FIJACION_CARRO);
            Delay(1000, "");
        }

        public void DisconnectDevice()
        {
            DeviceDisConnection();
        }

        public virtual void TestInitialization(int portCVM = 0, int portREC = 0)
        {
            CAMARA_DISPLAY = GetVariable<string>("CAMARA_DISPLAY", CAMARA_DISPLAY);
            CAMARA_DIFERENCIAL = GetVariable<string>("CAMARA_DIFERENCIAL", CAMARA_DIFERENCIAL);

            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            Cvm.Modbus.PerifericNumber = Convert.ToByte(1);

            if (portREC == 0)
                portREC = Comunicaciones.SerialPort2;

            Rec.Modbus.PerifericNumber = Convert.ToByte(1);

            DeviceConnection();

            DesfaseValue = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };

            Tower.ActiveVoltageCircuit();

            Tower.IO.DO.Off(PILOTO_ERROR);

            Tower.PowerSourceIII.Tolerance = 5;
        }

        public void ReadDiferencial()
        {
            Tower.IO.DO.On(57);
            var modeloDiferencial = this.TestHalconReadBarCodeProcedure(CAMARA_DIFERENCIAL, TestHalconExtension.TypeBarcode._25_Interleaved_Dotprint, "BARCODE", "BARCODE", 3);
            Resultado.Set("MODELO_MAGNETOTERMICO_LEIDO", modeloDiferencial, ParamUnidad.SinUnidad);

            var modeloFiltrado = FilterBarcode(modeloDiferencial);
            Assert.AreEqual(ConstantsParameters.Identification.MODELO_MAGNETOTERMICO, modeloFiltrado.ToUpper(), Identificacion.MODELO_MAGNETOTERMICO.ToUpper(), Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("El elemento de corte no es el modelo correcto"), ParamUnidad.SinUnidad);

            Tower.IO.DO.Off(57);
        }

        public void TestTimeStart()
        {
            Tower.IO.DO.On(CORRIENTE_DIRECTA);

            Rec.FlagTest();
            Delay(1000, "Espera REC modo TEST");

            Rec.WriteOutputs(RECMAX.Outputs.REARM, RECMAX.Outputs.DISPLAY_ACTIVATION);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "Error, no se ha detectado la salida estado MCB activada", 10, 100, 1500);

            Delay(1500, "Espera para asegurar el retorno del motor");

            Tower.ActiveVoltageCircuit(false, false, false, true);
            Tower.ActiveCurrentCircuit(false, false, false);

            this.InParellel(
           () =>
           {
               Tower.MUX.Reset();
               Tower.MUX.Connect2((int)InputMuxEnum.IN14, Outputs.FreCH1, (int)InputMuxEnum.IN2, Outputs.FreCH2);

               Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.AC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
               Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 0.7);

               Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
               Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
           });

            var adjustValue = new AdjustValueDef(Params.TIME.Null.TestPoint("ARRANQUE"), ParamUnidad.s);

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 2500, HP53131A.Channels.CH1, () =>
                    {
                        Delay(50, "");
                        Tower.IO.DO.On(24);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw new Exception(string.Format("Error, el equipo no ha arrancado"));
                }
            }, 0, 1, 1000);

            Tower.IO.DO.Off(CORRIENTE_DIRECTA);
        }

        public virtual void TestLock()
        {
            Tower.IO.DO.On(UNLOCK_DEVICE);
            SamplerWithCancel((p) => { return tower.IO.DI[DETECTION_PISTON_DESBLOQUEO_EQUIPO]; }, "No se ha detectado el bloque de deteccion del bloqueo amarillo llegar a su fin de carrera", 5, 500, 250, false, false);

            SamplerWithCancel((p) =>
            {
                var reading = Rec.ReadInputStatus();
                return !reading.HasFlag(RECMAX.Inputs.ARD_MANUAL);
            }, "Error, no se ha detectado el equipo bloqueado", 5, 100, 100, false, false);
            Resultado.Set("LOCK_DEVICE", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.On(PISTON_BLOQUEO);
            SamplerWithCancel((p) =>
            {
                return !Tower.IO.DI[DETECTION_BLOQUEO_EQUIPO];
            }, "Error, no se ha detectado el equipo desbloqueado", 5, 100, 100, false, false);


            SamplerWithCancel((p) =>
            {
                var reading = Rec.ReadInputStatus();
                return reading.HasFlag(RECMAX.Inputs.ARD_MANUAL);
            }, "Error, no se ha detectado el equipo desbloqueado", 5, 100, 100, false, false);
            Resultado.Set("UNLOCK_DEVICE", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.OffWait(100, PISTON_BLOQUEO);
            Tower.IO.DO.Off(UNLOCK_DEVICE);
        }

        public virtual void TestComunications()
        {
            var versionCVM = string.Empty;
            SamplerWithCancel((p) =>
            {
                versionCVM = Cvm.ReadFirmwareVersion();
                return true;
            }, "Error de comunicaciones por RS485", 1, 500, 500, false, false);
            Resultado.Set("COMUNICATION_RS485", "OK", ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionCVM, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware del CVM incorRecta"), ParamUnidad.SinUnidad);

            Cvm.FlagTest();

            Cvm.WriteDisabledMasterCommunications();

            var versionREC = string.Empty;
            SamplerWithCancel((p) =>
            {
                versionREC = Rec.ReadFirmwareVersion();
                return true;
            }, "Error de comunicaciones por TTL", 1, 500, 500, false, false);
            Resultado.Set("COMUNICATION_TTL", "OK", ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_2, versionREC, Identificacion.VERSION_FIRMWARE_2, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware del REC incorRecta"), ParamUnidad.SinUnidad);

            Rec.WriteDefaultCustomization();
            Delay(1200, "Escribiendo parametrizacion por defecto REC");

            Cvm.FlagTest();

            Cvm.WriteDisabledMasterCommunications();
        }

        public virtual void TestTrigger()
        {
            Tower.IO.DO.On(CORRIENTE_DIRECTA);

            Tower.ActiveCurrentCircuit(true, true, true);

            if (Identificacion.MODELO.Contains("4P"))
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 230, L3 = 230 }, new TriLineValue { L1 = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.15, ParamUnidad.A), L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);
            else
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 0, L3 = 0 }, new TriLineValue { L1 = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.15, ParamUnidad.A), L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);

            Rec.WriteOperatingMode(RECMAX.OperatingMode.NORMAL);
            Delay(1000, "Reset equipo al ponerlo modo normal");

            Rec.WriteTriggerCurrent(2);
            Rec.WriteTriggerTime(0);

            ///
            Delay(100, "Cambiando de escala");
            Tower.ActiveCurrentCircuit(false, true, true);
            Delay(2100, "Estabilizando medida del equipo");

            var currentLeakVerif = new AdjustValueDef(Params.I.Other("LEAK").Verificacion.Name, 0, 0, 0, 0, Params.I.Null.TestPoint("LEAK").Tol(), ParamUnidad.s);

            TestCalibracionBase(currentLeakVerif,
               () =>
               {
                   var reading = Rec.ReadLeakCurrent();
                   return reading;

               },
               () =>
               {
                   return Tower.PowerSourceIII.ReadCurrent().L1;
               }, 1, 2, 1050, "Error, corriente medida de fuga fuera de márgenes");


            Tower.ActiveCurrentCircuit(true, true, true);

            var current = Rec.ReadLeakCurrent();
            logger.InfoFormat("Corriente leída mientras esta cortocircuitada en la torre : {0}", current);

            Rec.WriteTriggerCurrent(0);
            Delay(2100, "");


            SamplerWithCancel((step) =>
            {
                if (step == 3)
                {
                    Rec.FlagTest();
                    Rec.WriteOutputs(RECMAX.Outputs.REARM, RECMAX.Outputs.DISPLAY_ACTIVATION);
                    Delay(3000, "Rearmando equipo");
                    Rec.WriteOperatingMode(RECMAX.OperatingMode.NORMAL);
                }

                return Tower.IO.DI[DETECTION_MCB_L1];
            }, "Error, no se ha detectado la continuidad del magnetotermico en L1", 6, 1000, 0, false, false);

            if (Identificacion.MODELO.Contains("4P"))
                {

                SamplerWithCancel((step) =>
                {
                    return Tower.IO.DI[DETECTION_MCB_L2];
                }, "Error, no se ha detectado la continuidad del magnetotermico en L2", 6, 1000, 0, false, false);

                SamplerWithCancel((step) =>
                {
                    return Tower.IO.DI[DETECTION_MCB_L3];
                }, "Error, no se ha detectado la continuidad del magnetotermico en L3", 6, 1000, 0, false, false);

            }

            Tower.MUX.Reset();
            Tower.MUX.Connect2((int)InputMuxEnum.IN9, Outputs.FreCH1, (int)InputMuxEnum.IN2, Outputs.FreCH2);

            Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

            Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

            var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 5000, HP53131A.Channels.CH1, () =>
                    {
                        Delay(50, "");

                        if (!Tower.IO.DI[DETECTION_OUT_BREAK_NC] && Tower.IO.DI[DETECTION_OUT_BREAK_NO])
                            logger.InfoFormat("El equipo ya está disparado antes de sacar la corriente de la torre.");

                        Tower.ActiveCurrentCircuit(false, true, true);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw new Exception(string.Format("Error, el equipo no ha disparado"));
                }
            }, 0, 1, 1000);

            Tower.ActiveCurrentCircuit(true, true, true);
        }

        public virtual void TestSetupDefault()
        {       
            Rec.FlagTest();

            typeInstalacion = CVMC10.TipoInstalacion.NULL;
            Enum.TryParse<CVMC10.TipoInstalacion>(Configuracion.GetString("TIPO_INSTALACION", CVMC10.TipoInstalacion.TRIFASICA.ToString(), ParamUnidad.SinUnidad).ToUpper().Trim(), out typeInstalacion);

            if (typeInstalacion == CVMC10.TipoInstalacion.NULL)
                throw new Exception("Error, no se ha definido el tipo de instalacion");          

            Cvm.WriteModbusMapSelection(CVMC10.MemoriesMaps.C10);
            Cvm.WriteTipoInstalacion(typeInstalacion);

            var hardwareVector = new CVMC10.HardwareVector();
            hardwareVector.Comunication = VectorHardware.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rs232 = VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.In_Medida = VectorHardware.GetString("MEDIDA_NEUTRO", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Flex = VectorHardware.GetString("FLEX", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele1 = VectorHardware.GetString("RELE1", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele2 = VectorHardware.GetString("RELE2", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele3 = VectorHardware.GetString("RELE3", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele4 = VectorHardware.GetString("RELE4", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "5A", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "300V", ParamUnidad.SinUnidad);
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "CVM-C10", ParamUnidad.SinUnidad);

            SetVariable("VectorHardware", hardwareVector);

            Cvm.FlagTest();
            Cvm.WriteHardwareVector(hardwareVector);

            Cvm.WriteComunications();
            Cvm.WritePassword((ushort)Configuracion.GetDouble("PASSWORD", 1234, ParamUnidad.SinUnidad));

            var bloqueoSetup = Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad) == "NO" ? false : true;

            Cvm.WriteSetupLock(bloqueoSetup);
            Cvm.WriteTransformationRatio();
            current = Configuracion.GetDouble("SECONDARY_CURRENT", 1, ParamUnidad.A);

            int imputCurrentPrimary = 1;

            if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary5A)
                imputCurrentPrimary = 5;
            else if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary2A)
                imputCurrentPrimary = 2;
            else if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary1A)
                imputCurrentPrimary = 1;

            transformationRelation = imputCurrentPrimary / current;

            Cvm.WriteMaximumDemandConfiguration();

            var voltageGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 3625, ParamUnidad.Puntos);
            var currentGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO", 11500, ParamUnidad.Puntos);
            var activepowerGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KW + "_DEFECTO", 5436, ParamUnidad.Puntos);
            var gainGapDefault = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_DESFASE + "_DEFECTO", 1000, ParamUnidad.Puntos);

            Cvm.WriteDefaultGains((ushort)voltageGain, (ushort)currentGain, (ushort)activepowerGain, (ushort)gainGapDefault);
            Cvm.WriteInitialMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));
            Cvm.WriteScreenSelection(Convert.ToInt32(0x0003FFFF));
            Cvm.WriteAllAlarmsDefault();

            if (Configuracion.GetString("CONFIG_ALARM", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                Dictionary<CVMC10.AlarmRegisters, string> alarmConfigurations = new Dictionary<CVMC10.AlarmRegisters, string>();

                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_1, Configuracion.GetString("ALARM_REL1", "PARAM_CODE=200;HI=170;LO=90;DELAYON=60;DELAYOFF=60;HYST=0;LATCH=0;STATE=NA", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_2, Configuracion.GetString("ALARM_REL2", "NO", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_3, Configuracion.GetString("ALARM_TR1", "PARAM_CODE=200;HI=170;LO=90;DELAYON=60;DELAYOFF=60;HYST=0;LATCH=0;STATE=NA", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_4, Configuracion.GetString("ALARM_TR2", "NO", ParamUnidad.SinUnidad));

                foreach (KeyValuePair<CVMC10.AlarmRegisters, string> alarm in alarmConfigurations)
                {
                    if (!alarm.Value.Contains("NO"))
                    {
                        var fileHelper = new FileHelperEngine<CVMC10_File_Alarms>();
                        fileHelper.ErrorMode = ErrorMode.IgnoreAndContinue;
                        var alarmConfiguration = fileHelper.ReadString(alarm.Value);

                        var alarmConfig = new CVMC10.AlarmConfiguration()
                        {
                            variableNumber = Convert.ToUInt16(alarmConfiguration[0].PARAM_CODE),
                            maxValue = Convert.ToInt32(alarmConfiguration[0].HI),
                            minValue = Convert.ToInt32(alarmConfiguration[0].LO),
                            alarmDelay = Convert.ToUInt16(alarmConfiguration[0].DELAYON),
                            alarmOffDelay = Convert.ToUInt16(alarmConfiguration[0].DELAYOFF),
                            histeresis = Convert.ToUInt16(alarmConfiguration[0].HYST),
                            latching = Convert.ToUInt16(alarmConfiguration[0].LATCH),
                            alarmLogic = Convert.ToUInt16(alarmConfiguration[0].STATE)
                        };

                        Cvm.WriteAlarm(alarm.Key, alarmConfig);
                    }

                    Resultado.Set(alarm.Key.ToString(), alarm.Value, ParamUnidad.SinUnidad);
                }
            }

            Cvm.WriteSynchronism(10000);

            var vector = Cvm.ReadHardwareVector();
            var vectorHardwareBBDD = GetVariable<CVMC10.HardwareVector>("VectorHardware", vector);

            vector.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vector.VectorHardwareBoolean, vectorHardwareBBDD.VectorHardwareBoolean, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el grabado"));           
        }

        public void TestInternalTension()
        {
            var param = new AdjustValueDef(Params.V_DC.Null.TestPoint("TP12V_PROTECTION"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return (Rec.ReadVoltageV12TestPointValue() * (Rec.ReadVoltageV3TestPointValue() / Math.Pow(2, 12))) / 0.175;
            }, 0, 10, 50);

            var param2 = new AdjustValueDef(Params.V_DC.Null.TestPoint("TP3V_PROTECTION"), ParamUnidad.V);
            TestMeasureBase(param2,
            (step) =>
            {
                return Rec.ReadVoltageV3TestPointValue();
            }, 0, 10, 50);
        }

        public void TestDetectionToroidal()
        {
            Tower.IO.DO.OffWait(500, DISCONECT_TOROIDAL);

            SamplerWithCancel((p) =>
            {
                Rec.WriteOutputs(RECMAX.Outputs.TOROIDAL_TEST, RECMAX.Outputs.DISPLAY_ACTIVATION);
                Delay(200, "Espera mientras se realiza el test de toro.");
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.TOROIDAL_NOT_CONNECTED);
            }, "Error, no se ha detectado la conexión del toroidal", 2, 100, 0);

            Resultado.Set("TOROIDAL_CONNECTION", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.OnWait(500, DISCONECT_TOROIDAL);

            SamplerWithCancel((p) =>
            {
                Rec.WriteOutputs(RECMAX.Outputs.TOROIDAL_TEST, RECMAX.Outputs.DISPLAY_ACTIVATION);
                Delay(200, "Espera mientras se realiza el test de toro.");
                return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.TOROIDAL_NOT_CONNECTED);
            }, "Error, no se ha detectado la desconexión del toroidal", 2, 100, 0);

            Resultado.Set("TOROIDAL_DISCONNECTION", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.OffWait(500, DISCONECT_TOROIDAL);

            SamplerWithCancel((p) =>
            {
                Rec.WriteOutputs(RECMAX.Outputs.TOROIDAL_TEST, RECMAX.Outputs.DISPLAY_ACTIVATION);
                Delay(200, "Espera mientras se realiza el test de toro.");
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.TOROIDAL_NOT_CONNECTED);
            }, "Error, no se ha detectado la conexión del toroidal al volver a conectarlo", 2, 100, 0);
        }

        public virtual void TestDigitalInputs()
        {
            Tower.IO.DO.On(EXTERNAL_TRIGGER);
            SamplerWithCancel((p) =>
            {
                return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.EXTERNAL_TRIGGER);
            }, "Error, no se ha detectado la entrada Ext N.O OFF activada", 10, 100, 100, false, false);
            Tower.IO.DO.Off(EXTERNAL_TRIGGER);
            SamplerWithCancel((p) =>
            {
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.EXTERNAL_TRIGGER);
            }, "Error, no se ha detectado la entrada Ext N.O OFF desactivada", 10, 100, 100, false, false);

            Resultado.Set("INPUT_NO_OFF", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.On(EXTERNAL_LOCKED);
            SamplerWithCancel((p) =>
            {
                return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.ARD_REMOTE);
            }, "Error, no se ha detectado la entrada EXTERNAL LOCKED activada", 10, 100, 100, false, false);
            Tower.IO.DO.Off(EXTERNAL_LOCKED);
            SamplerWithCancel((p) =>
            {
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.ARD_REMOTE);
            }, "Error, no se ha detectado la entrada EXTERNAL LOCKED desactivada", 10, 100, 100, false, false);

            Resultado.Set("INPUT_ARD_REMOTE", "OK", ParamUnidad.SinUnidad);
        }

        public virtual void TestMeasureIDiferencial()
        {
            Rec.WriteCalculateCuadraticPointsOrder(RECMAX.CuadraticPointsCalculOrder.I_LEAK_SCALE_1);
            logger.Info("Calculando Puntos Cuadraticos ESCALA 1");
            Delay(2000, "Calculando Puntos");


            var paramScale1 = new AdjustValueDef(Params.CUADRATICPOINTS.Null.Ajuste.TestPoint("5A"), ParamUnidad.V);
            TestMeasureBase(paramScale1,
            (step) =>
            {
                return Rec.ReadCuadraticPoints();
            }, 0, 3, 2000);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 230, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0.025, L2 = 0, L3 = 0 }, 50, 0, DesfaseValue, true);
            Rec.WriteCalculateCuadraticPointsOrder(RECMAX.CuadraticPointsCalculOrder.I_LEAK_SCALE_0);
            logger.Info("Calculando Puntos Cuadraticos ESCALA 0");
            Delay(2000, "Calculando Puntos");

            var paramScale0 = new AdjustValueDef(Params.CUADRATICPOINTS.Null.Ajuste.TestPoint("500mA"), ParamUnidad.V);
            TestMeasureBase(paramScale0,
            (step) =>
            {
                return Rec.ReadCuadraticPoints();
            }, 0, 3, 2000);

            var factors = new RECMAX.FactorsCalibration();
            factors.pointsScale0 = (int)paramScale0.Value;
            factors.pointsScale1 = (int)paramScale1.Value;

            Rec.WriteFactorsCalibrationConstant(factors);
        }

        public void TestOffsetDiferencial()
        {
            Rec.WriteCalculateCuadraticPointsOrder(RECMAX.CuadraticPointsCalculOrder.I_LEAK_SCALE_0);
            logger.Info("Calculando Puntos Cuadraticos");
            Delay(2000, "Calculando Puntos");

            var param = new AdjustValueDef(Params.CUADRATICPOINTS.Null.Ajuste.TestPoint("0A"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return Rec.ReadCuadraticPoints();
            }, 0, 3, 2000);
        }

        public virtual void TestDigitalOutputs()
        {
            var listOutputsStates = new List<RECMAX.Outputs>();
            listOutputsStates.Add(RECMAX.Outputs.ALL_RELAYS);

            foreach (var output in listOutputsStates)
            {
                Rec.WriteOutputs(output, RECMAX.Outputs.DISPLAY_ACTIVATION);

                SamplerWithCancel((p) =>
                {
                    if (Tower.IO.DI[DETECTION_OUT_TRIP] && Tower.IO.DI[DETECTION_OUT_AUX])
                        return true;

                    if (!Tower.IO.DI[DETECTION_OUT_TRIP] && !Tower.IO.DI[DETECTION_OUT_AUX])
                        throw new Exception("Error, no se ha detectado la activación de ninguna de las dos salidas");
                    else
                    {
                        if (!Tower.IO.DI[DETECTION_OUT_TRIP])
                            throw new Exception("Error, no se ha detectado la activación de la salida TRIP");
                        else
                            throw new Exception("Error, no se ha detectado la activación de la salida AUX");
                    }
                }, string.Empty, 10, 100, 100, false, false);

                Rec.WriteOutputs(RECMAX.Outputs.DISPLAY_ACTIVATION);
                SamplerWithCancel((p) =>
                {
                    if (!Tower.IO.DI[DETECTION_OUT_TRIP] && !Tower.IO.DI[DETECTION_OUT_AUX])
                        return true;

                    if (Tower.IO.DI[DETECTION_OUT_TRIP] && Tower.IO.DI[DETECTION_OUT_AUX])
                        throw new Exception("Error, no se ha detectado la desactivación de ninguna de las dos salidas");
                    else
                    {
                        if (Tower.IO.DI[DETECTION_OUT_TRIP])
                            throw new Exception("Error, no se ha detectado la desactivación de la salida TRIP");
                        else
                            throw new Exception("Error, no se ha detectado la desactivación de la salida AUX");
                    }
                }, string.Empty, 10, 100, 100, false, false);

                Resultado.Set(string.Format("{0}", output.ToString()), "OK", ParamUnidad.SinUnidad);
            }

            Cvm.WriteAlarmMeasureVI();
            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[DETECTION_OUT_ALARM];
            }, string.Format("Error, no se ha detectado la salida de alarma"), 10, 100, 100, false, false);

            Cvm.WriteAlarmMeasureVI(false);
            SamplerWithCancel((p) =>
            {
                return !Tower.IO.DI[DETECTION_OUT_ALARM];
            }, string.Format("Error, no se ha detectado la salida de alarma"), 10, 100, 100, false, false);

            Resultado.Set("OUT_ALARM_VI", "OK", ParamUnidad.SinUnidad);
        }

        public virtual void TestAnalogOuputs()
        {
            if (Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]) //TODO mirar sensor palanca "Espera modificacion util"
            {
                Rec.WriteOutputs(RECMAX.Outputs.REARM, RECMAX.Outputs.DISPLAY_ACTIVATION);
                SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NC] && !Tower.IO.DI[DETECTION_OUT_BREAK_NO]; }, "Error, no se ha detectado la salida estado MCB activada", 3, 100, 1500, false, false);
                Delay(2500, "Espera entre rearmes");
            }

            Rec.WriteOutputs(RECMAX.Outputs.TRIGGER_INDUCTOR, RECMAX.Outputs.DISPLAY_ACTIVATION);
            SamplerWithCancel((p) =>
            {
                return Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.MAGNETOTHERMIC);
            }, "Error, no se ha detectado el disparo de bobina", 5, 100, 500, false, false);
            Resultado.Set("TRIGGER_INDUCTOR", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "Error, no se ha detectado la salida estado MCB desactivada", 5, 100, 0, false, false);
            Resultado.Set("OUT_BREAK_NO", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NO] && !Tower.IO.DI[DETECTION_OUT_BREAK_NC]; }, "Error, no se ha detectado la palanca en la posición OFF", 5, 100, 0, false, false);
            //TODO a espera de reparación sensor del útil para detectar el otro estado.

            Delay(1000, "Reposo de motor entre rearmes");

            Rec.WriteOutputs(RECMAX.Outputs.REARM, RECMAX.Outputs.DISPLAY_ACTIVATION);
            SamplerWithCancel((p) =>
            {
                return !Rec.ReadInputStatus().HasFlag(RECMAX.Inputs.MAGNETOTHERMIC);
            }, string.Format("Error, no se ha detectado la activacion del Magnetotermico"), 10, 100, 1500, false, false);
            Resultado.Set("REARM", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) => { return Tower.IO.DI[DETECTION_OUT_BREAK_NC] && !Tower.IO.DI[DETECTION_OUT_BREAK_NO]; }, "Error, no se ha detectado la salida estado MCB activada", 5, 100, 0, false, false);
            Resultado.Set("OUT_BREAK_NC", "OK", ParamUnidad.SinUnidad);
        }

        public void TestKeyboard()
        {
            Rec.FlagTest();
            Delay(2000, "");
            Rec.WriteOutputs(RECMAX.Outputs.DISPLAY_ACTIVATION);

            var dictionaryKeyboardOutputs = new Dictionary<RECMAX.Inputs, byte>();

            var hasDisplay = Configuracion.GetString("HAS_DISPLAY", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            var pulseTime = 500;

            if (hasDisplay)
            {
                dictionaryKeyboardOutputs.Add(RECMAX.Inputs.RESET_KEY, RESET_KEY);
                dictionaryKeyboardOutputs.Add(RECMAX.Inputs.TEST_KEY, TEST_KEY);
                dictionaryKeyboardOutputs.Add(RECMAX.Inputs.PROG_KEY, PROG_KEY);    
            }
            else
            {
                dictionaryKeyboardOutputs.Add(RECMAX.Inputs.TEST_RESET_KEY, MODELO_CIEGO_KEY);
                pulseTime = 1000;
            }
        
            foreach (KeyValuePair<RECMAX.Inputs, byte> key in dictionaryKeyboardOutputs)
            {
                Tower.IO.DO.PulseOn(pulseTime, key.Value);
                SamplerWithCancel((p) =>
                {                   
                    var keyRead = Rec.ReadInputStatus();
                    return keyRead.HasFlag(key.Key);
                }, String.Format("Error: No se detecta la activacion de la {0}", key.Key.GetDescription()), 5, 0, 0);

                Resultado.Set(String.Format("{0}", key.Key.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestLeds()
        {
            var hasDisplay = Configuracion.GetString("HAS_DISPLAY", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            if (hasDisplay)
            {
                var statesList = new Dictionary<RECMAX.DisplayLedsStates, string>();
                statesList.Add(RECMAX.DisplayLedsStates.DISPLAY_ODD_BACKLIGHT_GREEN, "GREEN_LED");
                statesList.Add(RECMAX.DisplayLedsStates.DISPLAY_PAIR_BACKLIGHT_RED, "RED_LED");

                foreach (var state in statesList)
                {
                    Rec.WriteDisplayAndLeds(state.Key);
                    Delay(500, "Esperando activacion de LEDs");
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA_DISPLAY, string.Format("RECMAX_{0}", state.Value), state.Value, "RECMAX");
                }
            }

            else
            {
                var ledsList = new List<RECMAX.Outputs>()
                {
                    {RECMAX.Outputs.GREEN_LED},
                    {RECMAX.Outputs.RED_LED}
                };

                Rec.WriteOutputs(RECMAX.Outputs.DISPLAY_ACTIVATION);

                foreach (var led in ledsList)
                {
                    Rec.WriteOutputs(led);
                    Delay(500, "Esperando activacion de LEDs");
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA_DISPLAY, string.Format("RECMAX_BLIND_{0}", led.ToString()), string.Format("BLIND_{0}", led.ToString()), "RECMAX");
                }

                Rec.WriteOutputs(RECMAX.Outputs.ALL_OFF);
            }
        }

        public void TestDisplay()
        {
            var stateDisplayList = new List<RECMAX.DisplayLedsStates>();
            stateDisplayList.Add(RECMAX.DisplayLedsStates.SEGMENTS_PAIRS_COMMONS_ODDS_GREEN);
            stateDisplayList.Add(RECMAX.DisplayLedsStates.SEGMENTS_ODDS_COMMONS_PAIRS_RED);

            Rec.WriteOutputs(RECMAX.Outputs.DISPLAY_ACTIVATION);

            foreach (var state in stateDisplayList)
            {
                Rec.WriteDisplayAndLeds(state);
                Delay(1000, "Esperando al display");
                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMARA_DISPLAY, string.Format("RECMAX_{0}", state.GetDescription()), state.GetDescription(), "RECMAX");
            }
        }

        public void TestShortCircuitCVM(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 230, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 170, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 0.1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 0.2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 0.3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, DesfaseValue, true);

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            if (Cvm.ReadHardwareVector().In_Medida)
                crucePistasI.Neutro = new AdjustValueDef(Params.I.LN.CrucePistas.Name, 0, 0, 0, ConsignasI.L1, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A);

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varslist = new List<double>(){vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                     vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varslist.Add(vars.ThreePhase.NeutralCurrent);

                    return varslist.ToArray();

                }, delFirts, samples, timeInterval);
        }

        public void TestAdjustClockCVM()
        {
            double sync = 0;
            SamplerWithCancel((p) =>
            {
                var fRecReading = Cvm.ReadFrequency().Instantaneous;
                sync = (50000000 / fRecReading) + 1;

                return true;
            }, "Error en el ajuste de la frecuencia", 3, 1000, 2000, false, false);

            var param = new AdjustValueDef(Params.GAIN_FREC.Null.TestPoint("SYNC").Name, 0, Params.GAIN_FREC.Null.TestPoint("SYNC").Min(), Params.GAIN_FREC.Null.TestPoint("SYNC").Max(), 0, 0, ParamUnidad.Puntos);
            TestMeasureBase(param,
            (step) =>
            {
                return sync;
            }, 0, 3, 2000);

            Cvm.WriteSynchronism((ushort)sync);
        }

        public virtual void TestAdjustCVM(double PF = 0, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Tower.PowerSourceIII.Tolerance = 0.05;

            var adjustVoltage = new TriLineValue() { L1 = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V), L2 = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V), L3 = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V) };
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, PF, ParamUnidad.Grados);

            if (typeInstalacion == CVMC10.TipoInstalacion.TRIFASICA)
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, DesfaseValue, true);
            else
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue() { L1 = adjustVoltage.L1, L2 = 0, L3 = 0 }, new TriLineValue() { L1 = adjustCurrent, L2 = 0, L3 = 0 }, 50, powerFactor, DesfaseValue, true);

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            if (Cvm.ReadHardwareVector().In_Medida)
                triGains[1].Neutro = new AdjustValueDef(Params.GAIN_I.LN.Ajuste.Name, 0, Params.GAIN_I.LN.Ajuste.Min(), Params.GAIN_I.LN.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            triGains.Add(new TriAdjustValueDef(Params.GAIN_KW.L1.Ajuste.Name, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.L23.Ajuste.Name, Params.GAIN_V.L31.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var currentReadings = Tower.PowerSourceIII.ReadCurrent();

            var adjustCurrentValues = new CVMC10.TriDouble() { L1 = currentReadings.L1, L2 = currentReadings.L2, L3 = currentReadings.L3 };

            var measureGains = Cvm.CalculateAdjustMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltage.L1, adjustCurrentValues, transformationRelation,
                (value) =>
                {

                    triGains[0].L1.Value = value.Voltage.L1;
                    triGains[0].L2.Value = value.Voltage.L2;
                    triGains[0].L3.Value = value.Voltage.L3;

                    triGains[1].L1.Value = value.Current.L1;
                    triGains[1].L2.Value = value.Current.L2;
                    triGains[1].L3.Value = value.Current.L3;

                    if (triGains[1].Neutro != null)
                        triGains[1].Neutro.Value = value.neutralCurrent.Value;

                    triGains[2].L1.Value = value.Power.L1;
                    triGains[2].L2.Value = value.Power.L2;
                    triGains[2].L3.Value = value.Power.L3;

                    triGains[3].L1.Value = value.Compound.L12;
                    triGains[3].L2.Value = value.Compound.L23;
                    triGains[3].L3.Value = value.Compound.L31;

                    return !HasError(triGains, false).Any();

                });

            foreach (var res in triGains)
                res.AddToResults(Resultado);

            if (HasError(triGains, false).Any())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            Cvm.WriteMeasureGains(measureGains.Item2);
        }

        public virtual void TestGapAdjustCVM(double PF = 60, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 0, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 0, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 0, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, PF, ParamUnidad.Grados);

            if (typeInstalacion == CVMC10.TipoInstalacion.TRIFASICA)
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 50, angleGap, DesfaseValue, true);
            else
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue() { L1 = adjustVoltage, L2 = 0, L3 = 0 }, new TriLineValue() { L1 = realAdjustCurrents.L1, L2 = 0, L3 = 0 }, 50, angleGap, DesfaseValue, true);

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.L1.Ajuste.Min(), Params.GAIN_DESFASE.L1.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var powerFactor = Math.Cos((angleGap.ToRadians()));

            var voltageRef = Tower.PowerSourceIII.ReadVoltage();
            var currentRef = Tower.PowerSourceIII.ReadCurrent();

            currentRef.L1 -= adjustCurrentOffsets.L1 *= transformationRelation;
            currentRef.L2 -= adjustCurrentOffsets.L2 *= transformationRelation;
            currentRef.L3 -= adjustCurrentOffsets.L3 *= transformationRelation;

            var powerReference = new CVMC10.TriDouble()
            {
                L1 = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians())),
                L2 = voltageRef.L2 * currentRef.L2 * Math.Cos((angleGap.ToRadians())),
                L3 = voltageRef.L3 * currentRef.L3 * Math.Cos((angleGap.ToRadians())),
            };

            var offsetGains = Cvm.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, powerReference, powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                },
                (activePowerOffset) => (activePowerOffset.L1 > 0 && activePowerOffset.L2 > 0 && activePowerOffset.L3 > 0));

            triDefs.AddToResults(Resultado);

            Assert.IsTrue(offsetGains.Item1, Error().UUT.AJUSTE.MARGENES("Error valor de ajuste de ganancia del desfase fuera de márgenes"));

            Cvm.WritePhaseGapGains(offsetGains.Item2);
        }

        public virtual void TestVerificationCVM(double PF = 60, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.25, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, PF, ParamUnidad.Grados);

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, toleranciaI, ParamUnidad.A));
            if (Cvm.ReadHardwareVector().In_Medida)
                defs[2].Neutro = new AdjustValueDef(Params.I.LN.Verificacion.Name, 0, 0, 0, current, toleranciaI, ParamUnidad.A);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varsList = new List<double>() {
                      vars.Phases.L1.ActivePower, vars.Phases.L2.ActivePower, vars.Phases.L3.ActivePower,
                      vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                      vars.Phases.L1.Current , vars.Phases.L2.Current, vars.Phases.L3.Current};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(vars.ThreePhase.NeutralCurrent);

                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = Tower.PowerSourceIII.ReadVoltage();
                    var currentRef = Tower.PowerSourceIII.ReadCurrent();

                    currentRef.L1 = (currentRef.L1) * transformationRelation;
                    currentRef.L2 = (currentRef.L2) * transformationRelation;
                    currentRef.L3 = (currentRef.L3) * transformationRelation;

                    TriLineValue PowerActive = new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians())),
                        L2 = voltageRef.L2 * currentRef.L2 * Math.Cos((angleGap.ToRadians())),
                        L3 = voltageRef.L3 * currentRef.L3 * Math.Cos((angleGap.ToRadians())),
                    };

                    var varsList = new List<double>() {
                        PowerActive.L1, PowerActive.L2, PowerActive.L3,
                        voltageRef.L1, voltageRef.L2, voltageRef.L3,
                        currentRef.L1, currentRef.L2 , currentRef.L3};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(currentRef.L1);

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);
        }

        public virtual void TestHardwareVerificationCVM()
        {
            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.THD_V.L1.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new TriAdjustValueDef(Params.THD_I.L1.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));

            TestMeasureBase(defs,
                (step) =>
                {
                    var THD = Cvm.ReadTotalHarmonicDistorsion();
                    return new double[] { THD.VoltageL1, THD.VoltageL2, THD.VoltageL3, THD.CurrentL1, THD.CurrentL2, THD.CurrentL3 };
                }, 2, 10, 1000);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.Name, 0, Params.FREQ.Null.Verificacion.Min(), Params.FREQ.Null.Verificacion.Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(defsFreq,
              (step) =>
              {
                  var freq = Cvm.ReadFrequency();
                  return freq.Instantaneous / 100D;
              }, 2, 10, 750);
        }

        public virtual void TestCustomization()
        {
            Cvm.FlagTest();

            Cvm.WriteDisabledMasterCommunications();

            Cvm.WriteSerialNumberLong(TestInfo.NumSerie);

            TestInfo.DeviceID = Identificacion.ID;

            Cvm.WriteSerialNumber(Convert.ToUInt32(TestInfo.DeviceID));

            Cvm.WriteTransformationRatio(1, 1, 75, 5);

            Rec.FlagTest();

            Rec.WriteBastidor(Convert.ToInt32(TestInfo.NumBastidor));

            ParametrizationCustom();

            tower.ActiveVoltageCircuit(false,false,false,false);
            Delay(3500, "Reset equipo");
            tower.ActiveVoltageCircuit();
            Delay(3000, "Alimentado equipo");

            Rec.FlagTest();
            Cvm.FlagTest();

            var vectorHardware = Cvm.ReadHardwareVector();

            Resultado.Set("COMUNICACIONES", vectorHardware.ComunicationString, ParamUnidad.SinUnidad);
            Resultado.Set("RS232", vectorHardware.Rs232String, ParamUnidad.SinUnidad);
            Resultado.Set("SHUNT", vectorHardware.ShuntString, ParamUnidad.SinUnidad);
            Resultado.Set("MEDIDA_NEUTRO", vectorHardware.In_MedidaString, ParamUnidad.SinUnidad);
            Resultado.Set("RELE1", vectorHardware.Rele1String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE2", vectorHardware.Rele2String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE3", vectorHardware.Rele3String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE4", vectorHardware.Rele4String, ParamUnidad.SinUnidad);
            Resultado.Set("ALIMENTACION", vectorHardware.PowerSupply.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_CORRIENTE", vectorHardware.InputCurrent.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_TENSION", vectorHardware.InputVoltage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("MODELO_CVM", vectorHardware.Modelo.ToString(), ParamUnidad.SinUnidad);

            Resultado.Set(ConstantsParameters.Identification.VECTOR_HARDWARE, vectorHardware.VectorHardwareTrama, ParamUnidad.SinUnidad);

            var vectorHardwareBBDD = GetVariable<CVMC10.HardwareVector>("VectorHardware", vectorHardware);

            vectorHardware.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vectorHardwareBBDD.VectorHardwareBoolean, vectorHardware.VectorHardwareBoolean, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el de la base de datos"));

            var bastidor = Rec.ReadBastidor();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor.ToString(), TestInfo.NumBastidor.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, número de bastidor leído del equipo no coincide con el grabado"), ParamUnidad.SinUnidad);

            var numSerie = Cvm.ReadSerialNumberLong();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numSerie.ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, número de serie leido no coincide con el grabado"), ParamUnidad.SinUnidad);

            var id = Cvm.ReadSerialNumber();
            Assert.AreEqual("ID", id.ToString(), TestInfo.DeviceID, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, número de identificador leido no coincide con el grabado"), ParamUnidad.SinUnidad);

            var errorCode = Rec.ReadErrorCode();
            if (errorCode != RECMAX.ErrorCodes.NO_ERROR)
                Error().UUT.FIRMWARE.SETUP(errorCode.ToString()).Throw();

            Cvm.WriteModbusMapSelection(CVMC10.MemoriesMaps.MINI);

            var modbusMap = Cvm.ReadModbusMapSelection();
            if (modbusMap != CVMC10.MemoriesMaps.MINI)
                Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("MAPA MEMORIA INCORRECTO").Throw();

            Cvm.CleanRegisters(CVMC10.ClearRegisters.BORRAR_TODO);
        }

        public virtual void TestFinish()
        {
            Tower.IO.DO.Off(UNLOCK_DEVICE);

            try
            {
                if (!Tower.IO.DI[DETECTION_OUT_BREAK_NO])
                {
                    Delay(2000, "Esperando maniobra del motor para no dejarlo a medias");

                    Rec.FlagTest();

                    Rec.WriteOutputs(RECMAX.Outputs.ENGINE_DOWN, RECMAX.Outputs.TRIGGER_INDUCTOR, RECMAX.Outputs.DISPLAY_ACTIVATION);

                    SamplerWithCancelWhitOutCancelToken(() =>
                    {
                        return Tower.IO.DI[DETECTION_OUT_BREAK_NO];
                    }, "Error, no se ha detectado la palanca en la posición OFF", 10, 200, 200, false, false);
                }
            }
            catch (Exception) { };

            Tower.ShutdownSources();

            DeviceDisConnection();

            if (Rec != null)
                Rec.Dispose();

            if (Cvm != null)
                Cvm.Dispose();

            if (Tower != null)
                Tower.Dispose();

            if (IsTestError)
                tower.IO.DO.On(PILOTO_ERROR);
        }

        protected void ParametrizationCustom()
        {
            //BORRADO PAGS
            Rec.WritePageErasing(RECMAX.PagesErasing.SETUP);
            Delay(30, "Borrando página SETUP");
            Rec.WritePageErasing(RECMAX.PagesErasing.PARAMETRIZATIONS);
            Delay(50, "Borrando página PARAMETRIZACIONES");

            //MODELO SUBMODELO
            RECMAX.DeviceModel model;
            Enum.TryParse<RECMAX.DeviceModel>(Identificacion.MODELO, out model);
            Rec.WriteDeviceModel(model);
            Delay(30, "Escribiendo Modelo");
            Resultado.Set("MODELO", model.ToString(), ParamUnidad.SinUnidad);
            var modelRead = Rec.ReadDeviceModel();
            logger.InfoFormat(string.Format("MODELO LEIDO : {0}", modelRead));

            var subModel = Identificacion.SUBMODELO;
            Rec.WriteDeviceSubModel(subModel);
            Delay(30, "Escribiendo SubModelo");
            Resultado.Set("SUBMODELO", subModel, ParamUnidad.SinUnidad);
            var submodelRead = Rec.ReadDeviceSubModel();
            logger.InfoFormat(string.Format("SUBMODELO LEIDO : {0}", submodelRead));

            WriteCostumerTableCurrentTime();                       

            //FRECUENCIA DE TRABAJO
            var frec = RECMAX.OperatingFrequency._50Hz;
            Enum.TryParse<RECMAX.OperatingFrequency>(Parametrizacion.GetString("WORK_FRECUENCY", "_50Hz", ParamUnidad.SinUnidad), out frec);
            Rec.WriteOperatingFrequency(frec);
            Delay(30, "Escribiendo Frecuencia de trabajo");
            Resultado.Set("FRECUENCIA_TRABAJO", frec.ToString(), ParamUnidad.SinUnidad);
            var fr = Rec.ReadOperatingFrequency();
            logger.InfoFormat(string.Format("FRECUENCIA_TRABAJO LEIDA : {0}", fr.ToString()));

            //FUNCIÓN RELÉ AUXILIAR
            var alarm = RECMAX.TriggerCurrentAlarmPercentage.ERROR;
            Enum.TryParse<RECMAX.TriggerCurrentAlarmPercentage>(Parametrizacion.GetString("FUNCTION_RELAY_AUXILIAR", "ERROR", ParamUnidad.SinUnidad), out alarm);
            Rec.WritePrealarmCurrentPercentage(alarm);
            Delay(30, "Escribiendo Funcion relé auxiliar");
            Resultado.Set("FUNCION_RELE_AUX", alarm.ToString(), ParamUnidad.SinUnidad);
            var al = Rec.ReadPrealarmCurrentPercentage();
            logger.InfoFormat(string.Format("FUNCION RELE AUX LEIDA : {0}", al.ToString()));

            //SEGURIDAD RELÉ TRIP AND RELAY AUX
            var tripSecurity = RECMAX.RelayLogic.STANDARD;
            Enum.TryParse<RECMAX.RelayLogic>(Parametrizacion.GetString("RELAY_TRIP_SECURITY", "STANDARD", ParamUnidad.SinUnidad), out tripSecurity);
            Rec.WriteSecurityTripRelay(tripSecurity);
            Delay(30, "Escribiendo seguridad rele Trip");
            Resultado.Set("SEGURIDAD_RELE_TRIP", tripSecurity.ToString(), ParamUnidad.SinUnidad);
            var trip = Rec.ReadSecurityTripRelay();
            logger.InfoFormat(string.Format("SEGURIDAD RELE TRIP LEIDA : {0}", trip.ToString()));

            var auxiliarSecurity = RECMAX.RelayLogic.STANDARD;
            Enum.TryParse<RECMAX.RelayLogic>(Parametrizacion.GetString("RELAY_AUXILIAR_SECURITY", "STANDARD", ParamUnidad.SinUnidad), out auxiliarSecurity);
            Rec.WriteSecurityAuxRelay(auxiliarSecurity);
            Delay(30, "Escribiendo seguridad rele Auxiliar");
            Resultado.Set("SEGURIDAD_RELE_AUXILIAR", auxiliarSecurity.ToString(), ParamUnidad.SinUnidad);
            var aux = Rec.ReadSecurityAuxRelay();
            logger.InfoFormat(string.Format("SEGURIDAD RELE AUX LEIDA: {0}", aux.ToString()));

            //SECUENCIA RECONEXIÓN DIFERENCIAL
            var srd = Parametrizacion.GetString("SRD", "10", ParamUnidad.SinUnidad);
            Rec.WriteDifferentialReconnectionSequence(Convert.ToUInt16(srd));
            Delay(30, "Escribiendo SRD");
            Resultado.Set("SRD", srd, ParamUnidad.SinUnidad);
            var srdRead = Rec.ReadDifferentialReconnectionSequence();
            logger.InfoFormat(string.Format("SRD LEIDO : {0}", srdRead));

            //SECUENCIA RECONEXIÓN MAGNETOTÉRMICO
            var srm = Parametrizacion.GetString("SRM", "0", ParamUnidad.SinUnidad);
            Rec.WriteMagnetothermicReconnectionSequence(Convert.ToUInt16(srm));
            Delay(30, "Escribiendo SRM");
            Resultado.Set("SRM", srm, ParamUnidad.SinUnidad);
            var srmRead = Rec.ReadMagnetothermicReconnectionSequence();
            logger.InfoFormat(string.Format("SRM LEIDO : {0}", srmRead));

            //BLOQUEO TECLADO DISPLAY
            var keyboardLocking = RECMAX.KeyboardLocking.NO;
            Enum.TryParse<RECMAX.KeyboardLocking>(Parametrizacion.GetString("KEYBOARD_LOCK", "NO", ParamUnidad.SinUnidad), out keyboardLocking);
            Rec.WriteKeyboardLocking(keyboardLocking);
            Delay(30, "Escribiendo bloqueo teclado");
            Resultado.Set("BLOQUEO_TECLADO", keyboardLocking.ToString(), ParamUnidad.SinUnidad);
            var lockKeyboardRead = Rec.ReadKeyboardLocking();
            logger.InfoFormat(string.Format("BLOQUEO TECLADO LEIDO : {0}", lockKeyboardRead.ToString()));

            //BLOQUEO MENU OPCIONES
            var OptionsLocking = RECMAX.MenuOptions.NO;
            Enum.TryParse<RECMAX.MenuOptions>(Parametrizacion.GetString("OPTIONS_SETUP_LOCK", "NO", ParamUnidad.SinUnidad), out OptionsLocking);
            Rec.WriteMenuOptionsLocking(OptionsLocking);
            Delay(30, "Escribiendo bloqueo menús");
            Resultado.Set("BLOQUEO_MENU_OPCIONES", OptionsLocking.ToString(), ParamUnidad.SinUnidad);
            var oprionsLockRead = Rec.ReadMenuOptionsLocking();
            logger.InfoFormat(string.Format("BLOQUEO OPCIONES LEIDO : {0}", lockKeyboardRead.ToString()));

            WriteSetups();            

            Rec.WriteCodeParametrization((ushort)Identificacion.CRC_FIRMWARE);
            Delay(30, "Escribiendo CRC");
            var crcRead = Rec.ReadParametrizationCode();
            logger.InfoFormat(string.Format("CRC LEIDO : {0}", crcRead));
            Rec.WritePageErasing(RECMAX.PagesErasing.SETUP);
            Delay(2000, "Borrando paginas de parametrización");
        }

        protected void WriteSetups()
        {
            List<RECMAX.Settings> settings = new List<RECMAX.Settings>();

            var activationExternInput = RECMAX.ActivationTypeExternalInput._2TERMINALS;
            Enum.TryParse<RECMAX.ActivationTypeExternalInput>(Parametrizacion.GetString("ACTIVATION_TYPE_EXTERNAL_INPUT", "_2TERMINALS", ParamUnidad.SinUnidad), out activationExternInput);
            Resultado.Set("TIPO_ACTIVACION_ENTRADA_EXTERNA", activationExternInput.ToString(), ParamUnidad.SinUnidad);

            var activationModeExternInput = RECMAX.ActivationModeExternalInput.LEVEL;
            Enum.TryParse<RECMAX.ActivationModeExternalInput>(Parametrizacion.GetString("ACTIVATION_MODE_EXTERNAL_INPUT", "LEVEL", ParamUnidad.SinUnidad), out activationModeExternInput);
            Resultado.Set("MODO_ACTIVACION_ENTRADA_EXTERNA", activationModeExternInput.ToString(), ParamUnidad.SinUnidad);

            var infraTension = RECMAX.ValuesYesNo.NO;
            Enum.TryParse<RECMAX.ValuesYesNo>(Parametrizacion.GetString("SUPERVISION_INFRATENSION", "NO", ParamUnidad.SinUnidad), out infraTension);
            Resultado.Set("SUPERVISION_INFRATENSION", infraTension.ToString(), ParamUnidad.SinUnidad);

            var sobreTension = RECMAX.ValuesYesNo.NO;
            Enum.TryParse<RECMAX.ValuesYesNo>(Parametrizacion.GetString("SUPERVISION_SOBRETENSION", "NO", ParamUnidad.SinUnidad), out sobreTension);
            Resultado.Set("SUPERVISION_SOBRETENSION", sobreTension.ToString(), ParamUnidad.SinUnidad);

            var reconectionFixed = RECMAX.ValuesYesNo.NO;
            Enum.TryParse<RECMAX.ValuesYesNo>(Parametrizacion.GetString("SEQUENCE_RECONECTION_FIXED", "NO", ParamUnidad.SinUnidad), out reconectionFixed);
            Resultado.Set("SECUENCIA_RECONEXION_FIJA", reconectionFixed.ToString(), ParamUnidad.SinUnidad);

            settings.Add(activationExternInput == RECMAX.ActivationTypeExternalInput._2TERMINALS ? RECMAX.Settings.AMOUNT_TERMINALS : RECMAX.Settings.NO_SETTINGS);
            settings.Add(activationModeExternInput == RECMAX.ActivationModeExternalInput.PULSE ? RECMAX.Settings.EXTERNAL_TRIGGER : RECMAX.Settings.NO_SETTINGS);
            settings.Add(infraTension == RECMAX.ValuesYesNo.SI ? RECMAX.Settings.UNDERVOLTAGE_MONITORING : RECMAX.Settings.NO_SETTINGS);
            settings.Add(sobreTension == RECMAX.ValuesYesNo.SI ? RECMAX.Settings.OVERVOLTAGE_MONITORING : RECMAX.Settings.NO_SETTINGS);
            settings.Add(reconectionFixed == RECMAX.ValuesYesNo.SI ? RECMAX.Settings.RECONNECTION_SEQUENCE_FIXED : RECMAX.Settings.NO_SETTINGS);

            Rec.WriteSettings(settings.ToArray());
            Delay(30, "Escribiendo Ajustes1");
            var settingsRead = Rec.ReadSettings();
            logger.InfoFormat(string.Format("AJUSTES1 LEIDO : {0}", settingsRead));
        }

        protected void WriteCostumerTableCurrentTime()
        {
            RECMAX.ScaleTableConfiguration currents, times, SRDC, SRMC;

            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000", ParamUnidad.SinUnidad);          
            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;100;200;300;400;500;800;1000", ParamUnidad.SinUnidad);           
            var SRDCustomScaleElements = Parametrizacion.GetString("TABLA_SRD_CUSTOM", "NO", ParamUnidad.SinUnidad);             
            var SRMCustomScaleElements = Parametrizacion.GetString("TABLA_SRM_CUSTOM", "NO", ParamUnidad.SinUnidad);

            if (currentScaleElements.Trim() != "NO")
            {
                currents = GetCurrentScaleTable(currentScaleElements);
                Rec.WriteScaleTable(currents, RECMAX.TableKind.CURRENT);
            }
            if (timeTableElements.Trim() != "NO")
            {
                times = GetTimeScaleTable(timeTableElements);
                Rec.WriteScaleTable(times, RECMAX.TableKind.TIME);
            }

            if (SRDCustomScaleElements.Trim() != "NO")
            {
                SRDC = GetCurrentScaleTable(SRDCustomScaleElements);
                Rec.WriteScaleTable(SRDC, RECMAX.TableKind.SRDC);
            }

            if (SRMCustomScaleElements != "NO")
            {
                SRMC = GetCurrentScaleTable(SRMCustomScaleElements);
                Rec.WriteScaleTable(SRMC, RECMAX.TableKind.SRMC);
            }

            //LECTURA DE TABLAS

            string resultCurrents, resultTime, resultSRDC, resultSRMC;

            if (currentScaleElements.Trim() != "NO")
            {
                resultCurrents = Rec.ReadScaleTable(RECMAX.TableKind.CURRENT);

                Resultado.Set("TABLA_CORRIENTES", resultCurrents, ParamUnidad.SinUnidad, currents.listElementsString.Trim());

                if (resultCurrents.Trim() != currents.listElementsString.Trim())
                    Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("TABLA DE CORRIENTES LEIDA NO ES IGUAL A LA DE BBDD");

                var current = Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 30, ParamUnidad.mA);

                var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)current);

                if (posCurrent.Count() == 0)
                    throw new Exception("Error parametrización incorrecta, el valor del parametro CURRENT_TRIGGER no existe en la tabla de corrientes");

                Resultado.Set("POSICION_I_DISPARO_DEFECTO", posCurrent.FirstOrDefault().Position.ToString(), ParamUnidad.SinUnidad);

                Rec.WriteTriggerCurrent(posCurrent.FirstOrDefault().Position);
                Delay(30, "Escribirendo Posicion de la tabla de Corriente disparo");
            }

            if (timeTableElements.Trim() != "NO")
            {
                resultTime = Rec.ReadScaleTable(RECMAX.TableKind.TIME);

                Resultado.Set("TABLA_TIEMPOS", resultTime, ParamUnidad.SinUnidad, times.listElementsString.Trim());

                if (resultTime.Trim() != times.listElementsString.Trim())
                    Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("TABLA DE TIEMPOS DE RETARDO LEIDA NO ES IGUAL A LA DE BBDD");

                var tiempo = Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 100, ParamUnidad.ms);

                var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)tiempo);

                if (posTime.Count() == 0)
                    throw new Exception("Error parametrización incorrecta, la T_RETARDO_DEFECTO no existe en la tabla de tiempos");

                Resultado.Set("POSICION_T_RETARDO_DEFECTO", posTime.FirstOrDefault().Position, ParamUnidad.SinUnidad);

                Rec.WriteTriggerTime(posTime.FirstOrDefault().Position);
                Delay(30, "Escribirendo Posicion de la tabla de Tiempo de retardo de disparo");
            }

            if (SRDCustomScaleElements.Trim() != "NO")
            {
                resultSRDC = Rec.ReadScaleTable(RECMAX.TableKind.SRDC);

                Resultado.Set("TABLA_SRDC", resultSRDC, ParamUnidad.SinUnidad, SRDC.listElementsString.Trim());

                if (resultSRDC.Trim() != SRDC.listElementsString.Trim())
                    Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("TABLA SRDC LEIDA NO ES IGUAL A LA DE BBDD");
            }


            if (SRMCustomScaleElements.Trim() != "NO")
            {
                resultSRMC = Rec.ReadScaleTable(RECMAX.TableKind.SRMC);

                Resultado.Set("TABLA_SRMC", resultSRMC, ParamUnidad.SinUnidad, SRMC.listElementsString.Trim());

                if (resultSRMC.Trim() != SRMC.listElementsString.Trim())
                    Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("TABLA SRMC LEIDA NO ES IGUAL A LA DE BBDD");
            }
        }

        protected RECMAX.ScaleTableConfiguration GetCurrentScaleTable(string currentScaleElements)
        {
            var scaleElementsList = currentScaleElements.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return new RECMAX.ScaleTableConfiguration(scaleElementsList, "corriente");
        }

        protected RECMAX.ScaleTableConfiguration GetTimeScaleTable(string timeScaleElements)
        {
            var scaleElementsArray = timeScaleElements.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            for (int i = 0; i < scaleElementsArray.Count(); i++)
            {
                if (scaleElementsArray[i].Trim().ToUpper() == "INS")
                    scaleElementsArray[i] = "0";

                if (scaleElementsArray[i].Trim().ToUpper() == "SEL")
                    scaleElementsArray[i] = "1";
            };

            return new RECMAX.ScaleTableConfiguration(scaleElementsArray, "tiempo");
        }

        private string FilterBarcode(string barcode)
        {
            var resultBarcode = string.Empty;

            var charBarcode = barcode.ToList();
            var charBarcodeBBDD = Identificacion.MODELO_MAGNETOTERMICO.ToList();

            for (int i = 0; i < charBarcode.Count; i++)
                if (charBarcodeBBDD[i].ToString().ToUpper() == "X")
                    charBarcode[i] = 'X';

            foreach (var caracter in charBarcode)
                resultBarcode += caracter;

            return resultBarcode;
        }


        [DelimitedRecord(";")]
        private class CVMC10_File_Alarms
        {
            public string param_code = "";
            public string hi = "";
            public string lo = "";
            public string delayon = "";
            public string delayoff = "";
            public string hyst = "";
            public string latch = "";
            public string state = "";

            public string PARAM_CODE
            {
                get { return param_code.Replace("PARAM_CODE=", "").Trim(); }
            }
            public string HI
            {
                get { return hi.Replace("HI=", "").Trim(); }
            }
            public string LO
            {
                get { return lo.Replace("LO=", "").Trim(); }
            }
            public string DELAYON
            {
                get { return delayon.Replace("DELAYON=", "").Trim(); }
            }
            public string DELAYOFF
            {
                get { return delayoff.Replace("DELAYOFF=", "").Trim(); }
            }
            public string HYST
            {
                get { return hyst.Replace("HYST=", "").Trim(); }
            }
            public string LATCH
            {
                get { return latch.Replace("LATCH=", "").Trim(); }
            }
            public string STATE
            {
                get { return state.Replace("STATE=", "").Trim() == "NA" ? "0" : "1"; }
            }
        }
    }
}
