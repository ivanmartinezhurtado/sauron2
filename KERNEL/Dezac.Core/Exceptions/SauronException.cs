﻿using System;

namespace Dezac.Core.Exceptions
{
    public class SauronException : Exception
    {  
        public string CodeError { get; set; }

        public SauronException(string message, string codError)
            : base(message)
        {
            this.CodeError = codError;
        }

        public void Throw()
        {
            throw this;
        }
    }
}
