namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_VALORPARAMETROPRODUCTO
    {
        public T_VALORPARAMETROPRODUCTO()
        {
            this.T_VALORPARAMETROTOLPROD1 = new HashSet<T_VALORPARAMETROTOLPROD>();
            this.T_VALORPARAMETROTOLPROD2 = new HashSet<T_VALORPARAMETROTOLPROD>();
        }
    
        public int NUMGRUPO { get; set; }
        public int NUMPARAM { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public string VALOR { get; set; }
        public string VALORINICIO { get; set; }
        public Nullable<System.DateTime> FECHAALTA { get; set; }
        public string IDCATEGORIA { get; set; }
    
        public virtual PRODUCTO PRODUCTO { get; set; }
        public virtual T_VALORPARAMETRO T_VALORPARAMETRO { get; set; }
        public virtual T_VALORPARAMETROTOLPROD T_VALORPARAMETROTOLPROD { get; set; }
        public virtual ICollection<T_VALORPARAMETROTOLPROD> T_VALORPARAMETROTOLPROD1 { get; set; }
        public virtual ICollection<T_VALORPARAMETROTOLPROD> T_VALORPARAMETROTOLPROD2 { get; set; }
    }
}
