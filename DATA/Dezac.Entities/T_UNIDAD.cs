namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_UNIDAD
    {
        public T_UNIDAD()
        {
            this.T_PARAMETRO = new HashSet<T_PARAMETRO>();
            this.T_TESTVALOR = new HashSet<T_TESTVALOR>();
        }
    
        public int NUMUNIDAD { get; set; }
        public string UNIDAD { get; set; }
    
        public virtual ICollection<T_PARAMETRO> T_PARAMETRO { get; set; }
        public virtual ICollection<T_TESTVALOR> T_TESTVALOR { get; set; }
    }
}
