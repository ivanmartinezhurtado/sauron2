﻿using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Measure;

namespace Dezac.Tests.Extensions
{
    public static class TestOptrisExtension
    {
        public static void TestOptris(this TestBase testBase, byte optrisPort, string resultName, int retries)
        {
            var defs = new AdjustValueDef(testBase.Params.TEMP.Other(resultName).Verificacion.Name, 0, testBase.Params.TEMP.Other(resultName).Verificacion.Min(), testBase.Params.TEMP.Other(resultName).Verificacion.Max(), 0, 0, ParamUnidad.Grados);

            using (OPTRIS_CS optris = new OPTRIS_CS(optrisPort))
            {
                optris.Initialization();

                testBase.TestMeasureBase(defs,
                (step) =>
                {
                    return  optris.ReadTemperature();
                }, 0, retries, 1000);
            }
        }          
    }
}
