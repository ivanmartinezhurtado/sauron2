﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(FtpClientDownloadFileDescription))]
    public class FtpClientDownloadFile : ActionBase
    {
        [Description("Dirección IP del cliente (Desactivado si se usa el action GetLastDeviceIP)")]
        [Category("Configuracion")]
        public ActionVariable IpClient { get; set; } = new ActionVariable();

        [Description("Nombre de usuario")]
        [Category("Configuracion")]
        public ActionVariable User { get; set; } = new ActionVariable();

        [Description("Contraseña")]
        [Category("Configuracion")]
        public ActionVariable Password { get; set; } = new ActionVariable();

        [Category("File Info")]
        [Description("Ruta completa del fichero en local. Lugar donde se descargará.")]
        public ActionVariable LocalFile { get; set; } = new ActionVariable();

        [Category("File Info")]
        [Description("Ruta completa del fichero en el servidor")]
        public ActionVariable RemoteFile { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            FTPClient ftpClient = null;

            if (IpClient.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("IpClient").Throw();

            testBase.SamplerWithCancel((p) =>
            {
                ftpClient = new FTPClient(IpClient.Value.ToString(), User.Value.ToString(), Password.Value.ToString());
                return true;
            }, string.Format("Error, no se ha podido realizar la conexión por FTP a la direccion {0}", IpClient.Value.ToString()), 5, 5000, 100, false, false);       

            ftpClient.DownloadFile(LocalFile.Value.ToString(), RemoteFile.Value.ToString());
            ftpClient.Disconnect();
        }

        public class FtpClientDownloadFileDescription : ActionDescription
        {
            public override string Name { get { return "FTPClientDownloadFile"; } }
            public override string Category { get { return "Tools/Ethernet/FTP"; } }
            public override string Description { get { return "Action que sirve para descargar ficheros con el cliente FTP"; } }
            public override Image Icon { get { return Properties.Resources.ftp_icon; } }

        }
    }
}
