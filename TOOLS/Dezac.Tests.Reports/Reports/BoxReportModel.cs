﻿namespace Dezac.Tests.Reports.Reports
{
    public class BoxReportModel
    {
        public int NUMORDEN { get; set; }
        public string DESCRIPCION { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public decimal CANTIDAD { get; set; }

        public int UnidadCaja { get; set; }

        public int ORDEN { get; set; }
        public int NUMMATRICULA { get; set; }
        public string NROSERIE { get; set; }
        public int NUMCAJA { get; set; }
    }

    public class CajasOrdenModel
    {
        public int Numcaja { get; set; }
        public string Cajaorden { get; set; }
    }

    public class EquiposCajaModel
    {
        public int NUMCAJA { get; set; }
        public int NUMEQUIPO { get; set; }
        public string NROSERIE{ get; set; }
        public string SITUACION { get; set; }
        public string NROSERIEORIGINAL { get; set; }
    }
}
