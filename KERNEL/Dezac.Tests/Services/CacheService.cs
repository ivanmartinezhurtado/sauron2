﻿using Dezac.Tests.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Dezac.Tests.Services
{
    public class CacheService : ICacheService, IDisposable
    {
        private Dictionary<string, CacheItem> internalCache = new Dictionary<string, CacheItem>();
        private object syncRoot = new object();

        public T Add<T>(string key, T objectToCache) 
        {
            return Add(key, () => objectToCache, TimeSpan.MaxValue);
        }

        public T Add<T>(string key, Func<T> loadFunc, TimeSpan cacheFor) 
        {
            lock (syncRoot)
            {
                CacheItem item;
                if (this.internalCache.TryGetValue(key, out item))
                    if (item.IsExpired)
                    {
                        Remove(key);
                        item = null;
                    }

                if (item == null)
                {
                    item = new CacheItem(key, cacheFor);
                    item.Data = loadFunc();
                    this.internalCache.Add(key, item);
                }
                return (T) item.Data;
            }
        }

        public T AddOrSet<T>(string key, T objectToCache)
        {
            return AddOrSet(key, () => objectToCache, TimeSpan.MaxValue);
        }

        public T AddOrSet<T>(string key, Func<T> loadFunc, TimeSpan cacheFor)
        {
            lock (syncRoot)
            {
                CacheItem item;
                if (this.internalCache.TryGetValue(key, out item))
                    if (item.IsExpired)
                    {
                        Remove(key);
                        item = null;
                    }

                if (item == null)
                {
                    item = new CacheItem(key, cacheFor);
                    this.internalCache.Add(key, item);
                }
                item.Data = loadFunc();

                return (T)item.Data;
            }
        }

        public object Get(string key)
        {
            CacheItem item;
            if (this.internalCache.TryGetValue(key, out item))
                if (item.IsExpired)
                {
                    Remove(key);
                    item = null;
                }

            if (item != null)
                return item.Data;

            return null;
        }

        public T Get<T>(string key)
        {
            CacheItem item;
            if (this.internalCache.TryGetValue(key, out item))
                if (item.IsExpired)
                {
                    Remove(key);
                    item = null;
                }

            if (item != null)
                return (T)item.Data;

            return default(T);
        }

        public void Remove(string key)
        {
            lock (syncRoot)
            {
                CacheItem item;
                if (this.internalCache.TryGetValue(key, out item))
                {
                    this.internalCache.Remove(key);
                    var disp = item.Data as IDisposable;
                    if (disp != null)
                        disp.Dispose();
                }
            }
        }

        public void Clear()
        {
            foreach (var item in this.internalCache.ToList())
                Remove(item.Key);
        }

        public bool HasValidKey(string key)
        {
            CacheItem item;
            if (this.internalCache.TryGetValue(key, out item))
            {
                if (item.IsExpired)
                {
                    Remove(key);
                    return false;
                }
                return true;
            }

            return false;
        }

        public bool TryGet<T>(string key, out T cachedObject)
        {
            bool result = false;
            CacheItem item;
            if (this.internalCache.TryGetValue(key, out item))
            {
                if (item.IsExpired)
                {
                    Remove(key);
                    item = null;
                }
                else
                    result = true;
            }

            cachedObject = result ? (T)item.Data : default(T);

            return result;
        }

        public bool TryAdd<T>(string key, Expression<Func<T>> cacheItemProvider, out T cachedObject) 
        {
            bool result = TryGet(key, out cachedObject);

            if (!result)
                cachedObject = Add<T>(key, cacheItemProvider.Compile()());

            return result;
        }

        public void Dispose()
        {
            Clear();
        }
    }

    internal class CacheItem
    {
        internal string Key { get; set; }
        internal DateTime Expires { get; set; }
        internal object Data { get; set; }

        internal CacheItem(string key, TimeSpan lifeTime)
        {
            this.Key = key;
            this.Expires = lifeTime == TimeSpan.MaxValue ? DateTime.MaxValue : DateTime.Now.Add(lifeTime);
        }

        internal bool IsExpired
        {
            get
            {
                return this.Expires < DateTime.Now;
            }
        }
    }

}
