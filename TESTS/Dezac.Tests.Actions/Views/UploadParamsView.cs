﻿using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WinTaskRunner.Design;

namespace Dezac.Tests.Actions.Views
{
    public partial class UploadParamsView : Form
    {
        public int? IdFamilia { get; set; }
        public string Familia { get; set; }
        public int? NumProducto { get; set; }
        public int? Version { get; set; }
        public string Descripcion { get; set; }
        public string IdFase { get; set; }
        public string Fase { get; set; }
        public static string IdUser { get; set; }

        public UploadParamsView()
        {
            InitializeComponent();
        }

        public UploadParamsView(int? NumFamilia, int? idProducto, int? version, string idFase)
        {
            InitializeComponent();

            IdFamilia = NumFamilia;
            NumProducto = idProducto;
            Version = version;
            IdFase = idFase;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            lblParam.Visible = true;

            using (var svc = new DezacService())
            {
                var tipoGrupo = svc.GetTipoGrupoParametrizacion();
                if (tipoGrupo == null)
                    throw new Exception("error al leer los tipos grupos de la BBDD");

                cmbTipoGrupo.Sorted = true;
                cmbTipoGrupo.DataSource = tipoGrupo;
                cmbTipoGrupo.DisplayMember = "TIPO";
                cmbTipoGrupo.ValueMember = "NUMTIPOGRUPO";

                var tipoParam = svc.GetTipoParametros();
                if (tipoParam == null)
                    throw new Exception("error al leer los tipo parametros de la BBDD");

                cmbTipoParametro.Sorted = true;
                cmbTipoParametro.DataSource = tipoParam;
                cmbTipoParametro.DisplayMember = "TIPOPARAM";
                cmbTipoParametro.ValueMember = "NUMTIPOPARAM";

                var unidades = svc.GetUnidades();
                if (unidades == null)
                    throw new Exception("error al leer las unidades de la BBDD");

                cmbParamUnidad.Sorted = true;
                cmbParamUnidad.DataSource = unidades;
                cmbParamUnidad.DisplayMember = "UNIDAD";
                cmbParamUnidad.ValueMember = "NUMUNIDAD";

                var tipoValor = svc.GetTipoValorParametro();
                if (tipoValor == null)
                    throw new Exception("error al leer las tipo de valores de la BBDD");

                cmbTipoValor.Sorted = true;
                cmbTipoValor.DataSource = tipoValor;
                cmbTipoValor.DisplayMember = "TIPOVALOR";
                cmbTipoValor.ValueMember = "NUMTIPOVALOR";
            }

            UpdateUI();
        }

        private void UpdateUI()
        {
            lblUsuario.Text = IdUser;
            lblParamExist.Visible = rdbParamExistente.Checked;
            lblParam.Visible = !rdbParamExistente.Checked;
            btnParametros.Visible = rdbParamExistente.Checked;

            txtNombre.Enabled = rdbParamNuevo.Checked;
            lblTipoParam.Enabled = !rdbParamExistente.Checked;
            lblUnidad.Enabled = !rdbParamExistente.Checked;
            cmbParamUnidad.Enabled = !rdbParamExistente.Checked;
            cmbTipoParametro.Enabled = !rdbParamExistente.Checked;
            cmbTipoValor.Enabled = !rdbParamExistente.Checked;
        }

        public bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.OK)
                return Upload();

            return true;
        }

        private bool ValidateForm()
        {
            string msg = string.Empty;

            if (!IdFamilia.HasValue || IdFamilia <= 0)
                msg += "Falta la familia\n";

            if (NumProducto.GetValueOrDefault() <= 0)
                msg += "Falta el producto\n";

            if (Version.GetValueOrDefault() <= 0)
                msg += "Falta la versión\n";

            if (string.IsNullOrEmpty(IdFase))
                msg += "Falta la fase\n";

            if (string.IsNullOrEmpty(IdUser))
                msg += "Falta el usuario\n";

            if (msg != string.Empty)
                MessageBox.Show(msg, "Atención");

            return msg == string.Empty;
        }

        // TODO: No permetre vincular nomes producte
        // TODO: Resolució sequencia, si hi ha una nova no anar al sistema antic
        private bool Upload()
        {
            if (!ValidateForm())
                return false;
                
            return true;
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            var ctrl = new LoginView();

            if (Shell.ShowDialog("IDENTIFICACIÓN", ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;

            IdUser = ctrl.Empleado.IDEMPLEADO;
            UpdateUI();
        }
  
        private IShell Shell
        {
            get { return ServiceContainer.Default.Resolve<IShell>(); }
        }

        private void rdbParamExistente_CheckedChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void rdbParamNuevo_CheckedChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
                return;

            var name = txtNombre.Text;
            var valor = txtValor.Text;
            var tipoParam = (cmbTipoParametro.SelectedItem as T_TIPOPARAMETRO).NUMTIPOPARAM;
            var unidad = (cmbParamUnidad.SelectedItem as T_UNIDAD).NUMUNIDAD;
            var tipoValor = (cmbTipoValor.SelectedItem as T_TIPOVALORPARAMETRO).NUMTIPOVALOR;
            var TipoGrupo = (cmbTipoGrupo.SelectedItem as T_TIPOGRUPOPARAMETRIZACION);
            var alias = TipoGrupo.TIPO.Substring(0, 4);

            using (DezacService svc = new DezacService())
            {
               var IdParam = svc.CrearParametro(IdFamilia.Value, IdFase, TipoGrupo.NUMTIPOGRUPO, name, unidad, tipoValor, tipoParam, alias , valor);
            }

            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnParametros_Click(object sender, EventArgs e)
        {
            List<T_PARAMETRO> parametros = null;

            using (DezacService svc = new DezacService())
            {
                parametros = svc.GetParametros();
            }

            var item = Shell.ShowTableDialog<T_PARAMETRO>("PARAMETROS", new TableGridViewConfig { Data = parametros, Fields = "PARAM;UNIDAD;TIPOPARAM" });
            if (item == null)
                return;

            cmbParamUnidad.SelectedIndex = cmbParamUnidad.FindStringExact(item.T_UNIDAD.UNIDAD);
            cmbTipoParametro.SelectedIndex = cmbTipoParametro.FindStringExact(item.T_TIPOPARAMETRO.TIPOPARAM);
            cmbTipoValor.SelectedIndex = cmbTipoValor.FindStringExact(item.T_TIPOVALORPARAMETRO.TIPOVALOR);
            txtNombre.Text = item.PARAM;
        }
    }
}
