namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTFASEFILE")]
    public partial class T_TESTFASEFILE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTFF { get; set; }

        public int NUMTESTFASE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTIPODOC { get; set; }

        public byte[] FICHERO { get; set; }

        [StringLength(50)]
        public string NOMBRE { get; set; }

        public virtual T_TESTFASE T_TESTFASE { get; set; }
    }
}
