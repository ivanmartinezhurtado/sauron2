﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner.Model
{
    public class CustomContractResolverFrom0To1 : DefaultContractResolver
    {
        public static readonly CustomContractResolverFrom0To1 Instance = new CustomContractResolverFrom0To1();
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSampler")
            {
                if (property.PropertyName.Equals("InitDelayTime", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TiempoEsperaInicial";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSampler")
            {
                if (property.PropertyName.Equals("SampleNumber", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumeroMuestras";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSampler")
            {
                if (property.PropertyName.Equals("SampleMaxNumber", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumeroMaximoMuestras";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSampler")
            {
                if (property.PropertyName.Equals("DelayBetweenSamples", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TiempoEntreMuestras";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSamplerWithCancel")
            {
                if (property.PropertyName.Equals("InitDelayTime", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TiempoEsperaInicial";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSamplerWithCancel")
            {
                if (property.PropertyName.Equals("IterationNumber", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumeroIteraciones";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSamplerWithCancel")
            {
                if (property.PropertyName.Equals("DelayBetweenIteration", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TiempoEntreIteraciones";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSamplerWithCancel")
            {
                if (property.PropertyName.Equals("CancelIfError", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "CancelarSiHayError";
            }

            if (property.DeclaringType.FullName == "Dezac.Core.Utility.ConfigurationSamplerWithCancel")
            {
                if (property.PropertyName.Equals("ThrowException", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "LanzarExcepción";
            }

            //Cambios en Dezac.Test.Actions.Actions

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.RazorTemplateAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CalculateGainOffsetOfLineAction")
            {
                if (property.PropertyName.Equals("AddToResults", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "AddResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CalculateGainOffsetOfLineAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "variableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ChekSumAction")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableToCalculate";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ChekSumAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableResultado";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ComputeExpresion")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Get_GUID")
            {
                if (property.PropertyName.Equals("AddToResults", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "AddResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Get_Random")
            {
                if (property.PropertyName.Equals("DataInputMin", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Minino";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Get_Random")
            {
                if (property.PropertyName.Equals("DataInputMax", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Maximo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Get_Random")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Get_Random")
            {
                if (property.PropertyName.Equals("AddToResults", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "AddResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperationMathTwoOperator")
            {
                if (property.PropertyName.Equals("DataInput1", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Operador1";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperationMathTwoOperator")
            {
                if (property.PropertyName.Equals("DataInput2", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Operador2";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperationMathTwoOperator")
            {
                if (property.PropertyName.Equals("Operation", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Operacion";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperationMathTwoOperator")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VaroiableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperatorDateTime")
            {
                if (property.PropertyName.Equals("DataInput1", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "DateTime1";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperatorDateTime")
            {
                if (property.PropertyName.Equals("DataInput2", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "DateTime2";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.OperatorDateTime")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VaroiableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ShowDialogAction")
            {
                if (property.PropertyName.Equals("Message", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Mensaje";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ShowDialogAction")
            {
                if (property.PropertyName.Equals("Title", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Titulo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InputTextAction")
            {
                if (property.PropertyName.Equals("Message", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Mensaje";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InputTextAction")
            {
                if (property.PropertyName.Equals("Title", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Titulo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InputTextAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ImageViewAction")
            {
                if (property.PropertyName.Equals("DialogResult", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Result";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ImageViewAction")
            {
                if (property.PropertyName.Equals("DataInputName", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ImageViewAction")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ValueVariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ImageViewAction")
            {
                if (property.PropertyName.Equals("DataInputMax", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ValueMaxVariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ImageViewAction")
            {
                if (property.PropertyName.Equals("DataInputMin", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ValueMinVariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ImageViewAction")
            {
                if (property.PropertyName.Equals("AddToResults", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "AddResults";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.DeviceMeasure")
            {
                if (property.PropertyName.Equals("AddToResults", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "AddToResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableDateTime")
            {
                if (property.PropertyName.Equals("Instance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Instancia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableDateTime")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Variables";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableList")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableList")
            {
                if (property.PropertyName.Equals("DataInputList", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ListValue";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableList")
            {
                if (property.PropertyName.Equals("Instance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Instancia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableListFromStructure")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Structure";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariableListFromStructure")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesAction")
            {
                if (property.PropertyName.Equals("Instance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Instancia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Variables";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesAction")
            {
                if (property.PropertyName.Equals("SharedDataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "SharedVariables";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesFromCSV")
            {
                if (property.PropertyName.Equals("Instance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Instancia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesFromCSV")
            {
                if (property.PropertyName.Equals("DataType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoVariable";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesFromCSV")
            {
                if (property.PropertyName.Equals("PathDataMapFile", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "PathVariablesMapFile";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddVariablesFromCSV")
            {
                if (property.PropertyName.Equals("DataMapFile", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariablesMapFile";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AsignedVariablesAction")
            {
                if (property.PropertyName.Equals("Instance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Instancia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AsignedVariablesAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Variables";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.VariableAssigned")
            {
                if (property.PropertyName.Equals("GetData", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "GetVariable";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.VariableAssigned")
            {
                if (property.PropertyName.Equals("SetData", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "SetVariable";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.MoveVariable")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HexadecimalStringToInteger")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HexadecimalStringToInteger")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.IntegerToHexadecimalString")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.IntegerToHexadecimalString")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.VariableDeserializeJson")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.VariableDeserializeJson")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.StringFormat")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.StringFormat")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.StringFormat")
            {
                if (property.PropertyName.Equals("Prefix", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Prefijo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.StringFormat")
            {
                if (property.PropertyName.Equals("Sufix", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Sufijo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.StringFormat")
            {
                if (property.PropertyName.Equals("PositionInsert", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "PosicionInsert";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SearchStringInVariable")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SearchStringInVariable")
            {
                if (property.PropertyName.Equals("CharactersQuantity", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "CantidadCaracteres";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SearchStringInVariable")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableNameResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SearchStringInVariable")
            {
                if (property.PropertyName.Equals("TextToSearchStartWith", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TextToSearchStarWith";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetValueVariableNameSearch")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetValueVariableNameSearch")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableNameResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CountVariableName")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CountVariableName")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableNameResult";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateStrings")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateStrings")
            {
                if (property.PropertyName.Equals("Unit", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "unidad";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresMaxMin")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresMaxMin")
            {
                if (property.PropertyName.Equals("Unit", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "unidad";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresMaxMin")
            {
                if (property.PropertyName.Equals("Maximum", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Maximo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresMaxMin")
            {
                if (property.PropertyName.Equals("Minimum", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Minimo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresAvgTol")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresAvgTol")
            {
                if (property.PropertyName.Equals("Unit", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "unidad";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresAvgTol")
            {
                if (property.PropertyName.Equals("Average", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Averge";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateMeasuresAvgTol")
            {
                if (property.PropertyName.Equals("Tolerance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Tolerancia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateDateTimeMaxMin")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateDateTimeMaxMin")
            {
                if (property.PropertyName.Equals("Unit", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "unidad";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateDateTimeMaxMin")
            {
                if (property.PropertyName.Equals("Maximum", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Maximo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateDateTimeMaxMin")
            {
                if (property.PropertyName.Equals("Minimum", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Minimo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateDateTime")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.XmlSearchNode")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.XmlSearchNode")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.XmlSearchNode")
            {
                if (property.PropertyName.Equals("DataOutputXml", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarNameXmlNode";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.XmlFilterNode")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.XmlFilterNode")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CommentAction")
            {
                if (property.PropertyName.Equals("CommentsAction", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Notas";
            }


            //Cambios en Dezac.Test.Actions.DezacTest

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.PrintLabelsFaseAction")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "LabelFileName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetTestPointByMeterType")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetTestPointByMeterType")
            {
                if (property.PropertyName.Equals("PowerType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoPotencia";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetTestPointByMeterType")
            {
                if (property.PropertyName.Equals("TestPointName", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "testPointName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetMeterType")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetMeterType")
            {
                if (property.PropertyName.Equals("Model", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Modelo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetBinaryDataBase")
            {
                if (property.PropertyName.Equals("DocumentType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoDoc";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetMAC")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetMAC")
            {
                if (property.PropertyName.Equals("IDOperator", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "IdOperario";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetMAC")
            {
                if (property.PropertyName.Equals("FabricationOrder", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OrdenFabricacion";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SaveFileTestFase")
            {
                if (property.PropertyName.Equals("FileName", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NombreFichero";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SaveFileTestFase")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NombreVariableFichero";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SaveFileTestFase")
            {
                if (property.PropertyName.Equals("FileType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoFichero";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("Traceability", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Trazabilidad";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("AddSubsetsWithoutRegistering", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "AddSubconjuntosSinRegistrar";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("FrameAutoReader", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "BastidorAutoReader";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("SerialNumber", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "serialNumber";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("BarCodeType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "barCodeType";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitBastidorMultipleAction")
            {
                if (property.PropertyName.Equals("LedOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ledOutput";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ControlTrazabilidad")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.AddFrameToSubset")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumProducto";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.PackageCheck")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ParameterName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidacionImpresion")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidacionImpresion")
            {
                if (property.PropertyName.Equals("DataInputNames", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VarNames";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HalconCaptureMultiImageAsync")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HalconCaptureAsync")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ImageSaveName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HalconOCR")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ImageSaveName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HalconOCR")
            {
                if (property.PropertyName.Equals("DataInputCamera", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HalconProcedureAsync")
            {
                if (property.PropertyName.Equals("ListTestName", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "listTestName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CameraIDSModifyParameters")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.PrintLabelOffline")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Variables";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.DetectionDrive")
            {
                if (property.PropertyName.Equals("DriveType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "driveType";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.DetectionDrive")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.QueryScalarGenericDatabase")
            {
                if (property.PropertyName.Equals("DataInput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.QueryScalarGenericDatabase")
            {
                if (property.PropertyName.Equals("CommandSQL", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "commandSql";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.QueryReaderGenericDatabase")
            {
                if (property.PropertyName.Equals("CommandSQL", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "commandSql";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.QueryExecuteNoQueyGenericDatabase")
            {
                if (property.PropertyName.Equals("CommandSQL", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "commandSql";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.DHCPGetClients")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HttpGet")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HttpPost")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.HttpPut")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetClientsRouterDHCP")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.IPSetupDeviceNetwork")
            {
                if (property.PropertyName.Equals("ServerIP", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Server_IP";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.IPSetupDeviceNetwork")
            {
                if (property.PropertyName.Equals("DeviceMAC", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Device_MAC";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.IPSetupDeviceNetwork")
            {
                if (property.PropertyName.Equals("DeviceIP", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Device_IP";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.IPSetupDeviceNetwork")
            {
                if (property.PropertyName.Equals("DHCPEnabled", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "DHCP_Enabled";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.GetLastDeviceIP")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SSHRunCommand")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadCanBusSerialPortAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableResultado";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadCanBusSerialPortAction")
            {
                if (property.PropertyName.Equals("Retries", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Reintentos";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.WriteAndReadHexSerialPort")
            {
                if (property.PropertyName.Equals("Retries", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Reintentos";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadHexSerialPortAction")
            {
                if (property.PropertyName.Equals("Retries", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Reintentos";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadHexSerialPortAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableResultado";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadMidabusDevieAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadMidabusDevieAction")
            {
                if (property.PropertyName.Equals("DataOutputUnits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputUnidades";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.WMISerialPortInitialize")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadSerialPortAction")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadCirbus")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadCirbus")
            {
                if (property.PropertyName.Equals("DataOutputUnits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputUnidades";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.WriteRegister")
            {
                if (property.PropertyName.Equals("RegisterType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoRegistro";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.WriteRegister")
            {
                if (property.PropertyName.Equals("SendValue", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ValorEnviar";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.WriteInt32Action")
            {
                if (property.PropertyName.Equals("RegisterType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoRegistro";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.WriteInt32Action")
            {
                if (property.PropertyName.Equals("SendValue", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ValorEnviar";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadRegister")
            {
                if (property.PropertyName.Equals("RegisterType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoRegistro";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadRegister")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadRegister")
            {
                if (property.PropertyName.Equals("DataOutputUnits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputUnidades";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadMultipleRegister")
            {
                if (property.PropertyName.Equals("NumRegister", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumeroRegistros";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadMultipleRegister")
            {
                if (property.PropertyName.Equals("RegisterType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoRegistro";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadMultipleRegister")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadMultipleRegister")
            {
                if (property.PropertyName.Equals("DataOutputUnits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputUnidades";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadInt32Action")
            {
                if (property.PropertyName.Equals("RegisterType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoRegistro";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadInt32Action")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadInt32Action")
            {
                if (property.PropertyName.Equals("DataOutputUnits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputUnidades";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitModbusDeviceAction")
            {
                if (property.PropertyName.Equals("NumPeripheral", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumeroPeriferico";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitModbusDeviceAction")
            {
                if (property.PropertyName.Equals("TransportType", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoTransporte";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitModbusDeviceAction")
            {
                if (property.PropertyName.Equals("SerialPort", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "PuertoSerie";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitModbusDeviceAction")
            {
                if (property.PropertyName.Equals("Port", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Puerto";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitModbusDeviceAction")
            {
                if (property.PropertyName.Equals("ModbusTypeTCP", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TipoModbusTCP";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Update_Ls")
            {
                if (property.PropertyName.Equals("BinaryPath", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Ruta_binario";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Update_Ls")
            {
                if (property.PropertyName.Equals("BlocksToJump", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "bloques_a_saltar";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CreateDlmsSerialPort")
            {
                if (property.PropertyName.Equals("PhysicalAddress", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "physicalAddress";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.CreateDlmsSerialPort")
            {
                if (property.PropertyName.Equals("ServerAddress", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "serverAddress";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadDlms")
            {
                if (property.PropertyName.Equals("Start", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "start";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadDlms")
            {
                if (property.PropertyName.Equals("End", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "end";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadDlms")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputVarName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ReadDlms")
            {
                if (property.PropertyName.Equals("DataOutputUnits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "OutputUnidades";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Read_DLMS_Profile")
            {
                if (property.PropertyName.Equals("Obis", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ob";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.Read_DLMS_Profile")
            {
                if (property.PropertyName.Equals("Attribute", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "atrib";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.write_DLMS_Profile")
            {
                if (property.PropertyName.Equals("Obis", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ob";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.write_DLMS_Profile")
            {
                if (property.PropertyName.Equals("Attribute", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "atrib";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.write_DLMS_Profile")
            {
                if (property.PropertyName.Equals("Period", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "per";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestConsumo")
            {
                if (property.PropertyName.Equals("IntervalTime", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TiempoIntervalo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestConsumo")
            {
                if (property.PropertyName.Equals("Iterations", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Iteraciones";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestConsumo")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ResultName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestMeasureWithAnalizer")
            {
                if (property.PropertyName.Equals("IntervalTime", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "TiempoIntervalo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestMeasureWithAnalizer")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ResultName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointMeasure")
            {
                if (property.PropertyName.Equals("PrecisionDigits", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "digitosPrecision";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointMeasure")
            {
                if (property.PropertyName.Equals("Rank", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Rango";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointFrecuencyMeasure")
            {
                if (property.PropertyName.Equals("CouplingImpedance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ImpedanciaAcoplo";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointFrecuencyMeasure")
            {
                if (property.PropertyName.Equals("AtenuationImpedance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ImpedanciaAtenuacion";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointFrecuencyMeasure")
            {
                if (property.PropertyName.Equals("FilterImpedance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ImpedanciaFiltro";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointFrecuencyMeasure")
            {
                if (property.PropertyName.Equals("LevelImpedance", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ImpedanciaNivel";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.TestPointFrecuencyMeasure")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitProductAction")
            {
                if (property.PropertyName.Equals("NumProduct", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "NumProducto";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitProductAction")
            {
                if (property.PropertyName.Equals("Phase", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "Fase";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitProductAction")
            {
                if (property.PropertyName.Equals("IDEmployee", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "IDEmpleado";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.InitProductAction")
            {
                if (property.PropertyName.Equals("TestMode", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "ModoTest";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateStrings")
            {
                if (property.PropertyName.Equals("IsAlfaNumeric", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "IsAlfaNumericNumeric";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.ValidateStrings")
            {
                if (property.PropertyName.Equals("IsAlfa", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "IsAlfaNumeric";
            }

            if (property.DeclaringType.FullName == "Dezac.Tests.Actions.SSHReadAll")
            {
                if (property.PropertyName.Equals("DataOutput", StringComparison.OrdinalIgnoreCase))
                    property.PropertyName = "VariableName";
            }

            return property;
        }
    }
}
