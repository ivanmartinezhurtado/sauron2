﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(TestInputsOutputsDescription))]
    
    public class TestInputsOutputs : ActionBase
    {
        [DefaultValue("OutOn")]
        public string KeyContains { get; set; }

        [DefaultValue(stateEnum.ON)]
        public stateEnum OutputsState { get; set; }

        [DefaultValue(500)]
        public int WaitTime { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            var ouputsVariables = testBase.Variables.VariableList.Where((p) => p.Key.Contains(KeyContains)).ToDictionary(o => o.Key, o => o.Value);

            foreach (KeyValuePair<string, Object> outputs in ouputsVariables)
            {
                var output = Convert.ToByte(outputs.Key.ToUpper().Replace(string.Format("OUT{0}", OutputsState.ToString()), ""));
                var inputs = outputs.Value.ToString().Split(';');

                foreach (string input in inputs)
                {
                    tower.IO.DO[output] = OutputsState == stateEnum.ON;
                    testBase.Delay(WaitTime, "Espera activación de la salida");

                    var inputResult = tower.IO.DI[Convert.ToByte(input)];

                    tower.IO.DO[output] = OutputsState == stateEnum.OFF;
                    testBase.Delay(WaitTime, "Espera desactivación de la salida");

                    inputResult &= !tower.IO.DI[Convert.ToByte(input)];
  
                    testBase.Resultado.Set(outputs.Key, inputResult == true ? "OK" : "ERROR", ParamUnidad.SinUnidad);
                    testBase.Resultado.Set(string.Format("IN_{0}", input), inputResult == true ? "OK" : "ERROR", ParamUnidad.SinUnidad);
                }

                tower.IO.DO[output] = false;
            }

        }

        public enum stateEnum
        {
            ON,
            OFF
        }
    }

    public class TestInputsOutputsDescription : ActionDescription
    {
        public override string Name { get { return "TestInputsOutputs"; } }
        public override string Description { get { return "Acción genérica para autoTest IN/OUT de la torre"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.output; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }
}
