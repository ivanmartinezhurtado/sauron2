﻿using Dezac.Device;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(InitSerialPortCirbusDeviceActionDescription))]
    public class InitSerialPortCirbusDeviceAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número de Periférico")]
        public byte? Peripheral { get; set; }

        [Category("3. Configuration")]
        [Description("Puerto en el que se quiere trabajar")]
        public string Port { get; set; }

        [Category("3. Configuration")]
        [Description("Velocidad de transmsión del puerto")]
        public BpsEnum Bps { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            var portDevice = Convert.ToByte(GetVariable<string>(Port, Port).Replace("COM", ""));

            var device = new CirbusDeviceSerialPort(portDevice, (int)Bps, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);

            if (Peripheral.HasValue)
                device.PerifericNumber = Peripheral.Value;

            Context.Variables.AddOrUpdate(DeviceName ?? "CirbusDevice", device);
        }
    }

    public class InitSerialPortCirbusDeviceActionDescription : ActionDescription
    {
        public override string Name { get { return "InitCirbusSerialPort "; } }
        public override string Description { get { return "Crea una instancia de un dispositivo cirbus serie"; } }
        public override string Category { get { return "Tools/Comunications/Cirbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
    }

}
