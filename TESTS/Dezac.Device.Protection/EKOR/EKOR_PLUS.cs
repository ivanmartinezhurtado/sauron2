﻿using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.12)]
    public class EKOR_PLUS : DeviceBase
    {
        private FactorCalibrationCurrent factoresCalibracionCorriente;
        private FactorCalibrationVoltage factoresCalibracionVoltage;
        public static int factorTransformationCurrent = 300 * 1000;
        public static int factorTransformationVoltage = 10000;
   
        public CirbusDeviceSerialPort Cirbus { get; internal set; }
        public FactorCalibrationCurrent FactoresCalibracionCorriente
        {
            get
            {
                if (factoresCalibracionCorriente == null)
                    factoresCalibracionCorriente = new FactorCalibrationCurrent();
                
                return factoresCalibracionCorriente;
            }
        }

        public FactorCalibrationVoltage FactoresCalibracionVoltage
        {
            get
            {
                if (factoresCalibracionVoltage == null)
                    factoresCalibracionVoltage = new FactorCalibrationVoltage();

                return factoresCalibracionVoltage;
            }
        }

        public int FactorTransformationCurrent
        {
            get
            {
                return factorTransformationCurrent;
            }
            set
            {
                factorTransformationCurrent = value;
            }
        }
        public int FactorTransformationVoltage
        {
            get
            {
                return factorTransformationVoltage;
            }
            set
            {
                factorTransformationVoltage = value;
            }
        }

        public enum Scale
        {
            Fina = 0,
            Gruesa = 1,
            Cortoc = 2
        }

        public enum EkorPlusFamilyCalibration             // Diferenciamos los grupos generales, los cuales se calibraran de manera distinta
        {
            CAPACITIVO = 0,
            RESISTIVO = 1
        }

        public enum EkorPlusModelo
        {
            EKOR_PLUS_PASARELA,
            EKOR_PLUS_SIN_PASARELA,
            EKOR_DTC2
        }

        public enum EkorPlusFamilySubmodel          // Comentar a Ferran si es necesario, en el caso del RPA no se testea el puerto RS232 frontal, sinó que hay un USB
        {
            NO_RPA = 0,
            RPA = 1
        }

        public EKOR_PLUS()
        {
        }

        public EKOR_PLUS(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 4800, byte periferic = 0)  //port9=Cirbus Port5=cirbusTTL Port6=modbus
        {
            if (Cirbus == null)
                Cirbus = new CirbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Cirbus.PortCom = port;

            Cirbus.PerifericNumber = periferic;
        }

        // COMANDOS IDENTIFICACION DEL EQUIPO
        private const string READVERSIONFIRMWARE = "VER";
        private const string READVERSIONDEVELOPMENT = "VDP";
        private const string WRITEBASTIDOR = "ENS{0}";
        private const string READBASTIDOR = "LNS";
        private const string WRITESERIALNUMBER = "WSN{0}";        // 8 DIGITS NUMERO SERIE
        private const string READSERIALNUMBER = "RSN";
        private const string IDENTIFYHARDWARE = "HWR";

        //COMANDOS DE COMPROVACION DEL EQUIPO
        private const string TESTDISPLAY = "TDI";
        private const string TESTDISPLAY2 = "TD2";
        private const string READKEYBOARD = "TEC";
        private const string READINPUT = "ENT";
        private const string WRITEOUTPUT = "SAL{0}";
        private const string READMEASUREDCURRENTDISPLAY = "LPU";
        private const string READMEASUREDVOLTAGEDISPLAY = "LEV";
        private const string READSUMATORYCICLES = "LSC 0032";
        private const string READIPPASARELA = "IPR";

        // COMANDOS CALIBRACIÓN
        private const string WRITECURRENTFACTORSCALIBRATION = "EFC {0}";
        private const string WRITEVOLTAGEFACTORSCALIBRATION = "EFV {0}";
        private const string READCURRENTFACTORSCALIBRATION = "LFC";
        private const string READVOLTAGEFACTORSCALIBRATION = "LFV";

        private const string READFACTORSPROCOME = "LFP";
        private const string WRITEFACTORSPROCOME = "EFP {0}";

        private const string WRITECURRENTFACTORSCALIBRATIONCHANNEL1 = "FI1{0}";
        private const string WRITECURRENTFACTORSCALIBRATIONCHANNEL2 = "FI2{0}";
        private const string WRITECURRENTFACTORSCALIBRATIONCHANNEL3 = "FI3{0}";
        private const string WRITECURRENTFACTORSCALIBRATIONCHANNEL4 = "FI4{0}";
   
        private const string WRITECALIBRATIONPASATAPAS = "EPT {0:0000000} {1:0000000} {2:0000000}"; // 3 CANALS (V1, V2, V3)
        private const string READCALIBRATIONPASATAPAS = "LPT";

        private const string CLOCKCALIBRATION = "CLK DE2ACFAB S015";
        private const string GENERALCLOCKCALIBRATION = "CLK DE2ACFAB S{0:000}";    // genera una senyal de pulsos inhabilitando comunicaciones
        private const string SENDTEMPERATUREANDFREQUENCY = "CLK F{0} T{1}";
        private const string READCALIBRATIONCLOCK = "CLK I";
        private const string READTEMPERATURE = "CLK T";
        private const string READCOUNTTICKSLASTTEST = "CLK N";

        private const string CALIBRATIONPOINTSCHANNEL1 = "DL1";
        private const string CALIBRATIONPOINTSCHANNEL2 = "DL2";
        private const string CALIBRATIONPOINTSCHANNEL3 = "DL3";

        // COMANDOS PARAMETRIZACIÓN DEL EQUIPO
        private const string WRITEINSTALLERSETTINGS = "EAI{0}"; 
        private const string WRITEUSERSETTINGS = "EAU{0}";       
        private const string WRITECLOCKSETTINGS = "ERL{0:00}/{1:00}/{2:00} {3:00}:{4:00}:{5:00}";
        private const string READINSTALLERSETTINGS = "LAI";
        private const string READUSERSETTINGS = "LAU";
        private const string READCLOCKSETTINGS = "LRL";

        private const string GETDIRECTIONALPHASE = "GCF";  // Obtiene angulo entre Vh y Ih
        private const string READPHASEMEASUREDANGLE = "GF{0}";
        private const string GETMAXMINDIRECTIONALPHASE = "GFI";   // Obtiene angulo max y min entre Vh y Ih 
        private const string DIRECTIONALPHASECOMPROVATION = "CFI";  // Comprueba que el ángulo esté entre -180º i 180º
        private const string READDIRECTIONALPHASE = "LAD";  // COMPROVAR QUE ESTÁ OPERATIVO
        private const string WRITE_ANGLE_DEFAULT = "ECA 0000 0000 0000";  
        
        
        // COMANDOS DE DISPAROS I TABLAS DE LA VERDAD
        private const string READHISTORICTRIGGER = "HIS";
        private const string VALIDATETRIGGER = "VDI";
        private const string COUNTERANDTRIPDELETE = "BCO";

        private const string INITIALIZEPOINTER = "IPL";
        private const string WRITELOGICPROGRAMMINGTABLE = "EPL";
        private const string READLOGICPROGRAMMINGTABLE = "LPL";
        private const string RECORDLOGICPROGRAMMINGTABLE = "GPL";
        private const string READEEPROM = "LEE";
        
        // OTROS COMANDOS
        private const string BUFFERCOUNTER = "DB1";
        private const string DATACRASH = "DB2";
        private const string PRIORITYSYSTEMHANDLERS = "DB3";

        private const string IMPXML = "XML IMP {0}";
        private const string EXPXML = "XML EXP {0}";

        private const string READFLASHSTATUS = "FAT I";
        private const string FORMATFLASH = "FAT FORMAT";
        private const string FLASHIMP = "FAT IMP {0}";
        private const string FLASHEXP = "FAT EXP {0}";

        private Dictionary<byte, string> listErrorCirbus = new Dictionary<byte, string>()
        {
            { 1, "Error comando Cirbus" },
            { 2, "Error comando Cirbus" },
            { 6, "Error sintaxis del comando" },
            { 5, "Error rechazo de trama o desconocido" },
            { 7, "Error ChekSum del comando" }
        };

        private Dictionary<byte, string> listErrorPPP = new Dictionary<byte, string>()
        {
            { 0, "Error, no hay comunicación entre la pasarela y la cpu" },
            { 1, "Error, ethernets de la pasarela sin IP" }
        };

        #region ESTRUCTURAS

        public struct TriggerStruct
        {
            public long currentLastTrigger; 
            public int timeLastTrigger;  
            public byte dayLastTrigger;
            public byte monthLastTrigger;
            public byte yearLastTrigger;
            public byte hourLastTrigger;
            public byte minutesLastTrigger;
            public byte secondsLastTrigger;

            public int gapLastTrigger;
            public int gapPenultimateTrigger;

            public long currentPenultimateTrigger;  
            public int timePenultimateTrigger;  
            public byte dayPenultimateTrigger;
            public byte monthPenultimateTrigger;
            public byte yearPenultimateTrigger;
            public byte hourPenultimateTrigger;
            public byte minutesPenultimateTrigger;
            public byte secondsPenultimateTrigger;

            public int numberOffTriggers;
            public byte triggersStack;
            public int flagTrigger;            
        }

        public struct InfoTriggers
        {
            public double currentTrigger;
            public double currentTriggerHomopolar;
            public double timeTrigger;
            public double timeTriggerHomopolar;
        }

        public struct DirectionalPhase
        {
            public string cabecera;

            public int current;
            public int voltage;
            public int PActive;
            public int PReactive;
            public int angleLH;
            public string type;
        }

        public struct PasaTapas
        {
            public long L1;
            public long L2;
            public long L3;
        }

        #endregion

        #region READ POINTS (LSC) AND RMS CALCULATE

        public void WriteAngleCalibrationInitialization()
        {
            LogMethod();
            Cirbus.Write(WRITE_ANGLE_DEFAULT);
        }

        public struct MeasurePointsStructure
        {
            public string respuesta;
            public int Pt_I1;               //P1
            public int Pt_I2;               //P2
            public int Pt_I3;               //P3
            public int Pt_IH;               //P4
            public int Pt_V1;               //P5
            public int Pt_V2;               //P6
            public int Pt_V3;               //P7
            public int Pt_VH;               //P8
            public int Pt_POT1;             //P9
            public int Pt_POT2;             //P10
            public int Pt_POT3;             //P11
            public int Pt_POTH;             //P12
            public int Pt_Off_I1;           //P13
            public int Pt_Off_I2;           //P14
            public int Pt_Off_I3;           //P15
            public int Pt_Off_IH;           //P16
            public int Pt_Off_V1;           //P17
            public int Pt_Off_V2;           //P18
            public int Pt_Off_V3;           //P19
            public int Pt_Off_VH;           //P20
        }

        public MeasurePointsStructure? ReadRmsMeasurePoints()   // Comando LSC se debe leer dos veces, en la primera tenemos que obtener ACK (OK) y en la segunda se obtienen los valores de los puntos
        {
            LogMethod();

            var resp = Cirbus.Read(READSUMATORYCICLES);

            if (resp.Contains("ACK"))
            {
                System.Threading.Thread.Sleep(1000);
                return Cirbus.Read<MeasurePointsStructure>(READSUMATORYCICLES);
            }
            else
                return null;
        }

        public string ReadIPPasarelaPPP()
        {
            LogMethod();
            var ip = Cirbus.Read(READIPPASARELA, listErrorPPP);  
            return ip;
        }

        public RmsMesures ReadVoltageRmsMeasures(int delFirst = 1, int initCount = 2, int samples = 5, int interval = 750)
        {
 
            var readRms = ReadRmsMeasurePoints();
            if (!readRms.HasValue)
                throw new Exception("Error. No se han obtenido los valores medidos del sumatorio de ciclos de la trama LSC 0032 para poder calcular los valores eficaces de la calibración");

            var points = readRms.Value;
            var lecturas = new List<double>();

            RmsMesures resultadoAjuste = new RmsMesures();
            resultadoAjuste.RMS_VL1 = Math.Sqrt(Math.Abs((points.Pt_V1 / 16D) - Math.Pow((points.Pt_Off_V1 / 16D), 2)));
            resultadoAjuste.RMS_VL2 = Math.Sqrt(Math.Abs((points.Pt_V2 / 16D) - Math.Pow((points.Pt_Off_V2 / 16D), 2)));
            resultadoAjuste.RMS_VL3 = Math.Sqrt(Math.Abs((points.Pt_V3 / 16D) - Math.Pow((points.Pt_Off_V3 / 16D), 2)));                   

            _logger.DebugFormat("L1 V: {0}  OFF: {1}  RMS: {2}", points.Pt_V1, points.Pt_Off_V1, resultadoAjuste.RMS_VL1);
            _logger.DebugFormat("L2 V: {0}  OFF: {1}  RMS: {2}", points.Pt_V2, points.Pt_Off_V2, resultadoAjuste.RMS_VL2);
            _logger.DebugFormat("L3 V: {0}  OFF: {1}  RMS: {2}", points.Pt_V3, points.Pt_Off_V3, resultadoAjuste.RMS_VL3);                    

            return resultadoAjuste;
        }

        public FactorCalibrationCalculate CalculateReadRmsMeasure(int delFirst, int initCount, int samples, int interval, Func<RmsMesures, FactorCalibrationCalculate> calculateFactors)
        {
            var FCcalculated = new FactorCalibrationCalculate();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var readRms = ReadRmsMeasurePoints();
                    if (!readRms.HasValue)
                        throw new Exception("Error. No se han obtenido los valores medidos del sumatorio de ciclos de la trama LSC 0032 para poder calcular los valores eficaces de la calibración");

                    var points = readRms.Value;
                    var lecturas = new List<double>();

                    RmsMesures resultadoAjuste = new RmsMesures();
                    resultadoAjuste.RMS_IL1 = Math.Sqrt(Math.Abs((points.Pt_I1 / 16D) - Math.Pow((points.Pt_Off_I1 / 16D), 2)));
                    resultadoAjuste.RMS_IL2 = Math.Sqrt(Math.Abs((points.Pt_I2 / 16D) - Math.Pow((points.Pt_Off_I2 / 16D), 2)));
                    resultadoAjuste.RMS_IL3 = Math.Sqrt(Math.Abs((points.Pt_I3 / 16D) - Math.Pow((points.Pt_Off_I3 / 16D), 2)));
                    resultadoAjuste.RMS_ILH = Math.Sqrt(Math.Abs((points.Pt_IH / 16D) - Math.Pow((points.Pt_Off_IH / 16D), 2)));
                    resultadoAjuste.RMS_VL1 = Math.Sqrt(Math.Abs((points.Pt_V1 / 16D) - Math.Pow((points.Pt_Off_V1 / 16D), 2)));
                    resultadoAjuste.RMS_VL2 = Math.Sqrt(Math.Abs((points.Pt_V2 / 16D) - Math.Pow((points.Pt_Off_V2 / 16D), 2)));
                    resultadoAjuste.RMS_VL3 = Math.Sqrt(Math.Abs((points.Pt_V3 / 16D) - Math.Pow((points.Pt_Off_V3 / 16D), 2)));

                    _logger.DebugFormat("L1 I: {0}  OFF: {1}  RMS: {2}", points.Pt_I1, points.Pt_Off_I1, resultadoAjuste.RMS_IL1);
                    _logger.DebugFormat("L2 I: {0}  OFF: {1}  RMS: {2}", points.Pt_I2, points.Pt_Off_I2, resultadoAjuste.RMS_IL2);
                    _logger.DebugFormat("L3 I: {0}  OFF: {1}  RMS: {2}", points.Pt_I3, points.Pt_Off_I3, resultadoAjuste.RMS_IL3);
                    _logger.DebugFormat("LH I: {0}  OFF: {1}  RMS: {2}", points.Pt_IH, points.Pt_Off_IH, resultadoAjuste.RMS_ILH);

                    _logger.DebugFormat("L1 V: {0}  OFF: {1}  RMS: {2}", points.Pt_V1, points.Pt_Off_V1, resultadoAjuste.RMS_VL1);
                    _logger.DebugFormat("L2 V: {0}  OFF: {1}  RMS: {2}", points.Pt_V2, points.Pt_Off_V2, resultadoAjuste.RMS_VL2);
                    _logger.DebugFormat("L3 V: {0}  OFF: {1}  RMS: {2}", points.Pt_V3, points.Pt_Off_V3, resultadoAjuste.RMS_VL3);

                    lecturas.Add(resultadoAjuste.RMS_IL1);
                    lecturas.Add(resultadoAjuste.RMS_IL2);
                    lecturas.Add(resultadoAjuste.RMS_IL3);
                    lecturas.Add(resultadoAjuste.RMS_ILH);
                    lecturas.Add(resultadoAjuste.RMS_VL1);
                    lecturas.Add(resultadoAjuste.RMS_VL2);
                    lecturas.Add(resultadoAjuste.RMS_VL3);

                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    RmsMesures resultadoAjuste = new RmsMesures();
                    resultadoAjuste.RMS_IL1 = listValues.Average(0);
                    resultadoAjuste.RMS_IL2 = listValues.Average(1);
                    resultadoAjuste.RMS_IL3 = listValues.Average(2);
                    resultadoAjuste.RMS_ILH = listValues.Average(3);
                    resultadoAjuste.RMS_VL1 = listValues.Average(4);
                    resultadoAjuste.RMS_VL2 = listValues.Average(5);
                    resultadoAjuste.RMS_VL3 = listValues.Average(6);
                    FCcalculated = calculateFactors(resultadoAjuste);
                    return true;
                });

            return FCcalculated;
        }

        public struct RmsMesures
        {
            public double RMS_IL1;
            public double RMS_IL2;
            public double RMS_IL3;
            public double RMS_ILH;
            public double RMS_VL1;
            public double RMS_VL2;
            public double RMS_VL3;
        }

        #endregion

        #region CALCULATE FACTORS BY RMS VALUE

        public List<double> CalculateFactorCalibrationByScala(EkorPlusFamilyCalibration model, Scale scala, double current, double voltage, bool IsHomopolar, int delFirst, int initCount, int samples, int interval, TriLineValue mesurahp)
        {
            var list = new List<double>();

        
            var rmsMeasure = CalculateReadRmsMeasure(delFirst, initCount, samples, interval,
              (rms) =>
              {
                  var fcCalculate = new EKOR_PLUS.FactorCalibrationCalculate();

                  if (IsHomopolar)
                  {
                      fcCalculate.FC_ILH = ((current * 1000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_ILH);
                      list.Add(fcCalculate.FC_ILH);
                      _logger.DebugFormat("FC I LH: {0}", fcCalculate.FC_ILH);
                  }
                  else
                  {
                      fcCalculate.FC_IL1 = ((current * 1000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_IL1);
                      list.Add(fcCalculate.FC_IL1);
                      fcCalculate.FC_IL2 = ((current * 1000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_IL2);
                      list.Add(fcCalculate.FC_IL2);
                      fcCalculate.FC_IL3 = ((current * 1000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_IL3);
                      list.Add(fcCalculate.FC_IL3);

                      _logger.DebugFormat("FC I L1: {0}", fcCalculate.FC_IL1);
                      _logger.DebugFormat("FC I L2: {0}", fcCalculate.FC_IL2);
                      _logger.DebugFormat("FC I L3: {0}", fcCalculate.FC_IL3);

                      if (scala == Scale.Gruesa)
                      {
                          if (model == EkorPlusFamilyCalibration.RESISTIVO)
                          {
                              fcCalculate.FC_VL1 = ((voltage * 10000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_VL1);
                              list.Add(fcCalculate.FC_VL1);
                              fcCalculate.FC_VL2 = ((voltage * 10000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_VL2);
                              list.Add(fcCalculate.FC_VL2);
                              fcCalculate.FC_VL3 = ((voltage * 10000 * 65536 * 2 * Math.Sqrt(2)) / rms.RMS_VL3);
                              list.Add(fcCalculate.FC_VL3);
                          }
                          if (model == EkorPlusFamilyCalibration.CAPACITIVO)
                          {
                              var Vcal = 2.104;  // Valor fijado por el utillaje del EKOR, corroborar que sea correcto YA QUE ES UN VALOR ANTIGUO
                              //mesurahp;   Valor medido por el tester externo del sistema de test, V que cae en el puente de resistencias de la mangera

                              fcCalculate.FC_VL1 = ((((814.11 * mesurahp.L1) / Vcal) * 65536) / rms.RMS_VL1) * 7.6; 
                              list.Add(fcCalculate.FC_VL1);
                              fcCalculate.FC_VL2 = ((((814.11 * mesurahp.L2) / Vcal) * 65536) / rms.RMS_VL2) * 7.6;
                              list.Add(fcCalculate.FC_VL2);
                              fcCalculate.FC_VL3 = ((((814.11 * mesurahp.L3) / Vcal) * 65536) / rms.RMS_VL3) * 7.6;
                              list.Add(fcCalculate.FC_VL3);
                          }

                          _logger.DebugFormat("FC V L1: {0}", fcCalculate.FC_VL1);
                          _logger.DebugFormat("FC V L2: {0}", fcCalculate.FC_VL2);
                          _logger.DebugFormat("FC V L3: {0}", fcCalculate.FC_VL3);
                      }
                  }

                  return fcCalculate;
              });

            if (scala == EKOR_PLUS.Scale.Fina)
            {
                if (IsHomopolar)
                    FactoresCalibracionCorriente.FC_E1_HOMO = rmsMeasure.FC_ILH;
                else
                    FactoresCalibracionCorriente.FC_E1 = TriLineValue.Create(rmsMeasure.FC_IL1, rmsMeasure.FC_IL2, rmsMeasure.FC_IL3);
            }
            else if (scala == EKOR_PLUS.Scale.Gruesa)
            {
                if (IsHomopolar)
                    FactoresCalibracionCorriente.FC_E2_HOMO = rmsMeasure.FC_ILH;
                else
                {
                    FactoresCalibracionCorriente.FC_E2 = TriLineValue.Create(rmsMeasure.FC_IL1, rmsMeasure.FC_IL2, rmsMeasure.FC_IL3);
                    var voltage_FC_E2 = TriLineValue.Create(rmsMeasure.FC_VL1, rmsMeasure.FC_VL2, rmsMeasure.FC_VL3);
                    FactoresCalibracionVoltage.FC_E2 = voltage_FC_E2;
                    FactoresCalibracionVoltage.FC_E1 = (voltage_FC_E2 / 50.5);
                }
            }
            else if (scala == EKOR_PLUS.Scale.Cortoc)
                if (IsHomopolar)
                    FactoresCalibracionCorriente.FC_E3_HOMO = rmsMeasure.FC_ILH;
                else           
                    FactoresCalibracionCorriente.FC_E3 = TriLineValue.Create(rmsMeasure.FC_IL1, rmsMeasure.FC_IL2, rmsMeasure.FC_IL3);           

            return list;
        }

        public struct FactorCalibrationCalculate
        {
            public double FC_IL1;
            public double FC_IL2;
            public double FC_IL3;
            public double FC_ILH;
            public double FC_VL1;
            public double FC_VL2;
            public double FC_VL3;
        }

        #endregion

        #region WRITE AND READ FACTORS CALIBRATION

        // Escritura factores cal I
        public class FactorCalibrationCurrent
        {
            public double FCL1E1;
            public double FCL1E2;
            public double FCL1E3;
            public double FCL2E1;
            public double FCL2E2;
            public double FCL2E3;
            public double FCL3E1;
            public double FCL3E2;
            public double FCL3E3;
            public double FCLHE1;
            public double FCLHE2;
            public double FCLHE3;

            public TriLineValue FC_E1                 // Tres valors de línia per cada escala diferent
            {
                set
                {
                    FCL1E1 = value.L1;
                    FCL2E1 = value.L2;
                    FCL3E1 = value.L3;
                }
            }

            public TriLineValue FC_E2
            {
                set
                {
                    FCL1E2 = value.L1;
                    FCL2E2 = value.L2;
                    FCL3E2 = value.L3;
                }
            }

            public TriLineValue FC_E3
            {
                set
                {
                    FCL1E3 = value.L1;
                    FCL2E3 = value.L2;
                    FCL3E3 = value.L3;
                }
            }

            public double FC_E1_HOMO                   // Un valor per cada escala diferent homopolar
            {
                set
                {
                    FCLHE1 = value;
                }
            }

            public double FC_E2_HOMO
            {
                set
                {
                    FCLHE2 = value;
                }
            }

            public double FC_E3_HOMO
            {
                set
                {
                    FCLHE3 = value;
                }
            }

            public string TramaFI1
            {
                get
                {
                    return string.Format("{0:0000000} {1:0000000} {2:0000000}", FCL1E1, FCL1E2, FCL1E3);
                }
            }

            public string TramaFI2
            {
                get
                {
                    return string.Format("{0:0000000} {1:0000000} {2:0000000}", FCL2E1, FCL2E2, FCL2E3);
                }
            }

            public string TramaFI3
            {
                get
                {
                    return string.Format("{0:0000000} {1:0000000} {2:0000000}", FCL3E1, FCL3E2, FCL3E3);
                }
            }

            public string TramaFI4
            {
                get
                {
                    return string.Format("{0:0000000} {1:0000000} {2:0000000}", FCLHE1, FCLHE2, FCLHE3);
                }
            }

            public string TramaEFC
            {
                get
                {
                    return string.Format("{0:0000000} {1:0000000} {2:0000000} {3:0000000} {4:0000000} {5:0000000} {6:0000000} {7:0000000} {8:0000000} {9:0000000} {10:0000000} {11:0000000}", FCL1E1, FCL1E2, FCL1E3, FCL2E1, FCL2E2, FCL2E3, FCL3E1, FCL3E2, FCL3E3, FCLHE1, FCLHE2, FCLHE3);
                }
            }
        }

        public void WriteCurrentFactorCalibrationChannel1()
        {
            LogMethod();
            Cirbus.Write(WRITECURRENTFACTORSCALIBRATIONCHANNEL1, FactoresCalibracionCorriente.TramaFI1);
        }

        public void WriteCurrentFactorCalibrationChannel2()
        {
            LogMethod();
            Cirbus.Write(WRITECURRENTFACTORSCALIBRATIONCHANNEL2, FactoresCalibracionCorriente.TramaFI2);
        }

        public void WriteCurrentFactorCalibrationChannel3()
        {
            LogMethod();
            Cirbus.Write(WRITECURRENTFACTORSCALIBRATIONCHANNEL3, FactoresCalibracionCorriente.TramaFI3);
        }

        public void WriteCurrentFactorCalibrationChannel4()
        {
            LogMethod();
            Cirbus.Write(WRITECURRENTFACTORSCALIBRATIONCHANNEL4, FactoresCalibracionCorriente.TramaFI4);
        }

        public void WriteCurrentFactorCalibration()
        {
            LogMethod();
            Cirbus.Write(WRITECURRENTFACTORSCALIBRATION, FactoresCalibracionCorriente.TramaEFC);
        }

        // Lectura factores cal I
        public struct CalibrationCurrentStruct
        {
            public double FCL1E1;
            public double FCL1E2;
            public double FCL1E3;
            public double FCL2E1;
            public double FCL2E2;
            public double FCL2E3;
            public double FCL3E1;
            public double FCL3E2;
            public double FCL3E3;
            public double FCLHE1;
            public double FCLHE2;
            public double FCLHE3;

            public string TramaEFC
            {
                get
                {
                    return string.Format("{0:0000000} {1:0000000} {2:0000000} {3:0000000} {4:0000000} {5:0000000} {6:0000000} {7:0000000} {8:0000000} {9:0000000} {10:0000000} {11:0000000}", FCL1E1, FCL1E2, FCL1E3, FCL2E1, FCL2E2, FCL2E3, FCL3E1, FCL3E2, FCL3E3, FCLHE1, FCLHE2, FCLHE3);
                }
            }
        }

        public CalibrationCurrentStruct ReadCurrentFactorCalibration()
        {
            LogMethod();
            var resp = Cirbus.Read<CalibrationCurrentStruct>(READCURRENTFACTORSCALIBRATION, listErrorCirbus);
            return resp;
        }

        // Escritura factores cal V
        public class FactorCalibrationVoltage
        {
            public double FCL1E1;
            public double FCL2E1;
            public double FCL3E1;
            public double FCL1E2;
            public double FCL2E2;
            public double FCL3E2;

            public TriLineValue FC_E1
            {
                set
                {
                    FCL1E1 = value.L1;
                    FCL2E1= value.L2;
                    FCL3E1 = value.L3;
                }
            }

            public TriLineValue FC_E2
            {
                set
                {
                    FCL1E2 = value.L1;
                    FCL2E2 = value.L2;
                    FCL3E2 = value.L3;
                }
            }

            public string TramaEFV
            {
                get
                {
                    return string.Format("{0:00000000} {1:00000000} {2:00000000} {3:00000000} {4:00000000} {5:00000000}", FCL1E1, FCL1E2, FCL2E1, FCL2E2, FCL3E1, FCL3E2);
                }
            }
        }

        public void WriteVoltageFactorCalibration()
        {
            LogMethod();
            Cirbus.Write(WRITEVOLTAGEFACTORSCALIBRATION, FactoresCalibracionVoltage.TramaEFV);
        }

        // Lectura factores cal V
        public struct CalibrationVoltageStruct
        {
            public double FCL1E1;
            public double FCL1E2;
            public double FCL2E1;
            public double FCL2E2;
            public double FCL3E1;
            public double FCL3E2;

            public string TramaEFV
            {
                get
                {
                    return string.Format("{0:00000000} {1:00000000} {2:00000000} {3:00000000} {4:00000000} {5:00000000}", FCL1E1, FCL1E2, FCL2E1, FCL2E2, FCL3E1, FCL3E2);
                }
            }
        }

        public CalibrationVoltageStruct ReadVoltageFactorCalibration()
        {
            LogMethod();
            var resp = Cirbus.Read<CalibrationVoltageStruct>(READVOLTAGEFACTORSCALIBRATION, listErrorCirbus);
            return resp;
        }

        public struct FactoresProcome
        {
            string respuesta;
            double Procome_P_num;
            double Procome_P_den;
            double Procome_Q_num;
            double Procome_Q_den;
            double Procome_PT_num;
            double Procome_PT_den;
            double Procome_QT_num;
            double Procome_QT_den;
            double Procome_ST_num;
            double Procome_ST_den;
            public string TramaLFP
            {
                get
                {
                    return string.Format("{0:00000} {1:00000} {2:00000} {3:00000} {4:00000} {5:00000} {6:00000} {7:00000} {8:00000} {9:00000}", Procome_P_num, Procome_P_den, Procome_Q_num, Procome_Q_den, Procome_PT_num, Procome_PT_den, Procome_QT_num, Procome_QT_den, Procome_ST_num, Procome_ST_den);
                }
            }
        }

        public FactoresProcome ReadFactoresProcome()
        {
            LogMethod();
            var resp = Cirbus.Read<FactoresProcome>(READFACTORSPROCOME, false);
            return resp;
        }

        public void WriteFactoresProcome(string trama)
        {
           LogMethod();
           Cirbus.Write(WRITEFACTORSPROCOME, trama);
        }   

        #endregion

        #region READ MEASURE VOLTAGE, CURRENT, PHASE GAP

        public struct MedidaFases
        {
            public double L1;
            public double L2;
            public double L3;
            public double LH;

            public static MedidaFases operator /(MedidaFases y, int x)
            {
                var resultc1 = y.L1 / x;
                var resultc2 = y.L2 / x;
                var resultc3 = y.L3 / x;
                var resultc4 = y.LH / x;

                var resultMeasure = new MedidaFases();
                resultMeasure.L1 = resultc1;
                resultMeasure.L2 = resultc2;
                resultMeasure.L3 = resultc3;
                resultMeasure.LH = resultc4;
                return resultMeasure;
            }
        }

        public MedidaFases ReadMeasurementsCurrentDisplay()
        {
            LogMethod();
            var resp = Cirbus.Read<MedidaFases>(READMEASUREDCURRENTDISPLAY, listErrorCirbus);
            return resp / FactorTransformationCurrent; 
        }

        public MedidaFases ReadMeasurementsVolageDisplay()
        {
            LogMethod();
            var resp =  Cirbus.Read<MedidaFases>(READMEASUREDVOLTAGEDISPLAY, listErrorCirbus);
            return resp / FactorTransformationVoltage;
        } 

        public TriLineValue ReadMeasurementsVoltageDisplayInVolts(double Vref = 2.104)
        {
            LogMethod();

            var measures = ReadMeasurementsVolageDisplay();
            var pasaTapas = ReadPasaTapas();
               
            var L1 = (((measures.L1 * FactorTransformationVoltage) * 6.303387) / pasaTapas.L1) * 10;
            var L2 = (((measures.L2 * FactorTransformationVoltage) * 6.303387) / pasaTapas.L2) * 10;
            var L3 = (((measures.L3 * FactorTransformationVoltage) * 6.303387) / pasaTapas.L3) * 10;

            return TriLineValue.Create(L1, L2, L3);
        }

        public struct MeasureAnglePhase
        {
            public MeasureAngle L1;
            public MeasureAngle L2;
            public MeasureAngle L3;
        }

        public struct MeasureAngle
        {
            public string command;
            public double I;
            public double V;
            public double P;
            public double Q;
            public double CosPhi;
            public double Anglex10;

            public double Angle
            {
                get
                {  //Se invierte porque el EKOR su referencia es I1 al reves que el resto de equipos de medida 
                    return -(Anglex10 / 10);
                }
            }


            public double Angle_EkorPlus
            {
                get
                {  //Se invierte porque el EKOR su referencia es I1 al reves que el resto de equipos de medida 
                    return -(Anglex10 / 10) + 180;
                }
            }

            public double Current
            {
                get
                {
                    return (I / factorTransformationCurrent);
                }
            }
            public double Voltage
            {
                get
                {
                    return (V / factorTransformationVoltage);
                }
            }
        }

        public MeasureAnglePhase ReadMeasurementsAnglePhase()
        {
            LogMethod();
            var phaseAngle = new MeasureAnglePhase();
            phaseAngle.L1 = Cirbus.Read<MeasureAngle>(string.Format(READPHASEMEASUREDANGLE, 1));
            phaseAngle.L2 = Cirbus.Read<MeasureAngle>(string.Format(READPHASEMEASUREDANGLE, 2));
            phaseAngle.L3 = Cirbus.Read<MeasureAngle>(string.Format(READPHASEMEASUREDANGLE, 3));
            return phaseAngle;
        }

        public struct MeasureAngleHomopolar
        {
            public string command;
            public double P;
            public double Q;
            public double CosPhi;
            public double Anglex10;
            public double Angle
            {
                get
                {
                    return -(Anglex10 / 10);
                }
            }
        }

        public MeasureAngleHomopolar ReadMeasurementsAnglePhaseHomopolar()
        {
            LogMethod();
            return Cirbus.Read<MeasureAngleHomopolar>(string.Format(READPHASEMEASUREDANGLE, "I"));
        }

        #endregion

        #region CALIBRATION CLOCK

        public void WriteClockCalibration()
        {
            LogMethod();
            Cirbus.Write(CLOCKCALIBRATION);
            Cirbus.Read("ACK");
        }

        public struct CalibrationClockResponses
        {
            public int Arr_50Hz_TM1;
            public int Arr_60Hz_TM1;
            public int Po;
            public int Err25;
            public int RTC_PRS;
        }

        private Dictionary<byte, string> listErrorClock = new Dictionary<byte, string>(){
             { 1, "Falla EEP 1º Pagina" },
             { 2, "Falla EEP 2º Pagina" },
             { 3, "Falla EEP 1º y 2º Pagina" },
             { 4, "Fallo indeterminado" },
             { 5, "Comando calibración sin haber echo el comando Start Test" },
             { 6, "Error sintaxis del comando CLK" },
             { 7, "Error ChekSum del comando CLK" }
        };

        public CalibrationClockResponses ReadCalibrationClock()
        {
            LogMethod();
            return Cirbus.Read<CalibrationClockResponses>(READCALIBRATIONCLOCK, listErrorClock);
        }

        public CalibrationClockResponses CalibrationClock(double frequency, double temperature)
        {
            LogMethod();
            return Cirbus.Read<CalibrationClockResponses>(SENDTEMPERATUREANDFREQUENCY, listErrorClock, frequency.ToString("000.000000").Replace(',', '.'), temperature.ToString("00.0").Replace(',', '.'));
        }

        public void StartSignalClok(int secondDuration)
        {
            LogMethod();
            Cirbus.Retries = 0;
            Cirbus.WithTimeOut((p) => { p.Write(GENERALCLOCKCALIBRATION, listErrorClock, secondDuration); });
            Cirbus.Retries = 3;
        }

        //public DateTime ReadDateTime()
        //{
        //    LogMethod();
        //    var resp = Cirbus.Read(READCLOCKSETTINGS, listErrorClock);
        //    return Convert.ToDateTime(resp);
        //}

        public DateTime ReadDateTime()
        {
            LogMethod();
            var resp = Cirbus.Read(READCLOCKSETTINGS, listErrorClock);
            return FormatDateTime(resp);
        }

        private DateTime FormatDateTime(string ekorFormatDateTime)
        {
            var arrayFechaHora = ekorFormatDateTime.Split(' ');

            var Fecha = arrayFechaHora[0].Replace(':', '/');
            var Hora = arrayFechaHora[1].Replace('/', ':');

            return Convert.ToDateTime(string.Format("{0} {1}", Fecha, Hora));
        }

        #endregion

        #region KEYBOARD

        public void WriteClearKeyboard()
        {
            LogMethod();
            Cirbus.Read(READKEYBOARD);
        }

        public InputReadings ReadKeyboard()
        {
            LogMethod();
            var keyboardReading = Cirbus.Read(READKEYBOARD);

            var result = Convert.ToInt32(keyboardReading.Replace("ACK", string.Empty), 16);

            if (Enum.IsDefined(typeof(InputReadings), result))
                return (InputReadings)result;
            else
                return InputReadings.NO_BUTTON_OR_INPUT;
        }

        [Flags]
        public enum InputReadings
        {
            [Description("Ninguna tecla pulsada")]
            NO_BUTTON_OR_INPUT = 0x0000,
            [Description("Tecla DERECHA")]
            RIGHT_BUTTON = 0x0001,
            [Description("Tecla ABAJO")]
            DOWN_BUTTON = 0x0002,
            [Description("Tecla IZQUIERDA")]
            LEFT_BUTTON = 0x0004,
            [Description("Tecla ARRIBA")]
            UP_BUTTON = 0x0008,
            [Description("Tecla SET")]
            SET_BUTTON = 0X0010,
            [Description("Tecla ESC")]
            ESC_BUTTON = 0x0020,
        }
        #endregion 

        #region INPUTS OUTPUTS

        private Dictionary<byte, string> listErrorInputs = new Dictionary<byte, string>(){
             { 6, "Error sintaxis del comando" },
             { 7, "Error ChekSum del comando" }
        };

        public struct InputsStructure
        {
            public enumInputs input;
            public bool state;
        }

        public enum enumInputs
        {
            //NOTHING = 0x001f,   // Valores fijos de las entradas
            //IN1 = 0x001e,
            //IN2 = 0x001d,
            //IN3 = 0x001b,
            //IN4 = 0x0017,
            //IN5 = 0x000f,
            //IN6 = 0x003f,
            //IN7 = 0x005f,
            //IN8 = 0x009f,
            //IN9 = 0x011f,
            //IN10 = 0x021f,
            [Description("Ninguna entrada digital activada")]
            NOTHING = 0x001f,
            [Description("Entrada digital 1")]
            IN1 = NOTHING - 1,
            [Description("Entrada digital 2")]
            IN2 = NOTHING - (1 << 1),
            [Description("Entrada digital 3")]
            IN3 = NOTHING - (1 << 2),
            [Description("Entrada digital 4")]
            IN4 = NOTHING - (1 << 3),
            [Description("Entrada digital 5")]
            IN5 = NOTHING - (1 << 4),
            [Description("Entrada digital 6")]
            IN6 = (1 << 5) + NOTHING,
            [Description("Entrada digital 7")]
            IN7 = (1 << 6) + NOTHING,
            [Description("Entrada digital 8")]
            IN8 = (1 << 7) + NOTHING,
            [Description("Entrada digital 9")]
            IN9 = (1 << 8) + NOTHING,
            [Description("Entrada digital 10")]
            IN10 = (1 << 9) + NOTHING, 
            IN1_3_5 = IN1 & IN3 & IN5,  // TESTEO PARTICULAR DE UN MODELO DE 5 ENTRADAS (PARES / IMPARES) --> ESTANDARIZARLO                                             
            IN1_3 = IN1 & IN3,
            IN1_5 = IN1 & IN5,
            IN3_5 = IN3 & IN5,
            IN2_4 = IN2 & IN4,
            
            //IN6_8_10 = IN6 & IN8 & IN10,
            //IN6_8 = IN6 & IN8,
            //IN6_10 = IN6 & IN10,
            //IN8_10 = IN8 & IN10,
            //IN7_9 = IN7 & IN9,
            NULL = 9999
        }

        public int ReadInputs()
        {
            LogMethod();
            var input = Cirbus.Read(READINPUT, listErrorInputs);
            return int.Parse(input, NumberStyles.HexNumber);             // Convierte de alguna manera STRING -> HEXA
        }

        //public string ReadInputs()
        //{
        //    LogMethod();
        //    return Cirbus.Read(READINPUT, listErrorInputs);
        //}

        [Flags]
        public enum enumOutputs
        {
            [Description("Ninguna salida digital activada")]
            NOTHING = 0x0002,
            [Description("Salida digital 1")]
            OUT1 = 0x0001 + NOTHING,
            [Description("Salida digital 2")]
            OUT2 = 0,
            [Description("Salida digital 3")]
            OUT3 = (1 << 2) + NOTHING,          // 0x0006
            [Description("Salida digital 4")]
            OUT4 = (1 << 3) + NOTHING,          // 0x000A
            [Description("Salida digital 5")]
            OUT5 = (1 << 4) + NOTHING,
            [Description("Salida digital 6")]
            OUT6 = (1 << 5) + NOTHING,
            [Description("Salida digital 7")]
            OUT7 = (1 << 6) + NOTHING, 
        }

        public void WriteOutput(enumOutputs output)
        {
            LogMethod();
            Cirbus.Write(WRITEOUTPUT, ((byte)output).ToString("x4"));
        }

        #endregion

        #region PARAMETRIZATION MODELS

        public class InstallerSettings
        {
            private string setup { get; set; }
            private string password { get; set; }
            private string model { get; set; }
            private string frecuency { get; set; }
            private string relay { get; set; }
            private string protectionPhase { get; set; }
            private string saturationLevel { get; set; }
            private string saturationFactorOlvido { get; set; }
            private string configurationBit { get; set; }
            private string pulseTime { get; set; }
            private string filterInput { get; set; }
            private string mta { get; set; }
            private string correctionPhase { get; set; }
            private string relayDirectional { get; set; }
            private string ethernet { get; set; }

            public ushort Periferic
            {
                get { return (ushort)((Convert.ToUInt16(setup, 16) & 0xF800) >> 11); }
                set { configurationBit = Periferic.ToString(); }
            }
            public Protocol Protocol
            {
                get { return (Protocol)((Convert.ToUInt16(setup, 16) & 0x700) >> 8); }
                set { configurationBit += ((ushort)Protocol).ToString(); }
            }
            public Parity Parity
            {
                get { return (Parity)((Convert.ToUInt16(setup, 16) & 0x60) >> 5); }
                set { configurationBit = ((ushort)Parity).ToString(); }
            }
            public StopBits StopBits
            {
                get { return (StopBits)((Convert.ToUInt16(setup, 16) & 0x10) >> 4); }
                set { configurationBit = ((ushort)StopBits).ToString(); }
            }
            public NumberBits NumberBits
            {
                get { return (NumberBits)((Convert.ToUInt16(setup, 16) & 0x8) >> 3); }
                set { configurationBit = ((ushort)NumberBits).ToString(); }
            }
            public BaudRate BaudRate
            { get
                { return (BaudRate)(Convert.ToUInt16(setup, 16) & 0x7); }
                set { configurationBit = ((ushort)BaudRate).ToString(); }
            }

            public ushort Password
            {
                get { return Convert.ToUInt16(password, 16); }
                set { password = Password.ToString(); }
            }

            public byte Model
            {
                get { return Convert.ToByte(model, 16); }
                set { model = Model.ToString(); }
            }

            public Frecuency Frecuency
            {
                get { return (Frecuency)Convert.ToByte(frecuency); }
                set { frecuency = ((ushort)Frecuency).ToString(); }
            }

            public TypeRelay Relay
            {
                get { return (TypeRelay)Convert.ToByte(relay); }
                set { relay = ((ushort)Relay).ToString(); }
            }
           
            public TypeProtection ProtectionPhase
            {
                get { return (TypeProtection)Convert.ToByte(protectionPhase); }
                set { protectionPhase = ((ushort)ProtectionPhase).ToString(); }
            }

            public ushort SaturationLevel
            {
                get { return Convert.ToUInt16(saturationLevel); }
                set { saturationLevel = SaturationLevel.ToString(); }
            }

            public byte SaturationFactorOlvido
            {
                get { return Convert.ToByte(saturationFactorOlvido); }
                set { saturationFactorOlvido = SaturationFactorOlvido.ToString(); }
            }

            public ModeloPasatapas ConfigurationBit
            {
                get { return (ModeloPasatapas)Convert.ToUInt16(configurationBit); }
                set { configurationBit = ((ushort)ConfigurationBit).ToString(); }
            }

            public ushort PulseTime
            {
                get { return Convert.ToUInt16(pulseTime); }
                set { pulseTime = PulseTime.ToString(); }
            }

            public ushort FilterInput
            {
                get { return Convert.ToUInt16(filterInput); }
                set { filterInput = FilterInput.ToString(); }
            }

            public ushort MTA
            {
                get { return Convert.ToUInt16(mta); }
                set { mta = MTA.ToString(); }
            }

            public CorrectionPhase CorrectionPhase
            {
                get { return (CorrectionPhase)Convert.ToByte(correctionPhase); }
                set { correctionPhase = ((ushort)CorrectionPhase).ToString(); }
            }

            public bool RelayDirectional
            {
                get { return Convert.ToByte(relayDirectional) == 1 ? true : false; }
                set { relayDirectional = RelayDirectional ? "1" : "0"; }
            }

            public bool Ethernet
            {
                get { return Convert.ToByte(ethernet) == 1 ? true : false; }
                set { ethernet = Ethernet ? "1" : "0"; }
            }

            public string TramaInstallerSettings
            {
                get
                {
                    return string.Format("{0:0000} {1:0000} {2:00} {3:00} {4:00} {5:00} {6:0000} {7:00} {8:0000} {9:0000} {10:0000} {11:0000} {12:00} {13:00} {14:00}", setup, password, model, frecuency, relay, protectionPhase, saturationLevel, saturationFactorOlvido, configurationBit, pulseTime, filterInput, mta, correctionPhase, relayDirectional, ethernet);
                }
            }
        }

        public void WriteInstallerSettingsFAB(InstallerSettings configuracionInstalador)
        {
            LogMethod();
            Cirbus.Write(WRITEINSTALLERSETTINGS, configuracionInstalador.TramaInstallerSettings);
        }

        public class UserSettings
        {           
            public string iFaltaFase { get; set; }                  // UserSettings Familia Ekor +   ---> COMPLETO!
            public string iFaltaHomo { get; set; }
            public string tVerifAbsenciaV { get; set; }
            public string tRetardInd { get; set; }
            public string tResetV { get; set; }
            public string tResetT { get; set; }
            public string tHisteresisV { get; set; }
            public string umbralVfase { get; set; }
            public string flagDeteccionFaltaFase { get; set; }
            public string flagDeteccionFaltaHomo { get; set; }
            public string tFaltaFase { get; set; }
            public string tFaltaHomo { get; set; }
            public string passwordUser { get; set; }
            public string defectosSeccionalizador { get; set; }
            public string tBloqueo { get; set; }
            public string modoSeccionalizador { get; set; }               
            public string umbralVhomo { get; set; }
            public string modoDireccionalidad { get; set; }               

            public string TramaUserSettingsEkorPlus
            {
                get
                {
                    return string.Format("{0:0000} {1:0000} {2:0000} {3:0000} {4:0000} {5:0000} {6:00} {7:00} {8:00} {9:00} {10:00} {11:00} {12:0000} {13:0000} {14:0000} {15:00} {16:00} {17:00}", iFaltaFase, iFaltaHomo, tVerifAbsenciaV, tRetardInd, tResetV, tResetT, tHisteresisV, umbralVfase, flagDeteccionFaltaFase, flagDeteccionFaltaHomo, tFaltaFase, tFaltaHomo, passwordUser, defectosSeccionalizador, tBloqueo, modoSeccionalizador, umbralVhomo, modoDireccionalidad);
                }
            }
        }

        public void WriteUserSettingsFAB(UserSettings configuracionUsuario)
        {
            LogMethod();
            Cirbus.Write(WRITEUSERSETTINGS, configuracionUsuario.TramaUserSettingsEkorPlus);
        }

        #endregion

        #region COMANDOS

        public string ReadVersion()
        {
            LogMethod();
            return Cirbus.Read(READVERSIONDEVELOPMENT);
        }

        public string ReadFirmware()
        {
            LogMethod();
            return Cirbus.Read(READVERSIONFIRMWARE);
        }

        public TriggerStruct ReadHistoricTrigger()       // Comprovar que la trama que ens envia sigui correcta
        {
            LogMethod();
            return Cirbus.Read<TriggerStruct>(READHISTORICTRIGGER, true);
        }

        public void ReadyDisplay()
        {
            LogMethod();
            Cirbus.Write(TESTDISPLAY);
        }

        public void ReadyDisplay2()              // Preguntar si esta comprobación se debe hacer o no...
        {
            LogMethod();
            Cirbus.Write(TESTDISPLAY2);
        }

        public void WriteRemoveCounter()
        {
            LogMethod();
            Cirbus.Write(COUNTERANDTRIPDELETE);
        }

        public void WriteUserSettings(string userSettings)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITEUSERSETTINGS, userSettings, listErrorCirbus));
        }

        public string ReadUserSettings()
        {
            LogMethod();
            return Cirbus.Read(READUSERSETTINGS);
        }

        public void WriteInstallerSettings(string installerSettings)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITEINSTALLERSETTINGS, installerSettings));
        }

        public string ReadInstallerSettings()
        {
            LogMethod();
            return Cirbus.Read(READINSTALLERSETTINGS);
        }      

        public void WriteFrameNumber(string frameNumber)
        {
            LogMethod();

            Cirbus.Write(string.Format(WRITEBASTIDOR, frameNumber));
        }

        public void WriteSerialNumber(string serialNumber)
        {
            LogMethod();
            Cirbus.Write(WRITESERIALNUMBER, serialNumber);
        }

        public string ReadFrameNumber()
        {
            LogMethod();
            return Cirbus.Read(READBASTIDOR, listErrorCirbus).Remove(7, 1);
        }

        public string ReadSerialNumber()
        {
            LogMethod();
            return Cirbus.Read(READSERIALNUMBER, listErrorCirbus);
        }

        public void WriteDate(DateTime datetime)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITECLOCKSETTINGS, datetime.Day, datetime.Month, datetime.ToString("yy"), datetime.Hour, datetime.Minute, datetime.Second));
        }

        public double ReadHomopolarPhase()
        {
            LogMethod();
            return Cirbus.Read<double>(GETMAXMINDIRECTIONALPHASE);
        }

        public DirectionalPhase ReadDirectionalPhase()
        {
            LogMethod();
            return Cirbus.Read<DirectionalPhase>(GETDIRECTIONALPHASE);
        }

        public void WritePasatapas(long valorPasatapas1, long valorPasatapas2, long valorPasatapas3)
        {
            LogMethod();
            Cirbus.Write(string.Format(WRITECALIBRATIONPASATAPAS, valorPasatapas1, valorPasatapas2, valorPasatapas3));
        }

        public PasaTapas ReadPasaTapas()
        {
            LogMethod();
            return Cirbus.Read<PasaTapas>(READCALIBRATIONPASATAPAS, true);
        }

        public InfoTriggers ReadInfoTriggers()
        {
            LogMethod();

            var historic = Cirbus.Read(READHISTORICTRIGGER, listErrorCirbus);

            var infoTrigger = new InfoTriggers();
            infoTrigger.currentTriggerHomopolar = Convert.ToInt64(historic.Substring(8, 8), 16) / 30000D;
            infoTrigger.timeTriggerHomopolar = Convert.ToInt32(historic.Substring(20, 4), 16) / 100D;

            if (infoTrigger.currentTriggerHomopolar == 0 && infoTrigger.timeTriggerHomopolar == 0)
            {
                infoTrigger.currentTriggerHomopolar = Convert.ToInt64(historic.Substring(60, 8), 16) / 30000D;
                infoTrigger.timeTriggerHomopolar = Convert.ToInt32(historic.Substring(72, 4), 16) / 100D;
            }

            return infoTrigger;
        }

        public enum FileTypeExportImport
        {
            CAL,
            HIS,
            INS,
            USR,
            LOG,
            ALL,
        }

        private Dictionary<byte, string> listErrorXML = new Dictionary<byte, string>()
        {
            { 2, "Error no detecta flash o bloqueo USB" },
            { 3, "Error al importar/exportar un fichero" },
            { 4, "Error al grabar en la EEPROM" },
            { 5, "Valores de los ajustes fuera de límites o incorrectos" },
            { 6, "Fallo en la sintaxis del comando PRG" },
            { 7, "Fallo CheckSum del comando PRG" },
        };

        public void ImportXML (FileTypeExportImport typeFile)
        {
            LogMethod();
            Cirbus.Write(IMPXML, listErrorXML, typeFile.ToString());
        }

        public string ExportXML (FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(EXPXML, listErrorXML, file.ToString());
        }

        private Dictionary<byte, string> listErrorFAT = new Dictionary<byte, string>()
        {
            { 1, "Error al formatear la flash" },
            { 2, "Error no detecta flash o bloqueo USB" },
            { 3, "Error al importar/exportar un fichero" },
            { 4, "Error al grabar en la EEPROM" },
            { 5, "Valores de los ajustes fuera de límites o incorrectos" },
            { 6, "Fallo en la sintaxis del comando FAT" },
            { 7, "Fallo CheckSum del comando FAT" },        
        };

        public void FLASHFormat()
        {
            LogMethod();
            Cirbus.Transport.ReadTimeout = 6000;
            Cirbus.Write(FORMATFLASH);
            Cirbus.Transport.ReadTimeout = 2000;
        }

        public string ReadFLASHInformation()
        {
            LogMethod();
            return Cirbus.Read(READFLASHSTATUS, listErrorFAT);
        }

        public string FLASHImport (FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(FLASHIMP, listErrorFAT, file.ToString());
        }

        public string FLASHExport (FileTypeExportImport file)
        {
            LogMethod();
            return Cirbus.Read(FLASHEXP, listErrorFAT, file.ToString());
        }

        public string ReadValidateTrigger()
        {
            LogMethod();
            return Cirbus.Read(VALIDATETRIGGER, true);
        }

        #endregion

        public void RecoveryDefaultSettings(int port, int baudrate, byte periferico, System.IO.Ports.Parity parity = System.IO.Ports.Parity.None)
        {
            LogMethod();

            Cirbus.ClosePort();

            using (ModbusDeviceSerialPort Modbus = new ModbusDeviceSerialPort(port, baudrate, parity, System.IO.Ports.StopBits.One, _logger))
            {
                Modbus.UseFunction06 = true;
                Modbus.PerifericNumber = periferico;
                Modbus.Write((ushort)RegistersModbus.KEY_PSWI, (ushort)0x270F);
                Modbus.Write((ushort)RegistersModbus.PROTOCOL, (ushort)1);
                Modbus.Write((ushort)RegistersModbus.KEY_PSWI, (ushort)0x270F);
            };

            Cirbus.OpenPort();
        }

        public override void Dispose()
        {
            Cirbus.Dispose();
        }

        public enum RegistersModbus
        {
            KEY_PSWI = 0X0501,
            PROTOCOL = 0X0100
        }

        public enum Protocol
        {
            MODBUS = 0,
            CIRBUS = 1
        }

        public enum Parity
        {
            NONE = 0,
            EVEN = 1,
            ODD = 2
        }
        public enum BaudRate
        {
            _4800 = 2
        }

        public enum StopBits
        {
            ONE = 0,
            TWO = 1
        }

        public enum NumberBits
        {
            EIGHT = 0,
            SEVEN = 1
        }

        public enum Frecuency
        {
            _50Hz,
            _60Hz
        }

        public enum TypeRelay
        {
            ReleLinea,
            PasoFalta
        }

        public enum TypeProtection
        {
            NO,
            DT,
            NI,
            MI,
            EI
        }

        public enum ModeloPasatapas
        {
            OFF = 0,
            UNIPOLAR = 2,
            TRIPOLAR = 4,
            CALIBRADO = 6
        }

        public enum CorrectionPhase
        {
            _0º,
            _180º
        }
    }

    

}
