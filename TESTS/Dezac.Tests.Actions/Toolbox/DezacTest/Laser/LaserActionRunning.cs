﻿using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Laser;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(LaserActionRunningDescription))]
    public class LaserActionRunning : ActionBase
    {   
        [Category("3. Configuration")]
        [Description("Entrada de la señal Alarma del Laser")]
        [DefaultValue((byte)1)]
        public byte InputAlarm { get; set; }

        [Category("3. Configuration")]
        [Description("Entrada de la señal Ready del Laser")]
        [DefaultValue((byte)0)]
        public byte InputReady { get; set; }

        [Category("3. Configuration")]
        [Description("Salida de la señal Start del Laser")]
        [DefaultValue((byte)0)]
        public byte OutputStart { get; set; }

        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null)
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            if (data.NumOrden != null && data.NumProducto == 0)
                Error().PROCESO.PARAMETROS_ERROR.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test").Throw();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();
            
            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            List<LaserItemViewModel> laserItem;

            using (DezacService svc = new DezacService())
            {
                laserItem = svc.GetDatosLaser(data.NumProducto, data.Version, data.IdFase);

                if (data.NumOrden != null)
                {
                    var ok = svc.GetTestIndicators(data.NumOrden.Value, data.IdFase.ToString());
                }
            }

            using (var laser = new LaserControl())
            {
                laser.InputAlarm = InputAlarm;
                laser.InputReady = InputReady;
                laser.OutputStart = OutputStart;
                laser.TestBase = testBase;
                laser.LaserRuning(laserItem, tower.UsbIO);
            }
            
            SetVariable("LASER_FINISHED", true);
            data.Resultados.Set("CODIGO_BCN",laserItem.First().CodigoBcn, ParamUnidad.SinUnidad);
        }
    }

    public class LaserActionRunningDescription : ActionDescription
    {
        public override string Name { get { return "LaserRunning"; } }
        public override string Category { get { return "DezacTest/Laser"; } }
        public override string Description { get { return "Action que sirve para configurar la entrada y la salida de la señal del láser"; } }
        public override Image Icon { get { return Properties.Resources.Industry_Laser_Beam_icon; } }
    }
}
