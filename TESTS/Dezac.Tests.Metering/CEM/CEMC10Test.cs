﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Metering;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.24)]
    public class CEMC10Test : TestBase
    {
        private const int ILUMINACION_CAMARA = 51;
        private const int LEDS_OFF = 0;
        private const int LED_ON_UP = 1;
        private const int LED_ON_DOWN = 2;
        private const int KEY1 = 9;
        private const int KEY2 = 10;
        private const int OUT_LAMBDA = 20;

        private const int IN_BLOQ_UUT = 30;
        private const int OUT_RS485 = 6;
        private const int IN_UP_UUT = 31;
        private const int OUT_UP_UUT = 7;
        private const int IN_DOT_TEST_1_UUT = 32;
        private const int IN_DOT_TEST_2_UUT = 33;
        private const int OUT_DOT_TEST_UUT = 5;
        private const int OUT_IMPULSE_UUT = 8;
        private const int IN_IMPULSE_UUT = 34;
        private const int ERROR_LIGHT = 48;
        private const int IN_PRECINTO_UUT = 26;
        private const int IN_DETECT_UUT = 25;
        private const int IN_DOOR_OPEN_UUT = 24;
        private const int INPUT_MUX_IMPULSE_OUT = 2;
        private const int OUT_PRECINTO_UUT = 11;

        private CEMC10.Energy EnergyRead;
        private Tower3 tower;
        private CEMC10 cem;
        private MeterType meter;
        string CAMERA_DISPLAY;
       
        public void TestInitialization(int portCEM)
        {
            if (portCEM == 0)
                portCEM = Comunicaciones.SerialPort;

            cem = AddInstanceVar(new CEMC10(portCEM), "UUT");
            cem.Modbus.PerifericNumber = Comunicaciones.Periferico;
            cem.Modbus.TimeOut = 2000;
            ((ModbusDeviceSerialPort)cem.Modbus).RtsEnable = true;
            ((ModbusDeviceSerialPort)cem.Modbus).DtrEnable = true;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }


            var model = string.IsNullOrEmpty(Identificacion.MODELO) ? "212ES7C11E20" : Identificacion.MODELO;
            meter = new MeterType(model); 
            cem.Frequency = meter.FrequencyProperty.Frequency == 50D ? TypeMeterConection.frequency._50Hz : TypeMeterConection.frequency._60Hz;
            cem.MODELO_HARDWARE = Identificacion.MODELO_HARDWARE.Trim();
            logger.InfoFormat("MODELO_HARDWARE: {0}", Identificacion.MODELO_HARDWARE.Trim());

            tower.MTEPS.Output120A = meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO;
            tower.MTEWAT.Output120A = meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO;
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            if (tower.MTEPS.TypeSource == MTEPPS.typeSourceEnum.SPE120 && meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO)
                tower.ActiveCurrentHighLowUnionMTE(true);

          //  Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tower.WaitBloqSecurity(2000);

            tower.ActiveElectroValvule();

            if (!tower.IO.DI.WaitOn(IN_DOOR_OPEN_UUT, 2000))
                Shell.MsgBox("ATENCION PUERTA DEL UTIL ABIERTA", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DOOR_OPEN_UUT, 2000))
                throw new Exception("Error  puerta del util abierta");

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                Shell.MsgBox("ATENCION NO SE HA DETECTADO EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                throw new Exception("Error No se ha detectado el equipo");

            if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
                Shell.MsgBox("ATENCION NO SE HA DETECTADO EL PRECINTO DEL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
                throw new Exception("Error Precinto no detectado");

            tower.IO.DO.OnWait(500, OUT_UP_UUT);
            tower.OnOutAndWaitInput(OUT_DOT_TEST_UUT, 2000, IN_DOT_TEST_1_UUT, IN_DOT_TEST_2_UUT);
            tower.OnOutAndWaitInput(OUT_IMPULSE_UUT, 2000, IN_IMPULSE_UUT);

            tower.MTEPS.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.5, ParamUnidad.PorCentage);
            tower.MTEPS.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 35000, ParamUnidad.ms);
        }

        public void TestComunicaciones()
        {
            SamplerWithCancel((p) => { cem.WriteErrorCode(9999); return true; }, "Error comunicaciones, no hemos recibido respuesta del equipo");

            var version = cem.ReadSoftwareVersion();

            string[] versionBBDD = Identificacion.VERSION_FIRMWARE.Split('.');

            Assert.IsTrue(version.Major == Convert.ToUInt16(versionBBDD[0]) && version.Minor == Convert.ToUInt16(versionBBDD[1]) && version.Revision == Convert.ToUInt16(versionBBDD[2]), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, Versión de Firmware del equipo diferenete a la de la BBDD"));

            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Revision.ToString(), ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

            var crc = cem.ReadCRCFirmware();

            Resultado.Set(ConstantsParameters.Identification.CRC_FIRMWARE, crc.ToString(), ParamUnidad.SinUnidad, Identificacion.CRC_FIRMWARE.ToString());

            Assert.IsTrue(crc == Identificacion.CRC_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, CRC del Firmware del equipo diferenete a la de la BBDD"));

            var model = string.IsNullOrEmpty(Identificacion.MODELO) ? "212ES7C11E20" : Identificacion.MODELO;
            cem.WriteModel(model);
        }

        public void TestDisplay()
        {
            int timeWait = 1500;

            CAMERA_DISPLAY = GetVariable<string>("CAMERA_DISPLAY", CAMERA_DISPLAY);

            tower.MTEPS.ApplyOffAndWaitStabilisation();
            tower.MTEPS.ApplyAndWaitStabilisation();
            Delay(1000, "");
            tower.IO.DO.PulseOn(1000, KEY2);
            Delay(1000, "");

            cem.WriteDigitalOutputs(0x1010);

            cem.WriteDisplay(CEMC10.DisplayConfigurations.SEGMENTOS_IMPARES);
            Delay(timeWait, "Espera condicionamiento de los LEDS");
            this.TestHalconMatchingProcedure(CAMERA_DISPLAY, "CEMC10_IMPARES", "IMPARES", "CEMC10");

            cem.WriteDisplay(CEMC10.DisplayConfigurations.SEGMENTOS_PARES);
            Delay(timeWait, "Espera condicionamiento de los LEDS");
            this.TestHalconMatchingProcedure(CAMERA_DISPLAY, "CEMC10_PARES", "PARES", "CEMC10");
        }

        public void TestLeds()
        {
            CAMERA_DISPLAY = GetVariable<string>("CAMERA_DISPLAY", CAMERA_DISPLAY);

            cem.WriteDigitalOutputs(0x1000);

            cem.WriteDigitalOutputs(0x0003);
            Delay(1000, "Espera condicionamiento de los LEDS");
            this.TestHalconFindLedsProcedure(CAMERA_DISPLAY, "CEMC10_LED", "LED", "CEMC10");
        }

        public void TestTeclado()
        {
            tower.IO.DO.Off(KEY1, KEY2);

            SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 0; }, "Error Teclas activadas sin estar pulsadas", 5, 500);

            tower.IO.DO.On(KEY1);

            SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 1; }, "Error Tecla 1 no activada cuando esta pulsada", 5, 500);

            tower.IO.DO.Off(KEY1);
            tower.IO.DO.On(KEY2);

            SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 8; }, "Error Tecla 2 no activada cuando esta pulsada", 5, 500);

            tower.IO.DO.OffWait(500, KEY2);

            tower.IO.DO.PulseOn(500, OUT_PRECINTO_UUT);

            Delay(500, "tiempo espera piston");

            if (!tower.IO.DI[IN_PRECINTO_UUT])
            {
                tower.IO.DO.OnWait(1000, KEY2);

                SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 0; }, "Error Tecla 2 activada cuando esta con precinto ", 3, 1000);
            }
            else
                throw new Exception("Error Precinto se ha detectado cuando se ha activado el piston de entrada de precinto");

            tower.IO.DO.Off(KEY2);
        }

        public void TestSalidaImpulsos()
        {
            tower.IO.DO.On(OUT_LAMBDA);
            tower.LAMBDA.Initialize();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12, 0.1);
            tower.IO.DO.On(OUT_IMPULSE_UUT);

            cem.WriteDigitalOutputs(0);

            var defsVoltageOutputPulse = new AdjustValueDef() { Name = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("OFF").Name, Min = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("OFF").Min(), Max = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("OFF").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageOutputPulse,
              (step) =>
              {
                  return tower.MeasureMultimeter((InputMuxEnum)INPUT_MUX_IMPULSE_OUT, MagnitudsMultimeter.VoltDC);
              }, 0, 3, 500);

            cem.WriteDigitalOutputs(4);

            defsVoltageOutputPulse = new AdjustValueDef() { Name = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("ON").Name, Min = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("ON").Min(), Max = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("ON").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageOutputPulse,
              (step) =>
              {
                  return tower.MeasureMultimeter((InputMuxEnum)INPUT_MUX_IMPULSE_OUT, MagnitudsMultimeter.VoltDC);
              }, 0, 3, 500);

            cem.WriteDigitalOutputs(0);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            tower.IO.DO.Off(OUT_LAMBDA);
        }

        public void TestConfiguracionInicial()
        {
            cem.WriteBastidor((int)TestInfo.NumBastidor.Value);
            EnergyRead = cem.ReadEnergy();

            cem.WriteGainOffsetActive(0);
            cem.WriteGainOffsetReactive(0);
        }

        public void TestAjusteDesfase(int delFirst = 4, int initCount = 5, int samples= 10, int interval= 1100)
        {
            var anguloAjusteDesfase = 60;

            var description = string.Format("{0}_{1}Hz", Params.GAIN_DESFASE.L1.Name, meter.FrequencyProperty.Frequency.ToString());

            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase, anguloAjusteDesfase, meter.FrequencyProperty.Frequency);

            var valorAjuste = new AdjustValueDef(description, 0, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            Delay(5000, "Espera estabilización de la MTE");

            var resultadoAjuste = cem.CalculateGapAdjustMeasureFactors(delFirst, initCount, samples, interval, anguloAjusteDesfase,
            (value) =>
            {
                valorAjuste.Value = value;
                return !HasError(valorAjuste, false);
            });

            valorAjuste.AddToResults(Resultado);

            if (!valorAjuste.IsValid())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", valorAjuste.Name));
    
            cem.WriteGainsGap(Convert.ToUInt16(resultadoAjuste.Item2));
           
        }

        public void TestAjusteVoltage(int delFirst = 4, int initCount = 5, int samples = 10, int interval = 1100)
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase, 0, meter.FrequencyProperty.Frequency);

            var valoresAjuste = new AdjustValueDef(Params.GAIN_V.Null.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var resultadoAjuste = cem.CalculateVoltageAdjustMeasureFactors(delFirst, initCount, samples, interval, meter.VoltageProperty.VoltageNominal,
                (value) =>
                {
                    valoresAjuste.Value = value;
                    return !HasError(valoresAjuste, false);
                });

            valoresAjuste.AddToResults(Resultado);

            if (HasError(valoresAjuste, false))
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", valoresAjuste.Name));

            cem.WriteGainsVoltage(resultadoAjuste.Item2);
        }

        public void TestAjustePotencia(int delFirst = 4, int initCount = 5, int samples = 10, int interval = 1100)
        {
            var potenciaActiva = meter.VoltageProperty.VoltageNominal * meter.CurrentProperty.CorrienteBase;

            var description = string.Format("{0}_{1}Hz", Params.GAIN_KW.Null.Ajuste.Name, meter.FrequencyProperty.Frequency.ToString());

            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase, 0, meter.FrequencyProperty.Frequency);

            var valoresAjuste = new AdjustValueDef(description, 0, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var resultadoAjuste = cem.CalculatePowerMeasureFactors(delFirst, initCount, samples, interval, potenciaActiva,
                (value) =>
                {
                    valoresAjuste.Value = value;
                    return !HasError(valoresAjuste, false);
                });

            valoresAjuste.AddToResults(Resultado);

            if (HasError(valoresAjuste, false))
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", valoresAjuste.Name));

            cem.WriteGainsActive(Convert.ToUInt16(resultadoAjuste.Item2));
            cem.WriteGainsReactive(Convert.ToUInt16(resultadoAjuste.Item2));
        }

        public void TestPoint_MarchaVacio()
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal * 1.15, 0, 0, meter.FrequencyProperty.Frequency);

            Delay(2000, "Espera fuente MTE");

            var points = cem.ReadPointsActive();
            var gain = cem.ReadGainActive();

            Assert.AreGreater(Params.POINTS.Null.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain, points, Error().UUT.MEDIDA.MARGENES_SUPERIORES("Error. puntos de potencia leidos mayor a la ganacia el equipo consume energia"), ParamUnidad.Puntos);
           // Assert.AreGreater(gain, points, "Error. puntos de potencia leidos mayor a la ganacia el equipo consume energia");
        }

        public void TestPoint_Arranque()
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase *  0.04, 0, meter.FrequencyProperty.Frequency);

            Delay(2000, "Espera fuente MTE");

            var points = cem.ReadPointsActive();
            var gain = cem.ReadGainActive();

            Assert.AreGreater(Params.POINTS.Null.TestPoint("ACTIVE_ARRANQUE").Name, points, gain, Error().UUT.MEDIDA.MARGENES_INFERIORES("Error. puntos de potencia leidos menor a la ganacia el equipo no ve la corriente"), ParamUnidad.Puntos);
        }

        public void TestPoint_Imin([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia)
        {
            TestPointCalibration(TestPointsName.TP_IMIN, tipoPotencia);
        }

        public void TestPoint_IbCosPhi()
        {
            TestPointCalibration(TestPointsName.TP_IbCOS05,TipoPotencia.Activa);
        }

        public void TestPoint_IbSenPhi()
        {
            TestPointCalibration(TestPointsName.TP_IbSEN05, TipoPotencia.React);
        }

        public void TestPoint_IMax([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia)
        {
            TestPointCalibration(TestPointsName.TP_IMAX, tipoPotencia);
        }

        public void TestConfiguracionFrequency([Description("50=50Hz 60=60Hz")]TypeMeterConection.frequency freq)
        {
            if (meter.FrequencyProperty.IsAutomatico)
            {
                cem.Frequency = freq;
                meter.FrequencyProperty.Frequency = (double)freq;
                tower.MTEPS.ApplyOffAndWaitStabilisation();
            }
        }

        public void TestCustomize()
        {
            cem.WriteNumSerie(Convert.ToUInt32(TestInfo.NumSerie));

            var Energy = cem.ReadEnergy();
            if (Energy.ActiveExport <= EnergyRead.ActiveExport)
                throw new Exception("Error, no se ha incrementado la energia activa exportada del equipo");

            if (Model.modoPlayer != ModoTest.PostVenta)
            {
                cem.WriteReset();
                Delay(1500, "Reset CEM");
            }
            //var pesoImpulsos = Convert.ToUInt16(Configuracion.GetDouble("PESO_LED", 10, ParamUnidad.Whimp));
            //cem.WriteWeightimpulseOutput(pesoImpulsos);

            tower.MTEPS.ApplyOffAndWaitStabilisation();

            tower.MTEPS.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(230, 0, 0), 0, 50, 0, 0);

            int NumSerieRead = 0;
            SamplerWithCancel((p) => { NumSerieRead = cem.ReadNumSerie(); return true; }, "Error comunicaciones despues del reset de hardware para verificar la grabacion en memoria ");

            Assert.IsTrue(NumSerieRead.ToString() == TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el Numero de Serie grabado no coincide con el esperado"));

            var Bastidor = cem.ReadBastidor();
            Assert.IsTrue(Bastidor == (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error el numero de Bastidor grabado no coincide con el esperado"));

            //var pesoLedRead = cem.ReadWeightimpulseLed();
            //Resultado.Set(Params.WEIGHT_LED.Name, pesoLedRead, ParamUnidad.Whimp);
            //Assert.IsTrue(pesoLed == pesoLedRead, "Error el valor PESO_LED no corresponde el grabado al leido");

            Energy = cem.ReadEnergy();
            if (Energy.ActiveExport != 0)
                throw new Exception("Error, no se ha realizado un reset de energias al equipo");


            var codError = Convert.ToUInt16(Identificacion.CODE_ERROR);
            cem.WriteErrorCode(codError);

            Delay(3000, "Espera para grabar código de error");
        }

        public void TestFinish()
        {
            if (cem != null)
                cem.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    tower.OffOutAndWaitInput(OUT_IMPULSE_UUT, 2000, IN_IMPULSE_UUT);
                    tower.OffOutAndWaitInput(OUT_DOT_TEST_UUT, 2000, IN_DOT_TEST_1_UUT, IN_DOT_TEST_2_UUT);
                    tower.OffOutAndWaitInput(OUT_UP_UUT, 2000, IN_UP_UUT);
                }       
                tower.Dispose();
            }
        }

        //**********************************
        //**********************************

        private void TestPointCalibration(TestPointsName testPointName, TipoPotencia tipoPotencia)
        {
            TestPoint testPoint = new TestPoint(meter, testPointName, tipoPotencia);

            ConsignarMTE(testPoint.Voltage, testPoint.Current, testPoint.Desfase, testPoint.Frequency);

            Delay(1000, "Espera estabilización");

            internalVerification(testPoint);
        }

        private void internalVerification(TestPoint testPoint)
        {
            var potenciaAparente = testPoint.Voltage * testPoint.Current;
            var potenciaActiva = potenciaAparente * Math.Cos(testPoint.Desfase.ToRadians());
            var potenciaReactiva = potenciaAparente * Math.Sin(testPoint.Desfase.ToRadians());

            var Tolerance = testPoint.TipoPotencia == TipoPotencia.Activa ? testPoint.Meter.PrecisionProperty.PrecisionActiva : testPoint.Meter.PrecisionProperty.PrecisionReacctiva;
            var margen = Margenes.GetDouble(string.Format("PRECISION_{0}", testPoint.Name), Tolerance, ParamUnidad.PorCentage);

            var defs = new AdjustValueDef
            {
                Name = testPoint.Name,
                Tolerance = margen,
                Average = testPoint.TipoPotencia == TipoPotencia.Activa ? potenciaActiva : potenciaReactiva,
                Unidad = testPoint.TipoPotencia == TipoPotencia.Activa ? ParamUnidad.Kw : ParamUnidad.Kvar
            };
            TestCalibracionBase(defs,
            () =>
            {
                var vars = testPoint.TipoPotencia == TipoPotencia.React ? cem.ReadPowerReactiveHiResolution() : cem.ReadPowerActiveHiResolution();
                return vars;
            },
            () =>
            {
                var potenciaPatron = testPoint.TipoPotencia == TipoPotencia.React ? tower.MTEWAT.ReadReactivePower().Value.L1 : tower.MTEWAT.ReadActivePower().Value.L1;
                return potenciaPatron;
            }, 4, 8, 1200,
            string.Format("Error calculado en la calibración en {0} fuera de margenes", testPoint.Name));
        }

        private void ConsignarMTE(double tension, double corriente, double desfase, double frecuencia, double offsetPF = 3)
        {
            var voltage = TriLineValue.Create(tension);
            var current = TriLineValue.Create(corriente);
            var gap = TriLineValue.Create(desfase);

            if (meter.TipoEquipo == MeterType.TipoEquipoEnum.MONOFASICO)
            {
                voltage = TriLineValue.Create(tension, 0, 0);
                current = TriLineValue.Create(corriente, 0, 0);
                gap = TriLineValue.Create(desfase, 0, 0);
            }
            tower.MTEPS.ApplyPresetsAndWaitStabilisationWithValidator(voltage, current, frecuencia, gap, TriLineValue.Create(0, 120, 240), false, () =>
            {
                var measure = tower.MTEWAT.ReadAll();

                var pf = measure.PowerFactor;
                logger.InfoFormat("Stabilisation MTE PRS PowerFactor= {0}", pf);

                var freq = measure.Freq;
                logger.InfoFormat("Stabilisation MTE PRS Frequency= {0}", freq);

                var resultPF = pf.InToleranceFrom(gap, offsetPF);
                if (!resultPF)
                    logger.InfoFormat("PF Error {0} by MTE", pf.ToString());

                var errorFrec = (1 - (frecuencia / freq)) * 100;
                var resultFrec = Math.Abs(errorFrec) < tower.MTEPS.Tolerance;

                return resultFrec && resultPF;
            });
        }

    }
}
 