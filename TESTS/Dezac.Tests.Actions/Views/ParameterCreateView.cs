﻿using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Dezac.Tests.Actions.Views
{
    public partial class ParameterCreateView : Form
    {
        private BindingList<ParameterItemView> variables = new BindingList<ParameterItemView>();
        public ParameterCreateView(TypeParameter type)
        {
            InitializeComponent();

            List<string> db = GetConstants(typeof(ConstantsParameters.Electrics)); ;
            var fg = GetConstants(typeof(ConstantsParameters.FactorGains));
            db.AddRange(fg);

            switch (type)
            {
                case TypeParameter.Comunicaciones:
                    db = GetConstants(typeof(ConstantsParameters.Comunications));
                    grid.Columns["Fases"].IsVisible = false;
                    grid.Columns["TestPoint"].IsVisible = false;
                    grid.Columns["TestPoint2"].IsVisible = false;
                    grid.Columns["Margen"].IsVisible = false;
                    break;
                case TypeParameter.Identificacion:
                    db = GetConstants(typeof(ConstantsParameters.Identification));
                    grid.Columns["Fases"].IsVisible = false;
                    grid.Columns["TestPoint"].IsVisible = false;
                    grid.Columns["TestPoint2"].IsVisible = false;
                    grid.Columns["Margen"].IsVisible = false;
                    break;
                case TypeParameter.Margenes:
                    grid.Columns["Margen"].IsVisible = true;
                    break;
               // case TypeParameter.Vision:
            }

            var f = GetConstants(typeof(ConstantsParameters.Fases));
            var tp = GetConstants(typeof(ConstantsParameters.TestPoints));
            var pl = GetConstants(typeof(ConstantsParameters.PoweSourceLevel));
            var m = GetConstants(typeof(ConstantsParameters.MargenDesc));
            var u = GetConstants(typeof(ParamUnidad));

            ((GridViewComboBoxColumn)grid.Columns["Parametro"]).DataSource = db;
            ((GridViewComboBoxColumn)grid.Columns["Fases"]).DataSource = f;
            ((GridViewComboBoxColumn)grid.Columns["TestPoint"]).DataSource = tp;
            ((GridViewComboBoxColumn)grid.Columns["TestPoint2"]).DataSource = pl;
            ((GridViewComboBoxColumn)grid.Columns["Margen"]).DataSource = m;
            ((GridViewComboBoxColumn)grid.Columns["Unidades"]).DataSource = u;

            bsList.DataSource = variables;
            grid.DataSource = bsList;
        }


        public string parameters { get; set; }


        private List<string> GetConstants(Type type)
        {
            FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Public |
                 BindingFlags.Static | BindingFlags.FlattenHierarchy);

            return fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly).Select(p=> p.GetValue(null).ToString()).ToList();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            var vars = variables.ToList();

        }
    }

    public class ParameterItemView : INotifyPropertyChanged
    {
        private string parameter;
        private string fases;
        private string testPoint;
        private string modo;
        private string extension;
        private ParamUnidad unidades;

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"{Parametro}{Fases}{testPoint}{TestPoint2}{Margen};";
        }

        public string Parametro
        {
            get { return parameter; }
            set
            {
                if (value == parameter)
                    return;
                parameter = value;
                OnPropertyChanged();
            }
        }

        public string Fases
        {
            get { return fases; }
            set
            {
                if (value != fases)
                {
                    fases = value;
                    OnPropertyChanged();
                }
            }
        }

        public string TestPoint
        {
            get { return testPoint; }
            set
            {
                if (value != testPoint)
                {
                    testPoint = value;
                    OnPropertyChanged();
                }
            }
        }

        public string TestPoint2
        {
            get { return modo; }
            set
            {
                if (value != modo)
                {
                    modo = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Margen
        {
            get { return extension; }
            set
            {
                if (value != extension)
                {
                    extension = value;
                    OnPropertyChanged();
                }
            }
        }

        public ParamUnidad Unidades
        {
            get { return unidades; }
            set
            {
                if (value != unidades)
                {
                    unidades = value;
                    OnPropertyChanged();
                }
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
