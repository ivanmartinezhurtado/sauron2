﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionSoftwareLevel0
    {
        private TestExceptionSoftwareLevel1 psl;

        public string[] Category { get { return psl.Category; } set { psl.Category = value; } }

        public string ClassName { get { return psl.ClassName; } set { psl.ClassName = value; } }

        public TestExceptionSoftwareLevel1 SECUENCIA_TEST
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.SEQCUENCIA_TEST; return psl; }
        }

        public TestExceptionSoftwareLevel1 NO_CONTROLADO
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.NO_CONTROLADO; return psl; }
        }

        public TestExceptionSoftwareLevel1 BBDD
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.BBDD; return psl; }
        }

        public TestExceptionSoftwareLevel1 SAURON
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.SAURON; return psl; }
        }

        public TestExceptionSoftwareLevel1 STM32_FLASH_LOADER
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.STM32_FLASH_LOADER; return psl; }
        }

        public TestExceptionSoftwareLevel1 ESP32_FLASH_LOADER
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.ESP32_FLASH_LOADER; return psl; }
        }

        public TestExceptionSoftwareLevel1 BARTENDER
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.BARTENDER; return psl; }
        }

        public TestExceptionSoftwareLevel1 SISMA
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.SISMA; return psl; }
        }

        public TestExceptionSoftwareLevel1 FALLO_RED
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.FALLO_RED; return psl; }

        }

        public TestExceptionSoftwareLevel1 HALCON
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.HALCON; return psl; }
        }

        public TestExceptionSoftwareLevel1 DRIVER
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.DRIVER; return psl; }
        }

        public TestExceptionSoftwareLevel1 WEB_SERVICE
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.WEB_SERVICE; return psl; }
        }
        public TestExceptionSoftwareLevel1 ERROR_TRANSFERENCIA
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.ERROR_TRANSFERENCIA; return psl; }
        }
        public TestExceptionSoftwareLevel1 FIRMWARE
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.FIRMWARE; return psl; }
        }
        public TestExceptionSoftwareLevel1 IP
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.IP; return psl; }
        }
        public TestExceptionSoftwareLevel1 CPU
        {
            get { psl.SubCategory = ConstantsExceptions.SoftwareLevel0.CPU; return psl; }
        }

        public TestExceptionSoftwareLevel0()
        {
            psl = new TestExceptionSoftwareLevel1();
        }

    }
}
