﻿using Dezac.Core.Utility;
using Dezac.Device;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DisposeCirbusSerialPortActionDescription))]
    public class DisposeCirbusSerialPortAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM";
            var uut = GetVariable(name) as CirbusDeviceSerialPort;

            if (uut != null)
                uut.Dispose();
        }
    }

    public class DisposeCirbusSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "DisposeCirbusSerialPort"; } }
        public override string Description { get { return "Cierra el puerto cirbus serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/Cirbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
    }
}
