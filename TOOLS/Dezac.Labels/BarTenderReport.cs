﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using System;
using System.Collections.Generic;

namespace Dezac.Labels
{
    public class BarTenderReport : IDisposable
    {
        private BarTender.Application btApp;
        private BarTender.Format btFormat;
       
        private string labelFileName;

        public BarTenderReport(int identicalCopiesOfLabel = 1, int numberSerializedLabels = 1, bool visible = false)
        {
            IdenticalCopiesOfLabel = identicalCopiesOfLabel;
            NumberSerializedLabels = numberSerializedLabels;
            Visible = visible;
        }

        public string PathFile { get; set; }
        public string PrinterName { get; set; }
        public string FilterPrinterName { get; set; }
        public bool Visible { get; set; }
        public int IdenticalCopiesOfLabel { get; set; }
        public int NumberSerializedLabels { get; set; }

        public string LabelFileName
        {
            get { return labelFileName; }
            set
            {
                if (labelFileName != value)
                {
                    labelFileName = value;
                    LoadLabel();
                }
            }
        }

        private void LoadLabel()
        {
            if (string.IsNullOrEmpty(labelFileName))
                return;

            if (btApp == null)
            {
                btApp = new BarTender.Application();
                //btApp.LicenseServer.Address = "btls.dezac.net";
                //btApp.LicenseServer.Port = 5160;
                btApp.LicenseServer.Timeout = 10;
                btApp.LicenseServer.Retries = 1;
                if (!btApp.LicenseServer.IsConnected)
                    if (!btApp.LicenseServer.Connect())
                        throw new Exception("Error no se ha encontrado el LicenseServer conectado del BarTender, cominicarlo a Informatica");
            }

            if (btFormat != null)
                btFormat.Close(BarTender.BtSaveOptions.btDoNotSaveChanges);

            btFormat = btApp.Formats.Open(labelFileName, false, string.Empty);
        }

        public void PrintTest(Dictionary<string, object> ns)
        {
            FillValues(ns);
        }

        public void PrintTest(TestInfo testInfo)
        {
            if (testInfo == null)
                return;

            var ns = new Dictionary<string, object>();

            ns.Add("PN", testInfo.ProductoVersion);
            ns.Add("CODCIR", testInfo.CodCircutor);
            ns.Add("COD", testInfo.DeviceID);
            ns.Add("MAC_3G", testInfo.NumMAC3G == null ? string.Empty : testInfo.NumMAC3G.ToUpper().Replace(":", string.Empty));
            ns.Add("MAC_WIFI", testInfo.NumMACWifi == null ? string.Empty : testInfo.NumMACWifi.ToUpper().Replace(":", string.Empty));
            ns.Add("SSID", testInfo.SSID);
            var mac_PLC = testInfo.NumMAC_PLC == null ? string.Empty : testInfo.NumMAC_PLC.ToUpper().Replace(":", "");
            ns.Add("MAC", testInfo.NumMAC == null ? mac_PLC : testInfo.NumMAC.ToUpper().Replace(":", ""));
            ns.Add("MAC_PLC", mac_PLC);
            ns.Add("PSW", testInfo.PSW);
            ns.Add("NB", testInfo.NumBastidor);
            ns.Add("NS", testInfo.NumSerie);
            ns.Add("IMEI", testInfo.IMEI);
            ns.Add("ID", testInfo.NumSerieInterno);
            ns.Add("PER", testInfo.PerifericNumber);
            ns.Add("PV", testInfo.ProductoVersion);
            if (string.IsNullOrEmpty(testInfo.HardwareVersion))
                ns.Add("HW", testInfo.ProductoVersion);
            else
                ns.Add("HW", testInfo.HardwareVersion);

            ns.Add("FW", testInfo.FirmwareVersion);
            ns.Add("FECHAFAB", testInfo.FechaFabricacion);
            ns.Add("IPF", testInfo.FactoryIP);

            if (testInfo.NumSerieSubconjunto != null)
            {
                int i = 2;
                foreach (KeyValuePair<string, string> ns2 in testInfo.NumSerieSubconjunto)
                    ns.Add(string.Format("NS{0}", i++), ns2.Value);
            }

            if (testInfo.TypeLabel == TestInfo.labels.Numero_Serie_Subconjunto && !string.IsNullOrEmpty(testInfo.NumSerieInterno))
                ns["NS"] = testInfo.NumSerieInterno;

            FillValues(ns);
        }

        public void PrintTestError(int bastidor, string producto, string descriptionProduct, int ordenfab, int numTestFase, string puesto, string errorDescription)
        {
            var errorTest = new Dictionary<string, object>();

            errorTest.Add("producto", producto);
            errorTest.Add("descripcion", descriptionProduct);
            errorTest.Add("bastidor", bastidor);
            errorTest.Add("of", ordenfab);
            errorTest.Add("puesto", puesto);
            errorTest.Add("errorExt", errorDescription);
            errorTest.Add("nroTestFase", numTestFase);

            FillValues(errorTest);
        }

        public void FillValues(Dictionary<string, object> values)
        {
            foreach (KeyValuePair<string, object> item in values)
                try
                {
                    btFormat.SetNamedSubStringValue(item.Key, string.Format("{0}", item.Value));
                }
                catch (Exception) { }

            Print(PrinterName, FilterPrinterName);
        }

        public void Print(string printerName = null, string filterPrinterName = null)
        {
            if (string.IsNullOrEmpty(printerName))
                printerName = PrinterTools.SelectPrinterByPatternOrDefault(filterPrinterName);

            btFormat.Printer = printerName;
            btFormat.IdenticalCopiesOfLabel = IdenticalCopiesOfLabel;
            btFormat.NumberSerializedLabels = NumberSerializedLabels;
            btApp.Visible = Visible;

            btFormat.PrintOut(false, false);
        }

        public void Dispose()
        {
            if (btFormat != null)
                btFormat.Close( BarTender.BtSaveOptions.btDoNotSaveChanges);

            btFormat = null;

            btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);
            btApp = null;
        }

    }
}
