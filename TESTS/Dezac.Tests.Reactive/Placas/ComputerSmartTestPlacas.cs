﻿using Dezac.Device.Reactive;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Reactive.Placas
{
    [TestVersion(1.00)]
    public class COMPUTERSMARTTestPlacas : TestBase
    {
        //COMPUTERSMART smart;
        //Tower3 tower;

        //#region Variables

        //public const int OUT_PISTON_SW1 = 17;
        //public const int OUT_PISTON_SW2 = 18;
        //public const int OUT_PISTON_SW3 = 19;
        //public const int OUT_PISTON_SW4 = 20;
        //public const int OUT_PISTON_SW5 = 21;
        //public const int OUT_RED_LIGHT = 43;

        //public const int IN_RELAY_ALARM_NO = 7;
        //public const int IN_RELAY_ALARM_NC = 8;
        //public const int IN_RELAY_1 = 9;
        //public const int IN_RELAY_2= 10;
        //public const int IN_RELAY_3= 11;
        //public const int IN_RELAY_4= 12;
        //public const int IN_RELAY_5= 13;
        //public const int IN_RELAY_6= 14;
        //public const int IN_RELAY_7= 15;
        //public const int IN_RELAY_8= 16;
        //public const int IN_RELAY_9= 17;
        //public const int IN_RELAY_10= 18;
        //public const int IN_RELAY_11= 19;
        //public const int IN_RELAY_12= 20;
        //public const int IN_DEVICE_PRESENCE= 21;
        //public const int IN_DEVICE_LEDS= 22;

        //public const InputMuxEnum VRS_Measure = InputMuxEnum.IN2_AuxMedida1;
        //public const InputMuxEnum VCD_Measure = InputMuxEnum.IN9_AuxMedida8;
        //public const InputMuxEnum VCC_Measure = InputMuxEnum.IN11_AuxMedida10;
        //public const InputMuxEnum VREF_Measure = InputMuxEnum.IN12_V3;

        //#endregion

        //public void TestInitialization()
        //{
        //    tower = new Tower3();
        //    smart = new COMPUTERSMART(Comunicaciones.SerialPort);

        //    tower.LAMBDA.Initialize();
        //    tower.ShutdownSources();

        //    tower.On();
        //    tower.IO.DO.On(15);
        //}

        //public void TestConsumption()
        //{
        //    tower.Chroma.ApplyAndWaitStabilisation(Consignas.GetDouble("MINIMUM_VOLTAGE", 0, ParamUnidad.V), 0);

        //    COMPUTERSMART.RelayInformation relayInformation = new COMPUTERSMART.RelayInformation();

        //    relayInformation.DeactivateAll();
        //    smart.WriteRelaysInformation(relayInformation);

        //    var consumptionMargins = new List<AdjustValueDef>();
        //    consumptionMargins.Add(new AdjustValueDef(Params.W.Null.EnVacio.TestPoint("MINIMUM_VOLTAGE").Name, 0, Params.W.Null.EnVacio.TestPoint("MINIMUM_VOLTAGE").Min, Params.W.Null.EnVacio.TestPoint("MINIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.W));
        //    consumptionMargins.Add(new AdjustValueDef(Params.VAR.Null.EnVacio.TestPoint("MINIMUM_VOLTAGE").Name, 0, Params.VAR.Null.EnVacio.TestPoint("MINIMUM_VOLTAGE").Min, Params.VAR.Null.EnVacio.TestPoint("MINIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.Var));

        //    TestMeasureBase(consumptionMargins, (step) =>
        //        {
        //            var cvmMiniReadings = tower.CvmminiChroma.ReadAllVariables();

        //            return new double[] { cvmMiniReadings.Phases.L1.PotenciaActiva, cvmMiniReadings.Phases.L1.PotenciaReactiva };
        //        }, 2, 10, 1000);

        //    relayInformation.ActivateAll();
        //    smart.WriteRelaysInformation(relayInformation);

        //    consumptionMargins = new List<AdjustValueDef>();
        //    consumptionMargins.Add(new AdjustValueDef(Params.W.Null.ConCarga.TestPoint("MINIMUM_VOLTAGE").Name, 0, Params.W.Null.ConCarga.TestPoint("MINIMUM_VOLTAGE").Min, Params.W.Null.ConCarga.TestPoint("MINIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.W));
        //    consumptionMargins.Add(new AdjustValueDef(Params.VAR.Null.ConCarga.TestPoint("MINIMUM_VOLTAGE").Name, 0, Params.VAR.Null.ConCarga.TestPoint("MINIMUM_VOLTAGE").Min, Params.VAR.Null.ConCarga.TestPoint("MINIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.Var));

        //    TestMeasureBase(consumptionMargins, (step) =>
        //    {
        //        var cvmMiniReadings = tower.CvmminiChroma.ReadAllVariables();

        //        return new double[] { cvmMiniReadings.Phases.L1.PotenciaActiva, cvmMiniReadings.Phases.L1.PotenciaReactiva };
        //    }, 2, 10, 1000);

        //    tower.Chroma.ApplyAndWaitStabilisation(Consignas.GetDouble("MAXIMUM_VOLTAGE", 0, ParamUnidad.V), 0);

        //    relayInformation.DeactivateAll();
        //    smart.WriteRelaysInformation(relayInformation);

        //    consumptionMargins = new List<AdjustValueDef>();
        //    consumptionMargins.Add(new AdjustValueDef(Params.W.Null.EnVacio.TestPoint("MAXIMUM_VOLTAGE").Name, 0, Params.W.Null.EnVacio.TestPoint("MAXIMUM_VOLTAGE").Min, Params.W.Null.EnVacio.TestPoint("MAXIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.W));
        //    consumptionMargins.Add(new AdjustValueDef(Params.VAR.Null.EnVacio.TestPoint("MAXIMUM_VOLTAGE").Name, 0, Params.VAR.Null.EnVacio.TestPoint("MAXIMUM_VOLTAGE").Min, Params.VAR.Null.EnVacio.TestPoint("MAXIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.Var));

        //    TestMeasureBase(consumptionMargins, (step) =>
        //    {
        //        var cvmMiniReadings = tower.CvmminiChroma.ReadAllVariables();

        //        return new double[] { cvmMiniReadings.Phases.L1.PotenciaActiva, cvmMiniReadings.Phases.L1.PotenciaReactiva };
        //    }, 2, 10, 1000);

        //    relayInformation.ActivateAll();
        //    smart.WriteRelaysInformation(relayInformation);

        //    consumptionMargins = new List<AdjustValueDef>();
        //    consumptionMargins.Add(new AdjustValueDef(Params.W.Null.ConCarga.TestPoint("MAXIMUM_VOLTAGE").Name, 0, Params.W.Null.ConCarga.TestPoint("MAXIMUM_VOLTAGE").Min, Params.W.Null.ConCarga.TestPoint("MAXIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.W));
        //    consumptionMargins.Add(new AdjustValueDef(Params.VAR.Null.ConCarga.TestPoint("MAXIMUM_VOLTAGE").Name, 0, Params.VAR.Null.ConCarga.TestPoint("MAXIMUM_VOLTAGE").Min, Params.VAR.Null.ConCarga.TestPoint("MAXIMUM_VOLTAGE").Max, 0, 0, ParamUnidad.Var));

        //    TestMeasureBase(consumptionMargins, (step) =>
        //    {
        //        var cvmMiniReadings = tower.CvmminiChroma.ReadAllVariables();

        //        return new double[] { cvmMiniReadings.Phases.L1.PotenciaActiva, cvmMiniReadings.Phases.L1.PotenciaReactiva };
        //    }, 2, 10, 1000);

        //    tower.Chroma.ApplyAndWaitStabilisation(Consignas.GetDouble("NOMINAL_VOLTAGE", 0, ParamUnidad.V), 0);

        //    relayInformation.DeactivateAll();
        //    smart.WriteRelaysInformation(relayInformation);

        //    consumptionMargins = new List<AdjustValueDef>();
        //    consumptionMargins.Add(new AdjustValueDef(Params.W.Null.EnVacio.TestPoint("NOMINAL_VOLTAGE").Name, 0, Params.W.Null.EnVacio.TestPoint("NOMINAL_VOLTAGE").Min, Params.W.Null.EnVacio.TestPoint("NOMINAL_VOLTAGE").Max, 0, 0, ParamUnidad.W));
        //    consumptionMargins.Add(new AdjustValueDef(Params.VAR.Null.EnVacio.TestPoint("NOMINAL_VOLTAGE").Name, 0, Params.VAR.Null.EnVacio.TestPoint("NOMINAL_VOLTAGE").Min, Params.VAR.Null.EnVacio.TestPoint("NOMINAL_VOLTAGE").Max, 0, 0, ParamUnidad.Var));

        //    TestMeasureBase(consumptionMargins, (step) =>
        //    {
        //        var cvmMiniReadings = tower.CvmminiChroma.ReadAllVariables();

        //        return new double[] { cvmMiniReadings.Phases.L1.PotenciaActiva, cvmMiniReadings.Phases.L1.PotenciaReactiva };
        //    }, 2, 10, 1000);

        //    relayInformation.ActivateAll();
        //    smart.WriteRelaysInformation(relayInformation);

        //    consumptionMargins = new List<AdjustValueDef>();
        //    consumptionMargins.Add(new AdjustValueDef(Params.W.Null.ConCarga.TestPoint("NOMINAL_VOLTAGE").Name, 0, Params.W.Null.ConCarga.TestPoint("NOMINAL_VOLTAGE").Min, Params.W.Null.ConCarga.TestPoint("NOMINAL_VOLTAGE").Max, 0, 0, ParamUnidad.W));
        //    consumptionMargins.Add(new AdjustValueDef(Params.VAR.Null.ConCarga.TestPoint("NOMINAL_VOLTAGE").Name, 0, Params.VAR.Null.ConCarga.TestPoint("NOMINAL_VOLTAGE").Min, Params.VAR.Null.ConCarga.TestPoint("NOMINAL_VOLTAGE").Max, 0, 0, ParamUnidad.Var));

        //    TestMeasureBase(consumptionMargins, (step) =>
        //    {
        //        var cvmMiniReadings = tower.CvmminiChroma.ReadAllVariables();

        //        return new double[] { cvmMiniReadings.Phases.L1.PotenciaActiva, cvmMiniReadings.Phases.L1.PotenciaReactiva };
        //    }, 2, 10, 1000);

        //    relayInformation.DeactivateAll();
        //    smart.WriteRelaysInformation(relayInformation);
        //}

        //public void TestCommunications()
        //{
        //    var firmwareVersion = smart.ReadFirmwareVersion();

        //    Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, "Error al comprobar la versión de firmware del equipo. La versión leída no corresponde con la esperada, porfavor regrabar el equipo", ParamUnidad.SinUnidad);
        //}

        //public void TestLedsAndDisplay()
        //{
        //    smart.WriteLeds(false, false);
        //    smart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.NONE);

        //    TestUserInterface.ShowDialogWithImage(this, "Interaccion del usuario - Pruebas Display y leds", "Comprobar que el display y los leds estan apagados", 
        //        string.Format("{0}\\{1}\\{2}", PathCameraImages.TrimEnd('\\'), smart.GetType().Name, "LEDS_DISPLAYS_OFF"), 
        //        System.Windows.Forms.MessageBoxButtons.YesNo, 
        //        System.Windows.Forms.DialogResult.Yes);

        //    smart.WriteLeds(false, true);
        //    smart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.RIGHT_PART);

        //    TestUserInterface.ShowDialogWithImage(this, "Interaccion del usuario - Pruebas Display y leds", "Comprobar que el display y los leds estan apagados",
        //        string.Format("{0}\\{1}\\{2}", PathCameraImages.TrimEnd('\\'), smart.GetType().Name, "LED_2_DISPLAY_RIGHT"),
        //        System.Windows.Forms.MessageBoxButtons.YesNo,
        //        System.Windows.Forms.DialogResult.Yes);

        //    smart.WriteLeds(true, false);
        //    smart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.LEFT_PART);

        //    TestUserInterface.ShowDialogWithImage(this, "Interaccion del usuario - Pruebas Display y leds", "Comprobar que el display y los leds estan apagados",
        //        string.Format("{0}\\{1}\\{2}", PathCameraImages.TrimEnd('\\'), smart.GetType().Name, "LED_1_DISPLAY_LEFT"),
        //        System.Windows.Forms.MessageBoxButtons.YesNo,
        //        System.Windows.Forms.DialogResult.Yes);

        //    smart.WriteLeds(false, false);
        //    smart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.NONE);
        //}

        //public void TestDefaultParameters()
        //{
        //    COMPUTERSMART.IdpInformation idpInformation = new COMPUTERSMART.IdpInformation();
        //    idpInformation.FrameNumber = (int)TestInfo.NumBastidor;
        //    smart.WriteIDPRegistersInformation(idpInformation);

        //    COMPUTERSMART.SetupInfo setupInformation = new COMPUTERSMART.SetupInfo();
        //    setupInformation.DefaultInfo();
        //    setupInformation.Paso = (ushort)Configuracion.GetDouble("PASO", 6, ParamUnidad.SinUnidad);
        //    smart.WriteSetupInformation(setupInformation);

        //    COMPUTERSMART.SetupLimitsInfo setupLimitsInformation = new COMPUTERSMART.SetupLimitsInfo();
        //    setupLimitsInformation.DefaultInformation();
        //    setupLimitsInformation.PasoMaximo = (byte)Configuracion.GetDouble("PASO_MAXIMO", 6, ParamUnidad.SinUnidad);
        //    setupLimitsInformation.CalibracionMedidaV1 = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_V", 0x08A4, ParamUnidad.SinUnidad);
        //    setupLimitsInformation.CalibracionMedidaV2 = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_V", 0x08A4, ParamUnidad.SinUnidad);
        //    setupLimitsInformation.CalibracionMedidaV3 = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_V", 0x08A4, ParamUnidad.SinUnidad);
        //    setupLimitsInformation.CalibracionMedidaPotenciaActiva= (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_POTENCIA", 0x018B, ParamUnidad.SinUnidad);
        //    setupLimitsInformation.CalibracionMedidaPotenciaReactiva = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_POTENCIA", 0x018B, ParamUnidad.SinUnidad);
        //    smart.WriteSetupLimitsInformation(setupLimitsInformation);

        //    COMPUTERSMART.AlarmsInfo alarmsInfo = new COMPUTERSMART.AlarmsInfo();
        //    alarmsInfo.DefaultInformation();
        //    smart.WriteAlarmInformation(alarmsInfo);
        //}

        //public void TestRelays()
        //{
        //    var relayInfo = new COMPUTERSMART.RelayInformation();
        //    var impairRelaysInputs = new int[] { IN_RELAY_ALARM_NO, IN_RELAY_1, IN_RELAY_3, IN_RELAY_5, IN_RELAY_7, IN_RELAY_9, IN_RELAY_11 };
        //    var pairRelaysInputs = new int[] { IN_RELAY_ALARM_NC, IN_RELAY_2, IN_RELAY_4, IN_RELAY_6, IN_RELAY_8, IN_RELAY_10, IN_RELAY_12 };

        //    relayInfo.DeactivateAll();
        //    smart.WriteRelaysInformation(relayInfo);

        //    Delay(500, "Esperando activacion de los reles");

        //    if (!tower.IO.DI.ReadAllOff(impairRelaysInputs) && !tower.IO.DI.ReadAllOff(pairRelaysInputs))
        //        throw new Exception("Error en la detección del estado inicial de los reles.");

        //    relayInfo.ActivateImpairs();
        //    smart.WriteRelaysInformation(relayInfo);

        //    Delay(500, "Esperando activacion de los reles");

        //    if (!tower.IO.DI.ReadAllOn(impairRelaysInputs))
        //        throw new Exception("Error al detectar los reles impares activos.");
        //    if (!tower.IO.DI.ReadAllOff(pairRelaysInputs))
        //        throw new Exception("Error al detectar los reles pares inactivos.");

        //    relayInfo.DeactivateAll();
        //    relayInfo.ActivatePairs();
        //    smart.WriteRelaysInformation(relayInfo);

        //    Delay(500, "Esperando activacion de los reles");

        //    if (!tower.IO.DI.ReadAllOff(impairRelaysInputs))
        //        throw new Exception("Error al detectar los reles impares inactivos.");
        //    if (!tower.IO.DI.ReadAllOn(pairRelaysInputs))
        //        throw new Exception("Error al detectar los reles pares activos.");

        //    relayInfo.DeactivateAll();
        //    relayInfo.ActivateAll();
        //    smart.WriteRelaysInformation(relayInfo);

        //    Delay(500, "Esperando activacion de los reles");

        //    if (!tower.IO.DI.ReadAllOn(impairRelaysInputs))
        //        throw new Exception("Error al detectar los reles impares activos.");
        //    if (!tower.IO.DI.ReadAllOn(pairRelaysInputs))
        //        throw new Exception("Error al detectar los reles pares activos.");

        //    relayInfo.DeactivateAll();
        //    smart.WriteRelaysInformation(relayInfo);
        //}

        //public void TestTeclado()
        //{
        //    foreach (var item in keyboardPistonDictionary)
        //    {
        //        if (!item.Key.Equals(9999))
        //            tower.IO.DO.OnWait(200, item.Key);

        //        var keyboardReading = smart.ReadKeyboard();

        //        Assert.AreEqual(item.Value, keyboardPistonDictionary, "Error al comprobar el teclado, la tecla {0} no se detecta al pulsarla o se detectan otras teclas pulsadas");

        //        if (!item.Key.Equals(9999))
        //            tower.IO.DO.OffWait(200, item.Key);
        //    }
        //}

        //public void TestFinish()
        //{
        //    if (smart != null)        
        //        smart.Dispose();    

        //    if (tower != null)
        //    {
        //        tower.ShutdownSources();
        //        tower.Dispose();
        //    }
        //}

        //public Dictionary<int, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys> keyboardPistonDictionary = new Dictionary<int, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys>()
        //{
        //    {9999, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys.NONE},
        //    {17, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys.UP},
        //    {18, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys.LEFT},
        //    {19, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys.SETUP},
        //    {20, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys.DOWN},
        //    {21, Dezac.Device.Reactive.COMPUTERSMART.KeyboardKeys.RIGHT},
        //};
    }
}
