﻿using Dezac.Core.Exceptions;
using Dezac.Services;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner.Model;
namespace Dezac.Tests.Actions
{
    [ActionVersion(1.09)]
    [DesignerAction(Type = typeof(GetNumSerieDescription))]
    public class GetNumSerie : ActionBase
    {
        private static object lockObj = new object();

        public override void Execute()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                RunTestSequentiallyOrdered("_GETNUMSERIE_", InternalExecute);
            else
                InternalExecute();
        }

        private void InternalExecute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            string numSerie = null;

            if (data != null && data.TestInfo != null)
            {
                if (data.modoPlayer == ModoTest.PostVenta)
                    numSerie = ControlPostventaNumSerie(data);

                if (string.IsNullOrEmpty(numSerie))
                {
                    if (!string.IsNullOrEmpty(data.TestInfo.NumSerie) && !data.IsReproceso)
                    {
                        Logger.InfoFormat("TestInfo.NumSerie: {0}", data.TestInfo.NumSerie);
                        var numSerieInterno = new string(data.TestInfo.NumSerie.ToCharArray().Where(c => Char.IsDigit(c)).ToArray());
                        data.TestInfo.NumSerieInterno = numSerieInterno.Trim();
                        Logger.InfoFormat("BBDD Oracle -> Numero Serie Interno: {0}", numSerieInterno);
                        return;
                    }
                    using (DezacService svc = new DezacService())
                    {
                        if ((!data.NumOrden.HasValue || data.NumOrden == 0) && data.NumProducto != 0)
                            numSerie = svc.GetNumSerie(data.NumProducto, data.Version, 0);
                        else
                        {
                            if (data.modoPlayer == ModoTest.Normal && data.Configuracion.GetString("NUMERO_SERIE_POR_CAJAS", "SI", ParamUnidad.SinUnidad).Trim() == "SI")
                            {
                                if (!data.MantenerNumSerie)
                                    numSerie = svc.GetNumSerieReservaByBox(data.NumProducto, data.Version, data.NumOrden.Value, data.UdsCajaActual == 0 ? data.CantidadPendiente : data.UdsCajaActual);
                                else
                                    numSerie = svc.SetReservaBoxByNumSerie(data.NumProducto, data.Version, data.NumOrden.Value, data.UdsCajaActual == 0 ? data.CantidadPendiente : data.UdsCajaActual, data.TestInfo.NumSerie);
                            }
                            else
                                numSerie = svc.GetNumSerie(data.NumProducto, data.Version, data.NumOrden.Value);
                        }
                    }
                }

                if (string.IsNullOrEmpty(numSerie))
                    Error().SOFTWARE.BBDD.CONSULTA_PGI("NUMERO SERIE NO EXISTE PARA ESTA ORDEN O PRODUCTO").Throw();

                var numSerieClean = new string(numSerie.ToCharArray().Where(c => Char.IsDigit(c)).ToArray());
                Logger.InfoFormat("BBDD Oracle -> Numero Serie Interno: {0}", numSerieClean);
                data.TestInfo.NumSerieInterno = numSerieClean.Trim();
            
                data.TestInfo.NumSerie = numSerie.Trim();
                Logger.InfoFormat("TestInfo.NumSerie: {0}", data.TestInfo.NumSerie);

                using (DezacService svc = new DezacService())
                {
                    if (data.NumOrden.HasValue && data.NumOrden != 0)
                    {
                        data.IdCajaActual = svc.GetIDCajaActual(data.NumOrden.Value);
                        data.Lote = svc.GetLoteCaja(data.IdCajaActual);
                    }
                }
            }
            else
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestConext").Throw(); 
        }

        private string ControlPostventaNumSerie(ITestContext data)
        {
            var context = SequenceContext.Current;
            var shell = context.Services.Get<IShell>();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            string msg = "";
            string numSerie = "";

            if (String.IsNullOrEmpty(data.NumSerieAsistencia))
            {
                msg = "ASIGNAR NUMERO DE SERIE MANUAL (SI) - ASIGNAR NUEVO NUMERO DE SERIE (NO)";

                var result1 = shell.MsgBox(msg, string.Format("ASISTENCIA TÉCNICA {0}/{1}", data.NumAsistencia, data.NumLineaAsistencia), MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                    numSerie = testBase.GetIntroNumSerieByUser();
                else if (result1 == DialogResult.No)
                    numSerie = "";
            }
            else
            {
                msg = string.Format("ASIGNAR NUMERO DE SERIE MANUAL (SI) - ASIGNAR NUEVO NUMERO DE SERIE (NO) - MANTENER EL NUMERO DE SERIE {0} (CANCELAR)", data.NumSerieAsistencia);

                var result = shell.MsgBox(msg, string.Format("ASISTENCIA TÉCNICA {0}/{1}", data.NumAsistencia, data.NumLineaAsistencia), MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                    numSerie = testBase.GetIntroNumSerieByUser();
                else if (result == DialogResult.No)
                    numSerie = "";
                else if (result == DialogResult.Cancel)
                    numSerie = data.NumSerieAsistencia;
            }

            return numSerie;
        }
    }

    public class GetNumSerieDescription : ActionDescription
    {
        public override string Description { get { return "Action para coger el numero de serie de un producto y versión"; } }
        public override string Name { get { return "GetNumSerie"; } }
        public override string Category { get { return "DezacTest/Oracle/SerialNumber"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
        public override string Dependencies { get { return "InitTestBase;InitProductAction;InitBastidorMultipleAction"; } }
    }
}
