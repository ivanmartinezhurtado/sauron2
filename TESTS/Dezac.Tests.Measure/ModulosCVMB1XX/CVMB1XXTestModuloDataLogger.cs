﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.09)]
    public class CVMB1XXTestModuloDataLogger : CVMB1XXTestModulosGenerico
    {
        public void TestInitialization(byte portCVM)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            CVMDummy = AddInstanceVar(new CVMB1XX(portCVM), "UUT");
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", CVMDummy.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", CVMDummy.GetType().Name), ParamUnidad.SinUnidad);
        }

        public void TestDataLogger()
        {

            //CVMDummy = AddInstanceVar(new CVMB1XX(7), "UUT");
            var mac = CVMDummy.ReadModuleMac();
            Resultado.Set("MAC", mac.ToString(), ParamUnidad.SinUnidad);

            var IP_asigned = GetVariable("IP_DATALOGGER");

            var ipConfig = CVMDummy.ReadModuleIPConfig();
            Assert.AreEqual("IP_ASIGNADA", ipConfig.IP, IP_asigned, Error().SOFTWARE.ERROR_TRANSFERENCIA.CONFIGURACION_INCORRECTA("Error. IP asignada y leida incorrectas"), ParamUnidad.SinUnidad);

            var version = CVMDummy.ReadSoftwareVersion().CToNetString();

           Assert.AreEqual("VERSION_FIRMWARE", version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de Software incorrecta."), ParamUnidad.SinUnidad);

            var kernelVersion = CVMDummy.ReadKernelVersion().CToNetString();

            Assert.AreEqual("VERSION_KERNEL", kernelVersion, Identificacion.VERSION_EMBEDDED, Error().SOFTWARE.ERROR_TRANSFERENCIA.VERSION_INCORRECTA("Error. Version de Kernel incorrecta."), ParamUnidad.SinUnidad);

            var powerStudioVersion = CVMDummy.ReadPowerStudioVersion().CToNetString();

            Assert.AreEqual("POWER_STUDIO_VERSION", powerStudioVersion, Identificacion.VERSION_POWER_STUDIO, Error().SOFTWARE.ERROR_TRANSFERENCIA.VERSION_INCORRECTA("Error. Version de PowerStudio incorrecta."), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (CVMDummy != null)
                CVMDummy.Dispose();
        }
    }
}
