﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Core.Utility
{
    public enum ChecksumTypeEnum
    {
        NotDefined = 0,
        Decimal_Mod100,
        Hexadecimal_Mod256,
    }
}
