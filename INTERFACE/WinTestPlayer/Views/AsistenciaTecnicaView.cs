﻿using Dezac.Data;
using Dezac.Tests.UserControls;
using System.Data;
using System.Linq;
using WinTestPlayer.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class AsistenciaTecnicaView : DialogViewBase, IFocusable
    {
        protected readonly AppService app;
        protected readonly ShellService shell;
        protected readonly int NumProducto;

        public AsistenciaTecnicaView(AppService app, ShellService shell, int numProducto)
        {
            InitializeComponent();
            NumProducto = numProducto;
            this.app = app;
            this.shell = shell;
            UpdateUI();
        }

        private void UpdateUI()
        {
            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                var list = db.ASISTENCIATECNICA
                    .Include("ARTICULO")
                    .Include("PRODUCTO")
                    .Include("T_FAMILIA")
                    .Where(p => p.ARTICULO.PRODUCTO.Where(g => g.T_FAMILIA.PLATAFORMA == "TS2").Any() && p.ABIERTA == "S" && p.NUMPRODUCTO == NumProducto)
                    .GroupBy(g => new { g.NUMASISTENCIA, g.NUMPRODUCTO, g.NUMPEDIDO, g.NUMEROSERIE, g.ARTICULO.DESCRIPCION, g.FECHAFINPREV })
                    .Select(s => new AsistenciaTecnicaViewModel
                    {
                        NUMASISTENCIA = s.FirstOrDefault().NUMASISTENCIA,
                        NUMPRODUCTO = s.FirstOrDefault().NUMPRODUCTO,
                        NUMPEDIDO = s.FirstOrDefault().NUMPEDIDO,
                        NUMEROSERIE = s.FirstOrDefault().NUMEROSERIE,
                        FECHAFINPREV = s.FirstOrDefault().FECHAFINPREV,
                        DESCRIPCION = s.FirstOrDefault().ARTICULO.DESCRIPCION
                    })
                    .OrderByDescending(t => t.FECHAFINPREV)
                    .ToList();

                bsList.DataSource = list;
            }
        }

        public AsistenciaTecnicaViewModel Current
        {
            get
            {
                return bsList.Current as AsistenciaTecnicaViewModel;
            }
        }
    }
}
