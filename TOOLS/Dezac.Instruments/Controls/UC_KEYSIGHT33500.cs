﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Auxiliars;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(KeysightDescription))]
    public partial class UC_KEYSIGHT33500 : UserControl
    {
        private KeySight33500 generator = new KeySight33500();

        public Type InstrumentType
        {
            get
            {
                return typeof(KeySight33500);
            }
        }

        public UC_KEYSIGHT33500()
        {
            try
            {
                InitializeComponent();

                // cmb_Addres.Items.AddRange(computer.SerialPortNames.ToArray());
                // cmb_Addres.SelectedIndex = cmb_Addres.FindStringExact("COM" + generator.portAddress);
                txtAddres.Text = generator.portAddress.ToString();

                String[] SendType = new String[] { "V & Offset", "Vmax & Vmin" };
                cmb_SendType.Items.AddRange(SendType);
                cmb_SendType.SelectedIndex = 0;

                foreach (var item in Enum.GetValues(typeof(SignalFunction)))
                    cmb_SignalForm.Items.Add(item);
                cmb_SignalForm.SelectedIndex = 0;

                String[] Channels = new String[] { "1", "2" };
                cmb_Channel.Items.AddRange(Channels);
                cmb_Channel.SelectedIndex = 0;

                String[] impedancia = new String[] { "Infinity", "50" };
                cmbImpedance.Items.AddRange(impedancia);
                cmbImpedance.SelectedIndex = 0;

                String[] unidadFrec = new String[] { "uHz", "mHz", "Hz", "KHz", "MHz" };
                cmbFrec.Items.AddRange(unidadFrec);
                cmbFrec.SelectedIndex = 2;

                String[] unidadVolt = new String[] { "mV", "V" };
                cmbOffset.Items.AddRange(unidadVolt);
                cmbOffset.SelectedIndex = 1;
                cmbVmin.Items.AddRange(unidadVolt);
                cmbVmin.SelectedIndex = 1;
                cmbVmax.Items.AddRange(unidadVolt);
                cmbVmax.SelectedIndex = 1;

                String[] unidadAmplitude = new String[] { "mVpp", "Vpp", "mVrms", "Vrms" };
                cmbAmplitude.Items.AddRange(unidadAmplitude);
                cmbAmplitude.SelectedIndex = 1;
            }
            catch(Exception ex)
            {
                Logger.Error("Se ha producido un error al intentar comunicar con el KeySight3350", ex);
            }        
        }

        private void OnKeySight(object sender, EventArgs e)
        {
            try
            {
                generator.On(Convert.ToInt32(cmb_Channel.SelectedItem));
                if (cmbImpedance.Text.Equals(string.Empty))
                    cmbImpedance.SelectedIndex = 0;
                generator.SetOutputLoad(Convert.ToInt32(cmb_Channel.SelectedItem), cmbImpedance.Text);
                //cmb_Channel.Enabled = false;
            }
            catch (Exception ex)
            {
                Logger.Error("Error al encender el KeySight", ex);
            }
        }

        private void OffKeySight(object sender, EventArgs e)
        {
            try
            {
                generator.Off(Convert.ToInt32(cmb_Channel.SelectedItem));
                //cmb_Channel.Enabled = true;
            }
            catch(Exception ex)
            {
                Logger.Error("Error al apagar el KeySight", ex);
            }
        }

        private void ResetKeySight(object sender, EventArgs e)
        {
            try
            {
                generator.Reset();
            }
            catch (Exception ex)
            {
                Logger.Error("Error al resetear el KeySight", ex);
            }
        }

        private void selectionTypeInputVoltage(object sender, EventArgs e)
        {
            txtVmin.Text = string.Empty;
            txtVmax.Text = string.Empty;
            txtAmplitude.Text = string.Empty;
            txtVoffset.Text = string.Empty;

            ComboBox cb = (ComboBox)sender;
            switch(cb.SelectedIndex)
            {
                case 0:
                    txtVmin.Enabled = false;
                    txtVmax.Enabled = false;
                    txtAmplitude.Enabled = true;
                    txtVoffset.Enabled = true;
                    break;
                case 1:
                    txtAmplitude.Enabled = false;
                    txtVoffset.Enabled = false;
                    txtVmin.Enabled = true;
                    txtVmax.Enabled = true;
                    break;
            }
        }
        private void selectionWaveform(object sender, EventArgs e)
        {
            cmb_SendType.Enabled = true;
            txtAmplitude.Enabled = true;
            txtAmplitude.Text = string.Empty;
            txtDutyCycle.Enabled = true;
            txtDutyCycle.Text = string.Empty;
            txtFrequency.Enabled = true;
            txtFrequency.Text = string.Empty;
            txtPhase.Enabled = true;
            txtPhase.Text = string.Empty;
            txtVmax.Enabled = true;
            txtVmax.Text = string.Empty;
            txtVmin.Enabled = true;
            txtVmin.Text = string.Empty;
            txtVoffset.Enabled = true;
            txtVoffset.Text = string.Empty;

            if (cmb_SendType.SelectedItem.ToString() == "V & Offset")
            {
                txtVmin.Enabled = false;
                txtVmax.Enabled = false;
            }
            else
            {
                txtVoffset.Enabled = false;
                txtAmplitude.Enabled = false;
            }

            ComboBox cb = (ComboBox)sender;
            switch(cb.SelectedItem.ToString())
            {
                case "SINUSOIDAL":                   
                    txtDutyCycle.Enabled = false;
                    break;
                case "SQUARE":                  
                    break;
                case "TRIANGULAR":                   
                    txtDutyCycle.Enabled = false;
                    break;
                case "RAMP":                 
                    txtDutyCycle.Enabled = false;
                    break;
                case "PULSE":                  
                    break;
                case "NRAMP":                  
                    break;
                case "NOISE":
                    txtDutyCycle.Enabled = false;                   
                    txtFrequency.Enabled = false;
                    txtPhase.Enabled = false;
                    break;
                case "PRBS":                                    
                    txtDutyCycle.Enabled = false;
                    break;
                case "ARBITRARY":
                    txtDutyCycle.Enabled = false;                 
                    txtPhase.Enabled = false;
                    txtFrequency.Enabled = false;
                    break;
                case "DC":
                    cmb_SendType.Enabled = false;
                    txtFrequency.Enabled = false;
                    txtDutyCycle.Enabled = false;
                    txtAmplitude.Enabled = false;
                    txtVmin.Enabled = false;
                    txtVmax.Enabled = false;
                    txtPhase.Enabled = false;
                    txtVoffset.Enabled = true;
                    break;
            }        
        }

        private void ApplySignal(object sender, EventArgs e)
        {
            bool detectedKeySight = true;
            try
            {
                if (cmbImpedance.Text.Equals(string.Empty))
                    cmbImpedance.SelectedIndex = 0;
                generator.SetOutputLoad(Convert.ToInt32(cmb_Channel.SelectedItem), cmbImpedance.Text);
            }
            catch(Exception)
            {
                Logger.Error("Error al aplicar señal al generador de funciones", new Exception());
                detectedKeySight = false;
            }
            if (detectedKeySight)
                try
                {
                    SignalInfo signal = new SignalInfo();

                    signal.Channel = Convert.ToInt32(cmb_Channel.SelectedItem);
                    signal.Function = (SignalFunction)cmb_SignalForm.SelectedIndex;

                    if (txtFrequency.Enabled)
                        switch (cmbFrec.SelectedItem.ToString())
                        {
                            case "Hz":
                                signal.Frequency = Convert.ToDouble(txtFrequency.Text);
                                break;
                            case "KHz":
                                signal.Frequency = (Convert.ToDouble(txtFrequency.Text)) * 1000;
                                break;
                            case "MHz":
                                signal.Frequency = (Convert.ToDouble(txtFrequency.Text)) * 1000000;
                                break;
                            case "uHz":
                                signal.Frequency = (Convert.ToDouble(txtFrequency.Text)) / 1000000;
                                break;
                            case "mHz":
                                signal.Frequency = (Convert.ToDouble(txtFrequency.Text)) / 1000;
                                break;
                        }

                    if (txtVoffset.Enabled)
                        switch (cmbOffset.SelectedItem.ToString())
                        {
                            case "V":
                                signal.VoltageOffset = Convert.ToDouble(txtVoffset.Text);
                                break;
                            case "mV":
                                signal.VoltageOffset = (Convert.ToDouble(txtVoffset.Text)) / 1000;
                                break;
                        }
                    if (txtAmplitude.Enabled)
                        switch (cmbAmplitude.SelectedItem.ToString())
                        {
                            case "Vpp":
                                signal.UnitVoltage = UnitVoltage.VPP;
                                signal.Amplitude = Convert.ToDouble(txtAmplitude.Text);
                                break;
                            case "mVpp":
                                signal.UnitVoltage = UnitVoltage.VPP;
                                signal.Amplitude = (Convert.ToDouble(txtAmplitude.Text)) / 1000;
                                break;
                            case "Vrms":
                                signal.UnitVoltage = UnitVoltage.VRMS;
                                signal.Amplitude = (Convert.ToDouble(txtAmplitude.Text));
                                break;
                            case "mVrms":
                                signal.UnitVoltage = UnitVoltage.VRMS;
                                signal.Amplitude = (Convert.ToDouble(txtAmplitude.Text)) / 1000;
                                break;
                        }
                    if (txtVmin.Enabled)
                        switch (cmbVmin.SelectedItem.ToString())
                        {
                            case "V":
                                signal.MinVoltage = Convert.ToDouble(txtVmin.Text);
                                break;
                            case "mV":
                                signal.MinVoltage = (Convert.ToDouble(txtVmin.Text)) / 1000;
                                break;
                        }
                    if (txtVmax.Enabled)
                        switch (cmbVmax.SelectedItem.ToString())
                        {
                            case "V":
                                signal.MaxVoltage = Convert.ToDouble(txtVmax.Text);
                                break;
                            case "mV":
                                signal.MaxVoltage = (Convert.ToDouble(txtVmax.Text)) / 1000;
                                break;
                        }
                    if (txtPhase.Enabled)
                        signal.Phase = Convert.ToDouble(txtPhase.Text);
                    if (txtDutyCycle.Enabled)
                        signal.DutyCycle = Convert.ToDouble(txtDutyCycle.Text);

                    generator.SetSignal(signal, true);

                }
                catch (Exception)
                {
                    MessageBox.Show("Parametros incorrectos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }
    }
    public class KeysightDescription : ControlDescription
    {
        public override string Name { get { return "Waveform generador Keysight 33500"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Apps_esd_icon; } }
        public override Image IconBig { get { return Properties.Resources.KEYSIGHT_SG; } }
        public override Type SubcomponentType { get { return typeof(KeySight33500); } }
        public override bool Visible { get { return false; } }
    }
}
