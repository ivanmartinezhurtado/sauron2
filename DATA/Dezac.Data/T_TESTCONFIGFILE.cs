namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTCONFIGFILE")]
    public partial class T_TESTCONFIGFILE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_TESTCONFIGFILE()
        {
            T_TESTCONFIGFILEVERSION = new HashSet<T_TESTCONFIGFILEVERSION>();
            T_TESTCONFIGFILEPRODUCTO = new HashSet<T_TESTCONFIGFILEPRODUCTO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTCF { get; set; }

        public int NUMFAMILIA { get; set; }

        [StringLength(10)]
        public string IDTIPODOC { get; set; }

        [StringLength(255)]
        public string NOMBREFICHERO { get; set; }

        [StringLength(512)]
        public string DESCRIPCION { get; set; }

        [StringLength(512)]
        public string CLAVE { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(15)]
        public string USUARIOALTA { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTCONFIGFILEVERSION> T_TESTCONFIGFILEVERSION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTCONFIGFILEPRODUCTO> T_TESTCONFIGFILEPRODUCTO { get; set; }
    }
}
