using Comunications;
using Comunications.Ethernet;
using Dezac.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Dezac.Core.Utility;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(TftpClientUploadFileDescription))]
    public class TftpClientUploadFile : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Fichero que se ha de subir al servidor")]
        public string UploadFile { get; set; }

        [Category("3. Configuration")]
        [Description("Camino del cliente")]
        public string ClientPath { get; set; }

        [Category("3. Configuration")]
        [Description("Direcci�n IP del servidor")]
        public string ServerIP { get; set; }

        public override void Execute()
        {
            IPAddress ipClient;
            TFTPClient tftpClient;

            if (string.IsNullOrEmpty(ServerIP))
            {
                if (Context.Variables.ContainsKey("IPClienTFTPtDevice"))
                {
                    var ip = Context.Variables.Get("IPClienTFTPtDevice").ToString().Split('.').Select((s) => Convert.ToByte(s)).ToArray();
                    ipClient = new IPAddress(ip);
                    tftpClient = new TFTPClient(ipClient);
                }
                else
                    tftpClient = new TFTPClient();
            }
            else
            {
                ipClient = IPAddress.Parse(ServerIP);
                tftpClient = new TFTPClient(ipClient);
            }

            if (String.IsNullOrEmpty(UploadFile))
            {
                if (!Context.Variables.ContainsKey("BinaryItemViewModel"))
                    Error().SOFTWARE.SECUENCIA_TEST.FICHERO_NO_ENCONTRADO(string.Format("No se ha descargado ningun fichero ni se ha rellenado el campo 'UploadFile'", Path.Combine(ClientPath, UploadFile))).Throw();

                BinaryItemViewModel fileVersion = Context.Variables.Get("BinaryItemViewModel") as BinaryItemViewModel;
                ClientPath = ConfigurationManager.AppSettings["PathFileBinary"];
                UploadFile = fileVersion.NombreFichero;
            }

            if (!File.Exists(Path.Combine(ClientPath, UploadFile)))
                Error().SOFTWARE.SECUENCIA_TEST.FICHERO_NO_ENCONTRADO(string.Format("No exite el fichero {0} para enviar por TFTP", Path.Combine(ClientPath, UploadFile))).Throw();

            tftpClient.StartUpload(ClientPath, UploadFile);

            while (!tftpClient.TransferSucces && !tftpClient.TransferError)
                Thread.Sleep(100);

            if (tftpClient.TransferError)
                Error().UUT.COMUNICACIONES.ERROR_UPLOAD(tftpClient.TftpTransferError.ToString()).Throw();
        }

        public class TftpClientUploadFileDescription : ActionDescription
        {
            public override string Name { get { return "TFTPClientUploadFile"; } }
            public override string Category { get { return "Tools/Ethernet/TFTP"; } }
            public override string Description { get { return "Action que sirve para que el cliente suba un fichero en el servidor TFTP"; } }
            public override Image Icon { get { return Properties.Resources.ftp_icon; } }
        }
    }
}
