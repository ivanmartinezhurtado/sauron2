﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.IO;
using System.Text;
using TaskRunner.Utils;

namespace TaskRunner.Model
{
    public class SequenceModel
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public Step Root { get; protected set; }
        internal SequenceModel()
        {
            Root = new Step();
        }
        public static SequenceModel CreateNew()
        {
            SequenceModel model = new SequenceModel();
            model.Root.Steps.Add(new Step { Name = "Init" });
            model.Root.Steps.Add(new Step { Name = "Main" });
            model.Root.Steps.Add(new Step { Name = "End" });
            return model;
        }

        [JsonIgnore]
        public Step Init
        {
            get
            {
                return Root.Steps[0];
            }
        }

        [JsonIgnore]
        public Step Main
        {
            get
            {
                return Root.Steps[1];
            }
        }

        [JsonIgnore]
        public Step End
        {
            get
            {
                return Root.Steps[2];
            }
        }

        public void Save(string fileName)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;

            var json = JsonConvert.SerializeObject(this, settings);

            Name = fileName;

            if (fileName.IndexOf(".") < 0)
                fileName += ".json";

            File.WriteAllText(fileName, json);
        }

        public void Save(Stream stream, Encoding encoding = null)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;

            var json = JsonConvert.SerializeObject(this, settings);

            var data = (encoding ?? UTF8Encoding.UTF8).GetBytes(json);
            stream.Write(data, 0, data.Length);
        }

        public static SequenceModel LoadFile(string fileName)
        {
            string json = File.ReadAllText(fileName);

            return Load(json);
        }

        public static SequenceModel Load(string content)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Formatting = Formatting.Indented;

            return JsonConvert.DeserializeObject<SequenceModel>(content, settings);
        }
    }

    public enum PostActionStep
    {
        Next = 0,
        Terminate = 4
    }

    public enum LoopType
    {
        None = 0,
        OnFail = 1,
        OnSuccess = 2,
        Always = 4,
        OnCondition = 5
    }

    public enum LoopResult
    {
        LastResult = 0,
        Percentage = 1
    }

    public enum TestParallelismBehaviour
    {
        None = 0,
        RunOnlyOnce = 1,
        WaitAllAndRunOnlyOnce = 3,
        WaitAllAndRunSequentially = 4,
        RunOnlyLastTest = 5
    }

    public class Step
    {
        [Category("Step")]
        [ReadOnly(true)]
        [Description("Nombre del Step")]
        public string Name { get; set; }

        [Category("Step")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        [Description("Comentarios que se puden añadir al Step")]
        public string Comments { get; set; }

        [Category("Step")]
        [Description("Orden y nombre del grupo donde los resultados del Step se añadiran a un TestReport si se ha diseñado, (Ejemplo: 01,Verificación)")]
        public string ReportGroupName { get; set; }

        [Category("Step")]
        [Description("Para habilitar o deshabilitar el Action que no se ejecute (no recomendable mejor realizarlo por el Checbox de la sequencia de test del editor)")]
        public bool Enabled { get; set; }

        [Category("Run")]
        [Description("Ejecuta este Action Modo en el que podemos ejecutar este Step (Debug, Release or Testing) Debug y Testing es para pruebas y lo ejecuta el WinTaskRunner el modo Release es para que lo ejecute el WinTestPlayer")]
        public RunMode? RunMode { get; set; }
        [Category("Run")]
        [Description("Ejecuta este Action en la repeticion del test escpecificado (Ejemplo: si ponemos un 2 solo se ejecutra en el segundo test, antes y despues no se ejecutara pero si el segundo test falla antes de realizar el Action tampoco se ejecutara mas porque pasaremos al tercer test, si ponemos 0 lo ejecutara una vez solamente, sea el test que sea")]
        public int? RunOnTestNumber { get; set; }
        [Category("Run")]
        [Description("Ejecuta este Action solamente en el hilo que corresponda a la instancia del test, solo srive para multiples test en paralelismo")]
        public int? RunOnNumInstance { get; set; }
       
        [Category("Run")]
        [Description("Ejecuta todos los Actions hijos a la vez (en paralelo)")]
        public bool RunChildStepsInParallel { get; set; }

        [Category("Loop")]
        [Description("Tipo de bucle de repeticiones que se habilitara en el Action")]
        public LoopType LoopType { get; set; }
        [Category("Loop")]
        [Description("Numero de repeticiones que realizara el bucle antes de lanzar la excepcion")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string LoopMaxIterations { get; set; }
        [Category("Loop")]
        [Description("Tiempo en (ms) que pasará entre los reintentos del bucle")]
        public int TimeIntervalLoop { get; set; }
        [Category("Loop")]
        [Description("Expresion lógica booleana que se debe cumplir para seguir ejecutando el loop")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string LoopConditionExpr { get; set; }

        [Category("Condition")]
        [Description("Expresion lógica booleana que se debe cumplir para ejecutar el Action")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string RunConditionExpr { get; set; }

        [Browsable(false)]
        public IActionBase Action { get; set; }

        [Category("Step")]
        [Description("Acción que realizará el test si pasa este Action correctamente")]
        public PostActionStep PassAction { get; set; }
        [Category("Step")]
        [Description("Acción que realizará el test si falla este Action")]
        public PostActionStep FailAction { get; set; }

        [Category("Multi Test Synchronization")]
        [Description("Propiedad para poder sincronizar test en paralelo")]
        public TestParallelismBehaviour TestParallelismBehaviour { get; set; }

        [Browsable(false)]
        public string Path { get; set; }

        [Browsable(false)]
        public List<Step> Steps { get; private set; }

        [JsonIgnore]
        [Browsable(false)]
        internal int TotalCountOK { get; set; }

        [JsonIgnore]
        [Browsable(false)]
        public bool BreakPoint { get; set; }

        public Step()
        {
            Steps = new List<Step>();

            Enabled = true;

            PassAction = PostActionStep.Next;
            FailAction = PostActionStep.Terminate;
        }

        public bool HasStepName(string stepName)
        {
            foreach (var child in Steps)
            {
                if (child.Action != null)
                {
                    if (child.Action.Name == stepName)
                        return true;
                    else
                    {
                        if (child.HasStepName(stepName))
                            return true;
                    }                
                }
            }
            return false;
        }
    }

    public class StepResult
    {
        public StepResult()
        {
            ChildResults = new List<StepResult>();
        }

        public int ThreadId { get; internal set; }
        public string Name { get; internal set; }
        public int NumInstance { get; internal set; }

        [JsonIgnore]
        public Step Step { get; internal set; }

        public DateTime StartTime { get; internal set; }
        public DateTime EndTime { get; internal set; }
        public TimeSpan Duration { get { return EndTime - StartTime; } }

        public int Section { get; internal set; }

        public int LoopIndex { get; internal set; }
        [JsonIgnore]
        public int AbsoluteRunIndex { get; internal set; }
        //public int LoopNumPassed { get; internal set; }

        public Exception ExceptionResult { get; set; }

        public bool Succeed { get { return ExceptionResult == null; } }

        public string GetErrorMessage()
        {
            if (Succeed)
                return null;

            return ExceptionResult.Message;
        }

        [JsonIgnore]
        public List<StepResult> ChildResults { get; private set; }
        [JsonIgnore]
        internal StepResult ParentResult { get; set; }

        public IEnumerable<Model.StepResult> Flatten(Func<Model.StepResult, bool> include, Func<Model.StepResult, bool> includeChilds = null)
        {
            var list = new List<Model.StepResult>();

            AddSteps(list, this, include, includeChilds);

            return list;
        }

        private void AddSteps(List<Model.StepResult> list, Model.StepResult step, Func<Model.StepResult, bool> include, Func<Model.StepResult, bool> includeChilds)
        {
            if (step == null)
                return;

            if (include == null || include(step))
                list.Add(step);

            if (includeChilds == null || includeChilds(step))
                foreach (var item in step.ChildResults)
                    AddSteps(list, item, include, includeChilds);
        }

        public bool HasFaultedSteps()
        {
            if (!Succeed)
                return true;

            foreach (var child in ChildResults)
                if (child.HasFaultedSteps())
                    return true;

            return false;
        }
    }
}
