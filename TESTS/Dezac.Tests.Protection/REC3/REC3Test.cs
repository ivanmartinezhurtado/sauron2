﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.29)]
    public class REC3Test : TestBase
    {
        #region IN_OUT

        private const int IN_OUT_NC_RCB = 9;
        private const int IN_OUT_NO_RCB = 10;
        private const int IN_OUT_NC_RELE_BLOQ = 11;
        private const int IN_OUT_NO_RELE_BLOQ = 12;
        private const int IN_ESTADO_VN = 13;
        private const int IN_ESTADO_VL1 = 14;
        private const int IN_ESTADO_VL2 = 15;
        private const int IN_ESTADO_VL3 = 16;
        private const int IN_PRESENCIA_BORNES_DIGITALES = 17;
        private const int BLOQ_SECURITY = 23;
        private const int IN_DETECT_UUT = 24;
        private const int IN_DOOR_OPEN_UUT = 25;
        private const int IN_DETECT_PISTON_FIJACION_1 = 30;
        private const int IN_DETECT_PISTON_FIJACION_2 = 31;
        private const int IN_DETECT_PISTON_DOT_TEST_1 = 32;
        private const int IN_DETECT_PISTON_DOT_TEST_2 = 33;
        private const int IN_DETECT_PISTON_DOT_DIGITAL = 34;
        private const int IN_DETECT_PISTON_DOT_COMM = 35;

        private const int MUX_MEDIDA_RCC = 2;
        private const int MUX_MEDIDA_RDNI = 7;
        private const int MUX_MEDIDA_RD_NEUTRO = 9;
        private const int MUX_MEDIDA_RD_VL1 = 10;
        private const int MUX_MEDIDA_RD_VL2 = 11;
        private const int MUX_MEDIDA_RD_VL3 = 12;

        private const int OUT_PISTON_DOT_TEST = 6;
        private const int OUT_PISTON_FIJACION = 5;
        private const int OUT_PISTON_DOT_DIGITALES = 7;
        private const int OUT_PISTON_DOT_COMM = 8;
        private const int OUT_SELEC_REC_FUNCTION = 9;
        private const int OUT_PISTON_RECONECT_TEST = 10;
        private const int OUT_PUSH_TAPA = 11;
        private const int OUT_ACTIVA_230VCA = 15;
        private const int OUT_SELEC_RANGO_CALIB = 17;
        private const int OUT_ACTIVA_NEUTRO_CALIB = 18;
        private const int OUT_ALIMENTACION_RED = 19;
        private const int OUT_ACTIVATE_12VDC_N = 20;
        private const int OUT_ACTIVATE_12VDC_F = 21;
        private const int OUT_MODEL_RECHARGE = 22;
        private const int OUT_CONECTA_RCC_4R = 28;
        private const int OUT_CONECTA_RCC_0R = 29;
        private const int OUT_CONECTA_RL15_PIN_BOR6 = 30;
        private const int OUT_DESCONECTA_MODULO_CALIB = 43;
        private const int ERROR_LIGHT = 48;

        #endregion

        private Tower1 tower;

        private REC3 rec3;

        internal string CAMERA_IDS;

        private TypeDeviceREC3 typeREC3;

        public enum TypeDeviceREC3
        {
            RCB_2Polos = 0,
            RCB_4Polos = 1,
            RCB0 = 2,
        }

        public void TestInitialization([Description("0=RCB2P 1=RCB4P 2=RCBO")]TypeDeviceREC3 typeDeviceREC3, int port = 0)
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower1>("TOWER", () => { return new Tower1(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            typeREC3 = typeDeviceREC3;

            SetVariable("TypeDeviceREC3", typeREC3);

            if (Identificacion.MODELO_DIFERENCIAL != "0")
            {
                string barcode = "";
                SamplerWithCancel((p) =>
                {
                    var barcodeInput = new InputKeyBoard("Leer código de barras del diferencial");
                    Shell.ShowDialog("INICIO TEST", () => barcodeInput, MessageBoxButtons.OKCancel);

                    if (barcodeInput.TextValue.Length < Identificacion.MODELO_DIFERENCIAL.Length)
                    {
                        Shell.MsgBox("La longitud del diferencial del equipo no corresponde, debe de haberse leído el número de bastidor. Vuelva a leer el codigo diferencial", "LECTURA DEL CODIGO DE BARRAS DEL DIFERENCIAL INCORRECTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }

                    barcode = barcodeInput.TextValue;
                    return true;

                }, "Error no se ha leido el código de barras del diferencial");

                var codeDiferencial = barcode.ToCharArray();

                var codeDiferencialBBDD = Identificacion.MODELO_DIFERENCIAL.ToCharArray();

                if (typeREC3 == TypeDeviceREC3.RCB0)
                    Assert.AreEqual(ConstantsParameters.Identification.MODELO_DIFERENCIAL, barcode.Trim(), Identificacion.MODELO_DIFERENCIAL.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error. código del diferencial leido no coincide con el de la BBDD"), ParamUnidad.SinUnidad);
                else
                {
                    Resultado.Set(ConstantsParameters.Identification.MODELO_DIFERENCIAL, barcode, ParamUnidad.SinUnidad, Identificacion.MODELO_DIFERENCIAL);

                    ControlDiferencialCode(codeDiferencial, codeDiferencialBBDD);
                }
            }

            if (port == 0)
                port = Comunicaciones.SerialPort;

            rec3 = new REC3(port);

            rec3.Modbus.PerifericNumber = Comunicaciones.Periferico;

            ((ModbusDeviceSerialPort)rec3.Modbus).RtsEnable = true;

            ((ModbusDeviceSerialPort)rec3.Modbus).DtrEnable = true;
        }

        public void TestConsumption([Description("Tiempo espera estabilización consumo")]int timeWait = 2000)
        {
            tower.IO.DO.OffWait(200, OUT_ALIMENTACION_RED, OUT_ACTIVA_230VCA);

            tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;

            tower.PTE.Presets.Voltage = 230;

            tower.PTE.ApplyAndWaitStabilisation();

            Delay(timeWait, "Espera para la estabilización de la fuente");

            TestConsumo(Params.KW.Null.EnVacio.Vnom.Name, Params.KW.Null.EnVacio.Vnom.Min(), Params.KW.Null.EnVacio.Vnom.Max(), () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vnom fuera de margenes");

            TestConsumo(Params.KVAR.Null.EnVacio.Vnom.Name, Params.KVAR.Null.EnVacio.Vnom.Min(), Params.KVAR.Null.EnVacio.Vnom.Max(), () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vnom fuera de margenes");

            tower.IO.DO.OnWait(500, OUT_PUSH_TAPA);

            tower.IO.DO.PulseOn(500, OUT_SELEC_REC_FUNCTION);

            SamplerWithCancel((p) =>
            {
                try
                {
                    TestConsumo(Params.KW.Null.ConCarga.Vnom.Name, Params.KW.Null.ConCarga.Vnom.Min(), Params.KW.Null.ConCarga.Vnom.Max(), () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en con carga a Vnom fuera de margenes");

                    TestConsumo(Params.KVAR.Null.ConCarga.Vnom.Name, Params.KVAR.Null.ConCarga.Vnom.Min(), Params.KVAR.Null.ConCarga.Vnom.Max(), () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva con carga a Vnom fuera de margenes");

                    return true;
                }
                catch
                {
                    tower.PTE.ApplyOffAllAndWaitStabilisation();

                    Delay(2000, "Espera apagado del equpo");

                    tower.PTE.ApplyAndWaitStabilisation();

                    throw;
                }
            }, "", 4, timeWait, timeWait, false, false);

            tower.PTE.ApplyOffAllAndWaitStabilisation();

        }

        public void TestComunications()
        {
            tower.IO.DO.OnWait(2000, OUT_ALIMENTACION_RED);

            tower.IO.DO.OnWait(2000, OUT_ACTIVA_230VCA);

            //tower.IO.DO.OnWait(200, OUT_ALIMENTACION_RED, OUT_ACTIVA_230VCA);

            ushort version = 0;

            SamplerWithCancel((s) =>
            {
                if (s % 2 == 1)
                {
                    tower.IO.DO.OffWait(2000, OUT_PISTON_DOT_TEST);
                    tower.IO.DO.OnWait(500, OUT_PISTON_DOT_TEST);
                }
                version = rec3.ReadFimrwareVersion();
                return true;
            }, "Error. No se puede establecer comunicación con el equipo.", 5, 500);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware grabada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteErrorCode(255);

            rec3.WriteBastidor((int)TestInfo.NumBastidor);

            Delay(500, "Escritura del nuemro de bastidor al equipo");

            SetupDefault();
        }

        public void TestInternalTension()
        {
            var defs = new List<AdjustValueDef> {
                new AdjustValueDef { Name= Params.V.Other("3").Verificacion.Name, Min=Params.V.Other("3").Verificacion.Min(), Max =  Params.V.Other("3").Verificacion.Max(), Unidad= ParamUnidad.V},
                new AdjustValueDef { Name= Params.V.Other("12").Verificacion.Name, Min=Params.V.Other("12").Verificacion.Min(), Max =  Params.V.Other("12").Verificacion.Max(), Unidad= ParamUnidad.V} };

            TestMeasureBase(defs,
            (step) =>
            {
                var v3 = rec3.ReadValueMeasureV3();
                var v12 = rec3.ReadValueV12();

                return new double[] { v3, v12 };

            }, 1, 5, 600);
        }

        public void TestLeds()
        {
            CAMERA_IDS = GetVariable<string>("CAMERA_IDS", CAMERA_IDS);

            var timeDelay = 1000; 

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteOutputs(REC3.OuputState.AllOff);

            rec3.WriteOutputs(REC3.OuputState.RedLed);

            Delay(timeDelay, "Espera encenddo de LEDS");

            this.TestHalconFindLedsProcedure(CAMERA_IDS, "REC3_RED", "RED", "REC3");

            rec3.WriteOutputs(REC3.OuputState.GreenLed);

            Delay(timeDelay, "Espera encenddo de LEDS");

            this.TestHalconFindLedsProcedure(CAMERA_IDS, "REC3_GREEN", "GREEN", "REC3");

            rec3.WriteOutputs(REC3.OuputState.AllOff);
        }

        public void TestDigitalOutputs()
        {
            tower.IO.DO.OnWait(500, OUT_PISTON_DOT_DIGITALES);

            var InputState = rec3.ReadInputState();

            if (InputState != REC3.InputState.Trip)
                TestMotor(1000);

            if (!tower.IO.DI.WaitOn(IN_OUT_NC_RCB, 4000) || !tower.IO.DI.WaitOff(IN_OUT_NO_RCB, 4000))
                throw new Exception("Error Estado inicial salida TRIP incorrecto.");

            var sampler = Sampler.Run(2, 500, (step) =>
            {
                rec3.Rearme();

                Thread.Sleep(5000);

                if (!tower.IO.DI.WaitOff(IN_OUT_NC_RCB, 1000) || !tower.IO.DI.WaitOn(IN_OUT_NO_RCB, 1000))
                    return;

                step.Cancel = true;

            });

            if (!sampler.Canceled)
                throw new Exception("Error Estado final salida TRIP incorrecto.");

            Resultado.Set("RELE_TRIP", "OK", ParamUnidad.SinUnidad);

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteOutputs(REC3.OuputState.ReleyBloq);

            if (!tower.IO.DI.WaitOn(IN_OUT_NC_RELE_BLOQ, 2000) || !tower.IO.DI.WaitOff(IN_OUT_NO_RELE_BLOQ, 2000))
                throw new Exception("Error Estado final salida AUX incorrecto.");

            rec3.WriteOutputs(REC3.OuputState.AllOff);

            if (!tower.IO.DI.WaitOff(IN_OUT_NC_RELE_BLOQ, 2000) || !tower.IO.DI.WaitOn(IN_OUT_NO_RELE_BLOQ, 2000))
                throw new Exception("Error Estado final salida AUX desactivado incorrecto.");

            Resultado.Set("RELE_AUX", "OK", ParamUnidad.SinUnidad);

        }

        public void TestDigitalOutputTrip()
        {
            tower.IO.DO.OnWait(500, OUT_PISTON_DOT_DIGITALES);

            var InputState = rec3.ReadInputState();

            if (InputState != REC3.InputState.Trip)
                TestMotor(1000);

            if (!tower.IO.DI.WaitOn(IN_OUT_NC_RCB, 4000) || !tower.IO.DI.WaitOff(IN_OUT_NO_RCB, 4000))
                throw new Exception("Error Estado inicial salida TRIP incorrecto.");

            var sampler = Sampler.Run(2, 500, (step) =>
            {
                rec3.Rearme();

                Thread.Sleep(5000);

                if (!tower.IO.DI.WaitOff(IN_OUT_NC_RCB, 1000) || !tower.IO.DI.WaitOn(IN_OUT_NO_RCB, 1000))
                    return;

                step.Cancel = true;

            });

            if (!sampler.Canceled)
                throw new Exception("Error Estado final salida TRIP incorrecto.");

            Resultado.Set("RELE_TRIP", "OK", ParamUnidad.SinUnidad);

            tower.IO.DO.PulseOn(800, OUT_PISTON_RECONECT_TEST);

            var tripFinal = Sampler.Run(10, 500, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.Trip;
            });

            Assert.IsTrue(tripFinal.Canceled, Error().UUT.CONFIGURACION.NO_DISPARA("Error Estado del diferencial no disparado después del rearme"));

        }

        public void TestDigitalInputs()
        {
            rec3.WriteMeasurePointsCuadraticCommand(REC3.CuadraticPointsCommand.MeasureVc);

            Delay(600, "Esrcibimos Inicio medida puntos cuadraticos");

            var defs = new AdjustValueDef { Name = Params.CUADRATICPOINTS.Null.Verificacion.Name, 
                Max = Params.CUADRATICPOINTS.Null.Verificacion.Max(), 
                Min = Params.CUADRATICPOINTS.Null.Verificacion.Min(), Unidad= ParamUnidad.Puntos };

            TestMeasureBase(defs,
            (step) =>
            {
                return rec3.ReadCuadraticPoint().ValuePointsCuadratics;

            }, 1, 4, 500);

            defs = new AdjustValueDef { Name = Params.V_LINE.Null.Verificacion.Name, 
                Min = Params.V_LINE.Null.Verificacion.Min(), 
                Max = Params.V_LINE.Null.Verificacion.Max(), Unidad= ParamUnidad.V };

            TestMeasureBase(defs,
            (step) =>
            {
                return (int)rec3.ReadVoltageLine();

            }, 1, 4, 500);
        }

        public void TestMotor(byte ReconexionNumbers, int timeWaitRearme)
        {
            var sampler = Sampler.Run(
               () => { return new SamplerConfig { NumIterations = ReconexionNumbers, Interval = 3500, CancelOnException = true, ThrowException= true }; },
               (step) =>
               {
                   TestMotor(timeWaitRearme, step.Step);
               });
        }

        public void TestInputMotor(int ReconexionNumbers, int timeWaitRearme)
        {
            rec3.Rearme();
            Delay(timeWaitRearme, "Rearmando equipo");
            rec3.Reset();
            Delay(5000, "Rearmando equipo");
            tower.IO.DO.OnWait(500, OUT_PISTON_DOT_DIGITALES);
            var sampler = Sampler.Run(
               () => { return new SamplerConfig { NumIterations = ReconexionNumbers, Interval = 3500, CancelOnException = true, ThrowException = true }; },
               (step) =>
               {
                   InternalTestInputMotor(timeWaitRearme);
               });
            tower.IO.DO.OffWait(500, OUT_PISTON_DOT_DIGITALES);

            rec3.WorkMode(REC3.WorkModeValue.Test);
        }

        public void AdjustMeasureRdo()
        {
            if (rec3.ReadInputState() != REC3.InputState.Trip)
                TestMotor(1000);

            if (!tower.IO.DI.WaitAllOff(50, IN_ESTADO_VL1, IN_ESTADO_VL2, IN_ESTADO_VL3, IN_ESTADO_VN))
                throw new Exception("Error. Estado posición palanca del diferencial incorrecta");

            int Polos = typeREC3 == TypeDeviceREC3.RCB_4Polos ? 2 : 4;

            MeasureResistenceTesterRd(typeREC3);

            tower.IO.DO.On(OUT_ACTIVA_NEUTRO_CALIB, OUT_SELEC_RANGO_CALIB);

            tower.IO.DO.OnWait(500, OUT_CONECTA_RL15_PIN_BOR6);

            int FactorOffsetDefecto =(int)(Configuracion.GetDouble("FACTOR_OFFSET_DEFECTO", 300));

            int FactorGanaciaDefecto = (int)(Configuracion.GetDouble("FACTOR_GANACIA_DEFECTO", 202100));

            rec3.WriteCalibOffsetRdo(FactorOffsetDefecto);

            Delay(600, "Escribimos Inicio Calibración Offset Rdo");

            rec3.WriteCalibFactorRdo(FactorGanaciaDefecto);

            //----------------------------------------------------------------------------
            var defs = new List<AdjustValueDef> {                        
                new AdjustValueDef { Name= Params.GAIN_OFFSET_RDO.LN.Ajuste.Name, Min=Params.GAIN_OFFSET_RDO.LN.Ajuste.Min(), Max =  Params.GAIN_OFFSET_RDO.LN.Ajuste.Max(), Unidad= ParamUnidad.Puntos},
                new AdjustValueDef { Name= Params.GAIN_OFFSET_RDO.L1.Ajuste.Name,   Min=Params.GAIN_OFFSET_RDO.L1.Ajuste.Min(), Max =  Params.GAIN_OFFSET_RDO.L1.Ajuste.Max(), Unidad= ParamUnidad.Puntos}};

            if (typeREC3 == TypeDeviceREC3.RCB_4Polos)
            {
                defs.Add(new AdjustValueDef { Name = Params.GAIN_OFFSET_RDO.L2.Ajuste.Name, Min = Params.GAIN_OFFSET_RDO.L2.Ajuste.Min(), Max = Params.GAIN_OFFSET_RDO.L2.Ajuste.Max(), Unidad = ParamUnidad.Puntos });
                defs.Add(new AdjustValueDef { Name = Params.GAIN_OFFSET_RDO.L3.Ajuste.Name, Min = Params.GAIN_OFFSET_RDO.L3.Ajuste.Min(), Max = Params.GAIN_OFFSET_RDO.L3.Ajuste.Max(), Unidad = ParamUnidad.Puntos });
            }

            var GainOffset = MeasureRdoRd(defs,
            (value) =>
            {
                for (byte i = 0; i < value.Count; i++)
                {
                    if (value[i].Value > 60000)
                        value[i].Value = value[i].Value - 65536;

                    value[i].Value = (int)(FactorOffsetDefecto - value[i].Value);
                }
                return value;
            });

            rec3.WriteCalibOffsetRdo(GainOffset);

            tower.IO.DO.OffWait(500, OUT_SELEC_RANGO_CALIB);

            defs = new List<AdjustValueDef> {                        
                new AdjustValueDef { Name= Params.GAIN_FACTOR_RDO.LN.Ajuste.Name, Min=Params.GAIN_FACTOR_RDO.LN.Ajuste.Min(), Max =  Params.GAIN_FACTOR_RDO.LN.Ajuste.Max() ,Unidad= ParamUnidad.Puntos},
                new AdjustValueDef { Name= Params.GAIN_FACTOR_RDO.L1.Ajuste.Name,   Min=Params.GAIN_FACTOR_RDO.L1.Ajuste.Min(), Max =  Params.GAIN_FACTOR_RDO.L1.Ajuste.Max() , Unidad= ParamUnidad.Puntos}};

            if (typeREC3 == TypeDeviceREC3.RCB_4Polos)
            {
                defs.Add(new AdjustValueDef { Name = Params.GAIN_FACTOR_RDO.L2.Ajuste.Name, Min = Params.GAIN_FACTOR_RDO.L2.Ajuste.Min(), Max = Params.GAIN_FACTOR_RDO.L2.Ajuste.Max(), Unidad = ParamUnidad.Puntos });
                defs.Add(new AdjustValueDef { Name = Params.GAIN_FACTOR_RDO.L3.Ajuste.Name, Min = Params.GAIN_FACTOR_RDO.L3.Ajuste.Min(), Max = Params.GAIN_FACTOR_RDO.L3.Ajuste.Max(), Unidad = ParamUnidad.Puntos });
            }

            int ResistenciaMedidaUtil = 3740; 

            var GainFactor = MeasureRdoRd(defs,
              (value) =>
              {
                  for (byte i = 0; i < value.Count; i++)
                      value[i].Value = (int)(FactorGanaciaDefecto * (double)ResistenciaMedidaUtil / (double)value[i].Value);
                  return value;
              });

            rec3.WriteCalibFactorRdo(GainFactor);
        }

        public void VerificationMeasureRdo()
        {
            var defs = new List<AdjustValueDef> {                        
                new AdjustValueDef { Name= Params.RES_RDO.LN.Verificacion.Name, Average=Params.RES_RDO.LN.Verificacion.Avg(), Tolerance =  Params.RES_RDO.LN.Verificacion.Tol(), Unidad= ParamUnidad.Puntos},
                new AdjustValueDef { Name= Params.RES_RDO.L1.Verificacion.Name,   Average=Params.RES_RDO.L1.Verificacion.Avg(), Tolerance =  Params.RES_RDO.L1.Verificacion.Tol(), Unidad= ParamUnidad.Puntos}};

            if (typeREC3 == TypeDeviceREC3.RCB_4Polos)
            {
                defs.Add(new AdjustValueDef { Name = Params.RES_RDO.L2.Verificacion.Name, Average = Params.RES_RDO.L2.Verificacion.Avg(), Tolerance = Params.RES_RDO.L2.Verificacion.Tol(), Unidad = ParamUnidad.Puntos });
                defs.Add(new AdjustValueDef { Name = Params.RES_RDO.L3.Verificacion.Name, Average = Params.RES_RDO.L3.Verificacion.Avg(), Tolerance = Params.RES_RDO.L3.Verificacion.Tol(), Unidad = ParamUnidad.Puntos });
            }

            MeasureRdoRd(defs);
          
            tower.IO.DO.Off(OUT_ACTIVA_NEUTRO_CALIB);
            tower.IO.DO.Off(OUT_CONECTA_RL15_PIN_BOR6);
        }

        public void AdjustMeasureRcc()
        {
            if (rec3.ReadInputState() != REC3.InputState.Trip)
                TestMotor(1000);

            if (!tower.IO.DI.WaitAllOff(50, IN_ESTADO_VL1, IN_ESTADO_VL2, IN_ESTADO_VL3, IN_ESTADO_VN))
                throw new Exception("Error. Estado posición palanca del diferencial incorrecta");

            tower.IO.DO.OffWait(800, OUT_CONECTA_RCC_0R, OUT_CONECTA_RCC_4R, OUT_CONECTA_RL15_PIN_BOR6);

            var defs = new List<AdjustValueDef> {                        
                new AdjustValueDef { Name= Params.RES_RCC.Other("Vh").Infinito.Name, Min=Params.RES_RCC.Other("Vh").Infinito.Min(), Max =  Params.RES_RCC.Other("Vh").Infinito.Max(), Unidad = ParamUnidad.Puntos},
                new AdjustValueDef { Name= Params.RES_RCC.Other("Vshunt").Infinito.Name, Min=Params.RES_RCC.Other("Vshunt").Infinito.Min(), Max =  Params.RES_RCC.Other("Vshunt").Infinito.Max(), Unidad = ParamUnidad.Puntos}};

            MeasureRcc(defs);

            tower.IO.DO.OnWait(800, OUT_CONECTA_RCC_0R, OUT_CONECTA_RCC_4R);

            defs = new List<AdjustValueDef> {                        
                new AdjustValueDef { Name= Params.GAIN_OFFSET_RCC.Other("Vh").Ajuste.Name, Min=Params.GAIN_OFFSET_RCC.Other("Vh").Ajuste.Min(), Max =  Params.GAIN_OFFSET_RCC.Other("Vh").Ajuste.Max(), Unidad = ParamUnidad.Puntos},
                new AdjustValueDef { Name= Params.GAIN_OFFSET_RCC.Other("Vshunt").Ajuste.Name,  Min=Params.GAIN_OFFSET_RCC.Other("Vshunt").Ajuste.Min(), Max =  Params.GAIN_OFFSET_RCC.Other("Vshunt").Ajuste.Max(), Unidad = ParamUnidad.Puntos}};

            var Offset = MeasureRcc(defs);

            rec3.WriteCalibOffsetRcc((ushort)Offset[0], (ushort)Offset[1]);

            tower.IO.DO.OffWait(800, OUT_CONECTA_RCC_0R);

            tower.IO.DO.OnWait(800, OUT_CONECTA_RCC_4R);

            defs = new List<AdjustValueDef> {                        
                new AdjustValueDef { Name= Params.GAIN_FACTOR_RCC.Other("Vh").Ajuste.Name, Min=Params.GAIN_FACTOR_RCC.Other("Vh").Ajuste.Min(), Max =  Params.GAIN_FACTOR_RCC.Other("Vh").Ajuste.Max(), Unidad = ParamUnidad.Puntos},
                new AdjustValueDef { Name= Params.GAIN_FACTOR_RCC.Other("Vshunt").Ajuste.Name, Min=Params.GAIN_FACTOR_RCC.Other("Vshunt").Ajuste.Min(), Max =  Params.GAIN_FACTOR_RCC.Other("Vshunt").Ajuste.Max(), Unidad = ParamUnidad.Puntos}};

            var Factor = MeasureRcc(defs);

            rec3.WriteCalibFactorRcc((ushort)Factor[0], (ushort)Factor[1]);

            tower.IO.DO.OffWait(800, OUT_CONECTA_RCC_4R);

        }

        public void Verification_MeasureRcc()
        {
            tower.IO.DO.OnWait(500, OUT_CONECTA_RCC_4R);

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteMeasureCommand(REC3.MeasureCommand.Rcc);

            Delay(600, "Esrcibimos Inicio Verificación Medida Rcc");

            var defs = new AdjustValueDef { Name = Params.RES_RCC.Null.Verificacion.Name, Average = Params.RES_RCC.Null.Verificacion.Avg(), Tolerance = Params.RES_RCC.Null.Verificacion.Tol(), Unidad = ParamUnidad.Ohms };

            TestMeasureBase(defs,
            (step) =>
            {
                return rec3.ReadValueRcc();

            }, 1, 5, 600);

        }

        public void Customization()
        {
            SetupClient();

            rec3.WriteNumSerie(Convert.ToUInt32(TestInfo.NumSerie));

            Delay(600, "Esrcibimos Numero de Serie");

            tower.IO.DO.Off(OUT_ALIMENTACION_RED);

            tower.IO.DO.Off(OUT_ACTIVA_230VCA);

            Delay(2000, "Apagamos el equipo");

            tower.IO.DO.On(OUT_ALIMENTACION_RED);

            tower.IO.DO.On(OUT_ACTIVA_230VCA);

            Delay(100, "Encendemos el equipo");

            var sampler = Sampler.Run(
             () => { return new SamplerConfig { NumIterations = 2, Interval = 500, CancelOnException = true, ThrowException = true }; },
            (step) =>
            {
                try
                {
                    var IdpRegister = rec3.ReadRegisterIDP();

                    Assert.IsTrue(IdpRegister.NumSerie.ToString() == TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, verificación de la grabación del Numero de Serie incorrecto"));

                    Assert.IsTrue(IdpRegister.Bastidor ==(uint)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, verificación de la grabación del Numero de Bastidor incorrecto"));

                    step.Cancel = true;
                }
                catch (TimeoutException)
                {
                    Delay(500, "Excepción al leer Registro de IDP");
                }
            });

            Assert.IsTrue(sampler.Canceled, Error().UUT.COMUNICACIONES.NO_GRABADO("Error comunicaciones, no se ha verificado la grabacion del numero de serie o del bastidor"));

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteErrorCode(0);

            Delay(500, "Escribimos codigo error 0");
        }

        public void TestFinish()
        {
            if (rec3 != null)
                rec3.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(OUT_ALIMENTACION_RED);

                tower.IO.DO.Off(OUT_ACTIVA_230VCA);

                tower.IO.DO.Off(OUT_SELEC_REC_FUNCTION, OUT_PUSH_TAPA, OUT_SELEC_RANGO_CALIB, OUT_ACTIVA_NEUTRO_CALIB, OUT_ACTIVATE_12VDC_N, OUT_ACTIVATE_12VDC_F, OUT_MODEL_RECHARGE);

                tower.IO.DO.Off(OUT_CONECTA_RCC_4R, OUT_CONECTA_RCC_0R, OUT_CONECTA_RL15_PIN_BOR6, OUT_DESCONECTA_MODULO_CALIB);

                tower.IO.DO.OffWait(500, OUT_PISTON_DOT_DIGITALES);

                tower.IO.DO.OffWait(500, OUT_PISTON_DOT_COMM);

                tower.IO.DO.OffWait(500, OUT_PISTON_DOT_TEST);

                tower.IO.DO.OffWait(500, OUT_PISTON_FIJACION);

                tower.IO.DO.OffWait(500, OUT_PUSH_TAPA);

                tower.PTE.ApplyOffAllAndWaitStabilisation();

                Delay(500, "Espera por si el aire se quita, antes de subir el piston");

                tower.Dispose();
            }
        }

        //**********************************

        private void SetupDefault()
        {
            rec3.WriteDeleteFlashPage(REC3.DeletePageSetup.PageParametrizacion);

            REC3.SelectionRdRdoValue selectionRdRdoValue;

            REC3.NumReconexionSecuenceValue numReconexionSecuenceValue;

            string SubModel = "GE04";

            ushort CRC = 60633;


            if (typeREC3 == TypeDeviceREC3.RCB0)
            {
                rec3.WriteModel(REC3.ModeloValue.RCBO2Polos);
                numReconexionSecuenceValue = REC3.NumReconexionSecuenceValue.GE_RCBO;
                selectionRdRdoValue = REC3.SelectionRdRdoValue._30mA;
                SubModel = "GE05";
                CRC = 30561;
            }
            else
            {
                rec3.WriteModel(REC3.ModeloValue.RCB4Polos);
                numReconexionSecuenceValue = REC3.NumReconexionSecuenceValue.GE;
                selectionRdRdoValue = REC3.SelectionRdRdoValue._300mA;
            }

            rec3.WriteSelectionRdRdo(selectionRdRdoValue);

            rec3.WriteSecurityRelayBloq(REC3.SecurityRelayBloqValue.Standard);

            rec3.WriteNumReconexionSecuence(numReconexionSecuenceValue);

            rec3.WriteSubModel(SubModel);

            rec3.WriteSupervisionAjust(REC3.SupervisionAjust1Value.Whithout_Supervision);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rdo30mAHighLevel, 23000);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rd30mALowLevel, 15000);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rdo300mAHighLevel, 5000);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rd300mALowLevel, 2500);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RdoCustomHighLevel, 65535);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RdCustomLowLevel, 65535);

            if (typeREC3 == TypeDeviceREC3.RCB0)
            {
                rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RccoCustomHighLevel, 509);

                rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RccCustomLowLevel, 242);
            }

            rec3.WriteRearmeMode(REC3.RearmeModeValue.WithSupervision);

            rec3.WriteParametrizacionCode(CRC);

            rec3.WriteDeleteFlashPage(REC3.DeletePageSetup.PageSetup);

            Delay(600, "Grabación de todos los parametros por defecto");

            rec3.Reset();

            Delay(600, "Inicialización del equipo después del Reset");

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.ReadErrorState();

            var sampler = Sampler.Run(
            () => { return new SamplerConfig { NumIterations = 5, CancelOnException = true, Interval=500 }; },
            (step) =>
            {
                try
                {
                    var Inputs = rec3.ReadInputState();

                    if (Inputs != REC3.InputState.Trip)
                        tower.IO.DO.PulseOn(800, OUT_PISTON_RECONECT_TEST);
                    else
                        step.Cancel = true;
                }
                catch (TimeoutException)
                {
                    Delay(500, "Excepción al leer estado de las entradas");
                }
            });

            sampler.ThrowExceptionIfExists();

            if (!sampler.Canceled)
                throw new Exception("Estado posición palanca del diferencial incorrecta ");

            if (!tower.IO.DI.WaitAllOff(50, IN_ESTADO_VL1, IN_ESTADO_VL2, IN_ESTADO_VL3, IN_ESTADO_VN))
                throw new Exception("Error. Estado posición palanca del diferencial incorrecta");

        }

        private void SetupClient()
        {
            ParametrizacionRec3 Param = null;

            byte SupervisionInfratension = (byte)this.Parametrizacion.GetDouble("SupervisionInfratensión", 0, ParamUnidad.SinUnidad);
            byte SupervisionSobretensión = (byte)this.Parametrizacion.GetDouble("SupervisionSobretensión", 0, ParamUnidad.SinUnidad);

            Param = new ParametrizacionRec3
            {
                Modelo = (REC3.ModeloValue)this.Parametrizacion.GetDouble("Modelo", 2, ParamUnidad.SinUnidad),
                Submodel = this.Parametrizacion.GetString("SubModel", "GE05", ParamUnidad.SinUnidad),
                SecurityRelayBloq = (REC3.SecurityRelayBloqValue)this.Parametrizacion.GetDouble("LogicaReleBloqueo", 0, ParamUnidad.SinUnidad),
                RearmeMode = (REC3.RearmeModeValue)this.Parametrizacion.GetDouble("ModoRearme", 0, ParamUnidad.SinUnidad),
                NumReconexionSecuence = (REC3.NumReconexionSecuenceValue)this.Parametrizacion.GetDouble("NumeroSecuenciaReconexión", 4, ParamUnidad.SinUnidad),
                SelectionRdo = (REC3.SelectionRdRdoValue)this.Parametrizacion.GetDouble("SeleccionRdRdo", 1, ParamUnidad.SinUnidad),
                SupervisionAjust1 = (REC3.SupervisionAjust1Value)(SupervisionInfratension & SupervisionSobretensión),
                IsolationLevelRdo = new REC3.RegistersLevelInsolation
                {
                    Rd30mALowLevel = (ushort)this.Parametrizacion.GetDouble("Rd30mA_MinLevel", 15000, ParamUnidad.Ohms),
                    Rdo30mAHighLevel = (ushort)this.Parametrizacion.GetDouble("Rdo30mA_MaxLevel", 23000, ParamUnidad.Ohms),
                    Rdo300mAHighLevel = (ushort)this.Parametrizacion.GetDouble("Rdo300mA_MaxLevel", 5000, ParamUnidad.Ohms),
                    Rd300mALowLevel = (ushort)this.Parametrizacion.GetDouble("Rd300mA_MinLevel", 2500, ParamUnidad.Ohms),
                    RdCustomLowLevel = (ushort)this.Parametrizacion.GetDouble("RdCustom_MinLevel", 0xFFFF, ParamUnidad.Ohms),
                    RdoCustomHighLevel = (ushort)this.Parametrizacion.GetDouble("RdoCustom_MaxLevel", 0xFFFF, ParamUnidad.Ohms),
                },
                CRC = Convert.ToUInt16(this.Parametrizacion.GetString("CRC", "0x8840", ParamUnidad.SinUnidad), 16),
                ReconnectionTable = GetReconnectionTable()
            };

            Resultado.Set("Modelo", Param.Modelo.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("SubModel", Param.Submodel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("LogicaReleBloqueo", Param.SecurityRelayBloq.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ModoRearme", Param.RearmeMode.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("NumeroSecuenciaReconexión", Param.NumReconexionSecuence.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("SeleccionRdRdo", Param.SelectionRdo.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("SubModel", Param.Submodel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("LogicaReleBloqueo", Param.SecurityRelayBloq.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ModoRearme", Param.RearmeMode.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("SupervisionInfratensión", Param.SupervisionAjust1.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("SupervisionSobretensión", Param.SupervisionAjust1.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("Rdo30mA_MaxLevel", Param.IsolationLevelRdo.Rdo30mAHighLevel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("Rd30mA_MinLevel", Param.IsolationLevelRdo.Rd30mALowLevel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("Rdo300mA_MaxLevel", Param.IsolationLevelRdo.Rdo300mAHighLevel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("Rd300mA_MinLevel", Param.IsolationLevelRdo.Rd300mALowLevel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("RdoCustom_MaxLevel", Param.IsolationLevelRdo.RdoCustomHighLevel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("RdCustom_MinLevel", Param.IsolationLevelRdo.RdCustomLowLevel.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CRC", Param.CRC.ToString("X2"), ParamUnidad.SinUnidad);
            if (Param.ReconnectionTable.HasReconnections)
                Resultado.Set("Reconnection_Table", Param.ReconnectionTable.GetStageListString(), ParamUnidad.SinUnidad);

            if (typeREC3== TypeDeviceREC3.RCB0)
            {
                Param.IsolationLevelRcc = new REC3.RegistersRccLevelInsolation
                {
                    RccCustomLowLevel = (ushort)this.Parametrizacion.GetDouble("RccCustom_MinLevel", 145, ParamUnidad.Ohms),
                    RccoCustomHighLevel = (ushort)this.Parametrizacion.GetDouble("RccoCustom_MaxLevel", 509, ParamUnidad.Ohms),
                };

                Resultado.Set("RccoCustom_MaxLevel", Param.IsolationLevelRcc.RccoCustomHighLevel.ToString(), ParamUnidad.SinUnidad);
                Resultado.Set("RccCustom_MinLevel", Param.IsolationLevelRcc.RccCustomLowLevel.ToString(), ParamUnidad.SinUnidad);
            }
  
            WriteSetupClient(Param);
        }

        private REC3.ReconnectionTableConfiguration GetReconnectionTable()
        {
            var reconnectionInfo = new REC3.ReconnectionTableConfiguration(10);

            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_01", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_02", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_03", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_04", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_05", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_06", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_07", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_08", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_09", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);
            GetAndCheckReconnectionStageInfo(Parametrizacion.GetString("RECONEXION_CUSTOM_ETAPA_10", "SinEtapa", ParamUnidad.SinUnidad), reconnectionInfo);

            return reconnectionInfo;
        }

        private void GetAndCheckReconnectionStageInfo(string reconnectionInfo, REC3.ReconnectionTableConfiguration reconnectionConfiguration)
        {
            if (reconnectionInfo.Equals("SinEtapa"))
                reconnectionConfiguration.listStages.Add(null);
            else
                reconnectionConfiguration.listStages.Add(new REC3.ReconnectionTableConfiguration.ReconnectionStage(reconnectionInfo));
        }

        private class ParametrizacionRec3
        {
            public REC3.ModeloValue Modelo { get; set; }
            public string Submodel { get; set; }
            public REC3.SelectionRdRdoValue SelectionRdo { get; set; }
            public REC3.SecurityRelayBloqValue SecurityRelayBloq { get; set; }
            public REC3.SupervisionAjust1Value SupervisionAjust1 { get; set; }
            public REC3.NumReconexionSecuenceValue NumReconexionSecuence { get; set; }
            public REC3.RegistersLevelInsolation IsolationLevelRdo { get; set; }
            public REC3.RegistersRccLevelInsolation IsolationLevelRcc { get; set; }
            public REC3.RearmeModeValue RearmeMode { get; set; }
            public REC3.ReconnectionTableConfiguration ReconnectionTable { get; set; }
            public ushort CRC { get; set; }
        }

        private void WriteSetupClient(ParametrizacionRec3 Parametrizacion)
        {

            rec3.WriteDeleteFlashPage(REC3.DeletePageSetup.PageParametrizacion);

            rec3.WriteModel(Parametrizacion.Modelo);

            rec3.WriteSelectionRdRdo(Parametrizacion.SelectionRdo);

            rec3.WriteSecurityRelayBloq(Parametrizacion.SecurityRelayBloq);

            rec3.WriteNumReconexionSecuence(Parametrizacion.NumReconexionSecuence);

            rec3.WriteSubModel(Parametrizacion.Submodel);

            rec3.WriteSupervisionAjust(Parametrizacion.SupervisionAjust1);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rdo30mAHighLevel, Parametrizacion.IsolationLevelRdo.Rdo30mAHighLevel);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rd30mALowLevel, Parametrizacion.IsolationLevelRdo.Rd30mALowLevel);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rdo300mAHighLevel, Parametrizacion.IsolationLevelRdo.Rdo300mAHighLevel);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.Rd300mALowLevel, Parametrizacion.IsolationLevelRdo.Rd300mALowLevel);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RdoCustomHighLevel, Parametrizacion.IsolationLevelRdo.RdoCustomHighLevel);

            rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RdCustomLowLevel, Parametrizacion.IsolationLevelRdo.RdCustomLowLevel);

            if (Parametrizacion.Modelo == REC3.ModeloValue.RCBO2Polos)
            {
                rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RccoCustomHighLevel, Parametrizacion.IsolationLevelRcc.RccoCustomHighLevel);

                rec3.WriteLevelIsolation(REC3.RegistersLevelIsolation.RccCustomLowLevel, Parametrizacion.IsolationLevelRcc.RccCustomLowLevel);
            }

            rec3.WriteRearmeMode(Parametrizacion.RearmeMode);

            rec3.WriteParametrizacionCode(Parametrizacion.CRC);

            rec3.WriteDeleteFlashPage(REC3.DeletePageSetup.PageSetup);

            rec3.SendReconnectionTable(Parametrizacion.ReconnectionTable);

            Thread.Sleep(600);

            rec3.Reset();

            Thread.Sleep(600);

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.ReadErrorState();
        }

        private void TestMotor(int timeWaitRearme, int reconexion = 1)
        {
            rec3.Rearme();

            var sampler = Sampler.Run(10, 500, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.AllOff;
            });

            Assert.IsTrue(sampler.Canceled, Error().UUT.CONFIGURACION.SETUP("Error el equipo no rearma o no se detecta el estado de rearmado"));

            Delay(reconexion == 1 ? timeWaitRearme * 2 : timeWaitRearme, "Espera para asegurar que el diferencial se aguanta");

            sampler = Sampler.Run(10, 500, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.AllOff;
            });

            Assert.IsTrue(sampler.Canceled, reconexion == 0 ? Error().UUT.CONFIGURACION.SETUP("Error, el diferencial es defectuoso se debe de cambiar el diferencial") : Error().UUT.CONFIGURACION.SETUP("Error el diferencial no se mantiene rearmado"));
     
            tower.IO.DO.PulseOn(800, OUT_PISTON_RECONECT_TEST);

            sampler = Sampler.Run(10, 500, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.Trip;
            });

            Assert.IsTrue(sampler.Canceled, Error().UUT.CONFIGURACION.NO_DISPARA("Error Estado del diferencial no disparado después de Test"));

            rec3.ReadErrorState();
        }

        private void InternalTestInputMotor(int timeWaitRearme)
        {
            var statusDiferencial = rec3.ReadInputState();

            if (statusDiferencial != REC3.InputState.Trip)
            {
                tower.IO.DO.PulseOn(800, OUT_PISTON_RECONECT_TEST);
                var trip = Sampler.Run(10, 500, (step) =>
                {
                    step.Cancel = rec3.ReadInputState() == REC3.InputState.Trip;
                });

                Assert.IsTrue(trip.Canceled, Error().UUT.CONFIGURACION.NO_DISPARA("Error Estado del diferencial no disparado después de Test"));
            }

            Delay(10000, "Espera para comprobar que el equipo no rearma sin activar la entrada de rearme");
            var status = Sampler.Run(10, 500, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.Trip;
            });

            Assert.IsTrue(status.Canceled, Error().UUT.CONFIGURACION.NO_DISPARA("Error Estado del diferencial rearmado antes de mandar señal de rearme"));

            tower.IO.DO.OnWait(200, OUT_MODEL_RECHARGE);
            tower.IO.DO.OnWait(500, OUT_ACTIVATE_12VDC_F, OUT_ACTIVATE_12VDC_N);

            Delay(timeWaitRearme, "Esperando que el equipo rearme activando la entrada digital");
            var sampler = Sampler.Run(10, 1000, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.AllOff;
            });

            Assert.IsTrue(sampler.Canceled, Error().UUT.CONFIGURACION.NO_DISPARA("Error el equipo no rearma o no se detecta el estado de rearmado después de activar la entrada de rearme"));

            tower.IO.DO.OffWait(500, OUT_ACTIVATE_12VDC_F, OUT_ACTIVATE_12VDC_N);
            tower.IO.DO.OffWait(200, OUT_MODEL_RECHARGE);            

            tower.IO.DO.PulseOn(800, OUT_PISTON_RECONECT_TEST);
            var tripFinal = Sampler.Run(10, 500, (step) =>
            {
                step.Cancel = rec3.ReadInputState() == REC3.InputState.Trip;
            });

            Assert.IsTrue(tripFinal.Canceled, Error().UUT.CONFIGURACION.NO_DISPARA("Error Estado del diferencial no disparado después del rearme"));
        }

        private void MeasureResistenceTesterRd(TypeDeviceREC3 typeDeviceREC3)
        {
            tower.IO.DO.OnWait(500, OUT_DESCONECTA_MODULO_CALIB);

            MeasureTester(Params.RES_RDO.LN.TESTER.Name, 9, 3780, 3855);  

            MeasureTester(Params.RES_RDO.L1.TESTER.Name, 10, 3780, 3855); 

            if (typeDeviceREC3 == TypeDeviceREC3.RCB_4Polos)
            {

                MeasureTester(Params.RES_RDO.L2.TESTER.Name, 11, 3780, 3855);

                MeasureTester(Params.RES_RDO.L3.TESTER.Name, 12, 3780, 3855); 
            }

            tower.IO.DO.OffWait(500, OUT_DESCONECTA_MODULO_CALIB);
        }

        private void MeasureTester(string testPoint, byte InMux, double Min, double Max)
        {
            AdjustValueDef defs = new AdjustValueDef { Name = testPoint, Min = Min, Max = Max, Unidad = ParamUnidad.Ohms };

            tower.MUX.Reset();

            TestMeasureBase(defs,
            (step) =>
            {
                return tower.MeasureMultimeter((InputMuxEnum)InMux, MagnitudsMultimeter.Resistencia);

            }, 0, 4, 600);

        }

        private int[] MeasureRdoRd(List<AdjustValueDef> defs, Func<List<AdjustValueDef>, List<AdjustValueDef>> validFunc = null)
        {
            rec3.WriteMeasureCommand(REC3.MeasureCommand.Rdo);

            Delay(1000, "Esperamos equipo se prepare para iniciar medida RdoRd");

            var val = new double[defs.Count];

            var result = TestMeasureBase(defs,
            (step) =>
            {
                var MedidaRdoRd = rec3.ReadRdoRdValue();

                val[0] = MedidaRdoRd.ValueRdoRdNeutro;
                val[1] = MedidaRdoRd.ValueRdoRdFase1;

                if (defs.Count > 2)
                {
                    val[2] = MedidaRdoRd.ValueRdoRdFase2;
                    val[3] = MedidaRdoRd.ValueRdoRdFase3;
                }
                return val;
            },
            2, 10, 600,
            (value) =>
            {
                if (validFunc != null)
                {
                    value = validFunc(value);
                    for (byte i = 0; i < defs.Count; i++)
                        val[i] = (int)defs[i].Value;
                }
                return !HasError(value, false).Any();
            }
            );
      
            return val.Select((p)=> Convert.ToInt32(p)).ToArray();
        }

        private ushort[] MeasureRcc(List<AdjustValueDef> defs)
        {
            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteMeasureCommand(REC3.MeasureCommand.Rcc);

            Delay(1000, "Esperamos equipo se prepare para iniciar medida Rcc");

            var val = new double[defs.Count];

            var result = TestMeasureBase(defs,
            (step) =>
            {
                var value = rec3.ReadCuadraticPoint();
             
                val[0] = value.ValueRccVh;
                val[1] = value.ValueRccVshunt;

                return val;
            },
            2, 10, 600
            );

            return val.Select((p)=> Convert.ToUInt16(p)).ToArray();
        } 

        public void ReadResistenceTesterRcc()
        {
            tower.IO.DO.OffWait(200, OUT_ALIMENTACION_RED, OUT_ACTIVA_230VCA);

            tower.IO.DO.Off(OUT_CONECTA_RCC_0R);

            tower.IO.DO.On(OUT_CONECTA_RCC_4R);

            AdjustValueDef Resdef = new AdjustValueDef { Name = Params.RES_RCC.Null.TESTER.Name, Min = Params.RES_RCC.Null.TESTER.Min(), Max = Params.RES_RCC.Null.TESTER.Max() };

            var sampler = Sampler.Run(2, 500, (step) =>
            {
                Resdef.Value = IntroResistenceRccUserControl("4 Ohms");
                step.Cancel = !HasError(Resdef, false);
            });

            Resdef.AddToResults(Resultado);

            tower.IO.DO.Off(OUT_CONECTA_RCC_4R);

            Assert.IsTrue(sampler.Canceled, Error().UUT.MEDIDA.MARGENES("Error Medida Resistencia Rcc 4 Ohms fuera de margenes"));
        }

        private double IntroResistenceRccUserControl(string TestPoint)
        {
            var Res = Shell.ShowDialog<double>("INTRODUCE EL VALOR DE RESISTENCIA QUE INDICA EL TESTER",
                () => new InputKeyBoard("Leer valor resistencia de Rcc " + TestPoint, "9.99"),
                MessageBoxButtons.OKCancel,
                (c, r) => { return r == DialogResult.OK ? Convert.ToDouble(((InputKeyBoard)c).TextValue) : -1; });

            if (Res < 0)
                throw new Exception("El usuario ha cancelado la entrada del valor Resistencia Rcc " + TestPoint);

            return Res;
        }

        private void ControlDiferencialCode(char[] codeDiferencial, char[] codeDiferencialBBDD)
        {
            var errorSearch = new List<string>();

            if (codeDiferencial.Length != codeDiferencialBBDD.Length)
                throw new Exception("La longitud del diferencial del equipo no corresponde con la de la BBDD");

            for (byte i = 0; i < codeDiferencial.Length; i++)
                switch (i)
                {
                    case 0:
                        if (codeDiferencial[i] != codeDiferencialBBDD[i])
                            errorSearch.Add("marca");
                        break;
                    case 1:
                        if ((codeDiferencial[i] != codeDiferencialBBDD[i]) && (codeDiferencialBBDD[i] != '*'))
                            errorSearch.Add("submodelo ");
                        break;
                    case 2:
                    case 3:
                        if ((codeDiferencial[i] != codeDiferencialBBDD[i]) && (codeDiferencialBBDD[i] != '*'))
                            if (!errorSearch.Contains("número de normativa"))
                                errorSearch.Add("número de normativa");
                        break;
                    case 4:
                        if (codeDiferencial[i] != codeDiferencialBBDD[i])
                            errorSearch.Add("número de polos");
                        break;
                    case 5:
                        if (Convert.ToByte(codeDiferencial[i]) < Convert.ToByte(codeDiferencialBBDD[i]))
                            errorSearch.Add("corriente nominal");
                        break;
                    case 6:
                    case 7:
                        if (codeDiferencial[i] != codeDiferencialBBDD[i])
                            if (!errorSearch.Contains("sensibilidad disparo"))
                                errorSearch.Add("sensibilidad disparo");
                        break;
                    case 8:
                        if (codeDiferencial[i] != codeDiferencialBBDD[i])
                            errorSearch.Add("clase de diferencial");
                        break;
                }

            string errormessage = "";

            errorSearch.ForEach(p => { errormessage += p.ToString().Trim().ToUpper() + "; "; });

            Assert.IsTrue(errorSearch.Count == 0, Error().SOFTWARE.BBDD.VERSION_INCORRECTA("Error. código del diferencial leido no coincide con el de la BBDD. Caracteristicas diferentes: " + errormessage));
        }

    }
}
