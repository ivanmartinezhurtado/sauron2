using Comunications.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;

namespace Comunications.Message
{
    public class TcpModbusTransport : ModbusTransport
	{
        private static readonly object _transactionIdLock = new object();
        private ushort _transactionId;

        public TcpModbusTransport(IStreamResource streamResource)
            : base(streamResource)
		{
		}

        public TcpModbusTransport(IStreamResource streamResource, ILog logger)
            : base(streamResource, logger)
        {
        }

        protected override byte[] BuildRequestMessageFrame(ITransportMessage message)
        {
            ModbusMessage msg = message as ModbusMessage;

            msg.TransactionId = GetNewTransactionId();

            byte[] pdu = message.MessageFrame; // Mensaje sin el CRC - base.BuildRequestMessageFrame(msg); 

            List<byte> messageBody = new List<byte>();
            messageBody.AddRange(GetMbapHeader(msg, pdu.Length));
            messageBody.AddRange(pdu);

            return messageBody.ToArray();
        }

        internal static byte[] GetMbapHeader(ModbusMessage message, int lengthPDU)
        {
            List<byte> header = new List<byte>();

            byte[] transactionId = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)message.TransactionId));
            byte[] protocol = { 0, 0 };
            byte[] length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)(lengthPDU)));

            header.AddRange(transactionId);
            header.AddRange(protocol);
            header.AddRange(length);
            //header.Add(message.SlaveAddress);

            return header.ToArray();
        }

        internal virtual ushort GetNewTransactionId()
        {
            lock (_transactionIdLock)
            {
                _transactionId = _transactionId == UInt16.MaxValue ? (ushort)1 : ++_transactionId;
            }

            return _transactionId;
        }

        protected override byte[] ReadRequestResponse(ITransportMessage message)
        {
            byte[] mbapHeader = new byte[6];
            int numBytesRead = 0;

            while (numBytesRead != 6)
            {
                numBytesRead += _streamResource.Read(mbapHeader, numBytesRead, 6 - numBytesRead);

                if (numBytesRead == 0)
                    throw new IOException("Read resulted in 0 bytes returned.");
            }

            ushort frameLength = (ushort)IPAddress.HostToNetworkOrder(BitConverter.ToInt16(mbapHeader, 4));

            // read message
            byte[] messageFrame = new byte[frameLength];
            numBytesRead = 0;
            while (numBytesRead != frameLength)
            {
                numBytesRead += _streamResource.Read(messageFrame, numBytesRead, frameLength - numBytesRead);

                if (numBytesRead == 0)
                    throw new IOException("Read resulted in 0 bytes returned.");

                if (numBytesRead == 3 && messageFrame[1] > Modbus.ExceptionOffset)
                    break;
            }

            byte[] frame = mbapHeader.Concat(messageFrame).ToArray();

            _logger.InfoFormat("RX: {0}", GetLogFrameString(frame));

            return frame;
        }

        public override T CreateMessage<T>(byte[] frame)
        {
            byte[] mbapHeader = frame.Take(6).ToArray(); // frame.Slice(0, 6).ToArray();
            byte[] messageFrame = frame.Skip(6).ToArray(); // frame.Slice(6, frame.Length - 6).ToArray();

            var response = base.CreateMessage<T>(messageFrame);
            response.TransactionId = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(mbapHeader, 0));

            return response;
        }

        protected override void ValidateResponse(ITransportMessage request, ITransportMessage response)
        {
            if (request.TransactionId != response.TransactionId)
                throw new IOException(String.Format(CultureInfo.InvariantCulture, "Response was not of expected transaction ID. Expected {0}, received {1}.", request.TransactionId, response.TransactionId));

            base.ValidateResponse(request, response);
        }
	}
}
