﻿
using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.41)]
    public class CDCAutoSupplyTest : CDCTestBase
    {
        private const int PILOTO_ERROR = 48;
        private const int POWER_SUPPLY_TTLRS232 = 15;
        private const int OUTPUT_DIRECT_CURRENT_SBT_1 = 53;
        private const int PULSADOR_RESET_5M = 9;
        private const int PULSADOR_RESET_7M = 10;

        private const int OUT_PLC_NEUTRO = 22;
        private const int PISTON_GENERAL = 4;
        private const int OUT_ALIMENTACION_CONTADORES =56;
        private const int OUT_CONTADORES_TO_PLS = 54;

        public void TestInitialization()
        {
            CAMERA_CPU_PSU = GetVariable<string>("CAMARA_IDS_CPU", CAMERA_CPU_PSU);
            CAMERA_SBT = GetVariable<string>("CAMARA_IDS_SBT", CAMERA_SBT);
            CAMERA_AUX = GetVariable<string>("CAMARA_IDS_AUX", CAMERA_AUX);

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            //******************************************
            tower.IO.DO.On(PISTON_GENERAL, POWER_SUPPLY_TTLRS232, OUTPUT_DIRECT_CURRENT_SBT_1);
            tower.IO.DO.OffWait(1000, PILOTO_ERROR, OUT_ALIMENTACION_CONTADORES, OUT_CONTADORES_TO_PLS);

            InstanceNumber = (int)Configuracion.GetDouble("NUMERO_EQUIPOS_SBT", 1, ParamUnidad.SinUnidad);
            has3G = Configuracion.GetString("HAS_MODULE_MODEM", "NO").Trim().ToUpper() == "SI" ? "3G_" : "";
            onlySBT = InstanceNumber == 1 ? true : false;
            DoubleSecondary = !onlySBT;

            PULSADOR_RESET = has3G.Contains("3G") ? PULSADOR_RESET_5M : PULSADOR_RESET_7M;
            SBT_NUMBER = 1;
        }

        public void TestConsumption()
        {
            List<AdjustValueDef> consumptionValues = new List<AdjustValueDef>();

            consumptionValues.Add(new AdjustValueDef(Params.W.Null.EnVacio.Name, 0, Params.W.Null.EnVacio.Min(), Params.W.Null.EnVacio.Max(), 0, 0, ParamUnidad.W));
            consumptionValues.Add(new AdjustValueDef(Params.VAR.Null.EnVacio.Name, 0, Params.VAR.Null.EnVacio.Min(), Params.VAR.Null.EnVacio.Max(), 0, 0, ParamUnidad.Var));

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(230, 0, 50, 0, TriLineValue.Create(0, 120, 240));

            TestMeasureBase(consumptionValues, (step) =>
            {
                var cvmMteReadings = tower.CvmminiMTE.ReadAllVariables().Phases.L2;

                return new double[] { cvmMteReadings.PotenciaActiva, cvmMteReadings.PotenciaReactiva };
            }, 2, 5, 1000);
        }

        public void TestLeds_SBT()
        {
            this.TestHalconFindLedsProcedure(CAMERA_SBT, "CDC_" + has3G + "AutoSupply_SBT", has3G + "LEDS_SBT", "CDC", 3);
        }

        public void TestLeds_SBT_Modelo3G()
        {
            this.TestHalconFindLedsProcedure(CAMERA_CPU_PSU, "CDC_" + has3G + "AutoSupply_PLC", has3G + "LEDS_PLC", "CDC", 3);
        }

        public override void TestLedsCPU()
        {
            ActivateLedsCPU();
            this.TestHalconFindLedsProcedure(string.IsNullOrEmpty(has3G) ? CAMERA_CPU_PSU : CAMERA_SBT, "CDC_" + has3G + "AutoSupply_CPU", has3G + "LEDS_CPU", "CDC", 3, 
                (step) => 
                {
                    ActivateLedsCPU();
                    Delay(1000, "");
                });
        }
         
        public override void TestVerificationSBT(int initCount = 3, int samples = 8, int timeInterval = 1200)
        {
            base.TestVerificationSBT(initCount, samples, timeInterval);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            WaitAllTasksAndRunOnlyOnce(InstanceNumber, "MTE_TEST_POWER_VERIFICATION_OFF_SBT", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0, 50, 0, new TriLineValue() { L1 = 0, L2 = 120, L3 = 240 }); });

            tower.IO.DO.Off(OUTPUT_DIRECT_CURRENT_SBT_1);
        }
 
        public void TestPLCfromMeter()
        {
            internalTestPLCfromMeter("L1", 15, "CIR0000000001");
            internalTestPLCfromMeter("L2", 1, 15, 30, true, "CIR0000000002");
            internalTestPLCfromMeter("L3", 1, 15, 30, true, "CIR0000000003");
        }

        public override void DeleteNodesRestart()
        {
            base.DeleteNodesRestart();

            tower.IO.DO.On(OUT_ALIMENTACION_CONTADORES);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            Delay(2000, "Esperamos se apague el equipo");

            tower.PowerSourceIII.ApplyAndWaitStabilisation();

            if (ZonaHoraria == "NO")
                dateTimeLocalRestart = DateTime.Now;
            else
            {
                var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
                dateTimeLocalRestart = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            }
        }

        public override void TestFinish()
        {
            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    tower.IO.DO.On(OUT_PLC_NEUTRO);
                    tower.IO.DO.Off(POWER_SUPPLY_TTLRS232);

                    if (string.IsNullOrEmpty(TestInfo.NumSerie))
                        tower.IO.DO.On(PILOTO_ERROR);

                    Delay(1500, "Espera de equipo malo");

                    tower.IO.DO.Off(OUT_PLC_NEUTRO);
                    tower.IO.DO.Off(PISTON_GENERAL);
                }
            }
            base.TestFinish();
        }
    }
}
