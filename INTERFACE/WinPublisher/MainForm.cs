﻿using AssemblyResolve;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace WinPublisher
{
    public partial class MainForm : Form
    {
        private int progress;

        private List<FileAssemblyItem> listFileUpdate;

        public ProjectItem Project { get; set; }

        public List<FileAssemblyItem> ListFileUpdate
        {
            get
            {
                if (listFileUpdate == null)
                    listFileUpdate = new List<FileAssemblyItem>();

                return listFileUpdate;
            }
        }

        public string ApplicationName { get; set; }

        public string TargetPath { get; set; }

        public string SourcePath { get; set; }

        public string TargetPathAssembly
        {
            get
            {
                return Path.Combine(TargetPath, Project.CurrentVersion);
            }
        }

        public string UserPath
        {
            get
            {
                return string.Format("C:\\Users\\{0}\\Documents\\", Environment.UserName);
            }
        }

        public int numVersionSync { get; set; }

        //******************************************
        public MainForm(string argument)
        {
            InitializeComponent();
            ApplicationName = argument;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadProjects();
        }

        private void LoadProjects()
        {
            btPublish.Enabled = false;
            progressBar1.Value = 0;

            Project = Tools.LoadProject(ApplicationName, Application.StartupPath);
            if (Project == null)
            {
                MessageBox.Show(string.Format("Error no se encuentra el projecto {0} en el fichero app.json", ApplicationName), "ERROR AL CARGAR PATH PROYECTO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                return;
            }

            SourcePath = UserPath + Project.SourcePath;
            TargetPath = Project.TargetPath;

            string ApplicationFile = Path.Combine(SourcePath, Project.ApplicationFile.NameAssembly);
            if (!File.Exists(ApplicationFile))
            {
                MessageBox.Show(string.Format("Error no se encuentra el fichero {0} en la ruta {1}", Project.ApplicationFile, SourcePath), "ERROR AL CARGAR PATH PROYECTO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                return;
            }

            if (!Directory.Exists(Project.TargetPath))
                Directory.CreateDirectory(Project.TargetPath);

            var asm = AssemblyName.GetAssemblyName(ApplicationFile);
            Project.CurrentVersion = asm.Version.ToString();

            lblNameProject.Text = Project.ApplicationFile.NameAssembly;
            lblVersionProject.Text = Project.CurrentVersion;
            lblSourcePath.Text = SourcePath;
            lblTargetPath.Text = TargetPathAssembly;

            timer1.Enabled = true;
        }

        //******************************************

        private void timer1_Tick(object sender, EventArgs e)
        {
            progress = 0;
            timer1.Enabled = false;
            ComparaFicherosFileAssemblys(Project.LoadAssemblies);
            dataGridView1.DataSource = ListFileUpdate;
            dataGridView1.Refresh();
        }

        private void ComparaFicherosFileAssemblys(List<FileAssemblyItem> loadAssemblies)
        {
            var fileCompare = Path.Combine(TargetPath + "\\current.json");

            progressBar1.Maximum = loadAssemblies.Count() - 1;

            if (File.Exists(fileCompare))
            {
                var vefilesAssembly = Tools.LoadFile(fileCompare);

                foreach (var assemblies in loadAssemblies)
                {
                    FileAssemblyItem fileAssemblyTarget = null;

                    UpdateUI(assemblies.NameAssembly);

                    var fileAssemblyItem = vefilesAssembly.Where((p) => p.NameAssembly == assemblies.NameAssembly).FirstOrDefault();
                    var assemblyTargetPath = Path.Combine(TargetPathAssembly, assemblies.NameAssembly);

                    if (fileAssemblyItem != null && File.Exists(assemblyTargetPath))
                    {
                        var asmassemblyTarget = AssemblyName.GetAssemblyName(assemblyTargetPath);
                        fileAssemblyTarget = CompareVersionAssemblyClases(assemblies, asmassemblyTarget.Version.ToString());
                    }
                    else
                        fileAssemblyTarget = Tools.AddAssemblyTarget(SourcePath, assemblies);

                    ListFileUpdate.Add(fileAssemblyTarget);

                    txtLog.Text += string.Format("FINISH Assembly: {0}  \r\n", assemblies.NameAssembly);
                    txtLog.Text += "--------------------  \r\n";

                    txtLog.SelectionStart = txtLog.TextLength;
                    txtLog.ScrollToCaret();

                    continue;
                }
            }
            else
            {
                foreach (var assemblies in loadAssemblies)
                {
                    UpdateUI(assemblies.NameAssembly);

                    txtLog.Text += string.Format("--> ADD  \r\n", assemblies.NameAssembly);

                    var fileAssemblyTarget = Tools.AddAssemblyTarget(SourcePath, assemblies);
                    ListFileUpdate.Add(fileAssemblyTarget);

                    txtLog.Text += string.Format("FINISH Assembly: {0}  \r\n", assemblies.NameAssembly);
                    txtLog.Text += "--------------------  \r\n";
                }
            }
        }
   
        private FileAssemblyItem CompareVersionAssemblyClases(FileAssemblyItem assemblies, string versionCurrent)
        {
            var versionClasesSource = new List<ClassVersion>();
            var versionClasesTarget = new List<ClassVersion>();
            var pathSource = Path.Combine(SourcePath, assemblies.NameAssembly);
            var pathTarget = Path.Combine(TargetPathAssembly, assemblies.NameAssembly);

            if (!assemblies.ControlClassversion)
            {
                var asmSource = AssemblyName.GetAssemblyName(pathSource);
                var classVersionSource = new ClassVersion() { NameClass = asmSource.Name, Version = asmSource.Version.ToString() };
                versionClasesSource.Add(classVersionSource);

                var asmTarget = AssemblyName.GetAssemblyName(pathTarget);
                var classVersionTarget = new ClassVersion() { NameClass = asmTarget.Name, Version = asmTarget.Version.ToString() };
                versionClasesTarget.Add(classVersionTarget);
            }
            else
            {
                versionClasesSource = new IsolatedInvoker().Invoke(pathSource);
                versionClasesTarget = new IsolatedInvoker().Invoke(pathTarget);
            }

            var AddClassAssembly = true;
            bool incrementVersion = true;

            if (versionClasesTarget != null && versionClasesTarget.Count() == versionClasesSource.Count())
            {
                var _txtLog = "";

                foreach (ClassVersion classVersion in versionClasesSource)
                {
                    _txtLog += string.Format("Clase: {0} Source Version: {1} \r\n", classVersion.NameClass, classVersion.Version);

                    var classVersionTarget = versionClasesTarget.Where((p) => p.NameClass == classVersion.NameClass);
                    if (classVersionTarget.Count() == 0)
                    {
                        _txtLog += string.Format("No exist in Target \r\n");
                        txtLog.Text += _txtLog + "!!! DIFERENT !!!";
                        AddClassAssembly = true;
                        break;
                    }
                    else
                        _txtLog += string.Format("Clase: {0} Target Version: {1} \r\n", classVersionTarget.FirstOrDefault().NameClass, classVersionTarget.FirstOrDefault().Version);

                    var nameTarget = classVersionTarget.FirstOrDefault().NameClass;
                    var versionTarget = classVersionTarget.FirstOrDefault().Version;
                    if (versionTarget == classVersion.Version)
                        AddClassAssembly = false;
                    else
                    {
                        if (nameTarget == assemblies.NameAssembly.Replace(".dll","").Trim())
                            incrementVersion = false;

                        txtLog.Text += _txtLog + "!!! DIFERENT !!!";
                        AddClassAssembly = true;
                        break;
                    }

                    txtLog.SelectionStart = txtLog.TextLength;
                    txtLog.ScrollToCaret();
                }
            }         
            return Tools.AddAssemblyTarget(SourcePath, assemblies, AddClassAssembly, versionCurrent, versionClasesSource, incrementVersion);
        }

        private void UpdateUI(string file)
        {
            txtLog.Text += "-------------------- \r\n";
            txtLog.Text += string.Format("START Assembly: {0}  \r\n", file);

            progressBar1.Value = progress++;

            txtLog.SelectionStart = txtLog.TextLength;
            txtLog.ScrollToCaret();

            Application.DoEvents();
        }

        //*******************************************

        private void btPublish_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(TargetPathAssembly))
                Directory.CreateDirectory(TargetPathAssembly);

            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                var listItemCollection = ListFileUpdate.Where((p) => p.NameAssembly == r.Cells[1].Value.ToString());
                if (listItemCollection != null)
                {
                    var listItem = listItemCollection.FirstOrDefault();
                    DataGridViewCheckBoxCell ck = r.Cells["UPDATE"] as DataGridViewCheckBoxCell;
                    if (!Convert.ToBoolean(ck.Value))
                        listItem.Update = false;
                    else
                        listItem.CurrentVersion = listItem.NewVersion;
                }
            }

            var directory = new DirectoryInfo(SourcePath);

            var listDirectories = Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories);

            foreach (var dir in listDirectories)
                Directory.CreateDirectory(dir.Replace(SourcePath, TargetPathAssembly));   
                     
            var listInnerFiles = Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories).ToList();

            progressBar1.Maximum = listInnerFiles.Count;
            progress = 0;

            var body = new StringBuilder();
            body.AppendLine(string.Format("    PUBLICACION DE SYNCRONIZACION ->  {0} v{1}     ", Project.Name, Project.CurrentVersion));
            body.AppendLine("**********************************************************************************");
            body.AppendLine("**********************************************************************************");
            body.AppendLine("                                                         ");

            body.AppendLine("                 LIBRERIAS AFECTADAS Y CAMBIOS REALIZADOS                         ");
            body.AppendLine("----------------------------------------------------------------------------------");
            body.AppendLine("                                                          ");

            var commits = RepositoryGit.ReadCommits();
            if (commits == null)
                body.AppendLine(string.Format("NO SE HA ENCONTRADO NINGUN COMMIT ASSOCIADO A ESTA SYNCRONIZACION"));

            foreach (var file in listInnerFiles)
            {
                var fileName = Path.GetFileName(file);
                UpdateUI(fileName);
                var fileUpdate = this.ListFileUpdate.Where((r) => r.NameAssembly == fileName).FirstOrDefault();
                if (fileUpdate == null || fileUpdate.Update)
                {
                    try
                    {
                        if (fileUpdate != null && fileUpdate.Update)
                        {
                            body.AppendLine(string.Format("ACTUALIZACION -> Proyecto: {0} Version: {1} ", fileUpdate.NameAssembly, fileUpdate.NewVersion));

                            if (commits != null)
                            {
                                var commitFind = commits.Where((t) => t.ToUpper().Contains(fileUpdate.NameAssembly.ToUpper())).ToList();
                                foreach (string item in commitFind)
                                {
                                    body.AppendLine(item);
                                }
                                if (fileUpdate.ControlClassversion && fileUpdate.Clases != null)
                                {
                                    foreach (ClassVersion classes in fileUpdate.Clases)
                                    {
                                        var commitFindClasses = commits.Where((t) => t.ToUpper().Contains(classes.NameClass.ToUpper())).ToList();

                                        if (commitFindClasses.Count > 0)
                                        {
                                            body.AppendLine(string.Format("Clase: {0} Version: {1} ", classes.NameClass, classes.Version));
                                            body.AppendLine("--------------------------------------------------");
                                        }
                                        foreach (string commitClase in commitFindClasses)
                                        {
                                            body.AppendLine(commitClase);
                                        }
                                    }
                                }
                            }

                            if (fileUpdate.ControlClassversion && ApplicationName.Trim().ToUpper() == "WINTESTPLAYER" && fileUpdate.IncrementVersion)
                            {
                                var newVersion = new ChangeVersionAssemblyInfo();
                                fileUpdate.NewVersion = newVersion.IncreaseFileVersionBuild(UserPath, fileUpdate);
                                RepositoryGit.Commit(fileUpdate);
                                body.AppendLine(string.Format("ACTUALIZACION (SUBIMOS VERSION) -> Proyecto: {0} Version: {1} ", fileUpdate.NameAssembly, fileUpdate.NewVersion));
                            }
                        }
                        Application.DoEvents();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("Proceso finalizado con excepcion por: {0}", ex.Message), "Publish project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }

            System.Threading.Thread.Sleep(1000);
            System.GC.Collect();
            System.Threading.Thread.Sleep(1000);

            if (!CompilerAssembly.CompilerSolution())
            {
                MessageBox.Show("Proceso finalizado con excepcion al COMPILAR con MSBUILD", "Publish project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            progress = 0;

            foreach (var file in listInnerFiles)
            {
                var fileName = Path.GetFileName(file);
                UpdateUI(fileName);
                var fileSource = file;
                var FileTarget = file.Replace(SourcePath, TargetPathAssembly);
                File.Copy(fileSource, FileTarget, true);
                Application.DoEvents();
            }

            var fileCompare = Path.Combine(TargetPath + "\\current.json");
            Tools.Save(fileCompare, ListFileUpdate);

            SendMail.EnviarCorreo("SYNCRONIZACION TEST STUDIO DEZAC (TSD)", body.ToString());

            if (ApplicationName.Trim().ToUpper() == "WINTESTPLAYER")
            {
                var changeLoggPathh = SourcePath.Replace("\\bin\\Debug", "");
                var changeLogg = Path.Combine(changeLoggPathh + "\\changeLogg.txt");
                File.WriteAllText(changeLogg, body.ToString());

                RepositoryGit.CommitSync(changeLogg);
                try
                {
                    RepositoryGit.Push();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Proceso finalizado con excepcion al realizar PUSH por {0}", ex.Message), "Publish project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
            dataGridView1.DataSource = null;
            dataGridView1.Refresh();
            this.listFileUpdate = null;
            timer1.Enabled = true;

            MessageBox.Show("Proceso finalizado satisfactoriamente!", "Publish project", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //*******************************************

        #region Botton Events Secundary

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dataGrid = sender as DataGridView;
            var listItem = dataGrid.Rows[e.RowIndex].DataBoundItem as FileAssemblyItem;
            if (listItem == null)
                return;

            if (listItem.Update)
            {
                e.CellStyle.BackColor = Color.Red;
                btPublish.Enabled = true;
            }
            else
                e.CellStyle.BackColor = Color.White;
        }

        private void bs_CurrentChanged(object sender, EventArgs e)
        {
            var item = bs.Current as FileAssemblyItem;

            btPublish.Enabled = item != null && item.NewVersion != item.CurrentVersion;
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            Process.Start(lblSourcePath.Text);
        }

        private void toolStripLabel2_Click(object sender, EventArgs e)
        {
            var item = bs.Current as FileAssemblyItem;

            Process.Start(lblTargetPath.Text);
        }

        private void toolStripLabel3_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion
    }
}

