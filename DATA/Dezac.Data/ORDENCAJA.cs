namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ORDENCAJA")]
    public partial class ORDENCAJA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMORDEN { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMCAJA { get; set; }

        public int UDSCAJA { get; set; }

        public DateTime FECHA { get; set; }

        public virtual ORDEN ORDEN { get; set; }
    }
}
