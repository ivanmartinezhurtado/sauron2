﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Metering;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.50)]
    public class CEMC20Test: TestBase
    {
        private const int ILUMINACION_CAMARA = 51;
        private const int OUT_LAMBDA = 20;

        private const int INPUT_MUX_IMPULSE_OUT = 2;
        private const int IN_DETECT_UUT = 24;
        private const int IN_DOOR_OPEN_UUT = 25;
        private const int IN_PRECINTO_UUT = 26;
        private const int IN_BLOQ_UUT = 30;
        private const int IN_UP_UUT = 31;
        private const int IN_DOT_TEST_1_UUT = 32;
        private const int IN_DOT_TEST_2_UUT = 33;
        private const int IN_IMPULSE_UUT = 34;
        private const int IN_SUPPLY_PISTON = 35;

        private const int OUT_DOT_TEST_UUT = 5;
        private const int OUT_RS485 = 7;
        private const int OUT_IMPULSE_UUT = 8;
        private const int KEY1 = 9;
        private const int KEY2 = 10;
        private const int OUT_PRECINTO_UUT = 11;

        private const int OUT_TiPO_IMPULSE_UUT = 21;
        private const int OUT_ACTIVE_IMPULSE_INPUT = 43;


        private const int ERROR_LIGHT = 48;

        private Tower3 tower;
        private MeterType meter;

        private CEMC20.Energy EnergyRead;
        private CEMC20 cem;

        private string CAMERA_DISPLAY;
        internal string modelo { get; set; }

        public void TestInitialization(int portCEM)
        {
            if (portCEM == 0)
                portCEM = Comunicaciones.SerialPort;

            cem = AddInstanceVar(new CEMC20(portCEM, Comunicaciones.BaudRate), "UUT");
            cem.Modbus.PerifericNumber = Comunicaciones.Periferico;
            cem.Modbus.TimeOut = 2000;
            ((ModbusDeviceSerialPort)cem.Modbus).RtsEnable = true;
            ((ModbusDeviceSerialPort)cem.Modbus).DtrEnable = true;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            meter = new MeterType(Identificacion.MODELO);
            cem.Frequency = meter.FrequencyProperty.Frequency == 50D ? TypeMeterConection.frequency._50Hz : TypeMeterConection.frequency._60Hz;
            modelo = string.Format("CEMC{0}0", meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO ? "2" : "3");

            //DIGITAL_OUTPUTS = 0x0021; //SOY CEM-21 / 31  y esta direccion de memoria cambia
            cem.MODELO_HARDWARE = Identificacion.MODELO_HARDWARE.Trim();
            logger.InfoFormat("MODELO_HARDWARE: {0}", Identificacion.MODELO_HARDWARE.Trim());

            tower.MTEPS.Output120A = meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO;
            tower.MTEWAT.Output120A = meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO;
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            if (tower.MTEPS.TypeSource == MTEPPS.typeSourceEnum.SPE120 && meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO)
                tower.ActiveCurrentHighLowUnionMTE(true);

            tower.WaitBloqSecurity(2000);

            tower.ActiveElectroValvule();

            if (!tower.IO.DI.WaitOn(IN_DOOR_OPEN_UUT, 2000))
                Shell.MsgBox("ATENCION PUERTA DEL UTIL ABIERTA", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DOOR_OPEN_UUT, 2000))
                throw new Exception("Error  puerta del util abierta");

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                Shell.MsgBox("ATENCION NO SE HA DETECTADO EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                throw new Exception("Error No se ha detectado el equipo");

            //if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
            //    Shell.MsgBox("ATENCION NO SE HA DETECTADO EL PRECINTO DEL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
            //    throw new Exception("Error Precinto no detectado");
         
            tower.OnOutAndWaitInput(OUT_DOT_TEST_UUT, 2000, IN_DOT_TEST_1_UUT, IN_DOT_TEST_2_UUT);
            tower.OnOutAndWaitInput(OUT_IMPULSE_UUT, 2000, IN_IMPULSE_UUT);

            if (Comunicaciones.Tipo == "RS485")
                tower.IO.DO.OnWait(500, OUT_RS485);
            
            tower.MTEPS.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.5, ParamUnidad.PorCentage);
            tower.MTEPS.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 35000, ParamUnidad.ms);
        }

        public void TestInitializationKeyboardFatigue(int portCEM)
        {
            if (portCEM == 0)
                portCEM = Comunicaciones.SerialPort;

            cem = AddInstanceVar(new CEMC20(portCEM, Comunicaciones.BaudRate), "UUT");
            cem.Modbus.PerifericNumber = Comunicaciones.Periferico;
            cem.Modbus.TimeOut = 2000;
            ((ModbusDeviceSerialPort)cem.Modbus).RtsEnable = true;
            ((ModbusDeviceSerialPort)cem.Modbus).DtrEnable = true;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();

            Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tower.WaitBloqSecurity(2000);

            tower.ActiveElectroValvule();

            if (!tower.IO.DI.WaitOn(IN_DOOR_OPEN_UUT, 2000))
                Shell.MsgBox("ATENCION PUERTA DEL UTIL ABIERTA", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DOOR_OPEN_UUT, 2000))
                throw new Exception("Error  puerta del util abierta");

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                Shell.MsgBox("ATENCION NO SE HA DETECTADO EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                throw new Exception("Error No se ha detectado el equipo");

            if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
                Shell.MsgBox("ATENCION NO SE HA DETECTADO EL PRECINTO DEL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
                throw new Exception("Error Precinto no detectado");

            tower.OnOutAndWaitInput(OUT_DOT_TEST_UUT, 2000, IN_DOT_TEST_1_UUT, IN_DOT_TEST_2_UUT);
            tower.OnOutAndWaitInput(OUT_IMPULSE_UUT, 2000, IN_IMPULSE_UUT);

            if (Comunicaciones.Tipo == "RS485")
                tower.IO.DO.OnWait(500, OUT_RS485);

            tower.MTEPS.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.5, ParamUnidad.PorCentage);
            tower.MTEPS.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 35000, ParamUnidad.ms);
        }

        public void TestComunicaciones()
        {
            SamplerWithCancel((p) => { cem.WriteErrorCode(9999); return true; }, "Error comunicaciones, no hemos recibido respuesta del equipo");

            var version = cem.ReadSoftwareVersion();

            string[] versionBBDD = Identificacion.VERSION_FIRMWARE.Split('.');

            Assert.IsTrue(version.Major == Convert.ToUInt16(versionBBDD[0]) && version.Minor == Convert.ToUInt16(versionBBDD[1]) && version.Revision == Convert.ToUInt16(versionBBDD[2]), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, Versión de Firmware del equipo diferenete a la de la BBDD"));

            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Revision.ToString(), ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

            var crc = cem.ReadCRCFirmware();

            Resultado.Set(ConstantsParameters.Identification.CRC_FIRMWARE, crc.ToString(), ParamUnidad.SinUnidad, Identificacion.CRC_FIRMWARE.ToString());

            Assert.IsTrue(crc == Identificacion.CRC_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, CRC del Firmware del equipo diferenete a la de la BBDD"));

            cem.WriteModel(Identificacion.MODELO);
        }

        public void TestAjusteReloj()
        {
            tower.IO.DO.On(OUT_LAMBDA);
            tower.LAMBDA.Initialize();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12, 0.1);
            tower.IO.DO.On(OUT_IMPULSE_UUT);

            cem.WriteDigitalOutputs(255); //8

            tower.HP53131A
       .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
       .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.5) //1.5 Level pasar a Double
       .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 7);

            var defDesviacion = new AdjustValueDef() { Name = Params.PPM.Other("CLOCK").TestPoint("DESVIATION").Name, Min = Params.PPM.Other("CLOCK").TestPoint("DESVIATION").Min(), Max = Params.PPM.Other("CLOCK").TestPoint("DESVIATION").Max(), Unidad = ParamUnidad.ppm };
            
            var gainIni = (ushort)Configuracion.GetDouble(Params.PPM.Other("GAIN_INI_CLOCK").Name, 12, ParamUnidad.Puntos);
            var gainFinal = (ushort)Configuracion.GetDouble(Params.PPM.Other("GAIN_FINAL_CLOCK").Name, 16, ParamUnidad.Puntos);

            double medidaFreq = 0.0;
            double mejorFreq = 0.0;
            double desviacionFreq = 500;
            ushort? gain = null;

            for (ushort i = gainIni; i <= gainFinal; i++)
            {
                cem.WriteGainOscilator(i);

                medidaFreq = tower.MeasureWithFrecuency(InputMuxEnum.IN2, 20000);

                if (Math.Abs(medidaFreq - 500) < desviacionFreq)
                {
                    desviacionFreq = Math.Abs(medidaFreq - 500);
                    gain = i;
                    mejorFreq = medidaFreq;
                }
            }

            Assert.IsTrue(gain.HasValue, Error().UUT.CONFIGURACION.MARGENES("No se podido ajustar el reloj"));
            cem.WriteGainOscilator(gain.Value);

            Resultado.Set("GANANCIA_RELOJ", gain.Value, ParamUnidad.Puntos);
            Resultado.Set(Params.FREQ.Other("RELOJ").Name, mejorFreq, ParamUnidad.Hz);
        }

        public void TestDisplay()
        {
            int timeWait = 1000;
            
            CAMERA_DISPLAY = GetVariable<string>("CAMERA_DISPLAY", CAMERA_DISPLAY);

            cem.WriteDigitalOutputs(0x0010);

            cem.WriteDisplay(CEMC20.DisplayConfigurations.SEGMENTOS_IMPARES);
            Delay(timeWait, "Espera condicionamiento de los LEDS");
            this.TestHalconMatchingProcedure(CAMERA_DISPLAY, string.Format("{0}_IMPARES", modelo), "IMPARES", modelo);

            cem.WriteDisplay(CEMC20.DisplayConfigurations.SEGMENTOS_PARES);
            Delay(timeWait, "Espera condicionamiento de los LEDS");
            this.TestHalconMatchingProcedure(CAMERA_DISPLAY, string.Format("{0}_PARES", modelo), "PARES", modelo);
        }

        public void TestLeds()
        {
            CAMERA_DISPLAY = GetVariable<string>("CAMERA_DISPLAY", CAMERA_DISPLAY);

            cem.WriteDigitalOutputs(0x1000);

            cem.WriteDigitalOutputs(0x0003);
            Delay(1000, "Espera condicionamiento de los LEDS");
            this.TestHalconFindLedsProcedure(CAMERA_DISPLAY, string.Format("{0}_LED", modelo), "LED", modelo);
        }

        public void TestTeclado()
        {
            tower.IO.DO.Off(KEY1, KEY2);

            SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 0; }, "Error Teclas activadas sin estar pulsadas", 5, 500);

            tower.IO.DO.On(KEY1);

            SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 1; }, "Error Tecla 1 no activada cuando esta pulsada", 5, 500);

            tower.IO.DO.Off(KEY1);
            tower.IO.DO.On(KEY2);

            SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 8; }, "Error Tecla 2 no activada cuando esta pulsada", 5, 500);

            tower.IO.DO.OffWait(500, KEY2);

            tower.IO.DO.PulseOn(500, OUT_PRECINTO_UUT);

            Delay(500, "tiempo espera piston");

            if (!tower.IO.DI[IN_PRECINTO_UUT])
            {
                tower.IO.DO.OnWait(1000, KEY2);

                SamplerWithCancel((p) => { return cem.ReadDigitalInputsAndKeyboard() == 0; }, "Error Tecla 2 activada cuando esta con precinto ", 3, 1000);
            }else
                throw new Exception("Error Precinto se ha detectado cuando se ha activado el piston de entrada de precinto");
   
            tower.IO.DO.Off(KEY2);
        }

        public void TestSalidaImpulsos()
        {
            tower.IO.DO.Off(OUT_TiPO_IMPULSE_UUT); // rele para que se indica que tipo de impulsos es en OFF es salidas

            tower.IO.DO.On(OUT_LAMBDA);
            tower.LAMBDA.Initialize();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12, 0.1);
            tower.IO.DO.On(OUT_IMPULSE_UUT);

            cem.WriteDigitalOutputs(0x1000);

            var defsVoltageOutputPulse = new AdjustValueDef() { Name = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("OFF").Name, Min = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("OFF").Min(), Max = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("OFF").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageOutputPulse,
              (step) =>
              {
                  return tower.MeasureMultimeter((InputMuxEnum)INPUT_MUX_IMPULSE_OUT, MagnitudsMultimeter.VoltDC);
              }, 0, 3, 500);

            cem.WriteDigitalOutputs(4);

            defsVoltageOutputPulse = new AdjustValueDef() { Name = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("ON").Name, Min = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("ON").Min(), Max = Params.V_DC.Other("OUTPUT_PULSE").TestPoint("ON").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageOutputPulse,
              (step) =>
              {
                  return tower.MeasureMultimeter((InputMuxEnum)INPUT_MUX_IMPULSE_OUT, MagnitudsMultimeter.VoltDC);
              }, 0, 3, 500);

            cem.WriteDigitalOutputs(0);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            tower.IO.DO.Off(OUT_LAMBDA);
        }

        public void TestEntradImpulsos()
        {
            cem.WriteReset();
            Delay(1500, "Espera para la realización del reset");

            cem.WriteDigitalInputImpulsesActivate();

            tower.IO.DO.On(OUT_TiPO_IMPULSE_UUT); // rele para que se indica que tipo de impulsos es en On es entradas
            logger.InfoFormat("Activamos rele para ewl tipo de impulsos com entrada");

            var impulsos = cem.ReadNumImpulseInput();
            logger.InfoFormat("Numero de impulsos iniciales = {0}", impulsos);

            var numImpulsos = Margenes.GetDouble("NUMERO_IMPULSOS_INICIALES", 1, ParamUnidad.Pulse);
            logger.InfoFormat("Margen de impulsos iniciales= {0}", numImpulsos);

            if (impulsos > numImpulsos)
                Error().UUT.HARDWARE.DIGITAL_INPUTS("IMPULSOS INICIALES FUERA DE MARGENES").Throw();

            for (var i = 0; i<=5; i++)
            {
                tower.IO.DO.PulseOn(500, OUT_ACTIVE_IMPULSE_INPUT); // Creamos un pulso
                logger.InfoFormat("Creamos un pulso de 500ms con el rele {0}", OUT_ACTIVE_IMPULSE_INPUT);

                var impulsosTmp = cem.ReadNumImpulseInput();
                logger.InfoFormat("Numero de impulsos leidos = {0} en la iteracion {1}", impulsosTmp, i);
            }


            var impulsosVerif = cem.ReadNumImpulseInput();
            logger.InfoFormat("Numero de impulsos leidos finales= {0} en la iteracion", impulsosVerif);

            if (impulsosVerif < 1)
                Error().UUT.HARDWARE.DIGITAL_INPUTS("NO SE HA DETECTADO NINGUN PULSO").Throw();

            if (impulsosVerif <= Margenes.GetDouble("NUMERO_IMPULSOS_LEIDOS", 5, ParamUnidad.Pulse))
                Error().UUT.HARDWARE.DIGITAL_INPUTS("IMPULSOS LEIDOS FUERA DE MARGENES").Throw();
        }

        public void TestConfiguracionInicial()
        {
            cem.WriteBastidor((int)TestInfo.NumBastidor.Value);
            EnergyRead = cem.ReadEnergy();
            logger.InfoFormat("Energia leida = {0}", EnergyRead);
            cem.WriteGainOffsetActive(new CEMBase.TriLineMeasure() { L1 = 0, L2 = 0, L3 = 0 });
            cem.WriteGainOffsetReactive(new CEMBase.TriLineMeasure() { L1 = 0, L2 = 0, L3 = 0 });
        }

        public void TestTensionNeutro()
        {
            var consignaTensionNeutro = Consignas.GetDouble(Params.V.Null.TestPoint("NEUTRO").Name, 0.5, ParamUnidad.V);
            var consignaCorrienteNeutro = Consignas.GetDouble(Params.I.Null.TestPoint("NEUTRO").Name, 5, ParamUnidad.A);
            var voltage = new TriLineValue { L1 = 230, L2 = 230, L3 = consignaTensionNeutro };
            var current = new TriLineValue { L1 = consignaCorrienteNeutro, L2 = consignaCorrienteNeutro, L3 = consignaCorrienteNeutro };

            tower.MTEPS.ApplyPresetsAndWaitStabilisation(voltage, current, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
            
            var teoricVoltage = new TriAdjustValueDef(Params.V.L1.TestPoint("NEUTRO").Name, Params.V.L1.TestPoint("NEUTRO").Min(), Params.V.L1.TestPoint("NEUTRO").Max(), 0, 0, ParamUnidad.V);
            teoricVoltage.L3.Max = Params.V.L3.TestPoint("NEUTRO").Max();
            teoricVoltage.L3.Min = Params.V.L3.TestPoint("NEUTRO").Min();

            TestMeasureBase(teoricVoltage,
                (step) =>
                {
                    var vars = cem.ReadVoltage();
                    return new double[] { vars.L1 / 10D, vars.L2 / 10D, vars.L3 / 10D };

                }, 0, 3, 1000);

            tower.MTEPS.ApplyOffAndWaitStabilisation();
        }

        public void TestAjusteDesfase(byte Retry=2, int delFirst = 1, int initCount = 3, int samples = 6, int interval = 1100)
        {
            var description = string.Format("{0}_{1}Hz", Params.GAIN_DESFASE.L1.Name, meter.FrequencyProperty.Frequency.ToString());

            var GainDefaultL1 = Configuracion.GetDouble(description, -131, ParamUnidad.Puntos);
            var GainDefaultL2 = Configuracion.GetDouble(description.Replace("_L1", "_L2"), -105, ParamUnidad.Puntos);
            var GainDefaultL3 = Configuracion.GetDouble(description.Replace("_L1", "_L3"), -105, ParamUnidad.Puntos);

            cem.WriteGainsGap(new CEMBase.TriLineGain() { L1 = (ushort)GainDefaultL1, L2 = (ushort)GainDefaultL2, L3 = (ushort)GainDefaultL3 });

            SamplerWithCancel((p) =>
            {
                var gain = internalTestAjusteDesfase(description, delFirst, initCount, samples, interval);
                cem.WriteGainsGap(gain);

                if (p >= Retry)
                    return true;

                return false;
            }, "", (byte)(Retry + 2), 1100, 2000, false, false);
        }

        private CEMC20.TriLineGain internalTestAjusteDesfase(string description, int delFirst = 1, int initCount = 5, int samples = 10, int interval = 1100)
        {
            var anguloAjusteDesfase = Consignas.GetDouble("ANGULO_DESFASE_AJUSTE", 60, ParamUnidad.Grados);

            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase, anguloAjusteDesfase, meter.FrequencyProperty.Frequency, 1);

            var pf = tower.MTEWAT.ReadPowerFactor().Value;

            var valorAjuste = new TriAdjustValueDef(description, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var resultadoAjuste = cem.CalculateGapAdjustMeasureFactors(delFirst, initCount, samples, interval, pf,
            (value) =>
            {
                valorAjuste.L1.Value = Convert.ToDouble((short)value.L1);
                valorAjuste.L2.Value = Convert.ToDouble((short)value.L2);
                valorAjuste.L3.Value = Convert.ToDouble((short)value.L3);
                return !valorAjuste.HasMinMaxError();
            });

            valorAjuste.AddToResults(Resultado);

            if (valorAjuste.HasMinMaxError())
                throw new Exception("Error ajuste de Desfase, fuera de margenes");

            return resultadoAjuste.Item2;
        }

        public void TestAjusteVoltage(int delFirst = 1, int initCount = 3, int samples = 6, int interval = 1100)
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase, 0, meter.FrequencyProperty.Frequency);

            var description = string.Format("{0}_{1}Hz", Params.GAIN_V.L1.Ajuste.Name, meter.FrequencyProperty.Frequency.ToString());

            var valoresAjuste = new TriAdjustValueDef(description, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var voltageReal = tower.MTEWAT.ReadVoltage().Value;

            var resultadoAjuste = cem.CalculateVoltageAdjustMeasureFactors(delFirst, initCount, samples, interval, voltageReal,
                (value) =>
                {
                    valoresAjuste.L1.Value = value.L1;
                    valoresAjuste.L2.Value = value.L2;
                    valoresAjuste.L3.Value = value.L3;
                    return !valoresAjuste.HasMinMaxError();
                });

            valoresAjuste.AddToResults(Resultado);

            if (valoresAjuste.HasMinMaxError())
                throw new Exception("Error ajuste de Voltage, fuera de margenes");

            cem.WriteGainsVoltage(resultadoAjuste.Item2);
        }

        public void TestAjustePotencia(byte Retry = 2, bool AjusteReactiva = false, int delFirst = 1, int initCount = 3, int samples = 6, int interval = 1100)
        {
            var description = string.Format("{0}_{1}Hz", Params.GAIN_KW.L1.Ajuste.Name, meter.FrequencyProperty.Frequency.ToString());

            var GainDefaultL1 = Convert.ToUInt16(Configuracion.GetDouble(Params.GAIN_KW.L1.Name, 0, ParamUnidad.Puntos));
            var GainDefaultL2 = Convert.ToUInt16(Configuracion.GetDouble(Params.GAIN_KW.L2.Name, 0, ParamUnidad.Puntos));
            var GainDefaultL3 = Convert.ToUInt16(Configuracion.GetDouble(Params.GAIN_KW.L3.Name, 0, ParamUnidad.Puntos));

            cem.WriteGainsActive(new CEMBase.TriLineGain() { L1 = GainDefaultL1, L2 = GainDefaultL2, L3 = GainDefaultL3 });

            CEMC20.TriLineGain gainActive = new CEMBase.TriLineGain();

            SamplerWithCancel((p) =>
            {
                gainActive = InternalAjustePotencia(description, delFirst, initCount, samples, interval);
                cem.WriteGainsActive(gainActive);

                if (p >= Retry)
                    return true;

                return false;
            }, "", (byte)(Retry + 2), 1200, 1000, false, false);

            if (AjusteReactiva)
            {
                description = string.Format("{0}_{1}Hz", Params.GAIN_KVAR.L1.Ajuste.Name, meter.FrequencyProperty.Frequency.ToString());

                GainDefaultL1 = Convert.ToUInt16(Configuracion.GetDouble(Params.GAIN_KVAR.L1.Name, 0, ParamUnidad.Puntos));
                GainDefaultL2 = Convert.ToUInt16(Configuracion.GetDouble(Params.GAIN_KVAR.L2.Name, 0, ParamUnidad.Puntos));
                GainDefaultL3 = Convert.ToUInt16(Configuracion.GetDouble(Params.GAIN_KVAR.L3.Name, 0, ParamUnidad.Puntos));

                cem.WriteGainsReactive(new CEMBase.TriLineGain() { L1 = GainDefaultL1, L2 = GainDefaultL2, L3 = GainDefaultL3 });

                SamplerWithCancel((p) =>
                {
                    var gain = InternalAjustePotencia(description, delFirst, initCount, samples, interval, true);
                    cem.WriteGainsReactive(gain);

                    if (p >= Retry)
                        return true;

                    return false;
                }, "", (byte)(Retry + 2), 1200, 1000, false, false);

            }else
                cem.WriteGainsReactive(gainActive);
        }

        public CEMC20.TriLineGain InternalAjustePotencia(string description, int delFirst = 1, int initCount = 3, int samples = 6, int interval = 1100, bool IsReactive= false)
        {
            var potenciaAparente =meter.VoltageProperty.VoltageNominal * meter.CurrentProperty.CorrienteBase;
            double desfase = IsReactive == false ? 0 : 88;
            var potencia = IsReactive == false ? potenciaAparente * Math.Cos(desfase.ToRadians()) : potenciaAparente * Math.Sin(desfase.ToRadians());
            var MaxGain = IsReactive == false ? Params.GAIN_KW.Null.Ajuste.Max() : Params.GAIN_KVAR.Null.Ajuste.Max();
            var MinGain = IsReactive == false ? Params.GAIN_KW.Null.Ajuste.Min() : Params.GAIN_KVAR.Null.Ajuste.Min();

            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase, desfase, meter.FrequencyProperty.Frequency);

            var valoresAjuste = new TriAdjustValueDef(description, MinGain, MaxGain, 0, 0, ParamUnidad.Puntos);

            var powerReal = IsReactive == false ? tower.MTEWAT.ReadActivePower().Value : tower.MTEWAT.ReadReactivePower().Value;

            var resultadoAjuste = cem.CalculatePowerMeasureFactors(delFirst, initCount, samples, interval, powerReal, IsReactive,
                (value) =>
                {
                    valoresAjuste.L1.Value = value.L1;
                    valoresAjuste.L2.Value = value.L2;
                    valoresAjuste.L3.Value = value.L3;
                    return !valoresAjuste.HasMinMaxError();
                });

            valoresAjuste.AddToResults(Resultado);

            if (valoresAjuste.HasMinMaxError())
                throw new Exception("Error ajuste de Potencia, fuera de margenes");

            return resultadoAjuste.Item2;
        }

        public void TestAjusteOffsetVmin(byte Retry = 2, bool AjusteReactiva = false, int delFirst = 1, int initCount = 3, int samples = 6, int interval = 1100)
        {
            var description = string.Format("{0}_{1}Hz", Params.GAIN_OFFSET.L1.Ajuste.Name, meter.FrequencyProperty.Frequency.ToString());

            cem.WriteGainOffsetActive(new CEMBase.TriLineMeasure() { L1 = 0, L2 = 0, L3 = 0 });
            cem.WriteGainOffsetReactive(new CEMBase.TriLineMeasure() { L1 = 0, L2 = 0, L3 = 0 });

            CEMC20.TriLineMeasure gainOffset = new CEMBase.TriLineMeasure();

            SamplerWithCancel((p) =>
            {
                gainOffset = InternalAjusteOffsetVmin(description, delFirst, initCount, samples, interval);
                cem.WriteGainOffsetActive(gainOffset);
                  return true;
            }, "", (byte)(Retry + 2), 1200, 2000, false, false);
        }

        public CEMC20.TriLineMeasure InternalAjusteOffsetVmin(string description, int delFirst = 1, int initCount = 3, int samples = 6, int interval = 1100, bool IsReactive = false)
        {
            var potenciaAparente = meter.VoltageProperty.VoltageMinimo * meter.CurrentProperty.CorrienteMinima;
            double desfase = IsReactive == false ? 0 : 88;
            var potencia = IsReactive == false ? potenciaAparente * Math.Cos(desfase.ToRadians()) : potenciaAparente * Math.Sin(desfase.ToRadians());
            var MaxGain = IsReactive == false ? Params.GAIN_OFFSET.Null.Ajuste.Max() : Params.GAIN_OFFSET.Null.Ajuste.Max();
            var MinGain = IsReactive == false ? Params.GAIN_OFFSET.Null.Ajuste.Min() : Params.GAIN_OFFSET.Null.Ajuste.Min();

            ConsignarMTE(meter.VoltageProperty.VoltageMinimo, meter.CurrentProperty.CorrienteMinima, desfase, meter.FrequencyProperty.Frequency);

            var valoresAjuste = new TriAdjustValueDef(description, MinGain, MaxGain, 0, 0, ParamUnidad.Puntos);

            var powerReal = IsReactive == false ? tower.MTEWAT.ReadActivePower().Value : tower.MTEWAT.ReadReactivePower().Value;

            var resultadoAjuste = cem.CalculateOffsetFactors(delFirst, initCount, samples, interval, powerReal, IsReactive,
                (value) =>
                {
                    valoresAjuste.L1.Value = value.L1;
                    valoresAjuste.L2.Value = value.L2;
                    valoresAjuste.L3.Value = value.L3;
                    return !valoresAjuste.HasMinMaxError();
                });

            valoresAjuste.AddToResults(Resultado);

            if (valoresAjuste.HasMinMaxError())
                throw new Exception("Error ajuste de Potencia, fuera de margenes");

            return resultadoAjuste.Item2;
        }

        public void TestPoint_MarchaVacio()
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal * 1.15, 0, 0, meter.FrequencyProperty.Frequency, 8);

            var points = cem.ReadPointsActive();
            var gain = cem.ReadGainActive();

            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain.L1, points.L1, Error().UUT.CONSUMO.MARGENES("Error. puntos de potencia L1 leidos mayor a la ganacia el equipo consume energia"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L2.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain.L2, points.L2, Error().UUT.CONSUMO.MARGENES("Error. puntos de potencia L2 leidos mayor a la ganacia el equipo consume energia"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L3.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain.L3, points.L3, Error().UUT.CONSUMO.MARGENES("Error. puntos de potencia L3 leidos mayor a la ganacia el equipo consume energia"), ParamUnidad.Puntos);
        }

        public void TestPoint_Arranque()
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase * 0.04, 0, meter.FrequencyProperty.Frequency, 8);
            
            var points = cem.ReadPointsActive();
            var gain = cem.ReadGainActive();

            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_ARRANQUE").Name, points.L1, gain.L1, Error().UUT.CONSUMO.MARGENES("Error. puntos de potencia L1 leidos menor a la ganacia el equipo no ve la corriente"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_ARRANQUE").Name, points.L2, gain.L2, Error().UUT.CONSUMO.MARGENES("Error. puntos de potencia L2 leidos menor a la ganacia el equipo no ve la corriente"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_ARRANQUE").Name, points.L3, gain.L3, Error().UUT.CONSUMO.MARGENES("Error. puntos de potencia L3 leidos menor a la ganacia el equipo no ve la corriente"), ParamUnidad.Puntos);
        }

        public void TestPoint_Imin([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia, int NumSamplesToVerification = 3, int NumMaxSamplesToAbort=6, int SamplingTime= 1100)
        {
            TestPointCalibration(TestPointsName.TP_IMIN, tipoPotencia, NumSamplesToVerification,NumMaxSamplesToAbort,SamplingTime);     

            if(meter.VoltageProperty.IsMultiRangeVoltage)
                TestPointCalibration(TestPointsName.TP_IMIN, tipoPotencia, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime, meter.VoltageProperty.IsMultiRangeVoltage);     
        }

        public void TestPoint_IbCosPhi(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IbCOS05, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);    
        }

        public void TestPoint_IbSenPhi(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IbSEN05, TipoPotencia.React, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);          
        }

        public void TestPoint_IMax([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia, int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IMAX, tipoPotencia, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);  
        }

        public void TestCustomize()
        {
            tower.MTEPS.ApplyOffAndWaitStabilisation();

            var Energy = cem.ReadEnergy();

            if (Energy.ActiveExport <= EnergyRead.ActiveExport)
                throw new Exception("Error, no se ha incrementado la energia activa exportada del equipo");

            if (Identificacion.MID != "SI")
            {
                if (Configuracion.GetString("NUMERO_SERIE_LARGO", "NO", ParamUnidad.SinUnidad) == "SI")
                {
                    cem.WriteNumSerieASCII(TestInfo.NumSerie);
                    Logger.InfoFormat("Número de serie largo escrito {0}", TestInfo.NumSerie);

                    var id = Identificacion.CABECERA_NUMERO_SERIE.ToString().PadRight(9, '0');
                    cem.WriteNumSerie(Convert.ToUInt32(id));
                    Logger.InfoFormat("Número de ID escrito {0}", id);
                }
                else
                {
                    if (string.IsNullOrEmpty(TestInfo.NumSerie))
                        Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("NO SE HA OBTENIDO UN NÚMERO DE SERIE PARA ESTE PRODUCTO");
                    else
                        cem.WriteNumSerie(Convert.ToUInt32(TestInfo.NumSerie));
                }
            }
            else
            {
                var id = Identificacion.CABECERA_NUMERO_SERIE.ToString().PadRight(9, '0');
                cem.WriteNumSerie(Convert.ToUInt32(id));
                Logger.InfoFormat("Número de ID escrito {0}", id);
            }

            if (Model.modoPlayer != ModoTest.PostVenta)
            {
                cem.WriteReset();
                Delay(1500, "Espera para la realización del reset");
            }

            var codError = Convert.ToUInt16(Identificacion.CODE_ERROR);

            var pesoImpulsos = Convert.ToUInt16(Configuracion.GetDouble("ANCHO_IMPULSOS", 10, ParamUnidad.Whimp));

            if (meter.TipoConexion == MeterType.TipoConexionEnum.INDIRECTO)
            {
                cem.WriteEspecialProgramation();
                cem.WriteWeightimpulseOutput(pesoImpulsos);
            }

            cem.WriteErrorCode(codError);

            Delay(3000, "Espera para la grabación del codigo error");

            tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(1000, "Espera para el apagado del equipo");

            var Voltage = Consignas.GetDouble(Params.V.Null.TestPoint("NOMINAL").Name, 230, ParamUnidad.V);
            if (Voltage > 300)
            {
                Voltage /= 2;
                tower.IO.DO.On(13);
            }

            tower.Chroma.ApplyPresetsAndWaitStabilisation(Voltage, 0, 50);

            SamplerWithCancel((p) => { cem.ReadSoftwareVersion(); return true; }, "Error comunicaciones despues del reset de hardware para verificar la grabacion en memoria ", 5, 1000, 1000);

            if (Identificacion.MID != "SI")
            {
                if (Comunicaciones.Tipo == "RS485")
                {
                    var NumSerieReadASCII = cem.ReadNumSerieASCII();
                    Assert.IsTrue(NumSerieReadASCII == TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el Numero de Serie grabado no coincide con el esperado"));
                }
                else
                {
                    var NumSerieRead = cem.ReadNumSerie();
                    Assert.IsTrue(NumSerieRead == Convert.ToUInt32(TestInfo.NumSerie), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el Numero de Serie grabado no coincide con el esperado"));
                }
            }

            var Bastidor = cem.ReadBastidor();
            Assert.IsTrue(Bastidor == (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error el numero de Bastidor grabado no coincide con el esperado"));

            var model = cem.ReadModel();
            Assert.AreEqual(ConstantsParameters.Identification.MODELO, model, Identificacion.MODELO, Error().UUT.HARDWARE.NO_COINCIDE("Error el modelo grabado no coincide con el esperado"), ParamUnidad.SinUnidad);

            if (meter.TipoConexion == MeterType.TipoConexionEnum.INDIRECTO)
            {
                var pesoImpulsoRead = cem.ReadWeightimpulseOutput();
                Resultado.Set("ANCHO_IMPULSOS", pesoImpulsoRead, ParamUnidad.Whimp);
                Assert.IsTrue(pesoImpulsoRead == pesoImpulsos, Error().UUT.HARDWARE.NO_COINCIDE("Error el valor PESO_IMPULSO no corresponde el grabado al leido"));
            }

            if (Model.modoPlayer != ModoTest.PostVenta)
            {
                Energy = cem.ReadEnergy();
                if (Energy.ActiveExport != 0)
                    throw new Exception("Error, no se ha realizado un reset de energias al equipo");
            }

            codError= cem.ReadErrorCode();
            Assert.AreEqual(ConstantsParameters.Identification.CODE_ERROR, codError, Convert.ToUInt16(Identificacion.CODE_ERROR), Error().UUT.HARDWARE.NO_COINCIDE("Error no se ha grabado el codigo de error esperado en el equipo"), ParamUnidad.SinUnidad);
        }

        public void TestConfiguracionFrequency([Description("50=50Hz 60=60Hz")]TypeMeterConection.frequency freq)
        {
            if (meter.FrequencyProperty.IsAutomatico)
            {
                cem.Frequency = freq;
                meter.FrequencyProperty.Frequency = (double)freq;
                tower.MTEPS.ApplyOffAndWaitStabilisation();
            }
        }

        public void TestKeyboardFatigue(int pruebas)
        {
            tower.IO.DO.OffWait(500, KEY1, KEY2);

            ushort keyboardReading = cem.ReadDigitalInputsAndKeyboard();
            Assert.AreEqual(keyboardReading, (ushort)0, Error().UUT.HARDWARE.SETUP("Error, no se detecta el estado inicial de teclas sin pulsar en el test de fatiga del teclado"));

            Sampler.Run(() =>
                {
                    return new SamplerConfig() { CancelOnException = true, ThrowException = true, Interval = 100, NumIterations = pruebas };
                },
                (step) =>
                {
                    Resultado.Set("PASO_TEST_FATIGA", step.Step + 1, ParamUnidad.SinUnidad, max: pruebas);

                    tower.IO.DO.OnWait(150, KEY1);

                    keyboardReading = cem.ReadDigitalInputsAndKeyboard();
                    Assert.AreEqual(keyboardReading, (ushort)1, Error().UUT.HARDWARE.SETUP(string.Format("Error en el test de fatiga del teclado. No se detecta la pulsación de tecla 1 en el test {0} de {1}", step.Step, pruebas)));

                    tower.IO.DO.OffWait(150, KEY1);

                    keyboardReading = cem.ReadDigitalInputsAndKeyboard();
                    Assert.AreEqual(keyboardReading, (ushort)0, Error().UUT.HARDWARE.SETUP(string.Format("Error en el test de fatiga del teclado. No se detecta la liberación de tecla 1 en el test {0} de {1}", step.Step, pruebas)));

                    tower.IO.DO.OnWait(150, KEY2);

                    keyboardReading = cem.ReadDigitalInputsAndKeyboard();
                    Assert.AreEqual(keyboardReading, (ushort)8, Error().UUT.HARDWARE.SETUP(string.Format("Error en el test de fatiga del teclado. No se detecta la pulsación de tecla 2 en el test {0} de {1}", step.Step, pruebas)));

                    tower.IO.DO.OffWait(150, KEY2);

                    keyboardReading = cem.ReadDigitalInputsAndKeyboard();
                    Assert.AreEqual(keyboardReading, (ushort)0, Error().UUT.HARDWARE.SETUP(string.Format("Error en el test de fatiga del teclado. No se detecta la liberación de tecla 2 en el test {0} de {1}", step.Step, pruebas)));
                });

            tower.IO.DO.OffWait(500, KEY1, KEY2);
        }

        public void TestFinish()
        {
            if (cem != null)
                cem.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(500, OUT_RS485);
                    tower.OffOutAndWaitInput(OUT_IMPULSE_UUT, 2000, IN_IMPULSE_UUT);
                    tower.OffOutAndWaitInput(OUT_DOT_TEST_UUT, 2000, IN_DOT_TEST_1_UUT, IN_DOT_TEST_2_UUT);
                    Delay(500, "Espera");
                }       
                tower.Dispose();
            }
        }

        //**********************************

        private void TestPointCalibration(TestPointsName testPointName, TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200, bool isMultiRangeVoltage=false)
        {
            TestPoint testPoint = new TestPoint(meter, testPointName, tipoPotencia, isMultiRangeVoltage);

            ConsignarMTE(testPoint.Voltage, testPoint.Current, testPoint.Desfase, testPoint.Frequency);

           internalVerification(testPoint, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        private void internalVerification(TestPoint testPoint, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort=6, int SamplingTime= 1200)
        {
            var potenciaAparente = testPoint.Voltage * testPoint.Current;
            var potenciaActiva = potenciaAparente * Math.Cos(testPoint.Desfase.ToRadians());
            var potenciaReactiva = potenciaAparente * Math.Sin(testPoint.Desfase.ToRadians());

            var Tolerance = testPoint.Tolerance;
            var Average = testPoint.TipoPotencia == TipoPotencia.Activa ? potenciaActiva : potenciaReactiva;
            var Unidad = testPoint.TipoPotencia == TipoPotencia.Activa ? ParamUnidad.W : ParamUnidad.Var;

            var margen = Margenes.GetDouble(string.Format("PRECISION_{0}", testPoint.Name), Tolerance, ParamUnidad.PorCentage);

            var defs = new TriAdjustValueDef(testPoint.Name + "_L1", 0, 0, Average, margen, Unidad);
            TestCalibracionBase(defs,
            () =>
            {
                var powerActive = cem.ReadPowerActiveHiResolution();
                var powerReactive = cem.ReadPowerReactiveHiResolution();
                var lecturas = new List<double>();

                if (testPoint.TipoPotencia == TipoPotencia.Activa)
                {
                    lecturas.Add((double)powerActive.L1);
                    lecturas.Add((double)powerActive.L2);
                    lecturas.Add((double)powerActive.L3);
                    logger.InfoFormat("Potencia Activa CEM-C20 L1: {0}", powerActive.L1);
                    logger.InfoFormat("Potencia Activa CEM-C20 L2: {0}", powerActive.L2);
                    logger.InfoFormat("Potencia Activa CEM-C20 L3: {0}", powerActive.L3);
                }
                else
                {
                    lecturas.Add((double)powerReactive.L1);
                    lecturas.Add((double)powerReactive.L2);
                    lecturas.Add((double)powerReactive.L3);
                    logger.InfoFormat("Potencia Reactiva CEM-C20 L1: {0}", powerReactive.L1);
                    logger.InfoFormat("Potencia Reactiva CEM-C20 L2: {0}", powerReactive.L2);
                    logger.InfoFormat("Potencia Reactiva CEM-C20 L3: {0}", powerReactive.L3);
                }
                return lecturas.ToArray();
            },
            () =>
            {
                var lecturas = new List<double>();
                if (testPoint.TipoPotencia == TipoPotencia.React)
                {
                    var reaactivePower = tower.MTEWAT.ReadReactivePower();
                    lecturas.Add(reaactivePower.Value.L1);
                    lecturas.Add(reaactivePower.Value.L2);
                    lecturas.Add(reaactivePower.Value.L3);
                    logger.InfoFormat("Potencia Reactiva MTE PRS L1: {0}", reaactivePower.Value.L1);
                    logger.InfoFormat("Potencia Reactiva MTE PRS L2: {0}", reaactivePower.Value.L2);
                    logger.InfoFormat("Potencia Reactiva MTE PRS L3: {0}", reaactivePower.Value.L3);
                }
                else
                {
                    var activePower = tower.MTEWAT.ReadActivePower();
                    lecturas.Add(activePower.Value.L1);
                    lecturas.Add(activePower.Value.L2);
                    lecturas.Add(activePower.Value.L3);
                    logger.InfoFormat("Potencia Activa MTE PRS L1: {0}", activePower.Value.L1);
                    logger.InfoFormat("Potencia Activa MTE PRS L2: {0}", activePower.Value.L2);
                    logger.InfoFormat("Potencia Activa MTE PRS L3: {0}", activePower.Value.L3);
                }
                return lecturas.ToArray();
            }, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime,
            string.Format("Error calculado en la calibración en {0} fuera de margenes", testPoint.Name));

            logger.InfoFormat("L1 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L1.Value, defs.L1.Error, defs.L1.Tolerance);
            logger.InfoFormat("L2 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L2.Value, defs.L2.Error, defs.L2.Tolerance);
            logger.InfoFormat("L3 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L3.Value, defs.L3.Error, defs.L3.Tolerance);

            var ErrorTri = Math.Round((defs.L1.Error + defs.L2.Error + defs.L3.Error) / 3, 2);
            var defsError = new AdjustValueDef(testPoint.Name + "_III_ERROR", ErrorTri, -1 * Tolerance, Tolerance, 0, 0, ParamUnidad.PorCentage);
            Resultado.Set(defsError.Name, defsError.Value, ParamUnidad.PorCentage, -1 * Tolerance, Tolerance);

            Assert.IsTrue(defsError.IsValid() == true, Error().UUT.HARDWARE.MARGENES(string.Format("Error calculado trifasico en la calibración en {0} fuera de margenes", defsError.Name)));
        }

        private void ConsignarMTE(double tension, double corriente, double desfase, double frecuencia, double offsetPF = 2)
        {
            var voltage = TriLineValue.Create(tension);
            var current = TriLineValue.Create(corriente);
            var gap = TriLineValue.Create(desfase);

            if (meter.TipoEquipo == MeterType.TipoEquipoEnum.MONOFASICO)
            {
                voltage = TriLineValue.Create(tension, 0, 0);
                current = TriLineValue.Create(corriente, 0, 0);
                gap = TriLineValue.Create(desfase, 0, 0);
            }
            tower.MTEPS.ApplyPresetsAndWaitStabilisationWithValidator(voltage, current, frecuencia, gap, TriLineValue.Create(0, 120, 240), false, () =>
            {
                var measure = tower.MTEWAT.ReadAll();

                var pf = measure.PowerFactor;
                logger.InfoFormat("Stabilisation MTE PRS PowerFactor= {0}", pf);

                var freq = measure.Freq;
                logger.InfoFormat("Stabilisation MTE PRS Frequency= {0}", freq);

                var resultPF = pf.InToleranceFrom(gap, offsetPF);
                if (!resultPF)
                    logger.InfoFormat("PF Error {0} by MTE", pf.ToString());

                var errorFrec = (1 - (frecuencia / freq)) * 100;
                var resultFrec = Math.Abs(errorFrec) < tower.MTEPS.Tolerance;

                return  resultFrec && resultPF;
            });
        }

    }
}
 