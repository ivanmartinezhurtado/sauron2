﻿using Dezac.Tests.UserControls;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.Services
{
    public interface IShell
    {
        DialogResult MsgBox(string text, string caption);
        DialogResult MsgBox(string text, string caption, MessageBoxButtons buttons);
        DialogResult MsgBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon);

        DialogResult ShowDialog(string title, Control control, MessageBoxButtons? buttons, Action<Control> onLoad = null);
        DialogResult ShowDialog(string title, Func<Control> control, MessageBoxButtons? buttons, string description= null, int waitTime=500);
        T ShowDialog<T>(string title, Func<Control> control, MessageBoxButtons? buttons, Func<Control, DialogResult, T> result);
        Form ShowForm(Func<Control> control);

        DialogResult ShowTableDialog(string title, object data, MessageBoxButtons buttons = MessageBoxButtons.OK);
        DialogResult ShowTableDialog(string title, TableGridViewConfig config, MessageBoxButtons buttons = MessageBoxButtons.OK);
        T ShowTableDialog<T>(string title, object data);
        T ShowTableDialog<T>(string title, TableGridViewConfig config);
        T ShowInputKeyboard<T>(string title, string promptText, T defaultValue, string mask = null, string initValue = null);
        Task<T> Run<T>(Func<T> action);
        void Run(Action action);
        void RunSync(Action action);
    }
}
