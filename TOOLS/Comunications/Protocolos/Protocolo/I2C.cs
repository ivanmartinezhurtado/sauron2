﻿using System;
using System.IO.Ports;

namespace Comunications
{
    public class I2C
    {
        public static SerialPort sp;

        public static void spinit(int sn)  // function for serial port initialization
        {
            sp = new SerialPort("COM" + sn);
            Console.WriteLine("openning the port..........");
            try
            {
                sp.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot open port following exception occured:" + ex.Message.ToString());
            }

            //making Clcok and SDA line stable
            sp.RtsEnable = true;
            sp.DtrEnable = true;
        }

        //this samples SDA pin of DS1621 by reading value of ctsHolding of RS232
        public static bool sample()
        {
            return sp.CtsHolding;
        }

        // This is start condition for DS1621 
        public static void startcmd()
        {
            sp.DtrEnable = true;
            sp.DtrEnable = true;
            sp.RtsEnable = true;
            sp.DtrEnable = false;
        }
        //this is stop condition for Ds1621
        public static void stopcmd()
        {
            sp.DtrEnable = true;
            sp.DtrEnable = false;
            sp.RtsEnable = true;
            sp.DtrEnable = true;
        }

        //This function transmitt 1(high) to DS1621
        public static void tx_1()
        {
            sp.RtsEnable = false;
            sp.DtrEnable = true;
            sp.DtrEnable = true;
            sp.RtsEnable = true;
            sp.RtsEnable = false;
        }
        //This function transmitt 0(low) to DS1621
        public static void tx_0()
        {
            sp.RtsEnable = false;
            sp.DtrEnable = true;
            sp.DtrEnable = false;
            sp.RtsEnable = true;
            sp.RtsEnable = false;
        }

        //This functions tranmitt a byte value as a binary string
        public static bool tx_byte(string b)
        {
            foreach (char c in b)
                if (c == '0')
                    tx_0();
                else if (c == '1')
                    tx_1();
            return rx_bit();
        }

        //This reads a bit value from DS1621
        public static bool rx_bit()
        {
            bool temp;
            sp.DtrEnable = true;
            sp.RtsEnable = false;
            sp.RtsEnable = true;
            temp = sample();
            sp.RtsEnable = false;
            return temp;
        }

        //This reads a byte value from DS1621
        public static int rx_byte(bool ack)
        {
            int i;
            int retval = 0;
            for (i = 0; i <= 7; i++)
            {
                retval = retval * 2;
                if (rx_bit())
                    retval = retval + 1;
            }
            if (ack)
                tx_0();
            else
                tx_1();
            return retval;
        }

        //This Squence is for setting DS1621 for setting it in one shot mode (see data sheet)
        public static void one_shot_mode()
        {
            startcmd(); //start command for DS1621
            tx_byte("10010000");
            tx_byte("10101100");
            tx_byte("00000001");
            //no need of stop
        }

        //This Squence is for setting DS1621 for starting conversion of temperature (see data sheet)
        public static void Start_convert_temperature()
        {
            startcmd();
            tx_byte("10010000"); //Bus Master sends DS1621 address; R/ W= 0.DS1621 generates acknowledge bit.
            tx_byte("11101110");// Bus Master sends Start Convert T command protocol.DS1621 generates acknowledge bit.
            stopcmd();
        }
        //This Squence is for setting DS1621 for reading 2 byte temperature format  MSB and LSB (see data sheet)
        public static double read_temperature()
        {
            int temperature_MSB;
            int temperature_LSB;
            startcmd();
            tx_byte("10010000");
            tx_byte("10101010");
            startcmd();
            tx_byte("10010001");
            temperature_MSB = rx_byte(true); //MSB is integral value of temperature
            temperature_LSB = rx_byte(false); //LSB is fraction pf temperature
            stopcmd();

            //to convert temperature in readable format
            double temp = ((temperature_MSB * 256 + temperature_LSB)) / 256;
            if (temperature_MSB >= 128)
                temp = temp - 256;
            return temp;
        }

        public static double issue_read_temp()
        {
            //an extra stop to stabalize bus
            stopcmd();
            //issue a one shot mode command
            one_shot_mode();
            return read_temperature();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter the COM port number where Hardware is connected :");
            //serial port initialization
            spinit(int.Parse(Console.ReadLine()));
            Console.WriteLine("Temperature Value of DS1621 is :");
            //issue a read sequence for DS1621
            int i = 0;

            Console.WriteLine(issue_read_temp());
            while (i < 20)
                i++;
            Console.ReadKey();
        }
    }
}
