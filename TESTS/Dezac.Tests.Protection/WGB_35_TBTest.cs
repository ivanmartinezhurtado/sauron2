﻿using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.12)]
    public class WGB_35_TBTest : TestBase
    {
        Tower3 tower;

        WGB_35_TB wgb;

        private string CAMERA_IDS;

        private const byte DETECTION_RELAY_NO = 9;
        private const byte DETECTION_RELAY_NC = 10;
        private const byte INVIERTE_POLARIZACION = 17;
        private const byte SUPPLY_DC = 19;
        private const byte LAZO_CORRIENTE_DC = 43;
        private const byte PUNTAS_TEST = 5;
        private const byte PUNTAS_COMS = 6;
        private const byte TECLA_RESET = 9;
        private const byte TECLA_TEST = 10;
        private const byte FIJACION_CARRO = 11;
        private const byte PILOTO_ERROR = 48;
        private const byte VAC_AUXILIAR = 15;

        private const byte DEVICE_PRESENCE = 24;
        private const byte TOP_PRESENCE = 25;
        private const byte DETECTION_PISTON_COMS = 30;
        private const byte DETECTION_PISTON_BORNES = 31;

        public void TestInitialization()
        {
            var TrafoSN = Shell.ShowDialog<string>("ENTRADA NUMERO SERIE TRANSFORMADOR", () =>
            {
                return new InputKeyBoard("Leer código de barras del numero de serie del transformador");
            }
            , MessageBoxButtons.OKCancel, (c, d) =>
            {
                if (d == DialogResult.OK)
                    return ((InputKeyBoard)c).TextValue;

                return string.Empty;
            });

            TestInfo.DeviceID = TrafoSN;
            Resultado.Set("SN_TRANSFORMADOR", TestInfo.DeviceID, ParamUnidad.SinUnidad);

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            wgb = new WGB_35_TB(Comunicaciones.SerialPort);
            wgb.Modbus.PerifericNumber = Comunicaciones.Periferico;

            tower.PTE.InitConfig();

            tower.IO.DO.Off(PILOTO_ERROR);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DEVICE_PRESENCE];
            }, "Error. No se ha detectado la presencia del equipo", 10, 1500, 1000);

            tower.ActiveElectroValvule();
            tower.Active24VDC();
            tower.IO.DO.OnWait(500, FIJACION_CARRO);
            tower.IO.DO.On(PUNTAS_TEST, PUNTAS_COMS, VAC_AUXILIAR);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_PISTON_BORNES];
            }, "Error. No ha pinchado correctamente el piston de los bornes", 3, 500, 500);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_PISTON_COMS];
            }, "Error. No ha pinchado correctamente el piston de comunicaciones", 3, 500, 0);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);
        }

        public void TestComunications()
        {
            tower.PTE.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.Name, 230, ParamUnidad.V), 0, 0);
            
            SamplerWithCancel((p) =>
            {
                var version = wgb.ReadFirmwareVersion();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la versión de firmware es incorrecta"), ParamUnidad.SinUnidad);             
                return true;
            }, "Error. No se ha podido cominunicar con el equipo", 3, 500, 1000, false, false);

            wgb.WriteWorkMode(WGB_35_TB.WorkModes.TEST);

            Resultado.Set("COMUNICATIONS", "OK", ParamUnidad.SinUnidad);

            var model = WGB_35_TB.Models.NULL;
            Enum.TryParse<WGB_35_TB.Models>(Configuracion.GetString("MODELO_TEST", WGB_35_TB.Models.CI_30.ToString(), ParamUnidad.SinUnidad).ToUpper().Trim(), out model);

            if (model == WGB_35_TB.Models.NULL)
                throw new Exception("Error al recuperar el modelo de la base de datos");

            wgb.WriteModel(model);
        }

        public void TestOutputs()
        {
            var ledsDictionary = new List<WGB_35_TB.Outputs>();
            ledsDictionary.Add(WGB_35_TB.Outputs.LED_POWER);
            ledsDictionary.Add(WGB_35_TB.Outputs.LED_TRIP);

            wgb.WriteOutput(WGB_35_TB.Outputs.ANY);

            foreach (var currentOutput in ledsDictionary)
            {
                wgb.WriteOutput(currentOutput);

                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, string.Format("WGB_35_TB_{0}",currentOutput.ToString()), currentOutput.ToString(), "WGB_35_TB");

                wgb.WriteOutput(WGB_35_TB.Outputs.ANY);
            }

            SamplerWithCancel((p) =>
            {
                wgb.WriteOutput(WGB_35_TB.Outputs.ANY);
                Delay(200, "Esperando desactivación del relé");
                return tower.IO.DI[DETECTION_RELAY_NO];
            }, "Error. No se ha detectado la desactivación del relé", 3, 500, 0);

            SamplerWithCancel((p) =>
            {
                wgb.WriteOutput(WGB_35_TB.Outputs.RELAY);
                Delay(200, "Esperando activación del relé");
                return tower.IO.DI[DETECTION_RELAY_NC];
            }, "Error. No se ha detectado la activación del relé", 3, 500, 0);

            Resultado.Set("RELAY", "OK", ParamUnidad.SinUnidad);

            wgb.WriteOutput(WGB_35_TB.Outputs.ANY);
        }

        public void TestInputs()
        {
            var keysList = new Dictionary<WGB_35_TB.Inputs, byte>();
            keysList.Add(WGB_35_TB.Inputs.RESET, TECLA_RESET);
            keysList.Add(WGB_35_TB.Inputs.TEST, TECLA_TEST);

            foreach (var currentInput in keysList)
            {
                tower.IO.DO.On(currentInput.Value);
                SamplerWithCancel((p) =>
                {
                    return (wgb.ReadInputs().HasFlag(currentInput.Key));
                }, string.Format("No se detecta la tecla {0}", currentInput.ToString().Replace("_", " ")), 3, 500, 200);
                tower.IO.DO.OffWait(200, currentInput.Value);

                Resultado.Set(currentInput.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }

            SamplerWithCancel((p) =>
            {
                return !wgb.ReadInputs().HasFlag(WGB_35_TB.Inputs.ERROR_TRANSFORMADOR);
            }, "Error. No se ha detectado la conexión del transformador con el equipo", 3, 500);
            Resultado.Set("TRANSFORMADOR", "OK", ParamUnidad.SinUnidad);
        }

        public void TestAdjust()
        {
            /*************AJUSTE OFFSET*****************/
            tower.IO.DO.On(LAZO_CORRIENTE_DC);

            Delay(5000, "");

            wgb.WriteWorkMode(WGB_35_TB.WorkModes.AJUSTE_OFFSET_DC);
            Delay(6000, "Realizando calculo...");
            var x = wgb.ReadCurrent();
            var param = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("OFFSET"), ParamUnidad.Puntos);
            TestMeasureBase(param,
            (step) =>
            {
                var reading = wgb.ReadAdjustOffsetCalculate();
                return reading;
            }, 0, 5, 500);

            tower.IO.DO.PulseOff(3000, PUNTAS_TEST);
            Delay(3000, "Reseteando equipo");

            /*************AJUSTE DC(+)*****************/
            tower.IO.DO.On(SUPPLY_DC);
            tower.IO.DO.Off(INVIERTE_POLARIZACION);
            tower.IO.DO.On(LAZO_CORRIENTE_DC, 39);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint("DC").Name, 4.94, ParamUnidad.A);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0.7);

            var paramVerifCurrent = new AdjustValueDef(Params.I_DC.Null.Ajuste.TestPoint("DC_POS"), ParamUnidad.V);

            try
            {
                TestMeasureBase(paramVerifCurrent,
                (step) =>
                {
                    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500, null, null, true);
                }, 0, 3, 1000);
            }
            catch(Exception)
            {
                Logger.InfoFormat("Calculando consigna LAMBDA para obtener Corriente de ajuste");
                Logger.InfoFormat(string.Format("consigna ANTERIOR : {0}", adjustVoltage));

                adjustVoltage = (adjustVoltage * Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.06, ParamUnidad.A)) / paramVerifCurrent.Value;

                Logger.InfoFormat(string.Format("consigna POSTERIOR : {0}", adjustVoltage));

                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0.7);

                TestMeasureBase(paramVerifCurrent,
                (step) =>
                {
                    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500, null, null, true);
                }, 0, 3, 1000);
            }

            Delay(3000, "Estabilizando medida del equipo");

            wgb.WriteWorkMode(WGB_35_TB.WorkModes.AJUSTE_DC_POSITIVO);
            Delay(6000, "Realizando calculo...");
            x = wgb.ReadCurrent();
            var y = GetCurrentInAmpers(wgb.ReadCurrent(), wgb.ReadAdjustCalculateDCPositive());

            param = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("DC_POS"), ParamUnidad.Puntos);
            TestMeasureBase(param,
            (step) =>
            {
                var reading = wgb.ReadAdjustCalculateDCPositive();
                return reading;
            }, 0, 5, 500);
          

            var verif = new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("DC_POS").Name, 0, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A);
            TestCalibracionBase(verif,
               () =>
               {
                   return GetCurrentInAmpers(wgb.ReadCurrent(), param.Value);

               },
               () =>
               {
                   return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500, null, null, true);
               }, 1, 1, 500, "Error en la verificación de la medida de DC +");


            /*************AJUSTE DC(-)*****************/

            tower.IO.DO.On(INVIERTE_POLARIZACION);

            paramVerifCurrent = new AdjustValueDef(Params.I_DC.Null.Ajuste.TestPoint("DC_NEG"), ParamUnidad.V);

            try
            {
                TestMeasureBase(paramVerifCurrent,
                (step) =>
                {
                    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500, null, null, true);
                }, 0, 3, 2000);
            }
            catch (Exception)
            {
                Logger.InfoFormat("Calculando consigna LAMBDA para obtener Corriente de ajuste");
                Logger.InfoFormat(string.Format("consigna ANTERIOR : {0}", adjustVoltage));

                adjustVoltage = (adjustVoltage * -Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.06, ParamUnidad.A)) / paramVerifCurrent.Value;

                Logger.InfoFormat(string.Format("consigna POSTERIOR : {0}", adjustVoltage));

                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0.7);

                TestMeasureBase(paramVerifCurrent,
                (step) =>
                {
                    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500, null, null, true);
                }, 0, 3, 2000);
            }

            wgb.WriteWorkMode(WGB_35_TB.WorkModes.AJUSTE_DC_NEGATIVO);
            Delay(6000, "Realizando calculo...");

            param = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("DC_NEG"), ParamUnidad.Puntos);
            TestMeasureBase(param,
            (step) =>
            {
                var reading = wgb.ReadAdjustCalculateDCNegative();
                return reading;
            }, 0, 5, 500);
                       

            verif = new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("DC_NEG").Name, 0, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A);
            TestCalibracionBase(verif,
               () =>
               {
                   return -GetCurrentInAmpers(wgb.ReadCurrent(), param.Value);

               },
               () =>
               {
                   return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500, null, null, true);
               }, 1, 1, 500, "Error en la verificación de la medida de DC -");

            /*************AJUSTE AC*****************/

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            tower.IO.DO.OffWait(1000, SUPPLY_DC, INVIERTE_POLARIZACION, LAZO_CORRIENTE_DC, 39);

            tower.PTE.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.Name, 0, ParamUnidad.SinUnidad), Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.060,ParamUnidad.A), 50);
          
            wgb.WriteWorkMode(WGB_35_TB.WorkModes.AJUSTE_AC);
            Delay(6000, "Realizando calculo...");

            tower.PTE.SetSource(PTE.SourceType.PTE_50_CE);
            param = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("AC"), ParamUnidad.Puntos);
            TestMeasureBase(param,
            (step) =>
            {
                var reading = wgb.ReadAdjustCalculateAC();
                return reading;
            }, 0, 5, 500);

            verif = new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("AC").Name, 0, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A);
            TestCalibracionBase(verif,
               () =>
               {
                   return GetCurrentInAmpers(wgb.ReadCurrent(), param.Value);

               },
               () =>
               {
                   return tower.PTE.ReadCurrent();
               }, 1, 1, 500, "Error en la verificación de la medida de AC");

            tower.PTE.ApplyCurrentAndStartTimer(0);
            tower.PTE.ApplyOff();
            tower.PTE.ResetTimer();
        }

        public void TestVerification()
        {           
            var model = WGB_35_TB.Models.NULL;
            Enum.TryParse<WGB_35_TB.Models>(Configuracion.GetString("MODELO_TEST", WGB_35_TB.Models.CI_30.ToString(), ParamUnidad.SinUnidad).ToUpper().Trim(), out model);

            if (model == WGB_35_TB.Models.NULL)
                throw new Exception("Error al recuperar el modelo de la base de datos");

            wgb.WriteModel(model);

            wgb.WriteWorkMode(WGB_35_TB.WorkModes.NORMAL);

            /***********DISPARO AC***************/

            if (tower.IO.DI[DETECTION_RELAY_NC] && !tower.IO.DI[DETECTION_RELAY_NO])
                tower.IO.DO.PulseOn(500, TECLA_RESET);

            var triggerCurrent = 0.0;
            if (model == WGB_35_TB.Models.CI_30)
                triggerCurrent = 0.020;
            else
                triggerCurrent = 0.200;

            tower.PTE.ApplyPresetsAndWaitStabilisation(0, triggerCurrent, 50);
            SamplerWithCancel((p) =>
            {
                return !tower.IO.DI[DETECTION_RELAY_NC] && tower.IO.DI[DETECTION_RELAY_NO];
            }, "Error. El equipo ha disparado por debajo de la I de fuga nominal en AC", 3, 500, 2500);

            tower.PTE.ApplyCurrentAndStartTimer(0);
            tower.PTE.ResetTimer();
            tower.PTE.ApplyOff();
            Delay(1000, "");

            var verificationCurrent = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.030, ParamUnidad.A);

            tower.PTE.SelecctionSource = PTE.SourceType.PTE_50_CE;

            tower.PTE.ResetTimerTripMonitor();

            tower.PTE.ApplyConfigMonitor(3, PTE.Monitor.EventType.TRIP, PTE.Monitor.EventTrigger.ActivarseDesactivarse);

            tower.PTE.DesActivaTimer(108);

            tower.PTE.ApplyCurrentAndStartTimer(verificationCurrent);

            Delay(1000, "Esperando el disparo del equipo");

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_RELAY_NC] && !tower.IO.DI[DETECTION_RELAY_NO];
            }, "Error No se ha producido el disparo del equipo", 3, 150);

            var time = tower.PTE.ReadTime();

            Assert.AreBetween(Params.TIME.Null.TRIGGER.TestPoint("AC").Name, time, Params.TIME.Null.TRIGGER.TestPoint("AC").Min(), Params.TIME.Null.TRIGGER.TestPoint("AC").Max(), Error().UUT.MEDIDA.MARGENES("Error. valor de tiempo de disparo fuera de margenes"), ParamUnidad.s);

            tower.PTE.ApplyCurrentAndStartTimer(0);
            tower.PTE.ApplyOff();
            tower.PTE.ResetTimer();

            /*************DISPARO DC*****************/

            tower.IO.DO.On(SUPPLY_DC, LAZO_CORRIENTE_DC, 39);            

            tower.IO.DO.PulseOn(1000, TECLA_RESET);            

            SamplerWithCancel((p) =>
            {
                return !tower.IO.DI[DETECTION_RELAY_NC] && tower.IO.DI[DETECTION_RELAY_NO];
            }, "Error. El equipo no se ha reseteado despues de pulsarle la tecla Reset", 5, 500, 500);

            if(model == WGB_35_TB.Models.CI_30)
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(1.64, 0.5);
            else
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(16.4, 0.5);

            SamplerWithCancel((p) =>
            {
                return !tower.IO.DI[DETECTION_RELAY_NC] && tower.IO.DI[DETECTION_RELAY_NO];
            }, "Error. El equipo ha disparado por debajo de la I de fuga nominal en DC", 3, 500, 2500);

            var currentActual = tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
            var currentTrigger = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 0.030, ParamUnidad.A);
            var triggerVoltage = (tower.LAMBDA.ReadVoltage() * currentTrigger) / currentActual;

            tower.IO.DO.OffWait(250, LAZO_CORRIENTE_DC);

            this.InParellel(
            () =>
            {
                tower.MUX.Reset();
                tower.MUX.Connect2((int)InputMuxEnum.IN2, Outputs.FreCH1, (int)InputMuxEnum.IN3, Outputs.FreCH2);

                tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

                tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
            },
            () =>
            {        
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(triggerVoltage, 0.7);
            });

            var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER.TestPoint("DC"), ParamUnidad.s);

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 2000, HP53131A.Channels.CH1, () =>
                    {
                        Delay(500, "");
                        tower.IO.DO.On(LAZO_CORRIENTE_DC);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw new Exception(string.Format("Error, el equipo no ha disparado"));
                }
            }, 0, 1, 1000);

        }

        public void TestCustomization()
        {            
            wgb.WriteWorkMode(WGB_35_TB.WorkModes.TEST);

            var model = WGB_35_TB.Models.NULL;
            Enum.TryParse<WGB_35_TB.Models>(Configuracion.GetString(ConstantsParameters.Identification.MODELO, WGB_35_TB.Models.CI_30.ToString(), ParamUnidad.SinUnidad).ToUpper().Trim(), out model);

            if (model == WGB_35_TB.Models.NULL)
                throw new Exception("Error al recuperar el modelo de la base de datos");

            wgb.WriteModel(model);

            wgb.WriteErrorCode(0);

            wgb.WriteBastidor(Convert.ToInt32(TestInfo.NumBastidor));
            Delay(2000, "Escribiendo bastidor");

            tower.PTE.ApplyOffAllAndWaitStabilisation();
            Delay(2000, "Reseteando equipo");
            tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;
            tower.PTE.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230, ParamUnidad.V), 0, 0);

            var bastidorReading = 0;
            SamplerWithCancel((p) =>
            {
                bastidorReading = wgb.ReadBastidor();
                return true;
            }, "Error. No se ha podido comununicar con el equipo después del Reset", 5, 500, 0, false, false);                       
       
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidorReading.ToString(), TestInfo.NumBastidor.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_GRABADO("Error, numero de bastidor no grabado correctamente"), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            

            if (wgb != null)
                wgb.Dispose();

            if (tower != null)
            {                
                tower.IO.DO.OffWait(1000, TECLA_RESET, TECLA_TEST, PUNTAS_COMS, PUNTAS_TEST, FIJACION_CARRO);
                tower.IO.DO.Off(4);
                tower.ShutdownSources();
                tower.Dispose();
            }

            if (IsTestError)
                tower.IO.DO.On(PILOTO_ERROR);

        }


        private double GetCurrentInAmpers(int currentInPoints, double factor)
        {
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.060, ParamUnidad.A);

            return adjustCurrent * Math.Sqrt(currentInPoints / factor);

        }
    }
}
