﻿using Instruments.Utility;
using System;
using System.Collections.Generic;

namespace Instruments.Auxiliars
{
    public class InstrumentsAuxiliarBaseException : InstrumentException
    {
        public enum Codes
        {
            BUSY,
            INTERNAL_ERROR,
            CONFIG_ERROR,
            PRESETS_ERROR,
            STABILISATION_ERROR,
        };

        private static readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Codes.BUSY, "El equipo está ocupado!" },
            { (int)Codes.INTERNAL_ERROR, "Error interno de la fuente!" },
            { (int)Codes.CONFIG_ERROR, "Error en la configuracion de la fuente!" },
            { (int)Codes.PRESETS_ERROR, "Error, valores consignados no aceptados por la fuente!" },
            { (int)Codes.STABILISATION_ERROR, "Imposible estabilizar la fuente!" },
        };

        public InstrumentsAuxiliarBaseException(Codes code)
            : base((int)code, GetMessage((int)code))
        {
        }

        public InstrumentsAuxiliarBaseException(Codes code, Exception innerException)
            : base((int)code, GetMessage((int)code), innerException)
        {
        }

        private static string GetMessage(int code)
        {
            string message = null;

            messages.TryGetValue(code, out message);

            return message ?? "Error desconocido!";
        }
    }
}
