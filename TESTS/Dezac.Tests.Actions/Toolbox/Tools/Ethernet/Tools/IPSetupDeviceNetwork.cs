﻿using Comunications.Utility;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(IPSetupDeviceNetworkDescription))]
    public class IPSetupDeviceNetwork : ActionBase
    {
        [Description("Dirección IP del Servidor Local")]
        [Category("3. Configuration")]
        public string ServerIP { get; set; } 

        [Description("Dirección MAC del equipo a configurar")]
        [Category("3. Configuration")]
        public string DeviceMAC { get; set; } 

        [Description("Dirección IP del equipo a configurar si es por IPfija")]
        [Category("3. Configuration")]
        public string DeviceIP { get; set; } 

        [Category("3.1 Configuration DHCP")]
        [Description("Habilitar el DHCP")]
        public bool DHCPEnabled { get; set; } 

        [Category("3.1 Configuration DHCP")]
        [Description("Identificador del cliente")]
        public string ClientID { get; set; }

        public override void Execute()
        {
            if(string.IsNullOrEmpty(ServerIP))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("Server_IP").Throw();

            if (string.IsNullOrEmpty(DeviceIP))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("Device_IP").Throw();

            if (string.IsNullOrEmpty(DeviceMAC))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("Device_MAC").Throw();

            NetUtils.ConfigIP(ServerIP, ServerIP);

            var miIp = NetUtils.GetMyIP();

            NetUtils.DeleteARP();

            var resArp = NetUtils.GetARP(miIp).Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var trama in resArp)
            {
                Logger.InfoFormat("{0}", trama);

                if (trama.Contains(DeviceIP) || trama.Contains(DeviceMAC))
                    Error().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET("NO SE HA PODIDO BORRAR LA IP EN LA TABALA ARP").Throw();
            }

            NetUtils.AddIPtoARP(DeviceIP, DeviceMAC);

            var trobat = false;
            resArp = NetUtils.GetARP(miIp).Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var trama in resArp)
            {
                Logger.InfoFormat("{0}", trama);

                if (trama.Contains(DeviceIP) && trama.Contains(DeviceMAC.Replace(":", "-")))
                {
                    trobat = true;
                    break;
                }
            }

            if(!trobat)
                Error().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET("NO SE HA PODIDO AÑADIR LA IP EN LA TABALA ARP").Throw();

            var message = string.Format("SET_ETH|{0}||{1}|255.255.255.0|0.0.0.0|0.0.0.0|0.0.0.0|", DeviceMAC.Replace("-", ":"), DeviceIP);

            if (DHCPEnabled)
                message = string.Format("SET_ETH|{0}|{1}|0.0.0.0|0.0.0.0|0.0.0.0|0.0.0.0|{2}", DeviceMAC.Replace("-", ":"), "dhcpserver", ClientID);

            int UdpClientPort = 2000;
            int UdpServerPort = 64148;

           
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(miIp), UdpServerPort);
                IPEndPoint device = new IPEndPoint(IPAddress.Parse(DeviceIP), UdpClientPort);

                Logger.InfoFormat("Establecemos conexion en IP:{0}  Puerto:{1}", miIp, UdpServerPort);
                Logger.InfoFormat("Concetamos con IP:{0}  Puerto:{1}", DeviceIP, UdpClientPort);

                using (Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                {
                    _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, true);
                    _socket.DontFragment = true;
                    _socket.Bind(ipep);
                    byte[] data = Encoding.ASCII.GetBytes(message);

                    for (byte j = 0; j < 3; j++)
                    {
                        Logger.InfoFormat("Enviamos packete UDP {0} 1/{0}", message, j);
                        _socket.SendTo(data, SocketFlags.DontRoute, device);
                        Thread.Sleep(500);
                    }
                }

                Logger.InfoFormat("Esperamos 5seg. configuracion del equipo");
                Thread.Sleep(5000);

                if (!DHCPEnabled)
                {
                    Ping p = new Ping();
                    PingReply result;
                    int i = 0;

                    do
                    {
                        Logger.InfoFormat("Enviamos ping a la IP:{0}  reintento:{1}", DeviceIP, i);
                        result = p.Send(DeviceIP, 5000);
                        i++;
                    } while (result.Status != IPStatus.Success && i < 3);

                    if (result.Status != IPStatus.Success)
                        Error().UUT.COMUNICACIONES.ETHERNET("NO HAY RESPUESTA EN EL PING").Throw();
                }

                Logger.InfoFormat("Configuracion de la IP {0} correctamente en el dispositivo Ethenet", DeviceIP);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

    public class IPSetupDeviceNetworkDescription : ActionDescription
    {
        public override string Name { get { return "IPSetupMyNetwork"; } }
        public override string Category { get { return "Tools/Ethernet/Tools"; } }
        public override string Description { get { return "Action para hacer el Setup de las IP de la red"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
