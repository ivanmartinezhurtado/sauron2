﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.01)]
    public class ADAPTER480MyeBOXTest : TestBase
    {
        public const byte OUT_DUPLICATE_VOLTAGE = 13;
        public const byte OUT_ACTIVA_CARGA = 19;
        public const byte DEVICE_PRESENCE = 9;
        public const byte BLOQUE_SEGURIDAD = 23;
        public const byte OUT_ERROR = 43;

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        public virtual void TestInitialization()
        {
            Tower.Active24VDC();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);
            Tower.IO.DO.Off(OUT_ERROR);

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[BLOQUE_SEGURIDAD])
                {
                    if (Shell.MsgBox("NO SE HA DETECTADO BLOQUE DE SEGURIDAD ¿QUIERES VOLVER A VERIFICARLO?", "NO SE DETECTA BLOQUE SEGURIDAD",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                        return false;

                    Error().HARDWARE.UTILLAJE.BLOQUE_SEGURIDAD().Throw();
                }
                return true;
            }, "", 5, 500, 1000);

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[DEVICE_PRESENCE])
                {
                    if (Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL. ¿QUIERES VOLVER A VERIFICARLO?", "NO SE DETECTA EQUIPO",
                        System.Windows.Forms.MessageBoxButtons.YesNo) == DialogResult.Yes)
                        return false;

                    Error().HARDWARE.UTILLAJE.DETECCION_EQUIPO().Throw();
                }
                return true;
            }, "", 5, 500, 1000);
        }

        public void TestFuncionalEnVacio()
        {
            Tower.IO.DO.On(OUT_DUPLICATE_VOLTAGE);

            tower.Chroma.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.TestPoint("NOMINAL").Name, 240, ParamUnidad.V), 0, 50);

            var name = Params.VRMS.Null.EnVacio.Name;
            var min = Params.VRMS.Null.EnVacio.Min();
            var max = Params.VRMS.Null.EnVacio.Max();

            var adjustValueTestPoint = new AdjustValueDef(name, 0, min, max, 0, 0, ParamUnidad.V);

            internalTestMeasure(adjustValueTestPoint);
        }

        public void TestFuncionalConCarga()
        {
            Tower.IO.DO.On(OUT_ACTIVA_CARGA);

            var adjustValueTestPoint = new AdjustValueDef(Params.VRMS.Null.ConCarga.Name, 0, Params.VRMS.Null.ConCarga.Min(), Params.VRMS.Null.ConCarga.Max(), 0, 0, ParamUnidad.V);

            internalTestMeasure(adjustValueTestPoint);

            var adjustVoltage = new AdjustValueDef(Params.V.Other("OUTPUT").ConCarga.Name, 0, Params.V.Other("OUTPUT").ConCarga.Min(), Params.V.Other("OUTPUT").ConCarga.Max(), 0, 0, ParamUnidad.V);

            internalTestMeasureInputs(adjustVoltage, MagnitudsMultimeter.VoltDC, InputMuxEnum.IN2);

            var adjustCurrent = new AdjustValueDef(Params.I.Other("OUTPUT").ConCarga.Name, 0, Params.I.Other("OUTPUT").ConCarga.Min(), Params.I.Other("OUTPUT").ConCarga.Max(), 0, 0, ParamUnidad.V);

            internalTestMeasureInputs(adjustCurrent, MagnitudsMultimeter.AmpDC, InputMuxEnum.IN8);
        }

        //*******************************************************************************
        //*******************************************************************************

        private void internalTestMeasure(AdjustValueDef adjustValueTestPoint)
        {
            var RangoSelec = adjustValueTestPoint.Max == 9999999 ? 15 : adjustValueTestPoint.Max;

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltAC,
                DigitosPrecision = 4,
                NumMuestras = 3,
                NPLC = 0,
                Rango = Math.Abs(RangoSelec),
            };

            TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                measure.Magnitud = MagnitudsMultimeter.VoltAC;

                var resultAC = tower.MeasureMultimeter(InputMuxEnum.IN14, measure, 500);

                measure.Magnitud = MagnitudsMultimeter.VoltDC;

                var resultDC = tower.MeasureMultimeter(InputMuxEnum.IN14, measure, 500);

                var resultRMS = Math.Sqrt(Math.Pow(resultAC, 2) + Math.Pow(resultDC, 2));

                return resultRMS;

            }, 0, 3, 1000);
        }

        private void internalTestMeasureInputs(AdjustValueDef adjustValueTestPoint, MagnitudsMultimeter magnitud, InputMuxEnum input)
        {
         
            var RangoSelec = adjustValueTestPoint.Max == 9999999 ? 15 : adjustValueTestPoint.Max;

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = magnitud,
                DigitosPrecision = 4,
                NumMuestras = 3,
                NPLC = 0,
                Rango = Math.Abs(RangoSelec),
            };

            TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                return tower.MeasureMultimeter(input, measure, 500);

            }, 0, 3, 1000);

        }

        public void TestFinish()
        {
            if (Tower != null)
            {
                Tower.ShutdownSources();
                //Tower.IO.DO.Off(OUT_ACTIVA_CARGA, OUT_DUPLICATE_VOLTAGE);
                Tower.Dispose();

                if (IsTestError)
                    Tower.IO.DO.On(OUT_ERROR);
            }
        }
    }

}