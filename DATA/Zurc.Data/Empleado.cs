namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empleado")]
    public partial class Empleado
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(10)]
        public string Id { get; set; }

        [StringLength(200)]
        public string Nombre { get; set; }
        [StringLength(200)]
        public string EMail { get; set; }
    }
}
