﻿namespace Dezac.Device.Protection
{
    [DeviceVersion(1.00)]
    public class EKOR : DeviceBase
    {
        public EKOR()
        {
        }

        public EKOR(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 0)
        {
            if (Cirbus == null)
                Cirbus = new CirbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Cirbus.PortCom = port;

            Cirbus.PerifericNumber = periferic;
        }

        public CirbusDeviceSerialPort Cirbus { get; internal set; }

        private const string READVERSION = "VDP";
        private const string READPPP = "PPP";
        private const string READINSTALLERINFORMATION = "LAI";
        private const string WRITESN = "WSN";
        private const string WRITEUSERCONF = "EAU";
        private const string WRITEDEFAULTCONF = "EAI";

        public string ReadVersion()
        {
            LogMethod();
            return Cirbus.Read(READVERSION);
        }

        public string ReadPPP()
        {
            LogMethod();
            return Cirbus.Read(READPPP);
        }
        
        public string ReadInstallerInformation()
        {
            LogMethod();
            return Cirbus.Read(READINSTALLERINFORMATION);
        }

        public void WriteSerialNumber(int serialNumber)
        {
            LogMethod();
            Cirbus.Write(WRITESN, serialNumber);
        }

        public void WriteUserConfiguration(string configuration)
        {
            LogMethod();
            Cirbus.Write(WRITEUSERCONF, configuration);
        }

        public void WriteConfigurationDefault(string configuration)
        {
            LogMethod();
            Cirbus.Write(WRITEDEFAULTCONF, configuration);
        }

        public override void Dispose()
        {
            Cirbus.Dispose();
        }
    }
}
