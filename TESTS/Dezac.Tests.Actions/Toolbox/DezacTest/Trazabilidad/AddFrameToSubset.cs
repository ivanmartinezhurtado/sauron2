﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Utils;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(AddFrameToSubsetDescription))]
    public class AddFrameToSubset : ActionBase
    {
        [Description("Número de producto a añadir a la estructura de los componentes desde otro producto")]
        [CategoryAttribute("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();
        
        [Description("Versión del número de producto a añadir a la estructura de los componentes desde otro producto")]
        [CategoryAttribute("3. Configuration")]
        public int Version { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            BastidorService.AddBastidorToSubset(DataInput.GetValue<int>(), Version);

            testBase.TestInfo.ListComponents.Add();
            System.Threading.Thread.Sleep(500);

        }
    }

    public class AddFrameToSubsetDescription : ActionDescription
    {
        public override string Name { get { return "InitFrameAndAddToSubset"; } }
        public override string Category { get { return "DezacTest/Trazabilidad"; } }
        public override string Description { get { return "Action que sirve para añadir números de producto y sus versiones a un equipo"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
