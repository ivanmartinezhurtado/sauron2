﻿namespace WinTestPlayer.Views
{
    partial class IndicatorsView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblIndicadores = new System.Windows.Forms.Label();
            this.pnlIndicadores = new Telerik.WinControls.UI.RadPanel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.lblTotal = new Telerik.WinControls.UI.RadLabel();
            this.rgKO = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.label4 = new System.Windows.Forms.Label();
            this.radialGaugeArc5 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc6 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.lblKO = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.rgOK = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.label2 = new System.Windows.Forms.Label();
            this.radialGaugeArc1 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc2 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.lblOK = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.pnlTiempos = new Telerik.WinControls.UI.RadPanel();
            this.radLabelTiempoMedio = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempoCiclo = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempoTest = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempoCicloPrevisto = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempos = new Telerik.WinControls.UI.RadLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lblHour = new Telerik.WinControls.UI.RadLabel();
            this.lblHoraActual = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelFecha = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.bdBoxReport = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnlIndicadores)).BeginInit();
            this.pnlIndicadores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgKO)).BeginInit();
            this.rgKO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgOK)).BeginInit();
            this.rgOK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTiempos)).BeginInit();
            this.pnlTiempos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTiempoMedio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCiclo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCicloPrevisto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHoraActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdBoxReport)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIndicadores
            // 
            this.lblIndicadores.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIndicadores.AutoSize = true;
            this.lblIndicadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIndicadores.ForeColor = System.Drawing.Color.Navy;
            this.lblIndicadores.Location = new System.Drawing.Point(1, 4);
            this.lblIndicadores.Name = "lblIndicadores";
            this.lblIndicadores.Size = new System.Drawing.Size(79, 16);
            this.lblIndicadores.TabIndex = 15;
            this.lblIndicadores.Text = "Indicadores";
            // 
            // pnlIndicadores
            // 
            this.pnlIndicadores.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIndicadores.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlIndicadores.Controls.Add(this.radLabel7);
            this.pnlIndicadores.Controls.Add(this.lblTotal);
            this.pnlIndicadores.Controls.Add(this.rgKO);
            this.pnlIndicadores.Controls.Add(this.rgOK);
            this.pnlIndicadores.Location = new System.Drawing.Point(7, 23);
            this.pnlIndicadores.Name = "pnlIndicadores";
            this.pnlIndicadores.Size = new System.Drawing.Size(150, 335);
            this.pnlIndicadores.TabIndex = 9;
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.ForeColor = System.Drawing.Color.Navy;
            this.radLabel7.Location = new System.Drawing.Point(20, 288);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(40, 18);
            this.radLabel7.TabIndex = 25;
            this.radLabel7.Text = "Total:";
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = false;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTotal.Location = new System.Drawing.Point(60, 283);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(67, 28);
            this.lblTotal.TabIndex = 24;
            this.lblTotal.Text = "100";
            // 
            // rgKO
            // 
            this.rgKO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgKO.BackColor = System.Drawing.Color.AliceBlue;
            this.rgKO.CausesValidation = false;
            this.rgKO.Controls.Add(this.label4);
            this.rgKO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rgKO.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc5,
            this.radialGaugeArc6,
            this.lblKO});
            this.rgKO.Location = new System.Drawing.Point(1, 138);
            this.rgKO.Name = "rgKO";
            this.rgKO.RangeEnd = 10D;
            this.rgKO.Size = new System.Drawing.Size(147, 135);
            this.rgKO.StartAngle = 180D;
            this.rgKO.SweepAngle = 180D;
            this.rgKO.TabIndex = 16;
            this.rgKO.Text = "radRadialGauge3";
            this.rgKO.Value = 0F;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(60, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "NG";
            // 
            // radialGaugeArc5
            // 
            this.radialGaugeArc5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(66)))), ((int)(((byte)(68)))));
            this.radialGaugeArc5.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.radialGaugeArc5.BindEndRange = true;
            this.radialGaugeArc5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc5.Name = "radialGaugeArc5";
            this.radialGaugeArc5.RangeEnd = 0D;
            this.radialGaugeArc5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc5.Width = 40D;
            // 
            // radialGaugeArc6
            // 
            this.radialGaugeArc6.BackColor = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc6.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc6.BackColor3 = System.Drawing.Color.Black;
            this.radialGaugeArc6.BindStartRange = true;
            this.radialGaugeArc6.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc6.Name = "radialGaugeArc6";
            this.radialGaugeArc6.RangeEnd = 50D;
            this.radialGaugeArc6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc6.Width = 40D;
            // 
            // lblKO
            // 
            this.lblKO.BindValue = true;
            this.lblKO.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblKO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKO.ForeColor = System.Drawing.Color.Black;
            this.lblKO.LabelFontSize = 12F;
            this.lblKO.LabelText = "Text";
            this.lblKO.LocationPercentage = new System.Drawing.SizeF(0F, -0.1F);
            this.lblKO.Name = "lblKO";
            this.lblKO.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // rgOK
            // 
            this.rgOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgOK.BackColor = System.Drawing.Color.AliceBlue;
            this.rgOK.CausesValidation = false;
            this.rgOK.Controls.Add(this.label2);
            this.rgOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rgOK.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc1,
            this.radialGaugeArc2,
            this.lblOK});
            this.rgOK.Location = new System.Drawing.Point(1, 10);
            this.rgOK.Name = "rgOK";
            this.rgOK.RangeEnd = 800D;
            this.rgOK.Size = new System.Drawing.Size(147, 135);
            this.rgOK.StartAngle = 180D;
            this.rgOK.SweepAngle = 180D;
            this.rgOK.TabIndex = 15;
            this.rgOK.Text = "radRadialGauge1";
            this.rgOK.Value = 300F;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
            this.label2.Location = new System.Drawing.Point(61, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "OK";
            // 
            // radialGaugeArc1
            // 
            this.radialGaugeArc1.AngleTransform = 0F;
            this.radialGaugeArc1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(190)))), ((int)(((byte)(79)))));
            this.radialGaugeArc1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(191)))), ((int)(((byte)(80)))));
            this.radialGaugeArc1.BindEndRange = true;
            this.radialGaugeArc1.BindEndRangeOffset = 180D;
            this.radialGaugeArc1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc1.Name = "radialGaugeArc1";
            this.radialGaugeArc1.RangeEnd = 480D;
            this.radialGaugeArc1.Text = "º";
            this.radialGaugeArc1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc1.Width = 40D;
            // 
            // radialGaugeArc2
            // 
            this.radialGaugeArc2.AngleTransform = 0F;
            this.radialGaugeArc2.BackColor = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc2.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc2.BackColor3 = System.Drawing.Color.AliceBlue;
            this.radialGaugeArc2.BindStartRange = true;
            this.radialGaugeArc2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc2.Name = "radialGaugeArc2";
            this.radialGaugeArc2.RangeEnd = 800D;
            this.radialGaugeArc2.RangeStart = 300D;
            this.radialGaugeArc2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc2.Width = 40D;
            // 
            // lblOK
            // 
            this.lblOK.BindValue = true;
            this.lblOK.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOK.ForeColor = System.Drawing.Color.Black;
            this.lblOK.LabelFontSize = 12F;
            this.lblOK.LabelText = "Text";
            this.lblOK.LocationPercentage = new System.Drawing.SizeF(0F, -0.1F);
            this.lblOK.Name = "lblOK";
            this.lblOK.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // pnlTiempos
            // 
            this.pnlTiempos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTiempos.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlTiempos.Controls.Add(this.radLabelTiempoMedio);
            this.pnlTiempos.Controls.Add(this.radLabel6);
            this.pnlTiempos.Controls.Add(this.lblTiempoCiclo);
            this.pnlTiempos.Controls.Add(this.lblTiempoTest);
            this.pnlTiempos.Controls.Add(this.radLabel2);
            this.pnlTiempos.Controls.Add(this.lblTiempoCicloPrevisto);
            this.pnlTiempos.Location = new System.Drawing.Point(6, 384);
            this.pnlTiempos.MinimumSize = new System.Drawing.Size(150, 260);
            this.pnlTiempos.Name = "pnlTiempos";
            // 
            // 
            // 
            this.pnlTiempos.RootElement.MinSize = new System.Drawing.Size(150, 260);
            this.pnlTiempos.Size = new System.Drawing.Size(150, 290);
            this.pnlTiempos.TabIndex = 22;
            // 
            // radLabelTiempoMedio
            // 
            this.radLabelTiempoMedio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabelTiempoMedio.AutoSize = false;
            this.radLabelTiempoMedio.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelTiempoMedio.ForeColor = System.Drawing.Color.Navy;
            this.radLabelTiempoMedio.Location = new System.Drawing.Point(5, 50);
            this.radLabelTiempoMedio.Name = "radLabelTiempoMedio";
            this.radLabelTiempoMedio.Size = new System.Drawing.Size(137, 36);
            this.radLabelTiempoMedio.TabIndex = 23;
            this.radLabelTiempoMedio.Text = "00:00";
            this.radLabelTiempoMedio.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.ForeColor = System.Drawing.Color.Navy;
            this.radLabel6.Location = new System.Drawing.Point(32, 32);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(92, 18);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "Tiempo medio";
            // 
            // lblTiempoCiclo
            // 
            this.lblTiempoCiclo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoCiclo.AutoSize = false;
            this.lblTiempoCiclo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoCiclo.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblTiempoCiclo.Location = new System.Drawing.Point(5, 135);
            this.lblTiempoCiclo.Name = "lblTiempoCiclo";
            this.lblTiempoCiclo.Size = new System.Drawing.Size(137, 36);
            this.lblTiempoCiclo.TabIndex = 21;
            this.lblTiempoCiclo.Text = "00:00";
            this.lblTiempoCiclo.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTiempoTest
            // 
            this.lblTiempoTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoTest.AutoSize = false;
            this.lblTiempoTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoTest.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblTiempoTest.Location = new System.Drawing.Point(5, 220);
            this.lblTiempoTest.Name = "lblTiempoTest";
            this.lblTiempoTest.Size = new System.Drawing.Size(137, 36);
            this.lblTiempoTest.TabIndex = 20;
            this.lblTiempoTest.Text = "00:00";
            this.lblTiempoTest.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.Navy;
            this.radLabel2.Location = new System.Drawing.Point(28, 118);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(101, 18);
            this.radLabel2.TabIndex = 8;
            this.radLabel2.Text = "Tiempo de ciclo";
            // 
            // lblTiempoCicloPrevisto
            // 
            this.lblTiempoCicloPrevisto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoCicloPrevisto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoCicloPrevisto.ForeColor = System.Drawing.Color.Navy;
            this.lblTiempoCicloPrevisto.Location = new System.Drawing.Point(29, 202);
            this.lblTiempoCicloPrevisto.Name = "lblTiempoCicloPrevisto";
            this.lblTiempoCicloPrevisto.Size = new System.Drawing.Size(98, 18);
            this.lblTiempoCicloPrevisto.TabIndex = 14;
            this.lblTiempoCicloPrevisto.Text = "Tiempo del test";
            // 
            // lblTiempos
            // 
            this.lblTiempos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempos.ForeColor = System.Drawing.Color.Navy;
            this.lblTiempos.Location = new System.Drawing.Point(6, 363);
            this.lblTiempos.Name = "lblTiempos";
            this.lblTiempos.Size = new System.Drawing.Size(58, 18);
            this.lblTiempos.TabIndex = 21;
            this.lblTiempos.Text = "Tiempos";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lblHour
            // 
            this.lblHour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHour.AutoSize = false;
            this.lblHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHour.ForeColor = System.Drawing.Color.Black;
            this.lblHour.Location = new System.Drawing.Point(7, 45);
            this.lblHour.Name = "lblHour";
            this.lblHour.Size = new System.Drawing.Size(137, 39);
            this.lblHour.TabIndex = 13;
            this.lblHour.Text = "00:00";
            this.lblHour.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHoraActual
            // 
            this.lblHoraActual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHoraActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraActual.ForeColor = System.Drawing.Color.Navy;
            this.lblHoraActual.Location = new System.Drawing.Point(57, 25);
            this.lblHoraActual.Name = "lblHoraActual";
            this.lblHoraActual.Size = new System.Drawing.Size(36, 18);
            this.lblHoraActual.TabIndex = 16;
            this.lblHoraActual.Text = "Hora";
            // 
            // radPanel1
            // 
            this.radPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.radPanel1.Controls.Add(this.radLabelFecha);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Controls.Add(this.lblHoraActual);
            this.radPanel1.Controls.Add(this.lblHour);
            this.radPanel1.Location = new System.Drawing.Point(6, 700);
            this.radPanel1.MinimumSize = new System.Drawing.Size(100, 100);
            this.radPanel1.Name = "radPanel1";
            // 
            // 
            // 
            this.radPanel1.RootElement.MinSize = new System.Drawing.Size(100, 100);
            this.radPanel1.Size = new System.Drawing.Size(150, 179);
            this.radPanel1.TabIndex = 23;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Navy;
            this.radLabel1.Location = new System.Drawing.Point(7, 680);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(83, 18);
            this.radLabel1.TabIndex = 24;
            this.radLabel1.Text = "Fecha actual";
            // 
            // radLabelFecha
            // 
            this.radLabelFecha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabelFecha.AutoSize = false;
            this.radLabelFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelFecha.ForeColor = System.Drawing.Color.Black;
            this.radLabelFecha.Location = new System.Drawing.Point(3, 126);
            this.radLabelFecha.Name = "radLabelFecha";
            this.radLabelFecha.Size = new System.Drawing.Size(143, 36);
            this.radLabelFecha.TabIndex = 22;
            this.radLabelFecha.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.ForeColor = System.Drawing.Color.Navy;
            this.radLabel4.Location = new System.Drawing.Point(53, 105);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(44, 18);
            this.radLabel4.TabIndex = 21;
            this.radLabel4.Text = "Fecha";
            // 
            // bdBoxReport
            // 
            this.bdBoxReport.DataSource = typeof(WinTestPlayer.Model.TestModel);
            // 
            // IndicatorsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.pnlTiempos);
            this.Controls.Add(this.lblTiempos);
            this.Controls.Add(this.pnlIndicadores);
            this.Controls.Add(this.lblIndicadores);
            this.Name = "IndicatorsView";
            this.Size = new System.Drawing.Size(161, 886);
            ((System.ComponentModel.ISupportInitialize)(this.pnlIndicadores)).EndInit();
            this.pnlIndicadores.ResumeLayout(false);
            this.pnlIndicadores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgKO)).EndInit();
            this.rgKO.ResumeLayout(false);
            this.rgKO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgOK)).EndInit();
            this.rgOK.ResumeLayout(false);
            this.rgOK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTiempos)).EndInit();
            this.pnlTiempos.ResumeLayout(false);
            this.pnlTiempos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTiempoMedio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCiclo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCicloPrevisto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHoraActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdBoxReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIndicadores;
        private Telerik.WinControls.UI.RadPanel pnlIndicadores;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge rgKO;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc5;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc6;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel lblKO;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge rgOK;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel lblOK;
        private Telerik.WinControls.UI.RadPanel pnlTiempos;
        private Telerik.WinControls.UI.RadLabel lblTiempoCicloPrevisto;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel lblTiempos;
        private System.Windows.Forms.Timer timer;
        private Telerik.WinControls.UI.RadLabel lblTiempoTest;
        private Telerik.WinControls.UI.RadLabel lblTiempoCiclo;
        private System.Windows.Forms.BindingSource bdBoxReport;
        private Telerik.WinControls.UI.RadLabel radLabelTiempoMedio;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel lblTotal;
        private Telerik.WinControls.UI.RadLabel lblHour;
        private Telerik.WinControls.UI.RadLabel lblHoraActual;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabelFecha;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}
