﻿using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Circutor.Tests.MyCircutor.Client
{
    public partial class MyCircutorClientBase
    {
        private string _baseUrl = "https://mycapi-dev.mycircutor.com:8084/mycapi/v1.0.0";

        public string Token { get; set; }

        public MyCircutorClientBase()
        {
            _baseUrl = ConfigurationManager.AppSettings["MyCircutorUrl"] ?? _baseUrl;
        }

        public string BaseUrl
        {
            get { return _baseUrl; }
            set { _baseUrl = value; }
        }

        protected Task<HttpClient> CreateHttpClientAsync(System.Threading.CancellationToken cancellationToken)
        {
            var client = new System.Net.Http.HttpClient();

            if (!string.IsNullOrEmpty(Token))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            }

            return Task.FromResult(client);
        }

        protected HttpClient CreateHttpClient()
        {
            var client = new System.Net.Http.HttpClient();

            if (!string.IsNullOrEmpty(Token))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            }

            return client;
        }
    }
}