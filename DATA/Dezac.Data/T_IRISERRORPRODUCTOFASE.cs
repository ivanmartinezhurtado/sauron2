namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_IRISERRORPRODUCTOFASE")]
    public partial class T_IRISERRORPRODUCTOFASE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(50)]
        public string ERRORCODE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMARTICULO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(10)]
        public string IDFASE { get; set; }

        [StringLength(2000)]
        public string DESCRIPCIONML { get; set; }

        [StringLength(2000)]
        public string DIAGNOSTICOML { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual T_IRISERROR T_IRISERROR { get; set; }
    }
}
