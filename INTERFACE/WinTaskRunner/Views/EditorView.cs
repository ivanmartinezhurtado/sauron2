﻿using Dezac.Tests;
using Dezac.Tests.Actions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;
using TaskRunner.Enumerators;
using Telerik.WinControls.UI;
using WinTaskRunner.Services;
using WinTaskRunner.Models;

namespace WinTaskRunner.Views
{
    public partial class EditorView : UserControl
    {
        private const string CUT_NODE = "CUT_NODE";
        private const string COPY_NODE = "COPY_NODE";

        public event EventHandler<Step> StepSelectedOrChanged;
        public SequenceDocument Document
        {
            get { return sequenceDocument; }
            set
            {
                sequenceDocument = value;
                sequenceDocument.BeforeSave += OnBeforeSave;

                UpdateTitle();
                UpdateUI();
            }
        }

        private Dictionary<Step, CustomTreeNode> stepNodes;
        private Dictionary<RadTreeNode, StepResult> stepErrors = new Dictionary<RadTreeNode, StepResult>();

        protected readonly AppService appService;
        private static int numInstances;
        protected readonly EventBus eventBus;
        protected SequenceDocument sequenceDocument;

        private readonly SynchronizationContext sc;
        private bool multipleInstances;

        public EditorView()
        {
            InitializeComponent();
        }
        public EditorView(AppService appService, EventBus eventBus)
            : this()
        {
            this.appService = appService;
            this.eventBus = eventBus;

            sc = SynchronizationContext.Current;

            Disposed += OnDisposed;

            WireRunnerEvents(true);
        }

        private void WireRunnerEvents(bool enable)
        {
            TaskRunner.SequenceRunner runner = appService.Runner;

            if (enable)
            {
                runner.AllTestStarting += OnAllTestStarting;
                runner.StepStart += OnStepStart;
                runner.StepPaused += OnStepPaused;
                runner.StepEnd += OnStepEnd;
            }
            else
            {
                runner.AllTestStarting -= OnAllTestStarting;
                runner.StepStart -= OnStepStart;
                runner.StepPaused -= OnStepPaused;
                runner.StepEnd -= OnStepEnd;
            }
        }

        #region SequenceRunnerEvents
        private void OnAllTestStarting(object s, RunnerTestsInfo e)
        {
            numInstances = e.NumInstances;
            multipleInstances = e.NumInstances > 1;
            stepErrors.Clear();
            ClearTime();
            stepNodes.Values.ToList().ForEach(node =>
            {
                if (!node.Checked)
                    node.Collapse(true);
            });
        }
        private void OnStepStart(object s, StepResult e) => sc.Post(_ => { OnStepStart(e); }, null);
        private void OnStepPaused(object s, StepResult e) => sc.Post(_ => { OnStepPaused(e); }, null);
        private void OnStepEnd(object sender, StepResult e) => sc.Post(_ => { OnStepEnd(e); }, null);
        private void OnStepStart(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                node.EnsureVisible();
                node.BackColor = Color.LightSteelBlue;

                if (stepResult.ExecutionResult == StepExecutionResult.Pending)
                {
                    node.AddImage(stepResult.NumInstance, Properties.Resources._712);
                    WriteNodeInformation(stepResult);
                }
            }
        }
        private void OnStepPaused(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                if ((node.Tag as Step).BreakPoint)
                    node.AddImage(stepResult.NumInstance, Properties.Resources.breakpoint_flecha);
                else
                    node.AddImage(stepResult.NumInstance, Properties.Resources.flecha);

                node.BackColor = Color.FromArgb(255, 255, 128);
                node.EnsureVisible();
            }
        }
        private void OnStepEnd(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                var step = node.Tag as Step;
                if (step.BreakPoint)
                {
                    node.BackColor = step.BreakPoint ? Color.FromArgb(228, 20, 0) : Color.Empty;
                    node.AddImage(stepResult.NumInstance, Properties.Resources.breakpoint);
                    return;
                }
                switch (stepResult.ExecutionResult)
                {
                    case StepExecutionResult.Failed:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.ko_16);
                        break;
                    case StepExecutionResult.Aborted:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.aborted_16);
                        break;
                    case StepExecutionResult.Continued:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.next_16);
                        break;
                    case StepExecutionResult.Repeated:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.retry_16);
                        break;
                    case StepExecutionResult.Omitted:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.omitted_16);
                        break;
                    case StepExecutionResult.Disabled:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.omitted_16);
                        break;
                    case StepExecutionResult.Passed:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.ok_16);
                        break;
                }
                node.BackColor = Color.Empty;
                WriteNodeInformation(stepResult);
                if (stepResult.Exception != null && !stepErrors.ContainsKey(node))
                    stepErrors.Add(node, stepResult);
            }
        }
        #endregion

        #region TreeViewEvents
        private void TreeView_NodeMouseDoubleClick(object sender, RadTreeViewEventArgs e)
        {
            if (stepErrors.TryGetValue(e.Node, out StepResult result))
                using (var form = new LogItemDetails())
                {
                    form.LogText = result.GetErrorMessage();
                    form.ShowDialog();
                }
        }
        private void TreeView_SelectedNodeChanged(object sender, RadTreeViewEventArgs e)
        {
            UpdateProperties(e.Node);
        }
        private void TreeView_NodeCheckedChanged(object sender, TreeNodeCheckedEventArgs e)
        {
            var step = e.Node.Tag as Step;
            if (step != null)
            {
                step.Enabled = e.Node.Checked;
                if (e.Node.Selected)
                    UpdateProperties(e.Node);

                e.Node.ForeColor = e.Node.Checked ? Color.Black : Color.Gray;
            }
        }
        private void TreeView_NodeExpandedChanged(object sender, RadTreeViewEventArgs e)
        {
            var step = e.Node.Tag as Step;
            if (step != null)
                step.Expanded = e.Node.Expanded;
        }
        private void TreeView_Edited(object sender, TreeNodeEditedEventArgs e)
        {
            if (e.Node == null)
                return;

            string value = string.Format("{0}", e.Node.Value).Trim();

            if (!string.IsNullOrEmpty(value))
            {
                if (stepNodes.Values.Any(p => p.Text.Trim() == value.Trim() && p != e.Node))
                {
                    value = GetUniqueStepName(value);
                    e.Node.Value = value;
                }
                else if (!CheckMaximumNameLength(value))
                    e.Node.Value = "SinNombre";

                ((Step)e.Node.Tag).Name = value;
                UpdateProperties(e.Node);
            }
        }
        private void TreeView_Editing(object sender, TreeNodeEditingEventArgs e)
        {
            e.Cancel = e.Node.Level <= 1;

            if (!e.Cancel)
            {
                ClearTime();
                e.Editor.Value = e.Node.Text;
                e.Node.Value = e.Node.Text;
            }
        }
        private void TreeView_DragEnding(object sender, RadTreeViewDragCancelEventArgs e)
        {
            e.Cancel = e.TargetNode.Level < 2;

            if (!e.Cancel && e.DropPosition == DropPosition.AsChildNode)
                if (!PermisionToAddChildNodes(e.TargetNode))
                {
                    MessageBox.Show("Acción no permitida, solo se pueden Añadir Actions en los nodos Init, Main, End, GroupTask y NewTestFaseContext", "Aviso, acción no permittida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }

            if (!e.Cancel)
            {
                ClearTime();
                var oldStep = e.Node.Tag as Step;
                var parentStep = e.Node.Parent.Tag as Step;

                parentStep.Steps.Remove(oldStep);
            }
        }
        private void TreeView_DragEnded(object sender, RadTreeViewDragEventArgs e)
        {
            var step = e.Node.Tag as Step;
            var parentStep = e.Node.Parent.Tag as Step;

            int index = e.Node.Index;

            parentStep.Steps.Insert(index, step);
        }
        private void TreeView_ValueValidating(object sender, TreeNodeValidatingEventArgs e)
        {
            string value = string.Format("{0}", e.NewValue);

            e.Cancel = string.IsNullOrWhiteSpace(value);
        }
        private void treeView_DragDrop(object sender, DragEventArgs e)
        {
            var node = ((RadTreeView)sender);

            ClearTime();

            Point pt = ((RadTreeView)sender).PointToClient(new Point(e.X, e.Y));
            RadTreeNode dn = ((RadTreeView)sender).GetNodeAt(pt);

            if (dn == null)
                return;

            DropPosition nodeAdd = PermisionToAddBeforedNodes(dn);
            if (nodeAdd == DropPosition.None)
            {
                MessageBox.Show("Acción no permitida, solo se pueden Añadir Actions en los nodos Init, Main, End, GroupTask y NewTestFaseContext", "Aviso, acción no permittida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (e.Data.GetDataPresent("WinTaskRunner.Models.ToolboxItem"))
            {
                var tbItem = (ToolboxItem)e.Data.GetData("WinTaskRunner.Models.ToolboxItem");
                var action = Activator.CreateInstance(tbItem.Type) as ActionBase;
                AddWithChecks(action, dn, nodeAdd);
            }
            else if (e.Data.GetDataPresent("System.Reflection.RuntimeMethodInfo", false))
            {
                var method = e.Data.GetData("System.Reflection.RuntimeMethodInfo") as MethodInfo;

                AddWithChecks(method, dn, nodeAdd);
            }
            else if (e.Data.GetDataPresent("WinTaskRunner.Models.ComponentsViewItem", false))
            {
                var citem = e.Data.GetData("WinTaskRunner.Models.ComponentsViewItem") as ComponentsViewItem;

                AddFromSequenceFile(citem.FileName, dn);
            }
            else if (e.Data.GetDataPresent("Telerik.WinControls.UI.RadTreeNode"))
            {
                var srcNode = (RadTreeNode)e.Data.GetData(typeof(RadTreeNode));
                if (srcNode == dn)
                    return;

                mnuDropNode.Tag = new object[] { srcNode, dn };
                mnuDropNode.Show(new Point(e.X, e.Y));
            }
        }
        private void treeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.Effect == DragDropEffects.None ? DragDropEffects.Copy : DragDropEffects.Move;
        }
        private void treeView_DragOver(object sender, DragEventArgs e)
        {
            Point p = treeView.PointToClient(new Point(e.X, e.Y));
            RadTreeNode node = treeView.GetNodeAt(p.X, p.Y);
            if (node != null)
                node.Selected = true;
        }
        private void cmdCollapse_Click(object sender, EventArgs e)
        {
            RadTreeNode node = treeView.SelectedNode;
            if (node != null)
                node.Collapse(false);
        }
        private void cmdExpand_Click(object sender, EventArgs e)
        {
            RadTreeNode node = treeView.SelectedNode;
            if (node != null)
                node.ExpandAll();
        }
        private void cmdBreakpoint_Click(object sender, EventArgs e)
        {
            CustomTreeNode node = (CustomTreeNode)treeView.SelectedNode;
            if (node != null)
            {
                var step = node.Tag as Step;
                step.BreakPoint = !step.BreakPoint;

                node.BackColor = step.BreakPoint ? Color.FromArgb(228, 20, 0) : Color.Empty;
                node.Image = step.BreakPoint ? Properties.Resources.breakpoint : null;
            }
        }
        private void cmdRemove_Click(object sender, EventArgs e)
        {
            RemoveNode(GetValidNode());
        }
        private void cmdCopy_Click(object sender, EventArgs e)
        {
            ClearTime();
            CopyToClipboard(GetValidNode(), sender == cmdCut);
        }
        private void cmdPaste_Click(object sender, EventArgs e)
        {
            ClearTime();
            PasteFromClipboard(treeView.SelectedNode, 0);
        }
        private void cmdUp_Click(object sender, EventArgs e)
        {
            ClearTime();

            RadTreeNode node = GetValidNode();
            if (node == null)
                return;

            var parentStep = node.Parent.Tag as Step;
            var step = node.Tag as Step;

            int pos = parentStep.Steps.IndexOf(step);
            if (pos <= 0 || parentStep.Steps.Count == 1)
                return;

            Step before = parentStep.Steps[pos - 1];
            parentStep.Steps[pos - 1] = step;
            parentStep.Steps[pos] = before;

            RadTreeNodeCollection nodes = node.Parent.Nodes;
            nodes.Remove(node);
            nodes.Insert(pos - 1, node);
            treeView.SelectedNode = node;
        }
        private void cmdDown_Click(object sender, EventArgs e)
        {
            ClearTime();

            RadTreeNode node = GetValidNode();
            if (node == null)
                return;

            var parentStep = node.Parent.Tag as Step;
            var step = node.Tag as Step;

            int pos = parentStep.Steps.IndexOf(step);
            if ((pos >= parentStep.Steps.Count - 1) || parentStep.Steps.Count == 1)
                return;

            Step after = parentStep.Steps[pos + 1];
            parentStep.Steps[pos + 1] = step;
            parentStep.Steps[pos] = after;

            RadTreeNodeCollection nodes = node.Parent.Nodes;
            nodes.Remove(node);
            nodes.Insert(pos + 1, node);
            treeView.SelectedNode = node;
        }
        #endregion

        #region SearchBox
        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            treeView.Filter = searchBox.Text.Length > 0 ? "Custom" : null;
            treeView.TreeViewElement.FilterPredicate = FilterNode;

            if (!string.IsNullOrEmpty(searchBox.Text))
                treeView.ExpandAll();
        }
        private bool FilterNode(RadTreeNode node)
        {
            string text = searchBox.Text;
            if (string.IsNullOrEmpty(text) || node.Text == null)
                return true;

            if (NodeContainsText(node, text))
                return true;

            if (ParentsNodeContainsText(node, text))
                return true;

            return false;
        }
        private bool NodeContainsText(RadTreeNode node, string text)
        {
            if (node.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            foreach (RadTreeNode child in node.Nodes)
                if (NodeContainsText(child, text))
                    return true;

            return false;
        }
        private bool ParentsNodeContainsText(RadTreeNode node, string text)
        {
            if (node.Parent?.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            if (node.Parent != null && ParentsNodeContainsText(node.Parent, text))
                return true;

            return false;
        }
        #endregion

        private void AddWithChecks(MethodInfo method, RadTreeNode parent, DropPosition position = DropPosition.AsChildNode)
        {
            Type type = method.ReflectedType;

            AddMethod(method, parent, false, position);

            var methods = type
              .GetMethods()
              .Where(m => m.Name != method.Name && m.GetCustomAttributes(typeof(TestPointAttribute), false).Length > 0)
              .ToList();

            foreach (MethodInfo m in methods)
            {
                TestPointAttribute attr = m.GetCustomAttributes<TestPointAttribute>().FirstOrDefault();

                if (attr.Required)
                    AddMethod(m, GetLocation((int)attr.Location), true, position);
            }
        }
        private void AddWithChecks(ActionBase action, RadTreeNode parent, DropPosition position = DropPosition.AsChildNode)
        {
            if (action == null)
                return;

            Type type = action.GetType();
            DesignerActionAttribute designer = type.GetCustomAttribute<DesignerActionAttribute>();

            var description = Activator.CreateInstance(designer.Type) as ActionDescription;
            if (description != null && !string.IsNullOrEmpty(description.Dependencies))
            {
                var deps = description.Dependencies.Split(';').ToList();

                foreach (string dep in deps)
                {
                    string[] tokens = dep.Split(':');
                    string name = tokens[0];

                    string typeName = name.Contains(".") ? name : type.Namespace + "." + name;
                    if (!HasAction(typeName))
                    {
                        int location = tokens.Length > 1 ? Convert.ToInt32(tokens[1]) : 0;
                        Type dType = type.Assembly.GetType(typeName);

                        if (dType == null)
                            dType = Type.GetType(typeName);

                        if (dType != null)
                        {
                            var daction = Activator.CreateInstance(dType) as ActionBase;
                            AddAction(daction, GetLocation(location));
                        }
                    }
                }
            }

            AddAction(action, parent, position);
        }
        private bool AddMethod(MethodInfo method, RadTreeNode parent, bool checkExists = true, DropPosition position = DropPosition.AsChildNode)
        {
            if (method == null || (checkExists && HasMethod(method.ReflectedType, method.Name)))
                return false;

            TestPointAttribute attr = method.GetCustomAttributes<TestPointAttribute>().FirstOrDefault();
            if (attr != null && !string.IsNullOrEmpty(attr.Depends))
                attr.Depends.Split(';')
                    .ToList()
                    .ForEach(name => AddMethod(method.ReflectedType.GetMethod(name), GetLocation((int)attr.Location), true, position));

            var action = new RunMethodAction();

            var step = new Step { Action = action, Name = GetUniqueStepName(method.Name) };
            action.AssemblyFile = method.ReflectedType.Assembly.Location;
            action.TypeName = method.ReflectedType.FullName;
            action.Method = method.Name;

            method.GetParameters()
                .ToList()
                .ForEach(p =>
                {
                    var value = new RunMethodAction.Argument { Name = p.Name, Type = p.ParameterType.FullName };

                    var description = p.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (description != null)
                        value.Description = description.Description;

                    if (p.DefaultValue != null && p.HasDefaultValue)
                        value.Value = p.ParameterType.IsEnum ? ((int)p.DefaultValue).ToString() : p.DefaultValue.ToString();

                    action.Args.Add(value);
                });

            AddNode(parent, step, position);

            return true;
        }

        private bool HasMethod(Type type, string name)
        {
            return stepNodes.Keys
                .Where(p => p.Action is RunMethodAction)
                .Select(p => p.Action)
                .Cast<RunMethodAction>()
                .Any(p => p.TypeName == type.FullName && p.Method == name);
        }
        private void AddAction(ActionBase action, RadTreeNode parent, DropPosition position = DropPosition.AsChildNode)
        {
            if (action == null)
                return;

            foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(action))
            {
                var attr = prop.Attributes[typeof(DefaultValueAttribute)] as DefaultValueAttribute;
                if (attr != null)
                    prop.SetValue(action, attr.Value);
            }

            Type type = action.GetType();
            DesignerActionAttribute designer = type.GetCustomAttribute<DesignerActionAttribute>();
            var description = Activator.CreateInstance(designer.Type) as ActionDescription;

            AddNode(parent, new Step { Action = action, Name = GetUniqueStepName(description.Name) }, position);
        }
        private bool HasAction(string typeName)
        {
            return stepNodes.Keys.Any(p => p.Action != null && p.Action.GetType().FullName == typeName);
        }

        private void UpdateUI()
        {
            treeView.Nodes.Clear();

            stepNodes = new Dictionary<Step, CustomTreeNode>();

            var rootNode = new CustomTreeNode("Root");
            rootNode.Tag = sequenceDocument.Model.Root;

            treeView.Nodes.Add(rootNode);

            AddNodes(rootNode);

            //treeView.CollapseAll();
            //treeView.Nodes[0].Expand();

            //ExpandUptoLevel(GetLocation(0), 3);
            //ExpandUptoLevel(GetLocation(1), 3);
            //ExpandUptoLevel(GetLocation(2), 3);

            UpdateProperties(treeView.SelectedNode);

            stepNodes.Values.ToList().ForEach(node => node.ForeColor = node.Checked ? Color.Black : Color.Gray);
        }
        private void ExpandUptoLevel(RadTreeNode tn, int level)
        {
            if (tn == null)
                return;

            if (level >= tn.Level)
            {
                tn.Expand();
                foreach (RadTreeNode i in tn.Nodes)
                    ExpandUptoLevel(i, level);
            }
        }
        private void AddNodes(CustomTreeNode node)
        {
            var step = node.Tag as Step;
            if (step == null)
                return;

            if (step.Name == null)
                step.Name = node.Name;

            node.Checked = step.Enabled;
            node.Expanded = step.Expanded;
            node.Name = step.Name;

            stepNodes.Add(step, node);

            foreach (Step child in step.Steps)
            {
                var n = new CustomTreeNode(child.Name);
                n.Tag = child;

                node.Nodes.Add(n);

                AddNodes(n);
            }
        }
        private RadTreeNode AddNode(RadTreeNode parent, Step step, DropPosition position = DropPosition.AsChildNode)
        {
            var n = new CustomTreeNode(step.Name);
            n.Tag = step;

            if (position == DropPosition.AsChildNode)
            {
                var parentStep = parent.Tag as Step;
                parentStep.Steps.Add(step);
                parent.Nodes.Add(n);
            }
            else
            {
                RadTreeNode parentOfparent = parent.Parent;
                int index = parentOfparent.Nodes.ToList().FindIndex(p => p.Equals(parent));
                parentOfparent.Nodes.Insert(index, n);

                var parentOfParentStep = parentOfparent.Tag as Step;
                var parentStep = parent.Tag as Step;
                int indexParent = parentOfParentStep.Steps.FindIndex(p => p.Equals(parentStep));
                parentOfParentStep.Steps.Insert(indexParent, step);
            }

            stepNodes.Add(step, n);

            n.Checked = true;
            parent.Expand();

            treeView.SelectedNode = n;

            while (n.Parent != n.RootNode)
                n = n.Parent as CustomTreeNode;

            if (n.Text == "End")
                step.FailAction = PostActionStep.Next;

            return treeView.SelectedNode;
        }
        private bool PermisionToAddChildNodes(RadTreeNode dn)
        {
            var nameStep = dn.Tag as Step;
            if (nameStep == null)
                return false;

            if (nameStep.Name.Trim() != "Init" && nameStep.Name.Trim() != "Main" && nameStep.Name.Trim() != "End")
            {
                var nameAction = nameStep.Action as ActionBase;
                if (nameAction == null)
                    return false;

                if (nameAction.Name.Trim() != "GroupAction" && nameAction.Name.Trim() != "NewTestFaseContextAction")
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
        private DropPosition PermisionToAddBeforedNodes(RadTreeNode dn)
        {
            var nameStep = dn.Tag as Step;
            if (nameStep == null)
                return DropPosition.None;

            if (nameStep.Name.Trim() != "Init" && nameStep.Name.Trim() != "Main" && nameStep.Name.Trim() != "End")
            {
                var nameAction = nameStep.Action as ActionBase;
                if (nameAction == null)
                    return DropPosition.None;

                if (nameAction.Name.Trim() != "GroupAction" && nameAction.Name.Trim() != "NewTestFaseContextAction")
                {
                    var nameStepParent = dn.Parent.Tag as Step;
                    if (nameStepParent == null)
                        return DropPosition.None;

                    if (nameStepParent.Name.Trim() == "Init" || nameStepParent.Name.Trim() == "Main" || nameStepParent.Name.Trim() == "End")
                        return DropPosition.AfterNode;

                    var nameStepParentAction = nameStepParent.Action as ActionBase;
                    if (nameStepParentAction == null)
                        return DropPosition.None;

                    if (nameStepParentAction.Name.Trim() != "GroupAction" && nameStepParentAction.Name.Trim() != "NewTestFaseContextAction")
                        return DropPosition.None;
                    else
                        return DropPosition.AfterNode;
                }
                else
                    return DropPosition.AsChildNode;
            }
            else
                return DropPosition.AsChildNode;
        }
        private void AddFromSequenceFile(string fileName, RadTreeNode parent)
        {
            string name = System.IO.Path.GetFileNameWithoutExtension(fileName);

            var seq = SequenceModel.LoadFile(fileName);

            RadTreeNode rootNode = treeView.Nodes[0];

            if (seq.Init.Steps.Any())
            {
                RadTreeNode node = rootNode.Nodes[0];
                if (node.Nodes.Any())
                    node = AddNode(rootNode.Nodes[0], new Step { Action = new GroupAction(), Name = GetUniqueStepName("Init_" + name) });

                CopyStepsIntoNode(seq.Init.Steps, node, 0);
            }

            if (seq.Main.Steps.Any())
            {
                RadTreeNode node = rootNode.Nodes[1];
                if (node.Nodes.Any())
                    node = AddNode(parent, new Step { Action = new GroupAction(), Name = GetUniqueStepName("Main_" + name) });

                CopyStepsIntoNode(seq.Main.Steps, node, 0);
            }

            if (seq.End.Steps.Any())
            {
                RadTreeNode node = rootNode.Nodes[2];
                if (node.Nodes.Any())
                    node = AddNode(node, new Step { Action = new GroupAction(), Name = GetUniqueStepName("End_" + name) });

                CopyStepsIntoNode(seq.End.Steps, node, 0);
            }
        }
        private RadTreeNode GetLocation(int location)
        {
            return treeView.Nodes[0].Nodes[location];
        }
        private void UpdateTitle()
        {
            Text = string.Format("Sequence: {0}", sequenceDocument.Name);
        }
        private void WriteNodeInformation(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                var step = node.Tag as Step;
                double duration = stepResult.Duration.TotalSeconds;
                string durationText = "";
                if (duration >= 0)
                    durationText = $"({duration:N2}s)";

                if (multipleInstances)
                {
                    if (Regex.IsMatch(node.Text, $"#{stepResult.NumInstance}"))
                    {
                        if (Int32.TryParse(stepResult.Step.LoopMaxIterations, out int maxInterations) && maxInterations > 1)
                            node.Text = Regex.Replace(node.Text, $@"#{stepResult.NumInstance}(.*?)\|", $"#{stepResult.NumInstance} ({stepResult.LoopIndex + 1} de {stepResult.Step.LoopMaxIterations}) {durationText} |");
                        else if (stepResult.LoopIndex > 0)
                            node.Text = Regex.Replace(node.Text, $@"#{stepResult.NumInstance}(.*?)\|", $"#{stepResult.NumInstance} (Retry: {stepResult.LoopIndex + 1}) {durationText} |");
                        else
                            node.Text = Regex.Replace(node.Text, $@"#{stepResult.NumInstance}(.*?)\|", $"#{stepResult.NumInstance} {durationText} |");
                    }
                    else
                    {
                        if (Int32.TryParse(stepResult.Step.LoopMaxIterations, out int maxInterations) && maxInterations > 1)
                            node.Text += $" #{stepResult.NumInstance} ({stepResult.LoopIndex + 1} de {stepResult.Step.LoopMaxIterations}) {durationText} |";
                        else if (stepResult.LoopIndex > 0)
                            node.Text += $" #{stepResult.NumInstance} (Retry: {stepResult.LoopIndex + 1}) {durationText} |";
                        else
                            node.Text += $" #{stepResult.NumInstance} {durationText} |";
                    }
                }
                else
                {
                    if (Int32.TryParse(stepResult.Step.LoopMaxIterations, out int maxInterations) && maxInterations > 1)
                        node.Text = $"{stepResult.Step.Name} ({stepResult.LoopIndex + 1} de {stepResult.Step.LoopMaxIterations}) {durationText}";
                    else if (stepResult.LoopIndex > 1)
                        node.Text = $"{stepResult.Step.Name} (Retry: {stepResult.LoopIndex + 1}) {durationText}";
                    else
                        node.Text = $"{stepResult.Step.Name} {durationText}";
                }
            }
        }
        private void UpdateProperties(RadTreeNode node)
        {
            Step step = node != null ? node.Tag as Step : null;

            if (StepSelectedOrChanged != null)
                StepSelectedOrChanged(this, step);

            eventBus.Fire(this, "ShowObjectProperties", step);
        }
        private RadTreeNode GetValidNode()
        {
            RadTreeNode node = treeView.SelectedNode;
            if (node == null || node.Level <= 1)
                return null;

            return node;
        }
        private void mnuNodo_Opening(object sender, CancelEventArgs e)
        {
            bool isValidNode = GetValidNode() != null;
            cmdUp.Visible = isValidNode;
            cmdDown.Visible = isValidNode;
            cmdSeparator1.Visible = isValidNode;
            cmdRemove.Visible = isValidNode;
            cmdSeparator2.Visible = isValidNode;
            cmdPaste.Enabled = Clipboard.ContainsData(CUT_NODE) || Clipboard.ContainsData(COPY_NODE);
        }
        private void ClearTime()
        {
            stepNodes.Values.ToList().ForEach(node =>
            {
                var step = node.Tag as Step;
                if (!step.BreakPoint)
                {
                    node.BackColor = Color.Empty;
                    node.ClearImages();
                }

                if (node.Parent != null && node.Text != step.Name)
                    if (node.TreeView != null && !node.TreeView.IsDisposed)
                        node.Text = ((Step)node.Tag).Name;
            });
        }
        private string GetUniqueStepName(string name)
        {
            int pos = name.IndexOf("(");
            if (pos > 0)
                name = name.Substring(0, pos).Trim();

            int i = stepNodes.Values.Where(p => p.Text == name || p.Text.StartsWith(name + " (")).Count();
            if (i > 0)
            {
                string nameRoot = name;
                for (int f = 1; f <= (i + 1); f++)
                {
                    name = string.Format("{0} ({1})", nameRoot, f);

                    if (stepNodes.Values.Where(p => p.Text.Trim() == name.Trim()).Count() == 0)
                        break;
                }
            }

            if (stepNodes.Values.Where(p => p.Text.Trim() == name.Trim()).Count() != 0)
                name = string.Format("{0} ({1})", name, i);

            if (CheckMaximumNameLength(name))           
                return name;
            return "SinNombre";
        }
        private bool CheckMaximumNameLength(string name)
        {
            if (name.Length > 50)
            {
                MessageBox.Show("El nombre del step es demasiado largo", "Atención");
                return false;
            }
            return true;
        }
        private void RemoveNode(RadTreeNode node)
        {
            ClearTime();

            if (node == null)
                return;

            var parentStep = node.Parent.Tag as Step;
            var step = node.Tag as Step;

            foreach (Step st in step.Steps)
                stepNodes.Remove(st);

            parentStep.Steps.Remove(step);
            stepNodes.Remove(step);
            node.Remove();
        }
        private void CopyToClipboard(RadTreeNode node, bool cut = false)
        {
            if (node != null)
                Clipboard.SetData(cut ? CUT_NODE : COPY_NODE, JsonConvert.SerializeObject(node.Tag, new JsonSerializerSettings
                {
                    TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                    TypeNameHandling = TypeNameHandling.Objects
                }));
        }
        private void PasteFromClipboard(RadTreeNode node, int mode)
        {
            if (node == null || node.Tag == null)
                return;

            object json = null;
            bool cut = Clipboard.ContainsData(CUT_NODE);
            if (cut)
                json = Clipboard.GetData(CUT_NODE);
            else
                json = Clipboard.GetData(COPY_NODE);

            if (json == null)
                return;

            Step newStep = JsonConvert.DeserializeObject<Step>(json.ToString(), new JsonSerializerSettings
            {
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                TypeNameHandling = TypeNameHandling.Objects
            });
            if (newStep == null)
                return;

            RadTreeNode srcNode = stepNodes.Where(p => p.Key.Name == newStep.Name).Select(p => p.Value).FirstOrDefault();
            if (cut && srcNode != null)
                RemoveNode(srcNode);

            CopyStepIntoNode(newStep, node, mode);
        }
        private void CopyStepsIntoNode(List<Step> newSteps, RadTreeNode node, int mode)
        {
            newSteps.ForEach(p => CopyStepIntoNode(p, node, mode));
        }
        private RadTreeNode CopyStepIntoNode(Step newStep, RadTreeNode node, int mode)
        {
            var names = stepNodes.Values.Select(p => p.Name).ToList();

            ForEachStep(newStep, (s) =>
            {
                string name = GetUniqueStepName(s.Name);
                names.Add(name);
                s.Name = name;
            });

            var newNode = new CustomTreeNode(newStep.Name);
            newNode.Tag = newStep;

            if (mode == 0)
            {
                var parentStep = node.Tag as Step;
                parentStep.Steps.Add(newStep);
                node.Nodes.Add(newNode);
            }
            else
            {
                int index = node.Index;
                if (mode > 0)
                    index++;

                var parentStep = node.Parent.Tag as Step;
                parentStep.Steps.Insert(index, newStep);
                node.Parent.Nodes.Insert(index, newNode);
            }

            AddNodes(newNode);
            newNode.Expand();

            return newNode;
        }
        private void ForEachStep(Step step, Action<Step> action)
        {
            if (step == null)
                return;

            action(step);

            step.Steps.ForEach(p => ForEachStep(p, action));
        }

        private void OnDisposed(object sender, EventArgs e) => WireRunnerEvents(false);
        private void OnBeforeSave(object sender, CancelEventArgs e)
        {
            RadTreeNode repeated = stepNodes
                .GroupBy(p => p.Key.Name)
                .Where(p => p.Count() > 1)
                .Select(n => n.FirstOrDefault().Value)
                .FirstOrDefault();

            if (repeated != null)
            {
                e.Cancel = true;
                MessageBox.Show("Existen nombres repetidos en la secuencia!", "Atención");

                repeated.Selected = true;
                repeated.EnsureVisible();
            }
        }

        public class CustomTreeNode : RadTreeNode
        {
            public CustomTreeNode()
            {
            }
            public CustomTreeNode(string name)
                : base(name)
            {
            }

            public override Image Image
            {
                get
                {
                    if (images.Count == 0)
                        return base.Image;
                    return UnionImage;
                }
                set
                {
                    images.Clear();
                    base.Image = value;
                }
            }
            private Image UnionImage;
            private Dictionary<int, Image> images = new Dictionary<int, Image>();
            public void AddImage(int instance, Image image)
            {
                images[instance] = image;
                UnionImage = GetImageForList(images);
            }
            public void ClearImages()
            {
                images.Clear();
            }
            private Image GetImageForList(Dictionary<int, Image> images)
            {
                if (images.Count == 0 || numInstances == 0)
                    return null;
                if (numInstances == 1)
                    return images.FirstOrDefault().Value;

                var bitmap = new Bitmap(16 * numInstances, 16);
                var g = Graphics.FromImage(bitmap);
                for (int i = 0; i <= numInstances; i++)
                    if (images.ContainsKey(i + 1))
                        g.DrawImage(images[i + 1], 16 * i, 0, 16, 16);
                    else
                        g.DrawImage(Properties.Resources.pending_16, 16 * i, 0, 16, 16);
                return bitmap;
            }
        }
    }
}
