﻿using Dezac.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Dezac.Data.ViewModels;

namespace Dezac.Services
{
    public interface IReprocesosService : IDisposable
    {
        List<ANOMALIAREPFAB> GetAnomalias();
        List<ACTUACIONREPFAB> GetActuaciones();
        int Insert(int? numOrden, int numProducto, int version, int numTestFase, int numMatricula, string empleadoId, string actuacionId);
    }

    public class ReprocesosService : IReprocesosService
    {
        private DezacContext db = new Dezac.Data.DezacContext();

        public List<ANOMALIAREPFAB> GetAnomalias()
        {
            return db.ANOMALIAREPFAB.ToList();
        }

        public List<ACTUACIONREPFAB> GetActuaciones()
        {
            return db.ACTUACIONREPFAB.ToList().Where(p => p.IDACTUACION.StartsWith("O")).ToList();
        }

        public int Insert(int? numOrden, int numProducto, int version, int numTestFase, int numMatricula, string empleadoId, string actuacionId)
        {
            var item = new REPROCESOFAB
            {
                FECHAALTA = DateTime.Now,
                IDEMPLEADO = empleadoId,
                NUMPRODUCTO = numProducto,
                VERSION = version,
                NUMMATRICULA = numMatricula,
                NUMORDEN = numOrden,
                NUMTESTFASE = numTestFase,
                NOTAS = "GENERADO SISTEMA TEST",
                SITUACION = "E"
            };

            item.NUMREPROCESO = db.GetNextSerie("REPROCESOFABNUMREPROCESO");

            db.REPROCESOFAB.Add(item);

            var aItem = new RF_ACTUACION
            {
                NUMACTUACION = db.GetNextSerie("RF_ACTUACIONNUMACTUACION"),
                NUMREPROCESO = item.NUMREPROCESO,
                IDACTUACION = actuacionId,
                IDEMPLEADO = item.IDEMPLEADO,
                FECHA = DateTime.Now,
            };

            item.RF_ACTUACION.Add(aItem);

            db.SaveChanges();

            return item.NUMREPROCESO;
        }

        public void Dispose()
        {
            if (db != null)
                db.Dispose();
        }
    }
}
