﻿using Dezac.Tests.Actions.Kernel;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(AddVariableDateTimeDescription))]
    public class AddVariableDateTime : ActionBase
    {
        //TODO: UNIFICAR en un action generico para fechas
        [Description("Lista de las variables tipo DateTime a crear para utilizar en el test")]
        [Category("4. Results")]
        public DateTimeValuePair[] DataOutput { get; set; }

        private string ResolveDateTime(DateTimeEnum value, string format)
        {
            switch (value)
            {
                case DateTimeEnum.TimeNow:
                    return DateTime.Now.TimeOfDay.ToString();
                case DateTimeEnum.Now:
                    return DateTime.Now.ToString(format);
                case DateTimeEnum.NowUTC:
                    return DateTime.UtcNow.ToString(format); 
                case DateTimeEnum.Date:
                    return DateTime.Today.ToString(format); 
                case DateTimeEnum.Year:
                    if (string.IsNullOrEmpty(format))
                        return DateTime.Now.Year.ToString(format);
                    else
                        return DateTime.Now.ToString(format);
                case DateTimeEnum.Month:
                    if (string.IsNullOrEmpty(format))
                        return DateTime.Now.Month.ToString(format); 
                    else
                        return DateTime.Now.ToString(format);
                case DateTimeEnum.Day:
                    if (string.IsNullOrEmpty(format))
                        return DateTime.Now.Day.ToString(format); 
                    else
                        return DateTime.Now.ToString(format);
                case DateTimeEnum.Week:
                    CultureInfo ciCurr = CultureInfo.CurrentCulture;
                    int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                    return weekNum.ToString("D2");
                default:
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("AddVariableDateTime ENUMERADO DATETIME INCORRECTO").Throw();
                    return null;
            }
        }
        public override void Execute()
        {
            if (DataOutput != null)
            {
                foreach (var item in DataOutput)
                {
                    var value = ResolveDateTime(item.Value, item.Format);
                    Logger.InfoFormat("Add DateTime Variable {0} = {1}", item.Key, value);
                    Context.ResultList.Add(string.Format("Add DateTime Variable {0} = {1}", item.Key, value));
                    SetVariable(item.Key, value);
                }
            }
        }
    }


    

    public class DateTimeValuePair
    {
        [Category("2. Formato")]
        [Description("Formato que obtendrá el valor de la fecha o tiempo de una forma específica. Mirar ayuda de formato de fecha en MSDN")]
        public string Format { get; set; }

        [Category("1. Nombre de la Variable")]
        [Description("Nombre de la variable a crear en el contexto del test")] 
        public string Key { get; set; }

        [Category("3. Valor de la Variable")]
        [Description("Valor de la variable creada con el nombre asignado en la porpiedad Key")]
        public DateTimeEnum Value { get; set; }
    }


    public enum DateTimeEnum
    {
        Now =0,
        NowUTC = 1,
        Date = 2,
        Year = 3,
        Month =4,
        Week =5,
        Day = 6,
        TimeNow=7
    }

    public class AddVariableDateTimeDescription : ActionDescription
    {
        public override string Description { get { return "Este action sirve para crear datod del valor de tipo DateTime (tiempo o fecha) que puede venir de lo siguiente: 1.Un valor constante 2.Contexto del test 3.TestInfo 4.Base se datos de Dezac para reutilizar su valor"; } }
        public override string Name { get { return "AddVariableDateTime"; }}
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
