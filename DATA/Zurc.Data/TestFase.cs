namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TestFase")]
    public partial class TestFase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Test")]
        public int TestId { get; set; }

        [Required]
        [StringLength(10)]
        [ForeignKey("Fase")]
        public string FaseId { get; set; }

        [StringLength(10)]
        public string EmpleadoId { get; set; }

        public DateTime Fecha { get; set; }

        public int Duracion { get; set; }

        [Required]
        [StringLength(1)]
        public string Resultado { get; set; }

        public int? NumCaja { get; set; }

        [StringLength(50)]
        public string EquipoId { get; set; }

        public int? NumReintentos { get; set; }

        public virtual Fase Fase { get; set; }
        public virtual Test Test { get; set; }

        public virtual List<TestError> Errores { get; set; }
        public virtual List<TestFaseFile> Archivos { get; set; }
        public virtual List<TestStep> Steps { get; set; }
        public virtual List<TestValor> Valores { get; set; }
    }
}
