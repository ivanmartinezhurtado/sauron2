﻿using Dezac.Data;
using Dezac.Services;

namespace Dezac.Tests.Model
{
    public class ConfigurationFile
    {
        private T_TESTCONFIGFILEVERSION t_TESTCONFIGFILEVERSION;
        public string Description { get { return t_TESTCONFIGFILEVERSION.T_TESTCONFIGFILE.DESCRIPCION; } }
        public string Clave { get { return t_TESTCONFIGFILEVERSION.T_TESTCONFIGFILE.CLAVE; } }
        public string FileName { get { return t_TESTCONFIGFILEVERSION.T_TESTCONFIGFILE.NOMBREFICHERO; } }
        public string IdTipoDoc { get {return t_TESTCONFIGFILEVERSION.T_TESTCONFIGFILE.IDTIPODOC; } }
        public byte[] File
        {
            get
            {
                using(DezacService svc = new DezacService())
                {
                    return svc.GetTestConfigFile(t_TESTCONFIGFILEVERSION.NUMTCFV);
                }
            }
        }

        public ConfigurationFile(T_TESTCONFIGFILEVERSION t_TESTCONFIGFILEVERSION)
        {
            this.t_TESTCONFIGFILEVERSION = t_TESTCONFIGFILEVERSION;
        }
    }
}
