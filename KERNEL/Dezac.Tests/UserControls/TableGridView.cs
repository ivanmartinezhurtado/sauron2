﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;

namespace Dezac.Tests.UserControls
{
    public partial class TableGridView : UserControl
    {
        private bool multiSelect;

        public TableGridView()
        {
            InitializeComponent();
        }

        public TableGridView(object data) 
            : this()
        {
            grid.DataSource = data;
        }

        public TableGridView(TableGridViewConfig config)
            : this(config.Data)
        {
            SetVisibleFields(config.Fields);
            SetCheckableView(config.CheckBoxView);
            SetGroupBy(config.GroupBy);

            if (config.Width.HasValue)
                this.Width = config.Width.Value;
             
            radCommandBar1.Visible = config.AllowExcelExport;
            //cmdExcel.Visibility = config.AllowExcelExport ? Telerik.WinControls.ElementVisibility.Visible : Telerik.WinControls.ElementVisibility.Hidden;
        }

        private void SetVisibleFields(string visibleFields)
        {
            if (string.IsNullOrEmpty(visibleFields))
                return;

            var items = visibleFields.Split(';');

            foreach (var col in grid.Columns)
                col.IsVisible = items.Contains(col.FieldName);
        }

        private void SetGroupBy(string fieldsGroup)
        {
            if (string.IsNullOrEmpty(fieldsGroup))
                return;

            grid.EnableGrouping = true;
            grid.MasterTemplate.AllowDragToGroup = false;

            var items = fieldsGroup.Split(';');

            foreach (var col in grid.Columns)
                if (items.Contains(col.FieldName))
                {
                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add(col.FieldName, ListSortDirection.Ascending);
                    grid.GroupDescriptors.Add(descriptor);
                }
        }

        private void SetCheckableView(bool multiSelect)
        {
            this.multiSelect = multiSelect;
            if (!multiSelect)
                return;

            grid.ReadOnly = false;
            var newColumn = new GridViewCheckBoxColumn();
            newColumn.DataType = typeof(bool);
            newColumn.Name = "CHECK";
            newColumn.FieldName = "CHECK";
            newColumn.AllowFiltering = false;
            newColumn.EnableHeaderCheckBox = true;
            newColumn.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;

            grid.MasterTemplate.Columns.Add(newColumn);

            foreach (var col in grid.Columns)
                col.ReadOnly = !col.Name.Contains("CHECK");
        }


        public T GetCurrent<T>()
        {
            var current = grid.CurrentRow.DataBoundItem;

            if (current == null)
                return default(T);

            return (T) current;
        }

        public List<T> GetSelecteds<T>()
        {
            var selectedRowsList = new List<T>();

            if (!multiSelect)
                selectedRowsList.Add(GetCurrent<T>());
            else
                foreach (var row in grid.Rows)
                {
                    var rowIsChecked = row.Cells["CHECK"].Value ?? false;
                    if ((bool)rowIsChecked)
                        selectedRowsList.Add((T)(row.DataBoundItem));
                }

            return selectedRowsList;
        }

        private void cmdExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var exporter = new ExportToExcelML(grid);

                string name = sfd.FileName;
                if (name.IndexOf(".") < 0)
                    name += ".xls";

                exporter.RunExport(name);
                System.Diagnostics.Process.Start(name);
            }
        }

        private void grid_CellClick(object sender, GridViewCellEventArgs e)
        {
            if(multiSelect)
                if(e.RowIndex > -1)
                    grid.Rows[e.RowIndex].Cells["CHECK"].Value = grid.Rows[e.RowIndex].IsSelected;
        }
    }

    public class TableGridViewConfig
    {
        public object Data { get; set; }
        public string Fields { get; set; }
        public int? Width { get; set; }
        public bool AllowExcelExport { get; set; }
        public bool CheckBoxView { get; set; }
        public string GroupBy { get; set; }
    }

}
