﻿using Instruments.IO;
using System;
using System.Threading;

namespace Instruments.Utility
{
    public static class Extensions
    {
        public static bool ReadAllOff(this IOBase.InputList<bool> list, params int[] inputs)
        {
            foreach (int input in inputs)
                if (list.Read(input).Value)
                    return false;

            return true;
        }

        public static bool ReadAllOn(this IOBase.InputList<bool> list, params int[] inputs)
        {
            foreach (int input in inputs)
                if (!list.Read(input).Value)
                    return false;

            return true;
        }

        public static bool WaitAllOff(this IOBase.InputList<bool> list, int timeOut, params int[] inputs)
        {
            DateTime start = DateTime.Now;

            do
            {
                bool allOff = true;

                foreach (int input in inputs)
                    if (list.Read(input).Value)
                    {
                        allOff = false;
                        break;
                    }

                if (allOff)
                    return true;

                Thread.Sleep(10);
            }
            while ((DateTime.Now - start).TotalMilliseconds <= timeOut);

            return false;
        }

        public static bool WaitAllOn(this IOBase.InputList<bool> list, int timeOut, params int[] inputs)
        {
            DateTime start = DateTime.Now;

            do
            {
                bool allOn = true;

                foreach (int input in inputs)
                    if (!list.Read(input).Value)
                    {
                        allOn = false;
                        break;
                    }

                if (allOn)
                    return true;

                Thread.Sleep(10);
            }
            while ((DateTime.Now - start).TotalMilliseconds <= timeOut);

            return false;
        }

        public static bool WaitOn(this IOBase.InputList<bool> list, int input, int timeOut)
        {
            DateTime start = DateTime.Now;

            while (!list[input])
            {
                if ((DateTime.Now - start).TotalMilliseconds > timeOut)
                    return false;

                Thread.Sleep(10);
            }

            return true;
        }

        public static bool WaitOff(this IOBase.InputList<bool> list, int input, int timeOut)
        {
            DateTime start = DateTime.Now;

            while (list[input])
            {
                if ((DateTime.Now - start).TotalMilliseconds > timeOut)
                    return false;

                Thread.Sleep(10);
            }

            return true;
        }
   }
}
