﻿using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Forms
{
    public partial class InputMatrixImageView : UserControl, IDialogView
    {
        public int Rows { get; set; }
        public int Columns { get; set; }

        public int TotalItems { get; set; }

        public Func<int, int, int, Control> ItemInitializer { get; set; }
        public Func<InputMatrixImageView, bool> ValidateViewHandler { get; set; }

        public List<Control> Views { get; private set; }

        public InputMatrixImageView()
        {
            InitializeComponent();
        }

        public InputMatrixImageView(int rows, int columns, int? totalItems, Func<int, int, int, Control> itemInitializer, Func<InputMatrixImageView, bool> validateView)
            : this()
        {

            Rows = rows;
            Columns = columns;
            TotalItems = totalItems.GetValueOrDefault(Rows * Columns);
            ItemInitializer = itemInitializer;
            ValidateViewHandler = validateView;

            Build();
        }

        public event DialgoResultEventHandler CloseDialog;

        protected void Build()
        {
            Views = new List<Control>();

            layout.RowCount = Rows;
            layout.ColumnCount = Columns;

            if (TotalItems == 0)
                TotalItems = Rows * Columns;

            int num = 0;

            for (int r = 0; r < Rows; r++)
            for (int c = 0; c < Columns; c++, num++)
                if (num < TotalItems)
                {
                    var item = ItemInitializer(Rows, Columns, num);
                    var size = item.Size;

                    if (c == 0 && r == 0)
                        this.Size = new Size((size.Width + 5 ) * Columns, (size.Height + 5 ) * Rows);

                    Views.Add(item);

                    layout.Controls.Add(item, c, r);

                    if (r == 0)
                    {
                        if (c >= layout.ColumnStyles.Count)
                            layout.ColumnStyles.Add(new ColumnStyle { SizeType = SizeType.Absolute, Width = size.Width });
                        else
                        {
                            var style = layout.ColumnStyles[c];
                            style.SizeType = SizeType.Absolute;
                            style.Width = size.Width;
                        }
                    }
                    if (c == 0)
                    {
                        if (r >= layout.RowStyles.Count)
                            layout.RowStyles.Add(new RowStyle { SizeType = SizeType.Absolute, Height = size.Height });
                        else
                        {
                            var style = layout.RowStyles[r];
                            style.SizeType = SizeType.Absolute;
                            style.Height = size.Height;
                        }
                    }
                }
        }

        public virtual bool ValidateView(DialogResult result)
        {
            if (result != DialogResult.OK || ValidateViewHandler == null)
                return true;

            return ValidateViewHandler(this);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Enter) && GetItemFromActiveControl(this.ActiveControl) != Views.LastOrDefault())
            {
                SendKeys.Send("{TAB}");
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        public T GetView<T>(int idx) where T : Control
        {
            return (T)Views[idx];
        }

        private Control GetItemFromActiveControl(Control ctrl)
        {
            while (ctrl != null && !Views.Contains(ctrl))
                ctrl = ctrl.Parent;

            return ctrl;
        }
      
    }
}
