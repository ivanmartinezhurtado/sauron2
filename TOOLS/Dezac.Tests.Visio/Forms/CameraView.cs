﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.Visio.Forms
{
    public partial class CameraIDSView : UserControl
    {
        // IDS
        private CameraIDS camera;
        private DateTime lastFrame;

        private string imagePath;
        private string saveImagePath;
        private string saveName;

        public string LabelCamera
        {
            get { return lblCamera.Text; }
            set { lblCamera.Text = value; }
        }
        public string LabelImage
        {
            get { return lblImage.Text; }
            set { lblImage.Text = value; }
        }
        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                if (imagePath != value)
                {
                    imagePath = value;
                    LoadImage();
                }
            }
        }
        public string SaveImagePath
        {
            set
            {
                if (saveImagePath != value)
                    saveImagePath = value;
            }
        }
        public string SaveName
        {
            set
            {
                if (saveName != value)
                    saveName = value;
            }
        }

        public CameraIDSView(CameraIDS camera)
        {
            InitializeComponent();

            this.camera = camera;
            if (!DesignMode)
                this.camera.DisplayHandle = cameraImage.Handle;
        }

        public CameraIDSView(CameraIDS camera, string labelCamera, string labelImagen, string rutaImagen)
            :this(camera)
        {
            LabelCamera = labelCamera;
            LabelImage = labelImagen;
            ImagePath = rutaImagen;
        }

        // ??
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Play();
        }

        private async void LoadImage()
        {
            var pic = await Task.Run<Image>(() =>
            {
                if (File.Exists(imagePath))
                    return Image.FromFile(imagePath);

                return null;
            });

            if (pic != null)
                Image = pic;
        }

        public Image Image
        {
            get { return picPatron.Image; }
            set { picPatron.Image = value; }
        }

        public void Play()
        {
            lastFrame = DateTime.Now;

            if (camera != null)
                camera.Start();
        }
        public void Stop()
        {
            if (camera != null)
                camera.Stop();
        }
        public void Restart()
        {
            Stop();
            Play();
        }

        private delegate void DisplayImageDelegate(Bitmap Image);

        private void DisplayImage(Bitmap Image)
        {
            if (cameraImage.InvokeRequired)
                try
                {
                    this.BeginInvoke(new DisplayImageDelegate(DisplayImage), new object[] { Image });
                }
                catch (Exception)
                {
                }
            else
            {
                cameraImage.Image = Image;
                lblCamera.Text = string.Format("CAMARA  ({0:HH:mm:ss})", lastFrame);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!string.IsNullOrEmpty(saveImagePath))
                SaveCameraImage();

            Stop();

            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        public bool AllowSaveCameraImage { get; set; }

        private void cameraImage_DoubleClick(object sender, EventArgs e)
        {
            if (AllowSaveCameraImage)
                SaveCameraImage(true);
        }

        private void SaveCameraImage(bool showDialog=false)
        {                      
            if(showDialog)
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    sfd.Title = "Guardar imagen patrón";
                    sfd.DefaultExt = ".png";
                    sfd.FileName = ImagePath;
                    if (sfd.ShowDialog() == DialogResult.OK)
                        camera.SaveImage(sfd.FileName);                  
                }
            else
            {
                if (!Directory.Exists(saveImagePath))
                    Directory.CreateDirectory(saveImagePath);

                string fileName = Path.Combine(saveImagePath, string.Format("{0}_{1}_{2}.png", saveName, DateTime.Now.ToShortDateString().Replace("/", ""), DateTime.Now.ToLongTimeString().Replace(":", "")));
     
                camera.SaveImage(fileName);
            }
        }
    }
}
