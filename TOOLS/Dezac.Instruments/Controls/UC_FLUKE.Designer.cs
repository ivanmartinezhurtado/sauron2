﻿namespace Dezac.Instruments.Controls
{
    partial class UC_FLUKE
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblL1 = new System.Windows.Forms.Label();
            this.lblL2 = new System.Windows.Forms.Label();
            this.lblL3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.btnRST = new System.Windows.Forms.Button();
            this.lblF = new System.Windows.Forms.Label();
            this.txtFrec = new System.Windows.Forms.TextBox();
            this.rbV = new System.Windows.Forms.RadioButton();
            this.rbI = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCurrent1 = new System.Windows.Forms.Label();
            this.lblCurrent2 = new System.Windows.Forms.Label();
            this.lblCurrent3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.txtV1 = new System.Windows.Forms.TextBox();
            this.txtV2 = new System.Windows.Forms.TextBox();
            this.txtV3 = new System.Windows.Forms.TextBox();
            this.txtI1 = new System.Windows.Forms.TextBox();
            this.txtI2 = new System.Windows.Forms.TextBox();
            this.txtI3 = new System.Windows.Forms.TextBox();
            this.txtP1 = new System.Windows.Forms.TextBox();
            this.txtP2 = new System.Windows.Forms.TextBox();
            this.txtP3 = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.SystemColors.Window;
            this.btnOn.Location = new System.Drawing.Point(129, 189);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(75, 26);
            this.btnOn.TabIndex = 0;
            this.btnOn.Text = "On";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DimGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Window;
            this.button1.Location = new System.Drawing.Point(330, 189);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 26);
            this.button1.TabIndex = 1;
            this.button1.Text = "Off";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // lblL1
            // 
            this.lblL1.AutoSize = true;
            this.lblL1.BackColor = System.Drawing.Color.Transparent;
            this.lblL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblL1.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblL1.Location = new System.Drawing.Point(148, 5);
            this.lblL1.Name = "lblL1";
            this.lblL1.Size = new System.Drawing.Size(29, 20);
            this.lblL1.TabIndex = 2;
            this.lblL1.Text = "L1";
            // 
            // lblL2
            // 
            this.lblL2.AutoSize = true;
            this.lblL2.BackColor = System.Drawing.Color.Transparent;
            this.lblL2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblL2.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblL2.Location = new System.Drawing.Point(257, 5);
            this.lblL2.Name = "lblL2";
            this.lblL2.Size = new System.Drawing.Size(29, 20);
            this.lblL2.TabIndex = 3;
            this.lblL2.Text = "L2";
            // 
            // lblL3
            // 
            this.lblL3.AutoSize = true;
            this.lblL3.BackColor = System.Drawing.Color.Transparent;
            this.lblL3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblL3.ForeColor = System.Drawing.Color.MediumBlue;
            this.lblL3.Location = new System.Drawing.Point(359, 5);
            this.lblL3.Name = "lblL3";
            this.lblL3.Size = new System.Drawing.Size(29, 20);
            this.lblL3.TabIndex = 4;
            this.lblL3.Text = "L3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(138, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "Voltage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(139, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Phase";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 15);
            this.label4.TabIndex = 18;
            this.label4.Text = "Address";
            // 
            // cmb_Port
            // 
            this.cmb_Port.BackColor = System.Drawing.SystemColors.Window;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(9, 22);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(76, 21);
            this.cmb_Port.TabIndex = 19;
            // 
            // btnRST
            // 
            this.btnRST.BackColor = System.Drawing.Color.DimGray;
            this.btnRST.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRST.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRST.ForeColor = System.Drawing.SystemColors.Window;
            this.btnRST.Location = new System.Drawing.Point(234, 189);
            this.btnRST.Name = "btnRST";
            this.btnRST.Size = new System.Drawing.Size(75, 26);
            this.btnRST.TabIndex = 20;
            this.btnRST.Text = "Reset";
            this.btnRST.UseVisualStyleBackColor = false;
            this.btnRST.Click += new System.EventHandler(this.btnRST_Click);
            // 
            // lblF
            // 
            this.lblF.AutoSize = true;
            this.lblF.BackColor = System.Drawing.Color.Transparent;
            this.lblF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblF.Location = new System.Drawing.Point(421, 29);
            this.lblF.Name = "lblF";
            this.lblF.Size = new System.Drawing.Size(73, 15);
            this.lblF.TabIndex = 24;
            this.lblF.Text = "Frequency";
            // 
            // txtFrec
            // 
            this.txtFrec.BackColor = System.Drawing.SystemColors.Window;
            this.txtFrec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrec.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtFrec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrec.ForeColor = System.Drawing.Color.Black;
            this.txtFrec.Location = new System.Drawing.Point(429, 46);
            this.txtFrec.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtFrec.Name = "txtFrec";
            this.txtFrec.Size = new System.Drawing.Size(57, 26);
            this.txtFrec.TabIndex = 39;
            this.txtFrec.Text = "50";
            this.txtFrec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rbV
            // 
            this.rbV.AutoSize = true;
            this.rbV.BackColor = System.Drawing.Color.Transparent;
            this.rbV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbV.Location = new System.Drawing.Point(5, 42);
            this.rbV.Margin = new System.Windows.Forms.Padding(2);
            this.rbV.Name = "rbV";
            this.rbV.Size = new System.Drawing.Size(73, 19);
            this.rbV.TabIndex = 40;
            this.rbV.Text = "Voltage";
            this.rbV.UseVisualStyleBackColor = false;
            // 
            // rbI
            // 
            this.rbI.AutoSize = true;
            this.rbI.BackColor = System.Drawing.Color.Transparent;
            this.rbI.Checked = true;
            this.rbI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbI.Location = new System.Drawing.Point(5, 18);
            this.rbI.Margin = new System.Windows.Forms.Padding(2);
            this.rbI.Name = "rbI";
            this.rbI.Size = new System.Drawing.Size(72, 19);
            this.rbI.TabIndex = 41;
            this.rbI.TabStop = true;
            this.rbI.Text = "Current";
            this.rbI.UseVisualStyleBackColor = false;
            this.rbI.CheckedChanged += new System.EventHandler(this.rbI_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(241, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 15);
            this.label2.TabIndex = 42;
            this.label2.Text = "Voltage";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(347, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 15);
            this.label6.TabIndex = 43;
            this.label6.Text = "Voltage";
            // 
            // lblCurrent1
            // 
            this.lblCurrent1.AutoSize = true;
            this.lblCurrent1.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrent1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrent1.Location = new System.Drawing.Point(138, 74);
            this.lblCurrent1.Name = "lblCurrent1";
            this.lblCurrent1.Size = new System.Drawing.Size(54, 15);
            this.lblCurrent1.TabIndex = 44;
            this.lblCurrent1.Text = "Current";
            // 
            // lblCurrent2
            // 
            this.lblCurrent2.AutoSize = true;
            this.lblCurrent2.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrent2.Location = new System.Drawing.Point(236, 74);
            this.lblCurrent2.Name = "lblCurrent2";
            this.lblCurrent2.Size = new System.Drawing.Size(54, 15);
            this.lblCurrent2.TabIndex = 45;
            this.lblCurrent2.Text = "Current";
            // 
            // lblCurrent3
            // 
            this.lblCurrent3.AutoSize = true;
            this.lblCurrent3.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrent3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrent3.Location = new System.Drawing.Point(347, 74);
            this.lblCurrent3.Name = "lblCurrent3";
            this.lblCurrent3.Size = new System.Drawing.Size(54, 15);
            this.lblCurrent3.TabIndex = 46;
            this.lblCurrent3.Text = "Current";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(242, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Phase";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(349, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "Phase";
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(127, 170);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(279, 10);
            this.pbStatus.TabIndex = 58;
            this.pbStatus.Visible = false;
            // 
            // txtV1
            // 
            this.txtV1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV1.ForeColor = System.Drawing.Color.Blue;
            this.txtV1.Location = new System.Drawing.Point(119, 46);
            this.txtV1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV1.Name = "txtV1";
            this.txtV1.Size = new System.Drawing.Size(84, 26);
            this.txtV1.TabIndex = 59;
            this.txtV1.Text = "230";
            this.txtV1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtV2
            // 
            this.txtV2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV2.ForeColor = System.Drawing.Color.Blue;
            this.txtV2.Location = new System.Drawing.Point(223, 46);
            this.txtV2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV2.Name = "txtV2";
            this.txtV2.Size = new System.Drawing.Size(84, 26);
            this.txtV2.TabIndex = 60;
            this.txtV2.Text = "230";
            this.txtV2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtV3
            // 
            this.txtV3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV3.ForeColor = System.Drawing.Color.Blue;
            this.txtV3.Location = new System.Drawing.Point(327, 46);
            this.txtV3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV3.Name = "txtV3";
            this.txtV3.Size = new System.Drawing.Size(84, 26);
            this.txtV3.TabIndex = 61;
            this.txtV3.Text = "230";
            this.txtV3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtI1
            // 
            this.txtI1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtI1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI1.ForeColor = System.Drawing.Color.Red;
            this.txtI1.Location = new System.Drawing.Point(119, 93);
            this.txtI1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtI1.Name = "txtI1";
            this.txtI1.Size = new System.Drawing.Size(84, 26);
            this.txtI1.TabIndex = 62;
            this.txtI1.Text = "0";
            this.txtI1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtI2
            // 
            this.txtI2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtI2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI2.ForeColor = System.Drawing.Color.Red;
            this.txtI2.Location = new System.Drawing.Point(223, 93);
            this.txtI2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtI2.Name = "txtI2";
            this.txtI2.Size = new System.Drawing.Size(84, 26);
            this.txtI2.TabIndex = 63;
            this.txtI2.Text = "0";
            this.txtI2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtI3
            // 
            this.txtI3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtI3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI3.ForeColor = System.Drawing.Color.Red;
            this.txtI3.Location = new System.Drawing.Point(328, 93);
            this.txtI3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtI3.Name = "txtI3";
            this.txtI3.Size = new System.Drawing.Size(83, 26);
            this.txtI3.TabIndex = 64;
            this.txtI3.Text = "0";
            this.txtI3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtP1
            // 
            this.txtP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtP1.ForeColor = System.Drawing.Color.Green;
            this.txtP1.Location = new System.Drawing.Point(119, 137);
            this.txtP1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtP1.Name = "txtP1";
            this.txtP1.Size = new System.Drawing.Size(84, 26);
            this.txtP1.TabIndex = 65;
            this.txtP1.Text = "0";
            this.txtP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtP2
            // 
            this.txtP2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtP2.ForeColor = System.Drawing.Color.Green;
            this.txtP2.Location = new System.Drawing.Point(223, 137);
            this.txtP2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtP2.Name = "txtP2";
            this.txtP2.Size = new System.Drawing.Size(84, 26);
            this.txtP2.TabIndex = 66;
            this.txtP2.Text = "120";
            this.txtP2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtP3
            // 
            this.txtP3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtP3.ForeColor = System.Drawing.Color.Green;
            this.txtP3.Location = new System.Drawing.Point(328, 137);
            this.txtP3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtP3.Name = "txtP3";
            this.txtP3.Size = new System.Drawing.Size(83, 26);
            this.txtP3.TabIndex = 67;
            this.txtP3.Text = "240";
            this.txtP3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.rbI);
            this.groupBox1.Controls.Add(this.rbV);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(80, 69);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.PowerSourceFLUKE;
            this.pictureBox1.Location = new System.Drawing.Point(3, 170);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 69;
            this.pictureBox1.TabStop = false;
            // 
            // UC_FLUKE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtP1);
            this.Controls.Add(this.txtP2);
            this.Controls.Add(this.txtP3);
            this.Controls.Add(this.txtI1);
            this.Controls.Add(this.txtI2);
            this.Controls.Add(this.txtI3);
            this.Controls.Add(this.txtV1);
            this.Controls.Add(this.txtV2);
            this.Controls.Add(this.txtV3);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblCurrent3);
            this.Controls.Add(this.lblCurrent2);
            this.Controls.Add(this.lblCurrent1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtFrec);
            this.Controls.Add(this.lblF);
            this.Controls.Add(this.btnRST);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblL3);
            this.Controls.Add(this.lblL2);
            this.Controls.Add(this.lblL1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnOn);
            this.Name = "UC_FLUKE";
            this.Size = new System.Drawing.Size(510, 227);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblL1;
        private System.Windows.Forms.Label lblL2;
        private System.Windows.Forms.Label lblL3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.Button btnRST;
        private System.Windows.Forms.Label lblF;
        private System.Windows.Forms.TextBox txtFrec;
        private System.Windows.Forms.RadioButton rbV;
        private System.Windows.Forms.RadioButton rbI;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCurrent1;
        private System.Windows.Forms.Label lblCurrent2;
        private System.Windows.Forms.Label lblCurrent3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.TextBox txtV1;
        private System.Windows.Forms.TextBox txtV2;
        private System.Windows.Forms.TextBox txtV3;
        private System.Windows.Forms.TextBox txtI1;
        private System.Windows.Forms.TextBox txtI2;
        private System.Windows.Forms.TextBox txtI3;
        private System.Windows.Forms.TextBox txtP1;
        private System.Windows.Forms.TextBox txtP2;
        private System.Windows.Forms.TextBox txtP3;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
