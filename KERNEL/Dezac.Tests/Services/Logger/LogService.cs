﻿using Dezac.Tests.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Xsl;

namespace Dezac.Tests.Services.Logger
{
    public static class LogService
    {
        public static List<LoggingEventWrapper> MyEvents = new List<LoggingEventWrapper>();
        public static IRunnerExtensions RunnerExtensions { get; set; }

        static LogService()
        {
            LogInterceptor.OnLog += (s, ev) => {
                MyEvents.Add(ev);
            };
        }

        public static void Clear()
        {
            MyEvents.Clear();
        }

        public static void ShowLog(FormatLogViewer.LogsTypesEnum styleName, bool save, ITestContext context, bool IsProcess, string fileName = "")
        {
            var instances = MyEvents.Where(ev => ev.NumInstance != 0).GroupBy(p => p.NumInstance);

            foreach (var numInstance in instances)
                ShowLog(styleName, save, numInstance.Key, context, IsProcess, fileName);
        }

        public static void ShowLog(FormatLogViewer.LogsTypesEnum styleName, bool save, int? numInstance, ITestContext context, bool IsProcess, string fileName =  "")
        {
            var builder = new FormatLogViewer();

            try
            {
                var argsList = new XsltArgumentList();

                if (context != null)
                {
                    argsList.AddParam("IdEmpleado", "", context.IdEmpleado);
                    argsList.AddParam("NumProducto", "", context.NumProducto);
                    argsList.AddParam("Version", "", context.Version);

                    argsList.AddParam("DescripcionProducto", "", context.DescFamilia ?? string.Empty);
                    argsList.AddParam("IdFase", "", context.IdFase);
                    argsList.AddParam("PC", "", context.PC ?? string.Empty);
                    argsList.AddParam("Fecha", "", context.TestInfo.StartTime.ToString("dd/MM/yyyy HH:mm"));

                    argsList.AddParam("NumSerie", "", context.TestInfo != null ? context.TestInfo.NumSerie ?? string.Empty : string.Empty);
                    argsList.AddParam("NumBastidor", "", context.TestInfo != null && context.TestInfo.NumBastidor.HasValue ? context.TestInfo.NumBastidor.ToString() ?? string.Empty : string.Empty);

                    argsList.AddParam("VersionSauron", "", context.SauronVersion);
                    argsList.AddParam("InterfazSauron", "", context.SauronInterface);
                }

                if (save && context != null && context.TestInfo.NumTestFase.HasValue)
                    using (var stream = new MemoryStream())
                    {
                        builder.BuildHtml(stream, styleName, LogService.MyEvents, argsList, numInstance);

                        var extensions = RunnerExtensions ?? new DezacRunnerExtensions();

                        if (IsProcess)
                            extensions.SaveProcessFaseFile(context.TestInfo.NumTestFase.Value, stream.ToArray());
                        else
                            extensions.SaveFileNumTestFase(context.TestInfo.NumTestFase.Value, "Informe Test", "84", stream.ToArray());
                    }
                else
                {
                    fileName = fileName != "" ? fileName :
                        Path.Combine(Path.GetTempPath(), string.Format("Test_{0:ddMMyyHHmmss}_{1}.html", DateTime.Now, numInstance));
                    builder.BuildHtml(fileName, styleName, LogService.MyEvents, argsList, numInstance);
                    if (!save)
                        if (File.Exists(fileName))
                            Process.Start(fileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }         
        }
    }
}
