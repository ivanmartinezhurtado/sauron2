﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.07)]
    public class CBS4 : DeviceBase
    {
        #region Instanciacion y destrucción de la clase del equipo

        public CBS4()
        {
        }

        public CBS4(int port = 3)
        {
            SetPort(port);
            Modbus.UseFunction06 = true;
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
            {
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
                Modbus.Retries = 15;
                Modbus.WaitToRetryMilliseconds = 75;
                Modbus.TimeOut = 100;
            }
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
            Modbus.UseFunction06 = true;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }


        #endregion

        #region Operaciones de lectura

        public ushort ReadTriggerCurrentSelectionChannel(Channel channel)
        {
            LogMethod();
            
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_4;
                    break;
                default:
                    break;
            }
            return Modbus.ReadRegister(address);
        }

        public ushort ReadTriggerTimeSelectionChannel(Channel channel)
        {
            LogMethod();
            
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_4;
                    break;
                default:
                    break;
            }
            return Modbus.ReadRegister(address);
        }

        public RelayOutputsPolarity ReadRelayOutputsPolarity()
        {
            LogMethod();
            
            return (RelayOutputsPolarity)Modbus.ReadRegister((ushort)Registers.RELAY_OUTPUTS_POLARITY);
        }

        public ushort ReadPrealarmRelayLevel()
        {
            LogMethod();

             return (ushort)Modbus.ReadRegister((ushort)Registers.PREALARM_SETTING_CONFIGURATION);
        }

        public ushort ReadReconectionState()
        {
            LogMethod();

            return (ushort)Modbus.ReadRegister((ushort)Registers.RELAY_OUTPUTS_WORK_MODE);
        }

        public ushort ReadReconnectionSequenceSelectionChannel(Channel channel)
        {
            LogMethod();
            
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.RECONNECTION_SEQUENCE_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.RECONNECTION_SEQUENCE_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.RECONNECTION_SEQUENCE_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.RECONNECTION_SEQUENCE_CHANNEL_4;
                    break;
                default:
                    break;
            }
            return Modbus.ReadRegister(address);
        }

        public uint ReadSerialNumber()
        {
            LogMethod();
            return (uint)((Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_HIGH_LEVEL) << 16) + Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_LOW_LEVEL));
        }

        public uint ReadBastidorNumber()
        {
            LogMethod();
            return (uint)((Modbus.ReadRegister((ushort)Registers.FABRICATION_NUMBER_HI) << 16) + Modbus.ReadRegister((ushort)Registers.FABRICATION_NUMBER_LOW));
        }

        public ushort ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION);
        }

        public ushort ReadTablesVersion()
        {
            LogMethod();
            
            return Modbus.ReadRegister((ushort)Registers.TABLES_VERSION);
        }

        public ushort ReadHardwareVersion()
        {
            LogMethod();
            
            return Modbus.ReadRegister((ushort)Registers.HARDWARE_VERSION);
        }

        public ModelType ReadModelVersion()
        {
            LogMethod();

            return (ModelType)Modbus.ReadRegister((ushort)Registers.MODEL_VERSION);
        }

        public OutputsStatus ReadOutputStatus()
        {
            LogMethod();
            
            return (OutputsStatus)Modbus.ReadRegister((ushort)Registers.OUTPUTS_STATUS);
        }

        public ushort ReadLeakCurrentRMSValueChannel(Channel channel)
        {
            LogMethod();
            
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.VRMS_LEAK_CURRENT_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.VRMS_LEAK_CURRENT_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.VRMS_LEAK_CURRENT_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.VRMS_LEAK_CURRENT_CHANNEL_4;
                    break;
                default:
                    break;
            }
            return Modbus.ReadRegister(address);
        }

        public ushort ReadTriggerCurrentRMSValueChannel(Channel channel)
        {
            LogMethod();
            
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.VRMS_TRIGGER_CURRENT_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.VRMS_TRIGGER_CURRENT_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.VRMS_TRIGGER_CURRENT_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.VRMS_TRIGGER_CURRENT_CHANNEL_4;
                    break;
                default:
                    break;
            }
            return Modbus.ReadRegister(address);
        }

        public ushort ReadCurrentTableElement(byte scale)
        {
            LogMethod();
            
            
            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Thread.Sleep(500);

            Modbus.Write((ushort)Registers.CURRENT_TABLE, (ushort)value);

            Thread.Sleep(500);

            return Modbus.ReadRegister((ushort)Registers.CURRENT_TABLE);
        }

        public ushort ReadTimeTableElement(byte scale)
        {
            LogMethod();
            
            
            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TIME_TABLE, (ushort)value);

            Thread.Sleep(200);

            return Modbus.ReadRegister((ushort)Registers.TIME_TABLE);
        }

        public Inputs ReadInputsState()
        {
            LogMethod();
            
            return (Inputs)Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public ushort ReadCurrentlySelectedCalibrationChannel()
        {
            LogMethod();
            
            return Modbus.ReadRegister((ushort)Registers.CHANNEL_SELECTION_CALIBRATION_WRITTING);
        }

        public ushort ReadCalibrationConstantChannelScale(Channel scale)
        {
            LogMethod();
            
            ushort address = 0;
            switch (scale)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_4;
                    break;
                default:
                    break;
            }
            return Modbus.ReadRegister(address);

        }

        public ushort ReadCodeError()
        {
            LogMethod();
            
            return Modbus.ReadRegister((ushort)Registers.FABRICATION_ERROR_CODE);
        }

        public ushort ReadCodeErrorCC()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.CALIBRATION_CONSTANT_SCALE_4);
        }

        public uint ReadCalibrationResultChannel(Channel scale)
        {
            LogMethod();
            
            ushort addressHigh = 0;
            ushort addressLow = 0;
            switch (scale)
            {
                case Channel.CHANNEL_1:
                    addressHigh = (ushort)Registers.HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_1;
                    addressLow = (ushort)Registers.LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    addressHigh = (ushort)Registers.HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_2;
                    addressLow = (ushort)Registers.LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    addressHigh = (ushort)Registers.HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_3;
                    addressLow = (ushort)Registers.LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    addressHigh = (ushort)Registers.HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_4;
                    addressLow = (ushort)Registers.LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_4;
                    break;
                default:
                    break;
            }
            return (uint)((Modbus.ReadRegister(addressHigh) << 16) + Modbus.ReadRegister(addressLow));
        }

        public ushort ReadCommunicationsBoardVersion(string protocolo)
        {
            LogMethod();

            if (protocolo == "JOHNSON")
                return Modbus.ReadRegister((ushort) Registers.COMMUNICATIONS_BOARD_VERSION_JONSHON);
            else
                return Modbus.ReadRegister((ushort)Registers.COMMUNICATIONS_BOARD_VERSION);   
        }

        public Keyboard ReadKeyboard()
        {
            LogMethod();
            
            
            var keyboardReading = (ushort)ReadInputsState() & 0x0FFF;
            if (Enum.IsDefined(typeof(Keyboard), (int)keyboardReading))
                return (Keyboard)keyboardReading;

            throw new Exception("Error en la detección del teclado, se detecta la activación de otras entradas");
        }

        public ScaleCalibrationResults ReadCalibrationResultsAllChannels()
        {
            LogMethod();
            
            var calibrationResults = new ScaleCalibrationResults();

            calibrationResults.Channel1 = ReadCalibrationResultChannel(Channel.CHANNEL_1);
            calibrationResults.Channel2 = ReadCalibrationResultChannel(Channel.CHANNEL_2);
            calibrationResults.Channel3 = ReadCalibrationResultChannel(Channel.CHANNEL_3);
            calibrationResults.Channel4 = ReadCalibrationResultChannel(Channel.CHANNEL_4);

            return calibrationResults;
        }

        public string ReadScaleTable(TableKind typeTable)
        {
            LogMethod();
            
            
            StringBuilder table = new StringBuilder();

            for (byte pos = 0; pos < 255; pos++)
            {
                var valor = typeTable == TableKind.CURRENT ? ReadCurrentTableElement(pos) : ReadTimeTableElement(pos);

                if (valor == ushort.MaxValue)
                    break;

                table.Append(valor + ";");
            }

            return table.ToString();
        }

        public void ReadReconectionTable(ReconnectionTableConfiguration srdcTable)
        {
            LogMethod();

            byte position = 0;

            foreach (var item in srdcTable.ReconectionSequenceTable)
            {
                var value = ReadReconectionTableElement(position);
                if (value != item.Value.AmountOfReconnections)
                    throw new Exception("Error la tabla grabada de reconexion no coincide con la leida");

                position += 2;
                value = ReadReconectionTableElement(position);
                if (value != item.Value.ReconnectionTime)
                    throw new Exception("Error la tabla grabada de reconexion no coincide con la leida");

                position += 2;
                foreach (var sec in item.Value.SequenceTime)
                {
                    value = ReadReconectionTableElement(position);
                    if (value != Convert.ToUInt16(sec))
                        throw new Exception("Error la tabla grabada de reconexion no coincide con la leida");
                    position += 2;
                }
            }
        }

        public ushort ReadReconectionTableElement(byte position)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RECONNECTION_TABLE_POSITION, (ushort)(position));
            Thread.Sleep(500);
            return Modbus.ReadRegister((ushort)Registers.RECONNECTION_TABLE_VALUE);
        }

        #endregion

        #region Operaciones de escritura

        public void WriteTriggerCurrentSelectionChannel(Channel channel, ushort scaleSelection)
        {
            LogMethod();
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.CURRENT_SCALE_SELECTION_CHANNEL_4;
                    break;
                default:
                    break;
            }
            Modbus.Write(address, scaleSelection);
        }

        public void WriteTriggerTimeSelectionChannel(Channel channel, ushort scaleSelection)
        {
            LogMethod();
            ushort address = 0;
            switch (channel)
            {
                case Channel.CHANNEL_1:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    address = (ushort)Registers.TIME_SCALE_SELECTION_CHANNEL_4;
                    break;
                default:
                    break;
            }
            Modbus.Write(address, scaleSelection);
        }

        public void WriteRelayOutputsPolarity(params RelayOutputsPolarity[] outputsPolarity)
        {
            LogMethod();
            ushort relayPolarityConfiguration = 0;
            foreach (RelayOutputsPolarity relayPolarity in outputsPolarity)
                relayPolarityConfiguration |= (ushort)relayPolarity;
            Modbus.Write((ushort)Registers.RELAY_OUTPUTS_POLARITY, relayPolarityConfiguration);
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER_HIGH_LEVEL, (int)serialNumber);
        }

        public void WriteBastidorNumber(uint bastidor)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.FABRICATION_NUMBER_HI, (int)bastidor);
        }

        public void WriteChannelManagement(ChannelManagement channelAction)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS_MANAGEMENT, (ushort)channelAction);
            Thread.Sleep(500);
        }

        public void WriteConfigurationManagement(ConfigurationWritting configuration)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONFIGURATION_WRITTING_MANAGEMENT, (ushort)configuration);
            Thread.Sleep(1000);
        }

        public void WriteWorkMode(WorkModes mode)
        {
            LogMethod();

            if (mode == WorkModes.RESET)
                WriteReset();
            else
                Modbus.Write((ushort)Registers.DEVICE_WORK_MODE, (ushort)mode);

            Thread.Sleep(500);
        }

        public void WriteReset()
        {
            int modbusRetries = Modbus.Retries;
            Modbus.Retries = 0;
                try
                {
                    Modbus.Write((ushort)Registers.DEVICE_WORK_MODE, (ushort)WorkModes.RESET);
                }
                catch
                {
                }
                Modbus.Retries = modbusRetries;
        }

        public void WriteStartCalibration(ScaleCalibrations scale)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DEVICE_WORK_MODE, (ushort)scale);
        }

        public void WriteTableClear(TablesClearOrders clearOrder)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TABLES_CLEAR, (ushort)clearOrder);
        }

        public void WriteCurrentTableElement(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CURRENT_TABLE, value);
            Thread.Sleep(200);
        }

        public void WriteTimeTableElement(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TIME_TABLE, (ushort)value);
            Thread.Sleep(200);
        }

        public void WriteChannelSelectionScaleConstant(Channel channel)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CHANNEL_SELECTION_CALIBRATION_WRITTING, (ushort)channel);
        }

        public void WriteScaleConstant(ScaleCalibrations scale, ushort calibrationConstant)
        {
            LogMethod();
            ushort address = 0;
            switch (scale)
            {
                case ScaleCalibrations.SCALE_1:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_1;
                    break;
                case ScaleCalibrations.SCALE_2:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_2;
                    break;
                case ScaleCalibrations.SCALE_3:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_3;
                    break;
                case ScaleCalibrations.SCALE_4:
                    address = (ushort)Registers.CALIBRATION_CONSTANT_SCALE_4;
                    break;
                default:
                    break;
            }
            Modbus.Write(address, calibrationConstant);
        }

        public void WriteErrorCode(ushort errorCode)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FABRICATION_ERROR_CODE, errorCode);
        }

        public void WriteTablesVersion(ushort tableVersion)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TABLES_VERSION, tableVersion);
        }

        public void WriteHardwareVersion(ushort hardwareVersion)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.HARDWARE_VERSION, hardwareVersion);
        }

        public void WriteDisplay(DisplayOperations displayOperation)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)displayOperation);
        }

        public void WriteLeds(Leds led)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)led);
        }

        public void WriteRelays(Relays output)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)output);
        }

        public void WriteScaleTable(ScaleTableConfiguration currentScales, TableKind typeTable)
        {
            LogMethod();
            byte position = 0;
            int positionValue = 0;

            foreach (var elemnt in currentScales.ListElemnts)
            {
                positionValue = position << 11;
                var value = positionValue + ((elemnt.X100 ? 1 : 0) << 10) + elemnt.ElementValue;

                if (typeTable == TableKind.CURRENT)
                    WriteCurrentTableElement((ushort)value);
                else
                    WriteTimeTableElement((ushort)value);

                position++;
                Thread.Sleep(500);
            }

            positionValue = position << 11;

            if (typeTable == TableKind.CURRENT)
                WriteCurrentTableElement(ushort.MaxValue); //Grabamos el ultimo elemento con un 0 en la ultima posicion
            else
                WriteTimeTableElement(ushort.MaxValue); //Grabamos el ultimo elemento con un 0 en la ultima posicion

            Thread.Sleep(200);
        }

        public void WriteErrorCodeCC(ushort errorCode)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CALIBRATION_CONSTANT_SCALE_4, errorCode);
        }

        //***************************************************************

        public void WriteReconectionTable(ReconnectionTableConfiguration srdcTable)
        {
            LogMethod();

            byte position = 0;

            foreach (var item in srdcTable.ReconectionSequenceTable)
            {
                WriteReconectionTableElement(position, item.Value.AmountOfReconnections);
                position+=2;
                WriteReconectionTableElement(position, item.Value.ReconnectionTime);
                position+=2;
                foreach (var sec in item.Value.SequenceTime)
                {
                    WriteReconectionTableElement(position, Convert.ToUInt16(sec));
                    position+=2;
                }
            }
        }

        public void WriteReconectionTableElement(byte position, ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RECONNECTION_TABLE_POSITION, (ushort)((0xFF00) + (position)));
            Thread.Sleep(500);
            Modbus.Write((ushort)Registers.RECONNECTION_TABLE_VALUE, value);
            Thread.Sleep(500);
        }

        public void WriteReconectionSelectionChannel(byte scale, Channel channel)
        {
            LogMethod();
            var direcction = Registers.RECONNECTION_SEQUENCE_CHANNEL_1;

            if (scale >= 32)
                throw new Exception("La escala SRD seleccionada no es válida");

            switch (channel)
            {
                case Channel.CHANNEL_1:
                    direcction = Registers.RECONNECTION_SEQUENCE_CHANNEL_1;
                    break;
                case Channel.CHANNEL_2:
                    direcction = Registers.RECONNECTION_SEQUENCE_CHANNEL_2;
                    break;
                case Channel.CHANNEL_3:
                    direcction = Registers.RECONNECTION_SEQUENCE_CHANNEL_3;
                    break;
                case Channel.CHANNEL_4:
                    direcction = Registers.RECONNECTION_SEQUENCE_CHANNEL_4;
                    break;
            }

            Modbus.Write((ushort)direcction, (ushort)scale);
            Thread.Sleep(500);
        }

        public void WriteModelRA()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.MODEL_VERSION, (ushort)ModelType.RA);
            Thread.Sleep(500);
        }

        public void WriteModel(ModelType model)
        {
            LogMethod();
            ushort value = (ushort)(model == ModelType.STD ? 0xFFFF : (ushort)model);
            Modbus.Write((ushort)Registers.MODEL_VERSION, value);
            Thread.Sleep(500);
        }

        public void WritePrealarmRelayLevel(ushort level)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.PREALARM_SETTING_CONFIGURATION, (ushort)level);
        }

        public void WriteReconectionStateSCE()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.RELAY_OUTPUTS_WORK_MODE, (ushort)0x000F);
        }

        //***************************************************************

        #endregion

        #region Enums

        public enum Registers
        {
            CURRENT_SCALE_SELECTION_CHANNEL_1 = 0x0004,
            CURRENT_SCALE_SELECTION_CHANNEL_2 = 0x0005,
            CURRENT_SCALE_SELECTION_CHANNEL_3 = 0x0006,
            CURRENT_SCALE_SELECTION_CHANNEL_4 = 0x0007,
            TIME_SCALE_SELECTION_CHANNEL_1 = 0x0008,
            TIME_SCALE_SELECTION_CHANNEL_2 = 0x0009,
            TIME_SCALE_SELECTION_CHANNEL_3 = 0x000A,
            TIME_SCALE_SELECTION_CHANNEL_4 = 0x000B,
            RELAY_OUTPUTS_WORK_MODE = 0x000C,
            RELAY_OUTPUTS_POLARITY = 0x000D,
            PREALARM_SETTING_CONFIGURATION = 0x000E,
            RECONNECTION_SEQUENCE_CHANNEL_1 = 0x000F,
            RECONNECTION_SEQUENCE_CHANNEL_2 = 0x0010,
            RECONNECTION_SEQUENCE_CHANNEL_3 = 0x0011,
            RECONNECTION_SEQUENCE_CHANNEL_4 = 0x0012,
            SERIAL_NUMBER_HIGH_LEVEL = 0x0013,
            SERIAL_NUMBER_LOW_LEVEL = 0x0014,
            FIRMWARE_VERSION = 0x0015,
            TABLES_VERSION = 0x0016,
            HARDWARE_VERSION = 0x0017,
            MODEL_VERSION = 0x0018,
            OUTPUTS_STATUS = 0x0019,
            VRMS_LEAK_CURRENT_CHANNEL_1 = 0x001A,
            VRMS_LEAK_CURRENT_CHANNEL_2 = 0x001B,
            VRMS_LEAK_CURRENT_CHANNEL_3 = 0x001C,
            VRMS_LEAK_CURRENT_CHANNEL_4 = 0x001D,
            VRMS_TRIGGER_CURRENT_CHANNEL_1 = 0x001E,
            VRMS_TRIGGER_CURRENT_CHANNEL_2 = 0x001F,
            VRMS_TRIGGER_CURRENT_CHANNEL_3 = 0x0020,
            VRMS_TRIGGER_CURRENT_CHANNEL_4 = 0x0021,
            OUTPUTS_MANAGEMENT = 0x0022,
            CONFIGURATION_WRITTING_MANAGEMENT = 0x0023,
            DEVICE_WORK_MODE = 0x0024,
            TABLES_CLEAR = 0x0025,
            CURRENT_TABLE = 0x0026,
            TIME_TABLE = 0x0027,
            RECONNECTION_TABLE_POSITION = 0x0028,
            RECONNECTION_TABLE_VALUE = 0x0029,
            INPUTS = 0x002A,
            OUTPUTS = 0x002B,
            CHANNEL_SELECTION_CALIBRATION_WRITTING = 0x002C,
            CALIBRATION_CONSTANT_SCALE_1 = 0x002D,
            CALIBRATION_CONSTANT_SCALE_2 = 0x002E,
            CALIBRATION_CONSTANT_SCALE_3 = 0x002F,
            CALIBRATION_CONSTANT_SCALE_4 = 0x0030,
            FABRICATION_ERROR_CODE = 0x0031,
            FABRICATION_NUMBER_HI = 0x0031,
            FABRICATION_NUMBER_LOW = 0x0032,
            HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_1 = 0x001A,
            LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_1 = 0x001B,
            HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_2 = 0x001C,
            LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_2 = 0x001D,
            HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_3 = 0x001E,
            LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_3 = 0x001F,
            HIGH_LEVEL_CALIBRATION_RESULT_CHANNEL_4 = 0x0020,
            LOW_LEVEL_CALIBRATION_RESULT_CHANNEL_4 = 0x0021,
            COMMUNICATIONS_BOARD_VERSION = 0xABCD,
            COMMUNICATIONS_BOARD_VERSION_JONSHON = 0xFFFF,
        }

        public enum Channel
        {
            [Description("toroidal 1")]
            CHANNEL_1,
            [Description("toroidal 2")]
            CHANNEL_2,
            [Description("toroidal 3")]
            CHANNEL_3,
            [Description("toroidal 4")]
            CHANNEL_4
        }

        [Flags]
        public enum RelayOutputsPolarity
        {
            ESTANDAR = 0x0000,
            POSITIVA = 0x000F
        }

        public enum PrealarmRelayLevels
        {
            OFF = 0,
            SET_TO_50_PERCENT = 1,
            SET_TO_60_PERCENT = 2,
            SET_TO_70_PERCENT = 3,
            SET_TO_80_PERCENT = 4
        }

        [Flags]
        public enum OutputsStatus
        {
            ALL_OFF = 0x0000,
            COIL_CHANNEL_1 = 0x0001,
            COIL_CHANNEL_2 = 0x0002,
            COIL_CHANNEL_3 = 0x0004,
            COIL_CHANNEL_4 = 0x0008,
            PREALARM = 0x0010,
            COIL_CHANNEL_ALL = 0x000F
        }

        public enum ChannelManagement
        {
            RESET_CHANNEL_1 = 0x0001,
            RESET_CHANNEL_2 = 0x0002,
            RESET_CHANNEL_3 = 0x0004,
            RESET_CHANNEL_4 = 0x0008,
            RESET_ALL = 0x000F,
            TRIGGER_CHANNEL_1 = 0x0100,
            TRIGGER_CHANNEL_2 = 0x0200,
            TRIGGER_CHANNEL_3 = 0x0400,
            TRIGGER_CHANNEL_4 = 0x0800,
            TRIGGER_ALL = 0x0F00,
        }

        public enum ConfigurationWritting
        {
            ENABLE = 0x0000,
            SAVE = 0xFFFF
        }

        public enum WorkModes
        {
            START = 0x0000,
            NORMAL = 0x0001,
            TEST = 0x0007,
            RESET = 0x0008
        }

        public enum ScaleCalibrations
        {
            SCALE_1 = 0x0106,
            SCALE_2 = 0x0206,
            SCALE_3 = 0x0306,
            SCALE_4 = 0x0406,
        }

        public enum TablesClearOrders
        {
            DELETE_CURRENT_TIME_TABLES = 0x00FF,
            DELETE_RECONNECTION_TABLES = 0xFF00
        }

        [Flags]
        public enum Inputs
        {
            [Description("ninguna")]
            NO_INPUT = 0x0000,
            [Description("seleccion corriente")]
            CURRENT_SELECTION_KEY = 0x0001,
            [Description("seleccion polaridad")]
            POLARITY_SELECTION_KEY = 0x0002,
            [Description("seleccion tiempo")]
            TIME_SELECTION_KEY = 0x0004,
            [Description("Test")]
            TEST_KEY = 0x0008,
            [Description("Prog")]
            PROG_KEY = 0x0010,
            [Description("Reset")]
            RESET_KEY = 0x0020,
            [Description("Auto")]
            REC_KEY = 0x0040,
            [Description("entrada externa")]
            EXTERNAL_INPUT_ERROR = 0x0200,
            [Description("toroidal 1")]
            TOROIDAL_ERROR_INPUT_1 = 0x1000,
            [Description("toroidal 2")]
            TOROIDAL_ERROR_INPUT_2 = 0x2000,
            [Description("toroidal 3")]
            TOROIDAL_ERROR_INPUT_3 = 0x4000,
            [Description("toroidal 4")]
            TOROIDAL_ERROR_INPUT_4 = 0x8000,
        }

        public enum DisplayOperations
        {
            ALL_SEGMENTS = 0x0100,
            ODD_SEGMENTS = 0x0200,
            EVEN_SEGMENTS = 0x0400,
            NONE_SEGMENTS = 0x0800,
        }

        public enum Leds
        {
            [Description("la luz verde del backlight y de power")]
            TURN_OFF_BACKLIGHT_POWER_PREALARM_LEDS = 0x0000,
            [Description("el led naranja de prealarma")]
            ORANGE_PREALARM_LED = 0x0080,
            [Description("el led rojo de power")]
            RED_POWER_LED = 0x0020,
            [Description("el led rojo de backlight")]
            RED_BACKLIGHT = 0x0040,
            [Description("el led de prealarma, led rojo de alimentación, led rojo de backlight")]
            PREALARM_LED_RED_POWER_LED_RED_BACKLIGHT = 0x00E0,
        }

        [Flags]
        public enum Keyboard
        {
            [Description("ninguna")]
            NO_INPUT = 0x0000,
            [Description("seleccion corriente")]
            CURRENT_SELECTION_KEY = 0x0001,
            [Description("seleccion polaridad")]
            POLARITY_SELECTION_KEY = 0x0002,
            [Description("seleccion polaridad")]
            TIME_SELECTION_KEY = 0x0004,
            [Description("Test")]
            TEST_KEY = 0x0008,
            [Description("Prog")]
            PROG_KEY = 0x0010,
            [Description("Reset")]
            RESET_KEY = 0x0020,
            [Description("Set")]
            SET_KEY = 0x0040
        }

        public enum Relays
        {
            [Description("desactivacion de cualquiera de los reles")]
            TURN_OFF_RELAYS = 0x0000,
            [Description("rele de disparo canal 1")]
            TRIGGER_RELAY_CHANNEL_1 = 0x0001,
            [Description("rele de disparo canal 2")]
            TRIGGER_RELAY_CHANNEL_2 = 0x0002,
            [Description("rele de disparo canal 3")]
            TRIGGER_RELAY_CHANNEL_3 = 0x0004,
            [Description("rele de disparo canal 4")]
            TRIGGER_RELAY_CHANNEL_4 = 0x0008,
            [Description("rele de prealarma")]
            PREALARM_RELAY = 0x0010,
            TRIGGER_ALL_RELAY = 0x000F
        }

        public enum CalibrationScales
        {
            CALIBRATION_CONSTANT_SCALE_1 = 0x002D,
            CALIBRATION_CONSTANT_SCALE_2 = 0x002E,
            CALIBRATION_CONSTANT_SCALE_3 = 0x002F,
            CALIBRATION_CONSTANT_SCALE_4 = 0x0030
        }

        public enum TableKind
        {
            TIME,
            CURRENT
        }

        public enum ModelType
        {
            RA = 0x0000,
            STD = 0x00FF,
            CC = 0xFFFF
        }

        #endregion

        #region Structures

        public struct ScaleCalibrationResults
        {
            public uint Channel1;
            public uint Channel2;
            public uint Channel3;
            public uint Channel4;

            public double[] ToDoubleArray()
            {
                return new double[] { Channel1, Channel2, Channel3, Channel4 };
            }

            public static ScaleCalibrationResults operator /(ScaleCalibrationResults y, uint x)
            {
                var reusltc1 = y.Channel1 / x;
                var reusltc2 = y.Channel2 / x;
                var reusltc3 = y.Channel3 / x;
                var reusltc4 = y.Channel4 / x;

                var resultCalibration = new ScaleCalibrationResults();
                resultCalibration.Channel1 = reusltc1;
                resultCalibration.Channel2 = reusltc2;
                resultCalibration.Channel3 = reusltc3;
                resultCalibration.Channel4 = reusltc4;
                return resultCalibration;
            }

            public ScaleCalibrationResults Raiz()
            {
                var reusltc1 = (uint)Math.Sqrt(this.Channel1);
                var reusltc2 = (uint)Math.Sqrt(this.Channel2);
                var reusltc3 = (uint)Math.Sqrt(this.Channel3);
                var reusltc4 = (uint)Math.Sqrt(this.Channel4);

                var resultCalibration = new ScaleCalibrationResults();
                resultCalibration.Channel1 = reusltc1;
                resultCalibration.Channel2 = reusltc2;
                resultCalibration.Channel3 = reusltc3;
                resultCalibration.Channel4 = reusltc4;
                return resultCalibration;
            }

        }

        public struct TableElement
        {
            private bool x100;
            private ushort elementValue;
            private byte position;

            public TableElement(int value, byte positionValue)
            {
                if (value > 1024)
                {
                    x100 = true;
                    elementValue = (ushort)(value / 100);
                }
                else
                {
                    x100 = false;
                    elementValue = (ushort)value;
                }
                position = positionValue;
            }

            public bool X100
            {
                get
                {
                    return x100;
                }
            }

            public ushort GetElementValue
            {
                get
                {
                    return (ushort)(elementValue * (x100 ? 100 : 1));
                }
            }

            public ushort ElementValue
            {
                get
                {
                    return (ushort)elementValue;
                }
            }

            public byte Position
            {
                get
                {
                    return position;
                }
            }
        }

        public struct ScaleTableConfiguration
        {
            private string ScaleTableName;

            private List<TableElement> listElements;

            public bool HasElements
            {
                get
                {
                    return listElements.Any();
                }
            }

            public ScaleTableConfiguration(List<string> scaleElements, string scaleTableName)
            {
                ScaleTableName = scaleTableName;

                if (scaleElements.Count > 32)
                    throw new Exception("Error en la configuración de la tabla de reconexión, el numero de elementos está limitado a 32");

                listElements = new List<TableElement>();

                foreach (string scaleElement in scaleElements)
                {
                    int temporalValue = 0;

                    if (!int.TryParse(scaleElement, out temporalValue))
                        throw new Exception(string.Format("Error al configurar la tabla de escalas de {0}, el valor introducido en la base de datos no es un formato válido", scaleTableName));

                    listElements.Add(new TableElement(temporalValue, (byte)listElements.Count()));
                }
            }

            public List<TableElement> ListElemnts { get { return listElements; } }

            public string listElementsString
            {
                get
                {
                    StringBuilder list = new StringBuilder();

                    foreach (TableElement element in ListElemnts)
                        list.Append(element.GetElementValue.ToString() + ";");
                    return list.ToString();
                }

            }
        }

        public class ReconnectionTableConfiguration
        {
            private List<ReconnectionStage?> reconectionSequenceTable;

            public bool HasReconnections
            {
                get
                {
                    bool hasReconnections = false;
                    reconectionSequenceTable.ForEach(x => hasReconnections |= x != null);
                    return hasReconnections;
                }
            }

            public List<ReconnectionStage?> ReconectionSequenceTable
            {
                get
                {
                    return reconectionSequenceTable;
                }
            }

            public ReconnectionTableConfiguration(int numSecuenceReconections)
            {
                reconectionSequenceTable = new List<ReconnectionStage?>(numSecuenceReconections);
            }

            public struct ReconnectionStage
            {
                private List<ushort> reconnectionConfiguration;
                public ushort AmountOfReconnections;
                public ushort ReconnectionTime;
                public List<string> SequenceTime;
                public List<ushort> ReconectionSecuence { get { return reconnectionConfiguration; } }

                public ReconnectionStage(string info)
                {
                    reconnectionConfiguration = new List<ushort>();
                    var reconnectionParameters = info.Split(',');

                    if (reconnectionParameters.Length != 10)
                        throw new Exception("Error en la configuración de la etapa de reconexión, numero de parametros incorrectos");

                    AmountOfReconnections = Convert.ToUInt16(reconnectionParameters[0]);
                    ReconnectionTime = Convert.ToUInt16(reconnectionParameters[1]);
                    SequenceTime = reconnectionParameters.Skip(2).ToList();

                    foreach (string reconnectionParameter in reconnectionParameters)
                    {
                        ushort checkedValue = Convert.ToUInt16(reconnectionParameter);
                        reconnectionConfiguration.Add(checkedValue);
                    }
                }
            }

            public string GetStageListString()
            {
                if (HasReconnections)
                {
                    var stageList = new List<string>();
                    reconectionSequenceTable.ForEach(x => stageList.Add(x.ToString()));
                    return string.Join(", ", stageList);
                }
                else
                    return string.Empty;
            }
        }

	    #endregion

        #region Private functions

        public void WriteCalibrationConstantAllChannels(ScaleCalibrationResults calibrationInformation, ScaleCalibrations scale)
        {
            WriteChannelSelectionScaleConstant(Channel.CHANNEL_1);

            WriteScaleConstant(scale, (ushort)(calibrationInformation.Channel1));

            WriteChannelSelectionScaleConstant(Channel.CHANNEL_2);
            
            WriteScaleConstant(scale, (ushort)(calibrationInformation.Channel2));

            WriteChannelSelectionScaleConstant(Channel.CHANNEL_3);
            
            WriteScaleConstant(scale, (ushort)(calibrationInformation.Channel3));

            WriteChannelSelectionScaleConstant(Channel.CHANNEL_4);

            WriteScaleConstant(scale, (ushort)(calibrationInformation.Channel4));
        }


        #endregion

    }
}
