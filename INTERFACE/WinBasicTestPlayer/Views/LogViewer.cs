﻿using Dezac.Tests.Services.Logger;
using log4net.Core;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Enumerators;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using WinTestPlayer.Services;
using WinTestPlayer.Utils;

namespace WinTestPlayer.Views
{
    public partial class LogViewer : UserControl
    {
        protected AppService app;

        private static FastBindingList<LoggingEventWrapper> events;

        private FastBindingList<LoggingEventWrapper> filtered;

        private bool disableCheckLoggerFilters;

        public LogViewer()
        {
            InitializeComponent();

            grid.MasterTemplate.AllowAddNewRow = false;
            grid.MasterTemplate.AllowCellContextMenu = false;
            grid.MasterTemplate.AllowDeleteRow = false;
            grid.MasterTemplate.AllowEditRow = false;
            grid.MasterTemplate.EnableFiltering = false;
            grid.MasterTemplate.EnableGrouping = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            app = DI.Resolve<AppService>();

            cmbFilterLevel.Items.AddRange(new string[] { "DEBUG", "INFO", "WARN", "ERROR" });

            events = new FastBindingList<LoggingEventWrapper>();
            bsList.DataSource = events;

            UpdateFilter();

            app.Runner.AllTestStarting += (s, ee) =>
            {
                Clear();
                grid.Columns["NumInstance"].IsVisible = ee.NumInstances > 0;
            };

            app.Runner.TestEnd += (s, ee) =>
            {
                disableCheckLoggerFilters |= ee.ExecutionResult == TestExecutionResult.Completed;
            };
            
            timer1.Enabled = true;
        }

        private void Clear()
        {
            LogService.Clear();
            events.Clear();
            UpdateFilter();
        }

        private void AddFilterLoggers(LoggingEventWrapper e)
        {
            var filter = new NameValuePair { Value = e.LoggerName };

            int p = filter.Value.LastIndexOf(".");
            if (p > 0)
                filter.Name = filter.Value.Substring(p + 1);
            else
                filter.Name = filter.Value;
        }

        private void tsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        private void UpdateFilter()
        {
            int posl = cmbFilterLevel.SelectedIndex;

            if (posl > 0)
            {
                filtered = new FastBindingList<LoggingEventWrapper>();
                events.Where(ApplyFilter).ToList().ForEach(p => filtered.Add(p));
            }
            else
                filtered = events;

            bsList.DataSource = filtered;
        }

        private bool ApplyFilter(LoggingEventWrapper p)
        {
            int posl = cmbFilterLevel.SelectedIndex;

            return (posl <= 0 || p.Level.Name == cmbFilterLevel.Text);
        }

        private void MasterTemplate_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            LoggingEventWrapper ev = e.RowElement.RowInfo.DataBoundItem as LoggingEventWrapper;
            Color? color = null;

            if (ev.Level == Level.Error)
                color = Color.Red;
            else if (ev.Level == Level.Warn)
                color = Color.Orange;

            if (color.HasValue)
            {
                e.RowElement.DrawFill = true;
                e.RowElement.GradientStyle = GradientStyles.Solid;
                e.RowElement.BackColor = color.Value;
            }
            else
            {
                e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
                e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local);
                e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);
            }
        }

        private void grid_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            LoggingEventWrapper ev = e.Row.DataBoundItem as LoggingEventWrapper;
            if (ev == null)
                return;

            using (LogItemDetails form = new LogItemDetails())
            {
                form.LogText = ev.RenderedMessage;
                form.ShowDialog();
            }
        }

        private void cmbFilter_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            UpdateFilter();
        }

        private void commandBarButton2_Click(object sender, EventArgs e)
        {
            var context = app.TestContexts.FirstOrDefault();

            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.RawByTimeLog, false, context, false);
        }

        private void commandBarButton3_Click(object sender, EventArgs e)
        {
            var context = app.TestContexts.FirstOrDefault();

            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, false, context, false);
        }

        private void commandBarButton4_Click(object sender, EventArgs e)
        {
            var context = app.TestContexts.FirstOrDefault();

            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.CheckReport, false, context, false);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            bsList.SuspendBinding();
            events.Clear();
            int staticCount = LogService.MyEvents.Count;
            for (int i = staticCount - 1; i > staticCount - 11 && i >= 0; i--)
                events.Add(LogService.MyEvents[i]);
            bsList.ResumeBinding();
            timer1.Start();
        }
    }
}
