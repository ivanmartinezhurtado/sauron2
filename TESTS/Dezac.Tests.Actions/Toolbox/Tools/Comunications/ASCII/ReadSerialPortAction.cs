﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(ReadSerialPortActionDescription))]
    public class ReadSerialPortAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        [Description("Valor de texto que si se encuentra sale del action")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToRead { get; set; }

        [DefaultValue(false)]
        [Category("3. Configuration")]
        [Description("Flag para decir si queremos cancelar la tarea por otra tarea externa")]
        public bool CancelByOtherTask { get; set; }

        [Description("Variable que devolverá el estado de tarea externa para cancelar este action")]
        [Category("3. Configuration")]
        public ActionVariable NameVariableCancelation { get; set; } = new ActionVariable();

        [Category("3.1 Time")]
        [DefaultValue(2000)]
        [Description("Tiempo de espera a recibir la trama antes de dar error")]
        public int TimeOut { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(500)]
        [Description("Tiempo de espera antes de realizar otra comunicación")]
        public int TimeInterval { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(0)]
        [Description("Tiempo de espera antes de realizar la primera comunicación")]
        public int InitialDelay { get; set; }

        [Category("3.2 Retries")]
        [DefaultValue(1)]
        [Description("Número de repeticiones de lectura")]
        public int RetryReads { get; set; }

        [Category("3.2 Retries")]
        [DefaultValue(3)]
        [Description("Número de repeticiones de excepciones")]
        public int RetryException { get; set; }

        [Category("3.3 Exceptions")]
        [DefaultValue(false)]
        [Description("Si se habilita la cancelación o si se encuentra una excepción")]
        public bool CancelOnException { get; set; }

        [Category("3.3 Exceptions")]
        [DefaultValue(false)]
        [Description("Si se habilita la propagación de la excepción")]
        public bool ThrowException { get; set; }

        [Category("3. Configuration")]
        [Description("Valor para detectar la separación entre el final de lineas")]
        [DefaultValue("\r\n")]
        public string StringSeparators { get; set; }
        
        [Category("3. Configuration")]
        [Description("Tipo de string a retornar")]
        [DefaultValue(typeReturnString.ASCII)]
        public typeReturnString TypeReturn { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de formar de leer los datos por linea mirando su final de trama o por bytes recibidos")]
        [DefaultValue(typeRead.BY_READ_LINE)]
        public typeRead TypeRead { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var sender = GetVariable(DeviceName) as SerialPort;

            if (sender == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el Serial Port").Throw();

            var exception = "";

            string[] stringSeparators = new string[] { StringSeparators };

            string[] linesRx = TextToRead.Split(stringSeparators, StringSplitOptions.None);

            sender.ReadTimeout = TimeOut;

            int retryExceptionTmp = 0;

            var resultValue = new StringBuilder();

            for (int i = 0; i < linesRx.Length; i++)
            {
                var lineRx = VariablesTools.ResolveText(linesRx[i]);

                var sampler = Sampler.Run(
                () =>
                {
                    return new SamplerConfig { Interval = TimeInterval, InitialDelay = InitialDelay, NumIterations = RetryReads, CancelOnException = CancelOnException, ThrowException = ThrowException, CancellationToken = SequenceContext.Current.CancellationToken };
                },
                (step) =>
                {
                    try
                    {
                        var tramaRx = "";

                        if (sender.NewLine == "TimeOut" || TypeRead == typeRead.BY_BYTES_TO_READ)
                            tramaRx = Read(sender, TypeReturn);
                        else
                            tramaRx = sender.ReadLine();

                        resultValue.AppendLine(tramaRx);
                        
                        retryExceptionTmp = 0;

                        exception = "";

                        Logger.DebugFormat("Read: {0} / {1}  PORT: {2} RX: {3}", step.Step, RetryReads, DeviceName, tramaRx);

                        if (CancelByOtherTask)
                            step.Cancel = Convert.ToBoolean(NameVariableCancelation.Value);
                        else
                            step.Cancel = tramaRx.Contains(lineRx);
                    }
                    catch (TimeoutException e)
                    {
                        retryExceptionTmp += 1;

                        Logger.DebugFormat("TimeOut Exception: {0} / {1}  PORT: {2}", retryExceptionTmp, RetryException, DeviceName);

                        if (CancelByOtherTask)
                            step.Cancel = (bool)GetVariable(NameVariableCancelation.Value.ToString());
                        else
                        {
                            if (retryExceptionTmp > RetryException)
                            {
                                step.Cancel = true;
                                exception = "Sin respuesta del equipo";
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.DebugFormat("Exception: {0} PORT: {2}", e.Message, DeviceName);
                        step.Cancel = true;
                        exception = e.Message;
                    }
                });

                if (!sampler.Canceled)
                    exception = "No se ha recibido la respuesta del esperada";

                if (!string.IsNullOrEmpty(exception))
                    Error().UUT.COMUNICACIONES.NO_COMUNICA(exception).Throw();

                if (DataOutput != null && !DataOutput.IsKeyNullOrEmpty) 
                    DataOutput.Value = resultValue.ToString();            
            }
        }


        public enum typeReturnString
        {
            ASCII,
            HEX_STRING,
        }

        public enum typeRead
        {
            BY_READ_LINE,
            BY_BYTES_TO_READ,
        }

        public string Read(SerialPort streamResource, typeReturnString type)
        {
            var data = new List<byte>();
            byte[] ch = new byte[1];

            do
            {
                var bytesNum = streamResource.BytesToRead;

                Logger.DebugFormat("Num Bytes To READ: {0} ", bytesNum);

                int p = streamResource.Read(ch, 0, 1);
                if (p > 0)
                {
                    Logger.DebugFormat("Byte READ: {0} ", ch[0]);
                    byte b = ch[0];
                    data.Add(b);
                }

            } while (streamResource.BytesToRead > 0);

            var resp = "";

            if (type == typeReturnString.ASCII)
            {
                resp = ASCIIEncoding.ASCII.GetString(data.ToArray());
                Logger.DebugFormat("trama READ ASCII: {0} ", resp);
            }
            else
            {
                resp = Comunications.Utility.Extensions.BytesToHexString(data.ToArray());
                Logger.DebugFormat("trama READ HexString: {0} ", resp);
            }

            return resp;
        }
    }

    public class ReadSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "ReadSerialPort"; } }
        public override string Description { get { return "Lee una cadena de texto del puerto serie hasta encontrar el símbolo 'nueva línea'"; } }
        public override string Category { get { return "Tools/Comunications/ASCII"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
