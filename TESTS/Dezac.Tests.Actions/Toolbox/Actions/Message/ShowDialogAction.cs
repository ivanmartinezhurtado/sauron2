﻿using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(ShowDialogActionDescription))]
    public class ShowDialogAction : ActionBase
    {
        [Description("Mensaje sobre la entrada de datos")]
        [Category("3. Configuration")]
        public ActionVariable Message { get; set; } = new ActionVariable();

        [Description("Título del formulario")]
        [Category("3. Configuration")]
        public ActionVariable Title { get; set; } = new ActionVariable();


        [Browsable(false)]
        public string Caption { get; set; }

        [Browsable(false)]
        public string Text { get; set; }


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("No se ha llamado al metodo de inicialización de un test base").Throw() ; 

            if ((Title == null || Title.IsKeyNullOrEmpty) && !string.IsNullOrEmpty(Caption))
                Title = Caption;

            if ((Message == null || Message.IsKeyNullOrEmpty) && !string.IsNullOrEmpty(Text))
                Message = Text;

            testBase.ShowDialogMessage(Title.Value.ToString(), Message.Value.ToString(), 150, 250);
        }
    }
    
    public class ShowDialogActionDescription : ActionDescription
    {
        public override string Name { get { return "ShowDialog"; } }
        public override string Description { get { return "Permite crear un fragmento de codigo para hacer preguntas al operario"; } }
        public override string Category { get { return "Actions/Message"; } }
        public override Image Icon { get { return Properties.Resources.Text_Bubble_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
