﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Dezac.Core.Utility
{
    public static class Extensions
    {
        public static byte[] ToBytes(this object value)
        {
            if (value == null) return null;

            Type valueType = value.GetType();

            int rawsize = Marshal.SizeOf(value);

            if (rawsize % 2 != 0) rawsize -= 1;

            byte[] rawdata = new byte[rawsize];

            GCHandle handle = GCHandle.Alloc(rawdata, GCHandleType.Pinned);

            try
            {
                Marshal.StructureToPtr(value, handle.AddrOfPinnedObject(), false);
            }
            finally
            {
                handle.Free();
            }

            AdjustEndianness(valueType, rawdata);

            return rawdata;
        }

        public static ushort[] ToUshorts(this object value) 
        {
            if (value == null) return null;

            Type valueType = value.GetType();

            if (!valueType.IsValueType) return null;

            int rawsize = Marshal.SizeOf(value);

            if (rawsize % 2 != 0) rawsize -= 1;

            ushort[] rawdata = new ushort[rawsize / 2];

            GCHandle handle = GCHandle.Alloc(rawdata, GCHandleType.Pinned);

            try
            {
                Marshal.StructureToPtr(value, handle.AddrOfPinnedObject(), false);
            }
            finally
            {
                handle.Free();
            }

            AdjustEndianness(valueType, rawdata);

            return rawdata;
        }

        public static byte[] HexStringToBytes(this string value)
        {
            var lenght = value.Length == 2 ? 1 : (value.Length) / 2;

            byte[] data = new byte[lenght];
            int n = 0;

            for (int i = 0; i < value.Length - 1; i += 2) //Step 2
            {
                data[n] = byte.Parse(value.Substring(i, 2), NumberStyles.HexNumber);
                n += 1;
            }
            return data;
        }

        public static string BytesToHexString(this byte[] value)
        {
            string text = string.Empty;

            foreach (byte b in value)
                text += b.ToString("X2");
            return text;
        }

        private static void AdjustEndianness(Type type, byte[] data, int baseOffset = 0)
        {
            foreach (FieldInfo field in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.FieldType.Name == "String" || field.FieldType.Name == "Byte[]")
                    continue;

                int offset = Marshal.OffsetOf(type, field.Name).ToInt32();

                if (field.FieldType.IsValueType && !field.FieldType.IsPrimitive)
                    AdjustEndianness(field.FieldType, data, baseOffset + offset);
                else
                {
                    int size = Marshal.SizeOf(field.FieldType);
                    if (size > 1)
                        Array.Reverse(data, baseOffset + offset, size);
                }
            }
        }

        private static void AdjustEndianness(Type type, ushort[] data, int baseOffset = 0)
        {
            foreach (FieldInfo field in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.FieldType.Name == "String" || field.FieldType.Name == "Byte[]")
                    continue;

                int offset = Marshal.OffsetOf(type, field.Name).ToInt32();

                if (field.FieldType.IsValueType && !field.FieldType.IsPrimitive)
                    AdjustEndianness(field.FieldType, data, baseOffset + offset);
                else
                {
                    int size = Marshal.SizeOf(field.FieldType);
                    if (size > 1)
                        Array.Reverse(data, baseOffset + offset, size/2);
                }
            }
        }

        //*********************************************************

        public static byte ArrayBoolToByte(this bool[] arr)
        {
            byte val = 0;
            foreach (bool b in arr)
            {
                val <<= 1;
                if (b) val |= 1;
            }
            return val;
        }

        public static ushort ArrayBoolToRegister(this bool[] arr)
        {
            ushort val = 0;
            for( int i = arr.Count() -1; i>=0; i--)
            {
                val <<= 1;
                if (arr[i]) val |= 1;
            }
            return val;
        }

        public static bool[] ToBooleanArray(this ushort number)
        {
            var result = new bool[16] { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
            var numberBool = Convert.ToString(number, 2).Select(s => s.Equals('1')).ToArray();

            for (int n = 0; n <= numberBool.Length - 1; n++)
                if (numberBool[n] == true)
                    result[n + 16 - numberBool.Length] = true;

            return result;
        }

        public static bool[] ToFunction0F(this bool[] input)
        {
            var result = new bool[16];
            for (int nn = 0; nn <= 1; nn++)
            for (int n = 0; n <= 7; n++)
                result[7 - n + nn * 8] = input[n + nn * 8];

            return result;
        }

        public static void AddObject(Hashtable table, Object value, bool clear = true)
        {
            if (clear) table.Clear();

            if (value == null) return;

            FieldInfo[] fields = value.GetType().GetFields();

            foreach (FieldInfo f in fields) table.Add(f.Name, f.GetValue(value));

        }

        public static string GetReadableString(Object value)
        {

            if (value == null) return string.Empty;

            FieldInfo[] fields = value.GetType().GetFields();

            string text = string.Empty;

            foreach (FieldInfo f in fields)
                text += string.Format("{0}={1}{2}", f.Name, f.GetValue(value), Environment.NewLine);

            return text;
        }


        //***********************************************************

        public static double ToRadians(this double value)
        {
            return Math.Abs(value * Math.PI / 180);
        }

        public static double ToDegree(this double value)
        {
            return Math.Abs(value * 180 / Math.PI);
        }

        public static double Range(this StatisticalList list, int serie)
        {
            return list.Max(serie) - list.Min(serie);
        }

      
        public static double MaxRange(this StatisticalList list)
        {
            double value = 0;

            for (int i = 0; i < list.NumSeries; i++)
                value = Math.Max(value, list.Range(i));

            return value;
        }

        public static string StringSwap(this string stringToSwap)
        {
            if ((stringToSwap.Length % 2) != 0)
                throw new Exception("Error en la extensión Swap por pasar un string de longitud impar");

            char[] array = stringToSwap.ToCharArray();

            for (int i = 0; i < array.Length; i += 2)
            {
                char temp = array[i]; // Get temporary copy of character
                array[i] = array[i + 1]; // Assign element
                array[i + 1] = temp; // Assign element
            }
            return new string(array); // Return string
        }

        public static T ChangeEnum<T, S>(S enum1) where T : struct, IComparable, IConvertible, IFormattable where S : struct, IComparable, IConvertible, IFormattable
        {
            return (T)Enum.Parse(typeof(T), enum1.ToString());

            //return (T)Convert.ChangeType(enum1.ToString(), typeof(T));
        }

        public static decimal? ToDecimal(this double? value)
        {
            if (value == null)
                return null;

            return Convert.ToDecimal(value);
        }

        public static string CToNetString(this string value)
        {
            if (value == null)
                return null;

            return value.Replace("\0", "").Replace("\r", "").Replace("\n", "");
        }

        public static string CRoNetString(this string value)
        {
            if (value == null)
                return null;

            return value.Replace("\n", "");
        }

        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr =
                           Attribute.GetCustomAttribute(field,
                             typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                        return attr.Description;
                }
            }
            return value.ToString();
        }

        public static T JoinRegisters<T>(this ushort[] value)
        {
            byte[] value_aux = new byte[value.Length * 2]; 
            Buffer.BlockCopy(value, 0, value_aux, 0, value.Length * 2);
            byte[] value_aux2 = new byte[value.Length * 2];

            for (int i = 0; i< value_aux.Length; i += 2)
            {
                value_aux2[i] = value_aux[i + 1];
                value_aux2[i + 1] = value_aux[i]; 
            }

            return JoinRegisters<T>(value_aux2.ToArray());
        }

        public static T JoinRegisters<T>(this byte[] value)
        {
            Type valueType = typeof(T);

            if (valueType == typeof(ushort))
                return (T)Convert.ChangeType(IPAddress.NetworkToHostOrder(BitConverter.ToInt16(value, 0)), typeof(T));
            if (valueType == typeof(uint))
                return (T)Convert.ChangeType(IPAddress.NetworkToHostOrder(BitConverter.ToInt32(value, 0)), typeof(T));
            if (valueType == typeof(ulong))
                return (T)Convert.ChangeType(IPAddress.NetworkToHostOrder(BitConverter.ToInt64(value, 0)), typeof(T));
            return default(T);
        }
    }
}
