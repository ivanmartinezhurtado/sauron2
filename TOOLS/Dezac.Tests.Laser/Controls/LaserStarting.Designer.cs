﻿namespace Dezac.Tests.Laser.Controls
{
    partial class LaserStarting
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.lblItem = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStatuas = new System.Windows.Forms.Label();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.splitter = new System.Windows.Forms.SplitContainer();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pbLaser = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).BeginInit();
            this.splitter.Panel1.SuspendLayout();
            this.splitter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLaser)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.Location = new System.Drawing.Point(6, 41);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(488, 21);
            this.lblDescripcion.TabIndex = 15;
            this.lblDescripcion.Text = "lblDescripcion";
            this.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblItem
            // 
            this.lblItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItem.Location = new System.Drawing.Point(474, 100);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(488, 21);
            this.lblItem.TabIndex = 14;
            this.lblItem.Text = "lblItem";
            this.lblItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(474, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(488, 21);
            this.label3.TabIndex = 20;
            this.label3.Text = "FICHERO LASER:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatuas
            // 
            this.lblStatuas.Font = new System.Drawing.Font("Times New Roman", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatuas.Location = new System.Drawing.Point(-16, 562);
            this.lblStatuas.Name = "lblStatuas";
            this.lblStatuas.Size = new System.Drawing.Size(535, 28);
            this.lblStatuas.TabIndex = 22;
            this.lblStatuas.Text = "lblStatuas";
            this.lblStatuas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progress
            // 
            this.progress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progress.Location = new System.Drawing.Point(0, 593);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(502, 21);
            this.progress.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(488, 21);
            this.label4.TabIndex = 21;
            this.label4.Text = "DESCRIPCIÓN LASER:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter
            // 
            this.splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitter.Location = new System.Drawing.Point(0, 0);
            this.splitter.Name = "splitter";
            // 
            // splitter.Panel1
            // 
            this.splitter.Panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.splitter.Panel1.Controls.Add(this.btnCancel);
            this.splitter.Panel1.Controls.Add(this.lblDescripcion);
            this.splitter.Panel1.Controls.Add(this.lblItem);
            this.splitter.Panel1.Controls.Add(this.label3);
            this.splitter.Panel1.Controls.Add(this.lblStatuas);
            this.splitter.Panel1.Controls.Add(this.progress);
            this.splitter.Panel1.Controls.Add(this.label4);
            this.splitter.Panel1.Controls.Add(this.pbLaser);
            this.splitter.Panel2Collapsed = true;
            this.splitter.Size = new System.Drawing.Size(502, 614);
            this.splitter.SplitterDistance = 500;
            this.splitter.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Image = global::Dezac.Tests.Laser.Properties.Resources.Stop_Normal_Red_icon;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(187, 434);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(142, 71);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "CANCELAR";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pbLaser
            // 
            this.pbLaser.Image = global::Dezac.Tests.Laser.Properties.Resources.safety_sign_laser;
            this.pbLaser.Location = new System.Drawing.Point(140, 155);
            this.pbLaser.Name = "pbLaser";
            this.pbLaser.Size = new System.Drawing.Size(223, 238);
            this.pbLaser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLaser.TabIndex = 25;
            this.pbLaser.TabStop = false;
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // LaserStarting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 614);
            this.ControlBox = false;
            this.Controls.Add(this.splitter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "LaserStarting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LASER TRABAJANDO ....";
            this.splitter.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).EndInit();
            this.splitter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLaser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.Label lblItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStatuas;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SplitContainer splitter;
        private System.Windows.Forms.PictureBox pbLaser;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnCancel;
    }
}
