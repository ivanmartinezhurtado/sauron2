﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;

namespace Instruments.PowerSource
{
    public abstract class PowerSourceACIII : PowerSourceACBase<TriLineValue>, IPowerSourceIII
    {
        public PowerSourceACIII(ILog logger) : base(TriLineValue.Zero, logger)
        {
        }

        public virtual TriLineValue ReadVoltage()
        {
            var cmd = new Command<Commands>(Commands.READ_VOLTAGE, true);

            if (SendCommand(cmd))
                return DeviceData.Voltage;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return TriLineValue.Create(0);
        }

        public TriLineValue ReadCurrent()
        {
            var cmd = new Command<Commands>(Commands.READ_CURRENT, true);

            if (SendCommand(cmd))
                return DeviceData.Current;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return TriLineValue.Create(0);
        }

        public double ReadFreq()
        {
            var cmd = new Command<Commands>(Commands.READ_FREQ, true);

            if (SendCommand(cmd))
                return DeviceData.Freq;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        public TriLinePower ReadPower()
        {
            var voltage = ReadVoltage();
            var current = ReadCurrent();
            var pf = ReadPowerFactor();

            var power = TriLinePower.Create(voltage, current, pf);

            return power;        
        }

        public TriLineValue ReadPowerFactor()
        {
            var cmd = new Command<Commands>(Commands.READ_POWERFACTOR, true);

            if (SendCommand(cmd))
                return DeviceData.PF;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return TriLineValue.Create(0);
        }

        public TriLineValue ReadDesfase()
        {
            var cmd = new Command<Commands>(Commands.READ_DESFASE, true);

            if (SendCommand(cmd))
                return DeviceData.Desfase;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return TriLineValue.Create(0);
        }

        //**************************************************************
        public bool ApplyAndWaitStabilisation(bool forceApply = false)
        {
            return base.ApplyAndWaitStabilisationBase(forceApply);
        }

        public bool ApplyPresetsAndWaitStabilisation(TriLineValue voltage, TriLineValue current, double freq, TriLineValue pf , TriLineValue desfase, bool forceApply = false)
        {
                return base.ApplyAndWaitStabilisationBase(voltage, current, freq, pf, desfase, forceApply);
        }

        public bool ApplyPresetsAndWaitStabilisationWithValidator(TriLineValue voltage, TriLineValue current, double freq, TriLineValue pf, TriLineValue desfase, bool forceApply = false, Func<bool> validator = null)
        {
            return base.ApplyAndWaitStabilisationBase(voltage, current, freq, pf, desfase, forceApply, validator);
        }

        public void ApplyPresets(TriLineValue voltage, TriLineValue current, double freq, TriLineValue powerFase, TriLineValue desfases, bool forceApply = false)
        {
            ApplyPresetsBase(voltage, current, freq, powerFase, desfases, forceApply);
        }

        protected override void ResetPresets()
        {
            this.DevicePreset.Voltage = TriLineValue.Zero;
            this.DevicePreset.Current = TriLineValue.Zero; ;
            this.DevicePreset.Freq = 0;
            this.DevicePreset.PF = TriLineValue.Zero; ;
            this.DevicePreset.Desfase = TriLineValue.Zero; ;
        }
    }
}
