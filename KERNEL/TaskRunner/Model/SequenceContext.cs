﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using TaskRunner.Enumerators;

namespace TaskRunner.Model
{
    public class SequenceContext
    {
        private const string SEQUENCECONTEXT_KEY = "TaskRunner_SequenceContext";
        private const string SEQUENCECONTEXT_STEP_KEY = "TaskRunner_SequenceContext_Step";

        [JsonIgnore]
        public static SequenceContext Current
        {
            get
            {
                return CallContext.LogicalGetData(SEQUENCECONTEXT_KEY) as SequenceContext;
            }
            internal set
            {
                if (value == null)
                    CallContext.FreeNamedDataSlot(SEQUENCECONTEXT_KEY);
                else
                    CallContext.LogicalSetData(SEQUENCECONTEXT_KEY, value);
            }
        }
        [JsonIgnore]
        public Guid Guid { get; internal set; }
        [JsonIgnore]
        public ServiceCollection Services { get; internal set; }
        public SharedVariablesList Variables { get; internal set; }
        public SharedVariablesList SharedVariables { get; internal set; }
        public ResultsList ResultList { get; internal set; }
        public SequenceModel Sequence { get; set; }
        public RunMode RunMode { get; set; }
        public int NumInstance { get; set; }
        [JsonIgnore]
        public int TotalNumInstances { get; internal set; }
        [JsonIgnore]
        public int RunningInstances { get { return SharedVariables.Get<int>("_RUNNING_INSTANCES_", 1); } }
        [JsonIgnore]
        public CancellationToken CancellationToken { get; private set; }
        public CancellationTokenSource CancellationTokenSource { get; private set; }
        [JsonIgnore]
        public TaskScheduler InitialTaskScheduler { get; internal set; }
        [JsonIgnore]
        public Step Step
        {
            get
            {
                return CallContext.LogicalGetData(SEQUENCECONTEXT_STEP_KEY) as Step;
            }
            internal set
            {
                CallContext.LogicalSetData(SEQUENCECONTEXT_STEP_KEY, value);
            }
        }
        [JsonIgnore]
        public TestResult TestResult { get; set; }
        public TestExecutionResult ExecutionResult { get { return TestResult.ExecutionResult; } }
        public bool IsCancellationRequested { get { return CancellationToken != null && CancellationToken.IsCancellationRequested; } }
        [JsonIgnore]
        public Action SaveResultsMethod { get; set; }

        public SequenceContext(SequenceModel sequence, int numInstance)
        {
            Sequence = sequence;
            NumInstance = numInstance;
            Services = new ServiceCollection();
            Variables = new SharedVariablesList(numInstance);
            ResultList = new ResultsList();
            TestResult = new TestResult(this);
            CancellationTokenSource = new CancellationTokenSource();
            CancellationToken = CancellationTokenSource.Token;

            Guid = Guid.NewGuid();
        }

        public IEnumerable<StepResult> GetSteps(StepExecutionResult executionResult) => TestResult.GetSteps(executionResult);
        public IEnumerable<StepResult> GetSteps(Func<StepResult, bool> include, Func<StepResult, bool> includeChilds = null)
        {
            List<StepResult> list = new List<StepResult>();

            List<StepResult> steps = TestResult.StepResults
                .Where(p => p.ParentResult == null)
                .ToList();
            foreach (StepResult step in steps)
                list.AddRange(step.Flatten(include, includeChilds));

            return list;
        }
        public IEnumerable<StepResult> GetSteps(string stepName, Func<StepResult, bool> include, Func<StepResult, bool> includeChilds = null)
        {
            StepResult stepResult = TestResult.StepResults.Where(p => p.Name == stepName).FirstOrDefault();

            if (stepResult == null)
                return new List<StepResult>();

            return stepResult.Flatten(include, includeChilds);
        }

    }
}
