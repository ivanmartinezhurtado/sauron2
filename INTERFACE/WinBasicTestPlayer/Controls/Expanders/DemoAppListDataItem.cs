﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;

namespace WinTestPlayer.Controls.Expanders
{
    public class DemoAppListDataItem : RadListDataItem
    {
        public bool BetaControl { get; set; }
        public bool CtpControl { get; set; }
        public bool NewExample { get; set; }
    }
}
