﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Dezac.Device.Metering.SOAP
{
    public class MessageXmlSoap
    {
        private static int contador = 0;
        public string VersionXml { get; set; } // "3.1" ya no;
        public string IdValue { get; set; }// "445oqta";
        public string FileResquest { get; set; }// "G17";
        public string FileResquestChild { get; set; } //module

        public MessageXmlSoap(string versionxml)
        {
            VersionXml = versionxml;
            IdValue = "445oqta";
        }

        public string BuildMessageXml(string fileResquest, Dictionary<string, string> Atributos)
        {
            var doc = new XmlDocument();

            FileResquest = fileResquest;

            return BuildMessageXmlInternal(doc, () => { return BuildBodyXml(doc, Atributos); });
        }

        public string BuildMessageXml(string fileResquest, string fileResquestChild,  Dictionary<string, string> Atributos)
        {
            var doc = new XmlDocument();

            FileResquest = fileResquest;

            FileResquestChild = fileResquestChild;

            return BuildMessageXmlInternal(doc, () => { return BuildBodyMultiplNodesXml(doc, Atributos); });
        }

        public string BuildMessageXml(string fileResquest, string fileResquestChild,  List<Dictionary<string, string>> Atributos)
        {
            var doc = new XmlDocument();

            FileResquest = fileResquest;

            FileResquestChild = fileResquestChild;

            return BuildMessageXmlInternal(doc, () => { return BuildBodyMultiplNodesXml(doc, Atributos); });
        }

        public string BuildMessageXml(string fileResquest, string fileResquestChild, string fileResquestChild2, Dictionary<string, string> Atributos)
        {
            var doc = new XmlDocument();

            FileResquest = fileResquest;

            FileResquestChild = fileResquestChild;

            return BuildMessageXmlInternal(doc, () => { return BuildBodyMultiplNodesXml(doc, fileResquestChild2,Atributos); });
        }
     
        private string BuildMessageXmlInternal(XmlDocument doc, Func<XmlNode> buidBody)
        {
            var order = BuildHeaderXml(doc);

            var Cnc = doc.CreateElement("Cnc");

            var att = doc.CreateAttribute("Id");
            att.Value = IdValue;

            Cnc.Attributes.Append(att);

            var nodeIdReq = buidBody(); 

            Cnc.AppendChild(nodeIdReq);

            order.AppendChild(Cnc);

            doc.AppendChild(order);

            return doc.InnerXml;
        }

        private XmlElement BuildHeaderXml(XmlDocument doc)
        {
            var Order = doc.CreateElement("Order");

            var att = doc.CreateAttribute("IdReq");
            att.Value = FileResquest;
            Order.Attributes.Append(att);

            att = doc.CreateAttribute("IdPet");
            att.Value = Convert.ToString(contador++);
            Order.Attributes.Append(att);

            att = doc.CreateAttribute("Version");
            att.Value = VersionXml;
            Order.Attributes.Append(att);

            return Order;
        }

        private XmlNode BuildBodyXml(XmlDocument doc, Dictionary<string, string> Atributos)
        {
            var NodeIdReq = doc.CreateElement(FileResquest);

            foreach (KeyValuePair<string, string> it in Atributos)
            {
                var att = doc.CreateAttribute(it.Key);
                att.Value = it.Value;
                NodeIdReq.Attributes.Append(att);
            }
            return NodeIdReq;
        }

        private XmlNode BuildBodyMultiplNodesXml(XmlDocument doc, string child2, Dictionary<string, string> Atributos)
        {
            var NodeIdReq = doc.CreateElement(FileResquest);
            XmlNode node = doc.CreateElement(FileResquestChild);
            XmlNode node2 = doc.CreateElement(child2);
            foreach (KeyValuePair<string, string> it in Atributos)
            {
                var att = doc.CreateAttribute(it.Key);
                att.Value = it.Value;
                node2.Attributes.Append(att);
            }
            node.AppendChild(node2);
            NodeIdReq.AppendChild(node);

            return NodeIdReq;
        }

        private XmlNode BuildBodyMultiplNodesXml(XmlDocument doc, Dictionary<string, string> Atributos)
        {
            var NodeIdReq = doc.CreateElement(FileResquest);

            XmlNode node = doc.CreateElement(FileResquestChild);

            foreach (KeyValuePair<string, string> it in Atributos)
            {
                var att = doc.CreateAttribute(it.Key);
                att.Value = it.Value;
                node.Attributes.Append(att);
            }

            NodeIdReq.AppendChild(node);

            return NodeIdReq;
        }

        private XmlNode BuildBodyMultiplNodesXml(XmlDocument doc, List<Dictionary<string, string>> Atributos)
        {
            var NodeIdReq = doc.CreateElement(FileResquest);

            foreach (Dictionary<string, string> atributo in Atributos)
            {
                XmlNode node = doc.CreateElement(FileResquestChild);
                foreach (KeyValuePair<string, string> it in atributo)
                {               
                    var att = doc.CreateAttribute(it.Key);
                    att.Value = it.Value;
                    node.Attributes.Append(att);            
                }
                NodeIdReq.AppendChild(node);
            }
            return NodeIdReq;
        }


        //*********************************************************************

        public string BuildMessageXml(string fileResquest, Dictionary<string, string> fileResquestAtributo, string fileResquestChild,List<Dictionary<string, string>> fileResquestChildAtributos, string fileResquestChild2, List<Dictionary<string, string>> fileResquestChildAtributos2)
        {
            var doc = new XmlDocument();
            FileResquest = fileResquest;
            FileResquestChild = fileResquestChild;
          
            var xmlNodefileResquest = BuildNodesAttributesXml(doc, fileResquest, fileResquestAtributo);
            var xmlNodefileResquestChild = BuildNodesAttributesXml(doc, xmlNodefileResquest, fileResquestChild, fileResquestChildAtributos);
            var xmlNodefileResquestChild2 = BuildNodesAttributesXml(doc, xmlNodefileResquestChild, fileResquestChild2, fileResquestChildAtributos2);

            return BuildMessageXmlInternal(doc, () => { return xmlNodefileResquest; });
        }

        public string BuildMessageXml(string fileResquest, Dictionary<string, string> fileResquestAtributo, string fileResquestChild, Dictionary<string, string> fileResquestChildAtributos, string fileResquestChild2, List<Dictionary<string, string>> fileResquestChildAtributos2)
        {
            var doc = new XmlDocument();
            FileResquest = fileResquest;
            FileResquestChild = fileResquestChild;

            var xmlNodefileResquest = BuildNodesAttributesXml(doc, fileResquest, fileResquestAtributo);
            var xmlNodefileResquestChild = BuildNodesAttributesXml(doc, fileResquestChild, fileResquestChildAtributos);
            xmlNodefileResquest.AppendChild(xmlNodefileResquestChild);
            var xmlNodefileResquestChild2 = BuildNodesAttributesXml(doc, xmlNodefileResquestChild, fileResquestChild2, fileResquestChildAtributos2);

            return BuildMessageXmlInternal(doc, () => { return xmlNodefileResquest; });
        }


        private XmlNode BuildNodesAttributesXml(XmlDocument doc, string Node, Dictionary<string, string> Atributos)
        {
            XmlNode node2 = doc.CreateElement(Node);
            foreach (KeyValuePair<string, string> it in Atributos)
            {
                var att = doc.CreateAttribute(it.Key);
                att.Value = it.Value;
                node2.Attributes.Append(att);
            }
            return node2;
        }

        private XmlNode BuildNodesAttributesXml(XmlDocument doc, XmlNode NodeParent, string Node, List<Dictionary<string, string>> Atributos)
        {
            foreach (Dictionary<string, string> atributo in Atributos)
            {
                XmlNode node = doc.CreateElement(Node);
                foreach (KeyValuePair<string, string> it in atributo)
                {
                    var att = doc.CreateAttribute(it.Key);
                    att.Value = it.Value;
                    node.Attributes.Append(att);
                }
                NodeParent.AppendChild(node);
            }
            return NodeParent;
        }
    }
}
