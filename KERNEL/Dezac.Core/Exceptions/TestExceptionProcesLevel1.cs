﻿using System;

namespace Dezac.Core.Exceptions
{
    public class TestExceptionProcesLevel1
    {
        public string[] Category { get; set; }

        public string[] SubCategory { get; set; }

        public string ClassName { get; set; }

        public SauronException TEST_PLACAS()
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.TEST_PLACAS, null).Exception();
        }

        public SauronException TEST_CANCELADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.TEST_CANCELADO, parameter).Exception();
        }

        public SauronException PARAMETRO_INCOHERENTE(string parameter = null)     
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.PARAMETRO_INCOHERENTE, parameter).Exception();
        }

        public SauronException VALOR_INCORRECTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.VALOR_INCORRECTO, parameter).Exception();
        }

        public SauronException NO_VALIDADO_IDP(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.NO_VALIDADO_IDP, parameter).Exception();
        }

        public SauronException BLOQUEADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.BLOQUEADO, parameter).Exception();
        }

        public SauronException EXCEPTION_NOT_CONTROLLED(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.EXCEPTION_NOT_CONTROLLED, parameter).Exception();
        }

        public SauronException FALTA_VALOR_PARAMETRO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.FALTA_VALOR_PARAMETRO, parameter).Exception();
        }

        public SauronException FALTA_ANCLA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.FALTA_ANCLA, parameter).Exception();
        }

        public SauronException VERSION_FIRMWARE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.VERSION_FIRMWARE, parameter).Exception();

        }

        public SauronException COLOCACION_INCORRECTA_EQUIPO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.COLOCACION_INCORRECTA_EQUIPO, parameter).Exception();
        }

        public SauronException ETIQUETA_INCORRECTA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.ETIQUETA_INCORRECTA, parameter).Exception();
        }

        public SauronException FALTA_TAPA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.FALTA_TAPA, parameter).Exception();
        }

        public SauronException EMBALAJE_INDIVIDUAL_INCORRECTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.EMBALAJE_INDIVIDUAL_INCORRECTO, parameter).Exception();
        }

        public SauronException DISPOSITIVO_NO_ENCONTRADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.DISPOSITIVO_NO_ENCONTRADO, parameter).Exception();
        }

        public SauronException DISPOSITIVO_REPETIDO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.DISPOSITIVO_REPETIDO, parameter).Exception();
        }

        public SauronException PRODUCTO_INCORRECTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.PRODUCTO_INCORRECTO, parameter).Exception();
        }

        public SauronException FASES_TEST_INCORRECTAS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.FASES_TEST_INCORRECTAS, parameter).Exception();
        }

        public SauronException ESTRUCTURA_COMPONENTES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.ESTRUCTURA_COMPONENTES, parameter).Exception();
        }

        public SauronException PLANTILLAS_LASER(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.PLANTILLAS_LASER, parameter).Exception();
        }

        public SauronException INSTRUCCIONES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.INSTRUCCIONES, parameter).Exception();
        }

        public SauronException BINARIOS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.BINARIOS, parameter).Exception();
        }

        public SauronException FALTA_ARCHIVO_ETIQUETA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.FALTA_ARCHIVO_ETIQUETA, parameter).Exception();
        }
        public SauronException ERROR_SUBPROCESO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.ERROR_SUBPROCESO, parameter).Exception();
        }
        public SauronException NO_ORDEN_FABRICACION_NO_PRODUCTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.NO_ORDEN_FABRICACION_NO_PRODUCTO, parameter).Exception();
        }
        public SauronException FALTA_ARCHIVO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.FALTA_ARCHIVO, parameter).Exception();
        }
        public SauronException NO_COINCIDE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.NO_COINCIDE, parameter).Exception();
        }
        public SauronException BIEN(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.BIEN, parameter).Exception();
        }
        public SauronException LED(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.PROCESLevel1.LED, parameter).Exception();
        }
    }
}
