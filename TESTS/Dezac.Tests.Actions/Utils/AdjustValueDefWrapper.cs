﻿using Dezac.Tests.Model;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    public class AdjustValueDefWrapper
    {
        public string Name { get; set; }

        public ParamUnidad Unidad { get; set; }

        [Browsable(false)]
        public string Average { get; set; }

        [Browsable(false)]
        public string Tolerance { get; set; }

        [Browsable(false)]
        public string Min { get; set; }

        [Browsable(false)]
        public string Max { get; set; }

        [Browsable(false)]
        public string MaxVarianza { get; set; }
       
        public string VarNamesDevice { get; set; }

        public string VarNamesPattern { get; set; }

    }
}
