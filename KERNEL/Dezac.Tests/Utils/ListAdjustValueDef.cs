﻿using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Utils
{
    public interface IListTriOrAdjustValueDef
    {
        string[] CrateListName(string prefix = "");
        void FillVars(double[] list);
        void FillVars(double[] list, double[] listPatron);
        void FillVarsVariance(double[] variance);
        List<AdjustValueDef> HasIncorrect();
        List<AdjustValueDef> HasIncorrectVariance();
        List<AdjustValueDef> List { get; set; }
        string Name { get; set; }
    }

    public class ListAdjustValueDef: IListTriOrAdjustValueDef
    {
        public List<AdjustValueDef> List { get; set; }

        public string Name { get; set; }

        public ListAdjustValueDef(string name)
        {
            List = new List<AdjustValueDef>();
            Name = name;
        }

        public ListAdjustValueDef()
        {
            List = new List<AdjustValueDef>();
        }

        public void Add(AdjustValueDef defs)
        {
            List.Add(defs);
        }

        public void AddRange(IEnumerable<AdjustValueDef> defs)
        {
            List.AddRange(defs);
        }

        public void Add(TestModeParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null)
        {
            List.Add(new AdjustValueDef(paramater, unidad, Min, Max, MaxVarianza));
        }

        public void Add(TestModeParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null)
        {
            List.Add(new AdjustValueDef(paramater, unidad, isMaxMin, Avg, Tol, MaxVarianza));
        }

        public void Add(ExtensionParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null)
        {
            List.Add(new AdjustValueDef(paramater, unidad, Min, Max, MaxVarianza));
        }

        public void Add(ExtensionParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null)
        {
            List.Add(new AdjustValueDef(paramater, unidad, isMaxMin, Avg, Tol, MaxVarianza));
        }

        public string[] CrateListName(string prefix = "")
        {
            if (!string.IsNullOrEmpty(prefix))
                prefix = "_" + prefix;

            return List.Select((p) => string.Format("{0}{1}", p.Name, prefix)).ToArray();
        }

        public void FillVars(double[] list, double[] listPatron)
        {
            var index = 0;
            foreach (var adjust in List)
            {
                adjust.Value = list[index];
                adjust.Average = listPatron[index];
                index += 1;
            }
        }

        public void FillVars(double[] list)
        {
            var index = 0;
            foreach (var adjust in List)
            {
                adjust.Value = list[index];
                index += 1;
            }
        }

        public void FillVarsVariance(double[] variance)
        {
            var index = 0;
            foreach (var adjust in List)
            {
                adjust.Varianza= variance[index];
                index += 1;
            }
        }

        public List<AdjustValueDef> HasIncorrect()
        {
            var result = new List<AdjustValueDef>();

            foreach (var adjust in List)
                if (!adjust.IsCorrect())
                    result.Add(adjust);
            return result;
        }

        public List<AdjustValueDef> HasIncorrectVariance()
        {
            var result = new List<AdjustValueDef>();

            foreach (var adjust in List)
                if (!adjust.IsValidVariance())
                    result.Add(adjust);

            return result;
        }
    }

    public class ListTriAdjustValueDef : IListTriOrAdjustValueDef
    {
        public List<TriAdjustValueDef> ListTri { get; set; }

        public string Name { get; set; }

        public List<AdjustValueDef> List
        {
            get
            {
                var result = new List<AdjustValueDef>();

                foreach (var TriAdjust in ListTri)
                foreach (var adjust in TriAdjust.GetListAdjusValueDef())
                    result.Add(adjust);

                return result;
            }

            set
            {

            }
        }

        public ListTriAdjustValueDef(string name)
        {
            ListTri = new List<TriAdjustValueDef>();
            Name = name;
        }

        public void Add(TriAdjustValueDef defs)
        {
            ListTri.Add(defs);
        }

        public void Add(TestModeParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null)
        {
            ListTri.Add(new TriAdjustValueDef(paramater, unidad, Min, Max, MaxVarianza));
        }

        public void Add(TestModeParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null)
        {
            ListTri.Add(new TriAdjustValueDef(paramater, unidad, isMaxMin, Avg, Tol, MaxVarianza));
        }

        public void Add(ExtensionParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null)
        {
            ListTri.Add(new TriAdjustValueDef(paramater, unidad, Min, Max, MaxVarianza));
        }

        public void Add(ExtensionParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null)
        {
            ListTri.Add(new TriAdjustValueDef(paramater, unidad, isMaxMin, Avg, Tol, MaxVarianza));
        }

        public string[] CrateListName(string prefix = "")
        {
            var namesPatron = new List<string>();
            if (!string.IsNullOrEmpty(prefix))
                prefix = "_" + prefix;

            ListTri.ForEach((p) => { namesPatron.Add(p.NamesToList().Select((s) => string.Format("{0}{1}", s, prefix)).ToArray()); });
            return namesPatron.ToArray();
        }

        public void FillVars(double[] list, double[] listPatron)
        {
            var index = 0;

            foreach (var Triadjust in ListTri)
            foreach (var adjust in Triadjust.GetListAdjusValueDef())
            {
                adjust.Value = list[index];
                adjust.Average = listPatron[index];
                index += 1;
            }
        }

        public void FillVars(double[] list)
        {
            var index = 0;

            foreach (var Triadjust in ListTri)
            foreach (var adjust in Triadjust.GetListAdjusValueDef())
            {
                adjust.Value = list[index];
                index += 1;
            }
        }

        public void FillVarsVariance(double[] variance)
        {
            var index = 0;
            foreach (var TriAdjust in ListTri)
            foreach (var adjust in TriAdjust.GetListAdjusValueDef())
            {
                adjust.Varianza = variance[index];
                index += 1;
            }
        }

        public List<AdjustValueDef> HasIncorrect()
        {
            var result = new List<AdjustValueDef>();

            foreach (var TriAdjust in ListTri)
            foreach (var adjust in TriAdjust.GetListAdjusValueDef())
                if (!adjust.IsCorrect())
                    result.Add(adjust);

            return result;
        }

        public List<AdjustValueDef> HasIncorrectVariance()
        {
            var result = new List<AdjustValueDef>();

            foreach (var TriAdjust in ListTri)
            foreach (var adjust in TriAdjust.GetListAdjusValueDef())
                if (!adjust.IsValidVariance())
                    result.Add(adjust);

            return result;
        }

    }
}
