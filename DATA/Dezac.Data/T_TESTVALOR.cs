namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTVALOR")]
    public partial class T_TESTVALOR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTESTFASE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARAM { get; set; }

        [Required]
        [StringLength(500)]
        public string VALOR { get; set; }

        [StringLength(50)]
        public string STEPNAME { get; set; }

        public decimal? VALMAX { get; set; }

        public decimal? VALMIN { get; set; }

        [StringLength(500)]
        public string EXPECTEDVALUE { get; set; }

        public int? NUMUNIDAD { get; set; }

        public virtual T_PARAMETRO T_PARAMETRO { get; set; }

        public virtual T_TESTFASE T_TESTFASE { get; set; }

        public virtual T_UNIDAD T_UNIDAD { get; set; }
    }
}
