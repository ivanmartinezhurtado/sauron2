﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.27)]
    public class EKOR_RPA100TestFuncional : EKOR_SAFETestBaseNew
    {
        private const byte OUT_DB9 = 50;
        private const byte SUPPLY_232_EXT = 0;
        private const byte BYPASS = 5;
        private const byte ACTIVE_VEXT232_TO_IN2_MUX = 1;

        private const byte OUT_USB_IO = 9;

        protected USB5856 usbIO;

        public void TestInitialization()
        {
            var tower3 = tower as Tower3;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower3))
            {
                tower3 = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower3);
            }
            if (tower3 == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();
           
            tower = tower3;
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            usbIO = new USB5856();

            var utilNew = true;

            util = utilNew ? new UtillajeNuevoEkorPlus(this, tower) : new UtillajeViejoEkorPlus(this, tower) as IUtillajeEkor;

            util.TestDeviceConnection();

            Ekor = new EKOR_RPA100(Comunicaciones.SerialPort);

            tower.ActiveVoltageCircuit();
        }

        public void TestConsumo([Description("Vmax=0, Vmin=1, Vnom=2")]TypeTestVoltage typeVoltage)
        {
            var outputs = util.GetKeyValuePairOutputs();
            foreach(var Out in outputs)
                Ekor.ActivaDigitalOutput(Out.Value);

            var inputs = util.GetKeyValuePairInputs();
            foreach (var input in inputs)
                tower.IO.DO.On(input.Key);

            var voltageConsign = typeVoltage == TypeTestVoltage.VoltageMaximo ? Consignas.GetDouble(Params.V.Null.EnVacio.Vmax.Name, 100, ParamUnidad.V)
                               : typeVoltage == TypeTestVoltage.VoltageMinimo ? Consignas.GetDouble(Params.V.Null.EnVacio.Vmin.Name, 100, ParamUnidad.V)
                               : typeVoltage == TypeTestVoltage.VoltageNominal ? Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 100, ParamUnidad.V) : 0;

            var configuration = new TestPointConfiguration()
            {
                Voltage = voltageConsign,
                Current = 0,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = TypePowerSource.LAMBDA,
                typeTestExecute = TypeTestExecute.OnlyActive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = typeVoltage,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(tower, configuration);

            TestVGEN("_"+typeVoltage.ToString());
            TestVAUX("_"+typeVoltage.ToString());
            TestVUSB("_"+typeVoltage.ToString());

            foreach (var Out in outputs)
                Ekor.DesactivaDigitalOutput(Out.Value);

           
            foreach (var input in inputs)
                tower.IO.DO.Off(input.Key);
        }

        public void TestConsumptionBattery()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.TestPoint("PILA").Vnom.Name, 3.6, ParamUnidad.V);
            var limitCurrent = Consignas.GetDouble(Params.I.Null.TestPoint("PILA").Vnom.Name, 0.5, ParamUnidad.A);

            tower.IO.DO.On(20, 21);

            util.DesactivatePistonRS232();

            Delay(2000, "Esperando a que el equipo entre en modo Standbye");

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            var param = new AdjustValueDef(Params.I_DC.Null.EnVacio.TestPoint("PILA"), ParamUnidad.A);
            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC, 500);
            }, 0, 15, 2500);

            tower.IO.DO.Off(20, 21);

            util.TestDeviceDisconnection();
            Shell.MsgBox("SAQUE EL EQUIPO DEL ÚTIL, ENSAMBLE LA PILA Y VUELVA A INTRODUCIRLO", "PILA");
            util.TestDeviceConnection();

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.SinUnidad), 0.5);
        }

        public void TestCommunicationsRS232(int portEkor232)
        {
            if (portEkor232 == 0)
                portEkor232 = Comunicaciones.SerialPort2;

            usbIO.DO.On(BYPASS);

            Ekor.SetPort(portEkor232);
            string firmwareVersion = "";
            string developerVersion = "";

            SamplerWithCancel((p) =>
            {
                firmwareVersion = Ekor.ReadVersion().version;
                developerVersion = Ekor.ReadVersionDesarrollo().version;
                logger.InfoFormat("Puerto de comunicacion RS232 asignado en  COM {0}", portEkor232.ToString());
                return true;
            }, "Error. No comunica el equipo en RS232", 3, 500, 1000, false, false);

            Resultado.Set("TEST_RS232", "OK", ParamUnidad.s);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
            TestException.Create().UUT.FIRMWARE.NO_COINCIDE("VERSION_FIRMWARE"), ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_DEVELOPER, developerVersion.Trim(), Identificacion.VERSION_DEVELOPER.Trim(),
            TestException.Create().UUT.FIRMWARE.NO_COINCIDE("VERSION_DEVELOPER"), ParamUnidad.SinUnidad);

            var versionMicro = Ekor.ReadMicrocontrolerType();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, (string)versionMicro.cpu, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.HARDWARE.NO_COINCIDE("tipo de microcontrolador"), ParamUnidad.SinUnidad);

            Resultado.Set("CPU REVISION", (string)versionMicro.revision, ParamUnidad.SinUnidad);

            var versionBoot = Ekor.ReadVersionBoot();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_BOOT, (string)versionBoot, Identificacion.VERSION_BOOT.Trim(), Error().UUT.FIRMWARE.NO_COINCIDE("version de boot"), ParamUnidad.SinUnidad);

            var crcBoot = Ekor.ReadCRCBoot();

            Assert.AreEqual(ConstantsParameters.Identification.CRC_BOOT, crcBoot, Identificacion.CRC_BOOT.Trim(), Error().UUT.FIRMWARE.NO_COINCIDE("crc de boot"), ParamUnidad.SinUnidad);

            DriveInfo usbDrive = null;

            SamplerWithCancel((p) =>
            {
                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                {
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKOR_DISK", ParamUnidad.SinUnidad).Trim())
                    {
                        usbDrive = drive;
                        break;
                    }
                }

                if (usbDrive == null)
                    throw new Exception("Error no se detecta la unidad USB en el S.O.");

                logger.InfoFormat("USB_DRIVE RottDirectory: {0}", usbDrive.RootDirectory.Name);
                Resultado.Set("USB_DRIVE", usbDrive.VolumeLabel, ParamUnidad.SinUnidad);

                return !string.IsNullOrEmpty(usbDrive.RootDirectory.Name);

            }, "Error No se ha encontrado el dispositivo USB del EKOR", 60, 1000, 2000, false, false);
        }

        public void TestConsumoRS232()
        {
            tower.IO.DO.On(OUT_DB9);
            SamplerWithCancel((p) => { return tower.IO.DI[28]; }, "Error, no se ha detectado el piston DB9 entrar al equipo correctamente", 3, 500, 1000);

            usbIO.DO.OnWait(250, SUPPLY_232_EXT);
            usbIO.DO.On(ACTIVE_VEXT232_TO_IN2_MUX);

            var param = new AdjustValueDef(Params.V_DC.Null.TestPoint("EXT232"), ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC, 500);
            }, 1, 4, 2000);

            usbIO.DO.OffWait(250, SUPPLY_232_EXT);
            usbIO.DO.Off(ACTIVE_VEXT232_TO_IN2_MUX);
        }

        public override void TestCommunicationsUSB(string nameDevicePortVirtual)
        {
            base.TestCommunicationsUSB(nameDevicePortVirtual);

            Ekor.Cirbus.PortCom = Comunicaciones.SerialPort;
        }

        public override void TestLeds()
        {
            CAMERA_IDS_LEDS = GetVariable<string>("CAMARA_IDS_LED", CAMERA_IDS_LEDS);

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LEDS, "EKOR_PLUS_LED", "LED", "EKOR_PLUS", 5, (capture) => Delay(250 * capture, ""));           
        }

        public void TestCover()
        {
            var caratula = Identificacion.CARATULA;
            if (caratula != "NO")
                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS_DISPLAY, "EKOR_PLUS_CARATULA_" + caratula, "CARATULA_" + caratula, "EKOR_PLUS");           
        }

        public override void TestKeyborad()
        {
            var keyboardValueFromUtil = util.GetKeyValuePairKeyBoard();
            var inputsValue = new List<KeyValuePair<byte, EKOR_SAFE_OLD.enumKeyboard>>();

            foreach (var keys in keyboardValueFromUtil)
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumKeyboard>(keys.Key, (EKOR_SAFE_OLD.enumKeyboard)keys.Value));

            util.ActivatePistonTestKeyboard();
            foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumKeyboard> input in inputsValue)
            {
                logger.InfoFormat("Test key {0}", input.Value.ToString());

                if (input.Key == OUT_USB_IO)
                    usbIO.DO.On(input.Key);
                else
                    tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    return (bool)Ekor.ReadKeyState((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 6, 500, 250, false, false);

                if (input.Key == OUT_USB_IO)
                    usbIO.DO.Off(input.Key);
                else
                    tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    return !Ekor.ReadKeyState((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 3, 500, 250, false, false);

                Resultado.Set(string.Format("KEY_{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
            util.DesactivatePistonTestKeyboard();
        }

        public override void TestDesviationClock()
        {
            Ekor.WriteDateTime();
            util.SupplyUSB(false);
            tower.LAMBDA.ApplyOff();
            Delay(2000, "");
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.SinUnidad), Consignas.GetDouble(Params.I.Null.EnVacio.Vmax.Name, 24, ParamUnidad.SinUnidad));
            base.TestDesviationClock();
        }

        public void TestCalibrationMultiCurrents([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_SAFE_OLD.Scale scale, double consigna_mA = 0)
        {
            EKOR_SAFE_OLD.ChannelCurrent channel = EKOR_SAFE_OLD.ChannelCurrent.IL123;
            verificationArgumentsCurrent(scale, channel, false);

            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, consigna_mA, ParamUnidad.mA);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Other(channel.ToString()).Ajuste.Name, 45, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current / 1000, 0, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new TriAdjustValueDef(Params.I.L1.Ajuste.TestPoint(scaleDesc).Name, Params.I.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.I.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false);

            SamplerWithCancel((s) =>
            {
                TestMeasureBase(defs,
                (step) =>
                {
                    var calibrationValues = new List<double>();

                    SamplerWithCancel((p) =>
                    {
                        Ekor.CalibrationCurrents(channel, scale, (int)current);

                        Delay(4000, "Espera calibración");

                        var calibrationFactors = Ekor.ReadCalibrationScale<EKOR_RPA100.StructCalibrationMeasure>(scale);
                        calibrationValues.Add((double)calibrationFactors.factor_IL1, (double)calibrationFactors.factor_IL2, (double)calibrationFactors.factor_IL3);

                        return true;

                    }, "Error al intentar comunicar con el EKOR al Calibar corrientes", 5, 1000, 1000, false, false);

                    return calibrationValues.ToArray();

                }, 0, 2, 1500);

                Ekor.Reset();

                TestVerificationMultiCurrents(scale);

                return true;

            }, "", 2, 500, 1500, false, false);
        }

        public override void TestCalibrationSingleCurrent([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_SAFE_OLD.Scale scale, [Description("IL1= 1, IL2=2, IL3=4, IH=8")] EKOR_SAFE_OLD.ChannelCurrent channel = EKOR_SAFE_OLD.ChannelCurrent.IH, double consigna_mA = 0)
        {
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            verificationArgumentsCurrent(scale, channel, true);

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, consigna_mA, ParamUnidad.mA);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Other(channel.ToString()).Ajuste.Name, 45, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(voltage, 0, 0), TriLineValue.Create(current / 1000, 0, 0), 0, gap, TriLineValue.Create(0, 120, 240));

            var defs = new AdjustValueDef(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, 0, Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Min(), Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos);
            var defsVerif = new AdjustValueDef(Params.I.Other(channel.ToString()).Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, current, Params.I.Other(channel.ToString()).Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.mA);
            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            SamplerWithCancel((s) =>
            {
                TestMeasureBase(defs,
                (step) =>
                {
                    Ekor.CalibrationCurrents(channel, scale, (int)current);

                    Delay(4000, "Espera calibración");

                    var calibrationValues = Ekor.ReadCalibrationScale<EKOR_RPA100.StructCalibrationMeasure>(scale);

                    double value = 0;

                    switch (channel)
                    {
                        case EKOR_SAFE_OLD.ChannelCurrent.IL1:
                            value = calibrationValues.factor_IL1;
                            break;
                        case EKOR_SAFE_OLD.ChannelCurrent.IL2:
                            value = calibrationValues.factor_IL2;
                            break;
                        case EKOR_SAFE_OLD.ChannelCurrent.IL3:
                            value = calibrationValues.factor_IL3;
                            break;
                        case EKOR_SAFE_OLD.ChannelCurrent.IH:
                            value = calibrationValues.factor_IH;
                            break;
                    }

                    return value;

                }, 0, 2, 1500);

                Ekor.Reset();

                TestVerificationSingleCurrent(scale);                

                return true;

            }, "", 2, 500, 1500, false, false);
        }

        public override void TestCalibrationMultiVoltage([Description("VL= 112")] EKOR_SAFE_OLD.ChannelVoltage channel)
        {
            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 100, ParamUnidad.V);
            double freq = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50.1, ParamUnidad.Hz);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, 0, freq, 0, TriLineValue.Create(0, 120, 240));

            var defsList = new List<TriAdjustValueDef>();
            defsList.Add(new TriAdjustValueDef(Params.V.L1.Ajuste.Name, Params.V.Null.Ajuste.Min(), Params.V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos, false, false));

            var calibrationFactors = new EKOR_RPA100.StructCalibrationMeasure();

            MeasureCurrentApplyVoltageChannel();

            TestMeasureBase(defsList,
            (step) =>
            {
                Ekor.CalibrationVoltage(channel, measureValue.CurrentsVL.ToList(), EKOR_SAFE_OLD.Scale.Gruesa);

                SamplerWithCancel((p) =>
                {
                    return ((EKOR_SAFE_OLD)Ekor).ReadCalibrationFinished();
                }, "Error, no ha finalizado la autocalibración correctamente en el tiempo esperado", 40, 2000, 5000);

                calibrationFactors = Ekor.ReadCalibrationScale<EKOR_RPA100.StructCalibrationMeasure>(EKOR_SAFE_OLD.Scale.Gruesa);

                var calibrationValues = new List<double>();

                if (channel == EKOR_SAFE_OLD.ChannelVoltage.VL123)
                    calibrationValues.Add((double)calibrationFactors.factor_VL1, (double)calibrationFactors.factor_VL2, (double)calibrationFactors.factor_VL3);

                logger.InfoFormat("calibrationFactors Escala GRUESA sin ajustar ->  VL1={0}  VL2={1} VL3={2}", calibrationFactors.factor_VL1, calibrationFactors.factor_VL2, calibrationFactors.factor_VL3);

                return calibrationValues.ToArray();
            }, 0, 2, 1500);

            CalibrationVoltage(false);

            var factorsCalculatedScaleGruesa = Ekor.ReadCalibrationScale<EKOR_RPA100.StructCalibrationMeasure>(EKOR_SAFE_OLD.Scale.Gruesa);
            factorsCalculatedScaleGruesa.factor_VL1 = (int)(factorsCalculatedScaleGruesa.factor_VL1 * 0.992994);
            factorsCalculatedScaleGruesa.factor_VL2 = (int)(factorsCalculatedScaleGruesa.factor_VL2 * 0.992994);
            factorsCalculatedScaleGruesa.factor_VL3 = (int)(factorsCalculatedScaleGruesa.factor_VL3 * 0.992994);

            Ekor.WriteFactorsCalibration(EKOR_SAFE_OLD.Scale.Gruesa, factorsCalculatedScaleGruesa);
            logger.InfoFormat("calibrationFactors Escala GRUESA ->  VL1={0}  VL2={1} VL3={2}", factorsCalculatedScaleGruesa.factor_VL1, factorsCalculatedScaleGruesa.factor_VL2, factorsCalculatedScaleGruesa.factor_VL3);
            Delay(2500, "");

            var factorsCalculatedScaleFina = Ekor.ReadCalibrationScale<EKOR_RPA100.StructCalibrationMeasure>(EKOR_SAFE_OLD.Scale.Fina);
            factorsCalculatedScaleFina.factor_VL1 = (int)(factorsCalculatedScaleGruesa.factor_VL1 / 50.96);
            factorsCalculatedScaleFina.factor_VL2 = (int)(factorsCalculatedScaleGruesa.factor_VL2 / 50.96);
            factorsCalculatedScaleFina.factor_VL3 = (int)(factorsCalculatedScaleGruesa.factor_VL3 / 50.96);

            Ekor.WriteFactorsCalibration(EKOR_SAFE_OLD.Scale.Fina, factorsCalculatedScaleFina);
            logger.InfoFormat("calibrationFactors Escala FINA ->  VL1={0}  VL2={1} VL3={2}", factorsCalculatedScaleFina.factor_VL1, factorsCalculatedScaleFina.factor_VL2, factorsCalculatedScaleFina.factor_VL3);
            Delay(2500, "");

            var factorsCalculatedScaleCorto = Ekor.ReadCalibrationScale<EKOR_RPA100.StructCalibrationMeasure>(EKOR_SAFE_OLD.Scale.Cortoc);
            factorsCalculatedScaleCorto.factor_VL1 = (int)(factorsCalculatedScaleGruesa.factor_VL1);
            factorsCalculatedScaleCorto.factor_VL2 = (int)(factorsCalculatedScaleGruesa.factor_VL2);
            factorsCalculatedScaleCorto.factor_VL3 = (int)(factorsCalculatedScaleGruesa.factor_VL3);

            Ekor.WriteFactorsCalibration(EKOR_SAFE_OLD.Scale.Gruesa, factorsCalculatedScaleCorto);
            logger.InfoFormat("calibrationFactors Escala CORTOC ->  VL1={0}  VL2={1} VL3={2}", factorsCalculatedScaleCorto.factor_VL1, factorsCalculatedScaleCorto.factor_VL2, factorsCalculatedScaleCorto.factor_VL3);
            Delay(2500, "");
        }

        public override void TestVerificationMultiVoltage([Description("VL= 112")] EKOR_SAFE_OLD.ChannelVoltage channel)
        {
            var patronVoltage = new TriLineValue();
            var defsVerif = new List<AdjustValueDef>();
            if (channel == EKOR_SAFE_OLD.ChannelVoltage.VL123)
            {
                patronVoltage = measureValue.CurrentsVL;
                defsVerif.Add(new AdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, patronVoltage.L1, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
                defsVerif.Add(new AdjustValueDef(Params.V.L2.Verificacion.Name, 0, 0, 0, patronVoltage.L2, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
                defsVerif.Add(new AdjustValueDef(Params.V.L3.Verificacion.Name, 0, 0, 0, patronVoltage.L3, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            }          

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            TestCalibracionBase(defsVerif,
            () =>
            {
                var vars = Ekor.ReadVoltageMeasureVerification<EKOR_RPA100.VoltageMeasureVerification>();
                logger.InfoFormat("LEV  VL1: {0}", vars.VoltageL1);
                logger.InfoFormat("LEV  VL2: {0}", vars.VoltageL2);
                logger.InfoFormat("LEV  VL3: {0}", vars.VoltageL3);
                
                var result = new List<double>() { vars.VoltageL1, vars.VoltageL2, vars.VoltageL3 };
                return result.ToArray();              
            },
            () =>
            {
                var result = new List<double>() { patronVoltage.L1, patronVoltage.L2, patronVoltage.L3 };
                logger.InfoFormat("PATRON Metrix VL1: {0}", patronVoltage.L1);
                logger.InfoFormat("PATRON Metrix VL2: {0}", patronVoltage.L2);
                logger.InfoFormat("PATRON Metrix VL3: {0}", patronVoltage.L3);
                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1500);
        }

        public void CalibrationVoltage(bool consignar = true)
        {
            double freq = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 50.1, ParamUnidad.Hz);

            if (consignar)
            {
                double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 100, ParamUnidad.V);
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, 0, freq, 0, TriLineValue.Create(0, 120, 240));
            }

            var defsL1Metrix = new AdjustValueDef(Params.I.L1.TESTER.modo("VL_VERIFICACION").Name, 0, Params.I.Null.TESTER.modo("VL_VERIFICACION").Min(), Params.I.Null.TESTER.modo("VL_VERIFICACION").Max(), 0, 0, ParamUnidad.mV);
            var defsL2Metrix = new AdjustValueDef(Params.I.L2.TESTER.modo("VL_VERIFICACION").Name, 0, Params.I.Null.TESTER.modo("VL_VERIFICACION").Min(), Params.I.Null.TESTER.modo("VL_VERIFICACION").Max(), 0, 0, ParamUnidad.mV);
            var defsL3Metrix = new AdjustValueDef(Params.I.L3.TESTER.modo("VL_VERIFICACION").Name, 0, Params.I.Null.TESTER.modo("VL_VERIFICACION").Min(), Params.I.Null.TESTER.modo("VL_VERIFICACION").Max(), 0, 0, ParamUnidad.mV);

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES_VOLTAGE", 30, ParamUnidad.SinUnidad);

            using (var mtx = new MTX3293())
            {
                string nameDevicePortVirtual = "MetrixPortVirtual";
                var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
                mtx.PortCom = portMetrix;

                util.ActivateMeasurePasatapasL1();
                Delay(1000, "");

                var metrixL1Result = TestMeasureBase(defsL1Metrix,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC)*1000.0;
                    logger.InfoFormat("METRIX -> VL1 = {0}", vl);
                    return vl;
                }, 0, 3, 1000, true);

                util.DesactivateMeasurePasatapasL1();
                Delay(1000, "");

                util.ActivateMeasurePasatapasL2();
                Delay(1000, "");

                var metrixL2Result = TestMeasureBase(defsL2Metrix,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC) * 1000.0;
                    logger.InfoFormat("METRIX -> VL2 = {0}", vl);
                    return vl;
                }, 0, 3, 1000, true);

                util.DesactivateMeasurePasatapasL2();
                Delay(1000, "");

                util.ActivateMeasurePasatapasL3();
                Delay(1000, "");

                var metrixL3Result = TestMeasureBase(defsL3Metrix,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC) * 1000.0;
                    logger.InfoFormat("METRIX -> VL3 = {0}", vl);
                    return vl;
                }, 0, 3, 1000, true);

                util.DesactivateMeasurePasatapasL3();
                Delay(1000, "");
           
                var triDefs = new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), Params.V.Null.Verificacion.Var(), ParamUnidad.V);

                TestCalibracionBase(triDefs,
                () =>
                {
                    CheckCancellationRequested();
                    var vars = Ekor.ReadVoltageMeasureVerification<EKOR_RPA100.VoltageMeasureVerification>();
                    logger.InfoFormat("LEV  VL1: {0}", vars.VoltageL1);
                    return new double[] { vars.VoltageL1, vars.VoltageL2, vars.VoltageL3 }  ;
                },
                () =>
                {
                    return new double[] { metrixL1Result.Value, metrixL2Result.Value, metrixL3Result.Value };
                }, numMuestras, numMuestras, 1300);
            }
        }

        public override void MeasureCurrentApplyVoltageChannel()
        {
            measureValue = new StructCurrentsInVoltageChannel();

            measureValue.resistenceVL = Configuracion.GetDouble(Params.RES.Other("VL").TESTER.Name, 10000, ParamUnidad.Ohms);

            var defsL1 = new AdjustValueDef(Params.I.L1.TESTER.modo("VL").Name, 0, Params.I.Null.TESTER.modo("VL").Min(), Params.I.Null.TESTER.modo("VL").Max(), 0, 0, ParamUnidad.V);
            var defsL2 = new AdjustValueDef(Params.I.L2.TESTER.modo("VL").Name, 0, Params.I.Null.TESTER.modo("VL").Min(), Params.I.Null.TESTER.modo("VL").Max(), 0, 0, ParamUnidad.V);
            var defsL3 = new AdjustValueDef(Params.I.L3.TESTER.modo("VL").Name, 0, Params.I.Null.TESTER.modo("VL").Min(), Params.I.Null.TESTER.modo("VL").Max(), 0, 0, ParamUnidad.V);

            using (var mtx = new MTX3293())
            {
                string nameDevicePortVirtual = "MetrixPortVirtual";
                var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
                mtx.PortCom = portMetrix;

                util.ActivateMeasurePasatapasL1();
                Delay(1000, "");

                TestMeasureBase(defsL1,
               (step) =>
               {
                   CheckCancellationRequested();
                   var vl= mtx.ReadVoltage(MTX3293.measureTypes.AC);
                   logger.InfoFormat("METRIX -> VL1 = {0}", vl);
                   return vl;
               }, 0, 3, 1000, true);

                util.DesactivateMeasurePasatapasL1();
                logger.InfoFormat("Resultado Medida METRIX L1 {0}", defsL1.ToString());
                util.ActivateMeasurePasatapasL2();
                Delay(1000, "");

                TestMeasureBase(defsL2,
               (step) =>
               {
                   CheckCancellationRequested();
                   var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC);
                   logger.InfoFormat("METRIX -> VL2 = {0}", vl);
                   return vl;
               }, 0, 3, 1000, true);

                util.DesactivateMeasurePasatapasL2();
                logger.InfoFormat("Resultado Medida METRIX L2 {0}", defsL2.ToString());
                util.ActivateMeasurePasatapasL3();
                Delay(1000, "");

                TestMeasureBase(defsL3,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC);
                    logger.InfoFormat("METRIX -> VL3 = {0}", vl);
                    return vl;
                }, 0, 3, 1000, true);

                util.DesactivateMeasurePasatapasL3();
                logger.InfoFormat("Resultado Medida METRIX L3 {0}", defsL3.ToString());

                measureValue.currentVL = TriLineValue.Create(defsL1.Value, defsL2.Value, defsL3.Value);
            }
        }

        public void TestFinish()
        {
            if(usbIO != null)
                usbIO.DO.OffRange(0,31);

            if(tower != null)
                tower.IO.DO.OffWait(1000, util.GetKeyValuePairKeyBoard().Select(p => (int)p.Key).Where(p => p != OUT_USB_IO).ToArray());

            interalTestFinish();
        }

        /////////
        public void TestInitializationParametrization(string ekorPort)
        {
            var port = Convert.ToInt32(GetVariable<string>(ekorPort).Replace("COM", ""));
            Ekor = new EKOR_RPA100(port);

        }
    }
}
