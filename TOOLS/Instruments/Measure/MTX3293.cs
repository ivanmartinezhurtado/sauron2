﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Threading;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.00)]
    public class MTX3293 : MeasureBase<MTX3293>, IDisposable, IConsoleDevice
    {
        public MTX3293SCPI SCPI { get { return new MTX3293SCPI(this); } }

        private SerialPort sp;
        private byte _port = 19;
        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        public string idn { get { return id; } }
        private string id;

        public int WaitTimeMeasure { get; set; }

        private string measureReaded;

        public enum PrivateCommands
        {
            READ_MEASURE = Commands.USER_DEFINED_COMMANDS + 1,
            SET_FUNCTION = Commands.USER_DEFINED_COMMANDS + 2,
            SET_RANGE = Commands.USER_DEFINED_COMMANDS + 3,
            SET_AUTO_RANGE = Commands.USER_DEFINED_COMMANDS + 4,
            SET_MEASURE_TYPE = Commands.USER_DEFINED_COMMANDS + 5,
            READ_FUNCTION = Commands.USER_DEFINED_COMMANDS + 6,
            SET_FREQ_MODE = Commands.USER_DEFINED_COMMANDS + 7
        }

        public Dictionary<PrivateCommands, string> commandStrings = new Dictionary<PrivateCommands, string>()
        {
            {PrivateCommands.READ_MEASURE, "MEAS?"},
            {PrivateCommands.SET_FUNCTION, "SENS:FUNC "+'"'+"{0}"+'"'},
            {PrivateCommands.SET_RANGE, "SENS:RANG {0}"},
            {PrivateCommands.SET_AUTO_RANGE, "SENS:RANG:AUTO {0}"},
            {PrivateCommands.SET_MEASURE_TYPE, "INP:COUP {0}"},
            {PrivateCommands.READ_FUNCTION, "SENS:FUNC?"},
            {PrivateCommands.SET_FREQ_MODE, "FREQ:MOD {0}"}
        };

        public enum Functions
        {
            VOLT,
            CURR,
            RES,
            FREQ,
            CONT,
            TEMP,
            CAPA,
            DIOD,
            DIODEZ
        }

        public enum measureTypes
        {
            AC,
            DC,
            ACDC,
            NULL
        }

        public enum RangeVoltage
        {
            _100mV = 0,
            _1V = 1,
            _10V = 10,
            _100V = 100,
            _1000V = 1000,
            AUTO
        };

        public enum RangeResistance
        {
            _1K = 1,
            _10K = 10000,
            _100K = 100000,
            _1M = 1000000,
            _10M = 10000000,
            _100M = 100000000,
            AUTO
        };

        public enum RangeCapacitance
        {
            _1nF = 1,
            _10nF = 10,
            _1uF = 1000,
            _10uF = 10000,
            _100uF = 100000,
            _1mF = 1000000,
            AUTO
        };

        public enum RangeFrecuency
        {
            INF200KHZ,
            SUP200KHZ
        };
        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public MTX3293()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinTx = "\n", CaracterFinRx = "\n" };
            var spAdapter = new SerialPortAdapter(sp, "\n");
            transport = new MessageTransport(protocol, spAdapter, _logger);
            transport.Retries = 1;
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + _port;
            }
        }

        public Result<string> WriteCommand(string text)
        {
            try
            {
                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            sp.WriteTimeout = sp.ReadTimeout = 1000;
            sp.BaudRate = 19200;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();

            WaitTimeMeasure = 1000;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            switch (command.Commands)
            {
                case (int)PrivateCommands.READ_MEASURE:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.READ_MEASURE]);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);                   
                    break;
                case (int)PrivateCommands.SET_FUNCTION:                    
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.SET_FUNCTION], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);                   
                    response = null;
                    break;
                case (int)PrivateCommands.SET_RANGE:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.SET_RANGE], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    response = null;
                    break;
                case (int)PrivateCommands.SET_AUTO_RANGE:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.SET_AUTO_RANGE], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    response = null;
                    break;
                case (int)PrivateCommands.SET_MEASURE_TYPE:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.SET_MEASURE_TYPE], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    response = null;
                    break;
                case (int)PrivateCommands.READ_FUNCTION:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.READ_FUNCTION]);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    break;
                case (int)PrivateCommands.SET_FREQ_MODE:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.SET_FREQ_MODE], command.Args);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    break;
                default:
                    throw new NotImplementedException("Comando inexistente o no implementado");
                    
            }

            if (message.HasResponse)
                measureReaded = response.Text;
        }

        public void Reset()
        {            
            SCPI.Reset.Command();
        }

        public string GetIdentification()
        {
            var read = SCPI.Identification.Query();
            id = read.Value;
            return idn;
        }

        private double ReadMeasure(bool thowExcption= true)
        {
            try
            {
                Thread.Sleep(WaitTimeMeasure);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.READ_MEASURE, true));

                if (measureReaded.ToUpper().Trim().Contains("OL"))
                    return Double.NaN;

                return Convert.ToDouble(measureReaded.Replace(".", ","));
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Exception ReadMeasure by {0}", ex.Message);

                if (thowExcption)
                    throw;

                return Double.NaN;
            }

        }

        public double ReadVoltage(measureTypes typeMeasure, RangeVoltage range = RangeVoltage.AUTO, bool thowExcption = true)
        {
            SetFunction(Functions.VOLT);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MEASURE_TYPE, true, typeMeasure.GetDescription()));
            
            if (range == RangeVoltage.AUTO)
                SetAutoRange();
            else
                SetVoltageRange(range);

            return ReadMeasure(thowExcption);          
        }

        public double ReadCurrent(measureTypes typeMeasure, bool thowExcption = true)
        {
            SetFunction(Functions.CURR);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MEASURE_TYPE, true, typeMeasure.GetDescription()));

            return ReadMeasure(thowExcption);
        }

        public double ReadCapacitance(RangeCapacitance range = RangeCapacitance.AUTO, bool thowExcption = true)
        {
            SetFunction(Functions.CAPA);           
            
            if (range == RangeCapacitance.AUTO)
                SetAutoRange();
            else
                SetCapacitanceRange(range);

            return ReadMeasure(thowExcption);
        }

        public double ReadResistance(RangeResistance range = RangeResistance.AUTO, bool thowExcption = true)
        {
            SetFunction(Functions.RES);
            
            if (range == RangeResistance.AUTO)
                SetAutoRange();
            else
                SetResistanceRange(range);

            return ReadMeasure(thowExcption);
        }

        public double ReadFrecuency(RangeFrecuency range = RangeFrecuency.INF200KHZ, bool thowExcption = true)
        {
            SetFunction(Functions.FREQ);
            SetFrequencyRange(range);

            return ReadMeasure(thowExcption);
        }     

        public double ReadTemperature(bool thowExcption = true)
        {
            SetFunction(Functions.TEMP);
            return ReadMeasure(thowExcption);
        }

        public string ReadContinuity()
        {
            SetFunction(Functions.CONT);
            Thread.Sleep(WaitTimeMeasure);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.READ_MEASURE, true));

            return measureReaded;
        }

        public string ReadFunction()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.READ_FUNCTION, true));

            return measureReaded;
        }

        public void SetFunction(Functions function)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_FUNCTION, true, function.GetDescription()));
        }
             
        public void SetVoltageRange(RangeVoltage range)
        {          
            var rang = range.Equals(RangeVoltage._100mV) ? 0.1 : (double)range;
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RANGE, true, rang));
        }

        public void SetResistanceRange(RangeResistance range)
        {           
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RANGE, true, (double)range));
        }

        public void SetCapacitanceRange(RangeCapacitance range)
        {
            var rang = (double)range / 1000000000;
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RANGE, true, rang));
        }

        public void SetFrequencyRange(RangeFrecuency range)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_FREQ_MODE, true, range.GetDescription()));
        }

        public void SetAutoRange()
        {          
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_AUTO_RANGE, true,  "1"));
        }
       
        public void SetMode(measureTypes mode)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MEASURE_TYPE, true, mode.GetDescription()));
        }        

        public void Dispose()
        {
            if (sp != null && sp.IsOpen)
                sp.Close();
        }

        //public string ReadAmplitudeRangeInFrecuency()
        //{
        //    sp.DiscardInBuffer();
        //    sp.WriteLine("FREQ:THR:VOLT:RANG?");
        //    Thread.Sleep(100);
        //    var response = sp.ReadLine();
        //    return response;
        //}

        //public string ReadAutoRange()
        //{
        //    sp.Open();
        //    sp.DiscardInBuffer();
        //    sp.WriteLine("SENS:RANG:AUTO?");
        //    Thread.Sleep(100);
        //    var response = sp.ReadLine();
        //    sp.Close();
        //    return response;
        //}

        //public string ReadFunction()
        //{
        //    sp.DiscardInBuffer();
        //    sp.WriteLine("SENS:FUNC?");
        //    Thread.Sleep(100);
        //    var response = sp.ReadLine();
        //    return response;
        //}

        //public string ReadFunctionCalcdDisplaySecondary()
        //{
        //    sp.DiscardInBuffer();
        //    sp.WriteLine("*IDN?"); //CALC:FUNC?
        //    Thread.Sleep(100);
        //    var response = sp.ReadLine();
        //    return response;
        //}

        //public void SetFunction(string function)
        //{

        //    sp.WriteLine(string.Format("SENS:FUNC {0}{1}{2}",'"',function,'"'));

        //}
     
        public string help()
        {
            sp.Open();
            sp.DiscardInBuffer();
            sp.WriteLine("HELP?");
            
            Thread.Sleep(1000);
            var response = sp.ReadLine();
            sp.Close();
            return response;
        }
    }
}
