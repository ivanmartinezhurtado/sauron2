﻿using Dezac.Tests.Model;

namespace WinTestPlayer.Views
{
    partial class ResultsParamView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn9 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn10 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn11 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn12 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn13 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn14 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            this.enumBinder1 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder2 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder3 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder4 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder5 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder6 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder7 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder8 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder9 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder10 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder11 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder12 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.bsList = new System.Windows.Forms.BindingSource(this.components);
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.enumBinder13 = new Telerik.WinControls.UI.Data.EnumBinder();
            this.enumBinder14 = new Telerik.WinControls.UI.Data.EnumBinder();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.SuspendLayout();
            // 
            // enumBinder1
            // 
            this.enumBinder1.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn1.DataSource = this.enumBinder1;
            gridViewComboBoxColumn1.DisplayMember = "Description";
            gridViewComboBoxColumn1.ValueMember = "Value";
            this.enumBinder1.Target = gridViewComboBoxColumn1;
            // 
            // enumBinder2
            // 
            this.enumBinder2.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn2.DataSource = this.enumBinder2;
            gridViewComboBoxColumn2.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn2.DisplayMember = "Description";
            gridViewComboBoxColumn2.FieldName = "IdUnidad";
            gridViewComboBoxColumn2.HeaderText = "IdUnidad";
            gridViewComboBoxColumn2.IsAutoGenerated = true;
            gridViewComboBoxColumn2.Name = "IdUnidad";
            gridViewComboBoxColumn2.ValueMember = "Value";
            this.enumBinder2.Target = gridViewComboBoxColumn2;
            // 
            // enumBinder3
            // 
            this.enumBinder3.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn3.DataSource = this.enumBinder3;
            gridViewComboBoxColumn3.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn3.DisplayMember = "Description";
            gridViewComboBoxColumn3.FieldName = "IdUnidad";
            gridViewComboBoxColumn3.HeaderText = "IdUnidad";
            gridViewComboBoxColumn3.IsAutoGenerated = true;
            gridViewComboBoxColumn3.Name = "IdUnidad";
            gridViewComboBoxColumn3.ValueMember = "Value";
            this.enumBinder3.Target = gridViewComboBoxColumn3;
            // 
            // enumBinder4
            // 
            this.enumBinder4.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn4.DataSource = this.enumBinder4;
            gridViewComboBoxColumn4.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn4.DisplayMember = "Description";
            gridViewComboBoxColumn4.FieldName = "IdUnidad";
            gridViewComboBoxColumn4.HeaderText = "IdUnidad";
            gridViewComboBoxColumn4.IsAutoGenerated = true;
            gridViewComboBoxColumn4.Name = "IdUnidad";
            gridViewComboBoxColumn4.ValueMember = "Value";
            this.enumBinder4.Target = gridViewComboBoxColumn4;
            // 
            // enumBinder5
            // 
            this.enumBinder5.Source = typeof(TipoGrupo);
            gridViewComboBoxColumn5.DataSource = this.enumBinder5;
            gridViewComboBoxColumn5.DataType = typeof(TipoGrupo);
            gridViewComboBoxColumn5.DisplayMember = "Description";
            gridViewComboBoxColumn5.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn5.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn5.IsAutoGenerated = true;
            gridViewComboBoxColumn5.Name = "IdTipoGrupo";
            gridViewComboBoxColumn5.ValueMember = "Value";
            this.enumBinder5.Target = gridViewComboBoxColumn5;
            // 
            // enumBinder6
            // 
            this.enumBinder6.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn6.DataSource = this.enumBinder6;
            gridViewComboBoxColumn6.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn6.DisplayMember = "Description";
            gridViewComboBoxColumn6.FieldName = "IdUnidad";
            gridViewComboBoxColumn6.HeaderText = "IdUnidad";
            gridViewComboBoxColumn6.IsAutoGenerated = true;
            gridViewComboBoxColumn6.Name = "IdUnidad";
            gridViewComboBoxColumn6.ValueMember = "Value";
            this.enumBinder6.Target = gridViewComboBoxColumn6;
            // 
            // enumBinder7
            // 
            this.enumBinder7.Source = typeof(TipoGrupo);
            gridViewComboBoxColumn7.DataSource = this.enumBinder7;
            gridViewComboBoxColumn7.DataType = typeof(TipoGrupo);
            gridViewComboBoxColumn7.DisplayMember = "Description";
            gridViewComboBoxColumn7.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn7.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn7.IsAutoGenerated = true;
            gridViewComboBoxColumn7.Name = "IdTipoGrupo";
            gridViewComboBoxColumn7.ValueMember = "Value";
            this.enumBinder7.Target = gridViewComboBoxColumn7;
            // 
            // enumBinder8
            // 
            this.enumBinder8.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn8.DataSource = this.enumBinder8;
            gridViewComboBoxColumn8.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn8.DisplayMember = "Description";
            gridViewComboBoxColumn8.FieldName = "IdUnidad";
            gridViewComboBoxColumn8.HeaderText = "IdUnidad";
            gridViewComboBoxColumn8.IsAutoGenerated = true;
            gridViewComboBoxColumn8.Name = "IdUnidad";
            gridViewComboBoxColumn8.ValueMember = "Value";
            this.enumBinder8.Target = gridViewComboBoxColumn8;
            // 
            // enumBinder9
            // 
            this.enumBinder9.Source = typeof(TipoGrupo);
            gridViewComboBoxColumn9.DataSource = this.enumBinder9;
            gridViewComboBoxColumn9.DataType = typeof(TipoGrupo);
            gridViewComboBoxColumn9.DisplayMember = "Description";
            gridViewComboBoxColumn9.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn9.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn9.IsAutoGenerated = true;
            gridViewComboBoxColumn9.Name = "IdTipoGrupo";
            gridViewComboBoxColumn9.ValueMember = "Value";
            this.enumBinder9.Target = gridViewComboBoxColumn9;
            // 
            // enumBinder10
            // 
            this.enumBinder10.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn10.DataSource = this.enumBinder10;
            gridViewComboBoxColumn10.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn10.DisplayMember = "Description";
            gridViewComboBoxColumn10.FieldName = "IdUnidad";
            gridViewComboBoxColumn10.HeaderText = "IdUnidad";
            gridViewComboBoxColumn10.IsAutoGenerated = true;
            gridViewComboBoxColumn10.Name = "IdUnidad";
            gridViewComboBoxColumn10.ValueMember = "Value";
            this.enumBinder10.Target = gridViewComboBoxColumn10;
            // 
            // enumBinder11
            // 
            this.enumBinder11.Source = typeof(TipoGrupo);
            gridViewComboBoxColumn11.DataSource = this.enumBinder11;
            gridViewComboBoxColumn11.DataType = typeof(TipoGrupo);
            gridViewComboBoxColumn11.DisplayMember = "Description";
            gridViewComboBoxColumn11.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn11.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn11.IsAutoGenerated = true;
            gridViewComboBoxColumn11.Name = "IdTipoGrupo";
            gridViewComboBoxColumn11.ValueMember = "Value";
            this.enumBinder11.Target = gridViewComboBoxColumn11;
            // 
            // enumBinder12
            // 
            this.enumBinder12.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn12.DataSource = this.enumBinder12;
            gridViewComboBoxColumn12.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn12.DisplayMember = "Description";
            gridViewComboBoxColumn12.FieldName = "IdUnidad";
            gridViewComboBoxColumn12.HeaderText = "IdUnidad";
            gridViewComboBoxColumn12.IsAutoGenerated = true;
            gridViewComboBoxColumn12.Name = "IdUnidad";
            gridViewComboBoxColumn12.ValueMember = "Value";
            this.enumBinder12.Target = gridViewComboBoxColumn12;
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.Color.AliceBlue;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(10, 24);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowEditRow = false;
            this.radGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "NumInstance";
            gridViewTextBoxColumn1.HeaderText = "Equipo";
            gridViewTextBoxColumn1.Name = "NumInstance";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 68;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "StepName";
            gridViewTextBoxColumn2.HeaderText = "StepName";
            gridViewTextBoxColumn2.Name = "StepName";
            gridViewTextBoxColumn2.Width = 165;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Name";
            gridViewTextBoxColumn3.HeaderText = "Name";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.Name = "Name";
            gridViewTextBoxColumn3.Width = 218;
            gridViewDecimalColumn1.DataType = typeof(System.Nullable<double>);
            gridViewDecimalColumn1.EnableExpressionEditor = false;
            gridViewDecimalColumn1.FieldName = "Min";
            gridViewDecimalColumn1.HeaderText = "Min";
            gridViewDecimalColumn1.IsAutoGenerated = true;
            gridViewDecimalColumn1.Name = "Min";
            gridViewDecimalColumn1.Width = 85;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "Valor";
            gridViewTextBoxColumn4.HeaderText = "Valor";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.Name = "Valor";
            gridViewTextBoxColumn4.Width = 85;
            gridViewDecimalColumn2.DataType = typeof(System.Nullable<double>);
            gridViewDecimalColumn2.EnableExpressionEditor = false;
            gridViewDecimalColumn2.FieldName = "Max";
            gridViewDecimalColumn2.HeaderText = "Max";
            gridViewDecimalColumn2.IsAutoGenerated = true;
            gridViewDecimalColumn2.Name = "Max";
            gridViewDecimalColumn2.Width = 85;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "Unidad";
            gridViewTextBoxColumn5.HeaderText = "Unidad";
            gridViewTextBoxColumn5.IsAutoGenerated = true;
            gridViewTextBoxColumn5.Name = "Unidad";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "ValorEsperado";
            gridViewTextBoxColumn6.HeaderText = "Valor BD";
            gridViewTextBoxColumn6.Name = "ValorEsperado";
            gridViewTextBoxColumn6.Width = 89;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDecimalColumn1,
            gridViewTextBoxColumn4,
            gridViewDecimalColumn2,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.radGridView1.MasterTemplate.DataSource = this.bsList;
            this.radGridView1.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridView1.MasterTemplate.EnableGrouping = false;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.Size = new System.Drawing.Size(893, 304);
            this.radGridView1.TabIndex = 0;
            this.radGridView1.Text = "radGridView1";
            this.radGridView1.ThemeName = "ControlDefault";
            this.radGridView1.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.radGridView1_CellFormatting);
            // 
            // bsList
            // 
            this.bsList.DataSource = typeof(ParamValue);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.Navy;
            this.radLabel2.Location = new System.Drawing.Point(12, 3);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(74, 18);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Resultados";
            // 
            // enumBinder13
            // 
            this.enumBinder13.Source = typeof(TipoGrupo);
            gridViewComboBoxColumn13.DataSource = this.enumBinder13;
            gridViewComboBoxColumn13.DataType = typeof(TipoGrupo);
            gridViewComboBoxColumn13.DisplayMember = "Description";
            gridViewComboBoxColumn13.FieldName = "IdTipoGrupo";
            gridViewComboBoxColumn13.HeaderText = "IdTipoGrupo";
            gridViewComboBoxColumn13.IsAutoGenerated = true;
            gridViewComboBoxColumn13.Name = "IdTipoGrupo";
            gridViewComboBoxColumn13.ValueMember = "Value";
            this.enumBinder13.Target = gridViewComboBoxColumn13;
            // 
            // enumBinder14
            // 
            this.enumBinder14.Source = typeof(ParamUnidad);
            gridViewComboBoxColumn14.DataSource = this.enumBinder14;
            gridViewComboBoxColumn14.DataType = typeof(ParamUnidad);
            gridViewComboBoxColumn14.DisplayMember = "Description";
            gridViewComboBoxColumn14.FieldName = "IdUnidad";
            gridViewComboBoxColumn14.HeaderText = "IdUnidad";
            gridViewComboBoxColumn14.IsAutoGenerated = true;
            gridViewComboBoxColumn14.Name = "IdUnidad";
            gridViewComboBoxColumn14.ValueMember = "Value";
            this.enumBinder14.Target = gridViewComboBoxColumn14;
            // 
            // ResultsParamView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radGridView1);
            this.Name = "ResultsParamView";
            this.Size = new System.Drawing.Size(913, 334);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView radGridView1;
        private System.Windows.Forms.BindingSource bsList;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder1;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder2;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder3;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder4;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder5;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder6;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder7;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder8;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder9;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder10;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder11;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder12;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder13;
        private Telerik.WinControls.UI.Data.EnumBinder enumBinder14;
    }
}
