﻿using Circutor.Tests.MyCircutor.Client;
using Dezac.Tests;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Circutor.Tests.MyCircutor
{
    [TestVersion(1.0)]
    public class LVCBTest : TestBase
    {
        private string userName;
        private string password;

        private int modemPort;
        private X3GModem modem;

        private string token;
        private DateTime? registerDate;

        public LVCBTest()
        {
            ServicePointManager.ServerCertificateValidationCallback = (s, c, ch, p) => true;
        }

        public void TestInit(int modemPort)
        {
            this.modemPort = modemPort;
        }

        public void TestFinish()
        {
            modem?.Dispose();
            modem = null;
        }

        public void InitProduct()
        {
            ItemsInputView view = null;

            if (Shell.ShowDialog("Producto", () =>
            {
                view = new ItemsInputView();
                return view;
            }, MessageBoxButtons.OKCancel) != DialogResult.OK)
                throw new Exception("Test cancelado por el usuario!");

            Resultado.Set("ITEM_CODE", view.ItemCode, Dezac.Tests.Model.ParamUnidad.SinUnidad);
            Resultado.Set("STAGES", view.Stages.ToString(), Dezac.Tests.Model.ParamUnidad.SinUnidad);
            Resultado.Set("REGULATOR_ITEM_CODE", view.RegulatorItemCode, Dezac.Tests.Model.ParamUnidad.SinUnidad);
        }

        public void InputSerialNumber()
        {
            var sn = Shell.ShowInputKeyboard<string>("Nº de Serie", "Lee el nº de serie de la batería", null);

            Assert.IsNotNull(sn, Error().UUT.NUMERO_DE_SERIE.CANCELADO_POR_USUARIO("Lectura del nº de serie cancelada por el usuario!"));

            Resultado.Set("SERIAL_NUMBER", sn, Dezac.Tests.Model.ParamUnidad.SinUnidad);
        }

        public void InputRegulatorSerialNumber()
        {
            var sn = Shell.ShowInputKeyboard<string>("Nº de Serie", "Lee el nº de serie del regulador", null);

            Assert.IsNotNull(sn, Error().UUT.NUMERO_DE_SERIE.CANCELADO_POR_USUARIO("Lectura del nº de serie del regulador cancelada por el usuario!"));

            Resultado.Set("REGULATOR_SERIAL_NUMBER", sn, Dezac.Tests.Model.ParamUnidad.SinUnidad);
        }

        public void InputSIM()
        {
            var sn = Shell.ShowInputKeyboard<string>("SIM", "Lee el SIM de la tarjeta", null);

            Assert.IsNotNull(sn, Error().UUT.SIM.CANCELADO_POR_USUARIO("Lectura del SIM cancelada por el usuario!"));

            Resultado.Set("SIM", sn, Dezac.Tests.Model.ParamUnidad.SinUnidad);
        }

        public void InputIMEI()
        {
            var sn = Shell.ShowInputKeyboard<string>("IMEI", "Lee el IMEI del modem", null);

            Assert.IsNotNull(sn, Error().UUT.COMUNICACIONES.CANCELADO_POR_USUARIO("Lectura del IMEI cancelada por el usuario!"));

            Resultado.Set("IMEI", sn, Dezac.Tests.Model.ParamUnidad.SinUnidad);
        }

        public string Login(string userName, string password)
        {
            this.userName = userName;
            this.password = password;

            var client = new AuthClient();

            var result = client.LoginAsync(new Credentials
            {
                Username = userName,
                Password = password
            }).Result;

            if (result == null)
                throw new Exception("Invalid login!");

            token = result.Token;

            Resultado.Set("AUTH_TOKEN", token, Dezac.Tests.Model.ParamUnidad.SinUnidad);

            return token;
        }

        public void DeleteEntities()
        {
            var sn = Resultado.GetString("SERIAL_NUMBER", null, false);
            if (string.IsNullOrEmpty(sn))
                return;

            var rsn = Resultado.GetString("REGULATOR_SERIAL_NUMBER", null, false);
            var sim = Resultado.GetString("SIM", null, false);
            var imei = Resultado.GetString("IMEI", null, false);

            var client = new LvcbClient();
            client.Token = token;
            
            client.EntitiesAsync(sn, rsn, sim, imei).Wait();
        }

        public bool DeleteLVCB(string registrationCode)
        {
            var client = new DeleteClient();
            client.Token = token;

            client.LvcbAsync(registrationCode).Wait();

            return true;
        }

        public void CheckIfExists()
        {
            var sn = Resultado.GetString("SERIAL_NUMBER", null, false);
            var rsn = Resultado.GetString("REGULATOR_SERIAL_NUMBER", null, false);
            var sim = Resultado.GetString("SIM", null, false);
            var imei = Resultado.GetString("IMEI", null, false);

            var client = new LvcbClient();
            client.Token = token;

            var result = client.StatusAsync(sn, rsn, sim, imei).Result;

            if (!string.IsNullOrEmpty(result.RegistrationCode))
            {
                if (Shell.MsgBox("Este equipo ya se ha registrado!.¿Desea desregistrarlo y volverlo a registrar?", "Atención", MessageBoxButtons.YesNo) == DialogResult.No)
                    throw new Exception("Equipo ya registrado!");

                DeleteLVCB(result.RegistrationCode);
            }
            else if (result.LvcbRegistrationCodeRelationStatus || result.LvcbRegulatorRelationStatus ||
                result.LvcbStatus || result.LvcbXGModemRelationStatus || result.MnoSimRelationStatus ||
                result.RegulatorStatus || result.SimStatus || result.XGModemSimRelationStatus ||
                result.XGModemStatus)
            {
                if (Shell.MsgBox("Algunos componentes del equipo ya se ha registrado!.¿Desea desregistrarlos y volverlos a registrar?", "Atención", MessageBoxButtons.YesNo) == DialogResult.No)
                    throw new Exception("Componentes ya registrados!");

                DeleteEntities();
            }
        }

        public string RegisterLVCB()
        {
            registerDate = DateTime.Now;

            var client = new RegisterClient();
            client.Token = token;

            var item = new LvcbItem
            {
                Lvcb = new Lvcb
                {
                    ItemCode = Resultado.GetString("ITEM_CODE", null, false),
                    Sn = Resultado.GetString("SERIAL_NUMBER", null, false),
                    Stages = Convert.ToInt32(Resultado.GetString("STAGES", null, false))
                },
                Regulator = new Regulator
                {
                    ItemCode = Resultado.GetString("REGULATOR_ITEM_CODE", null, false),
                    Sn = Resultado.GetString("REGULATOR_SERIAL_NUMBER", null, false)
                },
                Sim = new Sim
                {
                    Iccid = Resultado.GetString("SIM", null, false)
                },
                XGModem = new XGModem
                {
                    Imei = Resultado.GetString("IMEI", null, false),
                    Model = XGModemModel.MTX3GJAVAT
                }
            };

            Logger.Info(item.ToJson());

            var result = client.LvcbAsync(item).Result;

            Resultado.Set("REGISTER_CODE", result.RegistrationCode, Dezac.Tests.Model.ParamUnidad.SinUnidad);

            return result.RegistrationCode;
        }

        public string GetModemConfiguration()
        {
            return GetModemConfiguration(Resultado.GetString("SERIAL_NUMBER", null, false));
        }

        public string GetModemConfiguration(string serialNumber)
        {
            var client = new XgmodemClient();
            client.Token = token;

            var result = client.ConfAsync(serialNumber).Result;

            result = result.Replace("\n", "\r\n");

            Resultado.Set("MODEM_CONFIG", result, Dezac.Tests.Model.ParamUnidad.SinUnidad);

            return result;
        }

        public void UpdateModemConfiguration()
        {
            UpdateModemConfiguration(Resultado.GetString("SERIAL_NUMBER", null, false), Resultado.GetString("MODEM_CONFIG", null, false));
        }

        public void UpdateModemConfiguration(string serialNumber, string conf)
        {
            Assert.IsNotNullOrEmpty(serialNumber, Error().UUT.NUMERO_DE_SERIE.NO_GRABADO("Falta el nº de serie!"));
            Assert.IsNotNullOrEmpty(conf, Error().UUT.CONFIGURACION.NO_GRABADO("Falta la configuración del modem!"));

            var fileName = Path.Combine(Path.GetTempPath(), $"{serialNumber}_config.txt");

            File.WriteAllText(fileName, conf);

            var match = new Regex("MTX_PIN: (\\d{4})").Match(conf);
            if (!match.Success)
                throw new Exception("Imposible encontrar el PIN");

            var pin = match.Groups[1].Value;

            Logger.InfoFormat("PIN {0}", pin);

            using (var modem = new X3GModem(modemPort))
            {
                Logger.InfoFormat("Copy modem confing file from {0}", fileName);

                modem.CopyFile(fileName, "config.txt");

                if (modem.IsLocked())
                    modem.Unlock(pin);

                modem.Lock(pin);
                modem.Reset();
            }
        }

        public XgModemStatusRecord GetModemStatus(double fromMinutesAgo)
        {
            var serialNumber = Resultado.GetString("SERIAL_NUMBER", null, false);

            XgModemStatusRecord result = null;

            var start = registerDate.GetValueOrDefault(DateTime.Now.AddMinutes(-5));

            SamplerWithCancel((p) =>
            {
                result = GetModemStatusFromInterval(serialNumber, start);

                return true;
            }, "Error, no se puede obtener el estado del modem!", 3, 2000, 0, false);

            return result;
        }

        private XgModemStatusRecord GetModemStatusFromInterval(string serialNumber, DateTime fromDate)
        {
            var client = new XgmodemClient();
            client.Token = token;

            var startTS = fromDate.ToUniversalTime().ToUnixTime().ToString();
            var stopTS = "9999999999999";

            var result = client.StatusAsync(serialNumber, startTS, stopTS).Result;

            Resultado.Set("MODEM_STATUS_IP", result.Ip, Dezac.Tests.Model.ParamUnidad.SinUnidad);
            Resultado.Set("MODEM_STATUS_CSQ", result.Csq, Dezac.Tests.Model.ParamUnidad.SinUnidad);
            Resultado.Set("MODEM_STATUS_LAST_UPDATE_TIME", result.LastUpdateTime, Dezac.Tests.Model.ParamUnidad.SinUnidad);

            return result;
        }
    }
}
