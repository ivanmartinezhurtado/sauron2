namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTSEQUENCEFILEVERSION")]
    public partial class T_TESTSEQUENCEFILEVERSION
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTSFV { get; set; }

        public int NUMTSF { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(15)]
        public string USUARIOALTA { get; set; }

        public DateTime? FECHAVALIDACION { get; set; }

        [StringLength(15)]
        public string USUARIOVALIDACION { get; set; }

        public byte[] FICHERO { get; set; }

        [StringLength(512)]
        public string DESCRIPCION { get; set; }

        public virtual T_TESTSEQUENCEFILE T_TESTSEQUENCEFILE { get; set; }
    }
}
