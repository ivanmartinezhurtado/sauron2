﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace TaskRunner.Tools
{
    public static class VariablesResolver
    {
        public const string REGEX_ENUMERABLE_INDEX = @"\[[0-9]{1,}\]";
        public const string REGEX_FORBIDDEN_CHARACTERS = @"[^a-zA-Z0-9_\.]";
        public const string REGEX_VARIABLE_INDEX = @"\[\$[a-zA-z]{1}[a-zA-Z0-9.]{0,}\]";        

        public static object GetPropertyValue(object value, string name)
        {
            if (value == null)
                return null;

            Type type = value.GetType();
            PropertyInfo prop = type.GetProperty(name);
            if (prop != null)
                return prop.GetValue(value);

            if (value is ValueType)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                    return field.GetValue(value);
            }

            return null;
        }
        public static object GetEnumerableElement(object value, string key)
        {
            if (value == null)
                return null;

            if (Regex.IsMatch(key, REGEX_ENUMERABLE_INDEX))
            {
                var matches = Regex.Matches(key, REGEX_ENUMERABLE_INDEX);

                string[] indexs = new string[matches.Count];
                int[] indexInts = new int[matches.Count];

                List<object> enumerable = value as List<object>;
                Array array = value as Array;

                for (int i = 0; i < matches.Count; i++)
                {
                    indexs[i] = matches[i].Value.Replace("[", "").Replace("]", "");
                    if (string.IsNullOrEmpty(indexs[i]) || !Int32.TryParse(indexs[i], out indexInts[i]))
                        throw new Exception($"Indice {indexs[i]} definido de forma incorrecta en la variable {key}");

                    if (enumerable != null)
                        try
                        {
                            enumerable = value as List<object>;
                            value = enumerable[indexInts[i]];                           
                        }
                        catch
                        {
                            throw new Exception($"La variable {key} no contiene el indice {indexInts[i]}");
                        }
                    else if (array != null)
                        try
                        {
                            array = value as Array;
                            value = array.GetValue(indexInts[i]);
                        }
                        catch
                        {
                            throw new Exception($"La variable {key} no contiene el indice {indexInts[i]}");
                        }
                    else
                        return null;
                }
            }
            return value;
        }

        public static void VaribleKeyValidation(ref string key)
        {
            if (key.StartsWith("$"))
                key.Replace("$", "");
            string[] forbiddenCharacters = Regex.Matches(key, REGEX_FORBIDDEN_CHARACTERS)
                .Cast<Match>()
                .Select(m => m.Value)
                .ToArray();
            if (forbiddenCharacters.Length > 0)
                throw new Exception($"La variable {key} contiene simbolos prohibidos:\" {string.Join(" \" , \" ", forbiddenCharacters)} \"");
        }

        public static void SetVariableProperty(object instance, string key, object value)
        {
            string[] tokens = key.Split('.');
            List<object> aux = new List<object>();
            aux.Add(instance);
            for (int i = 1; i < tokens.Length - 1; i++)
                if (aux.Last().GetType().GetField(tokens[i]) != null)
                    aux.Add(aux.Last().GetType().GetField(tokens[i]).GetValue(aux.Last()));
                else if (aux.Last().GetType().GetProperty(tokens[i]) != null)
                    aux.Add(aux.Last().GetType().GetProperty(tokens[i]).GetValue(aux.Last()));
                else
                    throw new Exception($"La variable {key} parece referirse a una propiedad pero dicha propiedad no existe en la variable {tokens[0]}.");
            aux.Last().GetType().GetField(tokens[tokens.Length - 1])?
                .SetValue(aux.Last(), RunnerHelper.ChangeType(value, aux.Last().GetType().GetField(tokens[tokens.Length - 1])?.FieldType));
            aux.Last().GetType().GetProperty(tokens[tokens.Length - 1])?
                .SetValue(aux.Last(), RunnerHelper.ChangeType(value, aux.Last().GetType().GetProperty(tokens[tokens.Length - 1])?.PropertyType));
            for (int i = tokens.Length - 2; i > 0; i--)
                if (aux[i - 1].GetType().GetField(tokens[i]) != null)
                    aux[i - 1].GetType().GetField(tokens[i]).SetValue(aux[i - 1], aux[i]);
                else if (aux[i - 1].GetType().GetProperty(tokens[i]) != null)
                    aux[i - 1].GetType().GetProperty(tokens[i]).SetValue(aux[i - 1], aux[i]);
                else
                    throw new Exception($"La variable {key} parece referirse a una propiedad pero dicha propiedad no existe en la variable {tokens[0]}.");
        }
        
        public static string ReplaceForbiddenCharacters(string key)
        {
            return Regex.Replace(key, REGEX_FORBIDDEN_CHARACTERS, "_");
        }        

    }
}
