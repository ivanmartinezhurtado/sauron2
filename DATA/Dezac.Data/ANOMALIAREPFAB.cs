namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ANOMALIAREPFAB")]
    public partial class ANOMALIAREPFAB
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ANOMALIAREPFAB()
        {
            RF_ANOMALIA = new HashSet<RF_ANOMALIA>();
        }

        [Key]
        [StringLength(10)]
        public string IDANOMALIA { get; set; }

        [Required]
        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [Required]
        [StringLength(1)]
        public string POSTVENTASINO { get; set; }

        [Required]
        [StringLength(1)]
        public string FABRICACIONSINO { get; set; }

        [Required]
        [StringLength(1)]
        public string ACTUACIONSINO { get; set; }

        [Required]
        [StringLength(1)]
        public string COMPONENTESINO { get; set; }

        [StringLength(10)]
        public string IDACTUACION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RF_ANOMALIA> RF_ANOMALIA { get; set; }
    }
}
