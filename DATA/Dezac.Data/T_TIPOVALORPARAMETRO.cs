namespace Dezac.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.T_TIPOVALORPARAMETRO")]
    public partial class T_TIPOVALORPARAMETRO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_TIPOVALORPARAMETRO()
        {
            T_PARAMETRO = new HashSet<T_PARAMETRO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTIPOVALOR { get; set; }

        [Required]
        [StringLength(20)]
        public string TIPOVALOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_PARAMETRO> T_PARAMETRO { get; set; }
    }
}
