﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(PackageCheckDescription))]
    public class PackageCheck : ActionBase
    {
        [Description("Nombre para leer el código de barras y que retorne si este es correcto")]
        [Category("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Si está en true, si el código de barras es incorrecto el test parará. Si está en false el test seguirá adelante aunque el código de barras sea erróneo")]
        [Category("3. Configuration")]
        [DefaultValue(true)]
        public bool cancelButton { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            testBase.PackageCheck(DataInput.Key, cancelButton);
        }     
    }

    public class PackageCheckDescription : ActionDescription
    {
        public override string Name { get { return "PackageCheck"; } }
        public override string Description { get { return "Este action sirve para hacer una comprobación del embalaje individual"; } }
        public override string Category { get { return "DezacTest/Validation"; } }
        public override Image Icon { get { return Properties.Resources.Validation; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
