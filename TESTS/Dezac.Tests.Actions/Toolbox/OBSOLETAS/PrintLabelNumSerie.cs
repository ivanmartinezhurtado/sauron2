﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Labels;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(PrintLabelNumSerieActionDescription))]
    [Obsolete("Usar la nueva action de impresión de etiquetas Print Labels Phase")]
    
    public class PrintLabelNumSerieAction : ActionBase
    {
        public enum typePrinter
        {
            Printer_Labels,
            Printer_Reports
        }
       
        [DefaultValue(false)]
        public bool BarTenderVisible { get; set; }

        [DefaultValue("112787")] 
        public string BarTenderLabel { get; set; }

        public string PathBarTenderLabels { get; set; }

        [DefaultValue(TestInfo.labels.Numero_Serie)]
        public TestInfo.labels TypeLabel { get; set; }

        [DefaultValue(typePrinter.Printer_Labels)]
        public typePrinter TypePrinter { get; set; }

        [DefaultValue(1)]
        public int NumberCopies { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;

            if (context.ExecutionResult != TestExecutionResult.Completed)
                return;

            var cacheSvc = context.Services.Get<ICacheService>();
            var useNewPrintLabel = cacheSvc.Get<PrintLabelsFaseAction>("PrintLabelsFaseAction");
            if (useNewPrintLabel == null && cacheSvc.Get<string>("UseNewPrintLabelAction") == null)
            {
                var newPrint = new PrintLabelsFaseAction { Template = LabelTamplete.LabelTemplate.Caracteristicas };
                if (newPrint.HasLabel())
                    useNewPrintLabel = newPrint;
                else
                {
                    newPrint.Template = LabelTamplete.LabelTemplate.NumSerie;
                    if (newPrint.HasLabel())
                        useNewPrintLabel = newPrint;
                }

                cacheSvc.Add("UseNewPrintLabelAction", useNewPrintLabel != null ? "1" : "0");
                cacheSvc.Add("PrintLabelsFaseAction", useNewPrintLabel);
            }

            if (useNewPrintLabel != null)
            {
                useNewPrintLabel.Execute();
                return;
            }

            var data = context.Services.Get<ITestContext>();
            data.TestInfo.TypeLabel = TypeLabel;

            string label;

            switch(TypeLabel)
            {
                case TestInfo.labels.Numero_Serie:
                    label=ConstantsParameters.LabelTest.LABEL_SERIAL_NUMBER;
                    break;
                case TestInfo.labels.Advertencia:
                    label=ConstantsParameters.LabelTest.LABEL_WARNING;
                    break;
                case TestInfo.labels.Embalaje_Individual:
                    label = ConstantsParameters.LabelTest.LABEL_SINGLE_BOX;
                    break;
                case TestInfo.labels.Normativa_Seguridad:
                    label = ConstantsParameters.LabelTest.LABEL_SECURITY_NORMATIVE;
                    break;
                case TestInfo.labels.Test:
                    label = ConstantsParameters.LabelTest.LABEL_TEST;
                    break;
                case TestInfo.labels.Otros:
                    label = ConstantsParameters.LabelTest.LABEL_OTHERS;
                    break;
                case TestInfo.labels.Numero_Serie_Subconjunto:
                    label = ConstantsParameters.LabelTest.LABEL_SERIAL_NUMBER_SUBCONJUNTO;
                    break;
                default:
                    throw new Exception("Tipo de etiqueta no implementada");
            }

            if (data != null)
            {
                BarTenderLabel = data.Identificacion.GetString(label, BarTenderLabel, ParamUnidad.SinUnidad);
                NumberCopies =(int)data.Identificacion.GetDouble(ConstantsParameters.LabelTest.LABEL_NUMBER_COPIES, NumberCopies, ParamUnidad.SinUnidad);

                if (data != null && data.TestInfo != null)
                    data.TestInfo.LabelTest = BarTenderLabel;
            }

            if (string.IsNullOrEmpty(PathBarTenderLabels))
                PathBarTenderLabels = ConfigurationManager.AppSettings["PathBarTenderLabels"];

            data.TestInfo.IdenticalCopiesOfLabel = NumberCopies;

            using (var btr = new BarTenderReport())
            {
                btr.LabelFileName = Path.Combine(PathBarTenderLabels, BarTenderLabel + ".btw");

                if (TypePrinter == typePrinter.Printer_Labels)
                {
                    if (!string.IsNullOrEmpty(data.TestInfo.PrinterLabel))
                        btr.PrinterName = data.TestInfo.PrinterLabel;

                    if (string.IsNullOrEmpty(btr.PrinterName))
                        btr.PrinterName = PrinterTools.SelectPrinterByPatternOrDefault("LABEL");
                }
                else
                {
                    if (!string.IsNullOrEmpty(data.TestInfo.PrinterReport))
                        btr.PrinterName = data.TestInfo.PrinterReport;

                    if (string.IsNullOrEmpty(btr.PrinterName))
                        btr.PrinterName = PrinterTools.SelectPrinterByPatternOrDefault("REPORT");
                }

                btr.IdenticalCopiesOfLabel = NumberCopies;
                btr.Visible = BarTenderVisible;
                btr.PrintTest(data.TestInfo);
            }
        }
    }

    public class PrintLabelNumSerieActionDescription : ActionDescription
    {
        public override string Name { get { return "PrintLabel"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
