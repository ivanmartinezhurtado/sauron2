namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ARTICULOBLOQUEO")]
    public partial class ARTICULOBLOQUEO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMARTICULO { get; set; }

        public DateTime FECHA { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string PROCESO { get; set; }

        [Required]
        [StringLength(30)]
        public string USERNAME { get; set; }

        [Required]
        [StringLength(400)]
        public string MOTIVO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDMOTIVO { get; set; }

        public int? VERSIONPRODUCTO { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }
    }
}
