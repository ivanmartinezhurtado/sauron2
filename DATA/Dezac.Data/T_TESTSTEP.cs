namespace Dezac.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.T_TESTSTEP")]
    public partial class T_TESTSTEP
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTESTFASE { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string STEPNAME { get; set; }

        public DateTime TIMESTART { get; set; }

        public DateTime TIMEEND { get; set; }

        [Required]
        [StringLength(1)]
        public string RESULT { get; set; }

        [StringLength(500)]
        public string EXCEPTION { get; set; }

        [StringLength(100)]
        public string GROUPNAME { get; set; }

        public int? DURACION { get; set; }

        public virtual T_TESTFASE T_TESTFASE { get; set; }
    }
}
