﻿using Dezac.Tests.Services;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(TowerInitializationActionDescription))]

    public class TowerInitialization : ActionBase
    {
        [Description("El tipo de torre a usar en el test")]
        [DefaultValue(TypeTower.Tower3)]
        public TypeTower TowerType { get; set; }

        public override void Execute()
        {
            ITower tower = null;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");
            //      throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var cacheSvc = testBase.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                switch (TowerType)
                {
                    case TypeTower.Tower3:
                        tower = new Tower3();
                        break;
                    case TypeTower.Tower1:
                        tower = new Tower1();
                        break;
                    case TypeTower.TowerBoard:
                        tower = new TowerBoard();
                        break;
                    default:
                        break;
                }

                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
        }

        public enum TypeTower
        {
            Tower3,
            Tower1,
            TowerBoard
        }
    }

    public class TowerInitializationActionDescription : ActionDescription
    {
        public override string Name { get { return "InitializeTower"; } }
        public override string Description { get { return "Inicializa una instancia de la torre seleccionada"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.chess_tower_icon; } }
        public override string Dependencies { get { return "TowerDispose:2"; } }
    }
}
