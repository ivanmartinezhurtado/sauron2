﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.Kernel 
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(AddToResultsActionDescription))]
    public class AddToResultsAction : ActionBase
    {
        [Category("3. Results")]
        [Description("Este Parámetro sirve para añadir cualquier tipo de dato en los resultados del test, se guarda el nombre del dato con su valor y su unidad")]
        public TextValueUnit[] Results { get; set; }

        public override void Execute()
        {
            var testContext = SequenceContext.Current.Services.Get<ITestContext>();
            var context = SequenceContext.Current;

            foreach (TextValueUnit result in Results)
            {
                var value = VariablesTools.ResolveValue(result.Value);
                Logger.InfoFormat("Add Result Key:{0}  --> Value {1}", result.Key, result.Value);

                if (testContext != null)
                    testContext.Resultados.Set(result.Key, value.ToString(), result.Unit);
                else
                    context.ResultList.Add(string.Format("{0} = {1} ({2})", result.Key, value, result.Unit));
            }
        }        
    }

    public class TextValueUnit
    {
        [Category("1. Nombre de la Variable")]
        [Description("Nombre de la variable de resultado a crear en el contexto del test")]
        public string Key { get; set; }

        [Category("2. Valor de la Variable")]
        [Description("Valor de la variable creada con el nombre asignado en la porpiedad Key")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]

        public string Value { get; set; }

        [Category("3. Unidad de la Variable")]
        [Description("Unidad en la que estará la Key")]
        public ParamUnidad Unit { get; set; }           
    }

    public class AddToResultsActionDescription : ActionDescription
    {
        public override string Name { get { return "AddToResults"; } }
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override string Description { get { return "Este action sirve para añadir cualquier tipo de resultado al test."; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
