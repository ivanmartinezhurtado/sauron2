﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(PLE200Description))]
    public partial class UC_PLE200 : UserControl, IDisposable
    {
        private static readonly string TAG = "PLE200";

        private TriLineValue Voltage;
        private TriLineValue Current;
        private TriLineValue Desfase;
        private TriLineValue PF;
        private double freq;

        private bool timerEnabled;
        private bool modeRead;

        private SynchronizationContext sc;
        private PLE200 ple200;
      
        public UC_PLE200()
        {
            InitializeComponent();
        }

        public Type InstrumentType
        {
            get
            {
                return typeof(PLE200);
            }
        }


        public PLE200 Instrument
        {
            get
            {
                if (ple200 == null)
                    ple200 = new PLE200();

                return ple200;
            }
            set
            {
                if (ple200 != null)
                {
                    ple200.Dispose();
                    ple200 = null;
                }

                ple200 = value;
            }
        }
     
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;
            try
            {
                var ports = new MyComputer().SerialPortNames.ToArray();
                if (ports.Length== 0)
                {
                    errorProvider1.SetError(cmb_Port, "No se ha encontrado puertos en el PC");
                    return;
                }

                cmb_Port.Items.AddRange(ports);
                cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM1");
                if (cmb_Port.SelectedItem == null)
                    cmb_Port.SelectedIndex = 0;

                SetMode(false);
            }
            catch (Exception)
            {
                errorProvider1.SetError(cmb_Port, "No se ha encontrado la PLE200");
            }

        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();

                if (cmb_Port.SelectedItem == null)
                    errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
                else
                 {
                    Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                    if (ValidatePresets())
                    {
                        ToggleButtonsState((Button)sender);

                        try
                        {
                            var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyPresetsAndWaitStabilisation(Voltage, Current, freq, PF, Desfase, true); }));

                            LifeMethod("Encendiendo", task);                        
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("Error al encender el PLE200", ex);
                        }
                        finally
                        {
                            ToggleButtonsState((Button)sender);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se ha encontrado la Fuente PLE200", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.Error("Error al inicializar la PLE200", ex);
            }
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();
                if (cmb_Port.SelectedItem == null)
                    errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
                else
                {
                    Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                    ToggleButtonsState((Button)sender);
                    var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyOffAndWaitStabilisation(); }));
                    LifeMethod("Apagando", task);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error al apagar el PLE200", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ToggleButtonsState((Button)sender);

                Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM",""));

                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.Reset(); }));

                LifeMethod("Reseteando", task);
            }
            catch (Exception ex)
            {
                Logger.Error("Error al resetear el PLE200", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        private bool ValidatePresets()
        {
            try
            {
                if (!ValidateVoltageFreq()) return false;
                if (!ValidateCurrent()) return false;
                if (!ValidateDesfase()) return false;
            }
            catch (Exception ex)
            {
                Logger.Error("Error comunicando con la PLE200", ex);
            }       
           return true;
        }

        private bool ValidateVoltageFreq()
        {
            Voltage = new TriLineValue { L1 = Convert.ToDouble(txtV1.Text.Replace('.', ',')), L2 = Convert.ToDouble(txtV2.Text.Replace('.', ',')), L3 = Convert.ToDouble(txtV3.Text.Replace('.', ',')) };

            if (Voltage.L1 > 400)
            {
                errorProvider1.SetError(txtV1, "Voltage must lower 400V");
                return false;
            }

            if (Voltage.L2 > 400)
            {
                errorProvider1.SetError(txtV2, "Voltage must lower 400V");
                return false;
            }

            if (Voltage.L3 > 400)
            {
                errorProvider1.SetError(txtV3, "Voltage must lower 400V");
                return false;
            }

            freq = Convert.ToDouble(txtFreq.Text);
            if (freq > 100)
            {
                errorProvider1.SetError(txtFreq, "Frequency must be lower 100Hz");
                return false;
            }

            return true;
        }

        private bool ValidateCurrent()
        {
            Current = new TriLineValue { L1 = Convert.ToDouble(txtI1.Text.Replace('.', ',')), L2 = Convert.ToDouble(txtI2.Text.Replace('.', ',')), L3 = Convert.ToDouble(txtI3.Text.Replace('.', ',')) };

            if (Current.L1 > 120)
            {
                errorProvider1.SetError(txtI1, "Current must lower 120A");
                return false;
            }
            if (Current.L2 > 120)
            {
                errorProvider1.SetError(txtI2, "Current must lower 120A");
                return false;
            }
            if (Current.L3 > 120)
            {
                errorProvider1.SetError(txtI3, "Current must lower 120A");
                return false;
            }
            return true;
        }

        private bool ValidateDesfase()
        {
            Desfase = new TriLineValue { L1 = Convert.ToDouble(txtP1.Text.Replace('.', ',')), L2 = Convert.ToDouble(txtP2.Text.Replace('.', ',')), L3 = Convert.ToDouble(txtP3.Text.Replace('.', ',')) };
            if (Desfase.L1 > 360)
            {
                errorProvider1.SetError(txtP1, "Anlge must lower 360º");
                return false;
            }
            if (Desfase.L2 > 360)
            {
                errorProvider1.SetError(txtP2, "Anlge must lower360º");
                return false;
            }
            if (Desfase.L3 > 360)
            {
                errorProvider1.SetError(txtP3, "Anlge must lower 360º");
                return false;
            }
            var pf = Convert.ToDouble(txtPF.Text.Replace('.', ','));
            PF = new TriLineValue { L1 =pf , L2 = pf, L3 = pf };
            if (PF.L1 > 360)
            {
                errorProvider1.SetError(txtPF, "Power Factor must lower 360º");
                return false;
            }
            return true;
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        protected override void Dispose(bool disposing)
        {
            timerEnabled = false;
            if (disposing && (components != null))
                components.Dispose();

            if(Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }

        private void EnableTimer()
        {
            timerEnabled = true;
            Task.Factory.StartNew(OnGraphTask);
        }

        private void OnGraphTask()
        {
            while (timerEnabled)
            {
                try
                {
                    UpdateGraphs();
                }
                catch { }
                Thread.Sleep(1000);
            }
        }

        private void UpdateGraphs()
        {
            TriLineValue current = TriLineValue.Zero;
            TriLineValue voltage = TriLineValue.Zero;
            TriLineValue angle = TriLineValue.Zero;
            TriLineValue pf = TriLineValue.Zero;

            if (modeRead)
            {
                current = Instrument.ReadCurrent();
                voltage = Instrument.ReadVoltage();
                angle = Instrument.ReadDesfase();
                pf = Instrument.ReadPowerFactor();
            }

            sc.Post(_ =>
            {
                if (current != TriLineValue.Zero)
                {
                    DataPointHub.Add(this, "I1", current.L1, TAG);
                    DataPointHub.Add(this, "I2", current.L2, TAG);
                    DataPointHub.Add(this, "I3", current.L3, TAG);
                }

                if (voltage != TriLineValue.Zero)
                {
                    DataPointHub.Add(this, "V1", voltage.L1, TAG);
                    DataPointHub.Add(this, "V2", voltage.L2, TAG);
                    DataPointHub.Add(this, "V3", voltage.L3, TAG);
                }

                if (angle != TriLineValue.Zero)
                {
                    DataPointHub.Add(this, "A1", angle.L1, TAG);
                    DataPointHub.Add(this, "A2", angle.L2, TAG);
                    DataPointHub.Add(this, "A3", angle.L3, TAG);
                }

                if (pf != TriLineValue.Zero)
                    DataPointHub.Add(this, "PF", pf.L1, TAG);               

            }, null);
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            SetMode(false);
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = modeRead ? Color.DimGray : Color.LightGreen;
            SetMode(!modeRead);
        }

        private void SetMode(bool read)
        {
            modeRead = read;

            Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));

            if (modeRead && !timerEnabled)
                EnableTimer();
            else 
                timerEnabled = modeRead;
        }
    }

    public class PLE200Description : ControlDescription
    {
        public override string Name { get { return "PLE200"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.PowerSource; } }
        public override Image IconBig { get { return Properties.Resources.PowerSourceACDC; } }
        public override Type SubcomponentType { get { return typeof(global::Instruments.PowerSource.PLE200); } }
    }
}
