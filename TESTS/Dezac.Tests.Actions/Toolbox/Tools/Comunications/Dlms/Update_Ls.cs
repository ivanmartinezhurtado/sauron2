﻿using Dezac.DLMS;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{


    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(Update_Ls_Description))]
    public class Update_Ls : ActionBase
    {

        [Category("3. Configuration")]
        [Description("Fichero del proyecto a grabar")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string BinaryPath { get; set; } 

        [Category("3. Configuration")]
        [Description("Multiplos del valor introducido por el usuario de bloques a no enviar al DUT")]
        [DefaultValue(0)]
        public int BlocksToJump { get; set; }




        public override void Execute()
        {
            string path1 ;            
            path1 = BinaryPath;
            var device = GetVariable("DlmsDevice") as DLMSDevice;//Obtener todas las caracteristicas de un DLMSDevice
                                                                 //y guardarlas en la variable device.
            
            
            device.Update(path1);      

            
        }
    }

    public class Update_Ls_Description : ActionDescription
    {
        public override string Name { get { return "UpgradeFirmwareDlms"; } }
        public override string Description { get { return "Actualización firmware protocolo DLMS"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
    }
}
