﻿using Dezac.Core.Exceptions;
using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(GetNumSerieSubconjuntoDescription))]
    
    public class GetNumSerieSubconjunto : ActionBase
    {
        [Description("Code number prodcut to get serial number from database")]
        [Category("Configuration")]
        public int? NumProduct { get; set; }

        [Description("Version number prodcut to get serial number from database")]
        [Category("Configuration")]
        public int? Version { get; set; }

        [Description("Flag to get number prodcut from database in parameter NUM_SUBCONJUNTO")]
        [Category("Configuration")]
        [DefaultValue(false)]
        public bool InDDBB { get; set; }

        [Description("Flag to check and Add this product in main structure")]
        [Category("Configuration")]
        [DefaultValue(false)]
        public bool NoControlStructure { get; set; }

        [Description("Descripción del nombre de la variable")]
        [Category("Resultado")]
        [DefaultValue("NS_SUBCONJUNTO")]
        public string VariableName { get; set; }

        public override void Execute()
        {
            int producto = 0;
            int version = 1;
            string numSerie = null;

            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            Dictionary<string, string> NumSerieSubconjunto = new Dictionary<string, string>();

            if (data != null)
            {
                if (InDDBB)
                {
                    producto = (int)data.Identificacion.GetDouble(string.Format("{0}", ConstantsParameters.Identification.NUM_SUBCONJUNTO), 0, ParamUnidad.SinUnidad);
                    if (producto == 0)
                        TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("En Identification NUM_SUBCONJUNTO").Throw();

                    version = (int)data.Identificacion.GetDouble(string.Format("{0}", ConstantsParameters.Identification.VER_SUBCONJUNTO), 1, ParamUnidad.SinUnidad);
                    if (version == 0)
                        TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("En Identification VER_SUBCONJUNTO").Throw();
                }
                else
                {
                    if (NumProduct.HasValue)
                        producto = NumProduct.Value;
                    else
                        TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("En Action NumProducto").Throw();

                    if (Version.HasValue)
                        version = Version.Value;
                    else
                        TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("En Action Version").Throw();
                }
            }

            if (!NoControlStructure)
            {
                if (data.TestInfo.ListComponents == null)
                    TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("En data.TestInfo.ListComponents").Throw();

                foreach (ComponentsEstructureInProduct component in data.TestInfo.ListComponents)
                    if (component.numproducto == producto)
                    {
                        using (DezacService svc = new DezacService())
                        {
                            numSerie = svc.GetNumSerie(producto, version, 0);
                            if (numSerie.Trim() == "0")
                                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("En GetNumSerie al llamar a pp.NextSerialCode").Throw();
                        }

                        NumSerieSubconjunto.Add(component.descripcion, numSerie.Trim());
                    }
            }
            else
                using (DezacService svc = new DezacService())
                {
                    var descripcion = svc.GetProducto(producto, version).ARTICULO.DESCRIPCION;
                    numSerie = svc.GetNumSerie(producto, version, 0);

                    if (numSerie.Trim() == "0")
                        TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("En GetNumSerie al llamar a pp.NextSerialCode").Throw();

                    NumSerieSubconjunto.Add(descripcion, numSerie.Trim());
                }

            data.TestInfo.NumSerieSubconjunto = NumSerieSubconjunto;

            SetVariable(VariableName ?? "NS_SUBCOJUNTO", numSerie);
            Logger.InfoFormat("{0} -> {1}", VariableName ?? "NS_SUBCOJUNTO", numSerie);
            Context.ResultList.Add(string.Format("{0} -> {1}", VariableName ?? "NS_SUBCOJUNTO", numSerie));
        }
    }

    public class GetNumSerieSubconjuntoDescription : ActionDescription
    {
        public override string Description  { get { return "Action to get serial number from Product and Version that it is subStructure from main test prodcut";  } }
        public override string Name { get { return "GetNumSerieSubconjunto"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
        public override string Dependencies { get { return "InitTestBase;InitProductAction;InitBastidorMultipleAction"; } }
    }
}
