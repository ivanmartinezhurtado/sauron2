﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.UI.Design;

namespace TaskRunner.Model
{
    public class SequenceModel
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string ActionsVersion { get; set; }
        internal SequenceModel()
        {
            Root = new Step();
        }
        public static SequenceModel CreateNew()
        {
            SequenceModel model = new SequenceModel();

            model.Root.Steps.Add(new Step { Name = "Init" });
            model.Root.Steps.Add(new Step { Name = "Main" });
            model.Root.Steps.Add(new Step { Name = "End" });

            return model;
        }

        public Step Root { get; protected set; }
        [JsonIgnore]
        public Step Init
        {
            get
            {
                return Root.Steps[0];
            }
        }
        [JsonIgnore]
        public Step Main
        {
            get
            {
                return Root.Steps[1];
            }
        }
        [JsonIgnore]
        public Step End
        {
            get
            {
                return Root.Steps[2];
            }
        }

        public bool ExistStepName(string StepName)
        {
            bool AssigendNumSerieInit = Init.HasStepName(StepName);
            bool AssigendNumSerieMain = Main.HasStepName(StepName);
            bool AssigendNumSerieEnd = End.HasStepName(StepName);
            return AssigendNumSerieInit | AssigendNumSerieMain | AssigendNumSerieEnd;
        }

        public void Save(string fileName)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;

            string json = JsonConvert.SerializeObject(this, settings);

            Name = fileName;

            if (fileName.IndexOf(".") < 0)
                fileName += ".json";

            File.WriteAllText(fileName, json);
        }
        public void Save(Stream stream, Encoding encoding = null)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Formatting = Formatting.Indented;

            string json = JsonConvert.SerializeObject(this, settings);

            byte[] data = (encoding ?? UTF8Encoding.UTF8).GetBytes(json);
            stream.Write(data, 0, data.Length);
        }
        public static SequenceModel LoadFile(string fileName)
        {
            string json = File.ReadAllText(fileName);
            return Load(json);
        }
        public static SequenceModel Load(string content)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Formatting = Formatting.Indented;

            SequenceModel sequence = JsonConvert.DeserializeObject<SequenceModel>(content, settings);

            if (String.IsNullOrEmpty(sequence.ActionsVersion) || sequence.ActionsVersion == "0.0")
            {
                settings.ContractResolver = CustomContractResolverFrom0To1.Instance;
                sequence = JsonConvert.DeserializeObject<SequenceModel>(content, settings);
                sequence.ActionsVersion = "1.0";
            }

            return sequence;
        }

        public List<Step> GetDisabledSteps()
        {
            var disableSteps = new List<Step>();
            return InternalGetDisabledSteps(Root, disableSteps);
        }

        private List<Step> InternalGetDisabledSteps(Step step, List<Step> disableSteps)
        {
            if (!step.Enabled)
                disableSteps.Add(step);

            foreach (var childstep in step.Steps)
                InternalGetDisabledSteps(childstep, disableSteps);

            return disableSteps;
        }
    }
}
