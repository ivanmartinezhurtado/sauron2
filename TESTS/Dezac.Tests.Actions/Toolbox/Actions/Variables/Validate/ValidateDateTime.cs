﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System;
using System.ComponentModel;
using System.Drawing;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ValidateDateTimeDescription))]
    public class ValidateDateTime : ActionBase
    {
        //TODO: UNIFICAR en un action generico para fechas

        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInput { get; set; }

        [Category("3.1 Margins")]
        [Description("Máxima desviación de tiempo aceptada")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable MaximumClockDeviation { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Tipo de tiempo a evaluar (UTC, System)")]
        [DefaultValue(false)]
        public bool IsUtc { get; set; }


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            if (!DataInput.Trim().StartsWith("$"))
                DataInput = "$" + DataInput.Trim();

            object variable = VariablesTools.ResolveValue(DataInput, null, ParamUnidad.s);

            var name = DataInput.Trim().Replace("$", "");

            DateTime decimalVariable = (DateTime)variable;
            Logger.InfoFormat($"Variable: {name}, Date: {decimalVariable.ToShortDateString()}, Time: {decimalVariable.TimeOfDay}");
            if (IsUtc)
                Logger.InfoFormat($"UTC PC DateTime PC - Date: {DateTime.UtcNow.ToShortDateString()}  Time: {DateTime.UtcNow.TimeOfDay}");
            else
                Logger.InfoFormat($"PC DateTime - Date: {DateTime.Now.ToShortDateString()}  Time: {DateTime.Now.TimeOfDay}");

            double timeDiff = 0;

            if (IsUtc)
                timeDiff = (decimalVariable.Subtract(DateTime.UtcNow)).TotalSeconds;
            else
                timeDiff = (decimalVariable.Subtract(DateTime.Now)).TotalSeconds; 

            if (MaximumClockDeviation == null || MaximumClockDeviation.IsKeyNullOrEmpty)
            {
                testBase.Margenes.GetDouble(testBase.Params.TIME.Null.TestPoint("DEVIATION").modo("MAX").Name, timeDiff, ParamUnidad.s);
                MaximumClockDeviation = string.Format("$Margenes.{0}", testBase.Params.TIME.Null.TestPoint("DEVIATION").modo("MAX").Name);
            }           

            var minDeviationTime = -Convert.ToDouble(MaximumClockDeviation.Value);
            var maxDeviationTime = Convert.ToDouble(MaximumClockDeviation.Value);

            Logger.InfoFormat($"Variable: {DataInput}, Desviation: {timeDiff}s, Min: {minDeviationTime}s, Max: {maxDeviationTime}s");

            Assert.AreBetween(name, timeDiff, minDeviationTime, maxDeviationTime, Error().UUT.HARDWARE.RTC("DESVIACION FUERA MARGENES"), ParamUnidad.s);
        }
    }

    public class ValidateDateTimeDescription : ActionDescription
    {
        public override string Name { get { return "ValidateDateTime"; } }
        public override string Description { get { return "Acción genérica para comparar variables o resultados de tiempo con sus márgenes"; } }
        public override string Category { get { return "Actions/Variables/Validate"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
    }
}
