﻿using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Dataman;
using Dezac.Tests.Dataman.Controls;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.FirmwareUpdate
{
    [ActionVersion(1.09)]
    [DesignerAction(Type = typeof(DatamanActionDescription))]
    public class DatamanAction : ActionBase
    {
      
        [Category("3.1 Path Dataman")]
        [Description("Ruta donde está el Dataman instalado")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue(@"C:\Program Files (x86)\Dataman\Dataman-Pro")]
        public string DatamanPath { get; set; }

        [Category("3.2 Path individual binary")]
        [Description("Fichero del proyecto a grabar, si está en blanco se coge de la base de datos por el contexto")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ProjectFileName { get; set; }

        [Category("3.3 Timeout Recoder")]
        [Description("Si el programa no se ha encontrado o si la operación no se ha podido realizar antes del Timeout el test se cancela")]
        [DefaultValue(60)]
        public int TimeoutGrabador { get; set; }

        [Category("3. Configuration")]
        [Description("Acción a realizar con el grabador grabar,verificar, borrar ... ")]
        [DefaultValue(ActionEnum.ProgramDevice)]
        public ActionEnum DoAction { get; set; }

        [Category("3.4 Configuration Multirecoder")]
        [Description("Activar las posiciones múltiples a grabar del Dataman con sus ficheros")]
        public FileFasePosition[] PositionConfiguration { get; set; }



        public enum ActionEnum
        {
            BlankCheck = 2,
            ReadDevice = 3,
            VerifyDevice = 4,
            ProgramDevice = 5,
            EraseDevice = 6,
        }

        public enum Programers
        {
            Position_1 = 0,
            Position_2 = 1,
            Position_3 = 2,
            Position_4 = 3,
            Position_5 = 4,
            Position_6 = 5,
            Position_7 = 6,
            Position_8 = 7
        }

        private Dictionary<Programers, string> serialNumberDeviceProgramers = new Dictionary<Programers, string>()
        {
            {  Programers.Position_1, "1180-09199" },
            {  Programers.Position_2, "1180-09200" },
            {  Programers.Position_3, "1180-09201" },
            {  Programers.Position_4, "1180-09202" },
            {  Programers.Position_5, "1180-09203" },
            {  Programers.Position_6, "1180-09204" },
            {  Programers.Position_7, "1180-09205" },
            {  Programers.Position_8, "1180-09206" },
        };

        public class FileFasePosition
        {
            public string Name { get { return PosicionActiva.ToString(); } }

            [Category("Posiciones")]
            [Description("Avtivar las posiciones a grabar del Dataman")]
            [DefaultValue(Programers.Position_1)]
            public Programers PosicionActiva { get; set; }

            [Category("Path binario")]
            [Description("fichero del proyecto a grabar, si esta en blanco se coge de la BBDD por la fase seleccionada o por contexto si esta en blanco")]
            [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
            [DefaultValue("")]
            public string NombreProjecto { get; set; }

            [Category("Fase")]
            [Description("Fase a obtener el binario")]
            [DefaultValue(TestInfo.FasesEnum.Grabar_070)]
            public TestInfo.FasesEnum Fase { get; set; }

            [Category("Equipo")]
            [Description("Numero de hilo o instancia a la que equivale la posicion del dataman")]
            [DefaultValue("")]
            public int? Instancia { get; set; }
        }

        public override void Execute()
        {
            var Title = "DATAMAN - 48PRO2";

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var cacheSvc = context.Services.Get<ICacheService>();
            var modelContext = context.Services.Get<ITestContext>();

            var multiTest = SequenceContext.Current.TotalNumInstances > 1;
            var Disconect = false;

            var timeoutGrabador = TimeoutGrabador != 0 ? TimeoutGrabador : 90;
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase != null)
                timeoutGrabador = (int)testBase.Configuracion.GetDouble("TIMEOUT_GRABADOR", 60, ParamUnidad.Numero);

            WaitAllTestsAndRunOnlyOnce("_INITDATAMAN_", () =>
            { 
                SharedVariables.AddOrUpdate("DATAMAN_END", false);
                SharedVariables.AddOrUpdate("DATAMAN_EXCEPTION", null);
                try
                {
                    DatamanManager manager = null;

                    var result = iShell.ShowDialog(Title, (Func<Control>)(() =>
                    {

                    List<PositionSite> positions = new List<PositionSite>();

                        if (PositionConfiguration == null)
                            positions.Add(new PositionSite() { position = 0, serialNumber = "", ProjectFile = GetProjectFileName(ProjectFileName), timeoutGrabador = timeoutGrabador, instance = 1 });
                        else
                        {
                            var i = 1;
                            foreach (var pos in PositionConfiguration.ToList().OrderBy(p=> p.PosicionActiva))
                            {
                                var serialNumberDevice = serialNumberDeviceProgramers[pos.PosicionActiva];
                                var numinst = pos.Instancia.HasValue ? pos.Instancia.Value : i++;
                                positions.Add(new PositionSite() { position = (int)pos.PosicionActiva + 1, serialNumber = serialNumberDevice, ProjectFile = GetProjectFileName(pos), timeoutGrabador = timeoutGrabador, instance = numinst });
                            }
                        }

                        cacheSvc.TryAdd("DatamanSvc", () => new DatamanManager
                        {
                            DatamanPath = DatamanPath,
                            ViewLog = true,
                            Verbose = true,
                            MultiSite = PositionConfiguration != null,
                        }, out manager);

                        if (!manager.HasClientsConnected)
                            manager.FirstConnect(positions);

                        var uiControl = new DataManView { Manager = manager, Text = Title };

                        manager.Connect((DatamanManager.OperationType)((int)DoAction));

                        return uiControl;

                    }), MessageBoxButtons.OK);

                    if (manager == null)
                        throw new Exception("No se puede inicializar Dataman");

                    if (result == DialogResult.Cancel)
                        Disconect = true;

                    SharedVariables.AddOrUpdate("DATAMAN_END", true);
                }
                catch (Exception e)
                {
                    SharedVariables.AddOrUpdate("DATAMAN_EXCEPTION", e);
                    throw e;
                }
            });

            if (!GetSharedVariable<bool>("DATAMAN_END", false))
            {
                Exception datamanException = GetSharedVariable<Exception>("DATAMAN_EXCEPTION", new Exception());
                if (datamanException != null)
                    throw datamanException;
                throw new Exception("No se ha podido ejecutar Dataman por error desconocido");
            }

            var mngr = cacheSvc.Get<DatamanManager>("DatamanSvc");

            if (multiTest && !mngr.Sites.Any(p => p.instance == context.NumInstance))
                throw new Exception("No se ha podido obtener los resultados de la grabación!");

            var sb = new StringBuilder();

            foreach (var site in mngr.Sites)
            {
                if (multiTest && site.instance != context.NumInstance)
                    continue;

                if (site.LoadProjectResult != DatamanManager.FileResult.Good)
                    sb.AppendFormat("No se ha podido cargar correctamente el archivo del proyecto - Site: {0}!", site.Site);
                else
                {
                    context.ResultList.Add(string.Format("Resultado #{2}, {0}: {1}", DoAction, site.OpResult, site.Site));
                    site.OpActionsDone.ForEach(p => context.ResultList.Add(string.Format("Operación realizada: #{2} {0} ({1} seg.)", p.OpCode, p.TotalSeconds, site.Site)));

                    if (modelContext != null)
                    {       
                        modelContext.Resultados.Set("DATAMAN_RESULT_" + site.Site.ToString(), ((int)site.OpResult).ToString(), ParamUnidad.SinUnidad);

                        site.OpActionsDone.ForEach(p =>
                        {
                            var key = "_" + p.OpCode.ToString().ToUpper();

                            if (site.Site > 0 && !multiTest)
                                key += "_" + site.Site.ToString();

                            modelContext.Resultados.Set("DATAMAN_" + site.Site.ToString() + "_"+ key, p.Error ? "0" : "1", ParamUnidad.SinUnidad);
                                
                            modelContext.Resultados.Set("DATAMAN_" + site.Site.ToString() + "_" + ConstantsParameters.Electrics.TIME + key, p.TotalSeconds.ToString(), ParamUnidad.s);
                        });
                    }
                }

                if (site.OpResult != DatamanManager.DetailedOpResult.Good)
                {
                    sb.AppendFormat("Error Dataman (#{2}) {0} ->  {1}", site.OpResult.ToString(), site.InfoLine, site.Site);

                    if (site.OpResult == DatamanManager.DetailedOpResult.NoCreditBox)
                        Disconect = true;
                }
            }

            if (Disconect)
                cacheSvc.Remove("DatamanSvc");


            if (sb.Length > 0)
                throw new Exception(sb.ToString());
        }

        private string GetProjectFileName(FileFasePosition position)
        {
            return GetProjectFileName(position.NombreProjecto, ((int)position.Fase).ToString("000"));
        }

        private string GetProjectFileName(string projectFileName = "", string idFase = "" )
        {
            if (!string.IsNullOrEmpty(projectFileName))
                return projectFileName;

            var context = SequenceContext.Current;
            var modelContext = context.Services.Get<ITestContext>();
            if (modelContext == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestContext").Throw();

            var cacheSvc = context.Services.Get<ICacheService>();

            string key = $"Dataman_Binary_Project_{modelContext.NumProducto}_{modelContext.Version}_{idFase}.eprj";
            string fileName = null;

            if (cacheSvc.TryGet(key, out fileName))
                return fileName;

            BinaryItemViewModel item = null;

            string IdFase = modelContext.IdFase;
            if (!string.IsNullOrEmpty(idFase) && idFase!="000")
                IdFase = idFase;

            using (var svc = new DezacService())
            {
                item = svc.GetBinario(modelContext.NumProducto, modelContext.Version, IdFase, "706");
            }

            if (item == null)
                Error().SOFTWARE.SECUENCIA_TEST.FICHERO_NO_ENCONTRADO("binario para este producto").Throw();

            fileName = SaveBinaryFile(key, item.Fichero);

            cacheSvc.Add(key, fileName);

            if (!File.Exists(fileName))
                Error().SOFTWARE.SECUENCIA_TEST.FICHERO_NO_ENCONTRADO("archivo de proyecto").Throw();

            return fileName;
        }

        private string PathFileBinary
        {
            get { return ConfigurationManager.AppSettings["PathFileBinary"]; }
        }

        private string SaveBinaryFile(string name, byte[] data)
        {
            if (!Directory.Exists(PathFileBinary))
                Directory.CreateDirectory(PathFileBinary);

            string fileName = Path.Combine(PathFileBinary, name);

            File.WriteAllBytes(fileName, data);

            return fileName;
        }
    }

    public class DatamanActionDescription : ActionDescription
    {
        public override string Description { get { return "Action para la grabación de binarios usando el DATAMAN -48PRO2 (individual o Multiple)"; } }
        public override string Name { get { return "Dataman"; } }
        public override string Category { get { return "Tools/Firmware"; } }
        public override Image Icon { get { return Properties.Resources.Dataman_logo; } }
        public override string Dependencies { get { return "InitTestBase;"; } }
    }
}
