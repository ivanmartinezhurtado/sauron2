﻿using Dezac.DLMS;
using System.ComponentModel;
using System.Drawing;
using System.IO;

namespace Dezac.Tests.Actions
{


    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(Read_DLMS_Profile_Description))]

    public class Read_DLMS_Profile : ActionBase
    {





        [Category("3. Configuration")]
        [Description("Número de atributo")]
        [DefaultValue(0)]
        public int Attribute { get; set; }

        [Category("3. Configuration")]
        [Description("Número Obis")]
        [DefaultValue("0")]
        public string Obis { get; set; }




        public override void Execute()
        {

            
            var T = GetVariable("DlmsDevice") as DLMSDevice;
            T.Read_profile( Attribute, Obis);

          



     


        }
    }

    public class Read_DLMS_Profile_Description : ActionDescription
    {
        public override string Name { get { return "ReadDLMSProfile"; } }
        public override string Description { get { return "Action que sirve para leer el DLMS de un perfil"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
    }





}

