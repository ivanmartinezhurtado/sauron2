namespace Dezac.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.ARTICULOCLIENTEDESCRIPCION")]
    public partial class ARTICULOCLIENTEDESCRIPCION
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMARTICULO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string IDCLIENTE { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(35)]
        public string REFCLIENTE { get; set; }

        [StringLength(2)]
        public string IDIDIOMA { get; set; }

        [StringLength(30)]
        public string DESCRIPCION { get; set; }
    }
}
