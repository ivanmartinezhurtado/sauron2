﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dezac.Core.Utility
{
    public struct TriLineGenericValue<T>
    {
        public readonly static TriLineGenericValue<T> Zero = new TriLineGenericValue<T>();

        public T L1 { get; set; }
        public T L2 { get; set; }
        public T L3 { get; set; }

        public static TriLineGenericValue<T> Create(T v1, T v2, T v3)
        {
            return new TriLineGenericValue<T> { L1 = v1, L2 = v2, L3 = v3 };
        }

        public static TriLineGenericValue<T> Create(T v)
        {
            return new TriLineGenericValue<T> { L1 = v, L2 = v, L3 = v };
        }
 
        public static implicit operator TriLineGenericValue<T>(T value)
        {
            return new TriLineGenericValue<T> { L1 = value, L2 = value, L3 = value };
        }

        public override string ToString()
        {
            return string.Format("L1={0}, L2={1}, L3={2}", L1, L2, L3);
        }

        public T[] ToArray()
        {
            var list = new List<T>();
            list.Add(L1);
            list.Add(L2);
            list.Add(L3);
            return list.ToArray();
        }

        public List<T> ToList()
        {
            var list = new List<T>();
            list.Add(L1);
            list.Add(L2);
            list.Add(L3);
            return list;
        }

        public static T[] ToArray(TriLineGenericValue<T> value)
        {
            var list = new List<T>();
            list.Add(value.L1);
            list.Add(value.L2);
            list.Add(value.L3);
            return list.ToArray();
        }
    }
}
