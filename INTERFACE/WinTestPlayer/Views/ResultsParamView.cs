﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;
using Telerik.WinControls.UI;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class ResultsParamView : UserControl
    {
        private SynchronizationContext sc;

        public ResultsParamView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            sc = SynchronizationContext.Current;

            AppService app = DI.Resolve<AppService>();
            app.Runner.AllTestStarting += (s, ee) =>
            {
                bsList.Clear();
            };
            app.Runner.TestStart += (s, ee) =>
            {
                ITestContext testContext = SequenceContext.Current.Services.Get<ITestContext>();
                if (testContext != null)
                    testContext.Resultados.ItemChanged += OnResultAdded;
            };
            app.Runner.TestEnd += (s, ee) =>
            {
                ITestContext testContext = SequenceContext.Current.Services.Get<ITestContext>();
                if (testContext != null)
                    testContext.Resultados.ItemChanged -= OnResultAdded;
            };
        }
        void OnResultAdded(object sender, ParamValue e)
        {
            sc.Post(_ => { AddItem(e); }, null);
        }

        public void AddItem(ParamValue item)
        {
            int pos = bsList.IndexOf(item);
            if (pos < 0)
                bsList.Add(item);
            else
                bsList.ResetItem(pos);

            if (pos > 0)
                radGridView1.Rows[pos].Selected = true;

            radGridView1.FirstDisplayedScrollingRowIndex = radGridView1.Rows.Count-1;
        }

        private void radGridView1_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement.ColumnInfo.Name == "Valor")
            {
                var value = e.Row.DataBoundItem as ParamValue;
                if (value != null && !value.IsValid)
                {
                    e.CellElement.BackColor = Color.Red;
                    e.CellElement.DrawFill = true;
                    e.CellElement.NumberOfColors = 1;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, Telerik.WinControls.ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.NumberOfColorsProperty, Telerik.WinControls.ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.BackColorProperty, Telerik.WinControls.ValueResetFlags.Local);
                }
            }
        }
    }
}
