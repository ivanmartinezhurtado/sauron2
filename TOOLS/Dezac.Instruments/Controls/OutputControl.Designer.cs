﻿using Telerik.WinControls.UI;

namespace Dezac.Instruments.Controls
{
    partial class OutputControl
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OutputControl));
            this.ImageListIO = new System.Windows.Forms.ImageList(this.components);
            this.btState = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ImageListIO
            // 
            this.ImageListIO.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListIO.ImageStream")));
            this.ImageListIO.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListIO.Images.SetKeyName(0, "btnRed.png");
            this.ImageListIO.Images.SetKeyName(1, "btnGreen.png");
            this.ImageListIO.Images.SetKeyName(2, "btnBlue.png");
            this.ImageListIO.Images.SetKeyName(3, "btnYellow.png");
            this.ImageListIO.Images.SetKeyName(4, "btnGray.png");
            // 
            // btState
            // 
            this.btState.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btState.Cursor = System.Windows.Forms.Cursors.Default;
            this.btState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btState.FlatAppearance.BorderSize = 0;
            this.btState.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btState.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btState.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btState.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btState.ImageIndex = 0;
            this.btState.ImageList = this.ImageListIO;
            this.btState.Location = new System.Drawing.Point(0, 0);
            this.btState.Margin = new System.Windows.Forms.Padding(2);
            this.btState.Name = "btState";
            this.btState.Size = new System.Drawing.Size(30, 32);
            this.btState.TabIndex = 0;
            this.btState.Text = "50";
            this.btState.UseVisualStyleBackColor = true;
            this.btState.Click += new System.EventHandler(this.btState_Click);
            this.btState.MouseEnter += new System.EventHandler(this.OutputControl_MouseEnter);
            this.btState.MouseLeave += new System.EventHandler(this.OutputControl_MouseLeave);
            // 
            // OutputControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btState);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "OutputControl";
            this.Size = new System.Drawing.Size(30, 32);
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.ImageList ImageListIO;
        private System.Windows.Forms.Button btState;
    }
}
