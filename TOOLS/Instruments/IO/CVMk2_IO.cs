﻿using Dezac.Device;
using Instruments.Utility;
using log4net;
using System;

namespace Instruments.IO
{
    [InstrumentsVersion(1.00)]
    public class CVMk2_IO : IOBase
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(PCI7296));
   
        private ChannelConfiguration config;

        private static readonly object syncObject = new object();

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public enum ChannelsEnum
        {
            SLOT1 = 0
        }     

        public CVMk2_IO()
            : base(logger)
        {
            SetPort();
            Config().SetChannels(1, 1);
            Layout();
        }

        public ChannelConfiguration Config()
        {
            config = config ?? new ChannelConfiguration(this);

            return config;
        }

        public void SetPort(int port = 5, byte perifericNumber = 10, int baudRate = 38400)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = perifericNumber;
        }

        public void Layout(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                ApplyDefaultLayout();
        }

        public void ApplyDefaultLayout()
        {
            Config()
               .SetChannelAsOutput((int)ChannelsEnum.SLOT1)
               .Apply();
        }

        protected override void InternalApplyConfig(IIOBaseConfiguration config)
        {
            this.config = config as ChannelConfiguration;

            foreach (IOChannel channel in this.config.Channels)
                foreach (var port in channel.Ports)
                {
                    port.Data = 0;

                    if (port.IOType == IOType.Output)
                        InternalWritePort(port);
                    else
                        InternalReadPort(port);
                }
        }

        protected override T InternalRead<T>(IOType type, int num)
        {
            var port = GetPort(type, num);
            int bit = ((num - port.MinBit) % port.BitsPerPort);
            bool result = false;

            lock (syncObject)
            {
                var value = InternalReadPort(port);

                result = (value & (1 << bit)) > 0;

                port[bit] = result;
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        protected override void InternalWrite<T>(int num, T value)
        {
            var port = GetPort(IOType.Output, num);
            lock (syncObject)
            {
                port.Data = Convert.ToByte(num);

                InternalWritePort(port);
            }
        }

        protected byte InternalReadPort(IOPort port)
        {
            int value = 0;

            var GetSlot = GetAdressSlote(port);
            value = ReadIOSlote(GetSlot);
          
            if (port.IOType == IOType.Output)
                value = ~value;

            return (byte)(0xFF & value);
        }

        protected void InternalWritePort(IOPort port)
        {
            var value = 0xFF00 | port.Data;
            var GetSlot = GetAdressSlote(port);
            WriteIOSlote(GetSlot, value);
        }

        private void WriteIOSlote(ushort slot, int value)
        {       
            Modbus.Write((ushort)slot, (ushort)value);
        }

        private int ReadIOSlote(ushort slot)
        {
           var result= Modbus.ReadInt32((ushort)slot);
           return result;
        }
        private ushort GetAdressSlote(IOPort port)
        {
            var register = port.Channel.NumChannel == 0 ? Registers.SLOT1 : port.Channel.NumChannel == 1 ? Registers.SLOT2 : Registers.SLOT3;
            return (ushort)register;
        }

        internal IOPort GetPort(IOType type, int num)
        {
            foreach(var channel in config.Channels)
                foreach (IOPort port in channel.Ports)
                    if (port.IOType == type && num >= port.MinBit && num <= port.MaxBit)
                        return port;

            throw new IOException(IOException.Codes.INVALID_PORT);
        }

        public enum Registers
        {
            SLOT1 = 0x5DC0,
            SLOT2 = 0x7148,
            SLOT3 = 0x84D0,
        };

        public override void Dispose()
        {
            Modbus.Dispose();
            base.Dispose();
        }
    }
}
