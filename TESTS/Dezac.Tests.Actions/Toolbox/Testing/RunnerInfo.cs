﻿using log4net;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(RunnerInfoDescription))]
    [Browsable(false)]
    public class RunnerInfo : ActionBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("RunnerInfo");



        [Description("Nota de donde ha fallado el Test dentro de la secuencia")]
        [Category("Notas")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Notas { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var context = SequenceContext.Current;
            var result = context.TestResult;

            logger.InfoFormat("RESULT INIT {0} !!", result.InitStepResult.ExecutionResult.ToString());
            testBase.Resultado.Set("RESULT INIT", result.InitStepResult.ExecutionResult.ToString(), Model.ParamUnidad.SinUnidad);

            logger.InfoFormat("RESULT MAIN {0} !!", result.MainStepResult.ExecutionResult.ToString());
            testBase.Resultado.Set("RESULT MAIN", result.MainStepResult.ExecutionResult.ToString(), Model.ParamUnidad.SinUnidad);

            logger.InfoFormat("TEST {0} !!", result.ExecutionResult.ToString());
            testBase.Resultado.Set("RESULTADO",  result.ExecutionResult.ToString(), Model.ParamUnidad.SinUnidad);
        }
    }

    public class RunnerInfoDescription : ActionDescription
    {
        public override string Name { get { return "RunnerInfo"; }}
        public override string Description { get { return "Test para probar que los actions fallan en algún punto de la secuencia"; } }
        public override string Category { get { return "Actions/Context"; } }
        public override Image Icon { get { return Properties.Resources.kernel; } }
    }
}
