﻿using System.Drawing;
using Dezac.Tests.AOI;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using Dezac.Tests.Actions.Views;
using System.IO;
using Newtonsoft.Json;
using System.Linq;
using System;
using Dezac.Tests.Model;
using TaskRunner;
using Dezac.Tests.Services;
using Dezac.Core.Exceptions;
using TaskRunner.Model;
using Dezac.Core.Model;

namespace Dezac.Tests.Actions.AOI
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AOIClassificationV2Description))]
    [Browsable(false)]
    public class AOIClassificationV2 : ActionBase
    {
        public override void Execute()
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var data = context.Services.Get<ITestContext>();
            if (data == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CARGA_SERVICIO("ITestContext").Throw();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CARGA_SERVICIO("TestBase").Throw();

            int numColumns = (int)testBase.Configuracion.GetDouble("NUM_COLUMNS", 1, ParamUnidad.Numero);
            int numRows = (int)testBase.Configuracion.GetDouble("NUM_ROWS", 1, ParamUnidad.Numero);

            string auxNumeroProducto = data.NumProducto + (testBase.Model.IdFase == "039" ? "_B" : "");

            var files = Directory.GetFiles($@"W:\_AOI\IMAGES\{auxNumeroProducto}", "*.png");
            Logger.InfoFormat($"{files.Length} images found");

            AOIClassficationInfo configuration = new AOIClassficationInfo()
            {
                matchingFileName = $@"W:/_AOI/ROI/MATCHING_MODEL_{auxNumeroProducto}.shm",
                searchFileName = $@"W:/_AOI/ROI/MATCHING_BOARD_MODEL_{auxNumeroProducto}.shm",
                ROIFileName = $@"W:/_AOI/ROI/ROI_MODEL_{auxNumeroProducto}.hobj",
                boardRegionFileName = $@"W:/_AOI/ROI/MATCHING_BOARD_REGION_{auxNumeroProducto}.hobj",
                numRows = numColumns,
                numColumns = numRows,
                rowDistance = 0,
                columnDistance = 0,
                barcodeRead = false,
                orderPath = $@"C:\SAURON_SYSTEM\AOIResultsSave\0\",
                weldingSVM = $@"W:/_AOI/ROI/WeldingSVM_{auxNumeroProducto}.tup",
            };

            int i = 0;
            while (File.Exists(@"W:\_AOI\SVM\SVMClassifier_" + i + ".gsc"))
            {
                configuration.SVMFile.Add(@"W:\_AOI\SVM\SVMClassifier_" + i + ".gsc");
                i++;
            }

            files.ToList().AsParallel().ForAll((file) =>
            {
                try
                {
                    var inspectionResults = HalconInterface.AOIClassification(file, configuration);
                    inspectionResults.NumProduct = testBase.Model.NumProducto;
                    inspectionResults.NumFamilia = testBase.Model.NumFamilia.Value;
                    inspectionResults.NumOperario = testBase.Model.IdOperario;
                    inspectionResults.NumOrden = testBase.Model.NumOrden.Value;
                    inspectionResults.IdFase = testBase.Model.IdFase;
                    inspectionResults.Date = DateTime.Now;

                    var json = JsonConvert.SerializeObject(inspectionResults);
                    var path = ConfigurationManager.AppSettings["PathAOIResultsSave"] + testBase.Model.NumOrden.Value.ToString() + "\\";
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    File.WriteAllText(path + string.Join("_", inspectionResults.Barcodes) + ".txt", json);

                    Logger.InfoFormat($"Image {file} Classified");
                }
                catch (Exception ex)
                {
                    Logger.WarnFormat($"Error in image {file}: {ex.Message}");
                }
            });
        }
    }

    public class AOIClassificationV2Description : ActionDescription
    {
        public override string Name { get { return "AOIClassificationV2"; } }
        public override string Category { get { return "DezacTest/Vision/AOI"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
