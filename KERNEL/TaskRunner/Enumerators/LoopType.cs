﻿namespace TaskRunner.Enumerators
{
    public enum LoopType
    {
        None = 0,
        OnFail = 1,
        OnSuccess = 2,
        Always = 4,
        OnCondition = 5
    }
}
