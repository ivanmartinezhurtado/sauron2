﻿using Comunications.Message;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(ReadCanBusSerialPortActionDescription))]
    public class ReadCanBusSerialPortAction : ActionBase
    {
        [DefaultValue(200)]
        [Description("Reintentos de comunicaciones")]
        [Category("3. Configuration")]
        public int Retries { get; set; }

        [DefaultValue(1000)]
        [Description("Tiempo de espera a recibir la trama amtes de dar error")]
        [Category("3. Configuration")]
        public int TimeOut { get; set; }

        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_CAN")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            string name = DeviceName ?? "COM_CAN";
            var transport = GetVariable(name) as MessageTransport;
            if (transport == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("DeviceName").Throw();

            transport.ReadTimeout = TimeOut;
            transport.Retries = Retries;
            transport.LogRawMessage = true;

            var varName = DataOutput.Value.ToString() ?? "CAN";
            var message = CanBusMessage.Create("","");

            var response = transport.ReadResponse(message);
            
            Context.Variables.AddOrUpdate(varName, response.ToString());
            Context.ResultList.Add(string.Format("{0} => {1}", varName, response.ToString()));

            for (int b = 0; b < response.MessageFrame.Count(); b++)
            {

                Context.Variables.AddOrUpdate(varName + string.Format("_BYTE_{0}", b + 1), response.MessageFrame[b]);
                Context.ResultList.Add(string.Format("{0} => {1}", varName + string.Format("_BYTE_{0}", b + 1), response.MessageFrame[b]));
            }
        }
    }

    public class ReadCanBusSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "ReadCanBusSerialPort"; } }
        public override string Description { get { return "Lee el puerto serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/Canbus"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
