﻿using Comunications.Utility;
using Dezac.Core.Utility;
using log4net;
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class CIRLAMP : DeviceBase
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(CIRLAMP));

        public ushort FactorGainDesfase { get; set; }
        public ushort FactorGainVoltage { get; set; }
        public ushort FactorGainPower { get; set; }

        public CIRLAMP()
        {
        }

        public CIRLAMP(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 38400, byte periferic = 1)
        {
            if (CirPLCModbus == null)
                CirPLCModbus = new ModbusDeviceCirPLC(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, logger);
            else
                CirPLCModbus.PortCom = port;

            CirPLCModbus.Modbus.TimeOut = 8000;

            CirPLCModbus.PerifericNumber = periferic;

            FactorGainDesfase = 7;
            FactorGainVoltage = 242;
            FactorGainPower = 3401;
        }

        public FirmwareVersion ReadSoftwareVersion()
        {
            LogMethod();
            return CirPLCModbus.Modbus.Read<FirmwareVersion>();
        }

        public int ReadNumSerie()
        {
            LogMethod();
            return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.NUM_SERIE_READ);
        }

        public void WriteNumSerie(UInt32 value)
        {
            LogMethod();
            CirPLCModbus.Modbus.WriteInt32((ushort)Registers.NUM_SERIE_WRITE, (int)value);
        }

        public enum ModelCirLamp
        {
            _1_10=0,
            DN=1,
            DALI=2,
        }

        public void WriteModel(ModelCirLamp value)
       {
           LogMethod();
           CirPLCModbus.Modbus.Write((ushort)Registers.MODEL, (ushort)value);
       }

       public int ReadVoltage()
       {
           LogMethod();
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.VOLTAGE);
       }

       public double ReadCurrent()
       {
           LogMethod();
           int current = CirPLCModbus.Modbus.ReadInt32((ushort)Registers.CURRENT);
           return ((double)current / 10);
       }

       public int ReadPowerActive()
       {
           LogMethod();
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.POWER_ACTIVE);
       }

       public int ReadPowerReactive()
       {
           LogMethod();
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.POWER_REACTIVE);
       }

       public int AdjustDesfase(double Angle, int numMuestras = 2, int intervaloMuestras = 1100)
       {
           WriteDesfaseFactor(FactorGainDesfase);

           Thread.Sleep(4000);

           var list = new StatisticalList(2);

           var sampler = Sampler.Run(
             () => new SamplerConfig { Interval = intervaloMuestras, NumIterations = numMuestras, InitialDelay = 500, CancelOnException = true },
             (step) =>
             {
                 var PointActive = ReadPointActive();

                 var PointReactive = ReadPointReactive();

                 if (step.Step < 1)
                     return;

                 list.Add(PointActive, PointReactive);

             });

           sampler.ThrowExceptionIfExists();

           double AnlgeCalculate = Math.Atan(list.Average(1) / list.Average(0)).ToDegree();

           double ErrorCalculate = AnlgeCalculate - Angle;

           double GainDesfase = ((ErrorCalculate / 0.1446875) + FactorGainDesfase); 

           WriteDesfaseFactor((ushort)GainDesfase);

           return (int)GainDesfase;
       }

       public int AdjustVoltage(double TensionRef, int numMuestras =2, int intervaloMuestras = 1100)
       {

           WriteVoltageFactor(FactorGainVoltage);

           Thread.Sleep(4000);

           var list = new StatisticalList(1);

           var sampler = Sampler.Run(
            () => new SamplerConfig { Interval = intervaloMuestras, NumIterations = numMuestras, InitialDelay = 500, CancelOnException = true },
            (step) =>
            {

                var PointVoltage = ReadPointVoltage();

                list.Add(PointVoltage);
            });

           sampler.ThrowExceptionIfExists();

           double result = TensionRef / (list.Average(0) / FactorGainVoltage);

           WriteVoltageFactor((ushort)result);

           return (int)result;
       }

       public int AdjustPower(double PowerActiveRef, int numMuestras = 2, int intervaloMuestras = 1100)
       {
           WritePowerFactor(FactorGainPower);
           
           Thread.Sleep(4000);

           var list = new StatisticalList(1);

           var sampler = Sampler.Run(
           () => new SamplerConfig { Interval = intervaloMuestras, NumIterations = numMuestras, InitialDelay = 500, CancelOnException = true },
           (step) =>
           {
               var PointsActive = ReadPointActive();

               if (step.Step < 1)
                   return;

               list.Add(PointsActive);

           });

           sampler.ThrowExceptionIfExists();

           double result = (list.Average(0) * FactorGainPower) / PowerActiveRef;

           WritePowerFactor((ushort)result);

           return (int)result;
       }

       public int ReadPointVoltage()
       {
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.POINT_VOLTAGE);
       }

       public int ReadPointCurrent()
       {
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.POINT_CURRENT);
       }

       public int ReadPointActive()
       {
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.POINT_ACTIVE);
       }

       public int ReadPointReactive()
       {
           return CirPLCModbus.Modbus.ReadInt32((ushort)Registers.POINT_REACTIVE);
       }

       public ushort ReadDesfaseFactor()
       {
          return CirPLCModbus.Modbus.ReadRegister((ushort)Registers.DESFASE_FACTOR);
       }

       public void WriteDesfaseFactor(ushort value)
       {
           CirPLCModbus.Modbus.Write((ushort)Registers.DESFASE_FACTOR, value);
       }

       public void WriteVoltageFactor(ushort value)
       {
           CirPLCModbus.Modbus.Write((ushort)Registers.VOLTAGE_FACTOR, value);
       }

       public ushort ReadVoltageFactor()
       {
           return CirPLCModbus.Modbus.ReadRegister((ushort)Registers.VOLTAGE_FACTOR);
       }

       public void WritePowerFactor(ushort value)
       {
           CirPLCModbus.Modbus.Write((ushort)Registers.POWER_FACTOR, value);
       }

       public ushort ReadPowerFactor()
       {
           return CirPLCModbus.Modbus.ReadRegister((ushort)Registers.POWER_FACTOR);
       }
       //-----------------------------------------------------------------------------------

       public void WriteTemporalInterval(TemporalInterval value)
        {
            CirPLCModbus.Modbus.Write<TemporalInterval>(value);
        }

       public ushort ReadTestDaly()
       {
           return CirPLCModbus.Modbus.ReadRegister((ushort)Registers.TEST_DALY);
       }

       public void ResetTotalTimeLight()
       {
           CirPLCModbus.Modbus.WriteSingleCoil((ushort)Registers.RESET_ACTIVE_TIME, true);
       }

        //------------------------------------------------------------------------------

        public ModbusDeviceCirPLC CirPLCModbus { get; internal set; }

        public override void Dispose()
        {
            CirPLCModbus.Dispose();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VERSION_SOFT)]
        public struct FirmwareVersion
        {
            public UInt16 Major;
            public UInt16 Minor;
            public UInt16 Revision;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SET_TEMPORAL_INTERVAL)]
        public struct TemporalInterval
        {
            public UInt16 Power;
            public UInt16 Time;
        };

        public enum Registers
        {
            VERSION_SOFT = 0x0050,
            NUM_SERIE_READ = 0x0060,
            NUM_SERIE_WRITE = 0xF000,
            MODEL = 0xF010,

            DESFASE_FACTOR = 0xF461,
            VOLTAGE_FACTOR = 0xF411,
            POWER_FACTOR = 0xF421,

            VOLTAGE=0xF200,
            CURRENT=0xF202,
            POWER_ACTIVE = 0xF204,
            POWER_REACTIVE = 0xF206,

            POINT_VOLTAGE = 0xF200,
            POINT_CURRENT = 0xF202,
            POINT_ACTIVE = 0xF204,
            POINT_REACTIVE = 0xF206,

            DALI_TEST=0x0030,
            CONECTED_PLC=0x0F101,     
            SET_TEMPORAL_INTERVAL= 0x2010,
            RESET_ACTIVE_TIME=0x1005,
            TEST_DALY=0x1002,
        };

    }
}
