﻿namespace Dezac.Core.Utility
{
    public static class UACScript
    {
        public static void ImportModule_UACScript()
        {
            PowerShell.ProcessNetsh("Import-Module ", "SwitchUACLevel.psm");
        }
        public static void Get_UAC_Level()
        {
            PowerShell.ProcessNetsh("Get-UACLevel","");
        }

        public static void Set_UAC_Level(string value)
        {
            PowerShell.ProcessNetsh("Set-UACLevel", value);
        }
    }
}
