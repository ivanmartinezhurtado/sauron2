﻿using System.Collections.Concurrent;
using System.Threading;

namespace TaskRunner.Tools
{
    public static class CallTaskContext
    {
        static ConcurrentDictionary<string, AsyncLocal<object>> state = new ConcurrentDictionary<string, AsyncLocal<object>>();

        public static void SetData(string name, object data)
        {
            state.GetOrAdd(name, _ => new AsyncLocal<object>()).Value = data;
        }

        public static object GetData(string name)
        {

            if (state.TryGetValue(name, out AsyncLocal<object> data))
                return data.Value;

            return null;
        }

        public static void Remove(string name)
        {

            if (state.TryRemove(name, out AsyncLocal<object> data))
                data.Value = null;
        }
    }
}
