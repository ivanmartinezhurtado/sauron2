﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(TestPointFrecuencyMeasureDescription))]
    public class TestPointFrecuencyMeasure : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Canal por el que vamos a medir")]
        [DefaultValue(HP53131A.Channels.CH1)]
        public HP53131A.Channels Channel { get; set; }

        [Category("3.5 Impedance")]
        [Description("Tipo acoplo de la Impedancia")]
        [DefaultValue(HP53131A.Impedancia.CoupEnum.DC)]
        public HP53131A.Impedancia.CoupEnum CouplingImpedance { get; set; }

        [Category("3.5 Impedance")]
        [Description("Atenuación de la impedancia")]
        [DefaultValue(HP53131A.Impedancia.AttenEnum.X10)]
        public HP53131A.Impedancia.AttenEnum AtenuationImpedance { get; set; }

        [Category("3.5 Impedance")]
        [Description("Activación del filtro de impedancia")]
        [DefaultValue(HP53131A.Impedancia.FilterEnum.Filt_ON)]
        public HP53131A.Impedancia.FilterEnum FilterImpedance { get; set; }

        [Category("3.5 Impedance")]
        [Description("Activación del nivel de impedancia")]
        [DefaultValue(HP53131A.Impedancia.ImpedanciaEnum.OHM_50)]
        public HP53131A.Impedancia.ImpedanciaEnum LevelImpedance { get; set; }

        [Category("3.2 Trigger")]
        [Description("Tipo de trigger de la señal que vamos a medir")]
        [DefaultValue(HP53131A.Trigger.LevelEnum.NUM_V_OFF)]
        public HP53131A.Trigger.LevelEnum TriggerLevelType { get; set; }

        [Category("3.2 Trigger")]
        [Description("Tipo de pendiende de la señal de detección del trigger")]
        [DefaultValue(HP53131A.Trigger.SlopeEnum.NEG)]
        public HP53131A.Trigger.SlopeEnum TriggerSlope { get; set; }

        [Category("3.2 Trigger")]
        [Description("Sensibilidad del trigger")]
        [DefaultValue(HP53131A.Trigger.SensibilitiEnum.LOW)]
        public HP53131A.Trigger.SensibilitiEnum TriggerSensibility { get; set; }

        [Category("3.2 Trigger")]
        [Description("Nivel de la señal para detectar el trigger")]
        [DefaultValue(5)]
        public double TiggerLevelValue { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de magnitud de disparo de inicio de la señal")]
        [DefaultValue(HP53131A.Gate.FuncionGateEnum.FREQ)]
        public HP53131A.Gate.FuncionGateEnum GateMagnitudMeasure { get; set; }

        [Category("3. Configuration")]
        [Description("Modo de disparo de inicio de la señal")]
        [DefaultValue(HP53131A.Gate.ModoGateStartEnum.AutoStart)]
        public HP53131A.Gate.ModoGateStartEnum GateStartMode { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de pendiente de la señal de inicio de disparo")]
        [DefaultValue(HP53131A.Gate.SlopeEnum.POS)]
        public HP53131A.Gate.SlopeEnum GateSlopeStart { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de disparo de parada para medr la señal")]
        [DefaultValue(HP53131A.Gate.ModoGateStopEnum.Digits)]
        public HP53131A.Gate.ModoGateStopEnum GateStopMode { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de pendiente de la señal de parada de disparo")]
        [DefaultValue(HP53131A.Gate.SlopeEnum.NEG)]
        public HP53131A.Gate.SlopeEnum GateSlopeStop { get; set; }

        [Category("3. Configuration")]
        [Description("Valor del nivel de resolución de la señal para disparo de parada")]
        [DefaultValue(8)]
        public int GateStopDiggitsResoluction { get; set; }

        [Category("3. Configuration")]
        [Description("Numero de de las primeras muestras que descartamos")]
        [DefaultValue(0)]
        public int DeleteFirst { get; set; }

        [Category("3. Configuration")]
        [Description("Numero de muestras")]
        [DefaultValue(3)]
        public int NumberSamples { get; set; }

        [Category("3.3 Time")]
        [Description("Tiempo de espera entre muestras en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeBetweenSamples { get; set; }

        [Category("3.3 Time")]
        [Description("Tiempo de espera antes de realizar la medida en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeReadMeasure { get; set; }


        [Category("3. Configuration")]
        [Description("Magnitud electrica a medir")]
        [DefaultValue(HP53131A.Variables.FREQ)]
        public HP53131A.Variables MagnitudToMeasure { get; set; }

        [Category("3.4 MUX")]
        [Description("Entrada de la multimplexora a seleccionar")]
        [DefaultValue(InputMuxEnum.NOTHING)]
        public InputMuxEnum InputMultiplexora { get; set; }

        [Category("3.1 Output")]
        [Description("Salida digital de la torre a activar para realizar la medida")]
        public int? ActiveOutput { get; set; }

        [Category("3.1 Output")]
        [Description("Salida digital de la torre a desactivar para realizar la medida")]
        public int? DesactiveOutput { get; set; }

        [Category("3. Configuration")]
        [Description("Descripción del nombre con el que se registrara en la BBDD los resultados de este test point")]
        public string TestPointDescription { get; set; }

        [Category("3. Configuration")]
        [Description("Usar Inputs/ Ouputs (Para sobremesa)")]
        [DefaultValue(false)]
        public bool WithOutIO { get; set; }


        [Category("4. Results")]
        [Description("Nombre de la variable donde guardaremos el valor devuelto por el metodo ejecutado")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw(); ;

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw(); ;

            string name = "";
            double min = 0;
            double max = 0;
            ParamUnidad unidad = ParamUnidad.SinUnidad;

            switch (MagnitudToMeasure)
            {
                case HP53131A.Variables.FREQ:
                    name = testBase.Params.FREQ.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.FREQ.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.FREQ.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.Hz;
                    break;

                case HP53131A.Variables.PER:
                    name = testBase.Params.PER.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.PER.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.PER.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.s;
                    break;

                case HP53131A.Variables.PWID:
                    name = testBase.Params.PWID.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.PWID.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.PWID.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.s;
                    break;

                case HP53131A.Variables.NWID:
                    name = testBase.Params.NWID.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.NWID.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.NWID.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.s;
                    break;

                case HP53131A.Variables.PHASE:
                    name = testBase.Params.PHASE.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.PHASE.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.PHASE.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.Grados;
                    break;

                case HP53131A.Variables.VOLT_MAX:
                    name = testBase.Params.VPEAK_MAX.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.VPEAK_MAX.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.VPEAK_MAX.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;

                case HP53131A.Variables.VOLT_MIN:
                    name = testBase.Params.VPEAK_MIN.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.VPEAK_MIN.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.VPEAK_MIN.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;

                case HP53131A.Variables.TINT:
                    name = testBase.Params.TINT.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.TINT.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.TINT.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.s;
                    break;

                case HP53131A.Variables.DCYCLE:
                    name = testBase.Params.DCYCLE.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.DCYCLE.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.DCYCLE.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.PorCentage;
                    break;

            }

            tower.HP53131A
            .PresetConfigImpendance(Channel, CouplingImpedance, AtenuationImpedance, FilterImpedance, LevelImpedance)
            .PresetConfigTrigger(Channel, TriggerLevelType, TriggerSlope, TriggerSensibility, TiggerLevelValue)
            .PresetConfigGate(GateMagnitudMeasure, GateStartMode, GateSlopeStart, GateStopMode, GateSlopeStop, GateStopDiggitsResoluction);

            var adjustValueTestPoint = new AdjustValueDef(name, 0, min, max, 0, 0, unidad);

            testBase.TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                return tower.MeasureWithFrecuency(InputMultiplexora, WaitTimeReadMeasure, MagnitudToMeasure, Channel, () =>
                {
                    if (ActiveOutput.HasValue)
                        tower.IO.DO.On(ActiveOutput.Value);

                    if (DesactiveOutput.HasValue)
                        tower.IO.DO.Off(DesactiveOutput.Value);
                }, () =>
                {
                    if (ActiveOutput.HasValue)
                        tower.IO.DO.Off(ActiveOutput.Value);

                    if (DesactiveOutput.HasValue)
                        tower.IO.DO.On(DesactiveOutput.Value);
                });

            }, DeleteFirst, NumberSamples, WaitTimeBetweenSamples);

            var varName = DataOutput ?? name;

            context.Variables.AddOrUpdate(varName, adjustValueTestPoint.Value);
            context.ResultList.Add(string.Format("{0} = {1}", varName, adjustValueTestPoint.Value));
        }
    }

    public class TestPointFrecuencyMeasureDescription : ActionDescription
    {
        public override string Name { get { return "TestPointFrecuencyMeasure"; } }
        public override string Description { get { return "Acción genérica para medir con el Frecuencimetro los puntos de test de las placas"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.LineChart; } }
        public override string Dependencies { get { return "InitTestBase"; } }

    }
}
