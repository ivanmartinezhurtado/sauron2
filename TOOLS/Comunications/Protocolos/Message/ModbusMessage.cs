using Comunications.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;

namespace Comunications.Message
{
    public class ModbusMessage : ITransportMessage, IValidateMessage
	{
        internal int _minimumFrameSize = 5; 

		public ModbusMessage()
		{
		}

		public ModbusMessage(byte slaveAddress, byte functionCode)
		{
            SlaveAddress = slaveAddress;
            FunctionCode = functionCode;
		}

        public byte? ByteCount { get; set; }

        public byte? ExceptionCode { get; set; }

        public int TransactionId { get; set; }

        public byte FunctionCode { get; set; }

        public ushort? NumberOfPoints { get; set; }

        public byte SlaveAddress { get; set; }

        public ushort? StartAddress { get; set; }

        public ushort? SubFunctionCode { get; set; }

        public byte[] Data { get; set; }

        public virtual byte[] MessageFrame
        {
            get
            {
                List<byte> frame = new List<byte>();
                frame.Add(SlaveAddress);
                frame.AddRange(ProtocolDataUnit);

                return frame.ToArray();
            }
        }

        public byte[] ProtocolDataUnit
        {
            get
            {
                List<byte> pdu = new List<byte>();

                pdu.Add(FunctionCode);

                if (ExceptionCode.HasValue)
                    pdu.Add(ExceptionCode.Value);

                if (SubFunctionCode.HasValue)
                    pdu.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)SubFunctionCode.Value)));

                if (StartAddress.HasValue)
                    pdu.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)StartAddress.Value)));

                if (NumberOfPoints.HasValue)
                    pdu.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)NumberOfPoints.Value)));

                if (ByteCount.HasValue)
                    pdu.Add(ByteCount.Value);

                if (Data != null)
                    pdu.AddRange(Data/*.NetworkBytes*/);

                return pdu.ToArray();
            }
        }

        public virtual int MinimumFrameSize { get { return Modbus.MinimumFrameSize; } }

		public virtual void Initialize(byte[] frame)
		{
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            ByteCount = frame[2];

            Data = frame.Skip(3).ToArray();
		}

        public virtual void InitializeRequest(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            StartAddress = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));
            NumberOfPoints = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 4));
            ByteCount = (byte)NumberOfPoints;

            //Data = frame.Skip(3).ToArray();
        }

        public virtual void ValidateResponse(ITransportMessage response)
        {
            var typedResponse = response as ModbusMessage;

            if (FunctionCode != typedResponse.FunctionCode)
                throw new IOException(String.Format(CultureInfo.InvariantCulture, "Received response with unexpected Function Code. Expected {0}, received {1}.", FunctionCode, typedResponse.FunctionCode));

            if (SlaveAddress != typedResponse.SlaveAddress)
                throw new IOException(String.Format(CultureInfo.InvariantCulture, "Response slave address does not match request. Expected {0}, received {1}.", SlaveAddress, typedResponse.SlaveAddress));
        }

        public bool HasResponse
        {
            get { return true; }
            set { }
        }

        public int SearchLastEndLine
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int? AnswerLen
        {
            get
            {
                return null;
            }

            set
            {
                throw new NotImplementedException();
            }
        }


        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "Address {0}, Function {1}, Data: {2}", SlaveAddress, FunctionCode, Data != null ? Extensions.BytesToHexString(Data) : null);
        }
	}

    public class ReadHoldingInputRegistersRequest : ModbusMessage
    {
        public ReadHoldingInputRegistersRequest()
        {
        }

        public ReadHoldingInputRegistersRequest(byte functionCode, byte slaveAddress, ushort startAddress, ushort numberOfPoints)
            : base(slaveAddress, functionCode)
        {
            StartAddress = startAddress;
            NumberOfPoints = numberOfPoints;
        }

        public override int MinimumFrameSize
        {
            get { return _minimumFrameSize; }
        }

        public ushort NumberOfPoints
        {
            get
            {
                return base.NumberOfPoints.Value;
            }
            set
            {
                if (value > Modbus.MaximumRegisterRequestResponseSize)
                    throw new ArgumentOutOfRangeException("NumberOfPoints", String.Format(CultureInfo.InvariantCulture, "Maximum amount of data {0} registers.", Modbus.MaximumRegisterRequestResponseSize));

                base.NumberOfPoints = value;
            }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            base.ValidateResponse(response);

            var typedResponse = response as ModbusMessage;

            var expectedByteCount = NumberOfPoints * 2;
            if (expectedByteCount != typedResponse.ByteCount)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected byte count. Expected {0}, received {1}.",
                    expectedByteCount,
                    typedResponse.ByteCount));
        }

        public ushort GetRegister(int num)
        {
            ushort value;

            value = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(Data, num));

            return value;
        }
    }

    public class ReadCoilsInputsRequest : ModbusMessage
    {
        public ReadCoilsInputsRequest()
        {
        }

        public ReadCoilsInputsRequest(byte functionCode, byte slaveAddress, ushort startAddress, ushort numberOfPoints)
            : base(slaveAddress, functionCode)
        {
            StartAddress = startAddress;
            NumberOfPoints = numberOfPoints;
        }

        public override int MinimumFrameSize
        {
            get { return 6; }
        }

        public ushort NumberOfPoints
        {
            get
            {
                return base.NumberOfPoints.Value;
            }
            set
            {
                if (value > Modbus.MaximumDiscreteRequestResponseSize)
                    throw new ArgumentOutOfRangeException("NumberOfPoints", String.Format(CultureInfo.InvariantCulture, "Maximum amount of data {0} coils.", Modbus.MaximumDiscreteRequestResponseSize));

                base.NumberOfPoints = value;
            }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            base.ValidateResponse(response);

            var typedResponse = response as ModbusMessage;

            // best effort validation - the same response for a request for 1 vs 6 coils (same byte count) will pass validation.
            var expectedByteCount = (NumberOfPoints + 7) / 8;
            if (expectedByteCount != typedResponse.ByteCount)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected byte count. Expected {0}, received {1}.",
                    expectedByteCount,
                    typedResponse.ByteCount));
        }
    }

    public class WriteSingleCoilRequest : ModbusMessage
    {
        public WriteSingleCoilRequest()
        {

        }

        public WriteSingleCoilRequest(byte slaveAddress, ushort startAddress, bool coilState)
            : base(slaveAddress, Modbus.WriteSingleCoil)
        {
            StartAddress = startAddress;
            Data = coilState ? Modbus.CoilOn : Modbus.CoilOff;
        }

        public override int MinimumFrameSize
        {
            get { return 6; }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            base.ValidateResponse(response);

            var typedResponse = response as ModbusMessage;

            if (Data.First() != typedResponse.Data.First())
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected data in response. Expected {0}, received {1}.",
                    Data.First(),
                    typedResponse.Data.First()));
        }
        public override void Initialize(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            if (frame.Length < MinimumFrameSize)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Message frame must contain at least {0} bytes of data.", MinimumFrameSize));

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            StartAddress = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));

            Data = frame.Skip(4).ToArray();
        }
    }

    public class WriteSingleRegisterRequest : ModbusMessage
    {
        public WriteSingleRegisterRequest()
        {

        }

        public WriteSingleRegisterRequest(byte slaveAddress, ushort startAddress, ushort registerValue)
            : base(slaveAddress, Modbus.WriteSingleRegister)
        {
            StartAddress = startAddress;
            byte[] data = BitConverter.GetBytes(registerValue);
            Data = new byte[] { data[1], data[0] };
        }

        public override int MinimumFrameSize
        {
            get { return 6; }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            base.ValidateResponse(response);

            var typedResponse = response as ModbusMessage;

            if (Data[0] != typedResponse.Data[0] && Data[1] != typedResponse.Data[1])
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected data in response. Expected {0}, received {1}.",
                    Data.First(),
                    typedResponse.Data.First()));
        }

        public override void Initialize(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            if (frame.Length < MinimumFrameSize)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Message frame must contain at least {0} bytes of data.", MinimumFrameSize));

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            StartAddress = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));

            Data = frame.Skip(4).ToArray();
        }
    }

    public class WriteMultipleCoilsRequest : ModbusMessage
    {
        public WriteMultipleCoilsRequest()
        {
        }

        public WriteMultipleCoilsRequest(byte slaveAddress, ushort startAddress, IList<bool> data)
            : base(slaveAddress, Modbus.WriteMultipleCoils)
        {
            StartAddress = startAddress;
            NumberOfPoints = (ushort)data.Count;
            ByteCount = (byte)((NumberOfPoints + 7) / 8);

            BitArray bitArray = new BitArray(data.ToArray());

            Data = new byte[(int)ByteCount];
            bitArray.CopyTo(Data, 0);
        }

        public ushort NumberOfPoints
        {
            get
            {
                return base.NumberOfPoints.Value;
            }
            set
            {
                if (value > Modbus.MaximumDiscreteRequestResponseSize)
                    throw new ArgumentOutOfRangeException("NumberOfPoints", String.Format(CultureInfo.InvariantCulture, "Maximum amount of data {0} coils.", Modbus.MaximumDiscreteRequestResponseSize));

                base.NumberOfPoints = value;
            }
        }

        public override int MinimumFrameSize
        {
            get { return 7; }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            base.ValidateResponse(response);

            var typedResponse = response as ModbusMessage;

            if (NumberOfPoints != typedResponse.NumberOfPoints)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected number of points in response. Expected {0}, received {1}.",
                    NumberOfPoints,
                    typedResponse.NumberOfPoints));
        }

        public override void Initialize(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            StartAddress = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));
            NumberOfPoints = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 4));
            
            Data = frame.Skip(6).ToArray();
        }
    }

    public class WriteMultipleRegistersRequest : ModbusMessage
    {
        public WriteMultipleRegistersRequest()
        {
        }

        public WriteMultipleRegistersRequest(byte slaveAddress, ushort startAddress, IList<ushort> data)
            : base(slaveAddress, Modbus.WriteMultipleRegisters)
        {
            StartAddress = startAddress;
            NumberOfPoints = (ushort)data.Count;
            ByteCount = (byte)(data.Count * 2);

            List<byte> bytes = new List<byte>(NumberOfPoints * 2);

            foreach (ushort register in data)
                bytes.AddRange(BitConverter.GetBytes((ushort)IPAddress.HostToNetworkOrder((short)register)));

            Data = bytes.ToArray();
        }

        public WriteMultipleRegistersRequest(byte slaveAddress, ushort startAddress, byte[] data)
            : base(slaveAddress, Modbus.WriteMultipleRegisters)
        {
            StartAddress = startAddress;
            NumberOfPoints = (ushort) (data.Length / 2);
            ByteCount = (byte) data.Length;

            Data = data;
        }

        public ushort NumberOfPoints
        {
            get
            {
                return base.NumberOfPoints.Value;
            }
            set
            {
                if (value > Modbus.MaximumRegisterRequestResponseSize)
                    throw new ArgumentOutOfRangeException("NumberOfPoints", String.Format(CultureInfo.InvariantCulture, "Maximum amount of data {0} registers.", Modbus.MaximumRegisterRequestResponseSize));

                base.NumberOfPoints = value;
            }
        }

        public override int MinimumFrameSize
        {
            get { return 7; }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            base.ValidateResponse(response);

            var typedResponse = response as ModbusMessage;

            if (NumberOfPoints != typedResponse.NumberOfPoints)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected number of points in response. Expected {0}, received {1}.",
                    NumberOfPoints,
                    typedResponse.NumberOfPoints));
        }

        public override void Initialize(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            StartAddress = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));
            NumberOfPoints = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 4));
            //ByteCount = frame[6];

            Data = frame.Skip(6).ToArray();
        }
    }

    public class WriteFileRecordRequest : ModbusMessage
    {
        private byte referenceType;
        private ushort fileNumber;
        private ushort recordNumber;
        private ushort recordLength;

        private bool hasRecordInfo;

        public WriteFileRecordRequest()
        {
        }

        public WriteFileRecordRequest(byte slaveAddress, byte referenceType, ushort fileNumber, ushort recordNumber, byte[] data)
            : base(slaveAddress, Modbus.WriteFileRecord)
        {
            this.referenceType = referenceType;
            this.fileNumber = fileNumber;
            this.recordNumber = recordNumber;

            Data = data;
            recordLength = Convert.ToByte(data.Length);
            ByteCount = Convert.ToByte(recordLength + 7);
            hasRecordInfo = true;
        }

        public WriteFileRecordRequest(byte slaveAddress, ushort fileNumber, byte[] data)
            : base(slaveAddress, Modbus.WriteFileRecord)
        {
            this.referenceType = 0x06;
            this.fileNumber = fileNumber;
            this.hasRecordInfo = false;

            Data = data;
            ByteCount = Convert.ToByte(data.Length + 3);
        }

        public override byte[] MessageFrame
        {
            get
            {
                List<byte> frame = new List<byte>();
                frame.Add(SlaveAddress);
                frame.Add(FunctionCode);
                frame.Add(ByteCount.Value);
                frame.Add(referenceType);
                frame.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)fileNumber)));
                if (hasRecordInfo)
                {
                    frame.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)recordNumber)));
                    frame.AddRange(BitConverter.GetBytes(IPAddress.HostToNetworkOrder((short)recordLength)));
                }
                frame.AddRange(Data);

                return frame.ToArray();
            }
        }

        public override int MinimumFrameSize
        {
            get { return 9; }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            var typedResponse = response as ModbusMessage;

            if (FunctionCode != typedResponse.FunctionCode)
                throw new IOException(String.Format(CultureInfo.InvariantCulture, "Received response with unexpected Function Code. Expected {0}, received {1}.", FunctionCode, typedResponse.FunctionCode));

            if (ByteCount != typedResponse.ByteCount)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected number of bytes receibed in response. Expected {0}, received {1}.",
                    ByteCount,
                    typedResponse.ByteCount));
        }

        public override void Initialize(byte[] frame)
        {
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            SlaveAddress = frame[0];
            FunctionCode = frame[1];
            ByteCount = frame[2];
            referenceType = frame[3];
            fileNumber = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 4));
            recordNumber = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 6));
            recordLength = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 8));

            Data = frame.Skip(11).ToArray();
        }
    }

    public static class ModbusMessageFactory
    {
        private const int MinRequestFrameLength = 3;

        public static T CreateModbusMessage<T>(byte[] frame) where T : ModbusMessage, new()
        {
            ModbusMessage message = new T();
            message.Initialize(frame);

            return (T)message;
        }

        public static ModbusMessage CreateModbusRequest(byte[] frame)
        {
            if (frame.Length < MinRequestFrameLength)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Argument 'frame' must have a length of at least {0} bytes.", MinRequestFrameLength));

            ModbusMessage request = null;
            byte functionCode = frame[1];

            switch (functionCode)
            {
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                    request = CreateModbusMessage<ReadCoilsInputsRequest>(frame);
                    break;
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                    request = CreateModbusMessage<ReadHoldingInputRegistersRequest>(frame);
                    break;
                case Modbus.WriteSingleCoil:
                    request = CreateModbusMessage<WriteSingleCoilRequest>(frame);
                    break;
                case Modbus.WriteSingleRegister:
                    request = CreateModbusMessage<WriteSingleRegisterRequest>(frame);
                    break;
                /*case Modbus.Diagnostics:
                    request = CreateModbusMessage<DiagnosticsRequestResponse>(frame);
                    break;*/
                case Modbus.WriteMultipleCoils:
                    request = CreateModbusMessage<WriteMultipleCoilsRequest>(frame);
                    break;
                case Modbus.WriteMultipleRegisters:
                    request = CreateModbusMessage<WriteMultipleRegistersRequest>(frame);
                    break;
                /*case Modbus.ReadWriteMultipleRegisters:
                    request = CreateModbusMessage<ReadWriteMultipleRegistersRequest>(frame);
                    break;*/
                default:
                    throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Unsupported function code {0}", functionCode), "frame");
            }

            return request;
        }

        public static T CreateModbusRequestMessage<T>(byte[] frame) where T : ModbusMessage, new()
        {
            ModbusMessage message = new T();
            message.InitializeRequest(frame);

            return (T)message;
        }

        public static ModbusMessage CreateModbusRequestMessage(byte[] frame)
        {
            if (frame.Length < MinRequestFrameLength)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Argument 'frame' must have a length of at least {0} bytes.", MinRequestFrameLength));

            ModbusMessage request = null;
            byte functionCode = frame[1];

            switch (functionCode)
            {
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                    request = CreateModbusRequestMessage<ReadCoilsInputsRequest>(frame);
                    break;
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                    request = CreateModbusRequestMessage<ReadHoldingInputRegistersRequest>(frame);
                    break;
                case Modbus.WriteSingleCoil:
                    request = CreateModbusRequestMessage<WriteSingleCoilRequest>(frame);
                    break;
                case Modbus.WriteSingleRegister:
                    request = CreateModbusRequestMessage<WriteSingleRegisterRequest>(frame);
                    break;
                case Modbus.WriteMultipleCoils:
                    request = CreateModbusRequestMessage<WriteMultipleCoilsRequest>(frame);
                    break;
                case Modbus.WriteMultipleRegisters:
                    request = CreateModbusRequestMessage<WriteMultipleRegistersRequest>(frame);
                    break;
                default:
                    throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Unsupported function code {0}", functionCode), "frame");
            }

            return request;
        }
    }
}
