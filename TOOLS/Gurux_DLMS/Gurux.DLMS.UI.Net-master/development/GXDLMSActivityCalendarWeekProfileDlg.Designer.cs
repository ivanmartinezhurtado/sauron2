﻿namespace Gurux.DLMS.UI
{
    partial class GXDLMSActivityCalendarWeekProfileDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelBtn = new System.Windows.Forms.Button();
            this.OkBtn = new System.Windows.Forms.Button();
            this.NameTb = new System.Windows.Forms.TextBox();
            this.NameLbl = new System.Windows.Forms.Label();
            this.MondayCb = new System.Windows.Forms.CheckBox();
            this.TuesdayCb = new System.Windows.Forms.CheckBox();
            this.WednesdayCb = new System.Windows.Forms.CheckBox();
            this.ThursdayCb = new System.Windows.Forms.CheckBox();
            this.FridayCb = new System.Windows.Forms.CheckBox();
            this.SaturdayCb = new System.Windows.Forms.CheckBox();
            this.SundayCb = new System.Windows.Forms.CheckBox();
            this.AsciiBtn = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // CancelBtn
            // 
            this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(259, 207);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 7;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            // 
            // OkBtn
            // 
            this.OkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkBtn.Location = new System.Drawing.Point(178, 207);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(75, 23);
            this.OkBtn.TabIndex = 6;
            this.OkBtn.Text = "OK";
            this.OkBtn.UseVisualStyleBackColor = true;
            this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
            // 
            // NameTb
            // 
            this.NameTb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameTb.Location = new System.Drawing.Point(91, 12);
            this.NameTb.Name = "NameTb";
            this.NameTb.Size = new System.Drawing.Size(162, 20);
            this.NameTb.TabIndex = 22;
            // 
            // NameLbl
            // 
            this.NameLbl.AutoSize = true;
            this.NameLbl.Location = new System.Drawing.Point(11, 12);
            this.NameLbl.Name = "NameLbl";
            this.NameLbl.Size = new System.Drawing.Size(38, 13);
            this.NameLbl.TabIndex = 23;
            this.NameLbl.Text = "Name:";
            // 
            // MondayCb
            // 
            this.MondayCb.AutoSize = true;
            this.MondayCb.Location = new System.Drawing.Point(91, 38);
            this.MondayCb.Name = "MondayCb";
            this.MondayCb.Size = new System.Drawing.Size(64, 17);
            this.MondayCb.TabIndex = 24;
            this.MondayCb.Text = "Monday";
            this.MondayCb.UseVisualStyleBackColor = true;
            // 
            // TuesdayCb
            // 
            this.TuesdayCb.AutoSize = true;
            this.TuesdayCb.Location = new System.Drawing.Point(91, 61);
            this.TuesdayCb.Name = "TuesdayCb";
            this.TuesdayCb.Size = new System.Drawing.Size(70, 17);
            this.TuesdayCb.TabIndex = 25;
            this.TuesdayCb.Text = "Tuesday ";
            this.TuesdayCb.UseVisualStyleBackColor = true;
            // 
            // WednesdayCb
            // 
            this.WednesdayCb.AutoSize = true;
            this.WednesdayCb.Location = new System.Drawing.Point(91, 84);
            this.WednesdayCb.Name = "WednesdayCb";
            this.WednesdayCb.Size = new System.Drawing.Size(83, 17);
            this.WednesdayCb.TabIndex = 26;
            this.WednesdayCb.Text = "Wednesday";
            this.WednesdayCb.UseVisualStyleBackColor = true;
            // 
            // ThursdayCb
            // 
            this.ThursdayCb.AutoSize = true;
            this.ThursdayCb.Location = new System.Drawing.Point(91, 107);
            this.ThursdayCb.Name = "ThursdayCb";
            this.ThursdayCb.Size = new System.Drawing.Size(70, 17);
            this.ThursdayCb.TabIndex = 27;
            this.ThursdayCb.Text = "Thursday";
            this.ThursdayCb.UseVisualStyleBackColor = true;
            // 
            // FridayCb
            // 
            this.FridayCb.AutoSize = true;
            this.FridayCb.Location = new System.Drawing.Point(91, 130);
            this.FridayCb.Name = "FridayCb";
            this.FridayCb.Size = new System.Drawing.Size(54, 17);
            this.FridayCb.TabIndex = 28;
            this.FridayCb.Text = "Friday";
            this.FridayCb.UseVisualStyleBackColor = true;
            // 
            // SaturdayCb
            // 
            this.SaturdayCb.AutoSize = true;
            this.SaturdayCb.Location = new System.Drawing.Point(91, 153);
            this.SaturdayCb.Name = "SaturdayCb";
            this.SaturdayCb.Size = new System.Drawing.Size(68, 17);
            this.SaturdayCb.TabIndex = 29;
            this.SaturdayCb.Text = "Saturday";
            this.SaturdayCb.UseVisualStyleBackColor = true;
            // 
            // SundayCb
            // 
            this.SundayCb.AutoSize = true;
            this.SundayCb.Location = new System.Drawing.Point(91, 176);
            this.SundayCb.Name = "SundayCb";
            this.SundayCb.Size = new System.Drawing.Size(62, 17);
            this.SundayCb.TabIndex = 30;
            this.SundayCb.Text = "Sunday";
            this.SundayCb.UseVisualStyleBackColor = true;
            // 
            // AsciiBtn
            // 
            this.AsciiBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AsciiBtn.Location = new System.Drawing.Point(266, 15);
            this.AsciiBtn.Name = "AsciiBtn";
            this.AsciiBtn.Size = new System.Drawing.Size(68, 17);
            this.AsciiBtn.TabIndex = 31;
            this.AsciiBtn.Text = "ASCII";
            this.AsciiBtn.UseVisualStyleBackColor = false;
            // 
            // GXDLMSActivityCalendarWeekProfileDlg
            // 
            this.AcceptButton = this.OkBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(346, 242);
            this.Controls.Add(this.AsciiBtn);
            this.Controls.Add(this.SundayCb);
            this.Controls.Add(this.SaturdayCb);
            this.Controls.Add(this.FridayCb);
            this.Controls.Add(this.ThursdayCb);
            this.Controls.Add(this.WednesdayCb);
            this.Controls.Add(this.TuesdayCb);
            this.Controls.Add(this.MondayCb);
            this.Controls.Add(this.NameTb);
            this.Controls.Add(this.NameLbl);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.OkBtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GXDLMSActivityCalendarWeekProfileDlg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Week Profile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button OkBtn;
        private System.Windows.Forms.TextBox NameTb;
        private System.Windows.Forms.Label NameLbl;
        private System.Windows.Forms.CheckBox MondayCb;
        private System.Windows.Forms.CheckBox TuesdayCb;
        private System.Windows.Forms.CheckBox WednesdayCb;
        private System.Windows.Forms.CheckBox ThursdayCb;
        private System.Windows.Forms.CheckBox FridayCb;
        private System.Windows.Forms.CheckBox SaturdayCb;
        private System.Windows.Forms.CheckBox SundayCb;
        private System.Windows.Forms.CheckBox AsciiBtn;
    }
}