namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_VALORPARAMETROLOG")]
    public partial class T_VALORPARAMETROLOG
    {
        [Key]
        [Column(Order = 0)]
        public DateTime FECHA { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMGRUPO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARAM { get; set; }

        public int? NUMPRODUCTO { get; set; }

        public int? VERSION { get; set; }

        [StringLength(200)]
        public string OLDVALOR { get; set; }

        [StringLength(200)]
        public string NEWVALOR { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(30)]
        public string USUARIO { get; set; }

        [StringLength(600)]
        public string NOTAS { get; set; }
    }
}
