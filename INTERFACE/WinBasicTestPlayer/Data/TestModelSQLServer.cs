﻿using System;

namespace WinTestPlayer.Data
{
    public class TestModelSQLServer : DataBaseService, ITestModelDataBaseService
    {
        public TestModelSQLServer()
        {
            base.DatabaseType = DatabaseTypeEnum.SQLServer;
        }

        public int GetNumEquiposOK(string lote)
        {
            var sqlComand = string.Format("select count(*) from Test where Lote = '{0}' and Result = 'OK'", lote);
            var result = base.ExecuteScalar(sqlComand);
            if (result == null)
                return 0;
            else
                return Convert.ToInt32(result);
        }

        public int GetNumEquiposNG(string lote)
        {
            var sqlComand = string.Format("select count(*) from Test where Lote = '{0}' and Result = 'KO'", lote);
            var result = base.ExecuteScalar(sqlComand);
            if (result == null)
                return 0;
            else
                return Convert.ToInt32(result);
        }

        public void Dispose()
        {

        }
    }
}
