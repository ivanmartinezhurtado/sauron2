﻿using Dezac.Device.Protection;
using Dezac.Device.ThermalStations;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.07)]
    public class EKOR_MODULAR_CPUTestPlacas : TestBase
    {
        public EKOR_PLUS EkorCPU;
        private TowerBoard tower;
        public double shuntConsumptionResistance = 0.47D;

        #region Constants
        internal const int OUT_ACTIVATE_POWER_SUPPLY = 17;
        internal const int OUT_POWER_SUPPLY_SWITCH = 20;

        #endregion

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("There is not any tower instance to initializate in this test");

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.OnWait(200, 14);
            tower.IO.DO.OnWait(1000, OUT_ACTIVATE_POWER_SUPPLY); //17
        }

        public void TestComunicationsTTL(int porEkorttL5)
        {
            if (porEkorttL5 == 0)
                porEkorttL5 = Comunicaciones.SerialPort;

            EkorCPU = new EKOR_PLUS(porEkorttL5);
            EkorCPU.Cirbus.PerifericNumber = Comunicaciones.Periferico;
            EkorCPU.Cirbus.BaudRate = 9600;
            EkorCPU.Cirbus.Parity = System.IO.Ports.Parity.Even;

            string developerVersion = "";
            string firmwareVersion = "";

            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    EkorCPU.RecoveryDefaultSettings(porEkorttL5, 9600, 1, System.IO.Ports.Parity.Even);
                    Delay(2000, "");

                    EkorCPU.Cirbus.BaudRate = 9600;
                    EkorCPU.Cirbus.Parity = System.IO.Ports.Parity.Even;
                    EkorCPU.Cirbus.PerifericNumber = 0;
                }
                if (p > 1)
                {
                    logger.InfoFormat("Comprobamos configuracion del equipo en fase final de test (38400, n, 0)");
                    EkorCPU.Cirbus.BaudRate = 38400;
                    EkorCPU.Cirbus.Parity = System.IO.Ports.Parity.None;
                    EkorCPU.Cirbus.PerifericNumber = 0;
                }

                developerVersion = EkorCPU.ReadVersion(); logger.InfoFormat("Puerto de comunicacion TTL asignado en  COM {0}", porEkorttL5.ToString());
                return true;

            }, "Error. No comunica el equipo en TTL", 3, 500, 1000, false, false);

            Assert.AreEqual("VERSION_DEVELOPER", developerVersion.Trim(), Identificacion.VERSION_EMBEDDED.Trim(),
            Error().UUT.CONFIGURACION.NO_COINCIDE("Error en la programación del equipo, la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                firmwareVersion = EkorCPU.ReadFirmware();
                logger.InfoFormat("Puerto de comunicacion TTL asignado en  COM {0}", porEkorttL5.ToString());
                return true;
            }, "Error. No comunica el equipo", 3, 500, 1000, false, false);

            Resultado.Set("TEST_TTL", "OK", ParamUnidad.s);

            Assert.AreEqual("VERSION_FIRMWARE_COM_CIRBUS", firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de firmware es incorrecta"), ParamUnidad.SinUnidad);

            EkorCPU.WriteDate(DateTime.Now);

            EkorCPU.WriteFrameNumber(TestInfo.NumBastidor.ToString());

            EkorCPU.WriteRemoveCounter();

            EkorCPU.WriteAngleCalibrationInitialization();

            //EkorCPU.WriteInstallerSettings("0143 270f 0A 00 00 00 1964 06 0004 0096 0003 ffa6 00 01 00");
            //Delay(2000, "Escribiendo ajustes usuario para inicio test");

            //EkorCPU.Cirbus.BaudRate = 9600;
            //EkorCPU.Cirbus.Parity = System.IO.Ports.Parity.Even;
            //EkorCPU.Cirbus.PerifericNumber = 0;

            //EkorCPU.WriteUserSettings("04b0 012c 003c 003c 003c 003c 00 24 00 00 00 00 0000 0001 0258 00 ff 00");
            //Delay(2000, "Escribiendo ajustes usuario para inicio test");
        }

        public void TestConsumo()
        {                      
            tower.IO.DO.OffWait(1000, OUT_POWER_SUPPLY_SWITCH);

            var consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("ALIMENTACION").Name, 0, Params.I_DC.Null.TestPoint("ALIMENTACION").Min(), Params.I_DC.Null.TestPoint("ALIMENTACION").Max(), 0, 0, ParamUnidad.mA);

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltDC,
                NumMuestras = 3,
                Rango = consumption.Max,
                DigitosPrecision = 4,
                NPLC = 0
            };

            TestMeasureBase(consumption, (s) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, measure, 1500) * 2128;
            }, 1, 5, 2000);
        }

        public void EmptyModeMeasurement()
        {
            //Se tendría que activar esta salida para poder probar los consumos en vacío del equipo, pero el equipo no comunica con la salida encendida... TSD2013 no se hace esta prueba 
            //tower.IO.DO.OnWait(500, OUT_POWER_SUPPLY_SWITCH);
             
            SamplerWithCancel((p) =>
            {
                var Emmptymode = EkorCPU.ReadMeasurementsCurrentDisplay();
                logger.InfoFormat("Error comunicaciones medida en vacio");

                return true;
            }, "Error. No comunica el equipo", 3, 500, 1000, false, false);

            TriAdjustValueDef lineMeasurements = new TriAdjustValueDef(Params.V.L1.EnVacio.Name, Params.V.Null.EnVacio.Min(), Params.V.Null.EnVacio.Max(), 0, 0, ParamUnidad.mV, true);

            TestMeasureBase(lineMeasurements,
                (step) =>
                {
                    var result = EkorCPU.ReadMeasurementsVolageDisplay();
                    var VoltageList = new List<double>();
                    VoltageList.Add(result.L1);
                    VoltageList.Add(result.L2);
                    VoltageList.Add(result.L3);
                    VoltageList.Add(result.LH);

                    return VoltageList.ToArray();
                }, 2, 5, 500);

            TriAdjustValueDef lineMeasurements2 = new TriAdjustValueDef(Params.I_AC.L1.EnVacio.Name, Params.I_AC.Null.EnVacio.Min(), Params.I_AC.Null.EnVacio.Max(), 0, 0, ParamUnidad.mA, true);

            TestMeasureBase(lineMeasurements2,
                (step) =>
                {
                    var result = EkorCPU.ReadMeasurementsCurrentDisplay();
                    var CurrentList = new List<double>();
                    CurrentList.Add(result.L1);
                    CurrentList.Add(result.L2);
                    CurrentList.Add(result.L3);
                    CurrentList.Add(result.LH);
                    return CurrentList.ToArray();

                }, 2, 5, 500);

            tower.IO.DO.Off(OUT_POWER_SUPPLY_SWITCH);
        }

        //public void ClockCalibration(string portCrono)
        //{
        //    tower.Chroma.SetVoltageRange(Instruments.PowerSource.Chroma.VoltageRangeEnum.V300);
        //    tower.Chroma.ApplyAndWaitStabilisation(230, 0, 50);

        //    if (string.IsNullOrEmpty(portCrono))
        //        throw new Exception("No se ha pasado el puerto del CronoThermostato");

        //    var cronoPort = Convert.ToByte(GetVariable<string>(portCrono, portCrono).Replace("COM", ""));

        //    double temperature = 0;
        //    Delay(15000, "Inicio cronotermo");

        //    SamplerWithCancel((p) =>
        //        {
        //            using (var crono = new CRONOTERMOSTATO(cronoPort))
        //            {
        //                crono.Modbus.BaudRate = 19200;
        //                var temp = new AdjustValueDef(Params.TEMP.Null.RTC.Name, 0, Params.TEMP.Null.RTC.Min(), Params.TEMP.Null.RTC.Max(), 0, 0, ParamUnidad.Grados);
        //                var result = TestMeasureBase(temp,
        //                   (step) =>
        //                   {
        //                       return crono.ReadTemperatureAndConvertToReal();
        //                   }, 0, 3, 2000);

        //                temperature = temp.Value;
        //            }
        //            return true;
        //        }, string.Empty, cancelOnException: false, throwException: false);

        //    tower.Chroma.ApplyOff();

        //    tower.HP53131A
        //       .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
        //       .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 1.5)
        //       .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

        //    var defs = new List<AdjustValueDef>();

        //    defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
        //    defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
        //    defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
        //    defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
        //    defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

        //    TestMeasureBase(defs,
        //    (step) =>
        //    {
        //        logger.InfoFormat("Configurando la tolerancia del Error en ppm");

        //        EkorCPU.StartSignalClok(15); // genera una señal de pulsos durante el tiempo especificado inhabilitando comunicaciones

        //        var medidaFreqMed = tower.MeasureWithFrecuency(InputMuxEnum.IN3, 8000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);

        //        Delay(15000, "Espera a que acabe de generar el tren de pulsos");
        //        var calibrationClk = EkorCPU.CalibrationClock(medidaFreqMed, temperature);

        //        return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
        //                                calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
        //    }, 0, 2, 1000);

        //    logger.InfoFormat("Almacenando todos los errores en base de datos");

        //    EkorCPU.FLASHFormat();
        //    Delay(5000, "Esperando formateo de la FLASH");

        //    SamplerWithCancel((p) =>
        //    {
        //        var info = EkorCPU.ReadFLASHInformation();

        //        if (info.Contains("ID"))
        //            return true;
        //        return false;

        //    }, "Error. No se ha podido formatear la unidad logica FLASH", 10, 1000, 0);

        //    Resultado.Set("FLASH_FORMAT", "OK", ParamUnidad.SinUnidad);

        //    //EkorCPU.WriteInstallerSettings("0105 270f 0A 00 00 00 1964 06 0004 0096 0003 ffa6 00 00 00");
        //    //Delay(2000, "Configuramos equipo para salida a ajuste y verificación");
        //}

        public void ClockCalibration(string portOptris)
        {
            //tower.Chroma.SetVoltageRange(Instruments.PowerSource.Chroma.VoltageRangeEnum.V300);
            //tower.Chroma.ApplyAndWaitStabilisation(230, 0, 50);     

            //if (string.IsNullOrEmpty(portOptris))
            //    throw new Exception("No se ha pasado el puerto del Thermometro Optris");         

            //var optrisPort = Convert.ToByte(GetVariable<string>(portOptris, portOptris).Replace("COM", ""));
            //tower.IO.DO.On(15); //Activa ventilador ajusto ppm

            double temperature = 21.5;
            Delay(15000, "Inicio cronotermo");

            //SamplerWithCancel((p) =>
            //{               
            //    using (var thermometer = new OPTRIS_CS(optrisPort))
            //    {
            //        thermometer.Initialization();

            //        var temp = new AdjustValueDef(Params.TEMP.Null.RTC.Name, 0, Params.TEMP.Null.RTC.Min(), Params.TEMP.Null.RTC.Max(), 0, 0, ParamUnidad.Grados);
            //        var result = TestMeasureBase(temp,
            //           (step) =>
            //           {
            //               return thermometer.ReadTemperature();
            //           }, 0, 3, 2000);

            //        temperature = temp.Value;
            //        Resultado.Set("TEMPERATURA", temperature, ParamUnidad.Grados);
            //    }

            //    return true;
            //}, string.Empty, cancelOnException: false, throwException: false);

            //tower.Chroma.ApplyOff();

            tower.HP53131A
               .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
               .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 1.5)
               .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            TestMeasureBase(defs,
            (step) =>
            {
                logger.InfoFormat("Configurando la tolerancia del Error en ppm");

                EkorCPU.StartSignalClok(15); // genera una señal de pulsos durante el tiempo especificado inhabilitando comunicaciones

                var medidaFreqMed = tower.MeasureWithFrecuency(InputMuxEnum.IN3, 8000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);

                Delay(15000, "Espera a que acabe de generar el tren de pulsos");
                var calibrationClk = EkorCPU.CalibrationClock(medidaFreqMed, temperature);

                return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
            }, 0, 2, 1000);

            tower.IO.DO.Off(15); //Desactiva ventilador ajusto ppm

            logger.InfoFormat("Almacenando todos los errores en base de datos");

            EkorCPU.FLASHFormat();
            Delay(5000, "Esperando formateo de la FLASH");

            SamplerWithCancel((p) =>
            {
                var info = EkorCPU.ReadFLASHInformation();

                if (info.Contains("ID"))
                    return true;
                return false;

            }, "Error. No se ha podido formatear la unidad logica FLASH", 10, 1000, 0);

            Resultado.Set("FLASH_FORMAT", "OK", ParamUnidad.SinUnidad);

            //EkorCPU.WriteInstallerSettings("0105 270f 0A 00 00 00 1964 06 0004 0096 0003 ffa6 00 00 00");
            //Delay(2000, "Configuramos equipo para salida a ajuste y verificación");
        }

        public void TestFinish()
        {
             if (EkorCPU != null)
                EkorCPU.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                tower.IO.DO.Off(OUT_ACTIVATE_POWER_SUPPLY, 20); //17

                tower.Dispose();
            }
        }

    }
}
