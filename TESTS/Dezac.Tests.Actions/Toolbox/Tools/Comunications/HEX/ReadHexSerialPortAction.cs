﻿using Comunications.Message;
using Dezac.Tests.Actions.Views;
using log4net;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WReadHexSerialPortActionDescription))]
    public class ReadHexSerialPortAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Texto que se debe enviar")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable TextToSend { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Longitud del mensaje de respuesta")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable AnswerLen { get; set; } = new ActionVariable();


        [Description("Nombre de la variable resultado")]
        [Category("4. Results")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataOutput { get; set; }

        [Category("3. Configuration")]
        [Description("Reintentos de comunicaciones")]
        [DefaultValue(200)]
        public int Retries { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de espera a recibir la trama antes de dar error")]
        [DefaultValue(200)]
        public int TimeOut { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_HEX")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM_HEX";
            var transport = GetVariable(name) as MessageTransport;
            if (transport == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("DeviceName").Throw();

            transport.ReadTimeout = TimeOut;
            transport.Retries = Retries;
            transport.LogRawMessage = true;

            HexStringMessage message, response;
            message = new HexStringMessage();

            if (AnswerLen != null && !AnswerLen.IsKeyNullOrEmpty)
                message.AnswerLen = Convert.ToInt32(AnswerLen.Value);
        
            message.Text = VariablesTools.ResolveText(TextToSend.Value.ToString());
            response = transport.SendMessage<HexStringMessage>(message);
            Logger.InfoFormat("{0} -> {1}", TextToSend, response);
            Context.ResultList.Add(string.Format("{0} -> {1}", TextToSend, response.ToString().Trim()));
            SetVariable(DataOutput, response.ToString().Trim());
        }
    }

    public class WReadHexSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "ReadHEXSerialPort"; } }
        public override string Description { get { return "Recibir la cadena de de texto especificada"; } }
        public override string Category { get { return "Tools/Comunications/HEX"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
        public override string Dependencies { get { return "InitHexSerialPort;DisposeHexSerialPort:2"; } }
    }
}
