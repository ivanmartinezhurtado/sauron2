﻿namespace TaskRunner.Enumerators
{
    public enum TestExecutionResult
    {
        Faulted = -1,
        Aborted = 0,
        Completed = 1,
    }
}
