﻿using Dezac.Core.Exceptions;
using Dezac.Services;
using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(GetNumSerieByFamilyDescription))]
    

    public class GetNumSerieByFamily: ActionBase
    {
        public string IdFamilia { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            string numSerieFamilia = null;

            if (data != null && data.TestInfo != null)
            {
                if (!string.IsNullOrEmpty(data.TestInfo.NumSerieFamilia))
                    return;

                var familia = string.IsNullOrEmpty(IdFamilia) ? data.IdFamilia : IdFamilia;

                if (data.modoPlayer == ModoTest.PostVenta)
                    numSerieFamilia = ControlPostventaNumSerie(data);

                if (string.IsNullOrEmpty(numSerieFamilia))
                    using (DezacService svc = new DezacService())
                    {
                        numSerieFamilia = svc.GetNumSerie(data.NumProducto, data.Version, 0, familia);
                    }

                Logger.InfoFormat("BBDD Oracle -> Numero Serie Familia: {0}", numSerieFamilia);

                if (string.IsNullOrEmpty(numSerieFamilia))
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("NUMERO SERIE NO EXISTE PARA ESTA FAMILIA").Throw();

                data.TestInfo.NumSerieFamilia = numSerieFamilia.Trim();
            }
            else
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestConext").Throw();
        }

        private string ControlPostventaNumSerie(ITestContext data)
        {
            var context = SequenceContext.Current;
            var shell = context.Services.Get<IShell>();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            string msg = "";
            string numSerie = "";


            msg = "ASIGNAR NUMERO DE SERIE FAMILIA MANUAL (SI) - ASIGNAR NUEVO NUMERO DE SERIE FAMILIA (NO)";

            var result1 = shell.MsgBox(msg, string.Format("ASISTENCIA TÉCNICA {0}/{1}", data.NumAsistencia, data.NumLineaAsistencia), MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
                numSerie = testBase.GetIntroNumSerieByUser();
            else if (result1 == DialogResult.No)
                numSerie = "";
 
            return numSerie;
        }
    }

    public class GetNumSerieByFamilyDescription : ActionDescription
    {
        public override string Name { get { return "GetNumSerieByFamily"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
