﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using WinTestPlayer.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class Header : UserControl
    {
        protected AppService app;
        protected ReportsService reportService;
        protected ShellService shell;
        protected int idPorcess = -1;
        protected TestModel model;


        public Header()
        {
            InitializeComponent();
            btNumSerieLabel.ButtonElement.Shortcuts.Add(new RadShortcut(Keys.None, Keys.F4));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            app = DI.Resolve<AppService>();
            reportService = DI.Resolve<ReportsService>();
            shell = DI.Resolve<ShellService>();
            app.TestInfoChanged += (s, ee) => { UpdateUI(); };
            app.ModelChanged += (s, ee) => { UpdateUI(); };
            app.Runner.AllTestEnded += (s, ee) =>
            {
                UpdateUI();
            };

            model = app.Model;

            UpdateUI();

        }

        private void UpdateUI()
        {
            btPlay.Enabled = model.Producto != null;
            btPlay.ImageIndex = !app.IsRunning ? 2 : 3;

            lblNumOrden.Text = model.Orden != null ? model.Orden.NUMORDEN.ToString() : null;
            lblDesc.Text = model.Producto != null ? model.Producto.ARTICULO.DESCRIPCION : null;
            lblFase.Text = model.IdFase != null ? string.Format("{0}-{1}", model.IdFase, model.ProductoFase != null ? model.ProductoFase.FASE.DESCRIPCION : null) : null;
            lblOperario.Text = string.Format("{0}-{1}", model.IdOperario, model.Operario);
            lblCantidad.Text = model.Orden != null ? model.Orden.CANTIDAD.ToString() : string.Empty;
            imageAssignedNumSerie.Visible = model.FaseNumSerieAssigned;
            string version = model.Producto != null ? model.Producto.VERSION.ToString() : null;
            string producto = model.Producto != null ? model.Producto.NUMPRODUCTO.ToString() : null;
            if (!string.IsNullOrEmpty(producto) && !string.IsNullOrEmpty(version))
                lblProducto.Text = producto + " / " + version;

            UpdateTestInfo();

            switch (model.ModoPlayer)
            {
                case ModoPlayer.AutoTest:
                    pnlLeft.BackColor = Color.Black;
                    break;
                case ModoPlayer.PostVenta:
                    pnlLeft.BackColor = Color.DarkViolet;
                    break;
                case ModoPlayer.SAT:
                    pnlLeft.BackColor = Color.OrangeRed;
                    break;
                case ModoPlayer.TIC:
                    pnlLeft.BackColor = Color.DarkGreen;
                    break;
                case ModoPlayer.PrintLabel:
                    pnlLeft.BackColor = Color.DarkTurquoise;
                    break;
                default:
                    pnlLeft.BackColor = Color.RoyalBlue;
                    break;
            }
        }

        private void UpdateTestInfo()
        {
            var sb = new StringBuilder();

            app.TestContexts
                .Where(p => p.Value.TestInfo != null)
                .ToList()
                .ForEach(p =>
                {
                    sb.AppendLine(p.Value.TestInfo.NumBastidor.ToString());
                });

            lblMatricula.Text = sb.ToString();

            TestModel model = app.Model;
        }

        private void btNumSerieLabel_Click(object sender, EventArgs e)
        {
            PrintLabelNumSerie();
        }

        private void btInstrucciones_Click(object sender, EventArgs e)
        {
            int numProducto = app.Model.Producto.NUMPRODUCTO;
            int version = app.Model.Producto.VERSION;
            string fase = app.Model.IdFase;

            string url = string.Format(@"http://portalweb.dezac.com:8080/?idEmpleado={0}&numOrden={1}#/home", app.Model.IdOperario, app.Model.NumOrden);
            System.Diagnostics.Process.Start(url);
        }

        private void btPlay_Click(object sender, EventArgs e)
        {
            if (!app.IsRunning)
                app.Play();
            else
            {
                btPlay.Enabled = false;
                app.Runner.Stop();
            }
        }

        //*****************************************************

        private void PrintLabelNumSerie()
        {
            try
            {
                ReportsService.DatosEquipoToPrint Current = reportService.GetdatosByBastidor(app.Model.ModoPlayer == ModoPlayer.PostVenta);
                if (Current == null)
                    return;

                ReportsService.GetDatosBBDDToPrinLabel result = reportService.ConfigPrintLabels(app, Current);

                if (result.Context == null)
                    foreach (string label in result.NumCodeLabel)
                        reportService.PrintLabelCurrentNumSerie(result.TestFase, label, result.NumCopiesLabel, app.Model.PrinterLabel);
                else
                {
                    string labelDataFileName = result.TipoEtiqueta == "01" ? "PRINT_LABEL_Caracteristicas"
                                  : result.TipoEtiqueta == "02" ? "PRINT_LABEL_EmbalajeIndividual"
                                  : result.TipoEtiqueta == "03" ? "PRINT_LABEL_EmbalajeConjunto"
                                  : result.TipoEtiqueta == "04" ? "PRINT_LABEL_NumSerie"
                                  : result.TipoEtiqueta == "05" ? "PRINT_LABEL_EtiquetasEsquema"
                                  : result.TipoEtiqueta == "06" ? "PRINT_LABEL_EtiquetaPromocional"
                                  : "PRINT_LABEL_NumSerie";

                    string labelDataFile = reportService.GetFileLabelData(Current.NUMMATRICULA.Value, labelDataFileName);
                    if (!string.IsNullOrEmpty(labelDataFile))
                        reportService.PrintLabelFromDataFile(result.Context, result.TipoEtiqueta, labelDataFile, result.FileName, result.NumCopiesLabel);
                    else
                        reportService.GenerateLabelAndPrint(result.Context, result.TestFase, result.TipoEtiqueta, result.FileName, result.NumCopiesLabel);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al imprimir la etiqueta por " + ex.Message, "Print Test Lbel Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ParentForm.Enabled = true;
            }
        }
    }
}
