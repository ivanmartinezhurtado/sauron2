﻿using Circutor.Tests.MyCircutor;
using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AFQEvoMyCircutorRegisterActionDescription))]
    public class AFQEvoMyCircutorRegister : ActionBase
    {
        [Description("Valor o variable del UserName para la conexión a MyCircutor")]
        [Category("Login")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string UserName { get; set; }

        [Description("Valor o variable del password para la conexión a MyCircutor")]
        [Category("Login")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Password { get; set; }

        [Description("Valor o variable del Codigo del producto para registrarlo en MyCircutor")]
        [Category("Configuracion")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string CodigoProducto { get; set; }

        [Description("Valor o variable del Numero de serie del producto para registrarlo en MyCircutor")]
        [Category("Configuracion")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SerialNumber { get; set; }

        public bool DeveloperApi { get; set; }

        public override void Execute()
        {
            var testContext = Context.Services.Get<ITestContext>();
            if (testContext == null)
                throw new Exception("No se ha inicializado el producto!");

            var afqTest = new AFQTest();

            // Url de test
            if (DeveloperApi)
                afqTest.BaseUrl = "https://mycapi-dev.mycircutor.com:8085/mycapi/v1.0.0/";

            var userName = VariablesTools.ResolveValue(UserName).ToString();

            Logger.InfoFormat("Identificándose en myCircutor con el usuario {0} -> {1}", userName, afqTest.BaseUrl);

            afqTest.Login(userName, VariablesTools.ResolveValue(Password).ToString());

            var itemCode = (VariablesTools.ResolveValue(CodigoProducto) ?? testContext.Codigo ?? string.Empty).ToString();
            var serialNumber = (VariablesTools.ResolveValue(SerialNumber) ?? testContext.TestInfo.NumSerie ?? string.Empty).ToString();

            Assert.IsNotNullOrEmpty(itemCode, Error().UUT.CONFIGURACION.SETUP("El código de artículo no está informado!"));
            Assert.IsNotNullOrEmpty(serialNumber, Error().UUT.NUMERO_DE_SERIE.NO_GRABADO("No existe nº de serie del equipo!"));

            Logger.InfoFormat("Verificando registro AFQ en myCircutor {0}/{1}", itemCode, serialNumber);

            var registerInfo =  afqTest.GetRegisteredInfo(itemCode, serialNumber);
            if (registerInfo == null)
            {
                Logger.InfoFormat("Registrando equipo nº de serie {0} en myCircutor!", serialNumber);
                registerInfo = afqTest.RegisterAFQ(itemCode, serialNumber);
            }
            else
            {
                Logger.InfoFormat("AFQ con código producto {0} y nº de serie {1} ya registrado en myCircutor!", itemCode, serialNumber);

                Assert.AreEqual(itemCode, registerInfo.ItemCode,
                    Error().UUT.CONFIGURACION.NO_CORRESPONDE_CON_ENVIO(string.Format("El código de producto registrado en myCircutor es {0} y es distinto al del test {1}",
                    registerInfo.ItemCode, itemCode)));
            }

            SetVariable("UUID" , registerInfo.RegistrationCode);
            SetVariable("TOKEN", registerInfo.DeviceToken);
        }
    }

    public class AFQEvoMyCircutorRegisterActionDescription : ActionDescription
    {
        public override string Name { get { return "AFQEvoMyCircutorRegister"; } }
        public override string Description { get { return "Action para registrar en MyCircutor un producto AFQEvo por su código de producto y su numero de serie"; } }
        public override string Category { get { return "ZurcTest/Tools"; } }
        public override Image Icon { get { return Properties.Resources.Tools_icon; } }
    }
}
