﻿using Comunications.Ethernet;
using Dezac.Core.Utility;
using System.ComponentModel;
using System.Drawing;
using System.Net;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(TftpServerInitDescription))]
    public class TftpServerInit : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Carpeta Servidor TFTP")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ServerDirectory { get; set; }

        public override void Execute()
        {
            var Ip = IPAddress.Parse(NetUtils.GetMyIP());

            var port = 69;

            if (Context.Variables.ContainsKey("DirectoryGetBinaryPGI"))
                ServerDirectory = Context.Variables.Get("DirectoryGetBinaryPGI").ToString();

           TFTPServer tftpServer = new TFTPServer(ServerDirectory, Ip, port);

           Context.Variables.AddOrUpdate("serverIP", NetUtils.GetMyIP());

           Context.Variables.AddOrUpdate("TFTPServer", tftpServer);
        }

        public class TftpServerInitDescription : ActionDescription
        {
            public override string Name { get { return "TFTPServerInit"; } }
            public override string Category { get { return "Tools/Ethernet/TFTP"; } }
            public override string Description { get { return "Action que sirve para inicializar el servidor TFTP"; } }
            public override Image Icon { get { return Properties.Resources.ftp_icon; } }
        }
    }

}
