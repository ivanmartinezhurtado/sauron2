﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Protection
{
    public class UtillajePlacasEkorPlus : IUtillajeEkor
    {
       
        internal const int ANY_BUTTON = 9999;
        internal const int OUT_SET_BUTTON_ACTUATOR = 7;
        internal const int OUT_ESC_BUTTON_ACTUATOR = 8;
        internal const int OUT_UP_BUTTON_ACTUATOR = 9;
        internal const int OUT_LEFT_BUTTON_ACTUATOR = 10;
        internal const int OUT_RIGHT_BUTTON_ACTUATOR = 11;
        internal const int OUT_DOWN_BUTTON_ACTUATOR = 12;
        internal const int OUT_RESET_BUTTON = 8;

        List<KeyValuePair<byte, byte>> keyBoardValue = new List<KeyValuePair<byte, byte>>
        {
                new KeyValuePair<byte, byte>(OUT_SET_BUTTON_ACTUATOR, 0),
                new KeyValuePair<byte, byte>(OUT_ESC_BUTTON_ACTUATOR, 2),
                new KeyValuePair<byte, byte>(OUT_UP_BUTTON_ACTUATOR, 1),
                new  KeyValuePair<byte, byte>(OUT_DOWN_BUTTON_ACTUATOR, 3),
                new KeyValuePair<byte, byte>(OUT_LEFT_BUTTON_ACTUATOR, 5),
                new KeyValuePair<byte, byte>(OUT_RIGHT_BUTTON_ACTUATOR, 4)
        };      

        public void ActivateMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public void ActivatePistonRS232()
        {
            throw new NotImplementedException();
        }

        public void ActivatePistonTestKeyboard()
        {
        }

        public void DesactivateMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public void DesactivatePistonRS232()
        {
            throw new NotImplementedException();
        }

        public void DesactivatePistonTestKeyboard()
        {
        }

        public int GetInputDetectionTrigger()
        {
            throw new NotImplementedException();
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairInputs()
        {
            throw new NotImplementedException();
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairKeyBoard()
        {
            return keyBoardValue;
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairOutputs()
        {
            throw new NotImplementedException();
        }

        public byte GetOutActivateVF()
        {
            throw new NotImplementedException();
        }

        public byte GetOutActivateVL()
        {
            throw new NotImplementedException();
        }

        public int GetOutCLK()
        {
            throw new NotImplementedException();
        }

        public void ResetUSB()
        {
            throw new NotImplementedException();
        }

        public void SupplyUSB(bool state = true)
        {
            throw new NotImplementedException();
        }

        public void TestDeviceConnection()
        {
            throw new NotImplementedException();
        }

        public void TestDeviceDisconnection()
        {
            throw new NotImplementedException();
        }
    }
}
