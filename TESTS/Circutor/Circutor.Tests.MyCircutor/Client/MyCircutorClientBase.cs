﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Circutor.Tests.MyCircutor.Client
{
    public partial class MyCircutorClientBase
    {
        public string Token { get; set; }

        public string BaseUrl { get; set; } = "https://mycapi.mycircutor.com/mycapi";

        protected Task<HttpClient> CreateHttpClientAsync(System.Threading.CancellationToken cancellationToken)
        {
            var client = new System.Net.Http.HttpClient();

            if (!string.IsNullOrEmpty(Token))
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            return Task.FromResult(client);
        }

        protected HttpClient CreateHttpClient()
        {
            var client = new System.Net.Http.HttpClient();

            if (!string.IsNullOrEmpty(Token))
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            return client;
        }
    }
}