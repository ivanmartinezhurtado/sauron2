﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace Dezac.Tests.Actions
{
    /// <summary>
    /// Calibrate a unit following the criteria defined in CalibrationParameters by making measures from the unit and from a pattern.
    /// </summary>
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(DeviceMeasureDescription))]
    public class DeviceMeasure : ActionBase
    {
        private ConfigurationSampler configuration = new ConfigurationSampler();

        /// <summary>
        /// Array of margins definition using AdjustValueDef class.
        /// </summary>
        [Description("Matriz de definición de márgenes usando la clase AdjustValueDef")]
        [Category("3. Configuration")]
        [Editor(typeof(AdjustValueList), typeof(System.Drawing.Design.UITypeEditor))]
        public List<AdjustValueDefWrapper> MeasureParameters { get; set; }

        /// <summary>
        /// Function used to read measures from the unit to calibrate.
        /// </summary>
        [Description("Función para leer medidas desde las unidades de calibración")]
        [Category("3. Configuration")]
        public RunMethodAction ReadMeasureDevice { get; set; }

     
        /// <summary>
        /// Sampling parameters.
        /// </summary>
        [Category("3. Configuration")]
        [Description("Muestreo de los parámetros que utilizamos en el action")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ConfigurationSampler Configuration { get { return configuration; } set { configuration = value; } }


        [Category("4. Results")]
        [Description("Añade los datos medidos en los resultados del test")]
        [DefaultValue(false)]
        public bool AddToResults { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var parametros = ResolveProperties(MeasureParameters);                     

            testBase.TestMeasure(parametros,
            () =>
            {
                ReadMeasureDevice?.Execute();

                var medidas = MeasureParameters.Select(p => Convert.ToDouble(VariablesTools.ResolveValue(p.VarNamesDevice))).ToList();

                return medidas.ToArray();

            }, Configuration, AddToResults);
        }        
    }
    
    class DeviceMeasureDescription : ActionDescription
    {
        public override string Name { get { return "DeviceMeasure"; } }
        public override string Description { get { return "Mide una unidad siguiendo los criterios establecidos como \"Parameters\" haciendo medidas desde la unidad"; } }
        public override string Category { get { return "Actions/StadisticMeasure"; } }
        public override Image Icon { get { return Properties.Resources.BarChart; } }
        public override string Dependencies { get { return "InitTestBase;"; } }
    }
}