﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;

namespace Dezac.Tests.Actions.Views
{
    public class ActionsCollectionEditor : CollectionEditor
    {
        private static Type[] types;

        public ActionsCollectionEditor(Type type) 
            : base(type)
        {
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (value == null)
                value = new List<ActionBase>();

            var result = base.EditValue(context, provider, value);

            return result;
        }

        protected override Type[] CreateNewItemTypes()
        {
            types = types ?? GetType().Assembly
                        .GetTypes()
                        .Where(t => t.IsClass && t.IsPublic && !t.IsAbstract && t.IsSubclassOf(typeof(ActionBase)))
                        .OrderBy(t => t.Name)
                        .ToArray();

            return types;
        }

        protected override CollectionForm CreateCollectionForm()
        {
            var form = base.CreateCollectionForm();

            form.Text = "Actions List";

            return form;
        }
    }
}
