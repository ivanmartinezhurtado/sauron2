﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WinTaskRunner.Design;
using WinTaskRunner.Design.Services;
using WinTaskRunner.Views;

namespace WinTaskRunner.Services
{
    public class SequenceDocumentManager : IDocumentManager
    {
        private string DOC_TYPE = ".seq";       // Podria ser un mimetype

        public string DefaultExtension { get { return DOC_TYPE; } }

        public IDocument AddNew()
        {
            return OpenDocument(new SequenceDocument());
        }

        public IDocument Open()
        {
            var ofd = new OpenFileDialog();

            string directory = DI.Resolve<AppService>().Config.PathSequences.FirstOrDefault();
            Directory.CreateDirectory(directory);
            ofd.InitialDirectory = directory;
            if (ofd.ShowDialog() == DialogResult.OK)
                return Open(ofd.FileName);

            return null;
        }

        public IDocument Open(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return null;

            var doc = new SequenceDocument();

            try
            {
                doc.Load(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Fichero incorrecto: \r {0}.SEQ  \r Causa: \r {1}", fileName, ex.Message), "ERROR LOAD SEQUENCE.SEQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            return OpenDocument(doc);
        }

        public IDocument Open(Stream stream, string fileName)
        {
            if (stream == null)
                return null;

            var doc = new SequenceDocument();

            try
            {
                doc.Load(stream);
                doc.Name = fileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Stream incorrecto: \r Causa: \r {0}", ex.Message), "ERROR LOAD SEQUENCE.SEQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            return OpenDocument(doc);
        }

        public void Save(IDocument document)
        {
            var doc = document as SequenceDocument;
            if (doc == null)
                return;

            if (string.IsNullOrEmpty(doc.FileName))
                SaveAs(document);
            else
                doc.Save();
        }

        public void SaveAs(IDocument document)
        {
            var doc = document as SequenceDocument;
            if (doc == null)
                return;

            var sfd = new SaveFileDialog();

            string directory = DI.Resolve<AppService>().Config.PathSequences.FirstOrDefault();
            Directory.CreateDirectory(directory);

            if (!String.IsNullOrEmpty(doc.Name))
                sfd.FileName = doc.Name;

            sfd.DefaultExt = "seq";

            if (!String.IsNullOrEmpty(doc.FileName) && (doc.FileName.ToUpper() != string.Format("{0}{1}", doc.Name, doc.DocType).ToUpper()))
                sfd.InitialDirectory = doc.FileName;
            else
                sfd.InitialDirectory = directory;

            if (sfd.ShowDialog() == DialogResult.OK)
                doc.Save(sfd.FileName);
        }

        private IDocument OpenDocument(SequenceDocument doc)
        {
            IDocumentService docService = DI.Resolve<IDocumentService>();
            EditorView editor = DI.Resolve<EditorView>();
            editor.Document = doc;

            docService.AddDocument(editor, doc, 200);

            return doc;
        }
    }
}
