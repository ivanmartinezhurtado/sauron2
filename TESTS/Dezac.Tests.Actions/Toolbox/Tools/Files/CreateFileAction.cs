﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(CreateFileActionDescription))]
    public class CreateFileAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Variable o contenido del fichero a crear")]
        [Editor(typeof(TemplateEditor), typeof(UITypeEditor))]
        public string Content { get; set; }

        [Category("3. Configuration")]
        [Description("Ruta donde se creará el fichero")]
        public string PathFile { get; set; }

        [Category("3.Configuration")]
        [Description("Nombre del fichero con extensión")]
        public string FileName { get; set; }

        public override void Execute()
        {
            try
            {
                if (!Directory.Exists(PathFile))
                    Directory.CreateDirectory(PathFile);

                var newFile = File.CreateText(Path.Combine(PathFile, FileName));
                newFile.WriteLine(VariablesTools.ResolveValue(Content));
                newFile.Dispose();
            }
            catch (Exception e)
            {
                Error().SOFTWARE.SAURON.FICHERO_NO_ENCONTRADO(e.Message).Throw();

            }
        }

        class CreateFileActionDescription : ActionDescription
        {
            public override string Name { get { return "CreateFile"; } }
            public override string Description { get { return "Crea un fichero con el contenido de la propiedad 'Content'"; } }
            public override string Category { get { return "Tools/Files"; } }
            public override Image Icon { get { return Properties.Resources.report; } }
        }
    }
}
