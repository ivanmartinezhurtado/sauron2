﻿namespace TaskRunner.Enumerators
{
    public enum RunMode
    {
        Debug,
        Release,
        Testing
    }
}
