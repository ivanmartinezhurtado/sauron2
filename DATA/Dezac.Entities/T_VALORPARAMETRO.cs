namespace Dezac.Entities
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;


    public partial class T_VALORPARAMETRO
    {
        public T_VALORPARAMETRO()
        {
            this.T_VALORPARAMETROPRODUCTO = new HashSet<T_VALORPARAMETROPRODUCTO>();
            this.T_VALORPARAMETROTOL1 = new HashSet<T_VALORPARAMETROTOL>();
            this.T_VALORPARAMETROTOL2 = new HashSet<T_VALORPARAMETROTOL>();
        }

        public int NUMGRUPO { get; set; }
        public int NUMPARAM { get; set; }
        public string VALOR { get; set; }
        public string VALORINICIO { get; set; }
        public Nullable<System.DateTime> FECHAALTA { get; set; }
        public string IDCATEGORIA { get; set; }

        [JsonIgnore]
        public virtual T_GRUPOPARAMETRIZACION T_GRUPOPARAMETRIZACION { get; set; }
        public virtual T_PARAMETRO T_PARAMETRO { get; set; }
        public virtual ICollection<T_VALORPARAMETROPRODUCTO> T_VALORPARAMETROPRODUCTO { get; set; }
        [JsonIgnore]
        public virtual T_VALORPARAMETROTOL T_VALORPARAMETROTOL { get; set; }
        [JsonIgnore]
        public virtual ICollection<T_VALORPARAMETROTOL> T_VALORPARAMETROTOL1 { get; set; }
        [JsonIgnore]
        public virtual ICollection<T_VALORPARAMETROTOL> T_VALORPARAMETROTOL2 { get; set; }

        private string valorFamilia { get; set; }

        public bool DeProducto { get; set; }

        [JsonIgnore]
        public string ValorProducto
        {
            get { return DeProducto ? VALOR : null; }
            set
            {
                DeProducto = true;
                valorFamilia = value;
                VALOR = value;
            }
        }

        public string ValorFamilia
        {
            get { return DeProducto ? valorFamilia : VALOR; }
        }
    }
}
