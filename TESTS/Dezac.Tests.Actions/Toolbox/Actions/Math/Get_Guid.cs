﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(Get_GuidDescription))]
    public class Get_GUID : ActionBase
    {
        [Description("Sirve para guardar la variable temporal en los resultados del test")]
        [Category("3. Results")]
        public bool AddToResults { get; set; }

        public override void Execute()
        {
            var guid = Guid.NewGuid();
            Logger.InfoFormat("GUID = {0}", guid.ToString());
            Context.ResultList.Add("GUID = {0}", guid.ToString());
            SetVariable("GUID", guid.ToString()); 

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            if (AddToResults)
                testBase.Resultado.Set("GUID", guid.ToString(), Model.ParamUnidad.SinUnidad);

            testBase.TestInfo.GUID = guid.ToString();
        }

        public class Get_GuidDescription : ActionDescription
        {
            public override string Name { get { return "GetGUID"; } }
            public override string Category { get { return "Actions/Math"; } }
            public override string Description { get { return "Devuelve un valor único como Identificador Único Global (GUID)"; } }
            public override Image Icon { get { return Properties.Resources.Math; } }
        }
    }
}
