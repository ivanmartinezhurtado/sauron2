﻿using Dezac.Device;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Model;
using System.ComponentModel;
using System.Drawing;
using TaskRunner;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(ReadCirbusDescription))]
    public class ReadCirbus : ActionBase
    {
        [Description("Trama Cirbus a mandar")]
        [Category("3. Configuration")]
        public string Message { get; set; }
        
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        [Category("4. Results")]
        [Description("Unidades con las que trabaja el valor a obtener")]
        [DefaultValue(ParamUnidad.SinUnidad)]
        public ParamUnidad DataOutputUnits { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            string name = DeviceName ?? "CirbusDevice";
            var uut = GetVariable(name) as CirbusDevice;

            var result = uut.Read(Message);

            Context.ResultList.Add(string.Format("{0} - TX:{1} -> RX:{2}", name, Message, result));

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (DataOutput == null || DataOutput.IsKeyNullOrEmpty)
                DataOutput = Message;

            if (testBase != null)
                testBase.Resultado.Set(DataOutput.Value.ToString(), result, DataOutputUnits);
            else
                Context.ResultList.Add(string.Format("{0}={1}", DataOutput, result));

            Context.Variables.AddOrUpdate(DataOutput.Value.ToString(), result);
        }
    }

    public class ReadCirbusDescription : ActionDescription
    {
        public override string Name { get { return "ReadCirbus"; } }
        public override string Description { get { return "Leer Cirbus"; } }
        public override string Category { get { return "Tools/Comunications/Cirbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
    }
}
