﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Xml;
using System.Windows.Forms;


namespace WinTestPlayer.Controls.Expanders
{
    //public delegate void ControlChangedEventHandler(object sender, SelectedControlEventArgs e);

    public class ControlsExpander : Expander
    {
        ControlsExpanderElement expanderElement;

        protected override ExpanderElement CreateExpanderElement()
        {
            expanderElement = new ControlsExpanderElement();
            //expanderElement.ControlsList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(ControlsList_SelectedIndexChanged);
            //expanderElement.ControlsList.MouseUp += new System.Windows.Forms.MouseEventHandler(ControlsList_MouseUp);
            return expanderElement;
        }

        public bool AllowExpand
        {
            get { return expanderElement.AllowExpand; }
            set 
            {
                if (expanderElement.IsExpanded)
                    this.Collapse();

                expanderElement.AllowExpand = value; 
            }
        }

        //void ControlsList_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        //{
        //    RadElement element = this.expanderElement.ControlsList.ElementTree.GetElementAtPoint(e.Location);
        //    DemoAppVisualListElement visualListItem = element as DemoAppVisualListElement;
        //    if (visualListItem != null && visualListItem.Data.Selected)
        //    {
        //        SelectedControlEventArgs args = new SelectedControlEventArgs((XmlNodeList)visualListItem.Data.Tag, (RadListDataItem)visualListItem.Data);
        //        this.OnSelectedControlChanged(args);
        //    }
        //}

        //void ControlsList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        //{
        //    SelectedControlEventArgs args = new SelectedControlEventArgs((XmlNodeList)expanderElement.ControlsList.Items[e.Position].Tag, (RadListDataItem)expanderElement.ControlsList.Items[e.Position]);
        //    this.OnSelectedControlChanged(args);
        //}

        //public event ControlChangedEventHandler SelectedControlChanged;

        //protected virtual void OnSelectedControlChanged(SelectedControlEventArgs e)
        //{
        //    if (SelectedControlChanged != null)
        //        SelectedControlChanged(this, e);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        expanderElement.ControlsList.SelectedIndexChanged -= new Telerik.WinControls.UI.Data.PositionChangedEventHandler(ControlsList_SelectedIndexChanged);
        //        expanderElement.ControlsList.MouseUp += new System.Windows.Forms.MouseEventHandler(ControlsList_MouseUp);
        //    }
        //    base.Dispose(disposing);
        //}

        public RadListElement ControlsList
        {
            get
            {
                return this.expanderElement.ControlsList;
            }
        }

        private string thisControlListSelectedItemText = "Home";

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (!this.ControlsList.OnControlMouseDown(e))
            {
                if (this.ControlsList.SelectedItem != null)
                    thisControlListSelectedItemText = this.ControlsList.SelectedItem.Text;

                base.OnMouseDown(e);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (ControlsList != null && !this.ControlsList.OnControlMouseMove(e))
                base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (ControlsList != null && !this.ControlsList.OnControlMouseUp(e))
                if (this.ControlsList.SelectedItem != null && thisControlListSelectedItemText == this.ControlsList.SelectedItem.Text)
                    base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            // This doesn't work
            //((ControlsExpanderElement)this.ExpanderElement).ControlsList.CallDoMouseWheel(e);

            if (e.Delta > 0)
                expanderElement.DoScrollList(true);
            else
                expanderElement.DoScrollList(false);
        }
    }

    //public class SelectedControlEventArgs : EventArgs
    //{
    //    XmlNodeList controlsExamples;
    //    RadListDataItem controlListItem;

    //    public SelectedControlEventArgs()
    //    {

    //    }

    //    public SelectedControlEventArgs(XmlNodeList controlsExamples, RadListDataItem controlListItem)
    //    {
    //        this.controlsExamples = controlsExamples;
    //        this.controlListItem = controlListItem;
    //    }

    //    public XmlNodeList ControlsExamples
    //    {
    //        get
    //        {
    //            return controlsExamples;
    //        }
    //    }

    //    public RadListDataItem ControlListItem
    //    {
    //        get
    //        {
    //            return this.controlListItem;
    //        }
    //    }
    //}
}
