﻿using Dezac.Core.Utility;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DisposeSerialPortActionDescription))]
    public class DisposeSerialPortAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM";
            var uut = GetVariable(name) as SerialPort;

            if (uut != null)
            {
                uut.Close();
                uut.Dispose();
            }
        }
    }

    public class DisposeSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "DisposeSerialPort"; } }
        public override string Description { get { return "Cierra el puerto serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/ASCII"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
