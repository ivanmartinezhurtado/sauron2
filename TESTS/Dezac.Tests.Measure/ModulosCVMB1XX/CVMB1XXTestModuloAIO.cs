﻿using Dezac.Core.Utility;
using Dezac.Device.Measure.ModulosCVMB1XX;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System.Collections.Generic;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.05)]
    public class CVMB1XXTestModuloAIO : CVMB1XXTestModulosGenerico
    {
        #region Constantes
        private const byte SALIDAS_MODO_TENSION = 15;
        private const byte DESACTIVA_DATAMAN = 18;
        #endregion

        TowerBoard tower;
        private CVMB1XXModuloAIO cvmAnalog;

        public void TestInitialization(byte portCVM)
        {
            if (Model.IdFase != "120")
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }

                cvmAnalog = AddInstanceVar(new CVMB1XXModuloAIO(5), "UUT");
                Resultado.Set("VERSION_DEVICE", string.Format("{0}", cvmAnalog.GetDeviceVersion()), ParamUnidad.SinUnidad);
                Resultado.Set("CLASSE_DEVICE", string.Format("{0}", cvmAnalog.GetType().Name), ParamUnidad.SinUnidad);

                tower.IO.DO.On(14);
                tower.IO.DO.Off(DESACTIVA_DATAMAN);
            }

            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;
        }

        public void TestConsumption()
        {
            tower.IO.DO.On(DESACTIVA_DATAMAN);

            var configuration = new TestPointConfiguration()
            {
                Current = 0.5,
                Voltage = 12.2,
                Frecuency = 0,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = TypePowerSource.LAMBDA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(tower, configuration);

            var param = new AdjustValueDef(Params.V_DC.Null.TestPoint("12V"), ParamUnidad.V);
            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltDC,
                NumMuestras = 3,
                Rango = param.Max,
                DigitosPrecision = 4,
                NPLC = 0
            };

            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN6, measure);
            }, 0, 3, 2000);

            param = new AdjustValueDef(Params.V_DC.Null.TestPoint("3V3"), ParamUnidad.V);
            measure.Rango = param.Max;
            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN5, measure);
            }, 0, 3, 2000);
        }

        public void TestComunications()
        {
            tower.LAMBDA.ApplyOff();

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12.2, 0.5);

            Delay(10000, "");

            SamplerWithCancel((p) => { cvmAnalog.FlagTest(); return true; }, "No se ha podido comunicar con el equipo", 10, 1500, 2000);

            Resultado.Set("COMUNICATIONS", "OK", ParamUnidad.SinUnidad);
        }

        public void AdjustOutVoltage()
        {
            tower.IO.DO.On(SALIDAS_MODO_TENSION);
            //tower.LAMBDA.ApplyOff();
            //tower.LAMBDA.ApplyAndWaitStabilisation(12.2, 0.5);

            tower.IO.DO.On((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT1);
            tower.IO.DO.On((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT2);
            tower.IO.DO.On((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT3);
            tower.IO.DO.On((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT4);

            var DictionaryOutsAndInputsMUX = new Dictionary<CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage, InputMuxEnum>();
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT1, InputMuxEnum.IN9);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT2, InputMuxEnum.IN10);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT3, InputMuxEnum.IN11);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT4, InputMuxEnum.IN12);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT5, InputMuxEnum.IN13);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT6, InputMuxEnum.IN2);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT7, InputMuxEnum.IN3);
            DictionaryOutsAndInputsMUX.Add(CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage.FACTOR_CALIBRATION_VOLTAGE_OUT8, InputMuxEnum.IN4);

            cvmAnalog.FlagTest();

            cvmAnalog.WriteOuputsModeModuleAnalog(CVMB1XXModuloAIO.AnalogModulesOuputsModes.VOLTAGE);

            var outputsValues = new CVMB1XXModuloAIO.OutputsModuleAnalog();     

            foreach (KeyValuePair<CVMB1XXModuloAIO.RegistersFactorCalibrationVoltage, InputMuxEnum> outputs in DictionaryOutsAndInputsMUX)
            {
                cvmAnalog.FlagTest();

                outputsValues.SetAllOuputsValue(20000);
                cvmAnalog.WriteOutputsValues(outputsValues);               

                Delay(1000, "Esperando al equipo");

                double factorCalibratedCalc = 0;
                var param = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("VOLTAGE_" + outputs.Key.GetDescription()).Name, 0, Params.POINTS.Null.Ajuste.TestPoint("VOLTAGE").Min(), Params.POINTS.Null.Ajuste.TestPoint("VOLTAGE").Max(), 0, 0, ParamUnidad.Puntos);

                var paramVerif = new AdjustValueDef(Params.V.Null.Verificacion.TestPoint(outputs.Key.GetDescription()).Name, 0, Params.V.Null.Verificacion.Min(), Params.V.Null.Verificacion.Max(), 0, 0, ParamUnidad.V);

                var measure = new MagnitudToMeasure()
                {
                    Frequency = freqEnum._50Hz,
                    Magnitud = MagnitudsMultimeter.VoltDC,
                    NumMuestras = 3,
                    Rango = paramVerif.Max,
                    DigitosPrecision = 4,
                    NPLC = 0
                };

                TestMeasureBase(param,
                (step) =>
                {
                    var readingMeasure = tower.MeasureMultimeter(outputs.Value, measure);
                    cvmAnalog.FlagTest();
                    var currentFactor = cvmAnalog.ReadFactorCalibrationVoltage(outputs.Key);
                    factorCalibratedCalc = cvmAnalog.CalculaFactorVoltage(readingMeasure, currentFactor);
                    return factorCalibratedCalc;
                }, 0, 3, 2000);

                cvmAnalog.WriteFactorCalibrationVoltage(outputs.Key, factorCalibratedCalc);
               
                TestMeasureBase(paramVerif,
                (step) =>
                {
                    return tower.MeasureMultimeter(outputs.Value, measure);
                }, 0, 3, 2000);

                outputsValues.SetAllOutsOFF();
                cvmAnalog.WriteOutputsValues(outputsValues);
                Delay(1000, "");

                var paramOffset = new AdjustValueDef(Params.V.Null.Offset.TestPoint(outputs.Key.GetDescription()).Name, 0, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V);
                measure.Rango = paramOffset.Max;
                measure.DigitosPrecision = 6;

                TestMeasureBase(paramOffset,
                (step) =>
                {                   
                    return tower.MeasureMultimeter(outputs.Value, measure);
                }, 0, 3, 2000);
                                           
            }

            tower.IO.DO.Off((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT1);
            tower.IO.DO.Off((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT2);
            tower.IO.DO.Off((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT3);
            tower.IO.DO.Off((byte)CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT4);          

            outputsValues.SetAllOutsOFF();
            cvmAnalog.WriteOutputsValues(outputsValues);
            
                     
        }

        public void AdjustOutCurrent()
        {
            tower.IO.DO.Off(SALIDAS_MODO_TENSION); // MODO CORRIENTE

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12.2, 0.5);
            Delay(10000, "");

            cvmAnalog.FlagTest();

            var DictionaryOutsAndRegistersFactorCalibrationCurrent = new Dictionary<CVMB1XXModuloAIO.RelayAnalogOuts, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent>();
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT1, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT1);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT2, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT2);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT3, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT3);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT4, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT4);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT5, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT5);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT6, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT6);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT7, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT7);
            DictionaryOutsAndRegistersFactorCalibrationCurrent.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT8, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent.FACTOR_CALIBRATION_CURRENT_OUT8);

            
            Delay(1000, "");

            cvmAnalog.WriteOuputsModeModuleAnalog(CVMB1XXModuloAIO.AnalogModulesOuputsModes.CURRENT);

            var outputsValues = new CVMB1XXModuloAIO.OutputsModuleAnalog();
            outputsValues.SetAllOutsOFF();
            cvmAnalog.WriteOutputsValues(outputsValues);
            Delay(1000, "Apagando todas las salidas");

            foreach (KeyValuePair<CVMB1XXModuloAIO.RelayAnalogOuts, CVMB1XXModuloAIO.RegistersFactorCalibrationCurrent> outputs in DictionaryOutsAndRegistersFactorCalibrationCurrent)
            {
                cvmAnalog.FlagTest();

                if ((byte)outputs.Key == 0)
                    tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);               
                else
                    tower.IO.DO.On((byte)outputs.Key);

                outputsValues.ActiveOutput(outputs.Key);
                Delay(2000, "");
                cvmAnalog.WriteOutputsValues(outputsValues);
                Delay(1000, "Esperando al equipo");

                double FactorCalibrationCalc = 0;
                var param = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("CURRENT_" + outputs.Key.ToString()).Name, 0, Params.POINTS.Null.Ajuste.TestPoint("CURRENT").Min(), Params.POINTS.Null.Ajuste.TestPoint("CURRENT").Max(), 0, 0, ParamUnidad.mA);

                var paramVerif = new AdjustValueDef(Params.I.Null.Verificacion.TestPoint(outputs.Key.ToString()).Name, 0, Params.I.Null.Verificacion.Min(), Params.I.Null.Verificacion.Max(), 0, 0, ParamUnidad.mA);

                var measure = new MagnitudToMeasure()
                {
                    Frequency = freqEnum._50Hz,
                    Magnitud = MagnitudsMultimeter.AmpDC,
                    NumMuestras = 3,
                    Rango = paramVerif.Max /1000,
                    DigitosPrecision = 4,
                    NPLC = 0
                };


                TestMeasureBase(param,
                (step) =>
                {
                    var readingMeasure = tower.MeasureMultimeter(InputMuxEnum.NOTHING, measure) * 1000;
                    var fcActual = cvmAnalog.ReadFactorCalibrationCurrent(outputs.Value);
                    FactorCalibrationCalc = fcActual * (20000 / (readingMeasure * 1000));
                    return FactorCalibrationCalc;
                }, 0, 3, 2000);

                cvmAnalog.WriteFactorCalibrationCurrent(outputs.Value, FactorCalibrationCalc);


                TestMeasureBase(paramVerif,
                (step) =>
                {
                    var readingMeasure = tower.MeasureMultimeter(InputMuxEnum.NOTHING, measure) * 1000;
                    return readingMeasure;
                }, 0, 3, 2000);
               
                outputsValues.SetAllOutsOFF();
                cvmAnalog.WriteOutputsValues(outputsValues);
                Delay(1000, "Esperando al equipo");               

                var paramOffset = new AdjustValueDef(Params.I.Null.Offset.TestPoint(outputs.Key.ToString()).Name, 0, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.mA);
                measure.Rango = paramOffset.Max ;

                TestMeasureBase(paramOffset,
                (step) =>
                {
                    var readingMeasureOffsets = tower.MeasureMultimeter(InputMuxEnum.NOTHING, measure);
                    return readingMeasureOffsets;
                }, 0, 3, 2000);


                if ((byte)outputs.Key == 0)
                    tower.Chroma.ApplyOffAndWaitStabilisation();
                else
                    tower.IO.DO.Off((byte)outputs.Key);                               
            }                     
        }

        public void AdjustIN()
        {                       
            tower.IO.DO.Off(SALIDAS_MODO_TENSION);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12.2, 0.5);
            Delay(10000, "");

            cvmAnalog.FlagTest();

            cvmAnalog.WriteOuputsModeModuleAnalog(CVMB1XXModuloAIO.AnalogModulesOuputsModes.CURRENT); //Sampler si no comunica a la primera

            var outputsValues = new CVMB1XXModuloAIO.OutputsModuleAnalog();
            outputsValues.SetAllOutsOFF();
            cvmAnalog.WriteOutputsValues(outputsValues);
            Delay(1000, "Apagando todas las salidas");

            var InputAndRegistersFactorCalibration = new Dictionary<CVMB1XXModuloAIO.RelayAnalogOuts, CVMB1XXModuloAIO.AnalogInputPoints>();
            InputAndRegistersFactorCalibration.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT1, CVMB1XXModuloAIO.AnalogInputPoints.INPUT1);
            InputAndRegistersFactorCalibration.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT2, CVMB1XXModuloAIO.AnalogInputPoints.INPUT2);
            InputAndRegistersFactorCalibration.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT3, CVMB1XXModuloAIO.AnalogInputPoints.INPUT3);
            InputAndRegistersFactorCalibration.Add(CVMB1XXModuloAIO.RelayAnalogOuts.OUTPUT4, CVMB1XXModuloAIO.AnalogInputPoints.INPUT4);

            foreach (KeyValuePair<CVMB1XXModuloAIO.RelayAnalogOuts, CVMB1XXModuloAIO.AnalogInputPoints> input in InputAndRegistersFactorCalibration)
            {
                tower.IO.DO.On((byte)input.Key);
                outputsValues.ActiveOutput(input.Key);
                cvmAnalog.WriteOutputsValues(outputsValues);
                Delay(1000, "Esperando al equipo");

                var paramVerifIAjuste = new AdjustValueDef(Params.I.Null.Ajuste.TestPoint(input.Value.ToString()).Name, 0, Params.I.Null.Ajuste.TestPoint("INPUTS").Min(), Params.I.Null.Ajuste.TestPoint("INPUTS").Max(), 0, 0, ParamUnidad.mA);
                var measure = new MagnitudToMeasure()
                {
                    Frequency = freqEnum._50Hz,
                    Magnitud = MagnitudsMultimeter.AmpDC,
                    NumMuestras = 3,
                    Rango = paramVerifIAjuste.Max/1000,
                    DigitosPrecision = 4,
                    NPLC = 0
                };

                TestMeasureBase(paramVerifIAjuste,
                (step) =>
                {
                    var readingMeasure = tower.MeasureMultimeter(InputMuxEnum.NOTHING, measure) * 1000;
                    return readingMeasure;
                }, 0, 3, 2000);

                var FactorCalibrationCalc = cvmAnalog.CalculaFactorCalibrationInput(input.Value);

                Resultado.Set(Params.POINTS.Null.Ajuste.TestPoint(input.Value.ToString()).Name, FactorCalibrationCalc, ParamUnidad.Puntos);

                cvmAnalog.WriteFactorCalibrationInputs(input.Value, FactorCalibrationCalc);
                Delay(1000, "Escribiendo factores de calibración en el equipo");

                var paramVerif = new AdjustValueDef(Params.POINTS.Null.Verificacion.TestPoint(input.Value.ToString()).Name, 0, Params.POINTS.Null.Verificacion.TestPoint("INPUTS").Min(), Params.POINTS.Null.Verificacion.TestPoint("INPUTS").Max(), 0, 0, ParamUnidad.Puntos);
                TestMeasureBase(paramVerif,
                (step) =>
                {
                    return cvmAnalog.ReadInputsPointsCalibrated(input.Value);
                }, 0, 3, 2000);

                outputsValues.SetAllOutsOFF();
                cvmAnalog.WriteOutputsValues(outputsValues);
                Delay(2000, "");

                var paramOffset = new AdjustValueDef(Params.POINTS.Null.Offset.TestPoint(input.Value.ToString()).Name, 0, Params.POINTS.Null.Offset.TestPoint("INPUTS").Min(), Params.POINTS.Null.Offset.TestPoint("INPUTS").Max(), 0, 0, ParamUnidad.Puntos);
                TestMeasureBase(paramOffset,
                (step) =>
                {
                    return cvmAnalog.ReadInputsPointsCalibrated(input.Value);
                }, 0, 3, 2000);


                tower.IO.DO.Off((byte)input.Key);
                Delay(200, "");                              
            }
            tower.LAMBDA.ApplyOffAndWaitStabilisation();         
        }

        public void TestFinish()
        {
            if (cvmAnalog != null)
                cvmAnalog.Dispose();

            if (CVMDummy != null)
                TestDispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.IO.DO.Off(14,15,18,19,20,21,22,17,43,28);
                tower.Dispose();
            }
        }

        #region
        

        public enum RelayAnalogInputs
        {
            INPUT1 = 19,
            INPUT2 = 20,
            INPUT3 = 21,
            INPUT4 = 22,
        }
     
        #endregion
    }
}

