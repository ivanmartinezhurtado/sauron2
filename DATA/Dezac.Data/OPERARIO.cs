namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.OPERARIO")]
    public partial class OPERARIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OPERARIO()
        {
            ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            DIARIOMARCAJE = new HashSet<DIARIOMARCAJE>();
            DIARIOMARCAJE1 = new HashSet<DIARIOMARCAJE>();
            DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
            ESTRUCTURAMATRICULA = new HashSet<ESTRUCTURAMATRICULA>();
            ORDENPRODUCTOERROR = new HashSet<ORDENPRODUCTOERROR>();
            T_LOG = new HashSet<T_LOG>();
            T_TESTFASE = new HashSet<T_TESTFASE>();
        }

        [Key]
        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        [StringLength(10)]
        public string IDCATEGORIA { get; set; }

        public decimal? PRECIO { get; set; }

        [Required]
        [StringLength(1)]
        public string VARIASORDENES { get; set; }

        public decimal PRODUCTIVIDAD { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJE> DIARIOMARCAJE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJE> DIARIOMARCAJE1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ESTRUCTURAMATRICULA> ESTRUCTURAMATRICULA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENPRODUCTOERROR> ORDENPRODUCTOERROR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_LOG> T_LOG { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }
    }
}
