﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    /// <summary>
    /// Calibrate a unit following the criteria defined in CalibrationParameters by making measures from the unit and from a pattern.
    /// </summary>
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(StadisticListMeasureDescription))]
    public class StadisticListMeasure : ActionBase
    {
        private ConfigurationSampler configuration = new ConfigurationSampler();

        /// <summary>
        /// Function used to read measures from the unit to calibrate.
        /// </summary>
        [Category("3. Configuration")]
        [Description("Función para leer medidas desde unidades de calibración")]
        public RunMethodAction ReadMeasureDevice { get; set; }


        /// <summary>
        /// Sampling parameters.
        /// </summary>
        [Category("3. Configuration")]
        [Description("Muestreo de los parámetros que utilizamos en el action")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ConfigurationSampler Configuration { get { return configuration; } set { configuration = value; } } 


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            if (configuration.SampleMaxNumber == 0 || configuration.SampleNumber == 0)
                testBase.Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("numMuestrasToAverage = 0").Throw();

            var list = new StatisticalList() { Name = "StadisticListMeasure" };

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = configuration.DelayBetweenSamples, InitialDelay = configuration.InitDelayTime, NumIterations = configuration.SampleMaxNumber, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var iteracion = step.Step + 1;

                   ReadMeasureDevice?.Execute();

                   var VarNamesDevice = ReadMeasureDevice.Variables.VariableList.Where(p => p.Key.StartsWith(string.Format("{0}-", ReadMeasureDevice.VariableName)) || p.Key == ReadMeasureDevice.VariableName);

                   if (list.series.Count == 0)
                   {
                       var series = VarNamesDevice.Select(p => p.Key).ToList();
                       foreach (var serie in series)
                           list.AddSerie(serie);
                   }

                   var medidas = VarNamesDevice.Select(p => Convert.ToDouble(VariablesTools.ResolveValue(string.Format("${0}",p.Key)))).ToList();

                   var varsVariables = medidas.ToArray();
                   Logger.InfoFormat("Muestra :{0} de {1}", iteracion, configuration.SampleNumber);
                   list.Add(varsVariables);

                   Logger.InfoFormat("{0}", list.ToString()); 
                   if (list.Count(0) >= configuration.SampleNumber)
                   {
                       foreach (var serie in list.series)
                       {
                           Variables.AddOrUpdate(string.Format("{0}_AVG", serie.Name), serie.Data.Average());
                           Variables.AddOrUpdate(string.Format("{0}_MIN", serie.Name), serie.Data.Min());
                           Variables.AddOrUpdate(string.Format("{0}_MAX", serie.Name), serie.Data.Max());
                           Variables.AddOrUpdate(string.Format("{0}_DESVEST", serie.Name), serie.DesviacionStandard());
                       }

                       step.Cancel = true;

                   }
               });
        }
    }
    
    class StadisticListMeasureDescription : ActionDescription
    {
        public override string Name { get { return "StadisticListMeasure"; } }
        public override string Description { get { return "Medidas de StadisticList en una unidad siguiendo \"Paramenters\" haciendo medidas desde la unidad"; } }
        public override string Category { get { return "Actions/StadisticMeasure"; } }
        public override Image Icon { get { return Properties.Resources.BarChart; } }
        public override string Dependencies { get { return "InitTestBase;"; } }
    }
}