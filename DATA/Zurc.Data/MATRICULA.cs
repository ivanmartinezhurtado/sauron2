namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Matricula")]
    public partial class Matricula
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime FechaAlta { get; set; }
        [StringLength(50)]
        public string UserId { get; set; }

        //public DateTime? FECHAIMPRESION { get; set; }

        //[Required]
        //[StringLength(1)]
        //public string SITUACION { get; set; }
    }
}
