﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.11)]
    public class T_SABTTest : TestBase
    {
        private Tower3 tower3;
        private DeviceDlmsTrifasic meter;
        private bool HasWattimetro = false;
        private bool InternalStabilization = false;

        private string CAMERA_IDS_LEDS;

        private const int SWITCH_RJ45 = 20;
        private const int LEFT_RJ45 = 7;
        private const int RIGHT_RJ45 = 8;

        internal Tower3 tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower3))
                {
                    tower3 = AddOrGetSharedVar("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower3);
                }
                return tower3;
            }
        }

        private MeterType meterModel;

        internal DeviceDlmsTrifasic Meter
        {
            get
            {
                if (meter == null)
                {
                    meter = AddInstanceVar(new DeviceDlmsTrifasic(), "SBT");
                    Resultado.Set("VERSION_DEVICE2", string.Format("{0}: {1}", Meter.GetType().Name, Meter.GetDeviceVersion()), ParamUnidad.SinUnidad);
                }

                return meter;
            }
        }

        public void TestInicialization(string nameDevicePortVirtual)
        {
            var portDevice = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));

            Meter.SetPort(portDevice, 115200);

            logger.InfoFormat("New T-SABT by  COM PORT:{0}", portDevice);

            Meter.Retries = 3;

            meterModel = new MeterType("310QT1A");

            InternalStabilization = true;

            tower.PowerSourceIII.Tolerance = 0.2;

            HasWattimetro = Configuracion.GetString("HAS_WATTIMETRO", "NO", ParamUnidad.SinUnidad) == "SI";

            if (HasWattimetro)
                tower.MTEWAT.Output120A = false;
        }

        public void VerificationParameters()
        {
            Meter.StartSession();

            var versionSW = this.Meter.ReadFirmwareVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, versionSW, Identificacion.VERSION_FIRMWARE_SBT, Error().PROCESO.PARAMETROS_ERROR.VERSION_FIRMWARE(), ParamUnidad.SinUnidad);

            var position = Meter.ReadPhysycalAdress();
            logger.InfoFormat("Addres Hardware: {0}", position);

            //this.Meter.WriteLeds(1);
        }

        public void WriteFactorsDefault()
        {
            //Meter.EscribirFechaFabricacion(DateTime.Now);

            Meter.WriteFabricationStatus(999);

            Meter.WriteDateTimeCurrent();

            // Meter.EscribirFechaVerificacion(DateTime.Now);

            Meter.WriteOffset<int>(TipoPotencia.Activa, 0);

            //Meter.WriteOffset<int>(TipoPotencia.Reactiva, 0);

            Meter.WriteVoltageGain<ushort>(3018);

            Meter.WriteGapGain<short>(TypeMeterConection.frequency._50Hz, 0);

            Meter.WritePowerGain<ushort>(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa, Convert.ToUInt16(35594));

            //Meter.WritePowerGain<ushort>(TypeMeterConection.frequency._50Hz, TipoPotencia.Reactiva, Convert.ToUInt16(35594));
        }

        public void TestRJ45()
        {
            Meter.ReadCrcBinary();
            Resultado.Set("RJ45_1", "OK", ParamUnidad.SinUnidad);

            tower.IO.DO.Off(LEFT_RJ45);
            tower.IO.DO.OnWait(2500, SWITCH_RJ45, RIGHT_RJ45);
            Meter.StartSession();
            Meter.ReadCrcBinary();
            Resultado.Set("RJ45_2", "OK", ParamUnidad.SinUnidad);                           
        }       

        public void TestSelector()
        {
            var selectors = Meter.ReadPhysycalAdress();
            CheckSelector("5", selectors);

            Shell.MsgBox("Coloque los dos selectores en la posicion inicial (0) y pulse Aceptar", "SELECTOR POSITION 0");
            Delay(10000, "Esperando cambio selector");

            Meter.StartSession();
            selectors = Meter.ReadPhysycalAdress();
            CheckSelector("0", selectors);
        }

        public void TestLeds()
        {
            CAMERA_IDS_LEDS = GetVariable<string>("CAMARA_LEDS", CAMERA_IDS_LEDS);

            Meter.StartSession();
            Meter.WriteLeds(85);
            Delay(750, "Espera encendido de leds verdes");
            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LEDS, "TSABT_GREEN_LED", "GREEN_LED", "TSABT");
            Meter.WriteLeds(170);     
            Delay(750, "Espera encendido leds rojos");
            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LEDS, "TSABT_RED_LED", "RED_LED", "TSABT");
        }

        public void TesAdjustVoltage(double voltage = 300)
        {
            var defs = new TriAdjustValueDef(string.Format("{0}_T_SABT", Params.GAIN_V.L1.Name), Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var result = Meter.AdjustVoltage(voltage, (value) =>
            {
                defs.L1.Value = value.L1;
                defs.L2.Value = value.L2;
                defs.L3.Value = value.L3;
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES("GANANCIA VOLTAGE").Throw();

            this.Meter.WriteVoltageGain(result.Item2);
        }

        public void TestAdjustGap(double defase = 60)
        {
            double desfaseFactor = -22.2;

            var defs = new TriAdjustValueDef(string.Format("{0}_T_SABT", Params.GAIN_DESFASE.L1.Name), Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var result = Meter.AdjustGap(TypeMeterConection.frequency._50Hz, defase, desfaseFactor, (value) =>
            {
                defs.L1.Value = value.L1;
                defs.L2.Value = value.L2;
                defs.L3.Value = value.L3;
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES("GANANCIA DESFASE").Throw();

            Meter.WriteGapGain(TypeMeterConection.frequency._50Hz, result.Item2);
        }

        public void TestAdjustPowerActive(double voltage = 300, double current = 5, double defase = 0)
        {
            var powerReference = voltage * current * Math.Cos(defase.ToRadians());

            var defs = new TriAdjustValueDef(string.Format("{0}_T_SABT", Params.GAIN_KW.L1.Name), Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var result = Meter.AdjustPowerActive(TypeMeterConection.frequency._50Hz, powerReference, (value) =>
            {
                defs.L1.Value = value.L1;
                defs.L2.Value = value.L2;
                defs.L3.Value = value.L3;
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES("GANANCIA POTENCIA ACTIVA").Throw();

            Meter.WritePowerGain(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa, result.Item2);
        }

        public void TestCRCIntegritySBT()
        {
            var Crc_Binario = this.Meter.ReadCrcBinary();

            Assert.AreEqual(string.Format("{0}_T_SABT", ConstantsParameters.Identification.CRC_FIRMWARE), Crc_Binario, (uint)Identificacion.CRC_FIRMWARE, Error().UUT.FIRMWARE.SETUP("CRC BINARIO"), ParamUnidad.SinUnidad);
        }

        public void TestVerification(double voltage = 300, double current = 1, double desfase = 60, int initCount = 1, int samples = 2, int timeInterval = 1100)
        {
            var power = TriLinePower.Create(voltage, current, desfase);

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(string.Format("{0}_T_SABT", Params.KW.L1.Verificacion.Name), 0, 0, power.KW.L1, toleranciaKw, ParamUnidad.W));

            int count = 1;

            TestCalibracionBase(defs, (Func<double[]>)(() =>
            {
                logger.DebugFormat("TestCalibracionBase Reint:{0}", count);

                var powerActive = this.Meter.ReadPower(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);
                logger.DebugFormat("PowerActive Potencia Activa: {0}", powerActive);

                var varsList = new List<double>();
                varsList.AddRange(powerActive.ToArray());

                count++;

                return varsList.ToArray();
            }),
             () =>
             {
                 List<TriLineValue> result = new List<TriLineValue>();
                 result.Add(tower.PowerSourceIII.ReadVoltage());
                 result.Add(tower.PowerSourceIII.ReadCurrent());

                 var Power = TriLinePower.Create(result[0], result[1], TriLineValue.Create(desfase));
                 logger.DebugFormat("with SBT-> Powers MTE PPS {0}", Power);

                 var varsList = new List<double>();
                 varsList.AddRange(Power.KW.ToArray());

                 return varsList.ToArray();

             }, initCount, samples, timeInterval);
        }

        public void TestPoint_MarchaVacio()
        {
            ConsignarMTE("MARCHA_VACIO", meterModel.VoltageProperty.VoltageNominal * 1.15, 0, 0, meterModel.FrequencyProperty.Frequency, 8);

            var points = Meter.ReadPowerPoints<double>(TipoPotencia.Activa);
            var gain = Meter.ReadPowerGain<ushort>(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);

            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain.L1, points.L1, Error().UUT.MEDIDA.MARGENES_SUPERIORES("PUNTOS POTENCIA L1"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L2.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain.L2, points.L2, Error().UUT.MEDIDA.MARGENES_SUPERIORES("PUNTOS POTENCIA L2"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L3.TestPoint("ACTIVE_MARCHA_VACIO").Name, gain.L3, points.L3, Error().UUT.MEDIDA.MARGENES_SUPERIORES("PUNTOS POTENCIA L3"), ParamUnidad.Puntos);
        }

        public void TestPoint_Arranque()
        {
            ConsignarMTE("ARRANQUE", meterModel.VoltageProperty.VoltageNominal, meterModel.CurrentProperty.CorrienteBase * 0.04, 0, meterModel.FrequencyProperty.Frequency, 8);

            var points = Meter.ReadPowerPoints<double>(TipoPotencia.Activa);
            var gain = Meter.ReadPowerGain<ushort>(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);

            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_ARRANQUE").Name, points.L1, gain.L1, Error().UUT.MEDIDA.MARGENES_INFERIORES("PUNTOS POTENCIA L1"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_ARRANQUE").Name, points.L2, gain.L2, Error().UUT.MEDIDA.MARGENES_INFERIORES("PUNTOS POTENCIA L1"), ParamUnidad.Puntos);
            Assert.AreGreater(Params.POINTS.L1.TestPoint("ACTIVE_ARRANQUE").Name, points.L3, gain.L3, Error().UUT.MEDIDA.MARGENES_INFERIORES("PUNTOS POTENCIA L1"), ParamUnidad.Puntos);
        }

        public void TestPoint_Imin(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IMIN, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IbCosPhi(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IbCOS05, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IbSenPhi(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IbSEN05, TipoPotencia.React, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IMax(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IMAX, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestCustomize(string numSerieChecsum)
        {
            tower.MTEPS.ApplyOffAndWaitStabilisation();

            Meter.WriteDateTimeCurrent();

            if (string.IsNullOrEmpty(TestInfo.NumSerieFamilia))
                TestInfo.NumSerieFamilia = "0";      

            Meter.WriteSerialNumber(TestInfo.NumSerieFamilia);

            Meter.WriteFabricationNumber((uint)TestInfo.NumBastidor.Value);

            var week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            TestInfo.FechaFabricacion = string.Format("{0}/{1:00}", DateTime.Now.Year, week);

            Meter.WriteModel(Identificacion.MODELO);
            Resultado.Set("MODEL", Identificacion.MODELO, ParamUnidad.SinUnidad);

            Meter.WriteManufactureCode(Identificacion.MODELO_FABRICA);
            Resultado.Set("CODIGO_FABRICA", Identificacion.MODELO, ParamUnidad.SinUnidad);

            Meter.WriteMDateTimeManufacture(DateTime.Now);

            Meter.WriteDateTimeVerification(DateTime.Now);

            Meter.WritePartNumber(TestInfo.ProductoVersion + "            ");
            Resultado.Set("PART_NUMBER", TestInfo.ProductoVersion, ParamUnidad.SinUnidad);

            //Meter.WriteUnesaCode(Identificacion.CODIGO_UNESA);
            //Resultado.Set("CODIGO_UNESA", Identificacion.CODIGO_UNESA, ParamUnidad.SinUnidad);

            Meter.WriteFabricationStatus(0);           

            Delay(3000, "Espera para la realización del reset hardware");

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
            Delay(2500, "Espera para el apagado del equipo");
            var Voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V);
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Voltage, 0.5);

            SamplerWithCancel((p) => { meter.StartSession(); return true; }, "", 5, 1000, 1000,false, true, (p)=> Error().UUT.COMUNICACIONES.INICIO_SESION("Despues de un Reset").Throw());

            var NumSerieRead = Meter.ReadSerialNumber();
            Resultado.Set("NUMERO_SERIE_CLIENTE", TestInfo.NumSerieFamilia, ParamUnidad.SinUnidad);
            Assert.IsTrue(NumSerieRead == TestInfo.NumSerieFamilia, ()=> Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Numero de Serie grabado no coincide con el esperado").Throw()); // "Error el Numero de Serie grabado no coincide con el esperado");

            TestInfo.NumSerieFamilia = numSerieChecsum;

            Resultado.Set("NUMERO_SERIE", TestInfo.NumSerie, ParamUnidad.SinUnidad);

            var Bastidor = Meter.ReadFabricationNumber();
            Resultado.Set("NUMERO_MATRICULA", TestInfo.NumBastidor.Value, ParamUnidad.SinUnidad);
            Assert.IsTrue(Bastidor == (uint)TestInfo.NumBastidor.Value, () => Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Numero de Bastidor grabado no coincide con el esperado").Throw());

            var FabricationStatus = Meter.ReadFabricationStatus();
            Resultado.Set("ESTADO_FABRICACION", FabricationStatus, ParamUnidad.SinUnidad);
            Assert.IsTrue(FabricationStatus == 0, () => Error().UUT.HARDWARE.SETUP("Numero de Fabricación grabado no coincide con el esperado").Throw());           
        }

        public void TestPPM()
        {
            var ppmValue = (short)Configuracion.GetDouble("PPM_AVG", 0, ParamUnidad.ppm);
            Meter.WritePPM(ppmValue);
        }

        public void TestFinish()
        {
            if (Meter != null)
                Meter.Dispose();            
        }

        //*************************************************************************************

        private bool CheckSelector(string positionExpected, string selectors)
        {
            Assert.AreEqual(selectors.Substring(0, 1), positionExpected, Error().PROCESO.PARAMETROS_ERROR.ERROR_SUBPROCESO(string.Format("Error, selector izquierdo no leído en la posición {0}", positionExpected)));
            Resultado.Set(string.Format("LEFT_SELECTOR_{0}", positionExpected), "OK", ParamUnidad.SinUnidad);
            logger.InfoFormat("Left Selector : {0}", selectors.Substring(0, 1));
            Assert.AreEqual(selectors.Substring(1, 1), positionExpected, Error().PROCESO.PARAMETROS_ERROR.ERROR_SUBPROCESO(string.Format("Error, selector derecho no leído en la posición {0}", positionExpected)));
            Resultado.Set(string.Format("RIGHT_SELECTOR_{0}", positionExpected), "OK", ParamUnidad.SinUnidad);
            logger.InfoFormat("Right Selector : {0}", selectors.Substring(1, 1));

            return true;
        }

        private void TestPointCalibration(TestPointsName testPointName, TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200, bool isMultiRangeVoltage = false)
        {
            TestPoint testPoint = new TestPoint(meterModel, testPointName, tipoPotencia, isMultiRangeVoltage);

            ConsignarMTE(testPointName.ToString(), testPoint.Voltage, testPoint.Current, testPoint.Desfase, testPoint.Frequency);

            internalVerification(testPoint, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        private void internalVerification(TestPoint testPoint, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            var potenciaAparente = testPoint.Voltage * testPoint.Current;
            var potenciaActiva = potenciaAparente * Math.Cos(testPoint.Desfase.ToRadians());
            var margen = Margenes.GetDouble(string.Format("PRECISION_{0}", testPoint.Name), testPoint.Tolerance, ParamUnidad.PorCentage);

            var defs = new TriAdjustValueDef(testPoint.Name + "_L1", 0, 0, potenciaActiva, margen, ParamUnidad.W);
            TestCalibracionBase(defs,
            () =>
            {
                var powerActive = Meter.ReadPower(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);
                var lecturas = new List<double>();

                lecturas.Add((double)powerActive.L1);
                lecturas.Add((double)powerActive.L2);
                lecturas.Add((double)powerActive.L3);
                logger.InfoFormat("Potencia Activa T-SABT L1: {0}", powerActive.L1);
                logger.InfoFormat("Potencia Activa T-SABT L2: {0}", powerActive.L2);
                logger.InfoFormat("Potencia Activa T-SABT L3: {0}", powerActive.L3);

                return lecturas.ToArray();
            },
            () =>
            {           
                var varsList = new List<double>();

                if (HasWattimetro)
                {
                    var power = tower.MTEWAT.ReadActivePower().Value.ToArray();
                    varsList.AddRange(power);
                    logger.DebugFormat("MTE Wattimetro -> Power Active:{0}", power);
                }
                else
                {
                    List<TriLineValue> result = new List<TriLineValue>();
                    result.Add(tower.PowerSourceIII.ReadVoltage());
                    result.Add(tower.PowerSourceIII.ReadCurrent());
                    var Power = TriLinePower.Create(result[0], result[1], TriLineValue.Create(testPoint.Desfase));
                    logger.DebugFormat("MTE PowerSource ->  Power Active:{0}", Power);
                    varsList.AddRange(Power.KW.ToArray());
                }

                return varsList.ToArray();

            }, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime,
            string.Format("Error calculado en la calibración en {0} fuera de margenes", testPoint.Name));

            logger.InfoFormat("L1 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L1.Value, defs.L1.Error, defs.L1.Tolerance);
            logger.InfoFormat("L2 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L2.Value, defs.L2.Error, defs.L2.Tolerance);
            logger.InfoFormat("L3 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L3.Value, defs.L3.Error, defs.L3.Tolerance);

            var ErrorTri = Math.Round((defs.L1.Error + defs.L2.Error + defs.L3.Error) / 3, 2);

            var defsError = new AdjustValueDef(testPoint.Name + "_III_ERROR", ErrorTri, -1 * testPoint.Tolerance, testPoint.Tolerance, 0, 0, ParamUnidad.PorCentage);
            Resultado.Set(defsError.Name, defsError.Value, ParamUnidad.PorCentage, -1 * testPoint.Tolerance, testPoint.Tolerance);

            Assert.IsTrue(defsError.IsValid() == true,Error().UUT.CALIBRACION.MARGENES(defsError.Name));
        }

        private void ConsignarMTE(string testPointName, double tension, double corriente, double desfase, double frecuencia, double currentTolerance = 2)
        {
            var voltage = TriLineValue.Create(tension);
            var current = TriLineValue.Create(corriente);
            var gap = TriLineValue.Create(desfase);

            if (meterModel.TipoEquipo == MeterType.TipoEquipoEnum.MONOFASICO)
            {
                voltage = TriLineValue.Create(tension, 0, 0);
                current = TriLineValue.Create(corriente, 0, 0);
                gap = TriLineValue.Create(desfase, 0, 0);
            }

            var config = new PowerSourceIIIStabilizationExtension.PowerSourceIIIConfiguration()
            {
                V_L1 = voltage.L1,
                V_L2 = voltage.L2,
                V_L3 = voltage.L3,
                I_L1 = current.L1,
                I_L2 = current.L2,
                I_L3 = current.L3,
                PF_L1 = gap.L1,
                PF_L2 = gap.L2,
                PF_L3 = gap.L3,
                Gap_L1 = 0,
                Gap_L2 = 120,
                Gap_L3 = 240,
                Frequency = frecuencia,
                InternalStabilization = InternalStabilization,
                Samples = 2,
                MaxSamples = 15,
                Timeout = 35000,
                TimeSamples = 1500,
                TestPoint = "PSIII_" + testPointName,
                Tolerance = 0.5,
                VoltageTolerance = 1,
                VoltageMaxVariance = 100,
                CurrentTolerance = currentTolerance,
                CurrentMaxVariance = 100,
                PFDegreeTolerance = 2,
                PFDegreeMaxVariance = 100,
                FrequencyMaxVaraince = 100,
                FrequencyTolerance = 1,
                GapDegreeMaxVariance = 100,
                GapDegreeTolerence = 2
            };

            this.PowerSourceIIIStabilization(tower, config);
        }

    }
}
