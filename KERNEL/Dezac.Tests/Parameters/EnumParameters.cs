﻿namespace Dezac.Tests.Parameters
{
    public enum MagnitudsTester
    {
        Resistencia,
        VoltAC,
        VoltDC,
        AmpAC,
        AmpDC,
    }

    public enum Frequency
    {
        _50Hz = 50,
        _60Hz = 60,
    }    
}
