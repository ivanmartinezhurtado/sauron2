﻿using Dezac.Core.Utility;
using Instruments.Auxiliars;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(FEASA_LEDAnalyzer_Description))]
    public partial class UC_FEASA_LEDAnalyzer : UserControl, IDisposable
    {
        private static readonly string TAG = "FEASA_LEDAnalyzer";
        private FEASA_LedAnalyzer feasa;

        public Type InstrumentType
        {
            get
            {
                return typeof(FEASA_LedAnalyzer);
            }
        }

        public FEASA_LedAnalyzer Instrument
        {
            get
            {
                if (feasa == null)
                    feasa = new FEASA_LedAnalyzer();

                return feasa;
            }
            set
            {
                if (feasa != null)
                {
                    feasa.Dispose();
                    feasa = null;
                }

                feasa = value;
            }
        }

        public UC_FEASA_LEDAnalyzer()
        {
            InitializeComponent();         
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))       
                components.Dispose();
            
            if(Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }

    public class FEASA_LEDAnalyzer_Description : ControlDescription
    {
        public override string Name { get { return "FEASA LED Analyzer"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Led; } }
        public override Image IconBig { get { return Properties.Resources.FEASA_LED; } }
        public override Type SubcomponentType { get { return typeof(FEASA_LedAnalyzer); } }
        public override bool Visible => false;
    }
}
