﻿using Dezac.Core.Exceptions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(GetTestPointByMeterTypeDescription))]
    public class GetTestPointByMeterType : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Test point - punto del ensayo según normativa MID o IEC a realizar")]
        public TestPointsName TestPointName { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo potencia a testear en el  punto del ensayo según normativa MID o IEC a realizar")]
        [DefaultValue(TipoPotencia.Activa)]
        public TipoPotencia PowerType { get; set; }

        [Category("3. Configuration")]
        [DefaultValue("METER_TYPE")]
        [Description("Nombre de la variable donde está el objeto MetertType")]
        public string MeterType { get; set; }

        [Category("4. Results")]
        [Description("Nombre de la variable donde se guardará el objeto TestPoint")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            try
            {
                if (!MeterType.StartsWith("$"))
                    MeterType = MeterType.Insert(0,"$");

                var meter = VariablesTools.ResolveValue(MeterType) as MeterType;
                if (meter == null)
                    Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("MeterType").Throw();

                Logger.InfoFormat("Contador Tipo Equipo:{0}", meter.TipoEquipo.ToString());
                Logger.InfoFormat("Contador Tipo Conexion:{0}", meter.TipoConexion.ToString());

                TestPoint testPoint = new TestPoint(meter, TestPointName, PowerType, false);

                Logger.InfoFormat("TestPoint Name {0}", testPoint.Name);
                Logger.InfoFormat("TestPoint Voltage {0}", testPoint.Voltage);
                Logger.InfoFormat("TestPoint Current {0}", testPoint.Current);
                Logger.InfoFormat("TestPoint Desfase {0}", testPoint.Desfase);
                Logger.InfoFormat("TestPoint Frequencia {0}", testPoint.Frequency);
                Logger.InfoFormat("TestPoint Tipo Potencia {0}", testPoint.TipoPotencia);
                Logger.InfoFormat("TestPoint Toleramcia {0}", testPoint.Tolerance);

                if (string.IsNullOrEmpty(DataOutput))
                    DataOutput = testPoint.Name;

                SetVariable(DataOutput, testPoint);

                var voltage = string.Format("{0}_VOLTAGE", DataOutput);
                SetVariable(voltage, testPoint.Voltage);

                var current = string.Format("{0}_CORRIENTE", DataOutput);
                SetVariable(current, testPoint.Current);

                var desfase = string.Format("{0}_DESFASE", DataOutput);
                SetVariable(desfase, testPoint.Desfase);

                var freq = string.Format("{0}_FREQUENCIA", DataOutput);
                SetVariable(freq, testPoint.Frequency);

                var tol = string.Format("{0}_TOLERANCIA", DataOutput);
                SetVariable(tol, testPoint.Tolerance);
            }
            catch (Exception ex)
            {
                Logger.WarnFormat("TestPoint: {0} Incorrecto no se ha podido crear una clase TestPoint por ", TestPointName.ToString(), ex.Message);
            }
        }
    }

    public class GetTestPointByMeterTypeDescription : ActionDescription
    {
        public override string Name { get { return "GetTestPointByMeterType"; } }
        public override string Category { get { return "DezacTest/Metering"; } }
        public override string Description { get { return "Action que sirve para crear un punto de test"; } }
        public override Image Icon { get { return Properties.Resources.Validation; } }
    }
}
