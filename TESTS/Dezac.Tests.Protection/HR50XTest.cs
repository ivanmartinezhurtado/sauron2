﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.02)]
    public class HR50XTest : TestBase
    {
        Tower1 tower;
        HR50X hager1;
        //HR50X hager2;
        //HR50X hager3;
        HR50X.Models model;

        #region TOWER CONTROLS

            #region OUTPUTS

            private const ushort ALIMENTACION_EQ2 = 18;
            private const ushort ALIMENTACION_EQ3 = 19;
            //private const ushort SENYAL_TOROIDAL_1 = 20;
            //private const ushort SENYAL_TOROIDAL_2 = 21;
            private const ushort ACTIVA_N_IN_EXT = 22;
            private const ushort _24V = 14;
            #endregion

            #region INPUTS

            private const ushort LED_PW_EQ1 = 10;
            private const ushort LED_TRF_EQ1 = 9;
            private const ushort LED_PW_EQ2 = 12;
            private const ushort LED_TRF_EQ2 = 11;
            private const ushort LED_PW_EQ3 = 14;
            private const ushort LED_TRF_EQ3 = 13;
            private const ushort ESTADO_RL_NC_EQ1 = 15;
            private const ushort ESTADO_RL_NO_EQ1 = 16;
            private const ushort ESTADO_RL_NC_EQ2 = 17;
            private const ushort ESTADO_RL_NO_EQ2 = 18;
            private const ushort ESTADO_RL_NC_EQ3 = 19;
            private const ushort ESTADO_RL_NO_EQ3 = 20;
            private const ushort BLOCK_SECURITY = 23;
            #endregion


        #endregion

        #region MACHINE CONTROL
            #region OUTPUTS
            private const ushort NEON_VERDE_EQ1 = 48;
            private const ushort NEON_ROJO_EQ1 = 49;
            private const ushort NEON_VERDE_EQ2 = 50;
            private const ushort NEON_ROJO_EQ2 = 51;
            private const ushort NEON_VERDE_EQ3 = 52;
            private const ushort NEON_ROJO_EQ3 = 53;
            #endregion

            #region INPUTS
            private const ushort DEVICE_PRESENCE_EQ1 = 24;
            private const ushort DEVICE_PRESENCE_EQ2 = 25;
            private const ushort DEVICE_PRESENCE_EQ3 = 26;
            //private const ushort ANCLA_PRESENCE_EQ1 = 27;
            //private const ushort ANCLA_PRESENCE_EQ2 = 28;
            //private const ushort ANCLA_PRESENCE_EQ3 = 29;
            #endregion
        #endregion

        #region VALVULE CONTROL
            #region OUTPUTS
            private const ushort EV_GENERAL = 4;
            private const ushort COMUNICATIONS_BLOCK = 5;
            private const ushort OUT_TEST = 9;
            private const ushort OUT_RESET = 10;
            private const ushort OUT_TEST_RESET_EXTERNAL = 22;
            private const ushort OUT_DISCONNECT_TOROIDAL = 21;
            private const ushort ESCAPE_EQ1 = 6;
            private const ushort ESCAPE_EQ2 = 7;
            private const ushort ESCAPE_EQ3 = 8;
            #endregion
        #endregion


        public void InitTower()
        {            
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower1>("TOWER", () => { return new Tower1(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
            tower.ActiveElectroValvule();
        }

        public void TestInitialization()
        {

            hager1 = new HR50X(Comunicaciones.SerialPort);
                               
            tower.IO.DO.On(_24V);           
       
            SamplerWithCancel((p) =>
            {              
                return tower.IO.DI[BLOCK_SECURITY];
            }, "Error No se ha detectado el bloque de seguridad", 30, 1000, 2500, false, false);
            
            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DEVICE_PRESENCE_EQ1];
            }, "Error No se ha detectado la presencia del equipo 1 o el ancla del equipo 1 no esta puesta", 5, 500, 2500, false, false);

            tower.IO.DO.On(COMUNICATIONS_BLOCK);

            model = HR50X.Models.NULL;   
            Enum.TryParse<HR50X.Models>(Identificacion.MODELO, out model);
        }

        public void TestActivatePowerSupply()
        {
            tower.PTE.InitConfig();
            tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;

            tower.PTE.Presets.Voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V);
            tower.PTE.ApplyAndWaitStabilisation();

            //SamplerWithCancel((p) =>
            //{
            //    tower.IO.DO.On(OUT_RESET);
            //    Delay(100, "");
            //    tower.IO.DO.Off(OUT_RESET);
            //    return tower.IO.DI[ESTADO_RL_NC_EQ1] && !tower.IO.DI[ESTADO_RL_NO_EQ1];
            //}, "Error Condiciones iniciales del equipo incorrectas", 5, 500, 0, false, false);

        }

        public void TestCommunications()
        {
            SamplerWithCancel((p) =>
            {
                hager1.FlagTest();
                return true;
            }, "Error No se ha podido comunicar con el equipo", 5, 500, 0, false, false);

            var firmwareVersion = hager1.ReadFimwareVersion();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la versión de firmware del equipo 1 es incorrecta"), ParamUnidad.SinUnidad);

            hager1.WriteModel(model);

            Delay(1000, "");

            hager1.Reset();

            SamplerWithCancel((p) =>
            {
                hager1.FlagTest();
                return true;
            }, "Error No se ha podido comunicar con el equipo despues reset software al grabar modelo", 3, 500, 1000); 
        }

        public void TestInputs()
        {
           
           Dictionary<HR50X.Inputs, int> deviceInputsTowerOutputs = new Dictionary<HR50X.Inputs, int>()
            {          
                {HR50X.Inputs.TECLA_TEST, OUT_TEST},
                {HR50X.Inputs.TECLA_RESET, OUT_RESET}
            };
           if (model == HR50X.Models.CIRCUTOR_500)
               deviceInputsTowerOutputs.Add(HR50X.Inputs.EXTERNAL_TEST_RESET, OUT_TEST_RESET_EXTERNAL);

            foreach (KeyValuePair<HR50X.Inputs, int> deviceInputTowerOutput in deviceInputsTowerOutputs)
            {
                tower.IO.DO.On(deviceInputTowerOutput.Value);

                Delay(500, "Moviendo Piston");

                var inputsReading = hager1.ReadInputs();

                tower.IO.DO.Off(deviceInputTowerOutput.Value);

                if (inputsReading.ToString().Contains(deviceInputTowerOutput.Key.ToString()))
                    Resultado.Set("INPUT_" + inputsReading.ToString(), "OK", ParamUnidad.SinUnidad);
                else
                    throw new Exception(string.Format("Error No se ha detectado la entrada {0}", deviceInputTowerOutput.Key.ToString().Replace("_", " ")));
            }

            var read = "";
            SamplerWithCancel((p) =>
            {
                var reading = hager1.ReadInputs();
                read = reading.ToString();
                if (!read.ToString().Contains(HR50X.Inputs.TECLA_RESET.ToString()) && !read.ToString().Contains(HR50X.Inputs.TECLA_TEST.ToString()))
                    return true;
                else
                    return false;
            }, string.Format("Se ha detectado la tecla {0} cuando no debería detectarse", read), 3, 500, 500);

            SamplerWithCancel((p) =>
            {
                var reading = hager1.ReadInputs();
                read = reading.ToString();
                if (!read.Contains(HR50X.Inputs.TOROIDAL_ERROR.ToString()))
                    return true;
                else
                    return false;
            }, "Error No se ha detectado la conexión del toroidal", 3, 150);

            hager1.Reset();
            Delay(2000, "Reseteando el equipo");

            Dictionary<HR50X.Inputs, int> toroidal = new Dictionary<HR50X.Inputs, int>()
            {                           
                {HR50X.Inputs.TOROIDAL_ERROR, OUT_DISCONNECT_TOROIDAL},
            };

            foreach (KeyValuePair<HR50X.Inputs, int> deviceInputTowerOutput in toroidal)
            {
                tower.IO.DO.On(deviceInputTowerOutput.Value);

                SamplerWithCancel((p) =>
                {
                    var inputsReading = hager1.ReadInputs();
                    return inputsReading == deviceInputTowerOutput.Key;
                }, ("No se ha detectado la Desconexion del toroidal"), 10, 1000);

                tower.IO.DO.Off(deviceInputTowerOutput.Value);

                Resultado.Set("DETECCION_DESCONEXION_TOROIDAL", "OK", ParamUnidad.SinUnidad);
            }

            tower.IO.DO.On(OUT_RESET);
            Delay(250, "Activando Piston");
            tower.IO.DO.Off(OUT_RESET);
            Delay(250, "Esperando apagar la alarma del equipo"); 
        }

        public void TestOutputs()
        {
            hager1.FlagTest();

            Dictionary<HR50X.Outputs, int> deviceRelaysTowerInputs = new Dictionary<HR50X.Outputs, int>()
            {
                {HR50X.Outputs.RELE_NC, ESTADO_RL_NC_EQ1},
                {HR50X.Outputs.RELE_NO, ESTADO_RL_NO_EQ1}
            };

            hager1.WriteOutputs(HR50X.Outputs.NONE);
            Delay(200, "Desactivando salidas");
            foreach (KeyValuePair<HR50X.Outputs, int> deviceRelayTowerInput in deviceRelaysTowerInputs)
            {
                hager1.WriteOutputs(deviceRelayTowerInput.Key);
                Delay(2000, "Escribiendo en el equipo");
                var towerInputState = tower.IO.DI.Read(deviceRelayTowerInput.Value).Value;

                hager1.WriteOutputs(HR50X.Outputs.NONE);
                Delay(200, "Desactivando salidas");

                Assert.IsTrue(towerInputState, Error().UUT.DISPARO.NO_COMUNICA(string.Format("Error, no se detecta el {0} cuando debería detectarse", deviceRelayTowerInput.Key.GetDescription().Replace("_", " "))));

                Resultado.Set(deviceRelayTowerInput.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestLeds()
        {
            hager1.FlagTest();

            Dictionary<HR50X.Leds, int> deviceLedsTowerInputs = new Dictionary<HR50X.Leds, int>()
            {
                {HR50X.Leds.ON, LED_PW_EQ1},
                {HR50X.Leds.TRIGGER, LED_TRF_EQ1}
            };

            hager1.WriteLeds(HR50X.Leds.NONE);
            Delay(100, "Apagando leds");

            foreach (KeyValuePair<HR50X.Leds, int> deviceLedTowerInput in deviceLedsTowerInputs)
            {
                
                hager1.WriteLeds(deviceLedTowerInput.Key);

                Delay(100, "Espera activación led");

                var towerInputState = tower.IO.DI.Read(deviceLedTowerInput.Value).Value;

                hager1.WriteLeds(HR50X.Leds.NONE);
                
                Assert.IsTrue(towerInputState, Error().UUT.HARDWARE.LEDS(string.Format("Error, No se ha detectado el LED {0}", deviceLedTowerInput.Key.GetDescription())));

                Resultado.Set("LED_" + deviceLedTowerInput.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }

        }

        public void TestCalibration()
        {
            hager1.FlagTest();

            var calibrationResult = 0;
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.6, ParamUnidad.A);
            tower.PTE.ApplyPresetsAndWaitStabilisation(230, adjustCurrent, 50);
            Delay(5000, "Estabilizando la fuente");
            var adjustValue = new AdjustValueDef(Params.POINTS.Null.Ajuste.Name, 0, Params.POINTS.Null.Ajuste.Min(), Params.POINTS.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(adjustValue, (s) =>
            {
                hager1.WriteCalibrationOrder();

                Delay(2500, "Calibrando equipo");

                calibrationResult = hager1.ReadCalibrationResult();

                return calibrationResult;

            }, 0, 8, 500);

            //Assert.AreBetween(Params.POINTS.Null.Ajuste.Name, calibrationResult, Params.POINTS.Null.Ajuste.Min, Params.POINTS.Null.Ajuste.Max, "Constante de calibración leida fuera de margenes", ParamUnidad.Puntos);

            hager1.WriteCalibrationFactors(calibrationResult);
            Delay(500, "");
            hager1.ReadCalibrationResult();

            tower.PTE.ApplyPresetsAndWaitStabilisation(230, 0, 50);
        }

        public void TestVerification()
        {
            hager1.Reset();

            Delay(2500, "Reseteando el equipo");

            //SamplerWithCancel((p) =>
            //{
            //    var reading = hager1.ReadInputs();
            //    var read = reading.ToString();
            //    if (!read.Contains(HR50X.Inputs.TOROIDAL_ERROR.ToString()))
            //        return true;
            //    else
            //        return false;
            //}, "Error No se ha detectado la conexión del toroidal", 3, 150);

            //Dictionary<HR50X.Inputs, int> toroidal = new Dictionary<HR50X.Inputs, int>()
            //{                           
            //    {HR50X.Inputs.TOROIDAL_ERROR, OUT_DISCONNECT_TOROIDAL},
            //};

            //foreach (KeyValuePair<HR50X.Inputs, int> deviceInputTowerOutput in toroidal)
            //{
            //    tower.IO.DO.On(deviceInputTowerOutput.Value);

            //    SamplerWithCancel((p) =>
            //    {
            //        var inputsReading = hager1.ReadInputs();
            //        return inputsReading == deviceInputTowerOutput.Key;
            //    },("No se ha detectado la Desconexion del toroidal"), 10, 1000);

            //    tower.IO.DO.Off(deviceInputTowerOutput.Value);

            //    Resultado.Set("DETECCION_DESCONEXION_TOROIDAL", "OK", ParamUnidad.SinUnidad);                
            //}

            //tower.IO.DO.On(OUT_RESET);
            //Delay(250, "Activando Piston");
            //tower.IO.DO.Off(OUT_RESET);
            //Delay(250, "Esperando apagar la alarma del equipo");

            var verificationCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 0.9, ParamUnidad.A);

            tower.PTE.SelecctionSource = PTE.SourceType.PTE_50_CE;

            tower.PTE.ResetTimerTripMonitor();

            tower.PTE.ApplyConfigMonitor(3, PTE.Monitor.EventType.TRIP, PTE.Monitor.EventTrigger.ActivarseDesactivarse);

            tower.PTE.DesActivaTimer(108);

            tower.PTE.ApplyCurrentAndStartTimer(verificationCurrent);

            Delay(1000, "Esperando el disparo del equipo");

            SamplerWithCancel((p) =>
            {
                return !tower.IO.DI[ESTADO_RL_NC_EQ1] && tower.IO.DI[ESTADO_RL_NO_EQ1];
            }, "Error No se ha producido el disparo del equipo", 3, 150);

            var time = tower.PTE.ReadTime();

            tower.PTE.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            Assert.AreBetween(Params.TIME.Null.TRIGGER.Name, time, Params.TIME.Null.TRIGGER.Min(), Params.TIME.Null.TRIGGER.Max(), Error().UUT.DISPARO.MARGENES("Error. valor de tiempo de disparo fuera de margenes"), ParamUnidad.s);                                     
        }

        public void TestCustomization()
        {
            hager1.WriteSerialNumber(Convert.ToInt32(TestInfo.NumSerie));
            Delay(150, "Escribiendo numero serie");

            hager1.WriteFrameNumber((int)TestInfo.NumBastidor);
            Delay(150, "Escribiendo bastidor");

            hager1.WriteErrorCode(0);
            Delay(150, "Escribiendo code error");

            Delay(1000, "Esperando la grabacion de la parametrizacion en el equipo");
            
            hager1.Reset();
            Delay(2000, "Reseteando el equipo");

            SamplerWithCancel((p) =>
            {
                hager1.ReadSerialNumber();
                return true;
            }, "Error No se ha podido comunicar con el equipo despues del reset", 5, 1000, 0, false, false); 

            var numserie = hager1.ReadSerialNumber();

            var numbastidor = hager1.ReadFrameNumber();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numserie.ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, el numero de serie leido es diferente al grabado"), ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, numbastidor.ToString(), TestInfo.NumBastidor.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, el numero de bastidor leido es diferente al grabado"), ParamUnidad.SinUnidad);            

            //135517 ETIQUETA
        }

        public void TestFinish()
        {
            if (hager1 != null)               
                hager1.Dispose();
           
                       

            if (tower != null)
            {
                tower.PTE.ApplyOffAllAndWaitStabilisation();
                tower.IO.DO.Off(COMUNICATIONS_BLOCK);
                Delay(500, "Subiendo Puntas");
                tower.IO.DO.On(ESCAPE_EQ1);
                Delay(500, "Expulsando Equipo");
                tower.IO.DO.Off(4);
                tower.Dispose();
            }          
        }
    }
}
