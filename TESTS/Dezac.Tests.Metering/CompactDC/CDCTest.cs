﻿using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using System;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.35)]
    public class CDCTest : CDCTestBase
    {
        private const int PILOTO_ERROR = 48;
        private const int POWER_SUPPLY_TTLRS232 = 15;
        private const int OUT_PLC_NEUTRO = 22; 
        private const int OUT_PLC2_NEUTRO = 43;

        public enum TestLine
        {
            PLC1_L1 = 17,
            PLC1_L2 = 18,
            PLC1_L3 = 19,
            PLC2_L1 = 49,
            PLC2_L2 = 50,
            PLC2_L3 = 52,
        }

        public void TestInitialization()
        {
            CAMERA_CPU_PSU = GetVariable<string>("CAMARA_IDS_CPU", CAMERA_CPU_PSU);
            CAMERA_SBT = GetVariable<string>("CAMARA_IDS_SBT", CAMERA_SBT);
            CAMERA_AUX = GetVariable<string>("CAMARA_IDS_SBT", CAMERA_AUX);

            tower.Active24VDC();
            tower.ActiveElectroValvule();
            tower.IO.DO.Off(PILOTO_ERROR);
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            InstanceNumber = 1;
            DoubleSecondary = false;
            onlySBT = !DoubleSecondary;
            METER_PLC = Configuracion.GetString("CONTADOR_PLC1", "CIR0141508383", ParamUnidad.SinUnidad);
            SBT_NUMBER = 1;
            PULSADOR_RESET = 10;
        }

        public void TestLeds_PLC_SBT()
        {
            Delay(3000, "Esperando leds equipo");
            this.TestHalconFindLedsProcedure(CAMERA_SBT, "CDC_PLC_SBT", "PLC_SBT", "CDC", 4);
        }

        public override void TestVerificationSBT(int initCount = 3, int samples = 8, int timeInterval = 1200)
        {
            base.TestVerificationSBT(2, 5, timeInterval);
        }

        public void TestPLCfromMeter()
        {
            internalTestPLCfromMeter("L1", 20, "CIR0000000001");
            internalTestPLCfromMeter("L2", 20, "CIR0000000002");
            internalTestPLCfromMeter("L3", 20, "CIR0000000003");
        }

        public override void DeleteNodesRestart()
        {
           // tower.IO.DO.Off((int)TestLine.PLC1_L1, (int)TestLine.PLC1_L2, (int)TestLine.PLC1_L3, OUT_PLC_NEUTRO);
            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
            base.DeleteNodesRestart();

            tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(2000, "Esperamos se apague el equipo");
            tower.Chroma.ApplyAndWaitStabilisation();

            if (ZonaHoraria == "NO")
                dateTimeLocalRestart = DateTime.Now;
            else
            {
                var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
                dateTimeLocalRestart = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            }
        }

        public override void TestFinish()
        {
            if (tower != null)
                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(200, 5, 7, 8, 6, 9);

                    if (string.IsNullOrEmpty(TestInfo.NumSerie))
                        tower.IO.DO.On(PILOTO_ERROR);

                    Delay(1000, "Espera de equipo malo");
                }

            base.TestFinish();
        }

    }
}
