﻿using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(PowerSourceShutDownActionDescription))]
    [Browsable(false)]
    public class PowerSourcesShutdown : ActionBase
    {
        [DefaultValue(TypePowerSource.ALL)]
        public TypePowerSource PowerSource { get; set; }

        [DefaultValue(true)]
        public bool WaitStabilisation { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            switch (PowerSource)
            {
                case TypePowerSource.CHROMA:
                    if(WaitStabilisation)
                        tower.Chroma.ApplyOffAndWaitStabilisation();
                    else
                        tower.Chroma.ApplyOff();
                    break;
                case TypePowerSource.PTE:
                    if(WaitStabilisation)
                        tower.PTE.ApplyOffAllAndWaitStabilisation();
                    else
                        tower.PTE.ApplyOff();
                    tower.PTE.Reset();
                    break;
                case TypePowerSource.MTE:
                    if(WaitStabilisation)
                        tower.MTEPS.ApplyOffAndWaitStabilisation();
                    else
                        tower.MTEPS.ApplyOff();
                    break;
                case TypePowerSource.LAMBDA:
                    if(WaitStabilisation)
                        tower.LAMBDA.ApplyOffAndWaitStabilisation();
                    else
                        tower.LAMBDA.ApplyOff();
                    break;
                case TypePowerSource.FLUKE:
                    if(WaitStabilisation)
                        tower.FLUKE.ApplyOffAndWaitStabilisation();
                    else
                        tower.FLUKE.ApplyOff();
                    break;
                case TypePowerSource.ALL:
                    if (WaitStabilisation)
                        tower.ShutdownSources();
                    else
                        tower.ShutdownSourcesQuick();
                    break;
                case TypePowerSource.ZERA:
                    if (WaitStabilisation)
                        tower.ZERA.ApplyOffAndWaitStabilisation();
                    else
                        tower.ZERA.ApplyOff();
                    break;
                default:
                    if(WaitStabilisation)
                        tower.ShutdownSources();
                    else
                        tower.ShutdownSourcesQuick();
                    break;
            }
        }
    }

    public class PowerSourceShutDownActionDescription : ActionDescription
    {
        public override string Name { get { return "ShutdownPowerSource"; } }
        public override string Description { get { return "Lleva a cabo el apagado de una fuente de alimentación"; } }
        public override string Category { get { return "Hardware/Tower/PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }

    public enum TypePowerSource
    {
        CHROMA,
        PTE,
        MTE,
        LAMBDA,
        ALL,
        FLUKE,
        ZERA
    }
}