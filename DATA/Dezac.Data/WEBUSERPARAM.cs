namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.WEBUSERPARAM")]
    public partial class WEBUSERPARAM
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string IDUSER { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string APPLICATION { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string PARAM { get; set; }

        [StringLength(100)]
        public string VALUE { get; set; }
    }
}
