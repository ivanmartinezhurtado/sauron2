﻿using Dezac.Core.Utility;
using Instruments.Utility;
using System;
using System.Threading;
using Agilent.AgM918x.Interop;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.06)]
    public class M9183A : MeasureBase<M9183A>, IDisposable
    {
        public enum PrivateCommands
        {
            READ_BUSY = Commands.USER_DEFINED_COMMANDS + 1
        };

        private AgM918x driver;

        public AgM918x Multimeter
        {
            get
            {
                if (driver == null)
                    driver = new AgM918x();

                return driver;
            }
        }

        public M9183A()
        {
        }

        public void connect()
        {
            string resourceDesc = "M9183A"; 

            // If true, this will query the instrument model and fail initialization 
            // if the model is not supported by the driver
            bool idQuery = false;

            // If true, the instrument is reset at initialization
            bool reset = false;

            // Setup IVI-defined initialization options
            string standardInitOptions = "Simulate=false";

            //// Setup driver-specific initialization options
            //string driverSetupOptions = "DriverSetup= Model=M9182A, Trace=false";

            Multimeter.Initialize(resourceDesc, idQuery, reset, standardInitOptions);
        }

        public string GetIdentification()
        {
            _logger.DebugFormat("Identifier:  {0}", Multimeter.Identity.Identifier);
            _logger.DebugFormat("Revision:    {0}", Multimeter.Identity.Revision);
            _logger.DebugFormat("Vendor:      {0}", Multimeter.Identity.Vendor);
            _logger.DebugFormat("Description: {0}", Multimeter.Identity.Description);
            _logger.DebugFormat("Model:       {0}", Multimeter.Identity.InstrumentModel);
            _logger.DebugFormat("FirmwareRev: {0}", Multimeter.Identity.InstrumentFirmwareRevision);
            _logger.DebugFormat("Serial #:    {0}", Multimeter.System.SerialNumber);

            return Multimeter.Identity.Identifier;
        }

        public double ResultMeasure { get; set; }

        public void ConfigurePowerLineFrequency(freqEnum freq)
        {
            Multimeter.Advanced.PowerlineFrequency = freq == freqEnum._50Hz ? 50 : 60;
        }

        public void ConfigureNPLC(string magnitud, double nplc = 1)
        {
            switch (magnitud.Trim().ToUpper())
            {
                case "AMPAC":
                    _logger.InfoFormat("BEFORE -> NPLC ={0}   ApertureTime ={1} ", Multimeter.ACCurrent.Nplc, Multimeter.ACCurrent.Aperture);

                    if (Multimeter.ACCurrent.Nplc < nplc)
                    {
                        Multimeter.ACCurrent.Nplc = nplc; // integrationTime.nplc;
                        _logger.InfoFormat("AFTER -> NPLC ={0}   ApertureTime ={1} ", Multimeter.ACCurrent.Nplc, Multimeter.ACCurrent.Aperture);
                    }

                    break;
                case "AMPDC":
                    _logger.InfoFormat("BEFORE -> NPLC ={0}   ApertureTime ={1} ", Multimeter.DCCurrent.Nplc, Multimeter.DCCurrent.Aperture);

                    if (Multimeter.DCCurrent.Nplc < nplc)
                    {
                        Multimeter.DCCurrent.Nplc = nplc; //integrationTime.nplc;
                        _logger.InfoFormat("AFTER -> NPLC ={0}   ApertureTime ={1} ", Multimeter.DCCurrent.Nplc, Multimeter.DCCurrent.Aperture);
                    }
                    break;

                case "RESISTENCIA":
                    _logger.InfoFormat("BEFORE -> NPLC ={0}   ApertureTime ={1} ", Multimeter.Resistance.Nplc, Multimeter.Resistance.Aperture);

                    if (Multimeter.Resistance.Nplc < nplc)
                    {
                        Multimeter.Resistance.Nplc = nplc;
                        _logger.InfoFormat("AFTER -> NPLC ={0}   ApertureTime ={1} ", Multimeter.Resistance.Nplc, Multimeter.Resistance.Aperture);
                    }
                    break;
                case "VOLTAC":
                    _logger.InfoFormat("BEFORE -> NPLC ={0}   ApertureTime ={1} ", Multimeter.ACVoltage.Nplc, Multimeter.ACVoltage.Aperture);

                    if (Multimeter.ACVoltage.Nplc < nplc)
                    {
                        Multimeter.ACVoltage.Nplc = nplc; //integrationTime.nplc;
                        _logger.InfoFormat("AFTER -> NPLC ={0}   ApertureTime ={1} ", Multimeter.ACVoltage.Nplc, Multimeter.ACVoltage.Aperture);
                    }
                    break;
                case "VOLTDC":
                    _logger.InfoFormat("BEFORE -> NPLC ={0}   ApertureTime ={1} ", Multimeter.DCVoltage.Nplc, Multimeter.DCVoltage.Aperture);

                    if (Multimeter.DCVoltage.Nplc < nplc)
                    {
                        Multimeter.DCVoltage.Nplc = nplc; //integrationTime.nplc;
                        _logger.InfoFormat("AFTER -> NPLC ={0}   ApertureTime ={1} ", Multimeter.DCVoltage.Nplc, Multimeter.DCVoltage.Aperture);
                    }
                    break;

                case "RESISTENICIA 4W":
                    _logger.InfoFormat("BEFORE -> NPLC ={0}   ApertureTime ={1} ", Multimeter.Resistance4Wire.Nplc, Multimeter.Resistance4Wire.Aperture);

                    if (Multimeter.Resistance4Wire.Nplc < nplc)
                    {
                        Multimeter.Resistance4Wire.Nplc = nplc; //integrationTime.nplc;
                        _logger.InfoFormat("AFTER -> NPLC ={0}   ApertureTime ={1} ", Multimeter.Resistance4Wire.Nplc, Multimeter.Resistance4Wire.Aperture);
                    }
                    break;
            }
        }

        public void ConfigureFunction(string magnitud, double range, double digitosPrecision)
        {
            var function = AgM918xFunctionEnum.AgM918xFunctionDCVolts;
            switch (magnitud.Trim().ToUpper())
            {
                case "AMPAC":
                    function = AgM918xFunctionEnum.AgM918xFunctionACCurrent;
                    break;
                case "AMPDC":
                    function = AgM918xFunctionEnum.AgM918xFunctionDCCurrent;
                    break;
                case "RESISTENCIA":
                    function = AgM918xFunctionEnum.AgM918xFunction2WireRes;
                    break;
                case "VOLTAC":
                    function = AgM918xFunctionEnum.AgM918xFunctionACVolts;
                    break;
                case "VOLTDC":
                    function = AgM918xFunctionEnum.AgM918xFunctionDCVolts;
                    break;

                case "RESISTENICIA 4W":
                    function = AgM918xFunctionEnum.AgM918xFunction4WireRes;
                    break;
            }

            if (range == 0)
            {
                _logger.WarnFormat("El rango no puede ser 0");
                range = 2E-6;
            }

            _logger.InfoFormat("Rango Definido ={0} ", range);
            _logger.InfoFormat("Digitos Prrecision ={0} ", digitosPrecision);

            var resolution = range / (Math.Pow(10, digitosPrecision));
            _logger.InfoFormat("Resolution ={0} ", resolution);


            Multimeter.Configure(function, range, resolution, false);
        }

        public double ReadMeasure(int timeOut = 100, int countSamples = 1)
        {
            Thread.Sleep(timeOut);

            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, timeOut, countSamples));
            return ResultMeasure;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            switch (command.Commands)
            {
                case (int)Commands.READ_VARIABLE:
                    var timeOut = (int)command.Args[0];
                    var count = (int)command.Args[1];

                    if (count > 1)
                    {
                        if (timeOut <= 1000)
                            timeOut = 1000 * count;

                        Multimeter.Trigger.Source = AgM918xTriggerSourceEnum.AgM918xTriggerSourceImmediate;
                        Multimeter.Trigger.Count = count;
                        Multimeter.Measurement.Initiate();
                        try
                        {
                            var measuredValues = Multimeter.Measurement.FetchMultiPoint(timeOut);       //time out is 90,000 mS
                            _logger.InfoFormat("Reading without null = " + measuredValues);
                            StatisticalList list = new StatisticalList(count);
                            list.Add(measuredValues);
                            _logger.InfoFormat("Average= {0}  Min= {1}  Max= {2}", list.Average(0), list.Min(0), list.Max(0));
                            ResultMeasure = list.Average(0);
                        }
                        catch (Exception)
                        {
                            Multimeter.Measurement.Abort();
                            Multimeter.Close();
                            driver = null;
                            connect();
                        }
                    }
                    else
                        ResultMeasure = Multimeter.Measurement.Read(timeOut);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            Multimeter.Close();
        }
    }
}
