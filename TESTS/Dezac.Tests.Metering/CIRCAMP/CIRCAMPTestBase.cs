﻿
using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Device.Metering.SOAP;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.05)]
    public class CIRCAMPTestBase : TestBase
    {
        private Tower3 tower3;
        private CirwattDlms sbt;
        private MeterType meterType;
        private COMPACT_DC compacdc;

        internal Tower3 tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower3))
                {
                    tower3 = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower3);

                }
                return tower3;
            }
        }

        internal CirwattDlms SBT
        {
            get
            {
                if (sbt == null)
                    sbt = AddInstanceVar<CirwattDlms>(new CirwattDlms(), "CIRWATT");

                return sbt;
            }
        }

        internal COMPACT_DC cdc
        {
            get
            {
                if (compacdc == null)
                    compacdc = AddInstanceVar(new COMPACT_DC(Identificacion.VERSION_STG), "UUT");

                compacdc.Password = "admin";
                compacdc.UserName = "admin";

                return compacdc;
            }
        }

        internal MeterType meter
        {
            get
            {
                if (meterType == null)
                    meterType = AddInstanceVar(new MeterType(Identificacion.MODELO), "meterType");

                return meterType;
            }
        }

        internal CirwattDlms.enumFases Fases { get; set; }

        internal string IDS_CAMERA_DISPLAY { get; set; }

        internal string IDS_CAMERA_LED { get; set; }

        internal byte OUT_ACTIVE_230V_CDC { get; set; }

        internal byte OUT_ACTIVE_VL2_TO_CDC { get; set; }

        internal byte OUT_ACTIVE_NEUTRO_RELAY_CONTROL { get; set; }

        internal byte OUT_PISTON_TECLA1 { get; set; }

        internal byte OUT_PISTON_TECLA2 { get; set; }

        internal byte OUT_PISTON_OPTICO { get; set; }

        //***********************************************************************

        public virtual void TestInitialization()
        {    
            logger.InfoFormat("COM PORT:{0}", SBT.PuertoCom);

            Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.WaitBloqSecurity(2000);
            tower.ActiveElectroValvule();

            IDS_CAMERA_DISPLAY = GetVariable<string>("CAMARA_IDS", IDS_CAMERA_DISPLAY);
            IDS_CAMERA_LED = GetVariable<string>("CAMARA_LED", IDS_CAMERA_LED);

            tower.IO.DO.On(OUT_ACTIVE_VL2_TO_CDC);
        }

        public void TestConsumo(int waitTimeStabilisation = 2000)
        {
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 100, L3 = 0 }, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.L2.EnVacio.Vmin.Name, Params.KW.L2.EnVacio.Vmin.Min(), Params.KW.L2.EnVacio.Vmin.Max(),
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vmin fuera de margenes L2", 4000);

            TestConsumo(Params.KVAR.L2.EnVacio.Vmin.Name, Params.KVAR.L2.EnVacio.Vmin.Min(), Params.KVAR.L2.EnVacio.Vmin.Max(),
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vmin fuera de margenes L2");

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 230, L3 = 0 }, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.L2.EnVacio.Vnom.Name, Params.KW.L2.EnVacio.Vnom.Min(), Params.KW.L2.EnVacio.Vnom.Max(),
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vnom fuera de margenes L2", 4000);

            TestConsumo(Params.KVAR.L2.EnVacio.Vnom.Name, Params.KVAR.L2.EnVacio.Vnom.Min(), Params.KVAR.L2.EnVacio.Vnom.Max(),
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vnom fuera de margenes L2");

            tower.IO.DO.On((int)OUT_ACTIVE_230V_CDC, (int)OUT_ACTIVE_VL2_TO_CDC);
        }

        public void TestComunicacionesOpticas()
        {
            TestComunicaciones(tipoCom.OPTICAS);
        }

        public void TestComunicacionesRS485()
        {
            TestComunicaciones(tipoCom.RS485);
        }

        public enum tipoCom 
        {
              OPTICAS,
              RS485
         }

        private void TestComunicaciones(tipoCom tipoCom)
        {
            if (tipoCom == CIRCAMPTestBase.tipoCom.RS485)
                SBT.PuertoCom = Comunicaciones.SerialPort;
            else
                SBT.PuertoCom = (byte)Comunicaciones.SerialPort2;

            SBT.IniciarSesion();

            SBT.EscribirReset();

            SBT.EscribirEstadoFabricacion(9999);

            SamplerWithCancel((p) =>
            {
                var codunesa = SBT.LeerCodigoUnesa();
                Assert.AreEqual(ConstantsParameters.Identification.CODIGO_UNESA, codunesa, Identificacion.CODIGO_UNESA, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. el codigo UNESA es incorrecto."), ParamUnidad.SinUnidad);

                var versionSW = SBT.LeerVersionSW();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, versionSW, Identificacion.VERSION_FIRMWARE_SBT, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. la versión de firmware es incorrecto."), ParamUnidad.SinUnidad);

                if (tipoCom != tipoCom.RS485)
                {
                    var versionPLC = SBT.LeerVersionSWPLC();
                    Assert.AreEqual(ConstantsParameters.Identification.VERSION_PLC, versionPLC.ToString(), Identificacion.VERSION_PLC, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. la version de firmware del PLC del equipo es incorrecto."), ParamUnidad.SinUnidad);
                }
                SBT.CerrarSesion(false);

                return true;
            }, "",3,1500,500);
           
        }
       
        internal void internalTestRelesControlEnergia(List<KeyValuePair<byte, byte>> ouputsValue)
        {
            tower.IO.DO.On(OUT_ACTIVE_NEUTRO_RELAY_CONTROL);

            SBT.IniciarSesion();

            foreach (KeyValuePair<byte, byte> output in ouputsValue)
            {
                SBT.WriteRelayEnergyControl(output.Value, CirwattDlms.enumStateRelay.Closed);
                Delay(200, "Espera al enviar  cirre de los reles energia individuales");
            }

            foreach (KeyValuePair<byte, byte> output in ouputsValue)
            {               
                logger.InfoFormat("Test Rele control energia {0}", output.Value.ToString());

                SBT.WriteRelayEnergyControl(output.Value, CirwattDlms.enumStateRelay.Open);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 10, 200);

                SBT.WriteRelayEnergyControl(output.Value, CirwattDlms.enumStateRelay.Closed);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} activada despues de desactivarla", output.Value.ToString()), 10, 200);
            }

           tower.IO.DO.Off(OUT_ACTIVE_NEUTRO_RELAY_CONTROL);

           SBT.CerrarSesion(false);

           Resultado.Set("TEST_RELES_CONTROL_ENERGIA", "OK", ParamUnidad.SinUnidad);
        }

        internal void internalTestRelesControImpulsos(List<KeyValuePair<byte, byte>> ouputsValue)
        {
            SBT.IniciarSesion();

            foreach (KeyValuePair<byte, byte> output in ouputsValue)
            {
                SBT.WriteRelayImpulseControl(output.Value, CirwattDlms.enumStateRelay.Closed);
                Delay(200, "Espera al enviar  cirre de los reles impulsos");
            }

            foreach (KeyValuePair<byte, byte> output in ouputsValue)
            {
                logger.InfoFormat("Test Rele control impulso {0}", output.Value.ToString());

                SBT.WriteRelayImpulseControl(output.Value, CirwattDlms.enumStateRelay.Open);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la entrada {0} activada", output.Value.ToString()), 10, 200);

                SBT.WriteRelayImpulseControl(output.Value, CirwattDlms.enumStateRelay.Closed);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la entrada {0} activada despues de desactivarla", output.Value.ToString()), 10, 200);
            }

            SBT.CerrarSesion(false);

            Resultado.Set("TEST_RELES_CONTROL_IMPULSOS", "OK", ParamUnidad.SinUnidad);
        }

        internal void internalTestEntradaImpulsos(List<KeyValuePair<byte, CirwattDlms.enumKeyInputs>> inputsValue)
        {
            SBT.IniciarSesion();

            foreach (KeyValuePair<byte, CirwattDlms.enumKeyInputs> input in inputsValue)
                SamplerWithCancel((t) =>
                {
                    tower.IO.DO.OffWait(150, input.Key);

                    SamplerWithCancel((p) =>
                    {
                        return this.SBT.ReadInputsImpulse() == CirwattDlms.enumKeyInputs.NOTHING;
                    }, "Error. alguna entrada activada cuando no deberia", 5, 200);

                    logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                    tower.IO.DO.On(input.Key);

                    SamplerWithCancel((p) =>
                    {
                        var val = (int)input.Value;
                        var valsbt = (int)this.SBT.ReadInputsImpulse();
                        logger.InfoFormat("Input Tower={0}   Circamp={1}", val, valsbt);
                        return val == valsbt;
                    }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 30, 100, 0, false, false);

                    tower.IO.DO.Off(input.Key);

                    SamplerWithCancel((p) =>
                    {
                        return this.SBT.ReadInputsImpulse() == CirwattDlms.enumKeyInputs.NOTHING;
                    }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 5, 500, 1000);

                    return true;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()));

            SamplerWithCancel((p) =>
            {
                return this.SBT.ReadInputsImpulse() == CirwattDlms.enumKeyInputs.NOTHING;
            }, "Error. alguna entrada activada después de desactivar todos los reles", 5, 200);

            SBT.CerrarSesion(false);

            Resultado.Set("TEST_ENTRADAS_IMPULSOS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestTeclado()
        {
            SBT.IniciarSesion();

            var inputsValue = new List<KeyValuePair<byte, CirwattDlms.enumKeyBoard>>();
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyBoard>(OUT_PISTON_TECLA1, CirwattDlms.enumKeyBoard.TECLA1));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyBoard>(OUT_PISTON_TECLA2, CirwattDlms.enumKeyBoard.TECLA2));

            SamplerWithCancel((p) =>
            {
                return this.SBT.ReadKeyboard() == CirwattDlms.enumKeyBoard.NOTHING;
            }, "Error. alguna entrada activada cuando no deberia", 5, 200);

            foreach (KeyValuePair<byte, CirwattDlms.enumKeyBoard> input in inputsValue)
            {
                tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    return this.SBT.ReadKeyboard() == input.Value;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 5, 200);

                tower.IO.DO.Off(input.Key);
            }

            SamplerWithCancel((p) =>
            {
                return this.SBT.ReadKeyboard() == CirwattDlms.enumKeyBoard.NOTHING;
            }, "Error. Alguna tecla detectada cuando no esta pulsada", 5, 200);

            SBT.CerrarSesion(false);

            Resultado.Set("TEST_TECLADO", "OK", ParamUnidad.SinUnidad);
        }

        public void TestPLC()
        {
            SamplerWithCancel((p) =>
            {
                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);
                var meter = device.Where((r) => r.IsSBT == false && r.MeterID == string.Format("CIR{0:0000000000}", 1000000)).FirstOrDefault();
                if (meter == null)
                    throw new Exception("Error. no se ha detectado el CIRCAMP conectado al concentrador");

                logger.InfoFormat("TEST_PLC OK conectado CIRCAMP con meterID: {0} y MAC: {1}", meter.MeterID, meter.MAC);

                if(meter.MAC == "00:00:00:00:00:00")
                    throw new Exception("Error. se ha leido una MAC = 0 del CIRCAMP");

                TestInfo.NumMAC_PLC = meter.MAC;

                Resultado.Set("MAC_PLC", meter.MAC, ParamUnidad.SinUnidad);

                Resultado.Set("TEST_PLC", "OK", ParamUnidad.SinUnidad);

              //  cdc.Soap.TestDeviceComunication(meter.MeterID);

                return true;
            }, "", 20, 5000, 15000, false, false);

            tower.IO.DO.Off(OUT_ACTIVE_VL2_TO_CDC);

            SamplerWithCancel((p) =>
            {
                logger.InfoFormat("Borramos todos los nodos del concentrador");
                cdc.DeleteListNodes();
                Delay(2000, "Esperamos eliminar nodos del concentrador");
                return true;
            }, "Error. comunicaciones SSH al borrar BBDD del COMPAC DC", 5, 1000, 1000, false, false);

            tower.IO.DO.Off(OUT_ACTIVE_230V_CDC);
        }

        public void TestLeds()
        {
            string modelo = this.GetType().Name == "CIRCAMP2IPTes" ? "2IP" : "4DP";

            SBT.IniciarSesion();

            SBT.WriteActivateLeds(CirwattDlms.enumLeds.PLC_GREEN);

            tower.IO.DO.Off(OUT_PISTON_OPTICO);

            this.TestHalconFindLedsProcedure(IDS_CAMERA_LED, string.Format("CIRCAMP_{0}_LED_GREEN",modelo), "LED_GREEN", modelo);

            SBT.WriteActivateLeds(CirwattDlms.enumLeds.PLC_RED);

            this.TestHalconFindLedsProcedure(IDS_CAMERA_LED, string.Format("CIRCAMP_{0}_LED_RED",modelo), "LED_RED", modelo);

            tower.IO.DO.On(OUT_PISTON_OPTICO);


            SBT.WriteActivateLeds(CirwattDlms.enumLeds.SEPARATE_LED);

            Delay(1000, "Espera condicionamiento de los LEDS");

            this.TestHalconFindLedsProcedure(IDS_CAMERA_DISPLAY, string.Format("CIRCAMP_{0}_LEDS_PARES",modelo), "LEDS_PARES", modelo);

            SBT.WriteActivateDisplay(1);

            SBT.WriteActivateLeds(CirwattDlms.enumLeds.REST_SEPARATE_LED);

            Delay(1000, "Espera condicionamiento de los LEDS");

            this.TestHalconFindLedsProcedure(IDS_CAMERA_DISPLAY, string.Format("CIRCAMP_{0}_LEDS_IMPARES",modelo), "LEDS_IMPARES", modelo);

            SBT.CerrarSesion(false);
        }

        public virtual void TestDisplay()
        {
            string modelo = this.GetType().Name == "CIRCAMP2IPTes" ? "2IP" : "4DP";

            SBT.IniciarSesion();

            SBT.WriteActivateLeds(CirwattDlms.enumLeds.NULL);

            tower.IO.DO.PulseOn(200, OUT_PISTON_TECLA1);

            SBT.WriteActivateDisplay(1);

            Delay(2000, "Espera condicionamiento del Diaplay");

            this.TestHalconMatchingProcedure(IDS_CAMERA_DISPLAY, string.Format("CIRCAMP_{0}_LCD", modelo), "LCD", modelo);

            SBT.CerrarSesion(false);
        }

        //***********************************************************************************

        internal void TestAjusteVoltage(double adjustVoltage)
        {
            var defs = CreateListAdjustValueDefMaxMin(Params.GAIN_V, ParamUnidad.Puntos, "AJUSTE");

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1200, SampleMaxNumber = 5 };

            var result = SBT.AdjustVoltage(Fases, adjustVoltage, controlAdjust,
           (value) =>
           {
               for (byte i = 0; i < defs.Count(); i++)
               {
                   defs[i].Value = value[i];
                   if (!defs[i].IsValid())
                       return false;
               }

               return true;
           });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de voltage valores fuera de margenes");

            SBT.WriteVoltageGain(Fases, result.Item2);

            SBT.CerrarSesion(false);
        }

        internal void TestAjusteDesfase(double powerFactor)
        {
            double desfaseFactor = -22.2;

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1200, SampleMaxNumber = 5 };

            var defs = CreateListAdjustValueDefMaxMin(Params.GAIN_DESFASE, ParamUnidad.Puntos, "AJUSTE");

            var result = SBT.AdjustGap(Fases, TypeMeterConection.frequency._50Hz, powerFactor, desfaseFactor, controlAdjust, 
            (value) =>
            {
                for (byte i = 0; i < defs.Count(); i++)
                {
                    defs[i].Value = value[i];
                    if (!defs[i].IsValid())
                        return false;
                }
                return true; 
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de desfase valor fuera de margenes");

            SBT.WriteGapGain(Fases, TypeMeterConection.frequency._50Hz, result.Item2);

            SBT.CerrarSesion(false);
        }

        internal void TestAjustePotencia(double powerReference)
        {
            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1200, SampleMaxNumber = 5 };

            var defs = CreateListAdjustValueDefMaxMin(Params.GAIN_KW, ParamUnidad.Puntos, "AJUSTE");

            var result = SBT.AdjustPowerActive(Fases, TypeMeterConection.frequency._50Hz, powerReference, controlAdjust,
            (value) =>
            {
                for (byte i = 0; i < defs.Count(); i++)
                {
                    defs[i].Value = value[i];
                    if (!defs[i].IsValid())
                        return false;
                }
                return true;
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de potencia  valor fuera de margenes");

            SBT.WritePowerActiveGain(Fases, TypeMeterConection.frequency._50Hz, result.Item2);

            SBT.CerrarSesion(false);
        }

        // ***********************************************************************************

        public void TestPoint_MarchaVacio()
        {
            ConsignarMTE(TriLineValue.Create(meter.VoltageProperty.VoltageMaximo),TriLineValue.Create(0),TriLineValue.Create(0), meter.FrequencyProperty.Frequency);

            var defs = CreateListAdjustValueDefMaxMin(Params.POINTS, ParamUnidad.Puntos, "MARCHA_VACIO");

            SamplerWithCancel((p) =>
            {
                SBT.IniciarSesion();

                var points = SBT.ReadPowerPoints(Fases, TipoPotencia.Activa).ToArray();
                var gain = SBT.ReadPowerGain(Fases, TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);

                for (byte i = 0; i < defs.Count(); i++)
                {
                    defs[i].Value = points[i];
                    defs.AddToResults(Resultado);

                    if (!defs[i].IsValid())
                        throw new Exception(string.Format("Error. puntos de potencia L{0} leidos mayor a la ganacia el equipo, consume energia", i + 1));                 
                }

                SBT.CerrarSesion(false);

                return true;

            }, "", initialDelay:1000, cancelOnException: false, throwException: false);
        }

        public void TestPoint_Arranque()
        {
            if (Fases == CirwattDlms.enumFases.QUATRIFASICO)
                ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase * (0.4 / 100), 0, meter.FrequencyProperty.Frequency);
            else
                ConsignarMTE(TriLineValue.Create(meter.VoltageProperty.VoltageNominal), TriLineValue.Create(meter.CurrentProperty.CorrienteBase * (0.4 / 100)), TriLineValue.Create(0), meter.FrequencyProperty.Frequency);

            var defs = CreateListAdjustValueDefMaxMin(Params.POINTS, ParamUnidad.Puntos, "ARRANQUE");

            SamplerWithCancel((p) =>
            {
                SBT.IniciarSesion();

                var points = SBT.ReadPowerPoints(Fases, TipoPotencia.Activa).ToArray();
                var gain = SBT.ReadPowerGain(Fases, TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);

                for (byte i = 0; i < defs.Count(); i++)
                {
                    defs[i].Value = points[i];
                    defs.AddToResults(Resultado);

                    if (!defs[i].IsValid())              
                        throw new Exception(string.Format("Error. puntos de potencia L{0} leidos menor a la ganacia el equipo no ve la corriente", i + 1));              
                }

                SBT.CerrarSesion(false);

                return true;

            }, "", initialDelay:3000, cancelOnException: false, throwException: false);
        }

        public void TestPoint_Imin([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            TestPointCalibration(TestPointsName.TP_IMIN, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IbCosPhi(int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            TestPointCalibration(TestPointsName.TP_IbCOS05, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestCostumize()
        {
            SBT.IniciarSesion();

            SBT.EscribirNumeroSerie(Convert.ToUInt32(TestInfo.NumSerie));

            Delay(1000, "Espera grabacion numero de serie");

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            Delay(1000, "Espera grabacion numero de serie");

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 230, L3 = 0 }, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            SamplerWithCancel((p) =>
            {
                this.SBT.IniciarSesion();
                return true;
            }, "Error de comunicaciones, No inicia sesion después de hacer un reset hardware", 2, 1000, 500, true, true);

            var serialNumber = SBT.LeerNumeroSerie();
            Assert.IsTrue(serialNumber.ToString() == TestInfo.NumSerie.ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El número de serie grabado en el equipo no corresponde con el leido"));

            Resultado.Set("NUMERO_SERIE", serialNumber.ToString(), ParamUnidad.SinUnidad);

            var data = SBT.LeerFechaHora();

            double timeDiff = (DateTime.Now.Subtract(data)).TotalSeconds;

            Resultado.Set("DIFF_TIME", timeDiff, ParamUnidad.s);

            Assert.AreGreater(Configuracion.GetDouble("MAX_DIFF_TIME", 30, ParamUnidad.s), timeDiff, Error().UUT.HARDWARE.MARGENES("Error. El equipo ha perdido mas de 30 segundos después del reset de hardware"));

            var modelo = SBT.LeerModelo();
            Resultado.Set(ConstantsParameters.Identification.MODELO, modelo, ParamUnidad.SinUnidad, Identificacion.MODELO);
            Assert.IsTrue(modelo == Identificacion.MODELO, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Modelo del equipo incorrecto"));

            SBT.EscribirEstadoFabricacion(0);

            Resultado.Set("CODE_ERROR", 0, ParamUnidad.SinUnidad);

            SBT.EscribirReset();

            SBT.CerrarSesion(false);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public virtual void TestFinish()
        {
            if (SBT != null)
            {
                SBT.CerrarSesion(false);
                SBT.Dispose();
            }

            if (cdc != null)
                cdc.Dispose();
            
            if (tower != null)
                tower.Dispose();
        }

        //**********************************************************************************

        private void TestPointCalibration(TestPointsName testPointName, TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            TestPoint testPoint = new TestPoint(meter, testPointName, tipoPotencia);

            if (Fases == CirwattDlms.enumFases.QUATRIFASICO)
                ConsignarMTE(testPoint.Voltage, testPoint.Current, testPoint.Desfase, testPoint.Frequency);
            else
                ConsignarMTE(TriLineValue.Create(testPoint.Voltage), TriLineValue.Create(testPoint.Current), TriLineValue.Create(testPoint.Desfase), testPoint.Frequency);

            internalVerification(testPoint, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        private void internalVerification(TestPoint testPoint, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            var potenciaAparente = testPoint.Voltage * testPoint.Current;
            var potenciaActiva = potenciaAparente * Math.Cos(testPoint.Desfase.ToRadians());

            var parameter = new ParamValueCollection(testPoint.Name);
            parameter.Set(testPoint.Name, 0, ParamUnidad.W);
            var parameters = new FasesParameters(parameter);

            var defs = CreateListAdjustValueDefVarification(parameters, potenciaActiva, testPoint.Tolerance,ParamUnidad.W,  "AJUSTE");

            TestCalibracionBase(defs, (Func<double[]>)(() =>
            {
                List<double> powerActive;
                if (Fases == CirwattDlms.enumFases.QUATRIFASICO)
                    powerActive = this.SBT.Leer_Potencia(Fases, TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);
                else
                    powerActive = this.SBT.Leer_Potencia(Fases, TipoPotencia.Activa);

                logger.InfoFormat("Potencia Activa CIRCAMP : {0}", (object)powerActive.ToString());

                return (double[])powerActive.ToArray();
            }),
            () =>
            {
                var lecturas = new List<double>();

                var current = tower.PowerSourceIII.ReadCurrent();
                var voltage = tower.PowerSourceIII.ReadVoltage();
                var powerFactor = tower.PowerSourceIII.ReadPowerFactor();

                var activePower = TriLinePower.Create(voltage, current, powerFactor);

                if (Fases == CirwattDlms.enumFases.QUATRIFASICO)
                {
                    lecturas.Add(activePower.KW.L2);
                    lecturas.Add(activePower.KW.L2);
                    lecturas.Add(activePower.KW.L2);
                    lecturas.Add(activePower.KW.L2);
                }else 
                {
                    activePower = 1000 * activePower;
                    lecturas.Add(activePower.KW.L1);
                    lecturas.Add(activePower.KW.L2);
                    lecturas.Add(activePower.KW.L3);
                    lecturas.Add(activePower.KW.L1);
                    lecturas.Add(activePower.KW.L2);
                    lecturas.Add(activePower.KW.L3);
                }

                logger.InfoFormat("Potencia Activa MTE PRS :{0}", activePower.ToString());

                return lecturas.ToArray();

            }, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        private void ConsignarMTE(double tension, double corriente, double desfase, double frecuencia)
        {
            var voltage = TriLineValue.Create(0,tension, 0);
            var current = TriLineValue.Create(0, corriente, 0);
            var gap = TriLineValue.Create(0, desfase, 0);

            ConsignarMTE(voltage, current, gap, frecuencia);
        }

        private void ConsignarMTE(TriLineValue voltage, TriLineValue current, TriLineValue gap, double frecuencia)
        {
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, frecuencia, gap, TriLineValue.Create(0, 120, 240), true);
        }

        private List<AdjustValueDef> CreateListAdjustValueDefMaxMin(FasesParameters name, ParamUnidad unidad, string testPoint = "")
        {
            var defs = new List<AdjustValueDef>();

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_1))
                defs.Add(new AdjustValueDef(name.L1.TestPoint(testPoint).Name, 0, name.Null.TestPoint(testPoint).Min(), name.Null.TestPoint(testPoint).Max(), 0, 0, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_2))
                defs.Add(new AdjustValueDef(name.L2.TestPoint(testPoint).Name, 0, name.Null.TestPoint(testPoint).Min(), name.Null.TestPoint(testPoint).Max(), 0, 0, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_3))
                defs.Add(new AdjustValueDef(name.L3.TestPoint(testPoint).Name, 0, name.Null.TestPoint(testPoint).Min(), name.Null.TestPoint(testPoint).Max(), 0, 0, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_4))
                defs.Add(new AdjustValueDef(name.L4.TestPoint(testPoint).Name, 0, name.Null.TestPoint(testPoint).Min(), name.Null.TestPoint(testPoint).Max(), 0, 0, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_5))
                defs.Add(new AdjustValueDef(name.L5.TestPoint(testPoint).Name, 0, name.Null.TestPoint(testPoint).Min(), name.Null.TestPoint(testPoint).Max(), 0, 0, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_6))
                defs.Add(new AdjustValueDef(name.L6.TestPoint(testPoint).Name, 0, name.Null.TestPoint(testPoint).Min(), name.Null.TestPoint(testPoint).Max(), 0, 0, unidad));

            return defs;
        }

        private List<AdjustValueDef> CreateListAdjustValueDefVarification(FasesParameters name, double average, double tolerance, ParamUnidad unidad, string testPoint = "")
        {
            var defs = new List<AdjustValueDef>();

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_1))
                defs.Add(new AdjustValueDef(name.L1.TestPoint(testPoint).Name, 0, 0, 0, average, tolerance, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_2))
                defs.Add(new AdjustValueDef(name.L2.TestPoint(testPoint).Name, 0, 0, 0, average, tolerance, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_3))
                defs.Add(new AdjustValueDef(name.L3.TestPoint(testPoint).Name, 0, 0, 0, average, tolerance, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_4))
                defs.Add(new AdjustValueDef(name.L4.TestPoint(testPoint).Name, 0, 0, 0, average, tolerance, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_5))
                defs.Add(new AdjustValueDef(name.L5.TestPoint(testPoint).Name, 0, 0, 0, average, tolerance, unidad));

            if (Fases.HasFlag(CirwattDlms.enumFases.FASE_6))
                defs.Add(new AdjustValueDef(name.L6.TestPoint(testPoint).Name, 0, 0, 0, average, tolerance, unidad));

            return defs;
        }
    }
}
