﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(OperationMathTwoOperatorDescription))]
    public class OperationMathTwoOperator : ActionBase
    {
        [Description("Nombre del dato o el valor en double correspondiente al operador 1")]
        [Category("3. Configuration")]
        public ActionVariable DataInput1 { get; set; } = new ActionVariable();

        [Description("Nombre del dato o el valor en double correspondiente al operador 2")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable DataInput2 { get; set; } = new ActionVariable();

        [Description("Tipo de operación matemática a realizar entre DataInPut1 y DataInput2 (Las operaciones de redondeo solo tienen en cuenta el DataInput1)")]
        [Category("3. Configuration")]
        public Operator Operation { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public enum Operator
        {
            Addition,
            Subtraction,
            Multiplication,
            Division,
            Exponent,
            Division_Int,
            Module,
            RoundOff,
            Ceiling,
            Floor,
            Truncate
        }
       
        public override void Execute()
        {
            var result = 0D;
            var opr1 = Convert.ToDouble(DataInput1.Value.ToString());
            var opr2 = Convert.ToDouble(DataInput2.Value.ToString());

            Logger.InfoFormat("Operador1={0}  {1}  Operador2={2}", opr1, Operation.ToString(), opr2);

            switch (Operation)
            {
                case Operator.Addition:
                    result = opr1 + opr2;
                    break;
                case Operator.Subtraction:
                    result = opr1 - opr2;
                    break;
                case Operator.Multiplication:
                    result = opr1 * opr2;
                    break;
                case Operator.Division:
                    result = opr1 / opr2;
                    break;
                case Operator.Exponent:
                    result = Math.Pow(opr1 , opr2);
                    break;
                case Operator.Division_Int:
                    result = Math.Truncate(opr1 / opr2);
                    break;
                case Operator.Module:
                    result = (int)opr1 % (int)opr2;
                    break;
                case Operator.RoundOff:
                    result = Math.Round(opr1);
                    break;
                case Operator.Floor:
                    result = Math.Floor(opr1);
                    break;
                case Operator.Ceiling:
                    result = Math.Ceiling(opr1);
                    break;
                case Operator.Truncate:
                    result = (double)(Math.Truncate(opr1 * opr2) / opr2);
                    break;
            }

            Logger.InfoFormat("Resultado ={0}", result);
            Context.ResultList.Add(string.Format("Operador1={0}  {1}  Operador2={2}  Resultado={3}", opr1, Operation.ToString(), opr2, result));
            DataOutput.Value = result;
        }
    }

    public class OperationMathTwoOperatorDescription : ActionDescription
    {
        public override string Name { get { return "OperationMathTwoOperator"; } }
        public override string Description { get { return "Realiza la operación matemática elegida con dos operadores como entrada"; } }
        public override string Category { get { return "Actions/Math"; } }
        public override Image Icon { get { return Properties.Resources.Math; } }
    }
}
