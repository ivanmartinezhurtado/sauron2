﻿using Comunications.Message;
using System.Text;

namespace Comunications.Protocolo
{
    public class StringProtocol : ASCIIProtocol
    {
        public override byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            if (message.SearchLastEndLine > 1)
            {
                string result = "";
                for(var i =0;i< message.SearchLastEndLine;i++)
                    result += streamResource.ReadLineString();
                return ASCIIEncoding.ASCII.GetBytes(result);
            }
            else
            {
                string result = streamResource.ReadLineString();
                return ASCIIEncoding.ASCII.GetBytes(result);
            }
        }
    }
}
