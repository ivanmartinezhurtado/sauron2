﻿using System;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class HR5XX : DeviceBase
    {
        #region Instance and disposal

        public HR5XX()
        {
        }

        public HR5XX(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
            Modbus.UseFunction06 = true;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #endregion

        #region Read operations

        public string ReadFimwareVersion()
        {
            LogMethod();
            return (Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION) / 10D).ToString();
        }

        public Models ReadModel()
        {
            LogMethod();
            return (Models)Modbus.ReadRegister((ushort)Registers.DEVICE_MODEL);
        }

        public Inputs ReadInputs()
        {
            LogMethod();
            return (Inputs)Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public int ReadCurrent()
        {
            LogMethod();
            var current = Modbus.ReadRegister((ushort)Registers.CURRENT);
            return current;
        }

        public int ReadTime()
        {
            LogMethod();
            var time = Modbus.ReadRegister((ushort)Registers.TIME);
            return time;
        }

        public int ReadCalibrationResult()
        {
            LogMethod();
            var highLevel = Modbus.ReadRegister((ushort)Registers.CALIBRATION_RESULT_HIGH_LEVEL);
            var lowLevel =  Modbus.ReadRegister((ushort)Registers.CALIBRATION_RESULT_LOW_LEVEL);

            return ((highLevel << 16) + lowLevel);            
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            var highLevel = Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_HIGH_LEVEL);
            var lowLevel = Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_LOW_LEVEL);

            return ((highLevel << 16) + lowLevel);
        }

        public int ReadFrameNumber()
        {
            LogMethod();
            var highLevel = Modbus.ReadRegister((ushort)Registers.FRAME_NUMBER_HIGH_LEVEL);
            var lowLevel = Modbus.ReadRegister((ushort)Registers.FRAME_NUMBER_LOW_LEVEL);

            return ((highLevel << 16) + lowLevel);
        }

        public ushort ReadErrorCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        #endregion

        #region Write operations

        public void FlagTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)Operations.FLAG_TEST);
        }

        public void Reset()
        {
            LogMethod();
            Modbus.Retries = 0;
            Modbus.WithTimeOut((x) => x.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)Operations.RESET));
            Modbus.Retries = 3;
        }

        public void WriteModel(string model)
        {
            LogMethod();

            Models modelEnum = (Models)Enum.Parse(typeof(Models), model);

            Modbus.Write((ushort)Registers.DEVICE_MODEL, (ushort)modelEnum);
        }

        public void WriteDeviceOperations(Operations operation)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)operation);
        }

        public void WriteOutputs(Outputs outputs)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)outputs);
        }

        public void WriteLeds(Leds leds)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LEDS, (ushort)leds);
        }

        public void WriteCalibrationOrder()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DEVICE_OPERATIONS, (ushort)Operations.START_CALIBRATION);
        }

        public void WriteCalibrationFactors(int calibrationFactor)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CALIBRATION_RESULT_HIGH_LEVEL, (ushort)(calibrationFactor / ushort.MaxValue));
            Modbus.Write((ushort)Registers.CALIBRATION_RESULT_LOW_LEVEL, (ushort)(calibrationFactor % ushort.MaxValue));
        }

        public void WriteSerialNumber(int serialNumber)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SERIAL_NUMBER_HIGH_LEVEL, (ushort)(serialNumber >> 16));
            Modbus.Write((ushort)Registers.SERIAL_NUMBER_LOW_LEVEL, (ushort)(serialNumber));
        }

        public void WriteFrameNumber(int frameNumber)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FRAME_NUMBER_HIGH_LEVEL, (ushort)(frameNumber >> 16));
            Modbus.Write((ushort)Registers.FRAME_NUMBER_LOW_LEVEL, (ushort)(frameNumber));
        }

        public void WriteErrorCode(ushort errorCode)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ERROR_CODE, (ushort)(errorCode / ushort.MaxValue));

        }

        #endregion

        #region Registers and registers info


        public enum Models
        {
            HR_510 = 0x0A,
            HR_520 = 0x14,
            HR_522 = 0x16,
            HR_523 = 0X17,
        }

        public enum Operations
        {
            RESET = 0x0000,
            FLAG_TEST = 0x0002,
            START_CALIBRATION = 0x0003
        }

        [Flags]
        public enum Inputs
        {
            EXTERNAL_TEST_RESET = 0x0001,
            TECLA_TEST = 0x0008,
            TECLA_RESET = 0x0010,
            TOROIDAL_ERROR = 0x0020
        }

        public enum Outputs
        {
            RELE_DISPARO = 0x0001,
            RELE_SEGURIDAD_POSITIVA = 0x0002,
            RELE_PREALARMA = 0x0004,
        }

        public enum Leds
        {
            NONE = 0x0000, 
            ON = 0x0006,
            DISPARO = 0x000C,
            LED_5 =0012,
            LED_15 = 0x0018,
            LED_30 = 0x001E,
            LED_45 = 0x0100,
            LED_60 = 0x0200,
        }

        public enum Registers
        {
            FIRMWARE_VERSION = 0x0000,
            DEVICE_OPERATIONS = 0x0001,
            OUTPUTS = 0x0002,
            LEDS = 0x0003,
            INPUTS = 0x0004,
            CURRENT = 0x0005,
            TIME = 0x0006,
            CALIBRATION_RESULT_HIGH_LEVEL = 0x0007,
            CALIBRATION_RESULT_LOW_LEVEL = 0x0008,
            DEVICE_MODEL = 0x0009,
            SERIAL_NUMBER_HIGH_LEVEL = 0x000A,
            SERIAL_NUMBER_LOW_LEVEL = 0x000B,
            FRAME_NUMBER_HIGH_LEVEL = 0x000C,
            FRAME_NUMBER_LOW_LEVEL = 0x000D,
            ERROR_CODE = 0x000E,
        }

        #endregion
    }
}