namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.HOJAMARCAJE")]
    public partial class HOJAMARCAJE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HOJAMARCAJE()
        {
            DIARIOMARCAJE = new HashSet<DIARIOMARCAJE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMHOJA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDACTIVIDAD { get; set; }

        public int? NUMORDEN { get; set; }

        [StringLength(10)]
        public string IDACTIVIDADREPROCESO { get; set; }

        [StringLength(10)]
        public string IDCAUSANTEREPROCESO { get; set; }

        public int? NUMTRABAJO { get; set; }

        [StringLength(1)]
        public string MODIFICACIONSINO { get; set; }

        [StringLength(1)]
        public string PREPARACIONSINO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJE> DIARIOMARCAJE { get; set; }

        public virtual ORDEN ORDEN { get; set; }
    }
}
