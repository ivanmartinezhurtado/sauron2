﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.18)]
    public class EKOR_SAFETestBaseNew : TestBase
    { 
        protected ITower tower { get; set; }
        protected IEKOR_SAFE Ekor { get; set; }       
        protected IUtillajeEkor util { get; set; }
        protected bool TestPlacas { get; set; }

        internal const byte IN_TRIGGER_RELAY = 12;
        internal const int OUT_ACTIVE_CRGA_33R = 22;
        internal const int OUT_ACTIVE_RS485_B = 17;
        internal const int OUT_ACTIVE_RS485_EMBEDDED = 43;

        protected string CAMERA_IDS_DISPLAY;
        protected string CAMERA_IDS_LEDS;
        private int OUT_DETECTION_DO_2 = 43;
        private int EXTERN_POLARIZATION_INPUTS = 22;

        protected struct StructCurrentsInVoltageChannel
        {
            public TriLineValue currentVL { get; set; }
            public TriLineValue currentVF { get; set; }
            public TriLineValue resistenceVL { get; set; }
            public TriLineValue resistenceVF { get; set; }

            public TriLineValue CurrentsVL
            {
                get
                {
                    var values = (currentVL / resistenceVL) * 1000000;
                    return values;
                }
            }
            public TriLineValue CurrentsVF
            {
                get
                {
                    var values = (currentVF / resistenceVF) * 1000000;
                    return values;
                }
            }

            public List<double> AllCurrents
            {
                get
                {
                    var value = new List<double>();
                    value.AddRange(CurrentsVL.ToArray());
                    value.AddRange(CurrentsVF.ToArray());
                    return value;
                }
            }
        }

        protected StructCurrentsInVoltageChannel measureValue;

        public string Version { get; set; }

        public EKOR_SAFE_OLD.ModelHardware ModeloEKOR
        {
            get
            {
                EKOR_SAFE_OLD.ModelHardware modelo = EKOR_SAFE_OLD.ModelHardware.WTP100;
                switch (Identificacion.MODELO.ToUpper().Trim())
                {
                    case "WTP100":
                        modelo = EKOR_SAFE_OLD.ModelHardware.WTP100;
                        break;
                    case "WTP10":
                        modelo = EKOR_SAFE_OLD.ModelHardware.WTP10;
                        break;
                    case "WTP200B":
                        modelo = EKOR_SAFE_OLD.ModelHardware.WTP200;
                        break;
                    case "WTP200":
                        modelo = EKOR_SAFE_OLD.ModelHardware.WTP200;
                        break;
                    case "MVE":
                        modelo = EKOR_SAFE_OLD.ModelHardware.MVE;
                        break;
                    case "RPA100":
                        modelo = EKOR_SAFE_OLD.ModelHardware.RPA100;
                        break;
                    case "MVE_CPU":
                        modelo = EKOR_SAFE_OLD.ModelHardware.MVE_CPU;
                        break;
                    case "RPG_CI_RTU":
                        modelo = EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU;
                        break;
                    case "RPA200":
                        modelo = EKOR_SAFE_OLD.ModelHardware.RPA200_C;
                        break;
                }
                return modelo;
            }
        }

        public EKOR_SAFE_OLD.ModelHardware ModeloFabricacionEKOR
        {
            get
            {
                EKOR_SAFE_OLD.ModelHardware modelo = EKOR_SAFE_OLD.ModelHardware.WTP100;
                switch (Identificacion.MODELO_FABRICA.ToUpper().Trim())
                {
                    case "FABPLUS":
                        modelo = EKOR_SAFE_OLD.ModelHardware.FABPLUS;
                        break;
                    case "FABSAFE":
                        modelo = EKOR_SAFE_OLD.ModelHardware.FABSAFE;
                        break;
                    case "FAB_ENERGYE":
                        modelo = EKOR_SAFE_OLD.ModelHardware.FAB_ENERGY_C;
                        break;
                    default:
                        Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("MODELO_FABRICA").Throw();
                        break;              
                }
                return modelo;
            }
        }

        public virtual void TestSetupDefault()
        {
            EKOR_SAFE_OLD.ModelHardware model = ModeloFabricacionEKOR;

            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    Delay(5000, "Esperando a que el equipo genere puerto virtual después del reset");
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    Ekor.RecoveryDefaultSettings(Ekor.Cirbus.PortCom, 9600, 1);
                    Delay(2000, "");
                }

                Ekor.FATFormat();

                Ekor.FATResetDefaultFabrica();

                logger.InfoFormat("Modelo Fabrica: {0}", model.ToString());

                Ekor.WriteConfigurationDefaultFab(model);

                return true;
            }, "Error de comunicaciones USB", 5, 1000, 1000, false, false,
            (p) =>
                 Error().UUT.COMUNICACIONES.INICIALIZACION("USB").Throw()
            );

            Delay(2500, "Aplicando cambio de modelo");

            if (model != EKOR_SAFE_OLD.ModelHardware.FABPLUS)
                util.ResetUSB();
            
        }

        public virtual void TestCommunicationsUSB(string nameDevicePortVirtual)
        {
            var portEkor = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));

            Ekor.Cirbus.PortCom = portEkor;

            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    Delay(5000, "Esperando a que el equipo genere puerto virtual después del reset");
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    Ekor.RecoveryDefaultSettings(Ekor.Cirbus.PortCom, 9600, 1);
                    Delay(2000, "");
                }

                var versionEkor = Ekor.ReadVersion();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, (string)versionEkor.version, Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del Ekor no es correcta"), ParamUnidad.SinUnidad);

                var versionEkorDeveloper = Ekor.ReadVersionDesarrollo();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_DEVELOPER, (string)versionEkorDeveloper.version, Identificacion.VERSION_DEVELOPER.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware de desarrolo del Ekor no es correcta"), ParamUnidad.SinUnidad);

                var versionMicro = Ekor.ReadMicrocontrolerType();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, (string)versionMicro.cpu, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.HARDWARE.NO_COINCIDE("Error, la  tipo de microcontrolador no es el correcto"), ParamUnidad.SinUnidad);

                Resultado.Set("CPU REVISION", (string)versionMicro.revision, ParamUnidad.SinUnidad);

                var versionBoot = Ekor.ReadVersionBoot();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_BOOT, (string)versionBoot, Identificacion.VERSION_BOOT.Trim(), Error().UUT.HARDWARE.NO_COINCIDE("Error, version de boot incorrecto"), ParamUnidad.SinUnidad);

                //var crcBoot = Ekor.ReadCRCBoot();

                //Assert.AreEqual(ConstantsParameters.Identification.CRC_BOOT, crcBoot, Identificacion.CRC_BOOT.Trim(), "Error, crc de boot incorrecto", ParamUnidad.SinUnidad);

                return true;

            }, string.Empty, 3, 1000, 500, false, false);

            Resultado.Set("COMMUNICATIONS_USB", "OK", ParamUnidad.SinUnidad);

            Ekor.WriteBastidor(TestInfo.NumBastidor.Value.ToString());

            var stateFlash = Ekor.ReadStateFLASH();

            Resultado.Set("FLASH_ID", stateFlash.ID, ParamUnidad.SinUnidad);
            Resultado.Set("FLASH_STATUS", stateFlash.state.ToString(), ParamUnidad.SinUnidad);

            Assert.IsTrue(stateFlash.state == EKOR_SAFE_OLD.stateFLAASH.READY, Error().UUT.MEMORIA.ALARMA(string.Format("flash {0}", stateFlash.state.ToString())));

            DriveInfo usbDrive = null;

            SamplerWithCancel((p) =>
            {
                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                {
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKOR_DISK", ParamUnidad.SinUnidad).Trim())
                    {
                        usbDrive = drive;
                        break;
                    }
                }

                if (usbDrive == null)
                    throw new Exception("Error no se detecta la unidad USB en el S.O.");

                logger.InfoFormat("USB_DRIVE RottDirectory: {0}", usbDrive.RootDirectory.Name);
                Resultado.Set("USB_DRIVE", usbDrive.VolumeLabel, ParamUnidad.SinUnidad);

                return !string.IsNullOrEmpty(usbDrive.RootDirectory.Name);
            }, "Error No se ha encontrado el dispositivo USB del EKOR", 3, 1000, 1000, false, false);
        }

        public virtual void TestInputs()
        {
            var inputsValueFromUtil = util.GetKeyValuePairInputs();
            var inputsValue = new List<KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>>();
            inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[0].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[0].Value));
            inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[1].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[1].Value));

            if (ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.WTP100 && ModeloEKOR!= EKOR_SAFE_OLD.ModelHardware.MVE_CPU)
            {
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[2].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[2].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[3].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[3].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[4].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[4].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[5].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[5].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[6].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[6].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[7].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[7].Value));

                if (ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU)
                {
                    inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[8].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[8].Value));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[9].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[9].Value));
                }
            }

            foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs> input in inputsValue)
            {
                Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                if ((ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA100 || ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && input.Value > EKOR_SAFE_OLD.enumInputs.IN3)
                    tower.IO.DO.On(EXTERN_POLARIZATION_INPUTS);

                tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    if (ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA100 && input.Value == EKOR_SAFE_OLD.enumInputs.IN7)
                        return !Ekor.ReadDigitalInput((int)input.Value);
                    return Ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 3, 500, 200, false, false);

                if ((ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPA100 && ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && (input.Value == EKOR_SAFE_OLD.enumInputs.SAFE1 || input.Value == EKOR_SAFE_OLD.enumInputs.SAFE2))
                {
                    if (!tower.IO.DI.WaitOn(IN_TRIGGER_RELAY, 5000))
                        throw new Exception("Error. no se detecta la IN12 (Rele disparo del equipo) activada");
                }

                tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    return !Ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 3, 500, 200, false, false);

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);

                if ((ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPA100 && ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && (input.Value == EKOR_SAFE_OLD.enumInputs.SAFE1 || input.Value == EKOR_SAFE_OLD.enumInputs.SAFE2))
                {
                    if (!tower.IO.DI.WaitOff(IN_TRIGGER_RELAY, 5000))
                        throw new Exception("Error. no se detecta la IN12 (Rele disparo del equipo) desactivada");

                    Resultado.Set("INPUT_TRIGGER", "OK", ParamUnidad.SinUnidad);
                }
            }

            tower.IO.DO.Off(EXTERN_POLARIZATION_INPUTS);
        }

        public virtual void TestInputsAll()
        {
            var inputsValueFromUtil = util.GetKeyValuePairInputs();
            var inputsValue = new List<KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>>();
            inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[0].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[0].Value));
            inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[1].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[1].Value));

            if (ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.WTP100 && ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.MVE_CPU)
            {
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[2].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[2].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[3].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[3].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[4].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[4].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[5].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[5].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[6].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[6].Value));
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[7].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[7].Value));

                if (ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU)
                {
                    inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[8].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[8].Value));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs>(inputsValueFromUtil[9].Key, (EKOR_SAFE_OLD.enumInputs)inputsValueFromUtil[9].Value));
                }
            }

            foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs> input in inputsValue)
            {
                Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                if ((ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA100 || ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && input.Value > EKOR_SAFE_OLD.enumInputs.IN3)
                    tower.IO.DO.On(EXTERN_POLARIZATION_INPUTS);

                tower.IO.DO.On(input.Key);            
            }

            Delay((int)Configuracion.GetDouble("WAIT_TIME_ALL_INPUTS", 5000, ParamUnidad.s), "Wait Inputs all On");

            foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs> input in inputsValue)
            {
                SamplerWithCancel((p) =>
                {
                    if (ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA100 && input.Value == EKOR_SAFE_OLD.enumInputs.IN7)
                        return !Ekor.ReadDigitalInput((int)input.Value);
                    return Ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 3, 500, 200, false, false);

                if ((ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPA100 && ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && (input.Value == EKOR_SAFE_OLD.enumInputs.SAFE1 || input.Value == EKOR_SAFE_OLD.enumInputs.SAFE2))
                {
                    if (!tower.IO.DI.WaitOn(IN_TRIGGER_RELAY, 5000))
                        throw new Exception("Error. no se detecta la IN12 (Rele disparo del equipo) activada");
                }
            }


           foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumInputs> input in inputsValue)
            {
                tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    return !Ekor.ReadDigitalInput((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 3, 500, 200, false, false);

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);

                if ((ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPA100 && ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && (input.Value == EKOR_SAFE_OLD.enumInputs.SAFE1 || input.Value == EKOR_SAFE_OLD.enumInputs.SAFE2))
                {
                    if (!tower.IO.DI.WaitOff(IN_TRIGGER_RELAY, 5000))
                        throw new Exception("Error. no se detecta la IN12 (Rele disparo del equipo) desactivada");

                    Resultado.Set("INPUT_TRIGGER", "OK", ParamUnidad.SinUnidad);
                }
            }

            tower.IO.DO.Off(EXTERN_POLARIZATION_INPUTS);
        }

        public void TestOutputs()
        {
            var outputsValueFromUtil = util.GetKeyValuePairOutputs();

            var ouputsValue = new List<KeyValuePair<byte, EKOR_SAFE_OLD.enumOutputs>>();
            ouputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumOutputs>(outputsValueFromUtil[0].Key, (EKOR_SAFE_OLD.enumOutputs)outputsValueFromUtil[0].Value));
            ouputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumOutputs>(outputsValueFromUtil[1].Key, (EKOR_SAFE_OLD.enumOutputs)outputsValueFromUtil[1].Value));

            if (ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.WTP100 && ModeloEKOR != EKOR_SAFE_OLD.ModelHardware.MVE_CPU)
            {
                ouputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumOutputs>(outputsValueFromUtil[2].Key, (EKOR_SAFE_OLD.enumOutputs)outputsValueFromUtil[2].Value));
                ouputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumOutputs>(outputsValueFromUtil[3].Key, (EKOR_SAFE_OLD.enumOutputs)outputsValueFromUtil[3].Value));
            }

            foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumOutputs> output in ouputsValue)
            {
                var outputInverted = false;
                Logger.InfoFormat("{0}", output.Value.ToString());
                if ((ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA100 || ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && output.Value == EKOR_SAFE_OLD.enumOutputs.RELE2)
                {
                    outputInverted = Configuracion.GetString("OUTPUT_2_INVERTED", "NO", ParamUnidad.SinUnidad) == "SI";
                    tower.IO.DO.On(OUT_DETECTION_DO_2);
                }

                SamplerWithCancel((p) =>
                {
                    Ekor.ActivaDigitalOutput((int)output.Value);
                    if(!outputInverted)
                        return tower.IO.DI[output.Key];
                    else
                        return !tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 5, 200, 200, false, false);

                SamplerWithCancel((p) =>
                {
                    Ekor.DesactivaDigitalOutput((int)output.Value);
                    if(!outputInverted)
                        return !tower.IO.DI[output.Key];
                    else
                        return tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} desactivada", output.Value.ToString()), 5, 200, 200, false, false);

                if ((ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA100 || ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPG_CI_RTU) && output.Value == EKOR_SAFE_OLD.enumOutputs.RELE2)
                    tower.IO.DO.Off(OUT_DETECTION_DO_2);

                Resultado.Set(string.Format("{0}", output.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestTrip()
        {
            int max = 50;
            do
            {
                Ekor.Trip();
            } while (!tower.IO.DI.WaitOn(util.GetInputDetectionTrigger(), 100) && max-- != 0);

            if (max < 1)
                throw new Exception("Error no se ha detectado la señal de TRIP por software");

            Resultado.Set("TRIP", "OK", ParamUnidad.s);
        }

        public virtual void TestDetectionInvertedPhase()
        {
            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 100, ParamUnidad.V);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, 0.08, 0, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);
            Delay(2000, "Espera estabilizacion");

            SamplerWithCancel((p) =>
            {
                Ekor.ReadVoltage();
                var measure = Ekor.ReadCurrentPhase();

                double resultPhaseL1 = measure.phase1;
                if (measure.phase1 > 300)
                    resultPhaseL1 = measure.phase1 - 360;

                Assert.AreBetween(Params.PHASE.L1.Verificacion.Name, resultPhaseL1, Params.PHASE.L1.Verificacion.Min(), Params.PHASE.L1.Verificacion.Max(), TestException.Create().UUT.MEDIDA.MARGENES( "angulo de fase L1"), ParamUnidad.Grados);

                Assert.AreBetween(Params.PHASE.L2.Verificacion.Name, (double)measure.phase2, Params.PHASE.L2.Verificacion.Min(), Params.PHASE.L2.Verificacion.Max(), TestException.Create().UUT.MEDIDA.MARGENES("angulo de fase L2"), ParamUnidad.Grados);

                Assert.AreBetween(Params.PHASE.L3.Verificacion.Name, (double)measure.phase3, Params.PHASE.L3.Verificacion.Min(), Params.PHASE.L3.Verificacion.Max(), TestException.Create().UUT.MEDIDA.MARGENES("angulo de fase L3"), ParamUnidad.Grados);

                return true;

            }, "", 5, 1500, 0, false, false);

        }

        public virtual void TestDisplay()
        {
            CAMERA_IDS_DISPLAY = GetVariable<string>("CAMARA_IDS_DISPLAY", CAMERA_IDS_DISPLAY);

            Ekor.WriteDisplayTestSegmentsEven();

            Delay(750, "Espera condicionamiento del display par");

            var deviceName = ModeloFabricacionEKOR == EKOR_SAFE_OLD.ModelHardware.FABPLUS ? "EKOR_PLUS" : "EKOR_SAFE_OLD";

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS_DISPLAY, string.Format("{0}_DISPLAY_PAR", deviceName), "DISPLAY_PAR", deviceName);
            Ekor.WriteDisplayTestSegmentsOdd();

            Delay(750, "Espera condicionamiento del display impar");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS_DISPLAY, string.Format("{0}_DISPLAY_IMPAR", deviceName), "DISPLAY_IMPAR", deviceName);
        }

        public virtual void TestLeds()
        {
            CAMERA_IDS_LEDS = GetVariable<string>("CAMARA_IDS_LEDS", CAMERA_IDS_LEDS);

            var ouputsValue = new List<EKOR_SAFE_OLD.Leds>();
            ouputsValue.Add(EKOR_SAFE_OLD.Leds.LED_FASE);
            ouputsValue.Add(EKOR_SAFE_OLD.Leds.LED_NEUTRO);
            ouputsValue.Add(EKOR_SAFE_OLD.Leds.LED_EXT);

            foreach (EKOR_SAFE_OLD.Leds led in ouputsValue)
            {
                Ekor.ActivaLED((int)led);
            }

            Delay(1000, "Espera bit");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LEDS, "EKOR_LEDS_ON", "LEDS_ON", "EKOR_SAFE_OLD");

            foreach (EKOR_SAFE_OLD.Leds led in ouputsValue)
            {
                Ekor.DesactivaLED((int)led);
            }

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LEDS, "EKOR_LEDS_OFF", "LEDS_OFF", "EKOR_SAFE_OLD");
        }

        public virtual void TestKeyborad()
        {
            var keyboardValueFromUtil = util.GetKeyValuePairKeyBoard();
            var inputsValue = new List<KeyValuePair<byte, EKOR_SAFE_OLD.enumKeyboard>>();

            foreach(var keys in keyboardValueFromUtil)
                inputsValue.Add(new KeyValuePair<byte, EKOR_SAFE_OLD.enumKeyboard>(keys.Key, (EKOR_SAFE_OLD.enumKeyboard)keys.Value));

            util.ActivatePistonTestKeyboard();
            foreach (KeyValuePair<byte, EKOR_SAFE_OLD.enumKeyboard> input in inputsValue)
            {
                logger.InfoFormat("Test key {0}", input.Value.ToString());

                tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    return (bool)Ekor.ReadKeyState((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 6, 500, 250, false, false);

                tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    return !Ekor.ReadKeyState((int)input.Value);
                }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 3, 500, 250, false, false);

                Resultado.Set(string.Format("KEY_{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
            util.DesactivatePistonTestKeyboard();
        }

        public virtual void MeasureCurrentApplyVoltageChannel()
        {
            measureValue = new StructCurrentsInVoltageChannel();

            measureValue.resistenceVL = Configuracion.GetDouble(Params.RES.Other("VL").TESTER.Name, 10000, ParamUnidad.Ohms);
            measureValue.resistenceVF = Configuracion.GetDouble(Params.RES.Other("VF").TESTER.Name, 10000, ParamUnidad.Ohms);

            tower.MUX.Reset();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.I.L1.TESTER.modo("VL").Name, Params.I.Null.TESTER.modo("VL").Min(), Params.I.Null.TESTER.modo("VL").Max(), 0, 0, ParamUnidad.uA));
            defs.Add(new TriAdjustValueDef(Params.I.L1.TESTER.modo("VF").Name, Params.I.Null.TESTER.modo("VF").Min(), Params.I.Null.TESTER.modo("VF").Max(), 0, 0, ParamUnidad.uA));

            var measureVL = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltAC,
                NumMuestras = 3,
                Rango = 20,
                DigitosPrecision = 4,
                NPLC = 8
            };

            var measureVF = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltAC,
                NumMuestras = 3,
                Rango = 20,
                DigitosPrecision = 4,
                NPLC = 8
            };

            TestMeasureBase(defs,
                (step) =>
                {
                    CheckCancellationRequested();

                    Delay(2000, "");

                    var VL1 = tower.MeasureMultimeter(InputMuxEnum.IN2, measureVL, 5000);
                    var VL2 = tower.MeasureMultimeter(InputMuxEnum.IN3, measureVL, 5000);


                    var VL3 = tower.MeasureMultimeter(InputMuxEnum.IN4, measureVL, 5000);

                    measureValue.currentVL = TriLineValue.Create(VL1, VL2, VL3);

                    CheckCancellationRequested();

                    Delay(2000, "");

                    var VF1 = tower.MeasureMultimeter(InputMuxEnum.IN5, measureVF, 5000);

                    var VF2 = tower.MeasureMultimeter(InputMuxEnum.IN6, measureVF, 5000);

                    var VF3 = tower.MeasureMultimeter(InputMuxEnum.IN7, measureVF, 5000);
                    tower.MUX.Reset();

                    measureValue.currentVF = TriLineValue.Create(VF1, VF2, VF3);

                    return measureValue.AllCurrents.ToArray();

                }, 0, 2, 500);
        }

        public virtual void TestCalibrationMultiCurrents([Description("Fina=0, Gruesa=1, Cortocircuito=2")] EKOR_SAFE_OLD.Scale scale, double consigna_mA = 0)
        {
            EKOR_SAFE_OLD.ChannelCurrent channel = EKOR_SAFE_OLD.ChannelCurrent.IL123;
            verificationArgumentsCurrent(scale, channel, false);

            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, consigna_mA, ParamUnidad.mA);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Other(channel.ToString()).Ajuste.Name, 45, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current / 1000, 0, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new TriAdjustValueDef(Params.I.L1.Ajuste.TestPoint(scaleDesc).Name, Params.I.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.I.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false);

            SamplerWithCancel((s) =>
            {
                if (s > 0)
                {
                    Ekor.Reset();
                    util.ResetUSB();
                }

                TestMeasureBase(defs,
                (step) =>
                {
                    var calibrationValues = new List<double>();

                    SamplerWithCancel((p) =>
                        {
                            Ekor.ReadCalibrationScale<EKOR_SAFE_OLD.StructCalibrationMeasure>(scale);
                            Ekor.CalibrationCurrents(channel, scale, (int)current);

                            Delay(5000, "Espera calibración");

                            var calibrationFactors = Ekor.ReadCalibrationScale<EKOR_SAFE_OLD.StructCalibrationMeasure>(scale);
                            calibrationValues.Add((double)calibrationFactors.factor_IL1, (double)calibrationFactors.factor_IL2, (double)calibrationFactors.factor_IL3);

                            return true;

                        }, "Error al intentar comunicar con el EKOR al Calibar corrientes", 5, 1000, 2500, false, false);

                    return calibrationValues.ToArray();

                }, 0, 2, 1500);

                TestVerificationMultiCurrents(scale);

                return true;

            }, "", 2, 500, 1500, false, false);
        }

        public virtual void TestCalibrationSingleCurrent([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_SAFE_OLD.Scale scale, [Description("IL1= 1, IL2=2, IL3=4, IH=8")] EKOR_SAFE_OLD.ChannelCurrent channel = EKOR_SAFE_OLD.ChannelCurrent.IH, double consigna_mA = 0)
        {
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();
            verificationArgumentsCurrent(scale, channel, true);
            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, consigna_mA, ParamUnidad.mA);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Other(channel.ToString()).Ajuste.Name, 45, ParamUnidad.Grados);
            var desfases = ModeloFabricacionEKOR == EKOR_SAFE_OLD.ModelHardware.FABSAFE ? TriLineValue.Create(0, 0, 0) : TriLineValue.Create(0, 120, 240);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current / 1000, 0, gap, desfases);

            var defs = new AdjustValueDef(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, 0, Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Min(), Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos);
            SamplerWithCancel((s) =>
            {
            if (s > 0)
            {
                Ekor.Reset();
                util.ResetUSB();
            }
            TestMeasureBase(defs,
                (step) =>
                {
                    Ekor.ReadCalibrationScale<EKOR_SAFE_OLD.StructCalibrationMeasure>(scale);
                   Ekor.CalibrationCurrents(channel, scale, (int)current);

                    Delay(5000, "Espera calibración");
                    var calibrationValues = Ekor.ReadCalibrationScale<EKOR_SAFE_OLD.StructCalibrationMeasure>(scale);
                    double value = 0;
                    switch (channel)
                    {
                        case EKOR_SAFE_OLD.ChannelCurrent.IL1:
                            value = calibrationValues.factor_IL1;
                            break;
                        case EKOR_SAFE_OLD.ChannelCurrent.IL2:
                            value = calibrationValues.factor_IL2;
                            break;
                        case EKOR_SAFE_OLD.ChannelCurrent.IL3:
                            value = calibrationValues.factor_IL3;
                            break;
                        case EKOR_SAFE_OLD.ChannelCurrent.IH:
                            value = calibrationValues.factor_IH;
                            break;
                    }
                    return value;

                }, 0, 2, 2500);

                TestVerificationSingleCurrent(scale);
                return true;

            }, "", 2, 500, 1500, false, false);
        }

        public virtual void TestCalibrationMultiVoltage([Description("VL= 112, VF=896")] EKOR_SAFE_OLD.ChannelVoltage channel)
        {
            EKOR_SAFE_OLD.Scale scale = EKOR_SAFE_OLD.Scale.Fina;

            tower.IO.DO[util.GetOutActivateVL()] = channel == EKOR_SAFE_OLD.ChannelVoltage.VL123;
            tower.IO.DO[util.GetOutActivateVF()] = channel == EKOR_SAFE_OLD.ChannelVoltage.VF123;

            verificationArgumentsVoltage(scale, channel, false);

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, 0, 0, 0, TriLineValue.Create(0, 120, 240));

            var defsList = new List<TriAdjustValueDef>();

            if (channel == EKOR_SAFE_OLD.ChannelVoltage.VL123)
                defsList.Add(new TriAdjustValueDef(Params.V.L1.Ajuste.Name, Params.V.Null.Ajuste.Min(), Params.V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos, false, false));

            if (channel == EKOR_SAFE_OLD.ChannelVoltage.VF123)
                defsList.Add(new TriAdjustValueDef(Params.VF.L1.Ajuste.Name, Params.VF.Null.Ajuste.Min(), Params.VF.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos, false, false));


            SamplerWithCancel((Func<int, bool>)((s) =>
            {
            if (s > 0)
            {
                Ekor.Reset();
                util.ResetUSB();
            }

            TestMeasureBase(defsList,
                (step) =>
                 {
                     Ekor.ReadCalibrationScale<EKOR_SAFE_OLD.StructCalibrationMeasure>(scale);

                     Ekor.CalibrationVoltage(channel, channel == EKOR_SAFE_OLD.ChannelVoltage.VL123 ? measureValue.CurrentsVL.ToList() : measureValue.CurrentsVF.ToList());

                    Delay(5000, "Espera calibración");

                     var calibrationFactors = Ekor.ReadCalibrationScale<EKOR_SAFE_OLD.StructCalibrationMeasure>(scale);

                     var calibrationValues = new List<double>();

                     if (channel == EKOR_SAFE_OLD.ChannelVoltage.VL123)
                         calibrationValues.Add((double)calibrationFactors.factor_VL1, (double)calibrationFactors.factor_VL2, (double)calibrationFactors.factor_VL3);

                     if (channel == EKOR_SAFE_OLD.ChannelVoltage.VF123)
                         calibrationValues.Add((double)calibrationFactors.factor_VF1, (double)calibrationFactors.factor_VF2, (double)calibrationFactors.factor_VF3);

                     return calibrationValues.ToArray();

                 }, 0, 2, 2500);

                TestVerificationMultiVoltage(channel);

                return true;

            }), "", 2, 500, 1500, false, false);

        }

        public void TestAutoAlimentacion()
        {
            tower.ActiveCurrentCircuit(true, true, true);

            tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(2000, "Wait reset hardware");

            double current = Consignas.GetDouble(Params.I.Null.TestPoint("AUTOSUPPLY").Name, 0.8, ParamUnidad.A);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, current, 0, 0, 0);

            tower.ActiveCurrentCircuit(false, true, true);

            internalTestAutoConsumo("L1");

            tower.ActiveCurrentCircuit(true, false, true);

            Delay(500, "espera cambio L2");

            internalTestAutoConsumo("L2");

            tower.ActiveCurrentCircuit(true, true, false);

            Delay(500, "espera cambio L3");

            internalTestAutoConsumo("L3");

            tower.ActiveCurrentCircuit(true, true, true);

            Delay(500, "espera cambio L3");

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            tower.ActiveCurrentCircuit(false, false, false);

        }
       
        public void TestVerificationMultiCurrents([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_SAFE_OLD.Scale scale, double consigna_mA = 0)
        {           
            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            EKOR_SAFE_OLD.ChannelCurrent channel = EKOR_SAFE_OLD.ChannelCurrent.IL123;

            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, consigna_mA, ParamUnidad.mA);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Other(channel.ToString()).Ajuste.Name, 45, ParamUnidad.Grados);

            if (!hasAdjust)
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current / 1000, 0, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defsVerif = new List<TriAdjustValueDef>();
            defsVerif.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, current, Params.I.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.mA, false, false));
            defsVerif.Add(new TriAdjustValueDef(Params.ANGLE_GAP.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, gap, Params.ANGLE_GAP.Null.Verificacion.Tol(), ParamUnidad.Grados, false, false));

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            Delay(5000, "");

            TestCalibracionBase(defsVerif,
            () =>
            {               
                var varsL1 = Ekor.ReadMeasure(EKOR_SAFE_OLD.Lines.L1);
                logger.DebugFormat("VIP L1 I: {0} , A1: {1}", varsL1.Current, varsL1.Angle);
                var varsL2 = Ekor.ReadMeasure(EKOR_SAFE_OLD.Lines.L2);
                logger.DebugFormat("VIP L2 I: {0} , A2: {1}", varsL2.Current, varsL2.Angle);
                var varsL3 = Ekor.ReadMeasure(EKOR_SAFE_OLD.Lines.L3);
                logger.DebugFormat("VIP L3 I: {0} , A3: {1}", varsL3.Current, varsL3.Angle);

                Ekor.ReadVoltageInstant();
                var result = new List<double>() { varsL1.Current, varsL2.Current, varsL3.Current, varsL1.Angle, varsL2.Angle, varsL3.Angle };
                return result.ToArray();
            },
            () =>
            {
                var result = new List<double>() { current, current, current, gap, gap, gap };
                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);
        }

        public void TestVerificationSingleCurrent([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_SAFE_OLD.Scale scale, [Description("IL1= 1, IL2=2, IL3=4, IH=8")] EKOR_SAFE_OLD.ChannelCurrent channel = EKOR_SAFE_OLD.ChannelCurrent.IH, double consigna_mA = 0)
        {
            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            string scaleDesc = "ESC_" + scale.ToString().ToUpper();
            EKOR_SAFE_OLD.Lines line = EKOR_SAFE_OLD.Lines.LH;

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Other(channel.ToString()).Ajuste.TestPoint(scaleDesc).Name, consigna_mA, ParamUnidad.mA);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Other(channel.ToString()).Ajuste.Name, 45, ParamUnidad.Grados);
            var desfases = ModeloFabricacionEKOR == EKOR_SAFE_OLD.ModelHardware.FABSAFE ? TriLineValue.Create(0, 0, 0) : TriLineValue.Create(0, 120, 240);

            if (!hasAdjust)                             
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current / 1000, 0, gap, desfases);

            var defsVerif = new List<AdjustValueDef>();
            defsVerif.Add(new AdjustValueDef(Params.I.Other(channel.ToString()).Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, current, Params.I.Other(channel.ToString()).Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.mA));
            defsVerif.Add(new AdjustValueDef(Params.ANGLE_GAP.Other(channel.ToString()).Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, gap, Params.ANGLE_GAP.Other("LH").Verificacion.Tol(), ParamUnidad.Grados));

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;                

            Delay(5000, "");

            TestCalibracionBase(defsVerif,
            () =>
            {
                var varsL1 = Ekor.ReadMeasure(line);
                logger.DebugFormat("VPI CHANNEL {0} I: {1}", channel, varsL1.Current);

                Ekor.ReadVoltageInstant();

                var result = new List<double>() { (double)varsL1.Current , (double)varsL1.Angle } ;
                return result.ToArray();
            },
            () =>
            {
                var result = new List<double>() { current, gap };
                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);

        }
        
        public virtual void TestVerificationMultiVoltage([Description("VL= 112, VF=896")] EKOR_SAFE_OLD.ChannelVoltage channel)
        {
            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            if (!hasAdjust)
            {
                EKOR_SAFE_OLD.Scale scale = EKOR_SAFE_OLD.Scale.Fina;

                tower.IO.DO[util.GetOutActivateVL()] = channel == EKOR_SAFE_OLD.ChannelVoltage.VL123;
                tower.IO.DO[util.GetOutActivateVF()] = channel == EKOR_SAFE_OLD.ChannelVoltage.VF123;

                verificationArgumentsVoltage(scale, channel, false);

                double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 240, ParamUnidad.V);

                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, 0, 0, 0, TriLineValue.Create(0, 120, 240));
            }

            var patronVoltage = new TriLineValue();

            var defsVerif = new List<AdjustValueDef>();

            if (channel == EKOR_SAFE_OLD.ChannelVoltage.VL123)
            {
                patronVoltage = measureValue.CurrentsVL;
                defsVerif.Add(new AdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, patronVoltage.L1, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
                defsVerif.Add(new AdjustValueDef(Params.V.L2.Verificacion.Name, 0, 0, 0, patronVoltage.L2, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
                defsVerif.Add(new AdjustValueDef(Params.V.L3.Verificacion.Name, 0, 0, 0, patronVoltage.L3, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            }

            if (channel == EKOR_SAFE_OLD.ChannelVoltage.VF123)
            {
                patronVoltage = measureValue.CurrentsVF;
                defsVerif.Add(new AdjustValueDef(Params.V.L1.Verificacion.TestPoint("FUSE").Name, 0, 0, 0, patronVoltage.L1, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
                defsVerif.Add(new AdjustValueDef(Params.V.L2.Verificacion.TestPoint("FUSE").Name, 0, 0, 0, patronVoltage.L2, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
                defsVerif.Add(new AdjustValueDef(Params.V.L3.Verificacion.TestPoint("FUSE").Name, 0, 0, 0, patronVoltage.L3, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            }

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            Delay(5000, "");

            TestCalibracionBase(defsVerif,
            () =>
            {
                if (channel == EKOR_SAFE_OLD.ChannelVoltage.VL123)
                {
                    var vars = Ekor.ReadVoltageMeasureVerification<EKOR_SAFE_OLD.VoltageMeasureVerification>();
                    logger.DebugFormat("LEV L1 V: {0}", (object)vars.VoltageL1);
                    logger.DebugFormat("LEV L2 V: {0}", (object)vars.VoltageL2);
                    logger.DebugFormat("LEV L3 V: {0}", (object)vars.VoltageL3);

                    Ekor.ReadVoltageInstant();

                    var result = new System.Collections.Generic.List<double>() { vars.VoltageL1, vars.VoltageL2, vars.VoltageL3 };
                    return result.ToArray();
                }
                else
                {
                    var vars = Ekor.ReadVoltageMeasureVerification<EKOR_SAFE_OLD.VoltageMeasureVerification>();
                    logger.DebugFormat("LEV L1 V: {0}", (object)vars.VoltageL1Aux);
                    logger.DebugFormat("LEV L2 V: {0}", (object)vars.VoltageL2Aux);
                    logger.DebugFormat("LEV L3 V: {0}", (object)vars.VoltageL3Aux);
                    var result = new System.Collections.Generic.List<double>() { vars.VoltageL1Aux, vars.VoltageL2Aux, vars.VoltageL3Aux };
                    return result.ToArray();
                }
            },
            () =>
            {
                var result = new List<double>() { patronVoltage.L1, patronVoltage.L2, patronVoltage.L3 };
                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);
        }

        public void TestVAUX(string step = "")
        {
            logger.InfoFormat("Test signal control {0}", EKOR_SAFE_OLD.enumPowerStates.VAUX_OK.ToString());         

            SamplerWithCancel((p) =>
            {
                var result = Ekor.ReadPowerBitState((int)EKOR_SAFE_OLD.enumPowerStates.VAUX_OK);

                Resultado.Set(string.Format("VAUX_OK{0}",step), (string)result.ToString(), ParamUnidad.SinUnidad);

                return (bool)result;
            }, string.Format("Error. no se detecta la entrada {0} activada", EKOR_SAFE_OLD.enumPowerStates.VAUX_OK.ToString()), 2, 500, 100, false, false);
        }
        public void TestVGEN(string step = "")
        {

            logger.InfoFormat("Test signal control {0}", EKOR_SAFE_OLD.enumPowerStates.VGEN_OK.ToString());

            var inputsValue = new List<EKOR_SAFE_OLD.enumPowerStates>();
            inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VCC_OK);
            inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VTRP_OK);
            inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VGEN_OK);

            foreach (EKOR_SAFE_OLD.enumPowerStates input in inputsValue)
            {
                SamplerWithCancel((p) =>
                {
                    var result = Ekor.ReadPowerBitState((int)input);
                    Resultado.Set(string.Format(input.ToString()+"{0}",step), (string)result.ToString(), ParamUnidad.SinUnidad);
                    return (bool)result;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.ToString()), 3, 500, 100, false, false);
            }
        }
        public virtual void TestVUSB(string step = "")
        {
            SamplerWithCancel((p) =>
            {
                var result = Ekor.ReadPowerBitState((int)EKOR_SAFE_OLD.enumPowerStates.VUSB_OK);

                Resultado.Set(string.Format("VUSB_OK{0}", step), (string)result.ToString(), ParamUnidad.SinUnidad);

                if (!result)
                    throw new Exception(string.Format("Error. no se detecta la entrada {0} activada", EKOR_SAFE_OLD.enumPowerStates.VUSB_OK.ToString()));

                return (bool)result;

            }, string.Empty, 5, 500, 100, false, false);
        }
        public void TestReadCalibrationClock()
        {
            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            TestMeasureBase(defs,
            (step) =>
            {
                var calibrationClk = Ekor.ReadCalibrationClock();

                return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
            }, 0, 2, 1000);
        }
        public virtual void TestDesviationClock()
        {
            SamplerWithCancel((p) =>
            {
                var timeEkor = Ekor.ReadDateTime();

                logger.InfoFormat("EKOR_WTP FECHA:{0} HORA:{1}", (object)timeEkor.ToShortDateString(), (object)timeEkor.ToLongTimeString());
                logger.InfoFormat("PC FECHA:{0} HORA:{1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());

                double timeDiff = (DateTime.Now.Subtract((DateTime)timeEkor)).TotalSeconds;

                logger.InfoFormat("DERIVA RELOJ:{0}", timeDiff);

                Assert.AreBetween(Params.TIME.Null.Offset.Name, timeDiff, Params.TIME.Null.Offset.Min(), Params.TIME.Null.Offset.Max(), TestException.Create().UUT.MEDIDA.MARGENES("deriva de reloj"), ParamUnidad.ms);

                Ekor.WriteDateTime();

                return true;

            }, string.Empty, 2);

        }
        public void TestBattery()
        {
            var param = new AdjustValueDef(Params.V.Null.Bateria, ParamUnidad.V);
            var vRef = Consignas.GetDouble(Params.V.Null.TestPoint("REF_BATTERY").Name, 3, ParamUnidad.V);
            TestMeasureBase(param,
            (step) =>
            {
                var batteryLevel = Ekor.ReadBattery(vRef);
                return batteryLevel;
            }, 0, 3, 2000);
        }
        public void TestEEPROM()
        {
            var rdm = new Random();
            var number = rdm.Next(10, 100);

            if (number == Ekor.ReadEEPROM())
                number += 1;

            Ekor.WriteEEPROM(number);

            Delay(2000, "Escribiendo en EEPROM");

            var reading = Ekor.ReadEEPROM();

            Assert.AreEqual(reading, number, Error().UUT.EEPROM.NO_CORRESPONDE_CON_ENVIO("Error EEPROM, valor leído diferente al escrito anteriormente"));

            Resultado.Set("EEPROM", "OK", ParamUnidad.SinUnidad);

        }

        public virtual void TestCalibrationClock()
        {
            tower.IO.DO.On(22);

            tower.HP53131A
                .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
                .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 0.6)
                .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 6);

            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            tower.MUX.Reset();
            tower.MUX.Connect(InputMuxEnum.IN13, Instruments.IO.Outputs.FreCH1);

            try
            {
                TestMeasureBase(defs,
                (step) =>
                {
                    double temperature = Ekor.ReadTemperatureI2C();
                    Resultado.Set(Params.TEMP.Null.Ajuste.TestPoint("RELOJ").Name, temperature, ParamUnidad.Grados);

                    Ekor.StartSignalClok(015);

                    tower.LAMBDA.ApplyOff();
                    var medidaFreqMed = tower.HP53131A.RunMeasure(HP53131A.Variables.FREQ, 8000, HP53131A.Channels.CH1);
                    Resultado.Set(Params.FREQ.Null.Ajuste.TestPoint("RELOJ").Name, medidaFreqMed.Value, ParamUnidad.Hz);

                    tower.LAMBDA.ApplyAndWaitStabilisation();
                    SamplerWithCancel((p) =>
                    {
                        Ekor.ReadVersion();
                        return true;
                    }, "Error de comunicaciones despues de esperar 10seg. al enviar CLK_DE2ZACFAB", 2, 3000, 8000, true, true);

                    var calibrationClk = Ekor.CalibrationClock(medidaFreqMed.Value, temperature);

                    return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
                }, 0, 2, 1000);
            }
            finally
            {
                tower.MUX.Reset();


                var defsTicks = new List<AdjustValueDef>();

                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("MAX_TICK", ParamUnidad.Hz).Null.Ajuste.Name, Min = Params.PARAMETER("MAX_TICK", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("MAX_TICK", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.Hz });
                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("AVG_TICK", ParamUnidad.Hz).Null.Ajuste.Name, Min = Params.PARAMETER("AVG_TICK", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("AVG_TICK", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.Hz });
                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("MIN_TICK", ParamUnidad.Hz).Null.Ajuste.Name, Min = Params.PARAMETER("MIN_TICK", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("MIN_TICK", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.Hz });
                defsTicks.Add(new AdjustValueDef() { Name = Params.PARAMETER("TIME_TICK", ParamUnidad.s).Null.Ajuste.Name, Min = Params.PARAMETER("TIME_TICK", ParamUnidad.Hz).Null.Ajuste.Min(), Max = Params.PARAMETER("TIME_TICK", ParamUnidad.Hz).Null.Ajuste.Max(), Unidad = ParamUnidad.s });

                TestMeasureBase(defsTicks,
                (step) =>
                {
                    var ticks = Ekor.ReadCountTicksLastTest();
                    return new double[] { ticks.MaxTick, ticks.AvgTick, ticks.MinTick, ticks.TimeTick };
                }, 0, 1, 1000);
            }

            tower.IO.DO.Off(22);
            Logger.InfoFormat(string.Format("Funcion realizada por el puerto COM{0}", Ekor.Cirbus.PortCom));
        }

        public void ConfigComunicationsRS485()
        {
            tower.IO.DO.On(OUT_ACTIVE_RS485_EMBEDDED);
            Ekor.Cirbus.PortCom = 7;
        }

        public void ClosePortEkorCirbus()
        {
            Ekor.Cirbus.ClosePort();
        }

        public void TestComunicationRS485()
        {           
            Ekor.SetPort(Comunicaciones.SerialPort);

            SamplerWithCancel((p) =>
            {
                var versionEkor = Ekor.ReadVersion();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, versionEkor.version, Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version del firmware del eKOR no es correcta"), ParamUnidad.SinUnidad);

                return true;
            }, "Error de comunicaciones por el puerto RS485 frontal al preguntar Version firmware el eKOR", 2, 100, 1000, true, true);

            Resultado.Set("RS485", "OK", ParamUnidad.SinUnidad);
        }

        public void TestCustomize()
        {
            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {           
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    Ekor.RecoveryDefaultSettings(Ekor.Cirbus.PortCom, 9600, 1);
                    Delay(2000, "");
                }

                Ekor.WriteSerialNumber(TestInfo.NumSerie);

                Ekor.WriteBastidor(TestInfo.NumBastidor.Value.ToString());              
                
                Ekor.Reset();
                Delay(2000, "Reseteando el equipo");

                return true;

            }, "Error de comunicaciones USB al grabar numero de serie y bastidor", 3, 1000, 1000, false, false);

            SamplerWithCancel((p) =>
            {
                if (Ekor.Cirbus.PortCom != Comunicaciones.SerialPort)
                    util.ResetUSB();

                var bastidor = Ekor.ReadBastidor();
                var bastidorInfo = TestInfo.NumBastidor.Value.ToString().PadLeft(8, '0');

                var numSerie = Ekor.ReadSerialNumber();

                Assert.IsTrue(bastidor.Trim() == bastidorInfo, Error().UUT.NUMERO_DE_BASTIDOR.NO_CORRESPONDE_CON_ENVIO("Error, el numero de bastidor leido no corresponde con el grabado en el equipo"));
                Assert.IsTrue(numSerie.Trim() == TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_CORRESPONDE_CON_ENVIO("Error, el numero de serie leido no corresponde con el grabado en el equipo"));

                return true;
            }, string.Empty, 3, 1000, 1000, false, false);

            if (ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.RPA200_C)
                Ekor.WriteConfigurationDefaultFab(ModeloEKOR, Identificacion.SUBMODELO.ToUpper().TrimStart().TrimEnd());
            else
                Ekor.WriteConfigurationDefaultFab(ModeloEKOR);
            
            SamplerWithCancel((p) =>
            {
                var modelRead = Ekor.ReadModelModbus(Ekor.Cirbus.PortCom);
                Assert.AreEqual(ConstantsParameters.Identification.MODELO_CLIENTE, modelRead.ToString(), Identificacion.MODELO_CLIENTE, Error().UUT.HARDWARE.NO_COINCIDE("Modelo Cliente leido diferente al especificado"), ParamUnidad.SinUnidad);
                var deviceRead = Ekor.ReadDeviceModbus(Ekor.Cirbus.PortCom);
                Assert.AreEqual(ConstantsParameters.Identification.MODELO, deviceRead.ToString(), Identificacion.MODELO, Error().UUT.HARDWARE.NO_COINCIDE("Modelo Hardware Cliente leido diferente al grabado"), ParamUnidad.SinUnidad);
                return true;
            }, string.Empty, 5, 1000, 4000, false, false);         

        }

        public void TestSerialPortPPP()
        {
            Ekor.ReadPPP();
            Resultado.Set("SERIAL_PORT_PPP", "OK", ParamUnidad.SinUnidad);
        }

        protected void verificationArgumentsCurrent(EKOR_SAFE_OLD.Scale scale, EKOR_SAFE_OLD.ChannelCurrent channel, bool singleCurrents = true)
        {
            switch (scale)
            {
                case EKOR_SAFE_OLD.Scale.Fina:
                case EKOR_SAFE_OLD.Scale.Gruesa:
                case EKOR_SAFE_OLD.Scale.Cortoc:
                    break;
                default:
                    throw new Exception("Valor del arguemnto de la escala incorrecto");
            }
            switch (channel)
            {
                case EKOR_SAFE_OLD.ChannelCurrent.IL1:
                case EKOR_SAFE_OLD.ChannelCurrent.IL2:
                case EKOR_SAFE_OLD.ChannelCurrent.IL3:
                case EKOR_SAFE_OLD.ChannelCurrent.IH:
                    if (!singleCurrents)
                        throw new Exception("Valor del argumento del canal de corriente incorrecto");
                    break;
                case EKOR_SAFE_OLD.ChannelCurrent.IL123:
                    if (singleCurrents)
                        throw new Exception("Valor del argumento del canal de corriente incorrecto");
                    break;
                default:
                    throw new Exception("Valor del argumento del canal de corriente incorrecto");
            }
        }

        protected void verificationArgumentsVoltage(EKOR_SAFE_OLD.Scale scale, EKOR_SAFE_OLD.ChannelVoltage channel, bool singleVoltage = true)
        {
            switch (scale)
            {
                case EKOR_SAFE_OLD.Scale.Fina:
                    break;
                default:
                    throw new Exception("Valor del arguemnto de la escala incorrecto");
            }

            switch (channel)
            {
                case EKOR_SAFE_OLD.ChannelVoltage.VL1:
                case EKOR_SAFE_OLD.ChannelVoltage.VL2:
                case EKOR_SAFE_OLD.ChannelVoltage.VL3:
                case EKOR_SAFE_OLD.ChannelVoltage.VF1:
                case EKOR_SAFE_OLD.ChannelVoltage.VF2:
                case EKOR_SAFE_OLD.ChannelVoltage.VF3:
                    if (!singleVoltage)
                        throw new Exception("Valor del argumento del canal de voltage incorrecto");
                    break;
                case EKOR_SAFE_OLD.ChannelVoltage.VF123:
                case EKOR_SAFE_OLD.ChannelVoltage.VL123:
                    if (singleVoltage)
                        throw new Exception("Valor del argumento del canal de voltage incorrecto");
                    break;
                default:
                    throw new Exception("Valor del argumento del canal de voltage  incorrecto");
            }
        }

        private void internalTestAutoConsumo(string line)
        {
            if (ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.MVE_CPU)
            {
                try
                {
                    Ekor.ReadPowerBitState((int)EKOR_SAFE_OLD.enumPowerStates.VCC_OK);
                    throw new Exception("Error modelo MVE este equipo no debe autoalimentarse");
                }
                catch (Exception)
                {
                    return;
                }
            }
            else
            {
                var inputsValue = new Dictionary<EKOR_SAFE_OLD.enumPowerStates, bool>();
                inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VCC_OK, ModeloEKOR == EKOR_SAFE_OLD.ModelHardware.MVE ? false : true);
                inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VAUX_OK, false);
                inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VUSB_OK, TestPlacas ? false : true);
                inputsValue.Add(EKOR_SAFE_OLD.enumPowerStates.VGEN_OK, false);            

                Delay(1000, "Espera temps canvi linea");

                foreach (KeyValuePair<EKOR_SAFE_OLD.enumPowerStates, bool> input in inputsValue)
                {
                    Logger.InfoFormat("Test signal control {0}", input.ToString());

                    SamplerWithCancel((p) =>
                    {

                        var result = Ekor.ReadPowerBitState((int)input.Key);
                        return result == input.Value;

                    }, string.Format("Error. no se detecta la entrada {0} en el estado esperado {1}", input.Key.ToString(), input.Value.ToString()), 2, 500, 100, false, false);
                }

               // ResetUSB();
            }
            Resultado.Set(string.Format("AUTOSUPPLY_{0}", line), "OK", ParamUnidad.SinUnidad);
        }

        protected void interalTestFinish()
        {
            if (tower != null)
                tower.ShutdownSourcesQuick();

            Delay(2000, "");

            if (util != null)
            {
                util.DesactivatePistonRS232();
                util.TestDeviceDisconnection();
            }

            if (Ekor != null)
            {
                logger.Debug("Entramos a liberar el objeto EKOR");
                Ekor.Dispose();
                logger.Debug("Se ha liberado el objeto EKOR correctamente");
            }

            if (tower != null)
            {
                logger.Debug("Entramos a liberar el objeto TORRE correctamente");
                tower.IO.DO.OffRange(0, tower.IO.DO.Count - 1);
                logger.Debug("Se han desactivado las salidas de la TORRE correctamente");
                logger.Debug("Entramos a liberar el objeto TORRE");
                tower.Dispose();
                logger.Debug("Se ha liberado el objeto TORRE correctamente");
            }
        }

    }
}
