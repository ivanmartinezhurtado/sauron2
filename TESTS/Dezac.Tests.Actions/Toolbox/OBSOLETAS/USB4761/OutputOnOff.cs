﻿using Instruments.IO;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(OutputOnOffDescription))]
    
    public class OutputOnOff : ActionBase
    {
        [Description("valor de la salida o con $ variable de la salida que des/activamos,")]
        public string Output { get; set; }

        [Description("Tiempo de espera antes de relizar acción en milisegundos")]
        [DefaultValue(10)]
        public int WaitTimeBefore { get; set; }

        [Description("Tiempo de espera después de relizar acción en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeAfter { get; set; }

        [Description("Salida que activamos para despues detectar la entrada solicitada")]
        [DefaultValue(StatusOutput.ACTIVED)]
        public StatusOutput Action { get; set; }

        public enum StatusOutput
        {
            ACTIVED,
            DESACTIVED
        }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var IO = GetVariable<USB4761>("USB4761", () => { return null; });
            if (IO == null)
            {
                IO = new USB4761();
                SetVariable("USB4761", IO);
            }

            System.Threading.Thread.Sleep(WaitTimeBefore);

            if (string.IsNullOrEmpty(Output))
                if (testBase.Shell.MsgBox("Error, no se ha indicado ninguna Output para activar", "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.OK, MessageBoxIcon.Error) != DialogResult.Yes)
                    Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("ninguna Output para activar");

            var output = GetPropertyVar<byte>(Output);

            if (Action == StatusOutput.ACTIVED)
                IO.DO.On(output);
            else
                IO.DO.Off(output);

            System.Threading.Thread.Sleep(WaitTimeAfter);
        }
    }

    public class OutputOnOffDescription : ActionDescription
    {
        public override string Name { get { return "OutputOnOff"; } }
        public override string Description { get { return "Acción genérica para des/activar las salidas la targeta USB4761"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.output; } }
    }
}
