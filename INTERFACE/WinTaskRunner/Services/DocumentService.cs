﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using WinTaskRunner.Design;
using WinTaskRunner.Design.Services;

namespace WinTaskRunner.Services
{
    public class DocumentService : IDocumentService
    {
        public event EventHandler<DockWindow> ActiveWindowChanged;
        public event EventHandler<IDocument> ActiveDocumentChanged;

        private Dictionary<string, object> docManagers = new Dictionary<string, object>();
        private Dictionary<string, DocumentWindow> sequence = new Dictionary<string, DocumentWindow>();

        private bool NoDelete { get; set; }
        private bool CloseActive { get; set; }

        public DocumentService(AppService app)
        {
            foreach (Utils.AppConfig.AppConfigEditor editor in app.Config.Editors)
                docManagers.Add(editor.DocType, editor.Manager);
        }

        private RadDock manager;

        public RadDock Manager
        {
            get { return manager; }
            set
            {
                if (manager != null)
                {
                    manager.ActiveWindowChanged -= radDock_ActiveWindowChanged;
                    manager.DockWindowClosed -= radDock_DockWindowClosed;
                    manager.DockWindowClosing -= radDock_DockWindowClosing;
                }

                manager = value;
                manager.ActiveWindowChanged += radDock_ActiveWindowChanged;
                manager.DockWindowClosed += radDock_DockWindowClosed;
                manager.DockWindowClosing += radDock_DockWindowClosing;
            }
        }

        private void Manager_DockWindowClosing(object sender, DockWindowCancelEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void RegisterDocumentType(string type, IDocumentManager documentManager)
        {
            docManagers.Add(type, documentManager);
        }

        private IDocumentManager GetFromType(string type)
        {

            if (!docManagers.TryGetValue(type, out object manager))
                return null;

            if (manager is string)
            {
                var t = Type.GetType(manager.ToString());
                manager = Activator.CreateInstance(t);
                docManagers[type] = manager;
            }

            return manager as IDocumentManager;
        }

        public IDocumentManager CurrentDocumentManager
        {
            get
            {
                IDocument doc = CurrentDocument;
                if (doc == null)
                    return null;

                return GetFromType(doc.DocType);
            }
        }

        public IDocument CurrentDocument
        {
            get
            {
                if (Manager.DocumentManager.ActiveDocument != null)
                    return Manager.DocumentManager.ActiveDocument.Tag as IDocument;

                return null;
            }
        }

        public IDocument AddNew(string type)
        {
            return GetFromType(type).AddNew();
        }

        public IDocument Open(string type)
        {
            return GetFromType(type).Open();
        }

        public IDocument Open(string type, Stream stream, string nameDoc)
        {
            return GetFromType(type).Open(stream, nameDoc);
        }

        public void Save()
        {
            IDocument doc = CurrentDocument;
            if (doc == null)
                return;

            CurrentDocumentManager.Save(doc);
        }

        public void SaveAs()
        {
            IDocument doc = CurrentDocument;
            if (doc == null)
                return;

            CurrentDocumentManager.SaveAs(doc);
        }

        public void AddDocument(Control control, IDocument document, int width = 0, int height = 0)
        {
            string name = string.Format("{0}_{1}", DateTime.Now.ToString(), document.Name);
            var win = new DocumentWindow(document.Name);
            win.CloseAction = DockWindowCloseAction.Close;
            win.ToolCaptionButtons = ToolStripCaptionButtons.None;
            win.Name = name;
            sequence.Add(name, win);

            control.Dock = DockStyle.Fill;
            win.Controls.Add(control);
            win.Tag = document;
            Manager.AddDocument(win);

            if (width != 0 || height != 0)
            {
                win.TabStrip.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
                win.TabStrip.SizeInfo.AbsoluteSize = new Size(width, height);
            }

            Manager.ActivateWindow(win);
        }

        public void AddDocument(Control control, string title = null, int width = 0, int height = 0)
        {
            string name = string.Format("{0}_{1}", DateTime.Now.ToString(), string.IsNullOrEmpty(title) ? control.Text : title);
            var win = new DocumentWindow(string.IsNullOrEmpty(title) ? control.Text : title);
            win.CloseAction = DockWindowCloseAction.Close;
            win.ToolCaptionButtons = ToolStripCaptionButtons.None;
            win.Name = name;
            sequence.Add(name, win);

            control.Dock = DockStyle.Fill;
            win.Controls.Add(control);
            Manager.AddDocument(win);

            if (width != 0 || height != 0)
            {
                win.TabStrip.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
                win.TabStrip.SizeInfo.AbsoluteSize = new Size(width, height);
            }

            Manager.ActivateWindow(win);
        }

        public ToolWindow AddToolWindow(Control control, DockPosition position, string title = null, DockWindow target = null, int width = 0, int height = 0, int widthFloat = 0, int heightFloat = 0, DockWindowCloseAction closeAction = DockWindowCloseAction.Hide, DockStyle dockStyle = DockStyle.Fill)
        {
            var win = new ToolWindow(string.IsNullOrEmpty(title) ? control.Text : title);
            win.CloseAction = closeAction;
            win.ToolCaptionButtons = ToolStripCaptionButtons.None;
            win.DockState = DockState.Docked;
            control.Dock = dockStyle;
            win.Controls.Add(control);

            if (target == null)
                Manager.DockWindow(win, position);
            else
                Manager.DockWindow(win, target, position);

            win.TabStrip.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
            win.TabStrip.SizeInfo.AbsoluteSize = new Size(width, height);
            win.DefaultFloatingSize = new Size(widthFloat, heightFloat); ;
            return win;
        }

        void radDock_ActiveWindowChanged(object sender, DockWindowEventArgs e)
        {
            DockWindow win = e.DockWindow;
            if (win == null)
                return;

            if (ActiveWindowChanged != null)
                ActiveWindowChanged(sender, win);

            var doc = win.Tag as IDocument;

            if (ActiveDocumentChanged != null)
                ActiveDocumentChanged(sender, doc);
        }

        void radDock_DockWindowClosed(object sender, DockWindowEventArgs e)
        {
            if (!NoDelete)
            {
                var win = e.DockWindow as DocumentWindow;
                if (win != null)
                    sequence.Remove(win.Name);
            }
        }

        void radDock_DockWindowClosing(object sender, DockWindowCancelEventArgs e)
        {
            DockWindow win = e.NewWindow;
            if (win.CloseAction == DockWindowCloseAction.Hide && !CloseActive)
                e.Cancel = true;
        }

        public DockWindow SetControlParentSize(Control ctrl, Size size)
        {
            DockWindow tw = manager.DockWindows.ToolWindows.Where(p => p.Controls.Contains(ctrl)).FirstOrDefault();
            if (tw != null)
            {
                tw.Show();
                if (tw.TabStrip != null && tw.TabStrip.Parent != null)
                {
                    var pnl = tw.TabStrip.Parent as SplitPanel;
                    pnl.SizeInfo.AbsoluteSize = size;
                    pnl.Show();
                }
                return tw;
            }
            return null;
        }

        public void SetControlHide(Control ctrl, bool hide = true)
        {
            CloseActive = true;

            DockWindow tw = manager.DockWindows.ToolWindows.Where(p => p.Controls.Contains(ctrl)).FirstOrDefault();
            if (tw != null)
            {
                if (hide)
                    tw.Hide();
                else
                    tw.Show();
            }
            CloseActive = false;
        }

        public void SetAllControlHide(bool hide = true)
        {
            CloseActive = true;

            var tw = manager.DockWindows.ToolWindows.ToList();
            if (tw != null)
                foreach (DockWindow cn in tw)
                    if (hide)
                        cn.Hide();
                    else
                        cn.Show();

            CloseActive = false;
        }

        public void SetDocumnentsHide(bool hide = true)
        {
            if (hide)
            {
                NoDelete = true;
                Manager.RemoveAllDocumentWindows();
                NoDelete = false;
            }
            else
                foreach (KeyValuePair<string, DocumentWindow> seq in sequence)
                {
                    Manager.AddDocument(seq.Value);
                    Manager.ActivateWindow(seq.Value);
                }
        }
    }
}

