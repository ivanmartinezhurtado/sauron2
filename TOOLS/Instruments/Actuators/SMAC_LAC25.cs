﻿using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace Instruments.Actuators
{
    [InstrumentsVersion(1.31)]
    public class SMAC_LAC25 : ActuatorBase<SMAC_LAC25>, IDisposable
    {
        // cte del propio controlados, nos sirven para pasar de puntos a unidades sistema internacional
        private const double CTE_POS_LIN = 200;
        private const double CTE_VEL_LIN = 2621.44;
        private const double CTE_ACE_LIN = 524.288;
        private const double CTE_POS_ROT = 55.55;
        private const double CTE_VEL_ROT = 262144;
        private const double CTE_ACE_ROT = 52.4288;

        private SerialPort sp;
        private byte _port = 9;

        private const int TR_TIME = 400;

        private int RunningMacro = 0;

        // Tramas creadas a partir de clases para que sean dinamicas
        public class PID
        {
            public int ProportionalGain { get; set; }
            public int IntegralGain { get; set; }
            public int DerivateiveGain { get; set; }
            public int Offset { get; set; }
            public int DeadBand { get; set; }
            public int IntegralLimit { get; set; }
            public AxisActuator Actuator;

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}SG{1},SI{2},SD{3},OO{4},DB{5},IL{6}", (int)Actuator, ProportionalGain, IntegralGain, DerivateiveGain, Offset, DeadBand, IntegralLimit);

                return sb.ToString();
            }
        }

        public class DYNAMICS
        {
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }
            public AxisActuator Actuator;

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("{0}SV{1},SA{2},SQ{3},SE{4}", (int)Actuator, Velocity, Aceleration, Torque, TrackError);

                return sb.ToString();
            }
        }

        public class SOFTLAND_1
        {
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }
            public int Position { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("MD40,MG\"*M40*\",AL0,AR1,AR40,AR105,1PM,MN,SV{0},SA{1},SQ{2},SE{3},MA{4},GO,WS20", Velocity, Aceleration, Torque, TrackError, Position);

                return sb.ToString();
            }
        }

        public class SOFTLAND_2
        {
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("MD41,MG\"*M41*\",VM,MN,SV{0},SA{1},SQ{2},SE{3},DI0,GO,WA100,AL0,WL1830", Velocity, Aceleration, Torque, TrackError);

                return sb.ToString();
            }
        }

        public class SOFTLAND_5
        {
            public int PushTorque { get; set; }
            public int TrackError { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("MD44,MG\"*M44*\",1MN,QM0,SQ{0},SE{1},", PushTorque, TrackError);

                return sb.ToString();
            }
        }

        public class SOFTLAND_6
        {
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("MD45,MG\"*M45*\",2VM,SV{0},SA{1},SQ{2},SE{3},MN,AL0,WL1830,GO,DI1", Velocity, Aceleration, Torque, TrackError);

                return sb.ToString();
            }
        }

        public class SOFTLAND_8
        {
            public int Torque { get; set; }
            public int TorqueDelta { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat($"MD47, MG\"*M47*\",2SQ{Torque/TorqueDelta},WA500,SQ{Torque /(2* TorqueDelta)},WA500,SQ{Torque / (4 * TorqueDelta)},WA500,MF,WA500,DH,MG\"OK\",AL1,AR40,EP");

                return sb.ToString();
            }
        }
        public class GOFINALPOSITION_1
        {
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("MD110,MG\"*M110*\",2VM,SV{0},SA{1},SQ{2},SE{3},MN,AL0,WL1830,AR1,AR110,AR105,GO,DI0", Velocity, Aceleration, Torque, TrackError);

                return sb.ToString();
            }
        }

        public class POSITION_1
        {
            public byte NumPosition { get; set; }
            public int Position { get; set; }
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }
            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("MD{0},MG\"*M{0}*\",AL0,AR60,2PM,MN,SV{1},SA{2},SQ{3},SE{4},MA{5},DI0,GO,WS300,MJ60", NumPosition + 70, Velocity, Aceleration, Torque, TrackError, Position); 
                return sb.ToString();
            }
        }

        public class RUN_POSITION
        {
            public byte NumPosition { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("MS{0}", NumPosition + 70);

                return sb.ToString();
            }
        }

        public class TORQUESTEP_1
        {
            public int Velocity { get; set; }
            public int Aceleration { get; set; }
            public int Torque { get; set; }
            public int TrackError { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("MD90,MG\"*M90*\",AL0,AR90,2VM,MN,SV{0},SA{1},SQ{2},SE{3},DI0,GO", Velocity, Aceleration, Torque, TrackError);

                return sb.ToString();
            }         
        }

        public class TORQUESTEP_4
        {
            public int MaximumTime    { get; set; }
            public int MinimumDrop { get; set; }
            public int MinimumTorque { get; set; }

            public override string ToString()
            {
                CultureInfo enUS = new CultureInfo("en-US");
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("MD93,MG\"*M93*\",RL1830,IG{0},NO,MJ103,AL0,AR105,RL674,AR100,IG@102,AR102,NO,AS@102,IB-{1},NO,MJ94,RA100,AR101,RP", MaximumTime, MinimumDrop);

                return sb.ToString();
            }
        }

        public SMAC_LAC25(byte portName)
        {
            InitializeComponent();

            protocol = new LAC25Protocol() { CaracterFinTx = "\r", CaracterFinRx = "\r" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
            PortCom = portName;
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + _port;
            }
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            sp.WriteTimeout = sp.ReadTimeout = 500;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
            sp.DtrEnable = true;
            sp.RtsEnable = true;
            sp.Handshake = Handshake.XOnXOff;
        }

        /// <summary>
        /// Funcion para agrupar las funciones iniciales del controlador.</summary>
        public void InitLAC25()
        {
            RevomeMacros();

            RunInitialization();

            SetPIDConstants(SMAC_LAC25.AxisActuator.LINEAL, 39, 25, 2970, -285, 10, 8000);
            SetPIDConstants(SMAC_LAC25.AxisActuator.ROTATIVO, 11, 1, 222, 0, 10, 5000);

            WriteAndRunHomingLineal(true, false);
            WriteAndRunHomingRotative(true, false);

            MotorOff();
        }

        /// <summary>
        /// Funcion para enviar las macros basicas del controlador.</summary>
        public void RunInitialization()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ALL_MOTOR_OFF, true));
            Thread.Sleep(TR_TIME);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.INIT_DRIVER, true));
            Thread.Sleep(TR_TIME);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.INIT_VECTORES_FALLO, true));
            Thread.Sleep(TR_TIME);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.INIT_MACRO_ERROR_POS_LINEAL, true));
            Thread.Sleep(TR_TIME);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.INIT_MACRO_ERROR_POS_ROTATIVO, true));
            Thread.Sleep(TR_TIME);
        }

        /// <summary>
        /// Funcion para enviar las constantes PID del controlador.</summary>
        public void SetPIDConstants(AxisActuator actuador, int proportionalGain, int integralGain, int derivativeGain, int offset, int deadBand, int integralLimit)
        {
            var pid = new PID
            {
                ProportionalGain = proportionalGain,
                IntegralGain = integralGain,
                DerivateiveGain = derivativeGain,
                Offset = offset,
                DeadBand = deadBand,
                IntegralLimit = integralLimit,
                Actuator = actuador
            };

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_PID_CONSTANTS, true, pid.ToString()));
        }

        /// <summary>
        /// Funcion para hacer un Home del eje Lineal.</summary>
        /// <param name="write"> Las macros se envian al controlador y se guardan en su memoria interna</param>
        /// <param name="execute"> Las macros se ejecutan haciendo el homing del eje lineal </param>
        public void WriteAndRunHomingLineal(bool write = true, bool execute = true)
        {
            if (write)
            {
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_1, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_2, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_3, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_4, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_X1, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_5, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_LINEAL_6, true));
                Thread.Sleep(TR_TIME);
            }
            if (execute)
                RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_HOMING_LINEAL, true)); },
                    () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_HOMING_LINEAL, true)); }
                    , 15000, "Error en la ejecucion de la macro RunHomingLineal");
        }

        /// <summary>
        /// Funcion para hacer un Home del eje Rotativo.</summary>
        /// <param name="write"> Las macros se envian al controlador y se guardan en su memoria interna</param>
        /// <param name="execute"> Las macros se ejecutan haciendo el homing del eje rotativo </param>
        public void WriteAndRunHomingRotative(bool write = true, bool execute = true)
        {
            if (write)
            {
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_ROTATIVE_1, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_ROTATIVE_2, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_ROTATIVE_3, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_ROTATIVE_X1, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_ROTATIVE_4, true));
                Thread.Sleep(TR_TIME);
                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_HOMING_ROTATIVE_5, true));
                Thread.Sleep(TR_TIME);
            }
            if (execute)
                RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_HOMING_ROTATIVE, true)); },
                    () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_HOMING_ROTATIVE, true)); }
                    , 15000, "Error en la ejecucion de la macro RunHomingRotative");
        }

        /// <summary>
        /// Funcion para softland, entrada en el selector y poner el selector en la posicion 0.</summary>
        /// <param name="gapPosition"> Posicion a la que se avanza rapidamente</param>
        /// <param name="gapVelocity"> Velocidad de avance rapido</param>
        /// <param name="gapAceleration"> Aceleracion de avance rapido</param>
        /// <param name="gapTorque"> Torque de avance rapido</param>
        /// <param name="aproxVelocity"> Velocidad de Softlanding</param>
        /// <param name="aproxAceleration"> Aceleracion de Softlanding</param>
        /// <param name="aproxTorque"> Torque de Softlanding</param>
        /// <param name="pushTorque"> Empuje lineal una vez se ha hecho contacto con el selector</param>
        /// <param name="rotVelocity"> Velocidad de giro para la entrada en el selector</param>
        /// <param name="rotAceleration"> Aceleration angular</param>
        /// <param name="rotTorque"> Par maximo de movimiento rotacional, limitado para que cuando llegue a la posicion 0 no pueda avanzar</param>
        /// <param name="execute"> Determina si se debe ejecutar la macro una vez escrita</param>
        /// <param name="write"> Determina si en necesario escribir las macro, si ya han sido escritas no hace falta volveras a escribir puediendo ahorra tiempo</param>
        public void WriteAndRunFindSelector(bool execute = true, bool write = false, double gapPosition = 0 , int gapVelocity = 0, int gapAceleration = 0, int gapTorque = 0, int aproxVelocity = 0, int aproxAceleration = 0, int aproxTorque = 0, int pushTorque = 0, int rotVelocity = 0, int rotAceleration = 0, int rotTorque = 0)
        {
            if (write)
            {
                var softland1 = new SOFTLAND_1
                {
                    Velocity = gapVelocity,
                    Aceleration = gapAceleration,
                    Torque = gapTorque,
                    TrackError = 5000,
                    Position = (int)(gapPosition * CTE_POS_LIN)
                };

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_1, true, softland1.ToString()));
                Thread.Sleep(TR_TIME);

                var softland2 = new SOFTLAND_2
                {
                    Velocity = aproxVelocity,
                    Aceleration = aproxAceleration,
                    Torque = aproxTorque,
                    TrackError = 5000
                };

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_2, true, softland2.ToString()));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_3, true));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_X1, true));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_X2, true));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_4, true));
                Thread.Sleep(TR_TIME);

                var softland5 = new SOFTLAND_5
                {
                    PushTorque = pushTorque,
                    TrackError = 500
                };

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_5, true, softland5.ToString()));
                Thread.Sleep(TR_TIME);

                var softland6 = new SOFTLAND_6
                {
                    Velocity = rotVelocity,
                    Aceleration = rotAceleration,
                    Torque = rotTorque,
                    TrackError = 12000
                };

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_6, true, softland6.ToString()));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_7, true));
                Thread.Sleep(TR_TIME);

                var softland8 = new SOFTLAND_8
                {
                    Torque = rotTorque,
                    TorqueDelta = 2
                };

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_8, true, softland8.ToString()));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_9, true));
                Thread.Sleep(TR_TIME);

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_SOFTLAND_10, true));
                Thread.Sleep(TR_TIME);
            }

            if (execute)
                RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_SOFTLAND, true)); },
                    () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_SOFTLAND, true)); }
                    , 22000, "Error en la ejecucion de la macro RunFindSelector");
        }

        /// <summary>
        /// Funcion para ir a una posicion del eje rotacional definida previamente por la funcion "WritePositions".</summary>
        /// <param name="position"> Nº Posicion a la que se quiere ir</param>
        public void RunGoPosition(byte position)
        {        
            var pos = new RUN_POSITION
            {
                NumPosition = position,
            };

            RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_POSTION, true, pos.ToString())); },
                () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_POSITION, true)); }
                , 15000, "Error en la ejecucion de la macro RunFindSelector");
        }

        /// <summary>
        /// Funcion para definir posiciones del eje rotativo para despues ser usadas con la funcion RunGoPosition.</summary>
        /// <param name="positions"> Array de posiciones que se desean almacenar por orden</param>
        /// <param name="velocity"> Velocidad del movimiento de rotacion </param>
        /// <param name="aceleration"> Aceleracion del movimiento de rotacion </param>
        /// <param name="torque"> Torque del movimiento de rotacion </param>
        public void WritePositions(double[] positions, int velocity, int aceletarion, int torque, int trackError)
        {
            byte numPosition = 0;
            foreach (int pos in positions)
            {
                var positionClass = new POSITION_1
                {
                    Position = (int)(pos * CTE_POS_ROT),
                    Velocity = velocity,
                    Aceleration = aceletarion,
                    Torque = torque,
                    TrackError = (int)(trackError * CTE_POS_ROT),
                    NumPosition = numPosition,
                };

                SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_POSITION_1, true, positionClass.ToString()));
                Thread.Sleep(TR_TIME);
                numPosition++;
            }

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_POSITION_2, true));
            Thread.Sleep(TR_TIME);
        }

        /// <summary>
        /// Funcion para ir de una escala a otra en base a los saltos de fuerza del eje rotativo 
        /// </summary>
        public void WriteTorqueStep(int velocity, int aceletarion, int torque, int trackError, int maximumTime, int minimumDrop)
        {
            var class1 = new TORQUESTEP_1
            {
                Velocity = velocity,
                Aceleration = aceletarion,
                Torque = torque,
                TrackError = (int)(trackError * CTE_POS_ROT),
            };

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_1, true, class1.ToString()));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_2, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_3, true));
            Thread.Sleep(TR_TIME);

            var class4 = new TORQUESTEP_4
            {
                MaximumTime = maximumTime,
                MinimumDrop = minimumDrop,
            };

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_4, true, class4.ToString()));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_X1, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_5, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_6, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_7, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_8, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_TORQUESTEP_9, true));
            Thread.Sleep(TR_TIME);
        }

        /// <summary>
        /// Funcion para ir a la siguiente posicion con la funcion TorqueStep
        /// </summary>
        public void GoNextTorqueStep()
        {
            RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_TORQUESTEP, true)); },
                () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_TORQUESTEP, true)); }
                , 10000, "Error en la ejecucion de la macro RunHomingRotative");
        }

        /// <summary>
        /// Funcion para volver a la poscion 0, utiliza el segundo trozo de la funcion softland
        /// </summary>
        public void GoInitialPosition()
        {
            RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_INITIAL_POSITION, true)); },
                () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_SOFTLAND, true)); }
                , 17000, "Error en la ejecucion de la macro RunHomingRotative");
        }

        /// <summary>
        /// Funcion para escribir la macro para ir a la posicion final del selector, prueba del LC
        /// </summary>
        public void WriteGoFinalPosition(int rotVelocity = 0, int rotAceleration = 0, int rotTorque = 0)
        {
            var finalPosition = new GOFINALPOSITION_1
            {
                Velocity = rotVelocity,
                Aceleration = rotAceleration,
                Torque = rotTorque,
                TrackError = 12000
            };

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_GOFINALPOSITION_1, true, finalPosition.ToString()));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_GOFINALPOSITION_2, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_GOFINALPOSITION_3, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_GOFINALPOSITION_4, true));
            Thread.Sleep(TR_TIME);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.MACRO_GOFINALPOSITION_X1, true));
            Thread.Sleep(TR_TIME);
        }

        /// <summary>
        /// Funcion para ir a la posicion final del selector, prueba del LC
        /// </summary>
        public void GoFinalPosition()
        {
            RunMacro(() => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.RUN_GOFINALPOSITION, true)); },
                () => { return SendCommand(new Command<PrivateCommands>(PrivateCommands.CHECK_GOFINALPOSITION, true)); }
                , 17000, "Error en la ejecucion de la macro GoFinalPosition");

        }

        /// <summary>        
        /// /// Funcion para apagar el motor.</summary>
        public void MotorOff()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.ALL_MOTOR_OFF, true));
            Thread.Sleep(TR_TIME);
        }

        /// <summary>
        /// Funcion para borrar todas las macro.</summary>
        public void RevomeMacros()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.REMOVE_MACRO, true));
            Thread.Sleep(TR_TIME);
        }
        
        /// <summary>
        /// Funcion interna para de control de ejecucion de macro, donde se comprueban si hay errores y se le añade un control por timeout.</summary>
        private void RunMacro(Func<bool> funcRunMacro, Func<bool> funcCheckMacro, int timeOut, string errMessage )
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            funcRunMacro();

            RunningMacro = 1;

            while ((RunningMacro == 1) && (stopWatch.ElapsedMilliseconds < timeOut))
                try
                {
                    funcCheckMacro();
                    Thread.Sleep(200);
                }
                catch (TimeoutException e)
                {
                    _logger.Warn(e.Message);
                }

            if (RunningMacro < 0)
            {
                SendCommand(new Command<PrivateCommands>(PrivateCommands.DEBUG_TIMER, true));
                throw new Exception(dictionaryMacroErrors[RunningMacro]);
            }

            if (RunningMacro == 1)
            {
                MotorOff();
                throw new Exception("ERROR: LAC-25-RUNMACRO-Timeout: " + errMessage);
            }

        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            var message = new LAC25Message();
            CultureInfo enUS = new CultureInfo("en-US");
            bool IsChecking = false;

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)PrivateCommands.ALL_MOTOR_OFF:
                case (int)PrivateCommands.INIT_DRIVER:
                case (int)PrivateCommands.INIT_VECTORES_FALLO:
                case (int)PrivateCommands.INIT_MACRO_ERROR_POS_LINEAL:
                case (int)PrivateCommands.INIT_MACRO_ERROR_POS_ROTATIVO:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_1:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_2:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_3:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_4:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_X1:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_5:
                case (int)PrivateCommands.MACRO_HOMING_LINEAL_6:
                case (int)PrivateCommands.MACRO_HOMING_ROTATIVE_1:
                case (int)PrivateCommands.MACRO_HOMING_ROTATIVE_2:
                case (int)PrivateCommands.MACRO_HOMING_ROTATIVE_3:
                case (int)PrivateCommands.MACRO_HOMING_ROTATIVE_4:
                case (int)PrivateCommands.MACRO_HOMING_ROTATIVE_5:
                case (int)PrivateCommands.MACRO_HOMING_ROTATIVE_X1:
                case (int)PrivateCommands.MACRO_SOFTLAND_3:
                case (int)PrivateCommands.MACRO_SOFTLAND_X1:
                case (int)PrivateCommands.MACRO_SOFTLAND_X2:
                case (int)PrivateCommands.MACRO_SOFTLAND_4:
                case (int)PrivateCommands.MACRO_SOFTLAND_7:
                case (int)PrivateCommands.MACRO_SOFTLAND_9:
                case (int)PrivateCommands.MACRO_SOFTLAND_10:
                case (int)PrivateCommands.MACRO_GOFINALPOSITION_2:
                case (int)PrivateCommands.MACRO_GOFINALPOSITION_3:
                case (int)PrivateCommands.MACRO_GOFINALPOSITION_4:
                case (int)PrivateCommands.MACRO_GOFINALPOSITION_X1:
                case (int)PrivateCommands.MACRO_POSITION_2:
                case (int)PrivateCommands.RUN_SOFTLAND:
                case (int)PrivateCommands.RUN_HOMING_LINEAL:
                case (int)PrivateCommands.RUN_HOMING_ROTATIVE:
                case (int)PrivateCommands.REMOVE_MACRO:
                case (int)PrivateCommands.MACRO_TORQUESTEP_2:
                case (int)PrivateCommands.MACRO_TORQUESTEP_3:
                case (int)PrivateCommands.MACRO_TORQUESTEP_5:
                case (int)PrivateCommands.MACRO_TORQUESTEP_6:
                case (int)PrivateCommands.MACRO_TORQUESTEP_7:
                case (int)PrivateCommands.MACRO_TORQUESTEP_X1:
                case (int)PrivateCommands.MACRO_TORQUESTEP_8:
                case (int)PrivateCommands.MACRO_TORQUESTEP_9:
                case (int)PrivateCommands.RUN_TORQUESTEP:
                case (int)PrivateCommands.RUN_INITIAL_POSITION:
                case (int)PrivateCommands.RUN_GOFINALPOSITION:
                case (int)PrivateCommands.DEBUG_TIMER:
                    message.SetArgs(enUS, dictionaryCommands[command.Commands]);
                    message.NumCommands = 2;
                    sp.WriteTimeout = sp.ReadTimeout = 500;
                    break;
                case (int)PrivateCommands.CHECK_HOMING_LINEAL:
                case (int)PrivateCommands.CHECK_HOMING_ROTATIVE:
                case (int)PrivateCommands.CHECK_SOFTLAND:
                case (int)PrivateCommands.CHECK_GOFINALPOSITION:
                case (int)PrivateCommands.CHECK_POSITION:
                case (int)PrivateCommands.CHECK_TORQUESTEP:
                    message.SetArgs(enUS, dictionaryCommands[command.Commands]);
                    message.NumCommands = 2;
                    sp.WriteTimeout = sp.ReadTimeout = 5000;
                    IsChecking = true;
                    break;
                case (int)PrivateCommands.SET_PID_CONSTANTS:
                case (int)PrivateCommands.MACRO_SOFTLAND_1:
                case (int)PrivateCommands.MACRO_SOFTLAND_2:
                case (int)PrivateCommands.MACRO_SOFTLAND_5:
                case (int)PrivateCommands.MACRO_SOFTLAND_6:
                case (int)PrivateCommands.MACRO_SOFTLAND_8:
                case (int)PrivateCommands.MACRO_GOFINALPOSITION_1:
                case (int)PrivateCommands.MACRO_POSITION_1:
                case (int)PrivateCommands.WRITE_POSITIONS:
                case (int)PrivateCommands.RUN_POSTION:
                case (int)PrivateCommands.MACRO_TORQUESTEP_1:
                case (int)PrivateCommands.MACRO_TORQUESTEP_4:
                    message.SetArgs(enUS, command.Args[0].ToString());
                    message.NumCommands = 2;
                    sp.WriteTimeout = sp.ReadTimeout = 500;
                    break;
                default:
                    throw new NotSupportedException();
            }

            try
            {
                LAC25Message response = transport.SendMessage(message);
                string[] responeArray = response.Text.Trim().Replace("\n", "").Split(';');

                if (IsChecking)
                {
                    if (responeArray[1] == "1")
                        RunningMacro = 0;

                    int responeResult;
                    Int32.TryParse(responeArray[1], out responeResult);
                    if (responeResult < 0)
                        RunningMacro = responeResult;
                }
                else
                {
                    if (responeArray[1].Contains("?"))
                    {
                        int responeResult;
                        Int32.TryParse(responeArray[1].Replace("?", ""), out responeResult);

                        throw new Exception(dictionaryCommunicationErrors[responeResult]);
                    }
                }
            }
            catch (IndexOutOfRangeException) { }
        }

        /// <summary>
        /// Funcion para hacer un Home de los dos ejes .</summary>
        /// <param name="write"> Las macros se envian al controlador y se guardan en su memoria interna</param>
        /// <param name="execute"> Las macros se ejecutan haciendo el homing de los dos eje s</param>
        public void Home(bool write = true, bool execute = true)
        {
            WriteAndRunHomingLineal(write, execute);

            WriteAndRunHomingRotative(write, execute);

            MotorOff();
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }

        // Tramas que pueden ser hardcodeadas porque son siempre las mismas
        private static readonly Dictionary<int, string> dictionaryCommands = new Dictionary<int, string>
        {
            { (int)PrivateCommands.ALL_MOTOR_OFF, "0MF" },
            { (int)PrivateCommands.INIT_DRIVER, "RM,0MF,FR0,RI0,PH0" },
            { (int)PrivateCommands.INIT_VECTORES_FALLO, "AL240,LV31,EV31,AL241,LV30,EV30" },
            { (int)PrivateCommands.INIT_MACRO_ERROR_POS_LINEAL, "MD240,MG\"*M240*\",1MF,MG\"ERROR\",EP" },
            { (int)PrivateCommands.INIT_MACRO_ERROR_POS_ROTATIVO, "MD241,MG\"*M241*\",2MF,MG\"ERROR\",EP" },
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_1, "MD20,MG\"*M20*\",AL0,AR1,AR20,AR105,1VM,MN,SV131072,SA26214,SQ15000,SE12000,DI1,GO,WA5" },
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_2, "MD21,MG\"*M21*\",RW538,ib-1000,no,mj22,RP" },
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_3, "MD22,MG\"*M22*\",DH,DI0,FI0,AL0,WL1830" },
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_4, "MD23,MG\"*M23*\",RL448,ic10,st,mj25,RL494,ig1000,no,mj24,RL1830,ig3000,NO,MJ101,AL0,AR105,RP" },
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_X1, "MD101,MG\"*M101*\",RA105,AA1,IG3,NO,MJ24,AR105,MJ23"},
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_5, "MD24,MG\"*M24*\",MF,MG\"ERROR\",AL-1,AR20,RL1830,AR1,EP" },
            { (int)PrivateCommands.MACRO_HOMING_LINEAL_6, "MD25,MG\"*M25*\",PM,MN,SV26210,GH,MG\"OK\",AL1,AR20,EP" },
            { (int)PrivateCommands.RUN_HOMING_LINEAL, "MS20" },
            { (int)PrivateCommands.CHECK_HOMING_LINEAL, "TR20" },
            { (int)PrivateCommands.MACRO_HOMING_ROTATIVE_1, "MD30,MG\"*M30*\",AL0,AR1,AR30,AR105,2VM,MN,SV262140,SA520,SQ15000,SE16000,DI1"},
            { (int)PrivateCommands.MACRO_HOMING_ROTATIVE_2, "MD31,MG\"*M31*\",DH,FI,AL0,WL1830,WA20,GO" },
            { (int)PrivateCommands.MACRO_HOMING_ROTATIVE_3, "MD32,MG\"*M32*\",RL592,ic10,st,mj34,RL638,RL1830,ig5000,NO,MJ102,AL0,AR105,RP" },
            { (int)PrivateCommands.MACRO_HOMING_ROTATIVE_X1, "MD102,MG\"*M102*\",RA105,AA1,IG3,NO,MJ33,AR105,MJ32"},
            { (int)PrivateCommands.MACRO_HOMING_ROTATIVE_4, "MD33,MG\"*M33*\",ST,MF,MG\"ERROR\",RL1830,AL-2,AR30,RL1830,AR1,EP" },
            { (int)PrivateCommands.MACRO_HOMING_ROTATIVE_5, "MD34,MG\"*M34*\",PM,MN,GH,MG\"OK\",AL1,AR30,EP" },
            { (int)PrivateCommands.RUN_HOMING_ROTATIVE, "MS30" },
            { (int)PrivateCommands.CHECK_HOMING_ROTATIVE, "TR30" },
            { (int)PrivateCommands.MACRO_SOFTLAND_3, "MD42,MG\"*M42*\",RW538,IG100,AB,MJ43,RL1830,IG3000,NO,MJ100,AL0,AR105,RP"},
            { (int)PrivateCommands.MACRO_SOFTLAND_X1, "MD100,MG\"*M100*\",RA105,AA1,IG3,NO,MJ48,AR105,MJ42"},
            { (int)PrivateCommands.MACRO_SOFTLAND_4, "MD43,MG\"*M43*\",MG\"Plano encontrado en:\":N,TP,SE200,RL494,AR10,AA100,AR11"},
            { (int)PrivateCommands.MACRO_SOFTLAND_7, "MD46,MG\"*M46*\",RW682,IB-2000,NO,MJ47,RL1830,IG14000,NO,MJ104,AL0,AR105,RP"},
            { (int)PrivateCommands.MACRO_SOFTLAND_X2, "MD104,MG\"*M104*\",RA105,AA1,IG3,NO,MJ49,AR105,MJ46"},
            { (int)PrivateCommands.MACRO_SOFTLAND_8, "MD47,MG\"*M47*\",2SQ-10000,WA500,SQ-5000,WA500,SQ-2500,WA500,MF,WA500,DH,MG\"OK\",AL1,AR40,EP"},
            { (int)PrivateCommands.MACRO_SOFTLAND_9, "MD48,MG\"*M48*\",0MF,MG\"ERROR\",AL-3,AR40,RL1830,AR1,EP"},
            { (int)PrivateCommands.MACRO_SOFTLAND_10, "MD49,MG\"*M49*\",0MF,MG\"ERROR\",AL-4,AR40,RL1830,AR1,EP"},
            { (int)PrivateCommands.RUN_SOFTLAND, "MS40"},
            { (int)PrivateCommands.CHECK_SOFTLAND, "TR40" },
            {(int)PrivateCommands.MACRO_POSITION_2, "MD60,MG\"*M60*\",MF,MG\"OK\",AL1,AR60,EP"},
            { (int)PrivateCommands.CHECK_POSITION, "TR60" },
            { (int)PrivateCommands.REMOVE_MACRO, "RM" },
            { (int)PrivateCommands.MACRO_TORQUESTEP_2, "MD91,MG\"*M91*\",AL0,AR1,AR101,AR102,AR104,AR90,AR105,WL1830,RL638,AR103"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_3, "MD92,MG\"*M92*\",RL638,TR0,AS@103,IG30,NO,MJ93,RP"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_X1, "MD103,MG\"*M103*\",RA105,AA1,IG3,NO,MJ96,AR105,MJ93"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_5, "MD94,MG\"*M94*\",RA100,IG@101,NO,MC97,RA104,IG5,NO,MJ98,RA100,AR101,MJ93"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_9, "MD98,MG\"*M98*\",2VM,SV0,SA6000,SQ16000,SE12000,MN,WA500,MJ95"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_6, "MD95,MG\"*M95*\",MF,MG\"OK\",AL1,AR90,EP"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_7, "MD96,MG\"*M96*\",MF,MG\"ERROR\",AL-6,AR90,RL1830,AR1,EP"},
            { (int)PrivateCommands.MACRO_TORQUESTEP_8, "MD97,MG\"*M97*\",RA104,AA1,AR104,RC"},  
            { (int)PrivateCommands.RUN_TORQUESTEP, "MS90"},
            { (int)PrivateCommands.RUN_INITIAL_POSITION, "MS45"},
            { (int)PrivateCommands.CHECK_TORQUESTEP, "TR90"},
            { (int)PrivateCommands.DEBUG_TIMER, "TR1"},
            { (int)PrivateCommands.MACRO_GOFINALPOSITION_2, "MD111,MG\"*M111*\",RW682,IG2000,NO,MJ112,RL1830,IG14000,NO,MJ105,AL0,AR105,RP"},
            { (int)PrivateCommands.MACRO_GOFINALPOSITION_X1, "MD105,MG\"*M105*\",RA105,AA1,IG3,NO,MJ113,AR105,MJ111"},
            { (int)PrivateCommands.MACRO_GOFINALPOSITION_3, "MD112,MG\"*M112*\",2QM0,SQ15000,WA500,SQ7500,WA500,SQ2500,WA500,MF,WA500,DH,MG\"OK\",AL1,AR110,EP"},
            { (int)PrivateCommands.MACRO_GOFINALPOSITION_4, "MD113,MG\"*M113*\",0MF,MG\"ERROR\",AL-7,AR110,RL1830,AR1,EP"},
            { (int)PrivateCommands.RUN_GOFINALPOSITION, "MS110"},
            { (int)PrivateCommands.CHECK_GOFINALPOSITION, "TR110"},
        };

        // Errores que se han codificado dentro de las macros
        private static readonly Dictionary<int, string> dictionaryMacroErrors = new Dictionary<int, string>
        {
             { -1, "ERROR : LAC-25-HOMING: No se encontró el indice eje 1 (Lineal)" },
             { -2, "ERROR : LAC-25-HOMING: No se encontró el indice eje 2 (Rotativo)" },
             { -3, "ERROR: LAC-25-SOFTLAND: No se ha detectado contacto con el selector" },
             { -4, "ERROR: LAC-25-SOFTLAND: La punta no ha entrado en el selector" },
            { -5, "ERROR: LAC-25-SELECT_POSITION: No se ha podido llegar a la posicion objetivo" },
            { -6, "ERROR: LAC-25-TORQUE_STEP: No se ha podido determinar la siguiente escala" },
            { -7, "ERROR: LAC-25-GO_FINAL_POSITION: No se ha podido llegar a la posicion final del selector" },
        };

        // Errores propios del controlador
        private static readonly Dictionary<int, string> dictionaryCommunicationErrors = new Dictionary<int, string>
        {
             { 1, "ERROR - LAC-25-COMUNICATIONS: ERR-1: ARGUMENT ERROR" },
             { 2, "ERROR - LAC-25-COMUNICATIONS: ERR-2: INVALID COMMAND" },
             { 3, "ERROR - LAC-25-COMUNICATIONS: ERR-3: INVALID MACRO COMMAND" },
             { 4, "ERROR - LAC-25-COMUNICATIONS: ERR-4: MACRO ARGUMENT ERROR" },
             { 5, "ERROR - LAC-25-COMUNICATIONS: ERR-5: MACRO NOT DEFINED" },
             { 6, "ERROR - LAC-25-COMUNICATIONS: ERR-6: MACRO OUT OF RANGE" },
             { 7, "ERROR - LAC-25-COMUNICATIONS: ERR-7: OUT OF MACRO SPACE" },
             { 8, "ERROR - LAC-25-COMUNICATIONS: ERR-8: NESTING MACRO" },
             { 9, "ERROR - LAC-25-COMUNICATIONS: ERR-9: CANT DEFINE A MACRO DURING SERVO ENABLED" },
             { 10, "ERROR - LAC-25-COMUNICATIONS: ERR-10: MACRO JUMP ERROR" },
             { 11, "ERROR - LAC-25-COMUNICATIONS: ERR-11: OUT OF MACRO STACK SPACE" },
             { 12, "ERROR - LAC-25-COMUNICATIONS: ERR-12: MACRO MUST BE FIRST COMMAND" },
             { 13, "ERROR - LAC-25-COMUNICATIONS: ERR-13: STRING ERROR" },
             { 14, "ERROR - LAC-25-COMUNICATIONS: ERR-14: MACRO STRING ERROR" },
             { 15, "ERROR - LAC-25-COMUNICATIONS: ERR-15: SYNTAX ERROR" },
             { 16, "ERROR - LAC-25-COMUNICATIONS: ERR-16: MACRO SYNTAX ERROR" },
             { 17, "ERROR - LAC-25-COMUNICATIONS: ERR-17: AXIS RANGE ERROR" },
             { 18, "ERROR - LAC-25-COMUNICATIONS: ERR-18: INTERRUPT MACRO NOT DEFINED" },
             { 19, "ERROR - LAC-25-COMUNICATIONS: ERR-19: INTERRUPT MACRO STACK ERROR" },
             { 20, "ERROR - LAC-25-COMUNICATIONS: ERR-20: MACRO STACK OVERFLOW" },
             { 21, "ERROR - LAC-25-COMUNICATIONS: ERR-21: MACRO STACK UNDERFLOW" },
        };

        private enum PrivateCommands
        {
            ALL_MOTOR_OFF = Commands.USER_DEFINED_COMMANDS + 1,
            INIT_DRIVER = Commands.USER_DEFINED_COMMANDS + 2,
            INIT_VECTORES_FALLO = Commands.USER_DEFINED_COMMANDS + 3,
            INIT_MACRO_ERROR_POS_LINEAL = Commands.USER_DEFINED_COMMANDS + 4,
            INIT_MACRO_ERROR_POS_ROTATIVO = Commands.USER_DEFINED_COMMANDS + 5,
            SET_PID_CONSTANTS = Commands.USER_DEFINED_COMMANDS +6,
            MACRO_HOMING_LINEAL_1 = Commands.USER_DEFINED_COMMANDS + 7,
            MACRO_HOMING_LINEAL_2 = Commands.USER_DEFINED_COMMANDS + 8,
            MACRO_HOMING_LINEAL_3 = Commands.USER_DEFINED_COMMANDS + 9,
            MACRO_HOMING_LINEAL_4 = Commands.USER_DEFINED_COMMANDS + 10,
            MACRO_HOMING_LINEAL_X1 = Commands.USER_DEFINED_COMMANDS + 100,
            MACRO_HOMING_LINEAL_5 = Commands.USER_DEFINED_COMMANDS + 11,
            MACRO_HOMING_LINEAL_6 = Commands.USER_DEFINED_COMMANDS + 12,
            RUN_HOMING_LINEAL = Commands.USER_DEFINED_COMMANDS + 13,
            CHECK_HOMING_LINEAL = Commands.USER_DEFINED_COMMANDS + 31,
            MACRO_HOMING_ROTATIVE_1 = Commands.USER_DEFINED_COMMANDS + 14,
            MACRO_HOMING_ROTATIVE_2 = Commands.USER_DEFINED_COMMANDS + 15,
            MACRO_HOMING_ROTATIVE_3 = Commands.USER_DEFINED_COMMANDS + 16,
            MACRO_HOMING_ROTATIVE_4 = Commands.USER_DEFINED_COMMANDS + 17,
            MACRO_HOMING_ROTATIVE_5 = Commands.USER_DEFINED_COMMANDS + 18,
            MACRO_HOMING_ROTATIVE_X1 = Commands.USER_DEFINED_COMMANDS + 101,
            RUN_HOMING_ROTATIVE = Commands.USER_DEFINED_COMMANDS + 19,
            CHECK_HOMING_ROTATIVE = Commands.USER_DEFINED_COMMANDS + 32,
            MACRO_SOFTLAND_1 = Commands.USER_DEFINED_COMMANDS + 20,
            MACRO_SOFTLAND_2 = Commands.USER_DEFINED_COMMANDS + 21,
            MACRO_SOFTLAND_3 = Commands.USER_DEFINED_COMMANDS + 22,
            MACRO_SOFTLAND_X1 = Commands.USER_DEFINED_COMMANDS + 102,
            MACRO_SOFTLAND_X2 = Commands.USER_DEFINED_COMMANDS + 104,
            MACRO_SOFTLAND_4 = Commands.USER_DEFINED_COMMANDS + 23,
            MACRO_SOFTLAND_5 = Commands.USER_DEFINED_COMMANDS + 24,
            MACRO_SOFTLAND_6 = Commands.USER_DEFINED_COMMANDS + 25,
            MACRO_SOFTLAND_7 = Commands.USER_DEFINED_COMMANDS + 26,
            MACRO_SOFTLAND_8 = Commands.USER_DEFINED_COMMANDS + 27,
            MACRO_SOFTLAND_9 = Commands.USER_DEFINED_COMMANDS + 28,
            MACRO_SOFTLAND_10 = Commands.USER_DEFINED_COMMANDS + 29,
            RUN_SOFTLAND = Commands.USER_DEFINED_COMMANDS + 30,
            CHECK_SOFTLAND = Commands.USER_DEFINED_COMMANDS + 33,
            MACRO_POSITION_1 = Commands.USER_DEFINED_COMMANDS + 34,
            MACRO_POSITION_2 = Commands.USER_DEFINED_COMMANDS + 35,
            RUN_POSTION = Commands.USER_DEFINED_COMMANDS + 38,
            CHECK_POSITION = Commands.USER_DEFINED_COMMANDS + 39,
            REMOVE_MACRO = Commands.USER_DEFINED_COMMANDS + 40,
            WRITE_POSITIONS = Commands.USER_DEFINED_COMMANDS + 41,
            MACRO_TORQUESTEP_1 = Commands.USER_DEFINED_COMMANDS + 42,
            MACRO_TORQUESTEP_2 = Commands.USER_DEFINED_COMMANDS + 43,
            MACRO_TORQUESTEP_3 = Commands.USER_DEFINED_COMMANDS + 44,
            MACRO_TORQUESTEP_4 = Commands.USER_DEFINED_COMMANDS + 45,
            MACRO_TORQUESTEP_5 = Commands.USER_DEFINED_COMMANDS + 46,
            MACRO_TORQUESTEP_6 = Commands.USER_DEFINED_COMMANDS + 47,
            MACRO_TORQUESTEP_7 = Commands.USER_DEFINED_COMMANDS + 48,
            MACRO_TORQUESTEP_8 = Commands.USER_DEFINED_COMMANDS + 49,
            MACRO_TORQUESTEP_9 = Commands.USER_DEFINED_COMMANDS + 60,
            MACRO_TORQUESTEP_X1 = Commands.USER_DEFINED_COMMANDS + 103,
            RUN_TORQUESTEP = Commands.USER_DEFINED_COMMANDS + 50,
            CHECK_TORQUESTEP = Commands.USER_DEFINED_COMMANDS + 51,
            RUN_INITIAL_POSITION = Commands.USER_DEFINED_COMMANDS + 52,
            DEBUG_TIMER = Commands.USER_DEFINED_COMMANDS + 105,
            MACRO_GOFINALPOSITION_1 = Commands.USER_DEFINED_COMMANDS + 53,
            MACRO_GOFINALPOSITION_2 = Commands.USER_DEFINED_COMMANDS + 54,
            MACRO_GOFINALPOSITION_3 = Commands.USER_DEFINED_COMMANDS + 55,
            MACRO_GOFINALPOSITION_4 = Commands.USER_DEFINED_COMMANDS + 56,
            MACRO_GOFINALPOSITION_X1 = Commands.USER_DEFINED_COMMANDS + 106,
            RUN_GOFINALPOSITION = Commands.USER_DEFINED_COMMANDS + 57,
            CHECK_GOFINALPOSITION = Commands.USER_DEFINED_COMMANDS + 58,
        }

        public enum AxisActuator
        {
            ALL = 0,
            LINEAL = 1,
            ROTATIVO = 2,
        }

        public enum EnumWorkMode
        {
            POSITION,
            VELOCITY,
            TORQUE
        }

    }
}
