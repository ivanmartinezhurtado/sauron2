﻿using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Text.RegularExpressions;

namespace TaskRunner.Model
{
    public class ScriptService
    {
        private Script script;
        public ScriptHostBase ScriptHost { get; set; }

        private static readonly object _syncObject = new object();

        public Script Script
        {
            get
            {
                if (script == null)
                    script = CSharpScript.Create("", ScriptOptions.Default.WithReferences(typeof(System.Math).Assembly), ScriptHost.GetType());

                return script;
            }
        }

        public ScriptService()
        {
            ScriptHost = new ScriptHostBase();
        }

        public T Evaluate<T>(string code)
        {
            code = ResolveVariables(code);

            object result;
            lock (_syncObject)
            {
                result = Script.ContinueWith(code).RunAsync(ScriptHost).Result.ReturnValue;
            }
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);

            return (T)result;
        }

        public object Evaluate(string code)
        {
            code = ResolveVariables(code);

            object result;
            lock (_syncObject)
            {
                result = Script.ContinueWith(code).RunAsync(ScriptHost).Result.ReturnValue;
            }
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);

            return result;
        }

        private static string ResolveVariables(string code)
        {
            var variables = Regex.Matches(code, @"\B\$[\w\.]+");

            foreach (Match variable in variables)
            {
                var value = ResolveValue(variable.Value);
                var type = value.GetType();
                code = Regex.Replace(code, $@"\B\${variable.ToString().Replace("$", "")}(?![\w\d])", value.ToString().Replace(",", "."));
            }

            return code;
        }
        private static object ResolveValue(string name)
        {
            if (name == null)
                return name;

            if (!name.StartsWith("$"))
                return name;

            if (SequenceContext.Current == null)
                return null;

            var key = name.Trim().Substring(1);
            object variable = null;
            variable = SequenceContext.Current.Variables.Get(key);
            if (variable == null)
                variable = SequenceContext.Current.SharedVariables.Get(key);
            if (variable == null)
                return null;

            if (variable.GetType() == typeof(string))
                return $"\"{(variable as string)}\"";
            else
                return variable;
        }
    }
}
