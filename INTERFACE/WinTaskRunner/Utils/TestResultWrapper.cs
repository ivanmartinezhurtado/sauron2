﻿using TaskRunner.Model;

namespace WinTaskRunner.Utils
{
    public class TestResultWrapper
    {
        public TestResult TestResult { get; set; }
    }
}
