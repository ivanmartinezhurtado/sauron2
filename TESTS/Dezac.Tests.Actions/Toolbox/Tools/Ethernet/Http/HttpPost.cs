﻿using Comunications.Ethernet;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(HttpPostActionDescription))]
    public class HttpPost : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre o dirección del servidor")]
        public ActionVariable Host { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Dirección URL/URI")]
        public ActionVariable URL { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Contenido a enviar")]
        public ActionVariable Content { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var url = VariablesTools.ResolveText(URL.Value.ToString());
            var host = Host.Value.ToString();
            string content = string.Empty;
            if(Content != null && !Content.IsKeyNullOrEmpty)
                content = Content.Value.ToString();

            var resp = ComunicationHttpClient.HttpPostContent(host, url, content);

            var varName = DataOutput.Value.ToString() ?? Name;

            if (Context.Variables.ContainsKey(varName))
                Context.Variables.AddOrUpdate(varName, resp);
            else
                Context.Variables.AddOrUpdate(varName, resp);

            Context.ResultList.Add(string.Format("{0} = {1}", varName, resp));
        }
    }

    public class HttpPostActionDescription : ActionDescription
    {
        public override string Name { get { return "HttpPost"; } }
        public override string Description { get { return "Enviar documento al servidor"; } }
        public override string Category { get { return "Tools/Ethernet/Http"; } }
        public override Image Icon { get { return Properties.Resources.HTTP; } }
    }
}
