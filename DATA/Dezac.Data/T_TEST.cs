namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TEST")]
    public partial class T_TEST
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_TEST()
        {
            ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            T_TESTFASE = new HashSet<T_TESTFASE>();
            T_TESTFASES = new HashSet<T_TESTFASES>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTEST { get; set; }

        public int IDTEST { get; set; }

        public int NUMPROCESO { get; set; }

        public int? NUMFABRICACION { get; set; }

        public DateTime FECHA { get; set; }

        public int? TIEMPO { get; set; }

        [StringLength(1)]
        public string RESULTADO { get; set; }

        public int? NUMMATRICULATEST { get; set; }

        public int? NUMPRODUCTOTEST { get; set; }

        public int? VERSIONPRODUCTOTEST { get; set; }

        public int? NUMMATRICULA { get; set; }

        [StringLength(20)]
        public string NROSERIE { get; set; }

        [StringLength(20)]
        public string NROSERIEORIGINAL { get; set; }

        public int? VERSIONREPORT { get; set; }

        public int? NUMFAMILIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }

        public virtual MATRICULA MATRICULA { get; set; }

        public virtual ORDENPRODUCTO ORDENPRODUCTO { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASES> T_TESTFASES { get; set; }

        public virtual T_TESTREPORT T_TESTREPORT { get; set; }
    }
}
