﻿using Dezac.Core.Exceptions;
using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Threading;

namespace Dezac.Core.Utility
{
    public static class PowerShell
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PowerShell));

        public static void ProcessNetsh(string function, string argmunet, bool IsAdmin = false)
        {
            var FileName = @"c:\windows\system32\netsh.exe";
            var WorkingDirectory = Path.GetDirectoryName(FileName);
            string pass = "ControlOver2020";
            var secure = new SecureString();
            foreach (char c in pass)
                secure.AppendChar(c);

            try
            {
                var process = new Process();
                logger.InfoFormat("{0} -- ProcessStartInfo Start ->  netsh {1}", function, argmunet);

                var StartInfo = new ProcessStartInfo("netsh", argmunet);
                StartInfo.Verb = "runas";
                StartInfo.UseShellExecute = IsAdmin;
                StartInfo.WorkingDirectory = WorkingDirectory;
                StartInfo.FileName = FileName;

                if (!IsAdmin)
                {
                    StartInfo.Domain = "dezac.net";
                    StartInfo.Password = secure;
                    StartInfo.UserName = "idpuser";
                    StartInfo.RedirectStandardOutput = true;
                    StartInfo.RedirectStandardError = true;

                    StartInfo.LoadUserProfile = true;
                    StartInfo.ErrorDialog = true;
                    StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    StartInfo.CreateNoWindow = true;
                }

                process.StartInfo = StartInfo;
                process.Start();

                if (!IsAdmin)
                {
                    logger.InfoFormat(process.StandardOutput.ReadToEnd());
                    logger.InfoFormat(process.StandardError.ReadToEnd());
                }

                process.WaitForExit();
                var successful = process.ExitCode == 0;
                process.Dispose();

                logger.InfoFormat("{0} -- ProcessStartInfo end ->  succes {1}", function, successful);

                if (!successful)
                    TestException.Create().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET("NO SE HA PODIDO CONFIGURAR EL ADAPTADOR DE RED").Throw();

                logger.InfoFormat("{0} -- ProcessStartInfo wait 5seg.", function);
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                logger.InfoFormat(ex.Message);
                TestException.Create().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET(ex.Message).Throw();
            }
        }

    }
}
