namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_FAMILIAPARAM")]
    public partial class T_FAMILIAPARAM
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMFAMILIA { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string IDPARAM { get; set; }

        [Required]
        [StringLength(50)]
        public string PARAM { get; set; }

        [StringLength(10)]
        public string IDFASE { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }
    }
}
