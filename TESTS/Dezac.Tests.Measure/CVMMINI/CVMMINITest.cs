using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO.Ports;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(2.05)]
    public class CVMMINITest : TestBase
    {
        CVMMINI cvm;
        Tower3 tower;
        private SerialPort sp;

        string CAMARA_IDS;

        public double transformationRelation = 1;
        public double factorTension = 1;

        protected const byte IN_SECURITY_DETECTION = 23;
        protected const byte IN_RS485_DETECTION = 24; 
        protected const byte IN_ETH_DETECTION = 25;
        protected const byte IN_PREPAGO_DETECTION = 26;
        protected const byte IN_COM_PISTON_RS485 = 27;
        protected const byte IN_COM_PISTON_ETH = 28;
        protected const byte IN_TTL_DETECTION = 29;
        protected const byte IN_TERMINALS_1TO9_DETECTION = 30;
        protected const byte IN_TERMINALS_10TO15_DETECTION = 31;
        protected const byte IN_ATTACHMENT_DETECTION = 32;
        protected const byte IN_COVER_DETECTION = 33;
        protected const byte IN_DOOR_DETECTION = 34;
        protected const byte IN_DEVICE_DETECTION = 26;

        protected const byte IN_PISTON_BORNES_RS485 = 31;

        protected const byte OUT_MAIN_EV = 4;
        protected const byte OUT_ATTACHMENT_PISTON = 5;
        protected const byte OUT_TERMINALS_PISTON = 6;
        protected const byte OUT_TTL_PISTON = 7;
        protected const byte OUT_COM_RS485_POSITION = 8;

        protected const byte OUT_EV_COM = 9;
        protected const byte OUT_RS485_PISTON = 49;
        protected const byte OUT_ETH_PISTON = 50;

        protected const byte OUT_KEYBOARD1 = 10;
        protected const byte OUT_KEYBOARD2 = 11;

        //protected const byte OUT_COM_CIRBUS = 22; //
        //protected const byte OUT_COM_OFF_1 = 15;//
        //protected const byte OUT_COM_OFF_2 = 43;//
        protected const byte OUT_333mV = 54;
        protected const byte OUT_PILOTO_ERROR = 51;
        protected const byte OUT_COM_CIRBUS = 18;

        public CVMMINI CVMMini
        {
            get
            {
                if (cvm==null)
                    cvm = AddInstanceVar(new CVMMINI(), "UUT");

                return cvm;
            }
        }

        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        public virtual void TestInitialization(int serialPort = 0)
        {
            byte Periferico = Comunicaciones.Periferico;
            int BaudRate = Comunicaciones.BaudRate;
            if (serialPort == 0)
                    serialPort = Comunicaciones.SerialPort;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            Tower.IO.DO.Off(OUT_PILOTO_ERROR);
            CVMMini.SetPort(serialPort, Periferico, BaudRate);        

            this.InParellel(() =>
            {
                tower.PowerSourceIII.ApplyOff();
            },
            () =>
            {
                tower.Chroma.ApplyOff();
            },
            () =>
            {
                tower.LAMBDA.ApplyOff();
            }
            );

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[IN_DOOR_DETECTION])
                    return false;
                return true;
            }, "Error: No se ha detectado puerta cerrada", 50, 500, 0, false, false, (s) =>
            {
                Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES().Throw();
            });

            tower.IO.DO.On(OUT_MAIN_EV);

            tower.IO.DO.On(OUT_ATTACHMENT_PISTON);
            SamplerWithCancel((p) => { return (tower.IO.DI[IN_ATTACHMENT_DETECTION]); }, "Error: No se han detectado los pistones de fijacion",
                20, 100, 700, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            tower.IO.DO.On(OUT_TERMINALS_PISTON);

            SamplerWithCancel((p) => { return (tower.IO.DI[IN_TERMINALS_1TO9_DETECTION]); }, "Error: No se ha detectado el piston de la bornera derecha",
                20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            SamplerWithCancel((p) => { return (tower.IO.DI[IN_TERMINALS_10TO15_DETECTION]); }, "Error: No se han detectado el piston de la bornera izquierda",
                20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            SamplerWithCancel((p) => { return (tower.IO.DI[IN_DEVICE_DETECTION]); }, "Error: No se detecta el equipo o no lleva el ancla puesta",
                 2, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES(s).Throw(); });

            switch (Comunicaciones.SerialPort)
            {
                case 5:
                    tower.IO.DO.On(OUT_TTL_PISTON);
                    SamplerWithCancel((p) => { return tower.IO.DI[IN_TTL_DETECTION]; }, "Error: No se ha detectado el piston de la bornera de comunicaciones TTL",
                        20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

                    break;

                case 7:
                    tower.IO.DO.On(OUT_COM_RS485_POSITION);
                    SamplerWithCancel((p) => { return !tower.IO.DI[IN_COM_PISTON_ETH] && tower.IO.DI[IN_COM_PISTON_RS485]; }, "Error: No se ha detectado el piston de seleccion de COM en posicion RS485",
                        20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

                    tower.IO.DO.OnWait(200, OUT_EV_COM);
                    tower.IO.DO.On(OUT_RS485_PISTON);
                    SamplerWithCancel((p) => { return tower.IO.DI[IN_RS485_DETECTION]; }, "Error: No se ha detectado el piston de ETHERNET",
                        20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

                    break;
            }

            if (VectorHardware.GetString("IS_333mV", "NO", ParamUnidad.SinUnidad) == "SI")
                tower.IO.DO.On(OUT_333mV);

            tower.PowerSourceIII.Tolerance = Configuracion.GetDouble("MTE_TOLERANCIA", 0.2, ParamUnidad.PorCentage);
            tower.PowerSourceIII.TimeOutStabilitation = (int)Configuracion.GetDouble("MTE_TIMEOUT_ESTABILIZACION", 35000, ParamUnidad.ms);
        }

        public void TestConsumption([Description("Tiempo espera estabilización consumo")]int timeWait = 1000)
        {
            var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);

            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 5,
                ResultName = "",
                TiempoIntervalo = 100,
                typePowerSource = typeSupply == "AC" ? TypePowerSource.CHROMA : TypePowerSource.LAMBDA,
                typeTestExecute = typeSupply == "AC" ? TypeTestExecute.ActiveAndReactive : TypeTestExecute.OnlyActive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 0, ParamUnidad.V),
                WaitTimeReadMeasure = timeWait
            };

            if (typeSupply != "AC")
                tower.IO.DO.On(23);

            this.TestConsumo(tower, configuration);
        }

        public virtual void TestCommunications()
        {
            SamplerWithCancel((p) =>
            {
                if (p > 1 && (CVMMini.Modbus is Device.ModbusDeviceSerialPort))
                {
                    var baudRate = (byte)Configuracion.GetDouble("COMUNICACIONES_03_VELOCIDAD", 3, ParamUnidad.SinUnidad);

                    var modbus = CVMMini.Modbus as Device.ModbusDeviceSerialPort;
                    modbus.BaudRate = CVMMini.baudrateDictionary[baudRate];

                    CVMMini.Modbus = modbus;
                }

                CVMMini.FlagTest();

                return true;

            }, "Error de comunicaciones", 3, 500, 500, false, false, (s) =>
            {
                Error().UUT.COMUNICACIONES.INICIALIZACION(s).Throw();
            });

            //SamplerWithCancel((p) => { CVMMini.FlagTest(); return true; }, "Error de comunicaciones", 3, 500, 500, false, false);

            var version = CVMMini.ReadSoftwareVersion().Trim();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware incorrecta, por favor actualice el equipo"), ParamUnidad.SinUnidad);
            Assert.AreEqual(ConstantsParameters.Identification.ROM_CODE, CVMMini.ReadROMCode(),Identificacion.ROM_CODE, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, ROM code incorrecto"), ParamUnidad.SinUnidad );

            transformationRelation = VectorHardware.GetDouble("RELACION_TRANSFORMACION", 1, ParamUnidad.SinUnidad);
            factorTension = Configuracion.GetDouble("FACTOR_TENSION", 1, ParamUnidad.SinUnidad);
        }

        public void TestComunicationsBACnet()
        {
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_PLACA_COM, CVMMini.ReadBacnetID().Trim(), Identificacion.VERSION_FIRMWARE_PLACA_COM, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware de la placa COM incorrecta"), ParamUnidad.SinUnidad);

            CVMMini.WriteBacnetID(TestInfo.BacNetID.Value);
            Resultado.Set("BACNET_ID", TestInfo.BacNetID.Value.ToString(), ParamUnidad.SinUnidad);

            CVMMini.WriteBacnetTemperatura(0);

            tower.IO.DO.Off(OUT_TTL_PISTON);

            ResetPowerSource(false);

            CVMMini.Modbus.ClosePort();

            tower.IO.DO.On(OUT_COM_RS485_POSITION);

            SamplerWithCancelWhitOutCancelToken(() => { return !tower.IO.DI[IN_COM_PISTON_ETH] && tower.IO.DI[IN_COM_PISTON_RS485]; }, "Error: No se ha detectado el piston de seleccion de COM en posicion RS485",
            40, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            Tower.IO.DO.OnWait(150,OUT_EV_COM);
            tower.IO.DO.On(OUT_RS485_PISTON);

            Delay(8000, "Esperando que el equipo empieze a lanzar tramas");

            try
            {
                sp = new SerialPort("COM" + 7, 38400, Parity.None, 8, StopBits.One);
                sp.ReadTimeout = 1000;
                sp.WriteTimeout = 1000;

                var Protocol = new BacNetProtocol();
                Protocol.CaracterFinRx = string.Empty;
                Protocol.CaracterFinTx = string.Empty;               
                var Transport = new MessageTransport(Protocol, new SerialPortAdapter(sp), Logger);
                Transport.LogMessage = false;
                Transport.LogRawMessage = true;
                Transport.Retries = 10;

                sp.Open();

                CultureInfo enUS = new CultureInfo("en-US");

                var tx = BacNetMessage.Create("", "");
                tx.HasResponse = true;

                var rx = Transport.SendMessage<BacNetMessage>(tx);
            }
             finally
            {
                sp.Close();
                sp.Dispose();
            }

            Resultado.Set("COMUNICACION_BACNET", "OK", ParamUnidad.SinUnidad);

            tower.IO.DO.OffWait(1000, OUT_RS485_PISTON);
            tower.IO.DO.Off(OUT_EV_COM);

            tower.IO.DO.Off(OUT_COM_RS485_POSITION);

            SamplerWithCancelWhitOutCancelToken(() => { return tower.IO.DI[IN_COM_PISTON_ETH] && !tower.IO.DI[IN_COM_PISTON_RS485]; }, "Error: No se ha detectado el piston de seleccion de COM en posicion RS485",
            40, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            tower.IO.DO.On(OUT_TTL_PISTON);
        }

        public void TestSetupDefault()
        {                     
            CVMMini.FlagTest();

            CVMMini.WriteHardwareVector(GetHardwareVectorConfiguration());

            Delay(500, "");

            CVMMini.WriteSetupConfiguration(GetSetupConfiguration());

            Delay(500, "");

            CVMMini.WriteMaximumDemandConfiguration(GetMaximumDemandConfiguration());

            Delay(500, "");

            CVMMini.WriteCalibrationFactors(GetCalibrationFactors());

            Delay(500, "");

            CVMMini.WriteCompoundPhaseFactors(new CVMMINI.CompoundVoltageGains((ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_COMPUESTAS_DEFECTO", 10628, ParamUnidad.Puntos)));

            Delay(500, "");

            CVMMini.WriteITFCalibrationFactors(GetITFCalibrationFactors());

            Delay(500, "");

            CVMMini.WritePasswordConfiguration(GetPasswordConfiguration());

            Delay(500, "");

            CVMMini.WriteSerialNumber(0);

            Delay(500, "");

            CVMMini.WriteScreenConfiguration(GetScreenConfiguration());

            Delay(500, "");

            CVMMini.WriteClearAll();

            var mensaje = Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad);
            var mensajeFinal = mensaje == "NO" ? "        " : mensaje;
                CVMMini.WriteInitialMessage(mensajeFinal);
            Resultado.Set("MENSAJE_INICIAL", mensaje, ParamUnidad.SinUnidad);

            var time = (ushort)Configuracion.GetDouble("LCD_TIEMPO_OFF", 0, ParamUnidad.SinUnidad);
            if (time != 0)
                CVMMini.WriteLCDTime(time);
            Resultado.Set("LCD_TIEMPO_OFF", time.ToString(), ParamUnidad.SinUnidad);
    
            CVMMini.WriteAlarmConfiguration(new CVMMINI.AlarmConfig(), CVMMINI.Alarms.ALARM_1);

            CVMMini.WriteAlarmConfiguration(new CVMMINI.AlarmConfig(), CVMMINI.Alarms.ALARM_2);

            Delay(3000, "");

            ResetPowerSource();

            Delay(8000, "");
            
            SaveResultsSetupConfiguration(CVMMini.ReadSetupConfig());

            SaveResultsMaximumDemandConfiguration(CVMMini.ReadMaximumDemandConfiguration());

            SaveResultsHardwareVectorConfiguration(CVMMini.ReadHardwareVector());

            SaveResultsPasswordConfiguration(CVMMini.ReadPasswordConfiguration());
        }

        public void TestLeds()
        {
            var timeWait = 200;

            CAMARA_IDS = GetVariable<string>("CAMARA_IDS", CAMARA_IDS);

            CVMMini.FlagTest(true);        

            CVMMini.WriteLedState(new CVMMINI.LedsInformation(CVMMINI.State.ON));

            Delay(timeWait, "Espera condicionamiento de los leds");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA_IDS, "CVMMINI_LEDS", "LEDS", "CVMMINI");

            CVMMini.WriteLedState(new CVMMINI.LedsInformation(CVMMINI.State.OFF));
        }

        public void TestLedsNET()
        {
            var timeWait = 200;

            CAMARA_IDS = GetVariable<string>("CAMARA_IDS", CAMARA_IDS);

            CVMMini.FlagTest(true);

            var ledsInfo = new CVMMINI.LedsInformation();
            ledsInfo.CPU = CVMMINI.State.ON;
            ledsInfo.COMMUNICATIONS = CVMMINI.State.ON;
            ledsInfo.BACKLIGHT = CVMMINI.State.OFF;

            CVMMini.WriteLedState(ledsInfo);

            Delay(timeWait, "Espera condicionamiento de los leds");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA_IDS, "CVMMINI_LEDS", "LEDS", "CVMMINI");

            CVMMini.WriteLedState(new CVMMINI.LedsInformation(CVMMINI.State.OFF));
        }

        public void TestDisplay()
        {
            var timeWait = 1000;

            CAMARA_IDS = GetVariable<string>("CAMARA_IDS", CAMARA_IDS);

            CVMMini.FlagTest(true);

            CVMMINI.LedsInformation leds = new CVMMINI.LedsInformation(CVMMINI.State.OFF);

            leds.BACKLIGHT = CVMMINI.State.ON;

            CVMMini.WriteLedState(leds);

            CVMMini.SendDisplayInfo(CVMMINI.DisplayState.EVEN);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMARA_IDS, "CVMMINI_EVEN", "EVEN", "CVMMINI");

            CVMMini.SendDisplayInfo(CVMMINI.DisplayState.ODD);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMARA_IDS, "CVMMINI_ODD", "ODD", "CVMMINI");
        }

        public void TestRelays()
        {
            CVMMini.WriteOutputState(CVMMINI.Outputs.OUTPUT_1, CVMMINI.State.OFF);
            CVMMini.WriteOutputState(CVMMINI.Outputs.OUTPUT_2, CVMMINI.State.OFF);

            Delay(150, "Esperando desactivacion de la salida");

            foreach (CVMMINI.Outputs output in Enum.GetValues(typeof(CVMMINI.Outputs)))
            {
                SamplerWithCancel((p) =>
                    {
                        CVMMini.WriteOutputState(output, CVMMINI.State.ON);

                        Delay(150, "Esperando activacion de la salida");

                        var readingON = tower.IO.DI.Read(deviceOutputInputInformation[output]).Value;

                        CVMMini.WriteOutputState(output, CVMMINI.State.OFF);

                        Delay(100, "Esperando desactivacion de la salida");

                        var readingOFF = tower.IO.DI.Read(deviceOutputInputInformation[output]).Value;

                        Resultado.Set(output.ToString(), (readingON && !readingOFF) ? "OK" : "KO", ParamUnidad.SinUnidad);

                        Assert.IsTrue(readingON && !readingOFF, Error().UUT.CALIBRACION.DESCRIPCION_INCORRECTA(string.Format("Error al comprobar la {0} del equipo", output.GetDescription())));

                        return true;
                    }, string.Format("", output.GetDescription()));

                CVMMini.WriteOutputState(output, CVMMINI.State.OFF);

                Delay(150, "Esperando desactivacion de la salida");
            }

        }

        public void TestKeyboard()
        {
            var keys = new List<CVMMINI.Key>()
                {
                    CVMMINI.Key.MAX,
                    CVMMINI.Key.DISPLAY,
                    CVMMINI.Key.CLEAR_ENERGY,
                    CVMMINI.Key.RESET,
                    CVMMINI.Key.CLEAR_MAX_PD,
                    CVMMINI.Key.SETUP,
                };

            CVMMINI.Key keysPressed = CVMMINI.Key.NO_KEY;

            SamplerWithCancel((p) =>
            {
                tower.IO.DO.OnWait(150, OUT_KEYBOARD2);

                keysPressed = CVMMini.ReadKeyboard();
                keys.RemoveAll(x => keysPressed.HasFlag(x));

                tower.IO.DO.OffWait(150, OUT_KEYBOARD2);

                if (keys.Any())
                    throw new Exception(string.Format("Error: No se detecta la tecla/s {0}", string.Join<string>(", ", keys.ConvertAll<string>((x) => x.GetDescription()))));

                return true;
            }, "", 3, 500,0, false, false);

            SamplerWithCancel((p) =>
            {
                keysPressed = CVMMini.ReadKeyboard();

                if (keysPressed != CVMMINI.Key.NO_KEY)
                    throw new Exception(string.Format("Error: Tecla/s {0} encallada al desactivarla", keysPressed.ToString()));

                return true;
            }, "", 3, 500, 0, false, false);

            keys = new List<CVMMINI.Key>()
                {
                    CVMMINI.Key.MIN,
                };

            SamplerWithCancel((p) =>
            {
                tower.IO.DO.OnWait(150, OUT_KEYBOARD1);

                keysPressed = CVMMini.ReadKeyboard();
                keys.RemoveAll(x => keysPressed.HasFlag(x));

                tower.IO.DO.OffWait(150, OUT_KEYBOARD1);

                if (keys.Any())
                    throw new Exception(string.Format("Error: No se detecta la tecla/s {0}", string.Join<string>(", ", keys.ConvertAll<string>((x) => x.GetDescription()))));

                return true;
            }, "", 3, 500, 0, false, false);

            SamplerWithCancel((p) =>
            {
                keysPressed = CVMMini.ReadKeyboard();
                if (keysPressed != CVMMINI.Key.NO_KEY)
                    throw new Exception(string.Format("Error: Tecla/s {0} encallada al desactivarla", keysPressed.ToString()));

                return true;
            }, "", 3, 500, 0, false, false);

            Resultado.Set("KEYBOARD", "OK", ParamUnidad.SinUnidad);
        }

        public void TestKeyboardNET()
        {
            SamplerWithCancel((p) =>
            {
                tower.IO.DO.OnWait(150, OUT_KEYBOARD1);

                var result = CVMMini.ReadKeyboard().HasFlag(CVMMINI.Key.MIN);

                return result;
            }, "Error: No se detecta la tecla ", 3, 500);

            SamplerWithCancel((p) =>
            {
                tower.IO.DO.OffWait(150, OUT_KEYBOARD1);

                var result = CVMMini.ReadKeyboard() == CVMMINI.Key.NO_KEY;

                return result;
            }, "Error: Tecla encallada", 3, 500);


            Resultado.Set("KEYBOARD", "OK", ParamUnidad.SinUnidad);
        }

        public void TestOffsets(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var offsetsV = new TriAdjustValueDef(Params.V.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.V.Null.CrucePistas.TestPoint("OFFSETS").Min(), Params.V.Null.CrucePistas.TestPoint("OFFSETS").Max(), 0, 0, ParamUnidad.V);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.I.Null.CrucePistas.TestPoint("OFFSETS").Min(), Params.I.Null.CrucePistas.TestPoint("OFFSETS").Max(), 0, 0, ParamUnidad.A);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = CVMMini.ReadAllVariables();
                    return new double[] { vars.Phases.L1.Tension / 10D, vars.Phases.L2.Tension / 10D, vars.Phases.L3.Tension / 10D,
                        vars.Phases.L1.Corriente, vars.Phases.L2.Corriente, vars.Phases.L3.Corriente };

                }, delFirts, samples, timeInterval);
        }

        public void TestConsignaMTEShortCircuit()
        {
            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1 * factorTension ;
            crucePistasV.L2.Average = ConsignasV.L2 * factorTension;
            crucePistasV.L3.Average = ConsignasV.L3 * factorTension;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = CVMMini.ReadAllVariables();

                    return new double[] { vars.Phases.L1.Tension/ 10D, vars.Phases.L2.Tension/ 10D, vars.Phases.L3.Tension/ 10D, 
                        (double)vars.Phases.L1.Corriente/1000, (double)vars.Phases.L2.Corriente/1000, (double)vars.Phases.L3.Corriente/1000 };
                }, delFirts, samples, timeInterval);


            var offsetsChannelsV = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.Puntos);

            var offsetsChannelsI = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.Puntos);

            var OffsetsChannelsList = new List<TriAdjustValueDef>();
            OffsetsChannelsList.Add(offsetsChannelsV);
            OffsetsChannelsList.Add(offsetsChannelsI);

            TestMeasureBase(OffsetsChannelsList,
                (step) =>
                {
                    var vars = CVMMini.ReadChannelOffsets();

                    return new double[] { vars.VoltageL1, vars.VoltageL2, vars.VoltageL3,
                                    vars.CurrentL1, vars.CurrentL2, vars.CurrentL3 };
                }, delFirts, samples, timeInterval);
        }

        public void TestConsignaMTEVoltageCurrentAndCompound(EscalaEnum escala = EscalaEnum.ESC1)
        {
            internalTestAdjustVerificationVoltageCurrent(escala);
        }

        public void TestAdjustVoltageCurrentAndCompound(int delFirts = 2, int initCount = 3, int samples = 14, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestReadMTEVoltageCurrent(EscalaEnum.ESC1);

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var measureGains = CVMMini.CalculateAdjustMeasureFactors(delFirts, initCount, samples, timeInterval, realVoltageCurrent.Voltage, realVoltageCurrent.Current, transformationRelation, factorTension,
                (value) =>
                {
                    triGains[0].L1.Value = value.VoltageCurrent.VoltageL1;
                    triGains[0].L2.Value = value.VoltageCurrent.VoltageL2;
                    triGains[0].L3.Value = value.VoltageCurrent.VoltageL3;

                    triGains[1].L1.Value = value.VoltageCurrent.CurrentL1;
                    triGains[1].L2.Value = value.VoltageCurrent.CurrentL2;
                    triGains[1].L3.Value = value.VoltageCurrent.CurrentL3;

                    triGains[2].L1.Value = value.Compound.L12;
                    triGains[2].L2.Value = value.Compound.L23;
                    triGains[2].L3.Value = value.Compound.L31;

                    return !HasError(triGains, false).Any();
                });

            triGains.ForEach(x => x.AddToResults(Resultado));

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            CVMMini.WriteCalibrationFactors(measureGains.Item2.VoltageCurrent);
            CVMMini.WriteCompoundPhaseFactors(measureGains.Item2.Compound);
        }

        public void TestVerificationVoltageCurrentAndCompound(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {         
            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A));
            defs.Add(new TriAdjustValueDef(Params.V.L12.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = CVMMini.ReadAllVariables();
                    var varsList = new List<double>()
                    {
                        vars.Phases.L1.Tension/ 10D, vars.Phases.L2.Tension/ 10D, vars.Phases.L3.Tension/ 10D,
                        vars.Phases.L1.Corriente / 1000D, vars.Phases.L2.Corriente / 1000D, vars.Phases.L3.Corriente / 1000D,
                        vars.Trifasicas.TensionLineaL1L2/ 10D, vars.Trifasicas.TensionLineaL2L3/ 10D, vars.Trifasicas.TensionLineaL3L1/ 10D
                    };
                    return varsList.ToArray();
                },
                () =>
                {
                    var realVoltageCurrent = internalTestReadMTEVoltageCurrent(EscalaEnum.ESC1);
                    var voltageRef = realVoltageCurrent.Voltage;
                    var currentRef = realVoltageCurrent.Current;
                    var voltageCompoundRef = voltageRef * Math.Sqrt(3);

                    var varsList = new List<double>()
                    {
                        voltageRef.L1 * factorTension, voltageRef.L2 * factorTension, voltageRef.L3 * factorTension,
                        currentRef.L1 * transformationRelation, currentRef.L2 *transformationRelation , currentRef.L3 * transformationRelation,
                        voltageCompoundRef.L1 * factorTension, voltageCompoundRef.L2 * factorTension, voltageCompoundRef.L3 * factorTension,
                    };
                    return varsList.ToArray();
                }, countSamples, maxSamples, timeInterval);
        }

        public void TestAdjustVoltageCurrentEscala2AndCompound(int delFirts = 2, int initCount = 3, int samples = 14, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestReadMTEVoltageCurrent(EscalaEnum.ESC2);

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.esc_2.Name, Params.GAIN_I.Null.Ajuste.esc_2.Min(), Params.GAIN_I.Null.Ajuste.esc_2.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var measureGains = CVMMini.CalculateAdjustMeasureFactorsWithEscala2(delFirts, initCount, samples, timeInterval, realVoltageCurrent.Voltage, realVoltageCurrent.Current, transformationRelation, factorTension,
                (value) =>
                {
                    triGains[0].L1.Value = value.VoltageCurrent.VoltageL1;
                    triGains[0].L2.Value = value.VoltageCurrent.VoltageL2;
                    triGains[0].L3.Value = value.VoltageCurrent.VoltageL3;

                    triGains[1].L1.Value = value.VoltageCurrent.CurrentL1Scale2;
                    triGains[1].L2.Value = value.VoltageCurrent.CurrentL2Scale2;
                    triGains[1].L3.Value = value.VoltageCurrent.CurrentL3Scale2;

                    triGains[2].L1.Value = value.Compound.L12;
                    triGains[2].L2.Value = value.Compound.L23;
                    triGains[2].L3.Value = value.Compound.L31;

                    return !HasError(triGains, false).Any();
                });

            triGains.ForEach(x => x.AddToResults(Resultado));

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            CVMMini.WriteCalibrationFactors(measureGains.Item2.VoltageCurrent);
            CVMMini.WriteCompoundPhaseFactors(measureGains.Item2.Compound);
        }

        public void TestVerificationVoltageCurrentEscala2AndCompound(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.esc_2.Name, 0, 0, 1, Params.I.Null.Verificacion.esc_2.Tol(), ParamUnidad.A));
            defs.Add(new TriAdjustValueDef(Params.V.L12.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = CVMMini.ReadAllVariables();
                    var varsList = new List<double>()
                    {
                        vars.Phases.L1.Tension/ 10D, vars.Phases.L2.Tension/ 10D, vars.Phases.L3.Tension/ 10D,
                        vars.Phases.L1.Corriente / 1000D, vars.Phases.L2.Corriente / 1000D, vars.Phases.L3.Corriente / 1000D,
                        vars.Trifasicas.TensionLineaL1L2/ 10D, vars.Trifasicas.TensionLineaL2L3/ 10D, vars.Trifasicas.TensionLineaL3L1/ 10D
                    };
                    return varsList.ToArray();
                },
                () =>
                {
                    var realVoltageCurrent = internalTestReadMTEVoltageCurrent(EscalaEnum.ESC2);
                    var voltageRef = realVoltageCurrent.Voltage;
                    var currentRef = realVoltageCurrent.Current;
                    var voltageCompoundRef = voltageRef * Math.Sqrt(3);

                    var varsList = new List<double>()
                    {
                        voltageRef.L1 * factorTension, voltageRef.L2 * factorTension, voltageRef.L3 * factorTension,
                        currentRef.L1 * transformationRelation, currentRef.L2 *transformationRelation , currentRef.L3 * transformationRelation,
                        voltageCompoundRef.L1 * factorTension, voltageCompoundRef.L2 * factorTension, voltageCompoundRef.L3 * factorTension,
                    };
                    return varsList.ToArray();
                }, countSamples, maxSamples, timeInterval);
        }

        public void TestAdjustCurrentEscala2(int delFirts = 2, int initCount = 3, int samples = 14, int timeInterval = 1100)
        {
            Tuple<bool, CVMMINI.MeasureGains> measureGains2 = new Tuple<bool, CVMMINI.MeasureGains>(false, new CVMMINI.MeasureGains());

            if (VectorHardware.GetString("SHUNT", "SI", ParamUnidad.SinUnidad).ToUpper() == "NO")
            {
                var realVoltageCurrent2 = internalTestReadMTEVoltageCurrent(EscalaEnum.ESC2);

                var triGains2 = new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.esc_2.Name, Params.GAIN_I.Null.Ajuste.esc_2.Min(), Params.GAIN_I.Null.Ajuste.esc_2.Max(), 0, 0, ParamUnidad.Puntos);

                measureGains2 = CVMMini.CalculateAdjustMeasureFactorsScale2(delFirts, initCount, samples, timeInterval, realVoltageCurrent2.Current, transformationRelation,
                    (value) =>
                    {
                        triGains2.L1.Value = value.CurrentL1Scale2;
                        triGains2.L2.Value = value.CurrentL2Scale2;
                        triGains2.L3.Value = value.CurrentL3Scale2;

                        return !triGains2.HasMinMaxError();
                    });

                triGains2.AddToResults(Resultado);

                if (!measureGains2.Item1)
                    throw new Exception(string.Format("Error en el ajuste de corriente Escala2 fuera de margenes"));
            }
            else
                measureGains2 = new Tuple<bool, CVMMINI.MeasureGains>(false, GetCalibrationFactors());

            CVMMini.WriteCalibrationFactors(measureGains2.Item2);
        }

        public void TestVerificationCurrentEscala2(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            var defs = new TriAdjustValueDef(Params.I.L1.Verificacion.esc_2.Name,0, 0, 1, Params.I.Null.Verificacion.esc_2.Tol(), ParamUnidad.A);

            TestCalibracionBase(defs,
            () =>
            {
                var vars = CVMMini.ReadAllVariables();

                var varsList = new List<double>() 
                { 
                    vars.Phases.L1.Corriente / 1000D, vars.Phases.L2.Corriente / 1000D, vars.Phases.L3.Corriente / 1000D
                };
                return varsList.ToArray();
            },
            () =>
            {
                var currentRef = tower.PowerSourceIII.ReadCurrent();

                var varsList = new List<double>()
                    {
                        currentRef.L1 * transformationRelation, currentRef.L2 * transformationRelation , currentRef.L3 * transformationRelation
                    };

                return varsList.ToArray();
            }, countSamples, maxSamples, timeInterval, "Error en la verificacion de la corriente de la Escala2 fuera de margenes"); 
        }

        public void TestAdjustGap(int delFirts = 2, int initCount = 3, int samples = 14, int timeInterval = 1100)
        {
            internalTestAdjustVerificationVoltageCurrent(EscalaEnum.GAP);

            var realVoltageCurrent = internalTestReadMTEVoltageCurrent(EscalaEnum.GAP);

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
          
            var power = TriLinePower.Create(realVoltageCurrent.Voltage, realVoltageCurrent.Current * transformationRelation, realVoltageCurrent.powerFactor);

            var offsetGains = CVMMini.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, power.KW, realVoltageCurrent.powerFactor,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                });

            triDefs.AddToResults(Resultado);
            
            if (!offsetGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de desfase fuera de margenes"));

            CVMMini.WriteITFCalibrationFactors(offsetGains.Item2);
        }

        public void TestVerificationGap(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            var defs = new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, Params.KW.Null.Verificacion.Tol(), ParamUnidad.W);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = CVMMini.ReadAllVariables();
                    var varsList = new List<double>() 
                    { 
                       vars.Phases.L1.PotenciaActiva, vars.Phases.L2.PotenciaActiva, vars.Phases.L3.PotenciaActiva,
                    };
                    return varsList.ToArray();
                },
                () =>
                {
                    var realVoltageCurrent = internalTestReadMTEVoltageCurrent(EscalaEnum.GAP);
                    var voltageRef = realVoltageCurrent.Voltage;
                    var currentRef = realVoltageCurrent.Current;

                    var power = TriLinePower.Create(voltageRef, currentRef * transformationRelation, realVoltageCurrent.powerFactor);
                    return power.KW.ToArray();

                }, countSamples, maxSamples, timeInterval, "Error en la verificación del desfase fuera de margenes");
        }

        public void TestVerificationHardware(int countSamples = 1, int maxSamples = 5, int timeInterval = 1100)
        {
            //var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent(EscalaEnum.THD);

            var frequency = CVMMini.ReadAllVariables().Trifasicas.Frecuencia / 10D;

            Resultado.Set(ConstantsParameters.Electrics.FREQ, frequency, ParamUnidad.Hz);

            var harmonicsAdjustValues = new List<TriAdjustValueDef>();
            harmonicsAdjustValues.Add(new TriAdjustValueDef(Params.THD_V.L1.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.V));
            harmonicsAdjustValues.Add(new TriAdjustValueDef(Params.THD_I.L1.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.A));
          
            TestMeasureBase(harmonicsAdjustValues, 
                (step) =>
                {
                    var harmonicsReadings = CVMMini.ReadAllVariables().THD;

                    return new double[] { harmonicsReadings.V1/10D , harmonicsReadings.V2/ 10D, harmonicsReadings.V2/ 10D,
                        harmonicsReadings.I1/ 10D, harmonicsReadings.I2/ 10D, harmonicsReadings.I3/ 10D };

                }, 1, 5, 1100);
        }

        public virtual void TestCustomization()
        {
            CVMMini.FlagTest();

            var setupCliente = GetSetupConfiguration(true);
            var setupDefault = GetSetupConfiguration(false);

            if (!setupCliente.Equals(setupDefault))
                CVMMini.WriteSetupConfiguration(setupCliente);

            CVMMini.WriteAlarmConfiguration(GetAlarmConfiguration("1"), CVMMINI.Alarms.ALARM_1);

            CVMMini.WriteAlarmConfiguration(GetAlarmConfiguration("2"), CVMMINI.Alarms.ALARM_2);

            CVMMini.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));

            CVMMini.WriteCommunicationConfig(GetCommunicationConfiguration());

            if (CVMMini.Modbus is Device.ModbusDeviceSerialPort)
            {
                var baudRate = (byte)Configuracion.GetDouble("COMUNICACIONES_03_VELOCIDAD", 3, ParamUnidad.SinUnidad);

                var modbus = CVMMini.Modbus as Device.ModbusDeviceSerialPort;

                if (modbus.PortCom == 7)
                {
                    logger.InfoFormat("Cambiamos la configuración de las comunicaciones del RS485 para Cliente");

                    modbus.BaudRate = CVMMini.baudrateDictionary[baudRate];

                    logger.InfoFormat("Cambiamos el BaudRate a {0}", modbus.BaudRate);

                    CVMMini.Modbus = modbus;
                }
            }

            this.InParellel(() =>
           {
               tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
           }, () =>
           {
               ResetPowerSource();
           });

            CVMMini.WriteClearAll();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, CVMMini.ReadSerialNumber().ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.PARAMETROS_NO_GRABADOS("Error en la memoria, el numero de serie no se ha grabado correctamente"), ParamUnidad.SinUnidad);

            Assert.IsTrue(CVMMini.ReadEnergies().AreZero(), Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error: las energias no estan a 0 despues del reset"));

            if (setupCliente.Equals(setupDefault))
                SaveResultsSetupConfiguration(CVMMini.ReadSetupConfig(), true);

            SaveResultsAlarmConfiguration(CVMMini.ReadAlarmConfig(CVMMINI.Alarms.ALARM_1), CVMMini.ReadAlarmConfig(CVMMINI.Alarms.ALARM_2));

            CVMMINI.EEPROMErrorCode eepromError = CVMMINI.EEPROMErrorCode.ALARM_1_NOT_INITIALIZED;

            SamplerWithCancel((p) =>
            {
                eepromError = CVMMini.ReadEEPromErrors();
                if (eepromError == CVMMINI.EEPROMErrorCode.NO_ERROR)
                    return true;

                TestException.Create().UUT.EEPROM.ALARMA(GetEEPROMErrorCodesTranslation(eepromError)).Throw();
                return false;

            }, "", 3, 1000, 1000, false, false);

            Resultado.Set("EEPROM ERROR CODE", (ushort)CVMMINI.EEPROMErrorCode.NO_ERROR, ParamUnidad.SinUnidad);
        }

        public void TestJohnson()
        {
            CVMMini.WriteCommunicationConfig(GetCommunicationConfiguration(true));

            ResetPowerSource(false);

            logger.InfoFormat("Cerramos puerto CVM-MNINI ");

            CVMMini.Modbus.ClosePort();

            logger.InfoFormat("Creamos nuevo SerialPort para Test Jhonson -> Puerto:{0}", Comunicaciones.SerialPort.ToString());

            sp = new SerialPort("COM" + Comunicaciones.SerialPort.ToString(), Comunicaciones.BaudRate, Parity.None, 8, StopBits.One);

            sp.Open();
            sp.NewLine = "\r";
            sp.ReadTimeout = 500;

            byte NUM_REINTENTOS = 10;

            SamplerWithCancel((s) =>
            {
                logger.InfoFormat("Test Comunicaciones Jhonson -> Reintento:{0}", s);

                if (s < NUM_REINTENTOS-1)
                {
                    logger.Info("Test Comunicaciones Jhonson -> TX: >01FA7");
                    sp.WriteLine(">01FA7");
                    var result = sp.ReadLine();
                    logger.InfoFormat("Test Comunicaciones Jhonson <- RX: {0}", result);
                    return (result == "A1061");
                }
                else
                {
                    logger.WarnFormat("Error Test Comunicaciones Jhonson -> Ceramos puerto");
                    sp.Close();
                    sp.Dispose();
                    return false;
                }
            }, "", NUM_REINTENTOS, 500, 500, false, true,
            (p)=>
            {
                Error().UUT.COMUNICACIONES.TIME_OUT("TEST_JHONSON").Throw();
            });

            Resultado.Set("COMUNICACION_JOHNSON", "OK", ParamUnidad.SinUnidad);

            logger.InfoFormat("Test Comunicaciones Jhonson OK -> Ceramos puerto");
            sp.Close();
            sp.Dispose();
        }

        public virtual void TestFinish()
        {
            if (CVMMini != null)
                CVMMini.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(1000,OUT_ETH_PISTON,OUT_RS485_PISTON);

                    tower.IO.DO.OffWait(1000, OUT_EV_COM);

                    tower.IO.DO.Off(OUT_COM_RS485_POSITION);

                    SamplerWithCancelWhitOutCancelToken(() => { return tower.IO.DI[IN_COM_PISTON_ETH] && !tower.IO.DI[IN_COM_PISTON_RS485]; }, "Error: No se ha detectado el piston de seleccion de COM en posicion RS485",
                    40, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

                    tower.IO.DO.OffWait(1000, OUT_TTL_PISTON, OUT_TERMINALS_PISTON);

                    tower.IO.DO.OffWait(1000, OUT_ATTACHMENT_PISTON);

                    tower.IO.DO.OffWait(1000, OUT_MAIN_EV);

                }

                tower.Dispose();
            }

            if (IsTestError)
                Tower.IO.DO.On(OUT_PILOTO_ERROR);
        }

        public void GuardarTramas()
        {
            CVMMini.SetPort(1, 1, 9600);

            Logger.DebugFormat("Trama Setup CLIENTE");
            Logger.DebugFormat(Comunications.Utility.Extensions.ToBytes(GetSetupConfiguration(true)).BytesToHexString());

            Logger.DebugFormat("Trama Maxima Demanda");
            Logger.DebugFormat(Comunications.Utility.Extensions.ToBytes(GetMaximumDemandConfiguration()).BytesToHexString());

            Logger.DebugFormat("Trama Vector Hardware");
            try
            {
                CVMMini.WriteHardwareVector(GetHardwareVectorConfiguration());
            }
            catch { }       

            Logger.DebugFormat("Trama Password");
            Logger.DebugFormat(Comunications.Utility.Extensions.ToBytes(GetPasswordConfiguration()).BytesToHexString());

            Logger.DebugFormat("Trama Alarma1");
            Logger.DebugFormat(Comunications.Utility.Extensions.ToBytes(GetAlarmConfiguration("1")).BytesToHexString());

            Logger.DebugFormat("Trama Alarma2");
            Logger.DebugFormat(Comunications.Utility.Extensions.ToBytes(GetAlarmConfiguration("2")).BytesToHexString());

            CVMMini.Dispose();
        }

        #region Metodos privados

        public virtual void ResetPowerSource(bool test = true)
        {
            var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);
            var voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.EnVacio.Vnom.Name, 0, ParamUnidad.A);

            Delay(1000, "Esperando finalizacion de tareas del equipo");

            if (typeSupply == "AC")
            {
                tower.Chroma.ApplyOff();
                Delay(1000, "Esperando apagado del equipo");
                if (voltage > 300)
                {
                    voltage /= 2;
                    tower.IO.DO.On(13);
                }
                tower.Chroma.ApplyPresetsAndWaitStabilisation(voltage, 0, 50);
            }
            else
            {
                tower.LAMBDA.ApplyOff();
                Delay(1500, "Esperando apagado del equipo");
                tower.IO.DO.On(23);
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltage, current);
            }

            if (test == true)
                SamplerWithCancel((p) => { CVMMini.FlagTest(); return true; }, "", 5, 500, 100, false, true, (p) =>
                {
                    Error().UUT.COMUNICACIONES.FLAG_TEST("RESET HARDWARE");
                });
        }

        public enum EscalaEnum
        {
            ESC1,
            ESC2,
            GAP,
        }

        private struct VoltageCurrentReading
        {
            public TriLineValue Voltage { get; set; }
            public TriLineValue Current { get; set; }
            public double powerFactor { get; set; }
        };

        private VoltageCurrentReading internalTestAdjustVerificationVoltageCurrent(EscalaEnum escala)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint(escala.ToString()).Name, 300, ParamUnidad.V);
            var adjustCurrent = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint(escala.ToString()).Name, 5, ParamUnidad.A),
                L2 = Consignas.GetDouble(Params.I.L2.Ajuste.TestPoint(escala.ToString()).Name, 5, ParamUnidad.A),
                L3 = Consignas.GetDouble(Params.I.L3.Ajuste.TestPoint(escala.ToString()).Name, 5, ParamUnidad.A),
            };

            var adjustCurrentOffsets = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Offset.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
                L2 = Consignas.GetDouble(Params.I.L2.Offset.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
                L3 = Consignas.GetDouble(Params.I.L3.Offset.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
            };

            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint(escala.ToString()).Name, 0, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent + adjustCurrentOffsets, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var currentReadings = tower.PowerSourceIII.ReadCurrent();
            var voltageReadings = tower.PowerSourceIII.ReadVoltage();

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(currentReadings.L1, currentReadings.L2, currentReadings.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        private VoltageCurrentReading internalTestReadMTEVoltageCurrent(EscalaEnum escala)
        {
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint(escala.ToString()).Name, 0, ParamUnidad.Grados);

            var adjustCurrentOffsets = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Offset.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
                L2 = Consignas.GetDouble(Params.I.L2.Offset.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
                L3 = Consignas.GetDouble(Params.I.L3.Offset.TestPoint(escala.ToString()).Name, 0, ParamUnidad.A),
            };

            var currentReadings = tower.PowerSourceIII.ReadCurrent() - adjustCurrentOffsets;
            var voltageReadings = tower.PowerSourceIII.ReadVoltage();

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(currentReadings.L1, currentReadings.L2, currentReadings.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        //***************************************************************

        private CVMMINI.SetupConfig GetSetupConfiguration(bool cliente = false)
        {
            var setupConfig = new CVMMINI.SetupConfig();

            string clienteString = cliente == true ? "_CLIENTE" : "";

            setupConfig.VoltagePrimary = (int)Configuracion.GetDouble("SETUP_00_PRIMARIO_TENSION", 1, ParamUnidad.V);
            setupConfig.VoltageSecondary = (byte)Configuracion.GetDouble("SETUP_01_SECUNDARIO_TENSION", 1, ParamUnidad.V);
            setupConfig.CurrentPrimary = (ushort)Configuracion.GetDouble("SETUP_02_PRIMARIO_CORRIENTE" + clienteString, 5, ParamUnidad.A); //Solo este tiene cliente
            setupConfig.DefaultPage = (byte)Configuracion.GetDouble("SETUP_03_PAGINA_DEFECTO", 0, ParamUnidad.SinUnidad);
            setupConfig.VoltageSimpleOrCompound = (byte)(Configuracion.GetString("SETUP_04_TENSIONES_SIMPLES", "SI", ParamUnidad.SinUnidad) == "SI" ? 0 : 1);
            setupConfig.BacklightTime = (byte)Configuracion.GetDouble("SETUP_05_TIEMPO_BACKLIGHT", 10, ParamUnidad.s);
            setupConfig.HarmonicsCalculus = (byte)(Configuracion.GetString("SETUP_06_CALCULO_HARMONICOS", "THD", ParamUnidad.SinUnidad) == "THD" ? 0 : 1);

            return setupConfig;
        }

        private void SaveResultsSetupConfiguration(CVMMINI.SetupConfig SetupConfiguration, bool cliente = false)
        {
            var SetupConfigurationBBDD = GetSetupConfiguration(cliente);

            Assert.IsTrue(SetupConfigurationBBDD.Equals(SetupConfiguration), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion de setup leida no es la misma que la de BBDD"));

            string clienteString = cliente == true ? "_CLIENTE" : "";

            Resultado.Set("TENSION_PRIMARIO" + clienteString, SetupConfiguration.VoltagePrimary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TENSION_SECUNDARIO"+ clienteString, SetupConfiguration.VoltageSecondary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CORRIENTE_PRIMARIO"+ clienteString, SetupConfiguration.CurrentPrimary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TIPO_TENSIONES"+ clienteString, SetupConfiguration.VoltageSimpleorCompoundString, ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_PRINCIPAL_DEFECTO"+ clienteString, SetupConfiguration.DefaultPage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CALCULO_HARMONICOS_SETUP_DEFECTO"+ clienteString, SetupConfiguration.HarmonicsCalculusString, ParamUnidad.SinUnidad);
            Resultado.Set("TIEMPO_DESCONEXION_BACKLIGHT"+ clienteString, SetupConfiguration.BacklightTime.ToString(), ParamUnidad.SinUnidad);
        }

        private CVMMINI.MaximumDemandConfiguration GetMaximumDemandConfiguration()
        {
            var maximumDemandConfig = new CVMMINI.MaximumDemandConfiguration();

            maximumDemandConfig.RegisterTime = (ushort)Configuracion.GetDouble("MAXIMA_DEMANDA_01_TIEMPO_REGISTRO", 15, ParamUnidad.SinUnidad);
            maximumDemandConfig.ValueToCalculate = Configuracion.GetString("MAXIMA_DEMANDA_00_VARIABLE_CALCULO", "NO_PD", ParamUnidad.SinUnidad);

            return maximumDemandConfig;
        }

        private void SaveResultsMaximumDemandConfiguration(CVMMINI.MaximumDemandConfiguration MaximumDemandConfiguration)
        {
            var MaximumDemandConfigurationBBDD = GetMaximumDemandConfiguration();

            Assert.IsTrue(MaximumDemandConfigurationBBDD.Equals(MaximumDemandConfiguration), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion de maxima demanda leida no es la misma que la de BBDD"));

            Resultado.Set("TIEMPO_REGISTRO_MAXIMA_DEMANDA", MaximumDemandConfiguration.RegisterTime.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("VARIABLE_MAXIMA_DEMANDA", MaximumDemandConfiguration.ValueToCalculate, ParamUnidad.SinUnidad);
        }

        protected CVMMINI.HardwareVector GetHardwareVectorConfiguration()
        {
            var hardwareVector = new CVMMINI.HardwareVector();
            hardwareVector.Comunication = VectorHardware.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            hardwareVector.Rs232 = VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele1 = VectorHardware.GetString("RELE1", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele2 = VectorHardware.GetString("RELE2", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "5A", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "300V", ParamUnidad.SinUnidad);
            hardwareVector.Johnson = VectorHardware.GetString("JOHNSON", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.CalculoHarmonicos = VectorHardware.GetString("CALCULO_HARMONICOS", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Cuadrantes = VectorHardware.GetString("CUADRANTES", "2_CUADRANTES", ParamUnidad.SinUnidad).ToUpper() == "2_CUADRANTES" ? true : false;
            hardwareVector._1Fase = VectorHardware.GetString("NUMERO_FASES", "3", ParamUnidad.SinUnidad).ToUpper() == "1" ? true : false;
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "CVM_MINI", ParamUnidad.SinUnidad);

            SetVariable("VectorHardware", hardwareVector);

            return hardwareVector;
        }

        private void SaveResultsHardwareVectorConfiguration(CVMMINI.HardwareVector HardwareVector)
        {
            var HardwareVectorBBDD = GetHardwareVectorConfiguration();

            Assert.AreEqual(HardwareVector.VectorHardwareBoolean, HardwareVectorBBDD.VectorHardwareBoolean, Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion de vector de hardware leida no es la misma que la de BBDD"));

            Resultado.Set("COMUNICACIONES", HardwareVector.ComunicationString, ParamUnidad.SinUnidad);
            Resultado.Set("RS232", HardwareVector.Rs232String, ParamUnidad.SinUnidad);
            Resultado.Set("SHUNT", HardwareVector.ShuntString, ParamUnidad.SinUnidad);
            Resultado.Set("RELE1", HardwareVector.Rele1String, ParamUnidad.SinUnidad);
            Resultado.Set("RELE2", HardwareVector.Rele2String, ParamUnidad.SinUnidad);
            Resultado.Set("ALIMENTACION", HardwareVector.PowerSupply.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_CORRIENTE", HardwareVector.InputCurrent.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_TENSION", HardwareVector.InputVoltage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("MODELO", HardwareVector.Modelo.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CUADRANTES", HardwareVector.CuadrantesString, ParamUnidad.SinUnidad);
            Resultado.Set("HARMONICOS", HardwareVector.CalculoHarmonicosString, ParamUnidad.SinUnidad);
            Resultado.Set("NUMERO_FASES", HardwareVector._1FaseString, ParamUnidad.SinUnidad);
            Resultado.Set("JONHSON", HardwareVector.JohnsonString, ParamUnidad.SinUnidad);

            Resultado.Set(ConstantsParameters.Identification.VECTOR_HARDWARE, HardwareVector.VectorHardwareTrama, ParamUnidad.SinUnidad);
        }

        private CVMMINI.MeasureGains GetCalibrationFactors()
        {
            var currentFactor = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO", 27400, ParamUnidad.Puntos);
            var currentScale2Factor = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO_ESCALA_2", 27400, ParamUnidad.Puntos);
            var voltageFactor = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 11250, ParamUnidad.Puntos);

            return new CVMMINI.MeasureGains(currentFactor, currentScale2Factor, voltageFactor);
        }

        private CVMMINI.PhaseITFGains GetITFCalibrationFactors()
        {
            var phaseFactor = (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_DESFASE + "_DEFECTO", 0, ParamUnidad.Puntos);

            return new CVMMINI.PhaseITFGains(phaseFactor);
        }

        private CVMMINI.PasswordConfig GetPasswordConfiguration()
        {
            var passwordConfiguration = new CVMMINI.PasswordConfig();

            passwordConfiguration.PasswordNumber = Convert.ToUInt16(Configuracion.GetString("NUMERO_PASSWORD", "1234", ParamUnidad.SinUnidad));
            passwordConfiguration.SetupLocking = Convert.ToByte(Configuracion.GetString("BLOQUEO_SETUP", "0", ParamUnidad.SinUnidad));

            return passwordConfiguration;
        }

        private void SaveResultsPasswordConfiguration(CVMMINI.PasswordConfig passwordConfiguration)
        {
            Resultado.Set("NUMERO_PASSWORD", passwordConfiguration.PasswordNumber.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("BLOQUEO_SETUP", passwordConfiguration.SetupLocking.ToString(), ParamUnidad.SinUnidad);
        }

        private CVMMINI.ScreenConfiguration GetScreenConfiguration()
        {
            var screenConfiguration = new CVMMINI.ScreenConfiguration();

            screenConfiguration.VoltagePhaseNeuter= Configuracion.GetString("PANTALLA_00_TENSION_NEUTRO", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.VoltagePhasePhase = Configuracion.GetString("PANTALLA_01_TENSION_LINEA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.Current = Configuracion.GetString("PANTALLA_02_CORRIENTE", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ActivePower = Configuracion.GetString("PANTALLA_03_POTENCIA_ACTIVA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ReactivePower = Configuracion.GetString("PANTALLA_04_POTENCIA_REACTIVA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ApparentPower = Configuracion.GetString("PANTALLA_05_POTENCIA_APARENTE", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.PowerFactor = Configuracion.GetString("PANTALLA_06_FACTOR_POTENCIA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.VoltageTHD = Configuracion.GetString("PANTALLA_07_TENSION_THD", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.CurrentTHD = Configuracion.GetString("PANTALLA_08_THD", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.TriPhasePower = Configuracion.GetString("PANTALLA_09_POTENCIA_ACTIVA_TRIFASICA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.TriPhaseInductivePower = Configuracion.GetString("PANTALLA_10_POTENCIA_TRIFASICA_INDUCTIVA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.TriPhaseCapacitivePower = Configuracion.GetString("PANTALLA_11_POTENCIA_TRIFASICA_CAPACITIVA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.TriPhaseApparentPower = Configuracion.GetString("PANTALLA_12_POTENCIA_TRIFASICA_APARENTE", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.NeuterCurrentFrequencyTemperature = Configuracion.GetString("PANTALLA_13_I_NEUTRO_FREC_TEMP", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.Pd = Configuracion.GetString("PANTALLA_14_PD", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ActivePowerPerHourConsumed = Configuracion.GetString("PANTALLA_15_POTENCIA_ACTIVA_CONSUMIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.InductivePowerPerHourConsumed = Configuracion.GetString("PANTALLA_16_POTENCIA_INDUCTIVA_CONSUMIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.CapacitivePowerPerHourConsumed = Configuracion.GetString("PANTALLA_17_POTENCIA_CAPACITIVA_CONSUMIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ApparentPowerPerHourConsumed = Configuracion.GetString("PANTALLA_18_POTENCIA_APARENTE_CONSUMIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ActivePowerPerHourProduced = Configuracion.GetString("PANTALLA_19_POTENCIA_ACTIVA_PRODUCIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.InductivePowerPerHourProduced = Configuracion.GetString("PANTALLA_20_POTENCIA_INDUCTIVA_PRODUCIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.CapacitivePowerPerHourProduced = Configuracion.GetString("PANTALLA_21_POTENCIA_CAPACITIVA_PRODUCIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.ApparentPowerPerHourProduced = Configuracion.GetString("PANTALLA_22_POTENCIA_APARENTE_PRODUCIDA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.VoltageHarmonics = Configuracion.GetString("PANTALLA_23_HARMONICOS_TENSION", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";
            screenConfiguration.CurrentHarmonics = Configuracion.GetString("PANTALLA_24_HARMONICOS_CORRIENTE", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI";

            return screenConfiguration;
        }

        private void SaveResultsScreenConfiguration(CVMMINI.ScreenConfiguration screenConfiguration)
        {
            var screenConfigurationDDBB = GetScreenConfiguration();

            Assert.IsTrue(screenConfigurationDDBB.Equals(screenConfiguration), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion pantalla leida no es la misma que la de BBDD"));

            Resultado.Set("PANTALLA_00_TENSION_NEUTRO", screenConfiguration.VoltagePhaseNeuter.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_01_TENSION_LINEA", screenConfiguration.VoltagePhasePhase.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_02_CORRIENTE", screenConfiguration.Current.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_03_POTENCIA_ACTIVA", screenConfiguration.ActivePower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_04_POTENCIA_REACTIVA", screenConfiguration.ReactivePower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_05_POTENCIA_APARENTE", screenConfiguration.ApparentPower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_06_FACTOR_POTENCIA", screenConfiguration.PowerFactor.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_07_TENSION_THD", screenConfiguration.VoltageTHD.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_08_THD", screenConfiguration.CurrentTHD.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_09_POTENCIA_ACTIVA_TRIFASICA", screenConfiguration.TriPhasePower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_10_POTENCIA_TRIFASICA_INDUCTIVA", screenConfiguration.TriPhaseInductivePower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_11_POTENCIA_TRIFASICA_CAPACITIVA", screenConfiguration.TriPhaseCapacitivePower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_12_POTENCIA_TRIFASICA_APARENTE", screenConfiguration.TriPhaseApparentPower.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_13_CORRIENTE_NEUTRO_FRECUENCIA_TEMPERATURA", screenConfiguration.NeuterCurrentFrequencyTemperature.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_14_PD", screenConfiguration.Pd.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_15_POTENCIA_ACTIVA_CONSUMIDA", screenConfiguration.ActivePowerPerHourConsumed.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_16_POTENCIA_INDUCTIVA_CONSUMIDA", screenConfiguration.InductivePowerPerHourConsumed.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_17_POTENCIA_CAPACITIVA_CONSUMIDA", screenConfiguration.CapacitivePowerPerHourConsumed.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_18_POTENCIA_APARENTE_CONSUMIDA", screenConfiguration.ApparentPowerPerHourConsumed.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_19_POTENCIA_ACTIVA_PRODUCIDA", screenConfiguration.ActivePowerPerHourProduced.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_20_POTENCIA_INDUCTIVA_PRODUCIDA", screenConfiguration.InductivePowerPerHourProduced.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_21_POTENCIA_CAPACITIVA_PRODUCIDA", screenConfiguration.CapacitivePowerPerHourProduced.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_22_POTENCIA_APARENTE_PRODUCIDA", screenConfiguration.ApparentPowerPerHourProduced.ToString(), ParamUnidad.SinUnidad);
        }

        private CVMMINI.AlarmConfig GetAlarmConfiguration(string alarma)
        {
            var alarmConfiguration = new CVMMINI.AlarmConfig();

            alarmConfiguration.MaxValue = (int)Configuracion.GetDouble("ALARMAS_" + alarma + "_00_VALOR_MAXIMO", 0, ParamUnidad.SinUnidad);
            alarmConfiguration.MinValue = (int)Configuracion.GetDouble("ALARMAS_" + alarma + "_01_VALOR_MINIMO", 0, ParamUnidad.SinUnidad);
            alarmConfiguration.Delay = (ushort)Configuracion.GetDouble("ALARMAS_" + alarma + "_02_RETRASO", 0, ParamUnidad.SinUnidad);
            alarmConfiguration.VariableNumber = (byte)Configuracion.GetDouble("ALARMAS_" + alarma + "_03_VARIABLE", 0, ParamUnidad.SinUnidad);

            return alarmConfiguration;
        }

        private void SaveResultsAlarmConfiguration(CVMMINI.AlarmConfig alarmConfiguration1, CVMMINI.AlarmConfig alarmConfiguration2)
        {
            var alarmConfiguration1BBDD = GetAlarmConfiguration("1");
            var alarmConfiguration2BBDD = GetAlarmConfiguration("2");

            Assert.IsTrue(alarmConfiguration1BBDD.Equals(alarmConfiguration1), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion alarma 1 leida no es la misma que la de BBDD"));
            Assert.IsTrue(alarmConfiguration2BBDD.Equals(alarmConfiguration2), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion alarma 1 leida no es la misma que la de BBDD"));

            Resultado.Set("VALOR_MAXIMO_ALARMA_1", alarmConfiguration1.MaxValue.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("VALOR_MINIMO_ALARMA_1", alarmConfiguration1.MinValue.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("RETRASO_ALARMA_1", alarmConfiguration1.Delay.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("VARIABLE_ALARMA_1", alarmConfiguration1.VariableNumber.ToString(), ParamUnidad.SinUnidad);

            Resultado.Set("VALOR_MAXIMO_ALARMA_2", alarmConfiguration2.MaxValue.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("VALOR_MINIMO_ALARMA_2", alarmConfiguration2.MinValue.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("RETRASO_ALARMA_2", alarmConfiguration2.Delay.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("VARIABLE_ALARMA_2", alarmConfiguration2.VariableNumber.ToString(), ParamUnidad.SinUnidad);
        }

        private CVMMINI.Comunicaciones GetCommunicationConfiguration(bool cliente = false)
        {
            var comunicaciones = new CVMMINI.Comunicaciones();
            var Cliente = cliente == true ? "_CLIENTE" : "";

            comunicaciones.Periferico = (byte)Configuracion.GetDouble("COMUNICACIONES_00_PERIFERICO", 1, ParamUnidad.SinUnidad);
            comunicaciones.Protocolo = (byte)Configuracion.GetDouble("COMUNICACIONES_01_PROTOCOLO" + Cliente, 0, ParamUnidad.SinUnidad);
            comunicaciones.Paridad = (byte)Configuracion.GetDouble("COMUNICACIONES_02_PARIDAD", 0, ParamUnidad.SinUnidad);
            comunicaciones.Velocidad = (byte)Configuracion.GetDouble("COMUNICACIONES_03_VELOCIDAD", 3, ParamUnidad.SinUnidad);
            comunicaciones.Stop = (byte)Configuracion.GetDouble("COMUNICACIONES_04_STOP", 0, ParamUnidad.SinUnidad);
            comunicaciones.Longitud = (byte)Configuracion.GetDouble("COMUNICACIONES_05_LONGITUD_DATOS", 8, ParamUnidad.SinUnidad);

            Resultado.Set("COMUNICACIONES_00_PERIFERICO", comunicaciones.Periferico.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("COMUNICACIONES_01_PROTOCOLO" + Cliente, comunicaciones.Protocolo.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("COMUNICACIONES_02_PARIDAD", comunicaciones.Paridad.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("COMUNICACIONES_03_VELOCIDAD", comunicaciones.Velocidad.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("COMUNICACIONES_04_STOP", comunicaciones.Stop.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("COMUNICACIONES_05_LONGITUD_DATOS", comunicaciones.Longitud.ToString(), ParamUnidad.SinUnidad);

            return comunicaciones;
        }

        private string GetEEPROMErrorCodesTranslation(CVMMINI.EEPROMErrorCode errorCodes)
        {
            var errorList = new List<string>();

            foreach (CVMMINI.EEPROMErrorCode item in Enum.GetValues(typeof(CVMMINI.EEPROMErrorCode)))
                if ((errorCodes.HasFlag(item)) && (item != CVMMINI.EEPROMErrorCode.NO_ERROR))
                    errorList.Add(CVMMini.eepromDictionary[item]);

            return string.Join(", ", errorList.ToArray());
        }

        #endregion

        #region Diccionarios salidas util

        private Dictionary<CVMMINI.Outputs, int> deviceOutputInputInformation = new Dictionary<CVMMINI.Outputs, int>()
        {
            {CVMMINI.Outputs.OUTPUT_1, 15},
            {CVMMINI.Outputs.OUTPUT_2, 16}
        };

        #endregion
    }
}

