﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Auxiliars;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(GE15Description))]
    public partial class UC_GE15 : UserControl, IDisposable
    {
        private static readonly string TAG = "GE15";

        public int numGraphs = 0;

        private SynchronizationContext sc;

        private GE15 mte;

        public Type InstrumentType
        {
            get
            {
                return typeof(GE15);
            }
        }

        public GE15 Instrument
        {
            get
            {
                if (mte == null)
                    mte = new GE15();

                return mte;
            }
            set
            {
                if (mte != null)
                {
                    mte.Dispose();
                    mte = null;
                }

                mte = value;
            }
        }

        public UC_GE15()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;

            try
            {
                var ports = new MyComputer().SerialPortNames.ToArray();
                if (ports.Length == 0)
                {
                    errorProvider1.SetError(cmb_Port, "No se ha encontrado puertos en el PC");
                    return;
                }

                cmb_Port.Items.AddRange(ports);
                cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM1");
                if (cmb_Port.SelectedItem == null)
                    cmb_Port.SelectedIndex = 0;
            }
            catch (Exception)
            {
                errorProvider1.SetError(cmb_Port, "No se ha encontrado la MTE");
            }

        }

        protected override void Dispose(bool disposing)
        {
            numGraphs = 0;

            if (disposing && (components != null))
                components.Dispose();

            if (Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Instrument.SelectiBurder(GE15.Burden.IEC50);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Instrument.SetPort(cmb_Port.SelectedText);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Instrument.SelectiBurder(GE15.Burden.IEC50);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }
    }

    public class GE15Description : ControlDescription
    {
        public override string Name { get { return "GE15"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Flash_Updater_icon; } }
        public override Image IconBig { get { return Properties.Resources.ZERA_MT; } }
        public override Type SubcomponentType { get { return typeof(GE15); } }
    }
}
