﻿using System.Collections.Generic;

namespace AssemblyResolve
{
    public static  class SendMail
    {
        public static void EnviarCorreo(string titulo, string messageBody)
        {
            var destinatarios = new List<string>()
            {
                "flsanchez@dezac.com",
                "imartinez@dezac.com",
                "cjordan@dezac.com",
            };

            EnviarCorreo(titulo, messageBody, destinatarios, null);
        }

        public static void EnviarCorreo(string titulo, string messageBody, List<string> destinatarios, List<string> copia)
        {
            /*-------------------------MENSAJE DE CORREO----------------------*/
            //Creamos un nuevo Objeto de mensaje
            System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

            //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario
            //Asunto
            mmsg.Subject = titulo;
            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

            //Direccion de correo electronico a la que queremos enviar el mensaje
            foreach (string destiny in destinatarios)
            {
                mmsg.To.Add(destiny);
            }

            //Direccion de correo electronico que queremos que reciba una copia del mensaje
            if (copia != null)
            {
                foreach (string copy in copia)
                {
                    mmsg.Bcc.Add(copy);
                }
            }

            //Cuerpo del Mensaje
            mmsg.Body = messageBody;
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = false; //Si no queremos que se envíe como HTML

            //Correo electronico desde la que enviamos el mensaje
            mmsg.From = new System.Net.Mail.MailAddress("tsd@dezac.com");

            /*-------------------------CLIENTE DE CORREO----------------------*/
            //Creamos un objeto de cliente de correo
            System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

            //Hay que crear las credenciales del correo emisor
            cliente.Credentials =
                new System.Net.NetworkCredential("tsd@dezac.com", "D4rkF4com");

            //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
            /*
            cliente.Port = 587;
            cliente.EnableSsl = true;
            */
            cliente.Host = "mail.dezac.com"; //Para Gmail "smtp.gmail.com";

            /*-------------------------ENVIO DE CORREO----------------------*/
            try
            {
                //Enviamos el mensaje      
                cliente.Send(mmsg);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                //Aquí gestionamos los errores al intentar enviar el correo
            }
        }
    }
}
