﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(CvmB150Description))]
    public partial class UC_CVMB150 : UserControl, IDisposable
    {
        private static readonly string TAG = "CVMB150";

        private CVMB100Tower cvm;
        private MyComputer computer = new MyComputer();
        private Vars dataVars = new Vars();

        private SynchronizationContext sc;

        private bool timerEnabled;
        private bool modeRead;

        public class Vars
        {
            public double TensionL1 { get; set; }
            public double CorrienteL1 { get; set; }
            public double PotenciaActivaL1 { get; set; }
            public double PotenciaReactivaInductivaL1 { get; set; }
            public double PotenciaReactivaCapicitivaL1 { get; set; }
            public double PotenciaAparenteL1 { get; set; }
            public double FactorPotenciaL1 { get; set; }
            public double CosenoPhiL1 { get; set; }

            public double TensionL2 { get; set; }
            public double CorrienteL2 { get; set; }
            public double PotenciaActivaL2 { get; set; }
            public double PotenciaReactivaInductivaL2 { get; set; }
            public double PotenciaReactivaCapicitivaL2 { get; set; }
            public double PotenciaAparenteL2 { get; set; }
            public double FactorPotenciaL2 { get; set; }
            public double CosenoPhiL2 { get; set; }

            public double TensionL3 { get; set; }
            public double CorrienteL3 { get; set; }
            public double PotenciaActivaL3 { get; set; }
            public double PotenciaReactivaInductivaL3 { get; set; }
            public double PotenciaReactivaCapicitivaL3 { get; set; }
            public double PotenciaAparenteL3 { get; set; }
            public double FactorPotenciaL3 { get; set; }
            public double CosenoPhiL3 { get; set; }

            public double TensionNeutro { get; set; }
            public double CorrienteNeutro { get; set; }
            public double Frecuencia { get; set; }
            public double TensionLineaL1L2 { get; set; }
            public double TensionLineaL2L3 { get; set; }
            public double TensionLineaL3L1 { get; set; }
            public double TensionLineaIII { get; set; }
            public double TensionFaseIII { get; set; }
            public double CorrienteIII { get; set; }
            public double PotenciaActivaIII { get; set; }
            public double PotenciaInductivaIII { get; set; }
            public double PotenciaCapacitivaIII { get; set; }
            public double PotenciaAparenteIII { get; set; }
            public double FacttorPotenciaIII { get; set; }
            public double CosenoPhiIII { get; set; }
        }

        public Type InstrumentType
        {
            get
            {
                return typeof(CVMB100Tower);
            }
        }

        public CVMB100Tower Instrument
        {
            get
            {
                if (cvm == null)
                    cvm = new CVMB100Tower();

                return cvm;
            }
            set
            {
                if (cvm != null)
                {
                    cvm.Dispose();
                    cvm = null;
                }
                cvm = value;
            }
        }

        public UC_CVMB150()
        {
            InitializeComponent();            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;

            Instrument.GetDeviceVersion();

            cmb_Port.Items.AddRange(computer.SerialPortNames.ToArray());
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM8");

            String[] Periferic = new String[] { "1", "2", "3", "4", "5","6", "7","8" };
            cmb_Periferic.Items.AddRange(Periferic);
            cmb_Periferic.SelectedIndex = cmb_Periferic.FindStringExact("5");
            comboBoxBaudRate.SelectedIndex = comboBoxBaudRate.FindStringExact("19200");

            Instrument.SetPort("8",1, 19200);

            bsAllVars.DataSource = dataVars;

            SetMode(false);
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Selecciona un Puerto Serie");
            else if (cmb_Periferic.SelectedItem == null)            
                errorProvider1.SetError(cmb_Periferic, "Selecciona un Numero de Periferico");
            else
            {
                Instrument.Modbus.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                Instrument.Modbus.PerifericNumber = Convert.ToByte(cmb_Periferic.SelectedItem.ToString());
                Instrument.Modbus.BaudRate = Convert.ToInt32(comboBoxBaudRate.SelectedItem.ToString());
                ToggleButtonsState((Button)sender);
                timer.Interval = Convert.ToInt32(txtInterval.Text);
                ReadAllVariables();
                ToggleButtonsState((Button)sender);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ToggleButtonsState((Button)sender);
            StartTask("Reseteando");
            try
            {
                Instrument.Reset();
                LoadDataVar(new CVMB100Tower.AllVariables());
                EndTask();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        private void LoadDataVar(CVMB100Tower.AllVariables variables)
        {
            dataVars.TensionL1 = variables.Fases.L1.Tension;
            dataVars.CorrienteL1 = variables.Fases.L1.Corriente;
            dataVars.CosenoPhiL1 = variables.Fases.L1.CosenoPhi;
            dataVars.FactorPotenciaL1 = variables.Fases.L1.FactorPotencia;
            dataVars.PotenciaActivaL1 = variables.Fases.L1.PotenciaActiva;
            dataVars.PotenciaAparenteL1 = variables.Fases.L1.PotenciaAparente;
            dataVars.PotenciaReactivaCapicitivaL1 = variables.Fases.L1.PotenciaReactivaCapicitiva;
            dataVars.PotenciaReactivaInductivaL1 = variables.Fases.L1.PotenciaReactivaInductiva;

            dataVars.TensionL2 = variables.Fases.L2.Tension;
            dataVars.CorrienteL2 = variables.Fases.L2.Corriente;
            dataVars.CosenoPhiL2 = variables.Fases.L2.CosenoPhi;
            dataVars.FactorPotenciaL2 = variables.Fases.L2.FactorPotencia;
            dataVars.PotenciaActivaL2 = variables.Fases.L2.PotenciaActiva;
            dataVars.PotenciaAparenteL2 = variables.Fases.L2.PotenciaAparente;
            dataVars.PotenciaReactivaCapicitivaL2 = variables.Fases.L2.PotenciaReactivaCapicitiva;
            dataVars.PotenciaReactivaInductivaL2 = variables.Fases.L2.PotenciaReactivaInductiva;

            dataVars.TensionL3 = variables.Fases.L3.Tension;
            dataVars.CorrienteL3 = variables.Fases.L3.Corriente;
            dataVars.CosenoPhiL3 = variables.Fases.L3.CosenoPhi;
            dataVars.FactorPotenciaL3 = variables.Fases.L3.FactorPotencia;
            dataVars.PotenciaActivaL3 = variables.Fases.L3.PotenciaActiva;
            dataVars.PotenciaAparenteL3 = variables.Fases.L3.PotenciaAparente;
            dataVars.PotenciaReactivaCapicitivaL3 = variables.Fases.L3.PotenciaReactivaCapicitiva;
            dataVars.PotenciaReactivaInductivaL3 = variables.Fases.L3.PotenciaReactivaInductiva;

            dataVars.TensionFaseIII = variables.Trifasicas.TensionFaseIII;
            dataVars.TensionLineaIII = variables.Trifasicas.TensionLineaIII;
            dataVars.TensionLineaL1L2 = variables.Trifasicas.TensionLineaL1L2;
            dataVars.TensionLineaL2L3 = variables.Trifasicas.TensionLineaL2L3;
            dataVars.TensionLineaL3L1 = variables.Trifasicas.TensionLineaL3L1;
            dataVars.TensionNeutro = variables.Trifasicas.TensionNeutro;

            dataVars.CorrienteIII = variables.Trifasicas.CorrienteIII;
            dataVars.CorrienteNeutro = variables.Trifasicas.CorrienteNeutro;
            dataVars.CosenoPhiIII = variables.Trifasicas.CosenoPhiIII;
            dataVars.FacttorPotenciaIII = variables.Trifasicas.FacttorPotenciaIII;
            dataVars.Frecuencia = variables.Trifasicas.Frecuencia;
            dataVars.PotenciaActivaIII = variables.Trifasicas.PotenciaActivaIII;
            dataVars.PotenciaAparenteIII = variables.Trifasicas.PotenciaAparenteIII;
            dataVars.PotenciaCapacitivaIII = variables.Trifasicas.PotenciaCapacitivaIII;
            dataVars.PotenciaInductivaIII = variables.Trifasicas.PotenciaInductivaIII;

            bsAllVars.ResetBindings(false);
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private async void ReadAllVariables()
        {
            try
            {
                StartTask("Lectura de las variables instantaneas");
                var data = await Instrument.Modbus.ReadAsync<CVMB100Tower.AllVariables>();
                LoadDataVar(data);
                EndTask();
                timer.Enabled = checkBox1.Checked;
            }
            catch (Exception ex)
            {
                Logger.Error("Error al pedir variables instantaneas al CVM-B150", ex);
            }
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;
            ReadAllVariables();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            checkBox1.BackColor = checkBox1.Checked ? Color.DarkGreen : Color.DimGray;
        }

        private void OnChangePeriferic(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;           

            //if (Convert.ToString(cb.SelectedItem).Equals("LOW"))
            //    Instrument = Tower.CvmB100Low;
            //else
            //    Instrument = Tower.CvmB100Hi;        
        }

        private void SetMode(bool read)
        {
            modeRead = read;
            if (modeRead && !timerEnabled)
                EnableTimer();
            else
                timerEnabled = false;
        }

        private void EnableTimer()
        {
            timerEnabled = true;
            Task.Factory.StartNew(OnGraphTask);
        }

        private void OnGraphTask()
        {
            while (timerEnabled)
            {
                try
                {
                    UpdateGraphs();
                }
                catch { }
                Thread.Sleep(1000);
            }
        }

        private void UpdateGraphs()
        {
            var vars = Instrument.ReadVariablesInstantaneas();

            TriLineValue current = TriLineValue.Zero;
            TriLineValue voltage = TriLineValue.Zero;
            TriLineValue kw = TriLineValue.Zero;
            TriLineValue pf = TriLineValue.Zero;

            if (modeRead)
                current = TriLineValue.Create(vars.L1.Corriente, vars.L2.Corriente, vars.L3.Corriente);

            if (modeRead)
                voltage = TriLineValue.Create(vars.L1.Tension, vars.L2.Tension, vars.L3.Tension);

            sc.Post(_ =>
            {
                if (current != TriLineValue.Zero)
                {
                    DataPointHub.Add(this, "I1", current.L1, TAG);
                    DataPointHub.Add(this, "I2", current.L2, TAG);
                    DataPointHub.Add(this, "I3", current.L3, TAG);
                }

                if (voltage != TriLineValue.Zero)
                {
                    DataPointHub.Add(this, "V1", voltage.L1, TAG);
                    DataPointHub.Add(this, "V2", voltage.L2, TAG);
                    DataPointHub.Add(this, "V3", voltage.L3, TAG);
                }
            }, null);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (cvm != null)
                cvm.Dispose();

            base.Dispose(disposing);
        }
    }
    public class CvmB150Description : ControlDescription
    {
        public override string Name { get { return "CVMB150"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.cvm_b; } }
        public override Image IconBig { get { return Properties.Resources.CVM_B100; } }
        public override Type SubcomponentType { get { return typeof(CVMB100Tower); } }
    }
}
