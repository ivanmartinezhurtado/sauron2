﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Metering.SOAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class CIRLAMP_MANAGER : DeviceBase
    {
        private CirlampSoapService _soap;

        protected CirlampSoapService Soap
        {
            get
            {
                if (_soap == null)
                    _soap = new CirlampSoapService();

                return _soap;
            }
        }

        public string ReadXMLFile(string file)
        {
            LogMethod();

            try
            {
                var response = Soap.ReadFile(file);

                _logger.InfoFormat("response: {0}", response);

                if (string.IsNullOrEmpty(response))
                    TestException.Create().UUT.COMUNICACIONES.SOAP_SERVICE(string.Format("SynchReport file:{0}", file)).Throw();

                return response;

            }catch(Exception ex)
            {
                TestException.Create().UUT.COMUNICACIONES.SOAP_SERVICE(string.Format("SynchReport file:{0} Exception:{1}", file, ex.Message)).Throw();
            }

            return null;
        }

        public void WriteFile(string file)
        {
            LogMethod();

            try
            {
                Soap.WriteFile(file);

                _logger.InfoFormat("Orde File succes!");
            }
            catch (Exception ex)
            {
                TestException.Create().UUT.COMUNICACIONES.SOAP_SERVICE(string.Format("SynchReport writefile Exception:{0}", ex.Message)).Throw();
            }
        }


        public void WriteOutputs(byte output, bool state)
        {
            LogMethod();

            try
            {
                 Soap.WriteOutputs(output, state);

                _logger.InfoFormat("Orde WriteOutputs Succes!");
            }
            catch (Exception ex)
            {
                TestException.Create().UUT.COMUNICACIONES.SOAP_SERVICE(string.Format("SynchReport WriteOutputs Exception:{0}", ex.Message)).Throw();
            }
        }

        public override void Dispose()
        {
       
                           
        }
    }
}
