﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(TestDetectioDigitalIputsDescription))]
    

    public class TestDetectioDigitalIputs : ActionBase
    {
        [Category("PARAMETRO")]
        [Description("Entrada que queremos detectar activada")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string InputDetectionDevice { get; set; }

        [Category("PARAMETRO")]
        [DefaultValue(true)]
        [Description("Estado que esperamos de la entrada a detectar")]
        public bool StateInputDetection { get; set; }

        [Category("PARAMETRO")]
        [Description("valor de la salida o con $ variable de la salida que des/activamos,")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string OuputActivatedPiston { get; set; }

        [Category("CONFIGURACION")]
        [Description("Tiempo de espera antes de leer el estado de la entrada si esta activada")]
        [DefaultValue((int)500)]
        public int WaitTimetToDetectionDevice { get; set; }

        [Category("CONFIGURACION")]
        [Description("Numero de reintentos para detectar la entrada")]
        [DefaultValue((byte)3)]
        public byte NumRetries { get; set; }

        [Category("CONFIGURACION")]
        [Description("Tiempo de espera entre los reintentos")]
        [DefaultValue((int)3)]
        public int WaitTimeBetweenRetries { get; set; }

        [Category("OPCIONES")]
        [Description("Si queremos mostrar un mensaje de aviso antes de dar como error la acción")]
        [DefaultValue(false)]
        public bool ShowMessageWarning { get; set; }

        [Category("OPCIONES")]
        [DefaultValue(false)]
        public bool saveResult { get; set; }

        [Category("CODIFICACION ERROR")]
        [Description("Categoria del Error que lanzaremos en el Test")]
        [DefaultValue(Hardware.UTIL)]
        public Hardware HardwareError { get; set; }

        [Category("CODIFICACION ERROR")]
        [Description("Descripcion del Error de la entrada que lanzaremos en el Test ")]
        public string DescriptionAction { get; set; }

        public enum Hardware
        {
            MANGUERA,
            UTIL,
            TORRE,
            UUT,
        }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            byte? ouputActivatedPiston = null;

            if (!string.IsNullOrEmpty(OuputActivatedPiston))
                ouputActivatedPiston = Convert.ToByte(VariablesTools.ResolveValue(OuputActivatedPiston));

            if (string.IsNullOrEmpty(InputDetectionDevice))
            if (testBase.Shell.MsgBox(string.Format("Error, no se ha indicado ninguna Input para detectar en {0}", DescriptionAction), "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.OK, MessageBoxIcon.Error) != DialogResult.Yes)
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("NO SE HA INDICADO NINGUNA ENTRADA A DETECTAR").Throw();
            //throw new Exception(string.Format("Error, no se ha indicado ninguna Input para detectar en {0}", DescriptionAction));

            byte inputDetectionDevice = Convert.ToByte(VariablesTools.ResolveValue(InputDetectionDevice));            

            testBase.SamplerWithCancel((p) =>
            {
                if (ouputActivatedPiston.HasValue)
                    tower.IO.DO.On(ouputActivatedPiston.Value);

                testBase.Delay(WaitTimetToDetectionDevice, "Espera a la detección de la entrada");

                if (tower.IO.DI[inputDetectionDevice] == StateInputDetection)
                {
                    if (saveResult)
                        testBase.Resultado.Set(string.Format("IN_{0}", InputDetectionDevice), "OK", ParamUnidad.SinUnidad);

                    return true;
                }

                if (ShowMessageWarning)
                    if (testBase.Shell.MsgBox(string.Format("Entrada digital {0}  {1}  no activada, ¿Quieres volver a leerla?", InputDetectionDevice, DescriptionAction), "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    {
                        if (ouputActivatedPiston.HasValue)
                            tower.IO.DO.Off(ouputActivatedPiston.Value);

                        Error().PROCESO.OPERARIO.TEST_CANCELADO("REPETIR DETECCION ENTRADA").Throw();
                        //throw new Exception(string.Format("Error en la detección de la entrada digital {0}, el usuario ha indicado que no quiere volver a comprobarla", InputDetectionDevice));
                    }

                if (p >= NumRetries)
                    if (ouputActivatedPiston.HasValue)
                        tower.IO.DO.Off(ouputActivatedPiston.Value);

                if (saveResult)
                    testBase.Resultado.Set(string.Format("IN_{0}", InputDetectionDevice), "ERROR", ParamUnidad.SinUnidad);

                return false;

            },"", NumRetries, WaitTimeBetweenRetries, 0, false, true,
            (p) =>
            {
                switch (HardwareError)
                {
                    case Hardware.MANGUERA:
                         Error().HARDWARE.MANGUERA.ENTRADAS_DIGITALES(DescriptionAction).Throw();
                         break;
                    case Hardware.TORRE:
                         Error().HARDWARE.TORRE.ENTRADAS_DIGITALES(DescriptionAction).Throw();
                        break;
                    case Hardware.UTIL:
                        Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES(DescriptionAction).Throw();
                        break;
                };
            });

            //string.Format("Error. Entrada {0}  {1} no detectada en estado {2}", InputDetectionDevice, DescriptionAction, StateInputDetection)
        }
    }

    public class TestDetectioDigitalIputsDescription : ActionDescription
    {
        public override string Name { get { return "DetectionInput"; } }
        public override string Description { get { return "Acción genérica para detectar las entradas activadas de la torre"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.input; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;TowerDispose:2"; } }
    }
}
