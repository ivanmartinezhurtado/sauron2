﻿using System;
using System.Runtime.InteropServices;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.03)]
    public class SET90 : DeviceBase
    {
        private const string PASSWORD = "AA9E93";
        private const string FAB_STATE = "C0FF";
        private const string RS232_ENABLED = "91DE2ACFAB";
        private const string WRITE_PPM = "920088{0:00000000}";
        private const string READ_VERSION = "10";
        private const string READ_CODE_ERROR = "17000A01";
        private const string RESET = "11FF";
        private const string WRITE_SERIAL_NUMBER = "99DE2ACFAB{0}";
        private const string READ_SERIAL_NUMBER = "19";
        private const string CONFIGURATION_DEFEAULT = "13F1";
        private const string CONFIGURATION_CLIENTE = "13F1";
        private const string WRITE_FACTORS_CALIBRATION = "ADA0{0}";
        private const string WRITE_PROTECCTION_FLASH = "A5F0CA";
        private const string READ_FACTORS_CALIBRATION = "2E";
        private const string WRITE_CONF_READ_CURRENT = "9601B9{0}";
        private const string WRITE_DATETIME = "9D{0}00";
        private const string READ_DATETIME = "1D";
        private const string DISPLAY1 = "9800FF00FF00FF00FF00FF00FF00FF00FF00FF00FFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00";
        private const string DISPLAY2 = "98FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF";
        private const string READ_KEYBOARD = "16000806";
        private const string READ_INOUT_DIGITAL = "16000008";
        private const string WRITE_OUTPUTS = "960110{0}{1}{2}{3}{4}{5}";
        private const string WRITE_ADJUST_INPUT_300Vc = "2E{0}";

        private const string MEDID_AN0 = "17004A01";
        private const string MEDID_AN1 = "17004B01";
        private const string MEDID_AN2 = "17004C01";
        private const string MEDID_AN3 = "17004D01";
        private const string MEDID_I1 = "17004B01";
        private const string MEDID_I2 = "17004C01";
        private const string MEDID_I3 = "17004D01";
        private const string MEDID_V1 = "17004E01";
        private const string MEDID_V2 = "17004F01";
        private const string MEDID_V3 = "17005001";

        private const string WRITE_CONFIG_ANALOG_INPUTS_ACDC = "9601B9{0}";


        public SET90()
        {
        }

        public SET90(int port)
        {
            SetPort(port);
        }

        public override void Dispose()
        {
            if (Midabus != null)
                Midabus.Dispose();
        }

        public void SetPort(int port, int baudRate = 38400, byte periferic = 1)
        {
            if (Midabus == null)
                Midabus = new MidabusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, 8, _logger);
            else
                Midabus.PortCom = port;

            Midabus.PerifericNumber = periferic;
            Midabus.BaudRate = baudRate;
        }

        public MidabusDeviceSerialPort Midabus { get; internal set; }

        public void WritePassword()
        {
            LogMethod();
            Midabus.Write(PASSWORD, "00");
        }

        public void WriteFabState()
        {
            LogMethod();
            Midabus.Write(FAB_STATE, "00");
        }

        public void WriteRS232Enabled()
        {
            LogMethod();
            Midabus.Write(RS232_ENABLED, "00");
        }

        public void WritePPM(double points, int flagRetDesfase)
        {
            LogMethod();
            var Points = (uint)points;

            if (Points > 31557600)
                Points = 0xFFFFFFFF;
            else if (flagRetDesfase == 1)
                Points += 0x40000000;                   

            string ppm = Points.ToString("X").PadLeft(8, '0');
            Midabus.Write(WRITE_PPM, "00", ppm);
        }

        public string ReadVersion()
        {
            LogMethod();
            var result = Midabus.Read(READ_VERSION, "90");
            var versionHi = byte.Parse(result.Substring(4,1), System.Globalization.NumberStyles.HexNumber);
            var versionLow = byte.Parse(result.Substring(5, 1), System.Globalization.NumberStyles.HexNumber);
            var modelo = result.Substring(6, 2);
            return string.Format("{0}.{1} {2}", versionHi, versionLow, modelo == "02" ? "A" : "C");
        }

        public string HasErrors()
        {
            LogMethod();
            var result = Midabus.Read(READ_VERSION, "90");
            var errorHi = byte.Parse(result.Substring(6, 1), System.Globalization.NumberStyles.HexNumber);
            var errorLow = byte.Parse(result.Substring(7, 1), System.Globalization.NumberStyles.HexNumber);
            if ((errorHi + errorLow) == 0)
                return "OK";
            else
                return ReadCodeError();
        }

        public string ReadCodeError()
        {
            LogMethod();
            var result = Midabus.Read(READ_CODE_ERROR, "97000A");
            var errorType = result.Substring(2, result.Length - 4);
            return errorType;
        }

        public void WriteReset()
        {
            LogMethod();
            Midabus.WriteWhitoutResponse(RESET);
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            LogMethod();
            Midabus.Write(WRITE_SERIAL_NUMBER, "00", serialNumber.ToString("X8"));
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            var trama = Midabus.Read(READ_SERIAL_NUMBER, "99");
            var ns = Convert.ToInt32(trama.Substring(2, 8), 16);
            return ns;
        }

        public void WriteConfigurationClient()
        {
            LogMethod();
            Midabus.Write(CONFIGURATION_CLIENTE, "00");
        }

        public void WriteConfigurationDefault()
        {
            LogMethod();
            Midabus.Write(CONFIGURATION_DEFEAULT, "00");
        }


        public void WriteProtecctionFlash()
        {
            LogMethod();
            Midabus.WriteWhitoutResponse(WRITE_PROTECCTION_FLASH);
        }

        public void WriteConfigurationReadCurrent(bool readCurrent)
        {
            LogMethod();
            Midabus.Write(WRITE_CONF_READ_CURRENT, "00",  readCurrent ? "FF" : "00");
        }

        public enum FactorsEnum
        {
            AN0 = 0,
            NEUTRO = 1,
            AN1 = 2,
            V1 = 3,
            AN2 = 4,
            V2 = 5,
            AN3 = 6,
            V3 = 7,
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Sumatorios
        {
            public byte InputSelection;
            public int Sumat2;
            public int Sumat1;
            public short SampleNumber;
            public byte State;
        }

        public Sumatorios ReadFactorCalibration(FactorsEnum factor)
        {
            LogMethod();
            var tx = string.Format("{0}{1:00}", READ_FACTORS_CALIBRATION, (byte)factor);
            var trama = Midabus.ReadStruct<Sumatorios>(tx, "AE");
            return trama;
        }

        public enum FactorsCurrentDCEnum
        {
            AN0 = 0,
            AN1 = 2,
            AN2 = 4,
            AN3 = 6,
        }

        public double CalibrationAnalogInputsVoltageDC(FactorsCurrentDCEnum factor, double voltageRead)
        {
            var sum = ReadFactorCalibration((FactorsEnum)factor);
            _logger.InfoFormat("Lectura 2E {0}= Sumatorio = {1}  Sumatorios2 = {2}  NumMuesrtas = {3}", factor.ToString(), sum.Sumat1, sum.Sumat2, sum.SampleNumber);
            var neutro = ReadFactorCalibration(FactorsEnum.NEUTRO);
            _logger.InfoFormat("Lectura 2E Neutro= Sumatorio = {0}  Sumatorios2 = {1}  NumMuesrtas = {2}", neutro.Sumat1, neutro.Sumat2, neutro.SampleNumber);
            var valorD = (20000 * voltageRead) / 2;
            _logger.InfoFormat("ValorD = {0}", valorD);
            var FC = (valorD * 256 * sum.SampleNumber) / (sum.Sumat1 - neutro.Sumat1);
            _logger.InfoFormat("FactorCalibracion = {0}", FC);
            return FC;
        }

        public enum FactorsVoltageEnum
        {
            V1 = 3,
            V2 = 5,
            V3 = 7,
        }

        public double CalibrationAnalogInputsVoltageAC(FactorsVoltageEnum factor, double voltageRead)
        {
            return CalibrationAnalogInputs((FactorsEnum)factor, voltageRead, 300);
        }

        public enum FactorsCurrentACEnum
        {
            I1 = 2,
            I2 = 4,
            I3 = 6,
        }

        public double CalibrationAnalogInputsCurrentAC(FactorsCurrentACEnum factor, double voltageRead)
        {
            return CalibrationAnalogInputs((FactorsEnum)factor, voltageRead, 1.5);
        }

        private double CalibrationAnalogInputs(FactorsEnum factor, double voltageRead, double consigna)
        {
            var sum = ReadFactorCalibration(factor);
            _logger.InfoFormat("Lectura 2E {0}= Sumatorio = {1}  Sumatorios2 = {2}  NumMuesrtas = {3}", factor.ToString(), sum.Sumat1, sum.Sumat2, sum.SampleNumber);

            var valorD = (48000 * voltageRead) / consigna;
            _logger.InfoFormat("ValorD = {0}", valorD);

            var FC = (Math.Pow(valorD, 2) * sum.SampleNumber) / (sum.Sumat2 - (Math.Pow(sum.Sumat1, 2) / sum.SampleNumber));
            _logger.InfoFormat("FactorCalibracion = {0}", FC);
            return FC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FactorCalibration
        {
            public int Cal1_VL1;
            public int Cal2_IL1;
            public int Cal3_VL2;
            public int Cal4_IL2;
            public int Cal5_VL3;
            public int Cal6_IL3;
            public int Cal7_CAN0;
            public int Cal8_CAN1;
            public int Cal9_CAN2;
            public int Cal10_CAN3;
        }

        public void WriteCalibrationFactors(FactorCalibration factores)
        {
            LogMethod();
            var trama1 = string.Format("{0:X4}-{1:X4}-{2:X4}-{3:X4}-{4:X4}", factores.Cal1_VL1, factores.Cal2_IL1, factores.Cal3_VL2, factores.Cal4_IL2, factores.Cal5_VL3);
            _logger.InfoFormat("Trama1:{0}", trama1);
            var trama2 = string.Format("{0:X4}-{1:X4}-{2:X4}-{3:X4}-{4:X4}", factores.Cal6_IL3, factores.Cal7_CAN0, factores.Cal8_CAN1, factores.Cal9_CAN2, factores.Cal10_CAN3);
            _logger.InfoFormat("Trama2:{0}", trama2);
            Midabus.Write(WRITE_FACTORS_CALIBRATION, "00", trama1.Replace("-","") + trama2.Replace("-", ""));
        }


        public void WriteDateTime()
        {
            LogMethod();
            var trama = string.Format("{0:00}{1:00}{2:00}{3:00}{4:00}{5:00}", DateTime.Now.Second, DateTime.Now.Minute, DateTime.Now.Hour, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year.ToString().Substring(2));
            Midabus.Write(WRITE_DATETIME, "00", trama);
        }



        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct DateSet90
        {
            public byte second;
            public byte minute;
            public byte hour;
            public byte day;
            public byte month;
            public byte year;
        }

        public DateTime Read_DATETIME()
        {
            LogMethod();
            var trama = Midabus.ReadStrings<DateSet90>(READ_DATETIME, "00");
            var fecha = new DateTime(trama.year + 2000, trama.month, trama.day, trama.hour, trama.minute, trama.second);
            return fecha;
        }

        public enum DisplayEnum
        {
            DISPLAY_1 = 0,
            DISPLAY_2 = 1,
        }

        public void WriteDisplay(DisplayEnum typeDisplayTest)
        {
            LogMethod();

            string tramaTx = DISPLAY1;

            if (typeDisplayTest == DisplayEnum.DISPLAY_2)
                tramaTx = DISPLAY2;

            Midabus.Write(tramaTx, "00");
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct KeyBorad
        {
            public string Left;
            public string Up;
            public string Down;
            public string Right;
            public string Exit;
            public string Ok;
        }

         public KeyBorad ReadKeyboard()
        {
            LogMethod();
            var trama = Midabus.ReadStrings<KeyBorad>(READ_KEYBOARD, "960008");
            return trama;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct IputDigital
        {
            public string E1;
            public string E2;
            public string E3;
            public string E4;
            public string E5;
            public string E6;
            public string E7;
            public string E8;
        }

        public IputDigital ReadInputDigital()
        {
            LogMethod();
            var trama = Midabus.ReadStrings<IputDigital>(READ_INOUT_DIGITAL, "960000");
            return trama;
        }

        public void WriteOuput(bool rele1 =false, bool rele2= false, bool rele3= false, bool rele4= false, bool rele5= false, bool rele6= false)
        {
            LogMethod();  
            var tramaTx = string.Format(WRITE_OUTPUTS, rele1? "FF":"00", rele2 ? "FF" : "00", rele3 ? "FF" : "00", rele4 ? "FF" : "00", rele5 ? "FF" : "00", rele6 ? "FF" : "00");
            Midabus.Write(tramaTx, "00");
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MeasureInputsDC
        {
            public double AN0;
            public double AN1;
            public double AN2;
            public double AN3;
        }

        public MeasureInputsDC ReadMeasureInputsDC()
        {
            LogMethod();
            var ano = Midabus.ReadStruct<MeasureRegister>(MEDID_AN0, "97004A");
            var an1 = Midabus.ReadStruct<MeasureRegister>(MEDID_AN1, "97004B");
            var an2 = Midabus.ReadStruct<MeasureRegister>(MEDID_AN2, "97004C");
            var an3 = Midabus.ReadStruct<MeasureRegister>(MEDID_AN3, "97004D");

            var result = new MeasureInputsDC()
            {
                AN0 = ano.measure / 1000D,
                AN1 = an1.measure / 1000D,
                AN2 = an2.measure / 1000D,
                AN3 = an3.measure / 1000D,
            };

            _logger.InfoFormat("Medidas Inputs DC {0}", result);
            return result;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MeasureInputsVoltageAC
        {
            public double V1;
            public double V2;
            public double V3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MeasureRegister
        {
            public Int16 measure;
        }

        public MeasureInputsVoltageAC ReadMeasureInputsVoltageAC()
        {
            LogMethod();
            var v1 = Midabus.ReadStruct<MeasureRegister>(MEDID_V1, "97004E");
            var v2 = Midabus.ReadStruct<MeasureRegister>(MEDID_V2, "97004F");
            var v3 = Midabus.ReadStruct<MeasureRegister>(MEDID_V3, "970050");

            var result = new MeasureInputsVoltageAC()
            {
                V1 = v1.measure / 10D,
                V2 = v2.measure / 10D,
                V3 = v3.measure / 10D,
            };

            return result;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MeasureInputsCurrentsAC
        {
            public double I1;
            public double I2;
            public double I3;
        }

        public MeasureInputsCurrentsAC ReadMeasureInputsCurrentsAC()
        {
            LogMethod();
            var i1 = Midabus.ReadStruct<MeasureRegister>(MEDID_I1, "97004B");
            var i2 = Midabus.ReadStruct<MeasureRegister>(MEDID_I2, "97004C");
            var i3 = Midabus.ReadStruct<MeasureRegister>(MEDID_I3, "97004D");

            var result = new MeasureInputsCurrentsAC()
            {
                I1 = i1.measure / 667D, //Relacion transformacion de 1.5Vac / 100A = 66.6 y *10 por la resolucion un decimal
                I2 = i2.measure / 667D,
                I3 = i3.measure / 667D,
            };

            return result;
        }

        public enum InputAnalogType
        {
            AC = 0,
            DC = 1,
        }

        public void WriteConfigAnalogInputsType(InputAnalogType type)
        {
            LogMethod();
            Midabus.Write(WRITE_CONFIG_ANALOG_INPUTS_ACDC, "00", type== InputAnalogType.AC ? "FF" : "00" );
        }
    }
}
