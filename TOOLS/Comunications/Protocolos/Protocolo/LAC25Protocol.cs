﻿using Comunications.Message;
using System;
using System.Collections.Generic;

namespace Comunications.Protocolo
{
    public class LAC25Protocol : ASCIIProtocol
    {
        public LAC25Protocol()
        {
            CaracterFinRx = "\r";
            CaracterFinTx = "\r";
        }

        public override byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            LAC25Message lacMessage = message as LAC25Message;

            int numCommands = lacMessage.NumCommands;

            var data = new List<byte>();

            while(numCommands > 0)
            {
                if (data.Count > 0)
                    data.Add((byte)';');
                if (numCommands == 1)
                    try
                    {
                        data.AddRange(base.ReadMessage(message, streamResource));
                    }
                    catch (TimeoutException)
                    {
                    }
                else
                    data.AddRange(base.ReadMessage(message, streamResource));

                numCommands--;
            }

            return data.ToArray();
        }
    }
}
