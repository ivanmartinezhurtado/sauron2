﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    /// <summary>
    /// Scp client to upload file to the server Scp. 
    /// </summary>
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(ScpClientUpLoadFileDescription))]
    public class ScpClientUploadFile : ActionBase
    {
        /// <summary>
        /// Path of file to upload.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Ruta del fichero a subir")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable UploadFile { get; set; } = new ActionVariable();

        /// <summary>
        /// Destination path of uploaded file into the server.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Ruta donde se guardará el fichero en el servidor")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable DestinationFile { get; set; } = new ActionVariable();

        /// <summary>
        /// Instance of object SCPClient.
        /// </summary>
        [DefaultValue("SCPClient")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SCPClient")]
        public string DeviceName { get; set; }

        /// <summary>
        /// Execute the action.
        /// </summary>
        public override void Execute()
        {
            var scp = GetVariable(DeviceName) as SCPClient;
            if (scp == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("ScpClient").Throw();
            
            var uploadFile = UploadFile.Value.ToString();
            var destinationFile = DestinationFile.Value.ToString();
            try
            {
                scp.UpLoad(uploadFile, destinationFile);
            }
            catch (Exception e)
            {
                Error().UUT.COMUNICACIONES.ERROR_UPLOAD(e.Message).Throw();
            }


        }
    }

    class ScpClientUpLoadFileDescription : ActionDescription
    {
        public override string Name { get { return "SCPClientUploadFile"; } }
        public override string Category { get { return "Tools/Ethernet/SCP"; } }
        public override string Description { get { return "Sube un fichero a un servidor mediante el protocolo SCP"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
