﻿using System.Collections.Generic;
using System.ComponentModel;

namespace WinTestPlayer.Model
{
    public class TestModel : INotifyPropertyChanged
    {
        public TestModel()
        {
            ResultadosComunes = new Dictionary<string, string>();
        }

        public string IdUsuario { get; set; }
        public string IdEmpleado { get; set; }
        public string IdOperario { get; set; }
        public string Operario { get; set; }
        public string Lote { get; set; }
        public string Codigo { get; set; }
        public string Cliente { get; set; }
        public string IdFase { get; set; }
        public string NumOrden { get; set; }
        public string NumProducto { get; set; }
        public string Descripcion { get; set; }
        public string DevicePC { get; set; }
        public string SauronVersion { get; set; }
        public int Cantidad { get; set; }
        public bool HasDatabase { get; set; }
        public int NumOK { get; set; }
        public int NumNG { get; set; }

        public Dictionary<string, string> ResultadosComunes { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CommonParams
    {
        public const string DEVICE_PC = "DEVICE_PC";
        public const string TESTPLAYER_VERSION = "TESTPLAYER_VERSION";
        public const string USER_ID = "USER_ID";
    }
}
