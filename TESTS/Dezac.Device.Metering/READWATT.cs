﻿using Comunications.Utility;
using System;
using System.Runtime.InteropServices;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class READWATT : DeviceBase
    {
        private ModbusDeviceSerialPort modbus;
        private int port=7;

        public READWATT()
        {
        }

        public READWATT(int portCom)
        {
            SetPort(portCom);
        }

        public void SetPort(int portCom)
        {
            port = portCom;
        }

        public ModbusDeviceSerialPort Modbus
        {
            get
            {
                if (modbus == null)
                    modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);

                return modbus;
            }
        }

        public void WriteResetEnergyPulse()
        {
            LogMethod();
            modbus.Write<EnergyRegister>(new EnergyRegister() { Energy = 0 });
        }
  
        public void WriteNumSerie(int value)
        {
            LogMethod();
            modbus.WriteInt32((ushort)Registers.NUMSERIE, value);
        }

        public void WriteBastidor(int value)
        {
            LogMethod();
            modbus.WriteInt32((ushort)Registers.BASTIDOR, value);
        }

        public void WriteCodeError(ushort codeError)
        {
            LogMethod();
            modbus.Write((ushort)Registers.CODERROR, codeError);
        }

        public void WriteModbusAddress(ushort address)
        {
            LogMethod();
            modbus.Write((ushort)Registers.MODBUSADRESS, address);
        }

        public void WriteMaxPulseTime(ushort maxPulseTime)
        {
            LogMethod();
            modbus.Write((ushort)Registers.MAXTIMEPULSOS, maxPulseTime);
        }

        public void WriteMinPulseTime(ushort minPulseTime)
        {
            LogMethod();
            modbus.Write((ushort)Registers.MIMTIMEPULSOS, minPulseTime);
        }

        public void WriteDivPulse(ushort divPulse)
        {
            LogMethod();
            modbus.Write((ushort)Registers.DIVPULSOS, divPulse);
        }

        public void WriteRatioPulse(ushort ratioPulse)
        {
            LogMethod();
            modbus.Write((ushort)Registers.RATIOPULSOS, ratioPulse);
        }

        public void WriteTransformRatioPulse(ushort transformRatioPulse)
        {
            LogMethod();
            modbus.Write((ushort)Registers.TRANSFORMRATIO, transformRatioPulse);
        }

        public void WriteWidthPulse(ushort widthPulse)
        {
            LogMethod();
            modbus.Write((ushort)Registers.TRANSFORMRATIO, widthPulse);
        }

        
        //**************************************
        //**************************************

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SOFT_VERSION)]
        public struct FirmwareVersion
        {
            public UInt16 Major;
            public UInt16 Minor;
            public UInt16 Revision;
        };

        public FirmwareVersion ReadSoftwareVersion()
        {
            LogMethod();
            return modbus.Read<FirmwareVersion>();
        }

        public int ReadNumberPulse()
        {
            LogMethod();
            return modbus.ReadInt32((ushort)Registers.VALORPULSOS);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ENERGIAPULSOS)]
        public struct EnergyRegister
        {
            public UInt64 Energy;
        };

        //public int ReadValorPulsos()
        //{
        //    return Modbus.Read((ushort)Registers.VALORPULSOS);
        //}

        public EnergyRegister ReadEnergyPulsos()
        {
            LogMethod();
            return Modbus.Read<EnergyRegister>();
        }

        public int ReadNumSerie()
        {
            LogMethod();
            return modbus.ReadInt32((ushort)Registers.NUMSERIE);
        }

        public int ReadBastidor()
        {
            LogMethod();
            return modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public int ReadCodError()
        {
            LogMethod();
            return modbus.ReadRegister((ushort)Registers.CODERROR);
        }

        //**************************************
        //**************************************

        public override void Dispose()
        {
            modbus.Dispose();
        }

        public enum Registers
        {
            MODBUSADRESS = 8,
            NUMSERIE = 61440,
            BASTIDOR = 61504,
            SOFT_VERSION = 80,
            CODERROR = 61536,
            MAXTIMEPULSOS = 262,
            MIMTIMEPULSOS = 263,
            DIVPULSOS = 264,
            RATIOPULSOS = 265,
            TRANSFORMRATIO = 266,
            DURACIONPULSO = 267,
            VALORPULSOS = 1536,
            ENERGIAPULSOS = 2000
        };
    }
}
