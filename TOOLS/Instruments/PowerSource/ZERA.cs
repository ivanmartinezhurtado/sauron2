﻿using Comunications;
using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;
using System.ComponentModel;
using System.Globalization;
using System.IO.Ports;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.01)]
    public class ZERA : PowerSourceACIII, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(ZERA));

        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private SerialPort sp;
        private byte _port = 1;

        private const string PRESET_CONFIG_FRECUENCY_COMMAND = "SFR{0:00.00}";
        private const string PRESET_CONFIG_VOLTAGE_COMMAND = "SUPAAR{0:000.000}{1:000.00}S{2:000.000}{3:000.00}T{4:000.000}{5:000.00}";
        private const string PRESET_CONFIG_CURRENT_COMMAND = "SIPEAR{0:000.000}{1:000.00}S{2:000.000}{3:000.00}T{4:000.000}{5:000.00}";

        private const string APPLY_CONFIG_COMMAND = "SUI{0}{1}AAA";
        private const string APPLY_OFF_COMMAND = "SUIAAAAAAAAA";
        private const string READ_VERSION = "SAW";
        private const string READ_STATUS_COMMAND = "SSM";

        public ZERA()
            : base(logger)
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinRx = "\r", CaracterFinTx = "\r" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp, "\r"), logger);
            transport.Retries = 1;
            transport.ReadTimeout = 10000;
            transport.OnMessageException += OnMessageException;
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumento port a byte");
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = 1000;
            sp.ReadTimeout = 3000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.Two;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_VOLTAGE_COMMAND, Presets.Voltage.L1, Presets.Desfase.L1, Presets.Voltage.L2, Presets.Desfase.L2, Presets.Voltage.L3, Presets.Desfase.L3);
                    message.Validator = (m) => { return m.Text == "SOKUP"; };
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Voltage = Presets.Voltage;
                    break;
                case (int)Commands.SET_CURRENT:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_CURRENT_COMMAND, Presets.Current.L1, Presets.PF.L1, Presets.Current.L2, Presets.PF.L2, Presets.Current.L3, Presets.PF.L3);
                    message.Validator = (m) => { return m.Text == "SOKIP"; };
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    break;
                case (int)Commands.SET_FREQ:
                    message = StringMessage.Create(enUS,PRESET_CONFIG_FRECUENCY_COMMAND, Presets.Freq);
                    message.Validator = (m) => { return m.Text == "SOKFR"; };             
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Freq = Presets.Freq;
                    break;
                case (int)Commands.SET_PF:
                    DevicePreset.PF = Presets.PF;
                    break;
                case (int)Commands.SET_DESFASE:
                    DevicePreset.Desfase = Presets.Desfase;
                    break;
                case (int)Commands.APPLY_CONFIG:
                    var voltage = string.Format("{0}{1}{2}", Presets.Voltage.L1 == 0 ? "A" : "E", Presets.Voltage.L2 == 0 ? "A" : "E", Presets.Voltage.L3 == 0 ? "A" : "E");
                    var current = string.Format("{0}{1}{2}", Presets.Current.L1 == 0 ? "A" : "P", Presets.Current.L2 == 0 ? "A" : "P", Presets.Current.L3 == 0 ? "A" : "P");
                    message = StringMessage.Create(enUS, APPLY_CONFIG_COMMAND, voltage, current);
                    message.Validator = (m) => { return m.Text == "SOKUI"; };
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)Commands.RESET:
                case (int)Commands.APPLY_OFF:
                    message = new StringMessage { Text = APPLY_OFF_COMMAND };
                    message.Validator = (m) => { return m.Text == "SOKUI"; };
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)Commands.READ_STATUS:
                    message = new StringMessage { Text = READ_STATUS_COMMAND };
                    message.Validator = (m) => { return m.Text.StartsWith("SSM"); };
                    response = transport.SendMessage<StringMessage>(message);

                    string text = response.Text.Substring(3);
                    int intParsed;
                    if (!int.TryParse(text.Trim(), out intParsed))
                        intParsed = 1;

                    Status.HasError = intParsed != 0;
                    break;
                case (int)Commands.READ_VOLTAGE:
                case (int)Commands.READ_CURRENT:
                case (int)Commands.READ_POWERFACTOR:
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }

        protected override bool ValidateEstabilization(double tolerance)
        {
            return DeviceData.Voltage.InToleranceFrom(DevicePreset.Voltage, tolerance) &&
               DeviceData.Current.InToleranceFrom(DevicePreset.Current, tolerance);
        }

        [Browsable(false)]
        public override TriLineValue ReadVoltage()
        {
            throw new NotSupportedException();
        }

        public virtual void OnMessageException(object sender, MessageExceptionHandler e)
        {
            logger.WarnFormat("PowerSource ZERA OnMessageExceptionr by {0}", e.Exception.Message);
            if (e.Exception is ApplicationException)
                if (!e.Throw)
                {
                    SendCommand(new Command<Commands>(Commands.RESET, true));
                    System.Threading.Thread.Sleep(1000);
                }
        }
    }
}
