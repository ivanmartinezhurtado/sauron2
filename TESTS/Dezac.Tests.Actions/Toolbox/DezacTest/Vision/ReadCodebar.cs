﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ReadCodebarDescription))]
    public class ReadCodebar : ActionBase
    {
        [Category("1.- Configuracion de Test")]
        [Description("Numero de reintentos en la lectura")]
        public ActionVariable Retries { get; set; } = new ActionVariable();

        [Category("1.- Configuracion de Test")]
        [Description("Numero de reintentos en la lectura")]
        [DefaultValue(TestHalconExtension.TypeBarcode.None)]
        public TestHalconExtension.TypeBarcode BarcodeType { get; set; } 

        [Category("2.- Camera Selection")]
        [Description("Numero de serie de la camara")]
        public ActionVariable SerialNumber { get; set; } = new ActionVariable();

        [Category("3.- Recortar Imagen")]
        [Description("Posicion del pixel de la izquierda en X del area a recortar (0 si no se requiere recortar la imagen)")]
        public ActionVariable Column1 { get; set; } = new ActionVariable();

        [Category("3.- Recortar Imagen")]
        [Description("Posicion del pixel de la derecha en X del area a recortar (0 si no se requiere recortar la imagen)")]
        public ActionVariable Column2 { get; set; } = new ActionVariable();

        [Category("3.- Recortar Imagen")]
        [Description("Posicion del pixel de arriba en Y del area a recortar (0 si no se requiere recortar la imagen)")]
        public ActionVariable Row1 { get; set; } = new ActionVariable();

        [Category("3.- Recortar Imagen")]
        [Description("Posicion del pixel de abajo en Y del area a recortar (0 si no se requiere recortar la imagen)")]
        public ActionVariable Row2 { get; set; } = new ActionVariable();

        [Category("4.- Data Output")]
        [Description("Variable de salida")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del TestBase");
           
            var readCodeBar = testBase.TestHalconReadBarCodeProcedure(SerialNumber.Value.ToString(), BarcodeType, 
                "BARCODE", "BARCODE", Retries.GetValue<byte>(), null, Row1.GetValue<int>(), 
                Column1.GetValue<int>(), Row2.GetValue<int>(), Column2.GetValue<int>(), 0);
            DataOutput.Value = readCodeBar;      
        }
    }

    public class ReadCodebarDescription : ActionDescription
    {
        public override string Name { get { return "ReadBarcode"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
