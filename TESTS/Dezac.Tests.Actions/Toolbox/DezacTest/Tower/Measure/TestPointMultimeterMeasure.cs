﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.06)]
    [DesignerAction(Type = typeof(TestPointMeasureDescription))]
    public class TestPointMeasure : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número de de las primeras muestras que descartamos")]
        [DefaultValue(0)]
        public int DeleteFirst { get; set; }

        [Category("3. Configuration")]
        [Description("Número de muestras")]
        [DefaultValue(3)]
        public int NumberSamples { get; set; }

        [Category("3.3 Time")]
        [Description("Tiempo de espera entre muestras en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeBetweenSamples { get; set; }

        [Category("3.3 Time")]
        [Description("Tiempo de espera antes de realizar la medida en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeReadMeasure { get; set; }

        [Category("3. Configuration")]
        [Description("Magnitud eléctrica a medir")]
        [DefaultValue(MagnitudsMultimeter.VoltDC)]
        public MagnitudsMultimeter MagnitudToMeasure { get; set; }

        [Category("3.2 MUX")]
        [Description("Entrada de la multimplexora a seleccionar")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string InputMultiplexora { get; set; }

        [Category("3.1 Output")]
        [Description("Salida digital de la torre a activar para realizar la medida")]
        public int? ActiveOutput { get; set; }

        [Category("3.1 Output")]
        [Description("Salida digital de la torre a desactivar para realizar la medida")]
        public int? DesactiveOutput { get; set; }

        [Category("3. Configuration")]
        [Description("Descripción del nombre con el que se registrara en la BBDD los resultados de este test point")]
        public string TestPointDescription { get; set; }

        [Category("3. Configuration")]
        [Description("Usar Inputs/ Ouputs (Para sobremesa)")]
        [DefaultValue(false)]
        public bool WithOutIO { get; set; }

        [Category("3. Configuration")]
        [Description("Usar Inputs/ Ouputs (Para sobremesa)")]
        [DefaultValue(false)]
        public bool WithAverage { get; set; }

        [Category("3. Configuration")]
        [Description("Factor resolución con la que se quiere leer el multímetro")]
        [DefaultValue(1)]
        public double NPLC { get; set; }

        [Category("3. Configuration")]
        [Description("Precisión de dígitos")]
        [DefaultValue(4)]
        public double PrecisionDigits { get; set; } 

        [Category("3. Configuration")]
        [Description("Rango")]
        [DefaultValue(0)]
        public double Rank { get; set; } 

        public override void Execute()
        {
            if (string.IsNullOrEmpty(TestPointDescription))
                TestPointDescription = "TESPOINTMEASURE_" + DateTime.Now.ToShortTimeString();
                InternalExecute();
        }

        public void InternalExecute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw(); ;

            string name = "";
            double min = 0;
            double max = 0;
            ParamUnidad unidad = ParamUnidad.SinUnidad;

            switch (MagnitudToMeasure)
            {
                case MagnitudsMultimeter.AmpAC:
                    name = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.A;
                    break;

                case MagnitudsMultimeter.AmpDC:
                    name = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.A;
                    break;

                case MagnitudsMultimeter.Resistencia:
                    name = testBase.Params.RES.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.RES.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.RES.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.Ohms;
                    break;

                case MagnitudsMultimeter.VoltAC:
                    name = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;

                case MagnitudsMultimeter.VoltDC:
                    name = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;
            }

            var adjustValueTestPoint = new AdjustValueDef(name, 0, min, max, 0, 0, unidad);

            if (PrecisionDigits == 0)
                PrecisionDigits = 4;

            var RangoSelec = max == 9999999 ? Rank : max;

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudToMeasure,
                DigitosPrecision = PrecisionDigits,
                NumMuestras = NumberSamples,
                NPLC = NPLC,
                Rango = Math.Abs(RangoSelec),
            };

            if (!WithOutIO)
            {

                if (ActiveOutput.HasValue)
                    tower.IO.DO.On(ActiveOutput.Value);

                if (DesactiveOutput.HasValue)
                    tower.IO.DO.Off(DesactiveOutput.Value);
            }

            if( string.IsNullOrEmpty(InputMultiplexora))
                InputMultiplexora = "NOTHING";

            var InputMultiplexoraValue = VariablesTools.ResolveValue(InputMultiplexora);

            var value = (InputMuxEnum)Enum.Parse(typeof(InputMuxEnum), InputMultiplexoraValue.ToString());

            testBase.TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                var result = tower.MeasureMultimeter(value, measure, WaitTimeReadMeasure, whitOutIO: WithOutIO);

                Logger.InfoFormat("{0} == {1}", name, result);

                testBase.Variables.AddOrUpdate(name, result);

                return result;

            }, DeleteFirst, NumberSamples, WaitTimeBetweenSamples, WithAverage);

            if (!WithOutIO)
            {
                if (ActiveOutput.HasValue)
                    tower.IO.DO.Off(ActiveOutput.Value);

                if (DesactiveOutput.HasValue)
                    tower.IO.DO.On(DesactiveOutput.Value);
            }
        }
    }

    public class TestPointMeasureDescription : ActionDescription
    {
        public override string Name { get { return "TestPointMeasure"; } }
        public override string Description { get { return "Acción genérica para medir con el Multímetro los puntos de test de las placas"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
