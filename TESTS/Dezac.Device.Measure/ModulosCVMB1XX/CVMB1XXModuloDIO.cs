﻿using Comunications.Utility;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    class CVMB1XXModuloDIO : DeviceBase<CVMB1XXModuloDIO>
    {
        public CVMB1XXModuloDIO()
        {
        }

        public CVMB1XXModuloDIO(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 115200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }
        
        public override void Dispose()
        {
            Modbus.Dispose();
        } 

        public void WriteOutputs(OutputState output1, OutputState output2, OutputState output3, OutputState output4, OutputState output5, OutputState output6, OutputState output7, OutputState output8)
        {
            Modbus.WriteInt32((ushort)Registers.OUTPUT_1, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_2, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_3, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_4, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_5, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_6, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_7, output1 == OutputState.ON ? 257 : 0);
            Modbus.WriteInt32((ushort)Registers.OUTPUT_8, output1 == OutputState.ON ? 257 : 0);
        }

        public void FlagTest()
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, true);
        }

        public bool ReadInputStatus()
        {
            return Modbus.Read<Input>().inputState;
        }

         
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.INPUT_STATUS)]
        public struct Input
        {
            private ushort _input1;
            private ushort _input2;
            private ushort _input3;
            private ushort _input4;
            private ushort _input5;
            private ushort _input6;
            private ushort _input7;
            private ushort _input8;

            public bool inputState { get { return _input1 == 0 && _input2 == 0 && _input3 == 0 && _input4 == 0 && _input5 == 0 && _input6 == 0 && _input7 == 0 && _input8 == 0; } }
        }

        public enum Registers
        {
            OUTPUT_1 = 0xC428, // 2 REGISTROS, UNO PARA INDICAR SALIDA MANUAL O AUTOMATICA Y OTRO PARA FORZAR EL VALOR
            OUTPUT_2 = 0xC43C,
            OUTPUT_3 = 0xC450,
            OUTPUT_4 = 0xC464,
            OUTPUT_5 = 0xC478,
            OUTPUT_6 = 0xC48C,
            OUTPUT_7 = 0xC4A0,
            OUTPUT_8 = 0xC4B4,
            MODULE_TYPE = 0x2918, // 14 REGISTRO
            INPUT_STATUS  = 0x2882, // 8 REGISTROS
            FLAG_TEST = 0X2AF9,
        }

        public enum OutputState
        {
            ON,
            OFF
        }
       
    }
}
