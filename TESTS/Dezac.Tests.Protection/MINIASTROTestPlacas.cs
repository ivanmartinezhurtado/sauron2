﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.02)]
    public class MINIASTROTestPlacas : TestBase
    {
  
        private const byte IN_RELE_C1 = 9;
        private const byte IN_RELE_C2 = 10;
        private const byte IN_RELE_C3 = 11;
        private const byte IN_DEVICE_PRESENCE = 14;

        private MINIASTRO miniAstro;
        private TowerBoard tower;

        public void TestInitialization()
        {
            miniAstro = new MINIASTRO(Comunicaciones.SerialPort);
            miniAstro.Midabus.PerifericNumber = Comunicaciones.Periferico;
           
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.IO.DO.Off(17);
            tower.LAMBDA.ApplyOff();

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[IN_DEVICE_PRESENCE])
                    return false;

                return true;

            }, "Error No se ha detectado el equipo o util abierto", 5, 500, 1500, false, false);
        }

        public void TestConsumptions()
        {
            tower.IO.DO.On(23);

            var powerSupply = Consignas.GetDouble(Params.V_DC.Null.EnVacio.Vnom.Name, 11.5, ParamUnidad.V);
            // Se comprueban los consumos antes de encender el equipo
            var configuration = new TestPointConfiguration()
            {
                Current = 0.25,
                Frecuency = 50,
                Iteraciones = 2,
                ResultName = "",
                TiempoIntervalo = 100,
                typePowerSource = TypePowerSource.LAMBDA,
                typeTestExecute = TypeTestExecute.OnlyActive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = powerSupply,
                WaitTimeReadMeasure = 500
            };
            this.TestConsumo(tower, configuration);


            Delay(1000, "Espera quitar alimentacion");
        }

        public void TestConsumptionsBateria()
        {
            tower.IO.DO.On(17);

            var powerSupply = Consignas.GetDouble(Params.V_DC.Null.Bateria.Vnom.Name, 3, ParamUnidad.V);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(powerSupply, 0.05);

            AdjustValueDef consumption = new AdjustValueDef(Params.I_DC.Null.Bateria.Name, 0, Params.I_DC.Null.Bateria.Min(), Params.I_DC.Null.Bateria.Max(), 0, 0, ParamUnidad.mA);

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.AmpDC,
                NumMuestras = 3,
                Rango = 0.002,
                DigitosPrecision = 6,
                NPLC = 1
            };

            TestMeasureBase(consumption, (s) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.NOTHING, measure, 2000);
            }, 1, 5, 2000);

            tower.IO.DO.Off(17);
        }

        public void TestCommunications()
        {
            SamplerWithCancel((p) =>
            {
                var firmwareVersion = miniAstro.ReadVersion();

                Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString(), ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

                Assert.AreEqual(firmwareVersion.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la versión de firmware incorrecta"));

                return true;
            }, "", 3, 500, 500, false, false);
        }

        public void TestOutputs()
        {
            var ouputsValue = new List<KeyValuePair<byte, MINIASTRO.Reles>>();
            ouputsValue.Add(new KeyValuePair<byte, MINIASTRO.Reles>(IN_RELE_C1, MINIASTRO.Reles.C1));
            ouputsValue.Add(new KeyValuePair<byte, MINIASTRO.Reles>(IN_RELE_C2, MINIASTRO.Reles.C2));
            ouputsValue.Add(new KeyValuePair<byte, MINIASTRO.Reles>(IN_RELE_C3, MINIASTRO.Reles.C3));

            foreach (KeyValuePair<byte, MINIASTRO.Reles> output in ouputsValue)
            {
                miniAstro.WriteOuput(output.Value, true);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 10, 200);

                miniAstro.WriteOuputsOFF();

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} activada despues de desactivarla", output.Value.ToString()), 10, 200);

                Resultado.Set(string.Format("RELE{0}", output.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestDisplay()
        {
            var IMAGE_PATH = @"\\SFSERVER01\Idp\Idp público\IMAGESOURCE\MINIASTRO\";

            var ouputsValue = new List<MINIASTRO.DisplayEnum>();
            ouputsValue.Add(MINIASTRO.DisplayEnum.SEGMENTOS_HORIZONTALES);
            ouputsValue.Add(MINIASTRO.DisplayEnum.SEGMENTOS_VERTICALES);
            ouputsValue.Add(MINIASTRO.DisplayEnum.TODOS_SEGEMENTOS);
            ouputsValue.Add(MINIASTRO.DisplayEnum.TODOS_SIMBOLOS);

            foreach (MINIASTRO.DisplayEnum output in ouputsValue)
            {

                var frm = this.ShowForm(() => new ImageView("TEST DISPLAY", IMAGE_PATH + output.ToString() + ".jpg"), MessageBoxButtons.OKCancel);
                try
                {
                    SamplerWithCancel((p) =>
                    {
                        miniAstro.WriteDisplay(output);
                        return frm.DialogResult != DialogResult.None;
                    }, "Error display incorrecto", 50, 1000, 100);
                }
                finally
                {
                    if (frm.DialogResult != DialogResult.OK)
                        throw new Exception("Error, Display incorrecto detectado por el operario");

                    this.CloseForm(frm);
                }

                Resultado.Set(string.Format("DISPLAY_{0}", output.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestKeyboard()
        {
            foreach (MINIASTRO.KeyBoardEnum key in Enum.GetValues(typeof(MINIASTRO.KeyBoardEnum)))
            {
                var imageView = new ImageView(string.Format("Pulse la tecla {0} y a continuación pulse aceptar", key.GetDescription()), string.Format("{0}\\{1}\\TECLADO_{2}.jpg", PathCameraImages.TrimEnd('\\'), miniAstro.GetType().Name, key.ToString()));

                var frm = this.ShowForm(() => imageView, MessageBoxButtons.RetryCancel);
                try
                {
                    SamplerWithCancel((s) =>
                    {
                        if (s == 40)
                        {
                            var diagResult = Shell.MsgBox("¿DESEA CONTINUAR CON EL TEST DE TECLADO?", "TEST DE TECLADO", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (diagResult == DialogResult.No)
                                throw new Exception("Error en la comprobación de tecla {0}");
                        }

                        var lastKeyPressed = miniAstro.ReadKeyBorad(); 

                        var teclaCorrecta = lastKeyPressed == key;

                        return teclaCorrecta;
                    }, string.Format("Error en la comprobación de tecla {0}, se detecta el pulsado de la tecla {1}", key.ToString(), key.ToString()), 100, 500, 1000);
                }
                finally
                {
                    this.CloseForm(frm);
                }

                Resultado.Set(string.Format("TECLA_{0}", key.ToString()), "OK" , ParamUnidad.SinUnidad);
            }
        }

        public void TestSetupDefault()
        {
            miniAstro.WriteFormatEEPROM();
            Resultado.Set("FORMAT_EEPROM", "OK", ParamUnidad.SinUnidad);
            Delay(6000, "");
            miniAstro.WriteFormatEEPROMVerification();
            Resultado.Set("FORMAT_EEPROM_VERIFICATION", "OK", ParamUnidad.SinUnidad);
            miniAstro.WriteTestCPU();
            Resultado.Set("TEST_CPU", "OK", ParamUnidad.SinUnidad);
            miniAstro.WriteInitializationTablaEventos();
            Resultado.Set("INIT_TABLA_EVENTOS", "OK", ParamUnidad.SinUnidad);
            miniAstro.WriteValidacionTablasEventos();
            Resultado.Set("TABLA_EVENTOS_VALIDACION", "OK", ParamUnidad.SinUnidad);
            miniAstro.WriteInitializationContadores();
            Resultado.Set("INIT_TABLA_CONTADORES", "OK", ParamUnidad.SinUnidad);
        }

        public void TestCalibrationClock()
        {
            tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 2.5);
            tower.HP53131A.PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 9);

            var resFreq = new AdjustValueDef() { Name = Params.FREQ.Other("CLOCK").Verificacion.Name, Min = Params.FREQ.Other("CLOCK").Verificacion.Min(), Max = Params.FREQ.Other("CLOCK").Verificacion.Max(), Unidad = ParamUnidad.Hz };

            tower.MUX.Connect(InputMuxEnum.IN2, Outputs.FreCH1);

            var temperaturaInit = miniAstro.ReadTemperature();
            logger.InfoFormat("Temperatura Inicial: {0}", temperaturaInit);

            TestMeasureBase(resFreq,
            (step) =>
            {
                miniAstro.WriteActivePulseFreq();

                Delay(500, "");

                var medidaFreqMed = tower.HP53131A.RunMeasure(HP53131A.Variables.FREQ, 8000, HP53131A.Channels.CH1);

                resFreq.Value = medidaFreqMed.Value;

                return medidaFreqMed.Value;

            }, 0, 2, 1000);

            miniAstro.WriteDesActivePulseFreq();

            var temperaturaFin = 0D;

            SamplerWithCancel((p) =>
            {
                temperaturaFin = miniAstro.ReadTemperature();
                logger.InfoFormat("Temperatura Final: {0}", temperaturaFin);
                return true;
            }, "Error al leer la temperatura", 3, 500, 1000, false, false);

            var TemperaturaResult = (temperaturaInit + temperaturaFin) / 2;
            logger.InfoFormat("Temperatura Media: {0}", TemperaturaResult);
            Resultado.Set("TEMPERATURA", TemperaturaResult, ParamUnidad.Grados);

            var Frecuencia = (1 + 0.000000039 * Math.Pow(TemperaturaResult - 25, 2)) * resFreq.Value;
            logger.InfoFormat("Frecuencia compensada Temperatura: {0}", Frecuencia);
            Resultado.Set(Params.FREQ.Other("CLOCK").TestPoint("CALCULADA").Name, Frecuencia, ParamUnidad.Hz);

            double G = 0;

            if(Frecuencia < 2048)
            {
                G = Frecuencia / (2048 - Frecuencia);
                G =  G + (2 ^ 31); //'Se fuerza a 1 el bit 31 de G cuando es negativo
            }
            else if (Frecuencia > 2048)
                G = Frecuencia / (Frecuencia - 2048);

            logger.InfoFormat("Ganacia reloj: {0}", G);
            Resultado.Set(Params.GAIN_FREC.Other("CLOCK").Name, G, ParamUnidad.Puntos);

            if (G < 40000)
                throw new Exception("Error, Ganacia de reloj fuera de margenes");
            else if (G > 31556600)
                G = 4294967295;

            miniAstro.WriteFrequenceClock(G);
            Resultado.Set("TEST_ADJUSTE_CLOCK", "OK", ParamUnidad.SinUnidad);
        }

        public void Costumization()
        {
            //Tabla CambioHorario
            miniAstro.WriteTablaCambioHorario();
            Resultado.Set("TABLA_CAMBIO_HORARIO", "OK", ParamUnidad.SinUnidad);
            Delay(2000, "");

            //TablaProgTeclado
            miniAstro.WriteTablaProgTeclado();
            Resultado.Set("TABLA_PROGRAMACION_TECLADO", "OK", ParamUnidad.SinUnidad);
            Delay(2000, "");

            //ConfiguracionUsuario
            miniAstro.WriteConfiguracionFabrica();
            Resultado.Set("CONFIGURACUION_FABRICA", "OK", ParamUnidad.SinUnidad);
            Delay(2000, "");

            miniAstro.WriteConfiguracionUsuario();
            Resultado.Set("CONFIGURACION_USARIO", "OK", ParamUnidad.SinUnidad);
            Delay(2000, "");

            //Escribir y leer NumSerie
            miniAstro.WriteSerialNumber(Convert.ToInt32(TestInfo.NumBastidor));
            Resultado.Set("GRABACION_NUMERO_SERIE", "OK", ParamUnidad.SinUnidad);
            Delay(2000, "");

            //Configuracion Hora
            miniAstro.WriteDateTime();
            Resultado.Set("GRABACION_PUESTA_HORA", "OK", ParamUnidad.SinUnidad);
            Delay(2000, "");

            //ResetCPU
            miniAstro.WriteResetCPU();
            Resultado.Set("RESET", "OK", ParamUnidad.SinUnidad);
            Delay(7000, "");
       
            var numserie = miniAstro.ReadSerialNumber();
            Assert.AreEqual(numserie, (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el numero de serie grabado no corresponde al leido"));

        }

        public void TestGPS()
        {
            var temp1 = miniAstro.WriteTestGPS();
            Delay(2000, "");
            var temp2 = miniAstro.WriteTestGPS();

            Assert.IsTrue(temp2 != temp1, Error().UUT.COMUNICACIONES.GPS("Error GPS no comunica"));
            Resultado.Set("TEST_GPS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestComunicationIR()
        {


        }


        public void TestFinish()
        {
            if (miniAstro != null)
                miniAstro.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(17);
                tower.ShutdownSources();
                tower.Dispose();
            }
        }
    }
}
