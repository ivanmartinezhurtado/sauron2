﻿using Comunications.Utility;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class CVMB1XXModuloModBusTCP : DeviceBase
    {
        public ModbusDeviceTCP tcp;

        public CVMB1XXModuloModBusTCP()
        {
        }

        public CVMB1XXModuloModBusTCP(string hostname, int tcpPort, ModbusTCPProtocol modbusTCPProtocol)
        {
            SetTCP(hostname, tcpPort, modbusTCPProtocol);
        }

        public void SetTCP(string hostname, int tcpPort, ModbusTCPProtocol modbusTCPProtocol)
        {
            tcp = new ModbusDeviceTCP(hostname, tcpPort, modbusTCPProtocol);
        }

        public CVMB1XX.MAC ReadMAC()
        {
            LogMethod();
            return tcp.Read<CVMB1XX.MAC>();
        }

        public void WriteMACTCP(string mac)
        {
            LogMethod();

            var macReading = new CVMB1XX.MAC();
            var macArray = ModbusUtility.HexToBytes(mac);
            macReading.Field_1 = macArray[0];
            macReading.Field_2 = macArray[1];
            macReading.Field_3 = macArray[2];
            macReading.Field_4 = macArray[3];
            macReading.Field_5 = macArray[4];
            macReading.Field_6 = macArray[5];

            tcp.Write<CVMB1XX.MAC>(macReading);
        }

        public int ReadLine1Voltage()
        {
            LogMethod();
            return tcp.ReadInt32((ushort)Registers.L1_VOLTAGE);
        }

        public string ReadNumSerie()
        {
            LogMethod();
            return tcp.ReadString((ushort)Registers.NUM_SERIE, 6);
        }

     
        public enum Registers
        {
            MAC = 0xF236, // 3 registros
            L1_VOLTAGE = 0X0000,
            NUM_SERIE = 0x2756,
        }
    }
}
