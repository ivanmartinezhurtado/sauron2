﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Comunications.Message
{
    public class FLUKEMessage : StringMessage
    {
        private readonly char COMMAND_SEPARATOR = ';';
        private static CultureInfo enUS = new CultureInfo("en-US");

        private readonly Dictionary<string, string> responses = new Dictionary<string, string> {
            { "!T", "<0T" }, { "!M", "<0M" },{ "!K", "<0K" }
        };

        public static FLUKEMessage Create(string format, params object[] args)
        {
            FLUKEMessage message = new FLUKEMessage { Format = format };

            message.SetArgs(enUS, args);

            return message;
        }

        public int NumCommands
        {
            get { return this.Text.Split(COMMAND_SEPARATOR).Length; }
        }

        public string[] Commands
        {
            get
            {
                return this.Text.Split(COMMAND_SEPARATOR);
            }
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            if (Validator != null)
            {
                if (!Validator((StringMessage)response))
                    throw new FormatException("Error la respuesta del FLUKE tiene un formato incorrecto");
                return;
            }

            string[] commands = Commands;
            string[] results = ((FLUKEMessage)response).Commands;

            string cmd = commands[0];
            bool valid = false;

            var res = cmd.Substring(0, 2);
            valid = responses.ContainsKey(res) && results[0].StartsWith(responses[res]);

            if (!valid)
                throw new FormatException(string.Format("Error ejecutando el comando {0} (Resp.: {1})", cmd, results[0]));
        }
    }
}
