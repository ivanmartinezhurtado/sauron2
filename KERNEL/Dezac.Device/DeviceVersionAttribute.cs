﻿using System;

namespace Dezac.Device
{
    public class DeviceVersionAttribute : Attribute
    {
        public double Version { get; set; }

        public DeviceVersionAttribute()
        {

        }

        public DeviceVersionAttribute(double version)
        {
            Version = version;
        }
    }
}
