﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using log4net;
using System.Collections.Generic;

namespace Dezac.Tests.Extensions
{
    public static class PowerSourceIIIStabilizationExtension
    {
        public class PowerSourceIIIConfiguration
        {
            public double V_L1 { get; set; }
            public double V_L2 { get; set; }
            public double V_L3 { get; set; }
            public double I_L1 { get; set; }
            public double I_L2 { get; set; }
            public double I_L3 { get; set; }
            public double PF_L1 { get; set; }
            public double PF_L2 { get; set; }
            public double PF_L3 { get; set; }
            public double Gap_L1 { get; set; }
            public double Gap_L2 { get; set; }
            public double Gap_L3 { get; set; }
            public double Frequency { get; set; }
            public bool InternalStabilization { get; set; }
            public double Tolerance { get; set; }
            public int Timeout { get; set; }
            public int InitDelay { get; set; }
            public int Samples { get; set; }
            public int TimeSamples { get; set; }
            public int MaxSamples { get; set; }
            public double VoltageTolerance { get; set; }
            public double VoltageMaxVariance { get; set; }
            public double CurrentTolerance { get; set; }
            public double CurrentMaxVariance { get; set; }
            public double PFDegreeTolerance { get; set; }
            public double PFDegreeMaxVariance { get; set; }
            public double GapDegreeTolerence { get; set; }
            public double GapDegreeMaxVariance { get; set; }
            public double FrequencyTolerance { get; set; }
            public double FrequencyMaxVaraince { get; set; }
            public string TestPoint { get; set; }
            public bool UseWatimetro { get; set; }
            public bool Output120A { get; set; }
        }

        public static void PowerSourceIIIStabilization(this TestBase testBase, ITower tower, PowerSourceIIIConfiguration configuration)
        {
            var voltage = new TriLineValue()
            {
                L1 = testBase.Consignas.GetDouble(testBase.Params.V.L1.TestPoint(configuration.TestPoint.ToString()).Name, configuration.V_L1, ParamUnidad.V),
                L2 = testBase.Consignas.GetDouble(testBase.Params.V.L2.TestPoint(configuration.TestPoint.ToString()).Name, configuration.V_L2, ParamUnidad.V),
                L3 = testBase.Consignas.GetDouble(testBase.Params.V.L3.TestPoint(configuration.TestPoint.ToString()).Name, configuration.V_L3, ParamUnidad.V),
            };

            var current = new TriLineValue()
            {
                L1 = testBase.Consignas.GetDouble(testBase.Params.I.L1.TestPoint(configuration.TestPoint.ToString()).Name, configuration.I_L1, ParamUnidad.A),
                L2 = testBase.Consignas.GetDouble(testBase.Params.I.L2.TestPoint(configuration.TestPoint.ToString()).Name, configuration.I_L2, ParamUnidad.A),
                L3 = testBase.Consignas.GetDouble(testBase.Params.I.L3.TestPoint(configuration.TestPoint.ToString()).Name, configuration.I_L3, ParamUnidad.A),
            };

            var frequency = testBase.Consignas.GetDouble(testBase.Params.FREQ.Null.TestPoint(configuration.TestPoint.ToString()).Name, configuration.Frequency, ParamUnidad.Hz);

            var pfAngle = new TriLineValue()
            {
                L1 = testBase.Consignas.GetDouble(testBase.Params.PF.L1.TestPoint(configuration.TestPoint.ToString()).Name, configuration.PF_L1, ParamUnidad.Grados),
                L2 = testBase.Consignas.GetDouble(testBase.Params.PF.L2.TestPoint(configuration.TestPoint.ToString()).Name, configuration.PF_L2, ParamUnidad.Grados),
                L3 = testBase.Consignas.GetDouble(testBase.Params.PF.L3.TestPoint(configuration.TestPoint.ToString()).Name, configuration.PF_L3, ParamUnidad.Grados),
            };

            var gap = new TriLineValue()
            {
                L1 = testBase.Consignas.GetDouble(testBase.Params.ANGLE_GAP.L1.TestPoint(configuration.TestPoint.ToString()).Name, configuration.Gap_L1, ParamUnidad.Grados),
                L2 = testBase.Consignas.GetDouble(testBase.Params.ANGLE_GAP.L2.TestPoint(configuration.TestPoint.ToString()).Name, configuration.Gap_L2, ParamUnidad.Grados),
                L3 = testBase.Consignas.GetDouble(testBase.Params.ANGLE_GAP.L3.TestPoint(configuration.TestPoint.ToString()).Name, configuration.Gap_L3, ParamUnidad.Grados),
            };

            tower.PowerSourceIII.Tolerance = configuration.Tolerance;
            tower.PowerSourceIII.TimeOutStabilitation = configuration.Timeout;

            if (configuration.InternalStabilization)
            {
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, frequency, pfAngle, gap, true);
                return;
            }

            tower.PowerSourceIII.ApplyPresets(voltage, current, frequency, pfAngle, gap);

            var defs = new ListAdjustValueDef("PowerSource");
            defs.Add(testBase.Params.V.L1.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.V, false, voltage.L1, configuration.VoltageTolerance, configuration.VoltageMaxVariance);
            defs.Add(testBase.Params.V.L2.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.V, false, voltage.L2, configuration.VoltageTolerance, configuration.VoltageMaxVariance);
            defs.Add(testBase.Params.V.L3.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.V, false, voltage.L3, configuration.VoltageTolerance, configuration.VoltageMaxVariance);

            defs.Add(testBase.Params.I.L1.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.A, false, current.L1, configuration.CurrentTolerance, configuration.CurrentMaxVariance);
            defs.Add(testBase.Params.I.L2.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.A, false, current.L2, configuration.CurrentTolerance, configuration.CurrentMaxVariance);
            defs.Add(testBase.Params.I.L3.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.A, false, current.L3, configuration.CurrentTolerance, configuration.CurrentMaxVariance);

            if (configuration.PFDegreeTolerance != 0)
            {
                defs.Add(testBase.Params.PF.L1.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.SinUnidad, true, pfAngle.L1, configuration.PFDegreeTolerance, configuration.PFDegreeMaxVariance);
                defs.Add(testBase.Params.PF.L2.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.SinUnidad, true, pfAngle.L2, configuration.PFDegreeTolerance, configuration.PFDegreeMaxVariance);
                defs.Add(testBase.Params.PF.L3.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.SinUnidad, true, pfAngle.L3, configuration.PFDegreeTolerance, configuration.PFDegreeMaxVariance);
            }

            if (configuration.GapDegreeTolerence != 0)
            {
                defs.Add(testBase.Params.ANGLE_GAP.L1.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.Grados, true, gap.L1, configuration.GapDegreeTolerence, configuration.GapDegreeMaxVariance);
                defs.Add(testBase.Params.ANGLE_GAP.L2.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.Grados, true, gap.L2, configuration.GapDegreeTolerence, configuration.GapDegreeMaxVariance);
                defs.Add(testBase.Params.ANGLE_GAP.L3.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.Grados, true, gap.L3, configuration.GapDegreeTolerence, configuration.GapDegreeMaxVariance);
            }

            defs.Add(testBase.Params.FREQ.Null.Verificacion.TestPoint(configuration.TestPoint.ToString()), ParamUnidad.Hz, false, frequency, configuration.FrequencyTolerance, configuration.FrequencyMaxVaraince);

            var controlSampler = new ConfigurationSampler() { SampleNumber = configuration.Samples, SampleMaxNumber = configuration.MaxSamples, DelayBetweenSamples = configuration.TimeSamples, InitDelayTime = configuration.InitDelay };

            if (configuration.UseWatimetro)
            {
                if (configuration.Output120A || (current.L1 > 12 || current.L2 > 12 || current.L3 > 12))
                    tower.MTEWAT.Output120A = true;          
                else
                    tower.MTEWAT.Output120A = false;
            }

            testBase.TestCalibrationWithVariance(defs,
            () =>
            {
                if (configuration.UseWatimetro)
                {
                    var varsList = new List<double>();
                    var measure = tower.MTEWAT.ReadAll();
                    varsList.AddRange(measure.Voltage.ToArray());
                    varsList.AddRange(measure.Current.ToArray());

                    if (configuration.PFDegreeTolerance != 0)
                    {
                        var pfWatt = measure.PhasesAngleVoltage - measure.PhasesAngleCurrent;
                        varsList.AddRange(pfWatt.ToArray());
                    }

                    if (configuration.GapDegreeTolerence != 0)
                        varsList.AddRange(measure.PhasesAngleVoltage.ToArray());

                    varsList.Add(measure.Freq);
                    return varsList.ToArray();
                }
                else
                {
                    CVMB100Tower.VariablesInstantaneas ReadMedidor;
                    CVMB100Tower.VariablesTrifasicas ReadMedidorIII;
                    CVMB100Tower.VariablesAngulos ReadAngulos;

                    if (current.L1 > 12 || current.L2 > 12 || current.L3 > 12)
                    {
                        ReadMedidor = tower.CvmB100Hi.ReadVariablesInstantaneas();
                        ReadMedidorIII = tower.CvmB100Hi.ReadVariablesTrifasicas();
                        ReadAngulos = tower.CvmB100Hi.ReadAngulos();
                    }
                    else
                    {
                        ReadMedidor = tower.CvmB100Low.ReadVariablesInstantaneas();
                        ReadMedidorIII = tower.CvmB100Low.ReadVariablesTrifasicas();
                        ReadAngulos = tower.CvmB100Low.ReadAngulos();
                    }

                    var varsList = new List<double>() {
                    ReadMedidor.L1.Tension, ReadMedidor.L2.Tension, ReadMedidor.L3.Tension,
                    ReadMedidor.L1.Corriente,   ReadMedidor.L2.Corriente ,   ReadMedidor.L3.Corriente };

                    testBase.Logger.InfoFormat("Voltage -> L1= {0} L2={1} L3={2}", ReadMedidor.L1.Tension, ReadMedidor.L2.Tension, ReadMedidor.L3.Tension);
                    testBase.Logger.InfoFormat("Current -> L1= {0} L2={1} L3={2}", ReadMedidor.L1.Corriente, ReadMedidor.L2.Corriente, ReadMedidor.L3.Corriente);
                    testBase.Logger.InfoFormat("PF -> V1I1= {0} V2I2={1} V3I3={2}", ReadAngulos.AnguloV1I1, ReadAngulos.AnguloV3I3, ReadAngulos.AnguloV3I3);
                    testBase.Logger.InfoFormat("Desfase Read-> V1V2= {0} V2V3={1} V3V1={2}", ReadAngulos.AnguloV1V2, ReadAngulos.AnguloV2V3, ReadAngulos.AnguloV3V1);
                    testBase.Logger.InfoFormat("Desfase Calc-> V1V2= {0} V2V3={1} V3V1={2}", ReadAngulos.AnguloV1V2 - gap.L2, ReadAngulos.AnguloV2V3, ReadAngulos.AnguloV3V1 + gap.L2);

                    if (configuration.PFDegreeTolerance != 0)
                        varsList.Add(ReadAngulos.AnguloV1I1, ReadAngulos.AnguloV2I2, ReadAngulos.AnguloV3I3);

                    if (configuration.GapDegreeTolerence != 0)
                        varsList.Add(ReadAngulos.AnguloV1V2 - gap.L2, ReadAngulos.AnguloV2V3, ReadAngulos.AnguloV3V1 + gap.L2);

                    varsList.Add(ReadMedidorIII.Frecuencia);

                    return varsList.ToArray();
                }
            },
            () =>
            {
                var varsList = new List<double>();
                varsList.AddRange(voltage.ToArray());
                varsList.AddRange(current.ToArray());

                if (configuration.PFDegreeTolerance != 0)
                    varsList.AddRange(pfAngle.ToArray());

                if (configuration.GapDegreeTolerence != 0)
                    varsList.AddRange(gap.ToArray());

                varsList.Add(frequency);
                return varsList.ToArray();

            }, controlSampler);
        }
    }
}
