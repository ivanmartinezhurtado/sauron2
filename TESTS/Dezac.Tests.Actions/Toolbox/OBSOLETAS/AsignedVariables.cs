﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(AsignedVariablesActionDescription))]
    public class AsignedVariablesAction : ActionBase
    {
        [Description("Instancia/ hilo (Equipo en paralelo) de las variables a asignar")]
        [Category("3. Configuration")]
        public int? Instance { get; set; } 

        [Description("Nombre de las variables a obtener su valor y asignarlo a otra variable")]
        [Category("4. Results")]
        public VariableAssigned[] DataOutput { get; set; } 

        public override void Execute()
        {
            var testContext = SequenceContext.Current.Services.Get<ITestContext>();
            var NumInstance = SequenceContext.Current.NumInstance;
            if (!Instance.HasValue)
                Instance = 1;

            if (NumInstance == Instance.Value)
            {
                if (DataOutput != null)
                {
                    foreach (var item in DataOutput)
                    {
                        var variableLoad = VariablesTools.ResolveValue(item.GetData);
                        Logger.InfoFormat("Variable:{0}  ==  Value:{1}  --> Assigned To: {2}", item.GetData, variableLoad, item.SetData);
                        VariablesTools.SetVariable(item.SetData, variableLoad, false);
                        var valueResult = VariablesTools.ResolveValue(item.SetData);
                        Logger.InfoFormat("Variable:{0}  Assigned Value:{1}", item.SetData, valueResult);
                    }
                }
            }
        }


    }

    public class VariableAssigned
    {
        [Description("Nombre de la variable a obtener su valor")]
        [Category("Entrada")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string GetData { get; set; } //ToDo Miki: Cambiado de GetVariable a GetData

        [Description("Nombre de la variable a asignar el valor obtenido en GetVariable")]
        [Category("Salida")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SetData { get; set; } //ToDo Miki: Cambiado de SetVariable a SetData
    }

    public class AsignedVariablesActionDescription : ActionDescription
    {
        public override string Description { get { return "Este action sirve para asignar las variables de contexto del test, de TestInfo o de la base se datos de Dezac a variables temporales de test para reutilizar su valor";  } }
        public override string Name { get { return "AssignedVariables"; }}
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
