﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(Get_RandomDescription))]
    public class Get_Random : ActionBase
    {
        [Description("Valor que asignará como mínimo al valor del Random. Miau")]
        [Category("3. Configuration")]
        public ActionVariable DataInputMin { get; set; } = new ActionVariable();

        [Description("Valor que asignará como máximo al valor del Random")]
        [Category("3. Configuration")]
        public ActionVariable DataInputMax { get; set; } = new ActionVariable();
        
        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable(); 
        public override void Execute()
        {
            var min = 0;
            var max = 0;

            if (!int.TryParse(DataInputMin.Value.ToString(), out min))
                TestException.Create().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE("Valor de la propiedad DataInputMin no es numérico").Throw();

            if (!int.TryParse(DataInputMax.Value.ToString(), out max))
                TestException.Create().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE("Valor de la propiedad DataInputMax no es numérico").Throw();


            var rdm = new Random();
            var value = rdm.Next(min, max);
            Logger.InfoFormat("{0} Random -> = {1}", DataOutput, value.ToString());
            Context.ResultList.Add(string.Format("{0} = {1}", DataOutput, value));
            DataOutput.Value = value;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();
        }

        public class Get_RandomDescription : ActionDescription
        {
            public override string Name { get { return "Random"; } }
            public override string Category { get { return "Actions/Math"; } }
            public override string Description { get { return "Devuelve un valor aleatorio según los parámetros de configuración máximos y mínimos"; } }
            public override Image Icon { get { return Properties.Resources.Math; } }
            public override string Dependencies { get { return "InitTestBase"; } }

        }
    }
}
