namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_PARAMETRO
    {
        public T_PARAMETRO()
        {
            this.T_TESTVALOR = new HashSet<T_TESTVALOR>();
            this.T_VALORPARAMETRO = new HashSet<T_VALORPARAMETRO>();
            this.T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
        }
    
        public int NUMPARAM { get; set; }
        public int NUMTIPOPARAM { get; set; }
        public int NUMUNIDAD { get; set; }
        public int NUMTIPOVALOR { get; set; }
        public string PARAM { get; set; }
        public short NIVELSEGURIDAD { get; set; }
        public string METROLOGIASINO { get; set; }
        public string DESCRIPCION { get; set; }
    
        public virtual T_TIPOPARAMETRO T_TIPOPARAMETRO { get; set; }
        public virtual T_TIPOVALORPARAMETRO T_TIPOVALORPARAMETRO { get; set; }
        public virtual T_UNIDAD T_UNIDAD { get; set; }
        public virtual ICollection<T_TESTVALOR> T_TESTVALOR { get; set; }
        public virtual ICollection<T_VALORPARAMETRO> T_VALORPARAMETRO { get; set; }
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }

        public string TestPoint
        {
            get
            {
                return string.IsNullOrEmpty(DESCRIPCION) ? PARAM : DESCRIPCION;
            }
        }
    }
}
