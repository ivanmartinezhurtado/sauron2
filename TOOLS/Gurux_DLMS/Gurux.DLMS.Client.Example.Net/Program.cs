using Gurux.Common;
using Gurux.DLMS.Enums;
using Gurux.DLMS.Secure;
using Gurux.Serial;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;

namespace Gurux.DLMS.Client.Example
{
    public class Settings
    {
        public IGXMedia media = null;
        public TraceLevel trace = TraceLevel.Verbose;
        public bool iec = false;
        public GXDLMSSecureClient client = new GXDLMSSecureClient(true);
        //Objects to read.
        public List<KeyValuePair<string, int>> readObjects = new List<KeyValuePair<string, int>>();
    }

    class Program
    {
        static int Main(string[] args)
        {
            Settings settings = new Settings();
            Reader.GXDLMSReader reader = null;
            try
            {
                var LogicalAddress = 1;
                var PhysicalAddress = 16;

                settings.client.Password = ASCIIEncoding.ASCII.GetBytes("qxzbravo");
                settings.client.UseLogicalNameReferencing = true;
                settings.client.ClientAddress = 73;
                settings.client.ServerAddress = GXDLMSClient.GetServerAddress(LogicalAddress, Convert.ToInt32(PhysicalAddress));
                settings.client.Authentication = Authentication.Low;

                GXSerial serial = new GXSerial();
                serial.PortName = "COM9";
                serial.BaudRate = 115200;
                serial.DataBits = 8;
                serial.Parity = Parity.None;
                serial.StopBits = StopBits.One;
                settings.media = serial;
                
                settings.media.OnTrace += (s, e) => Console.WriteLine($"{e.Type}-{e.DataToString(false)}");
                
                reader = new Reader.GXDLMSReader(settings.client, settings.media, settings.trace);
                
                settings.media.Open();
                if (settings.readObjects.Count != 0)
                {
                    reader.InitializeConnection();
                    reader.GetAssociationView(false);
                    foreach (KeyValuePair<string, int> it in settings.readObjects)
                    {
                        object val = reader.Read(settings.client.Objects.FindByLN(ObjectType.None, it.Key), it.Value);
                        reader.ShowValue(val, it.Value);
                    }
                }
                else
                {
                    reader.ReadAll(false);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.ToString());
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    Console.ReadKey();
                }
                return 1;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    Console.WriteLine("Ended. Press any key to continue.");
                    Console.ReadKey();
                }
            }
            return 0;
        }
    }
}
