﻿using Dezac.Tests.Model;
using System;

namespace Dezac.Tests.Parameters
{

    public class ExtensionParameters
    {
        private ParamValueCollection context;

        public string poweSourceLevel { internal get; set; }
        public string variable { internal get; set; }
        public string testPoint { internal get; set; }
        public string Fase { internal get; set; }
        public ParamUnidad Unidad { internal get; set; }

        public string Name { get { return variable + Fase + testPoint + poweSourceLevel; } }

        public double Min(double defaultValue = -9999999)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + poweSourceLevel + "_MIN", defaultValue, Unidad));
        }

        public double Max(double defaultValue = 9999999)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + poweSourceLevel + "_MAX", defaultValue, Unidad));
        }

        public double Avg(double defaultValue = 1)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + poweSourceLevel + "_AVG", defaultValue, Unidad));
        }

        public double Tol(double defaultValue = 5)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + poweSourceLevel + "_TOL", defaultValue,  ParamUnidad.PorCentage));
        }

        public double Var(double defaultValue = 999)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + poweSourceLevel + "_VAR", defaultValue, ParamUnidad.SinUnidad));
        }

        public ExtensionParameters(ParamValueCollection contextmodel)
        {
            context = contextmodel;
        }
    }
}
