namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTFASE")]
    public partial class T_TESTFASE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_TESTFASE()
        {
            REPROCESOFAB = new HashSet<REPROCESOFAB>();
            T_TESTERROR = new HashSet<T_TESTERROR>();
            T_TESTFASEFILE = new HashSet<T_TESTFASEFILE>();
            T_TESTSTEP = new HashSet<T_TESTSTEP>();
            T_TESTVALOR = new HashSet<T_TESTVALOR>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTESTFASE { get; set; }

        public int NUMTEST { get; set; }

        [Required]
        [StringLength(10)]
        public string IDFASE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public DateTime FECHA { get; set; }

        public int TIEMPO { get; set; }

        [Required]
        [StringLength(1)]
        public string RESULTADO { get; set; }

        [StringLength(200)]
        public string NOTAS { get; set; }

        public int? NUMMARCAJE { get; set; }

        public int? NUMCAJA { get; set; }

        [StringLength(15)]
        public string NOMBREPC { get; set; }

        public int? NUMREINTENTOS { get; set; }

        public virtual DIARIOMARCAJEFASE DIARIOMARCAJEFASE { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPROCESOFAB> REPROCESOFAB { get; set; }

        public virtual T_TEST T_TEST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTERROR> T_TESTERROR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASEFILE> T_TESTFASEFILE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTSTEP> T_TESTSTEP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTVALOR> T_TESTVALOR { get; set; }
    }
}
