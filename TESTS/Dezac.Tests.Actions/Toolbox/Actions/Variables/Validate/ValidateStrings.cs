﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ValidateStringsDescription))]
    public class ValidateStrings : ActionBase
    {
        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInput { get; set; } 
 
        [Description("Descripción de las unidades de la variable a obtener")]
        [Category("4. Results")]
        public ParamUnidad Unit { get; set; }

        [Description("Nombre de la cadena con la que queremos comparar la cadena de entrada")]
        [Category("3.1 Validation")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string StringToCompare { get; set; }

        [Description("Tipo de comparación que se quiere realizar, puede ser una comparación a una cadena igual o diferente")]
        [Category("3.1 Validation")]
        [DefaultValue(TypeCompare.Equal)]
        public TypeCompare TypeOfCompare { get; set; }

        [Description("Longitud máxima de la cadena que se quiere validar")]
        [Category("3.1 Validation")]
        public int? MaxLenght { get; set; }

        [Description("Longitud mínima de la cadena que se quiere validar")]
        [Category("3.1 Validation")]
        public int? MinLenght { get; set; }

        [Description("Validación para saber si la cadena es numérica Decimal")]
        [Category("3.1 Validation")]
        public bool IsNumeric { get; set; }

        [Description("Validación para saber si la cadena es de letras")]
        [Category("3.1 Validation")]
        public bool IsAlfa { get; set; }

        [Description("Validación para saber si la cadena es alfanumérica")]
        [Category("3.1 Validation")] 
        public bool IsAlfaNumeric { get; set; }

        [Description("Validación para saber si la cadena es numérica Hexadecimal")]
        [Category("3.1 Validation")]
        public bool IsHexdecimal { get; set; }

        [Description("Campo donde se ha de escribir la Expresión regular de la cadena con la que queremos comparar")]
        [Category("3.1 Validation")]
        public string RegularExpresion { get; set; }

        //TODO: Estos enumerados deberian estar en un namespace comun y no dentro del action
        //ToDo Miki: En StringFormat también estan los enumerados dentro del action
        public enum TypeCompare
        {
            Equal,
            Different
        }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            if (!DataInput.Trim().StartsWith("$"))
                DataInput = "$" + DataInput.Trim();

            string value;
            if (Unit != 0)
                value = VariablesTools.ResolveValue(DataInput, null, Unit).ToString().Trim();
            else
                value = VariablesTools.ResolveValue(DataInput).ToString().Trim();

            var name = DataInput.Trim().Replace("$", "");

            if (MaxLenght.HasValue)
            {
                if (value.Length > MaxLenght)
                {
                    testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("LONGITUD MAXIMA " + name).Throw();
                }
                Logger.InfoFormat("{0} -> {1} Lenght <= {2} Success!!", name, value, MaxLenght);
            }

            if (MinLenght.HasValue)
            {
                if (value.Length < MinLenght)
                {
                    testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("LONGITUD MINIMA " + name).Throw();
                }
                Logger.InfoFormat("{0} -> {1} Lenght >= {2} Success!!", name, value, MinLenght);
            }

            if (IsNumeric)
            {
                var regex = new Regex(@"^\d+$");
                if (!regex.IsMatch(value))
                {
                    testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("NO NUMERICO " + name).Throw();
                }
                Logger.InfoFormat("{0} -> {1} IsNumeric Success!!", name, value);
            }

            if (IsAlfa)
            {
                var regex = new Regex(@"^[a-zA-Z]*$");
                if (!regex.IsMatch(value))
                {
                   testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("NO ALFANUMERICO " + name).Throw();
                }

                Logger.InfoFormat("{0} -> {1} IsAlfaNumeric Success!!", name, value);
            }

            if (IsAlfaNumeric)
            {
                var regex = new Regex(@"^[a-zA-Z0-9]*$");
                if (!regex.IsMatch(value))
                {
                    testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("NO NUMERICO NI ALFANUMERICO " + name).Throw();
                }               
                Logger.InfoFormat("{0} -> {1} IsAlfaNumericNumeric Success!!", name, value);
            }

            if (IsHexdecimal)
            {
                var regex = new Regex(@"^[a-fA-F0-9]*$");
                if (!regex.IsMatch(value))
                {
                    testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("NO HEXDECIMAL " + name).Throw();
                }
                Logger.InfoFormat("{0} -> {1} IsHexdecimal Success!!", name, value);
            }

            if (!string.IsNullOrEmpty(RegularExpresion))
            {
                var rgx = new Regex(RegularExpresion);
                if (!rgx.IsMatch(value))
                {
                    testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                    Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("NO CUMPLE EXPRESION REGULAR " + name).Throw();
                }
                Logger.InfoFormat("{0} -> {1} IsMatch {2}  Success!!", name, value, RegularExpresion);
            }

            if (!string.IsNullOrEmpty(StringToCompare))
            {
                var compare = VariablesTools.ResolveValue(StringToCompare.Trim()).ToString().Trim();

                Logger.InfoFormat("{0} -> {1} == {2}", name, value, compare);

                testBase.Resultado.Set(name, value.Trim(), Unit, compare);

                if (TypeOfCompare == TypeCompare.Equal)
                {
                    if (value != compare)
                    {
                       testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                       Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(name).Throw();
                    }
                }
                else
                {
                    if (value == compare)
                    {
                       testBase.Resultado.Set("RESULT_" + name, "ERROR", Unit);

                        Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(name).Throw();
                    }
                }

                Logger.InfoFormat("{0} -> {1} == {2}  Success!!", name, value, compare);

                testBase.Resultado.Set("RESULT_" + name, "OK", Unit);
            }
        }
    }

    public class ValidateStringsDescription : ActionDescription
    {
        public override string Name { get { return "ValidateStrings"; } }
        public override string Description { get { return "Acción genérica para comparar variables o resultados tipo string"; } }
        public override string Category { get { return "Actions/Variables/Validate"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
