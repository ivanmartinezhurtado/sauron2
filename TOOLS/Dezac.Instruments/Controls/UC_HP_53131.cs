﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Measure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(HP53131Description))]
    public partial class UC_HP_53131 : UserControl, IDisposable
    {
        private HP53131A hp;
        private HP53131A.Variables variable;

        public Type InstrumentType
        {
            get
            {
                return typeof(HP53131A);
            }
        }


        public HP53131A Instrument
        {
            get
            {
                if (hp == null)
                    hp = new HP53131A();

                return hp;
            }
            set
            {
                if (hp != null)
                {
                    hp.Dispose();
                    hp = null;
                }

                hp = value;
            }
        }

        public UC_HP_53131()
        {
            InitializeComponent();

            string[] Adress=new string[]{"ADDR1","ADDR2","ADDR3"};

            cmb_Port.Items.AddRange(Adress);
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("ADDR" + Instrument.PortAdress.ToString());

            chkFRECCH1.Checked = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (cmb_Port.SelectedItem == null)
                errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
            else
                try
                {
                    Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
                    ToggleButtonsState((Button)sender);
                    Instrument.SCPI.Reset.Command();
                    ToggleButtonsState((Button)sender);
                }
                catch (Exception ex)
                {
                    Logger.Error("Error al realizar reset al HP53131", ex);
                }
        }

       private void btnClear_Click(object sender, EventArgs e)
       {
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
               try
               {
                   Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
                   ToggleButtonsState((Button)sender);
                   Instrument.SCPI.Clear.Command();
                   ToggleButtonsState((Button)sender);
               }
               catch (Exception ex)
               {
                   Logger.Error("Error al realizar clear al HP53131", ex);
               }
       }

       private void btnRun_Click(object sender, EventArgs e)
       {
           if (cmb_Port.SelectedItem == null)
               errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
           else
               try
               {
                   errorProvider1.Clear();

                   Instrument.PortAdress = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("ADDR", ""));
                   ToggleButtonsState((Button)sender);

                   var iden = Instrument.SCPI.Identification.Query();
                   lbl_Vers_tester.Text = iden.ToString();

                   var result = Instrument.RunMeasure(variable, 20000);
                   lbl_Mesur_Mult.Text = result.ToString();

                   var status = Instrument.SCPI.StandardEventStatus.Query();

                   ToggleButtonsState((Button)sender);

               }
               catch (Exception ex)
               {
                   Logger.Error("Error al realizar la medida con el HP53131", ex);
               }
       }

       private bool ConfigImpedancia(HP53131A.Channels Canal)
       {
           var Acoplo = new HP53131A.Impedancia.CoupEnum();
           var Atenuacion = new HP53131A.Impedancia.AttenEnum();
           var Filtro = new HP53131A.Impedancia.FilterEnum();
           var LeveImpedancia = new HP53131A.Impedancia.ImpedanciaEnum();

           if (Canal == HP53131A.Channels.CH1)
           {
               if (Imped1.SelectedItem != null)
                   switch (Imped1.SelectedItem.ToString().Trim())
                   {
                       case "1M":
                           LeveImpedancia = (HP53131A.Impedancia.ImpedanciaEnum)1000000;
                           break;
                       case "50":
                           LeveImpedancia = (HP53131A.Impedancia.ImpedanciaEnum)50;
                           break;
                       default:
                           errorProvider1.SetError(Imped1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Imped1, "Selected Item incorrect");
                   return false;
               }

               if (Acoplo1.SelectedItem != null)
                   switch (Acoplo1.SelectedItem.ToString().Trim())
                   {
                       case "AC":
                           Acoplo = HP53131A.Impedancia.CoupEnum.AC;
                           break;
                       case "DC":
                           Acoplo = HP53131A.Impedancia.CoupEnum.DC;
                           break;
                       default:
                           errorProvider1.SetError(Acoplo1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Acoplo1, "Selected Item incorrect");
                   return false;
               }

               if (Atenua1.SelectedItem != null)
                   switch (Atenua1.SelectedItem.ToString().Trim())
                   {
                       case "x1":
                           Atenuacion = HP53131A.Impedancia.AttenEnum.X1;
                           break;
                       case "x10":
                           Atenuacion = HP53131A.Impedancia.AttenEnum.X10;
                           break;
                       default:
                           errorProvider1.SetError(Atenua1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Atenua1, "Selected Item incorrect");
                   return false;
               }

               if (Filtro1.SelectedItem != null)
                   switch (Filtro1.SelectedItem.ToString().Trim())
                   {
                       case "ON":
                           Filtro = HP53131A.Impedancia.FilterEnum.Filt_ON;
                           break;
                       case "OFF":
                           Filtro = HP53131A.Impedancia.FilterEnum.Filt_OFF;
                           break;
                       default:
                           errorProvider1.SetError(Filtro1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Filtro1, "Selected Item incorrect");
                   return false;
               }
           }
           else
           {
               if (Imped2.SelectedItem != null)
                   switch (Imped2.SelectedItem.ToString().Trim())
                   {
                       case "1M":
                           LeveImpedancia = (HP53131A.Impedancia.ImpedanciaEnum)1000000;
                           break;
                       case "50":
                           LeveImpedancia = (HP53131A.Impedancia.ImpedanciaEnum)50;
                           break;
                       default:
                           errorProvider1.SetError(Imped2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Imped2, "Selected Item incorrect");
                   return false;
               }

               if (Acoplo2.SelectedItem != null)
                   switch (Acoplo2.SelectedItem.ToString().Trim())
                   {
                       case "AC":
                           Acoplo = HP53131A.Impedancia.CoupEnum.AC;
                           break;
                       case "DC":
                           Acoplo = HP53131A.Impedancia.CoupEnum.DC;
                           break;
                       default:
                           errorProvider1.SetError(Acoplo2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Acoplo2, "Selected Item incorrect");
                   return false;
               }

               if (Atenua2.SelectedItem != null)
                   switch (Atenua2.SelectedItem.ToString().Trim())
                   {
                       case "x1":
                           Atenuacion = HP53131A.Impedancia.AttenEnum.X1;
                           break;
                       case "x10":
                           Atenuacion = HP53131A.Impedancia.AttenEnum.X10;
                           break;
                       default:
                           errorProvider1.SetError(Atenua2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Atenua2, "Selected Item incorrect");
                   return false;
               }

               if (Filtro2.SelectedItem != null)
                   switch (Filtro2.SelectedItem.ToString().Trim())
                   {
                       case "ON":
                           Filtro = HP53131A.Impedancia.FilterEnum.Filt_ON;
                           break;
                       case "OFF":
                           Filtro = HP53131A.Impedancia.FilterEnum.Filt_OFF;
                           break;
                       default:
                           errorProvider1.SetError(Filtro2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Filtro2, "Selected Item incorrect");
                   return false;
               }
               
           }
           errorProvider1.Clear();
           Instrument.PresetAndConfigImpendance(Canal, Acoplo, Atenuacion, Filtro, LeveImpedancia);    

           return true;
       }

       private bool ConfigTrigger(HP53131A.Channels Canal)
       {
           HP53131A.Trigger.SlopeEnum Pend = new HP53131A.Trigger.SlopeEnum();
           HP53131A.Trigger.LevelEnum Level = new HP53131A.Trigger.LevelEnum();
           HP53131A.Trigger.SensibilitiEnum Sens = new HP53131A.Trigger.SensibilitiEnum();
           double ValorMedio = 0;

           if (Canal == HP53131A.Channels.CH1)
           {
               if (Pend1.SelectedItem != null)
                   switch (Pend1.SelectedItem.ToString().Trim())
                   {
                       case "POSITIVO":
                           Pend = HP53131A.Trigger.SlopeEnum.POS;
                           break;
                       case "NEGATIVO":
                           Pend = HP53131A.Trigger.SlopeEnum.NEG;
                           break;
                       default:
                           errorProvider1.SetError(Pend1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Pend1, "Selected Item incorrect");
                   return false;
               }
               if (Level1.SelectedItem != null)
                   switch (Level1.SelectedItem.ToString().Trim())
                   {
                       case "AUTO ON":
                           Level = HP53131A.Trigger.LevelEnum.AUTO_ON;
                           break;
                       case "LEVEL V":
                           Level = HP53131A.Trigger.LevelEnum.NUM_V_OFF;
                           if (ValorLevel1.Text == "")
                           {
                               errorProvider1.SetError(ValorLevel1, "Must write Level in Mode Level V");
                               return false;
                           }
                           ValorMedio = Convert.ToDouble(ValorLevel1.Text.Replace(".",","));
                           break;
                       case "LEVEL %":
                           Level = HP53131A.Trigger.LevelEnum.NUM_PCT_OFF;
                           if (ValorLevel1.Text == "")
                           {
                               errorProvider1.SetError(ValorLevel1, "Must write Level in Mode Level %");
                               return false;
                           }
                           ValorMedio = Convert.ToDouble(ValorLevel1.Text.Replace(".", ","));
                           break;
                       default:
                           errorProvider1.SetError(Level1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Level1, "Selected Item incorrect");
                   return false;
               }
               if (Sens1.SelectedItem != null)
                   switch (Sens1.SelectedItem.ToString().Trim())
                   {
                       case "HIGH":
                           Sens = HP53131A.Trigger.SensibilitiEnum.HI;
                           break;
                       case "MED":
                           Sens = HP53131A.Trigger.SensibilitiEnum.MED;
                           break;
                       case "LOW":
                           Sens = HP53131A.Trigger.SensibilitiEnum.LOW;
                           break;
                       default:
                           errorProvider1.SetError(Sens1, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Sens1, "Selected Item incorrect");
                   return false;
               }

           }
           else
           {

               if (Pend2.SelectedItem != null)
                   switch (Pend2.SelectedItem.ToString().Trim())
                   {
                       case "POSITIVO":
                           Pend = HP53131A.Trigger.SlopeEnum.POS;
                           break;
                       case "NEGATIVO":
                           Pend = HP53131A.Trigger.SlopeEnum.NEG;
                           break;
                       default:
                           errorProvider1.SetError(Pend2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Pend2, "Selected Item incorrect");
                   return false;
               }
               if (Level2.SelectedItem != null)
                   switch (Level2.SelectedItem.ToString().Trim())
                   {
                       case "AUTO ON":
                           Level = HP53131A.Trigger.LevelEnum.AUTO_ON;
                           break;
                       case "LEVEL V":
                           Level = HP53131A.Trigger.LevelEnum.NUM_V_OFF;
                           if (ValorLevel2.Text == "")
                           {
                               errorProvider1.SetError(ValorLevel2, "Must write Level in Mode Level V");
                               return false;
                           }
                           ValorMedio = Convert.ToDouble(ValorLevel2.Text);
                           break;
                       case "LEVEL %":
                           Level = HP53131A.Trigger.LevelEnum.NUM_PCT_OFF;
                           if (ValorLevel2.Text == "")
                           {
                               errorProvider1.SetError(ValorLevel1, "Must write Level in Mode Level %");
                               return false;
                           }
                           ValorMedio = Convert.ToDouble(ValorLevel2.Text);
                           break;
                       default:
                           errorProvider1.SetError(Level2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Level2, "Selected Item incorrect");
                   return false;
               }
               if (Sens2.SelectedItem != null)
                   switch (Sens2.SelectedItem.ToString().Trim())
                   {
                       case "HIGH":
                           Sens = HP53131A.Trigger.SensibilitiEnum.HI;
                           break;
                       case "MED":
                           Sens = HP53131A.Trigger.SensibilitiEnum.MED;
                           break;
                       case "LOW":
                           Sens = HP53131A.Trigger.SensibilitiEnum.LOW;
                           break;
                       default:
                           errorProvider1.SetError(Sens2, "Selected Item incorrect");
                           return false;
                   }
               else
               {
                   errorProvider1.SetError(Sens2, "Selected Item incorrect");
                   return false;
               }
           }
           errorProvider1.Clear();
           Instrument.PresetAndConfigTrigger(Canal, Level, Pend, Sens, ValorMedio);
           return true;
       }

       private bool ConfigGate()
       {

           HP53131A.Gate.FuncionGateEnum Func = new HP53131A.Gate.FuncionGateEnum();
           HP53131A.Gate.ModoGateStartEnum ModoStart = new HP53131A.Gate.ModoGateStartEnum();
           HP53131A.Gate.SlopeEnum PendStart = new HP53131A.Gate.SlopeEnum();
           HP53131A.Gate.SlopeEnum PendStop = new HP53131A.Gate.SlopeEnum();
           HP53131A.Gate.ModoGateStopEnum ModoStop = new HP53131A.Gate.ModoGateStopEnum();
           int ValorGate = 0;

           if (StartGate.SelectedItem != null)
               switch (StartGate.SelectedItem.ToString().Trim())
               {
                   case "AUTO":
                       ModoStart = HP53131A.Gate.ModoGateStartEnum.AutoStart;
                       break;
                   case "EXT":
                       ModoStart = HP53131A.Gate.ModoGateStartEnum.ExtStart;
                       break;
                   default:
                       errorProvider1.SetError(StartGate, "Selected Item incorrect");
                       return false;
               }
           else
           {
               errorProvider1.SetError(StartGate, "Selected Item incorrect");
               return false;
           }

           if (StopGate.SelectedItem != null)
               switch (StopGate.SelectedItem.ToString().Trim())
               {
                   case "AUTO":
                       ModoStop = HP53131A.Gate.ModoGateStopEnum.AutoStop;
                       break;
                   case "EXT":
                       ModoStop = HP53131A.Gate.ModoGateStopEnum.ExtStop;
                       break;
                   case "DIGIT":
                       ModoStop = HP53131A.Gate.ModoGateStopEnum.Digits;
                       if (ValorGateStop.Text == "")
                       {
                           errorProvider1.SetError(ValorLevel1, "Must write Level in Mode Level V");
                           return false;
                       }
                       ValorGate = Convert.ToInt32(ValorGateStop.Text);
                       break;
                   case "TIME":
                       ModoStop = HP53131A.Gate.ModoGateStopEnum.G_Time;
                       if (ValorGateStop.Text == "")
                       {
                           errorProvider1.SetError(ValorLevel1, "Must write Level in Mode Level V");
                           return false;
                       }
                       ValorGate = Convert.ToInt32(ValorGateStop.Text);
                       break;
                   default:
                       errorProvider1.SetError(StopGate, "Selected Item incorrect");
                       return false;
               }
           else
           {
               errorProvider1.SetError(StopGate, "Selected Item incorrect");
               return false;
           }
           if (StartPend.SelectedItem != null)
               switch (StartPend.SelectedItem.ToString().Trim())
               {
                   case "POS":
                       PendStart = HP53131A.Gate.SlopeEnum.POS;
                       break;
                   case "NEG":
                       PendStart = HP53131A.Gate.SlopeEnum.NEG;
                       break;
                   default:
                       errorProvider1.SetError(StartPend, "Selected Item incorrect");
                       return false;
               }
           else
           {
               errorProvider1.SetError(StartPend, "Selected Item incorrect");
               return false;
           }
           if (StopPend.SelectedItem != null)
               switch (StopPend.SelectedItem.ToString().Trim())
               {
                   case "POS":
                       PendStop = HP53131A.Gate.SlopeEnum.POS;
                       break;
                   case "NEG":
                       PendStop = HP53131A.Gate.SlopeEnum.NEG;
                       break;
                   default:
                       errorProvider1.SetError(StopPend, "Selected Item incorrect");
                       return false;
               }
           else
           {
               errorProvider1.SetError(StopPend, "Selected Item incorrect");
               return false;
           }
           errorProvider1.Clear();
           Instrument.PresetAndConfigGate(Func, ModoStart, PendStart, ModoStop, PendStop, ValorGate);
           return true;
       }

       private bool ConfigStatistics()
       {
           HP53131A.Statistics.KindMeasShowEnum TipoStatisctics = new HP53131A.Statistics.KindMeasShowEnum();
           int NumMuestras = 0;
           bool UseAllMeas = true;
           bool OnSingle = true;

           if (Meas.SelectedItem != null)
               switch (Meas.SelectedItem.ToString().Trim())
               {
                   case "MEAN":
                       TipoStatisctics = HP53131A.Statistics.KindMeasShowEnum.Data_MEAN;
                       break;
                   case "MAX":
                       TipoStatisctics = HP53131A.Statistics.KindMeasShowEnum.Data_MAX;
                       break;
                   case "MIN":
                       TipoStatisctics = HP53131A.Statistics.KindMeasShowEnum.Data_MIN;
                       break;
                   case "STD DEV":
                       TipoStatisctics = HP53131A.Statistics.KindMeasShowEnum.Data_SDEV;
                       break;
                   default:
                       errorProvider1.SetError(Meas, "Selected Item incorrect");
                       return false;
               }
           else
           {
               errorProvider1.SetError(Meas, "Selected Item incorrect");
               return false;
           }
           if (ValorN.Text == "")
           {
               errorProvider1.SetError(ValorN, "Must write Num. Muestras.");
               return false;
           }
           NumMuestras = Convert.ToInt32(ValorN.Text);

           if (RadioButton2.Checked) UseAllMeas = true;
           else UseAllMeas = false;

           if (RadioButton3.Checked) OnSingle = true;
           else OnSingle = false;

           errorProvider1.Clear();
           Instrument.PresetAndConfigStatistics(TipoStatisctics, NumMuestras, UseAllMeas, OnSingle);
           return true;
       }

       private void ToggleButtonsState(Button btn)
       {
           if (btn.Equals(btnReset))
               btnReset.Enabled = !btnReset.Enabled;
       }

       private void chkFRECCH1_CheckedChanged(object sender, EventArgs e)
       {
           if (chkFRECCH1.Checked)
           {
               DisabledButton();
               chkFRECCH1.BackColor = Color.DarkGreen;
               variable = HP53131A.Variables.FREQ;
           }
           else DisabledButton();
       }

       private void chkFRECCH2_CheckedChanged(object sender, EventArgs e)
       {
           if (chkFRECCH2.Checked)
           {
               DisabledButton();
               chkFRECCH2.BackColor = Color.DarkGreen;
               variable = HP53131A.Variables.FREQ;
           }
           else DisabledButton();
       }

       private void chkPERCH1_CheckedChanged(object sender, EventArgs e)
       {
           if (chkPERCH1.Checked)
           {
               DisabledButton();
               chkPERCH1.BackColor = Color.DarkGreen;
               variable = HP53131A.Variables.FREQ;
           }
           else DisabledButton();
       }

       private void chkTINT_CheckedChanged(object sender, EventArgs e)
       {
           if (chkTINT.Checked)
           {
               DisabledButton();
               chkTINT.BackColor = Color.DarkGreen;
               variable = HP53131A.Variables.TINT;
           }
           else DisabledButton();
       }

       private void DisabledButton()
       {
           chkTINT.BackColor = Color.DimGray;
           chkTINT.Checked = false;
           chkPERCH1.BackColor = Color.DimGray;
           chkPERCH1.Checked = false;
           chkFRECCH2.BackColor = Color.DimGray;
           chkFRECCH2.Checked = false;
           chkFRECCH1.BackColor = Color.DimGray;
           chkFRECCH1.Checked = false; ;
       }

       private void chkCh1_CheckedChanged(object sender, EventArgs e)
       {
           if (chkCh1.Checked)
               try
               {
                   errorProvider1.Clear();
                   chkCh1.BackColor = Color.DarkGreen;
                   if (ConfigImpedancia(HP53131A.Channels.CH1) == false)
                   {
                       chkCh1.Checked = false;
                       chkCh1.BackColor = Color.DimGray;
                       return;
                   }
                   if (ConfigTrigger(HP53131A.Channels.CH1) == false)
                   {
                       chkCh1.Checked = false;
                       chkCh1.BackColor = Color.DimGray;
                       return;
                   }
               }
               catch (Exception ex)
               {
                   Logger.Error("Error al realizar la configuracion del CH1 en el HP53131", ex);
               }
           else chkCh1.BackColor = Color.DimGray;
       }

       private void chkCh2_CheckedChanged(object sender, EventArgs e)
       {
           if (chkCh2.Checked)
               try
               {
                   errorProvider1.Clear();
                   chkCh2.BackColor = Color.DarkGreen;
                   if (ConfigImpedancia(HP53131A.Channels.CH2) == false)
                   {
                       chkCh2.Checked = false;
                       chkCh2.BackColor = Color.DimGray;
                       return;
                   }
                   if (ConfigTrigger(HP53131A.Channels.CH2) == false)
                   {
                       chkCh2.Checked = false;
                       chkCh2.BackColor = Color.DimGray;
                       return;
                   }
               }
               catch (Exception ex)
               {
                   Logger.Error("Error al realizar la configuracion del CH2 en el HP53131", ex);
               }
           else chkCh2.BackColor = Color.DimGray;
       }

       private void chkGate_CheckedChanged(object sender, EventArgs e)
       {
           if (chkGate.Checked)
               try
               {
                   errorProvider1.Clear();
                   chkGate.BackColor = Color.DarkGreen;
                   if (ConfigGate() == false)
                   {
                       chkGate.BackColor = Color.DimGray;
                       chkGate.Checked = false;
                       return;
                   }
               }
               catch (Exception ex)
               {
                   Logger.Error("Error al realizar la configuracion del Gate en el HP53131", ex);
               }
           else chkGate.BackColor = Color.DimGray;
       }

       private void checStats_CheckedChanged(object sender, EventArgs e)
       {
           if (checStats.Checked)
               try { 
                   errorProvider1.Clear();
                   checStats.BackColor = Color.DarkGreen;
                   if (ConfigStatistics() == false)
                   {
                       checStats.BackColor = Color.DimGray;
                       checStats.Checked = false;
                       return;
                   }
               }
               catch (Exception ex)
               {
                   Logger.Error("Error al realizar la configuracion del Stats en el HP53131", ex);
               }
           else checStats.BackColor = Color.DimGray;
       }

       protected override void Dispose(bool disposing)
       {
           if (disposing && (components != null))
               components.Dispose();
           Instrument.Dispose();
           base.Dispose(disposing);
       }

    }
    public class HP53131Description : ControlDescription
    {
        public override string Name { get { return "Frecuencimetro HP53131"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override Image IconBig { get { return Properties.Resources.HP53131A1; } }
        public override Type SubcomponentType { get { return typeof(HP53131A); } }
    }
}
