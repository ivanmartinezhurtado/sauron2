﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace WinTaskRunner.Utils
{
    public class AppConfig
    {
        public AppConfig()
        {
            LoadExtensions = new List<string>();
            PathSequences = new List<string>();
            PathAssemblies = new List<pathAssenbly>();
            PathReports = new List<string>();
            Toolbox = new AppConfigToolbox { LoadAssemblies = new List<string>() };
            ComponentsView = new AppConfigComponentsView { LoadAssemblies = new List<string>() };
            Editors = new List<AppConfigEditor>();
            ImageActions = new List<IconTreeNode>();
        }

        public List<string> LoadExtensions { get; set; }
        public List<string> PathSequences { get; set; }
        public List<pathAssenbly> PathAssemblies { get; set; }
        public List<string> PathReports { get; set; }
        public AppConfigToolbox Toolbox { get; set; }
        public AppConfigComponentsView ComponentsView { get; set; }
        public List<AppConfigEditor> Editors { get; set; }
        public List<IconTreeNode> ImageActions { get; set; }

        public string ModeType
        {
            get
            {
                string type = Application.StartupPath.StartsWith(@"C:\SAURON_SYSTEM") ? "SYNC" : "LOCAL";
                return type;
            }
        }

        public string PathCurrentAsssembly
        {
            get
            {
                string path4 = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string path = System.IO.Path.GetDirectoryName(path4);
                return path;
            }
        }

        public string LoadSequence { get; set; }

        public class AppConfigToolbox
        {
            public List<string> LoadAssemblies { get; set; }
        }

        public class AppConfigComponentsView
        {
            public List<string> LoadAssemblies { get; set; }
        }

        public class AppConfigEditor
        {
            public string DocType { get; set; }
            public string Manager { get; set; }
        }

        public class IconTreeNode
        {
            public string Key { get; set; }
            public string Icon { get; set; }
        }

        public class pathAssenbly
        {
            public string type { get; set; }
            public string path { get; set; }
        }
    }
}
