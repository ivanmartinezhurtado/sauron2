﻿namespace Dezac.Instruments.Controls
{
    partial class UC_CVMMINI
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label factorPotenciaL1Label;
            System.Windows.Forms.Label factorPotenciaL2Label;
            System.Windows.Forms.Label factorPotenciaL3Label;
            System.Windows.Forms.Label potenciaActivaL1Label;
            System.Windows.Forms.Label potenciaActivaL2Label;
            System.Windows.Forms.Label potenciaActivaL3Label;
            System.Windows.Forms.Label potenciaActivaIIILabel;
            System.Windows.Forms.Label tensionL1Label;
            System.Windows.Forms.Label tensionL2Label;
            System.Windows.Forms.Label corrienteL1Label;
            System.Windows.Forms.Label corrienteL2Label;
            System.Windows.Forms.Label corrienteL3Label;
            System.Windows.Forms.Label potenciaReactivaInductivaL1Label;
            System.Windows.Forms.Label potenciaReactivaInductivaL2Label;
            System.Windows.Forms.Label potenciaReactivaInductivaL3Label;
            System.Windows.Forms.Label potenciaInductivaIIILabel;
            System.Windows.Forms.Label tensionLineaL1L2Label;
            System.Windows.Forms.Label tensionLineaL2L3Label;
            System.Windows.Forms.Label tensionLineaL3L1Label;
            System.Windows.Forms.Label frecuenciaLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label3;
            this.corrienteL3TextBox = new System.Windows.Forms.TextBox();
            this.bsAllVars = new System.Windows.Forms.BindingSource(this.components);
            this.corrienteL2TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL1TextBox = new System.Windows.Forms.TextBox();
            this.tensionL3TextBox = new System.Windows.Forms.TextBox();
            this.tensionL2TextBox = new System.Windows.Forms.TextBox();
            this.tensionL1TextBox = new System.Windows.Forms.TextBox();
            this.btnOn = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.factorPotenciaL3TextBox = new System.Windows.Forms.TextBox();
            this.factorPotenciaL2TextBox = new System.Windows.Forms.TextBox();
            this.factorPotenciaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaInductivaIIITextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaL3L1TextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaL2L3TextBox = new System.Windows.Forms.TextBox();
            this.tensionLineaL1L2TextBox = new System.Windows.Forms.TextBox();
            this.frecuenciaTextBox = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cmbPeriferic = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            factorPotenciaL1Label = new System.Windows.Forms.Label();
            factorPotenciaL2Label = new System.Windows.Forms.Label();
            factorPotenciaL3Label = new System.Windows.Forms.Label();
            potenciaActivaL1Label = new System.Windows.Forms.Label();
            potenciaActivaL2Label = new System.Windows.Forms.Label();
            potenciaActivaL3Label = new System.Windows.Forms.Label();
            potenciaActivaIIILabel = new System.Windows.Forms.Label();
            tensionL1Label = new System.Windows.Forms.Label();
            tensionL2Label = new System.Windows.Forms.Label();
            corrienteL1Label = new System.Windows.Forms.Label();
            corrienteL2Label = new System.Windows.Forms.Label();
            corrienteL3Label = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL1Label = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL2Label = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL3Label = new System.Windows.Forms.Label();
            potenciaInductivaIIILabel = new System.Windows.Forms.Label();
            tensionLineaL1L2Label = new System.Windows.Forms.Label();
            tensionLineaL2L3Label = new System.Windows.Forms.Label();
            tensionLineaL3L1Label = new System.Windows.Forms.Label();
            frecuenciaLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // factorPotenciaL1Label
            // 
            factorPotenciaL1Label.AutoSize = true;
            factorPotenciaL1Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL1Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL1Label.Location = new System.Drawing.Point(12, 95);
            factorPotenciaL1Label.Name = "factorPotenciaL1Label";
            factorPotenciaL1Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL1Label.TabIndex = 0;
            factorPotenciaL1Label.Text = "PF L1:";
            // 
            // factorPotenciaL2Label
            // 
            factorPotenciaL2Label.AutoSize = true;
            factorPotenciaL2Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL2Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL2Label.Location = new System.Drawing.Point(12, 123);
            factorPotenciaL2Label.Name = "factorPotenciaL2Label";
            factorPotenciaL2Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL2Label.TabIndex = 2;
            factorPotenciaL2Label.Text = "PF L2:";
            // 
            // factorPotenciaL3Label
            // 
            factorPotenciaL3Label.AutoSize = true;
            factorPotenciaL3Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL3Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL3Label.Location = new System.Drawing.Point(12, 151);
            factorPotenciaL3Label.Name = "factorPotenciaL3Label";
            factorPotenciaL3Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL3Label.TabIndex = 4;
            factorPotenciaL3Label.Text = "PF L3:";
            // 
            // potenciaActivaL1Label
            // 
            potenciaActivaL1Label.AutoSize = true;
            potenciaActivaL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL1Label.Location = new System.Drawing.Point(307, 12);
            potenciaActivaL1Label.Name = "potenciaActivaL1Label";
            potenciaActivaL1Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL1Label.TabIndex = 0;
            potenciaActivaL1Label.Text = "Pot. Act L1:";
            // 
            // potenciaActivaL2Label
            // 
            potenciaActivaL2Label.AutoSize = true;
            potenciaActivaL2Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL2Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL2Label.Location = new System.Drawing.Point(307, 38);
            potenciaActivaL2Label.Name = "potenciaActivaL2Label";
            potenciaActivaL2Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL2Label.TabIndex = 2;
            potenciaActivaL2Label.Text = "Pot. Act L2:";
            // 
            // potenciaActivaL3Label
            // 
            potenciaActivaL3Label.AutoSize = true;
            potenciaActivaL3Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL3Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL3Label.Location = new System.Drawing.Point(307, 64);
            potenciaActivaL3Label.Name = "potenciaActivaL3Label";
            potenciaActivaL3Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL3Label.TabIndex = 4;
            potenciaActivaL3Label.Text = "Pot. Act L3:";
            // 
            // potenciaActivaIIILabel
            // 
            potenciaActivaIIILabel.AutoSize = true;
            potenciaActivaIIILabel.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaIIILabel.ForeColor = System.Drawing.Color.Black;
            potenciaActivaIIILabel.Location = new System.Drawing.Point(309, 180);
            potenciaActivaIIILabel.Name = "potenciaActivaIIILabel";
            potenciaActivaIIILabel.Size = new System.Drawing.Size(73, 13);
            potenciaActivaIIILabel.TabIndex = 6;
            potenciaActivaIIILabel.Text = "Pot. Act III:";
            // 
            // tensionL1Label
            // 
            tensionL1Label.AutoSize = true;
            tensionL1Label.BackColor = System.Drawing.Color.Transparent;
            tensionL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL1Label.ForeColor = System.Drawing.Color.Black;
            tensionL1Label.Location = new System.Drawing.Point(23, 11);
            tensionL1Label.Name = "tensionL1Label";
            tensionL1Label.Size = new System.Drawing.Size(33, 13);
            tensionL1Label.TabIndex = 0;
            tensionL1Label.Text = "V L1";
            // 
            // tensionL2Label
            // 
            tensionL2Label.AutoSize = true;
            tensionL2Label.BackColor = System.Drawing.Color.Transparent;
            tensionL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL2Label.ForeColor = System.Drawing.Color.Black;
            tensionL2Label.Location = new System.Drawing.Point(23, 39);
            tensionL2Label.Name = "tensionL2Label";
            tensionL2Label.Size = new System.Drawing.Size(37, 13);
            tensionL2Label.TabIndex = 2;
            tensionL2Label.Text = "V L2:";
            // 
            // corrienteL1Label
            // 
            corrienteL1Label.AutoSize = true;
            corrienteL1Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL1Label.ForeColor = System.Drawing.Color.Black;
            corrienteL1Label.Location = new System.Drawing.Point(166, 12);
            corrienteL1Label.Name = "corrienteL1Label";
            corrienteL1Label.Size = new System.Drawing.Size(33, 13);
            corrienteL1Label.TabIndex = 0;
            corrienteL1Label.Text = "I L1:";
            // 
            // corrienteL2Label
            // 
            corrienteL2Label.AutoSize = true;
            corrienteL2Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL2Label.ForeColor = System.Drawing.Color.Black;
            corrienteL2Label.Location = new System.Drawing.Point(166, 40);
            corrienteL2Label.Name = "corrienteL2Label";
            corrienteL2Label.Size = new System.Drawing.Size(33, 13);
            corrienteL2Label.TabIndex = 2;
            corrienteL2Label.Text = "I L2:";
            // 
            // corrienteL3Label
            // 
            corrienteL3Label.AutoSize = true;
            corrienteL3Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL3Label.ForeColor = System.Drawing.Color.Black;
            corrienteL3Label.Location = new System.Drawing.Point(166, 68);
            corrienteL3Label.Name = "corrienteL3Label";
            corrienteL3Label.Size = new System.Drawing.Size(33, 13);
            corrienteL3Label.TabIndex = 4;
            corrienteL3Label.Text = "I L3:";
            // 
            // potenciaReactivaInductivaL1Label
            // 
            potenciaReactivaInductivaL1Label.AutoSize = true;
            potenciaReactivaInductivaL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaInductivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaInductivaL1Label.Location = new System.Drawing.Point(296, 96);
            potenciaReactivaInductivaL1Label.Name = "potenciaReactivaInductivaL1Label";
            potenciaReactivaInductivaL1Label.Size = new System.Drawing.Size(86, 13);
            potenciaReactivaInductivaL1Label.TabIndex = 0;
            potenciaReactivaInductivaL1Label.Text = "Pot. Reac L1:";
            // 
            // potenciaReactivaInductivaL2Label
            // 
            potenciaReactivaInductivaL2Label.AutoSize = true;
            potenciaReactivaInductivaL2Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaInductivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL2Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaInductivaL2Label.Location = new System.Drawing.Point(296, 126);
            potenciaReactivaInductivaL2Label.Name = "potenciaReactivaInductivaL2Label";
            potenciaReactivaInductivaL2Label.Size = new System.Drawing.Size(86, 13);
            potenciaReactivaInductivaL2Label.TabIndex = 2;
            potenciaReactivaInductivaL2Label.Text = "Pot. Reac L2:";
            // 
            // potenciaReactivaInductivaL3Label
            // 
            potenciaReactivaInductivaL3Label.AutoSize = true;
            potenciaReactivaInductivaL3Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaInductivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL3Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaInductivaL3Label.Location = new System.Drawing.Point(296, 154);
            potenciaReactivaInductivaL3Label.Name = "potenciaReactivaInductivaL3Label";
            potenciaReactivaInductivaL3Label.Size = new System.Drawing.Size(86, 13);
            potenciaReactivaInductivaL3Label.TabIndex = 4;
            potenciaReactivaInductivaL3Label.Text = "Pot. Reac L3:";
            // 
            // potenciaInductivaIIILabel
            // 
            potenciaInductivaIIILabel.AutoSize = true;
            potenciaInductivaIIILabel.BackColor = System.Drawing.Color.Transparent;
            potenciaInductivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaInductivaIIILabel.ForeColor = System.Drawing.Color.Black;
            potenciaInductivaIIILabel.Location = new System.Drawing.Point(294, 206);
            potenciaInductivaIIILabel.Name = "potenciaInductivaIIILabel";
            potenciaInductivaIIILabel.Size = new System.Drawing.Size(88, 13);
            potenciaInductivaIIILabel.TabIndex = 6;
            potenciaInductivaIIILabel.Text = "Pot. Reac  III:";
            // 
            // tensionLineaL1L2Label
            // 
            tensionLineaL1L2Label.AutoSize = true;
            tensionLineaL1L2Label.BackColor = System.Drawing.Color.Transparent;
            tensionLineaL1L2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaL1L2Label.ForeColor = System.Drawing.Color.Black;
            tensionLineaL1L2Label.Location = new System.Drawing.Point(158, 96);
            tensionLineaL1L2Label.Name = "tensionLineaL1L2Label";
            tensionLineaL1L2Label.Size = new System.Drawing.Size(44, 13);
            tensionLineaL1L2Label.TabIndex = 0;
            tensionLineaL1L2Label.Text = "V L12:";
            // 
            // tensionLineaL2L3Label
            // 
            tensionLineaL2L3Label.AutoSize = true;
            tensionLineaL2L3Label.BackColor = System.Drawing.Color.Transparent;
            tensionLineaL2L3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaL2L3Label.ForeColor = System.Drawing.Color.Black;
            tensionLineaL2L3Label.Location = new System.Drawing.Point(158, 124);
            tensionLineaL2L3Label.Name = "tensionLineaL2L3Label";
            tensionLineaL2L3Label.Size = new System.Drawing.Size(44, 13);
            tensionLineaL2L3Label.TabIndex = 2;
            tensionLineaL2L3Label.Text = "V L23:";
            // 
            // tensionLineaL3L1Label
            // 
            tensionLineaL3L1Label.AutoSize = true;
            tensionLineaL3L1Label.BackColor = System.Drawing.Color.Transparent;
            tensionLineaL3L1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionLineaL3L1Label.ForeColor = System.Drawing.Color.Black;
            tensionLineaL3L1Label.Location = new System.Drawing.Point(158, 152);
            tensionLineaL3L1Label.Name = "tensionLineaL3L1Label";
            tensionLineaL3L1Label.Size = new System.Drawing.Size(44, 13);
            tensionLineaL3L1Label.TabIndex = 4;
            tensionLineaL3L1Label.Text = "V L31:";
            // 
            // frecuenciaLabel
            // 
            frecuenciaLabel.AutoSize = true;
            frecuenciaLabel.BackColor = System.Drawing.Color.Transparent;
            frecuenciaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            frecuenciaLabel.ForeColor = System.Drawing.Color.Black;
            frecuenciaLabel.Location = new System.Drawing.Point(20, 209);
            frecuenciaLabel.Name = "frecuenciaLabel";
            frecuenciaLabel.Size = new System.Drawing.Size(36, 13);
            frecuenciaLabel.TabIndex = 0;
            frecuenciaLabel.Text = "Frec:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.Black;
            label1.Location = new System.Drawing.Point(23, 67);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(33, 13);
            label1.TabIndex = 6;
            label1.Text = "V L3";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.Black;
            label2.Location = new System.Drawing.Point(154, 182);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(48, 13);
            label2.TabIndex = 6;
            label2.Text = "Cos III:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = System.Drawing.Color.Transparent;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.ForeColor = System.Drawing.Color.Black;
            label4.Location = new System.Drawing.Point(14, 181);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(42, 13);
            label4.TabIndex = 6;
            label4.Text = "PF III:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = System.Drawing.Color.Transparent;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.ForeColor = System.Drawing.Color.Black;
            label5.Location = new System.Drawing.Point(117, 18);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(66, 13);
            label5.TabIndex = 48;
            label5.Text = "Serial Port";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = System.Drawing.Color.Transparent;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.ForeColor = System.Drawing.Color.Black;
            label6.Location = new System.Drawing.Point(325, 18);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(61, 13);
            label6.TabIndex = 49;
            label6.Text = "Periferico";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.BackColor = System.Drawing.Color.Transparent;
            label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.ForeColor = System.Drawing.Color.Black;
            label7.Location = new System.Drawing.Point(18, 119);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(84, 13);
            label7.TabIndex = 50;
            label7.Text = "Intervalo (ms)";
            // 
            // corrienteL3TextBox
            // 
            this.corrienteL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL3", true));
            this.corrienteL3TextBox.ForeColor = System.Drawing.Color.Black;
            this.corrienteL3TextBox.Location = new System.Drawing.Point(206, 63);
            this.corrienteL3TextBox.Name = "corrienteL3TextBox";
            this.corrienteL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL3TextBox.TabIndex = 5;
            // 
            // bsAllVars
            // 
            this.bsAllVars.DataSource = typeof(Dezac.Instruments.Controls.UC_CVMMINI.Vars);
            // 
            // corrienteL2TextBox
            // 
            this.corrienteL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL2", true));
            this.corrienteL2TextBox.ForeColor = System.Drawing.Color.Black;
            this.corrienteL2TextBox.Location = new System.Drawing.Point(206, 35);
            this.corrienteL2TextBox.Name = "corrienteL2TextBox";
            this.corrienteL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL2TextBox.TabIndex = 3;
            // 
            // corrienteL1TextBox
            // 
            this.corrienteL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL1", true));
            this.corrienteL1TextBox.ForeColor = System.Drawing.Color.Black;
            this.corrienteL1TextBox.Location = new System.Drawing.Point(206, 7);
            this.corrienteL1TextBox.Name = "corrienteL1TextBox";
            this.corrienteL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL1TextBox.TabIndex = 1;
            // 
            // tensionL3TextBox
            // 
            this.tensionL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL3", true));
            this.tensionL3TextBox.ForeColor = System.Drawing.Color.Black;
            this.tensionL3TextBox.Location = new System.Drawing.Point(62, 63);
            this.tensionL3TextBox.Name = "tensionL3TextBox";
            this.tensionL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL3TextBox.TabIndex = 5;
            // 
            // tensionL2TextBox
            // 
            this.tensionL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL2", true));
            this.tensionL2TextBox.ForeColor = System.Drawing.Color.Black;
            this.tensionL2TextBox.Location = new System.Drawing.Point(62, 35);
            this.tensionL2TextBox.Name = "tensionL2TextBox";
            this.tensionL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL2TextBox.TabIndex = 3;
            // 
            // tensionL1TextBox
            // 
            this.tensionL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL1", true));
            this.tensionL1TextBox.ForeColor = System.Drawing.Color.Black;
            this.tensionL1TextBox.Location = new System.Drawing.Point(62, 7);
            this.tensionL1TextBox.Name = "tensionL1TextBox";
            this.tensionL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL1TextBox.TabIndex = 1;
            // 
            // btnOn
            // 
            this.btnOn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.FlatAppearance.BorderSize = 0;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(23, 212);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(72, 34);
            this.btnOn.TabIndex = 4;
            this.btnOn.Text = "READ";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.BackgroundImage = global::Dezac.Instruments.Properties.Resources.Actions_exit_icon;
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(40, 259);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(38, 38);
            this.btnReset.TabIndex = 6;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(189, 15);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(77, 21);
            this.cmb_Port.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // factorPotenciaL3TextBox
            // 
            this.factorPotenciaL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL3", true));
            this.factorPotenciaL3TextBox.ForeColor = System.Drawing.Color.Black;
            this.factorPotenciaL3TextBox.Location = new System.Drawing.Point(62, 147);
            this.factorPotenciaL3TextBox.Name = "factorPotenciaL3TextBox";
            this.factorPotenciaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL3TextBox.TabIndex = 5;
            // 
            // factorPotenciaL2TextBox
            // 
            this.factorPotenciaL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL2", true));
            this.factorPotenciaL2TextBox.ForeColor = System.Drawing.Color.Black;
            this.factorPotenciaL2TextBox.Location = new System.Drawing.Point(62, 119);
            this.factorPotenciaL2TextBox.Name = "factorPotenciaL2TextBox";
            this.factorPotenciaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL2TextBox.TabIndex = 3;
            // 
            // factorPotenciaL1TextBox
            // 
            this.factorPotenciaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL1", true));
            this.factorPotenciaL1TextBox.ForeColor = System.Drawing.Color.Black;
            this.factorPotenciaL1TextBox.Location = new System.Drawing.Point(62, 91);
            this.factorPotenciaL1TextBox.Name = "factorPotenciaL1TextBox";
            this.factorPotenciaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL1TextBox.TabIndex = 1;
            // 
            // potenciaActivaL3TextBox
            // 
            this.potenciaActivaL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL3", true));
            this.potenciaActivaL3TextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaActivaL3TextBox.Location = new System.Drawing.Point(388, 60);
            this.potenciaActivaL3TextBox.Name = "potenciaActivaL3TextBox";
            this.potenciaActivaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL3TextBox.TabIndex = 5;
            // 
            // potenciaActivaL2TextBox
            // 
            this.potenciaActivaL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL2", true));
            this.potenciaActivaL2TextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaActivaL2TextBox.Location = new System.Drawing.Point(388, 34);
            this.potenciaActivaL2TextBox.Name = "potenciaActivaL2TextBox";
            this.potenciaActivaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL2TextBox.TabIndex = 3;
            // 
            // potenciaActivaL1TextBox
            // 
            this.potenciaActivaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL1", true));
            this.potenciaActivaL1TextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaActivaL1TextBox.Location = new System.Drawing.Point(388, 8);
            this.potenciaActivaL1TextBox.Name = "potenciaActivaL1TextBox";
            this.potenciaActivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL1TextBox.TabIndex = 1;
            // 
            // potenciaActivaIIITextBox
            // 
            this.potenciaActivaIIITextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaIII", true));
            this.potenciaActivaIIITextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaActivaIIITextBox.Location = new System.Drawing.Point(388, 176);
            this.potenciaActivaIIITextBox.Name = "potenciaActivaIIITextBox";
            this.potenciaActivaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaIIITextBox.TabIndex = 7;
            // 
            // potenciaReactivaInductivaL3TextBox
            // 
            this.potenciaReactivaInductivaL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaInductivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaL3", true));
            this.potenciaReactivaInductivaL3TextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaReactivaInductivaL3TextBox.Location = new System.Drawing.Point(388, 150);
            this.potenciaReactivaInductivaL3TextBox.Name = "potenciaReactivaInductivaL3TextBox";
            this.potenciaReactivaInductivaL3TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL3TextBox.TabIndex = 5;
            // 
            // potenciaReactivaInductivaL2TextBox
            // 
            this.potenciaReactivaInductivaL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaInductivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaL2", true));
            this.potenciaReactivaInductivaL2TextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaReactivaInductivaL2TextBox.Location = new System.Drawing.Point(388, 122);
            this.potenciaReactivaInductivaL2TextBox.Name = "potenciaReactivaInductivaL2TextBox";
            this.potenciaReactivaInductivaL2TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL2TextBox.TabIndex = 3;
            // 
            // potenciaReactivaInductivaL1TextBox
            // 
            this.potenciaReactivaInductivaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaInductivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaL1", true));
            this.potenciaReactivaInductivaL1TextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaReactivaInductivaL1TextBox.Location = new System.Drawing.Point(388, 92);
            this.potenciaReactivaInductivaL1TextBox.Name = "potenciaReactivaInductivaL1TextBox";
            this.potenciaReactivaInductivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL1TextBox.TabIndex = 1;
            // 
            // potenciaInductivaIIITextBox
            // 
            this.potenciaInductivaIIITextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaInductivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaInductivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaInductivaIII", true));
            this.potenciaInductivaIIITextBox.ForeColor = System.Drawing.Color.Black;
            this.potenciaInductivaIIITextBox.Location = new System.Drawing.Point(388, 202);
            this.potenciaInductivaIIITextBox.Name = "potenciaInductivaIIITextBox";
            this.potenciaInductivaIIITextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaInductivaIIITextBox.TabIndex = 7;
            // 
            // tensionLineaL3L1TextBox
            // 
            this.tensionLineaL3L1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionLineaL3L1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaL3L1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaL3L1", true));
            this.tensionLineaL3L1TextBox.ForeColor = System.Drawing.Color.Black;
            this.tensionLineaL3L1TextBox.Location = new System.Drawing.Point(206, 147);
            this.tensionLineaL3L1TextBox.Name = "tensionLineaL3L1TextBox";
            this.tensionLineaL3L1TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaL3L1TextBox.TabIndex = 5;
            // 
            // tensionLineaL2L3TextBox
            // 
            this.tensionLineaL2L3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionLineaL2L3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaL2L3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaL2L3", true));
            this.tensionLineaL2L3TextBox.ForeColor = System.Drawing.Color.Black;
            this.tensionLineaL2L3TextBox.Location = new System.Drawing.Point(206, 119);
            this.tensionLineaL2L3TextBox.Name = "tensionLineaL2L3TextBox";
            this.tensionLineaL2L3TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaL2L3TextBox.TabIndex = 3;
            // 
            // tensionLineaL1L2TextBox
            // 
            this.tensionLineaL1L2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionLineaL1L2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionLineaL1L2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionLineaL1L2", true));
            this.tensionLineaL1L2TextBox.ForeColor = System.Drawing.Color.Black;
            this.tensionLineaL1L2TextBox.Location = new System.Drawing.Point(206, 91);
            this.tensionLineaL1L2TextBox.Name = "tensionLineaL1L2TextBox";
            this.tensionLineaL1L2TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionLineaL1L2TextBox.TabIndex = 1;
            // 
            // frecuenciaTextBox
            // 
            this.frecuenciaTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.frecuenciaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frecuenciaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "Frecuencia", true));
            this.frecuenciaTextBox.ForeColor = System.Drawing.Color.Black;
            this.frecuenciaTextBox.Location = new System.Drawing.Point(62, 205);
            this.frecuenciaTextBox.Name = "frecuenciaTextBox";
            this.frecuenciaTextBox.Size = new System.Drawing.Size(79, 20);
            this.frecuenciaTextBox.TabIndex = 1;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.BackColor = System.Drawing.Color.DimGray;
            this.checkBox1.FlatAppearance.BorderSize = 0;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.White;
            this.checkBox1.Location = new System.Drawing.Point(23, 164);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 34);
            this.checkBox1.TabIndex = 45;
            this.checkBox1.Text = "LOOP";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtInterval
            // 
            this.txtInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInterval.Location = new System.Drawing.Point(29, 135);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(57, 20);
            this.txtInterval.TabIndex = 46;
            this.txtInterval.Text = "1000";
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(120, 295);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(476, 11);
            this.pbStatus.TabIndex = 41;
            this.pbStatus.Visible = false;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiIII", true));
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(206, 177);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(79, 20);
            this.textBox1.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FacttorPotenciaIII", true));
            this.textBox2.ForeColor = System.Drawing.Color.Black;
            this.textBox2.Location = new System.Drawing.Point(62, 177);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(79, 20);
            this.textBox2.TabIndex = 7;
            // 
            // cmbPeriferic
            // 
            this.cmbPeriferic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPeriferic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPeriferic.FormattingEnabled = true;
            this.cmbPeriferic.Location = new System.Drawing.Point(392, 15);
            this.cmbPeriferic.Name = "cmbPeriferic";
            this.cmbPeriferic.Size = new System.Drawing.Size(69, 21);
            this.cmbPeriferic.TabIndex = 0;
            this.cmbPeriferic.SelectedIndexChanged += new System.EventHandler(this.cmbPeriferic_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.tensionL1TextBox);
            this.panel1.Controls.Add(tensionL1Label);
            this.panel1.Controls.Add(this.tensionL2TextBox);
            this.panel1.Controls.Add(this.corrienteL1TextBox);
            this.panel1.Controls.Add(tensionL2Label);
            this.panel1.Controls.Add(corrienteL1Label);
            this.panel1.Controls.Add(this.tensionL3TextBox);
            this.panel1.Controls.Add(this.corrienteL2TextBox);
            this.panel1.Controls.Add(this.factorPotenciaL1TextBox);
            this.panel1.Controls.Add(corrienteL2Label);
            this.panel1.Controls.Add(factorPotenciaL1Label);
            this.panel1.Controls.Add(this.corrienteL3TextBox);
            this.panel1.Controls.Add(this.frecuenciaTextBox);
            this.panel1.Controls.Add(this.factorPotenciaL2TextBox);
            this.panel1.Controls.Add(frecuenciaLabel);
            this.panel1.Controls.Add(label1);
            this.panel1.Controls.Add(label2);
            this.panel1.Controls.Add(this.potenciaActivaL1TextBox);
            this.panel1.Controls.Add(potenciaInductivaIIILabel);
            this.panel1.Controls.Add(factorPotenciaL2Label);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(potenciaActivaL1Label);
            this.panel1.Controls.Add(label4);
            this.panel1.Controls.Add(this.factorPotenciaL3TextBox);
            this.panel1.Controls.Add(potenciaReactivaInductivaL3Label);
            this.panel1.Controls.Add(this.potenciaActivaL2TextBox);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(corrienteL3Label);
            this.panel1.Controls.Add(potenciaActivaIIILabel);
            this.panel1.Controls.Add(this.tensionLineaL1L2TextBox);
            this.panel1.Controls.Add(tensionLineaL3L1Label);
            this.panel1.Controls.Add(potenciaActivaL2Label);
            this.panel1.Controls.Add(this.potenciaInductivaIIITextBox);
            this.panel1.Controls.Add(tensionLineaL1L2Label);
            this.panel1.Controls.Add(this.potenciaActivaIIITextBox);
            this.panel1.Controls.Add(this.potenciaActivaL3TextBox);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL3TextBox);
            this.panel1.Controls.Add(this.tensionLineaL2L3TextBox);
            this.panel1.Controls.Add(potenciaReactivaInductivaL2Label);
            this.panel1.Controls.Add(factorPotenciaL3Label);
            this.panel1.Controls.Add(potenciaActivaL3Label);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL1TextBox);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL2TextBox);
            this.panel1.Controls.Add(tensionLineaL2L3Label);
            this.panel1.Controls.Add(this.tensionLineaL3L1TextBox);
            this.panel1.Controls.Add(potenciaReactivaInductivaL1Label);
            this.panel1.Location = new System.Drawing.Point(120, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 242);
            this.panel1.TabIndex = 51;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.circutor1;
            this.pictureBox1.Location = new System.Drawing.Point(21, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(74, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = System.Drawing.Color.Transparent;
            label3.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.ForeColor = System.Drawing.Color.Black;
            label3.Location = new System.Drawing.Point(15, 74);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(86, 19);
            label3.TabIndex = 61;
            label3.Text = "CVM-MINI";
            // 
            // UC_CVMMINI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(label7);
            this.Controls.Add(label6);
            this.Controls.Add(label5);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btnOn);
            this.Controls.Add(this.txtInterval);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.cmbPeriferic);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.cmb_Port);
            this.DoubleBuffered = true;
            this.Name = "UC_CVMMINI";
            this.Size = new System.Drawing.Size(611, 314);
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.BindingSource bsAllVars;
        private System.Windows.Forms.TextBox tensionL3TextBox;
        private System.Windows.Forms.TextBox tensionL2TextBox;
        private System.Windows.Forms.TextBox tensionL1TextBox;
        private System.Windows.Forms.TextBox potenciaActivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaActivaL3TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL1TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL3TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL2TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL1TextBox;
        private System.Windows.Forms.TextBox corrienteL3TextBox;
        private System.Windows.Forms.TextBox corrienteL2TextBox;
        private System.Windows.Forms.TextBox corrienteL1TextBox;
        private System.Windows.Forms.TextBox frecuenciaTextBox;
        private System.Windows.Forms.TextBox tensionLineaL3L1TextBox;
        private System.Windows.Forms.TextBox tensionLineaL2L3TextBox;
        private System.Windows.Forms.TextBox tensionLineaL1L2TextBox;
        private System.Windows.Forms.TextBox potenciaInductivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL3TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL1TextBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox cmbPeriferic;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
