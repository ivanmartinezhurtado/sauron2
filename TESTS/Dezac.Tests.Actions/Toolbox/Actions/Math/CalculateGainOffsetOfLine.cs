﻿using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Extensions;
using Dezac.Tests.Utils;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(CalculateGainOffsetOfLineActionDescription))]
    public class CalculateGainOffsetOfLineAction : ActionBase
    {
        [Description("Sirve para guardar la variable temporal en los resultados del test")]
        [Category("4. Results")]
        public bool AddToResults { get; set; }

        [Description("Nombre de la variable con el valor del eje de las X del punto inicial")]
        [Category("3.1. Configuration Point1")]
        public ActionVariable PointX1 { get; set; } = new ActionVariable();

        [Description("Nombre de la variable con el valor del eje de las Y del punto inicial")]
        [Category("3.1. Configuration Point1")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable PointY1 { get; set; } = new ActionVariable();


        [Description("Nombre de la variable con el valor del eje de las X del punto final")]
        [Category("3.2. Configuration Point2")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable PointX2 { get; set; } = new ActionVariable();


        [Description("Nombre de la variable con el valor del eje de las Y del punto final")]
        [Category("3.2. Configuration Point2")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable PointY2 { get; set; } = new ActionVariable();


        [Description("Nombre de la variable que se creará como sufijo de la Ganacia y el Offset. Ej: Si llamamos Test al sufijo, se genera dos variables de la siguiente forma: Test_Gain y Test_Offset")]
        [Category("4. Results")]
        public string DataOutput { get; set; } //TODO se debería hacer Dos dataoutputs, uno para el Gain y el otro para el offset.

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var pointInit = new PointOfLine() { x = Convert.ToDouble(PointX1.Value), y = Convert.ToDouble(PointY1.Value) };
            var pointFin = new PointOfLine() { x = Convert.ToDouble(PointX2.Value), y = Convert.ToDouble(PointY2.Value) };

            Logger.InfoFormat("(X1, Y1) = ({0}, {1})", pointInit.x, pointInit.y);
            Logger.InfoFormat("(X2, Y2) = ({0}, {1})", pointFin.x, pointFin.y);

            var calculate = testBase.CalculateGainOffsetOfLine(pointInit, pointFin);
            var varGain = string.Format("{0}_GAIN", DataOutput);
            var varOffset = string.Format("{0}_OFFSET", DataOutput);
            SetVariable(varGain, calculate.gain.ToString());
            SetVariable(varOffset, calculate.offset.ToString());

            Logger.InfoFormat("{0} = {1}    {2} = {3}", varGain, calculate.gain, varOffset, calculate.offset);
            Context.ResultList.Add(varGain, calculate.gain.ToString());
            Context.ResultList.Add(varOffset,  calculate.offset.ToString());

            if (AddToResults)
            {
                testBase.Resultado.Set(varGain, calculate.gain.ToString(), Model.ParamUnidad.Numero);
                testBase.Resultado.Set(varOffset, calculate.offset.ToString(), Model.ParamUnidad.Numero);
            }
        }

        public class CalculateGainOffsetOfLineActionDescription : ActionDescription
        {
            public override string Name { get { return "CalculateGainOffsetLine"; } }
            public override string Category { get { return "Actions/Math"; } }
            public override string Description { get { return "Calcula la ganancia y el Offset de una recta con dos puntos de la recta"; } }
            public override Image Icon { get { return Properties.Resources.Math; } }
        }
    }
}
