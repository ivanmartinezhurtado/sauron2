﻿using Comunications.Utility;
using Dezac.Core.Utility;
using Dezac.Device;
using log4net;
using System;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace Instruments.Measure
{
    [DeviceVersion(1.00)]
    public class CVMMINITower: DeviceBase 
    {
        protected static readonly ILog logger = LogManager.GetLogger("CVMMINITower");

        public CVMMINITower()
        {

        }
        public CVMMINITower(int port)
        {
            SetPort(port);
        }

        public CVMMINITower(SerialPort serialPort)
        {
            serialPort.BaudRate = 9600;
            Modbus = new ModbusDeviceSerialPort(serialPort, logger);
        }

        public void SetPort(int port, byte periferico=1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, logger);

            Modbus.PortCom = port;
            Modbus.PerifericNumber = periferico;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public void SetupDefault(int voltagePrimary = 1, ushort VoltageSecundary = 1, ushort CurrentPrimary = 5, VectorHardwareVoltageInputs Inputvoltage= VectorHardwareVoltageInputs.Primary300V , VectorHardwareCurrentInputs InputCurrent = VectorHardwareCurrentInputs.Primary5A)
        {
            LogMethod();

            WriteHardwareVectorDefault(Inputvoltage, InputCurrent);
            WriteRelationTransformation(voltagePrimary, VoltageSecundary, CurrentPrimary);
        }

        public AllVariables ReadAllVariables()
        {
            LogMethod();

            logger.InfoFormat("ReadAllVariables");

            var Fases = Modbus.Read<VariablesInstantaneas>();
            Fases.L1.Tension /= 10;
            Fases.L1.FactorPotencia /= 100;
            Fases.L2.Tension /= 10;
            Fases.L2.FactorPotencia /= 100;
            Fases.L3.Tension /= 10;
            Fases.L3.FactorPotencia /= 100;

            var Trifasicas = Modbus.Read<VariablesTrifasicas>();
            Trifasicas.CosenoPhiIII /= 100;
            Trifasicas.FactorPotenciaIII /= 100;
            Trifasicas.Frecuencia /= 10;
            Trifasicas.TensionLineaL1L2 /= 10;
            Trifasicas.TensionLineaL2L3 /= 10;
            Trifasicas.TensionLineaL3L1 /= 10;

            AllVariables All = new AllVariables();
            All.Phases = Fases;
            All.Trifasicas = Trifasicas;

            return All;
        }

        public void FlagTest(bool state = true)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void Reset()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public SetupConfig ReadSetupConfig()
        {
            LogMethod();

            return Modbus.Read<SetupConfig>();
        }

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();

            var hardwareVectorReading = Modbus.ReadMultipleCoil((ushort)Registers.HARDWARE_CONFIGURATION, 96);

            return new HardwareVector(hardwareVectorReading);
        }

        public void WriteHardwareVector(HardwareVector hardwareConfig)
        {
            LogMethod();

            GetHardwareVectorConfiguration();

            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_CONFIGURATION, hardwareConfig.ToSingleCoil());

            Reset();

            System.Threading.Thread.Sleep(3000);
        }

        public void WriteHardwareVectorDefault(VectorHardwareVoltageInputs voltagePrimary, VectorHardwareCurrentInputs currentSecundary)
        {

            var hardwareConfig = GetHardwareVectorConfiguration();

            var vectorHard = ReadHardwareVector();

            if (vectorHard.InputCurrent == currentSecundary && vectorHard.InputVoltage == voltagePrimary)
                return;

            FlagTest();

            LogMethod();
            hardwareConfig.InputCurrent = currentSecundary;
            hardwareConfig.InputVoltage = voltagePrimary;
            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_CONFIGURATION, hardwareConfig.ToSingleCoil());

            Reset();

            System.Threading.Thread.Sleep(3000);    
        }

        public void WriteRelationTransformation(int voltagePrimary = 1, ushort VoltageSecundary = 1, ushort CurrentPrimary = 5)
        {
            var setupConfig = ReadSetupConfig();

            if ((setupConfig.VoltagePrimary == voltagePrimary) & (setupConfig.VoltageSecondary == VoltageSecundary) & (setupConfig.CurrentPrimary == CurrentPrimary))
                return;

            setupConfig.VoltageSecondary = VoltageSecundary;
            setupConfig.CurrentPrimary = CurrentPrimary;
            setupConfig.VoltagePrimary = voltagePrimary;

            LogMethod();

            Modbus.Write<SetupConfig>(setupConfig);

            Reset();

            System.Threading.Thread.Sleep(3000);    
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Phases;
            public VariablesTrifasicas Trifasicas;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            public Int32 Tension;
            public Int32 Corriente;
            public Int32 PotenciaActiva;
            public Int32 PotenciaReactiva;
            public Int32 FactorPotencia;
            public Double PotenciaAparente
            {
                get
                {
                    return Tension * Corriente / 1000;
                }
            }          
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x001E)]
        public struct VariablesTrifasicas
        {
            public Int32 PotenciaActivaIII;
            public Int32 PotenciaInductivaIII;
            public Int32 PotenciaCapacitivaIII;
            public Int32 CosenoPhiIII;
            public Int32 FactorPotenciaIII;
            public Int32 Frecuencia;
            public Int32 TensionLineaL1L2;
            public Int32 TensionLineaL2L3;
            public Int32 TensionLineaL3L1;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP_CONFIG)]
        public struct SetupConfig
        {
            public int VoltagePrimary;
            public ushort VoltageSecondary;
            public ushort CurrentPrimary;
            public byte DefaultPage;
            public byte VoltageSimpleOrCompound;
            public byte BacklightTime;
            public byte HarmonicsCalculus;
        }

        public enum Registers
        {
            RESET = 2000,
            FLAG_TEST = 0x2AF8,
            SETUP_CONFIG = 0x044C,
            HARDWARE_CONFIGURATION = 0x2710,
        }

        #region Vector de Wardware

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele2 { get; set; }
            public bool Rele1 { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Cuadrantes { get; set; }
            public bool Shunt { get; set; }
            public bool CalculoHarmonicos { get; set; }

            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value;
                    if (powerSupplyString.ToUpper().Contains("AC"))
                    {
                        if (powerSupplyString.Contains("230"))
                        {
                            PowerSupply = VectorHardwareSupply._230Vac;
                            return;
                        }
                        else
                        {
                            if (powerSupplyString.Contains("400"))
                            {
                                PowerSupply = VectorHardwareSupply._400Vac;
                                return;
                            }
                            else
                            {
                                if (powerSupplyString.Contains("480"))
                                {
                                    PowerSupply = VectorHardwareSupply._480Vac;
                                    return;
                                }
                                else
                                {
                                    if (powerSupplyString.Contains("85") || powerSupplyString.Contains("265"))
                                    {
                                        PowerSupply = VectorHardwareSupply._80_265Vac;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (powerSupplyString.Contains("20") || powerSupplyString.Contains("21") || powerSupplyString.Contains("50") || powerSupplyString.Contains("51"))
                        {
                            PowerSupply = VectorHardwareSupply._21_51Vdc;
                            return;
                        }
                    }
                    throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else
                    {
                        if (inputVoltageString.Contains("110"))
                        {
                            InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                            return;
                        }
                        else
                        {
                            if (inputVoltageString.Contains("500"))
                            {
                                InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                                return;
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("5"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                        return;
                    }
                    else
                    {
                        if (inputCurrentString.Contains("2"))
                        {
                            InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                            return;
                        }
                        else
                        {
                            if (inputCurrentString.Contains("1"))
                            {
                                InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                                return;
                            }
                            else
                            {
                                if (inputCurrentString.ToUpper().Contains("ITF"))
                                {
                                    InputCurrent = VectorHardwareCurrentInputs.ITFext;
                                    return;
                                }
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString.Contains("CVM_MINI"))
                    {
                        Modelo = VectorHardwareModelo.CVM_MINI;
                        return;
                    }
                    throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele1 = (vectorHardware[0] & 0x10) == 0x10;
                Rele2 = (vectorHardware[0] & 0x20) == 0x20;
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                Cuadrantes = (vectorHardware[10] & 0x02) == 0x02;
                CalculoHarmonicos = (vectorHardware[10] & 0x01) == 0x01;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele1;
                vector[5] = Rele2;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[81] = Cuadrantes;
                vector[80] = CalculoHarmonicos;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2,
            ITFext = 7,
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _21_51Vdc = 3,
            _80_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            CVM_MINI = 1
        }

        private HardwareVector GetHardwareVectorConfiguration()
        {
            var hardwareVector = new HardwareVector();
            hardwareVector.Comunication = true;
            hardwareVector.Rs232 = false;
            hardwareVector.Rele1 = false;
            hardwareVector.Rele2 = false;
            hardwareVector.Shunt = true;
            hardwareVector.PowerSupplyString = "230Vac";
            hardwareVector.InputCurrentString = "5A";
            hardwareVector.InputVoltageString = "300V";
            hardwareVector.ModeloString = "CVM_MINI";
            return hardwareVector;
        }

        #endregion
    }
}
