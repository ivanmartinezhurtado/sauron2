﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Device.ThermalStations;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.14)]
    public class EKOR_PLUS_CPUTestPlacas : TestBase
    {
        public EKOR_PLUS EkorPlus;
        private TowerBoard tower;
        public double shuntConsumptionResistance = 0.47D;

        #region Constants
        internal const int ANY_BUTTON = 9999;
        internal const int OUT_SET_BUTTON_ACTUATOR = 7;
        internal const int OUT_ESC_BUTTON_ACTUATOR = 8;
        internal const int OUT_UP_BUTTON_ACTUATOR = 9;
        internal const int OUT_LEFT_BUTTON_ACTUATOR = 10;
        internal const int OUT_RIGHT_BUTTON_ACTUATOR = 11;
        internal const int OUT_DOWN_BUTTON_ACTUATOR = 12;
        internal const int OUT_RESET_BUTTON = 8;
        internal const int OUT_ACTIVATE_POWER_SUPPLY = 17;
        internal const int OUT_DIVISOR_INPUT_CURRENT = 19;
        internal const int OUT_POWER_SUPPLY_SWITCH = 20;
        internal const int OUT_FAN = 15;
        internal const int IN_EKORPLUS_LED = 16;
        internal const int MODBUS_COM = 6;
        #endregion

        public void TestInitialization()
        {
            EkorPlus = new EKOR_PLUS(Comunicaciones.SerialPort);
            EkorPlus.Cirbus.PerifericNumber = Comunicaciones.Periferico;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("There is not any tower instance to initializate in this test");

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.OnWait(200, 4);// Activa Valbula de aire
            tower.IO.DO.OnWait(200, 14);
            tower.IO.DO.OnWait(200, OUT_ACTIVATE_POWER_SUPPLY); //17
            tower.IO.DO.OnWait(200, OUT_POWER_SUPPLY_SWITCH);  //20
        }

        public void TestComunicationsRS232(int porEkorfront9)
        {
            if (porEkorfront9 == 0)
                porEkorfront9 = Comunicaciones.SerialPort;

            EkorPlus.Cirbus.PortCom = porEkorfront9;
            EkorPlus.Cirbus.BaudRate = 9600;
            EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.Even;
            EkorPlus.Cirbus.PerifericNumber = 0;

            string developerVersion = "";
            string  firmwareVersion = "";

            SamplerWithCancel((p) =>
            {
                developerVersion = EkorPlus.ReadVersion();
                firmwareVersion = EkorPlus.ReadFirmware();

                logger.InfoFormat("Puerto de comunicacion RS232 asignado en  COM {0}", porEkorfront9.ToString());

                return true;

            }, "Error. No comunica el equipo por RS232", 3, 500, 1000, false, false);

            Assert.AreEqual("VERSION_DEVELOPER", developerVersion.Trim(), Identificacion.VERSION_EMBEDDED.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);
            Assert.AreEqual("VERSION_FIRMWARE", firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de firmware es incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestComunicationsTTL(int porEkorttL5)
        {
            tower.IO.DO.OffWait(500, OUT_POWER_SUPPLY_SWITCH);

            if (porEkorttL5 == 0)
                porEkorttL5 = Comunicaciones.SerialPort2;

            EkorPlus.Cirbus.PortCom = porEkorttL5;
            EkorPlus.Cirbus.PerifericNumber = Comunicaciones.Periferico;
            EkorPlus.Cirbus.BaudRate = 9600;
            EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.Even;

            string developerVersion = "";
            string firmwareVersion = "";

            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    EkorPlus.RecoveryDefaultSettings(porEkorttL5, 9600, 1, System.IO.Ports.Parity.Even);
                    Delay(2000, "");
                    EkorPlus.Cirbus.PortCom = porEkorttL5;
                    EkorPlus.Cirbus.BaudRate = 9600;
                    EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.Even;
                    EkorPlus.Cirbus.PerifericNumber = 0;
                }
                if (p > 1)
                {
                    logger.InfoFormat("Enviamos configuracion finalización equipo: 0, cirbus, 38400, n, 8, 1");
                    EkorPlus.Cirbus.BaudRate = 38400;
                    EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.None;
                    EkorPlus.Cirbus.PerifericNumber = 0;
                }


                    developerVersion = EkorPlus.ReadVersion();
                    firmwareVersion = EkorPlus.ReadFirmware();

                logger.InfoFormat("Puerto de comunicacion TTL asignado en  COM {0}", porEkorttL5.ToString());

                return true;

            }, "Error. No comunica el equipo por TTL", 3, 500, 1000, false, false);


            Assert.AreEqual("VERSION_DEVELOPER", developerVersion.Trim(), Identificacion.VERSION_EMBEDDED.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                firmwareVersion = EkorPlus.ReadFirmware();
                logger.InfoFormat("Puerto de comunicacion TTL asignado en  COM {0}", porEkorttL5.ToString());
                return true;
            }, "Error. No comunica el equipo", 3, 500, 1000, false, false);

            Resultado.Set("TEST_TTL", "OK", ParamUnidad.s);

            Assert.AreEqual("VERSION_FIRMWARE", firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de firmware es incorrecta"), ParamUnidad.SinUnidad);

            EkorPlus.WriteDate(DateTime.Now);

            EkorPlus.WriteFrameNumber(TestInfo.NumBastidor.ToString());

            EkorPlus.WriteAngleCalibrationInitialization();

            EkorPlus.WriteUserSettings("04b0 012c 003c 003c 003c 003c 00 24 00 00 00 00 0000 0001 0258 00 ff 00");
            Delay(2000, "Escribiendo ajustes usuario para inicio test");

            EkorPlus.WriteInstallerSettings("0143 270f 0A 00 00 00 1964 06 0004 0096 0003 ffa6 00 01 00");
            Delay(2000, "Escribiendo ajustes usuario para inicio test");

            EkorPlus.Cirbus.BaudRate = 9600;
            EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.Even;
            EkorPlus.Cirbus.PerifericNumber = 0;
        }

        public void TestDisplay()
        {
            var model = Identificacion.MODELO;
            var pathImage = string.Empty;

            if (model == "RPA")
                pathImage = "\\EKORPLUS_55\\TEST_DISPLAY_PLACAS_SIN_FLECHAS.jpg";
            else
                pathImage = "\\EKORPLUS_55\\TEST_DISPLAY_PLACAS.jpg";

            EkorPlus.ReadyDisplay();

            var frm = this.ShowForm(() => new ImageView("TEST DISPLAY: Comprobar display y LED encendido", this.PathCameraImages + pathImage), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    EkorPlus.ReadyDisplay();
                    return frm.DialogResult != DialogResult.None;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                if (frm.DialogResult != DialogResult.OK)
                    throw new Exception("Error, Display incorrecto detectado por el operario");

                this.CloseForm(frm);
            }
        }

        public void TestConsumo()
        {
            tower.IO.DO.OnWait(3000, 17);

            //*************************************** RS232
            AdjustValueDef consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("ALIMENTACION_COM").Name, 0, Params.I_DC.Null.TestPoint("ALIMENTACION_COM").Min(), Params.I_DC.Null.TestPoint("ALIMENTACION_COM").Max(), 0, 0, ParamUnidad.mA);

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltDC,
                NumMuestras = 3,
                Rango = consumption.Max,
                DigitosPrecision = 4,
                NPLC = 0
            };

            TestMeasureBase(consumption, (s) =>
                {
                    return tower.MeasureMultimeter(InputMuxEnum.IN2, measure, 1000) / 0.47;
                }, 1, 5, 2000);

            //**************************************** FUENTE 
            tower.IO.DO.OffWait(500, 20);
            consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("ALIMENTACION").Name, 0, Params.I_DC.Null.TestPoint("ALIMENTACION").Min(), Params.I_DC.Null.TestPoint("ALIMENTACION").Max(), 0, 0, ParamUnidad.mA);

            consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("ALIMENTACION").Name, 0, Params.I_DC.Null.TestPoint("ALIMENTACION").Min(), Params.I_DC.Null.TestPoint("ALIMENTACION").Max(), 0, 0, ParamUnidad.mA);

            measure.Rango = consumption.Max;

            TestMeasureBase(consumption, (s) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, measure, 1000) / 0.47;
            }, 1, 5, 2000);

            //**************************************** BATERIA 
            //tower.IO.DO.OnWait(500, 39);
            //Delay(1500, "estabilizacion consumo");

            ////SetLambaInSteps(3.3D, 0.005D, 3);
            //tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3.3, 0.2);
            //Delay(2000, "estabilizacion consumo");

            //tower.IO.DO.OffWait(500, 17);

            //Delay(3000, "Espera de estabilización");

            //consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("BATERIA_SIN_ALIMENTAR").Name, 0, Params.I_DC.Null.TestPoint("BATERIA_SIN_ALIMENTAR").Min, Params.I_DC.Null.TestPoint("BATERIA_SIN_ALIMENTAR").Max, 0, 0, ParamUnidad.mA);

            //var measureAC = new MagnitudToMeasure()
            //{
            //    Frequency = freqEnum._50Hz,
            //    Magnitud = MagnitudsMultimeter.AmpDC,
            //    NumMuestras = 3,
            //    Rango = consumption.Max,
            //    DigitosPrecision = 4,
            //    NPLC = 0
            //};

            //tower.IO.DO.OnWait(500, 39);

            //TestMeasureBase(consumption, (s) =>
            //{
            //    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, measureAC, 1000, whitOutIO: true);
            //}, 1, 5, 2000, "Error de consumo de la bateria, el consumo sin alimentación se encuentra fuera de márgenes");

            //****************************************BATERIA ALIMENTADA
            //tower.IO.DO.On(17);

            //Delay(500, "desconexión multiplexora");

            //consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("BATERIA_ALIMENTADA").Name, 0, Params.I_DC.Null.TestPoint("BATERIA_ALIMENTADA").Min(), Params.I_DC.Null.TestPoint("BATERIA_ALIMENTADA").Max(), 0, 0, ParamUnidad.mA);

            //measureAC.Rango = consumption.Max;

            //TestMeasureBase(consumption, (s) =>
            //{
            //    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, measureAC, 1000);
            //}, 1, 5, 2000);

            //tower.IO.DO.OffWait(500, 39);

            //tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void EmptyModeMeasurement()
        {
            SamplerWithCancel((p) =>
            {
                var Emmptymode = EkorPlus.ReadMeasurementsCurrentDisplay();
                logger.InfoFormat("Error comunicaciones medida en vacio");

                return true;
            }, "Error. No comunica el equipo", 3, 500, 1000, false, false);

           // tower.IO.DO.On(OUT_POWER_SUPPLY_SWITCH);

            TriAdjustValueDef lineMeasurements = new TriAdjustValueDef(Params.V.L1.EnVacio.Name, Params.V.Null.EnVacio.Min(), Params.V.Null.EnVacio.Max(), 0, 0, ParamUnidad.mV, true);

            TestMeasureBase(lineMeasurements,
                (step) =>
                {
                    var result = EkorPlus.ReadMeasurementsVolageDisplay();
                    var VoltageList = new List<double>();
                    VoltageList.Add(result.L1);
                    VoltageList.Add(result.L2);
                    VoltageList.Add(result.L3);
                    VoltageList.Add(result.LH);

                    return VoltageList.ToArray();
                }, 2, 5, 500);

            TriAdjustValueDef lineMeasurements2 = new TriAdjustValueDef(Params.I_AC.L1.EnVacio.Name, Params.I_AC.Null.EnVacio.Min(), Params.I_AC.Null.EnVacio.Max(), 0, 0, ParamUnidad.mA, true);

            TestMeasureBase(lineMeasurements2,
                (step) =>
                {
                    var result = EkorPlus.ReadMeasurementsCurrentDisplay();
                    var CurrentList = new List<double>();
                    CurrentList.Add(result.L1);
                    CurrentList.Add(result.L2);
                    CurrentList.Add(result.L3);
                    CurrentList.Add(result.LH);
                    return CurrentList.ToArray();

                }, 2, 5, 500);

            tower.IO.DO.Off(OUT_POWER_SUPPLY_SWITCH);
        }

        public void TestKeyboard()
        {
            foreach (KeyValuePair<int, EKOR_PLUS.InputReadings> keyToDeactivate in keyboardActuatorsDictionary) //resetear todas teclas
                if (keyToDeactivate.Key != ANY_BUTTON)
                    tower.IO.DO.Off((int)keyToDeactivate.Key);

            Delay(100, "Esperando retirada de los pistones");

            foreach (KeyValuePair<int, EKOR_PLUS.InputReadings> keyToRead in keyboardActuatorsDictionary) //Se comprueva cada una de las teclas
            {
                EkorPlus.WriteClearKeyboard();

                Delay(100, "Espera limpiado operación lectura teclado");

                if (keyToRead.Key != ANY_BUTTON)
                    tower.IO.DO.OnWait(500, (int)keyToRead.Key);

                SamplerWithCancel((s) =>
                {
                    return EkorPlus.ReadKeyboard() == keyToRead.Value;

                }, string.Format("Error al verificar el estado del teclado, imposible detectar {0}", keyToRead.Value.GetDescription()), 70, 100);

                logger.InfoFormat("Testeando tecla {0}", keyToRead.Value.ToString());


                if (keyToRead.Key != ANY_BUTTON)
                    tower.IO.DO.Off((int)keyToRead.Key);
            }

            foreach (KeyValuePair<int, EKOR_PLUS.InputReadings> keyToDeactivate in keyboardActuatorsDictionary) //resetear todas teclas
                if (keyToDeactivate.Key != ANY_BUTTON)
                    tower.IO.DO.Off((int)keyToDeactivate.Key);
        }
        
        public void TestManualReset()
        {
            tower.IO.DO.On(OUT_ESC_BUTTON_ACTUATOR);

            Stopwatch manualResetStopWatch = new Stopwatch();

            manualResetStopWatch.Start();

            EkorPlus.Cirbus.Retries = 1;
            EkorPlus.Cirbus.TimeOut = 500;

            while (manualResetStopWatch.ElapsedMilliseconds < 15000)
                try
                {
                    EkorPlus.ReadVersion();
                }
                catch (Exception)
                {
                    break;
                }

            manualResetStopWatch.Stop();

            EkorPlus.Cirbus.Retries = 3;
            EkorPlus.Cirbus.TimeOut = 2000;

            if (manualResetStopWatch.ElapsedMilliseconds >= 15000)
            {
                Resultado.Set("RESET_MANUAL", "KO", ParamUnidad.SinUnidad);
                throw new Exception("No se ha detectado la activación del reset manual del equipo");
            }

            tower.IO.DO.Off(OUT_ESC_BUTTON_ACTUATOR);

            Resultado.Set("RESET_MANUAL", "OK", ParamUnidad.SinUnidad);
        }

        public void ClockCalibration(string portCrono)
        {
            tower.Chroma.SetVoltageRange(Instruments.PowerSource.Chroma.VoltageRangeEnum.HIGH);
            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            if (string.IsNullOrEmpty(portCrono))
                throw new Exception("No se ha pasado el puerto del CronoThermostato");

            var cronoPort = Convert.ToByte(GetVariable<string>(portCrono, portCrono).Replace("COM", ""));
            tower.IO.DO.On(15);//Activa ventilador ajusto ppm

            double temperature = 22;
            Delay(10000, "Inicio cronotermo");

            //SamplerWithCancel((p) =>
            //    {
            //        using (var crono = new CRONOTERMOSTATO(cronoPort))
            //        {
            //            crono.Modbus.BaudRate = 19200;
            //            var temp = new AdjustValueDef(Params.TEMP.Null.RTC.Name, 0, Params.TEMP.Null.RTC.Min(), Params.TEMP.Null.RTC.Max(), Params.TEMP.Null.RTC.Avg(), Params.TEMP.Null.RTC.Tol(), ParamUnidad.Grados);
            //            var result = TestMeasureBase(temp,
            //               (step) =>
            //               {
            //                   return crono.ReadTemperatureAndConvertToReal();
            //               }, 0, 3, 2000);

            //            temperature = temp.Value;
            //        }
            //        return true;
            //    }, string.Empty, cancelOnException: false, throwException: false);


            tower.Chroma.ApplyOff();

            tower.HP53131A
               .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
               .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 1.5)
               .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            TestMeasureBase(defs,
            (step) =>
            {
                logger.InfoFormat("Configurando la tolerancia del Error en ppm");

                tower.IO.DO.On(15);//Activa ventilador ajusto ppm

                EkorPlus.StartSignalClok(15); // genera una señal de pulsos durante el tiempo especificado inhabilitando comunicaciones

                var medidaFreqMed = tower.MeasureWithFrecuency(InputMuxEnum.IN3, 8000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);

                Delay(10000, "Espera a que acabe de generar el tren de pulsos");
                
                var calibrationClk = EkorPlus.CalibrationClock(medidaFreqMed, temperature);

                return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1, 
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
            }, 0, 2, 1000);

            logger.InfoFormat("Almacenando todos los errores en base de datos");

            EkorPlus.FLASHFormat();
            Delay(2000, "Esperando formateo de la FLASH");

            SamplerWithCancel((p) =>
            {
                var info = EkorPlus.ReadFLASHInformation();

                if (info.Contains("ID"))
                    return true;
                return false;

            }, "Error. No se ha podido formatear la unidad logica FLASH", 10, 1000, 0);

            Resultado.Set("FLASH_FORMAT", "OK", ParamUnidad.SinUnidad);

            EkorPlus.WriteInstallerSettings("0105 270f 0A 00 00 00 1964 06 0004 0096 0003 ffa6 00 00 00");
            Delay(2000, "Configuramos equipo para salida a ajuste y verificación");            
        }

        //MODELO RPA
        //*********************************
        public void TestVUSB(string nameDevicePortVirtual)
        {
            SamplerWithCancel((p) =>
            {
                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKOR_DISK", ParamUnidad.SinUnidad).Trim())
                        return true;
                return false;
            }, "Error No se ha encontrado el dispositivo USB del EKOR", 15, 1500, 2000);

            var portEkor = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));

            logger.InfoFormat("Enviamos configuración por defecto: 0, cirbus, 9600, e, 8, 1");

            EkorPlus.Cirbus.PortCom = portEkor;
            //EkorPlus.Cirbus.BaudRate = 9600;
            //EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.Even;
            //EkorPlus.Cirbus.PerifericNumber = 0;

            string developerVersion = "";
            string firmwareVersion = "";

            SamplerWithCancel((p) =>
            {              
                    developerVersion = EkorPlus.ReadVersion();
                    firmwareVersion = EkorPlus.ReadFirmware();               

                logger.InfoFormat("Puerto de comunicacion USB asignado en  COM {0}", portEkor.ToString());

                return true;
            }, "Error. No comunica el equipo por USB", 3, 500, 1000, false, false);

            Assert.AreEqual("VERSION_DEVELOPER", developerVersion.Trim(), Identificacion.VERSION_EMBEDDED.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);
            Assert.AreEqual("VERSION_FIRMWARE", firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de firmware es incorrecta"), ParamUnidad.SinUnidad);

            var porEkorttL5 = Comunicaciones.SerialPort2;
            EkorPlus.Cirbus.PortCom = porEkorttL5;
        }

        public void TestFinish()
        {
             if (EkorPlus != null)
                EkorPlus.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                tower.IO.DO.Off(OUT_ACTIVATE_POWER_SUPPLY); //17
                tower.IO.DO.Off(OUT_POWER_SUPPLY_SWITCH); //20

                tower.IO.DO.Off(OUT_DOWN_BUTTON_ACTUATOR, OUT_ESC_BUTTON_ACTUATOR, OUT_LEFT_BUTTON_ACTUATOR, OUT_RIGHT_BUTTON_ACTUATOR, OUT_SET_BUTTON_ACTUATOR, OUT_UP_BUTTON_ACTUATOR);

                tower.IO.DO.Off(OUT_FAN, 14, 4);

                tower.Dispose();
            }
        }

        //*****************************************
        //******************************************

        private Dictionary<int, EKOR_PLUS.InputReadings> keyboardActuatorsDictionary = new Dictionary<int, EKOR_PLUS.InputReadings>()
        {
            {ANY_BUTTON, EKOR_PLUS.InputReadings.NO_BUTTON_OR_INPUT},
            {OUT_SET_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.SET_BUTTON},
            {OUT_ESC_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.ESC_BUTTON},
            {OUT_UP_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.UP_BUTTON},
            {OUT_LEFT_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.LEFT_BUTTON},
            {OUT_RIGHT_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.RIGHT_BUTTON},
            {OUT_DOWN_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.DOWN_BUTTON}
        };

    }
}
