﻿using Dezac.Device;
using Dezac.Tests.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ReadMidabusDevieActionDescription))]
    public class ReadMidabusDevieAction : ActionBase
    {
        [Description("Trama Midabus a mandar")]
        [Category("3. Configuration")]
        public string Message { get; set; }

        [Description("Cabecera de la respuesta para que ésta sea válida")]
        [Category("3. Configuration")]
        public string Response { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        [Description("Nombre de la variable donde guardar el valor")]
        [Category("4. Results")]
        public string DataOutput { get; set; }


        [Category("4. Results")]
        [Description("Unidades con las que trabaja el valor a obtener")]
        public ParamUnidad DataOutputUnits { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "MidabusDevice";
            var uut = GetVariable(name) as MidabusDevice;

            var result = uut.Read(Message, Response);

            if (string.IsNullOrEmpty(DataOutput))
                DataOutput = Message;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (string.IsNullOrEmpty(DataOutput))
                DataOutput = Message;

            if (testBase != null)
                testBase.Resultado.Set(DataOutput, result, DataOutputUnits);
            else
                Context.ResultList.Add(string.Format("{0}={1}", DataOutput, result));

            Context.Variables.AddOrUpdate(DataOutput, result);
        }
    }

    public class ReadMidabusDevieActionDescription : ActionDescription
    {
        public override string Name { get { return "ReadMidabus"; } }
        public override string Description { get { return "Lee los registros especificados"; } }
        public override string Category { get { return "Tools/Comunications/Midabus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitMidabusDeviceAction;DisposeMidabusDeviceAction:2"; } }
    }
}
