﻿namespace Dezac.Instruments.Controls
{
    partial class UC_HP_34401
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnReset = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lbl_Vers_tester = new System.Windows.Forms.Label();
            this.btnVAC = new System.Windows.Forms.Button();
            this.btnVDC = new System.Windows.Forms.Button();
            this.btnIDC = new System.Windows.Forms.Button();
            this.btnIAC = new System.Windows.Forms.Button();
            this.btnR = new System.Windows.Forms.Button();
            this.lbl_Mesur_Mult = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.DarkRed;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(386, 119);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(90, 30);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_Port.BackColor = System.Drawing.Color.Black;
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.ForeColor = System.Drawing.Color.LawnGreen;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(426, 14);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(76, 21);
            this.cmb_Port.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lbl_Vers_tester
            // 
            this.lbl_Vers_tester.AutoSize = true;
            this.lbl_Vers_tester.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Vers_tester.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Vers_tester.ForeColor = System.Drawing.Color.Black;
            this.lbl_Vers_tester.Location = new System.Drawing.Point(119, 7);
            this.lbl_Vers_tester.Name = "lbl_Vers_tester";
            this.lbl_Vers_tester.Size = new System.Drawing.Size(51, 13);
            this.lbl_Vers_tester.TabIndex = 154;
            this.lbl_Vers_tester.Text = "Version:";
            this.lbl_Vers_tester.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnVAC
            // 
            this.btnVAC.BackColor = System.Drawing.Color.DimGray;
            this.btnVAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVAC.ForeColor = System.Drawing.Color.White;
            this.btnVAC.Location = new System.Drawing.Point(51, 87);
            this.btnVAC.Name = "btnVAC";
            this.btnVAC.Size = new System.Drawing.Size(90, 30);
            this.btnVAC.TabIndex = 165;
            this.btnVAC.Text = "V (AC)";
            this.btnVAC.UseVisualStyleBackColor = false;
            this.btnVAC.Click += new System.EventHandler(this.btnVAC_Click);
            // 
            // btnVDC
            // 
            this.btnVDC.BackColor = System.Drawing.Color.DimGray;
            this.btnVDC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVDC.ForeColor = System.Drawing.Color.White;
            this.btnVDC.Location = new System.Drawing.Point(225, 87);
            this.btnVDC.Name = "btnVDC";
            this.btnVDC.Size = new System.Drawing.Size(90, 30);
            this.btnVDC.TabIndex = 166;
            this.btnVDC.Text = "V (DC)";
            this.btnVDC.UseVisualStyleBackColor = false;
            this.btnVDC.Click += new System.EventHandler(this.btnVDC_Click);
            // 
            // btnIDC
            // 
            this.btnIDC.BackColor = System.Drawing.Color.DimGray;
            this.btnIDC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIDC.ForeColor = System.Drawing.Color.White;
            this.btnIDC.Location = new System.Drawing.Point(225, 119);
            this.btnIDC.Name = "btnIDC";
            this.btnIDC.Size = new System.Drawing.Size(90, 30);
            this.btnIDC.TabIndex = 168;
            this.btnIDC.Text = "I (DC)";
            this.btnIDC.UseVisualStyleBackColor = false;
            this.btnIDC.Click += new System.EventHandler(this.btnIDC_Click);
            // 
            // btnIAC
            // 
            this.btnIAC.BackColor = System.Drawing.Color.DimGray;
            this.btnIAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIAC.ForeColor = System.Drawing.Color.White;
            this.btnIAC.Location = new System.Drawing.Point(51, 119);
            this.btnIAC.Name = "btnIAC";
            this.btnIAC.Size = new System.Drawing.Size(90, 30);
            this.btnIAC.TabIndex = 167;
            this.btnIAC.Text = "I (AC)";
            this.btnIAC.UseVisualStyleBackColor = false;
            this.btnIAC.Click += new System.EventHandler(this.btnIAC_Click);
            // 
            // btnR
            // 
            this.btnR.BackColor = System.Drawing.Color.DimGray;
            this.btnR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnR.ForeColor = System.Drawing.Color.White;
            this.btnR.Location = new System.Drawing.Point(386, 87);
            this.btnR.Name = "btnR";
            this.btnR.Size = new System.Drawing.Size(90, 30);
            this.btnR.TabIndex = 169;
            this.btnR.Text = "R (Ohm)";
            this.btnR.UseVisualStyleBackColor = false;
            this.btnR.Click += new System.EventHandler(this.btnR_Click);
            // 
            // lbl_Mesur_Mult
            // 
            this.lbl_Mesur_Mult.BackColor = System.Drawing.Color.Black;
            this.lbl_Mesur_Mult.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbl_Mesur_Mult.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Mesur_Mult.ForeColor = System.Drawing.Color.LawnGreen;
            this.lbl_Mesur_Mult.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_Mesur_Mult.Location = new System.Drawing.Point(69, 33);
            this.lbl_Mesur_Mult.Name = "lbl_Mesur_Mult";
            this.lbl_Mesur_Mult.Size = new System.Drawing.Size(335, 39);
            this.lbl_Mesur_Mult.TabIndex = 132;
            this.lbl_Mesur_Mult.Text = "0.0000000,00";
            this.lbl_Mesur_Mult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UC_HP_34401
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BackgroundImage = global::Dezac.Instruments.Properties.Resources.hp34401A_01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.lbl_Mesur_Mult);
            this.Controls.Add(this.btnR);
            this.Controls.Add(this.btnIDC);
            this.Controls.Add(this.btnIAC);
            this.Controls.Add(this.btnVDC);
            this.Controls.Add(this.btnVAC);
            this.Controls.Add(this.lbl_Vers_tester);
            this.Controls.Add(this.btnReset);
            this.DoubleBuffered = true;
            this.Name = "UC_HP_34401";
            this.Size = new System.Drawing.Size(518, 171);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        internal System.Windows.Forms.Label lbl_Vers_tester;
        private System.Windows.Forms.Button btnR;
        private System.Windows.Forms.Button btnIDC;
        private System.Windows.Forms.Button btnIAC;
        private System.Windows.Forms.Button btnVDC;
        private System.Windows.Forms.Button btnVAC;
        internal System.Windows.Forms.Label lbl_Mesur_Mult;
    }
}
