﻿using Dezac.Tests.Model;

namespace Dezac.Tests.Parameters
{
    public class FasesParameters
    {
        private TestPointsParameters psl;

        public string Variable { get { return psl.Variable; } set { psl.Variable = value; } }

        public ParamUnidad Unidad { get { return psl.Unidad; } set { psl.Unidad = value; } }

        public TestPointsParameters Null { get { psl.Fase = string.Empty; return psl; } }

        public TestPointsParameters L1 { get { psl.Fase = ConstantsParameters.Fases.L1; return psl; } }

        public TestPointsParameters L2 { get { psl.Fase = ConstantsParameters.Fases.L2; return psl; } }

        public TestPointsParameters L3 { get { psl.Fase = ConstantsParameters.Fases.L3; return psl; } }

        public TestPointsParameters L4 { get { psl.Fase = ConstantsParameters.Fases.L4; return psl; } }

        public TestPointsParameters L5 { get { psl.Fase = ConstantsParameters.Fases.L5; return psl; } }

        public TestPointsParameters L6 { get { psl.Fase = ConstantsParameters.Fases.L6; return psl; } }

        public TestPointsParameters LN { get { psl.Fase = ConstantsParameters.Fases.LN; return psl; } }

        public TestPointsParameters LK { get { psl.Fase = ConstantsParameters.Fases.LK; return psl; } }

        public TestPointsParameters LN_CALC { get { psl.Fase = ConstantsParameters.Fases.LN_CALC; return psl; } }

        public TestPointsParameters L12 { get { psl.Fase = ConstantsParameters.Fases.L12; return psl; } }

        public TestPointsParameters L23 { get { psl.Fase = ConstantsParameters.Fases.L23; return psl; } }

        public TestPointsParameters L31 { get { psl.Fase = ConstantsParameters.Fases.L31; return psl; } }

        public TestPointsParameters III { get { psl.Fase = ConstantsParameters.Fases.III; return psl; } }

        public string Name { get { return Variable; } }

        public TestPointsParameters Other(string value)
        {
            if (string.IsNullOrEmpty(value))
                psl.Fase = "";
            else
                psl.Fase = "_" + value;

            return psl;
        }

        public TestPointsParameters Fase(int value)
        {
            psl.Fase = "_L" + value.ToString();
            return psl;
        }

        public FasesParameters(ParamValueCollection margenes)
        {
            psl = new TestPointsParameters(margenes);
        }
    }
}
