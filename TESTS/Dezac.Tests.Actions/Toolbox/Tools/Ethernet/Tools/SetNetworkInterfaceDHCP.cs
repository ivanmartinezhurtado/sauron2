﻿using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(SetNetworkInterfaceDHCPDescription))]
    public class SetNetworkInterfaceDHCP : ActionBase
    {
     
        public override void Execute()
        {
            NetUtils.ConfigIP_DHCP();

            var ip_pc = NetUtils.GetMyIP();
            var ip_getaway = NetUtils.GetDefaultGateway();

            SetVariable("IP_PC", ip_pc);
            SetVariable("IP_GETAWAY", ip_getaway);
        }
    }

    public class SetNetworkInterfaceDHCPDescription : ActionDescription
    {
        public override string Name { get { return "DHCPClient"; } }
        public override string Category { get { return "Tools/Ethernet/Tools"; } }
        public override string Description { get { return "Configura la interfaz de red especificada como cliente DHCP"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
    }
