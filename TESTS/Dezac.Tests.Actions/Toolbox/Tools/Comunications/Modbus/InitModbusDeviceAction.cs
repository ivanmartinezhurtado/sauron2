﻿using Dezac.Device;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;
using Dezac.Core.Exceptions;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(InitModbusDeviceActionDescription))]
    public class InitModbusDeviceAction : ActionBase
    {
        [Description("Número de Periférico")]
        [Category("3. Configuration")]
        public ActionVariable NumPeripheral { get; set; } = new ActionVariable();

        [Description("Tipo transporte")]
        [Category("3. Configuration")]
        [DefaultValue(TipoTransporte.SERIAL_PORT)]
        public TipoTransporte TransportType { get; set; }

        [Description("Puerto serie (COM del puerto o nombre de la variable que lo contiene)")]
        [Category("3.1 Serial Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]

        public ActionVariable SerialPort { get; set; } = new ActionVariable();

        [Description("BaudRate velocidad transmision")]
        [Category("3.1 Serial Configuration")]
        public BpsEnum Bps { get; set; }

        [Description("Hostname (Direccion Ip o nombre de red del dispositivo o nombre de la variable que la contiene ")]
        [Category("3.1 Ethernet Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable HostName { get; set; } = new ActionVariable();

        [Description("Puerto (socket) por el que nos conectamos")]
        [Category("3.1 Ethernet Configuration")]
        public ActionVariable Port { get; set; } = new ActionVariable();

        [Description("Tipo modbus TCP (RTU o TCP)")]
        [Category("3.1 Ethernet Configuration")]
        [DefaultValue(ModbusTCPProtocol.TCP)]
        public ModbusTCPProtocol ModbusTypeTCP { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [DefaultValue("ModbusDevice")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            ModbusDevice device;

            if (TransportType == TipoTransporte.SERIAL_PORT)
            {
                var portDevice = Convert.ToByte(SerialPort.Value);
                device = new ModbusDeviceSerialPort(portDevice, (int)Bps, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);
            }
            else
            {
                if(HostName.IsKeyNullOrEmpty)
                    TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("HostName").Throw();

                var hostName = HostName.Value.ToString();
                var puerto = Convert.ToInt32(Port.Value);
                device = new ModbusDeviceTCP(hostName, puerto, ModbusTypeTCP);
            }

            device.PerifericNumber = Convert.ToByte(NumPeripheral.Value);  
            Context.Variables.AddOrUpdate(DeviceName ?? "ModbusDevice", device);
        }
    }

    public class InitModbusDeviceActionDescription : ActionDescription
    {
        public override string Name { get { return "Init Modbus Device"; } }
        public override string Description { get { return "Crea una instancia de un dispositivo modbus"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "DisposedModbusDeviceAction:2"; } }
    }
}
