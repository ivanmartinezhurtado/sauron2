namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTFASEFILE
    {
        public int NUMTFF { get; set; }
        public int NUMTESTFASE { get; set; }
        public string IDTIPODOC { get; set; }
        public byte[] FICHERO { get; set; }
        public string NOMBRE { get; set; }
    
        public virtual T_TESTFASE T_TESTFASE { get; set; }
    }
}
