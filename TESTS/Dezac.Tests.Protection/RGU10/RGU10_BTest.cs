﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.04)]
    public class RGU10_BTest : RGU10TestBase
    {
        public void TestAdjustDC()
        {
            var currentScala1 = Consignas.GetDouble(Params.I.L1.Ajuste.esc_1_DC.Name, 0, ParamUnidad.A);
            var currentScala2 = Consignas.GetDouble(Params.I.L1.Ajuste.esc_2_DC.Name, 3, ParamUnidad.A);
            var currentScala3 = Consignas.GetDouble(Params.I.L1.Ajuste.esc_3_DC.Name, 0, ParamUnidad.A);

            var currentDictionaryTemporal = new Dictionary<RGU10.ScaleCalibration,double>()
            {
                {RGU10.ScaleCalibration.SCALE_1,currentScala1},
                {RGU10.ScaleCalibration.SCALE_2,currentScala2},
                {RGU10.ScaleCalibration.SCALE_3,currentScala3},
            };

            Rgu.WriteFlagTest();

            foreach (KeyValuePair<RGU10.ScaleCalibration, double> scale in currentDictionaryTemporal)
            {
                var NumEspiras = 1;

                if (scale.Value > 1.5)
                {
                    NumEspiras = 5;
                    Tower.IO.DO.OnWait(100, OUT_CORRIENTE_DC_5ESPIRA);
                }
                else
                    Tower.IO.DO.OnWait(100, OUT_CORRIENTE_DC_1ESPIRA);

                Tower.LAMBDA.ApplyPresets(2, scale.Value / NumEspiras);
                Delay(5000, "Estabiliazando LAMBDA");

                var adjustValue = new AdjustValueDef(Params.I.Null.Ajuste.TestPoint(scale.Key.ToString() + "_DC"), ParamUnidad.Puntos);

                var timeOut = 500;

                var lambdaCurrent = TestMeasureBase(adjustValue, (s) =>
                {
                    timeOut = timeOut + 500;
                    logger.DebugFormat("Espera de {0} ms para la medida", timeOut);
                    return Tower.MeasureMultimeter(InputMuxEnum.IN8, MagnitudsMultimeter.AmpDC, timeOut) * NumEspiras;
                }, 0, 4, 500);

                adjustValue = new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(scale.Key.ToString() + "_DC"), ParamUnidad.Puntos);

                var resadjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    Rgu.WriteStartCalibration(scale.Key);

                    Delay(1500, "Esperando a la finalización de la calibración.");

                    var resultadoCalibracion = Rgu.ReadCalibrationResultDC();

                    return resultadoCalibracion / 8;

                }, 0, 4, 1500);

                Delay(500, "Espera para no colapsar el equipo");

                Rgu.WriteDCScaleConstant(Rgu.calibrationDCScaleDictionary[scale.Key], (ushort)resadjustResult.Value);

                Delay(500, "Espera para no colapsar el equipo");

                var lecturaConstante = Rgu.ReadDCScaleConstant(Rgu.calibrationDCScaleDictionary[scale.Key]);

                Assert.AreEqual(lecturaConstante, (ushort)resadjustResult.Value, Error().UUT.CONFIGURACION.NO_GRABADO(String.Format("Error. No se ha grabado correctamente la constante de la {0} DC", scale.Key.ToString())));

                Tower.LAMBDA.ApplyOffAndWaitStabilisation();

                Delay(100, "");

                Tower.IO.DO.Off(OUT_CORRIENTE_DC_1ESPIRA, OUT_CORRIENTE_DC_5ESPIRA);
            }
        }

        public void TestAdjustAC()
        {
            var currentScala1 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_1.Name, 0.6, ParamUnidad.A), 0 };
            var currentScala2 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_2.Name, 3, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.Ajuste.esc_2.Name, 0, ParamUnidad.A) };
            var currentScala3 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_3.Name, 0, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.Ajuste.esc_3.Name, 1.5, ParamUnidad.A) };

            var currentDictionaryTemporal = new Dictionary<RGU10.ScaleCalibration, double[]>()
            {
                {RGU10.ScaleCalibration.SCALE_1,currentScala1},
                {RGU10.ScaleCalibration.SCALE_2,currentScala2},
                {RGU10.ScaleCalibration.SCALE_3,currentScala3},
            };

            Rgu.WriteFlagTest();

            foreach (KeyValuePair<RGU10.ScaleCalibration, double[]> scale in currentDictionaryTemporal)
            {
                Tower.IO.DO.On(OUT_CORRIENTE_L1, OUT_CORRIENTE_L2);

                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, new TriLineValue { L1 = scale.Value[0], L2 = scale.Value[1], L3 = 0 }, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

                Tower.IO.DO.OffWait(150, OUT_CORRIENTE_L1, OUT_CORRIENTE_L2);

                var adjustValue = new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(scale.Key.ToString()), ParamUnidad.Puntos);

                var resadjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    Rgu.WriteStartCalibration(scale.Key);

                    Delay(1500, "Esperando a la finalización de la calibración.");

                    var resultadoCalibracion = Rgu.ReadCalibrationResultAC();

                    return Math.Sqrt(resultadoCalibracion / 8);

                }, 0, 4, 1500);

                Delay(500, "Espera para no colapsar el equipo");

                Rgu.WriteACScaleConstant(Rgu.calibrationACScaleDictionary[scale.Key], (ushort)resadjustResult.Value);

                Delay(500, "Espera para no colapsar el equipo");

                var lecturaConstante = Rgu.ReadACScaleConstant(Rgu.calibrationACScaleDictionary[scale.Key]);

                Assert.AreEqual(lecturaConstante, (ushort)resadjustResult.Value, Error().UUT.CONFIGURACION.NO_GRABADO(String.Format("Error. No se ha grabado correctamente la constante de la {0} AC", scale.Key.ToString())));
            }

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        protected override void WriteCostumerTableCurrentTime()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad);

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad);

            var times = GetTimeScaleTable(timeTableElements);

            Rgu.WriteAllowParametersWriting();

            var current = Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 20, ParamUnidad.mA);

            var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)current);

            if (posCurrent.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la I_DISPARO_DEFECTO no existe en la tabla de corrientes");

            CurrentPosition = posCurrent.FirstOrDefault().Position;

            Resultado.Set("POSICION_I_DISPARO_DEFECTO", CurrentPosition.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteTriggerCurrentSelection(posCurrent.FirstOrDefault().Position);

            var tiempo = Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 100, ParamUnidad.ms);

            var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)tiempo);

            if (posTime.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la T_RETARDO_DEFECTO no existe en la tabla de tiempos");

            TimePosition = posTime.FirstOrDefault().Position;

            Resultado.Set("POSICION_T_RETARDO_DEFECTO", TimePosition.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteTriggerTimeSelection(posTime.FirstOrDefault().Position);

            Rgu.WriteSetupWriting();
        }

    }
}
  