﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using FileHelpers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.05)]
    public class CVMC10ROGOWSKITest : CVMC10TEST
    {

        public override void TestInitialization(int portCVM = 0)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            Cvm.Modbus.PortCom = portCVM;
            Cvm.Modbus.PerifericNumber = Convert.ToByte(1);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            tipoAlimentacionDC = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad).Contains("Vdc");

            if (tipoAlimentacionDC)
                SamplerWithCancel((p) =>
                {
                    Tower.LAMBDA.Initialize();
                    Tower.LAMBDA.ApplyOff();
                    return true;
                }, "Error. No se puede establecer comunicación con la fuente DC LAMBDA", 2, 1500, 0, false, false);
            else
                Tower.Chroma.ApplyOff();

            Tower.FLUKE.ApplyOff();

            Tower.IO.DO.OffWait(500, OUTPUT_PISTON_EXPULSION_EQUIPO);
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();

            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);
        }

        public override void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            if (Cvm.ReadHardwareVector().In_Medida)
                Tower.IO.DO.On(OUTPUT_NEUTRO);

            var offsetsV = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = Cvm.ReadAllVariables();
                    return new double[] { vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                        vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current };

                }, delFirts, samples, timeInterval);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };
            var angleSimulationRogowsky = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, -90, ParamUnidad.Grados);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 0, angleSimulationRogowsky, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defsFreq = new AdjustValueDef { Name = Params.FREQ.Null.Ajuste.Name, Min = Params.FREQ.Null.Ajuste.Min(), Max = Params.FREQ.Null.Ajuste.Max(), Unidad = ParamUnidad.Hz };

            Cvm.FlagTest();

            int Sincronismo = 0;
            int i = 0;

            TestMeasureBase(defsFreq,
              (step) =>
              {
                  i++;
                  Cvm.WriteSynchronism(10000);
                  Delay(1500, "Esperando grabación factor de defecto de auste de sincronismo...");
                  var freq = Cvm.ReadAllVariables().ThreePhase.Frequency;
                  Sincronismo = (int)(50000000 / (freq * 100)) + i;
                  Cvm.WriteSynchronism((ushort)Sincronismo);
                  Delay(1500, "Esperando grabación factor calculado de sincronismo...");
                  return Cvm.ReadAllVariables().ThreePhase.Frequency;

              }, 0, 5, timeInterval);

            Resultado.Set(Params.SYNC.Null.Ajuste.Name, Sincronismo, ParamUnidad.Puntos);

            Delay(2000, "Esperando grabación factor calculado de auste de sincronismo...");

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var relacionTransformacionIntegradorRogowski = 50;  //0.1V entrada --> 5A salida
            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation * relacionTransformacionIntegradorRogowski;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation * relacionTransformacionIntegradorRogowski;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation * relacionTransformacionIntegradorRogowski;

            if (Cvm.ReadHardwareVector().In_Medida)
                crucePistasI.Neutro = new AdjustValueDef(Params.I.LN.CrucePistas.Name, 0, 0, 0, ConsignasI.L1 * relacionTransformacionIntegradorRogowski, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A);

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varslist = new List<double>(){vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                     vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varslist.Add(vars.ThreePhase.NeutralCurrent);

                    return varslist.ToArray();

                }, delFirts, samples, timeInterval);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();
        }

        public override void TestDefaultParameters()
        {
            var hardwareVector = new CVMC10.HardwareVector();
            hardwareVector.Comunication = VectorHardware.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rs232 = VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.In_Medida = VectorHardware.GetString("MEDIDA_NEUTRO", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Flex = VectorHardware.GetString("FLEX", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele1 = VectorHardware.GetString("RELE1", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele2 = VectorHardware.GetString("RELE2", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele3 = VectorHardware.GetString("RELE3", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele4 = VectorHardware.GetString("RELE4", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "5A", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "300V", ParamUnidad.SinUnidad);
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "CVM-C10", ParamUnidad.SinUnidad);

            SetVariable("VectorHardware", hardwareVector);

            Cvm.WriteHardwareVector(hardwareVector);
            Cvm.WriteComunications();
            Cvm.WritePassword((ushort)Configuracion.GetDouble("PASSWORD", 1234, ParamUnidad.SinUnidad));

            var bloqueoSetup = Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad) == "NO" ? false : true;

            Cvm.WriteSetupLock(bloqueoSetup);
            Cvm.WriteTransformationRatio();
            current = Configuracion.GetDouble("SECONDARY_CURRENT", 1, ParamUnidad.A);

            int imputCurrentPrimary = 1;

            if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary5A)
                imputCurrentPrimary = 5;
            else if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary2A)
                imputCurrentPrimary = 2;
            else if (hardwareVector.InputCurrent == CVMC10.VectorHardwareCurrentInputs.Primary1A)
                imputCurrentPrimary = 1;

            transformationRelation = imputCurrentPrimary / current;

            Cvm.WriteMaximumDemandConfiguration();

            var voltageGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 3625, ParamUnidad.Puntos);
            var currentGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO", 11500, ParamUnidad.Puntos);
            var activepowerGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KW + "_DEFECTO", 5436, ParamUnidad.Puntos);
            var gainGapDefault = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_DESFASE + "_DEFECTO", 1000, ParamUnidad.Puntos);


            var currentGainEsc2 = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_ESC2_DEFECTO", 500, ParamUnidad.Puntos);
            var activePowerGainEsc2 = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KW + "_ESC2_DEFECTO", 3800, ParamUnidad.Puntos);
            var GainsDefaultEScala2 = new CVMC10.MeasureGainsEsc2();
            GainsDefaultEScala2.Current = new CVMC10.TriUShort { L1 = (ushort)currentGainEsc2, L2 = (ushort)currentGainEsc2, L3 = (ushort)currentGainEsc2 };
            GainsDefaultEScala2.neutralCurrent = (ushort)currentGainEsc2;
            GainsDefaultEScala2.Power = new CVMC10.TriUShort { L1 = (ushort)activePowerGainEsc2, L2 = (ushort)activePowerGainEsc2, L3 = (ushort)activePowerGainEsc2 };

            Cvm.WriteDefaultGains((ushort)voltageGain, (ushort)currentGain, (ushort)activepowerGain, (ushort)gainGapDefault);
            Cvm.WriteMeasureGainsEscala2(GainsDefaultEScala2);

            Cvm.WriteInitialMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));
            Cvm.WriteScreenSelection(Convert.ToInt32(0x0003FFFF));
            Cvm.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumBastidor.Value));
            Cvm.WriteAllAlarmsDefault();

            if (Configuracion.GetString("CONFIG_ALARM", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                Dictionary<CVMC10.AlarmRegisters, string> alarmConfigurations = new Dictionary<CVMC10.AlarmRegisters, string>();

                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_1, Configuracion.GetString("ALARM_REL1", "PARAM_CODE=200;HI=170;LO=90;DELAYON=60;DELAYOFF=60;HYST=0;LATCH=0;STATE=NA", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_2, Configuracion.GetString("ALARM_REL2", "NO", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_3, Configuracion.GetString("ALARM_TR1", "PARAM_CODE=200;HI=170;LO=90;DELAYON=60;DELAYOFF=60;HYST=0;LATCH=0;STATE=NA", ParamUnidad.SinUnidad));
                alarmConfigurations.Add(CVMC10.AlarmRegisters.ALARM_4, Configuracion.GetString("ALARM_TR2", "NO", ParamUnidad.SinUnidad));

                foreach (KeyValuePair<CVMC10.AlarmRegisters, string> alarm in alarmConfigurations)
                    if (!alarm.Value.Contains("NO"))
                    {
                        var fileHelper = new FileHelperEngine<CVMC10_File_Alarms>();
                        fileHelper.ErrorMode = ErrorMode.IgnoreAndContinue;
                        var alarmConfiguration = fileHelper.ReadString(alarm.Value);

                        var alarmConfig = new CVMC10.AlarmConfiguration()
                        {
                            variableNumber = Convert.ToUInt16(alarmConfiguration[0].PARAM_CODE),
                            maxValue = Convert.ToInt32(alarmConfiguration[0].HI),
                            minValue = Convert.ToInt32(alarmConfiguration[0].LO),
                            alarmDelay = Convert.ToUInt16(alarmConfiguration[0].DELAYON),
                            alarmOffDelay = Convert.ToUInt16(alarmConfiguration[0].DELAYOFF),
                            histeresis = Convert.ToUInt16(alarmConfiguration[0].HYST),
                            latching = Convert.ToUInt16(alarmConfiguration[0].LATCH),
                            alarmLogic = Convert.ToUInt16(alarmConfiguration[0].STATE)
                        };

                        Cvm.WriteAlarm(alarm.Key, alarmConfig);
                    }
            }

            Cvm.WriteSynchronism(10000);

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyOff();
            else
                Tower.Chroma.ApplyOff();

            Delay(2000, "Espera para el reset de hardware");

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyAndWaitStabilisation();
            else
                Tower.Chroma.ApplyAndWaitStabilisation();

            SamplerWithCancel((p) =>
            {
                Cvm.FlagTest();
                return true;
            }, "Error. No se puede establecer comunicación con el equipo despues del reset", 10, 500);

            var vector = Cvm.ReadHardwareVector();
            var vectorHardwareBBDD = GetVariable<CVMC10.HardwareVector>("VectorHardware", vector);

            vector.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vector.VectorHardwareBoolean, vectorHardwareBBDD.VectorHardwareBoolean, Error().UUT.CONFIGURACION.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el grabado"));
        }

        public override void TestHardwareVerification()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 60, ParamUnidad.Grados);
            var angleSimulationRogowsky = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, -90, ParamUnidad.Grados);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();
            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleGap + angleSimulationRogowsky, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.THD_V.L1.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new TriAdjustValueDef(Params.THD_I.L1.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            TestMeasureBase(defs,
                (step) =>
                {
                    var THD = Cvm.ReadTotalHarmonicDistorsion();
                    return new double[] { THD.VoltageL1, THD.VoltageL2, THD.VoltageL3, THD.CurrentL1, THD.CurrentL2, THD.CurrentL3 };
                }, 2, 10, 1000);

            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 50, angleGap + angleSimulationRogowsky, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.Name, 0, Params.FREQ.Null.Verificacion.Min(), Params.FREQ.Null.Verificacion.Max(), 0, 0, ParamUnidad.Hz);
            TestMeasureBase(defsFreq,
              (step) =>
              {
                  var freq = Cvm.ReadFrequency();
                  return freq.Instantaneous / (double)100;
              }, 2, 10, 200);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();

        }

        #region ADJUST WITH FLUKE OF CVMC10 ROGOWSKI

        public override void TestAdjust(double PF = 1, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Cvm.FlagTest();

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.1, ParamUnidad.V);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.V), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.V), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.V) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, PF, ParamUnidad.Grados);


            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            if (Cvm.ReadHardwareVector().In_Medida)
                triGains[1].Neutro = new AdjustValueDef(Params.GAIN_I.LN.Ajuste.Name, 0, Params.GAIN_I.LN.Ajuste.Min(), Params.GAIN_I.LN.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            triGains.Add(new TriAdjustValueDef(Params.GAIN_KW.L1.Ajuste.Name, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.L23.Ajuste.Name, Params.GAIN_V.L31.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            //var currentReadings = tower.PowerSourceIII.ReadCurrent();

            var adjustCurrentValues = new CVMC10.TriDouble() { L1 = 5, L2 = 5, L3 = 5 };

            var measureGains = Cvm.CalculateAdjustMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltage, adjustCurrentValues, transformationRelation,
                (value) =>
                {

                    triGains[0].L1.Value = value.Voltage.L1;
                    triGains[0].L2.Value = value.Voltage.L2;
                    triGains[0].L3.Value = value.Voltage.L3;

                    triGains[1].L1.Value = value.Current.L1;
                    triGains[1].L2.Value = value.Current.L2;
                    triGains[1].L3.Value = value.Current.L3;

                    if (triGains[1].Neutro != null)
                        triGains[1].Neutro.Value = value.neutralCurrent.Value;

                    triGains[2].L1.Value = value.Power.L1;
                    triGains[2].L2.Value = value.Power.L2;
                    triGains[2].L3.Value = value.Power.L3;

                    triGains[3].L1.Value = value.Compound.L12;
                    triGains[3].L2.Value = value.Compound.L23;
                    triGains[3].L3.Value = value.Compound.L31;

                    return !HasError(triGains, false).Any();

                });

            foreach (var res in triGains)
                res.AddToResults(Resultado);

            if (HasError(triGains, false).Any())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            Cvm.WriteMeasureGains(measureGains.Item2);

        }

        public override void TestGapAdjust(double PF = 1, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.AjusteDesfase.Name, 0.1, ParamUnidad.V);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, PF, ParamUnidad.Grados);
            var angleSimulationRogowsky = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, PF, ParamUnidad.Grados);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            //Tower.FLUKE.ApplyAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 50, angleSimulationRogowsky - angleGap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleGap + angleSimulationRogowsky, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.L1.Ajuste.Min(), Params.GAIN_DESFASE.L1.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var powerFactor = Math.Cos((angleGap.ToRadians()));

            var voltageRef = new TriLineValue { L1 = adjustVoltage, L2 = adjustVoltage, L3 = adjustVoltage };
            var currentRef = new TriLineValue { L1 = 5, L2 = 5, L3 = 5 };

            currentRef.L1 -= adjustCurrentOffsets.L1 *= transformationRelation;
            currentRef.L2 -= adjustCurrentOffsets.L2 *= transformationRelation;
            currentRef.L3 -= adjustCurrentOffsets.L3 *= transformationRelation;

            var powerReference = new CVMC10.TriDouble()
            {
                L1 = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians())),
                L2 = voltageRef.L2 * currentRef.L2 * Math.Cos((angleGap.ToRadians())),
                L3 = voltageRef.L3 * currentRef.L3 * Math.Cos((angleGap.ToRadians())),
            };

            var offsetGains = Cvm.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, powerReference, powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                },
                (activePowerOffset) => (activePowerOffset.L1 > 0 && activePowerOffset.L2 > 0 && activePowerOffset.L3 > 0));

            triDefs.AddToResults(Resultado);

            Assert.IsTrue(offsetGains.Item1, Error().UUT.MEDIDA.MARGENES("Error valor de ajuste de ganancia del desfase fuera de márgenes"));

            Cvm.WritePhaseGapGains(offsetGains.Item2);
        }

        public void TestAdjustRogowskiEscala2(double PF = 0, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.esc_2.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.esc_2.Name, 0.01, ParamUnidad.V);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.V), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.V), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.V) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.esc_2.Name, PF, ParamUnidad.Grados);

            Tower.FLUKE.ApplyOffAndWaitStabilisation();
            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 0, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var triGains = new List<TriAdjustValueDef>();

            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.esc_2.Name, Params.GAIN_I.Null.Ajuste.esc_2.Min(), Params.GAIN_I.Null.Ajuste.esc_2.Max(), 0, 0, ParamUnidad.Puntos));

            if (Cvm.ReadHardwareVector().In_Medida)
                triGains[0].Neutro = new AdjustValueDef(Params.GAIN_I.LN.Ajuste.esc_2.Name, 0, Params.GAIN_I.LN.Ajuste.esc_2.Min(), Params.GAIN_I.LN.Ajuste.esc_2.Max(), 0, 0, ParamUnidad.Puntos);

            triGains.Add(new TriAdjustValueDef(Params.GAIN_KW.L1.Ajuste.esc_2.Name, Params.GAIN_KW.Null.Ajuste.esc_2.Min(), Params.GAIN_KW.Null.Ajuste.esc_2.Max(), 0, 0, ParamUnidad.Puntos));

            var adjustCurrentValues = new CVMC10.TriDouble() { L1 = 0.5, L2 = 0.5, L3 = 0.5 };

            var measureGains = Cvm.CalculateAdjustMeasureFactorsEscala2(delFirts, initCount, samples, timeInterval, adjustVoltage, adjustCurrentValues, transformationRelation,
                (value) =>
                {

                    triGains[0].L1.Value = value.Current.L1;
                    triGains[0].L2.Value = value.Current.L2;
                    triGains[0].L3.Value = value.Current.L3;

                    triGains[0].Neutro.Value = value.neutralCurrent;

                    triGains[1].L1.Value = value.Power.L1;
                    triGains[1].L2.Value = value.Power.L2;
                    triGains[1].L3.Value = value.Power.L3;



                    return !HasError(triGains, false).Any();

                });

            foreach (var res in triGains)
                res.AddToResults(Resultado);

            if (HasError(triGains, false).Any())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            Cvm.WriteMeasureGainsEscala2(measureGains.Item2);
        }

        #endregion

        public override void TestVerification(double PF = 60, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, PF, ParamUnidad.Grados);
            var angleSimulationRogowsky = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, -90, ParamUnidad.Grados);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleGap + angleSimulationRogowsky, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, toleranciaI, ParamUnidad.A));
            if (Cvm.ReadHardwareVector().In_Medida)
                defs[2].Neutro = new AdjustValueDef(Params.I.LN.Verificacion.Name, 0, 0, 0, current, toleranciaI, ParamUnidad.A);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varsList = new List<double>() {
                      vars.Phases.L1.ActivePower, vars.Phases.L2.ActivePower, vars.Phases.L3.ActivePower,
                      vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage,
                      vars.Phases.L1.Current , vars.Phases.L2.Current, vars.Phases.L3.Current};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(vars.ThreePhase.NeutralCurrent);

                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = new TriLineValue { L1 = adjustVoltage, L2 = adjustVoltage, L3 = adjustVoltage };
                    var currentRef = new TriLineValue { L1 = 5, L2 = 5, L3 = 5 }; ;

                    currentRef.L1 = (currentRef.L1 - adjustCurrentOffsets.L1) * transformationRelation;
                    currentRef.L2 = (currentRef.L2 - adjustCurrentOffsets.L2) * transformationRelation;
                    currentRef.L3 = (currentRef.L3 - adjustCurrentOffsets.L3) * transformationRelation;

                    TriLineValue PowerActive = new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1 * Math.Cos((angleGap.ToRadians())),
                        L2 = voltageRef.L2 * currentRef.L2 * Math.Cos((angleGap.ToRadians())),
                        L3 = voltageRef.L3 * currentRef.L3 * Math.Cos((angleGap.ToRadians())),
                    };

                    var varsList = new List<double>() {
                        PowerActive.L1, PowerActive.L2, PowerActive.L3,
                        voltageRef.L1, voltageRef.L2, voltageRef.L3,
                        currentRef.L1, currentRef.L2 , currentRef.L3};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(currentRef.L1);

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);
        }

        public void TestVerificationEscala2(double PF = 1, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.esc_2.Name, 0.01, ParamUnidad.V);
            var adjustCurrentOffsets = new TriLineValue() { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A), L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A), L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A) };
            var realAdjustCurrents = new TriLineValue() { L1 = adjustCurrent + adjustCurrentOffsets.L1, L2 = adjustCurrent + adjustCurrentOffsets.L2, L3 = adjustCurrent + adjustCurrentOffsets.L3 };
            var angleSimulationRogowsky = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, -90, ParamUnidad.Grados);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(adjustVoltage, realAdjustCurrents, 0, angleSimulationRogowsky, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var toleranciaI = Params.I.Null.Verificacion.esc_2.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.esc_2.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.esc_2.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.esc_2.Name, 0, 0, 0, toleranciaI, ParamUnidad.A));
            if (Cvm.ReadHardwareVector().In_Medida)
                defs[1].Neutro = new AdjustValueDef(Params.I.LN.Verificacion.Name, 0, 0, 0, current, toleranciaI, ParamUnidad.A);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadAllVariables();

                    var varsList = new List<double>() {
                      vars.Phases.L1.ActivePower, vars.Phases.L2.ActivePower, vars.Phases.L3.ActivePower,
                      vars.Phases.L1.Current , vars.Phases.L2.Current, vars.Phases.L3.Current};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(vars.ThreePhase.NeutralCurrent);

                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = new TriLineValue { L1 = adjustVoltage, L2 = adjustVoltage, L3 = adjustVoltage };
                    var currentRef = new TriLineValue { L1 = 0.5, L2 = 0.5, L3 = 0.5 };

                    currentRef.L1 = (currentRef.L1 - adjustCurrentOffsets.L1) * transformationRelation;
                    currentRef.L2 = (currentRef.L2 - adjustCurrentOffsets.L2) * transformationRelation;
                    currentRef.L3 = (currentRef.L3 - adjustCurrentOffsets.L3) * transformationRelation;

                    TriLineValue PowerActive = new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1,
                        L2 = voltageRef.L2 * currentRef.L2,
                        L3 = voltageRef.L3 * currentRef.L3
                    };

                    var varsList = new List<double>() {
                        PowerActive.L1, PowerActive.L2, PowerActive.L3,
                        currentRef.L1, currentRef.L2 , currentRef.L3};

                    if (Cvm.ReadHardwareVector().In_Medida)
                        varsList.Add(currentRef.L1);

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);
        }

        public override void TestCustomization()
        {
            Cvm.FlagTest();

            Cvm.WriteBacNetID(TestInfo.BacNetID.Value);

            Resultado.Set("BACNET_ID", TestInfo.BacNetID.Value, ParamUnidad.SinUnidad);

            Cvm.WriteBacNetMAC(2);

            var convenioMedida = Configuracion.GetDouble("CONVENIO_MEDIDA", 0, ParamUnidad.SinUnidad);

            var resultadoConvenioMedida = convenioMedida == 0 ? "CIRCUTOR" : convenioMedida == 1 ? "IEC" : "IEEE";

            Resultado.Set("CONVENIO_MEDIDA", resultadoConvenioMedida, ParamUnidad.SinUnidad);

            Cvm.WriteMedidaConveni((ushort)convenioMedida);

            Cvm.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));

            Delay(600, "Escribimos Numero de Serie");

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyOff();
            else
                Tower.Chroma.ApplyOff();

            Delay(2000, "Espera para el reset de hardware");

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyAndWaitStabilisation();
            else
                Tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            SamplerWithCancel((p) => { Cvm.FlagTest(); return true; }, "Error. El equipo no comunica al volver a encenderlo", 5, 1000);

            Cvm.CleanRegisters(CVMC10.ClearRegisters.BORRAR_TODO);

            Cvm.WriteTransformationRatio((uint)1, (ushort)1, (ushort)1000); //Escribimos transformacion del primario de corriente = 1000.
            var numserie = Cvm.ReadSerialNumber();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, TestInfo.NumSerie, numserie.ToString(), Error().UUT.CONFIGURACION.NO_CORRESPONDE_CON_ENVIO("Error. valor de numero de serie leido no coincide con el grabado"), ParamUnidad.SinUnidad);

            var vectorHardware = Cvm.ReadHardwareVector();

            Resultado.Set(ConstantsParameters.Identification.VECTOR_HARDWARE, vectorHardware.VectorHardwareTrama, ParamUnidad.SinUnidad);

            var vectorHardwareBBDD = GetVariable<CVMC10.HardwareVector>("VectorHardware", vectorHardware);

            vectorHardware.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vectorHardwareBBDD.VectorHardwareBoolean, vectorHardware.VectorHardwareBoolean, Error().UUT.CONFIGURACION.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el de la base de datos"));

            Delay(1000, "Esperamos borrado de energia");

        }
    }
}
