﻿using Comunications.Message;
using Dezac.Core.Utility;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DisposeHexSerialPortDescription))]
    public class DisposeHexSerialPort : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_HEX")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM_HEX";
            var uut = GetVariable(name) as MessageTransport;
            if (uut != null)
                uut.Dispose();
        }
    }

    public class DisposeHexSerialPortDescription : ActionDescription
    {
        public override string Name { get { return "DisposedHEXSerialPort"; } }
        public override string Description { get { return "Cierra el puerto serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/HEX"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
