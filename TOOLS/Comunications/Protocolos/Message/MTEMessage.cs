﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Comunications.Message
{
    public class MTEMessage : StringMessage
    {
        private readonly char COMMAND_SEPARATOR = ';';
        private static CultureInfo enUS = new CultureInfo("en-US");

        private readonly Dictionary<string, string> responses = new Dictionary<string, string> {
            { "?1", "E@" }, { "?2", "EA" },{ "?3", "EB" },{ "?4", "EC" }, { "?5", "ED" }, { "?6", "EE" }, { "?7", "EF" }, { "?8", "EG" }, { "?9", "EH" },
            { "?10", "EI" }, { "?11", "EJ" }, { "?12", "EK" }, { "?13", "EL" }, { "?21", "E^" }, { "?22", "E_" }, { "?23", "Ea" }, { "?26", "Ed" },{ "?29", "Eg" }
        };


        public static MTEMessage Create(string format, params object[] args)
        {
            MTEMessage message = new MTEMessage { Format = format };

            message.SetArgs(enUS, args);

            return message;
        }

        public int NumCommands
        {
            get { return this.Text.Split(COMMAND_SEPARATOR).Length; }
        }

        public string[] Commands
        {
            get
            {
                return this.Text.Split(COMMAND_SEPARATOR);
            }
        }

        public string GetFormattedText()
        {
            int p = Text.IndexOf(',');

            if (p > 0)
                return Text.Substring(p + 1).Replace("--------", "0");

            return Text;
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            if (Validator != null)
            {
                if (!Validator((StringMessage)response))
                    throw new FormatException("Error, formato de la trama de respuesta del MTE Messsage incorrecta");

                return;
            }

            string[] commands = Commands;
            string[] results = ((MTEMessage)response).Commands;

            if (commands.Length != results.Length)
                throw new FormatException("Error, longitud de la trama de respuesta del MTE Messsage incorrecta");

            for (int i = 0; i < results.Length; i++)
            {
                string cmd = commands[i];
                bool valid = false;

                if (cmd.StartsWith("?"))
                {
                    valid = responses.ContainsKey(cmd) && (results[i].StartsWith(responses[cmd]));
                    if (!valid)
                        throw new FormatException("MTEmessage incorrecto");
                }
                else
                {
                    valid = cmd + "=O" == results[i];
                    if (!valid)
                        throw new IOException(string.Format("Error al consignar {0}=E", cmd));
                }
            }
        }
    }
}
