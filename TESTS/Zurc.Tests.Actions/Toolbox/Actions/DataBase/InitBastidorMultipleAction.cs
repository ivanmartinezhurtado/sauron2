﻿using Dezac.Tests;
using Dezac.Tests.Actions;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;
using Zurc.Tests.Actions.Services;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.0)]
    [DesignerAction(Type = typeof(InitBastidorMultipleActionDescription))]
    public class InitBastidorMultipleAction : ActionBase
    {
        [Category("1.- Configuration")]
        [DefaultValue(1)]
        public int Rows { get; set; }

        [Category("1.- Configuration")]
        [DefaultValue(1)]
        public int Columns { get; set; }

        private static object _obj = new object();

        public override void Execute()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                WaitAllTestsAndRunOnlyOnce("_INITBASTIDORES_", InternalExecute);
            else
                InternalExecute();

            lock (_obj)
            {
                BastidorService.BastidorValidation();
                System.Threading.Thread.Sleep(500);
            }
        }

        private void InternalExecute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            SetSharedVariable("ROWS", Rows);
            SetSharedVariable("COLUMNS", Columns);

            BastidorService.BastidorController(testBase, Rows, Columns);
        }
    }

    public class InitBastidorMultipleActionDescription : ActionDescription
    {
        public override string Name { get { return "InitBastidorMultiple"; } }
        public override string Category { get { return "ZurcTest/Trazabilidad"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
    }
}
