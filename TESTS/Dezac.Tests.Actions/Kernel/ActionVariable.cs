﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using Newtonsoft.Json;
using System.ComponentModel;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace Dezac.Tests.Actions.Kernel
{
    [TypeConverter(typeof(SerializableExpandableObjectConverter))]
    public class ActionVariable
    {
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Clave de la Variables con simbolo $. Sin $ coge directamente el valor")]
        public string Key { get; set; }

        [JsonIgnore]
        [Browsable(false)]
        [Description("Valor de la Variable")]
        public object Value 
        { 
            get
            {
                var value = VariablesTools.ResolveValue(Key, null, null, true, true, IsSharedVariable);
                if (AddToResults)
                {
                    if (Unit == Model.ParamUnidad.SinDefinir)
                        TestException.Create().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA($"Unidades de la variable {Key} sin definir").Throw();
                    var testBase = SequenceContext.Current.Variables.Get<TestBase>("TEST_UUT", () => { return null; });
                    if (testBase == null)
                        TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();
                    testBase.Resultado.Set(Key.Replace("$",""), value.ToString(), Unit);
                }
                return value;
            }
            set
            {
                VariablesTools.SetVariable(Key, value, IsSharedVariable);
                if (AddToResults)
                {
                    if (Unit == Model.ParamUnidad.SinDefinir)
                        TestException.Create().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA($"Unidades de la variable {Key} sin definir").Throw();
                    var testBase = SequenceContext.Current.Variables.Get<TestBase>("TEST_UUT", () => { return null; });
                    if (testBase == null)
                        TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();
                    testBase.Resultado.Set(Key.Replace("$", ""), value.ToString(), Unit);
                }
            }
        }

        [JsonIgnore]
        [Browsable(false)]
        public bool IsKeyNullOrEmpty { get { return string.IsNullOrEmpty(Key); } }

        [Description("True si la Variable ha de ser guardada como resultado de test.")]
        public bool AddToResults { get; set; }

        [Description("True si la Variable ha de ser guardada y cargada solo de SharedVariables. False si la Variables ha de ser guardada y cargada solo de Variables. Vacio si la variable ha de ser buscada en Variables y SharedVariables y guardada en Variables")]
        [DefaultValue(null)]
        public bool? IsSharedVariable { get; set; } = null;

        [Description("Unidad de la Variable")]
        public Model.ParamUnidad Unit { get; set; }

        public ActionVariable()
        {      
        }

        public ActionVariable(string key)
        {
            Key = key;
        }

        public override string ToString()
        {
            return Key;
        }

        public T GetValue<T>()
        {
            return RunnerHelper.ChangeType<T>(Value);
        }

        public static implicit operator ActionVariable(string key)
        {
            return new ActionVariable(key);
        }

        public static implicit operator ActionVariable(double key)
        {
            return new ActionVariable(key.ToString().Replace(".",","));
        }

        public static implicit operator ActionVariable(int key)
        {
            return new ActionVariable(key.ToString());
        }

        public static implicit operator ActionVariable(bool key)
        {
            return new ActionVariable(key.ToString());
        }
    }
}
