﻿using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Instruments.Towers;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Metering.Placas
{
    public class CIRCAMP_PSU_TestPlacas : TestBase
    {
        CirwattDlms sbt;
        TowerBoard tower;

        private const int IN_RELAY_ENERGY_CONTROL_1 = 6;
        private const int IN_RELAY_ENERGY_CONTROL_2 = 7;
        private const int IN_RELAY_ENERGY_CONTROL_3 = 8;
        private const int IN_RELAY_ENERGY_CONTROL_4 = 14;
        private const int IN_RELAY_IMPULSE_CONTROL_1 = 10;
        private const int IN_RELAY_IMPULSE_CONTROL_2 = 11;
        private const int IN_RELAY_IMPULSE_CONTROL_3 = 12;
        private const int IN_RELAY_IMPULSE_CONTROL_4 = 13;

        private const int OUT_INPUTS_1_3_5 = 21;
        private const int OUT_INPUTS_2_4 = 22;

        public void TestInitialization()
        {
            sbt = AddInstanceVar(new CirwattDlms(), "UUT");
            sbt.Retries = 4;
            sbt.PuertoCom = Comunicaciones.SerialPort;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            Logger.InfoFormat("COM PORT:{0}", sbt.PuertoCom);

            Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void TestCommunications485()
        {
            sbt.PuertoCom = Comunicaciones.SerialPort2;

            var firmwareVersion = sbt.LeerVersionSW();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error la versión de firmware de la placa no es la esperada"), ParamUnidad.SinUnidad);

            sbt.PuertoCom = Comunicaciones.SerialPort;
        }

        public void TestRelays()
        {
            sbt.WriteRelayEnergyControl((byte)CirwattDlms.enumFases.QUATRIFASICO, CirwattDlms.enumStateRelay.Closed);
            
            foreach (var relayInfo in EnergyControlRelayOutputsTowerInputsRelationship)
            {
                var inputReading = tower.IO.DI.Read(relayInfo.Key);

                Assert.IsTrue(inputReading.Value, Error().UUT.CALIBRACION.RELE(string.Format("Error al detectar el estado inicial del rele de corte de {0}", relayInfo.Value.GetDescription())));
            }

            sbt.WriteRelayEnergyControl((byte)CirwattDlms.enumFases.QUATRIFASICO, CirwattDlms.enumStateRelay.Open);

            foreach (var relayInfo in EnergyControlRelayOutputsTowerInputsRelationship)
            {
                var inputReading = tower.IO.DI.Read(relayInfo.Key);

                Assert.IsFalse(inputReading.Value, Error().UUT.CALIBRACION.RELE(string.Format("Error al detectar el estado inicial del rele de corte de {0}", relayInfo.Value.GetDescription())));
            }

            sbt.WriteRelayImpulseControl((byte)CirwattDlms.enumFases.QUATRIFASICO, CirwattDlms.enumStateRelay.Closed);

            foreach (var relayInfo in ImpulseControlRelayOutputsTowerInputsRelationship)
            {
                var inputReading = tower.IO.DI.Read(relayInfo.Key);

                Assert.IsTrue(inputReading.Value, Error().UUT.CALIBRACION.RELE(string.Format("Error al detectar el estado inicial del rele de corte de {0}", relayInfo.Value.GetDescription())));
            }

            sbt.WriteRelayImpulseControl((byte)CirwattDlms.enumFases.QUATRIFASICO, CirwattDlms.enumStateRelay.Open);

            foreach (var relayInfo in ImpulseControlRelayOutputsTowerInputsRelationship)
            {
                var inputReading = tower.IO.DI.Read(relayInfo.Key);

                Assert.IsFalse(inputReading.Value, Error().UUT.CALIBRACION.RELE(string.Format("Error al detectar el estado inicial del rele de corte de {0}", relayInfo.Value.GetDescription())));
            }
        }

        public void TestInputs()
        {
            var inputsInfo = sbt.ReadInputsImpulse();

            Assert.AreEqual(inputsInfo, CirwattDlms.enumKeyInputs.NOTHING, Error().UUT.COMUNICACIONES.SETUP(string.Format("Error al detectar el estado inicial de las entradas, se detectan la/s siguiente/s entrada/s activa/s {0}", inputsInfo.ToString())));

            tower.IO.DO.OnWait(100, OUT_INPUTS_1_3_5);

            inputsInfo = sbt.ReadInputsImpulse();

            var inputsReading = inputsInfo.HasFlag(CirwattDlms.enumKeyInputs.IN1 | CirwattDlms.enumKeyInputs.IN3 | CirwattDlms.enumKeyInputs.REARME);

            Assert.IsTrue(inputsReading, Error().UUT.COMUNICACIONES.SETUP("Error al leer las entradas 1, 3 y de rearme, no se detecta alguna entrada"));

            tower.IO.DO.OffWait(100, OUT_INPUTS_1_3_5);

            tower.IO.DO.OnWait(100, OUT_INPUTS_2_4);

            inputsInfo = sbt.ReadInputsImpulse();

            inputsReading = inputsInfo.HasFlag(CirwattDlms.enumKeyInputs.IN2 | CirwattDlms.enumKeyInputs.IN4);

            Assert.IsTrue(inputsReading, Error().UUT.COMUNICACIONES.SETUP("Error al leer las entradas 1, 3 y de rearme, no se detecta alguna entrada"));

            tower.IO.DO.OffWait(100, OUT_INPUTS_2_4);
        }

        public void TestFinish()
        {
            if (sbt != null)
                sbt.Dispose();

            if (tower != null)
                tower.Dispose();
        }

        Dictionary<int, CirwattDlms.enumFases> EnergyControlRelayOutputsTowerInputsRelationship = new Dictionary<int, CirwattDlms.enumFases>()
        {
            {IN_RELAY_ENERGY_CONTROL_1, CirwattDlms.enumFases.FASE_1},
            {IN_RELAY_ENERGY_CONTROL_2, CirwattDlms.enumFases.FASE_2},
            {IN_RELAY_ENERGY_CONTROL_3, CirwattDlms.enumFases.FASE_3},
            {IN_RELAY_ENERGY_CONTROL_4, CirwattDlms.enumFases.FASE_4}
        };

        Dictionary<int, CirwattDlms.enumFases> ImpulseControlRelayOutputsTowerInputsRelationship = new Dictionary<int, CirwattDlms.enumFases>()
        {
            {IN_RELAY_IMPULSE_CONTROL_1, CirwattDlms.enumFases.FASE_1},
            {IN_RELAY_IMPULSE_CONTROL_2, CirwattDlms.enumFases.FASE_2},
            {IN_RELAY_IMPULSE_CONTROL_3, CirwattDlms.enumFases.FASE_3},
            {IN_RELAY_IMPULSE_CONTROL_4, CirwattDlms.enumFases.FASE_4}
        };


    }
}
