namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTERROR
    {
        public int NROERROR { get; set; }
        public Nullable<int> NUMTESTFASE { get; set; }
        public int NUMERROR { get; set; }
        public string PUNTOERROR { get; set; }
        public string EXTENSION { get; set; }
        public string ERRORCODE { get; set; }
    
        public virtual OPE_ERROR OPE_ERROR { get; set; }
        public virtual T_TESTFASE T_TESTFASE { get; set; }
        public virtual T_IRISERROR T_IRISERROR { get; set; }
    }
}
