﻿using System;
using System.Configuration;
using System.IO;

namespace Dezac.Tests.Reports.Utils
{
    public static class AppProfile
    {
        public static string ProgramPath
        {
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }

        public static string GetDataUserFile(string name)
        {
            string path = GetDataUserDir();
            string fileName = Path.Combine(path, name);

            return fileName;
        }

        public static string GetDataUserDir()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WinTaskRunner");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public static string ReportsPath
        {
            get { return ConfigurationManager.AppSettings["PathReports"]; }
        }
    
    }
}
