namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_VALORPARAMETROTOL
    {
        public int NUMGRUPO { get; set; }
        public int NUMPARAM { get; set; }
        public string TIPOTOL { get; set; }
        public int NUMGRUPO1 { get; set; }
        public int NUMPARAM1 { get; set; }
        public int NUMGRUPO2 { get; set; }
        public int NUMPARAM2 { get; set; }
    
        public virtual T_VALORPARAMETRO T_VALORPARAMETRO { get; set; }
        public virtual T_VALORPARAMETRO T_VALORPARAMETRO1 { get; set; }
        public virtual T_VALORPARAMETRO T_VALORPARAMETRO2 { get; set; }
    }
}
