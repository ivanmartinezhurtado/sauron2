﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Forms;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using RTF;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TaskRunner.Model;
using TaskRunner.Enumerators;
using WinTestPlayer.Services;
using System;

namespace WinTestPlayer.Views
{
    public partial class TestResultsView : InputMatrixImageView
    {
        private RunnerTestsInfo results;

        private List<InputMatrixImageItemView> items = new List<InputMatrixImageItemView>();

        public TestResultsView()
        {
            InitializeComponent();
        }

        public TestResultsView(RunnerTestsInfo e)
            : this()
        {
            results = e;
            
            Configure(e);
        }

        private void Configure(RunnerTestsInfo e)
        {
            Rows = 1;
            Columns = results.NumInstances;

            if (results.NumInstances > 1)
            {
                Rows = e.Results.FirstOrDefault().Context.SharedVariables.Get<int>("ROWS");
                Columns = e.Results.FirstOrDefault().Context.SharedVariables.Get<int>("COLUMNS");
            }           
            
            TotalItems = Columns * Rows;

            ItemInitializer = CreateMatrixItemControl;

            Build();
        }

        private Control CreateMatrixItemControl(int row, int col, int num)
        {
            var test = results.Results.Where(p => p.NumInstance == (num + 1)).FirstOrDefault();

            var data = test.Context.Services.Get<ITestContext>();

            //var fileName = "";

            Image image = Properties.Resources.barcode_scan_icon;

            //if (data.NumFamilia.HasValue)
            //    fileName = @"\\sfserver01\idp\Idp Público\IMAGESOURCE\FAMILIAS\" + data.NumFamilia + ".png";

            //if (File.Exists(fileName))
            //    image = Image.FromFile(fileName);

            var ctrl = new InputMatrixImageItemView
            {
                Title = string.Format("Nº BASTIDOR {0}", num + 1),
                Image = image,
                ViewLogger = cmdVerInforme,
                IsVisibleLogger = true,
            };

            var width = ctrl.Width * col < (Screen.PrimaryScreen.Bounds.Width - 100) ? ctrl.Width : (Screen.PrimaryScreen.Bounds.Width - 100) / col;
            ctrl.Width = width;

            var isOk = test.ExecutionResult == TestExecutionResult.Completed;
            if (!isOk)
            {
                ctrl.InputText = data.TestInfo.NumBastidor.ToString();
                ctrl.InfoRichText = BuildMessage(test);
                ctrl.ShowText();
            }

            ctrl.IsValid = isOk;

            items.Add(ctrl);

            return ctrl;
        }

        private string BuildMessage(TestResult test)
        {
            var sb = new RTFBuilder();

            sb.AppendLine();

            if (test.ExecutionResult == TestExecutionResult.Completed)
            {
                foreach (StepResult step in test.StepResults)
                {
                    if (!string.IsNullOrEmpty(step.GetErrorMessage()))
                    {
                        ResultError(sb, test, false);
                    }
                }
            }
            else
                ResultError(sb, test, test.ExecutionResult == TestExecutionResult.Aborted);

            //sb.Append(GetBoxReportMessage(boxReportMessage));

            //sb.Append("}");

            return sb.ToString();
        }

        private void ResultError(RTFBuilder sb, TestResult test, bool aborted)
        {
            sb.InsertImage(Properties.Resources.ErrorIcon).Alignment(StringAlignment.Near);
            sb.FontSize(35).FontStyle(FontStyle.Regular).Font(RTFFont.Impact).Alignment(StringAlignment.Center);

            if (aborted)
                sb.AppendLineFormat(" --- TEST {0} CANCELADO POR EL USUARIO ---", test.NumInstance);
            else
                sb.AppendLineFormat(" --- TEST {0} CON ERRORES!! ---", test.NumInstance);

            sb.AppendLine();

            var data = test.Context.Services.Get<ITestContext>();
            List<StepResult> stepsWithExceptions = test.StepResults.Where(p => p.Exception != null).ToList();
            foreach (StepResult sr in stepsWithExceptions)
            {
                var error = sr.Exception as SauronException;

                sb.FontSize(20).FontStyle(FontStyle.Bold).Font(RTFFont.ArialBlack).Append("Función: ").Append(sr.Name);
                sb.AppendLine();
                sb.AppendLine();

                sb.FontSize(20).FontStyle(FontStyle.Bold).Font(RTFFont.ArialBlack).Append("Error: ").Append($"{sr.GetErrorMessage()}" );
                sb.AppendLine();
            }
        }

        public override bool ValidateView(DialogResult result)
        {
            // Yes = Siguiente
            // No = Repetir
            // Cancel = Cancelar

            if (result == DialogResult.Cancel)
                return true;

            var iShell = DI.Resolve<ShellService>();

            var codes = Bastidores;

            int num;

            var sb = new StringBuilder();

            for (int i = 0; i < codes.Count; i++)
            {
                var isEmpty = string.IsNullOrEmpty(codes[i]);
                var view = GetView<InputMatrixImageItemView>(i);

                if (!view.IsDisabled)
                {
                    if (isEmpty)
                    {
                        view.IsValid = false;
                        sb.AppendFormat("Falta informar el bastidor nº {0}!\n", (i + 1));
                    }
                    else if (!int.TryParse(codes[i], out num))
                    {
                        view.IsValid = false;
                        sb.AppendFormat("El nº de bastidor {0} no es válido!\n", (i + 1));
                    }
                    else
                        view.IsValid = true;
                }
            }

            if (codes.GroupBy(p => p).Any(p => p.Count() > 1 && !string.IsNullOrEmpty(p.Key)))
                sb.Append("Hay nº de bastidores repetidos!");

            if (sb.Length > 0)
            {
                iShell.MsgBox(sb.ToString(), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        public List<string> Bastidores
        {
            get
            {
                return items.Select(p => p.InputText).ToList();
            }
        }

        private void cmdVerInforme()
        {
            LogService.ShowLog(FormatLogViewer.XSLFilesEnum.DebugLog, false, null, false);
        }

        public new void Dispose()
        {
            foreach (InputMatrixImageItemView item in items)
            {
                item.Dispose();
            }

            base.Dispose();
        }
    }
}
