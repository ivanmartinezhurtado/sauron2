namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class DIARIOMARCAJE
    {
        public DIARIOMARCAJE()
        {
            this.DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
        }
    
        public int NUMMARCAJE { get; set; }
        public string IDOPERARIO { get; set; }
        public int NUMHOJA { get; set; }
        public System.DateTime FECHA { get; set; }
        public Nullable<decimal> PRECIO { get; set; }
        public Nullable<System.DateTime> TIME_ID { get; set; }
        public string USERNAME { get; set; }
        public Nullable<System.DateTime> FECHAREGISTRO { get; set; }
        public string IDOPERARIOCONTROL { get; set; }
        public string COMPUTERNAME { get; set; }
    
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual HOJAMARCAJE HOJAMARCAJE { get; set; }
        public virtual OPERARIO OPERARIO1 { get; set; }
    }
}
