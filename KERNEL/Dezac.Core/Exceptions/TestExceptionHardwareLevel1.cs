﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionHardwareLevel1
    {
        public string[] Category { get; set; }

        public string[] SubCategory { get; set; }

        public SauronException RDNI_INCORRECTO()
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.RDNI_INCORRECTO, null).Exception();
        }

        public SauronException BLOQUE_SEGURIDAD()
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.BLOQUE_SEGURIDAD, null).Exception();
        }

        public SauronException LASER(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.LASER, parameter).Exception();
        }

        public SauronException ROUTER(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.ROUTER, parameter).Exception();
        }

        public SauronException ENTRADAS_DIGITALES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.ENTRADAS_DIGITALES, parameter).Exception();
        }

        public SauronException SALIDAS_DIGITALES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.SALIDAS_DIGITALES, parameter).Exception();
        }

        public SauronException DETECCION_INSTRUMENTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.DETECCION_INSTRUMENTO, parameter).Exception();
        }

        public SauronException DETECCION_EQUIPO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.DETECCION_EQUIPO, parameter).Exception();
        }

        public SauronException PNEUMATICA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.PNEUMATICA, parameter).Exception();
        }

        public SauronException CONFIGURACION_INSTRUMENTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.CONFIGURACION_INSTRUMENTO, parameter).Exception();
        }
        public SauronException CALIBRACION_CADUCADA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.CALIBRACION_CADUCADA, parameter).Exception();
        }

        public SauronException ASISTENCIA_CADUCADA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.ASISTENCIA_CADUCADA, parameter).Exception();
        }

        public SauronException CONSIGNAR_FUENTE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.CONSIGNAR_FUENTE, parameter).Exception();
        }

        public SauronException LECTURA_FUENTE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.LECTURA_FUENTE, parameter).Exception();
        }

        public SauronException FUNCION_PTE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.FUNCION_PTE, parameter).Exception();
        }

        public SauronException MEDIDA_INSTRUMENTO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.MEDIDA_INSTRUMENTO, parameter).Exception();
        }

        public SauronException ADAPTADOR_DE_RED_ETHERNET(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.ADAPTADOR_DE_RED_ETHERNET, parameter).Exception();
        }
        public SauronException ADAPTADOR_DE_RED_WIFI(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.ADAPTADOR_DE_RED_WIFI, parameter).Exception();
        }
        public SauronException BORNE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.HardwareLevel1.BORNE, parameter).Exception();
        }
    }
}
