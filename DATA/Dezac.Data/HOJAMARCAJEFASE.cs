namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.HOJAMARCAJEFASE")]
    public partial class HOJAMARCAJEFASE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HOJAMARCAJEFASE()
        {
            DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMHOJA { get; set; }

        public int NUMORDEN { get; set; }

        [StringLength(200)]
        public string NOTAS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }

        public virtual ORDEN ORDEN { get; set; }
    }
}
