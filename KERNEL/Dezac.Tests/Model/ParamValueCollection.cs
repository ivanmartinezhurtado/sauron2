﻿using Dezac.Core.Utility;
using Dezac.Tests.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskRunner.Model;

namespace Dezac.Tests.Model
{
    public class ParamValueCollection : List<ParamValue>
    {
        protected static readonly ILog _logger = LogManager.GetLogger("PARAMS_UUT");

        public event EventHandler<ParamValue> ItemChanged;

        public ParamValueCollection(string name)
        {
            Name = name;
        }

        public string Name { get; protected set; }

        public double GetDouble(string key, double defValue, bool createIfNotExists = true)
        {
            ParamValue pValue = Get(key);

            if (pValue == null)
            {
                if (!createIfNotExists)
                    return defValue;

                pValue = new ParamValue { Name = key, Valor = defValue.ToString() };
            }

            pValue.Valor = pValue.Valor.Replace(".", ",");

            return Convert.ToDouble(pValue.Valor);
        }

        public double GetDouble(string key, double defValue, ParamUnidad unidad)
        {
            ParamValue pValue = Get(key);

            if (pValue == null)
            {
                pValue = new ParamValue { Name = key, Valor = defValue.ToString(), IdUnidad = unidad };

                this.Add(pValue);
            }

            pValue.Valor = pValue.Valor.Replace(".", ",");

            return Convert.ToDouble(pValue.Valor);
        }

        public string GetString(string key, string defValue, bool createIfNotExists = true)
        {
            ParamValue pValue = Get(key);

            if (pValue == null)
            {
                if (!createIfNotExists)
                    return defValue;

                pValue = new ParamValue { Name = key, Valor = defValue };
            }

            if (pValue.IdTipoValor == 2)
                if (pValue.Valor.LongCount(p => p.Equals(".")) == 1)
                    pValue.Valor = pValue.Valor.Replace(".", ",");

            return pValue.Valor;
        }

        public string GetString(string key, string defValue, ParamUnidad unidad)
        {
            ParamValue pValue = Get(key);

            if (pValue == null)
            {
                pValue = new ParamValue { Name = key, Valor = defValue, IdUnidad = unidad };

                this.Add(pValue);
            }

            if (pValue.IdTipoValor == 2)
                if (pValue.Valor.LongCount(p => p.Equals(".")) == 1)
                    pValue.Valor = pValue.Valor.Replace(".", ",");

            return pValue.Valor;
        }

        public DateTime GetDate(string key, DateTime defValue, bool createIfNotExists = true)
        {
            ParamValue pValue = Get(key);

            if (pValue == null)
            {
                if (!createIfNotExists)
                    return defValue;

                pValue = new ParamValue { Name = key, Valor = defValue.ToString("dd/MM/yyyy HH:mm:ss") };
            }

            return Convert.ToDateTime(pValue.Valor);
        }

        public virtual void Set(string key, string value, ParamUnidad unidad, string expectedValue = null)
        {
            var item = Get(key);
            if (item == null)
                item = Add(key, value, unidad, null, null);
            else
                item.Valor = value;

            item.IdUnidad = unidad;
            item.ValorEsperado = expectedValue;

            NotifyChanges(item);
        }

        public virtual void Set(string key, double value, ParamUnidad unidad, double? min = null, double? max = null)
        {
            var item = Get(key);

            value = Math.Round(value, 12);

            if (min.HasValue)
                min = Math.Round(min.Value, 12);

            if (max.HasValue)
                max = Math.Round(max.Value, 12);

            if (item == null)
                item = Add(key, value.ToString(), unidad);
            else
                item.Valor = value.ToString();

            item.Min = min;
            item.Max = max;
            item.IdUnidad = unidad;

            NotifyChanges(item);
        }

        public virtual void Set(string key, DateTime value, ParamUnidad unidad)
        {
            var item = Get(key);
            if (item == null)
                item = Add(key, value.ToString("dd/MM/yyyy HH:mm:ss"), unidad);
            else
                item.Valor = value.ToString("dd/MM/yyyy HH:mm:ss");

            item.IdUnidad = unidad;

            NotifyChanges(item);
        }

        public virtual void Set(AdjustValueDef key)
        {
            var item = Get(key.Name);

            if (item == null)
                item = Add(key.Name, key.Value.ToString(), key.Unidad);
            else
                item.Valor = key.Value.ToString();

            item.Min = key.Min;
            item.Max = key.Max;
            item.IdUnidad = key.Unidad;

            NotifyChanges(item);
        }

        public virtual void Set(AdjustValueDef key, bool withError, bool withVariance)
        {
            Set(key);

            if (withError)
                SetPatronError(key);

            if (withVariance)
                SetVariance(key);
        }

        public virtual void Set(TriAdjustValueDef key, bool withError = true, bool withVariance = false)
        {
            Set(key.L1);

            if (withError)
                SetPatronError(key.L1);

            if (withVariance)
                SetVariance(key.L1);

            Set(key.L2);

            if (withError)
                SetPatronError(key.L2);

            if (withVariance)
                SetVariance(key.L2);

            Set(key.L3);

            if (withError)
                SetPatronError(key.L3);

            if (withVariance)
                SetVariance(key.L3);

            if (key.Neutro != null)
            {
                Set(key.Neutro);

                if (withError)
                    SetPatronError(key.Neutro);

                if (withVariance)
                    SetVariance(key.Neutro);
            }

            if (key.NeutroCalc != null)
            {
                Set(key.NeutroCalc);

                if (withError)
                    SetPatronError(key.NeutroCalc);

                if (withVariance)
                    SetVariance(key.NeutroCalc);
            }

            if (key.Fugas != null)
            {
                Set(key.Fugas);

                if (withError)
                    SetPatronError(key.Fugas);

                if (withVariance)
                    SetVariance(key.Fugas);
            }
        }

        //********************************************************

        private void SetPatronError(AdjustValueDef key)
        {
            var Patron = new AdjustValueDef();
            Patron.Name = key.Name + "_PATRON";
            Patron.Value = key.Average;
            Patron.Unidad = key.Unidad;
            Patron.Max = key.Max;
            Patron.Min = key.Min;
            Set(Patron);

            var Error = new AdjustValueDef();
            Error.Name = key.Name + "_ERROR";
            Error.Value = key.Error;
            Error.Unidad = ParamUnidad.PorCentage;       
            Error.Min = key.Tolerance * -1;
            Error.Max = key.Tolerance;
            Set(Error);
        }

        private void SetVariance(AdjustValueDef key)
        {
            var variance =  new AdjustValueDef();
            variance.Name = key.Name + "_VARIANZA";
            variance.Value = key.Varianza;
            variance.Max = key.MaxVarianza;
            variance.Min = 0;
            variance.Unidad = ParamUnidad.SinUnidad;
            Set(variance);
        }

        private ParamValue Add(string key, string value, ParamUnidad unidad, double? min = null, double? max = null, string expectedValue = null)
        {
            //_logger.DebugFormat("Añadimos parametros al ParamValueCollection key:{0} value:{1}", key, value);
            var item = new ParamValue { Name = key, Valor = value, IdUnidad = unidad, Unidad = unidad.ToString(), Min = min, Max = max, ValorEsperado = expectedValue };
            this.Add(item);

            return item;
        }

        private ParamValue Get(string key)
        {
            ParamValue pValue = this.Find(p => p.Name == key);
            return pValue;
        }

        private void NotifyChanges(ParamValue item)
        {
            item.Modified = DateTime.Now;

            if (item.IdTipoGrupo == TipoGrupo.RESULTADOS)
            {
                if (item.Min.HasValue || item.Max.HasValue)
                    _logger.InfoFormat("{0}: {1} = {2} ({3}) Min: {4} Max: {5}", item.Grupo, item.Name, item.Valor, item.Unidad, item.Min, item.Max);
                else
                    _logger.InfoFormat("{0}: {1} = {2} ({3})", item.Grupo, item.Name, item.Valor, item.Unidad);
            }
            else
                _logger.InfoFormat("{0}: {1} = {2}", item.Grupo, item.Name, item.Valor);

            var context = SequenceContext.Current;
            if (context != null)
            {
                if (context.Step != null)
                    item.StepName = context.Step.Name;

                item.NumInstance = context.NumInstance;

                context.ResultList.Add(item.Name, item);
            }

            if (ItemChanged != null)
                ItemChanged(this, item);

            DataPointHub.TryAdd(this, item.Name, item.Valor, "Results");
        }
    }
}
