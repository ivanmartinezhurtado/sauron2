﻿using Dezac.Core.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(WriteSerialPortActionDescription))]
    public class WriteSerialPortAction : ActionBase
    {
        [Description("Texto a enviar")]
        [Category("3. Configuration")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToSend { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(500)]
        [Description("Tiempo de espera antes de realizar otra comunicación")]
        public int TimeInterval { get; set; }

        [Category("3.1 Time")]
        [DefaultValue(0)]
        [Description("Tiempo de espera antes de realizar la primera comunicación")]
        public int InitialDelay { get; set; }

        [Category("3.2 Retries")]
        [DefaultValue(3)]
        [Description("Número de respeticiones")]
        public int Retry { get; set; }

        [Category("3.3 Exception")]
        [DefaultValue(false)]
        [Description("Si se habilita la cancelación o si se encuenrta una excepción")]
        public bool CancelOnException { get; set; }

        [Category("3.3 Exception")]
        [DefaultValue(false)]
        [Description("Si se habilita la propagación de la excepción")]
        public bool ThrowException { get; set; }

        [Category("3. Configuration")]
        [DefaultValue(false)]
        [Description("Si se quiere enviar las tramas por cada linea o toda de una vez")]
        public bool IsSingleLine { get; set; }

        [Category("3. Configuration")]
        [DefaultValue(";")]
        [Description("Carácter con el que unir todas las tramas en una sola")]
        public string UnionSingleLine { get; set; }



        public override void Execute()
        {
            var sender = GetVariable(DeviceName) as SerialPort;

            if (sender == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el serial port").Throw();

            string[] stringSeparators = new string[] { "\r\n" };

            string[] lines = TextToSend.Split(stringSeparators, StringSplitOptions.None);

            var sampler = Sampler.Run(
               () => { return new SamplerConfig { Interval = TimeInterval, InitialDelay = InitialDelay, NumIterations = Retry, CancelOnException = CancelOnException, ThrowException = ThrowException }; },
               (step) =>
               {
                   if (!IsSingleLine)
                       foreach (string trama in lines)
                       {
                           var tramaTx = VariablesTools.ResolveText(trama);
                           sender.WriteLine(tramaTx);
                           Logger.InfoFormat("{0} {2} TX: {1}", this.GetType().Name, sender.PortName, tramaTx);
                           Context.ResultList.Add(string.Format("{0} - TX: {1}", DeviceName, tramaTx));
                       }
                   else
                   {
                       string tramaTx = "";
                       foreach (string trama in lines)
                           tramaTx += VariablesTools.ResolveText(trama) + UnionSingleLine;
                       sender.WriteLine(tramaTx);
                       Logger.InfoFormat("{0} {2} TX: {1}", this.GetType().Name, sender.PortName, tramaTx);
                       Context.ResultList.Add(string.Format("{0} - TX: {1}", DeviceName, tramaTx));
                   }

                   if (Retry <= 1)
                       step.Cancel = true;
               });

            if (sampler.HasExceptions)
                Error().PROCESO.PARAMETROS_ERROR.ERROR_SUBPROCESO(sampler.Exceptions.Message).Throw();
        }
    }

    public class WriteSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "WriteASCIISerialPort"; } }
        public override string Description { get { return "Envía la cadena de texto especificada por el puerto serie mas el símbolo 'nueva linea'"; } }
        public override string Category { get { return "Tools/Comunications/ASCII"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
