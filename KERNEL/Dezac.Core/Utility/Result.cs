﻿using System;

namespace Dezac.Core.Utility
{

    public sealed class Result<T>
    {
        public T Value { get; private set; }
        public bool Error { get; private set;  }
        public Exception Exception { get; private set;  }

        public readonly static Result<bool> OK = new Result<bool>(true);

        public Result()
        {
        }

        public Result(T value)
        {
            this.Value = value;
        }

        public Result(Exception ex)
        {
            this.Error = true;
            this.Exception = ex;
        }

        public static implicit operator Result<T>(T value)
        {
            return new Result<T>(value);
        }

        public static bool operator true(Result<T> value)
        {
            return !value.Error;
        }

        public static bool operator false(Result<T> value)
        {
            return value.Error;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public string ErrorMessage
        {
            get { return this.Error && this.Exception != null ? this.Exception.Message : null; }
        }
    }
}
