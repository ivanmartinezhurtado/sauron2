﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

namespace Dezac.Core.Utility
{
    public static class PrinterTools
    {
        public static string SelectPrinterByPatternOrDefault(string pattern = null)
        {
            PrinterSettings printer = new PrinterSettings();
            string defaultPrinter = string.Empty;

            foreach (string item in PrinterSettings.InstalledPrinters)
            {
                printer.PrinterName = item;
                
                if (printer.IsValid)
                {
                    if (string.IsNullOrEmpty(defaultPrinter) && printer.IsDefaultPrinter)
                        defaultPrinter = item;

                    if (!string.IsNullOrEmpty(pattern))
                        if (item.ToUpper().Contains(pattern.ToUpper()))
                        {
                            defaultPrinter = item;
                            myPrinters.SetDefaultPrinter(defaultPrinter);
                            break;
                        }
                }
            }
            
            return defaultPrinter;
        }
    }

    public static class myPrinters
    {
        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultPrinter(string Name);
    }

}
