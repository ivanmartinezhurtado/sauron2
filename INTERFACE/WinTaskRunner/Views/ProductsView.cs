﻿using Dezac.Core.Model;
using Dezac.Data;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace WinTaskRunner.Views
{
    public partial class ProductsView : UserControl
    {
        public ProductsView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            incluirVersion = !ChechBoxVersionVisible;
            checkBoxVersion.Visible = ChechBoxVersionVisible;
            UpdateUI();
        }

        public void HidePanels()
        {
            panel1.Visible = false;
        }

        public int? FilterByNumFamilia { get; set; }

        public bool OnlyActive { get; set; }
        public bool ChechBoxVersionVisible { get; set; }
        private bool incluirVersion = true;

        private void UpdateUI()
        {
            using (var db = new DezacContext())
            {
                IQueryable<PRODUCTO> query = db.PRODUCTO.Include("ARTICULO").Include("T_FAMILIA")
                    .Where(p => p.T_FAMILIA != null && p.T_FAMILIA.PLATAFORMA == "TS2");

                if (FilterByNumFamilia.HasValue)
                    query = query.Where(p => p.NUMFAMILIA == FilterByNumFamilia);

                if (OnlyActive)
                    query = query.Where(p => p.ACTIVO == "S");

                if (!incluirVersion)
                    query = query.GroupBy(p => p.NUMPRODUCTO).Select(y => y.FirstOrDefault());

                grid.Columns.Where(c => c.HeaderText == "Versión").First().IsVisible = incluirVersion;

                var list = query.Select(s => new ProductsViewModel
                {
                    NUMPRODUCTO = s.NUMPRODUCTO,
                    VERSION = incluirVersion ? s.VERSION : 0,
                    FECHAVERSION = s.FECHAVERSION,
                    ACTIVO = s.ACTIVO,
                    NUMFAMILIA = s.T_FAMILIA != null ? s.T_FAMILIA.NUMFAMILIA : 0,
                    FAMILIA = s.T_FAMILIA != null ? s.T_FAMILIA.FAMILIA : null,
                    DESCRIPCION = s.ARTICULO.DESCRIPCION
                }).ToList();

                bsList.DataSource = list;
            }

            if (bsList.Count > 1 && !FilterByNumFamilia.HasValue)
                grid.MasterTemplate.CollapseAllGroups();
        }

        public ProductsViewModel Current
        {
            get
            {
                return bsList.Current as ProductsViewModel;
            }
        }

        private void cmdTSD2015_CheckStateChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void checkBoxVersion_ToggleStateChanged_1(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            incluirVersion = checkBoxVersion.Checked;
            UpdateUI();
        }
    }
}
