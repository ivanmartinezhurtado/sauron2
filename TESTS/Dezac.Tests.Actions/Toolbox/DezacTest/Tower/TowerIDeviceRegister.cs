﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(TowerIDeviceRegisterDescription))]
    [Browsable(false)]
    public class TowerIDeviceRegister : ActionBase
    {       
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE");

            if (tower.IsMTEPSInitialized)
                testBase.Resultado.Set("DEVICE_MTE_PS", tower.MTEPS.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsMTEWATInitialized)
                testBase.Resultado.Set("DEVICE_MTE_WAT", tower.MTEWAT.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsChromaInitialized)
                testBase.Resultado.Set("DEVICE_CHROMA", tower.Chroma.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsLambdaInitialized)
                testBase.Resultado.Set("DEVICE_LAMBDA", tower.LAMBDA.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsPTEInitialized)
                testBase.Resultado.Set("DEVICE_PTE", "PTE-50-CEX SMC", ParamUnidad.SinUnidad);

            if (tower.IsHP34401AInitialized)
                testBase.Resultado.Set("DEVICE_HP34401", tower.HP34401A.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsHP53131AInitialized)
                testBase.Resultado.Set("DEVICE_HP53131A", tower.HP53131A.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsSignalGeneratorInitialized)
                testBase.Resultado.Set("DEVICE_SIGNAL_GENERATOR", tower.SignalGenerator.GetIdentification(), ParamUnidad.SinUnidad);

            if (tower.IsFLUKEInitialized)
                testBase.Resultado.Set("DEVICE_FLUKE", tower.FLUKE.GetIdentification(), ParamUnidad.SinUnidad);
        }
    }

    public class TowerIDeviceRegisterDescription : ActionDescription
    {
        public override string Name { get { return "TowerIDeviceRegister"; } }
        public override string Description { get { return "Action to register used tower device in test"; } }
        public override string Category { get { return "DezacTest/Tower"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }
}
