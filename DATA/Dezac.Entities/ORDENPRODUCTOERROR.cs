namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ORDENPRODUCTOERROR
    {
        public int NROERROR { get; set; }
        public int NUMFABRICACION { get; set; }
        public Nullable<int> NUMCAUSA { get; set; }
        public int NUMERROR { get; set; }
        public string IDOPERARIO { get; set; }
        public System.DateTime FECHA { get; set; }
        public string OBSERVACIONES { get; set; }
    
        public virtual OPE_ERROR OPE_ERROR { get; set; }
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual ORDENPRODUCTO ORDENPRODUCTO { get; set; }
    }
}
