using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

using System.Drawing.Imaging;

namespace Dezac.Tests.Visio.Forms
{
    public partial class CameraWithControls : Form
    {
        private Capture _capture = null; 
        private bool _captureInProgress = false; 
        private int CameraDevice = 0; 
        private List<VideoDevice> webCams;

        private int Brightness_Store = 0;
        private int Contrast_Store = 0;
        private int Sharpness_Store = 0;
        private int widthStore = 640;
        private int heightStore = 480;

        public CameraWithControls()
        {
            InitializeComponent();

            Slider_Enable(false);

            webCams = CameraUtils.GetDetectedCameras();

            foreach(var c in webCams)
                Camera_Selection.Items.Add(c.ToString());

            if (webCams.Any())
            {
                Camera_Selection.SelectedIndex = 0;
                captureButton.Enabled = true; 
            }
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (!this.IsDisposed && RetrieveBgrFrame.Checked)
            {
                Image<Bgr, Byte> frame = _capture.RetrieveBgrFrame();
                if (frame != null)
                    DisplayImage(frame.ToBitmap());
            }
            else if (RetrieveGrayFrame.Checked)
            {
                Image<Gray, Byte> frame = _capture.RetrieveGrayFrame();
                if (frame != null)
                    DisplayImage(frame.ToBitmap());
            }
        }

        private delegate void DisplayImageDelegate(Bitmap Image);
        private void DisplayImage(Bitmap Image)
        {
            if (captureBox.InvokeRequired)
            {
                try
                {
                    DisplayImageDelegate DI = new DisplayImageDelegate(DisplayImage);
                    this.BeginInvoke(DI, new object[] { Image });
                }
                catch (Exception)
                {
                }
            }
            else
            {
                captureBox.Image = Image;
            }
        }

        private void captureButtonClick(object sender, EventArgs e)
        {
            if (_capture != null)
            {
                if (_captureInProgress)
                {  
                    //stop the capture
                    captureButton.Text = "Start Capture"; //Change text on button
                    Slider_Enable(false); 
                    _capture.Pause(); //Pause the capture
                    _captureInProgress = false; //Flag the state of the camera
                }
                else
                {
                    //Check to see if the selected device has changed
                    if (Camera_Selection.SelectedIndex != CameraDevice)
                    {
                        SetupCapture(Camera_Selection.SelectedIndex); //Setup capture with the new device
                    }

                    RetrieveCaptureInformation(); //Get Camera information
                    captureButton.Text = "Stop"; //Change text on button
                    StoreCameraSettings(); //Save Camera Settings
                    Slider_Enable(true);  //Enable User Controls
                    _capture.Start(); //Start the capture
                    _captureInProgress = true; //Flag the state of the camera
                }
                
            }
            else 
            {
                //set up capture with selected device
                SetupCapture(Camera_Selection.SelectedIndex);
                //Be lazy and Recall this method to start camera
                captureButtonClick(null, null);
            }
        }

        private void SetupCapture(int Camera_Identifier)
        {
            CameraDevice = Camera_Identifier;

            if (_capture != null) _capture.Dispose();
            try
            {
                _capture = new Capture(CameraDevice);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, sliderWidth.Value);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, sliderHeight.Value);

                _capture.ImageGrabbed += ProcessFrame;
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

        private void FlipHorizontalButtonClick(object sender, EventArgs e)
        {
            if (_capture != null) _capture.FlipHorizontal = !_capture.FlipHorizontal;
        }

        private void FlipVerticalButtonClick(object sender, EventArgs e)
        {
            if (_capture != null) _capture.FlipVertical = !_capture.FlipVertical;
        }

        private void RetrieveCaptureInformation()
        {
            richTextBox1.Clear();
            richTextBox1.AppendText("Camera: " + webCams[CameraDevice].DeviceName + " (-1 = Unknown)\n\n");

            try
            {
                richTextBox1.AppendText("Brightness: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_BRIGHTNESS).ToString() + "\n"); //get the value and add it to richtextbox
                Brigtness_SLD.Value = (int)_capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_BRIGHTNESS);  //Set the slider value
            } catch
            { }
            Brigthness_LBL.Text = Brigtness_SLD.Value.ToString(); //set the slider text

            richTextBox1.AppendText("Contrast: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_CONTRAST).ToString() + "\n");//get the value and add it to richtextbox
            Contrast_SLD.Value = (int)_capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_CONTRAST);  //Set the slider value
            Contrast_LBL.Text = Contrast_SLD.Value.ToString(); //set the slider text

            richTextBox1.AppendText("Sharpness: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SHARPNESS).ToString() + "\n");
            Sharpness_SLD.Value = (int)_capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SHARPNESS);  //Set the slider value
            Sharpness_LBL.Text = Sharpness_SLD.Value.ToString(); //set the slider text

            richTextBox1.AppendText("Frame Width: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH).ToString() + "\n");
            sliderWidth.Value = (int)_capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH);  //Set the slider value
            labelWidth.Text = sliderWidth.Value.ToString(); //set the slider text

            richTextBox1.AppendText("Frame Height: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT).ToString() + "\n");
            sliderHeight.Value = (int)_capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT);  //Set the slider value
            labelHeight.Text = sliderHeight.Value.ToString(); //set the slider text

            txtExposure.Text = _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_EXPOSURE).ToString();
            txtExposureCamera.Text = _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_AUTO_EXPOSURE).ToString();
            txtGain.Text = _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_GAIN).ToString();

            //TODO: ALL These need sliders setting up on main form
            richTextBox1.AppendText("Convert RGB : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_CONVERT_RGB).ToString() + "\n");
            richTextBox1.AppendText("Exposure control done by camera: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_AUTO_EXPOSURE).ToString() + "\n");
            richTextBox1.AppendText("Exposure: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_EXPOSURE).ToString() + "\n");
            richTextBox1.AppendText("Gain: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_GAIN).ToString() + "\n");
            richTextBox1.AppendText("Gamma: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_GAMMA).ToString() + "\n");
            richTextBox1.AppendText("Hue: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_HUE).ToString() + "\n");
            richTextBox1.AppendText("Saturation: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SATURATION).ToString() + "\n");
            richTextBox1.AppendText("Sharpness: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SHARPNESS).ToString() + "\n");
            richTextBox1.AppendText("Trigger: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_TRIGGER).ToString() + "\n");
            richTextBox1.AppendText("Trigger Delay: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_TRIGGER_DELAY).ToString() + "\n");
            richTextBox1.AppendText("White balance blue u : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_WHITE_BALANCE_BLUE_U).ToString() + "\n");
            richTextBox1.AppendText("White balance red v : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_WHITE_BALANCE_RED_V).ToString() + "\n");
            richTextBox1.AppendText("Max DC1394: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_MAX_DC1394).ToString() + "\n");
            richTextBox1.AppendText("Current Capture Mode: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_MODE).ToString() + "\n");
            richTextBox1.AppendText("Monocrome : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_MONOCROME).ToString() + "\n");
            richTextBox1.AppendText("Rectification : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_RECTIFICATION).ToString() + "\n");
            richTextBox1.AppendText("Preview (tricky property, returns cpnst char* indeed ): " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SUPPORTED_PREVIEW_SIZES_STRING).ToString() + "\n");

            #region Unused
            /*
            //OpenNI
            richTextBox1.AppendText("\nOpen NI specific devices: \n");
            richTextBox1.AppendText("OpenNI map generators : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_OPENNI_DEPTH_GENERATOR).ToString() + "\n");
            richTextBox1.AppendText("Depth generator baseline, in mm: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_OPENNI_DEPTH_GENERATOR_BASELINE).ToString() + "\n");
            richTextBox1.AppendText("Depth generator focal length, in pixels: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_OPENNI_DEPTH_GENERATOR_FOCAL_LENGTH).ToString() + "\n");
            richTextBox1.AppendText("OpenNI map generators: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_OPENNI_GENERATORS_MASK).ToString() + "\n");
            richTextBox1.AppendText("CV_CAP_OPENNI_IMAGE_GENERATOR: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_OPENNI_IMAGE_GENERATOR).ToString() + "\n");
            richTextBox1.AppendText("Image generator output mode : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE).ToString() + "\n");
            richTextBox1.AppendText("OpenNI Baseline mm: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_OPENNI_BASELINE).ToString() + "\n");
            richTextBox1.AppendText("OpenNI Focal Length, pixels: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_OPENNI_FOCAL_LENGTH).ToString() + "\n");
            richTextBox1.AppendText("OpenNI Max Depth mm: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_OPENNI_FRAME_MAX_DEPTH).ToString() + "\n");
            richTextBox1.AppendText("OpenNI Oputput Mode: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_OPENNI_OUTPUT_MODE).ToString() + "\n");
             
            //Android
            richTextBox1.AppendText("\nAndroid Only: \n");
            richTextBox1.AppendText("property for highgui class: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_AUTOGRAB).ToString() + "\n");
             
            //Video File
            richTextBox1.AppendText("\nVideo Files: \n");
            richTextBox1.AppendText("Format: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FORMAT).ToString() + "\n");
            richTextBox1.AppendText("4-character code of codec : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FOURCC).ToString() + "\n");
            richTextBox1.AppendText("Frame rate : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FPS).ToString() + "\n");
            richTextBox1.AppendText("Number of frames in video file : " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_COUNT).ToString() + "\n");
            richTextBox1.AppendText(" Relative position of the video file (0 - start of the film, 1 - end of the film): " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_AVI_RATIO).ToString() + "\n");
            richTextBox1.AppendText("0-based index of the frame to be decoded/captured next: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES).ToString() + "\n");
            richTextBox1.AppendText("Film current position in milliseconds or video capture timestamp: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_MSEC).ToString() + "\n");

            //GStreamer Device
            richTextBox1.AppendText("\nGStreamer: \n");
            richTextBox1.AppendText("Properties of cameras available through GStreamer interface: " + _capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_GSTREAMER_QUEUE_LENGTH).ToString() + "\n");*/
            #endregion

        }

        private void StoreCameraSettings()
        {
            Brightness_Store = Brigtness_SLD.Value;
            Contrast_Store = Contrast_SLD.Value;
            Sharpness_Store = Sharpness_SLD.Value;
            widthStore = sliderWidth.Value;
            heightStore = sliderHeight.Value;
        }

        private void Refresh_BTN_Click(object sender, EventArgs e)
        {
            if (_capture != null)
            {
                RetrieveCaptureInformation();
            }
        }

        private void Reset_Cam_Settings_Click(object sender, EventArgs e)
        {
            if (_capture != null)
            {
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_BRIGHTNESS, Brightness_Store);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_CONTRAST, Contrast_Store);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SHARPNESS, Sharpness_Store);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, widthStore);
                _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, heightStore);
                RetrieveCaptureInformation();
            }
        }

        private void Brigtness_SLD_Scroll(object sender, EventArgs e)
        {
            Brigthness_LBL.Text = Brigtness_SLD.Value.ToString();
            if (_capture != null) _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_BRIGHTNESS, Brigtness_SLD.Value);
        }

        private void Contrast_SLD_Scroll(object sender, EventArgs e)
        {
            Contrast_LBL.Text = Contrast_SLD.Value.ToString();
            if (_capture != null) _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_CONTRAST, Contrast_SLD.Value);
        }

        private void Sharpness_SLD_Scroll(object sender, EventArgs e)
        {
            Sharpness_LBL.Text = Sharpness_SLD.Value.ToString();
            if (_capture != null) _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_SHARPNESS, Sharpness_SLD.Value);
        }

        private void sliderWidth_Scroll(object sender, EventArgs e)
        {
            labelWidth.Text = sliderWidth.Value.ToString();
            if (_capture != null) _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, sliderWidth.Value);
        }

        private void sliderHeight_Scroll(object sender, EventArgs e)
        {
            labelHeight.Text = sliderHeight.Value.ToString();
            if (_capture != null) _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, sliderHeight.Value);
        }

        private void txtExposure_TextChanged(object sender, EventArgs e)
        {
            double value;

            if (double.TryParse(txtExposure.Text, out value))
                if (_capture != null)
                    _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_EXPOSURE, value);
        }

        private void txtExposureCamera_TextChanged(object sender, EventArgs e)
        {
            double value;

            if (double.TryParse(txtExposure.Text, out value))
                if (_capture != null) 
                    _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_AUTO_EXPOSURE, value);
        }

        private void txtGain_TextChanged(object sender, EventArgs e)
        {
            double value;

            if (double.TryParse(txtExposure.Text, out value))
                if (_capture != null)
                    _capture.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_GAIN, value);
        }

        private void Slider_Enable(bool State)
        {
            Brigtness_SLD.Enabled = State;
            Contrast_SLD.Enabled = State;
            Sharpness_SLD.Enabled = State;
            sliderWidth.Enabled = State;
            sliderHeight.Enabled = State;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (_capture != null)
            {
                Reset_Cam_Settings_Click(null, null);
                _capture.Dispose();
            }

        }

        #region Ensures that bothe checkboxs can't be checked
        private void RetrieveGrayFrame_CheckedChanged(object sender, EventArgs e)
        {
            if (RetrieveGrayFrame.Checked)
            {
                RetrieveBgrFrame.Checked = !RetrieveGrayFrame.Checked;
            }
        }

        private void RetrieveBgrFrame_CheckedChanged(object sender, EventArgs e)
        {
            if (RetrieveBgrFrame.Checked)
            {
                RetrieveGrayFrame.Checked = !RetrieveBgrFrame.Checked;
            }
        }
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            var img = captureBox.Image;
            if (img == null)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Guardar imagen patr�n";
                sfd.DefaultExt = ".png";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    captureBox.Image.Save(sfd.FileName, ImageFormat.Png);
                }
            }
        }

        private void btScanBarCode_Click(object sender, EventArgs e)
        {
            Bitmap bmp = captureBox.Image as Bitmap;

            if (bmp != null)
                lblBarCode.Text = Barcode.Read(bmp);
        }

    }
}

