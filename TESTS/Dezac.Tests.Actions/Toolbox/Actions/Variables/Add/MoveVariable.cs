﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions.Toolbox.Actions.Variables.Add
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(MoveVariableDescription))]
    public class MoveVariable : ActionBase
    {
        [Description("Origen de la variable a mover")]
        [Category("3. Configuration")]
        [DefaultValue(VariablesType.Data)]
        public VariablesType Origin { get; set; } 

        [Description("Destino de la variable a mover")]
        [Category("3. Configuration")]
        [DefaultValue(VariablesType.SharedData)]
        public VariablesType Target { get; set; }

        [Description("Nombre de la variable a mover")]
        [Category("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();

            var value = Origin == VariablesType.Data ? GetVariable(DataInput.Key.Replace("$", "")) :
                Origin == VariablesType.SharedData ? GetSharedVariable(DataInput.Key.Replace("$", "")) :
                Origin == VariablesType.Cache ? cacheSvc.Get<object>(DataInput.Key.Replace("$", "")) : GetVariable(DataInput.Key.Replace("$", ""));

            if (Target == VariablesType.SharedData)
                SetSharedVariable(DataInput.Key.Replace("$", ""), value);
            else if (Target == VariablesType.Data)
                SetVariable(DataInput.Key.Replace("$", ""), value);
            else
                cacheSvc.AddOrSet(DataInput.Key.Replace("$", ""), value);
        }
    }

    public class MoveVariableDescription : ActionDescription
    {
        public override string Description { get { return "Action para mover una variable de variables a variables compartidas o viceversa"; } }
        public override string Name { get { return "MoveVariable"; } }
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }

    public enum VariablesType
    {
        Data,
        SharedData,
        Cache
    }
}
