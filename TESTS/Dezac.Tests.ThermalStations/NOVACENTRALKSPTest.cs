﻿using Dezac.Core.Utility;
using Dezac.Device.ThermalStations;
using Dezac.Services;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.ThermalStations
{
    [TestVersion(1.07)]
    public class NOVACENTRALKSPTest : TestBase
    {
        #region INPUTS
        //private const byte STATE_RELAY1 = 15;
        //private const byte STATE_RELAY2 = 14;
        //private const byte STATE_RELAY3 = 13;
        //private const byte STATE_RELAY4 = 12;
        //private const byte STATE_RELAY5 = 11;
        //private const byte STATE_RELAY6 = 10;
        //private const byte STATE_RELAY7 = 9;

        private const byte STATE_RELAY1 = 9;
        private const byte STATE_RELAY2 = 10;
        private const byte STATE_RELAY3 = 11;
        private const byte STATE_RELAY4 = 12;
        private const byte STATE_RELAY5 = 13;
        private const byte STATE_RELAY6 = 14;
        private const byte STATE_RELAY7 = 15;

        private const byte BLOCK_SECURITTY = 23;
        private const byte IN_ANCLA1 = 24;
        private const byte IN_TAPA = 25;
        private const byte IN_ANCLA2 = 26;
        private const byte IN_ANTENA = 27;

        private const byte DETECCION_BORNES_J1 = 30;
        private const byte DETECCION_BORNES_J3 = 31;
        private const byte DETECCION_BORNES_J4 = 32;
        private const byte DETECCION_BORNES_J2 = 33;
        private const byte DETECCION_BORNES_J5 = 34;
        private const byte STATE_RECORDING = 35;
        #endregion

        #region OUTPUTS
        private const byte SWITCH_PROBES = 0; //OFF = SONDA1, ON = SONDA2
        private const byte SELECT_R25C = 1;
        private const byte SELECT_R0C = 2;
        private const byte COM_CHANNEL1 = 3; //ON = CH1, OFF = CH2
        private const byte _24V = 14;
        private const byte SELECTION_MEASURE_MUX = 17;
        private const byte SELECTION_MEASURE_MUX2 = 18;
        private const byte ACTIVATE_LOAD = 19;
        private const byte ALIM_CRONO_433Mhz = 20;
        private const byte ALIM_CRONO_434Mhz = 21;
        private const byte ACTIVATE_IN1 = 42;
        private const byte ACTIVATE_IN2 = 41;
        private const byte ACTIVATE_LIGHT = 51;
        private const byte DISCONNECT_RECORDING_DATAMAN = 52;

        private const byte EV_GENERAL = 4;
        private const byte J1_J3_J4 = 5;
        private const byte J2_J5 = 6;
        private const byte ANTENA = 7;
        private const byte GRABACION = 8;
        private const byte PISTON_DIPS = 9;
        private const byte FIJACION_CARRO = 10;
        #endregion
            
        private NOVACENTRALKSP ksp;       
        private Tower3 tower;
        private string CAMERA_IDS;
        private NOVACENTRALKSP.TemperaturePatterns patternsTemperature;

        public void TestInitialization(byte comPort = 0)
        {
            
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (comPort == 0)
                comPort = Comunicaciones.SerialPort;

            ksp = AddInstanceVar(new NOVACENTRALKSP(comPort), "UUT");

            CAMERA_IDS = GetVariable<string>("CAMERA_IDS", CAMERA_IDS);

            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveElectroValvule();
            tower.IO.DO.On(_24V);
            tower.IO.DO.On(COM_CHANNEL1);

            tower.IO.DO.On(FIJACION_CARRO);
            Delay(1000, "Fijando el equipo al util");
            tower.IO.DO.On(J1_J3_J4, J2_J5);
            tower.IO.DO.On(ANTENA);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[BLOCK_SECURITTY];
            }, "Error No se ha detectado el bloque de seguridad", 5, 500, 500, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[IN_ANCLA1];
            }, "Error No se ha detectado el ancla 1 del equipo", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[IN_ANCLA2];
            }, "Error No se ha detectado el ancla 2 del equipo", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[IN_ANTENA];
            }, "Error No se ha detectado la conexión de la antena", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECCION_BORNES_J1];
            }, "Error No se ha detectado la conexión de la bornera 1", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECCION_BORNES_J2];
            }, "Error No se ha detectado la conexión de la bornera 2", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECCION_BORNES_J3];
            }, "Error No se ha detectado la conexión de la bornera 3", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECCION_BORNES_J4];
            }, "Error No se ha detectado la conexión de la bornera 4", 5, 500, 50, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECCION_BORNES_J5];
            }, "Error No se ha detectado la conexión de la bornera 5", 5, 500, 50, false, false);

            
        }

        public void TestComunications()
        {
            SamplerWithCancel((p) =>
                {
                    ksp.FlagTest();
                    return true;
                }, "Error de comunicaciones por el canal 1 RS485", 5, 500, 200, false, false);
            Resultado.Set("COMUNICATIONS_CHANNEL_1", "OK", ParamUnidad.SinUnidad);
            ksp.AuthorizationStartTest();

            ksp.WriteIdentificationNumber(1234);
            ksp.SaveToFlash();

            tower.IO.DO.Off(COM_CHANNEL1);
            SamplerWithCancel((p) =>
                {
                    ksp.FlagTest();
                    return true;
                }, "Error de comunicaciones por el canal 2 RS485", 5, 500, 2500, false, false);
            Resultado.Set("COMUNICATIONS_CHANNEL_2", "OK", ParamUnidad.SinUnidad);
            tower.IO.DO.On(COM_CHANNEL1);

            TestInfo.FirmwareVersion = ksp.ReadFirmwareVersion();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, TestInfo.FirmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version de firmware no es la correcta"), ParamUnidad.SinUnidad);
         
        }

        public void TestVoltageTP()
        {
            tower.IO.DO.Off(SELECTION_MEASURE_MUX);

            
            var param5V = new AdjustValueDef(Params.V_DC.Null.TestPoint("5V "), ParamUnidad.V);
            TestMeasureBase(param5V,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
            }, 1, 5, 2000);

            var param12V = new AdjustValueDef(Params.V_DC.Null.TestPoint("12V"), ParamUnidad.V);
            TestMeasureBase(param12V,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN3, MagnitudsMultimeter.VoltDC);
            }, 1, 5, 2000);           
        }

        public void TestDigitalInputs()
        {
            var digitalInputsValues = new Dictionary<NOVACENTRALKSP.DigitalInputs, byte>();
            digitalInputsValues.Add(NOVACENTRALKSP.DigitalInputs.DIGITAL_INPUT1, ACTIVATE_IN1);
            digitalInputsValues.Add(NOVACENTRALKSP.DigitalInputs.DIGITAL_INPUT2, ACTIVATE_IN2);

            foreach(KeyValuePair<NOVACENTRALKSP.DigitalInputs, byte> CurrentInput in digitalInputsValues)
            {
                var defs = new AdjustValueDef(Params.POINTS.Null.TestPoint(CurrentInput.Key.GetDescription() + "_HIGH").Name, 0, Params.POINTS.Null.TestPoint(CurrentInput.Key.GetDescription() + "_HIGH").Min(), Params.POINTS.Null.TestPoint(CurrentInput.Key.GetDescription() + "_HIGH").Max(), 0, 0, ParamUnidad.Puntos);

                tower.IO.DO.On(CurrentInput.Value);

                TestMeasureBase(defs, (step) =>
                {
                    var x = (double)ksp.ReadDigitalInputs(CurrentInput.Key);
                    return (double)ksp.ReadDigitalInputs(CurrentInput.Key);
                }, 0, 5, 500);

                tower.IO.DO.Off(CurrentInput.Value);

                var defs2 = new AdjustValueDef(Params.POINTS.Null.TestPoint(CurrentInput.Key.GetDescription() + "_LOW").Name, 0, Params.POINTS.Null.TestPoint(CurrentInput.Key.GetDescription() + "_LOW").Min(), Params.POINTS.Null.TestPoint(CurrentInput.Key.GetDescription() + "_LOW").Max(), 0, 0, ParamUnidad.Puntos);

                TestMeasureBase(defs2, (step) =>
                {
                    var x = (double)ksp.ReadDigitalInputs(CurrentInput.Key);
                    return (double)ksp.ReadDigitalInputs(CurrentInput.Key);
                }, 0, 5, 500);

                Resultado.Set(CurrentInput.Key.GetDescription(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestSwitches()
        {
            var IMAGE_PATH = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\KSP_ZITY\";
            var switchValues = new List<NOVACENTRALKSP.Switches>();
            switchValues.Add(NOVACENTRALKSP.Switches.ALL_SWITCHES_OFF);
            switchValues.Add(NOVACENTRALKSP.Switches.ALL_SWITCHES_ON);
            

            foreach (NOVACENTRALKSP.Switches CurrentSwitch in switchValues)
            {
                if (CurrentSwitch == NOVACENTRALKSP.Switches.ALL_SWITCHES_ON)
                {
                    DisconectDeviceFromUtil();
                    Shell.ShowDialog("Test SWITCH", () => new ImageView("Saque el equipo del útil, coloque los DIPS como en la imagen e introduzcalo de nuevo", IMAGE_PATH + "SWITCH_ON.jpg"), MessageBoxButtons.OK, "Coloque los DIPS como en la imagen");
                    ConectDeviceFromUtil();
                    //tower.IO.DO.On(PISTON_DIPS);
                }
                    

                SamplerWithCancel(
                    (p) =>
                    {
                        return ksp.ReadSwitch() == (ushort)CurrentSwitch;
                    }, string.Format("Error no se ha detectado el switch {0}", CurrentSwitch.ToString()), 5, 500, 500);

                tower.IO.DO.Off(PISTON_DIPS);

                Resultado.Set(CurrentSwitch.ToString(), "OK", ParamUnidad.SinUnidad);
            }         
        }

        public void TestGridsMotor()
        {
            //tower.IO.DO.On(ACTIVATE_LOAD);            

            ksp.SelectMoveGrid();

            var gridsValues = new Dictionary<NOVACENTRALKSP.Grids, InputMuxEnum>();
            {
                //gridsValues.Add(NOVACENTRALKSP.Grids.GRID1, InputMuxEnum.IN2_AuxMedida1);
                //gridsValues.Add(NOVACENTRALKSP.Grids.GRID2, InputMuxEnum.IN3_AuxMedida2);
                //gridsValues.Add(NOVACENTRALKSP.Grids.GRID3, InputMuxEnum.IN4_AuxMedida3);
                //gridsValues.Add(NOVACENTRALKSP.Grids.GRID4, InputMuxEnum.IN5_AuxMedida4);
                //gridsValues.Add(NOVACENTRALKSP.Grids.GRID5, InputMuxEnum.IN6_AuxMedida5);
                //gridsValues.Add(NOVACENTRALKSP.Grids.GRID6, InputMuxEnum.IN7_AuxMedida6);

                gridsValues.Add(NOVACENTRALKSP.Grids.GRID1, InputMuxEnum.IN7);
                gridsValues.Add(NOVACENTRALKSP.Grids.GRID2, InputMuxEnum.IN6);
                gridsValues.Add(NOVACENTRALKSP.Grids.GRID3, InputMuxEnum.IN5);
                gridsValues.Add(NOVACENTRALKSP.Grids.GRID4, InputMuxEnum.IN4);
                gridsValues.Add(NOVACENTRALKSP.Grids.GRID5, InputMuxEnum.IN3);
                gridsValues.Add(NOVACENTRALKSP.Grids.GRID6, InputMuxEnum.IN2);
            }

            tower.IO.DO.On(SELECTION_MEASURE_MUX);

            foreach (KeyValuePair<NOVACENTRALKSP.Grids, InputMuxEnum> CurrentGrid in gridsValues)
            {
                tower.IO.DO.Off(SELECTION_MEASURE_MUX2);

                ksp.OpenGrid(CurrentGrid.Key);
    
                var defs = new AdjustValueDef(Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_OPEN").Name, 0, Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_OPEN").Min(), Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_OPEN").Max(), 0, 0, ParamUnidad.V);

                TestMeasureBase(defs, (step) =>
                {
                    if (step % 3 == 0 && step > 0)
                        ksp.OpenGrid(CurrentGrid.Key);

                    return tower.MeasureMultimeter(CurrentGrid.Value, MagnitudsMultimeter.VoltDC);
                }, 0, 6, 500);

                //Reset();

                tower.IO.DO.On(SELECTION_MEASURE_MUX2);

                ksp.CloseGrid(CurrentGrid.Key);

                var defs2 = new AdjustValueDef(Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_CLOSE").Name, 0, Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_CLOSE").Min(), Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_CLOSE").Max(), 0, 0, ParamUnidad.V);

                TestMeasureBase(defs2, (step) =>
                {
                    if (step % 3 == 0 && step > 0)
                        ksp.CloseGrid(CurrentGrid.Key);

                    return tower.MeasureMultimeter(CurrentGrid.Value, MagnitudsMultimeter.VoltDC);
                }, 0, 6, 500);

                //Reset();

            }

            //tower.IO.DO.Off(ACTIVATE_LOAD);
        }

        public void TestLedsGrids()
        {
            ksp.DesactivateRelays();

            ksp.DesactivateMaskControlLedsGrids();

            Delay(1000, "Capturando Imagen");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "KSP_ALL_OFF", "ALL_OFF", "KSP");

            ksp.ActivateMaskControlLedsGrids();
            
            ksp.OnLedsAllGreen();
            Delay(1000, "Capturando Imagen");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "KSP_ALL_GREEN", "ALL_GREEN", "KSP");

            ksp.OnLedsAllRed();
            Delay(1000, "Capturando Imagen");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "KSP_ALL_RED", "ALL_RED", "KSP");

            ksp.DesactivateMaskControlLedsGrids();
        }

        public void TestRelaysAndLesdRelays()
        {
            var RelaysValues = new Dictionary<NOVACENTRALKSP.RelaysCombination, List<byte>>();
            RelaysValues.Add(NOVACENTRALKSP.RelaysCombination.RELAY_ODDS, new List<byte> { STATE_RELAY1, STATE_RELAY3, STATE_RELAY5, STATE_RELAY7 });
            RelaysValues.Add(NOVACENTRALKSP.RelaysCombination.RELAY_PAIRS, new List<byte> { STATE_RELAY2, STATE_RELAY4, STATE_RELAY6});

            foreach (KeyValuePair<NOVACENTRALKSP.RelaysCombination, List<byte>> CurrentRelay in RelaysValues)
            {
                ksp.DesactivateRelays();

                SamplerWithCancel(
                    (p) =>
                    {
                        ksp.ActivateRelayCombination(CurrentRelay.Key);
                        Delay(200, "Activando Relé Actual");

                        foreach(byte relaysStates in CurrentRelay.Value)
                            if (!tower.IO.DI[relaysStates])
                                return false;      
              
                        return true;
                    }, string.Format("Error, no se han detectado todos los {0} activados", CurrentRelay.Key.GetDescription()));

                this.TestHalconFindLedsProcedure(CAMERA_IDS, $"KSP_{CurrentRelay.Key}", CurrentRelay.Key.ToString(), "KSP");

                ksp.DesactivateRelays();
                Delay(150, "Desactivando Reles");
                
                Resultado.Set(CurrentRelay.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestRadioComunication(NOVACENTRALKSP.RadioFrequency crono)
        {
          
            ksp.ResetCounters();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3, 0.2);

            if (crono == NOVACENTRALKSP.RadioFrequency._433MHz)
                ksp.SelectFrecuency433MHz();

            var Out = crono == NOVACENTRALKSP.RadioFrequency._433MHz ? ALIM_CRONO_433Mhz : ALIM_CRONO_434Mhz;
            tower.IO.DO.On(Out);
            Delay(15000, "Espera CRONO");

            int numberFrames = ksp.ReadCounters()[0];

            Assert.AreGreater(numberFrames, 180, Error().UUT.COMUNICACIONES.MARGENES_INFERIORES("Error, el numero de tramas recibido es inferior al esperado"));

            Resultado.Set(string.Format("COMUNICATIONS{0}", crono.GetDescription()), "OK", ParamUnidad.SinUnidad);

            tower.IO.DO.Off(Out);
            tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void TestLedsComunications()
        {
            Delay(5000, "Esperando a recibir tramas");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "KSP_COM", "COM", "KSP", 3);
        }

        public void TestReadTemperature()
        {
            var Sondas = new List<NOVACENTRALKSP.Probes>();
            Sondas.Add(NOVACENTRALKSP.Probes.PROBE1_C);
            Sondas.Add(NOVACENTRALKSP.Probes.PROBE2_C);

            tower.IO.DO.On(SELECT_R25C);

            foreach (NOVACENTRALKSP.Probes sonda in Sondas)
            {
                var defs = new AdjustValueDef(Params.TEMP.Null.TestPoint(sonda.GetDescription()).Name, 0, Params.TEMP.Null.TestPoint(sonda.GetDescription()).Min(), Params.TEMP.Null.TestPoint(sonda.GetDescription()).Max(), 0, 0, ParamUnidad.V);

                if (sonda == NOVACENTRALKSP.Probes.PROBE2_C)
                    tower.IO.DO.On(SWITCH_PROBES);

                Delay(5000, "Esperando a que el equipo se estabilice en la medida de la temperatura");
                TestMeasureBase(defs, (step) =>
                {
                    var x = ksp.ReadProbeTemperature(sonda);
                    return x;
                }, 1, 6, 500);

                tower.IO.DO.Off(SWITCH_PROBES);               
            }
            tower.IO.DO.Off(SELECT_R25C);
        }

        public void TestCalibration(NOVACENTRALKSP.Probes Sonda, int delFirst = 4, int initCount = 4, int samples = 10, int interval = 500)
        {
            if (Sonda == NOVACENTRALKSP.Probes.PROBE2_POINTS)
                tower.IO.DO.On(SWITCH_PROBES);

            var defsPatron25 = new AdjustValueDef(Params.RES.Null.TestPoint("PATRON_25C").TestPoint(Sonda.GetDescription()).Name, 0, Params.TEMP.Null.TestPoint("PATRON_25C").Min(), Params.TEMP.Null.TestPoint("PATRON_25C").Max(), 0, 0, ParamUnidad.Puntos);

            var Rntc25 = (int) Configuracion.GetDouble(Params.RES.Null.Ajuste.TestPoint("25C").Name, 10000, ParamUnidad.Ohms);
            var Rntc0 = (int) Configuracion.GetDouble(Params.RES.Null.Ajuste.TestPoint("0C").Name, 33900, ParamUnidad.Ohms);

            tower.IO.DO.OnWait(1000, SELECT_R25C);

            TestMeasureBase(defsPatron25, (step) =>
            {
                var temperatureReading = ksp.ReadProbeTemperaturePoints(Sonda);
                return temperatureReading;
            }, 1, 6, 1000, true);
           
            tower.IO.DO.OffWait(200, SELECT_R25C);

            var defsPatron0 = new AdjustValueDef(Params.RES.Null.TestPoint("PATRON_0C").TestPoint(Sonda.GetDescription()).Name, 0, Params.RES.Null.TestPoint("PATRON_0C").Min(), Params.RES.Null.TestPoint("PATRON_0C").Max(), 0, 0, ParamUnidad.Puntos);

            tower.IO.DO.OnWait(1000, SELECT_R0C);

            TestMeasureBase(defsPatron0, (step) =>
            {
                var temperatureReading = ksp.ReadProbeTemperaturePoints(Sonda);
                return temperatureReading;
            }, 1, 6, 1000, true);

  
            tower.IO.DO.OffWait(200, SELECT_R0C);            

            tower.IO.DO.Off(SWITCH_PROBES);           
        }

        public void TestCustomization()
        {
            ConectDeviceFromUtil();

            SamplerWithCancel(
            (p) =>
            {
                ksp.FlagTest();
                return true;
            },"Error no se ha podido comunicar con el equipo después del reset", 5, 500, 500, false, false);
           
            ksp.AuthorizationStartTest();

            int? idnNumberBBDD;
            int? perifericBBDD;

            using (DezacService svc = new DezacService())
            {
                try
                {
                    idnNumberBBDD = svc.GetIdentifierNumberKSPZity();
                    perifericBBDD = svc.GetMasterNumberKSPZity();
                }
                catch(Exception)
                {
                    throw new Exception("Error al intentar recuperar el identificador de la Base de Datos");
                }
            }

            if (idnNumberBBDD == 20 || idnNumberBBDD == 21)
                idnNumberBBDD = 22;       
            
            ksp.WriteIdentificationNumber(idnNumberBBDD);
            ksp.WriteSerialNumber(Convert.ToInt32(TestInfo.NumSerie));                       

            var parameters = new NOVACENTRALKSP.Parameters();
            parameters.DT1Freecol = Convert.ToUInt16(Parametrizacion.GetDouble("DT1_FREE_COOLING", 10, ParamUnidad.SinUnidad));
            parameters.DT2Freecol = Convert.ToUInt16(Parametrizacion.GetDouble("DT2_FREE_COOLING", 10, ParamUnidad.SinUnidad));
            parameters.HisTmpCold = Convert.ToUInt16(Parametrizacion.GetDouble("HISTERESI_TEMP_COLD", 3, ParamUnidad.SinUnidad));
            parameters.HisTempHot = Convert.ToUInt16(Parametrizacion.GetDouble("HISTERESI_TEMP_HOT", 3, ParamUnidad.SinUnidad));
            parameters.TimeMoveDamper = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_MOVE_DAMPER", 5, ParamUnidad.SinUnidad));
            parameters.TempAntiHielo = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_ANTIHIELO", 70, ParamUnidad.SinUnidad));
            parameters.LimitWaterTemperatureHot = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_LIMIT_WATER_HOT", 300, ParamUnidad.SinUnidad));
            parameters.LimitWaterTemperatureCold = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_LIMIT_WATER_COLD", 0, ParamUnidad.SinUnidad));
            parameters.TimeOffCheckAntiHielo = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_OFF_CHECK_ANTIHIELO", 1800, ParamUnidad.SinUnidad));
            parameters.LimitTempCaldera = Convert.ToUInt16(Parametrizacion.GetDouble("LIMIT_TEMPERATURE_CALDERA", 850, ParamUnidad.SinUnidad));
            parameters.TimeActivateFreeCooling = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_ACTIVATE_FREE_COOLING", 2700, ParamUnidad.SinUnidad));
            parameters.ReturnTempAirMax = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_AIR_MAX", 400, ParamUnidad.SinUnidad));
            parameters.ReturnTempAirMin = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_AIR_MIN", 60, ParamUnidad.SinUnidad));
            parameters.TimeProtocolDO = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_PROTOCOL_DO", 240, ParamUnidad.SinUnidad));
            parameters.IntensisVelocity = Convert.ToUInt16(Parametrizacion.GetDouble("VELOCITY_INTENSIS", 4, ParamUnidad.SinUnidad));
            parameters.AlarmAntiHielo = Convert.ToUInt16(Parametrizacion.GetDouble("ALARM_ANTIHIELO", 1, ParamUnidad.SinUnidad));
            parameters.ConfigFreeCooling = Convert.ToUInt16(Parametrizacion.GetDouble("CONFIG_FREE_COOLING", 0, ParamUnidad.SinUnidad));
            parameters.TimerStartMotor = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_START_MOTORS", 30, ParamUnidad.SinUnidad));
            parameters.TimerStopMotor = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_STOP_MOTORS", 30, ParamUnidad.SinUnidad));
            parameters.ValueRestaFactorVelocidad = Convert.ToUInt16(Parametrizacion.GetDouble("RESTA_FACTOR_VELOCIDAD", 1, ParamUnidad.SinUnidad));
            parameters.ValueSuma1FactorVelocidad = Convert.ToUInt16(Parametrizacion.GetDouble("SUMA_1_FACTOR_VELOCIDAD", 1, ParamUnidad.SinUnidad));
            parameters.ValueSuma2FactorVelocidad = Convert.ToUInt16(Parametrizacion.GetDouble("SUMA_2_FACTOR_VELOCIDAD", 2, ParamUnidad.SinUnidad));
            parameters.ApplyTableCompatibility = Convert.ToUInt16(Parametrizacion.GetDouble("APPLY_TABLE_COMPATIBILITY", 0, ParamUnidad.SinUnidad));
            parameters.TimerOffFan = Convert.ToUInt16(Parametrizacion.GetDouble("TIMMER_OFF_FAN", 30, ParamUnidad.SinUnidad));
            parameters.TempProtectionRadiant = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_PROTECTION_RADIANT", 280, ParamUnidad.SinUnidad));
            parameters.TempProtectionRefresh = Convert.ToUInt16(Parametrizacion.GetDouble("TEMPERATURE_PROTECTION_REFRESH", 200, ParamUnidad.SinUnidad));
            parameters.HisTempColdUbac = Convert.ToUInt16(Parametrizacion.GetDouble("HISTERESI_TEMP_COLD_UBAC", 1, ParamUnidad.SinUnidad));
            parameters.HisTempHotUbac = Convert.ToUInt16(Parametrizacion.GetDouble("HISTERESI_TEMP_HOT_UBAC", 1, ParamUnidad.SinUnidad));

            ksp.WriteParameters(parameters);
            Delay(1500, "Escribiendo parametros");

            ksp.WritePerifericNumber(perifericBBDD);

            ksp.Modbus.PerifericNumber = (byte)perifericBBDD;

            ksp.SaveToFlash();
            Delay(4000, "Grabando Customizacion en Flash");

            tower.Chroma.ApplyOff();
            Delay(3000, "Reseteando UUT");
            tower.Chroma.ApplyAndWaitStabilisation();

            SamplerWithCancel((p) =>
            {
                ksp.FlagTest();
                return true;
            }, "Error No se ha podido comunicar con el equipo despues del Reset", 5, 500, 50, false, false);

            ksp.AuthorizationStartTest();

            TestInfo.NumSerieInterno = ksp.ReadIdentificationNumber().ToString();
            Assert.AreEqual("IDENTIFICATION_NUMBER", TestInfo.NumSerieInterno, idnNumberBBDD.ToString(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error en la grabación del número de identificacion, el número grabado no coincide con el esperado"), ParamUnidad.SinUnidad);

            Resultado.Set("PERIFERIC_NUMBER", perifericBBDD.ToString(), ParamUnidad.SinUnidad);
            TestInfo.PerifericNumber = perifericBBDD.ToString();

            var serialNumber = ksp.ReadSerialNumber();
            Assert.AreEqual("SERIAL_NUMBER", serialNumber.ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error en la grabación del número de serie, el número grabado no coincide con el esperado"), ParamUnidad.SinUnidad);

            var parametersReading = ksp.ReadParameters();
            Assert.AreEqual(parametersReading, parameters, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, parámetros leídos del equipo no coinciden con los grabados"));

            tower.Chroma.ApplyOffAndWaitStabilisation();
        }

        public void TestFinish()
        {
            if (ksp != null)
                ksp.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.IO.DO.Off(J1_J3_J4, J2_J5, ANTENA);
                tower.IO.DO.Off(FIJACION_CARRO, COM_CHANNEL1, _24V, SWITCH_PROBES, GRABACION);
                tower.IO.DO.Off(SELECT_R25C, SELECT_R0C, SELECTION_MEASURE_MUX, SELECTION_MEASURE_MUX2, ACTIVATE_LOAD);
                tower.Dispose();
            }
        }

        public void TestWriteParameters()
        {
            ksp = AddInstanceVar(new NOVACENTRALKSP(1), "UUT");

            SamplerWithCancel((p) =>
            {
                ksp.FlagTest();
                return true;
            }, "Error No se ha podido comunicar con el equipo despues del Reset", 5, 500, 50, false, false);

            ksp.AuthorizationStartTest();

            var idnNumberBBDD = 0;
            using (DezacService svc = new DezacService())
              {
                  try
                  {
                      idnNumberBBDD = Convert.ToInt16(svc.GetParamByDescription(147987, "120", "IDENTIFICATION_NUMBER")) + 1;
                  }
                  catch(Exception)
                  {
                      throw new Exception("Error al intentar recuperar el identificador de la Base de Datos");
                  }
              }

            idnNumberBBDD = Shell.ShowInputKeyboard("Numero de Identificacion", "Introduce el Numero de Identificacion", 0);

            Resultado.Set("IDENTIFICATION_NUMBER", idnNumberBBDD, ParamUnidad.SinUnidad);

            ksp.WriteIdentificationNumber(idnNumberBBDD);

            patternsTemperature = new NOVACENTRALKSP.TemperaturePatterns();
            patternsTemperature.Patern_85C = 85;
            patternsTemperature.Patern_40C = 40;
            patternsTemperature.Patern_30C = 30;
            patternsTemperature.Patern_18C = 18;
            patternsTemperature.Patern_10C = 10;
            patternsTemperature.Patern_6C = 6;
            patternsTemperature.Patern_5C = 5;

            ksp.WriteTemperaturePatterns(patternsTemperature);

            var temporizatorsBBDD = new NOVACENTRALKSP.Temporizators();
            temporizatorsBBDD.DT1_freecol = Convert.ToUInt16(Parametrizacion.GetDouble("DT1_FREE_COOLING", 1));
            temporizatorsBBDD.DT2_freecol = Convert.ToUInt16(Parametrizacion.GetDouble("DT2_FREE_COOLING", 1));
            temporizatorsBBDD.His_tmpfrio = Convert.ToUInt16(Parametrizacion.GetDouble("HISTERESI_TEMP_COLD", 1));
            temporizatorsBBDD.His_tmpcalo = Convert.ToUInt16(Parametrizacion.GetDouble("HISTERESI_TEMP_HOT", 1));
            temporizatorsBBDD.Tim_offanti = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_OFF_CHECK_ANTIFREEZE", 1800));
            temporizatorsBBDD.Tim_onfreecol = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_ACTIVATION_FREE_COOLING", 2700));
            temporizatorsBBDD.Tim_protocol = Convert.ToUInt16(Parametrizacion.GetDouble("TIME_PROTOCOL", 1200));

            ksp.WriteTemporizators(temporizatorsBBDD);

            ksp.SaveToFlash();
            Delay(4000, "Grabando Customizacion en Flash");
            
            this.ShowDialogMessage("Reset", "Resetear equipo", 150, 250);

            SamplerWithCancel((p) =>
            {
                ksp.FlagTest();
                return true;
            }, "Error No se ha podido comunicar con el equipo despues del Reset", 5, 500, 50, false, false);

            ksp.AuthorizationStartTest();

            var idnNumber = ksp.ReadIdentificationNumber();
            Assert.IsTrue(idnNumber == idnNumberBBDD, Error().UUT.CONFIGURACION.NO_COINCIDE("Error en la grabación del número de identificacion, el número grabado no coincide con el esperado"));            

            var temperaturePatternsReading = ksp.ReadTemperaturePatterns();

            Assert.AreEqual(temperaturePatternsReading, patternsTemperature, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, patrones de temperatura leídos del equipo no coinciden con los calculados"));

            var temporizators = ksp.ReadTemporizators();
            Assert.AreEqual(temporizators, temporizatorsBBDD, Error().UUT.CONFIGURACION.NO_COINCIDE("Error,temporizadores leídos del equipo no coinciden con los de base de datos"));
        }

        private void DisconectDeviceFromUtil()
        {
            tower.IO.DO.OffRange(J1_J3_J4, FIJACION_CARRO);
            Delay(250, "");
            tower.IO.DO.Off(EV_GENERAL);
        }

        private void ConectDeviceFromUtil()
        {
            tower.IO.DO.On(EV_GENERAL);
            Delay(250, "");

            tower.IO.DO.On(FIJACION_CARRO);
            Delay(1000, "Fijando el equipo al util");
            tower.IO.DO.On(J1_J3_J4, J2_J5);
            tower.IO.DO.On(ANTENA);
        }

        private void Reset()
        {
            tower.IO.DO.PulseOff(500, J2_J5);

            SamplerWithCancel(
                (p) =>
                {
                    ksp.FlagTest();
                    return true;
                }, string.Format("Error de comunicaciones"), 5, 500, 1000);

            ksp.AuthorizationStartTest();
        }
    }
}
