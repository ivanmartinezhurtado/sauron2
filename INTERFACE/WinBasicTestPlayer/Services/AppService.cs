﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using WinTestPlayer.Data;
using WinTestPlayer.Model;

namespace WinTestPlayer.Services
{
    public class AppService : IEnvironmentService
    {
        private readonly AssemblyResolverService assemblyResolver;
        private bool StartRunning = false;
        private readonly ShellService shell;
        private readonly ICacheService cacheSvc;

        public SequenceRunner Runner { get; private set; }

        public SequenceModel Sequence { get; private set; }

        public TestModel Model { get; private set; }

        public List<TestContextModel> TestContexts { get; set; }

        public Dictionary<string, object> Environment { get; set; }

        public SynchronizationContext SC { get; set; }

        public event EventHandler SequenceLoaded;
        public event EventHandler ModelChanged;
        public event EventHandler TestInfoChanged;
        public event EventHandler<string> OnEventFired;
        public event EventHandler CloseForm;

        public AppService(AssemblyResolverService assemblyResolver
            , RunnerService runnerService
            , ShellService shell
            , ICacheService cacheService)
        {
            this.assemblyResolver = assemblyResolver;
            this.shell = shell;
            this.cacheSvc = cacheService;

            TestContexts = new List<TestContextModel>();
            Environment = new Dictionary<string, object>();

            Runner = runnerService.Runner;
            Runner.AllTestEnded += (s, e) =>
            {
                StartRunning = false;
                TestContexts.ForEach(p => p.TestInfo.PropertyChanged -= OnTestInfoPropertyChanged);
            };

            Model = new TestModel();
            Model.SauronVersion = Application.ProductVersion;
            Model.DevicePC = Dns.GetHostName();
            Model.PropertyChanged += (s, e) => { OnModelChanged(); };
            Model.HasDatabase = ConfigurationManager.AppSettings["HasDatabase"] == "SI";
        }

        public void Login(string idEmpleado)
        {
            if (string.IsNullOrEmpty(idEmpleado))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("código de empleado").Throw();

            Model.IdEmpleado = idEmpleado;
            OnModelChanged();
        }

        public void LoadOrden(string orden, string parametro)
        {
            if (string.IsNullOrEmpty(orden))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO(parametro).Throw();

            Model.NumOrden = orden;
            OnModelChanged();
        }

        public void LoadLote(string lote, string parametro)
        {
            if (string.IsNullOrEmpty(lote))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO(parametro).Throw();

            Model.Lote = lote;
            OnModelChanged();
        }

        public void LoadCodigo(string codigo, string parametro)
        {
            if (string.IsNullOrEmpty(codigo))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO(parametro).Throw();

            Model.Codigo = codigo;
            OnModelChanged();
        }

        public void LoadCantidad(string cantidad, string parametro)
        {
            if (string.IsNullOrEmpty(cantidad))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO(parametro).Throw();

            Model.Cantidad = Convert.ToInt32(cantidad);
            OnModelChanged();
        }

        public void LoadCliente(string cliente, string parametro)
        {
            if (string.IsNullOrEmpty(cliente))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO(parametro).Throw();

            Model.Cliente = cliente;
            OnModelChanged();
        }

        public void LoadSequence()
        {
            string sequenceFileName = ConfigurationManager.AppSettings["SequenceFileName"];

            if (string.IsNullOrEmpty(sequenceFileName))
                using (var ofd = new OpenFileDialog())
                {
                    ofd.InitialDirectory = ConfigurationManager.AppSettings["PathSequences"];
                    if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName != null)
                    {
                        Sequence = SequenceModel.LoadFile(ofd.FileName);
                        return;
                    }
                }

            if (!File.Exists(sequenceFileName))
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("No se ha encontrado el archivo de secuencias de test").Throw();

            try
            {
                Sequence = SequenceModel.LoadFile(sequenceFileName);
            }
            catch (Exception ex)
            {
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA(string.Format("Fichero incorrecto: \r { 0}.SEQ  \r Causa: \r { 1}", sequenceFileName, ex.Message)).Throw();
            }
        }

        private TestContextModel CreateTestContext()
        {
            var data = new TestContextModel
            {
                IdEmpleado = Model.IdEmpleado,
                IdOperario = Model.IdOperario,
                NumProducto = string.IsNullOrEmpty(Model.NumProducto) ? 0 : Convert.ToInt32(Model.NumProducto),
                IdFase = Model.IdFase ?? string.Empty,
                NumOrden = string.IsNullOrEmpty(Model.NumOrden) ? 0 : Convert.ToInt32(Model.NumOrden),
                PC = Dns.GetHostName(),
                SauronInterface = Application.ProductName,
                SauronVersion = Application.ProductVersion,
                Lote = Model.Lote,
                Codigo = Model.Codigo,
                Cantidad = Model.Cantidad,
                Cliente = Model.Cliente,
            };

            return data;
        }

        public bool IsUserLogged { get { return !string.IsNullOrEmpty(Model.IdUsuario); } }

        public bool IsOperarioLogged { get { return !string.IsNullOrEmpty(Model.IdEmpleado); } }

        private void OnTestInfoPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SC.Post(_ =>
            {
                if (TestInfoChanged != null)
                    TestInfoChanged(sender, EventArgs.Empty);
            }, null);
        }

        protected void OnModelChanged()
        {
            if (ModelChanged != null)
                SC.Post(_ => { ModelChanged(this, EventArgs.Empty); }, null);
        }

        protected void OnCloseForm()
        {
            if (ModelChanged != null)
                SC.Post(_ => { CloseForm(this, EventArgs.Empty); }, null);
        }

        public void Play(bool retry = false, List<string> bastidores = null)
        {
            var lastTestContexts = TestContexts.ToList();

            if (!Model.ResultadosComunes.ContainsKey(CommonParams.DEVICE_PC))
                Model.ResultadosComunes.Add(CommonParams.DEVICE_PC, Model.DevicePC);

            if (!Model.ResultadosComunes.ContainsKey(CommonParams.TESTPLAYER_VERSION))
                Model.ResultadosComunes.Add(CommonParams.TESTPLAYER_VERSION, Model.SauronVersion);

            if (!Model.ResultadosComunes.ContainsKey(CommonParams.USER_ID))
                Model.ResultadosComunes.Add(CommonParams.USER_ID, Model.IdEmpleado);

            TestContexts.Clear();
            StartRunning = true;

            Runner.Play((numInstance, sharedVariables) =>
            {
                var testContext = CreateTestContext();

                var context = new SequenceContext(Sequence, numInstance);
                context.Services.Add<ITestContext>(testContext);
                context.Services.Add<IShell>(shell);
                context.Services.Add<IEnvironmentService>(this);
                context.Services.Add<IAssemblyResolveService>(assemblyResolver);
                context.Services.Add<IInstanceResolveService>(InstanceResolveService.Default);
                context.Services.Add<ICacheService>(cacheSvc);
                context.RunMode = RunMode.Release;

                testContext.TestInfo.SC = SC;
                testContext.TestInfo.NumInstance = numInstance;
                testContext.TestInfo.PropertyChanged += OnTestInfoPropertyChanged;

                foreach (var keyPair in Model.ResultadosComunes)
                    testContext.Resultados.Set(keyPair.Key, keyPair.Value, ParamUnidad.SinUnidad);

                testContext.TestInfo.IsRetry = retry;

                if (bastidores != null && bastidores.Any())
                {
                    var bastidor = bastidores[numInstance - 1];
                    var prevBastidor = lastTestContexts.Where(p => p.TestInfo.NumInstance == numInstance).Select(p => p.TestInfo.NumBastidor).FirstOrDefault();

                    testContext.TestInfo.IsRetry = bastidor == prevBastidor.GetValueOrDefault(1).ToString();

                    if (numInstance == 1)
                        sharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, bastidores);
                }
                else if (retry)
                    testContext.TestInfo.NumBastidor = lastTestContexts.Where(p => p.TestInfo.NumInstance == numInstance).Select(p => p.TestInfo.NumBastidor).FirstOrDefault();

                TestContexts.Add(testContext);

                return context;
            }, 1);
        }

        public bool IsRunning
        {
            get { return Runner.RunningState == RunningState.Running || StartRunning; }
        }

        public void FireEvent(object sender, string name)
        {
            if (OnEventFired != null)
                OnEventFired(sender, name);
        }

        public void UpdateIndicators(TestResult ee)
        {
            var offline = ConfigurationManager.AppSettings["HasDatabase"];
            if (offline == "SI")
                using (var svc = DI.Resolve<ITestModelDataBaseService>())
                {
                    Model.NumOK = svc.GetNumEquiposOK(Model.Lote);
                    Model.NumNG = svc.GetNumEquiposNG(Model.Lote);
                }
            else
            {
                if (ee.ExecutionResult == TestExecutionResult.Completed)
                    Model.NumOK++;
                else if (ee.ExecutionResult == TestExecutionResult.Faulted)
                    Model.NumNG--;
            }
        }
    }
}
