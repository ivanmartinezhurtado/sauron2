﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionProcesLevel0
    {
        private TestExceptionProcesLevel1 psl;

        public string[] Category { get { return psl.Category; } set { psl.Category = value; } }

        public string ClassName { get { return psl.ClassName; } set { psl.ClassName = value; } }

        public TestExceptionProcesLevel1 PARAMETROS_ERROR
        {
            get {  psl.SubCategory = ConstantsExceptions.PROCESLevel0.PARAMETROS_ERROR; return psl;  }
        }

        public TestExceptionProcesLevel1 ACTION_EXECUTE
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.ACTION_EXECUTE; return psl; }
        }

        public TestExceptionProcesLevel1 OPERARIO
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.OPERARIO; return psl; }
        }

        public TestExceptionProcesLevel1 PRODUCTO
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.PRODUCTO; return psl; }
        }

        public TestExceptionProcesLevel1 FAMILIA
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.FAMILIA; return psl; }
        }

        public TestExceptionProcesLevel1 MATERIAL
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.MATERIAL; return psl; }
        }

        public TestExceptionProcesLevel1 TRAZABILIDAD
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.TRAZABILIDAD; return psl; }
        }

        public TestExceptionProcesLevel1 FALTA_PROCESO
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.FALTA_PROCESO; return psl; }
        }

        public TestExceptionProcesLevel1 LINEA
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.LINEA; return psl; }
        }

        public TestExceptionProcesLevel0()
        {
            psl = new TestExceptionProcesLevel1();
        }
        public TestExceptionProcesLevel1 FICHERO_NO_ENCONTRADO
        {
            get { psl.SubCategory = ConstantsExceptions.PROCESLevel0.FICHERO_NO_ENCONTRADO; return psl; }
        }
    }
}
