﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Visio
{
    public static class ImageCompressor
    {
        static public byte[] CompressImage(string imagePath)
        {
            byte[] byteImage;
            using (Bitmap bmp = new Bitmap(imagePath))
            {
                byteImage = CompressImage(bmp);
            }

            return byteImage;
        }

        static public byte[] CompressImage(Bitmap image)
        {
            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            var myEncoderParameter = new EncoderParameter(myEncoder, 40L);
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            myEncoderParameters.Param[0] = myEncoderParameter;

            //TODO quizas esto podria ser un using del MemoryStream
            var stream = new MemoryStream();
            image.Save(stream, jpgEncoder, myEncoderParameters);
            byte[] byteImage = new Byte[stream.Length];
            byteImage = stream.ToArray();
            return byteImage;
        }

        static private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
                if (codec.FormatID == format.Guid)
                    return codec;
            return null;
        }
    }
}
