namespace Dezac.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.T_PARAMETRO")]
    public partial class T_PARAMETRO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_PARAMETRO()
        {
            T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
            T_TESTREPORTVALOR1 = new HashSet<T_TESTREPORTVALOR>();
            T_TESTVALOR = new HashSet<T_TESTVALOR>();
            T_VALORPARAMETRO = new HashSet<T_VALORPARAMETRO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARAM { get; set; }

        public int NUMTIPOPARAM { get; set; }

        public int NUMUNIDAD { get; set; }

        public int NUMTIPOVALOR { get; set; }

        [StringLength(50)]
        public string PARAM { get; set; }

        public short NIVELSEGURIDAD { get; set; }

        [Required]
        [StringLength(1)]
        public string METROLOGIASINO { get; set; }

        [StringLength(100)]
        public string DESCRIPCION { get; set; }

        public virtual T_TIPOPARAMETRO T_TIPOPARAMETRO { get; set; }

        public virtual T_TIPOVALORPARAMETRO T_TIPOVALORPARAMETRO { get; set; }

        public virtual T_UNIDAD T_UNIDAD { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTVALOR> T_TESTVALOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_VALORPARAMETRO> T_VALORPARAMETRO { get; set; }
    }
}
