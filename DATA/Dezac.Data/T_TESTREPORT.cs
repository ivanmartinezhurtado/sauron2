namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTREPORT")]
    public partial class T_TESTREPORT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTEST { get; set; }

        public byte[] IMAGEDOC { get; set; }

        public virtual T_TEST T_TEST { get; set; }
    }
}
