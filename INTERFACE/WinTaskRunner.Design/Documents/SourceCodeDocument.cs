﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WinTaskRunner.Design.Documents
{
    public class SourceCodeDocument : IDocument
    {
        public static string CSHARP_DOC_TYPE = ".cs";           // Podria ser un mimetype
        public static string RAZOR_DOC_TYPE = ".cshtml";        // Podria ser un mimetype

        public event EventHandler<string> TextChanged;

        public Dictionary<string, object> Properties { get; private set; }

        public SourceCodeDocument()
        {
            Properties = new Dictionary<string, object>();
            Name = "Untitled";
            DocType = CSHARP_DOC_TYPE;
            IsNew = true;
        }

        public string Name
        {
            get;
            set;
        }

        public string FileName { get; set; }

        public bool IsNew
        {
            get;
            set;
        }

        public void Save()
        {
        }

        public void Save(string fileName)
        {
        }

        public void Save(Stream stream, Encoding encoding = null)
        {
            if (Text == null)
                return;

            byte[] data = (encoding ?? UTF8Encoding.UTF8).GetBytes(Text);
            stream.Write(data, 0, data.Length);
        }

        public string DocType { get; set; }

        private string text;
        public string Text
        {
            get { return text; }
            set
            {
                if (text != value)
                {
                    text = value;
                    if (TextChanged != null)
                        TextChanged(this, text);
                }
            }
        }
    }
}
