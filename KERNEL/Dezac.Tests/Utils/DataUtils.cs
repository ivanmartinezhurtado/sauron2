﻿using Dezac.Services;

namespace Dezac.Tests.Utils
{
    public class DataUtils
    {
        public static void SaveFileNumTestFase(int numTestFase, string name, string tipo, byte[] data)
        {
            using (var db = new DezacService())
            {
                db.AddTestFaseFile(numTestFase, name, data, tipo);
            }
        }

        public static void SaveFileVANumTestFase(int numTestFase, string name, string tipo, byte[] data)
        {
            using (var db = new DezacService())
            {
                db.AddTestFaseVAFile(numTestFase, name, data, tipo);
            }
        }

        public static void SaveProcessFaseFile(int numRegister, byte[] data)
        {
            using (var db = new DezacService())
            {
                db.AddProcessFaseFile(numRegister, data, "84");
            }
        }
    }
}
