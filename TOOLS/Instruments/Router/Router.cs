﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;

namespace Instruments.Router
{
    public abstract class Router
    {
        public virtual string UserName { get; set; }

        public string Password { get; set; }

        public string RouterIP { get; set; }

        public string RouterMaskSubnet { get; set; }

        internal static Router instance { get; set; }

        public Router()
        {
        }

        public static Router Default()
        {
            if (instance == null)
            {
                instance = TryRouter(new RouterDLink615());
                if (instance == null)
                {
                    var card = NetUtils.GetNetworAdapter();
                    if (card.Dhcp)
                    {
                        NetUtils.ConfigIP("100.0.0.2", "100.0.0.2");
                        Thread.Sleep(5000);
                    }

                    instance = TryRouter(new RouterVirtual());
                }

                if (instance == null)
                    TestException.Create().HARDWARE.INSTRUMENTOS.ROUTER("No se ha encontrado el router, compruebe su conexión").Throw();
            } 
            
            return instance;
        }

        private static Router TryRouter(Router router)
        {
            try
            {
                if (router is RouterDLink615)
                    router.RouterIP = NetUtils.GetDefaultGateway();

                var config = router.ReadConfig();

                if (router is RouterVirtual)
                    router.RouterIP = config.LanIP;

                router.RouterMaskSubnet = config.LanMaskIP ?? "255.255.255.0";

                return router;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public abstract RouterConfig ReadConfig();

        public abstract RouterConfig ApplyConfig(RouterConfig config);

        public abstract RouterConfigWifi ReadConfigWifi();

        public abstract RouterConfigWifi ApplyConfigWifi(RouterConfigWifi config);

        public abstract List<ClientEntry> GetConnectedClientsFromWeb(bool excludeMe, bool PRS600 = true);

        public abstract void DeleteDHCPEntry(string ip, string mac);

        public bool PingIP(string ip, int timeout = 5000, int retries = 5)
        {
            Ping p = new Ping();
            PingReply result;
            int i = 0;

            do
            {
                result = p.Send(ip, 5000);
                i++;
            } while (result.Status != IPStatus.Success && i < retries);

            return result.Status == IPStatus.Success;
        }

        public string GetIPFromMAC(string withMAC)
        {
            var item = GetConnectedClientsFromWeb(false).Where(p => p.MAC.ToUpper() == withMAC.ToUpper()).FirstOrDefault();           
            return item != null ? item.IP : null;
        }

        public void DeleteAllDHCPEntry(bool excludeMe)
        {
            var list = GetConnectedClientsFromWeb(excludeMe);
            if (list == null)
                return;

            foreach (var entry in list)
                DeleteDHCPEntry(entry.IP, entry.MAC);
        }

        public abstract void Disposed();

    }

    public class RouterConfig
    {
        public string WanIP { get; set; }
        public string RouterName { get; set; }
        public string HostName { get; set; }
        public string LanIP { get; set; }
        public string LanMaskIP { get; set; }
        public string LanGatewayIP { get; set; }
        public string LocalDNS { get; set; }
        public bool? EnableDHCP { get; set; }
        public int StartDHCP { get; set; }
        public string Domain { get; set; }
    }

    public class RouterConfigWifi
    {
        public bool Enabled { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public string Security { get; set; }
    }
}
