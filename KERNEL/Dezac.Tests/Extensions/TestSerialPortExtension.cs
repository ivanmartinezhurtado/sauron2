﻿using Comunications.Message;
using Dezac.Core.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Extensions
{

    public static class TestSerialPortExtension
    {
        public static string ReadToText(this TestBase testBase, IStreamResource sp, int interval, int initialDelay, int NumReady, int RetryException, params string[] TextToRead)
        {
            if (sp.GetType() == typeof(SerialPortAdapter))
                return ReadLineToText(testBase, (SerialPortAdapter)sp, interval, initialDelay, NumReady, RetryException, TextToRead);
            else
                return ReadLineToText(testBase, sp, interval, initialDelay, NumReady, RetryException, TextToRead);
        }

        public static string ReadLineToText(this TestBase testBase, IStreamResource sp, int interval, int initialDelay, int NumReady, int RetryException, params string[] TextToRead)
        {
            int retry = 0;

            string tramaRx = "";

            var sampler = Sampler.Run(
            () => { return new SamplerConfig { Interval = interval, InitialDelay = initialDelay, NumIterations = NumReady, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken }; },
            (step) =>
            {
                try
                {
                    tramaRx = sp.ReadLineString();
                    if (string.IsNullOrEmpty(tramaRx))
                        throw new TimeoutException(string.Format("no hemos recibido nada por {0}", sp.GetType().Name.Replace("ClientAdapter", "")));

                    retry = 0;
                    testBase.Logger.DebugFormat("RX: {0}", tramaRx);
                    foreach (string text in TextToRead)
                        if (tramaRx.Contains(text))
                        {
                            step.Cancel = true;
                            break;
                        }
                }
                catch (TimeoutException ex)
                {
                    retry += 1;
                    System.Threading.Thread.Sleep(2000);

                    testBase.Logger.WarnFormat("PORT: {0} Ex: {1}   Retry: {2} / {3}", sp.PortName, ex.Message, retry, RetryException);

                    if (retry > RetryException)
                        throw new Exception(string.Format("Error, no hemos recibido la respuesta esperada \"{0}\"", TextToRead));
                }
            });

            if (!sampler.Canceled)
                tramaRx = "";

            return tramaRx;
        }

        public static string ReadLineToText(this TestBase testBase, SerialPortAdapter sp, int interval, int initialDelay, int NumReady, int RetryException, params string[] TextToRead)
        {
            int retry = 0;
            byte[] mbapHeader = new byte[512];
            string tramaRx = "";

            var sampler = Sampler.Run(
            () => { return new SamplerConfig { Interval = interval, InitialDelay = initialDelay, NumIterations = NumReady, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken }; },
            (step) =>
            {
                try
                {
                    tramaRx = sp.ReadLineString().Replace("\r", "").Replace("\n", "");
                    retry = 0;

                    testBase.Logger.DebugFormat("PORT: {0} RX: {1}", sp.PortName, tramaRx);

                    foreach (string text in TextToRead)
                        if (tramaRx.Contains(text))
                        {
                            step.Cancel = true;
                            break;
                        }
                }
                catch (TimeoutException ex)
                {
                    retry += 1;
                    testBase.Logger.WarnFormat("PORT: {0} Ex: {1}   Retry: {2} / {3}", sp.PortName, ex.Message, retry, RetryException);

                    if (retry > RetryException)
                        throw new Exception(string.Format("Error, no hemos recibido la respuesta esperada \"{0}\"", TextToRead));
                }
            });

            if (!sampler.Canceled)
                tramaRx = "";

            return tramaRx;
        }

        public static string ReadBytesToText(this TestBase testBase, IStreamResource sp, int interval, int initialDelay, int NumReady, int RetryException, params string[] TextToRead)
        {
            if (sp.GetType() == typeof(SerialPortAdapter))
                return ReadBytesToText(testBase, (SerialPortAdapter)sp, interval, initialDelay, NumReady, RetryException, TextToRead);
            else
                return ReadLineToText(testBase, sp, interval, initialDelay, NumReady, RetryException, TextToRead);

        }

        public static string ReadBytesToText(this TestBase testBase, SerialPortAdapter sp, int interval, int initialDelay, int NumReady, int RetryException, params string[] TextToRead)
        {
            int retry = 0;
            string tramaRx = "";
            sp.OpenPort();

            var sampler = Sampler.Run(
            () => { return new SamplerConfig { Interval = interval, InitialDelay = initialDelay, NumIterations = NumReady, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken }; },
            (step) =>
            {
                try
                {
                    var toRead = sp.SerialPort.BytesToRead;
                    if (toRead > 0)
                    {
                        testBase.Logger.DebugFormat("PORT: {0}  -> SE HAN RECIBIDO {1} BYTES", sp.PortName, toRead);
                        retry = 0;

                        byte[] mbapHeader = new byte[toRead];
                        var readed = sp.SerialPort.Read(mbapHeader, 0, toRead);
                        tramaRx = UTF8Encoding.UTF8.GetString(mbapHeader, 0, readed);
                        testBase.Logger.DebugFormat("PORT: {0} RX: {1}", sp.PortName, tramaRx);

                        foreach (string text in TextToRead)
                            if (tramaRx.Contains(text))
                            {
                                step.Cancel = true;
                                break;
                            }
                    }
                    else
                        testBase.Logger.DebugFormat("PORT: {0} -> REINTENTO {1} / {2}  - NO HAY DATOS RECIBIDOS", sp.PortName, step.Step, NumReady);

                }
                catch (TimeoutException ex)
                {
                    retry += 1;
                    testBase.Logger.WarnFormat("PORT: {0} Ex: {1}  Retry: {2} / {3}", sp.PortName, ex.Message, retry, RetryException);

                    if (retry > RetryException)
                        throw new Exception(string.Format("Error, no hemos recibido la respuesta esperada \"{0}\"", TextToRead));
                }
            });

            if (!sampler.Canceled)
                tramaRx = "";

            return tramaRx;
        }

        public static string ReadBytesToText(this TestBase testBase, SerialPortAdapter sp, int interval, int initialDelay, int NumReady, int RetryException, bool IsHexadecimal, params string[] TextToRead)
        {
            int retry = 0;
            string tramaRx = "";
            sp.OpenPort();

            var sampler = Sampler.Run(
            () => { return new SamplerConfig { Interval = interval, InitialDelay = initialDelay, NumIterations = NumReady, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken }; },
            (step) =>
            {
                try
                {
                    var toRead = sp.SerialPort.BytesToRead;
                    if (toRead > 0)
                    {
                        testBase.Logger.DebugFormat("PORT: {0}  -> SE HAN RECIBIDO {1} BYTES", sp.PortName, toRead);
                        retry = 0;

                        byte[] mbapHeader = new byte[toRead];
                        var readed = sp.SerialPort.Read(mbapHeader, 0, toRead);                     
                        tramaRx = IsHexadecimal ? mbapHeader.BytesToHexString() : UTF8Encoding.UTF8.GetString(mbapHeader, 0, readed);
                        testBase.Logger.DebugFormat("PORT: {0} RX: {1}", sp.PortName, tramaRx);

                        foreach (string text in TextToRead)
                            if (tramaRx.Contains(text))
                            {
                                step.Cancel = true;
                                break;
                            }
                    }
                    else
                        testBase.Logger.DebugFormat("PORT: {0} -> REINTENTO {1} / {2}  - NO HAY DATOS RECIBIDOS", sp.PortName, step.Step, NumReady);

                }
                catch (TimeoutException ex)
                {
                    retry += 1;
                    testBase.Logger.WarnFormat("PORT: {0} Ex: {1}  Retry: {2} / {3}", sp.PortName, ex.Message, retry, RetryException);

                    if (retry > RetryException)
                        throw new Exception(string.Format("Error, no hemos recibido la respuesta esperada \"{0}\"", TextToRead));
                }
            });

            if (!sampler.Canceled)
                tramaRx = "";

            return tramaRx;
        }

        public static string WriteAndReadText(this TestBase testBase, IStreamResource sp, int interval, int initialDelay, int RetryReads, int RetryException, string TextToSend, params string[] TextToRead)
        {
            sp.DiscardInBuffer();

            sp.WriteLineString(TextToSend);

            testBase.Logger.DebugFormat("PORT: {0} TX: {1}", sp.PortName, TextToSend);

            if (sp.GetType() == typeof(SerialPortAdapter))
                return ReadLineToText(testBase, (SerialPortAdapter)sp, interval, initialDelay, RetryReads, RetryException, TextToRead);
            else
                return ReadLineToText(testBase, sp, interval, initialDelay, RetryReads, RetryException, TextToRead);
        }
     
        public static string WriteAndReadBytesText(this TestBase testBase, IStreamResource streamResource, int interval, int initialDelay, int RetryReads, int RetryException, string TextToSend, params string[] TextToRead)
        {
            if (streamResource.GetType() == typeof(SerialPortAdapter))
            {
                var sp = streamResource as SerialPortAdapter;
                sp.OpenPort();
                sp.DiscardInBuffer();
                sp.WriteLineStringAsBytes(TextToSend);
                testBase.Logger.DebugFormat("PORT: {0} TX: {1}", sp.PortName, TextToSend);
                return ReadBytesToText(testBase, sp, interval, initialDelay, RetryReads, RetryException, TextToRead);
            }
            else
                return WriteAndReadText(testBase, streamResource, interval, initialDelay, RetryReads, RetryException, TextToSend, TextToRead);
            
        }
    }
}