﻿using Comunications.Message;
using System.Collections.Generic;

namespace Comunications.Protocolo
{
    public class MTEProtocol : ASCIIProtocol
    {
        public MTEProtocol()
        {
            CaracterFinRx = "\r";
            CaracterFinTx = "\r";
        }

        public override byte[] ReadMessage(ITransportMessage message, IStreamResource streamResource)
        {
            MTEMessage mteMessage = message as MTEMessage;

            int numCommands = mteMessage.NumCommands;

            var data = new List<byte>();

            while(numCommands > 0)
            {
                if (data.Count > 0)
                    data.Add((byte)';');
                data.AddRange(base.ReadMessage(message, streamResource));
                numCommands--;
            }

            return data.ToArray();
        }
    }
}
