namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_VALORPARAMETROTOLPROD
    {
        public int NUMGRUPO { get; set; }
        public int NUMPARAM { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public string TIPOTOL { get; set; }
        public int NUMGRUPO1 { get; set; }
        public int NUMPARAM1 { get; set; }
        public int NUMGRUPO2 { get; set; }
        public int NUMPARAM2 { get; set; }
    
        public virtual T_VALORPARAMETROPRODUCTO T_VALORPARAMETROPRODUCTO { get; set; }
        public virtual T_VALORPARAMETROPRODUCTO T_VALORPARAMETROPRODUCTO1 { get; set; }
        public virtual T_VALORPARAMETROPRODUCTO T_VALORPARAMETROPRODUCTO2 { get; set; }
    }
}
