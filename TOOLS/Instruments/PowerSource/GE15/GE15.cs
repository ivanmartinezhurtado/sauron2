﻿using Comunications;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.08)]
    public class GE15 : PowerSourceACI, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(GE15));
        private GE15_Device ge;

        public enum PrivateCommands
        {
            SET_HARMONICS_VOLTAGE = Commands.USER_DEFINED_COMMANDS + 1,
            SET_HARMONICS_CURRENT = Commands.USER_DEFINED_COMMANDS + 2,
        };

        public enum OnOffEnum
        {
            UI = 1,
            U = 2,
            I = 3
        }


        public GE15()
            : base(logger)
        {

            ge = new GE15_Device();
            Tolerance = 0.5;
        }

        public void SetPort(int port, string host, byte periferico)
        {
            ge.SetPortAndHost(port, host, periferico);
        }

        public void ApplyHarmonicsVoltage(Phase phase, byte harmonic, int amplitud)
        {
            if (harmonic > 30)
                throw new NotSupportedException("No se puede configurar un armonico superior al numero 31");

            if (amplitud >= 100)
                throw new NotSupportedException("No se puede configurar una amplitud superior o igual al 100% de la fundamental");

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_HARMONICS_VOLTAGE, true, (byte)phase, harmonic, amplitud, 0));
        }

        public void ApplyHarmonicsCurrent(Phase phase, byte harmonic, int amplitud)
        {
            if (harmonic > 30)
                throw new NotSupportedException("No se puede configurar un armonico superior al numero 31");

            if (amplitud >= 100)
                throw new NotSupportedException("No se puede configurar una amplitud superior o igual al 100% de la fundamental");

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_HARMONICS_CURRENT, true, (byte)phase, harmonic, amplitud, 0));
        }

        public void ApplyConfig()
        {
            SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
            //this.IsOn = true;
           // SendCommand(new Command<Commands>(Commands.READ_STATUS, true));
        }

        public enum Phase
        {
            L1 = 1,
            L2 = 2,
            L3 = 3
        }


        #region  SWITCH CASE
        protected override void InternalSendCommand<T>(Command<T> command)
        {

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:
                    ge.WriteVoltage_Fundamental_AC_GLOBAL_SP((float)Presets.Voltage);
                    DevicePreset.Voltage = Presets.Voltage;
                    break;

                case (int)Commands.SET_FREQ:
                    ge.WriteFrequency_Output_GLOBAL((float)Presets.Freq);
                    DevicePreset.Freq = Presets.Freq;
                    break;

                case (int)Commands.SET_CURRENT:
                case (int)Commands.SET_PF:
                    break;

                case (int)Commands.SET_DESFASE:
                    ge.WriteVoltage_Fundamental_Phase_Angle_GLOBAL_SP((float)Presets.Desfase);
                    DevicePreset.Desfase = Presets.Desfase;
                    break;

                case (int)Commands.RESET:
                    ge.WriteCW_Reset();
                  
                    ResetPresets();
                    break;

                case (int)Commands.APPLY_CONFIG:
                    ge.WriteCW_Trigger_Config();
                    break;

                case (int)Commands.READ_STATUS:

                    int i = ge.ReadGrafcetState();

                    //0-Init, 1-SoftStart 2-Run, 3-Warning, 4-Alarm
                    Status.IsBusy = i == 1;
                    Status.HasError = i == 4;
                    Status.HasWarning = i == 3;
                    break;

                case (int)Commands.READ_VOLTAGE:
                    DeviceData.Voltage = ge.ReadVoltage_Fundamental_AC_GLOBAL_SP();
                    break;

                case (int)Commands.READ_CURRENT:
                    DeviceData.Current = ge.ReadCurrentOutput().Current_Output_U_RMS;
                    break;

                case (int)Commands.READ_FREQ:
                    DeviceData.Freq = ge.ReadFrequency_Output_GLOBAL();
                    break;

                case (int)Commands.READ_DESFASE:
                    DeviceData.PF = ge.ReadVoltage_Fundamental_Phase_Angle_GLOBAL_SP();
                    break;

                default:
                    logger.WarnFormat("COMMAND NOT IMPLEMENTED", command.Commands);
                    break;
            }
        }
        #endregion

        #region FUNCTIONS

        public void WriteVoltage(float L1, float L2, float L3)
        {
            ge.WriteVoltage_Fundamental_AC_U_SP(L1);
            ge.WriteVoltage_Fundamental_AC_V_SP(L2);
            ge.WriteVoltage_Fundamental_AC_W_SP(L3);
        }
        public void WriteFrequency(float L1, float L2, float L3)
        {
            ge.WriteFrequency_Output_U_SP(L1);
            ge.WriteFrequency_Output_V_SP(L2);
            ge.WriteFrequency_Output_W_SP(L3);
        }
        public void WriteDesfase(float L1, float L2, float L3)
        {
            ge.WriteVoltage_Fundamental_Phase_Angle_U_SP(L1);
            ge.WriteVoltage_Fundamental_Phase_Angle_V_SP(L2);
            ge.WriteVoltage_Fundamental_Phase_Angle_W_SP(L3);
        }

        public void EnableDisable(int value)
        {
            ge.WriteCW_EnableDisable(value);

        }

        public void WriteRunReady(int value)
        {
            ge.WriteCW_RunReady(value);
        }

        public GE15_Device.LimitMaxVoltage ReadVoltageLimitMax()
        {
            return ge.ReadLimitMaxVoltage();

        }

        public GE15_Device.LimitMinVoltage ReadVoltageLimitMin()
        {
            return ge.ReadLimitMinVoltage();

        }

        public void WriteLimitMaxVoltage(GE15_Device.LimitMaxVoltage limitVoltageMax)
        {
            ge.WriteLimitMaxVoltage(limitVoltageMax);

        }

        public void WriteLimitMinVoltage(GE15_Device.LimitMinVoltage limitVoltageMin)
        {
            ge.WriteLimitMinVoltage(limitVoltageMin);

        }
        public GE15_Device.GrafcetStatePhase GrafcetStatePerPhase()
        {
            return ge.ReadGrafcetStatePhase();

        }

        public float ReadVbus()
        {
            return ge.ReadVbusTotal();
        }


        public int ReadSatusOUT()
        {
            return ge.ReadSW_GrafcetState_OUT();
        }

        public int ReadSatusIN()
        {
            return ge.ReadSW_GrafcetState_IN();
        }
        public int ReadSatusU()
        {
            return ge.ReadGrafcetState();
        }


        #endregion

        public void Dispose()
        {
            if (ge != null)
                ge.Dispose();
        }

        
        protected override bool ValidateEstabilization(double tolerance)
        {
            ReadVoltage();

            if (DevicePreset.Voltage == 0)
                return DeviceData.Voltage < VoltageOffMin;

            var Max = Math.Round(DevicePreset.Voltage + (DevicePreset.Voltage * (tolerance / 100)), 6);

            var Min = Math.Round(DevicePreset.Voltage - (DevicePreset.Voltage * (tolerance / 100)), 6);

            return (DeviceData.Voltage > Min) && (DeviceData.Voltage < Max);
        }

        protected virtual void OnMessageException(object sender, MessageExceptionHandler e)
        {
            logger.WarnFormat("PowerSource MTE OnMessageExceptionr by {0}", e.Exception.Message);
            if (e.Exception is ApplicationException)
            {
                if (!e.Throw)
                {
                    SendCommand(new Command<Commands>(Commands.RESET, true));
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }
    }
}
