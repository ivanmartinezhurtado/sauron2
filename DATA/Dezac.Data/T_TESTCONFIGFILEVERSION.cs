namespace Dezac.Data
{
    using Oracle.ManagedDataAccess.Types;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTCONFIGFILEVERSION")]
    public partial class T_TESTCONFIGFILEVERSION
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTCFV { get; set; }

        public int NUMTCF { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(15)]
        public string USUARIOALTA { get; set; }

        //public OracleBFile FICHERO { get; set; }     
        //public byte[] FICHERO { get; set; }

        [StringLength(512)]
        public string DESCRIPCION { get; set; }

        public virtual T_TESTCONFIGFILE T_TESTCONFIGFILE { get; set; }
    }
}
