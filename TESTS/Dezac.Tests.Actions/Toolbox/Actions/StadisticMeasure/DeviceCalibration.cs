﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace Dezac.Tests.Actions
{
    /// <summary>
    /// Calibrate a unit following the criteria defined in CalibrationParameters by making measures from the unit and from a pattern.
    /// </summary>
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(DeviceCalibrationDescription))]
    public class DeviceCalibration : ActionBase
    {
        private ConfigurationSampler configuration = new ConfigurationSampler();      

        /// <summary>
        /// Function used to read measures from the unit to calibrate.
        /// </summary>
        [Description("Función para leer medidas desde las unidades de calibración")]
        [CategoryAttribute("3. Configuration")]
        public RunMethodAction ReadMeasureDevice { get; set; }     

        /// <summary>
        /// Function used to read measures from the measure pattern.
        /// </summary>
        [Description("Función para leer medidas desde los patrones de medida")]
        [CategoryAttribute("3. Configuration")]
        public RunMethodAction ReadMeasurePattern { get; set; }

        /// <summary>
        /// Array of margins definition using AdjustValueDef class.
        /// </summary>
        [Description("Matriz de definición de márgenes usando la clase AdjustValueDef")]
        [CategoryAttribute("3. Configuration")]
        [Editor(typeof(AdjustValueList), typeof(System.Drawing.Design.UITypeEditor))]
        public List<AdjustValueDefWrapper> CalibrationParameters { get; set; }

        /// <summary>
        /// Sampling parameters.
        /// </summary>
        [CategoryAttribute("3. Configuration")]
        [Description("Muestreo de los parámetros que utilizamos en el action")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ConfigurationSampler Configuration { get { return configuration; } set { configuration = value; } } 
        
        /// <summary>
        /// Test Point Name.
        /// </summary>
        [CategoryAttribute("4. Results")]
        [Description("Nombre del TestPoint para su resultado")]
        [DefaultValue("")]
        public string TestPoint { get; set; }
                  

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var parametros = ResolveProperties(CalibrationParameters, TestPoint);

            testBase.TestCalibrationWithVariance(parametros,
            () =>
            {
                ReadMeasureDevice?.Execute();

                var medidas = CalibrationParameters.Select(p => Convert.ToDouble(VariablesTools.ResolveValue(p.VarNamesDevice))).ToList();

                return medidas.ToArray();
            },
            () =>
            {
                ReadMeasurePattern?.Execute();

                var medidas = CalibrationParameters.Select(p => Convert.ToDouble(VariablesTools.ResolveValue(p.VarNamesPattern))).ToList();

                return medidas.ToArray();

            }, Configuration);
        }
    }
    
    class DeviceCalibrationDescription : ActionDescription
    {
        public override string Name { get { return "DeviceCalibration"; } }
        public override string Description { get { return "Calibra una unidad siguiendo los criterios definidos en \"CalibrationParameters\" comparando medidas desde la unidad y desde un patrón"; } }
        public override string Category { get { return "Actions/StadisticMeasure"; } }
        public override Image Icon { get { return Properties.Resources.BarChart; } }
        public override string Dependencies { get { return "InitTestBase;"; } }
    }
}