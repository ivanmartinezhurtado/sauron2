namespace Zurc.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TestStep")]
    public partial class TestStep
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TestFase")]
        public int TestFaseId { get; set; }
        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }

        [Required]
        [StringLength(1)]
        public string Resultado { get; set; }
        public string Excepcion { get; set; }
        [StringLength(100)]
        public string Grupo { get; set; }
        public int? Duracion { get; set; }

        public TestFase TestFase { get; set; }
    }
}
