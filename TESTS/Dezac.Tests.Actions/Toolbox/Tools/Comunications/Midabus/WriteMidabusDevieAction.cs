﻿using Dezac.Device;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{

    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WriteMidabusDevieActionDescription))]
    public class WriteMidabusDevieAction : ActionBase
    {
        [Description("Mensaje Midabus a mandar")]
        [Category("3. Configuration")]
        public string Message { get; set; }

        [Description("Cabecera de la respuesta que valida el mensaje, si la trama no recibe respuesta dejar en blanco")]
        [Category("3. Configuration")]
        public string Response { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            string name = DeviceName ?? "Midabus";
            var uut = GetVariable(name) as MidabusDevice;

            if (string.IsNullOrEmpty(Response))
                uut.Write(Message);
            else
                uut.Write(Message, Response);         
        }
    }

    public class WriteMidabusDevieActionDescription : ActionDescription
    {
        public override string Name { get { return "WriteMidabus"; } }
        public override string Description { get { return "Escribe los registros especificados"; } }
        public override string Category { get { return "Tools/Comunications/Midabus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitMidabusDeviceAction;DisposeMidabusDeviceAction:2"; } }
    }
}
