﻿using Dezac.Core.Exceptions;
using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    /// <summary>
    /// Interaction logic for ResultView.xaml
    /// </summary>
    public partial class ResultView : UserControl
    {
        public ulong? Bastidor { get; private set; }
        private bool bastidorValidated;
        public bool BastidorValidated
        {
            set
            {
                bastidorValidated = value;
                DrawTextboxBastidor();
            }
        }
        public bool ControlDisabled { get; set; }

        private SequenceContext sequenceContext;

        private ResultView()
        {
            InitializeComponent();
        }
        public ResultView(SequenceContext context, bool bastidorEntry)
            : this()
        {
            sequenceContext = context;
            textBastidor.Visibility = bastidorEntry ? Visibility.Visible : Visibility.Collapsed;

            if (bastidorEntry && context.ExecutionResult != TestExecutionResult.Completed)
            {
                var testContext = context.Services.Get<ITestContext>();
                if (testContext != null && testContext.TestInfo.NumBastidor != null)
                    Bastidor = testContext.TestInfo.NumBastidor;

                textBastidor.Text = Bastidor.ToString();
            }
            if (!String.IsNullOrEmpty(textBastidor.Text))
                BastidorValidated = true;

            BitmapImage bitmap = null;
            switch (context.ExecutionResult)
            {
                case TestExecutionResult.Faulted:
                    labelResult.Content = $"Test {context.NumInstance}: KO";
                    gridResult.Background = new SolidColorBrush(Color.FromRgb(0xe9, 0x3a, 0x3a));
                    labelResult.BorderBrush = new SolidColorBrush(Color.FromRgb(0xe9, 0x3a, 0x3a));
                    bitmap = new BitmapImage(new Uri("ErrorIcon.png", UriKind.Relative));
                    WriteErrorMessage(context.GetSteps(StepExecutionResult.Failed), context);
                    break;
                case TestExecutionResult.Aborted:
                    labelResult.Content = $"Test {context.NumInstance}: Abortado";
                    gridResult.Background = new SolidColorBrush(Color.FromRgb(0xac, 0x48, 0x48));
                    labelResult.BorderBrush = new SolidColorBrush(Color.FromRgb(0xac, 0x48, 0x48));
                    bitmap = new BitmapImage(new Uri("stopIcon.jpg", UriKind.Relative));
                    WriteErrorMessage(context.GetSteps(StepExecutionResult.Aborted), context);
                    break;
                case TestExecutionResult.Completed:
                    labelResult.Content = $"Test {context.NumInstance}: OK";
                    gridResult.Background = new SolidColorBrush(Color.FromRgb(0x4f, 0xd8, 0x53));
                    labelResult.BorderBrush = new SolidColorBrush(Color.FromRgb(0x4f, 0xd8, 0x53));
                    bitmap = new BitmapImage(new Uri("Ok-icon.png", UriKind.Relative));
                    break;
            }
            imageResult1.Source = bitmap;
            imageResult2.Source = bitmap;
            DrawTextboxBastidor();
        }

        private void WriteErrorMessage(IEnumerable<StepResult> steps, SequenceContext context)
        {
            foreach (StepResult result in steps)
            {
                if (result.Exception == null)
                    continue;

                List<Exception> innerExceptions;
                if (result.Exception is AggregateException)
                    innerExceptions = (result.Exception as AggregateException).InnerExceptions.ToList();
                else
                    innerExceptions = new List<Exception>() { result.Exception };

                foreach (Exception exception in innerExceptions)
                {
                    textErrorMessage.Inlines.Add(new Run { FontSize = 15, FontStyle = FontStyles.Italic, Text = result.Path });
                    textErrorMessage.Inlines.Add(Environment.NewLine);

                    var sauronException = exception as SauronException;
                    if (sauronException == null)
                    {
                        textErrorMessage.Inlines.Add(exception.Message);
                        textErrorMessage.Inlines.Add(Environment.NewLine);
                    }
                    else
                        using (IDezacService db = DI.Resolve<IDezacService>())
                        {
                            ITestContext testContext = context.Services.Get<ITestContext>();
                            ErrorsIrisDescription errorDescription = db.GetIrisErrorsDescriptions(sauronException.CodeError, testContext.NumFamilia.Value, testContext.NumProducto, testContext.IdFase);
                            if (errorDescription != null)
                            {
                                textErrorMessage.Inlines.Add(errorDescription.DESCRIPCION);
                                textErrorMessage.Inlines.Add(Environment.NewLine);
                                if (!string.IsNullOrEmpty(errorDescription.DIAGNOSTICO))
                                {
                                    textErrorMessage.Inlines.Add($"Diagnostico {errorDescription.DIAGNOSTICO}");
                                    textErrorMessage.Inlines.Add(Environment.NewLine);
                                }
                            }
                            else
                            {
                                textErrorMessage.Inlines.Add($"Codigo: {sauronException.Message}");
                                textErrorMessage.Inlines.Add(Environment.NewLine);
                            }
                            textErrorMessage.Inlines.Add($"Codigo: {sauronException.CodeError}");
                            textErrorMessage.Inlines.Add(Environment.NewLine);
                        }

                    textErrorMessage.Inlines.Add(new Line { X1 = 60, Y1 = 8, X2 = 285, Y2 = 8, Stroke = new SolidColorBrush(Colors.Black), StrokeThickness = 1.0 });
                    textErrorMessage.Inlines.Add(Environment.NewLine);
                }
            }
        }
        private void textBastidor_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.IsNullOrEmpty(textBastidor.Text))
            {
                Bastidor = null;
                BastidorValidated = false;
                return;
            }

            if (!UInt64.TryParse(textBastidor.Text, out ulong bastidor))
            {
                MessageBox.Show("No se permiten caracteres no numericos como bastidor", "Error entrada bastidor", MessageBoxButton.OK, MessageBoxImage.Warning);
                if (Bastidor != null)
                    textBastidor.Text = Bastidor.ToString();

                textBastidor.CaretIndex = textBastidor.Text.Length;
            }
            else
            {
                BastidorValidated = true;
                Bastidor = bastidor;
            }
        }
        public void SetFocus()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
            {
                if (!buttonLog.IsFocused)
                {
                    textBastidor.SelectAll();
                    textBastidor.Focus();
                }

            }));
        }
        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
            {
                if (!buttonLog.IsFocused)
                {
                    textBastidor.SelectAll();
                    textBastidor.Focus();
                }
            }));
        }
        private void textBastidor_GotFocus(object sender, RoutedEventArgs e)
        {
            DrawTextboxBastidor();
        }
        private void textBastidor_LostFocus(object sender, RoutedEventArgs e)
        {
            DrawTextboxBastidor();
        }
        private void DrawTextboxBastidor()
        {
            if (textBastidor.IsFocused)
                textBastidor.Background = new SolidColorBrush(Color.FromRgb(0xce, 0xce, 0xff));
            else if (String.IsNullOrEmpty(textBastidor.Text))
                textBastidor.Background = new SolidColorBrush(Color.FromRgb(0xd7, 0xd7, 0xd7));
            else if (!bastidorValidated)
            {
                textBastidor.BorderBrush = new SolidColorBrush(Color.FromRgb(0xff, 0x00, 0x00));
                textBastidor.Background = new SolidColorBrush(Color.FromRgb(0xff, 0xce, 0xce));
            }
            else
            {
                textBastidor.BorderBrush = new SolidColorBrush(Color.FromRgb(0x00, 0xff, 0x00));
                textBastidor.Background = new SolidColorBrush(Color.FromRgb(0xce, 0xff, 0xce));
            }
        }

        private void buttonLog_Click(object sender, RoutedEventArgs e)
        {
            ITestContext testContext = sequenceContext.Services.Get<ITestContext>();
            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, false, testContext, false);
        }
    }
}
