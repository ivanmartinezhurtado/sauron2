﻿using Comunications.Message;
using log4net;
using System;
using System.IO.Ports;

namespace Dezac.Device
{
    public class ModbusDeviceCirPLC: IDisposable
    {
        private int portCom;

        private SerialPort sp;

        protected ModbusDevice modbus;

        protected CirPLCModbusTransport transport;
   
        public ModbusDeviceCirPLC(int portCom, int bps = 9600, Parity parity = Parity.None, StopBits stopBits = StopBits.One, ILog _logger = null)
        {
            this.portCom = portCom;

            sp = new SerialPort("COM" + portCom, bps, parity, 8, stopBits);
            transport = new CirPLCModbusTransport(new SerialPortAdapter(sp), _logger);

            if (modbus == null)
                modbus = new ModbusDevice(transport);
        }

        public ModbusDeviceCirPLC(SerialPort SerialPort, ILog _logger = null)
        {
            sp = SerialPort;
            transport = new CirPLCModbusTransport(new SerialPortAdapter(sp), _logger);

            if (modbus == null)
                modbus = new ModbusDevice(transport);
        }

        public ModbusDevice Modbus
        {
            get
            {
                transport.CodeFunctionCirPLC = CirPLCModbusTransport.enumCodeFunctionCirPLC.CirPLCSendRequest;
                return modbus;
            }
        }

        public byte PerifericNumber
        {
            set
            {
                modbus.PerifericNumber = value;
            }
            get
            {
                return modbus.PerifericNumber;
            }
        }

        public int TimeOut { set { transport.ReadTimeout = value; } get { return transport.ReadTimeout; } }

        public int SerialNumberHost { set { transport.SerialNumberHost = value; } get { return transport.SerialNumberHost; } }

        public int PortCom
        {
            get { return portCom; }
            set
            {
                if (portCom == value)
                    return;

                if (sp.IsOpen)
                    sp.Close();

                portCom = value;
                sp.PortName = "COM" + portCom;
            }
        }

        public int BaudRate
        {
            get
            {
                return sp.BaudRate;
            }
            set
            {
                sp.BaudRate = value;
            }
        }

        public int DataBits
        {
            get
            {
                return sp.DataBits;
            }
            set
            {
                sp.DataBits = value;
            }
        }

        public Parity Parity
        {
            get
            {
                return sp.Parity;
            }
            set
            {
                sp.Parity = value;
            }
        }

        public StopBits StopBits
        {
            get
            {
                return sp.StopBits;
            }
            set
            {
                sp.StopBits = value;
            }
        }

        public bool RtsEnable
        {
            get { return sp.RtsEnable; }
            set { sp.RtsEnable = value; }
        }

        public bool DtrEnable
        {
            get { return sp.DtrEnable; }
            set { sp.DtrEnable = value; }
        }

        public bool IsOpen
        {
            get
            {
                return sp.IsOpen;
            }
        }

        public void OpenPort()
        {
            if (!sp.IsOpen)
                sp.Open();
        }

        public void ClosePort()
        {
            if (sp.IsOpen)
                sp.Close();
        }

        public  void Dispose()
        {
            if (sp != null)
                sp.Dispose();

            if (transport != null)
                transport.Dispose();

        }
    }
}
