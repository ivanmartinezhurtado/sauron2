﻿using Instruments.Actuators;
using Instruments.Auxiliars;
using Instruments.IO;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.IO.Ports;

namespace Instruments.Towers
{
    public interface ITower
    {
        MTEPPS MTEPS { get; }
        MTEPRS MTEWAT { get; }
        ZERA ZERA { get; }
        FLUKE6003A FLUKE { get; }
        Chroma Chroma { get; }
        Lambda LAMBDA { get; }
        PTE PTE { get; }
        CVMMINITower CvmminiMTE { get; }
        CVMMINITower CvmminiChroma { get; }
        CVMB100Tower CvmB100Hi { get; }
        CVMB100Tower CvmB100Low { get; }
        CVMK2Tower CvmK2 { get; }
        PCI7296 IO { get; }
        IMux MUX { get; }
        HP34401A HP34401A { get; }
        HP53131A HP53131A { get; }
        ISignalGenerator SignalGenerator { get; }
        IPowerSourceIII PowerSourceIII { get; }
        M9183A MultimeterM9183A { get; }
        //M9111A PowerSourceM9111A { get; }
        FLUKE183 Fluke183 { get; }
        SerialPort ModbusSerialPort { get; }
        SMAC_LAC25 Actuator_LAC25 { get; }
        USB4761 UsbIO { get; }

        bool IsZERAInitialized { get; set; }
        bool IsMTEPSInitialized { get; set; }
        bool IsMTEWATInitialized { get; set; }
        bool IsFLUKEInitialized { get; set; }
        bool IsChromaInitialized { get; set; }
        bool IsLambdaInitialized { get; set; }
        bool IsHP34401AInitialized { get; set; }
        bool IsHP53131AInitialized { get; set; }
        bool IsSignalGeneratorInitialized { get; set; }
        bool IsPTEInitialized { get; set; }

        int? PortZera { get; set; }
        int? PortMte { get; set; }
        int? PortMteWattimetro { get; set; }
        int? PortLambda { get; set; }

        double MeasureMultimeter(InputMuxEnum input, MagnitudToMeasure magnitud, int waitTimeMeasure = 500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false);
        double MeasureMultimeter(InputMuxEnum input, MagnitudsMultimeter magnitud, int waitTimeMeasure, Action preCondition = null, Action postCondition = null, bool whitOutIO = false);
        double MeasureMultimeter(MagnitudsMultimeter magnitud, int waitTimeMeasure = 500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false);

        double MeasureWithFrecuency(int TimeOut= 10000, HP53131A.Variables MagnitudToMeasure = HP53131A.Variables.FREQ, HP53131A.Channels channel = HP53131A.Channels.CH1, Action preCondition = null, Action postCondition = null);
        double MeasureWithFrecuency(InputMuxEnum input, int TimeOut = 10000, HP53131A.Variables MagnitudToMeasure = HP53131A.Variables.FREQ, HP53131A.Channels channel = HP53131A.Channels.CH1, Action preCondition = null, Action postCondition = null);
        
        void ActiveCurrentCircuit(bool l1, bool l2, bool l3);
        void ActiveVoltageCircuit(bool l1 = true, bool l2 = true, bool l3 = true, bool neutro = true);

        void ShutdownSources();
        void ShutdownSourcesQuick();
        void Dispose(bool whitOutIO);
        void Dispose();
    }
}
