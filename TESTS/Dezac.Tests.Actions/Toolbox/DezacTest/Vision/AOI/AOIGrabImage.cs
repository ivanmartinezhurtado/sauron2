﻿
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.AOI;

namespace Dezac.Tests.Actions.AOI
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AOIGrabImageDescription))]
    [Browsable(false)]
    public class AOIGrabImage : ActionBase
    {
        public string ImageFileName { get; set; }

        public override void Execute()
        {
            HalconInterface.AOIGrabAndSaveImage(ImageFileName);
        }
    }

    public class AOIGrabImageDescription : ActionDescription
    {
        public override string Name { get { return "AOIGrabImage"; } }
        public override string Category { get { return "DezacTest/Vision/AOI"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
