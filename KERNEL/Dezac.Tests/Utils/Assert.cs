﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using System;
using TaskRunner.Model;

namespace Dezac.Tests
{
    public class Assert
    {
        protected static ITestContext Context
        {
            get
            {
                return SequenceContext.Current.Services.Get<ITestContext>();
            }
        }       

        public static void AreEqual(object actual, object expected, SauronException exception)
        {
            if (AreEqual(actual, expected))
                return;           

            exception.Throw();
        }

        public static void AreEqual(string key, object actual, object expected, SauronException exception, ParamUnidad unidad)
        {
            Context.Resultados.Set(key, actual.ToString(), unidad, expected.ToString());
            AreEqual(actual, expected, exception);
        }

        public static void AreNotEqual(object actual, object expected, SauronException exception)
        {
            if (!AreEqual(actual, expected))
                return;

            exception.Throw();
        }

        private static bool AreEqual(object actual, object expected)
        {
            if (expected == null && actual == null)
                return true;

            if (expected == null || actual == null)
                return false;

            if (expected.Equals(actual))
                return true;

            if (Double.TryParse(expected.ToString(), out double expectedDouble) && Double.TryParse(actual.ToString(), out double actualDouble))
                if (expectedDouble == actualDouble)
                    return true;

            return false;
        }

        public static void AreGreater(double actual, double expected, SauronException exception)
        {
            if (actual <= expected)
                exception.Throw();
        }

        public static void AreGreater(string key, double actual, double min, SauronException exception, ParamUnidad unidad)
        {
            Context.Resultados.Set(key, actual, unidad, min, actual);
            AreGreater(actual, min, exception);
        }

        public static void AreLesser(double actual, double expected, SauronException exception)
        {
            if (actual >= expected)
                exception.Throw();
        }

        public static void AreLesser(string key, double actual, double max, SauronException exception, ParamUnidad unidad)
        {
            Context.Resultados.Set(key, actual, unidad, actual - 1, max);
            AreLesser(actual, max, exception);
        }

        public static void AreBetween(double actual, double min, double max, SauronException exception)
        {
            if (actual < min || actual > max)
                exception.Throw();
        }

        public static void AreBetween(string key, double actual, double min, double max, SauronException exception, ParamUnidad unidad)
        {
            Context.Resultados.Set(key, actual, unidad, min, max);
            AreBetween(actual, min, max, exception);
        }

        public static void IsTrue(bool value, Action exception)
        {
            if (!value)
                exception();
        }

        public static void IsTrue(bool value, SauronException exception)
        {
            if (!value)
                exception.Throw();
        }

        public static void IsTrue(string key, bool value, SauronException exception)
        {
            if (value)
                Context.Resultados.Set(key, "OK", ParamUnidad.SinUnidad);
            if (!value)
            {
                Context.Resultados.Set(key, "FAIL", ParamUnidad.SinUnidad);
                exception.Throw();
            }
        } 

        public static void IsFalse(bool value, SauronException exception)
        {
            if (value)
                exception.Throw();
        }

        public static void IsFalse(string key, bool value, SauronException exception)
        {
            if (!value)
                Context.Resultados.Set(key, "OK", ParamUnidad.SinUnidad);
            if (value)
            {
                Context.Resultados.Set(key, "FAIL", ParamUnidad.SinUnidad);
                exception.Throw();
            }
        }

        public static void IsNotNull(object value, SauronException exception)
        {
            if (value == null)
                exception.Throw();
        }

        public static void IsNotNullOrEmpty(object[] values, SauronException exception)
        {
            if (values == null || values.Length == 0)
                exception.Throw();
        }

        public static void IsNotNullOrEmpty(string text, SauronException exception)
        {
            if (string.IsNullOrEmpty(text))
                exception.Throw();
        }

        public static void IsValidMaxMin(string testPoint, double valor, ParamUnidad unidad, double min, double max, SauronException exception)
        {
            var medida = new AdjustValueDef { Name = testPoint, Value = valor, Max = max, Min = min };

            Context.Resultados.Set(testPoint, medida.Value, unidad, medida.Min, medida.Max);

            Assert.IsTrue(medida.IsValid(), exception);
        }

        public static bool GetIsValidMaxMin(string testPoint, double valor, ParamUnidad unidad, double min, double max, SauronException exception, bool thorwExcpetion = true)
        {
            var medida = new AdjustValueDef { Name = testPoint, Value = valor, Max = max, Min = min };

            Context.Resultados.Set(testPoint, medida.Value, unidad, medida.Min, medida.Max);

            if (thorwExcpetion)
                Assert.IsTrue(medida.IsValid(), exception);

            return medida.IsValid();
        }

    }

    public class AssertFailedException : Exception
    {
        public AssertFailedException()
        {

        }

        public AssertFailedException(string msg)
            : base(msg)
        {
        }

        public AssertFailedException(string msg, Exception exception)
            : base(msg, exception)
        {
        }
    }
}
