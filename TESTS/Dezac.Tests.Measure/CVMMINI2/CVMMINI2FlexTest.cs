﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.Utility;
using System;
using System.Collections.Generic;


namespace Dezac.Tests.Measure
{
    [TestVersion(1.05)]
    public class CVMMINI2FlexTest : CVMMINI2Test
    {
        public override void TestCommunications()
        {
            Tower.IO.DO.On(DISCONNECT_DATAMAN);
            Tower.IO.DO.Off(BLOQUE_GRABACION);
            Tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            SamplerWithCancel((p) =>
            {
                Mini2.FlagTest();
                return true;
            }, "Error de comunicaciones. No se ha podido establecer el modo test en el equipo", 3, 500, 2000, false, false);

            var firmwareVersion = Mini2.ReadFirmwareVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString().Trim(), Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error de versión. La versión de firmware del equipo es incorrecta"), ParamUnidad.SinUnidad);

            var voltageAjust = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentAjust = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(CVMMINI2.Scale.E1.ToString()).Name, 0.02, ParamUnidad.A);
            var powerFactorAjust = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltageAjust, currentAjust, 50, powerFactorAjust, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });  // I+D: Durante todo el test, el equipo le debe circular corriente y voltaje
        }

        public override void TestShortCircuit(int DelFirst = 2, int NumIteraciones = 3, int TimeInterval = 2000)
        {
            // Cruce pistas sin consignas, solo Voltajes ya que Intensidades se cuela ruido y desvirtúa la prueba
            var V_offset = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TestMeasureBase(V_offset,
                (step) =>
                {
                    var Variables = Mini2.ReadAllVariables();
                    return new double[] {
                        Variables.Phases.L1.Voltage, Variables.Phases.L2.Voltage, Variables.Phases.L3.Voltage};
                }, DelFirst, NumIteraciones, TimeInterval);

            var voltage_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 60, ParamUnidad.V);
            var voltage_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 120, ParamUnidad.V);
            var voltage_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 180, ParamUnidad.V);

            var current_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var current_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var current_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var consignas_V = new TriLineValue { L1 = voltage_L1, L2 = voltage_L2, L3 = voltage_L3 };
            var consignas_I = new TriLineValue { L1 = current_L1, L2 = current_L2, L3 = current_L3 };
            var desfase = new TriLineValue { L1 = 0, L2 = 120, L3 = 240 };

            // Cruce pistas con consignas de Voltaje, sin comprobar intensidades
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(consignas_V, 0, 50, 0, desfase, true);
           
            var crucePistas_V = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistas_V.L1.Average = voltage_L1;
            crucePistas_V.L2.Average = voltage_L2;
            crucePistas_V.L3.Average = voltage_L3;

            TestMeasureBase(crucePistas_V,
               (step) =>
               {
                   var Variables = Mini2.ReadAllVariables();
                   return new double[] {
                        Variables.Phases.L1.Voltage, Variables.Phases.L2.Voltage, Variables.Phases.L3.Voltage};
               }, DelFirst, NumIteraciones, TimeInterval);

            // Cruce pistas con consignas de Intensidades y voltajes nulos
            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(0, consignas_I, 50, 0, desfase, true);

            var currentRelation = Configuracion.GetDouble("RELACIÓN_TRANSFORMACIÓN", 1, ParamUnidad.SinUnidad);

            var CrucePistas_I_Voffset = new TriAdjustValueDef(Params.V.L1.EnVacio.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V);
            var crucePistas_I = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A);
            crucePistas_I.L1.Average = current_L1 * currentRelation;
            crucePistas_I.L2.Average = current_L2 * currentRelation;
            crucePistas_I.L3.Average = current_L3 * currentRelation;

            var verif_CrucePistas_I = new List<TriAdjustValueDef>();
            verif_CrucePistas_I.Add(crucePistas_I);
            verif_CrucePistas_I.Add(V_offset);

            TestMeasureBase(verif_CrucePistas_I,
                (step) =>
                {
                    var Variables = Mini2.ReadAllVariables();
                    return new double[] {
                        Variables.Phases.L1.Current, Variables.Phases.L2.Current, Variables.Phases.L3.Current,
                        Variables.Phases.L1.Voltage, Variables.Phases.L2.Voltage, Variables.Phases.L3.Voltage, };
                }, DelFirst, NumIteraciones, TimeInterval);
        }

        public override void TestCalibration()
        {
            Mini2.FlagTest();
           
            CVMMINI2.Scale currentScale = CVMMINI2.Scale.E1;

            var voltageAjust = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentAjust = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(currentScale.ToString()).Name, 0.02, ParamUnidad.A);
            var powerFactorAjust = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            Tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);
            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltageAjust, currentAjust, 50, powerFactorAjust, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var listCalibrationStateError = new Dictionary<CVMMINI2.CalibrationState, CVMMINI2.ErrorVariablesCalibration>();
            listCalibrationStateError.Add(CVMMINI2.CalibrationState.ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR);
            listCalibrationStateError.Add(CVMMINI2.CalibrationState.ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR);  

            var listSumatoriesError = new Dictionary<CVMMINI2.ErrorSumatorio, CVMMINI2.ErrorVariablesCalibration>();
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_MIN, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_MIN);
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_PROMEDIO_INFERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_PROMEDIO_INFERIOR);
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_PROMEDIO_SUPERIOR, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_PROMEDIO_SUPERIOR);
            listSumatoriesError.Add(CVMMINI2.ErrorSumatorio.ERROR_SUMATORIOS_MAX, CVMMINI2.ErrorVariablesCalibration.VARIABLE_ERROR_SUMATORIO_MAX);

            bool calibration = false;
            List<string> ErrorMessageList = new List<string>();
            string ErrorMessageString = string.Empty;
            bool scale1Calibrated = false;

            Mini2.WriteAutoCalibration(true);
          
            try
            {
                SamplerWithCancel((p) =>
                {
                    var state = Mini2.ReadCalibrationState();

                    if (state == CVMMINI2.CalibrationState.CAMBIO_ESCALA_CALIBRACION && !scale1Calibrated)
                    {
                        logger.Debug(string.Format("Realizando cambio de escala : {0}", state.ToString()));

                        ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType.MAX, currentScale);
                        ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType.MIN, currentScale);
                        ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType.AVG, currentScale);

                        scale1Calibrated = true;
                        currentScale = CVMMINI2.Scale.E2;
                        currentAjust = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(currentScale.ToString()).Name, 0.1, ParamUnidad.A);
                        Tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltageAjust, currentAjust, 50, powerFactorAjust, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });                       
                    }

                    else if (state == CVMMINI2.CalibrationState.FINALIZACION_CORRECTA_CALIBRACION)
                    {
                        logger.Debug(string.Format("Estado Calibracion : {0}", state.ToString()));
                        calibration = true;
                    }

                    else
                    {
                        logger.Debug(string.Format("Estado Calibracion : {0}", state.ToString()));
                        ErrorMessageList.Clear();

                        if ((int)state > 109 && (int)state < 126)
                            foreach (var errorSum in listSumatoriesError)
                                if (((CVMMINI2.ErrorSumatorio)state - 110).HasFlag(errorSum.Key))
                                {
                                    var errorsVars = Mini2.ReadParametersErrors(errorSum.Value);
                                    foreach (CVMMINI2.ErrorParametersCalibration errorVar in Enum.GetValues(typeof(CVMMINI2.ErrorParametersCalibration)))
                                        if (errorsVars.HasFlag(errorVar))
                                            ErrorMessageList.Add(string.Format("{0} en {1}", errorSum.Key.ToString().Replace("_", " "), errorVar.ToString().Replace("_", " ")));
                                }

                        if ((int)state > 149 && (int)state < 166)
                            foreach (var errorSum in listSumatoriesError)
                                if (((CVMMINI2.ErrorSumatorio)state - 150).HasFlag(errorSum.Key))
                                {
                                    var errorsVars = Mini2.ReadParametersErrors(errorSum.Value);
                                    foreach (CVMMINI2.ErrorParametersCalibration errorVar in Enum.GetValues(typeof(CVMMINI2.ErrorParametersCalibration)))
                                        if (errorsVars.HasFlag(errorVar))
                                            ErrorMessageList.Add(string.Format("{0} en {1}", errorSum.Key.ToString().Replace("_", " ") + " ESCALA 2", errorVar.ToString().Replace("_", " ")));
                                }

                        foreach (var errorState in listCalibrationStateError)
                            if (state.HasFlag(errorState.Key))
                                if (!state.HasFlag(CVMMINI2.CalibrationState.FASE_CALIBRACION_1) && !state.HasFlag(CVMMINI2.CalibrationState.FASE_CALIBRACION_2) && !state.HasFlag(CVMMINI2.CalibrationState.FASE_CALIBRACION_3))
                                {
                                    var errors = Mini2.ReadParametersErrors(errorState.Value);
                                    ErrorMessageList.Add(string.Format("{0} en {1}", errorState.Key.ToString().Replace("_", " "), errors.ToString()));
                                }
                    }

                    if (calibration)
                        return true;
                    else
                        return false;
                }, string.Empty, 60, 500);
            }
            finally
            {
                Tower.FLUKE.ApplyOffAndWaitStabilisation();

                ReadGainsAndSetToResult();
                ReadGainsEscale2AndSetToResult();
                ReadErrorsAndSetToResult();

                ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType.MAX, currentScale);
                ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType.MIN, currentScale);
                ReadSumatoriesAndSetToResult(CVMMINI2.SumatoriesType.AVG, currentScale);


                if (!calibration && ErrorMessageList.Count > 0)
                {
                    foreach (var errorMsg in ErrorMessageList)
                        ErrorMessageString += errorMsg + "\r\n";
                    Error().UUT.CALIBRACION.MARGENES(ErrorMessageString).Throw();
                }
            }

        }

        public void TestVerificationByScale(CVMMINI2.Scale scale, int initCount = 2, int samples = 3, int timeInterval = 1100)
        {
            Mini2.FlagTest();

            var voltageRef = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var currentRef = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint(scale.ToString()).Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 45, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltageRef, currentRef, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();
            var toleranciaKvar = Params.KVAR.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.KVAR.L1.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, toleranciaKvar, ParamUnidad.Kvar));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, toleranciaI, ParamUnidad.A));

            TestCalibracionBase(defs,
            () =>
            {
                var vars = this.Mini2.ReadAllVariables().Phases;

                return new double[] {
                    vars.L1.activePower,  vars.L2.activePower, vars.L3.activePower,
                    vars.L1.inductiveReactivePower, vars.L2.inductiveReactivePower, vars.L3.inductiveReactivePower,
                    vars.L1.Voltage, vars.L2.Voltage ,vars.L3.Voltage,
                    vars.L1.Current , vars.L2.Current , vars.L3.Current };  // factor de potencias, cos (), THDs (<5), trifásicos
            },
            () =>
            {
                var currentRelation = Configuracion.GetDouble("RELACIÓN_TRANSFORMACIÓN", 1, ParamUnidad.SinUnidad);

                var ReadMTEVoltage = Tower.PowerSourceIII.ReadVoltage();
                var ReadMTECurrent = Tower.PowerSourceIII.ReadCurrent() * currentRelation;

                var power = TriLinePower.Create(ReadMTEVoltage, ReadMTECurrent, powerFactor - 90);

                var varsList = new List<double>() {
                        power.KW.L1, power.KW.L2, power.KW.L3,
                        power.KVAR.L1, power.KVAR.L2, power.KVAR.L3,
                        ReadMTEVoltage.L1, ReadMTEVoltage.L2, ReadMTEVoltage.L3,
                        ReadMTECurrent.L1, ReadMTECurrent.L2, ReadMTECurrent.L3};

                return varsList.ToArray();

            }, initCount, samples, timeInterval);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, 0, Params.FREQ.Null.Verificacion.Tol(), ParamUnidad.Hz);
            TestCalibracionBase(defsFreq,
            () =>
            {
                var frecuency = this.Mini2.ReadAllVariables().ThreePhase.Frequency;
                return frecuency;
            },
            () =>
            {
                var ReadPaternFreq = this.Tower.CvmB100Low.ReadVariablesTrifasicas().Frecuencia;
                return ReadPaternFreq;

            }, initCount, samples, timeInterval);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        private void ReadGainsEscale2AndSetToResult()
        {
            var gainsI = Mini2.ReadCurrentGainsE2();

            Resultado.Set("GANANCIA_I1_E2", gainsI.L1, ParamUnidad.SinUnidad);
            Resultado.Set("GANANCIA_I2_E2", gainsI.L2, ParamUnidad.SinUnidad);
            Resultado.Set("GANANCIA_I3_E2", gainsI.L3, ParamUnidad.SinUnidad);

            var gainsPower = Mini2.ReadPowerGainsE2();

            Resultado.Set("GANANCIA_kW1_E2", gainsPower.L1, ParamUnidad.SinUnidad);
            Resultado.Set("GANANCIA_kW2_E2", gainsPower.L2, ParamUnidad.SinUnidad);
            Resultado.Set("GANANCIA_kW3_E2", gainsPower.L3, ParamUnidad.SinUnidad);
        }
    }
}
