﻿using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Measure.Placas
{
    public class QNA500CPUTestPlacas : TestBase
    {
        QNA500 qna;
        TowerBoard tower;

        private byte OUTPUT_PILOTO_ERROR = 43;

        public void TestInitialization(byte port = 7)
        {
            qna = AddInstanceVar(new QNA500(port), "UUT");
            qna.Modbus.PerifericNumber = 2;
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", qna.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", qna.GetType().Name), ParamUnidad.SinUnidad);


            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();

            tower.IO.DO.Off(OUTPUT_PILOTO_ERROR);
        }

        public void TestMAC()
        {
            //TestInfo.NumMAC
            qna.WriteMAC();
        }

        public void TestDCHP()
        {
            qna.WriteFlagDHCP();

            var IP = qna.ResetAndReadIP();

            Assert.AreEqual(IP, Configuracion.GetString("IP", "00.00.00.00"), Error().SOFTWARE.IP.VERSION_INCORRECTA("Error, la IP del dispositivo no coincide"));
        }

        public void TestVoltageMeasurements()
        {
            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef(Params.V.L1.Verificacion.Name, 0, Params.V.L1.Verificacion.Min(), Params.V.L1.Verificacion.Max(), 0, 0, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.L2.Verificacion.Name, 0, Params.V.L2.Verificacion.Min(), Params.V.L2.Verificacion.Max(), 0, 0, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.L3.Verificacion.Name, 0, Params.V.L3.Verificacion.Min(), Params.V.L3.Verificacion.Max(), 0, 0, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.LN.Verificacion.Name, 0, Params.V.LN.Verificacion.Min(), Params.V.LN.Verificacion.Max(), 0, 0, ParamUnidad.V));

            TestMeasureBase(defs,
                (step) =>
                {
                    var vars = qna.ReadVoltages();
                    return new double[] { vars.L1, vars.L2, vars.L3, vars.LN };

                }, 5, 15, 500);
        }

        public void TestCurrentMeasurements()
        {
            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef(Params.I.L1.Verificacion.Name, 0, Params.I.L1.Verificacion.Min(), Params.I.L1.Verificacion.Max(), 0,0, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.I.L2.Verificacion.Name, 0, Params.I.L2.Verificacion.Min(), Params.I.L2.Verificacion.Max(), 0,0, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.I.L3.Verificacion.Name, 0, Params.I.L3.Verificacion.Min(), Params.I.L3.Verificacion.Max(), 0,0, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.I.LN.Verificacion.Name, 0, Params.I.LN.Verificacion.Min(), Params.I.LN.Verificacion.Max(), 0,0, ParamUnidad.A));

            TestMeasureBase(defs,
                (step) =>
                {
                    var vars = qna.ReadVoltages();
                    return new double[] { vars.L1, vars.L2, vars.L3, vars.LN };

                }, 5, 15, 500);
        }

        public void TestClock()
        {
            qna.WriteFlagTest();

            var clock = new QNA500.QNADateTime();
            var dateTime = DateTime.Now;

            clock.Second = (ushort)dateTime.Second;
            clock.Minute = (ushort)dateTime.Minute;
            clock.Hour = (ushort)dateTime.Hour;
            clock.Day = (ushort)dateTime.Day;
            clock.Month = (ushort)dateTime.Month;
            clock.Year = (ushort)dateTime.Year;

            qna.WriteDateTime(clock);

            Delay(3000, "Esperando grabación de la fecha");

            var qnaReading = qna.ReadDateTime();

            Assert.AreBetween(qnaReading.Second, clock.Second, clock.Second + 10, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Las segundos no coinciden"));
            Assert.AreBetween(qnaReading.Minute, clock.Minute, clock.Minute + 1, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Las minutos no coinciden"));
            Assert.AreBetween(qnaReading.Hour, clock.Hour, clock.Hour + 1, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Las horas no coinciden"));
            Assert.AreBetween(qnaReading.Day, clock.Day, clock.Day + 1, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Las dias no coinciden"));
            Assert.AreBetween(qnaReading.Month, clock.Month, clock.Month + 1, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Las meses no coinciden"));
            Assert.AreBetween(qnaReading.Year, clock.Year, clock.Year + 1, Error().UUT.CONFIGURACION.NO_COINCIDE("Error. Las años no coinciden"));
        }

        public void TestFrameNumber()
        {
            qna.WriteFrameNumber((int)TestInfo.NumBastidor);
        }

        public void TestSynchronism(bool valueToCheck)
        {
            var signal = qna.ReadSynchronismSignal();

            Assert.AreEqual(signal, valueToCheck ? 0x0001 : 0x0000, Error().UUT.CONFIGURACION.GRABACION_SINCRONISMO("Error en el estado " + (valueToCheck ? "ON" : "OFF") + " del sincronismo"));
        }

        public void TestPowerSignal(bool valueToCheck)
        {
            var signal = qna.ReadPowerOnSignal();

            Assert.AreEqual(signal, valueToCheck ? 0x0001 : 0x0000, Error().UUT.CONFIGURACION.GRABACION_SINCRONISMO("Error en el estado " + (valueToCheck ? "ON" : "OFF") + " del sincronismo"));
        }

        public void TestLeds()
        {
            var leds = new Dictionary<string, QNA500.Registers.Leds>();

            leds.Add("led de POWER", QNA500.Registers.Leds.LED_1);
            leds.Add("led de Comunicaciones 1", QNA500.Registers.Leds.LED_2);
            leds.Add("led de Comunicaciones 2", QNA500.Registers.Leds.LED_3);
            leds.Add("led de Comunicaciones 3", QNA500.Registers.Leds.LED_4);
            
            foreach(KeyValuePair<string, QNA500.Registers.Leds> led in leds)
            {
                qna.WriteLed(led.Value, true);

                // Leer test point correspondiente a true

                qna.WriteLed(led.Value, false);

                // Leer test point correspondiente a false
            }
        }

        public void TestExternalRam()
        {
            qna.ReadExternalRam();
        }

        public void TestSD()
        {
            qna.WriteGenerateProvaFile();

            var result = qna.ReadProvaFileGeneration();

            Assert.AreEqual(result, 0, Error().UUT.CONFIGURACION.NO_GRABADO("Error. El fichero de prueba no se ha generado correctamente"));

            qna.WriteClearProvaFile();

            result = qna.ReadProvaFileDeletion();

            Assert.AreEqual(result, 0, Error().UUT.CONFIGURACION.NO_GRABADO("Error. El fichero de prueba no se ha borrado correctamente"));
        }

        public void TestEEPROM()
        {
            var frameNumber = qna.ReadFrameNumber();

            Assert.AreEqual(frameNumber, (int)TestInfo.NumBastidor, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. El numero de bastidor grabado no coincide."));
        }

        public void TestEthernet()
        {
            var IP = qna.ReadIP();
        }

        public void TestEnd()
        {
            if (qna != null)
                qna.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(14, 17, 18, 19);

                tower.Dispose();
            }
        }
    }
}
