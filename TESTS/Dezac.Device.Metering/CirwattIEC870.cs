﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using METERS;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.01)]
    public class CirwattIEC870 : DeviceBase
    {
        private CIRWATT_IEC870 sbt;

        public void SetPort(string PortCom, int Retries, ushort direccionEnlace, string claveAcceso)
        {
            LogMethod();

            if (sbt == null)
                sbt = new CIRWATT_IEC870();

            if (!PortCom.StartsWith("COM"))
                PortCom = "COM" + PortCom;

            sbt.ActAdressLink = direccionEnlace;
            sbt.AcrKeyWritte = claveAcceso;
            sbt.PuertoCom = PortCom;
            sbt.Retries = Retries;
        }

        public void IniciarSesion()
        {
            LogMethod();

            if (!sbt.IniciarSesion())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("IniciarSesion").Throw();
        }

        public string LeerVersionSW()
        {
            LogMethod();

            if (!sbt.Leer_VersionSW())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("LeerVersionSW").Throw();

            return sbt.VersionSoftware;
        }

        public string Leer_Modelo()
        {
            LogMethod();

            if (!sbt.Leer_Modelo())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Leer_Modelo").Throw();

            return sbt.ModeloContador;
        }

        public string Leer_Entradas()
        {
            LogMethod();

            if (!sbt.Leer_Entradas())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Leer_Entradas").Throw();

            return sbt.Inputs.ToString();
        }

        public TriLineValue Leer_Tension()
        {
            LogMethod();

            if (!sbt.Leer_Tension())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Leer_Tension").Throw();

            var tension = new TriLineValue() { L1 = sbt.Tension[0], L2 = sbt.Tension[1], L3 = sbt.Tension[2] };
            return tension;
        }

        public TriLineValue Leer_Corriente()
        {
            LogMethod();

            if (!sbt.Leer_Corriente())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Leer_Corriente").Throw();

            var corriente = new TriLineValue() { L1 = sbt.Corriente[0], L2 = sbt.Corriente[1], L3 = sbt.Corriente[2] };
            return corriente;
        }

        public TriLineValue Leer_PotenciaActiva()
        {
            LogMethod();

            if (!sbt.Leer_Potencia())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Leer_PotenciaActiva").Throw();

            var potAct = new TriLineValue() { L1 = sbt.PotenciaActiva[0], L2 = sbt.PotenciaActiva[1], L3 = sbt.PotenciaActiva[2] };
            return potAct;
        }

        public TriLineValue Leer_PotenciaReactiva()
        {
            LogMethod();

            if (!sbt.Leer_Potencia_Rectiva())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Leer_PotenciaReactiva").Throw();

            var potAct = new TriLineValue() { L1 = sbt.PotenciaReactiva[0], L2 = sbt.PotenciaReactiva[1], L3 = sbt.PotenciaReactiva[2] };
            return potAct;
        }

        public void EscribirModelo(string modelo)
        {
            LogMethod();

            sbt.ModeloContador = modelo;

            if (!sbt.Escribir_Modelo())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("EscribirModelo").Throw();
        }

        public void Escribir_OffsetsActiva_Zero()
        {
            LogMethod();

            if (!sbt.Escribir_OffsetsActiva_Zero())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Escribir_OffsetsActiva_Zero").Throw();
        }

        public void Escribir_TestDisplay(byte testDisplay)
        {
            LogMethod();

            sbt.Test_Display = testDisplay;

            if (!sbt.Escribir_TestDisplay())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Escribir_TestDisplay").Throw();
        }

        public void Escribir_Activar_LedActReact(byte estado)
        {
            LogMethod();

            if (!sbt.Escribir_Activar_LedActReact(estado))
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Escribir_Activar_LedActReact").Throw();
        }

        public void CerrarSesion()
        {
            LogMethod();
            sbt.CloseSesion();
        }

        public override void Dispose()
        {
            if (sbt != null)
            {
                sbt.CloseSesion();
                sbt = null;
            }
        }
    }
}
