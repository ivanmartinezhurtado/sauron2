﻿using System.Collections.Generic;

namespace AssemblyResolve
{
    public class ProjectItem
    {
        public string Name { get; set; }
        public string SourcePath { get; set; }
        public string TargetPath { get; set; }
        public FileAssemblyItem ApplicationFile { get; set; }
        public string CurrentVersion { get; set; }
        public string NewVersion { get; set; }
        public List<FileAssemblyItem> LoadAssemblies { get; set; }
    }
}
