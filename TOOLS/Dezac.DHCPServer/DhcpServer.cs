using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace WinDHCP.Library
{
    public class DhcpServer
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(DhcpServer));

        private const Int32 DhcpPort = 67;
        private const Int32 DhcpClientPort = 68;
        private const Int32 DhcpMessageMaxSize = 1024;

        public event EventHandlerString EventLogg;
        public delegate void EventHandlerString(object sender, MyEventArgs args);

        private NetworkInterface DhcpInterface;
        private InternetAddress StartAddress = new InternetAddress(192, 168, 1, 100);
        private InternetAddress EndAddress = new InternetAddress(192, 168, 1, 150);
        private InternetAddress Subnet = new InternetAddress(255, 255, 255, 255);
        private InternetAddress Gateway = new InternetAddress(192, 168, 1, 1);

        private TimeSpan OfferTimeout = TimeSpan.FromSeconds(60);
        private TimeSpan LeaseDuration = TimeSpan.FromDays(1);
        private IPAddress DhcpInterfaceAddress;

        private Dictionary<InternetAddress, AddressLease> m_ActiveLeases = new Dictionary<InternetAddress, AddressLease>();
        private SortedList<InternetAddress, AddressLease> m_InactiveLeases = new SortedList<InternetAddress, AddressLease>();
        private Dictionary<PhysicalAddress, InternetAddress> m_Reservations = new Dictionary<PhysicalAddress, InternetAddress>();

        private SortedList<PhysicalAddress, Boolean> m_Acl = new SortedList<PhysicalAddress, Boolean>();
        private Boolean m_AllowAny = true;

        private Object m_LeaseSync = new Object();
        private ReaderWriterLock m_AclLock = new ReaderWriterLock();
        private ReaderWriterLock m_AbortLock = new ReaderWriterLock();
        private Socket m_DhcpSocket;
        private Boolean m_Abort = false;

        private Timer m_CleanupTimer;

        public Dictionary<InternetAddress, AddressLease> ClientsActive
        {
            get { return m_ActiveLeases; }
        }

        public SortedList<InternetAddress, AddressLease> ClientsInactive
        {
            get { return m_InactiveLeases; }
        } 

        public Boolean AllowAny
        {
            get { return m_AllowAny; }
            set { m_AllowAny = value; }
        }

        public Dictionary<PhysicalAddress, InternetAddress> Reservations
        {
            get { return m_Reservations; }
        }

        public DhcpServer()
        {
           
        }

        public void ConfigIP(string startIpAddress, string subnetMask, string gateway = "")
        {
            var endAddressTmp = startIpAddress.Split('.');
            endAddressTmp.SetValue("254", 3);
            var endAddress = string.Join(".", endAddressTmp);
            ConfigIP(startIpAddress, endAddress, subnetMask, gateway);
        }

        public void ConfigIP(string startIpAddress, string endIpAddress, string subnetMask, string gateway)
        {
            StartAddress = InternetAddress.Parse(startIpAddress);
            EndAddress = InternetAddress.Parse(endIpAddress);
            Subnet = InternetAddress.Parse(subnetMask);
            Gateway = gateway != null ?  InternetAddress.Parse(gateway) : null;
        }

        public void AddAcl(PhysicalAddress address, Boolean deny)
        {
            m_AclLock.AcquireWriterLock(-1);

            try
            {
                if (m_Acl.ContainsKey(address))
                    m_Acl[address] = !deny;
                else
                    m_Acl.Add(address, !deny);
            }
            finally
            {
                m_AclLock.ReleaseLock();
            }
        }

        public void RemoveAcl(PhysicalAddress address)
        {
            m_AclLock.AcquireWriterLock(-1);

            try
            {
                if (m_Acl.ContainsKey(address))
                    m_Acl.Remove(address);
            }
            finally
            {
                m_AclLock.ReleaseLock();
            }
        }

        public void ClearAcls()
        {
            m_AclLock.AcquireWriterLock(-1);

            try
            {
                m_Acl.Clear();
            }
            finally
            {
                m_AclLock.ReleaseLock();
            }
        }

        public void Start()
        {
            Logger("Starting Dhcp Server...");

            m_ActiveLeases.Clear();
            m_InactiveLeases.Clear();

            for (InternetAddress address = StartAddress.Copy(); address.CompareTo(EndAddress) <= 0; address = address.NextAddress())
                m_InactiveLeases.Add(address, new AddressLease(null, address, DateTime.MinValue));

            DhcpInterface = NetUtils.GetNetworkInterfaceTest();
            if (DhcpInterface == null)
                TestException.Create().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET("NO SE HA PODIDO CONFIGURAR EL ADAPTADOR DE RED").Throw();

            foreach (UnicastIPAddressInformation interfaceAddress in DhcpInterface.GetIPProperties().UnicastAddresses)
                if (interfaceAddress.Address.AddressFamily == AddressFamily.InterNetwork)
                    DhcpInterfaceAddress = interfaceAddress.Address;

            if (DhcpInterfaceAddress == null)
                TestException.Create().HARDWARE.PC.ADAPTADOR_DE_RED_ETHERNET("NO SE HA PODIDO ENCONTRAR UNA IP VALIDA PARA EL ADAPTADOR DE RED").Throw();

            var props = DhcpInterface.GetIPProperties();
            var physicalAdress = new PhysicalAddress(DhcpInterface.GetPhysicalAddress().GetAddressBytes());
            var ipHost = new InternetAddress(DhcpInterfaceAddress.GetAddressBytes());

            var IpHost = new AddressLease(physicalAdress, ipHost, DateTime.Now.Add(LeaseDuration), Environment.MachineName);
            IpHost.Acknowledged = true;
            Logger("--------------------------------------------------------");
            Logger(" PC -> HOST NAME: {0}", IpHost.HostName);
            Logger(" PC -> MAC: {0}  IP: {1}", IpHost.Owner.MAC, IpHost.Address);
            Logger("--------------------------------------------------------");

            if (Gateway == null)
                Gateway = ipHost;

            m_InactiveLeases.Remove(IpHost.Address);
            m_ActiveLeases.Add(IpHost.Address, IpHost);

            m_Abort = false;
            m_CleanupTimer = new Timer(new TimerCallback(CleanUp), null, 60000, 30000);
            m_DhcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            m_DhcpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
            m_DhcpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            m_DhcpSocket.Bind(new IPEndPoint(DhcpInterfaceAddress, DhcpPort));

            Listen();
            Logger("Dhcp Service Started.");
        }

        public void Stop()
        {
            Logger("Closing Dhcp Server ...");
            m_AbortLock.AcquireWriterLock(-1);
            try
            {
                m_Abort = true;

                if(m_CleanupTimer != null)
                    m_CleanupTimer.Dispose();

                if (m_DhcpSocket != null)
                {
                    m_DhcpSocket.Close();
                    m_DhcpSocket = null;
                }
            }
            finally
            {
                m_AbortLock.ReleaseLock();
                Logger("Dhcp Server Closed ...");
            }
        }

        private void Listen()
        {
            Byte[] messageBufer = new Byte[DhcpMessageMaxSize];
            EndPoint source = new IPEndPoint(0, 0);

            m_AbortLock.AcquireReaderLock(-1);

            try
            {
                if (m_Abort)
                    return;
                
                Logger("Listening For Dhcp Request.");
                m_DhcpSocket.BeginReceiveFrom(messageBufer, 0, DhcpMessageMaxSize, SocketFlags.None, ref source, new AsyncCallback(OnReceive), messageBufer);
            }
            finally
            {
                m_AbortLock.ReleaseLock();
            }
        }

        private void CleanUp(Object state)
        {
            lock (m_LeaseSync)
            {
                List<AddressLease> toRemove = new List<AddressLease>();

                foreach (AddressLease lease in m_ActiveLeases.Values)
                    if (!lease.Acknowledged && lease.Expiration > DateTime.Now.Add(OfferTimeout) ||
                        lease.Acknowledged && lease.Expiration < DateTime.Now)
                        toRemove.Add(lease);

                foreach (AddressLease lease in toRemove)
                {
                    m_ActiveLeases.Remove(lease.Address);
                    lease.Acknowledged = false;
                    m_InactiveLeases.Add(lease.Address, lease);
                }
            }
        }

        private void OnReceive(IAsyncResult result)
        {
            DhcpData data = new DhcpData((Byte[])result.AsyncState);
            data.Result = result;

            if (!m_Abort)
            {
                Logger("OnReceive -> Dhcp Messages, Processing....");
                // Queue this request for processing
                ThreadPool.QueueUserWorkItem(new WaitCallback(CompleteRequest), data);

                Listen();
            }
        }

        private void CompleteRequest(Object state)
        {
            DhcpData messageData = (DhcpData)state;
            EndPoint source = new IPEndPoint(0, 0);

            m_AbortLock.AcquireReaderLock(1);

            try
            {
                if (m_Abort)
                    return;

                messageData.BufferSize = m_DhcpSocket.EndReceiveFrom(messageData.Result, ref source);
                messageData.Source = (IPEndPoint)source;
            }
            finally
            {
                m_AbortLock.ReleaseLock();
            }

            DhcpMessage message;

            try
            {
                message = new DhcpMessage(messageData);
            }
            catch (ArgumentException ex)
            {
                TraceException("Error Parsing Dhcp Message", ex);
                return;
            }
            catch (InvalidCastException ex)
            {
                TraceException("Error Parsing Dhcp Message", ex);
                return;
            }
            catch (IndexOutOfRangeException ex)
            {
                TraceException("Error Parsing Dhcp Message", ex);
                return;
            }
            catch (Exception ex)
            {
                TraceException("Error Parsing Dhcp Message", ex);
                throw;
            }

            if (message.Operation == DhcpOperation.BootRequest)
            {
                Byte[] messageTypeData = message.GetOptionData(DhcpOption.DhcpMessageType);

                if (messageTypeData != null && messageTypeData.Length == 1)
                {
                    DhcpMessageType messageType = (DhcpMessageType)messageTypeData[0];

                    switch (messageType)
                    {
                        case DhcpMessageType.Discover:
                            try
                            {
                                DhcpDiscover(message);
                            }catch(Exception ex)
                            {
                                TraceException("DhcpDiscover" ,ex);
                            }
                            break;
                        case DhcpMessageType.Request:
                            DhcpRequest(message);
                            break;
                        default:
                            Logger("Unknown Dhcp Message ({0}) Received, Ignoring.", messageType.ToString());
                            break;
                    }
                }
                else
                    Logger("Unknown Dhcp Data Received, Ignoring.");        
            }
        }

        private void TraceException(String prefix, Exception ex)
        {
            Logger("EXCEPTION -> {0}: ({1}) - {2}\r\n{3}", prefix, ex.GetType().Name, ex.Message, ex.StackTrace);

            if (ex.InnerException != null)
                TraceException("    Inner Exception", ex.InnerException);
        }

        private void DhcpDiscover(DhcpMessage message)
        {
            Byte[] addressRequestData = message.GetOptionData(DhcpOption.AddressRequest);
            if (addressRequestData == null)
                addressRequestData = message.ClientAddress;
            
            InternetAddress addressRequest = new InternetAddress(addressRequestData);
            string ClientName = "";

            // Assume we're on an ethernet network
            Byte[] hardwareAddressData = new Byte[6];
            Array.Copy(message.ClientHardwareAddress, hardwareAddressData, 6);
            PhysicalAddress clientHardwareAddress = new PhysicalAddress(hardwareAddressData);

            AddressLease offer = null;

            var client = message.GetOptionData(DhcpOption.ClientId);
            var hostName = message.GetOptionData(DhcpOption.Hostname);
            if (hostName != null)
            {
                var name = ASCIIEncoding.ASCII.GetString(hostName);
                ClientName = new string(name.ToCharArray().Where(c => Char.IsLetterOrDigit(c)).ToArray());
            }

            // If this client is explicitly allowed, or they are not denied and the allow any flag is set
            if (m_Acl.ContainsKey(clientHardwareAddress) && m_Acl[clientHardwareAddress] ||
                !m_Acl.ContainsKey(clientHardwareAddress) && m_AllowAny)
            {
                if (m_Reservations.ContainsKey(clientHardwareAddress))
                {
                    offer = new AddressLease(clientHardwareAddress, m_Reservations[clientHardwareAddress], DateTime.Now.Add(LeaseDuration), ClientName);                    
                    Logger("DHCP DISCOVERED EVENT -> Reservation -- MAC:{0}  IP:{1}", offer.Owner.MAC , offer.Address);
                }
                else
                    lock (m_LeaseSync)
                    {
                        foreach (var activeClient in m_ActiveLeases)
                            if (activeClient.Value.Owner.Equals(clientHardwareAddress))
                            {
                                offer = activeClient.Value;
                                Logger("DHCP DISCOVERED EVENT -> ActiveClient Exist By HardwareAddres -- MAC:{0}  IP:{1}", offer.Owner.MAC, offer.Address);
                                break;
                            }

                        if (offer == null)
                        {
                            if (!addressRequest.Equals(InternetAddress.Empty))
                            {
                                if (m_InactiveLeases.ContainsKey(addressRequest))
                                {
                                    offer = m_InactiveLeases[addressRequest];
                                    Logger("DHCP DISCOVERED EVENT -> InactiveLeases Exist -- MAC:{0}  IP:{1}", clientHardwareAddress.MAC, offer.Address);
                                    m_InactiveLeases.Remove(addressRequest);
                                    m_ActiveLeases.Add(addressRequest, offer);
                                }
                                else if (m_ActiveLeases.ContainsKey(addressRequest) && m_ActiveLeases[addressRequest].Owner.Equals(clientHardwareAddress))
                                {
                                    offer = m_ActiveLeases[addressRequest];
                                    Logger("DHCP DISCOVERED EVENT -> ActiveLeases Exist -- MAC:{0}  IP:{1}", clientHardwareAddress.MAC, offer.Address);
                                }
                            }
                            else if (m_InactiveLeases.Count > 0)
                            {
                                offer = m_InactiveLeases.Values[0];
                                Logger("DHCP DISCOVERED EVENT -> InactiveLeases First -- IP:{0}", offer.Address);
                                m_InactiveLeases.Remove(offer.Address);
                                m_ActiveLeases.Add(offer.Address, offer);
                            }
                        }
                    }
            }

            if (offer == null)
                SendNak(message);
            else
            {
                offer.Acknowledged = false;
                offer.Expiration = DateTime.Now.Add(OfferTimeout);
                offer.SessionId = message.SessionId;
                offer.Owner = clientHardwareAddress;
                offer.HostName = ClientName;
                SendOffer(message, offer);
            }
        }

        private void DhcpRequest(DhcpMessage message)
        {
            Byte[] addressRequestData = message.GetOptionData(DhcpOption.AddressRequest);
            if (addressRequestData == null)
                addressRequestData = message.ClientAddress;

            InternetAddress addressRequest = new InternetAddress(addressRequestData);

            if (addressRequest.IsEmpty)
            {
                SendNak(message);
                return;
            }

            // Assume we're on an ethernet network
            Byte[] hardwareAddressData = new Byte[6];
            Array.Copy(message.ClientHardwareAddress, hardwareAddressData, 6);
            PhysicalAddress clientHardwareAddress = new PhysicalAddress(hardwareAddressData);

            AddressLease assignment = null;
            Boolean ack = false;

            // If this client is explicitly allowed, or they are not denied and the allow any flag is set
            if (m_Acl.ContainsKey(clientHardwareAddress) && m_Acl[clientHardwareAddress] ||
                !m_Acl.ContainsKey(clientHardwareAddress) && m_AllowAny)
            {
                if (m_Reservations.ContainsKey(clientHardwareAddress))
                {
                    assignment = new AddressLease(clientHardwareAddress, m_Reservations[clientHardwareAddress], DateTime.Now.Add(LeaseDuration));
                    if (addressRequest.Equals(assignment.Address))
                    {
                        Logger("Reservations DHCP REQUEST EVENT ->  MAC:{0}  IP:{1}", assignment.Owner.MAC, assignment.Address.ToString());
                        ack = true;
                    }
                }
                else
                    lock (m_LeaseSync)
                    {
                        if (m_ActiveLeases.ContainsKey(addressRequest) &&
                            (m_ActiveLeases[addressRequest].Owner.Equals(clientHardwareAddress) || m_ActiveLeases[addressRequest].SessionId == message.SessionId))
                        {
                            assignment = m_ActiveLeases[addressRequest];
                            assignment.Acknowledged = true;
                            assignment.Owner = clientHardwareAddress;
                            assignment.Expiration = DateTime.Now.Add(LeaseDuration);
                            ack = true;
                            Logger("ActiveLeases DHCP REQUEST EVENT ->  MAC:{0}  IP:{1}", assignment.Owner.MAC, assignment.Address.ToString());
                        }
                    }
            }

            if (ack)
                SendAck(message, assignment);
            else
                SendNak(message);
        }

        private void SendOffer(DhcpMessage message, AddressLease offer)
        {
            Logger("{0} Sending Dhcp Offer.", Thread.CurrentThread.ManagedThreadId);

            DhcpMessage response = new DhcpMessage();
            response.Operation = DhcpOperation.BootReply;
            response.Hardware = HardwareType.Ethernet;
            response.HardwareAddressLength = 6;
            response.SecondsElapsed = message.SecondsElapsed;
            response.SessionId = message.SessionId;
            response.Flags = message.Flags;

            response.AssignedAddress = offer.Address.ToArray();
            response.ClientHardwareAddress = message.ClientHardwareAddress;

            response.AddOption(DhcpOption.DhcpMessageType, (Byte)DhcpMessageType.Offer);
            response.AddOption(DhcpOption.AddressRequest, offer.Address.ToArray());
            AddDhcpOptions(response);

            Byte[] paramList = message.GetOptionData(DhcpOption.ParameterList);
            if (paramList != null)
                response.OptionOrdering = paramList;

            SendReply(response);
            Logger("{0} Dhcp Offer Sent.", Thread.CurrentThread.ManagedThreadId);
        }

        private void SendAck(DhcpMessage message, AddressLease lease)
        {
            Logger("{0} Sending Dhcp Acknowledge.", Thread.CurrentThread.ManagedThreadId);

            DhcpMessage response = new DhcpMessage();
            response.Operation = DhcpOperation.BootReply;
            response.Hardware = HardwareType.Ethernet;
            response.HardwareAddressLength = 6;
            response.SecondsElapsed = message.SecondsElapsed;
            response.SessionId = message.SessionId;

            response.AssignedAddress = lease.Address.ToArray();
            response.ClientHardwareAddress = message.ClientHardwareAddress;

            response.AddOption(DhcpOption.DhcpMessageType, (Byte)DhcpMessageType.Ack);
            response.AddOption(DhcpOption.AddressRequest, lease.Address.ToArray());
            AddDhcpOptions(response);

            SendReply(response);
            Logger("{0} Dhcp Acknowledge Sent.", Thread.CurrentThread.ManagedThreadId);
        }

        private void AddDhcpOptions(DhcpMessage response)
        {
            response.AddOption(DhcpOption.AddressTime, DhcpMessage.ReverseByteOrder(BitConverter.GetBytes((Int32)LeaseDuration.TotalSeconds)));
            response.AddOption(DhcpOption.Router, Gateway.ToArray());
            response.AddOption(DhcpOption.SubnetMask, Subnet.ToArray());
        }

        private void SendNak(DhcpMessage message)
        {
            Logger("{0} Sending Dhcp Negative Acknowledge.", Thread.CurrentThread.ManagedThreadId);

            DhcpMessage response = new DhcpMessage();
            response.Operation = DhcpOperation.BootReply;
            response.Hardware = HardwareType.Ethernet;
            response.HardwareAddressLength = 6;
            response.SecondsElapsed = message.SecondsElapsed;
            response.SessionId = message.SessionId;

            response.ClientHardwareAddress = message.ClientHardwareAddress;

            response.AddOption(DhcpOption.DhcpMessageType, (Byte)DhcpMessageType.Nak);

            SendReply(response);
            Logger("{0} Dhcp Negative Acknowledge Sent.", Thread.CurrentThread.ManagedThreadId);
        }

        private void SendReply(DhcpMessage response)
        {
            response.AddOption(DhcpOption.DhcpAddress, DhcpInterfaceAddress.GetAddressBytes());

            Byte[] sessionId = BitConverter.GetBytes(response.SessionId);

            try
            {
                m_DhcpSocket.SendTo(response.ToArray(), new IPEndPoint(IPAddress.Broadcast, DhcpClientPort));
            }
            catch (Exception ex)
            {
                TraceException("Error Sending Dhcp Reply", ex);
                //throw;
            }
        }


        //**************************************************

        private void Logger(string format, object logg = null)
        {
            logger.InfoFormat(format, logg);
            var loggerText = string.Format(format, logg);

            if (EventLogg != null)
                EventLogg(this, new MyEventArgs(loggerText));
        }

        private void Logger(string format, object logg, object logg2)
        {
            logger.InfoFormat(format, logg, logg2);
            var loggerText = string.Format(format, logg, logg2);

            if (EventLogg != null)
                EventLogg(this, new MyEventArgs(loggerText));
        }

        private void Logger(string format, string prefix, string type, string message, string stackTrace)
        {
            logger.InfoFormat(format, prefix, type, message, stackTrace);
            var loggerText = string.Format(format, prefix, type, message, stackTrace);

            if (EventLogg != null)
                EventLogg(this, new MyEventArgs(loggerText));
        }
    }

    public class MyEventArgs : EventArgs
    {
        public string MyEventString { get; set; }
        public MyEventArgs(string myString)
        {
            MyEventString = myString;
        }
    }
}
