﻿using Dezac.Core.Exceptions;
using Instruments.Auxiliars;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.InstrumentsTools
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ClearDielectricStrengthSetPointsActionDescription))]
    
    public class ClearDielectricStrengthSetPoints : ActionBase 
    {
        [DefaultValue((byte)2)]
        [Description("Puerto de Comunicacion")]
        public byte Port { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            try
            {
                var wmi = new WMISerialPortInitialize();
                wmi.Execute("MoxaPort", "MOXA USB Serial Port");
                var moxa = GetVariable<string>("MoxaPort");
                Port = Convert.ToByte(moxa.Replace("COM", ""));
            }
            catch (Exception)
            {
                Port = Port == 0 ? testBase.Comunicaciones.SerialPort : Port;
            }

            using (var tester = new DielectricStrengthTester(Port))
            {
                int steps = tester.ReadAmountOfSteps();
                for (int i = steps; i > 0; i--)
                    tester.DeleteTestParameters(i);
                tester.StopTest();
            }
        }
    }

    public class ClearDielectricStrengthSetPointsActionDescription : ActionDescription
    {
        public override string Name { get { return "ClearDielectricStrength"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Flash_Updater_icon; } }
    }
}
