﻿using Dezac.Core.Utility;
using System;
using System.Globalization;

namespace Instruments.SCPI
{
    public class SCPICommand
    {
        protected SCPIContext context;

        public SCPICommand(SCPIContext context, string cmd, params object[] args)
        {
            if (args != null)
                for (int i = 0; i < args.Length; i++)
                {
                    if (i > 0)
                        cmd += ",";

                    cmd += args[i].ToString();
                }

            this.context = new SCPIContext { Parent = context, Cmd = cmd };
        }

        public void Command()
        {
            context.Device.WriteCommand(context.BuildCommand());
        }
    }

    public class SCPIQueryCommand<TResult> : SCPICommand
    {
        public SCPIQueryCommand(SCPIContext context, string cmd, params object[] args)
            : base(context, cmd + "? ", args)
        {
        }

        public Result<TResult> Query()
        {
            Result<string> result = context.Device.WriteCommand(context.BuildCommand());

            if (result.Error)
                return new Result<TResult>(result.Exception);

            TResult res = (TResult)Convert.ChangeType(result.Value, typeof(TResult));

            return new Result<TResult>(res);
        }

        public Result<double> QueryNumber()
        {
            Result<string> result = context.Device.WriteCommand(context.BuildCommand());

            if (result.Error)
                return new Result<double>(result.Exception);

            double res = double.Parse(result.Value, NumberStyles.Float, new CultureInfo("en-US"));

            return new Result<double>(res);
        }
    }
}
