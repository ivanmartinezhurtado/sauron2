﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.Reflection;
using static Telerik.WinControls.UI.RadTreeView;
using Dezac.Core.Utility;
using Dezac.Device;

namespace Dezac.Tests.Actions.Views
{
    public partial class TreeTypeViewer : UserControl
    {
        public event RadTreeViewEventHandler SelectedNodeChanged;

        public TreeTypeViewer()
        {
            InitializeComponent();

            searchBox.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            list.SelectedNodeChanged += SelectedNodeChanged;
        }

        private void LoadAssembly(Assembly assembly, RadTreeNode node, bool showReturnType = false)
        {
            try
            {
                var types = from t in assembly.GetTypes()
                            where t.IsClass && t.IsPublic && !t.IsAbstract && t.IsSubclassOf(typeof(DeviceBase))
                            select t;

                foreach (var type in types)
                    LoadType(type, node, showReturnType);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void LoadDevices(bool showReturnType = false)
        {
            var devices = AppDomain.CurrentDomain.GetAssemblies().Where(p => p.FullName.Contains("Dezac.Device")).ToList();

            foreach (Assembly assembly in devices)
            {
                RadTreeNode node = new RadTreeNode(assembly.FullName.Split(',')[0].Replace("Dezac.Device.", "").Replace(".dll", ""));
                node.ImageKey = "folder";

                LoadAssembly(assembly, node, showReturnType);

                if (node.Nodes.Count > 0)
                    list.Nodes.Add(node);
            }
        }

        public void LoadInstruments(bool showReturnType = true)
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblies().Where(p => p.FullName.StartsWith("Dezac.Instruments")).FirstOrDefault();
            if (assembly == null)
                return;

            RadTreeNode node = new RadTreeNode("Instruments");
            node.ImageKey = "folder";

            var types = from t in assembly.GetTypes()
                        where t.IsClass && t.IsPublic && !t.IsAbstract
                        select t;

            foreach (var type in types)
            {
                var descriptionClass = type.GetCustomAttributes(typeof(DesignerControlAttribute));
                if (!descriptionClass.Any())
                    continue;

                var att = descriptionClass.Cast<DesignerControlAttribute>().FirstOrDefault();
                ControlDescription instance = Activator.CreateInstance(att.Type) as ControlDescription;
                if (instance == null || instance.SubcomponentType == null)
                    continue;

                var category = instance.Category ?? "Others";

                var child = node.Nodes.Where(p => p.Name.Contains(category)).FirstOrDefault();
                if (child == null)
                    child = node.Nodes.Add(category);

                LoadType(instance.SubcomponentType, child, showReturnType);
            }

            list.Nodes.Add(node);
        }

        public void LoadType(Type type, RadTreeNode node, bool showReturnType = true)
        {
            RadTreeNode child = new RadTreeNode(type.Name);
            child.Tag = type;
            child.ToolTipText = type.Assembly.FullName;
            child.ImageKey = imageList.Images.ContainsKey(type.Name) ? type.Name : "type";

            var methods = type
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => !m.IsSpecialName && !m.DeclaringType.FullName.StartsWith("System."))
                .OrderBy(p => p.Name);

            foreach (var method in methods)
            {
                RadTreeNode mchild = new RadTreeNode(method.Name);
                mchild.Tag = method;
                mchild.ImageKey = "method";

                child.Nodes.Add(mchild);

                if (showReturnType)
                    AddType(mchild, method.ReturnType, string.Empty);
            }

            if (node != null)
                node.Nodes.Add(child);
            else
                list.Nodes.Add(child);
        }

        public void AddType(RadTreeNode node, Type type, string prefix)
        {
            InternalAddType(node, type, prefix, new List<string>());
        }

        private void InternalAddType(RadTreeNode node, Type type, string prefix, List<string> types)
        {
            bool isStruct = type.IsValueType && !type.IsPrimitive;
            bool isList = type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(List<>));

            if (isList)
                return;

            if ((isStruct || type.IsClass || type.IsInterface) && type.FullName != null && !type.FullName.StartsWith("System."))
            {
                if (types.Contains(type.FullName))
                    return;

                types.Add(type.FullName);

                var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);

                foreach (var field in fields)
                {
                    var child = new RadTreeNode(field.Name);
                    child.ImageKey = "field";

                    if (node == null)
                        list.Nodes.Add(child);
                    else
                        node.Nodes.Add(child);

                    InternalAddType(child, field.FieldType, prefix != string.Empty ? prefix + "." + field.Name : field.Name, types);
                }

                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (var prop in properties)
                {
                    var child = new RadTreeNode(prop.Name);
                    child.ImageKey = "property";

                    if (node == null)
                        list.Nodes.Add(child);
                    else
                        node.Nodes.Add(child);

                    InternalAddType(child, prop.PropertyType, prefix != string.Empty ? prefix + "." + prop.Name : prop.Name, types);
                }
            }
            else
            {
                node.Text += string.Format(" ({0})", type.Name);
                node.Tag = prefix;
            }
        }

        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            list.Filter = searchBox.Text.Length > 0 ? "Custom" : null;
            list.TreeViewElement.FilterPredicate = FilterNode;

            if (!string.IsNullOrEmpty(searchBox.Text))
                list.ExpandAll();
            else
                list.CollapseAll();
        }

        private bool FilterNode(RadTreeNode node)
        {
            var text = searchBox.Text;
            if (string.IsNullOrEmpty(text) || node.Text == null)
                return true;

            while (node.Parent != null && !(node.Tag is Type))
                node = node.Parent;

            return NodeContainsText(node, text);
        }

        private bool NodeContainsText(RadTreeNode node, string text)
        {
            if (node.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            foreach (var child in node.Nodes)
                if (NodeContainsText(child, text))
                    return true;

            return false;
        }

        public RadTreeNode FindNodeRecursive(Predicate<RadTreeNode> match)
        {
            return FindNodeRecursive(list.Nodes, match);
        }

        public RadTreeNode FindNodeRecursive(RadTreeNodeCollection nodes, Predicate<RadTreeNode> match)
        {
            foreach (RadTreeNode node in nodes)
            {
                if (match(node))
                    return node;

                var find = FindNodeRecursive(node.Nodes, match);
                if (find != null)
                    return find;
            }

            return null;
        }

        [Browsable(false)]
        public RadTreeNodeCollection Nodes { get { return list.Nodes; } }
        [Browsable(false)]
        public RadTreeNode SelectedNode { get { return list.SelectedNode; } }

    }
}
