﻿namespace TaskRunner.Enumerators
{
    public enum TestParallelismBehaviour
    {
        None = 0,
        RunOnlyOnce = 1,
        WaitAllAndRunOnlyOnce = 3,
        WaitAllAndRunSequentially = 4,
        RunOnlyLastTest = 5
    }
}
