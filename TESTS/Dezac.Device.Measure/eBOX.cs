﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.05)]
    public class eBOX : DeviceBase
    {
        public eBOX()
        {
        }

        public eBOX(int port, byte periferico)
        {
            SetPort(port, periferico);
        }

        public void SetPort(int port, byte periferico, int baudRate = 38400)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferico;
        }


        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();
            Modbus.Write<int>((ushort)Registers.BASTIDOR, bastidor);
        }

        public int ReadBastidor()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public double ReadTemperature()
        {
            LogMethod();
            return (double)((double)Modbus.ReadInt32((ushort)Registers.TEMPERATURE) / 100D);
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            var firmwareReading = Modbus.ReadInt32((ushort)Registers.VERSION_FIRMWARE).ToString("X2").PadLeft(8, '0').HexStringToBytes();
            var firmwareVersion = string.Empty;

            foreach (byte firmwareByte in firmwareReading)
                firmwareVersion += Convert.ToInt16(firmwareByte).ToString() + ".";

            return firmwareVersion.TrimEnd('.');
        }

        public string ReadGenericVersion()
        {
            LogMethod();

            var genericHardwareReading = Modbus.ReadInt32((ushort)Registers.VERSION_CONJUNTO).ToString("X2").PadLeft(8, '0').HexStringToBytes();

            return string.Join(".", genericHardwareReading);
        }

        public string ReadBootVersion()
        {
            LogMethod();
            var bootReading = Modbus.ReadInt32((ushort)Registers.VERSION_BOOT).ToString("X2").PadLeft(8, '0').HexStringToBytes();
            var bootVersion = string.Empty;

            foreach (byte bootByte in bootReading)
                bootVersion += Convert.ToInt16(bootByte).ToString() + ".";

            return bootVersion.TrimEnd('.');
        }

        public ModelCustomer ReadModel()
        {
            LogMethod();
            return Modbus.Read<ModelCustomer>();
        }

        public void WriteModel(ModelCustomer modelMyeBOX)
        {
            LogMethod();
            Modbus.Write<ModelCustomer>(modelMyeBOX);
        }

        public void WriteDateTime(DateTime? date = null)
        {
            LogMethod();
            Modbus.Write<FechaHora>((ushort)Registers.FECHA_HORA, FechaHora.Create(date));
        }

        public DateTime ReadDatetime()
        {
            LogMethod();
            return Modbus.Read<FechaHora>((ushort)Registers.FECHA_HORA).ToDateTime();
        }

        public void WriteTestInOutStart()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_IN_OUT, 1);
        }

        public void WriteTestInOutFinish()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_IN_OUT, 0);
        }

        public ResulTestInOut ReadTestInOut()
        {
            LogMethod();
            return (ResulTestInOut)Modbus.Read<ushort>((ushort)Registers.RESULT_TEST_IN_OUT);
        }

        public void WriteLeds(Leds led)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LEDS, (ushort)led);
        }

        public void WriteTestDisplayStart()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.DISPLAY, 1);
        }

        public void WriteTestDisplayFinish()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.DISPLAY, 0);
        }

        public void WriteKeyboardTestStart()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_TECLAT, 1);
        }

        public void WriteKeyboardTestFinish()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_TECLAT, 0);
        }

        public KeyboardKeys ReadTestTeclat()
        {
            return (KeyboardKeys)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_TECLADO);
        }

        public ResulTest ReadDetectionSD1()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_DETECTION_SD1);
        }

        public ResulTest ReadDetectionSD2()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_DETECTION_SD2);
        }

        public ushort ReadSD1Size()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.SD1_SIZE);
        }

        public ushort ReadSD2Size()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.SD2_SIZE);
        }

        public void WriteTestSD()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_SD, 1);
        }

        public NandFlashSizeEnum ReadNandFlashSize()
        {
            LogMethod();
            return (NandFlashSizeEnum)Modbus.ReadRegister((ushort)Registers.NANDFLASH_SIZE);
        }

        public ResulTest ReadTest3G()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_3G);
        }

        public void WriteTest3G()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_3G, 1);
        }

        public Setup3G Read3GSetup()
        {
            return Modbus.Read<Setup3G>();
        }

        public void WriteSetup3G(Setup3G setup)
        {
            LogMethod();
            Modbus.Write<Setup3G>((ushort)Registers.SETUP_3G, setup);
        }

        public SetupWifi ReadWifiSetup()
        {
            return Modbus.Read<SetupWifi>();
        }

        public ResulTest ReadTestWifi()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_WIFI);
        }

        public ushort ReadEmbeddedStatus()
        {
            return Modbus.ReadRegister((ushort)Registers.EMBEDDED_RUNNING);
        }

        public List<IMXErrorCode> ReadIMXStatus()
        {
            var imxStatus = Modbus.ReadRegister((ushort)Registers.IMX_STATUS);
            var result = GetEnumItemsFromUshort<IMXErrorCode>(imxStatus);
            if (result.Count > 1)
                result.Remove(eBOX.IMXErrorCode.NO_ERROR);

            return result;
        }

        public string ReadIMEI3G()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.IMEI_3G, 9).Substring(0, 15);
        }

        public bool ReadSDWifi3GTestResult()
        {
            var result = Modbus.ReadRegister((ushort)Registers.WIFI_3G_SD_TEST_RESULT);

            return result == 1;
        }

        public StatusSD ReadSDStatus()
        {
            LogMethod();
            return (StatusSD)Modbus.ReadRegister((ushort)Registers.SD_STATUS);
        }

        public void WriteSetupWifi(SetupWifi setup)
        {
            LogMethod();
            Modbus.Write<SetupWifi>((ushort)Registers.SETUP_WIFI, setup);
        }

        public void WriteTestWifi()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_WIFI, 1);
        }

        public void WriteTestUSB()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_USB, 1);
        }

        public ResulTest ReadTestUSB()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_USB);
        }

        public void WriteEepromClampsTestStart()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.EEPROM_CLAMPS_TEST, 1);
        }

        public void WriteEepromClampsTestFinish()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.EEPROM_CLAMPS_TEST, 0);
        }

        public ResultadoTestPinzas ReadTestEepromPinces()
        {
            LogMethod();
            return (ResultadoTestPinzas)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_EEPROM_PINCES);
        }

        public StatusWifi ReadWifiStatus()
        {
            LogMethod();
            return (StatusWifi)Modbus.ReadRegister((ushort)Registers.WIFI_STATUS);
        }

        public string Read3GIP()
        {
            LogMethod();
            return string.Join(".", Modbus.ReadMultipleRegister((ushort)Registers.IP_3G, 4));
        }

        public Status3G Read3GStatus()
        {
            LogMethod();
            return (Status3G)Modbus.ReadRegister((ushort)Registers._3G_STATUS);
        }

        public string ReadWiFiMAC()
        {
            var macReading = Modbus.ReadBytes((ushort)Registers.MAC_WIFI, 6);

            StringBuilder mac = new StringBuilder();

            for (int i = 0; i < macReading.Length; i = i + 2)
            {
                mac.Append(Char.ConvertFromUtf32(macReading[i]));
                mac.Append(Char.ConvertFromUtf32(macReading[i + 1]));
                mac.Append(":");
            }

            mac.Remove(mac.Length - 1, 1);

            return mac.ToString();
        }

        public string Read3GMAC()
        {
            var macReading = Modbus.ReadBytes((ushort)Registers.MAC_3G, 6);

            StringBuilder mac = new StringBuilder();

            for (int i = 0; i < macReading.Length; i = i + 2)
            {
                mac.Append(Char.ConvertFromUtf32(macReading[i]));
                mac.Append(Char.ConvertFromUtf32(macReading[i + 1]));
                mac.Append(":");
            }

            mac.Remove(mac.Length - 1, 1);

            return mac.ToString();
        }

        public void Disable3GEmbeddedConfiguration()
        {
            var embedded3GConfig = Modbus.Read<Setup3G>((ushort)Registers.SETUP_3G_EMBEDDED);

            embedded3GConfig.Connection = 0;

            Modbus.Write<Setup3G>((ushort)Registers.SETUP_3G_EMBEDDED, embedded3GConfig);
        }

        public void DisableWifiEmbeddedConnection()
        {
            var embeddedWiFiConfig = Modbus.Read<SetupWifi>((ushort)Registers.SETUP_WIFI_EMBEDDED);

            embeddedWiFiConfig.Connection = 0;

            Modbus.Write<SetupWifi>((ushort)Registers.SETUP_WIFI_EMBEDDED, embeddedWiFiConfig);
        }

        //*********************************************************************************

        public MeasureVoltageCurrent ReadVoltageCurrent()
        {
            LogMethod();

            return Modbus.Read<MeasureVoltageCurrent>();
        }

        public MeasurePower ReadPowers()
        {
            LogMethod();
            return Modbus.Read<MeasurePower>();
        }

        public Double ReadCurrentLK()
        {
            LogMethod();
            return (double)(Modbus.ReadInt32((ushort)0x9E8A) / 1000.0D);
        }

        public TriLineValue ReadPhaseGap()
        {
            LogMethod();
            var phaseGaps = Modbus.Read<TriIntStruct>((ushort)Registers.PHASE_GAP_READINGS);

            return new TriLineValue() { L1 = phaseGaps.Value.L1 / 1000D, L2 = phaseGaps.Value.L2 / 1000D, L3 = phaseGaps.Value.L3 / 1000D, };
        }

        //*********************************************************************************

        public FourIntStruct ReadGainVoltage(Frecuency FrecuencyGain = Frecuency._50Hz)
        {
            LogMethod();
            var addres = FrecuencyGain == Frecuency._50Hz ? Registers.GAIN_VOLTAGE : Registers.GAIN_VOLTAGE_60HZ;
            return Modbus.Read<FourIntStruct>((ushort)addres);
        }

        public void WriteGainVoltage(FourIntStruct gainVoltage, Frecuency FrecuencyGain = Frecuency._50Hz)
        {
            LogMethod();
            var addres = FrecuencyGain == Frecuency._50Hz ? Registers.GAIN_VOLTAGE : Registers.GAIN_VOLTAGE_60HZ;
            Modbus.Write<FourIntStruct>((ushort)addres, gainVoltage);
        }

        public TriIntStruct ReadGainVVoltageCompound()
        {
            LogMethod();
            return Modbus.Read<TriIntStruct>((ushort)Registers.GAIN_VOLTAGE_COMPOUND);
        }

        public void WriteGainVoltageCompound(TriIntStruct gainVoltageCompound)
        {
            LogMethod();
            Modbus.Write<TriIntStruct>((ushort)Registers.GAIN_VOLTAGE_COMPOUND, gainVoltageCompound);
        }

        public FourIntStruct ReadGainCurrents(CircuitsCurrent circuit)
        {
            LogMethod();
            return Modbus.Read<FourIntStruct>((ushort)GetRegistersByCircuit(circuit).CurrentsGainsRegister);
        }

        public void WriteGainCurrents(CircuitsCurrent circuit, FourIntStruct gainCurrents)
        {
            LogMethod();
            Modbus.Write<FourIntStruct>((ushort)GetRegistersByCircuit(circuit).CurrentsGainsRegister, gainCurrents);
        }

        public int ReadGainCurrentsLk(CircuitsCurrent circuit)
        {
            LogMethod();
            return Modbus.Read<int>((ushort)GetRegistersByCircuit(circuit).CurrentLkGainRegister);
        }

        public void WriteGainCurrentLk(CircuitsCurrent circuit, int gainCurrentLk)
        {
            LogMethod();
            Modbus.Write<int>((ushort)GetRegistersByCircuit(circuit).CurrentLkGainRegister, gainCurrentLk);
        }

        //**************************************************************************************

        public void WriteNeuterCalculusEnabling(bool activate)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ADJUST_NEUTER, (ushort)(activate ? 1 : 0));
        }

        public void WriteSerialNumber(string serialNumber)
        {
            LogMethod();
            string text = serialNumber.ToString().PadLeft(12, '0'); //6 Registros
            Modbus.WriteString((ushort)Registers.SERIAL_NUMBER, text);
        }

        public string ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.SERIAL_NUMBER, 6);
        }

        public string ReadRegisterCode()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.REGISTER_CODE, 10).CToNetString();
        }

        public void WriteReleDefault()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RELE_DEFAULT, true);
        }

        public void WriteResetControlEmbedded()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RESET_CONTROL, true);
        }

        public ResulTest ReadTestADC()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_ACDC);
        }

        public ResulTest ReadTestADC1()
        {
            LogMethod();
            return (ResulTest)Modbus.ReadRegister((ushort)Registers.RESULT_TEST_ACDC1);
        }

        public void WriteControlSignalForceps(Registersforceps register, ushort value)
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)register, value);
        }

        public void WriteTestRTC()
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)Registers.TEST_RTC, 1);
        }

        public int ReadVoltageBatery()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.VOLTAGE_BATERY);
        }

        public int ReadCurrentBatery()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.CURRENT_BATERY);
        }

        public int ReadPowerChargeStatus()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.POWER_CHARGE_STATUS);
        }

        //*********************************************************
        //*********************************************************

        public void WriteTestAdjust(CircuitsCurrent circuit)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TEST_ADJUST, (ushort)circuit);
        }

        public void WriteTestVerification(CircuitsCurrent circuit)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TEST_ADJUST,(ushort)GetRegistersByCircuit(circuit).VerificationMeasureRegister);
        }

        public FiveLineValue CalculGain(FiveLineValue measurePattern, FiveLineValue gainMeasureRead, FiveLineValue measureRead)
        {
            var calculatedGains = new FiveLineValue();

            calculatedGains.LINES = CalculGain(measurePattern.LINES, gainMeasureRead.LINES, measureRead.LINES);
            calculatedGains.LN = CalculGain(measurePattern.LN, gainMeasureRead.LN, measureRead.LN);
            calculatedGains.LK = CalculGain(measurePattern.LK, gainMeasureRead.LK, measureRead.LN);

            return calculatedGains;
        }

        public FourLineValue CalculGain(FourLineValue measurePattern, FourLineValue gainMeasureRead, FourLineValue measureRead)
        {
            var calculatedGains = new FourLineValue();

            calculatedGains.LINES = CalculGain(measurePattern.LINES, gainMeasureRead.LINES, measureRead.LINES);

            calculatedGains.LN = CalculGain(measurePattern.LN, gainMeasureRead.LN, measureRead.LN);

            return calculatedGains;
        }

        public TriLineValue CalculGain(TriLineValue measurePattern, TriLineValue gainMeasureRead, TriLineValue measureRead)
        {
            var calculatedGains = new TriLineValue();

            calculatedGains.L1 = CalculGain(measurePattern.L1, gainMeasureRead.L1, measureRead.L1);
            calculatedGains.L2 = CalculGain(measurePattern.L2, gainMeasureRead.L2, measureRead.L2);
            calculatedGains.L3 = CalculGain(measurePattern.L3, gainMeasureRead.L3, measureRead.L3);

            return calculatedGains;
        }

        public Int32 CalculGain(double measurePattern, double gainMeasureRead, double measureRead)
        {
            if (measureRead == 0)
                return 0;

            return Convert.ToInt32(measurePattern * gainMeasureRead / measureRead);
        }

        //*********************************************************

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x9E66)]
        public struct MeasureVoltageCurrent
        {
            public VoltageStruct Voltage;
            public VoltageCompoundStruct VoltageCompound;
            public CurrentStruct Currents;
            public Int32 _ILNCalc;
            public double ILNCalc { get { return _ILNCalc / 1000.0D; } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x9E7E)]
        public struct MeasurePower
        {
            public PowerStruct PowerActive;
            public PowerStruct PowerReactive;
        };

        //*********************************************************

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriIntStruct
        {
            private Int32 L1;
            private Int32 L2;
            private Int32 L3;
            public TriLineValue Value
            {
                get { return (TriLineValue.Create(L1, L2, L3)); }
                set
                {
                    L1 = (int)value.L1;
                    L2 = (int)value.L2;
                    L3 = (int)value.L3;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FourIntStruct
        {
            private TriIntStruct lines;
            private Int32 LN;
            public FourLineValue Value
            {
                get { return (FourLineValue.Create(lines.Value, LN)); }
                set
                {
                    lines.Value = value.LINES;
                    LN = (int)value.LN;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VoltageCompoundStruct
        {
            private TriIntStruct lines;
            public TriLineValue Value
            {
                get { return (lines.Value / 100); }
                set { lines.Value = value * 100; }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VoltageStruct
        {
            private TriIntStruct lines;
            private Int32 LN;

            public FourLineValue Value
            {
                get { return (FourLineValue.Create(lines.Value, LN) / 100.0D); }
                set { LN =(int)(value.LN * 100); lines.Value = value.LINES * 100; }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CurrentStruct
        {
            private TriIntStruct lines;
            private Int32 LN;

            public FourLineValue Value
            {
                get { return (FourLineValue.Create(lines.Value, LN) / 1000.0D); }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PowerStruct
        {
            private TriIntStruct lines;
            public TriLineValue Value
            {
                get { return (lines.Value); }
            }
        }

        //*********************************************************
        //*********************************************************

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address =(ushort)Registers.MODEL)]
        public struct ModelCustomer
        {
            public UInt16 ModelAndCustomer;
            public UInt16 Uknow1;
            public UInt16 Uknow2;
            public UInt16 Uknow3;

            public string ModelMyeBOX { get { return ModelAndCustomer.ToString("x4").Substring(0, 2); } }
            public string CustomerMyeBOX { get { return ModelAndCustomer.ToString("x4").Substring(2,2); } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FechaHora
        {
            public UInt16 Year;
            public UInt16 Month;
            public UInt16 Day;
            public UInt16 Hour;
            public UInt16 Minutes;
            public UInt16 Seconds;

            public static FechaHora Create(DateTime? date = null)
            {
                DateTime fecha = date.GetValueOrDefault(DateTime.Now);

                FechaHora value = new FechaHora
                {
                    Year = (ushort)fecha.Year,
                    Month = (ushort)fecha.Month,
                    Day = (ushort)fecha.Day,
                    Hour = (ushort)fecha.Hour,
                    Minutes = (ushort)fecha.Minute,
                    Seconds = (ushort)fecha.Second,
                };

                return value;
            }

            public DateTime ToDateTime()
            {
                return new DateTime(Year, Month, Day, Hour, Minutes, Seconds);
            }
        }

        [ModbusLayout(Address = (ushort)Registers.SETUP_3G)]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Setup3G
        {
            public UInt16 Connection;
            public UInt16 PinEnable;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] APN_name;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] APN_user;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] APN_password;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public byte[] PIN;

            public void SetAPNName(string name)
            {
                APN_name = new byte[32]; //Enumerable.Repeat((byte)0x00, 32).ToArray();
                var bytes = ASCIIEncoding.ASCII.GetBytes(name).Take(32).ToArray();

                bytes.CopyTo(APN_name, 0);
            }
            public void SetAPNUser(string user)
            {
                APN_user = new byte[32];  //Enumerable.Repeat((byte)0x00, 32).ToArray();
                var bytes = ASCIIEncoding.ASCII.GetBytes(user).Take(32).ToArray();

                bytes.CopyTo(APN_user, 0);
            }
            public void SetAPNPasswod(string password)
            {
                APN_password = new byte[32];  //Enumerable.Repeat((byte)0x00, 32).ToArray();
                var bytes = ASCIIEncoding.ASCII.GetBytes(password).Take(32).ToArray();

                bytes.CopyTo(APN_password, 0);
            }

            public void SetPIN(string pin)
            {
                PIN = new byte[4];  //Enumerable.Repeat((byte)0x00, 4).ToArray();
                var bytes = ASCIIEncoding.ASCII.GetBytes(pin).Take(4).ToArray();

                bytes.CopyTo(PIN, 0);
            }
        }

        [ModbusLayout(Address = (ushort)Registers.SETUP_WIFI)]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SetupWifi
        {
            public UInt16 Connection;
            public UInt16 WPS;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] SSID;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] Password;

            public void SetASSID(string ssid)
            {
                //SSID = Enumerable.Repeat((byte)0x00, 32).ToArray();
                SSID = new byte[32];
                var bytes = ASCIIEncoding.ASCII.GetBytes(ssid).Take(32).ToArray();

                bytes.CopyTo(SSID, 0);
            }
            public void SetPasswod(string password)
            {
                // Password = Enumerable.Repeat((byte)0x00, 32).ToArray();
                Password = new byte[32];
                var bytes = ASCIIEncoding.ASCII.GetBytes(password).Take(32).ToArray();

                bytes.CopyTo(Password, 0);
            }
        }

        //*********************************************************

        public enum ModelosMyeBOX
        {
            eBOX_1500= 2,
            eBOX_150 = 1,
        }

        public enum Leds
        {
            [Description("el backlight")]
            ALL_OFF = 0x0001,
            POWER = 0x0002,
            TECLAT = 0x0004,
            TENSIO = 0x0008,
            CURRENT = 0x0010,
            REC = 0x0020,
            SD_VERMELL = 0x0040,
            SD_VERD = 0x0080,
            SD_BLAU = 0x0100,
            DIO = 0x0200,
            WIFI_3G_USB = 0x0400,
            BATERIA_VERMELL = 0x0800,
            BATERIA_VERD = 0x1000,
            BATERIA_BLAU = 0x2000,
            [Description("los leds de teclado, tension, corriente, bateria roja y SD roja")]
            KEYB_VOLT_CURR_BAT_SD_RED = 0x085C,
            [Description("los leds de entradas/salidas, de registro de datos, bateria verde y SD verde")]
            REC_IO_BAT_SD_GREEN = 0x12A0,
            [Description("los leds de wifi, 3G, USB, bateria azul y SD azul")]
            WIFI_3G_USB_BAT_SD_BLUE = 0x2502,
        }

        public enum Registers
        {
            PHASE_GAP_READINGS = 0x00CC,
            IP_WIFI = 0x30EA,
            IP_3G = 0x30FE,
            MAC_WIFI = 0x30EE,
            MAC_3G = 0x3102,
            FLAG_TEST = 0x02AF8,
            SETUP_WIFI_EMBEDDED = 0x279C,
            SETUP_3G_EMBEDDED = 0x27C4,
            TEMPERATURE = 0x2862,
            MODEL = 0x2832,
            BASTIDOR = 0x2838,
            FECHA_HORA = 0x284E,
            TEST_IN_OUT = 0x9E36,
            RESULT_TEST_IN_OUT = 0x9E98,
            LEDS = 0x9E35,
            DISPLAY = 0x9E37,
            TEST_TECLAT = 0x9E39,
            RESULT_TEST_TECLADO = 0x9EAF,
            RESULT_TEST_UART1 = 0x9E9F,
            RESULT_TEST_UART2 = 0x9EA0,
            RESULT_DETECTION_SD1 = 0x9E9A,
            RESULT_DETECTION_SD2 = 0x9E9B,
            SD1_SIZE = 0x2BC8,
            SD2_SIZE = 0x2BC9,
            TEST_SD = 0x9E3E,
            TEST_NANDFLASH = 0x9E9C,
            NANDFLASH_SIZE = 0x9EA1,
            RESULT_TEST_3G = 0x9E9D,
            SETUP_3G = 0x3F16,
            TEST_3G = 0x9E3C,
            RESULT_TEST_WIFI = 0x9E9E,
            SETUP_WIFI = 0x3EE4,
            TEST_WIFI = 0x9E3D,
            TEST_USB = 0x9E3B,
            RESULT_TEST_USB = 0x9EA4,
            EEPROM_CLAMPS_TEST = 0x9E38,
            RESULT_TEST_EEPROM_PINCES = 0x9EB5,
            GAIN_VOLTAGE = 0x9C40,
            GAIN_VOLTAGE_COMPOUND = 0x9CF4,
            GAIN_VOLTAGE_60HZ = 0x9CFC,
            TEST_ADJUST = 0x9E34,
            ADJUST_NEUTER = 0x9E47,
            SERIAL_NUMBER = 0x2828,
            REGISTER_CODE = 0x3110,
            SETUP_DEFAULT_50HZ = 0xBBC,
            SETUP_DEFAULT_60HZ = 0xBBD,
            RELE_DEFAULT = 0x0C1C,
            RESULT_TEST_ACDC = 0x9EB0,
            RESULT_TEST_ACDC1 = 0x9EB1,
            VERSION_FIRMWARE = 0x2AF8,
            VERSION_CONJUNTO = 0x2AFA,
            VERSION_BOOT = 0x2AFE,
            TEST_RTC = 0x9E41,
            EMBEDDED_RUNNING = 0x9D6C,
            IMX_STATUS = 0x9EB4,
            SD_TEST_STARTED = 0x30D8,
            WIFI_3G_SD_TEST_RESULT = 0x9D6D,
            WIFI_STATUS = 0x9EB8,
            _3G_STATUS = 0x9EB9,
            SD_STATUS = 0x9EBA,
            RESET_CONTROL = 0x0CE4,
            POWER_CHARGE_STATUS = 0x286E,
            CURRENT_BATERY = 0x2870,
            VOLTAGE_BATERY = 0x2874,
            IMEI_3G = 0x311E
                
        }

        public enum Frecuency
        {
            _50Hz = 51,
            _60Hz = 60
        }

        public enum Registersforceps
        {
            VOLT_H = 0x9E3F,
            VOLT_N = 0x9E40,
            CLAMP_F1 = 0x9E42,
            CLAMP_F2 = 0x9E43,
            CLAMP_F3 = 0x9E44,
            CLAMP_N = 0x9E45,
            CLAMP_K = 0x9E46,
        }

        //*********************************************************

        public enum CircuitsCurrent
        {
            _2V = 0x0001,
            _1_28V = 0x0002,
            _333mV = 0x0003,
            _10mV = 0x0005,
            _100mV = 0x006,
            _1000mV = 0x0007,
            _250mA = 0x0004,
            _300mA = 0x0009,
            _30mA = 0x0008,
            _3000mA = 0x000A
        }

        private enum CircuitsCurrentVerif
        {
            _2V = 0x000B,
            _1_28V = 0x000C,
            _333mV = 0x000D,
            _10mV = 0x000F,
            _100mV = 0x010,
            _1000mV = 0x011,
            _250mA = 0x000E,
            _300mA = 0x0013,
            _30mA = 0x0012,
            _3000mA = 0x0014
        }

        private enum CurrentGainsRegisters
        {
            _2V = 0x9C4A,
            _1_28V = 0x9C54,
            _333mV = 0x9C5E,
            _10mV = 0x9C72,
            _100mV = 0x9C7C,
            _1000mV = 0x9C86,
            _250mA = 0x9C68,
        }

        private enum CurrentLkGainRegisters
        {
            _2V = 0x9C52,
            _1_28V = 0x9C5C,
            _333mV = 0x9C66,
            _250mA = 0x9C70,
            _300mA = 0x9C66,
            _30mA = 0x9C5C,
            _3000mA = 0x9C70
        }

        public struct CircuitRegisters
        {
            public Int32 CurrentsGainsRegister { get; set; }
            public Int32 CurrentLkGainRegister { get; set; }
            public Int32 AdjustMeasureRegister { get; set; }
            public Int32 VerificationMeasureRegister { get; set; }
        }

        public CircuitRegisters GetRegistersByCircuit(CircuitsCurrent circuito)
        {
            var reg = new CircuitRegisters();
            switch (circuito)
            {
                case CircuitsCurrent._2V:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._2V;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._2V;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._2V;
                    break;
                case CircuitsCurrent._1_28V:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._1_28V;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._1_28V;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._1_28V;
                    break;
                case CircuitsCurrent._333mV:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._333mV;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._333mV;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._333mV;
                    break;
                case CircuitsCurrent._1000mV:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._1000mV;
                    reg.CurrentLkGainRegister = 0;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._1000mV;
                    break;
                case CircuitsCurrent._10mV:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._10mV;
                    reg.CurrentLkGainRegister = 0;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._10mV;
                    break;
                case CircuitsCurrent._100mV:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._100mV;
                    reg.CurrentLkGainRegister = 0;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._100mV;
                    break;
                case CircuitsCurrent._250mA:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._250mA;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._250mA;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._250mA;
                    break;
                case CircuitsCurrent._30mA:
                    reg.CurrentsGainsRegister = 0;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._30mA;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._30mA;
                    break;
                case CircuitsCurrent._300mA:
                    reg.CurrentsGainsRegister = (int)CurrentGainsRegisters._250mA;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._300mA;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._300mA;
                    break;
                case CircuitsCurrent._3000mA:
                    reg.CurrentsGainsRegister = 0;
                    reg.CurrentLkGainRegister = (int)CurrentLkGainRegisters._3000mA;
                    reg.AdjustMeasureRegister = (int)circuito;
                    reg.VerificationMeasureRegister = (int)CircuitsCurrentVerif._3000mA;
                    break;
                default:
                    throw new Exception("Not implemented register circuit gain current");
            }
            return reg;
        }

        //******************************************************
        //******************************************************

        public enum KeyboardKeys
        {
            NONE = 0x0000,
            [Description("Tecla flecha arriba")]
            UP = 0x0001,
            [Description("Tecla flecha abajo")]
            DOWN = 0x0002,
            [Description("Tecla flecha izquierda")]
            LEFT = 0x0003,
            [Description("Tecla flecha derecha")]
            RIGHT = 0x0004,
            [Description("Tecla central")]
            CENTER = 0x0005,
            [Description("Tecla de encendido")]
            POWER = 0x0006,
            [Description("Tecla REC o de grabación")]
            REC = 0x0007
        }

        [Flags]
        public enum ResulTestInOut
        {
            [Description("Test sin acabar o en ejecución")]
            RUNNING = 0xFFFF,
            [Description("Test finalizado con éxito")]
            OK = 0x0000,
            [Description("Error al leer el estado bajo de la entrada/salida 1")]
            ERROR_IO1_LOW = 0x0001,
            [Description("Error al leer el estado alto de la entrada/salida 1")]
            ERROR_IO2_LOW = 0x0002,
            [Description("Error al leer el estado bajo de la entrada/salida 2")]
            ERROR_IO1_HIGH = 0x0010,
            [Description("Error al leer el estado alto de la entrada/salida 2")]
            ERROR_IO2_HIGH = 0x0020,
        }

        [Flags]
        public enum ResulTest
        {
            RUNNING = 0xFFFF,
            OK = 0x0001,
            ERROR = 0x0000,
            WARNING = 0x0002,
        }

        public enum NandFlashSizeEnum
        {
            RUNNING = 0xFFFF,
            SIZE_256MB = 0x0100,
        }

        [Flags]
        public enum ResultadoTestPinzas
        {
            RUNNING = 0xFFFF,
            OK = 0x0000,
            ERROR_LECTURA_PINZA_L1 = 0x0001,
            ERROR_LECTURA_PINZA_L2 = 0x0002,
            ERROR_LECTURA_PINZA_L3 = 0x0004,
            ERROR_LECTURA_PINZA_LN = 0x0008,
            ERROR_LECTURA_PINZA_LLEAK = 0x0010,
            ERROR_PRESENCIA_PINZA_L1 = 0x0100,
            ERROR_PRESENCIA_PINZA_L2 = 0x0200,
            ERROR_PRESENCIA_PINZA_L3 = 0x0400,
            ERROR_PRESENCIA_PINZA_LN = 0x0800,
            ERROR_PRESENCIA_PINZA_LLEAK = 0x1000,
        }

        [Flags]
        public enum IMXErrorCode
        {
            NO_ERROR = 0x0000,
            ERROR_DDR = 0x0001,
            ERROR_SD1 = 0x0002,
            ERROR_SD2 = 0x0004,
            ERROR_NAND = 0x0008,
            ERROR_3G = 0x0010,
            ERROR_WIFI = 0x0020,
            ERROR_UART1 = 0x0040,
            ERROR_UART2 = 0x0080,
            ERROR_IMX28_NO_ARRANCA = 0x0100,
            ERROR_TECLADO_CAPACITIVO = 0x0200,
            ERROR_ADC0 = 0x0400,
            ERROR_ADC1 = 0x0800,
            ERROR_VERSIONES_K70_EMBEDDED_INCOMPATIBLES = 0x1000,
            ERROR_CONFIGURACION_TECLADO_CAPACITIVO = 0x2000
        }

        public enum StatusWifi
        {
            [Description("Test sin acabar o en ejecución")]
            RUNNING = 0xFFFF,
            [Description("Test finalizado con éxito")]
            OK = 0x0001,
            [Description("Error en la lectura de la configuración de wifi")]
            CANNOT_READ_WIFI_SETUP = 0x0002,
            [Description("Error en el estado del imx, no se ha devuelto valor")]
            IMX_STATUS_NOT_RETURNED = 0x0003,
            [Description("El IMX no ha podido conectarse al router especificado")]
            IMX_COULD_NOT_CONNECT = 0x0004
        }

        public enum Status3G
        {
            [Description("Test sin acabar o en ejecución")]
            RUNNING = 0xFFFF,
            [Description("Test finalizado con éxito")]
            OK = 0x0001,
            [Description("No se detecta presencia de tarjeta SIM")]
            SIM_NOT_DETECTED = 0x0002,
            [Description("Error en la lectura de configuración de 3G")]
            CANNOT_READ_3G_SETUP = 0x0003,
            [Description("Error en el estado del imx, no se ha devuelto valor")]
            IMX_STATUS_NOT_RETURNED = 0x0004,
            [Description("El IMX no ha podido conectarse al router especificado")]
            IMX_COULD_NOT_CONNECT = 0x0005,
            [Description("El pin configurado no es válido")]
            SIM_PIN_INVALID = 0x0006,
            [Description("targeta bloqueada introduce el puk")]
            SIM_PUK_INVALID = 0x0007,
            [Description("Error de CME")]
            CME_ERROR = 0x0008,
            [Description("Error de APN")]
            APN_ERROR = 0x0009,
            [Description("Error desconocido")]
            UNKNOWN_ERROR = 0x000A,
        }

        public enum StatusSD
        {
            [Description("Test sin acabar o en ejecución")]
            RUNNING = 0xFFFF,
            [Description("Test finalizado con éxito")]
            OK = 0x0001,
            [Description("No se ha iniciado el formateo de las tarjetas SD")]
            IMX_DOES_NOT_START_FORMATTING = 0x0002,
            [Description("El IMX no devuelve resultado para la SD1")]
            IMX_DOES_NOT_RETURN_SD_1_RESULT = 0x0004,
            [Description("Error en el formateo de la SD1")]
            SD_1_FORMAT_ERROR = 0x0008,
            [Description("El IMX no devuelve resultado para la SD2")]
            IMX_DOES_NOT_RETURN_SD_2_RESULT = 0x0010,
            [Description("Error en el formateo de la SD2")]
            SD_2_FORMAT_ERROR = 0x0020
        }


        //****************************************************

        public VariablesInstantaneas ReadVariablesInstantaneas()
        {
            return Modbus.Read<VariablesInstantaneas>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            private Int32 tension;
            private Int32 corriente;
            private Int32 potenciaActiva;
            private Int32 potenciaReactivaInductiva;
            private Int32 potenciaReactivaCapicitiva;
            private Int32 potenciaAparente;
            private Int32 factorPotencia;
            private Int32 cosenoPhi;

            public double Tension { get { return Convert.ToDouble(tension) / 100; } }
            public double Corriente { get { return Convert.ToDouble(corriente) / 1000; } }
            public double PotenciaActiva { get { return Convert.ToDouble(potenciaActiva); } }
            public double CosenoPhi { get { return Convert.ToDouble(cosenoPhi) / 1000; } }
            public double PotenciaActivaAltaResolucion { get { return Convert.ToDouble(Tension * Corriente * FactorPotencia); } }
            public double PotenciaReactivaInductiva { get { return Convert.ToDouble(potenciaReactivaInductiva); } }
            public double PotenciaReactivaCapicitiva { get { return Convert.ToDouble(potenciaReactivaCapicitiva); } }
            public double PotenciaAparente { get { return Convert.ToDouble(potenciaAparente) / 10; } }
            public double FactorPotencia { get { return Convert.ToDouble(factorPotencia) / 100; } }
        };

        public VariablesTrifasicas ReadVariablesInstantaneasIII()
        {
            return Modbus.Read<VariablesTrifasicas>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0030)]
        public struct VariablesTrifasicas
        {
            private Int32 tensionNeutro;
            private Int32 corrienteNeutro;
            private Int32 frecuencia;
            private Int32 tensionLineaL1L2;
            private Int32 tensionLineaL2L3;
            private Int32 tensionLineaL3L1;
            private Int32 tensionLineaIII;
            private Int32 tensionFaseIII;
            private Int32 corrienteIII;
            private Int32 potenciaActivaIII;
            private Int32 potenciaInductivaIII;
            private Int32 potenciaCapacitivaIII;
            private Int32 potenciaAparenteIII;
            private Int32 facttorPotenciaIII;
            private Int32 cosenoPhiIII;

            public double TensionNeutro { get { return Convert.ToDouble(tensionNeutro) / 100; } }
            public double CorrienteNeutro { get { return Convert.ToDouble(corrienteNeutro / 1000); } }
            public double Frecuencia { get { return Convert.ToDouble(frecuencia) / 100; } }
            public double TensionLineaL1L2 { get { return Convert.ToDouble(tensionLineaL1L2) / 100; } }
            public double TensionLineaL2L3 { get { return Convert.ToDouble(tensionLineaL2L3) / 100; } }
            public double TensionLineaL3L1 { get { return Convert.ToDouble(tensionLineaL3L1) / 100; } }
            public double TensionLineaIII { get { return Convert.ToDouble(tensionLineaIII) / 100; } }
            public double TensionFaseIII { get { return Convert.ToDouble(tensionFaseIII) / 100; } }
            public double CorrienteIII { get { return Convert.ToDouble(corrienteIII) / 1000; } }
            public double PotenciaActivaIII { get { return Convert.ToDouble(potenciaActivaIII); } }
            public double PotenciaInductivaIII { get { return Convert.ToDouble(potenciaInductivaIII); } }
            public double PotenciaCapacitivaIII { get { return Convert.ToDouble(potenciaCapacitivaIII); } }
            public double PotenciaAparenteIII { get { return Convert.ToDouble(potenciaAparenteIII); } }
            public double FacttorPotenciaIII { get { return Convert.ToDouble(facttorPotenciaIII) / 1000; } }
            public double CosenoPhiIII { get { return Convert.ToDouble(cosenoPhiIII) / 1000; } }
        };

        //*******************************************************


        public override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}
