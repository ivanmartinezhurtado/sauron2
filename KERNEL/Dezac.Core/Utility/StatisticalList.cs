﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dezac.Core.Utility
{
    public class StatisticalList
    {
        public List<StatisticalListItem> series;

        public string Name { get; set; }

        public StatisticalList()
        {
            Name = "StatsList";
            series = new List<StatisticalListItem>();
        }

        public StatisticalList(int numSeries)
        {
            Name = "StatsList";
            series = new List<StatisticalListItem>(numSeries);

            for (int i = 0; i < numSeries; i++)
                series.Add(new StatisticalListItem(Name + i));
        }

        public StatisticalList(params string[] namedSeries)
        {
            Name = "StatsList";
            series = new List<StatisticalListItem>(namedSeries.Length);

            for (int i = 0; i < namedSeries.Length; i++)
                series.Add(new StatisticalListItem(namedSeries[i]));
        }

        public void AddSerie(string name)
        {
            series.Add(new StatisticalListItem(name));
        }

        public void Clear()
        {
            series.ForEach(p => p.Clear());
        }

        public void FirstClear()
        {
            for (int i = 0; i < series.Count; i++)
                series[i].Data.RemoveAt(0);
        }

        public void Add(params double[] values)
        {
            if (values.Length != series.Count)
                throw new Exception("Error cantidad de variables a testear diderentes a las variables a comparar");

            for (int i = 0; i < series.Count; i++)
            {
                series[i].Data.Add(values[i]);

                DataPointHub.Add(this, series[i].Name, values[i], Name);
            }
        }

        public double Average(int serie)
        {
            return series[serie].Data.Average();
        }

        public double[] Averages()
        {
            var list = new List<double>();

            for (int serie = 0; serie < series.Count(); serie++)
                list.Add(series[serie].Data.Average());

            return list.ToArray();
        }

        public double Max(int serie)
        {
            return series[serie].Data.Max();
        }

        public double Min(int serie)
        {
            return series[serie].Data.Min();
        }

        public double Last(int serie)
        {
            return series[serie].Data.Last();
        }

        public long Count(int serie)
        {
            return series[serie].Data.Count();
        }

        public int NumSeries
        {
            get { return series.Count(); }
        }

        public double Variance(int serie)
        {
            return series[serie].Variance();
        }

        public double[] Variances()
        {
            var list = new List<double>();

            for (int serie = 0; serie < series.Count(); serie++)
                list.Add(series[serie].Variance());

            return list.ToArray();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            series.ForEach(p => sb.AppendLine(p.ToString()));

            return sb.ToString();
        }      
    }

    public class StatisticalListItem
    {
        public string Name { get; set; }
        public List<double> Data { get; set; }

        public StatisticalListItem()
        {
            Data = new List<double>();
        }

        public StatisticalListItem(string name)
            : this()
        {
            Name = name;
        }

        public void Clear()
        {
            Data.Clear();
        }

        public double Variance()
        {
            if (Data.Count == 1)
                return 0;

            double m2 = 0.0d;

            for (int i = 0; i < Data.Count; i++)
            {
                double m = Data[i] - Data.Average();
                double mPow2 = m * m;
                m2 += mPow2;
            }

            // variance
            return  (m2 / Data.Count);
        }

        public double DesviacionStandard()
        {
            return Math.Sqrt(Variance());
        }

        public override string ToString()
        {
            return string.Format("Name: {0} --> First: {1} - Last: {2} - Avg: {3} - Max: {4} - Min: {5} - DesvEst:{6} - Count: {7}",
                Name,
                Data.First(),
                Data.Last(),
                Data.Average(),
                Data.Max(),
                Data.Min(),
                DesviacionStandard(),
                Data.Count);
        }
    }
}
