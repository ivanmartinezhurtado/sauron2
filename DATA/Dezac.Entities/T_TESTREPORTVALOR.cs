namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTREPORTVALOR
    {
        public int NUMVALOR { get; set; }
        public int NUMFAMILIA { get; set; }
        public Nullable<int> NUMPRODUCTO { get; set; }
        public Nullable<int> VERSION { get; set; }
        public string IDFASE { get; set; }
        public Nullable<int> NUMPARAM { get; set; }
        public string DESCRIPCION { get; set; }
        public string VALORAPLICADO { get; set; }
        public string TOLERANCIA { get; set; }
        public int NIVEL { get; set; }
        public int ORDEN { get; set; }
        public string VISIBLE { get; set; }
        public Nullable<int> TIPO { get; set; }
        public string UNIDAD { get; set; }
        public Nullable<int> VERSIONREPORT { get; set; }
    
        public virtual FASE FASE { get; set; }
        public virtual T_FAMILIA T_FAMILIA { get; set; }
        public virtual T_PARAMETRO T_PARAMETRO { get; set; }
        public virtual PRODUCTO PRODUCTO { get; set; }
    }
}
