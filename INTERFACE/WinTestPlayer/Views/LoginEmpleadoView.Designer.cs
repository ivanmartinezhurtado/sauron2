﻿namespace WinTestPlayer.Views
{
    partial class LoginEmpleadoView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picLock = new Telerik.WinControls.UI.RadPanel();
            this.imgLock = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtUserName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).BeginInit();
            this.picLock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            this.SuspendLayout();
            // 
            // picLock
            // 
            this.picLock.BackColor = System.Drawing.Color.AliceBlue;
            this.picLock.Controls.Add(this.imgLock);
            this.picLock.Controls.Add(this.pictureBox1);
            this.picLock.Controls.Add(this.txtUserName);
            this.picLock.Controls.Add(this.radLabel3);
            this.picLock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLock.Location = new System.Drawing.Point(0, 0);
            this.picLock.Name = "picLock";
            this.picLock.Size = new System.Drawing.Size(418, 234);
            this.picLock.TabIndex = 6;
            // 
            // imgLock
            // 
            this.imgLock.Image = global::WinTestPlayer.Properties.Resources._lock;
            this.imgLock.Location = new System.Drawing.Point(20, 192);
            this.imgLock.Name = "imgLock";
            this.imgLock.Size = new System.Drawing.Size(32, 32);
            this.imgLock.TabIndex = 19;
            this.imgLock.TabStop = false;
            this.imgLock.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinTestPlayer.Properties.Resources.dezac;
            this.pictureBox1.Location = new System.Drawing.Point(21, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 43);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(151, 136);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(118, 27);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUserName_KeyDown);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel3.Location = new System.Drawing.Point(42, 80);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(350, 26);
            this.radLabel3.TabIndex = 7;
            this.radLabel3.Text = "Introduzca el identificador de empleado";
            // 
            // LoginEmpleadoView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picLock);
            this.Name = "LoginEmpleadoView";
            this.Size = new System.Drawing.Size(418, 234);
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).EndInit();
            this.picLock.ResumeLayout(false);
            this.picLock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel picLock;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadTextBox txtUserName;
        private System.Windows.Forms.PictureBox imgLock;

    }
}
