﻿using System;
using System.Globalization;

namespace Comunications.Message
{
    public class RGRNMessage : HexStringMessage
    {
        private static CultureInfo enUS = new CultureInfo("en-US");

        private bool hasValidator = true;

        public bool HasValidator
        {
            get { return hasValidator; }
            set { hasValidator = value; }
        }

        public static RGRNMessage Create(string format, params object[] args)
        {
            RGRNMessage message = new RGRNMessage { Format = format };

            message.SetArgs(enUS, args);

            return message;
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            if (hasValidator && (response.MessageFrame[0] != 15 || response.MessageFrame[1] != 241))
                throw new Exception("Error validacion trama RGRN");
        }
    }
}
