﻿using Dezac.Tests.Model;

namespace Dezac.Tests.Parameters
{
    public class TestPointsParameters
    {
        private TestModeParameters psl;

        public string Variable { get { return psl.variable; } set { psl.variable = value; } }

        public string Fase { get { return psl.Fase; } set { psl.Fase = value; } }

        public ParamUnidad Unidad { get { return psl.Unidad; } set { psl.Unidad = value; } }

        public TestModeParameters Infinito { get { psl.testPoint = ConstantsParameters.TestPoints.INFINITO; return psl; } }

        public TestModeParameters EnVacio { get { psl.testPoint = ConstantsParameters.TestPoints.VACIO; return psl; } }

        public TestModeParameters ConCarga  { get { psl.testPoint = ConstantsParameters.TestPoints.CARGA; return psl; } }
   
        public TestModeParameters Ajuste  { get { psl.testPoint = ConstantsParameters.TestPoints.AJUSTE; return psl; } }

        public TestModeParameters AjusteDesfase { get { psl.testPoint = ConstantsParameters.TestPoints.AJUSTE_DESFASE; return psl; } }

        public TestModeParameters AjustePotencia { get { psl.testPoint = ConstantsParameters.TestPoints.AJUSTE_POTENCIA; return psl; } }

        public TestModeParameters Verificacion  { get { psl.testPoint = ConstantsParameters.TestPoints.VERIF; return psl; } }

        public TestModeParameters TRIGGER { get { psl.testPoint = ConstantsParameters.TestPoints.TRIGGER; return psl; } }

        public TestModeParameters RTC { get { psl.testPoint = ConstantsParameters.TestPoints.RTC; return psl; } }

        public TestModeParameters TESTER { get { psl.testPoint = ConstantsParameters.TestPoints.TESTER; return psl; } }

        public TestModeParameters PATRON { get { psl.testPoint = ConstantsParameters.TestPoints.PATRON; return psl; } }

        public TestModeParameters CrucePistas { get { psl.testPoint = ConstantsParameters.TestPoints.CRUCE_PISTAS; return psl; } }

        public TestModeParameters Offset { get { psl.testPoint = ConstantsParameters.TestPoints.OFFSET; return psl; } }

        public TestModeParameters Bateria { get { psl.testPoint = ConstantsParameters.TestPoints.BATERIA; return psl; } }

        public string Name { get { return Variable + Fase; } }

        public TestModeParameters TestPoint(string value)
        {
            if (string.IsNullOrEmpty(value))
                value = "";
            else
                value = "_" + value;

            psl.testPoint = value;
            return psl;
        }

        public TestPointsParameters(ParamValueCollection margenes)
        {
            psl = new TestModeParameters(margenes);
            psl.variable = margenes.Name;
        }
    }  
}
