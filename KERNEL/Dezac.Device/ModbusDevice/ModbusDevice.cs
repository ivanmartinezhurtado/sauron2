﻿using Comunications.Message;
using Dezac.Core.Upgrade;
using Dezac.Core.Utility;
using log4net;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dezac.Device
{
    public class ModbusDevice : IDisposable
    {
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(ModbusDevice));
        protected ModbusTransport transport;
        public ModbusDevice()
        {
        }
        public ModbusDevice(ModbusTransport Transport)
        {
            transport = Transport;
        }
        public ModbusTransport Transport
        {
            get { return transport; }
            set { transport = value; }
        }
        public virtual void ClosePort()
        {
        }

        public virtual void OpenPort()
        {
        }

        public byte PerifericNumber { set; get; }
        public bool UseFunction06 { get { return transport.UseFunction06; } set { transport.UseFunction06 = value; } }
        public int TimeOut { set { transport.ReadTimeout = value; } get { return transport.ReadTimeout; } }
        public int Retries { set { transport.Retries = value; } get { return transport.Retries; } }
        public int WaitToRetryMilliseconds { set { transport.WaitToRetryMilliseconds = value; } get { return transport.WaitToRetryMilliseconds; } }

        // COIL REGISTERS FUNCION 01 *******************************
        public byte[] ReadMultipleCoil(ushort address, ushort numberOfPoints = 1) => transport.ReadMultipleCoils(PerifericNumber, address, numberOfPoints);
        public T ReadMultipleCoil<T>() where T : struct =>  transport.ReadMultipleCoils<T>(PerifericNumber);

        public byte ReadCoil(ushort address) => transport.ReadCoil(PerifericNumber, address);

        // DISCRETE INPUTS REGISTERS FUNCION 02 *******************************
        public byte[] ReadMultipleInputs(ushort address, ushort numberOfPoints = 1) => transport.ReadMultipleInputs(PerifericNumber, address, numberOfPoints);
        public byte ReadSinlgeInputs(ushort address) => transport.ReadSinlgeInputs(PerifericNumber, address);
        public T ReadSinlgeInputs<T>() where T : struct => transport.ReadSinlgeInputs<T>(PerifericNumber);

        // INPUT REGISTERS FUNCION 04 *******************************
        public ushort ReadRegister(ushort address) => transport.ReadInputRegisters(PerifericNumber, address, 1).GetRegister(0);
        public int ReadInt32(ushort address) => transport.ReadInt32(PerifericNumber, address);
        public long ReadInt64(ushort address) => transport.ReadInt64(PerifericNumber, address);
        public string ReadString(ushort address, ushort numberOfPoints) => transport.ReadString(PerifericNumber, address, numberOfPoints);
        public string ReadHexString(ushort address, ushort numberOfPoints) => transport.ReadHexString(PerifericNumber, address, numberOfPoints);
        public float16 ReadFloat16(ushort address) => transport.ReadFloat16(PerifericNumber, address);
        public float ReadFloat32(ushort address) => transport.ReadFloat32(PerifericNumber, address);
        public DateTime ReadInt32Epoch(ushort address) => transport.ReadInt32Epoch(PerifericNumber, address);
        public DateTime ReadInt64Epoch(ushort address) => transport.ReadInt64Epoch(PerifericNumber, address);
        public ushort[] ReadMultipleRegister(ushort address, ushort numberOfPoints = 1) => transport.ReadMultipleRegister(PerifericNumber, address, numberOfPoints);
        public byte[] ReadBytes(ushort address, ushort numberOfPoints = 1) => transport.ReadInputRegisters(PerifericNumber, address, numberOfPoints).Data;
        public T Read<T>() where T : struct => transport.ReadInputRegisters<T>(PerifericNumber);
        public T Read<T>(ushort startAddress) where T : struct => transport.ReadInputRegisters<T>(PerifericNumber, startAddress);
        public object Read(Type type, ushort startAddress) => transport.ReadInputRegisters(type, PerifericNumber, startAddress);
        public Task<T> ReadAsync<T>() where T : struct => Task.Run<T>(() => transport.ReadInputRegisters<T>(PerifericNumber));

        // HOLDING REGISTERS FUNCION 03 *****************************
        public ushort ReadHoldingRegister(ushort address) => transport.ReadHoldingRegisters(PerifericNumber, address, 1).GetRegister(0);
        public string ReadStringHoldingRegister(ushort address, ushort numberOfPoints) => transport.ReadStringHoldingRegister(PerifericNumber, address, numberOfPoints);
        public string ReadHexStringHoldingRegister(ushort address, ushort numberOfPoints) => transport.ReadHexStringHoldingRegister(PerifericNumber, address, numberOfPoints);
        public ushort[] ReadMultipleHoldingRegister(ushort address, ushort numberOfPoints = 1) => transport.ReadMultipleHoldingRegister(PerifericNumber, address, numberOfPoints);
        public T ReadHolding<T>() where T : struct => transport.ReadHoldingRegisters<T>(PerifericNumber);
        public T ReadHolding<T>(ushort startAddress) where T : struct => transport.ReadHoldingRegisters<T>(PerifericNumber, startAddress);
        public int ReadInt32HoldingRegisters(ushort address) => transport.ReadInt32HoldingRegister(PerifericNumber, address);
        public long ReadInt64HoldingRegisters(ushort address) => transport.ReadInt64HoldingRegister(PerifericNumber, address);
        public float16 ReadFloat16HoldingRegister(ushort address) => transport.ReadFloat16HoldingRegister(PerifericNumber, address);
        public float ReadFloat32HoldingRegister(ushort address) => transport.ReadFloat32HoldingRegister(PerifericNumber, address);
        public DateTime ReadInt32EpochHoldingRegister(ushort address) => transport.ReadInt32EpochHoldingRegister(PerifericNumber, address);
        public DateTime ReadInt64EpochHoldingRegister(ushort address) => transport.ReadInt64EpochHoldingRegister(PerifericNumber, address);

        // WRITE METHODS ***************************
        public void Write(ushort address, ushort value) => transport.WriteSingleRegister(PerifericNumber, address, value);
        public void WriteInt32(ushort address, int value) => transport.WriteInt32(PerifericNumber, address, value);
        public void WriteMultipleInt32(ushort startAddress, params int[] values) => transport.WriteMultipleInt32(PerifericNumber, startAddress, values);
        public void WriteInt64(ushort address, long value) => transport.WriteInt64(PerifericNumber, address, value);
        public void WriteFloat16(ushort address, Dezac.Core.Utility.float16 value) => transport.WriteFloat16(PerifericNumber, address, value);
        public void WriteFloat32(ushort address, float value) => transport.WriteFloat32(PerifericNumber, address, value);
        public void WriteMultipleCoil<T>(T value) where T : struct => transport.WriteMultipleCoil<T>(PerifericNumber, value);
        public void WriteSingleCoil(ushort startAddress, bool coilState) => transport.WriteSingleCoil(PerifericNumber, startAddress, coilState);
        public void WriteMultipleCoil(ushort startAddress, bool[] coilState) => transport.WriteMultipleCoil(PerifericNumber, startAddress, coilState);
        public void Write<T>(ushort startAddress, T value) where T : struct => transport.WriteInputRegisters<T>(PerifericNumber, startAddress, value);
        public void Write<T>(T value) where T : struct => transport.WriteInputRegisters<T>(PerifericNumber, value);
        public void WriteObject(ushort startAddress, object value) => transport.WriteObject(PerifericNumber, startAddress, value);
        public void WriteBytes(ushort startAddress, byte[] value) => transport.WriteInputRegisters(PerifericNumber, startAddress, value);
        public void WriteString(ushort startAddress, string format, params object[] args) => WriteString(startAddress, string.Format(format, args));
        public void WriteString(ushort startAddress, string text) => transport.WriteString(PerifericNumber, startAddress, text);
        public void WriteStringByRegister(ushort startAddress, string text) => transport.WriteStringByRegister(PerifericNumber, startAddress, text);
        public void WriteHexString(ushort startAddress, string text) => transport.WriteHexString(PerifericNumber, startAddress, text);
        public void WriteFileRecord(ushort fileNumber, byte[] data) => transport.WriteFileRecord(PerifericNumber, fileNumber, data);
        public void WriteFileRecord(byte referenceType, ushort fileNumber, ushort recordNumber, byte[] data) => transport.WriteFileRecord(PerifericNumber, referenceType, fileNumber, recordNumber, data);
        public void WriteInt32Epoch(ushort address, DateTime value) => transport.WriteInt32Epoch(PerifericNumber, address, value);
        public void WriteInt64Epoch(ushort address, DateTime value) => transport.WriteInt64Epoch(PerifericNumber, address, value);
        public void WithTimeOut(Action<ModbusDevice> action, int? timeOut = null, bool throwTimeOutException = false)
        {
            int currentTimeOut = this.TimeOut;

            TimeOut = timeOut.GetValueOrDefault(currentTimeOut);
            try
            {
                action(this);
            }
            catch (TimeoutException)
            {
                if (throwTimeOutException)
                    throw;
            }
            finally
            {
                TimeOut = currentTimeOut;
            }
        }
        public bool UpgradeHexFile(ushort bootregister, ushort loadFirmware, string binaryName)
        {
            Retries = 0;
            _logger.InfoFormat("Cargando binario--- {0}", binaryName);
            var BinaryReader = new IntelHexReader();
            BinaryReader.Load(binaryName);

            try
            {
                _logger.Info("Enviando inicio Boot ...");
                WriteSingleCoil(bootregister, true);
            }
            catch { }

            _logger.Info("Esperando borrado ...");
            Thread.Sleep(6000);

            PerifericNumber = 255;

            if (WriteFile(BinaryReader, 40))
            {
                _logger.Info("Enviando FlagTest ...");
                WriteSingleCoil(0x2AF8, true);
                try
                {
                    _logger.Info("Enviando Cargar nuevo Firmware ...");
                    WriteSingleCoil(loadFirmware, false);
                    Thread.Sleep(3000);
                }
                catch { }

                PerifericNumber = 1;
                _logger.Info("Firmware cargado corrctamente");
                return true;
            }
            else
            {
                PerifericNumber = 1;
                _logger.Error("Error en el upgrade del Firmware");
                return false;
            }
        }
        public bool UpgradeBinFile(string binaryName)
        {
            Retries = 0;
            _logger.InfoFormat("Cargando binario--- {0}", binaryName);
            var BinaryReader = new BinReader();
      
            _logger.Info("Reading lengh blocks ...");
             var lenghtBlocks = ReadRegister(49951);

             _logger.Info("Starting device upgrade ...");
             WriteSingleCoil(39998, true);

             BinaryReader.Load(binaryName, lenghtBlocks);

            _logger.Info("Esperando borrado ...");

            if (WriteFile(BinaryReader, 3))
            {
                _logger.Info("Enviando Cargar nuevo Firmware ...");
                WriteSingleCoil(39999, false);
                Thread.Sleep(3000);

                PerifericNumber = 1;
                _logger.Info("Firmware cargado corrctamente");
                return true;
            }
            else
            {
                PerifericNumber = 1;
                _logger.Error("Error en el upgrade del Firmware");
                return false;
            }
        }
        public bool WriteFile(IBinaryReader BinaryReader, int MAX_FILE_RECORD_RETRIES)
        {
            ushort i = 0;
            int retries = 0;

            while (i < BinaryReader.RecordsCount && retries < MAX_FILE_RECORD_RETRIES)
            {
                _logger.InfoFormat("Enviando registro {0} de {1}", i, BinaryReader.RecordsCount);
                if (WriteFileRecord(BinaryReader.ReadRecord(i), i))
                {
                    i++;
                    retries = 0;
                }
                else
                {
                    _logger.ErrorFormat("Error comunicaciones Reintento {0} de {1}", retries, MAX_FILE_RECORD_RETRIES);
                    retries++;
                }
            }

            return retries < MAX_FILE_RECORD_RETRIES;
        }
        private bool WriteFileRecord(byte[] data, ushort i)
        {
            try
            {
                WriteFileRecord(0x06, 0xFFFF, i, data);
                return true;
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error comunicaciones WriteFileRecord {0}", ex.Message);
                return false;
            }
        }
        public ModbusMessage ReadRequest() => transport.ReadRequest();
        public void WriteRequest(ModbusMessage message) => transport.WriteRequest(message);

        public virtual void Dispose()
        {
            Transport.Dispose();
        }
    }
}

