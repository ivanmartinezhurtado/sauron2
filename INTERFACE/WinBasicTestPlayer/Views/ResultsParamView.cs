﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;
using Telerik.WinControls.UI;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class ResultsParamView : UserControl
    {
        private SynchronizationContext sc;

        public ResultsParamView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            this.sc = SynchronizationContext.Current;

            var app = DI.Resolve<AppService>();
            app.Runner.AllTestStarting += (s, ee) => 
            { 
                bsList.Clear();
                radGridView1.Columns["NumInstance"].IsVisible = ee.NumInstances > 0;
            };
            app.Runner.TestStart += (s, ee) => 
            {
                var testContext = SequenceContext.Current.Services.Get<ITestContext>();
                if (testContext != null)
                    testContext.Resultados.ItemChanged += OnResultAdded;

                SequenceContext.Current.Services.ServiceAdded += OnServiceAdded;
            };
            app.Runner.TestEnd += (s, ee) =>
            {
                SequenceContext.Current.Services.ServiceAdded -= OnServiceAdded;

                var testContext = SequenceContext.Current.Services.Get<ITestContext>();
                if (testContext != null)
                    testContext.Resultados.ItemChanged -= OnResultAdded;
            };
        }

        void OnServiceAdded(object sender, object e)
        {
            var testContext = e as ITestContext;
            if (testContext != null)
                testContext.Resultados.ItemChanged += OnResultAdded;
        }

        public void AddItem(ParamValue item)
        {
            int pos = bsList.IndexOf(item);
            if (pos < 0)
                bsList.Add(item);
            else
                bsList.ResetItem(pos);
        }

        void OnResultAdded(object sender, ParamValue e)
        {
            sc.Post(_ => { this.AddItem(e); }, null);
        }

        private void radGridView1_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.CellElement.ColumnInfo.Name == "Valor")
            {
                ParamValue value = e.Row.DataBoundItem as ParamValue;
                if (value != null && !value.IsValid)
                {
                    e.CellElement.BackColor = Color.Red;
                    e.CellElement.DrawFill = true;
                    e.CellElement.NumberOfColors = 1;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, Telerik.WinControls.ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.NumberOfColorsProperty, Telerik.WinControls.ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.BackColorProperty, Telerik.WinControls.ValueResetFlags.Local);
                }
            }
        }
    }
}
