namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ValorParametroLog")]
    public partial class ValorParametroLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime Fecha { get; set; }
        public int GrupoId { get; set; }
        public int ParametroId { get; set; }
        public int? ProductoId { get; set; }
        
        [StringLength(200)]
        public string ValorAnterior { get; set; }
        [StringLength(200)]
        public string NuevoValor { get; set; }
        [StringLength(30)]
        public string UserId { get; set; }
        public string Notas { get; set; }
    }
}
