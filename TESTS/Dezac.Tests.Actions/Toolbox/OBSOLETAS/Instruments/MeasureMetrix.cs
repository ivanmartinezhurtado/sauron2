﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(MeasureMetrixDescription))]
    

    public class MeasureMetrix : ActionBase
    {
        [Description("Numero de de las primeras muestras que descartamos")]
        [DefaultValue(0)]
        [Category("Configuracion muestras")]
        public int DeleteFirst { get; set; }

        [Description("Numero de muestras")]
        [DefaultValue(3)]
        [Category("Configuracion muestras")]
        public int NumberSamples { get; set; }

        [Description("Tiempo de espera entre muestras en milisegundos")]
        [DefaultValue(500)]
        [Category("Configuracion tiempos")]
        public int WaitTimeBetweenSamples { get; set; }

        [Description("Tiempo de espera antes de realizar la medida en milisegundos")]
        [DefaultValue(500)]
        [Category("Configuracion muestras")]
        public int WaitTimeReadMeasure { get; set; }

        [Description("Magnitud de la medida")]
        [DefaultValue(MTX3293.Functions.VOLT)]
        [Category("Configuracion Multimetro")]
        public MTX3293.Functions MagnitudToMeasure { get; set; }

        [Description("Tipo de medida AC/ DC")]
        [DefaultValue(MTX3293.measureTypes.AC)]
        [Category("Configuracion Multimetro")]
        public MTX3293.measureTypes MeasureType { get; set; }

        [Description("Descripción del nombre con el que se registrara en la BBDD los resultados de este test point")]
        [Category("Resultado")]
        public string TestPointDescription { get; set; }


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            var context = SequenceContext.Current;
            if (context == null)
                throw new Exception("Error. No se ha inicializado el contexto");

            var Shell = context.Services.Get<IShell>();
            if (Shell == null)
                throw new Exception("Error. No se ha inicializado el contexto de Shell");

            var wmi = new WMISerialPortInitialize();
            wmi.Execute("MetrixPort", "Silicon Labs");
            var metrixWMI = GetVariable<string>("MetrixPort");
            var port = Convert.ToByte(metrixWMI.Replace("COM", ""));

            string name = "";
            double min = 0;
            double max = 0;
            ParamUnidad unidad = ParamUnidad.SinUnidad;

            double result;

            using (var tester = new MTX3293())
            {
                tester.PortCom = port;

                switch (MagnitudToMeasure)
                {
                    case MTX3293.Functions.VOLT:
                        name = testBase.Params.V.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Name;
                        min = testBase.Params.V.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Min();
                        max = testBase.Params.V.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Max();
                        unidad = ParamUnidad.V;
                        break;

                    case MTX3293.Functions.CURR:
                        name = testBase.Params.I.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Name;
                        min = testBase.Params.I.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Min();
                        max = testBase.Params.I.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Max();
                        unidad = ParamUnidad.A;
                        break;

                    case MTX3293.Functions.RES:
                        name = testBase.Params.RES.Null.TestPoint(TestPointDescription).Name;
                        min = testBase.Params.RES.Null.TestPoint(TestPointDescription).Min();
                        max = testBase.Params.RES.Null.TestPoint(TestPointDescription).Max();
                        unidad = ParamUnidad.Ohms;
                        break;

                    case MTX3293.Functions.FREQ:
                        name = testBase.Params.FREQ.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Name;
                        min = testBase.Params.V_DC.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Min();
                        max = testBase.Params.V_DC.Null.TestPoint(MeasureType.ToString()).TestPoint(TestPointDescription).Max();
                        unidad = ParamUnidad.Hz;
                        break;

                    default: throw new Exception("Magnitud a medir incorrecta");
                }
                var adjustValueTestPoint = new AdjustValueDef(name, 0, min, max, 0, 0, unidad);

                testBase.TestMeasureBase(adjustValueTestPoint, (step) =>
                {
                    switch (MagnitudToMeasure)
                    {
                        case MTX3293.Functions.VOLT:
                            result = tester.ReadVoltage(MeasureType);
                            break;

                        case MTX3293.Functions.CURR:
                            result = tester.ReadCurrent(MeasureType);
                            break;

                        case MTX3293.Functions.RES:
                            result = tester.ReadResistance();
                            break;

                        case MTX3293.Functions.FREQ:
                            result = tester.ReadFrecuency();
                            break;
                        default:
                            throw new Exception("Magnitud a medir incorrecta");
                    }
                    return result;
                }, DeleteFirst, NumberSamples, WaitTimeBetweenSamples);
            }
        }
    }

    public class MeasureMetrixDescription : ActionDescription
    {
        public override string Name { get { return "MeasureMetrix"; } }
        public override string Description { get { return "Acción para medir con el Multimetro Metrix"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.m_16; } }
    }
}
