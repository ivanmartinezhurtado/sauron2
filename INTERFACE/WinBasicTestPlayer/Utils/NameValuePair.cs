﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinTestPlayer.Utils
{
    public class NameValuePair : NameValuePair<string>
    {
    }

    public class NameValuePair<T> 
    {
        public string Name { get; set; }
        public T Value { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
