﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.PowerSource;
using System;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.03)]
    public class SensorMTResistivoTest : TestBase
    {
        private MTEPPS mte;
        private FLUKE183 tester;
        private MTX3293 mtx;

        private double measurePrevious = 0;

        public void TestInitialitzation()
        {           
            mte = new MTEPPS();
            mte.PortCom = 1;
            tester = new FLUKE183();
            mtx = new MTX3293();
        }

        public void AsignPortFluke(string portFLUKE)
        {
            tester.PortCom = Convert.ToByte(GetVariable<string>(portFLUKE, portFLUKE).Replace("COM", ""));
            tester.WaitTimeMeaure = 1500;
        }

        public void AsignPortMetrix(string portFLUKE)
        {
            mtx.PortCom = Convert.ToByte(GetVariable<string>(portFLUKE, portFLUKE).Replace("COM", ""));
            mtx.WaitTimeMeasure = 1500;
        }

        public void ApplyVoltage()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var ConsignasV = new TriLineValue { L1 = voltage, L2 = 0, L3 = 0 };
            var ConsignasI = new TriLineValue { L1 = 0, L2 = 0, L3 = 0 };
            var freq = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 60, ParamUnidad.Hz);
            var desfase = new TriLineValue { L1 = 0, L2 = 0, L3 = 0 };
            mte.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, freq, 0, desfase, true);
        }

        public void MeasureVoltageNoSwitchFLUKE()
        {
            var defs = new AdjustValueDef(Params.V_AC.Null.TestPoint("NO_SWITCH").Name, 0, Params.V_AC.Null.TestPoint("NO_SWITCH").Min(), Params.V_AC.Null.TestPoint("NO_SWITCH").Max(), 0, 0, ParamUnidad.mV);
            TestMeasureBase(defs, (step) =>
            {
                var value = tester.ReadVoltage(Instruments.Measure.FLUKE183.TypeMeasure.AC, FLUKE183.MeasureData.RangeVoltage._5V).Value * 1000;
                measurePrevious = value ;
                return value;
            }, 1, 5, 500);
        }
        public void MeasureVoltageNoSwitch()
        {
            var defs = new AdjustValueDef(Params.V_AC.Null.TestPoint("NO_SWITCH").Name, 0, Params.V_AC.Null.TestPoint("NO_SWITCH").Min(), Params.V_AC.Null.TestPoint("NO_SWITCH").Max(), 0, 0, ParamUnidad.mV);
            TestMeasureBase(defs, (step) =>
            {
                var value = mtx.ReadVoltage(MTX3293.measureTypes.AC)*1000;
                measurePrevious = value;
                return value;
            }, 0, 5, 500);
        }

        public void MeasureVoltageOutFLUKE(string nameTestPoint)
        {
            var defs = new AdjustValueDef(Params.V_AC.Null.TestPoint(nameTestPoint).Name, 0, Params.V_AC.Null.TestPoint(nameTestPoint).Min(), Params.V_AC.Null.TestPoint(nameTestPoint).Max(), 0, 0, ParamUnidad.mV);

            var tempPrevious = measurePrevious;  
         
            TestMeasureBase(defs, (step) =>
            {
                var value = tester.ReadVoltage(Instruments.Measure.FLUKE183.TypeMeasure.AC, FLUKE183.MeasureData.RangeVoltage._5V).Value * 1000;
                measurePrevious = value;
                return  tempPrevious - value;
            }, 1, 5, 500);
            
        }
        public void MeasureVoltageOut(string nameTestPoint)
        {
            var defs = new AdjustValueDef(Params.V_AC.Null.TestPoint(nameTestPoint).Name, 0, Params.V_AC.Null.TestPoint(nameTestPoint).Min(), Params.V_AC.Null.TestPoint(nameTestPoint).Max(), 0, 0, ParamUnidad.mV);

            var tempPrevious = measurePrevious;

            TestMeasureBase(defs, (step) =>
            {
                var value = mtx.ReadVoltage(MTX3293.measureTypes.AC)*1000;
                measurePrevious = value;
                return tempPrevious - value;
            }, 0, 5, 500);

        }

        public void TestFinish()
        {
            if (tester != null)
                tester.Dispose();
            if (mtx != null)
                mtx.Dispose();
            if (mte != null)
            {
                mte.ApplyOffAndWaitStabilisation();
                mte.Dispose();
            }
        }

    }

    


}
