﻿using Instruments.Router;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(DeleteRouterDHCPDescription))]
    
    public class DeleteRouterDHCP : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del usuario en el router")]
        [DefaultValue("root")]
        public string UserName { get; set; }

        [Category("3. Configuration")]
        [Description("Contraseña")]
        [DefaultValue("dezac2309")]
        public string Password { get; set; }

        public override void Execute()
        {
            Router router = Router.Default();

            if (!string.IsNullOrEmpty(UserName))
            {
                router.UserName = UserName;
                router.Password = Password;
            }

            router.DeleteAllDHCPEntry(true);
        }
    }

    public class DeleteRouterDHCPDescription : ActionDescription
    {
        public override string Name { get { return "DeleteRouterDHCPClients"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override string Description { get { return "Action para borrar el router DHCP"; } }
        public override Image Icon { get { return Properties.Resources.router_icon; } }

    }
}
