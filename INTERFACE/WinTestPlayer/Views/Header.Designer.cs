﻿namespace WinTestPlayer.Views
{
    partial class Header
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Header));
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.lblCationMatricula = new System.Windows.Forms.Label();
            this.btNumSerieLabel = new Telerik.WinControls.UI.RadButton();
            this.lblMatricula = new System.Windows.Forms.Label();
            this.imageAssignedNumSerie = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btInstrucciones = new Telerik.WinControls.UI.RadButton();
            this.btPlay = new Telerik.WinControls.UI.RadButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblProducto = new System.Windows.Forms.Label();
            this.lblOperario = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFase = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblNumOrden = new System.Windows.Forms.Label();
            this.lblOrden = new System.Windows.Forms.Label();
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c = new Telerik.WinControls.RootRadElement();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btNumSerieLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageAssignedNumSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btInstrucciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPlay)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLeft
            // 
            this.pnlLeft.BackColor = System.Drawing.Color.LightCoral;
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(50, 96);
            this.pnlLeft.TabIndex = 0;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.lblCationMatricula);
            this.radPanel1.Controls.Add(this.btNumSerieLabel);
            this.radPanel1.Controls.Add(this.lblMatricula);
            this.radPanel1.Controls.Add(this.imageAssignedNumSerie);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.label8);
            this.radPanel1.Controls.Add(this.btInstrucciones);
            this.radPanel1.Controls.Add(this.btPlay);
            this.radPanel1.Controls.Add(this.lblProducto);
            this.radPanel1.Controls.Add(this.lblOperario);
            this.radPanel1.Controls.Add(this.label2);
            this.radPanel1.Controls.Add(this.lblFase);
            this.radPanel1.Controls.Add(this.label11);
            this.radPanel1.Controls.Add(this.lblQty);
            this.radPanel1.Controls.Add(this.lblCantidad);
            this.radPanel1.Controls.Add(this.label6);
            this.radPanel1.Controls.Add(this.lblDesc);
            this.radPanel1.Controls.Add(this.lblNumOrden);
            this.radPanel1.Controls.Add(this.lblOrden);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(50, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1230, 96);
            this.radPanel1.TabIndex = 35;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // lblCationMatricula
            // 
            this.lblCationMatricula.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCationMatricula.AutoSize = true;
            this.lblCationMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCationMatricula.ForeColor = System.Drawing.Color.Navy;
            this.lblCationMatricula.Location = new System.Drawing.Point(644, 9);
            this.lblCationMatricula.Name = "lblCationMatricula";
            this.lblCationMatricula.Size = new System.Drawing.Size(72, 16);
            this.lblCationMatricula.TabIndex = 59;
            this.lblCationMatricula.Text = "Matriculas:";
            this.lblCationMatricula.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btNumSerieLabel
            // 
            this.btNumSerieLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btNumSerieLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNumSerieLabel.Image = ((System.Drawing.Image)(resources.GetObject("btNumSerieLabel.Image")));
            this.btNumSerieLabel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btNumSerieLabel.Location = new System.Drawing.Point(1017, 10);
            this.btNumSerieLabel.Name = "btNumSerieLabel";
            this.btNumSerieLabel.Size = new System.Drawing.Size(75, 73);
            this.btNumSerieLabel.TabIndex = 51;
            this.btNumSerieLabel.Text = "Etiquetas";
            this.btNumSerieLabel.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btNumSerieLabel.Click += new System.EventHandler(this.btNumSerieLabel_Click);
            // 
            // lblMatricula
            // 
            this.lblMatricula.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMatricula.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatricula.ForeColor = System.Drawing.Color.Black;
            this.lblMatricula.Location = new System.Drawing.Point(649, 29);
            this.lblMatricula.Name = "lblMatricula";
            this.lblMatricula.Size = new System.Drawing.Size(264, 54);
            this.lblMatricula.TabIndex = 60;
            this.lblMatricula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // imageAssignedNumSerie
            // 
            this.imageAssignedNumSerie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.imageAssignedNumSerie.Image = ((System.Drawing.Image)(resources.GetObject("imageAssignedNumSerie.Image")));
            this.imageAssignedNumSerie.Location = new System.Drawing.Point(1191, 51);
            this.imageAssignedNumSerie.Name = "imageAssignedNumSerie";
            this.imageAssignedNumSerie.Size = new System.Drawing.Size(32, 32);
            this.imageAssignedNumSerie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imageAssignedNumSerie.TabIndex = 61;
            this.imageAssignedNumSerie.TabStop = false;
            this.imageAssignedNumSerie.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(17, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 38);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 54;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(178, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 53;
            this.label8.Text = "Descripción";
            // 
            // btInstrucciones
            // 
            this.btInstrucciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btInstrucciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInstrucciones.Image = ((System.Drawing.Image)(resources.GetObject("btInstrucciones.Image")));
            this.btInstrucciones.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btInstrucciones.Location = new System.Drawing.Point(1104, 10);
            this.btInstrucciones.Name = "btInstrucciones";
            this.btInstrucciones.Size = new System.Drawing.Size(75, 73);
            this.btInstrucciones.TabIndex = 52;
            this.btInstrucciones.TabStop = false;
            this.btInstrucciones.Text = "Instrucciones";
            this.btInstrucciones.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btInstrucciones.Click += new System.EventHandler(this.btInstrucciones_Click);
            // 
            // btPlay
            // 
            this.btPlay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btPlay.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btPlay.Enabled = false;
            this.btPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPlay.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btPlay.ImageIndex = 2;
            this.btPlay.ImageList = this.imageList1;
            this.btPlay.Location = new System.Drawing.Point(930, 10);
            this.btPlay.Name = "btPlay";
            this.btPlay.Size = new System.Drawing.Size(75, 73);
            this.btPlay.TabIndex = 28;
            this.btPlay.TabStop = false;
            this.btPlay.Text = "Run";
            this.btPlay.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.btPlay.Click += new System.EventHandler(this.btPlay_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Crystal_Clear_action_apply128x128.png");
            this.imageList1.Images.SetKeyName(1, "Crystal_Clear_action_button_cancel.png");
            this.imageList1.Images.SetKeyName(2, "Play-Normal-icon.png");
            this.imageList1.Images.SetKeyName(3, "Stop-Normal-Red-icon.png");
            // 
            // lblProducto
            // 
            this.lblProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProducto.ForeColor = System.Drawing.Color.Black;
            this.lblProducto.Location = new System.Drawing.Point(267, 29);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(87, 17);
            this.lblProducto.TabIndex = 45;
            this.lblProducto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOperario
            // 
            this.lblOperario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperario.Location = new System.Drawing.Point(21, 72);
            this.lblOperario.Name = "lblOperario";
            this.lblOperario.Size = new System.Drawing.Size(141, 17);
            this.lblOperario.TabIndex = 44;
            this.lblOperario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(14, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 43;
            this.label2.Text = "Operario";
            // 
            // lblFase
            // 
            this.lblFase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFase.ForeColor = System.Drawing.Color.Black;
            this.lblFase.Location = new System.Drawing.Point(480, 29);
            this.lblFase.Name = "lblFase";
            this.lblFase.Size = new System.Drawing.Size(141, 18);
            this.lblFase.TabIndex = 42;
            this.lblFase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(480, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 16);
            this.label11.TabIndex = 41;
            this.label11.Text = "Fase";
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQty.ForeColor = System.Drawing.Color.Navy;
            this.lblQty.Location = new System.Drawing.Point(380, 8);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(62, 16);
            this.lblQty.TabIndex = 40;
            this.lblQty.Text = "Cantidad";
            // 
            // lblCantidad
            // 
            this.lblCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.ForeColor = System.Drawing.Color.Black;
            this.lblCantidad.Location = new System.Drawing.Point(380, 29);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(53, 17);
            this.lblCantidad.TabIndex = 39;
            this.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(267, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 38;
            this.label6.Text = "Producto";
            // 
            // lblDesc
            // 
            this.lblDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.ForeColor = System.Drawing.Color.Black;
            this.lblDesc.Location = new System.Drawing.Point(178, 69);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(443, 19);
            this.lblDesc.TabIndex = 37;
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumOrden
            // 
            this.lblNumOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumOrden.ForeColor = System.Drawing.Color.Black;
            this.lblNumOrden.Location = new System.Drawing.Point(178, 29);
            this.lblNumOrden.Name = "lblNumOrden";
            this.lblNumOrden.Size = new System.Drawing.Size(58, 17);
            this.lblNumOrden.TabIndex = 36;
            this.lblNumOrden.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOrden
            // 
            this.lblOrden.AutoSize = true;
            this.lblOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrden.ForeColor = System.Drawing.Color.Navy;
            this.lblOrden.Location = new System.Drawing.Point(178, 8);
            this.lblOrden.Name = "lblOrden";
            this.lblOrden.Size = new System.Drawing.Size(45, 16);
            this.lblOrden.TabIndex = 35;
            this.lblOrden.Text = "Orden";
            // 
            // object_9ab53d85_09c9_4d32_bf57_92280561d02c
            // 
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c.Name = "object_9ab53d85_09c9_4d32_bf57_92280561d02c";
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c.StretchHorizontally = true;
            this.object_9ab53d85_09c9_4d32_bf57_92280561d02c.StretchVertically = true;
            // 
            // Header
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.pnlLeft);
            this.Name = "Header";
            this.Size = new System.Drawing.Size(1280, 96);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btNumSerieLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageAssignedNumSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btInstrucciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btPlay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeft;
        private Telerik.WinControls.UI.RadButton btPlay;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.Label lblOperario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFase;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Label lblNumOrden;
        private System.Windows.Forms.Label lblOrden;
        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.RootRadElement object_9ab53d85_09c9_4d32_bf57_92280561d02c;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadButton btInstrucciones;
        private Telerik.WinControls.UI.RadButton btNumSerieLabel;
        private System.Windows.Forms.Label lblMatricula;
        private System.Windows.Forms.Label lblCationMatricula;
        private System.Windows.Forms.PictureBox imageAssignedNumSerie;
    }
}
