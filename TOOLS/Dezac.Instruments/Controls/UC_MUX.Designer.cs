﻿namespace Dezac.Instruments.Controls
{
    partial class UC_MUX
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC_MUX));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.InMux16 = new System.Windows.Forms.Button();
            this.ImageListIO = new System.Windows.Forms.ImageList(this.components);
            this.InMux15 = new System.Windows.Forms.Button();
            this.InMux14 = new System.Windows.Forms.Button();
            this.InMux13 = new System.Windows.Forms.Button();
            this.InMux12 = new System.Windows.Forms.Button();
            this.InMux11 = new System.Windows.Forms.Button();
            this.InMux10 = new System.Windows.Forms.Button();
            this.InMux9 = new System.Windows.Forms.Button();
            this.InMux8 = new System.Windows.Forms.Button();
            this.InMux7 = new System.Windows.Forms.Button();
            this.InMux6 = new System.Windows.Forms.Button();
            this.InMux5 = new System.Windows.Forms.Button();
            this.InMux4 = new System.Windows.Forms.Button();
            this.InMux3 = new System.Windows.Forms.Button();
            this.InMux2 = new System.Windows.Forms.Button();
            this.InMux1 = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.btnApply = new System.Windows.Forms.Button();
            this.b_MUX_Reset = new System.Windows.Forms.Button();
            this.Label349 = new System.Windows.Forms.Label();
            this.lbl_CANALB = new System.Windows.Forms.Label();
            this.Label351 = new System.Windows.Forms.Label();
            this.lbl_CANALA = new System.Windows.Forms.Label();
            this.OutMux2 = new System.Windows.Forms.Button();
            this.OutMux1 = new System.Windows.Forms.Button();
            this.OutMux0 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Panel1.Controls.Add(this.InMux16);
            this.splitContainer1.Panel1.Controls.Add(this.InMux15);
            this.splitContainer1.Panel1.Controls.Add(this.InMux14);
            this.splitContainer1.Panel1.Controls.Add(this.InMux13);
            this.splitContainer1.Panel1.Controls.Add(this.InMux12);
            this.splitContainer1.Panel1.Controls.Add(this.InMux11);
            this.splitContainer1.Panel1.Controls.Add(this.InMux10);
            this.splitContainer1.Panel1.Controls.Add(this.InMux9);
            this.splitContainer1.Panel1.Controls.Add(this.InMux8);
            this.splitContainer1.Panel1.Controls.Add(this.InMux7);
            this.splitContainer1.Panel1.Controls.Add(this.InMux6);
            this.splitContainer1.Panel1.Controls.Add(this.InMux5);
            this.splitContainer1.Panel1.Controls.Add(this.InMux4);
            this.splitContainer1.Panel1.Controls.Add(this.InMux3);
            this.splitContainer1.Panel1.Controls.Add(this.InMux2);
            this.splitContainer1.Panel1.Controls.Add(this.InMux1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(1);
            this.splitContainer1.Size = new System.Drawing.Size(846, 209);
            this.splitContainer1.SplitterDistance = 399;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // InMux16
            // 
            this.InMux16.BackColor = System.Drawing.Color.Transparent;
            this.InMux16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux16.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux16.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux16.FlatAppearance.BorderSize = 0;
            this.InMux16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux16.ImageIndex = 1;
            this.InMux16.ImageList = this.ImageListIO;
            this.InMux16.Location = new System.Drawing.Point(307, 158);
            this.InMux16.Margin = new System.Windows.Forms.Padding(4);
            this.InMux16.Name = "InMux16";
            this.InMux16.Size = new System.Drawing.Size(83, 31);
            this.InMux16.TabIndex = 31;
            this.InMux16.TabStop = false;
            this.InMux16.Text = "In16";
            this.InMux16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux16.UseVisualStyleBackColor = false;
            this.InMux16.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux16.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux16.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // ImageListIO
            // 
            this.ImageListIO.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListIO.ImageStream")));
            this.ImageListIO.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListIO.Images.SetKeyName(0, "btnGreen.png");
            this.ImageListIO.Images.SetKeyName(1, "btnRed.png");
            this.ImageListIO.Images.SetKeyName(2, "btnYellow.png");
            this.ImageListIO.Images.SetKeyName(3, "btnGray.png");
            // 
            // InMux15
            // 
            this.InMux15.BackColor = System.Drawing.Color.Transparent;
            this.InMux15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux15.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux15.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux15.FlatAppearance.BorderSize = 0;
            this.InMux15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux15.ImageIndex = 1;
            this.InMux15.ImageList = this.ImageListIO;
            this.InMux15.Location = new System.Drawing.Point(307, 110);
            this.InMux15.Margin = new System.Windows.Forms.Padding(4);
            this.InMux15.Name = "InMux15";
            this.InMux15.Size = new System.Drawing.Size(83, 31);
            this.InMux15.TabIndex = 30;
            this.InMux15.TabStop = false;
            this.InMux15.Text = "In15";
            this.InMux15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux15.UseVisualStyleBackColor = false;
            this.InMux15.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux15.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux15.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux14
            // 
            this.InMux14.BackColor = System.Drawing.Color.Transparent;
            this.InMux14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux14.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux14.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux14.FlatAppearance.BorderSize = 0;
            this.InMux14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux14.ImageIndex = 1;
            this.InMux14.ImageList = this.ImageListIO;
            this.InMux14.Location = new System.Drawing.Point(307, 62);
            this.InMux14.Margin = new System.Windows.Forms.Padding(4);
            this.InMux14.Name = "InMux14";
            this.InMux14.Size = new System.Drawing.Size(83, 31);
            this.InMux14.TabIndex = 29;
            this.InMux14.TabStop = false;
            this.InMux14.Text = "In14";
            this.InMux14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux14.UseVisualStyleBackColor = false;
            this.InMux14.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux14.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux14.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux13
            // 
            this.InMux13.BackColor = System.Drawing.Color.Transparent;
            this.InMux13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux13.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux13.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux13.FlatAppearance.BorderSize = 0;
            this.InMux13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux13.ImageIndex = 1;
            this.InMux13.ImageList = this.ImageListIO;
            this.InMux13.Location = new System.Drawing.Point(307, 14);
            this.InMux13.Margin = new System.Windows.Forms.Padding(4);
            this.InMux13.Name = "InMux13";
            this.InMux13.Size = new System.Drawing.Size(83, 31);
            this.InMux13.TabIndex = 28;
            this.InMux13.TabStop = false;
            this.InMux13.Text = "In13";
            this.InMux13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux13.UseVisualStyleBackColor = false;
            this.InMux13.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux13.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux13.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux12
            // 
            this.InMux12.BackColor = System.Drawing.Color.Transparent;
            this.InMux12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux12.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux12.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux12.FlatAppearance.BorderSize = 0;
            this.InMux12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux12.ImageIndex = 1;
            this.InMux12.ImageList = this.ImageListIO;
            this.InMux12.Location = new System.Drawing.Point(210, 158);
            this.InMux12.Margin = new System.Windows.Forms.Padding(4);
            this.InMux12.Name = "InMux12";
            this.InMux12.Size = new System.Drawing.Size(83, 31);
            this.InMux12.TabIndex = 27;
            this.InMux12.TabStop = false;
            this.InMux12.Text = "In12";
            this.InMux12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux12.UseVisualStyleBackColor = false;
            this.InMux12.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux12.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux12.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux11
            // 
            this.InMux11.BackColor = System.Drawing.Color.Transparent;
            this.InMux11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux11.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux11.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux11.FlatAppearance.BorderSize = 0;
            this.InMux11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux11.ImageIndex = 1;
            this.InMux11.ImageList = this.ImageListIO;
            this.InMux11.Location = new System.Drawing.Point(210, 110);
            this.InMux11.Margin = new System.Windows.Forms.Padding(4);
            this.InMux11.Name = "InMux11";
            this.InMux11.Size = new System.Drawing.Size(83, 31);
            this.InMux11.TabIndex = 26;
            this.InMux11.TabStop = false;
            this.InMux11.Text = "In11";
            this.InMux11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux11.UseVisualStyleBackColor = false;
            this.InMux11.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux11.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux11.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux10
            // 
            this.InMux10.BackColor = System.Drawing.Color.Transparent;
            this.InMux10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux10.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux10.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux10.FlatAppearance.BorderSize = 0;
            this.InMux10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux10.ImageIndex = 1;
            this.InMux10.ImageList = this.ImageListIO;
            this.InMux10.Location = new System.Drawing.Point(210, 62);
            this.InMux10.Margin = new System.Windows.Forms.Padding(4);
            this.InMux10.Name = "InMux10";
            this.InMux10.Size = new System.Drawing.Size(83, 31);
            this.InMux10.TabIndex = 25;
            this.InMux10.TabStop = false;
            this.InMux10.Text = "In10";
            this.InMux10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux10.UseVisualStyleBackColor = false;
            this.InMux10.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux10.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux10.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux9
            // 
            this.InMux9.BackColor = System.Drawing.Color.Transparent;
            this.InMux9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux9.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux9.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux9.FlatAppearance.BorderSize = 0;
            this.InMux9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux9.ImageIndex = 1;
            this.InMux9.ImageList = this.ImageListIO;
            this.InMux9.Location = new System.Drawing.Point(210, 14);
            this.InMux9.Margin = new System.Windows.Forms.Padding(4);
            this.InMux9.Name = "InMux9";
            this.InMux9.Size = new System.Drawing.Size(83, 31);
            this.InMux9.TabIndex = 24;
            this.InMux9.TabStop = false;
            this.InMux9.Text = "In9";
            this.InMux9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux9.UseVisualStyleBackColor = false;
            this.InMux9.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux9.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux9.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux8
            // 
            this.InMux8.BackColor = System.Drawing.Color.Transparent;
            this.InMux8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux8.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux8.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux8.FlatAppearance.BorderSize = 0;
            this.InMux8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux8.ImageIndex = 1;
            this.InMux8.ImageList = this.ImageListIO;
            this.InMux8.Location = new System.Drawing.Point(111, 158);
            this.InMux8.Margin = new System.Windows.Forms.Padding(4);
            this.InMux8.Name = "InMux8";
            this.InMux8.Size = new System.Drawing.Size(83, 31);
            this.InMux8.TabIndex = 23;
            this.InMux8.TabStop = false;
            this.InMux8.Text = "In8";
            this.InMux8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux8.UseVisualStyleBackColor = false;
            this.InMux8.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux8.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux8.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux7
            // 
            this.InMux7.BackColor = System.Drawing.Color.Transparent;
            this.InMux7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux7.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux7.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux7.FlatAppearance.BorderSize = 0;
            this.InMux7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux7.ImageIndex = 1;
            this.InMux7.ImageList = this.ImageListIO;
            this.InMux7.Location = new System.Drawing.Point(111, 110);
            this.InMux7.Margin = new System.Windows.Forms.Padding(4);
            this.InMux7.Name = "InMux7";
            this.InMux7.Size = new System.Drawing.Size(83, 31);
            this.InMux7.TabIndex = 22;
            this.InMux7.TabStop = false;
            this.InMux7.Text = "In7";
            this.InMux7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux7.UseVisualStyleBackColor = false;
            this.InMux7.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux7.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux7.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux6
            // 
            this.InMux6.BackColor = System.Drawing.Color.Transparent;
            this.InMux6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux6.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux6.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux6.FlatAppearance.BorderSize = 0;
            this.InMux6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux6.ImageIndex = 1;
            this.InMux6.ImageList = this.ImageListIO;
            this.InMux6.Location = new System.Drawing.Point(111, 62);
            this.InMux6.Margin = new System.Windows.Forms.Padding(4);
            this.InMux6.Name = "InMux6";
            this.InMux6.Size = new System.Drawing.Size(83, 31);
            this.InMux6.TabIndex = 21;
            this.InMux6.TabStop = false;
            this.InMux6.Text = "In6";
            this.InMux6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux6.UseVisualStyleBackColor = false;
            this.InMux6.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux6.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux6.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux5
            // 
            this.InMux5.BackColor = System.Drawing.Color.Transparent;
            this.InMux5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux5.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux5.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux5.FlatAppearance.BorderSize = 0;
            this.InMux5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux5.ImageIndex = 1;
            this.InMux5.ImageList = this.ImageListIO;
            this.InMux5.Location = new System.Drawing.Point(111, 14);
            this.InMux5.Margin = new System.Windows.Forms.Padding(4);
            this.InMux5.Name = "InMux5";
            this.InMux5.Size = new System.Drawing.Size(83, 31);
            this.InMux5.TabIndex = 20;
            this.InMux5.TabStop = false;
            this.InMux5.Text = "In5";
            this.InMux5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux5.UseVisualStyleBackColor = false;
            this.InMux5.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux5.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux5.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux4
            // 
            this.InMux4.BackColor = System.Drawing.Color.Transparent;
            this.InMux4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux4.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux4.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux4.FlatAppearance.BorderSize = 0;
            this.InMux4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux4.ImageIndex = 1;
            this.InMux4.ImageList = this.ImageListIO;
            this.InMux4.Location = new System.Drawing.Point(13, 158);
            this.InMux4.Margin = new System.Windows.Forms.Padding(4);
            this.InMux4.Name = "InMux4";
            this.InMux4.Size = new System.Drawing.Size(83, 31);
            this.InMux4.TabIndex = 19;
            this.InMux4.TabStop = false;
            this.InMux4.Text = "In4";
            this.InMux4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux4.UseVisualStyleBackColor = false;
            this.InMux4.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux4.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux4.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux3
            // 
            this.InMux3.BackColor = System.Drawing.Color.Transparent;
            this.InMux3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux3.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux3.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux3.FlatAppearance.BorderSize = 0;
            this.InMux3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux3.ImageIndex = 1;
            this.InMux3.ImageList = this.ImageListIO;
            this.InMux3.Location = new System.Drawing.Point(13, 110);
            this.InMux3.Margin = new System.Windows.Forms.Padding(4);
            this.InMux3.Name = "InMux3";
            this.InMux3.Size = new System.Drawing.Size(83, 31);
            this.InMux3.TabIndex = 18;
            this.InMux3.TabStop = false;
            this.InMux3.Text = "In3";
            this.InMux3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux3.UseVisualStyleBackColor = false;
            this.InMux3.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux3.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux3.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux2
            // 
            this.InMux2.BackColor = System.Drawing.Color.Transparent;
            this.InMux2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux2.Cursor = System.Windows.Forms.Cursors.Default;
            this.InMux2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux2.FlatAppearance.BorderSize = 0;
            this.InMux2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux2.ImageIndex = 1;
            this.InMux2.ImageList = this.ImageListIO;
            this.InMux2.Location = new System.Drawing.Point(13, 62);
            this.InMux2.Margin = new System.Windows.Forms.Padding(4);
            this.InMux2.Name = "InMux2";
            this.InMux2.Size = new System.Drawing.Size(83, 31);
            this.InMux2.TabIndex = 17;
            this.InMux2.TabStop = false;
            this.InMux2.Text = "In2";
            this.InMux2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux2.UseVisualStyleBackColor = false;
            this.InMux2.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux2.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux2.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // InMux1
            // 
            this.InMux1.BackColor = System.Drawing.Color.Transparent;
            this.InMux1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.InMux1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.InMux1.FlatAppearance.BorderSize = 0;
            this.InMux1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.InMux1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.InMux1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InMux1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InMux1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InMux1.ImageIndex = 1;
            this.InMux1.ImageList = this.ImageListIO;
            this.InMux1.Location = new System.Drawing.Point(13, 14);
            this.InMux1.Margin = new System.Windows.Forms.Padding(4);
            this.InMux1.Name = "InMux1";
            this.InMux1.Size = new System.Drawing.Size(83, 31);
            this.InMux1.TabIndex = 16;
            this.InMux1.TabStop = false;
            this.InMux1.Text = "In1";
            this.InMux1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.InMux1.UseVisualStyleBackColor = false;
            this.InMux1.Click += new System.EventHandler(this.SelecEntrada);
            this.InMux1.MouseEnter += new System.EventHandler(this.btnMouseEnter);
            this.InMux1.MouseLeave += new System.EventHandler(this.btnMouseLeave);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer2.Panel1.Controls.Add(this.btnApply);
            this.splitContainer2.Panel1.Controls.Add(this.b_MUX_Reset);
            this.splitContainer2.Panel1.Controls.Add(this.Label349);
            this.splitContainer2.Panel1.Controls.Add(this.lbl_CANALB);
            this.splitContainer2.Panel1.Controls.Add(this.Label351);
            this.splitContainer2.Panel1.Controls.Add(this.lbl_CANALA);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer2.Panel2.Controls.Add(this.OutMux2);
            this.splitContainer2.Panel2.Controls.Add(this.OutMux1);
            this.splitContainer2.Panel2.Controls.Add(this.OutMux0);
            this.splitContainer2.Size = new System.Drawing.Size(442, 209);
            this.splitContainer2.SplitterDistance = 140;
            this.splitContainer2.SplitterWidth = 5;
            this.splitContainer2.TabIndex = 0;
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.CadetBlue;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.ForeColor = System.Drawing.Color.White;
            this.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.Location = new System.Drawing.Point(38, 163);
            this.btnApply.Margin = new System.Windows.Forms.Padding(4);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(101, 37);
            this.btnApply.TabIndex = 112;
            this.btnApply.TabStop = false;
            this.btnApply.Text = "APPLY";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // b_MUX_Reset
            // 
            this.b_MUX_Reset.BackColor = System.Drawing.Color.CadetBlue;
            this.b_MUX_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b_MUX_Reset.ForeColor = System.Drawing.Color.White;
            this.b_MUX_Reset.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.b_MUX_Reset.Location = new System.Drawing.Point(38, 2);
            this.b_MUX_Reset.Margin = new System.Windows.Forms.Padding(4);
            this.b_MUX_Reset.Name = "b_MUX_Reset";
            this.b_MUX_Reset.Size = new System.Drawing.Size(101, 29);
            this.b_MUX_Reset.TabIndex = 111;
            this.b_MUX_Reset.TabStop = false;
            this.b_MUX_Reset.Text = "RESET";
            this.b_MUX_Reset.UseVisualStyleBackColor = false;
            this.b_MUX_Reset.Click += new System.EventHandler(this.b_MUX_Reset_Click);
            // 
            // Label349
            // 
            this.Label349.BackColor = System.Drawing.Color.Azure;
            this.Label349.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label349.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label349.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Label349.Location = new System.Drawing.Point(19, 99);
            this.Label349.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label349.Name = "Label349";
            this.Label349.Size = new System.Drawing.Size(138, 25);
            this.Label349.TabIndex = 110;
            this.Label349.Text = "CANAL B";
            this.Label349.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CANALB
            // 
            this.lbl_CANALB.BackColor = System.Drawing.Color.Azure;
            this.lbl_CANALB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANALB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CANALB.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_CANALB.Location = new System.Drawing.Point(19, 136);
            this.lbl_CANALB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_CANALB.Name = "lbl_CANALB";
            this.lbl_CANALB.Size = new System.Drawing.Size(140, 23);
            this.lbl_CANALB.TabIndex = 109;
            this.lbl_CANALB.Text = "LIBRE";
            this.lbl_CANALB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label351
            // 
            this.Label351.BackColor = System.Drawing.Color.Azure;
            this.Label351.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label351.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label351.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Label351.Location = new System.Drawing.Point(19, 35);
            this.Label351.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label351.Name = "Label351";
            this.Label351.Size = new System.Drawing.Size(138, 25);
            this.Label351.TabIndex = 108;
            this.Label351.Text = "CANAL A";
            this.Label351.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_CANALA
            // 
            this.lbl_CANALA.BackColor = System.Drawing.Color.Azure;
            this.lbl_CANALA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_CANALA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CANALA.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_CANALA.Location = new System.Drawing.Point(19, 68);
            this.lbl_CANALA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_CANALA.Name = "lbl_CANALA";
            this.lbl_CANALA.Size = new System.Drawing.Size(140, 23);
            this.lbl_CANALA.TabIndex = 107;
            this.lbl_CANALA.Text = "LIBRE";
            this.lbl_CANALA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OutMux2
            // 
            this.OutMux2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OutMux2.BackColor = System.Drawing.Color.Transparent;
            this.OutMux2.BackgroundImage = global::Dezac.Instruments.Properties.Resources.HP53131A1;
            this.OutMux2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.OutMux2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.OutMux2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OutMux2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutMux2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.OutMux2.ImageIndex = 1;
            this.OutMux2.ImageList = this.ImageListIO;
            this.OutMux2.Location = new System.Drawing.Point(19, 145);
            this.OutMux2.Margin = new System.Windows.Forms.Padding(4);
            this.OutMux2.Name = "OutMux2";
            this.OutMux2.Size = new System.Drawing.Size(154, 61);
            this.OutMux2.TabIndex = 21;
            this.OutMux2.TabStop = false;
            this.OutMux2.Text = "CH2";
            this.OutMux2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.OutMux2.UseVisualStyleBackColor = false;
            this.OutMux2.Click += new System.EventHandler(this.SelecOutput);
            // 
            // OutMux1
            // 
            this.OutMux1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OutMux1.BackColor = System.Drawing.Color.Transparent;
            this.OutMux1.BackgroundImage = global::Dezac.Instruments.Properties.Resources.HP53131A1;
            this.OutMux1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.OutMux1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.OutMux1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OutMux1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutMux1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.OutMux1.ImageIndex = 1;
            this.OutMux1.ImageList = this.ImageListIO;
            this.OutMux1.Location = new System.Drawing.Point(19, 76);
            this.OutMux1.Margin = new System.Windows.Forms.Padding(4);
            this.OutMux1.Name = "OutMux1";
            this.OutMux1.Size = new System.Drawing.Size(154, 61);
            this.OutMux1.TabIndex = 20;
            this.OutMux1.TabStop = false;
            this.OutMux1.Text = "CH1";
            this.OutMux1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.OutMux1.UseVisualStyleBackColor = false;
            this.OutMux1.Click += new System.EventHandler(this.SelecOutput);
            // 
            // OutMux0
            // 
            this.OutMux0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OutMux0.BackColor = System.Drawing.Color.Transparent;
            this.OutMux0.BackgroundImage = global::Dezac.Instruments.Properties.Resources.HP34401A1;
            this.OutMux0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.OutMux0.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.OutMux0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OutMux0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutMux0.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.OutMux0.ImageIndex = 1;
            this.OutMux0.ImageList = this.ImageListIO;
            this.OutMux0.Location = new System.Drawing.Point(19, 7);
            this.OutMux0.Margin = new System.Windows.Forms.Padding(4);
            this.OutMux0.Name = "OutMux0";
            this.OutMux0.Size = new System.Drawing.Size(154, 61);
            this.OutMux0.TabIndex = 19;
            this.OutMux0.TabStop = false;
            this.OutMux0.Text = "TESTER";
            this.OutMux0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.OutMux0.UseVisualStyleBackColor = false;
            this.OutMux0.Click += new System.EventHandler(this.SelecOutput);
            // 
            // MUX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MUX";
            this.Size = new System.Drawing.Size(846, 209);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        internal System.Windows.Forms.Button InMux16;
        internal System.Windows.Forms.Button InMux15;
        internal System.Windows.Forms.Button InMux14;
        internal System.Windows.Forms.Button InMux13;
        internal System.Windows.Forms.Button InMux12;
        internal System.Windows.Forms.Button InMux11;
        internal System.Windows.Forms.Button InMux10;
        internal System.Windows.Forms.Button InMux9;
        internal System.Windows.Forms.Button InMux8;
        internal System.Windows.Forms.Button InMux7;
        internal System.Windows.Forms.Button InMux6;
        internal System.Windows.Forms.Button InMux5;
        internal System.Windows.Forms.Button InMux4;
        internal System.Windows.Forms.Button InMux3;
        internal System.Windows.Forms.Button InMux2;
        internal System.Windows.Forms.Button InMux1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        internal System.Windows.Forms.Button b_MUX_Reset;
        internal System.Windows.Forms.Label Label349;
        internal System.Windows.Forms.Label lbl_CANALB;
        internal System.Windows.Forms.Label Label351;
        internal System.Windows.Forms.Label lbl_CANALA;
        internal System.Windows.Forms.Button OutMux2;
        internal System.Windows.Forms.Button OutMux1;
        internal System.Windows.Forms.Button OutMux0;
        internal System.Windows.Forms.Button btnApply;
        internal System.Windows.Forms.ImageList ImageListIO;
    }
}
