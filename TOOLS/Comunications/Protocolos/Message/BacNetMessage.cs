﻿using System;
using System.Globalization;

namespace Comunications.Message
{
    public class BacNetMessage : HexStringMessage
    {
        private static CultureInfo enUS = new CultureInfo("en-US");

        public static BacNetMessage Create(string format, params object[] args)
        {
            BacNetMessage message = new BacNetMessage { Format = format };

            message.SetArgs(enUS, args);

            return message;
        }

        public override void ValidateResponse(ITransportMessage response)
        {
            for (var i = 0 ; i < response.MessageFrame.Length-1; i++)
                if (response.MessageFrame[i] == 85 && response.MessageFrame[i + 1] == 255)
                    return;
            throw new Exception("Error validacion trama BacNet");
        }
    }
}
