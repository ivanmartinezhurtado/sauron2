﻿ using Dezac.Device.Metering;
using Dezac.Device.Metering.Web_References.SOAP;
using Dezac.Tests.Core.Model;
using Dezac.Core.Utility;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Dezac.Tests.Visio.Forms;
using System.Globalization;
using System.ComponentModel;
using System.Xml.Linq;
using log4net;
using System.Threading;
using Dezac.Tests.UserControls;
using Dezac.Tests.Metering.Utils;
using Instruments.Tower;
using Dezac.Tests.Visio;
using Dezac.Tests.Extensions;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.13)]
    public class CIRCAMPTest : TestBase
    {
        #region Definition Inputs and Outputs

        private const byte IN_DEVICE_PRESENCE = 24;
        private const byte IN_BLOQUE_SEGURIDAD = 23;
        private const byte IN_DETECTA_ANCLA_1 = 25;
        private const byte IN_DETECTA_ANCLA_2 = 26;

        private const byte OUT_FIJACION_EQUIPO = 11;
        private const byte OUT_PISTON_OPTICO = 6;
        private const byte OUT_PISTON_BORNES = 5;
        private const byte OUT_PISTON_TECLA1 = 9;
        private const byte OUT_PISTON_TECLA2 = 10;

        private const byte IN_PISTON_OPTICO = 30;
        private const byte IN_PISTON_BORNES_1_N = 31;
        private const byte IN_PISTON_BORNES_7_12 = 32;
        private const byte IN_PISTON_BORNES_21_28 = 33;
        private const byte IN_PISTON_BORNES_31_40 = 34;

        private const byte OUT_ACTIVE_NEUTRO_RELAY_CONTROL = 19;
     
        private const byte OUT_ACTIVE_IN_CH1 = 0;
        private const byte OUT_ACTIVE_IN_CH2 = 1;
        private const byte OUT_ACTIVE_IN_CH3 = 2;
        private const byte OUT_ACTIVE_IN_CH4 = 3;
        private const byte OUT_ACTIVE_IN_RECLOSE = 41;

        private const byte OUT_ACTIVE_230V_CDC = 15;
        private const byte OUT_ACTIVE_VL2_TO_CDC = 43;

        private const byte IN_CH1_LOAD = 6;
        private const byte IN_CH2_LOAD = 7;
        private const byte IN_CH3_LOAD = 8;
        private const byte IN_CH4_LOAD = 22;

        private const byte IN_CH1_OUT_IMPULSE = 9;
        private const byte IN_CH2_OUT_IMPULSE = 10;
        private const byte IN_CH3_OUT_IMPULSE = 11;
        private const byte IN_CH4_OUT_IMPULSE = 12;

        #endregion

        private Tower3 tower;
        private CirwattDlms sbt;
        private MeterType meter;
        private COMPACT_DC cdc;

        private string IDS_CAMERA;

        public void TestInitialization()
        {
            tower = AddInstanceVar<Tower3>("TOWER");

            sbt = AddInstanceVar(new CirwattDlms(), "UUT");
            sbt.Retries = 4;
           
            logger.InfoFormat("COM PORT:{0}", sbt.PuertoCom);

            cdc = AddInstanceVar(new COMPACT_DC(), "COMPACT_DC");

            Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            meter = new MeterType("110ED7A83P22"); //Identificacion.MODELO);
            tower.MTEWAT.Output120A = false;
            tower.MTEPS.Output120A = false;  //meter.TipoConexion == MeterType.TipoConexionEnum.DIRECTO;
            tower.On();
            tower.OpenVoltageCircuit();
            tower.OpenCurrentCircuit(false, false, false);
            tower.WaitBloqSecurity(2000);
            tower.ActiveElectroValvule();

            tower.IO.DO.On(OUT_FIJACION_EQUIPO);

            IDS_CAMERA = GetVariable<string>("CAMARA_IDS", IDS_CAMERA);

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_1, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL ANCLA 1 COLOCADO EN EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_2, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL ANCLA 2 COLOCADO EN EL EQUIPO", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tower.IO.DO.OnWait(500, OUT_PISTON_OPTICO, OUT_PISTON_BORNES);

            if (!tower.IO.DI.WaitOn(IN_PISTON_OPTICO, 2000))
                throw new Exception("Error no se detecta piston optico");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_1_N, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 1 - 4 - N");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_7_12, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 7 - 12");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_21_28, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 21 - 28");

            if (!tower.IO.DI.WaitOn(IN_PISTON_BORNES_31_40, 2000))
                throw new Exception("Error no se detecta sensor piston bornes 31 - 40");

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                throw new Exception("Error no se el equipo en el util");

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_1, 2000))
                throw new Exception("Error no se detecta el ancla 1 en el equipo");

            if (!tower.IO.DI.WaitOn(IN_DETECTA_ANCLA_2, 2000))
                throw new Exception("Error no se detecta el ancla 2 en el equipo");
        }

        public void TestConsumo(int waitTimeStabilisation = 2000)
        {
            tower.MTEPS.ApplyAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 100, L3 = 0 }, 0, 50);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.L2.EnVacio.Vmin.Name, Params.KW.L2.EnVacio.Vmin.Min, Params.KW.L2.EnVacio.Vmin.Max,
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vmin fuera de margenes L2", 4000);

            TestConsumo(Params.KVAR.L2.EnVacio.Vmin.Name, Params.KVAR.L2.EnVacio.Vmin.Min, Params.KVAR.L2.EnVacio.Vmin.Max,
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vmin fuera de margenes L2");

            tower.MTEPS.ApplyAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 230, L3 = 0 }, 0, 50);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.L2.EnVacio.Vnom.Name, Params.KW.L2.EnVacio.Vnom.Min, Params.KW.L2.EnVacio.Vnom.Max,
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vnom fuera de margenes L2", 4000);

            TestConsumo(Params.KVAR.L2.EnVacio.Vnom.Name, Params.KVAR.L2.EnVacio.Vnom.Min, Params.KVAR.L2.EnVacio.Vnom.Max,
                () => tower.CvmminiMTE.ReadAllVariables().Phases.L2.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vnom fuera de margenes L2");

            tower.IO.DO.On((int)OUT_ACTIVE_230V_CDC, (int)OUT_ACTIVE_VL2_TO_CDC);
        }

        public void TestComunicacionesOpticas()
        {
            TestComunicaciones(tipoCom.OPTICAS);
        }

        public void TestComunicacionesRS485()
        {
            TestComunicaciones(tipoCom.RS485);
        }

        public enum tipoCom 
        {
              OPTICAS,
              RS485
         }

        private void TestComunicaciones(tipoCom tipoCom)
        {
            if (tipoCom == CIRCAMPTest.tipoCom.RS485)
                sbt.PuertoCom = (byte)Comunicaciones.SerialPort;
            else
                sbt.PuertoCom = (byte)Comunicaciones.SerialPort2;

            sbt.IniciarSesion();

            sbt.EscribirReset();

            sbt.EscribirEstadoFabricacion(9999);

            var codunesa = sbt.LeerCodigoUnesa();
            Assert.AreEqual(ConstantsParameters.Identification.CODIGO_UNESA, codunesa, Identificacion.CODIGO_UNESA, "Error. el codigo unesa es incorrecto.", ParamUnidad.SinUnidad);

            var versionSW = sbt.LeerVersionSW();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, versionSW, Identificacion.VERSION_FIRMWARE_SBT, "Error. la versión de firmware es incorrecto.", ParamUnidad.SinUnidad);

            var modelo = sbt.LeerModelo();
            Assert.AreEqual(ConstantsParameters.Identification.MODELO_SBT, modelo, Identificacion.MODELO_SBT, "Error. el modelo del equipo es incorrecto.", ParamUnidad.SinUnidad);

            SamplerWithCancel(() =>
            {
                var versionPLC = sbt.LeerVersionSWPLC();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_PLC, versionPLC.ToString(), Identificacion.VERSION_PLC, "Error. la version de firmware del PLC del equipo es incorrecto.", ParamUnidad.SinUnidad);
                return true;
            }, "", 4, 3000, 0, false, false);

            sbt.CerrarSesion(false);
        }

        public void TestConfiguracionInicial()
        {
            sbt.IniciarSesion();

            var mac = sbt.LeerMACPLC();
            Resultado.Set("MAC_PLC", mac, ParamUnidad.SinUnidad);
            TestInfo.NumMAC_PLC = mac;

            sbt.EscribirNumeroFabricacion((uint)TestInfo.NumBastidor);

            sbt.EscribirNumeroSerie(1000000);

            sbt.EscribirFechaFabricacion(DateTime.Now);

            sbt.EscribirEstadoFabricacion(999);

            sbt.WriteZeroOffsetActiveGain(CirwattDlms.enumFases.ALL_4_FASES);

            sbt.WriteVoltageGain(CirwattDlms.enumFases.ALL_4_FASES, new List<ushort>() { 3018, 3018, 3018, 3018 });

            sbt.WriteGapGain(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, new List<short>() { 0, 0, 0, 0 });

            sbt.WritePowerActiveGain(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, new List<ushort>() { 35594, 35594, 35594, 35594 });

            sbt.EscribirFechaHoraActual();

            sbt.EscribirFechaVerificacion(DateTime.Now);

            sbt.CerrarSesion(false);

        }

        public void TestRelesControlEnergia()
        {
            tower.IO.DO.On(OUT_ACTIVE_NEUTRO_RELAY_CONTROL);

            sbt.IniciarSesion();

            var ouputsValue = new List<KeyValuePair<byte, CirwattDlms.enumFases>>();
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH1_LOAD, CirwattDlms.enumFases.FASE_1));
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH2_LOAD, CirwattDlms.enumFases.FASE_2));
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH3_LOAD, CirwattDlms.enumFases.FASE_3));
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH4_LOAD, CirwattDlms.enumFases.FASE_4));

            foreach (KeyValuePair<byte, CirwattDlms.enumFases> output in ouputsValue)
            {
                sbt.WriteRelayEnergyControl(output.Value, CirwattDlms.enumStateRelay.Closed);
                Delay(200, "Espera al enviar  cirre de los reles energia individuales");
            }

            sbt.WriteRelayEnergyControl(0, CirwattDlms.enumStateRelay.Closed);
            Delay(200, "Espera al enviar  cirre de los reles eneriga global");

            foreach (KeyValuePair<byte, CirwattDlms.enumFases> output in ouputsValue)
            {               
                logger.InfoFormat("Test Rele control energia {0}", output.Value.ToString());

                sbt.WriteRelayEnergyControl(output.Value, CirwattDlms.enumStateRelay.Open);

                SamplerWithCancel(() =>
                {
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 10, 200);

                sbt.WriteRelayEnergyControl(output.Value, CirwattDlms.enumStateRelay.Closed);

                SamplerWithCancel(() =>
                {
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} activada despues de desactivarla", output.Value.ToString()), 10, 200);
            }

           tower.IO.DO.Off(OUT_ACTIVE_NEUTRO_RELAY_CONTROL);

           sbt.CerrarSesion(false);

        }

        public void TestRelesControImpulsos()
        {
            sbt.IniciarSesion();

            var ouputsValue = new List<KeyValuePair<byte, CirwattDlms.enumFases>>();
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH1_OUT_IMPULSE, CirwattDlms.enumFases.FASE_1));
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH2_OUT_IMPULSE, CirwattDlms.enumFases.FASE_2));
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH3_OUT_IMPULSE, CirwattDlms.enumFases.FASE_3));
            ouputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumFases>(IN_CH4_OUT_IMPULSE, CirwattDlms.enumFases.FASE_4));

            sbt.WriteRelayImpulseControl(0, CirwattDlms.enumStateRelay.Closed);

            foreach (KeyValuePair<byte, CirwattDlms.enumFases> output in ouputsValue)
            {
                logger.InfoFormat("Test Rele control impulso {0}", output.Value.ToString());

                sbt.WriteRelayImpulseControl(output.Value, CirwattDlms.enumStateRelay.Open);

                SamplerWithCancel(() =>
                {
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la entrada {0} activada", output.Value.ToString()), 10, 200);

                sbt.WriteRelayImpulseControl(output.Value, CirwattDlms.enumStateRelay.Closed);

                SamplerWithCancel(() =>
                {
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la entrada {0} activada despues de desactivarla", output.Value.ToString()), 10, 200);
            }

            sbt.CerrarSesion(false);
        }

        public void TestEntradaImpulsos()
        {
            sbt.IniciarSesion();

            var inputsValue = new List<KeyValuePair<byte, CirwattDlms.enumKeyInputs>>();
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs>(OUT_ACTIVE_IN_CH1, CirwattDlms.enumKeyInputs.IN1));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs>(OUT_ACTIVE_IN_CH2, CirwattDlms.enumKeyInputs.IN2));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs>(OUT_ACTIVE_IN_CH3, CirwattDlms.enumKeyInputs.IN3));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs>(OUT_ACTIVE_IN_CH4, CirwattDlms.enumKeyInputs.IN4));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyInputs>(OUT_ACTIVE_IN_RECLOSE, CirwattDlms.enumKeyInputs.REARME));

            SamplerWithCancel(() =>
            {
                return sbt.ReadInputsImpulse() == CirwattDlms.enumKeyInputs.NOTHING;
            }, "Error. alguna entrada activada cuando no deberia", 5, 200);

            foreach (KeyValuePair<byte, CirwattDlms.enumKeyInputs> input in inputsValue)
            {
                SamplerWithCancel(() =>
                {

                    tower.IO.DO.OffWait(150, input.Key);

                    logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                    tower.IO.DO.On(input.Key);

                    SamplerWithCancel(() =>
                    {
                        var val = (int)input.Value;
                        var valsbt = (int)sbt.ReadInputsImpulse();
                        logger.InfoFormat("Input Tower={0}   Circamp={1}", val, valsbt);
                        return val == valsbt;
                    }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 30, 100, 0, false, false);

                    tower.IO.DO.Off(input.Key);

                    SamplerWithCancel(() =>
                    {
                        return sbt.ReadInputsImpulse() == CirwattDlms.enumKeyInputs.NOTHING;
                    }, string.Format("Error. no se detecta la entrada {0} desactivada", input.Value.ToString()), 5, 500, 1000);
                    return true;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()));
            }

            SamplerWithCancel(() =>
            {
                return sbt.ReadInputsImpulse() == CirwattDlms.enumKeyInputs.NOTHING;
            }, "Error. alguna entrada activada después de desactivar todos los reles", 5, 200);

            sbt.CerrarSesion(false);
        }

        public void TestTeclado()
        {
            sbt.IniciarSesion();

            var inputsValue = new List<KeyValuePair<byte, CirwattDlms.enumKeyBoard>>();
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyBoard>(OUT_PISTON_TECLA1, CirwattDlms.enumKeyBoard.TECLA1));
            inputsValue.Add(new KeyValuePair<byte, CirwattDlms.enumKeyBoard>(OUT_PISTON_TECLA2, CirwattDlms.enumKeyBoard.TECLA2));

            SamplerWithCancel(() =>
            {
                return sbt.ReadKeyboard() == CirwattDlms.enumKeyBoard.NOTHING;
            }, "Error. alguna entrada activada cuando no deberia", 5, 200);

            foreach (KeyValuePair<byte, CirwattDlms.enumKeyBoard> input in inputsValue)
            {
                tower.IO.DO.On(input.Key);

                SamplerWithCancel(() =>
                {
                    return sbt.ReadKeyboard() == input.Value;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 5, 200);

                tower.IO.DO.Off(input.Key);
            }

            SamplerWithCancel(() =>
            {
                return sbt.ReadKeyboard() == CirwattDlms.enumKeyBoard.NOTHING;
            }, "Error. alguna entrada activada después de desactivar todos los reles", 5, 200);

            sbt.CerrarSesion(false);
        }

        public void TestPLC()
        {
            //TODO Test Leds del cabezal optico
            SamplerWithCancel((p) =>
            {
                var device = cdc.Soap.ReadDevice(ConcentratorSoapService.ComStatus.Conectado);
                var meter = device.Where((r) => r.IsSBT == false && r.MeterID == string.Format("CIR{0:0000000000}", 1000000)).FirstOrDefault();
                if (meter == null)
                    throw new Exception("Error. no se ha detectado un contador conectado al concentrador ");
                return true;
            }, "Error. No se ha podido comunicar con el medidor por PLC", 20, 5000, 15000, false, false);

            tower.IO.DO.Off(OUT_ACTIVE_VL2_TO_CDC);

            cdc.DeleteListNodes();

            Delay(2000, "Esperamos eliminar nodos del concentrador");

            tower.IO.DO.Off(OUT_ACTIVE_230V_CDC);
        }

        public void TestLeds()
        {
            sbt.IniciarSesion();

            sbt.WriteActivateLeds(CirwattDlms.enumLeds.PLC_GREEN);
            //*********************************************************************************************************

            tower.IO.DO.Off(OUT_PISTON_OPTICO);

            var cameraResult = Shell.ShowDialog("LED PLC", () =>
            {
                var camara = new CameraView(new CameraViewInfo() { CameraIndex = 0, ImageHeight = 800, ImageWidth = 600, Model = CameraViewType.Emgu }, "", "", string.Format("{0}\\{1}\\{2}.png", PathCameraImages.TrimEnd('\\'), this.GetType().Name, "LED_PLC_GREEN"));
                camara.AllowSaveCameraImage = true;
                camara.CameraInfo.Exposure = -10;
                camara.SaveImagePath = PathImageSave;
                camara.SaveName = string.Format("{0}_{1}", sbt.GetType().Name, "LED_PLC_GREEN");
                camara.Size = new System.Drawing.Size(1500, 850);
                return camara;
            }, MessageBoxButtons.YesNo, "Comprobar LED PLC encendidos como en la imagen patrón", 2000);

            Assert.AreEqual(cameraResult, DialogResult.Yes, "Error. LED PLC no se encendide ");

            sbt.WriteActivateLeds(CirwattDlms.enumLeds.PLC_RED);
            
            cameraResult = Shell.ShowDialog("LED PLC", () =>
            {
                var camara = new CameraView(new CameraViewInfo() { CameraIndex = 0, ImageHeight = 800, ImageWidth = 600, Model = CameraViewType.Emgu }, "", "", string.Format("{0}\\{1}\\{2}.png", PathCameraImages.TrimEnd('\\'), this.GetType().Name, "LED_PLC_RED"));
                camara.AllowSaveCameraImage = true;
                camara.CameraInfo.Exposure = -10;
                camara.SaveImagePath = PathImageSave;
                camara.SaveName = string.Format("{0}_{1}", sbt.GetType().Name, "LED_PLC_RED");
                camara.Size = new System.Drawing.Size(1500, 850);
                return camara;
            }, MessageBoxButtons.YesNo, "Comprobar LED PLC encendidos como en la imagen patrón", 2000);

            Assert.AreEqual(cameraResult, DialogResult.Yes, "Error. LED PLC rojo no se encendide ");

            tower.IO.DO.On(OUT_PISTON_OPTICO);

            //************************************************************************************************************

            sbt.WriteActivateLeds(CirwattDlms.enumLeds.SEPARATE_LED);

            Delay(1000, "Espera condicionamiento de los LEDS");

            this.TestHalconFindLedsProcedure(IDS_CAMERA, "CIRCAMP_LEDS_PARES", "LEDS_PARES", this.GetType().Name);

            sbt.WriteActivateDisplay(1);

            sbt.WriteActivateLeds(CirwattDlms.enumLeds.REST_SEPARATE_LED);

            Delay(1000, "Espera condicionamiento de los LEDS");

            this.TestHalconFindLedsProcedure(IDS_CAMERA, "CIRCAMP_LEDS_IMPARES", "LEDS_IMPARES", this.GetType().Name);

            sbt.WriteRelayImpulseControl(0, CirwattDlms.enumStateRelay.Closed);

            sbt.CerrarSesion(false);
        }

        public void TestDisplay()
        {
            sbt.IniciarSesion();

            sbt.WriteActivateLeds(CirwattDlms.enumLeds.NULL);

            tower.IO.DO.PulseOn(200, OUT_PISTON_TECLA1);

            sbt.WriteActivateDisplay(1);

            Delay(2000, "Espera condicionamiento del Diaplay");

            this.TestHalconMatchingProcedure(IDS_CAMERA, "CIRCAMP_LCD", "CIRCAMP_LCD", this.GetType().Name);

            sbt.CerrarSesion(false);
        }

        public void TestAjusteVoltage()
        {
            tower.MTEPS.ApplyOffAndWaitStabilisation();

            Delay(500, "");

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            tower.MTEPS.ApplyAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = adjustVoltage, L3 = 0 }, new TriLineValue { L1 = 0, L2 = adjustCurrent, L3 = 0 }, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var controlAdjust = new CirwattDlms.controlAdjust() { delFirst = 1, diffMaxRangeValid = 0, initCount = 2, interval = 1200, samples = 5 };

            var defs = new TriAdjustValueDef(Params.GAIN_V.L1.Name, Params.GAIN_V.Null.Ajuste.Min, Params.GAIN_V.Null.Ajuste.Max, 0, 0, ParamUnidad.Puntos);
            defs.Neutro = new AdjustValueDef(Params.GAIN_V.L4.Name, 0, Params.GAIN_V.Null.Ajuste.Min, Params.GAIN_V.Null.Ajuste.Max, 0, 0, ParamUnidad.Puntos);

            sbt.IniciarSesion();

            var result = sbt.AdjustVoltage(CirwattDlms.enumFases.ALL_4_FASES, adjustVoltage, controlAdjust, (value) =>
                {
                    defs.L1.Value = value.LINES.L1;
                    defs.L2.Value = value.LINES.L2;
                    defs.L3.Value = value.LINES.L3;
                    defs.Neutro.Value = value.LN;
                    return !defs.HasMinMaxError();
                });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de voltage valores fuera de margenes");

            var gainVoltage = FourLineValue.Create(result.Item2.LINES.L1, result.Item2.LINES.L2, result.Item2.LINES.L3, result.Item2.LN);

            sbt.WriteVoltageGain(CirwattDlms.enumFases.ALL_4_FASES, gainVoltage);

            sbt.CerrarSesion(false);
        }

        public void TestAjusteDesfase()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 60, ParamUnidad.Grados);

            tower.MTEPS.ApplyAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = adjustVoltage, L3 = 0 }, new TriLineValue { L1 = 0, L2 = adjustCurrent, L3 = 0 }, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            double desfaseFactor = -22.2;

            var controlAdjust = new CirwattDlms.controlAdjust() { delFirst = 1, diffMaxRangeValid = 0, initCount = 2, interval = 1200, samples = 5 };

            var defs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Name, Params.GAIN_DESFASE.Null.Ajuste.Min, Params.GAIN_DESFASE.Null.Ajuste.Max, 0, 0, ParamUnidad.Puntos);
            defs.Neutro = new AdjustValueDef(Params.GAIN_DESFASE.L4.Name,0,  Params.GAIN_DESFASE.Null.Ajuste.Min, Params.GAIN_DESFASE.Null.Ajuste.Max, 0, 0, ParamUnidad.Puntos);

            sbt.IniciarSesion();

            var result = sbt.AdjustGap(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, powerFactor, desfaseFactor, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0].puntosErrorDesfase;
                defs.L2.Value = value[1].puntosErrorDesfase;
                defs.L3.Value = value[2].puntosErrorDesfase;
                defs.Neutro.Value = value[3].puntosErrorDesfase;
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de desfase valor fuera de margenes");

            var gainGap = new List<CirwattDlms.structPhaseOffset>();
            gainGap.AddRange(result.Item2);

            sbt.WriteGapGain(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, gainGap);

            sbt.CerrarSesion(false);

        }

        public void TestAjustePotencia()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);
            var powerReference = adjustVoltage * adjustCurrent * Math.Cos(powerFactor.ToRadians());

            tower.MTEPS.ApplyAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = adjustVoltage, L3 = 0 }, new TriLineValue { L1 = 0, L2 = adjustCurrent, L3 = 0 }, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var controlAdjust = new CirwattDlms.controlAdjust() { delFirst = 1, diffMaxRangeValid = 0, initCount = 2, interval = 1200, samples = 5 };

            var defs = new TriAdjustValueDef(Params.GAIN_KW.L1.Name, Params.GAIN_KW.Null.Ajuste.Min, Params.GAIN_KW.Null.Ajuste.Max, 0, 0, ParamUnidad.Puntos);
            defs.Neutro = new AdjustValueDef(Params.GAIN_KW.L4.Name,0, Params.GAIN_KW.Null.Ajuste.Min, Params.GAIN_KW.Null.Ajuste.Max, 0, 0, ParamUnidad.Puntos);

            sbt.IniciarSesion();

            var result = sbt.AdjustPowerActive(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, powerReference, controlAdjust, (value) =>
            {
                defs.L1.Value = value.LINES.L1;
                defs.L2.Value = value.LINES.L2;
                defs.L3.Value = value.LINES.L3;
                defs.Neutro.Value = value.LN;
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de potencia  valor fuera de margenes");

            var gainPower = FourLineValue.Create(result.Item2.LINES.L1, result.Item2.LINES.L2, result.Item2.LINES.L3, result.Item2.LN);
            sbt.WritePowerActiveGain(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, gainPower);

            sbt.CerrarSesion(false);
        }

        // ***********************************************************************************

        public void TestPoint_MarchaVacio()
        {

            ConsignarMTE(meter.VoltageProperty.VoltageNominal, 0, 0, meter.FrequencyProperty.Frequency);

            sbt.IniciarSesion();

            var points = sbt.ReadPowerPoints(CirwattDlms.enumFases.ALL_4_FASES, TipoPotencia.Activa).ToArray();
            var gain = sbt.ReadPowerGain(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, TipoPotencia.Activa);

            var adjust = new AdjustValueDef(Params.POINTS.L1.TestPoint("MARCHA_VACIO").Name, points[0], points[0], gain[0] * 2, 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L1 leidos mayor a la ganacia el equipo, consume energia");

            adjust = new AdjustValueDef(Params.POINTS.L2.TestPoint("MARCHA_VACIO").Name, points[1], points[1], gain[1] * 2, 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L2 leidos mayor a la ganacia el equipo, consume energia");

            adjust = new AdjustValueDef(Params.POINTS.L3.TestPoint("MARCHA_VACIO").Name, points[2], points[2], gain[2] * 2, 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L3 leidos mayor a la ganacia el equipo, consume energia");


            adjust = new AdjustValueDef(Params.POINTS.L4.TestPoint("MARCHA_VACIO").Name, points[3], points[3], gain[3] * 2, 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L4 leidos mayor a la ganacia del equipo, consume energia");

            sbt.CerrarSesion(false);
        }

        public void TestPoint_Arranque()
        {
            ConsignarMTE(meter.VoltageProperty.VoltageNominal, meter.CurrentProperty.CorrienteBase * (0.4 / 100), 0, meter.FrequencyProperty.Frequency);

            sbt.IniciarSesion();

            var points = sbt.ReadPowerPoints(CirwattDlms.enumFases.ALL_4_FASES, TipoPotencia.Activa).ToArray();
            var gain = sbt.ReadPowerGain(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, TipoPotencia.Activa);

            var adjust = new AdjustValueDef(Params.POINTS.L1.TestPoint("ARRANQUE").Name, points[0], gain[0], points[0] , 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L1 leidos menor a la ganacia el equipo no ve la corriente");

            adjust = new AdjustValueDef(Params.POINTS.L2.TestPoint("ARRANQUE").Name, points[1], gain[1], points[1], 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L2 leidos menor a la ganacia el equipo no ve la corriente");

            adjust = new AdjustValueDef(Params.POINTS.L3.TestPoint("ARRANQUE").Name, points[2], gain[2], points[2], 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L3 leidos menor a la ganacia el equipo no ve la corriente");

            adjust = new AdjustValueDef(Params.POINTS.L4.TestPoint("ARRANQUE").Name, points[3], gain[3], points[3], 0, 0, ParamUnidad.Puntos);
            adjust.AddToResults(Resultado);
            if (!adjust.IsValid())
                throw new Exception("Error. puntos de potencia L4 leidos menor a la ganacia el equipo no ve la corriente");

            sbt.CerrarSesion(false);
        }

        public void TestPoint_Imin([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            TestPointCalibration(TestPointsName.TP_IMIN, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IbCosPhi(int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            TestPointCalibration(TestPointsName.TP_IbCOS05, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestCostumize()
        {
            sbt.IniciarSesion();

            sbt.EscribirNumeroSerie(Convert.ToUInt32(TestInfo.NumSerie));

            Delay(1000, "Espera grabacion numero de serie");

            tower.MTEPS.ApplyOffAndWaitStabilisation();

            Delay(1000, "Espera grabacion numero de serie");

            tower.MTEPS.ApplyAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 230, L3 = 0 }, 0, 50);

            SamplerWithCancel(() =>
            {
                sbt.IniciarSesion();
                return true;
            }, "Error de comunicaciones, No inicia sesion después de hacer un reset hardware", 2, 1000, 500, true, true);

            var serialNumber = sbt.LeerNumeroSerie();
            Assert.IsTrue(serialNumber.ToString() == TestInfo.NumSerie.ToString(), "Error. El número de serie grabado en el equipo no corresponde con el leido");

            Resultado.Set("NUMERO_SERIE", serialNumber.ToString(), ParamUnidad.SinUnidad);

            var data = sbt.LeerFechaHora();

            double timeDiff = (DateTime.Now.Subtract(data)).TotalSeconds;

            Resultado.Set("DIFF_TIME", timeDiff, ParamUnidad.s);

            Assert.AreGreater(Configuracion.GetDouble("MAX_DIFF_TIME", 30, ParamUnidad.s), timeDiff, "Error. El equipo ha perdido mas de 30 segundos después del reset de hardware");

            sbt.EscribirEstadoFabricacion(0);

            Resultado.Set("CODE_ERROR", 0, ParamUnidad.SinUnidad);

            sbt.EscribirReset();

            sbt.CerrarSesion(false);

            tower.MTEPS.ApplyOffAndWaitStabilisation();
        }

        public void TestFinish()
        {
            if (sbt != null)
            {
                sbt.CerrarSesion(false);
                sbt.Dispose();
            }

            if (cdc != null)
                cdc.Dispose();
            
            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(500, OUT_PISTON_OPTICO);
                    tower.IO.DO.OffWait(500, OUT_PISTON_BORNES);
                    tower.IO.DO.OffWait(500, OUT_FIJACION_EQUIPO);
                    tower.IO.DO.Off(OUT_ACTIVE_230V_CDC, OUT_ACTIVE_VL2_TO_CDC, OUT_ACTIVE_NEUTRO_RELAY_CONTROL);
                }
                tower.Dispose();
            }
        }

        //**********************************************************************************

        private void TestPointCalibration(TestPointsName testPointName, TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            TestPoint testPoint = new TestPoint(meter, testPointName, tipoPotencia);

            ConsignarMTE(testPoint.Voltage, testPoint.Current, testPoint.Desfase, testPoint.Frequency);

            internalVerification(testPoint, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        private void internalVerification(TestPoint testPoint, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            var potenciaAparente = testPoint.Voltage * testPoint.Current;
            var potenciaActiva = potenciaAparente * Math.Cos(testPoint.Desfase.ToRadians());

            var defs = new TriAdjustValueDef(testPoint.Name + "_L1", 0, 0, potenciaActiva, testPoint.Tolerance, ParamUnidad.W);
            defs.Neutro = new AdjustValueDef(testPoint.Name + "_L4", 0, 0, 0, potenciaActiva, testPoint.Tolerance, ParamUnidad.W);

            TestCalibracionBase(defs,
            () =>
            {
                var powerActive = sbt.Leer_Potencia(CirwattDlms.enumFases.ALL_4_FASES, Device.DeviceBase<CirwattDlms>.frequency._50Hz, TipoPotencia.Activa);

                var varsList = new List<double>() { powerActive.LINES.L1, powerActive.LINES.L2, powerActive.LINES.L3, powerActive.LN };

                logger.InfoFormat("Potencia Activa CIRCAMP : {0}", powerActive.ToString());

                return varsList.ToArray();
            },
            () =>
            {
                var lecturas = new List<double>();

                var activePower = tower.MTEWAT.ReadActivePower();
                lecturas.Add(activePower.Value.L2);
                lecturas.Add(activePower.Value.L2);
                lecturas.Add(activePower.Value.L2);
                lecturas.Add(activePower.Value.L2);

                logger.InfoFormat("Potencia Activa MTE PRS :{0}", activePower.ToString());

                return lecturas.ToArray();

            }, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime, string.Format("Error calculado en la calibración en {0} fuera de margenes", testPoint.Name));
        }

        private void ConsignarMTE(double tension, double corriente, double desfase, double frecuencia)
        {
            var voltage = TriLineValue.Create(0, tension, 0);
            var current = TriLineValue.Create(0, corriente, 0);
            var gap = TriLineValue.Create(0, desfase, 0);

            tower.MTEPS.ApplyAndWaitStabilisation(voltage, current, frecuencia, gap, 0, () =>
            {
                var pf = Math.Floor(tower.MTEWAT.ReadPowerFactor().Value.L2);
                logger.InfoFormat("Stabilisation MTE PRS PowerFactor= ", pf.ToString());

                if (pf > 350)
                    pf -= 360;
                var toleranceMax = gap.L2 + 3;
                var toleranceMin = gap.L2 - 3;

                return toleranceMin <= pf && toleranceMax >= pf;
            });
        }
    }
}
