﻿using Dezac.Data;
using Dezac.Tests.Model;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Services
{
    public class TestContextModel : ITestContext 
    {
        public string IdOperario { get; set; }
        public string IdEmpleado { get; set; }
        public string IdFamilia { get; set; }
        public int? NumFamilia { get; set; }
        public int NumProducto { get; set; }
        public int Version { get; set; }
        public string Lote { get; set; }
        public string Codigo { get; set; }
        public string IdFase { get; set; }
        public string DescFamilia { get; set; }
        public string ModelLeader { get; set; }
        public string Cliente { get; set; }
        public int? NumOrden { get; set; }
        public int Nivelcontroltest { get; set; }
        public int Cantidad { get; set; }
        public int CantidadPendiente { get; set; }
        public int IdCajaActual { get; set; }
        public int NumCajaActual { get; set; }
        public int NumEquiposCaja { get; set; }
        public int UdsCajaActual { get; set; }
        public string PC { get; set; }
        public string LocationTest { get; set; }
        public int? NumAsistencia { get; set; }
        public int? NumLineaAsistencia { get; set; }
        public string NumSerieAsistencia { get; set; }
        public ModoTest modoPlayer { get; set; }
        public bool ReloadParametersOnNextTest { get; set; }
        public bool IsAOITrace { get; set; }
        public string EAN { get; set; }
        public string EAN_MARCA { get; set; }
        public string MinSerialNumberBox { get; set; }
        public string MaxSerialNumberBox { get; set; }
        public List<string> SerialNumberBoxRange { get; set; }
        public string ReferenciaLineaCliente { get; set; }
        public string ReferenciaOrigen { get; set; }
        public string SequenceVersion { get; set; }
        public string SequenceName { get; set; }
        public string SauronVersion { get; set; }
        public string SauronInterface { get; set; }
        public bool IsReproceso { get; set; }
        public bool MantenerNumSerie { get; set; }
        public int NumUtilBien { get; set; }
        public bool HayUtilBien { get; set; }
        public BIEN UtilBien { get; set; }
        public int NumMangueraBien { get; set; }
        public bool HayMangueraBien { get; set; }
        public BIEN MangueraBien { get; set; }
        public List<string> Direcciones { get; set; }
        public List<string> Atributos { get; set; }
        public Dictionary<string, string> DescripcionLenguagesCliente { get; set; }
        public TestInfo TestInfo { get; set; }
        public ParamValueCollection Comunicaciones { get; private set; }
        public ParamValueCollection Consignas { get; private set; }
        public ParamValueCollection Configuracion { get; private set; }
        public ParamValueCollection Identificacion { get; private set; }
        public ParamValueCollection Margenes { get; private set; }
        public ParamValueResultsCollection Resultados { get; private set; }
        public ParamValueCollection Parametrizacion { get; private set; }
        public ParamValueCollection VectorHardware { get; private set; }
        public ParamValueCollection Vision { get; private set; }
        public ParamValueCollection EtiquetasEquipo { get; private set; }
        public ParamValueCollection EtiquetasNumSerie { get; private set; }
        public ParamValueCollection EtiquetasEmbalajeIndividual { get; private set; }
        public ParamValueCollection EtiquetasEmbalajeConjunto { get; private set; }
        public ParamValueCollection EtiquetasPromocion { get; private set; }
        public ParamValueCollection Laser { get; private set; }
        public ConfigurationFilesCollection ConfigurationFiles { get; private set; }
        public TestContextModel()
        {
            Comunicaciones = new ParamValueCollection("Comunicaciones");
            Consignas = new ParamValueCollection("Consignas");
            Configuracion = new ParamValueCollection("Configuración");
            Identificacion = new ParamValueCollection("Identificación");
            Margenes = new ParamValueCollection("Márgenes");
            Resultados = new ParamValueResultsCollection("Resultados");
            Parametrizacion = new ParamValueCollection("Parametrización");
            VectorHardware = new ParamValueCollection("VectorHardware");
            Vision = new ParamValueCollection("Visión");
            EtiquetasEquipo = new ParamValueCollection("EtiquetasEquipo");
            EtiquetasNumSerie = new ParamValueCollection("EtiquetasNumSerie");
            EtiquetasEmbalajeIndividual = new ParamValueCollection("EtiquetasEmbalajeIndividual");
            EtiquetasEmbalajeConjunto = new ParamValueCollection("EtiquetasEmbalajeConjunto");
            EtiquetasPromocion = new ParamValueCollection("EtiquetasPromocion");
            Laser = new ParamValueCollection("Laser");
            TestInfo = new TestInfo { StartTime = DateTime.Now };
            ConfigurationFiles = new ConfigurationFilesCollection();
        }
        public ParamValueCollection GetGroupList(int tipoGrupo)
        {
            if (tipoGrupo == 1)
                return Configuracion;
            if (tipoGrupo == 2)
                return Margenes;
            if (tipoGrupo == 5)
                return Identificacion;
            if (tipoGrupo == 6)
                return Comunicaciones;
            if (tipoGrupo == 7)
                return Consignas;
            if (tipoGrupo == 8)
                return Resultados;
            if (tipoGrupo == 16)
                return Parametrizacion;
            if (tipoGrupo == 17)
                return VectorHardware;
            if (tipoGrupo == 18)
                return Vision;

            if (tipoGrupo == 19)
                return EtiquetasEquipo;
            if (tipoGrupo == 21)
                return EtiquetasEmbalajeIndividual;
            if (tipoGrupo == 20)
                return EtiquetasEmbalajeConjunto;
            if (tipoGrupo == 22)
                return EtiquetasNumSerie;
            if (tipoGrupo == 25)
                return EtiquetasPromocion;

            if (tipoGrupo == 24)
                return Laser;

            return null;
        }
        public ITestContext Clone(bool copyValues)
        {
            var clone = new TestContextModel
            {
                IdOperario = this.IdOperario,
                IdEmpleado = this.IdEmpleado,
                IdFamilia = this.IdFamilia,
                DescFamilia = this.DescFamilia,
                NumFamilia = this.NumFamilia,
                NumProducto = this.NumProducto,
                Version = this.Version,
                IdFase = this.IdFase,
                NumOrden = this.NumOrden,
                Nivelcontroltest = this.Nivelcontroltest,
                Cantidad = this.Cantidad,
                NumCajaActual = this.NumCajaActual,
                NumEquiposCaja = this.NumEquiposCaja,
                UdsCajaActual = this.UdsCajaActual,
                PC = this.PC,
                LocationTest = this.LocationTest,
                NumAsistencia = this.NumAsistencia,
                NumLineaAsistencia = this.NumLineaAsistencia,
                NumSerieAsistencia = this.NumSerieAsistencia,
                modoPlayer = this.modoPlayer,
                ReloadParametersOnNextTest = this.ReloadParametersOnNextTest,
                NumUtilBien = this.NumUtilBien,
                HayUtilBien = this.HayUtilBien,
                UtilBien = this.UtilBien,
                NumMangueraBien = this.NumMangueraBien,
                HayMangueraBien = this.HayMangueraBien,
                MangueraBien = this.MangueraBien,
                Lote = this.Lote,
                TestInfo = this.TestInfo,
                ConfigurationFiles = this.ConfigurationFiles,
                SauronVersion = this.SauronVersion,
                SauronInterface = this.SauronInterface,
            };

            if (copyValues)
            {
                clone.Comunicaciones.AddRange(this.Comunicaciones);
                clone.Consignas.AddRange(this.Consignas);
                clone.Configuracion.AddRange(this.Configuracion);
                clone.Identificacion.AddRange(this.Identificacion);
                clone.Margenes.AddRange(this.Margenes);
                clone.Parametrizacion.AddRange(this.Parametrizacion);
                clone.VectorHardware.AddRange(this.VectorHardware);
                clone.Vision.AddRange(this.Vision);
                clone.EtiquetasEquipo.AddRange(this.EtiquetasEquipo);
                clone.EtiquetasEmbalajeIndividual.AddRange(this.EtiquetasEmbalajeIndividual);
                clone.EtiquetasPromocion.AddRange(this.EtiquetasPromocion);
                clone.ConfigurationFiles.AddRange(ConfigurationFiles);
            }

            return clone;
        }
    }
}
