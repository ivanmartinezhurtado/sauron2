﻿
namespace Dezac.Tests.Reports.Reports
{
    public interface ITestReportAdapter
    {
        object GetReport(object testResult);
    }

}
