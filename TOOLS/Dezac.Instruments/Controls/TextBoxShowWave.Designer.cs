﻿namespace Dezac.Instruments.Controls
{
    partial class TextBoxShowWave
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtVariable = new System.Windows.Forms.TextBox();
            this.buttonGrafh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtVariable);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.buttonGrafh);
            this.splitContainer1.Size = new System.Drawing.Size(117, 27);
            this.splitContainer1.SplitterDistance = 88;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 2;
            // 
            // txtVariable
            // 
            this.txtVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVariable.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVariable.Location = new System.Drawing.Point(0, 0);
            this.txtVariable.Name = "txtVariable";
            this.txtVariable.Size = new System.Drawing.Size(88, 26);
            this.txtVariable.TabIndex = 1;
            // 
            // buttonGrafh
            // 
            this.buttonGrafh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGrafh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonGrafh.FlatAppearance.BorderSize = 0;
            this.buttonGrafh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonGrafh.Image = global::Dezac.Instruments.Properties.Resources.icone8;
            this.buttonGrafh.Location = new System.Drawing.Point(0, 0);
            this.buttonGrafh.Name = "buttonGrafh";
            this.buttonGrafh.Size = new System.Drawing.Size(27, 27);
            this.buttonGrafh.TabIndex = 2;
            this.buttonGrafh.UseVisualStyleBackColor = true;
            this.buttonGrafh.Click += new System.EventHandler(this.buttonGrafh_Click);
            // 
            // TextBoxShowWave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "TextBoxShowWave";
            this.Size = new System.Drawing.Size(117, 27);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtVariable;
        private System.Windows.Forms.Button buttonGrafh;

    }
}
