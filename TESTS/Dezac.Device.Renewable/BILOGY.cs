﻿
using Comunications.Message;
using Comunications.Protocolo;
using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace Dezac.Device.Renewable
{
    [DeviceVersion(1.08)]
    public class BILOGY : DeviceBase
    {
        private string St_Num { get; set; }

        public BILOGY()
        {
        }

        public BILOGY(string port)
        {
            SetPort(port);
        }

        public void SetPort(string port)
        {
            if (!port.ToUpper().Contains("COM"))
                port = "COM" + port;

            if (SerailPort == null)
            {
                var sp = new SerialPort(port, 19200, Parity.None, 8, StopBits.One);
                sp.ReadTimeout = 2000;
                sp.WriteTimeout = 2000;
                sp.DtrEnable = false;
                sp.RtsEnable = false;
                sp.Handshake = Handshake.None;
                SerailPort = new SerialPortAdapter(sp);

                Transport = new MessageTransport(new ASCIIProtocol(), SerailPort, _logger);
            }
            else
                SerailPort.PortName = port;
        }

        public MessageTransport Transport { get; internal set; }
        public SerialPortAdapter SerailPort { get; internal set; }
  
        #region DEVICE INFORMATION

        public string ReadDeviceInformation()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_INFO);
            return response.Trim() == "5C" ? "BILOGY" : "TRILOGY";
        }

        public struct ExtendedInformation
        {
            public string dateVerificaction;
            public string versionHW;
            public string versionHWLighting;
            public string versionSW;
        }

        public ExtendedInformation ReadDeviceExtendedInformation()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_EXTENDED_INFO);

            var versionHW = response.Substring(4, 16).HexStringToBytes();
            var versionHWLighting = response.Substring(20, 16).HexStringToBytes();
            var versionASCII = Encoding.ASCII.GetString(new byte[] { versionHW[4], versionHW[5] });
            var versionASCIILighting = Encoding.ASCII.GetString(new byte[] { versionHWLighting[4], versionHWLighting[5] });
            var versionSW = response.Substring(36, 8).HexStringToBytes();

            var letterVersion = "a";
            switch (versionSW[2])
            {
                case 0:
                    letterVersion = "a";
                    break;
                case 1:
                    letterVersion = "b";
                    break;
                case 2:
                    letterVersion = "rc";
                    break;
                case 3:
                    letterVersion = "r";
                    break;
            }

            var info = new ExtendedInformation()
            {
                dateVerificaction = string.Format("{0}/{1}", response.Substring(0, 2), response.Substring(2, 2)),
                versionHW = string.Format("{0:00}{1:X}{2:00} R{3:00} W{4:00}{5:00} {6}", versionHW[0], versionHW[1], versionHW[2], versionHW[3], versionHW[6], versionHW[7], versionASCII),
                versionHWLighting = string.Format("{0:00}{1:X}{2:00} R{3:00} W{4:00}{5:00} {6}", versionHWLighting[0], versionHWLighting[1], versionHWLighting[2], versionHWLighting[3], versionHWLighting[6], versionHWLighting[7], versionASCIILighting),
                versionSW = string.Format("{0}.{1}{2}{3}", versionSW[0], versionSW[1], letterVersion, versionSW[3]),
            };
            return info;
        }

        public void WriteVersionHardware()
        {
            LogMethod();

            var pcbYear = 15; // Convert.ToByte(pcbDate[1]);
            var pcbNumProj = 59; // vectorHW[0].Substring(2, 2);
            var pcbVer = 44;// Convert.ToByte(vectorHW[0].Substring(4, 2));
            var rev = "04"; // vectorHW[1].Replace("R", "");

            var fabYear = Convert.ToByte(DateTime.Now.Year.ToString().Substring(2, 2));
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var fabWeek = Convert.ToByte(weekNum);
            var man = Encoding.ASCII.GetBytes("DZ");

            var versionHardware = string.Format("{0:00}{1:X2}{2:00}{3:X2}{4:00}{5:X2}{6:X2}{7:X2}{8:X2}", 0, pcbYear, pcbNumProj, pcbVer, rev, man[0], man[1], fabWeek, fabYear);
            var resp = SendCommandHex(Registers.XCPDT_WRITE_VERSION_HW, true, versionHardware);
        }

        public void WriteVersionHwTRILOGY()
        {
            LogMethod();

            var pcbYear = 15; // Convert.ToByte(pcbDate[1]);
            var pcbNumProj = 59; // vectorHW[0].Substring(2, 2);
            var pcbVer = 45;// Convert.ToByte(vectorHW[0].Substring(4, 2));
            var rev = "04"; // vectorHW[1].Replace("R", "");

            var fabYear = Convert.ToByte(DateTime.Now.Year.ToString().Substring(2, 2));
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var fabWeek = Convert.ToByte(weekNum);
            var man = Encoding.ASCII.GetBytes("DZ");

            var versionHardware = string.Format("{0:00}{1:X2}{2:00}{3:X2}{4:00}{5:X2}{6:X2}{7:X2}{8:X2}", 0, pcbYear, pcbNumProj, pcbVer, rev, man[0], man[1], fabWeek, fabYear);
            var resp = SendCommandHex(Registers.XCPDT_WRITE_VERSION_HW, true, versionHardware);
        }

        public void WriteVersionHwTRILOGYLight()
        {
            LogMethod();

            var pcbYear = 15; // Convert.ToByte(pcbDate[1]);
            var pcbNumProj = 59; // vectorHW[0].Substring(2, 2);
            var pcbVer = 41;// Convert.ToByte(vectorHW[0].Substring(4, 2));
            var rev = "02"; // vectorHW[1].Replace("R", "");

            var fabYear = Convert.ToByte(DateTime.Now.Year.ToString().Substring(2, 2));
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var fabWeek = Convert.ToByte(weekNum);
            var man = Encoding.ASCII.GetBytes("DZ");

            var versionHardware = string.Format("{0:00}{1:X2}{2:00}{3:X2}{4:00}{5:X2}{6:X2}{7:X2}{8:X2}", 1, pcbYear, pcbNumProj, pcbVer, rev, man[0], man[1], fabWeek, fabYear);
            var resp = SendCommandHex(Registers.XCPDT_WRITE_VERSION_HW, true, versionHardware);
        }

        public int ReadTemperature()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_TEMPERATURE);
            return Convert.ToInt32(response, 16);
        }

        public string WriteVerificationDate()
        {
            LogMethod();
            var fabYear = Convert.ToByte(DateTime.Now.Year.ToString().Substring(2, 2));
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var fabWeek = Convert.ToByte(weekNum);
            var dateVerificaction = string.Format("{0:00}{1:00}", fabWeek, fabYear);
            var response = SendCommandHex(Registers.XCPDT_WRITE_DATE_VERIFICATION, false, dateVerificaction);
            return response;
        }

        #endregion

        #region SAMPLES

        public struct SamplesDetecttion
        {
            public byte numSamplesFull;
            public byte numSamplesEmpty;
        }

        public SamplesDetecttion ReadDetectionSamples()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_DETECTION_SAMPLES);
            var numSampleFullString = response.Substring(0, 2);
            var numSampleEmptyString = response.Substring(2, 2);
            var samples = new SamplesDetecttion()
            {
                numSamplesEmpty = Convert.ToByte(numSampleEmptyString),
                numSamplesFull = Convert.ToByte(numSampleFullString)
            };
            return samples;
        }

        public string WriteDetectionSamples(int numSamplesFull, int numSamplesEmpty)
        {
            LogMethod();
            var NumSamplesFull = string.Format("{0:X2}", numSamplesFull);
            var NumSamplesEmpty = string.Format("{0:X2}", numSamplesEmpty);
            var response = SendCommandHex(Registers.XCPDT_WRITE_DETECTION_SAMPLES, true, NumSamplesFull, NumSamplesEmpty);
            return response;
        }

        #endregion

        #region LEDS


        public struct LEDS
        {
            public bool Red;
            public bool Green;
            public bool Blue;
        };

        public LEDS ReadColorLeds()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XDPDT_READ_COLOR_LEDS);
            var arrayLeds = response.HexStringToBytes();
            var Leds = new LEDS()
            {
                Red = arrayLeds[0] == 255,
                Green = arrayLeds[1] == 255,
                Blue = arrayLeds[2] == 255,
            };
            return Leds;
        }

        public string WriteColorLeds(bool red, bool green, bool blue)
        {
            LogMethod();
            var response = SendCommandHex(Registers.XDPDT_WRITE_COLOR_LEDS, true, red ? "FF" : "00", green ? "FF" : "00", blue ? "FF" : "00");
            return response;
        }
    
        public string ReadBrightLeds()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_BRIGHT_INDICATOR);
            return response;
        }

        public string WriteBrightLeds(int level)
        {
            LogMethod();
           var lev =  string.Format("{0:X2}",level);
           var response = SendCommandHex(Registers.XCPDT_WRITE_BRIGHT_INDICATOR, true, lev);
            return response;
        }

        public string WriteStateUknow()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_WRITE_STATE_UKNOW, true, 0);
            return response;
        }

        #endregion

        #region NUM PERIFERICO

        public void SetStNum(string stNum)
        {
            St_Num = stNum;
        }

        public string WriteResetStNum()
        {
            LogMethod();
            St_Num = "0000";
            var response = SendCommandHex(Registers.XCPDT_RESET_STNUM, false);
            return response;
        }

        public string WriteStNum(string StNum)
        {
            LogMethod();
            St_Num = StNum;
            var response = SendCommandHex(Registers.XCPDT_WRITE_STNUM, false);
            return response;
        }

        #endregion

        #region TRILOGY LUZ CORTESIA

        public string WriteModeLighting(byte mode)
        {
            LogMethod();
            var modeLight = string.Format("{0:X2}", mode);
            var response = SendCommandHex(Registers.XCPDT_WRITE_MODE_LIGHTING, true, modeLight);
            return response;
        }

        public string WriteBrightLighting(byte bright)
        {
            LogMethod();
            var Bright = string.Format("{0:X2}", bright);
            var response = SendCommandHex(Registers.XCPDT_WRITE_BRIGHT_LIGHTING, true, Bright);
            return response;
        }

        #endregion

        public string WriteSensibility(byte gainUp, byte gainDown)
        {
            LogMethod();
            var GainUp = string.Format("{0:X2}", gainUp);
            var GainDown = string.Format("{0:X2}", gainDown);
            var response = SendCommandHex(Registers.XCPDT_WRITE_SENSIBILITY, true, GainUp, GainDown);
            return response;
        }

        public string WriteHeightDepth(int altura, int profundidad)
        {
            LogMethod();
            var Altura = string.Format("{0:X4}", altura);
            var Profundidad = string.Format("{0:X4}", profundidad);
            var response = SendCommandHex(Registers.XCPDT_WRITE_HIGHT_DEPTH, true, "00",  Profundidad, Altura);
            return response;
        }

        public string WriteNumMinimunPulses(byte numPulses)
        {
            LogMethod();
            var NumPulses = string.Format("{0:X2}", numPulses);
            var response = SendCommandHex(Registers.XCPDT_WRITE_MINIMUN_PULSES, true, NumPulses);
            return response;
        }

        public string WriteNumPulses(byte numPulses)
        {
            LogMethod();
            var NumPulses = string.Format("{0:X2}", numPulses);
            var response = SendCommandHex(Registers.XCPDT_WRITE_NUM_PULSES, true, NumPulses);
            return response;
        }

        public string WriteNumMinimumWidth(byte width)
        {
            LogMethod();
            var Width = string.Format("{0:X2}", width);
            var response = SendCommandHex(Registers.XCPDT_WRITE_MINIMUN_WIDTH_SIGNAL, true, Width);
            return response;
        }

        public string WriteActivedSensor(byte activeSensor)
        {
            LogMethod();
            var ActiveSensor = string.Format("{0:X2}", activeSensor);
            var response = SendCommandHex(Registers.XCPDT_WRITE_ACTIVED_SENSORS, true, ActiveSensor);
            return response;
        }

        public string WriteTrasnmisionPower(byte powerUp, byte powerDown)
        {
            LogMethod();
            var PowerUp = string.Format("{0:X2}", powerUp);
            var PowerDown = string.Format("{0:X2}", powerDown);
            var response = SendCommandHex(Registers.XCPDT_WRITE_TX_POWER, true, PowerUp, PowerDown);
            return response;
        }

        #region CALIBRACION

        public struct CalibrationDetectionNew
        {
            public double distanceUp;
            public double distanceDown;
            public double pulseUp;
            public double pulseDown;
            public double errorUpDsitance;
            public double errorDownDistance;
        }

        #region CALIBRACION OLD

        public struct UltrasoundsStructD8
        {
            public UltrasoundsD8 Up1;
            public UltrasoundsD8 Up2;
            public UltrasoundsD8 Up3;
            public UltrasoundsD8 Across1;
            public UltrasoundsD8 Across2;
            public UltrasoundsD8 Across3;
            public UltrasoundsD8 Down1;
            public UltrasoundsD8 Down2;
            public UltrasoundsD8 Down3;
        }

        public struct UltrasoundsD8
        {
            public UInt16 TimeEco;
            public byte NumPulse;
            public byte Amplitud;
        }

        public UltrasoundsStructD8 ReadUltrasounds()
        {
            LogMethod();
            var response = SendCommandHex<UltrasoundsStructD8>(Registers.XCPDT_READ_ULTRASOUNDS);
            return response;
        }

        public CalibrationDetectionNew TestCalibrationDetection(byte numMuestras, double distance)
        {
            var factorGain = 0.0001715;

            var calibrationDetection = new CalibrationDetectionNew();

            var reult = AdjustBase(0, numMuestras, numMuestras + 5, 3000, 0,
                () =>
                {
                    var ultrasound = ReadUltrasounds();
                    var value = new List<double>();
                    _logger.InfoFormat("timeEcoUp : {0}", ultrasound.Up2.TimeEco.ToString());
                    value.Add(ultrasound.Up2.TimeEco);
                    _logger.InfoFormat("PulseEcoUp : {0}", ultrasound.Up2.NumPulse.ToString());
                    value.Add(ultrasound.Up2.NumPulse);
                    _logger.InfoFormat("timeEcoDown : {0}", ultrasound.Down2.TimeEco.ToString());
                    value.Add(ultrasound.Down2.TimeEco);
                    _logger.InfoFormat("PulseEcoDown : {0}", ultrasound.Down2.NumPulse.ToString());
                    value.Add(ultrasound.Down2.NumPulse);
                    return value.ToArray();
                },
                (list) =>
                {
                    _logger.InfoFormat("tiemEcoUp: {0}", list.series[0].ToString());
                    var distanceUp = factorGain * list.Average(0);
                    _logger.InfoFormat("distanceUp L1 : {0}", distanceUp);
                    var pulseUp = list.Average(1);
                    _logger.InfoFormat("PulseUp: {0}", pulseUp);
                    _logger.InfoFormat("tiemEcoDown: {0}", list.series[2].ToString());
                    var distanceDown = factorGain * list.Average(2);
                    _logger.InfoFormat("distanceDown: {0}", distanceDown);
                    var pulseDown = list.Average(3);
                    _logger.InfoFormat("PulseDown: {0}", pulseDown);

                    calibrationDetection.distanceUp = distanceUp;
                    calibrationDetection.pulseUp = pulseUp;
                    calibrationDetection.distanceDown = distanceDown;
                    calibrationDetection.pulseDown = pulseDown;
                    return true;
                }, (sample) =>
                {
                    if(sample.Any(p => p == 0))
                    {
                        _logger.InfoFormat("Descartamos muestra por algun valor a 0");
                        return false;
                    }else
                        return true;
                });

            var errorUp = 0D;
            var errorDown = 0D;

            errorUp = calibrationDetection.distanceUp - distance;
            _logger.InfoFormat("errorUp : {0}", errorUp);
            calibrationDetection.errorUpDsitance = errorUp;

            errorDown = calibrationDetection.distanceDown - distance;
            _logger.InfoFormat("errorDown : {0}", errorDown);
            calibrationDetection.errorDownDistance = errorDown;

            return calibrationDetection;
        }

        #endregion

        #region CALIBRACION NUEVA

        public struct UltrasoundsNewStruct
        {
            public UltrasoundsNew Reint0;
            public UltrasoundsNew Reint1;
            public UltrasoundsNew Reint2;
        }

        public struct UltrasoundsNew
        {
            public UInt16 UU_TimeEco;
            public byte UU_PulseEco;
            public byte UU_LevelEco;
            public UInt16 DU_TimeEco;
            public byte DU_PulseEco;
            public byte DU_LevelEco;
            public UInt16 UD_TimeEco;
            public byte UD_PulseEco;
            public byte UD_LevelEco;
            public UInt16 DD_TimeEco;
            public byte DD_PulseEco;
            public byte DD_LevelEco;
        }

        public UltrasoundsNewStruct ReadUltrasoundsNew()
        {
            LogMethod();
            var response = SendCommandHex<UltrasoundsNewStruct>(Registers.XCPDT_READ_ULTRASOUNDS_NEW);
            return response;     
        }

        public CalibrationDetectionNew TestCalibrationDetectionNew(byte numMuestras, double distance)
        {
            var calibrationDetection = new CalibrationDetectionNew();

            var reult = AdjustBase(0, numMuestras, numMuestras + 1, 3000, 0,
                () =>
                {
                    var ultrasound = ReadUltrasoundsNew();
                    var value = new List<double>();
                    _logger.InfoFormat("timeEcoUp : {0}", ultrasound.Reint1.UU_TimeEco.ToString());
                    value.Add(ultrasound.Reint1.UU_TimeEco);
                    _logger.InfoFormat("PulseEcoUp : {0}", ultrasound.Reint1.UU_PulseEco.ToString());
                    value.Add(ultrasound.Reint1.UU_PulseEco);
                    _logger.InfoFormat("timeEcoDown : {0}", ultrasound.Reint1.DD_TimeEco.ToString());
                    value.Add(ultrasound.Reint1.DD_TimeEco);
                    _logger.InfoFormat("PulseEcoDown : {0}", ultrasound.Reint1.DD_PulseEco.ToString());
                    value.Add(ultrasound.Reint1.DD_PulseEco);
                    return value.ToArray();
                },
                (list) =>
                {
                    var distanceUp1 = list.Average(0);
                    _logger.InfoFormat("distanceUp: {0}", distanceUp1);
                    var pulseUp1 = list.Average(1);
                    _logger.InfoFormat("PulseUp: {0}", pulseUp1);
                    var distanceDown1 = list.Average(2);
                    _logger.InfoFormat("distanceDown: {0}", distanceDown1);
                    var pulseDown1 = list.Average(3);
                    _logger.InfoFormat("PulseDown: {0}", pulseDown1);

                    calibrationDetection.distanceUp = distanceUp1 / 100.0;
                    calibrationDetection.pulseUp = pulseUp1;
                    calibrationDetection.distanceDown = distanceDown1 / 100.0;
                    calibrationDetection.pulseDown = pulseDown1;
                    return true;
                });

            var errorUp = 0D;
            var errorDown = 0D;

            errorUp = calibrationDetection.distanceUp - distance;
            _logger.InfoFormat("errorUp : {0}", errorUp);
            calibrationDetection.errorUpDsitance = errorUp;

            errorDown = calibrationDetection.distanceDown - distance;
            _logger.InfoFormat("errorDown : {0}", errorDown);
            calibrationDetection.errorDownDistance = errorDown;

            return calibrationDetection;
        }

        #endregion

        #endregion

        //*******************************************************************************
        //*******************************************************************************

        private string SendCommandHex(Registers instruction, bool hasResponse = true, params object[] args)
        {
            LogMethod();
            var trama = string.Format("{0}{1}", St_Num, ((int)instruction).ToString("X2"));

            if (args != null)
            {
                var tramaArgs = "";
                foreach (object ob in args)
                    tramaArgs += string.Format("{0:00}", ob);
                trama += tramaArgs;
            }

            var cheksum = ComunicationUtility.CheckSum(trama.HexStringToBytes());
            var cheksumHex = cheksum.ToString("X2");

            HexStringMessage message, response;
            message = new HexStringMessage
            {
                AnswerLen = CalcAnswerLen(instruction),
                Text = string.Format("{0}{1}", trama, cheksumHex),
            };

            message.HasResponse = hasResponse;

            response = Transport.SendMessage<HexStringMessage>(message);

            if (hasResponse)
            {
                var responseClear = response.Text.Substring(4, (message.AnswerLen.Value - 3) * 2); //3 bytes 1 del ST_NUM y dos del CHEKSUM
                _logger.InfoFormat("TX:{0} -> RX: Raw {1}   Bilogy {2}", message.Text, response.Text, responseClear);
                return responseClear;
            }
            else
            {
                _logger.InfoFormat("TX:{0} ", message.Text);
                return "NO HAS RESPONSE";
            }
        }

        private T SendCommandHex<T>(Registers instruction, params object[] args) where T : struct
        {
            LogMethod();
            var trama = string.Format("{0}{1}", St_Num, ((int)instruction).ToString("X2"));

            if (args != null)
            {
                var tramaArgs = "";
                foreach (object ob in args)
                    tramaArgs += string.Format("{0:00}", ob);
                trama += tramaArgs;
            }

            var cheksum = ComunicationUtility.CheckSum(trama.HexStringToBytes());
            var cheksumHex = cheksum.ToString("X2");

            HexStringMessage message, response;
            message = new HexStringMessage
            {
                AnswerLen = CalcAnswerLen(instruction),
                Text = string.Format("{0}{1}", trama, cheksumHex),
            };

            response = Transport.SendMessage<HexStringMessage>(message);

            var responseMessage = response.MessageFrame.Skip(3).ToArray();

            return Comunications.Utility.Extensions.AsignaEstructura<T>(responseMessage);
        }

        private enum Registers
        {
            XCPDT_WRITE_STNUM = 0x00, // Programa la direcci�n del equipo
            XCPDT_READ_INFO = 0x05, // Devuelve informaci�n del equipo (tipo y versi�n)
            XDPDT_READ_COLOR_LEDS = 0xB4,   // Leer color leds en modo de trabajo normal, color de plaza libre.
            XDPDT_WRITE_COLOR_LEDS = 0xB5,  // Modificar color leds en modo de trabajo normal, color de plaza libre.
            XCPDT_READ_BRIGHT_INDICATOR = 0xB6, // Devuelve el brillo del indicador
            XCPDT_WRITE_BRIGHT_INDICATOR = 0xB7,// Modifica brillo del indicador
            XCPDT_WRITE_SENSIBILITY = 0xBF, // Modifica la sensibilidad de la recepción de ultrasonidos
            XCPDT_WRITE_TX_POWER = 0xC1,    // Valor de la potencia de transmisión.;
            XCPDT_WRITE_ACTIVED_SENSORS = 0xD5, // Configurar el número de sensores activos
            XCPDT_WRITE_NUM_PULSES = 0xD7,  // Configurar el número de pulsos de transmisión
            XCPDT_READ_TEMPERATURE = 0xD0,  // Leer temperatura dispositivo Trilogy
            XCPDT_READ_EXTENDED_INFO = 0xB0,    // Leer información extendida del dispositivo (firmware, hardware, pcb lighting)
            XCPDT_WRITE_VERSION_HW = 0xD3,  // Escritura versiones de hardware pcb de detección y pcb de lighting
            XCPDT_RESET_STNUM = 0xDB,   // Elimina la dirección del equipo de la ram
            XCPDT_READ_ULTRASOUNDS = 0xD8,  // Leer el estado de detección de ultrasonidos
            XCPDT_READ_DETECTION_SAMPLES = 0x0B,    // Leer el número de muestras para que el sensor determine el estado de la plaza
            XCPDT_WRITE_DETECTION_SAMPLES = 0x0C,   // Escribir el número de muestras para que el sensor determine el estado de la plaza
            XCPDT_WRITE_DATE_VERIFICATION = 0xD9,   // Escribir el número de muestras para que el sensor determine el estado de la plaza
            XCPDT_WRITE_STATE_UKNOW = 0xDD, // Escribir el número de muestras para que el sensor determine el estado de la plaza
            XCPDT_READ_ULTRASOUNDS_NEW = 0xE4,  // Leer el estado de detección de ultrasonidos Nova Version
            XCPDT_WRITE_HIGHT_DEPTH = 0xBD,
            XCPDT_WRITE_MINIMUN_PULSES = 0xC3,
            XCPDT_WRITE_MINIMUN_WIDTH_SIGNAL = 0xDF,
            
            // TRILOGY
            XCPDT_READ_MODE_LIGHTING = 0xCE,    // Lectura modo trabajo de la luz de cortesia (fijo o automatico)
            XCPDT_WRITE_MODE_LIGHTING = 0xCF,   // Modifica el modo de trabajo de la luz de cortesia (fijo o automatico)
            XCPDT_READ_BRIGHT_LIGHTING = 0xCA,  // Lectura brillo en modo regulacion fijo
            XCPDT_WRITE_BRIGHT_LIGHTING = 0xCB, // Modifica el brillo en modo regulacion fijo
            XCPDT_READ_PATTERN_LIGHTING = 0xCC, // Lectura patron en modo regulacion automatico
            XCPDT_WRITE_PATTERN_LIGHTING = 0xCD,	// Modifica el patron en modo regulacion automatico
        }

        private int CalcAnswerLen(Registers instruction)
        {
            switch (instruction)
            {
                case Registers.XDPDT_READ_COLOR_LEDS: return 6;
                case Registers.XCPDT_READ_EXTENDED_INFO: return 25;
                case Registers.XCPDT_READ_ULTRASOUNDS: return 52;
                case Registers.XCPDT_READ_ULTRASOUNDS_NEW: return 52;
                case Registers.XCPDT_READ_DETECTION_SAMPLES: return 5;
            }
            return 4;
        }

        public override void Dispose()
        {
            if (SerailPort != null)
                SerailPort.Dispose();

            if (Transport != null)
                Transport.Dispose();
        }
    }
}
