using Comunications.Protocolo;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comunications.Message
{
    public class CirbusTransport : MessageTransport
	{
        public CirbusTransport(IStreamResource streamResource)
            : base(new ASCIIProtocol() { CaracterFinRx = "\n", CaracterFinTx="\n" }, streamResource)
        {
            this.ReadTimeout = 2000;
            LogRawMessage = true;
            LogMessage = false;
        }

        public CirbusTransport(IStreamResource streamResource, ILog logger)
            : base(new ASCIIProtocol() { CaracterFinRx = "\n", CaracterFinTx = "\n" }, streamResource, logger)
        {
            this.ReadTimeout = 2000;
            LogRawMessage = true;
            LogMessage = false;
        }

        protected override string GetLogFrameString(byte[] frame)
        {
            return string.Format("{0}", ASCIIEncoding.ASCII.GetString(frame).Replace("\n","")); //,Extensions.BytesToHexString(frame));
        }

        protected override void ValidateResponse(ITransportMessage request, ITransportMessage response)
		{
            var typedRequest = request as CirbusMessage;
            typedRequest.ValidateResponse(response);
		}

        public byte[] Read(byte[] slaveAddress, string message, bool validator = false)
        {
            var data = new CirbusMessage(slaveAddress, message);
            var response = SendMessage<CirbusMessage>(data);
            if (validator)
            {
                data.Validator = (m) =>
                {
                    var resp = ASCIIEncoding.ASCII.GetString(m.Data);

                    if (!resp.StartsWith("ACK"))
                        throw new Exception("Error Cirbus no se ha recibido la respuesta ACK");

                    return true;
                };
                return response.Data.Skip<byte>(3).ToArray();
            }
            return response.Data;
        }

        public byte[] Read(byte[] slaveAddress, string message, Dictionary<byte, string> listErrors)
        {

            var data = new CirbusMessage(slaveAddress, message);
            data.Validator = (m) =>
            {
                var resp = ASCIIEncoding.ASCII.GetString(m.Data);

                if (!resp.StartsWith("ACK"))
                    throw new Exception(SearchErrorinList(resp, listErrors));

                return true;

            };
            var response = SendMessage<CirbusMessage>(data);
            return response.Data.Skip<byte>(3).ToArray();
        }

        public void WriteWithoutResponse(byte[] slaveAddress, string message, Dictionary<byte, string> listErrors = null)
        {
            var data = new CirbusMessage(slaveAddress, message);

            data.HasResponse = false;

            SendMessage<CirbusMessage>(data);
        }

        public void Write(byte[] slaveAddress, string message, Dictionary<byte,string> listErrors=null)
        {
            var data = new CirbusMessage(slaveAddress, message);
            data.Validator = (m) =>
            {
                var resp = ASCIIEncoding.ASCII.GetString(m.Data);

                if (!resp.StartsWith("ACK"))
                {
                    if(listErrors == null)
                        throw new Exception(string.Format("Error en CirbusTransport en el Validator por respuesta {0}",resp));
                    else
                        throw new Exception(SearchErrorinList(resp, listErrors));
                }

                return true;

            };
            var response = SendMessage<CirbusMessage>(data);
        }

        private string SearchErrorinList(string request, Dictionary<byte,string> listErrors)
        {
            var position = request.IndexOf("NA") + 2;
            if (position != -1)
            {
                var resp = Convert.ToByte(request.Substring(position, 1));
                var errorSearch = listErrors.Where(p => p.Key == resp).FirstOrDefault();

                if (errorSearch.Value == null)
                    return "Error de respuesta en la trama Cirbus, pero con tipo de error desconocido";
                else
                    return errorSearch.Value;
            }
            else
                return "Error de respuesta en la trama Cirbus, por no encontrar NA";
        }
	}
}
