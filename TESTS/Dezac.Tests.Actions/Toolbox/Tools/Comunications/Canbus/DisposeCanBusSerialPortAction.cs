﻿using Comunications.Message;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DisposeCanBusSerialPortActionDescription))]
    public class DisposeCanBusSerialPortAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_CAN")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM_CAN";
            var uut = GetVariable(name) as MessageTransport;
            if (uut != null)
                uut.Dispose();
        }
    }

    public class DisposeCanBusSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "DisposedCanBusSerialPort"; } }
        public override string Description { get { return "Cierra el puerto serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/Canbus"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
