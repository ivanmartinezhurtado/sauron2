﻿using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.Utility
{
    public interface IConsoleDevice
    {
        Result<string> WriteCommand(string text);
    }
}
