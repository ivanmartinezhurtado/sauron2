﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;

namespace WinTaskRunner.Utils
{
    public static class AppProfile
    {

        public static string ProgramPath
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }

        public static string GetDataUserFile(string name)
        {
            string path = GetDataUserDir();
            string fileName = Path.Combine(path, name);

            return fileName;
        }

        public static string GetDataUserDir()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WinTaskRunner");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public static string GetConfigFile()
        {
            string name = ConfigurationManager.AppSettings["ConfigFile"] ?? "app.json";

            if (!name.EndsWith(".json", StringComparison.InvariantCultureIgnoreCase))
                name += ".json";

            return Path.Combine(ProgramPath, name);
        }

        public static void ClearConfigFile()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WinTaskRunner");

            if (!Directory.Exists(path))
                return;

            var director = new DirectoryInfo(path);

            foreach (FileInfo file in director.GetFiles())
                file.Delete();
        }

        public static string ReportsPath
        {
            get { return ConfigurationManager.AppSettings["PathReports"]; }
        }

        public static T DeserializeDataUserFile<T>(string name)
        {
            return DeserializeFile<T>(GetDataUserFile(name));
        }

        public static T DeserializeFile<T>(string fileName)
        {
            if (!File.Exists(fileName))
                return default(T);

            string json = File.ReadAllText(fileName);

            return Deserialize<T>(json);
        }

        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static void SerializeDataUserFile(string name, object data)
        {
            SerializeFile(GetDataUserFile(name), data);
        }

        public static void SerializeFile(string fileName, object data)
        {
            File.WriteAllText(fileName, Serialize(data));
        }

        public static string Serialize(object data)
        {
            return JsonConvert.SerializeObject(data);
        }
    }
}
