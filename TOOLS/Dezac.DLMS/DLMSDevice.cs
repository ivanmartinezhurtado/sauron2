﻿using Gurux.Common;
using Gurux.DLMS;
using Gurux.DLMS.Enums;
using Gurux.DLMS.Objects;
using Gurux.DLMS.Secure;
using log4net;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Globalization;


namespace Dezac.DLMS
{
    public class DLMSDevice
    {
        protected static readonly ILog _logger = LogManager.GetLogger("DLMSDevice");
        private DLMSSettings settings = null;
        private GXDLMSReader reader = null;
        private GXDLMSSecureClient client = null;
        private IGXMedia media = null;

        public IGXMedia Media
        {
            set
            {
                if (media == null)
                {
                    media = value;
                    Settings.media = media;
                   // Settings.media.OnTrace += (s, e) => _logger.DebugFormat($"{e.Type}-{e.DataToString(false)}");
                   // Settings.media.OnReceived += (s, e) => _logger.DebugFormat($"{e.Data}-{e.SenderInfo}");
                    Settings.media.OnError += (s, e) => _logger.ErrorFormat($"{e.Data}-{e.Message}");
                    Settings.media.OnClientConnected += (s, e) => _logger.DebugFormat($"{e.Accept}-{e.Info}");
                    Settings.media.OnClientDisconnected += (s, e) => _logger.DebugFormat($"{e.Accept}-{e.Info}");
                    Settings.media.OnMediaStateChange += (s, e) => _logger.DebugFormat($"{e.Accept}-{e.State}");
                }
                else
                {
                    media = value;
                    Settings.media = media;
                }
            }
        }

        public DLMSSettings Settings
        {
            get
            {
                if (settings == null)
                    settings = new DLMSSettings();

                return settings;
            }
        }

        public GXDLMSReader Reader
        {
            get
            {
                if (reader == null)
                    reader = new GXDLMSReader(Settings.client, Settings.media, Settings.trace);

                return reader;
            }
        }

        public GXDLMSSecureClient Client
        {
            get
            {
                if (client == null)
                    client = new GXDLMSSecureClient(true);

                Settings.client = client;
                return settings.client;
            }
        }

        public int RetryCount
        {
            set { Reader.RetryCount = value; }
        }

        public int WaitTime
        {
            set { Reader.WaitTime = value; }
        }

        public string password
        {
            set { Client.Password = ASCIIEncoding.ASCII.GetBytes(value); }
        }

        public int ClientAddress
        {
            set { Client.ClientAddress = value; }
        }

        public void ServerAddres(int serverAddres = 1, int physicalAdress = 0)
        {
            if (physicalAdress == 0)
                Client.ServerAddress = serverAddres;
            else
                Client.ServerAddress = GXDLMSClient.GetServerAddress(serverAddres, Convert.ToInt32(physicalAdress));
        }

        //*********************************************************
        //*********************************************************

        public DLMSDevice(string password = "qxzbravo", int clientAdress = 73, int serverAddres = 1, int physicalAdress = 0, Authentication authentication = Authentication.Low)
        {
            Client.Password = ASCIIEncoding.ASCII.GetBytes(password);
            Client.UseLogicalNameReferencing = true;
            Client.ClientAddress = clientAdress;
            Client.Authentication = authentication;

            if (physicalAdress == 0)
                Client.ServerAddress = serverAddres;
            else
                Client.ServerAddress = GXDLMSClient.GetServerAddress(serverAddres, Convert.ToInt32(physicalAdress));
        }

        public void InitializeConnection()
        {
            Settings.media.Open();
            Reader.InitializeConnection();
        }

        public bool IsOpen()
        {
            if (Settings != null &&  media != null)
                return Settings.media.IsOpen;

            return false;
        }

        public void Disconnect()
        {
            Reader.Disconnect();
        }

        //*********************************************************
        //*********************************************************

        public T ReadRegister<T>(string obis, int attribute)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var obj = Reader.Read(new GXDLMSRegister(obis), attribute);
            var typeobj = obj.GetType().IsArray;
            if (typeobj)
            {
                TypeConverter objConverter = TypeDescriptor.GetConverter(obj.GetType());
                byte[] data = (byte[])Convert.ChangeType(obj, typeof(byte[]));
                obj = Encoding.ASCII.GetString(data);
            }

            return (T)Convert.ChangeType(obj, typeof(T));
        }

        public T ReadData<T>(string obis, int attribute)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var obj = Reader.Read(new GXDLMSData(obis), attribute);
            var typeobj = obj.GetType().IsArray;
            if (typeobj)
            {
                var tipo = obj.GetType().FullName;
                var tipoPedido = typeof(T).FullName;

                if (tipo == tipoPedido)
                    return (T)Convert.ChangeType(obj, typeof(T));

                if (tipo.Trim().ToUpper().Contains("BYTE"))
                {
                    var resul = ((byte[])obj);
                    var res = ArrayToString(resul);
                    obj = res;
                }
                else
                {
                    var resul = ((object[])obj);
                    var res = ArrayToString(resul);
                    obj = res;
                }
            }
            return (T)Convert.ChangeType(obj, typeof(T));
        }

        public T ReadClock<T>(string obis, int attribute)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var obj = Reader.Read(new GXDLMSClock(obis), attribute);
            return (T)Convert.ChangeType(obj, typeof(T));
        }

        public DataTable ReadGenericProfile(string obis, int attribute, DateTime? start = null, DateTime? end = null)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var genericProfile = Reader.ReadGenericProfile(obis, start, end);

            DataTable dt = new DataTable(obis);
            int index = 0;

            foreach (var it in genericProfile.CaptureObjects)
            {
                string[] columns = ((IGXDLMSBase)it.Key).GetNames();
                if (it.Value.AttributeIndex == 0)
                    for (int a = 0; a != ((IGXDLMSBase)it.Key).GetAttributeCount(); ++a)
                    {
                        DataColumn dc = dt.Columns.Add(index.ToString());
                        dc.Caption = it.Key.LogicalName + Environment.NewLine + columns[a];
                        ++index;
                    }
                else
                {
                    DataColumn dc = dt.Columns.Add(index.ToString());
                    string str = it.Key.LogicalName;
                    if (it.Value.AttributeIndex < columns.Length)
                        str += Environment.NewLine + columns[it.Value.AttributeIndex - 1];
                    if (!string.IsNullOrEmpty(it.Key.Description))
                        str += Environment.NewLine + it.Key.Description;
                    dc.Caption = str;
                    ++index;
                }
            }

            for (int pos = dt.Rows.Count; pos < genericProfile.Buffer.Count; ++pos)
            {
                object[] row = genericProfile.Buffer[pos];
                dt.LoadDataRow(row, true);
            }

            return dt;
        }
  
        //*********************************************************
        //*********************************************************

        public void WriteRegister<T>(string obis, int attribute, T value)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            //var obj = Reader.ColectionObject.FindByLN(ObjectType.Register, obis) as GXDLMSRegister;

            var obj = new GXDLMSRegister(obis);
            obj.Value = value;
            Reader.Write(obj, attribute);
        }

        public void WriteData<T>(string obis, int attribute, T value)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var obj = new GXDLMSData(obis);
            obj.Value = value;
            Reader.Write(obj, attribute);
        }

        public void WriteClock(string obis, int attribute, DateTime value)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var obj = new GXDLMSClock(obis);
            obj.Time = value;
            Reader.Write(obj, attribute);
        }

        public void WriteTimeZone(string obis, int attribute, TimeZoneInfo value)
        {
            if (!obis.Contains("."))
                obis = ConvertToObis(obis);

            if (!Settings.media.IsOpen)
                Settings.media.Open();

            var obj = new GXDLMSClock(obis);
            obj.TimeZone = -(int)value.BaseUtcOffset.TotalMinutes;
            Reader.Write(obj, attribute);
        }

        //*********************************************************
        //*********************************************************

        private string ConvertToObis(string obis)
        {
            var resultObis = "";

            if (obis.Length != 12)
                throw new Exception("Longitud del codigo Obis incorrecto");

            for (byte i = 0; i < obis.Length; i += 2)
            {
                var trama = obis.Substring(i, 2);
                resultObis += Convert.ToByte(trama, 16).ToString();
                if (i != 10)
                    resultObis += ".";
            }
            return resultObis;
        }

        private string ArrayToString(object[] parm)
        {
            var resp = "";
            for (int index = 0; index < parm.Length; index++)
                resp += parm[index].ToString() + "\n";

            return resp;
        }

        private string ArrayToString(byte[] parm)
        {
            var resp = "";
            for (int index = 0; index < parm.Length; index++)
                resp += parm[index].ToString() + "\n";

            return resp;
        }

        //*********************************************************
        //*********************************************************

        public void  Update(string path)
        {

          byte[] fichero;
          string identification;
          int blocktoread;
          
          //Lectura de fichero binario.
          FileInfo info = new FileInfo(path);
          fichero = File.ReadAllBytes(path);
          blocktoread = fichero.Length;          
          identification = info.Name; 
            


          //Creación y set de los parametros del objeto Imagetransfer.
          GXDLMSImageTransfer image1 = new GXDLMSImageTransfer();
          image1.ImageBlockSize=64;
          image1.ImageSize =Convert.ToUInt32(blocktoread);

          //Aplicación del proceso de update.
          reader.ImageUpdate(image1, identification, fichero);

        }

        public string SumT(int y,int m,double day,double h,double min,double seg)
        {
            var b = "0.0.1.0.0.255";
            var A = new DateTime();
            var B =Convert.ToDateTime( reader.Read(new GXDLMSClock(b), 2)); 
            A = B.AddDays(day);
            A = A.AddHours(h);
            A = A.AddSeconds(seg);
            A = A.AddMinutes(min);
            A = A.AddMonths(m);
            A = A.AddYears(y);
            WriteClock(b, 2, A);
            var C=A.ToString("dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
            return C;
        }

        public void Read_and_write_profile(int periodo,int atributo, string obis)
        {
            GXDLMSProfileGeneric A = new GXDLMSProfileGeneric(obis);

            A.CapturePeriod = periodo;

            reader.Write(A, atributo);

        }
        
        public void Read_profile( int atributo, string obis)
        {
            GXDLMSProfileGeneric A = new GXDLMSProfileGeneric(obis);

            reader.Read(A, atributo);

        }

        public void write_register<T>(T val, int atributo, string obis)
        {

            GXDLMSRegister B = new GXDLMSRegister(obis);

            B.Value = val;
            reader.Write(B, atributo);

        }

    }













}












