﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace Dezac.Tests
{
    public class ThreadBase : IDisposable
    {
        [Browsable(false)]
        [JsonIgnore]
        public bool IsTestError { get { return SequenceContext.Current.TestResult.ExecutionResult != TestExecutionResult.Completed; } }

        public bool IsInstanceRunning(int numInstance) => RunnerHelper.IsInstanceRunning(numInstance);

        public bool WaitAllTasksAndRunOnlyOnce(int numTasksToWait, string signal, Action action) => RunnerHelper.WaitAllTasksAndRunOnlyOnce(numTasksToWait, signal, action);
        public T WaitAllTasksAndRunOnlyOnce<T>(int numTasksToWait, string signal, Func<T> action, T varType) => RunnerHelper.WaitAllTasksAndRunOnlyOnce<T>(numTasksToWait, signal, action, varType);

        public void WaitAllTests(string signal) => RunnerHelper.WaitAllTests(signal);
        public void RunOnlyOnce(string signal, Action action) => RunnerHelper.RunOnlyOnce(signal, action);
        public Exception WaitAllTestsAndRunOnlyOnce(string signal, Action action) => RunnerHelper.WaitAllTestsAndRunOnlyOnce(signal, action);
        public bool RunOnlyLastTest(string signal, Action action) => RunnerHelper.RunOnlyLastTest(signal, action);
        public void RunTestSequentiallyOrdered(string signal, Action action) => RunnerHelper.RunTestSequentiallyOrdered(signal, action);

        [Browsable(false)]
        [JsonIgnore]
        public SharedVariablesList SharedVariables => SequenceContext.Current.SharedVariables;
        public bool HasSharedVariable(string key) => SharedVariables.ContainsKey(key);
        public void SetSharedVariable(string key, object value) => SharedVariables.AddOrUpdate(key, value);
        public object GetSharedVariable(string key) => SharedVariables.Get(key);
        public T GetSharedVariable<T>(string key) => SharedVariables.Get<T>(key);
        public T GetSharedVariable<T>(string key, T def) => SharedVariables.Get<T>(key, def);
        public T AddOrGetSharedVar<T>(string key, Func<T> func) => SharedVariables.AddOrGet<T>(key, func);
        public bool RemoveSharedVar(object instance) => SharedVariables.RemoveInstance(instance);
        public bool RemoveSharedVar(string key) => SharedVariables.Remove(key);

        [Browsable(false)]
        [JsonIgnore]
        public SharedVariablesList Variables => SequenceContext.Current.Variables;
        public virtual void SetVariable(string key, object value) => Variables.AddOrUpdate(key, value);
        public object GetVariable(string key) => Variables.Get(key);
        public T GetVariable<T>(string key, T def) => Variables.Get<T>(key, def);
        public T GetVariable<T>(string key) => Variables.Get<T>(key);
        public T GetVariable<T>(string key, Func<T> def) => Variables.Get<T>(key, def);
        public void RemoveVariable(string key) => Variables.Remove(key);
        public T GetInstanceVar<T>() => Variables.Get<T>();
        public T AddInstanceVar<T>(T instance, string name = null)
        {
            SetVariable(name ?? typeof(T).Name, instance);
            return instance;
        }
        public T AddOrGetInstanceVar<T>(string key, Func<T> func)
        {
            if (Variables.ContainsKey(key))
                return RunnerHelper.ChangeType<T>(Variables.Get(key));

            T instance = func();
            SetVariable(key, instance);

            return instance;
        }
        public bool VariablesContainsKey(string key) => Variables.ContainsKey(key);

        [Browsable(false)]
        [JsonIgnore]
        public VariableLog VariablesLog => SequenceContext.Current.Variables.VariableLogger;

        public virtual void Dispose()
        {
        }
    }
}
