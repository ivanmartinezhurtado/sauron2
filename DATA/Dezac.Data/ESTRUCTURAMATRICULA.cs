namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ESTRUCTURAMATRICULA")]
    public partial class ESTRUCTURAMATRICULA
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMESTRUCTURA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public int NUMMATRICULA { get; set; }

        public DateTime FECHA { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        public virtual MATRICULA MATRICULA { get; set; }
    }
}
