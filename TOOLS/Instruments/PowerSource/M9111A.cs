﻿using Instruments.Measure;
using Instruments.Utility;
using Keysight.KtM911x.Interop;
using Ivi.Driver.Interop;
using log4net;
using System;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.00)]
    public class M9111A : MeasureBase<M9111A>, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(M9111A));

        private KtM911x driver;
        private IKtM911xOutputChannel outputChannel;
        private IKtM911xMeasurement measureChannel;
        private IKtM911xTrigger triggerChannel;

        public KtM911x PowerSourceM9111A
        {
            get
            {
                if(driver == null)
                    driver = new KtM911x();

                return driver;
            }
        }

        public IKtM911xOutputChannel OutputChannel
        {
            get
            {
                if (outputChannel == null)
                {
                    string OutputChannelName = string.Empty;
                    OutputChannelName = driver.Outputs.Name[1];
                    outputChannel = driver.Outputs.Item[OutputChannelName];
                }

                return outputChannel;
            }
        }

        public IKtM911xMeasurement MeasureChannel
        {
            get
            {
                if (measureChannel == null)
                {
                    // get interface pointer to the measurement channel
                    string MeasurementChannelName = string.Empty;
                    MeasurementChannelName = driver.Measurements.Name[1];
                    measureChannel = driver.Measurements.Item[MeasurementChannelName];
                }

                return measureChannel;
            }
        }

        public IKtM911xTrigger TriggerChannel
        {
            get
            {
                if (triggerChannel == null)
                {
                    string TriggerChannelName = string.Empty;
                    TriggerChannelName = driver.Triggers.Name[1];
                    triggerChannel = driver.Triggers.Item[TriggerChannelName];
                }

                return triggerChannel;
            }
        }

        public M9111A()
        {
          
        }

        public bool IsConnected
        {
            get
            {
                return PowerSourceM9111A.Initialized;
            }
        }

        public double MeasuredVoltage { get; set; }

        public double MeasuredCurrent { get; set; }

        //******************************************
    

        public void Connect()
        {
            string resourceDesc = "PXI11::0::0::INSTR"; // "PXI0::17-12.0::INSTR"; //PXI0::15-13.0::INSTR

            // if the model is not supported by the driver
            bool idQuery = false;
            // If true, the instrument is reset at initialization
            bool reset = false;
            // Setup IVI-defined initialization options
            string initOptions = string.Format("Simulate=false, DriverSetup= Model=, Trace=false");
            PowerSourceM9111A.Initialize(resourceDesc, idQuery, reset, initOptions);

            GetIdentification();
        }

        public string GetIdentification()
        {
            logger.DebugFormat("Identifier:  {0}", PowerSourceM9111A.Identity.Identifier);
            logger.DebugFormat("Revision:    {0}", PowerSourceM9111A.Identity.Revision);
            logger.DebugFormat("Vendor:      {0}", PowerSourceM9111A.Identity.Vendor);
            logger.DebugFormat("Description: {0}", PowerSourceM9111A.Identity.Description);
            logger.DebugFormat("Model:       {0}", PowerSourceM9111A.Identity.InstrumentModel);
            logger.DebugFormat("FirmwareRev: {0}", PowerSourceM9111A.Identity.InstrumentFirmwareRevision);
            logger.DebugFormat("Serial #:    {0}", PowerSourceM9111A.System.SerialNumber);

            return PowerSourceM9111A.Identity.Identifier;
        }

        public void Dispose()
        {
            driver.Close();
            driver = null;
            outputChannel = null;
            measureChannel = null;
            triggerChannel = null;
        }

        public void ApplyVoltage(double voltageLevel, double positiveLimitCurrent, double negativeLimitCurent)
        { 
            ShowOperatingCondition();

            // configure the output with voltage priority, voltage level = 1.23V, current limit = 0.5A, and enable the output
            OutputChannel.PriorityMode = KtM911xPriorityModeEnum.KtM911xPriorityModeVoltage;
            OutputChannel.Voltage.Level = voltageLevel;
            OutputChannel.Current.PositiveLimit = positiveLimitCurrent;
            OutputChannel.Current.NegativeLimit = negativeLimitCurent;
            OutputChannel.Enabled = true;
        }

        public void ApplyOFF()
        {
            if (OutputChannel != null)
                OutputChannel.Enabled = false;
        }

        public double ReadMeasure(string magnitud, int offset =0, int totalPoints = 1000, double rangCurrent=1000)
        {        
            // wait for output to settle
            //OutputChannel.Relay.WaitForSettled();
            ShowOperatingCondition();
            MeasureChannel.Sweep.OffsetPoints = offset;
            MeasureChannel.Sweep.Points = totalPoints;
            MeasureChannel.SenseCurrentRange = rangCurrent;
            logger.InfoFormat("   Sweep: Offset {0}, Total {1}  Rang {2}", offset, totalPoints, rangCurrent);

            if (magnitud.Contains("Volt"))
            {

                MeasuredVoltage = MeasureChannel.Measure(KtM911xMeasurementTypeEnum.KtM911xMeasurementVoltage);
                logger.InfoFormat("Output Measurements: {0:0.0##} V", MeasuredVoltage);
                return MeasuredVoltage;
            }
            else
            {
                MeasuredCurrent = MeasureChannel.Measure(KtM911xMeasurementTypeEnum.KtM911xMeasurementCurrent); //MeasureChannel.Fetch(KtM911xMeasurementTypeEnum.KtM911xMeasurementCurrent, 1);
                logger.InfoFormat("Output Measurements: {0:0.0###} A",  MeasuredCurrent);
                return MeasuredCurrent;
            }
        }

        //*******************************************

        private void ShowOperatingCondition()
        {
            int status = 0;
            // get output status and measurements
            status = driver.Status.GetStatus(KtM911xGetStatusRegister.KtM911xGetStatusRegisterOperationCondition);
            logger.InfoFormat("   Operation status: {0}", status);
            status = driver.Status.GetStatus(KtM911xGetStatusRegister.KtM911xGetStatusRegisterQuestionableCondition);
            logger.InfoFormat("Questionable status: {0}", status);
        }

        private void AcquireTriggeredMeasurement()
        {
            logger.InfoFormat(">> AcquireTriggeredMeasurement()...");
            
            // setup trigger source - Software Trigger
            TriggerChannel.Measurement.Source = KtM911xTriggerSourceEnum.KtM911xTriggerSourceSoftware;
            // arm the trigger system
            TriggerChannel.Measurement.Initiate();
            // Note: If pre-trigger acquisition is used, delay until pre-trigger acquisition is filled
            // before triggering the acquisition, otherwise trigger is ignored.
            if (MeasureChannel.Sweep.OffsetPoints < 0)
            {
                // wait until KtM911xOperationStatusBitWTGMeas is set
                for (;;)
                {
                    int WTG_meas = driver.Status.GetStatus(KtM911xGetStatusRegister.KtM911xGetStatusRegisterOperationCondition);
                    if ((WTG_meas & (int)KtM911xOperationStatusBitEnum.KtM911xOperationStatusBitWTGMeas)
                        == (int)KtM911xOperationStatusBitEnum.KtM911xOperationStatusBitWTGMeas) break;
                }
            }
            // generate an 0.0V-5.0V step transient
            OutputChannel.Voltage.Level = 5.0;
            // Send the software trigger to start the measurement
            driver.Triggers.SendSoftwareTrigger();
            // delay for acquisition to complete
            System.Threading.Thread.Sleep(50);

            ShowArrayMeasurement();

            logger.InfoFormat("<< AcquireTriggeredMeasurement() {0}");
        }

        private void AcquireInitiateMeasurement()
        {
            logger.InfoFormat(">> AcquireInitiatedMeasurement()...");
            // setup trigger source - Immediate trigger
            // Immediate trigger source is always enabled, it does not need to be set
            // arm the trigger system
            TriggerChannel.Measurement.Initiate();
            // Note: If pre-trigger acquisition is used, delay until pre-trigger acquisition is filled
            // before triggering the acquisition, otherwise trigger is ignored.
            if (MeasureChannel.Sweep.OffsetPoints < 0)
            {
                // wait until KtM911xOperationStatusBitWTGMeas is set
                for (;;)
                {
                    int WTG_meas = driver.Status.GetStatus(KtM911xGetStatusRegister.KtM911xGetStatusRegisterOperationCondition);
                    if ((WTG_meas & (int)KtM911xOperationStatusBitEnum.KtM911xOperationStatusBitWTGMeas)
                        == (int)KtM911xOperationStatusBitEnum.KtM911xOperationStatusBitWTGMeas) break;
                }
            }

            ShowArrayMeasurement();

            logger.InfoFormat("<< AcquireImmediatedMeasurement()");

        }

        private void AcquireImmediateMeasurement()
        {
            logger.InfoFormat(">> AcquireImmediatedMeasurement()...");
            // generate an 0.0V-5.0V step transient
            OutputChannel.Voltage.Level = 5.0;
            // use immediate trigger to trigger (start) the acquisition
            TriggerChannel.Measurement.Immediate();
            // delay for acquisition to complete
            System.Threading.Thread.Sleep(50);

            ShowArrayMeasurement();

            logger.InfoFormat("<< AcquireImmediatedMeasurement()");
        }

        private void ShowArrayMeasurement()
        {
            // allocate buffers for measurement data
            int bufsize = MeasureChannel.Sweep.Points;
            double[] CurrentArray = new double[bufsize];
            double[] VoltageArray = new double[bufsize];
            // fetch measured acquisition
            // in case acquisition is not done, allow some time (Timeout = 2.0) for it to finish
            MeasureChannel.FetchArray(KtM911xMeasurementTypeEnum.KtM911xMeasurementCurrent, ref CurrentArray, 2.0);
            // acquisition is already done, no extra time is needed (Timeout = 0.0)
            MeasureChannel.FetchArray(KtM911xMeasurementTypeEnum.KtM911xMeasurementVoltage, ref VoltageArray, 0.0);

            logger.InfoFormat("   Measurement Array Data ...");
            for (int i = 0; i < 50; i++)
            {
                logger.InfoFormat(" {0:0#}: {1:0.000} V, {2:0.0000} A", i, VoltageArray[i], CurrentArray[i]);
            }
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            throw new NotImplementedException();
        }
    }
}
