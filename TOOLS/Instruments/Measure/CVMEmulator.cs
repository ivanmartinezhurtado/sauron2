﻿using Comunications.Message;
using Comunications.Utility;
using Dezac.Core.Exceptions;
using Dezac.Device;
using log4net;
using System;
using System.Globalization;
using System.IO.Ports;
using System.Runtime.InteropServices;
using TaskRunner.Model;

namespace Instruments.Measure
{
    [DeviceVersion(1.00)]
    public class CVMEmulator : DeviceBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("CVMEmulator");
        private bool FirstDataRecived { get; set; }
        public CVMEmulator()
        {
        }

        public CVMEmulator(string port, byte periferico, int baudRate = 19200)
        {
            SetPort(port, periferico, baudRate);
        }

        public void SetPort(string port, byte periferico, int baudRate = 19200)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
            {
                if (ModbusDevice == null)
                    ModbusDevice = new ModbusDeviceSerialPort(result, baudRate, Parity.None, StopBits.One, logger);

                ModbusDevice.PortCom = result;
                ModbusDevice.PerifericNumber = periferico;
                ModbusDevice.SerialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceived);
                ModbusDevice.OpenPort();
                FirstDataRecived = false;
            }
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public ModbusDeviceSerialPort ModbusDevice { get; internal set; }

        private void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            LogMethod();
            logger.InfoFormat("Call to DataRecived Event type {0}, {1}", e.EventType);
            FirstDataRecived = true;
        }

        private ModbusMessage internalReadDataReceived(int secondsWait = 10)
        {
            LogMethod();
            var cancel = SequenceContext.Current.CancellationToken;
            FirstDataRecived = false;
            var timeSpend = new TimeSpan(0,0, secondsWait);
            var timeNow = DateTime.Now;
            do
            {
                logger.InfoFormat("Wait to Recived Data Event");
                var ellapsedTime = DateTime.Now.Subtract(timeNow);
                if (ellapsedTime.TotalMilliseconds > timeSpend.TotalMilliseconds)
                    TestException.Create().UUT.COMUNICACIONES.TIME_OUT("NO SE HA RECIBIDO NINGUN EVENTO DE COMUNICACIONES").Throw();

                if (cancel.IsCancellationRequested)
                    TestException.Create().PROCESO.OPERARIO.TEST_CANCELADO().Throw();

            } while (!FirstDataRecived);

            logger.InfoFormat("Modbus.ReadRequest");
            return ModbusDevice.ReadRequest();
        }

        public override void Dispose()
        {
            if (ModbusDevice != null)
            {
                ModbusDevice.SerialPort.DataReceived -= new SerialDataReceivedEventHandler(DataReceived);
                ModbusDevice.Dispose();
                ModbusDevice = null;
            }
        }

        public object ReadDataRecived(int secondsWait)
        {
            FirstDataRecived = true;
            var response = internalReadDataReceived(secondsWait);
            var result = ReturnValueByDataRecived(response);
            ModbusDevice.WriteRequest((ModbusMessage)result.message);
            return result.value; 
        }

        private ResponseMessage ReturnValueByDataRecived(ModbusMessage message)
        {
            ResponseMessage request;

            switch (message.FunctionCode)
            {
                case Modbus.ReadCoils:
                case Modbus.ReadInputs:
                    request = CreateModbusReadCoilsValue(message);
                    break;
                case Modbus.ReadHoldingRegisters:
                case Modbus.ReadInputRegisters:
                    request = CreateModbusReadInputRegistersValue(message);
                    break;
                case Modbus.WriteSingleCoil:
                    request = CreateModbusWriteSingleCoilValue(message);
                    break;
                case Modbus.WriteSingleRegister:
                    request = CreateModbusWriteSingleRegisterValue(message);
                    break;
                case Modbus.WriteMultipleCoils:
                    request = CreateModbusWriteMultipleCoilsValue(message);
                    break;
                case Modbus.WriteMultipleRegisters:
                    request = CreateModbusWriteMultipleRegistersValue(message);
                    break;
                default:
                    throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Unsupported function code {0}", message.FunctionCode), "frame");
            }

            return request;
        }

        public struct ResponseMessage
        {
            public ModbusMessage message { get; set; }
            public object value { get; set; }
        }

        #region ReadInput ReadHolging Value
        private ResponseMessage CreateModbusReadInputRegistersValue(ModbusMessage message)
        {
            ResponseMessage request;

            switch (message.StartAddress)
            {
                case 0:
                    request = ReturnVariablesInstantaneas(message.ByteCount);
                    break;
                default:
                    throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Unsupported address {0}", message.StartAddress), "frame");
            }

            return request;
        }

        private ResponseMessage ReturnVariablesInstantaneas(byte? byteCount)
        {
            ResponseMessage request = new ResponseMessage();

            var value = new VariablesFase()
            {
                //Corriente = new Random(),

            };

            request.value = value;
            request.message = new ModbusMessage();
            return request;
        }

        #region Structures And Enums

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VariablesFase
        {
            public double Tension { get; set; }
            public double Corriente { get; set; }
            public double PotenciaActiva { get; set; }
            public double PotenciaReactivaInductiva { get; set; }
            public double PotenciaReactivaCapicitiva { get; set; }
            public double PotenciaAparente { get; set; }
            public double FactorPotencia { get; set; }
            public double CosenoPhi { get; set; }
            public double Angle { get; set; }
        };

        #endregion

        #endregion

        #region ReadColis Value
        private ResponseMessage CreateModbusReadCoilsValue(ModbusMessage message)
        {
            ResponseMessage request = new ResponseMessage();
            return request;
        }

        #endregion

        #region ReadWriteSingleCoil
        private ResponseMessage CreateModbusWriteSingleCoilValue(ModbusMessage message)
        {
            ResponseMessage request = new ResponseMessage();
            return request;
        }

        #endregion

        #region WriteSingleRegisterValue
        private ResponseMessage CreateModbusWriteSingleRegisterValue(ModbusMessage message)
        {
            ResponseMessage request = new ResponseMessage();
            return request;
        }

        #endregion

        #region WriteMultipleCoilsValue
        private ResponseMessage CreateModbusWriteMultipleCoilsValue(ModbusMessage message)
        {
            ResponseMessage request = new ResponseMessage();
            return request;
        }

        #endregion

        #region WriteMultipleRegister
        private ResponseMessage CreateModbusWriteMultipleRegistersValue(ModbusMessage message)
        {
            ResponseMessage request = new ResponseMessage();
            return request;
        }

        #endregion
    }

}
