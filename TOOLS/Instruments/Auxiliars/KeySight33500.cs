﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using Ivi.Visa.Interop;
using log4net;
using System;
using System.Globalization;

namespace Instruments.Auxiliars
{
    [InstrumentsVersion(1.00)]
    public class KeySight33500 : ISignalGenerator, IConsoleDevice, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(KeySight33500));
        protected MessageTransport transport;
        protected IProtocol protocol;

        private CultureInfo enUS = new CultureInfo("en-US");

        private FormattedIO488 _Gpib;
        private ResourceManager _mgr;
        private byte _adrr = 4;
        private bool _IsOpen = false;

        private const string SET_SELECT_CHANNEL = "SOURce{0}";
        private const string SET_FUNCTION = "FUNC {0}";
        private const string SET_FREQUENCY = "FREQ {0}";
        private const string SET_VOLTAGE = "VOLT {0}";
        private const string SET_VOLTAGE_MIN = "VOLT:LOW {0}";
        private const string SET_VOLTAGE_MAX = "VOLT:HIGH {0}";
        private const string SET_VOLTAGE_OFFSET = "VOLT:OFFS {0}";
        private const string SET_PHASE = "PHAS {0}";
        private const string SET_OUTPUT = "OUTP{0} {1}";
        private const string SET_OUTPUT_LOAD = "OUTPut{0}:LOAD {1}";
        private const string SET_DUTY_CYCLE = "FUNC:{0}:DCYC {1}";
        private const string SET_VOLTAGE_UNIT = "SOURce{0}:VOLT:UNIT {1}";

        public KeySight33500()
        {
            _Gpib = new FormattedIO488();
            protocol = new StringProtocol() { CaracterFinRx = "\r\n", CaracterFinTx = "\r\n" };
            transport = new MessageTransport(protocol, new GpibPortAdapter(_Gpib), logger);
            _mgr = new ResourceManager();
        }

        public byte portAddress
        {
            get { return _adrr; }
            set
            {
                Close();
                _adrr = value;
            }
        }

        private void Close()
        {
            if (_IsOpen && _Gpib.IO != null)
                _Gpib.IO.Close();

            _IsOpen = false;
        }

        private void Open()
        {
            Close();

            _Gpib.IO = (IMessage)_mgr.Open("GPIB0::" + _adrr.ToString(), AccessMode.NO_LOCK, 2000, "");
            _Gpib.IO.TerminationCharacterEnabled = true;
            _IsOpen = true;
        }

        private bool IsOpen()
        {
            return _Gpib != null && _Gpib.IO != null;
        }

        public ISignalGenerator On(int channel)
        {
            WriteCommand(SET_OUTPUT, channel, "ON");
            return this;
        }

        public ISignalGenerator Off(int channel)
        {
            WriteCommand(SET_OUTPUT, channel, "OFF");
            return this;
        }

        public KeySight33500 Reset()
        {
            WriteCommand("*RST");
            return this;
        }

        public KeySight33500 SetOutputLoad(int channel, string impedance)
        {
            WriteCommand(SET_OUTPUT_LOAD, channel, impedance);
            return this;
        }

        public ISignalGenerator SetSignal(SignalInfo info, bool apply = false)
        {
            SetVoltageUnit(info.UnitVoltage, info.Channel);
            SetFunction(info.Channel, info.Function, info.DutyCycle);
            SetFrequency(info.Channel, info.Frequency);
            SetVoltage(info.Channel, info.Amplitude, info.MinVoltage, info.MaxVoltage, info.VoltageOffset);
            SetPhase(info.Channel, info.Phase);

            if (apply)
                On(info.Channel);

            return this;
        }

        private KeySight33500 SetVoltageUnit(UnitVoltage unit, int channel = 1)
        {
            WriteCommand(SET_VOLTAGE_UNIT, channel, unit.ToString());
            return this;
        }

        private KeySight33500 SetFunction(int channel, SignalFunction function, double? dutyCycle = null)
        {
            string command = string.Format(SET_SELECT_CHANNEL, channel) + ":" +
                             string.Format(SET_FUNCTION, GetSignalType(function));

            WriteCommand(command);

            if (dutyCycle.HasValue)
            {
                if (function == SignalFunction.PULSE || function == SignalFunction.SQUARE)
                    WriteCommand(string.Format(SET_SELECT_CHANNEL, channel) + ":" + string.Format(SET_DUTY_CYCLE, GetSignalType(function), dutyCycle));
                else
                    throw new Exception("La propiedad ciclo de trabajo solo puede ser asignada a formas de onda tipo pulso o cuadradas");
            }

            return this;
        }

        private KeySight33500 SetFrequency(int channel, double? frequency)
        {
            if (frequency.HasValue)
            {
                string command = string.Format(SET_SELECT_CHANNEL, channel) + ":" +
                                 string.Format(SET_FREQUENCY, frequency);

                WriteCommand(command);
            }
            return this;
        }

        private KeySight33500 SetVoltage(int channel, double? amplitude, double? minVoltage = null, double? maxVoltage = null, double? voltageOffset = null)
        {
            if (amplitude.HasValue)
                WriteCommand(string.Format(SET_SELECT_CHANNEL, channel) + ":" + StringMessage.Create(enUS, SET_VOLTAGE, amplitude).Text);

            if (minVoltage.HasValue)
                WriteCommand(string.Format(SET_SELECT_CHANNEL, channel) + ":" + StringMessage.Create(enUS, SET_VOLTAGE_MIN, minVoltage).Text);

            if (maxVoltage.HasValue)
                WriteCommand(string.Format(SET_SELECT_CHANNEL, channel) + ":" + StringMessage.Create(enUS, SET_VOLTAGE_MAX, maxVoltage).Text);

            if (voltageOffset.HasValue)
                WriteCommand(string.Format(SET_SELECT_CHANNEL, channel) + ":" + StringMessage.Create(enUS, SET_VOLTAGE_OFFSET, voltageOffset).Text);

            return this;
        }

        private KeySight33500 SetPhase(int channel, double? phase)
        {
            if (phase.HasValue)
            {
                string command = string.Format(SET_SELECT_CHANNEL, channel) + ":" +
                                 string.Format(SET_PHASE, phase);

                WriteCommand(command);
            }

            return this;
        }

        public string GetIdentification()
        {
            var resp = WriteCommand("*IDN?").Value.Split(',');
            return string.Format("{0}, MODEL={1}, SN={2}, VERSION={3}", resp[0].ToString().ToUpper(), resp[1], resp[2], resp[3]);
        }

        private Result<string> WriteCommand(string format, params object[] args)
        {
            return WriteCommand(StringMessage.Create(enUS, format, args).Text);
        }

        public Result<string> WriteCommand(string text)
        {
            try
            {
                if (!IsOpen())
                    Open();

                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                response = transport.SendMessage<SCPIMessage>(new SCPIMessage { Text = "SYST:ERR?" });
                var tokens = response.Text.Split(',');
                int result = Convert.ToInt32(tokens[0]);
                if (result != 0)
                    throw new Exception(string.Format("Keysight33500 error: {0}", response.Text));

                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetSignalType(SignalFunction function)
        {
            string argument = string.Empty;

            switch (function)
            {
                case SignalFunction.PULSE:
                    argument = "PULS";
                    break;
                case SignalFunction.RAMP:
                    argument = "RAMP";
                    break;
                case SignalFunction.NRAMP:
                    argument = "NRAMP";
                    break;
                case SignalFunction.SINUSOIDAL:
                    argument = "SIN";
                    break;
                case SignalFunction.SQUARE:
                    argument = "SQU";
                    break;
                case SignalFunction.TRIANGULAR:
                    argument = "TRI";
                    break;
                case SignalFunction.NOISE:
                    argument = "NOIS";
                    break;
                case SignalFunction.ARBITRARY:
                    argument = "ARB";
                    break;
                case SignalFunction.DC:
                    argument = "DC";
                    break;
            }
            return argument;
        }

        public void Dispose()
        {
            Close();
            _Gpib = null;
            _mgr = null;
            transport.Dispose();
        }
    }


}
