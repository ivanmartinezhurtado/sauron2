﻿using Dezac.Core.Exceptions;
using Dezac.Data;
using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Extensions;
using Dezac.Tests.Forms;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Instruments.Towers;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Utils
{
    public static class BastidorService
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(BastidorService));

        private static List<InputMatrixImageItemView> items;

        public static void BastidorAutoReader(TestBase testBase, int? ledOutput, string serialNumberIDS, TestHalconExtension.TypeBarcode barCodeType,
            byte retry =3, int row1 = 0, int column1 = 0, int row2 = 0, int column2 = 0, double resizeFactor = 1)
        {
            logger.Info("Realizando lectura automatica del bastidor");

            var cacheSvc = testBase.GetService<ICacheService>();
            ITower tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();

            string bastidor = null;

            if (ledOutput != null)
            {
                tower.IO.DO.On((int)ledOutput);
                try
                {
                    bastidor = testBase.TestHalconReadBarCodeProcedure(serialNumberIDS, barCodeType, "BARCODE", "BARCODE", retry, null, row1, column1, row2, column2, resizeFactor);
                }
                finally
                {
                    tower.IO.DO.Off((int)ledOutput);
                }
            }
            else
                bastidor = testBase.TestHalconReadBarCodeProcedure(serialNumberIDS, barCodeType, "BARCODE", "BARCODE", retry, null, row1, column1, row2, column2, resizeFactor);

            logger.Info("Lectura automatica del bastidor realizada con exito");
            testBase.Resultado.Set("BASTIDOR_AUTOLECTURA", bastidor.ToString(), ParamUnidad.SinUnidad);

            List<string> bastidores = new List<string>();
            bastidores.Add(bastidor);
            SequenceContext.Current.SharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, bastidores);

            cacheSvc.AddOrSet("BASTIDOR_AUTOREADER", true);
        }

        public static void BastidorController(TestBase testBase, int Rows = 1, int Columns = 1)
        {
            logger.Info("Validando bastidores");

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var data = context.Services.Get<ITestContext>();
            if (data == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CARGA_SERVICIO("ITestContext").Throw();


            if (data.modoPlayer == ModoTest.TIC)
            {
                List<string> matricula = new List<string>();
                matricula.Add(data.TestInfo.NumBastidor.ToString());
                SequenceContext.Current.SharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, matricula);
            }
            else
            {
                var bastidores = testBase.GetSharedVariable<List<string>>(ConstantsParameters.TestInfo.BASTIDORES, null);
                if (bastidores == null)
                {
                    items = new List<InputMatrixImageItemView>();

                    var result = iShell.ShowDialog("BASTIDORES", () => { return new InputMatrixImageView(Rows, Columns, context.TotalNumInstances, CreateMatrixItemController, ValidateView); }, MessageBoxButtons.OKCancel);

                    Assert.IsTrue(result == DialogResult.OK, TestException.Create().UUT.NUMERO_DE_BASTIDOR.CANCELADO_POR_USUARIO("El usuario ha cancelado la entrada del bastidor!"));
                }
            }

            if (data.NumOrden != null && data.NumProducto == 0)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("NO HAY ORDEN o PRODUCTO").Throw();

            logger.Info("Bastidores validados correctamente");
        }

        private static Control CreateMatrixItemController(int row, int col, int num)
        {
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            Image image = Properties.Resources.barcode_scan_icon; 

            var ctrl = new InputMatrixImageItemView
            {
                Title = string.Format("Nº BASTIDOR {0}", num + 1),
                Image = image
            };

            var width = ctrl.Width * col < (Screen.PrimaryScreen.Bounds.Width - 100) ? ctrl.Width : (Screen.PrimaryScreen.Bounds.Width - 100) / col;
            ctrl.Width = width;

            items.Add(ctrl);

            return ctrl;
        }

        private static bool ValidateView(InputMatrixImageView form)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            var codes = items.Select(p => p.InputText).ToList();

            int num;

            var sb = new StringBuilder();

            for (int i = 0; i < codes.Count; i++)
            {
                var isEmpty = string.IsNullOrEmpty(codes[i]);
                var view = form.GetView<InputMatrixImageItemView>(i);

                if (!view.IsDisabled)
                {
                    if (isEmpty)
                    {
                        view.IsValid = false;
                        sb.AppendFormat("Falta informar el bastidor nº {0}!\n", (i + 1));
                    }
                    else if (!int.TryParse(codes[i], out num))
                    {
                        view.IsValid = false;
                        sb.AppendFormat("El nº de bastidor {0} no es válido!\n", (i + 1));
                    }
                    else
                        view.IsValid = true;
                }
            }

            if (codes.GroupBy(p => p).Any(p => p.Count() > 1 && !string.IsNullOrEmpty(p.Key)))
                sb.Append("Hay nº de bastidores repetidos!");

            if (sb.Length > 0)
            {
                iShell.MsgBox(sb.ToString(), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            SequenceContext.Current.SharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, codes);

            foreach(InputMatrixImageItemView item in items)
            {
                item.Image.Dispose();
                item.Image = null;
            }

            return true;
        }

        public static void BastidorValidation(bool Trazabilidad, bool SortIntroComponents, bool SearchNumSerieTestFaseBefore, bool AddSubconjuntosSinRegistrar)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var data = context.Services.Get<ITestContext>();
            if (data == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CARGA_SERVICIO("ITestContext").Throw();

            var bastidores = context.SharedVariables.Get(ConstantsParameters.TestInfo.BASTIDORES) as List<string>;
            Assert.IsNotNull(bastidores, TestException.Create().UUT.NUMERO_DE_BASTIDOR.CANCELADO_POR_USUARIO("El usuario ha cancelado la entrada del bastidor!"));
            var text = bastidores[context.NumInstance - 1];
            Assert.IsFalse(string.IsNullOrEmpty(text), TestException.Create().UUT.NUMERO_DE_BASTIDOR.NO_GRABADO("La instancia no tiene bastidor"));
            int bastidor = Convert.ToInt32(text);

            using (DezacService svc = new DezacService())
            {
                var producto = svc.GetProducto(data.NumProducto, data.Version);

                var IDPBastidor = (IsIDPBastidor(bastidor) && (context.RunMode == RunMode.Debug || data.modoPlayer == ModoTest.SAT));
                if (!IDPBastidor)
                {
                    logger.InfoFormat("Entro en modo {0}, para obtener numero de fabricación", data.modoPlayer);

                    logger.Info("Comprobamos que el bastidor es una matricula que existe en la BBDD Oracle");
                    var Matricula = svc.GetMatricula(bastidor);
                    if (Matricula == null)
                        throw new Exception(string.Format("Bastidor del equipo {0} no registrado!", producto.ARTICULO.DESCRIPCION));

                    if (data.modoPlayer == ModoTest.Normal || data.modoPlayer == ModoTest.TIC || (data.modoPlayer == ModoTest.SAT && data.NumOrden != 0))
                    {
                        if (data.NumOrden == null || data.NumOrden.GetValueOrDefault() == 0)
                            throw new Exception("No se puede realizar un test en modo PRODUCCION o en modo TIC sin Orden de Fabricacion");

                        if (bastidor.ToString().Length < 7)
                            throw new Exception(string.Format("Bastidor {0} longitud incorrecta!", bastidor));

                        if ((data.IdFase != "038" && data.IdFase != "039") || data.IsAOITrace)
                        {
                            logger.InfoFormat("Fase = {0} --> Si soy fase != 038 y != 039 or IsAOITrace = {1}", data.IdFase, data.IsAOITrace);

                            logger.InfoFormat("Comprobamos si este bastidor es OK en las fases anteriores");

                            svc.GetResultadoNumTestFasesIsOK(bastidor, data.NumOrden, data.IdFase, data.IsReproceso);

                            logger.InfoFormat("Obtenemos el numero de fabricación de Orden Prodcto");
                            var nuFabricacion = svc.GetNumFabricacion(data.NumOrden.Value, bastidor);
                            if (nuFabricacion == 0)
                                throw new Exception("No se ha obtenido una ordenProducto de este bastidor");

                            data.TestInfo.NumFabricacion = nuFabricacion;
                            logger.InfoFormat("Numero de fabricación = {0}", nuFabricacion);

                            if (data.IsReproceso && data.MantenerNumSerie)
                                ReprocessModeController(data, svc, bastidor, nuFabricacion);
                            else
                                NormalModeController(data, svc, bastidor, nuFabricacion);
                        }

                        if (data.modoPlayer == ModoTest.Normal)
                        {
                            RetriesController(data, svc, bastidor);
                            LaserController(data, svc, producto);
                        }
                        if (SearchNumSerieTestFaseBefore)
                        {
                            var numSerie = svc.SearchNumSerieTestFaseBefore(bastidor, data.NumProducto, data.Version);

                            if (string.IsNullOrEmpty(numSerie))
                                throw new Exception("No se ha obtenido un Numero de Serie para un equipo con test de un subconjunto en una fase anterior");
                            else
                                data.TestInfo.NumSerie = numSerie;
                        }
                    }
                    else
                        svc.TestProductSameCkeck(bastidor, data.NumProducto);
                }

                if (!data.TestInfo.NumBastidor.HasValue && Trazabilidad && !data.TestInfo.IsRetry)
                    data.TestInfo.ListComponents = SortIntroComponents == false ? SubsetTrazabilityController(data, svc, bastidor, AddSubconjuntosSinRegistrar) : SubsetTrazabilityControllerUnsorted(data, svc, bastidor);

                if (data.TestInfo.ListComponents != null)
                    foreach (ComponentsEstructureInProduct component in data.TestInfo.ListComponents)
                        context.Variables.AddOrUpdate(component.numproducto.ToString(), component.matricula);
            }
            
            data.TestInfo.NumBastidor = Convert.ToUInt64(bastidor);
            data.TestInfo.ProductoVersion = string.Format("{0}/{1}", data.NumProducto, data.Version);
            data.TestInfo.Producto = data.NumProducto.ToString();;

            context.Variables.AddOrUpdate(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor);

            logger.InfoFormat("Nº de Bastidor: {0}", bastidor);
        }

        public static void AddBastidorToSubset(int productToAdd, int versionToAdd)
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();
            if (data == null)
                throw new Exception("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            using (DezacService svc = new DezacService())
            {
                var components = svc.GetComponentsEstructureInProduct(data.NumProducto, data.Version, false, false);
                var component = components.Where(p => p.numproducto == productToAdd && p.version == versionToAdd).FirstOrDefault();
                var product = svc.GetProducto(productToAdd, versionToAdd);

                if (component != null)
                {
                    int numEstructura = 0;

                    if (data.TestInfo.NumBastidor.HasValue)
                        numEstructura = svc.GetNumEstructura((int)data.TestInfo.NumBastidor.Value, data.IdOperario.ToString());

                    var subconjuntoMatricula = GetIntroBastiorByUser(product.ARTICULO.DESCRIPCION);

                    component.matricula = subconjuntoMatricula;

                    if (data.TestInfo.NumBastidor.HasValue)
                        svc.GrabarEstructuraMatricula(numEstructura, subconjuntoMatricula);

                    data.TestInfo.ListComponents.Add(component);
                }
            }
        }

        private static int GetIntroBastiorByUser(string device)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (string.IsNullOrEmpty(device))
                device = " a testear";

            var isMultiple = context.RunningInstances > 1;

            string msg = context.NumInstance > 1 || isMultiple ?
                string.Format("ENTRADA DEL NUMERO DE BASTIDOR DEL TEST {0}", context.NumInstance) :
                "ENTRADA DEL NUMERO DE BASTIDOR";

            if (isMultiple)
                device += string.Format("\n(TEST {0})", context.NumInstance);

            var bastidor = iShell.ShowDialog<string>(msg, () =>
            {
                return new InputKeyBoard(string.Format("Leer código de barras del bastidor del equipo {0}", device));
            }
           , MessageBoxButtons.OKCancel, (c, d) =>
           {
               if (d == DialogResult.OK)
                   return ((InputKeyBoard)c).TextValue;

               return string.Empty;
           });

            bastidor = bastidor.Trim();

            if (string.IsNullOrEmpty(bastidor))
                throw new Exception("El usuario ha cancelado la entrada del bastidor!");

            var intBastidor = Convert.ToInt32(bastidor);

            if (data.modoPlayer == ModoTest.Normal || data.modoPlayer == ModoTest.TIC)
            {
                if (bastidor.Length < 5)
                    throw new Exception("La longitud del bastidor introducido es mas pequeña de la esperada");
            }
            else
            { 
                if (!IsIDPBastidor(intBastidor))
                    if (bastidor.Length < 5)
                        throw new Exception("La longitud del bastidor introducido es mas pequeña de la esperada");
            }

            if (bastidor.Length > 7)
                throw new Exception("La longitud del bastidor introducido es mas grande de la esperada");

            if (isMultiple)
            {
                var bastidores = context.SharedVariables.Get<List<string>>(ConstantsParameters.SharedVars.BASTIDORES, () => null);
                if (bastidores != null && bastidores.Contains(bastidor))
                    throw new Exception("El nº de bastidor ya se ha informado para otro test en curso");

                if (bastidores == null)
                {
                    bastidores = new List<string>();
                    context.SharedVariables.AddOrUpdate(ConstantsParameters.SharedVars.BASTIDORES, bastidores);
                }

                bastidores.Add(bastidor);
            }

            return intBastidor;
        }

        private static List<ComponentsEstructureInProduct> SubsetTrazabilityController(ITestContext data, DezacService svc, int bastidor, bool AddSubconjuntosSinRegistrar = false)
        {
            var components = svc.GetComponentsEstructureInProduct(data.NumProducto, data.Version, true, true);

            if (components.Count >= 1)
            {
                var numEstructura = svc.GetNumEstructura(bastidor, data.IdOperario.ToString());

                foreach (ComponentsEstructureInProduct component in components)
                {
                    int subconjuntoMatricula = 0;

                    if (data.TestInfo.ListComponents != null && data.TestInfo.ListComponents.Where(p => p.numproducto == component.numproducto).Any())
                        subconjuntoMatricula = data.TestInfo.ListComponents.Where(p => p.numproducto == component.numproducto).FirstOrDefault().matricula;
                    else
                        subconjuntoMatricula = GetIntroBastiorByUser(component.descripcion);

                    var Matricula = svc.GetMatricula(subconjuntoMatricula);
                    if (Matricula == null)
                        throw new Exception(string.Format("Bastidor del equipo {0} no registrado!", component.descripcion));

                    if (!AddSubconjuntosSinRegistrar)
                    {
                        var op = svc.GetOrdenProductoByBastidorAndProducto(subconjuntoMatricula, component.numproducto);
                        if (op == null)
                            throw new Exception(string.Format("El bastidor {0} no esta registrado en ninguna OrdenProducto como este producto {1} !", subconjuntoMatricula, component.descripcion));

                        var ordenSubconjunto = svc.GetOrden(op.NUMORDEN);
                        if (ordenSubconjunto == null)
                            throw new Exception(string.Format("El bastidor {0} no esta registrado en ninguna Orden como este producto {1} !", subconjuntoMatricula, component.descripcion));

                        if (data.modoPlayer == ModoTest.Normal || data.modoPlayer == ModoTest.TIC)
                            if (component.numproducto != ordenSubconjunto.NUMPRODUCTO || component.version != ordenSubconjunto.VERSION)
                                throw new Exception(string.Format("Equipo {0} esta registrado como otro producto {1} o con otra versión {2}!", component.descripcion, op.ORDEN.NUMPRODUCTO, op.ORDEN.VERSION));

                        var product = svc.GetProducto(ordenSubconjunto.NUMPRODUCTO, ordenSubconjunto.VERSION);

                        var productoFases = svc.GetProductoFases(product);

                        foreach (var fase in productoFases)
                            if(fase.FASETEST == "S")
                                svc.GetResultadoNumTestFaseIsOK(subconjuntoMatricula, fase.IDFASE);
                    }

                    svc.GrabarEstructuraMatricula(numEstructura, subconjuntoMatricula);

                    component.matricula = subconjuntoMatricula;
                }
            }
            return components;
        }

        private static List<ComponentsEstructureInProduct> SubsetTrazabilityControllerUnsorted(ITestContext data, DezacService svc, int bastidor)
        {
            var estruct = svc.GetComponentsEstructureInProductByTrazabilidad(data.NumProducto, data.Version);

            var components = new List<ComponentsEstructureInProduct>();
            components.AddRange(estruct);

            foreach (ComponentsEstructureInProduct comp in estruct)
                if (comp.cantidad > 1)
                {
                    for (byte i = 1; i <= comp.cantidad - 1; i++)
                    {
                        var comp2 = new ComponentsEstructureInProduct();
                        comp2.matricula = 0;
                        comp2.cantidad = 1;
                        comp2.descripcion = string.Format("{0}_SUB{1}", comp.descripcion, i + 1);
                        comp2.numproducto = comp.numproducto;
                        comp2.version = comp.version;
                        components.Add(comp2);
                    }
                    comp.cantidad = 1;
                    comp.descripcion = string.Format("{0}_SUB{1}", comp.descripcion, 1);
                }

            if (components.Count >= 1)
            {
                var numEstructura = svc.GetNumEstructura(bastidor, data.IdOperario.ToString());

                int numIntro = 0;

                do
                {
                    int subconjuntoMatricula = 0;

                    //if (data.TestInfo.ListComponents != null && data.TestInfo.ListComponents.Where(p => p.numproducto == components.numproducto).Any())
                    //    subconjuntoMatricula = data.TestInfo.ListComponents.Where(p => p.numproducto == component.numproducto).FirstOrDefault().matricula;
                    //else
                        subconjuntoMatricula = GetIntroBastiorByUser(string.Format("SUBCONJUNTO {0} / {1}", numIntro + 1, components.Count + 1));

                    var Matricula = svc.GetMatricula(subconjuntoMatricula);
                    if (Matricula == null)
                        throw new Exception("Bastidor del subconjunto no registrado!");

                    var op = svc.GetOrdenProductoByBastidor(subconjuntoMatricula);
                    if (op == null)
                        throw new Exception("El bastidor del subconjunto no esta registrado en ninguna orden!");

                    var ordenSubconjunto = svc.GetOrden(op.NUMORDEN);
                    if (ordenSubconjunto == null)
                        throw new Exception("El bastidor del subconjunto no esta registrado en ninguna orden");

                    if (components.Where((p) => p.matricula == subconjuntoMatricula).Count() == 0)
                    {
                        bool trobat = false;

                        foreach (ComponentsEstructureInProduct component in components)
                            if (component.numproducto == ordenSubconjunto.NUMPRODUCTO && component.matricula == 0)
                            {
                                svc.GetResultadoNumTestIsOK(subconjuntoMatricula);

                                component.matricula = subconjuntoMatricula;

                                svc.GrabarEstructuraMatricula(numEstructura, subconjuntoMatricula);

                                trobat = true;
                                break;
                            }

                        if (!trobat)
                        {
                            if (data.modoPlayer == ModoTest.PostVenta)
                            {
                                svc.GetResultadoNumTestIsOK(subconjuntoMatricula);

                                var lenght = ordenSubconjunto.DESCRIPCION.Length;

                                var filterDescription = ordenSubconjunto.DESCRIPCION.Substring(0, lenght >= 22 ? 22 : lenght);

                                var subconjunto = components.Where((p) => p.descripcion.Contains(filterDescription)).FirstOrDefault();

                                if (subconjunto == null)
                                    TestException.Create().PROCESO.TRAZABILIDAD.ESTRUCTURA_COMPONENTES(string.Format("NO se encuentra subconjunto {0} en la estructura", subconjunto)).Throw();

                                subconjunto.matricula = subconjuntoMatricula;

                                svc.GrabarEstructuraMatricula(numEstructura, subconjuntoMatricula);
                            }
                            else
                                throw new Exception("Bastidor del subconjunto esta registrado como otro producto y no existe en la estructura de este equipo");
                        }
                    }

                    numIntro = components.Where((p) => { return p.matricula != 0; }).Count();

                } while (components.Where((p) => { return p.matricula == 0; }).Count() != 0);
            }
            return components;
        }

        private static void SelectIdActuacionReparacionToShowTable(ITestContext data)
        {
            var shell = SequenceContext.Current.Services.Get<IShell>();

            using (ReprocesosService db = new ReprocesosService())
            {
                var items = db.GetActuaciones();
                var actuacion = shell.ShowTableDialog<ACTUACIONREPFAB>("Actuación", items);
                if (actuacion != null)
                    data.TestInfo.IdActuacionReparacion = actuacion.IDACTUACION;
            }
        }

        private static void LaserController(ITestContext data, DezacService svc, PRODUCTO product)
        {
            var productoFase = svc.GetProductoFases(product);
            var faseLasear = productoFase.Where(p => p.FASE.CODIGOGRUPO == "LAS");

            if (faseLasear.Count() == 0)
                return;

            int laseredPieces = 0;            
            if (data.NumOrden.HasValue)
                laseredPieces = svc.GetLaseredPieces(data.NumOrden.Value);

            var indicators = svc.GetTestIndicators(data.NumOrden.Value, data.IdFase);
            var assembliesPieces = indicators.NumOK + indicators.NumError;

            //if (laseredPieces < (assembliesPieces - (assembliesPieces * 0.1)))
            //    MessageBox.Show("SE QUIEREN FABRICAR MAS PRODUCTOS DE LA ORDEN QUE PIEZAS HAY LASEADAS", "LASER CONTROLLER", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);                             
        }

        private static void RetriesController(ITestContext data, DezacService svc, int bastidor)
        {
            var value = data.Configuracion.GetString("PERMITIR_REPETICIONES", "SI", ParamUnidad.SinUnidad).Trim().ToUpper();

            if (value  == "NO" || value == "1" || value == "2")
            {
                var statusDevice = svc.GetStatusRepeat(bastidor, data.IdFase);

                var canRepit = false;

                if (value == "1")
                {
                    canRepit = statusDevice == TestRepeatStatus.REPEAT_1;

                    if (statusDevice == TestRepeatStatus.NOT_PASS_1 || (statusDevice == TestRepeatStatus.NOT_PASS_2) || (statusDevice == TestRepeatStatus.REPEAT_2))
                        throw new Exception(string.Format("Equipo con bastidor {0} ya se ha pasado una vez sin ninguna reparación!", bastidor));
                }
                else
                {
                    canRepit = statusDevice == TestRepeatStatus.REPEAT_2;

                    if (statusDevice == TestRepeatStatus.NOT_PASS_2)
                        throw new Exception(string.Format("Equipo con bastidor {0} ya se ha pasado dos veces sin ninguna reparación!", bastidor));
                }

                if (canRepit)
                {
                    SelectIdActuacionReparacionToShowTable(data);

                    if (!string.IsNullOrEmpty(data.TestInfo.IdActuacionReparacion))
                    {
                        var orden = svc.GetOrden(data.NumOrden.Value);
                        if (orden == null)
                            throw new Exception(string.Format("Error orden {0} no se ha encontrado", data.NumOrden.Value));

                        var numTestFaseAnterior = svc.GetLastNumTestFase(bastidor, data.NumOrden.Value, data.IdFase);
                        if (!numTestFaseAnterior.HasValue)
                        {
                            if (orden.NIVELCONTROLTEST != "0")
                                return;

                            throw new Exception("No se ha encontrado ninguna fase de test OK!");
                        }

                        using (ReprocesosService db = new ReprocesosService())
                        {
                            db.Insert(data.NumOrden.Value, data.NumProducto, data.Version, numTestFaseAnterior.Value, bastidor, data.IdOperario, data.TestInfo.IdActuacionReparacion);
                        }
                    }
                }
            }
        }

        private static void NormalModeController(ITestContext data, DezacService svc, int bastidor, int numFabricacion)
        {
            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var cts = new CancellationTokenSource();

            var op = svc.GetOrdenProductoByFabricacion(numFabricacion, bastidor);
            if (op != null)
            {
                var orden = svc.GetOrden(op.NUMORDEN);
                if (orden == null)
                    throw new Exception(string.Format("Este bastidor esta registrado en la orden {0} pero esta orden no se ha encontrado", op.NUMORDEN));

                if (data.Nivelcontroltest == 0)
                {
                    if (data.NumProducto != orden.NUMPRODUCTO || data.Version != orden.VERSION)
                        if (!svc.ExisteMatriculaSubconjunto(data.NumOrden.Value, bastidor))
                            throw new Exception(string.Format("Error equipo {0} / {1}  esta registrado como otro producto {2} /  {3}", data.NumProducto, data.Version, orden.NUMPRODUCTO, orden.VERSION));
                }
                else if (data.Nivelcontroltest == 1)
                    if (data.NumProducto != orden.NUMPRODUCTO)
                        if (!svc.ExisteMatriculaSubconjunto(data.NumOrden.Value, bastidor))
                            throw new Exception(string.Format("Error equipo {0}  esta registrado como otro producto {1}", data.NumProducto, orden.NUMPRODUCTO));

                if (op.NROSERIE != null)
                {
                    var testfase = svc.GetLastTestFase(bastidor, "0", data.NumOrden.Value);
                    if (testfase == null)
                        throw new Exception(string.Format("Error, no se ha encontrado el testfase anterior de ésta orden donde se asignó el número de serie {0} al equipo {1}", op.NROSERIE, bastidor));

                    if (data.NumOrden == orden.NUMORDEN && SequenceContext.Current.RunMode == RunMode.Release && testfase.IDFASE == data.IdFase)
                    {
                        var test = svc.GetTest(numFabricacion, bastidor);
                        if (test.NROSERIEORIGINAL == null)
                            test.NROSERIEORIGINAL = op.NROSERIE;

                        var msg = string.Format("! ATENCIÓN, ESTE EQUIPO YA ESTA REGISTRADO EN LA ORDEN {0} CON NUMERO DE SERIE {1} EN LA CAJA {2}  ¿DESEA VOLVER A TESTEARLO?", op.ORDEN.NUMORDEN, test.NROSERIEORIGINAL, op.NUMCAJA);

                        var resp = iShell.ShowDialog("EQUIPO REGISTRADO", new Label() { Text = msg, Width = 400, Height = 100, Padding = new Padding(10) }, MessageBoxButtons.OKCancel);
                        if (resp == DialogResult.OK)
                        {
                            if (op.NROSERIE != test.NROSERIE)
                                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("NUMERO DE SERIE DIFERENTE ENTRE ORDENPRODUCTO Y T_TEST").Throw();

                            data.TestInfo.NumSerie = op.NROSERIE;
                            data.NumCajaActual = op.NUMCAJA.Value;
                        }
                        else
                        {
                            cts.Cancel();
                            throw new Exception("Test cancelado al por el usuario");
                        }
                    }
                }
            }
        }

        private static void ReprocessModeController(ITestContext data, DezacService svc, int bastidor, int numFabricacion)
        {
            logger.InfoFormat("Reproceso y Mantener numero de serie");

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var op = svc.GetOrdenProductoByFabricacion(numFabricacion, bastidor);
            if (op != null)
            {
                var orden = svc.GetOrden(op.NUMORDEN);
                if (orden == null)
                    throw new Exception(string.Format("Este bastidor esta registrado en la orden {0} pero esta orden no se ha encontrado", op.NUMORDEN));

                if (data.Nivelcontroltest == 0)
                {
                    if (data.NumProducto != orden.NUMPRODUCTO || data.Version != orden.VERSION)
                        if (!svc.ExisteMatriculaSubconjunto(data.NumOrden.Value, bastidor))
                            throw new Exception(string.Format("Error equipo {0} / {1}  esta registrado como otro producto {2} /  {3}", data.NumProducto, data.Version, orden.NUMPRODUCTO, orden.VERSION));
                }
                else if (data.Nivelcontroltest == 1)
                    if (data.NumProducto != orden.NUMPRODUCTO)
                        if (!svc.ExisteMatriculaSubconjunto(data.NumOrden.Value, bastidor))
                            throw new Exception(string.Format("Error equipo {0}  esta registrado como otro producto {1}", data.NumProducto, orden.NUMPRODUCTO));

                if (SequenceContext.Current.RunMode == RunMode.Release)
                {              
                    var t_test = svc.GetTestByBeforeTest(bastidor);
                    if (t_test == null || t_test.NROSERIE == null)
                        throw new Exception(string.Format("Error, test recuperado del equipo con bastidor {0} no tiene número de serie", bastidor));

                    data.TestInfo.NumSerie = t_test.NROSERIE;
                    data.TestInfo.NumSerieInterno = t_test.NROSERIEORIGINAL;
                }
            }
        }

        private static bool IsIDPBastidor(int bastidor)
        {
            switch (bastidor)
            {
                case 985:
                case 379:
                case 392:
                case 393:
                case 230:
                case 394:
                case 666:
                case 413:
                    return true;
            }

            return false;
        }

    }
}
