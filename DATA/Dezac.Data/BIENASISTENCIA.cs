namespace Dezac.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.BIENASISTENCIA")]
    public partial class BIENASISTENCIA
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMASISTENCIA { get; set; }

        public int? NUMBIEN { get; set; }

        [StringLength(10)]
        public string IDGFH { get; set; }

        public DateTime? FECHAALTA { get; set; }

        public int? NUMINCIDENCIA { get; set; }

        [Required]
        [StringLength(1)]
        public string ABIERTA_SN { get; set; }

        public DateTime? FECHACIERRE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [StringLength(300)]
        public string NOTAS { get; set; }

        public int? NUMPROGRAMACION { get; set; }

        [Required]
        [StringLength(1)]
        public string TIPOASISTENCIA { get; set; }

        [Required]
        [StringLength(1)]
        public string REPARACION { get; set; }

        [Required]
        [StringLength(1)]
        public string CALIBRACION { get; set; }

        [Required]
        [StringLength(1)]
        public string PRESERIE { get; set; }

        [Required]
        [StringLength(1)]
        public string AJUSTE { get; set; }

        [Required]
        [StringLength(1)]
        public string MANTENIMIENTO { get; set; }

        public virtual BIEN BIEN { get; set; }
    }
}
