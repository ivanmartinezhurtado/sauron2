﻿using Comunications.Utility;
using System;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class CVMA1XXX : CVMB1XX
    {
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TEST_CONECTOR)]
        public new struct Conector
        {
            public Int16 B13_B14;
            public Int16 B15_B16;
            public Int16 B20_B21;
            public Int16 B23_B24;
            public Int16 A17_A18;
            public Int16 A21_A22;

            public bool IsValid { get { return B13_B14 == 0 && B20_B21 == 0 && B23_B24 == 0 && A17_A18 == 0 && A21_A22 == 0; } }
        }
    }
}
