﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Core.Model
{
    public class AOIClassficationInfo
    {
        public List<string> SVMFile { get; set; }
        public string matchingFileName { get; set; }
        public string searchFileName { get; set; }
        public string ROIFileName { get; set; }
        public string boardRegionFileName { get; set; }
        public int numRows { get; set; }
        public int numColumns { get; set; }
        public int rowDistance { get; set; }
        public int columnDistance { get; set; }
        public bool barcodeRead { get; set; }
        public string orderPath { get; set; }
        public string weldingSVM { get; set; }
        public AOIClassficationInfo()
        {
            SVMFile = new List<string>();
        }
    }
}
