namespace Dezac.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.FASE")]
    public partial class FASE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FASE()
        {
            DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
            ORDENPROCESOFASE = new HashSet<ORDENPROCESOFASE>();
            PRODUCTOFASE = new HashSet<PRODUCTOFASE>();
            T_FAMILIAPARAM = new HashSet<T_FAMILIAPARAM>();
            T_GRUPOPARAMETRIZACION = new HashSet<T_GRUPOPARAMETRIZACION>();
            T_IRISERRORPRODUCTOFASE = new HashSet<T_IRISERRORPRODUCTOFASE>();
            T_IRISERRORFAMILIAFASE = new HashSet<T_IRISERRORFAMILIAFASE>();
            T_TESTFASE = new HashSet<T_TESTFASE>();
            T_TESTFASES = new HashSet<T_TESTFASES>();
            T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
            T_TESTSEQUENCEFILE = new HashSet<T_TESTSEQUENCEFILE>();
        }

        [Key]
        [StringLength(10)]
        public string IDFASE { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [Required]
        [StringLength(1)]
        public string AVISOMARPTEVALID { get; set; }

        [Required]
        [StringLength(1)]
        public string FASETEST { get; set; }

        [Required]
        [StringLength(1)]
        public string DOCREQECP { get; set; }

        [StringLength(3)]
        public string CODIGOGRUPO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENPROCESOFASE> ORDENPROCESOFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTOFASE> PRODUCTOFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_FAMILIAPARAM> T_FAMILIAPARAM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_GRUPOPARAMETRIZACION> T_GRUPOPARAMETRIZACION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_IRISERRORPRODUCTOFASE> T_IRISERRORPRODUCTOFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_IRISERRORFAMILIAFASE> T_IRISERRORFAMILIAFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTFASES> T_TESTFASES { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTSEQUENCEFILE> T_TESTSEQUENCEFILE { get; set; }
    }
}
