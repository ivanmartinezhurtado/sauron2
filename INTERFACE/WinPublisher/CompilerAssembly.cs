﻿using AssemblyResolve;
using System;
using System.IO;
using System.Linq;

namespace WinPublisher
{
    public static class CompilerAssembly
    {
        public static bool CompilerSolution()
        {        
            var user = Environment.GetEnvironmentVariable("USERPROFILE");
            var logFile = user + "\\Documents\\GitLab\\SAURON\\msbuild.log";
            //var msbuildExe2015 = "\"C:\\Program Files (x86)\\MSBuild\\14.0\\Bin\\MsBuild.exe\"";
            var msbuildExe2017 = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\MSBuild\\15.0\\Bin\\MsBuild.exe\"";
            var msbuildExe2019 = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MsBuild.exe\"";

            var args = "\"" + user + "\\Documents\\GitLab\\SAURON\\SAURON.sln\" /t:\"build\" /p:Configuration=\"Debug\" /p:Platform=\"x86\" /flp:LogFile=" + logFile + "";
            
            var msbuildExe = File.Exists("C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MsBuild.exe") ? msbuildExe2019 : msbuildExe2017;

            var p = new System.Diagnostics.Process
            {
                StartInfo = new System.Diagnostics.ProcessStartInfo
                {
                    //UseShellExecute = false,
                    //RedirectStandardOutput = true,
                    FileName = msbuildExe,
                    Arguments = args
                }
            };

            p.Start();
            p.WaitForExit();

            var result = File.ReadAllLines(logFile, System.Text.Encoding.UTF8).ToList();
            var ok = result.Any(l => l == "Compilaci�n correcta.");
            var errors = result.Where(l => l.EndsWith("0 Errores"));

            return ok && errors.Any();
        }

        public static bool CompilerProject(FileAssemblyItem project)
        {
            var user = Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents";
            var logFile = user + "\\Documents\\GitLab\\SAURON\\msbuild.log";
            var msbuildExe = "C:\\Program Files (x86)\\MSBuild\\14.0\\Bin\\MsBuild.exe";
            var args = user + "\\{0}\\{1}.csproj /t:\"Clean;Rebuild\" /p:Configuration=\"Debug\" /p:Platform=\"x86\""; // /flp:LogFile=" + logFile + "";
            var argsProject = string.Format(args, project.PathProjectAssembly, project.Name);

            var p = new System.Diagnostics.Process
            {
                StartInfo = new System.Diagnostics.ProcessStartInfo
                {
                    //UseShellExecute = false,
                    //RedirectStandardOutput = true,
                    FileName = msbuildExe,
                    Arguments = argsProject
                }
            };

            p.Start();
            p.WaitForExit();

            var result = File.ReadAllLines(logFile, System.Text.Encoding.UTF8).ToList();
            var ok = result.Any(l => l == "Compilaci�n correcta.");
            var errors = result.Where(l => l.EndsWith("0 Errores")).LastOrDefault();

            return ok && errors.Contains("0 Errores");
        }
    }
}
