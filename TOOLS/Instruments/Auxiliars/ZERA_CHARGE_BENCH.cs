﻿using Comunications.Utility;
using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using log4net;
using System;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;

// V.3.1

namespace Instruments.Auxiliars
{
    public class ZERA_CHARGE_BENCH : IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(ZERA_CHARGE_BENCH));

        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private SerialPort sp;

        private byte _port = 1;

        //public enum Burden
        //{
        //    IEC50 = 0,
        //    IEC60 = 1,
        //    ANSI = 2
        //}

        public enum Id
        {
            UP = 0,
            IP = 1,
            US = 2,
            IS = 3
        }


        public struct VaStruct
        {
            public float va;
            public float cos;
        }

        public struct BurdenStruct
        {
            public string burdenName;
            public float range;
            public float va;
            public float cos;
        }

        public struct BurdenStructOn
        {
            public BurdenStruct burden;
            public int on;
        }
        public struct calibStruct
        {
            public int on;
            public float Rcoarse;
            public float Rfine;
            public float Lcoarse;
            public float Lfine;
            public float CompensationValue;
            public float SecAngle;
        }
        public struct measuredValuesStruct
        {
            public float MeasuredUvalue;
            public float MeasuredIvalue;
            public float reserved;
            public float MeasuredIangle;
            public float MeasuredCos;
            public float MeasuredVAvalue;
        }
        #region CONSTRUCTOR

        public ZERA_CHARGE_BENCH()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinRx = "\r", CaracterFinTx = "\r" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp, "ACK"), logger); // \r
            transport.Retries = 1;
            transport.ReadTimeout = 10000;

        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + _port;
            }
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = 1000;
            sp.ReadTimeout = 3000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.Two;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
        }

        protected ILog Logger { get; set; }


        protected IProtocol protocol { get; set; }
        protected MessageTransport transport { get; set; }

        protected void LogMethod([CallerMemberName] string name = "")
        {
            logger.InfoFormat(name);
        }

        #endregion

        public string GetInstrumentVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(InstrumentsVersionAttribute), true).FirstOrDefault() as InstrumentsVersionAttribute;
            if (attr != null) { return string.Format(enUS, "{0:0.00}", attr.Version); }
            else { return "SIN VERSIONAR"; }
        }

        #region READ
        public string[] ReadVersion()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "AV");
            //message.Validator = (m) => { return m.Text == "ABACK"; };
            var response = transport.SendMessage<StringMessage>(message);

            // Ejemplo para el formato: "EBV06.15X7AVACK"

            string[] version = response.Text.Split('X'); // "EBV06.15", "7AVACK"

            version[0] = version[0].Substring(3, 5); // 06.15
            version[1] = version[1].Split('A')[0]; // 7

            return version;
        }

        public string[] ReadBurden()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "AB");
            //message.Validator = (m) => { return m.Text == "ABACK"; };
            var response = transport.SendMessage<StringMessage>(message);
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            return response.Text.Split('\r');

            /*
            string [] s = response.Text.Split('\r');
            BurdenStruct b;
            b.burden = s[0];
            b.va = float.Parse(s[1], CultureInfo.InvariantCulture.NumberFormat);
            b.cosß = float.Parse(s[2], CultureInfo.InvariantCulture.NumberFormat);
            return b;
            */

            // Retorna varios strings.
        }

        public float[] ReadRanges(string burden)
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "AR" + burden);
            //message.Validator = (m) => { return m.Text == "ARACK"; };
            var response = transport.SendMessage<StringMessage>(message);
            response.Text = response.Text.Substring(0, response.Text.Length - 3);

            string[] s = response.Text.Split('\r');
            float[] f = new float[s.Length];

            for (int i = 0; i < s.Length; i++)
            {
                f[i] = float.Parse(s[i], CultureInfo.InvariantCulture.NumberFormat);
            }

            return f;

            // Retorna varios strings.
        }

        public VaStruct[] ReadVa(string burden)
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "AN" + burden);
            //message.Validator = (m) => { return m.Text == "ANACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            string[] s = response.Text.Split('\r');

            // VaStruct[] v = null;
            VaStruct[] v = new VaStruct[s.Length];

            VaStruct vAux;
            string[] sAux;

            for (int i = 0; i < s.Length; i++)
            {
                sAux = s[i].Split(';');

                vAux.va = float.Parse(sAux[0], CultureInfo.InvariantCulture.NumberFormat);
                vAux.cos = float.Parse(sAux[1], CultureInfo.InvariantCulture.NumberFormat);

                v[i] = vAux;
            }
            return v;
        }


        //    //return Extensions.AsignedTramaToStructure<T>(ASCIIEncoding.ASCII.GetString(data).Trim());

        //    //return Comunications.Utility.Extensions.AsignedTramaToStructure<int>(response.Text, '\r');



        public BurdenStructOn ReadStatus()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "ST");
            //message.Validator = (m) => { return m.Text == "STACK"; };

            var response = transport.SendMessage<StringMessage>(message);
            response.Text = response.Text.Substring(0, response.Text.Length - 3); // Retorna varios strings.
            string[] s = response.Text.Split('\r'); // s = "B:IEC50", "R:230", "N:5.00;0.8", "ON:1", "ST"

            BurdenStructOn bs;

            // BURDEN
            string[] s1 = s[0].Split(':');
            bs.burden.burdenName = s1[1];

            // RANGE
            s1 = s[1].Split(':');
            bs.burden.range = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // VA-VALUE
            s1 = s[2].Split(':');
            s1 = s1[1].Split(';');

            bs.burden.va = float.Parse(s1[0], CultureInfo.InvariantCulture.NumberFormat);
            bs.burden.cos = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // ON
            s1 = s[3].Split(':');
            bs.on = int.Parse(s1[1]);

            return bs;
        }

        public calibStruct ReadCalibration(string burden, float range, float va, float cosß)
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "GA" + burden + ";" + range.ToString() + ";" + va.ToString() + ";" + cosß.ToString());
            //message.Validator = (m) => { return m.Text == "GAACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            string[] s = response.Text.Split('\r');


            // VA-Value calibrated = "1;< Rcoarse >;< Rfine >;< Lcoarse >;< Lfine >;< Compensation value>;<Sec.angle><CR>"
            // VA-Value not calibrated = 0

            calibStruct cs = new calibStruct();

            if (s.Length > 1) // VA-Value calibrated
            {
                cs.on = int.Parse(s[0], CultureInfo.InvariantCulture.NumberFormat);
                cs.Rcoarse = float.Parse(s[1], CultureInfo.InvariantCulture.NumberFormat);
                cs.Rfine = float.Parse(s[2], CultureInfo.InvariantCulture.NumberFormat);
                cs.Lcoarse = float.Parse(s[3], CultureInfo.InvariantCulture.NumberFormat);
                cs.Lfine = float.Parse(s[4], CultureInfo.InvariantCulture.NumberFormat);
                cs.CompensationValue = float.Parse(s[5], CultureInfo.InvariantCulture.NumberFormat);
                cs.SecAngle = float.Parse(s[6], CultureInfo.InvariantCulture.NumberFormat);
            }
            else
            {
                //if (s[0] != "0") throw new NotSupportedException();
                cs.on = 0;
            }

            return cs;
        }

        public measuredValuesStruct ReadMeasureValues()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "ME");
            //message.Validator = (m) => { return m.Text == "MEACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            string[] s = response.Text.Split('\r');

            // s = '0;XXX', '3;XXX', '6;0', '9;XXX', '12;XXX', '21;XXX', 'ME'

            measuredValuesStruct mvs = new measuredValuesStruct();

            // MeasuredUvalue
            string[] s1 = s[0].Split(';');
            mvs.MeasuredUvalue = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // MeasuredIvalue
            s1 = s[1].Split(';');
            mvs.MeasuredIvalue = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // reserved
            s1 = s[2].Split(';');
            mvs.reserved = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // MeasuredIangle
            s1 = s[3].Split(';');
            mvs.MeasuredIangle = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // MeasuredCos
            s1 = s[4].Split(';');
            mvs.MeasuredCos = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // MeasuredVAvalue
            s1 = s[5].Split(';');
            mvs.MeasuredVAvalue = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            return mvs;
        }

        public string ReadUMeasuringCalibration(float range, string id = "P")
        {
            LogMethod();

            if (id != "P" && id != "S")  // Primary \ Secondary
            {
                throw new Exception("El valor debe ser 'P' o 'S'.");
            }

            var message = StringMessage.Create(enUS, "AU" + id + ";" + range.ToString());

            //message.Validator = (m) => { return m.Text == "AUACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            return response.Text.Split('\r')[0];

            // Calibration Value es un float? return float.parse(response.Text.Split('\r')[0]);
        }

        // id puede ser el primario o secundario.
        public string ReadIMeasuringCalibration(float range, string id = "P")
        {
            LogMethod();


            if (id != "P" && id != "S")  // Primary \ Secondary
            {
                throw new Exception("El valor debe ser 'P' o 'S'.");
            }

            var message = StringMessage.Create(enUS, "AI" + id + ";" + range.ToString());

            //message.Validator = (m) => { return m.Text == "AIACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            return response.Text.Split('\r')[0];

            // Calibration Value es un float? return float.parse(response.Text.Split('\r')[0]);
        }

        public float[] ReadIPrimaryRanges()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CI");
            //message.Validator = (m) => { return m.Text == "CIACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            string[] s = response.Text.Split('\r');

            float[] f = new float[s.Length];

            for (int i = 0; i < s.Length; i++)
            {
                f[i] = float.Parse(s[i], CultureInfo.InvariantCulture.NumberFormat);
            }

            return f;
        }

        public float[] ReadUPrimaryRanges()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "VI");
            //message.Validator = (m) => { return m.Text == "VIACK"; };
            var response = transport.SendMessage<StringMessage>(message); // Retorna varios strings.
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            string[] s = response.Text.Split('\r');

            float[] f = new float[s.Length];

            for (int i = 0; i < s.Length; i++)
            {
                f[i] = float.Parse(s[i], CultureInfo.InvariantCulture.NumberFormat);
            }

            return f;
        }
        public BurdenStructOn ReadCalibrationStatus()
        {
            // Està incompleta però no s'utilitza
            LogMethod();
            var message = StringMessage.Create(enUS, "CS");
            //message.Validator = (m) => { return m.Text == "STACK"; };
            var response = transport.SendMessage<StringMessage>(message);
            response.Text = response.Text.Substring(0, response.Text.Length - 3);
            string[] s = response.Text.Split('\r');
            // s = ’S: 1’(1 calibration is running, 0 calibration is finished), ‘B: IEC50’, ‘R: 230‘, ‘N: 5.00; 0.8‘, ‘CS‘

            BurdenStructOn bs;

            // ON
            string[] s1 = s[0].Split(':');
            bs.on = int.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // BURDEN
            s1 = s[1].Split(':');
            bs.burden.burdenName = s1[1];

            //RANGE
            s1 = s[2].Split(':');
          
            Console.WriteLine("S1[1] = ", s1[1]);
            bs.burden.range = float.Parse(s1[1], CultureInfo.InvariantCulture.NumberFormat);

            // VA-VALUE
            s1 = s[3].Split(':'); // ‘N: 5.00; 0.8‘ -> 'N', '5.00; 0.8'
            bs.burden.va = float.Parse(s1[1].Split(';')[0], CultureInfo.InvariantCulture.NumberFormat); // '5.00; 0.8' -> '5,00', '0,8'
            bs.burden.cos = float.Parse(s1[1].Split(';')[1], CultureInfo.InvariantCulture.NumberFormat); // '5.00; 0.8' -> '5,00', '0,8'

            return bs;
        }

        #endregion

        #region WRITE
        public void AddVa(string burden, float va, float cosß)
        {
            var message = StringMessage.Create(enUS, "IN" + burden + ";" + va.ToString().Replace(",", ".") + ";" + cosß.ToString().Replace(",", "."));
            message.Validator = (m) => { return m.Text == "IN"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void CancelCalibration()
        {
            var message = StringMessage.Create(enUS, "CC");
            message.Validator = (m) => { return m.Text == "CC"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void DeleteVa(string burden, float va, float cosß)
        {

            var message = StringMessage.Create(enUS, "DN" + burden + ";" + va.ToString().Replace(",", ".") + ";" + cosß.ToString().Replace(",", "."));
            message.Validator = (m) => { return m.Text == "DN"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SetVa(float va, float cosß)
        {

            var message = StringMessage.Create(enUS, "SN" + va.ToString().Replace(",", ".") + ";" + cosß.ToString().Replace(",", "."));
            message.Validator = (m) => { return m.Text == "SN"; };
            transport.SendMessage<StringMessage>(message);
         
        }


        public void DefaultCalibration()
        {
            var message = StringMessage.Create(enUS, "RM");
            message.Validator = (m) => { return m.Text == "RM"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void ProgramBurden(string burden)
        {
            var message = StringMessage.Create(enUS, "PR" + burden);
            message.Validator = (m) => { return m.Text == "PR"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void RestoreDefaultSettings()
        {
            var message = StringMessage.Create(enUS, "RD");
            message.Validator = (m) => { return m.Text == "RD"; };
            transport.SendMessage<StringMessage>(message);
        }
        public void SelectBurden(string burden)
        {
            var message = StringMessage.Create(enUS, "SB" + burden);
            message.Validator = (m) => { return m.Text == "SB"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SelectUPrimaryRange(int range)
        {
            var message = StringMessage.Create(enUS, "SU" + range.ToString());
            message.Validator = (m) => { return m.Text == "SU"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SelectIPrimaryRange(int range)
        {
            var message = StringMessage.Create(enUS, "SI" + range.ToString());
            message.Validator = (m) => { return m.Text == "SI"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SelectSecondaryRange(int range)
        {
            var message = StringMessage.Create(enUS, "SS" + range.ToString());
            message.Validator = (m) => { return m.Text == "SS"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SetRange(int range)
        {
            var message = StringMessage.Create(enUS, "SR" + range.ToString());
            message.Validator = (m) => { return m.Text == "SR"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SetMeasureTime(int time)
        {
            if (time < 1 || time > 99)
            {
                throw new System.Exception();
                Console.WriteLine("The time value must be between 1 and 99.");
            }

            var message = StringMessage.Create(enUS, "MT" + time.ToString());
            message.Validator = (m) => { return m.Text == "MT"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SetCalibrationValues(string burden, float range, float va, float cosß, float rCoarse, float rFine, float lCoarse, float lFine, float compensationValue, float secAngle)
        {

            var message = StringMessage.Create(enUS, "SA" + burden + ";" + range.ToString() + ";" + va.ToString() + ";" + cosß.ToString()
                + ";" + rCoarse.ToString() + ";" + rFine.ToString() + ";" + lCoarse.ToString() + ";" + lFine.ToString() + ";" + compensationValue.ToString() + ";" + secAngle.ToString());
            message.Validator = (m) => { return m.Text == "SA"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SetMeasuringCalibrationMode(int calib = 0)
        {

            //1 – Measuring Calibration Mode / 0 - Measuring Calibration not running

            if (calib != 0 && calib != 1)
            {
                throw new Exception("El valor debe ser '0 - Measuring Calibration Not running' o '1 – Measuring Calibration Mode'");
            }

            var message = StringMessage.Create(enUS, "MC" + calib.ToString());
            message.Validator = (m) => { return m.Text == "MC"; };
            transport.SendMessage<StringMessage>(message);
        }

        // No tengo muy claro el formato. ( CR(XXX);(YYY);(ZZ1;ZZ2)<CR> )
        public void StartCalibration(string burden, float range, float va, float cosß)
        {
            var message = StringMessage.Create(enUS, "CR" + burden + ";" + range.ToString() + ";" + va.ToString() + ";" + cosß.ToString());
            message.Validator = (m) => { return m.Text == "CR"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void StartMeasureCalibration(int start = 0)
        {

            if (start != 0 && start != 1)
            {
                throw new Exception("El valor debe ser '0 - Stop' o '1 – Start'");
            }

            var message = StringMessage.Create(enUS, "MR" + start.ToString());

            message.Validator = (m) => { return m.Text == "MR"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SwitchBurden(int OnOff = 0)
        {


            if (OnOff != 0 && OnOff != 1)
            {
                throw new Exception("El valor debe ser '0 - OFF' o '1 – ON'");
            }
            var message = StringMessage.Create(enUS, "ON" + OnOff.ToString());

            message.Validator = (m) => { return m.Text == "ON"; };
            transport.SendMessage<StringMessage>(message);
        }

        public void SetValuesMeasuringCalibration(Id id, float range, float value)
        {
            var message = StringMessage.Create(enUS, "MK" + id.ToString() + ";" + range.ToString() + ";" + value.ToString());

            message.Validator = (m) => { return m.Text == "MK"; };
            transport.SendMessage<StringMessage>(message);
        }

        #endregion

        #region METODOS
        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }


        #endregion
    }
}
