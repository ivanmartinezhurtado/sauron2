﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Runtime.InteropServices;

namespace Dezac.Device.Reactive
{
    [DeviceVersion(1.00)]
    public class COMPUTERMAX : DeviceBase
    {
        private byte hardwareTrafo = 0x00;
        private byte hardwareReles = 2;
        private byte hardwareVersion = 0;
        private byte paso = 0x03;
        private byte numReles = 4;
        private string vectorHardwareBoolean = "";
        
        public bool flagReles = false;
        public bool flagAlimentacion = false;

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public COMPUTERMAX()
        {
        }

        public COMPUTERMAX(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port = 10, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public string ModeloHardware
        {
            set
            {
                switch (value.ToUpper().Trim())
                {
                    case "MAX6":
                        hardwareReles = 0;
                        paso = 0x05;
                        numReles = 6;
                        break;
                    case "MAX8":
                        hardwareReles = 3;
                        paso = 0x07;
                        numReles = 8;
                        break;
                    case "MAX12":
                        hardwareReles = 1;
                        paso = 0x0B;
                        numReles = 12;
                        break;
                    case "MAX4":
                        hardwareReles = 2;
                        paso = 0x03;
                        numReles = 4;
                        break;
                    default:
                        flagReles = true;
                        break;
                };
            }
        }

        public int VoltageSupplyHardware
        {
            set
            {
                switch (value)
                {
                    case 110:
                        hardwareTrafo = 0;
                        break;
                    case 230:
                        hardwareTrafo = 1;
                        break;
                    case 400:
                        hardwareTrafo = 2;
                        break;
                    case 480:
                        hardwareTrafo = 3;
                        break;
                    default:
                        flagAlimentacion = true;
                        break;
                };
            }
        }

        public string VersionHardware
        {
            set
            {
                if (value.Contains("09"))
                    hardwareVersion = 0;
                else if (value.Contains("07"))
                    hardwareVersion = 1;
                else
                    hardwareVersion = 0;
            }
        }

        public byte SetupInfoHardware 
        { 
            get 
            { 
                var vector = new bool[8];
                vector[7] = ((int)hardwareTrafo & 0x01) == 0x01;
                vector[6] = ((int)hardwareTrafo & 0x02) == 0x02;
                vector[5] = ((int)hardwareTrafo & 0x04) == 0x04;
                vector[4] = ((int)hardwareReles & 0x01) == 0x01;
                vector[3] = ((int)hardwareReles & 0x02) == 0x02;
                vector[2] = false;
                vector[1] = false;
                vector[0] = ((int)hardwareVersion & 0x01) == 0x01;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector.ArrayBoolToByte();
            }
        }

        public string SetupInfoHardwareBoolean
        {
            get
            {
                return vectorHardwareBoolean;
            }
        }

        public byte SetupInfoPaso { get { return paso; } }

        public byte SetupInfoNumReles { get { return numReles; } }

        #region Register Writing
        public void WriteFlagTest()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, true);
        }

        public void WriteIdentificationParameters(IdentificationParameters serialNumber)
        {
            LogMethod();
            Modbus.Write<IdentificationParameters>(serialNumber);
        }

        public void WriteSoftwareVersion(string softwareVersion)
        {
            LogMethod();
            Modbus.WriteString((ushort)Registers.SOFTWARE_VERSION, softwareVersion);
        }

        public void WriteOutputs(bool state, params Relays[] outputs)
        {
            LogMethod();
            bool[] relesInfo = new bool[16];

            foreach (Relays output in outputs)      
                relesInfo[(int)output] = state;
      
            Modbus.WriteMultipleCoil((ushort)Registers.DIGITAL_OUPUTS, relesInfo);
        }

        public void WriteAllOutputs(bool state)
        {
            WriteOutputs(state, Relays.OUTPUT_1, Relays.OUTPUT_2, Relays.OUTPUT_3, Relays.OUTPUT_4, Relays.OUTPUT_5, Relays.OUTPUT_6, Relays.OUTPUT_7, Relays.OUTPUT_8, Relays.OUTPUT_9, Relays.OUTPUT_10, Relays.OUTPUT_11, Relays.OUTPUT_12);
        }

        public void WriteLeds(bool state, Leds led)
        {
            LogMethod();
            bool[] ledsInfo = new bool[8];
            ledsInfo[(int)led] = state;
            Modbus.WriteMultipleCoil((ushort)Registers.LEDS, ledsInfo);
        }

        public void WriteSetupInfoVariables(SetupInfo setupVariables)
        {
            LogMethod();
            Modbus.Write<SetupInfo>(setupVariables);
        }

        public void WriteSetupLimitVariables(SetupLimitsInfo limitVariables)
        {
            LogMethod();
            Modbus.Write<SetupLimitsInfo>(limitVariables);
        }

        public void WriteSetupLimitFastVariables(SetupLimitsInfoFast limitVariables)
        {
            LogMethod();
            Modbus.Write<SetupLimitsInfoFast>(limitVariables);
        }

        public void WriteMaximums(Maximums maximums)
        {
            LogMethod();
            Modbus.Write<Maximums>(maximums);
        }
        #endregion

        #region RegisterReading
        public IdentificationParameters ReadIdentificationParameters()
        {
            LogMethod();
            return Modbus.Read<IdentificationParameters>();
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            var ver= Modbus.ReadBytes((ushort)Registers.SOFTWARE_VERSION,1);
            return ver[0].ToString("X2"); ;
        }

        public SetupInfo ReadSetupInfo()
        {
            LogMethod();
            return Modbus.Read<SetupInfo>();
        }

        public SetupLimitsInfo ReadSetupLimits()
        {
            LogMethod();
            return Modbus.Read<SetupLimitsInfo>();
        }

        public SetupLimitsInfoFast ReadSetupLimitsFast()
        {
            LogMethod();
            return Modbus.Read<SetupLimitsInfoFast>();
        }

        public Maximums ReadMaximums()
        {
            LogMethod();
            return Modbus.Read<Maximums>();
        }

        public Keyboard ReadKeyboard()
        {
            LogMethod();
            var key = Modbus.ReadMultipleCoil((ushort)Registers.KEYBOARD, 8);
            
            Keyboard outKey = new Keyboard();

            switch (key[0])
            {
                case ((byte)Keyboard.KEY_UP):
                    outKey = Keyboard.KEY_UP;
                    break;
                case ((byte)Keyboard.KEY_DOWN):
                    outKey = Keyboard.KEY_DOWN;
                    break;
                case ((byte)Keyboard.KEY_SETUP):
                    outKey = Keyboard.KEY_SETUP;
                    break;
                default:
                    outKey = Keyboard.NO_KEYS;
                    break;
            }

            return outKey;
        }

        public MeasureVariables ReadMeasureVariables()
        {
            LogMethod();
            return Modbus.Read<MeasureVariables>();
        }

        public MeasureAdjustVariables ReadMeasureAdjustVariables()
        {
            MeasureVariables measureReadings = ReadMeasureVariables();

            MeasureAdjustVariables output = new MeasureAdjustVariables()
            {
                Measures = new MeasureVariables()
                {
                    CosenoFi = measureReadings.CosenoFi,
                    Nmos = measureReadings.Nmos,
                    OffsetCorriente = measureReadings.OffsetCorriente,
                    OffsetTension = measureReadings.OffsetTension,
                    SumTI = measureReadings.SumTI,
                    SumTKVar = measureReadings.SumTKVar,
                    SumTKW = measureReadings.SumTKW,
                    SumTV = measureReadings.SumTV,
                    Hertz = measureReadings.Hertz,
                },
                Voltage = ReadVoltageVariables(),
                Current = ReadCurrentVariables(),
                CurrentTHD = ReadTHDCurrent(),
            };
         

            return output;
        }

        public int ReadCurrentVariables()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.VAR_CURRENT);
        }

        public int ReadVoltageVariables()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.VAR_VOLTAGE);
        }

        public int ReadTHDCurrent()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.VAR_THD_CURRENT);
        }
        #endregion

        public Tuple<bool, SetupLimitsInfo> CalculateAdjustFactors(int delFirst, int initCount, int samples, int interval, double adjustVoltage, double adjustCurrent, double voltageOffset, Func<double[]> measureVariables, Func<SetupLimitsInfo, bool> adjustValidation, Func<MeasureAdjustVariables, bool> sampleValidation = null)
        {
            SetupLimitsInfo factors = ReadSetupLimits();

            WriteFlagTest();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();   
                },
                (listValues) =>
                {
                    factors.calibracionMedidaI = Convert.ToUInt16((adjustCurrent * 100000000 / listValues.Average(10)) * factors.calibracionMedidaI);

                    factors.calibracionMedidaV = Convert.ToUInt16((adjustVoltage - voltageOffset) * 100000 / listValues.Average(9) * factors.calibracionMedidaV);

                    factors.factorCalibracionCorriente = Convert.ToUInt16(Math.Sqrt(listValues.Average(1) / listValues.Average(4)) * (5 / adjustCurrent));

                    var desfase = Math.Round(((Math.Atan2(listValues.Average(2), listValues.Average(3)) * 180 / Math.PI) - 45) * 39.5);
                    factors.anguloDesfase = (byte)(desfase > 127 ? 127 : desfase < -127 ? -127 : desfase);

                    return adjustValidation(factors);
                },
                (samplesToValidate) =>
                {
                    MeasureAdjustVariables validateSamples = new MeasureAdjustVariables()
                    {
                        Measures = new MeasureVariables()
                        {
                            SumTV = (uint)samplesToValidate[0],
                            SumTI = (uint)samplesToValidate[1],
                            SumTKVar = (uint)samplesToValidate[2],
                            SumTKW = (uint)samplesToValidate[3],
                            Nmos = (uint)samplesToValidate[4],
                            OffsetTension = (short)samplesToValidate[5],
                            OffsetCorriente = (short)samplesToValidate[6],
                            CosenoFi = (short)samplesToValidate[7],
                            Hertz = (short)samplesToValidate[8],
                        },
                        Voltage = (int)samplesToValidate[9],
                        Current = (int)samplesToValidate[10],
                        CurrentTHD = (int)samplesToValidate[11]
                    };
                    return sampleValidation(validateSamples);
                });
            return Tuple.Create(adjustResult.Item1, factors);
        }

        public Tuple<bool, SetupLimitsInfoFast> CalculateAdjustFactorsFast(int delFirst, int initCount, int samples, int interval, double adjustVoltage, double adjustCurrent, double voltageOffset, Func<double[]> measureVariables, Func<SetupLimitsInfoFast, bool> adjustValidation, Func<MeasureAdjustVariables, bool> sampleValidation = null)
        {
            SetupLimitsInfoFast factors = ReadSetupLimitsFast();

            WriteFlagTest();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    factors.calibracionMedidaI = Convert.ToUInt16((adjustCurrent * 100000000 / listValues.Average(10)) * factors.calibracionMedidaI);

                    factors.calibracionMedidaV = Convert.ToUInt16((adjustVoltage - voltageOffset) * 100000 / listValues.Average(9) * factors.calibracionMedidaV);

                    factors.factorCalibracionCorriente = Convert.ToUInt16(Math.Sqrt(listValues.Average(1) / 300) * (5 / adjustCurrent));

                    factors.anguloDesfase = (ushort)Math.Round(((Math.Atan2(listValues.Average(2), listValues.Average(3)) * 180 / Math.PI) - 45) * 82.3);

                    return adjustValidation(factors);
                },
                (samplesToValidate) =>
                {
                    MeasureAdjustVariables validateSamples = new MeasureAdjustVariables()
                    {
                        Measures = new MeasureVariables()
                        {
                            SumTV = (uint)samplesToValidate[0],
                            SumTI = (uint)samplesToValidate[1],
                            SumTKVar = (uint)samplesToValidate[2],
                            SumTKW = (uint)samplesToValidate[3],
                            Nmos = (uint)samplesToValidate[4],
                            OffsetTension = (short)samplesToValidate[5],
                            OffsetCorriente = (short)samplesToValidate[6],
                            CosenoFi = (short)samplesToValidate[7],
                            Hertz = (short)samplesToValidate[8],
                        },
                        Voltage = (int)samplesToValidate[9],
                        Current = (int)samplesToValidate[10],
                        CurrentTHD = (int)samplesToValidate[11]
                    };
                    return sampleValidation(validateSamples);
                });
            return Tuple.Create(adjustResult.Item1, factors);
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SERIAL_NUMBER)]
        public struct IdentificationParameters
        {
            public uint serialNumber;
            public uint frameNumber;
            public ushort errorCode;

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MEASUREMENTS)]
        public struct MeasureVariables
        {
            public uint SumTV;
            public uint SumTI;
            public uint SumTKVar;
            public uint SumTKW;
            public uint Nmos;
            public short OffsetTension;
            public short OffsetCorriente;
            public short CosenoFi;
            public short Hertz;
        }

        public struct MeasureAdjustVariables
        {
            public MeasureVariables Measures;
            public int Voltage;
            public int Current;
            public int CurrentTHD;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VAR_SETUP_INFO)]
        public struct SetupInfo
        {
            public byte tipoCoseno;
            public byte cosenoFiObjetivo;
            public byte CK;
            public byte programaManiobra;
            public byte cuadrantes;
            public ushort tiempoRetraso;
            public byte paso;
            public byte fase;
            public ushort primarioCorriente;
            public byte escalaPrimarioCorriente;
            public byte hardware;
            private byte _numeroReles;
            public ushort Transformador
            {
                get { return (ushort)((hardware & 0x07) > 0x03 ? 480 : (hardware & 0x07) > 0x02 ? 400 : (hardware & 0x07) > 0x01 ? 230 : 110 ); }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VAR_LIMITS_SETUP)]
        public struct SetupLimitsInfo
        {
            public byte cosenoInductivo;
            public byte cosenoCapacitivo;
            public byte CKMaximo;
            public byte CKMinimo;
            public ushort tiempoRetrasoMaximo;
            public ushort tiempoRetrasoMinimo;
            public byte pasoMaximo;
            public byte intensidadMinima;
            public byte factorReconexion;
            public ushort factorCalibracionCorriente;
            public byte anguloDesfase;
            public ushort calibracionMedidaI;
            public ushort calibracionMedidaV;
            public uint valorMaximoI;
            public ushort valorMaximoV;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VAR_LIMITS_SETUP)]
        public struct SetupLimitsInfoFast
        {
            public byte cosenoInductivo;
            public byte cosenoCapacitivo;
            public byte CKMaximo;
            public byte CKMinimo;
            public ushort tiempoRetrasoMaximo;
            public ushort tiempoRetrasoMinimo;
            public byte pasoMaximo;
            public byte intensidadMinima;
            public byte factorReconexion;
            public ushort factorCalibracionCorriente;
            public ushort anguloDesfase;
            public ushort calibracionMedidaI;
            public ushort calibracionMedidaV;
            public uint valorMaximoI;
            public ushort valorMaximoV;
            public byte dummybyte;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAXIMUMS)]
        public struct Maximums
        {
            public Maximums(uint corrienteMaxima, ushort tensionMaxima)
            {
                valorMaximoI = corrienteMaxima;
                valorMaximoV = tensionMaxima;
            }
            public uint valorMaximoI;
            public ushort valorMaximoV;
        }

        public enum Relays
        {
            OUTPUT_1=8,
            OUTPUT_2=9,
            OUTPUT_3=10,
            OUTPUT_4=11,
            OUTPUT_5=12,
            OUTPUT_6=13,
            OUTPUT_7=14,
            OUTPUT_8=15,
            OUTPUT_9=0,
            OUTPUT_10=1,
            OUTPUT_11=2,
            OUTPUT_12=3,
        }

        public enum Leds
        {
            RUN=3,
            X10_IP=4
        }

        public enum State
        {
            OFF,
            ON,
        }

        public enum Keyboard
        {
            NO_KEYS = 0xB0,
            KEY_DOWN = 0x30,
            KEY_UP = 0x90,
            KEY_SETUP = 0xA0
        }

        public enum Registers
        {
            SERIAL_NUMBER = 1300,   // 5 REGISTERS
            SOFTWARE_VERSION = 62,  // 1 REGISTERS
            FLAG_TEST = 11000,      // 1 REGISTERS
            DIGITAL_OUPUTS = 0,     // 16 REGISTERS
            VAR_SETUP_INFO = 1100,  // 7 REGISTERS
            VAR_LIMITS_SETUP = 1200, // 12 REGISTERS
            MAXIMUMS = 1400,        // 3 REGISTERS
            LEDS = 24,              // 8 REGISTERS
            KEYBOARD = 16,          // 8 REGISTERS
            MEASUREMENTS = 20,      // 14 REGISTERS
            VAR_CURRENT = 54,       // 2 REGISTERS
            VAR_VOLTAGE = 56,       // 2 REGISTERS
            VAR_THD_CURRENT = 58,   // 2 REGISTERS
        }
    }
}