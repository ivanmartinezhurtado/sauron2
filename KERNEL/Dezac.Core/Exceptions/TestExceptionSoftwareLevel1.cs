﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionSoftwareLevel1
    {
        public string[] Category { get; set; }

        public string[] SubCategory { get; set; }

        public string ClassName { get; set; }

        public SauronException TORRE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.TORRE, parameter).Exception();
        }

        public SauronException INSTRUMENTOS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.INSTRUMENTOS, parameter).Exception();
        }

        public SauronException CAMARA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.CAMARA, parameter).Exception();
        }

        public SauronException IMPRESORAS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.IMPRESORAS, parameter).Exception();
        }

        public SauronException INSTANCIA_CLASE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.INSTANCIA_CLASE, parameter).Exception();
        }

        public SauronException CARGA_SERVICIO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.CARGA_SERVICIO, parameter).Exception();
        }

        public SauronException PARALELISMO_CANCELADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.PARALELISMO_CANCELADO, parameter).Exception();
        }

        public SauronException SAMPLER_WITH_CANCEL_MAL_IMPLEMENTADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.SAMPLER_WITH_CANCEL_MAL_IMPLEMENTADO, parameter).Exception();
        }

        public SauronException CONFIGURACION_INCORRECTA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.CONFIGURACION_INCORRECTA, parameter).Exception();
        }

        public SauronException VARIABLE_NO_ENCONTRADA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.VARIABLE_NO_ENCONTRADA, parameter).Exception();
        }

        public SauronException FICHERO_NO_ENCONTRADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.FICHERO_NO_ENCONTRADO, parameter).Exception();
        }

        public SauronException VARIABLE_ES_NULA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.VARIABLE_ES_NULA, parameter).Exception();
        }

        public SauronException NOMBRE_DE_LA_VARIABLE_NULO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.VARIABLE_ES_NULA, parameter).Exception();
        }

        public SauronException CREACION_O_MODIFICACION_DE_VARIABLE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.CREACION_O_MODIFICACION_DE_VARIABLE, parameter).Exception();
        }

        public SauronException NO_INSTALADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.NO_INSTALADO, parameter).Exception();
        }

        public SauronException CONSULTA_PGI(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.CONSULTA_PGI, parameter).Exception();
        }

        public SauronException ERROR_GRABACION(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.ERROR_GRABACION, parameter).Exception();
        }


        public SauronException ACTION_MAL_CONFIGURADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.ACTION_MAL_CONFIGURADO, parameter).Exception();
        }
        public SauronException FALTA_TEST_BASE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.FALTA_TEST_BASE, parameter).Exception();
        }
        public SauronException VERSION_INCORRECTA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.VERSION_INCORRECTA, parameter).Exception();
        }
        public SauronException WIFI(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.WIFI, parameter).Exception();
        }
        public SauronException TRESG(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.TRESG, parameter).Exception();
        }
        public SauronException DETECCION(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.SoftwareLevel1.DETECCION, parameter).Exception();
        }
    }
}
