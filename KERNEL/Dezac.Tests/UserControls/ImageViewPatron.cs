﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.UserControls
{
    public partial class ImageViewPatron : UserControl
    {
        private Image testImage;
        private Image patternImage;

        public Image PatternImage
        {
            get { return patternImage; }
            set { picPatron.Image = value; patternImage = value; }
        }

        public Image TestImage
        {
            get { return testImage; }
            set { cameraImage.Image = value; testImage = value; }
        }

        public ImageViewPatron(Image testImage, Image patternImage)
        {
            InitializeComponent();
            TestImage = testImage;
            PatternImage = patternImage;
        }

        protected override void Dispose(bool disposing)
        {
            cameraImage.Image = null;
            picPatron.Image = null;

            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }
    }
}
