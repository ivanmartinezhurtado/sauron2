﻿using System;

namespace Comunications.Utility
{
    public class FirmwareUpdateProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Constructor for the firmware update progress event args.  This is an event raised during at intervals
        /// during the firmware update process to allow the client to indicate the progress or failure.
        /// </summary>
        /// <param name="Percentage">0-100% progress value</param>
        /// <param name="Message">Text message</param>
        /// <param name="Failed">Set true if the process has failed, otherwise false</param>
        public FirmwareUpdateProgressEventArgs(float Percentage, String Message, bool Failed)
        {
            this.Percentage = Percentage;
            this.Message = Message;
            this.Failed = Failed;
        }

        public float Percentage;
        public String Message;
        public bool Failed;
    }
}
