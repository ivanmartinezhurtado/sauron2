﻿using Dezac.Tests;
using TaskRunner;

namespace WinTaskRunner.Services
{
    public class RunnerService
    {
        public SequenceRunner Runner { get; private set; }

        public RunnerService()
        {
            Runner = new SequenceRunner();
            Runner.ScriptService.ScriptHost = new TestScriptHost();
        }

        //public void Play(SequenceDocument doc)
        //{
        //    Runner.Play(doc.Model);
        //}
    }
}
