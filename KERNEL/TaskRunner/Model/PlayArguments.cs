﻿using System;
using System.Threading;
using TaskRunner.Enumerators;

namespace TaskRunner.Model
{
    public class PlayArguments
    {
        internal Func<PlayArguments, SequenceContext> FuncContext { get; set; }
        public Guid GroupGuid { get; internal set; }
        public RunMode RunMode { get; internal set; }
        public CancellationTokenSource CancellationTokenSource { get; internal set; }
        public int RunTestNumber { get; internal set; }
        public int NumInstance { get; internal set; }
        public int TotalNumInstances { get; internal set; }
        public SharedVariablesList SharedVariables { get; internal set; }
    }
}
