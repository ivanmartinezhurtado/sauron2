namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.RF_ACTUACION")]
    public partial class RF_ACTUACION
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMACTUACION { get; set; }

        public int NUMREPROCESO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDACTUACION { get; set; }

        [Required]
        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        public int? NUMANOMALIA { get; set; }

        public DateTime FECHA { get; set; }

        [StringLength(500)]
        public string NOTAS { get; set; }

        public virtual ACTUACIONREPFAB ACTUACIONREPFAB { get; set; }

        public virtual REPROCESOFAB REPROCESOFAB { get; set; }

        public virtual RF_ANOMALIA RF_ANOMALIA { get; set; }
    }
}
