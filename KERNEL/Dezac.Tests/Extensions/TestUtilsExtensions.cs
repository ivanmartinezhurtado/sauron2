﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dezac.Tests.Extensions
{
    public static class TestUtilsExtensions
    {
        public static void InParellel(this TestBase test, params Action[] actions)
        {
            try
            {
                var tasks = new List<Task>();
                actions.ToList().ForEach(p => tasks.Add(Task.Factory.StartNew(() =>p())));
                Task.WaitAll(tasks.ToArray());

            } catch(AggregateException aex)
            {
                var ex = aex.Flatten();
                throw ex;
            }
        }
    }
}
