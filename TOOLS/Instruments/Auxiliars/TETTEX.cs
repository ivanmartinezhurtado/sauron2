﻿using Comunications.Utility;
using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using log4net;
using System;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using Dezac.Core.Utility;
using Dezac.Core.Exceptions;

// V.1

namespace Instruments.Auxiliars
{
    public class TETTEX : IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(TETTEX));

        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private SerialPort sp;

        private byte _port = 1;

        #region CONSTRUCTOR
        public TETTEX()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinRx = "", CaracterFinTx = "\n" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp, "\n"), logger); 
            transport.Retries = 1;
            transport.ReadTimeout = 10000;

        }
        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + _port;
            }
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = 1000;
            sp.ReadTimeout = 3000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.Two;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
        }
        
        protected ILog Logger { get; set; }


        protected IProtocol protocol { get; set; }
        protected MessageTransport transport { get; set; }

        protected void LogMethod([CallerMemberName] string name = "")
        {
            logger.InfoFormat(name);
        }

        #endregion

        #region READ


        private float InternalReadFloat(string s, string error)
        {
            var message = StringMessage.Create(enUS, s);
            transport.SendMessage<StringMessage>(message);
            var response = transport.SendMessage<StringMessage>(message);

            if (float.TryParse(response.Text, out _))
                return float.Parse(response.Text, CultureInfo.InvariantCulture.NumberFormat);

            logger.WarnFormat("{0} {1}", error, response.Text);
            TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(error).Throw();
            return 0;

        }

        public float ReadRatioError()
        {
            LogMethod();
            return InternalReadFloat("?RP", "No se ha podido leer el ratio de error");
        }

        public float ReadPhaseError()
        {
            LogMethod();
            return InternalReadFloat("?PM", "No se ha podido leer la phase de error");
        }

        public float ReadCurrentPercentage()
        {
            LogMethod();
            return InternalReadFloat("?EP", "No se ha podido leer el porcentaje de corriente de error");
        }

        public string ReadStatus()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "?ST");
            var response = transport.SendMessage<StringMessage>(message);
            message.Validator = (m) => { return m.Text == "Done"; };
            return response.Text;

        }
        public string ReadVersion()
        {
            //Respuesta    TYPE 2767    EPROM 18663 Version M Date 27.10.95 
            // or          TYPE 2767 CT EPROM 18663 Version N Date 28.4.00

            LogMethod();
            var message = StringMessage.Create(enUS, "?VS");
            var response = transport.SendMessage<StringMessage>(message);

            return response.Text;

        }
        #endregion

        #region WRITE

        public void ConfigCurrentPrimaryValue()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CDP");
            var response = transport.SendMessage<StringMessage>(message);
           
            message.Validator = (m) => { return m.Text == "Done"; };

        }
        public void ConfigCurrentPrimaryPercentage()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CEP");
            var response = transport.SendMessage<StringMessage>(message);
           
            message.Validator = (m) => { return m.Text == "Done"; };

        }

        public void ConfigCurrentPrimaryMinutes()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CPM");
            var response = transport.SendMessage<StringMessage>(message);
            
            message.Validator = (m) => { return m.Text == "Done"; };

        }
        public void ConfigErrorPercentage()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CRP");
            var response = transport.SendMessage<StringMessage>(message);
         
            message.Validator = (m) => { return m.Text == "Done"; };

        }

        public void ConfigCurrentTestMode()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CCT");
            var response = transport.SendMessage<StringMessage>(message);
          
            message.Validator = (m) => { return m.Text == "Done"; };

        }

        public void ConfigInternalSyncrhonization(string freq)
        {
            LogMethod();

            if (freq != "50" && freq != "60")  // 50 Hz o 60 Hz
            {
                throw new Exception("El valor debe ser '50' o '60'.");
            }
            var message = StringMessage.Create(enUS, "C5I");
            var response = transport.SendMessage<StringMessage>(message);
            
            message.Validator = (m) => { return m.Text == "Done"; };

        }

        public void ConfigMeasureMode()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CMM");
            var response = transport.SendMessage<StringMessage>(message);
        
            message.Validator = (m) => { return m.Text == "Done"; };

        }
        public void ConfigAutoCalibration()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CAF");
            var response = transport.SendMessage<StringMessage>(message);
            
            message.Validator = (m) => { return m.Text == "Done"; };

        }

        public void Reset()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "DCL");
            var response = transport.SendMessage<StringMessage>(message);
           
            message.Validator = (m) => { return m.Text == "Done"; };

        }
        public void Calibration()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CAL");
            var response = transport.SendMessage<StringMessage>(message);
            message.Validator = (m) => { return m.Text == "Done"; };

        }

        public void CalibrationNOAuto()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "CAF");
            var response = transport.SendMessage<StringMessage>(message);
            
            message.Validator = (m) => { return m.Text == "Done"; };
        }
        //Bloquea el teclado para que sea modo control remoto
        public void RemoteControl()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "REM");
            var response = transport.SendMessage<StringMessage>(message);
           
            message.Validator = (m) => { return m.Text == "Done"; };
        }
        public void TransformerParametres()
        {
            LogMethod();
            var message = StringMessage.Create(enUS, "SXN");
            var response = transport.SendMessage<StringMessage>(message);
           
            message.Validator = (m) => { return m.Text == "Done"; };
        }

        #endregion

        #region METODOS
        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
        #endregion
    }
}
