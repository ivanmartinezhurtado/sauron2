﻿using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.AOI;

namespace Dezac.Tests.Actions.AOI
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AOIInitFrameGrabberDescription))]
    [Browsable(false)]
    public class AOIInitFrameGrabber : ActionBase
    {
        public override void Execute()
        {
            HalconFrameGrabber.InitFrameGrabber();
        }
    }

    public class AOIInitFrameGrabberDescription : ActionDescription
    {
        public override string Name { get { return "AOIInitFrameGrabber"; } }
        public override string Category { get { return "DezacTest/Vision/AOI"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
