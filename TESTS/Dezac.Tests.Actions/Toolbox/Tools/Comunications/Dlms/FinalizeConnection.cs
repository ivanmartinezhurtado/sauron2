﻿using Dezac.DLMS;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(FinalizeConnectionDescription))]
    public class FinalizeConnection : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("DlmsDevice")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "DlmsDevice";
            var dlms = GetVariable(name) as DLMSDeviceSerialPort;
            dlms.Disconnect();
            Context.ResultList.Add("FinalizeConnection = OK");
        }
    }

    public class FinalizeConnectionDescription : ActionDescription
    {
        public override string Name { get { return "FinalizeConnectionDlms"; } }
        public override string Description { get { return "Finaliza la session en protocolo DLMS"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
    }
}
