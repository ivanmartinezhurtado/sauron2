namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ORDENCAJA
    {
        public int NUMORDEN { get; set; }
        public int NUMCAJA { get; set; }
        public int UDSCAJA { get; set; }
        public System.DateTime FECHA { get; set; }
    
        public virtual ORDEN ORDEN { get; set; }
    }
}
