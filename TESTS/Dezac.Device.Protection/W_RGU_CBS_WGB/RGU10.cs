﻿using Dezac.Core.Enumerate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;


namespace Dezac.Device.Protection
{
    [DeviceVersion(1.04)]
    public class RGU10 : DeviceBase
    {
        // Instanciacion del equipo
        public RGU10()
        {
        }

        public RGU10(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port = 3, int bauRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, bauRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;
            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public ushort ValorFinTabla {get; set;}

        public int TimeWaitCom { get; set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Operaciones escritura

        public void WriteFlagTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MODE, (ushort)Modes.TEST);
        }

        public void Reset()
        {
            LogMethod();
            Modbus.WithTimeOut((m) => { m.Write((ushort)Registers.MODE, (ushort)Modes.RESET); });
        }

        public void WriteNormalMode()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.MODE, (ushort)Modes.NORMAL);
        }

        public void WriteDisplay(DisplayOperations displayOperation)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.DISPLAY_OUTPUTS, (ushort)displayOperation);
        }

        public void WriteDisplayNumber(ushort number, params DisplayDigits[] digits)
        {
            LogMethod();

            ushort digitsToWrite = 0;
            for (int i = 0; i < digits.Length; i++)
                digitsToWrite += (ushort)digits[i];
            Modbus.Write((ushort)Registers.DISPLAY_OUTPUTS, (ushort)(digitsToWrite + number * 0x100));
        }

        public void WriteRelays(Relays output)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.DISPLAY_OUTPUTS, (ushort)output);
        }

        public void WriteLeds(Leds led)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.DISPLAY_OUTPUTS, (ushort)led);
        }

        public void ClearCurrentTimeTables()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CURRENT_TIME_TABLES_CLEAR, (ushort)0xFFFF);
            Thread.Sleep(400);
        }

        public void ClearReconnectionTables()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CLEAR_RECONNECTION_TABLE, (ushort)0xFFFF);
            Thread.Sleep(1500);
        }

        //**************************************************************

        public void WriteScaleTable(ScaleTableConfiguration currentScales, TableKind typeTable)
        {
            LogMethod();

            byte position = 0;
            int positionValue = 0;

            foreach (var elemnt in currentScales.ListElemnts)
            {
                positionValue = position << 11;
                var value = positionValue + ((elemnt.X100 ? 1 : 0) << 10) + elemnt.ElementValue;

                if (typeTable == TableKind.CURRENT)
                    WriteCurrentTableElement((ushort)value);
                else
                    WriteTimeTableElement((ushort)value);

                position++;
            }

            positionValue = position << 11;

            if (typeTable == TableKind.CURRENT)
                WriteCurrentTableElement((ushort)(positionValue + ValorFinTabla)); //Grabamos el ultimo elemento con un 0 en la ultima posicion
            else
                WriteTimeTableElement((ushort)(positionValue + ValorFinTabla)); //Grabamos el ultimo elemento con un 0 en la ultima posicion

            Thread.Sleep(TimeWaitCom);
        }

        public void WriteCurrentTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CURRENT_TABLE, value);
            Thread.Sleep(TimeWaitCom);
        }

        public void WriteTimeTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TIME_TABLE, (ushort)value);
            Thread.Sleep(TimeWaitCom);
        }

        //***************************************************************

        public void WriteSRDTable(RGU10.ReconnectionTableConfiguration srdcTable)
        {
            LogMethod();

            byte position = 0;
            WriteSRDTableElement(position, srdcTable.TableSize);
            position++;
            WriteSRDTableElement(position, srdcTable.TotalAmountOfReconnections);
            position++;
            WriteSRDTableElement(position, srdcTable.ResetTime);
            position++;

            foreach (var item in srdcTable.listStages)
            {
                WriteSRDTableElement(position, item.Value.AmountOfReconnections);
                position++;
                WriteSRDTableElement(position, item.Value.ReconnectionTime);
                position++;
            }
        }

        public void WriteSRDTableElement(byte position, int value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRD_RECONNECTION_TABLE, (ushort)(position * 0x0800 + value));
            Thread.Sleep(400);
        }

        public void WriteSRMTable(RGU10.ReconnectionTableConfiguration srmcTable)
        {
            LogMethod();

            byte position = 0;
            WriteSRMTableElement(position, srmcTable.TableSize);
            position++;
            WriteSRMTableElement(position, srmcTable.TotalAmountOfReconnections);
            position++;
            WriteSRMTableElement(position, srmcTable.ResetTime);
            position++;

            foreach (var item in srmcTable.listStages)
            {
                WriteSRMTableElement(position, item.Value.AmountOfReconnections);
                position++;
                WriteSRMTableElement(position, item.Value.ReconnectionTime);
                position++;
            }
        }

        public void WriteSRMTableElement(byte position, int value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRM_RECONNECTION_TABLE, (ushort)(position * 0x0800 + value));
            Thread.Sleep(400);
        }

        public void WriteSRDTableEnd()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRD_RECONNECTION_TABLE, 0x1000);
            Thread.Sleep(400);
        }

        public void WriteSRMTableEnd()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRM_RECONNECTION_TABLE, 0x1000);
            Thread.Sleep(400);
        }

        public void WriteTablesVersion(string tablesVersion)
        {
            LogMethod();

            Modbus.WriteString((ushort)Registers.TABLES_VERSION, tablesVersion);
        }

        public void WriteCuttingType(CuttingTypes cuttingType)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CUTTING_TYPE, (ushort)cuttingType);
        }

        public void WriteErrorCode(int errorCode)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.ERROR_CODE, errorCode);
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.HIGH_LEVEL_SERIAL_NUMBER, (int)serialNumber);
        }

        public void WriteFrameNumber(int frameNumber)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.HIGH_LEVEL_FRAME_NUMBER, frameNumber);
        }

        public void WriteStartCalibration(ScaleCalibration scale)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CALIBRATION, (ushort)scale);
        }

        public void WriteDCScaleConstant(DCCalibrationRegisters dcCalibration, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)dcCalibration, value);
        }

        public void WriteACScaleConstant(ACCalibrationRegisters acCalibration, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)acCalibration, value);
        }

        public void WriteScaleConstant(CalibrationRegisters calibrationRegister, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)calibrationRegister, value);
        }

        public void WriteAllowParametersWriting()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.PARAMETERS_WRITING_1, (ushort)Writing.PARAMETERS);
        }

        public void WriteSetupWriting()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.PARAMETERS_WRITING_1, (ushort)Writing.SETUP);
        }

        public void WriteTriggerCurrentSelection(byte scale)
        {
            LogMethod();

            if (scale >= 32)
              throw new Exception("La escala de corriente seleccionada no es válida");

            Modbus.Write((ushort)Registers.CURRENT_SCALE_SELECTION, (ushort)scale);
        }

        public void WriteTriggerTimeSelection(byte scale)
        {
            LogMethod();

            if (scale >= 32)
                  throw new Exception("La escala de tiempo seleccioanda no es válida");

             Modbus.Write((ushort)Registers.TIME_SCALE_SELECTION, (ushort)scale);  
        }

        public void WriteSRDSelection(byte scale)
        {
            LogMethod();

            if (scale >= 32)
                throw new Exception("La escala SRD seleccionada no es válida");

             Modbus.Write((ushort)Registers.SRD_SCALE_SELECTION, (ushort)scale);
        }

        public void WriteSRMSelection(byte scale)
        {
            LogMethod();

            if (scale >= 32)
                throw new Exception("La escala SRM seleccionada no es válida");

            Modbus.Write((ushort)Registers.SRM_SCALE_SELECTION, (ushort)scale);
        }

        public void WriteToroidalTest()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.EXTERNAL_TRIGGER_RESET, (ushort)0xFFFF);
        }

        public void WritePeriphericNumber(ushort perifericNumber)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.PERIFERIC_NUMBER, perifericNumber);
        }

        public void ResetTrigger()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.EXTERNAL_TRIGGER_RESET, (ushort)0x0000);
        }

        public void SaveSequence(Sequence sequence, ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)sequence, value);
        }

        public void WriteInitialMessage(string message)
        {
            LogMethod();

            var screenMessage = message.PadLeft(4, '0');

            Modbus.WriteString((ushort)Registers.HIGH_LEVEL_INITIAL_MESSAGE, screenMessage);
        }

        public void WriteInitialMessageCC(string message)
        {
            LogMethod();

            var screenMessage = message.PadLeft(4, '0');

            Modbus.WriteString((ushort)Registers.CC_SIEMENS_INITIAL_MESSAGE, screenMessage);
        }

        public void WriteRegisterLocking(ushort registersToLock)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.LOCK_SELECTION, registersToLock);
        }

        public void WriteScaleLimit(Limits applyLimits, byte currentLimit, byte timeLimit)
        {
            LogMethod();

            ushort value = (ushort)((applyLimits == Limits.LOW ? 0x0000 : 0xF000) + 0x0010 * timeLimit + currentLimit);

            Modbus.Write((ushort)Registers.SCALE_LIMIT_SELECTION, value);
        }

        public void WriteReleSecurity(Logic relay1Logic, Logic relay2Logic)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.RELAY_SECURITY_SELECTION, (ushort)relay1Logic);
            Modbus.Write((ushort)Registers.RELAY_SECURITY_SELECTION_2, (ushort)relay2Logic);
        }

        public void WriteReleSecurityTimeInterval(ushort time)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TIME_DELAY_SECURITY_SELECTION2, time);
        }

        public void WriteReleSecurityStatus(ushort time)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.STATUS_DELAY_SECURITY_SELECTION2, time);
        }

        public void WriteFrequencySelection(TypeMeterConection.frequency frequency)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.FREQUENCY_SELECTION, (ushort)frequency);
        }

        #endregion 

        #region Operaciones lectura

        public Keyboard ReadKeyboard()
        {
            LogMethod();

            var keyboardReadingAux = Modbus.ReadRegister((ushort)Registers.TOROIDAL_INPUTS_KEYS);

            var keyboardReading = (Keyboard)keyboardReadingAux;

            if (keyboardReading.HasFlag(Keyboard.EXTERNAL_INPUT))
                _logger.Warn("Atención: la entrada externa esta activada durante la prueba de teclado");

            if (keyboardReading.HasFlag(Keyboard.MAGNETO_INPUT))
                _logger.Warn("Atención: la entrada de magnetotermico esta activada durante la prueba de teclado");

            if (keyboardReading.HasFlag(Keyboard.TOROIDAL_ERROR))
                _logger.Warn("Atención: la entrada de error de toroidal esta activada durante la prueba de teclado");

            return keyboardReading;
        }

        public Inputs ReadInputs()
        {
            LogMethod();

            return (Inputs)Modbus.ReadRegister((ushort)Registers.TOROIDAL_INPUTS_KEYS);
        }

        public Toroidal ReadToroidalStatus()
        {
            LogMethod();

            return (Toroidal)Modbus.ReadRegister((ushort)Registers.TOROIDAL_INPUTS_KEYS);
        }

        public ushort ReadErrorCode()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        public uint ReadSerialNumber()
        {
            LogMethod();
            return (uint)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_SERIAL_NUMBER) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_SERIAL_NUMBER));
        }

        public int ReadFrameNumber()
        {
            LogMethod();

            return (int)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_FRAME_NUMBER)) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_FRAME_NUMBER);
        }

        public int ReadDCCalibrationResult()
        {
            LogMethod();

            return (int)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_DC_CALIBRATION_RESULT)) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_DC_CALIBRATION_RESULT);
        }

        public int ReadACCalibrationResult()
        {
            LogMethod();

            return (int)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_AC_CALIBRATION_RESULT)) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_AC_CALIBRATION_RESULT);
        }

        public uint ReadCalibrationResult()
        {
            LogMethod();

            return (uint)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_CALIBRATION_RESULT)) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_CALIBRATION_RESULT);
        }

        public uint ReadCalibrationResultDC()
        {
            LogMethod();

            return (uint)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_DC_CALIBRATION_RESULT)) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_DC_CALIBRATION_RESULT);
        }

        public uint ReadCalibrationResultAC()
        {
            LogMethod();

            return (uint)((Modbus.ReadRegister((ushort)Registers.HIGH_LEVEL_AC_CALIBRATION_RESULT)) << 16) + Modbus.ReadRegister((ushort)Registers.LOW_LEVEL_AC_CALIBRATION_RESULT);
        }

        public ushort ReadScaleConstant(CalibrationRegisters Constant)
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Constant);
        }

        public ushort ReadDCScaleConstant(DCCalibrationRegisters dcConstant)
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)dcConstant);
        }

        public ushort ReadACScaleConstant(ACCalibrationRegisters acConstant)
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)acConstant);
        }

        public string ReadTablesVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.TABLES_VERSION, 2);
        }

        public ushort ReadCurrent()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.DEVICE_CURRENT_READING);
        }

        public ushort ReadTriggerCurrent()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.TRIGGER_CURRENT);
        }

        public ushort ReadTriggerTime()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.TRIGGER_TIME);
        }

        public ushort ReadFirmwareVersion()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION);
        }

        public ushort ReadCommunicationsBoardVersion()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.COMMUNICATIONS_BOARD_VERSION);
        }

        public ushort ReadCurrentTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.CURRENT_TABLE, (ushort)value);

            Thread.Sleep(TimeWaitCom);

            var result = Modbus.ReadRegister((ushort)Registers.CURRENT_TABLE);        

            Thread.Sleep(TimeWaitCom);

            return result;
        }

        public ushort ReadTimeTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TIME_TABLE, (ushort)value);

            Thread.Sleep(TimeWaitCom);
            
            var result = Modbus.ReadRegister((ushort)Registers.TIME_TABLE);

            Thread.Sleep(TimeWaitCom);

            return result;
        }

        public ushort ReadSRDTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.SRD_RECONNECTION_TABLE, (ushort)(value));

            Thread.Sleep(500);         

            var result = Modbus.ReadRegister((ushort)Registers.SRD_RECONNECTION_TABLE);

            Thread.Sleep(500);

            return result;
        }

        public ushort ReadSRMTableElement(byte scale)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRM_RECONNECTION_TABLE, (ushort)(scale * 0x800 + 0x7FF));

            Thread.Sleep(500);
            
            var result = Modbus.ReadRegister((ushort)Registers.SRM_RECONNECTION_TABLE);

            Thread.Sleep(500);

            return result;
        }

        public Modes ReadMode()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.MODE) == (ushort)Modes.NORMAL ? Modes.NORMAL : Modes.TEST;
        }

        public string ReadInitialMessage()
        {
            LogMethod();

            var initialMessage = Modbus.ReadString((ushort)Registers.HIGH_LEVEL_INITIAL_MESSAGE,1);
            var initialMessage2 = Modbus.ReadString((ushort)Registers.LOW_LEVEL_INITIAL_MESSAGE,1);

            return initialMessage + initialMessage2;
        }

        public string ReadInitialMessageCC()
        {
            LogMethod();

            var initialMessage = Modbus.ReadRegister((ushort)Registers.CC_SIEMENS_INITIAL_MESSAGE);
            var initialMessage2 = Modbus.ReadRegister((ushort)Registers.CC_SIEMENS_INITIAL_MESSAGE + 1);


            return initialMessage.ToString();
        }

        public string ReadScaleTable(TableKind typeTable)
        {
            LogMethod();

            StringBuilder table = new StringBuilder();

            for(byte pos=0; pos< 255; pos++) 
            {
                var valor = typeTable == TableKind.CURRENT ? ReadCurrentTableElement(pos) : ReadTimeTableElement(pos);

                if (valor == ValorFinTabla)
                    break;

                table.Append(valor + ";");
            }

            return table.ToString();
        }

        public string ReadReconectionTable(TableKindReconection typeTable)
        {
            LogMethod();

            StringBuilder table = new StringBuilder();

            for (byte pos = 3; pos < 255; pos++)
            {
                var valor = typeTable == TableKindReconection.SRDT ? ReadSRDTableElement(pos) : ReadSRMTableElement(pos);

                if (valor == 0xFFFF)
                    break;

                table.Append(valor + ";");
            }

            return table.ToString();
        }

        public ushort ReadReleSecurityStatus()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.STATUS_DELAY_SECURITY_SELECTION2);
        }

        public ushort ReadReleSecurityTime()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.TIME_DELAY_SECURITY_SELECTION2);
        }

        public ushort ReadSRDSelection()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.SRD_SCALE_SELECTION);
        }

        public ushort ReadSRMSelection()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.SRM_SCALE_SELECTION);
        }

        public LockRegister ReadRegisterLocking()
        {
            LogMethod();

            return (LockRegister)Modbus.ReadRegister((ushort)Registers.LOCK_SELECTION);
        }

        public ushort ReadFrequency()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.FREQUENCY_SELECTION);
        }

        public ushort ReadRelaySecurity1()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.RELAY_SECURITY_SELECTION);
        }

        public ushort ReadRelaySecurity2()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.RELAY_SECURITY_SELECTION_2);
        }

        public ushort ReadScaleLimit()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.SCALE_LIMIT_SELECTION);
        }

        #endregion

        #region Enumerados

        public enum TableKind
        {
            TIME,
            CURRENT
        }

        public enum TableKindReconection
        {
            SRDT,
            SRMT
        }
        //Enumerados
        public enum Limits
        {
            HIGHT,
            LOW
        }

        public enum Logic
        {
            ESTANDAR,
            POSITIVA
        }

        public enum Sequence
        {
            SRD = 0x0010,
            SRM = 0x0011
        }

        public enum Multiplier
        {
            x100 = 0x0400,
            x1 = 0x0000,
        }

        public enum ScaleCalibration
        {
            SCALE_1 = 0x0104,
            SCALE_2 = 0x0204,
            SCALE_3 = 0x0304,
            SCALE_4 = 0x0404,
        }

        public enum Modes
        {
            NORMAL = 0x0000,
            TEST = 0x0005,
            RESET = 0x000B,
        }

        public enum Writing
        {
            PARAMETERS = 0x0000,
            SETUP = 0xFFFF,
        }

        public enum CuttingTypes
        {
            CONT = 0x0000,
            MAG = 0x0001,
            MAGBOB = 0x0002
        }
        public enum DisplayDigits
        {
            DIGIT_1 = 0x8000,
            DIGIT_2 = 0x4000,
            DIGIT_3 = 0x2000,
            DIGIT_4 = 0x1000,
            ALL_DIGITS = 0xF000,
        }

        public enum DisplayOperations
        {
            TURN_ON_ALL_DIGITS = 0x0020,
            TURN_OFF_ALL_DIGITS = 0x0040,
            INVERT_SEGMENTS = 0x0080,
        }

        [Flags]
        public enum Inputs
        {
            NO_INPUT = 0x0000,
            MAGNETO_INPUT = 0x0080,
            EXTERNAL_INPUT = 0x0200,
        }

        [Flags]
        public enum Inputs_B
        {
            NO_INPUT = 0x0000,
            EXTERNAL_INPUT = 0x0080,
        }

        [Flags]
        public enum Toroidal
        {
            TOROIDAL_OK = 0x0000,
            TOROIDAL_ERROR = 0x1000,
        }

        [Flags]
        public enum Keyboard
        {
            [Description("ninguna")]
            NO_INPUT = 0x0000,
            [Description("seleccion corriente")]
            CURRENT_SELECTION_KEY = 0x0001,
            [Description("seleccion polaridad")]
            POLARITY_SELECTION_KEY = 0x0002,
            [Description("seleccion polaridad")]
            TIME_SELECTION_KEY = 0x0004,
            [Description("Test")]
            TEST_KEY = 0x0008,
            [Description("Prog")]
            PROG_KEY = 0x0010,
            [Description("Reset")]
            RESET_KEY = 0x0020,
            [Description("Auto")]
            AUTO_KEY = 0x0040,
            MAGNETO_INPUT = 0x0080,
            EXTERNAL_INPUT = 0x0200,
            TOROIDAL_ERROR = 0x1000,
        }

        public enum Leds
        {
            [Description("la luz verde del backlight y de power")]
            TURN_OFF_BACKLIGHT_POWER_PREALARM_LEDS = 0x0000,
            [Description("el led naranja de prealarma")]
            ORANGE_PREALARM_LED = 0x0008,
            [Description("el led rojo de power")]
            RED_POWER_LED = 0x0010,
            [Description("el led rojo de power")]
            ORANGE_PREALARM_LED_RED_POWER_LED = 0x0018,
            RED_BACKLIGHT = 0x0004,
            ORANGE_RED_LED_RED_BACKLIGHT = 0x001C,
        }

        public enum Relays
        {
            [Description("desactivacion de cualquiera de los reles")]
            TURN_OFF_RELAYS = 0x0000,
            [Description("rele de disparo")]
            TRIGGER_RELAY = 0x0001,
            [Description("rele de prealarma")]
            PREALARM_RELAY = 0x0002,
            [Description("activacion rele motor")]
            RELAY_ENGINE_ON = 0x0100,
            [Description("desactivacion rele motor")]
            RELAY_ENGINE_OFF = 0x0200,
        }

        public enum DCCalibrationRegisters
        {
            DC_CALIBRATION_CONSTANTE_SCALE_1 = 0x0026,
            DC_CALIBRATION_CONSTANTE_SCALE_2 = 0x0027,
            DC_CALIBRATION_CONSTANTE_SCALE_3 = 0x0028
        }

        public enum ACCalibrationRegisters
        {
            AC_CALIBRATION_CONSTANTE_SCALE_1 = 0x0029,
            AC_CALIBRATION_CONSTANTE_SCALE_2 = 0x002A,
            AC_CALIBRATION_CONSTANTE_SCALE_3 = 0x002B
        }

        public enum CalibrationRegisters
        {
            CALIBRATION_CONSTANT_SCALE_1 = 0x0026,
            CALIBRATION_CONSTANT_SCALE_2 = 0x0027,
            CALIBRATION_CONSTANT_SCALE_3 = 0x0028,
            CALIBRATION_CONSTANT_SCALE_4 = 0x0029
        }

        [Flags]
        public enum LockRegister
        {
            BLOQUEO_TECLA_TIEMPO = 0x0001,
            BLOQUEO_TECLA_INTENSIDAD = 0x0002,
            BLOQUEO_TECLA_RELE = 0x0004,
            BLOQUEO_TECLA_REC = 0x0008,
            MOSTRAR_MENU_SETUP_COM = 0x0010,
            MOSTRAR_MENU_SETUP_FREC = 0x0020,
            MOSTRAR_MENU_SETUP_LIM = 0x0040,
            MOSTRAR_MENU_SETUP_SECT = 0x080,
            MOSTRAR_MENU_SETUP_TRIP = 0x0100,
            MOSTRAR_MENU_SETUP_RSTC = 0x0200,
            BLOQUEO_ESCRITURA_MODBUS = 0x0400,
            SR_FIJA = 0x0800,
            ACTIVACION_ENTRADA_EXTERNA = 0x1000,
            BLOQUEO_SELECCION_2_RELES = 0x2000
        }

        public enum Registers
        {
            PERIFERIC_NUMBER = 0x0000,
            FREQUENCY_SELECTION = 0x0003,
            CURRENT_SCALE_SELECTION = 0x0004,
            TRIGGER_CURRENT = 0x0004,
            TIME_SCALE_SELECTION = 0x0005,
            TRIGGER_TIME = 0x0005,
            RELAY_SECURITY_SELECTION = 0X0006,
            STATUS_DELAY_SECURITY_SELECTION2 = 0x0007,
            TIME_DELAY_SECURITY_SELECTION2 = 0X0008,
            RELAY_SECURITY_SELECTION_2 = 0X0009,
            HIGH_LEVEL_SERIAL_NUMBER = 0x000B,
            LOW_LEVEL_SERIAL_NUMBER = 0x000C,
            FIRMWARE_VERSION = 0x000D,
            TABLES_VERSION = 0x000E,
            CUTTING_TYPE = 0x000E,
            CC_SIEMENS_INITIAL_MESSAGE = 0x000E,
            LOCK_SELECTION = 0x000F,
            SRD_SCALE_SELECTION = 0x0010,
            HIGH_LEVEL_FRAME_NUMBER = 0x0010,
            LOW_LEVEL_FRAME_NUMBER = 0x0011,
            SRM_SCALE_SELECTION = 0x0011,
            ERROR_CODE = 0x0012,
            DEVICE_CURRENT_READING = 0x0015,
            PARAMETERS_WRITING_2 = 0x0016,
            HIGH_LEVEL_DC_CALIBRATION_RESULT = 0x0018,
            LOW_LEVEL_DC_CALIBRATION_RESULT = 0x0019,
            EXTERNAL_TRIGGER_RESET = 0x0019,
            RESET_TRIGGER = 0X0019,
            SETUP_WRITING = 0x001A,
            PARAMETERS_WRITING_1 = 0x001A,
            HIGH_LEVEL_AC_CALIBRATION_RESULT = 0x001A,
            LOW_LEVEL_AC_CALIBRATION_RESULT = 0x001B,
            RESET_TRIGGER_2 = 0x001C,
            CALIBRATION = 0x0020,
            MODE = 0x0020,
            CURRENT_TIME_TABLES_CLEAR = 0x0021,
            RECONNECTION_TABLES_CLEAR = 0x0021,
            CURRENT_TABLE = 0x0022,
            TIME_TABLE = 0x0023,
            TOROIDAL_INPUTS_KEYS = 0x0024,
            DISPLAY_OUTPUTS = 0x0025,
            HIGH_LEVEL_CALIBRATION_RESULT = 0x002A,
            LOW_LEVEL_CALIBRATION_RESULT = 0x002B,
            CLEAR_RECONNECTION_TABLE = 0x002C,
            SRD_RECONNECTION_TABLE = 0x002D,
            SRM_RECONNECTION_TABLE = 0x002E,
            SCALE_LIMIT_SELECTION = 0x002F,
            HIGH_LEVEL_INITIAL_MESSAGE = 0x0030,
            LOW_LEVEL_INITIAL_MESSAGE = 0x0031,
            COMMUNICATIONS_BOARD_VERSION = 0xABCD,

        }

        #endregion

        public Dictionary<ScaleCalibration, CalibrationRegisters> calibrationScaleDictionary = new Dictionary<ScaleCalibration, CalibrationRegisters>()
        {
            {ScaleCalibration.SCALE_1, CalibrationRegisters.CALIBRATION_CONSTANT_SCALE_1},
            {ScaleCalibration.SCALE_2, CalibrationRegisters.CALIBRATION_CONSTANT_SCALE_2},
            {ScaleCalibration.SCALE_3, CalibrationRegisters.CALIBRATION_CONSTANT_SCALE_3},
            {ScaleCalibration.SCALE_4, CalibrationRegisters.CALIBRATION_CONSTANT_SCALE_4}
        };

        public Dictionary<ScaleCalibration, DCCalibrationRegisters> calibrationDCScaleDictionary = new Dictionary<ScaleCalibration, DCCalibrationRegisters>()
        {
            {ScaleCalibration.SCALE_1, DCCalibrationRegisters.DC_CALIBRATION_CONSTANTE_SCALE_1},
            {ScaleCalibration.SCALE_2, DCCalibrationRegisters.DC_CALIBRATION_CONSTANTE_SCALE_2},
            {ScaleCalibration.SCALE_3, DCCalibrationRegisters.DC_CALIBRATION_CONSTANTE_SCALE_3},
        };

        public Dictionary<ScaleCalibration, ACCalibrationRegisters> calibrationACScaleDictionary = new Dictionary<ScaleCalibration, ACCalibrationRegisters>()
        {
            {ScaleCalibration.SCALE_1, ACCalibrationRegisters.AC_CALIBRATION_CONSTANTE_SCALE_1},
            {ScaleCalibration.SCALE_2, ACCalibrationRegisters.AC_CALIBRATION_CONSTANTE_SCALE_2},
            {ScaleCalibration.SCALE_3, ACCalibrationRegisters.AC_CALIBRATION_CONSTANTE_SCALE_3},
        };

        // Estructuras
        public struct ScaleTableConfiguration
        {
            private  string ScaleTableName;

            private List<TableElement> listElements;

            public bool HasElements
            {
                get
                {
                    return listElements.Any(); 
                }
            }

            public ScaleTableConfiguration(List<string> scaleElements, string scaleTableName)
            {
                ScaleTableName = scaleTableName;

                if (scaleElements.Count() > 32)
                    throw new Exception("Error en la configuración de la tabla de reconexión, el numero de elementos está limitado a 32");

                listElements = new List<TableElement>();

                foreach (string scaleElement in scaleElements)
                {
                    int temporalValue = 0;

                    if (!int.TryParse(scaleElement, out temporalValue))
                        throw new Exception(string.Format("Error al configurar la tabla de escalas de {0}, el valor introducido en la base de datos no es un formato válido", scaleTableName));

                    listElements.Add(new TableElement(temporalValue, (byte)listElements.Count()));
                }
            }

            public List<TableElement> ListElemnts { get { return listElements; } }

            public string listElementsString
            {
                get
                {
                    StringBuilder list = new StringBuilder();

                    foreach (TableElement element in ListElemnts)
                        list.Append( element.GetElementValue.ToString()  + ";");
                    return list.ToString();
                }
            
            }
        }

        public struct TableElement
        {
            private bool x100;
            private ushort elementValue;
            private byte position;

            public TableElement(int value, byte positionValue)
            {
                if (value > 1024)
                {
                    x100 = true;
                    elementValue = (ushort)(value / 100);
                }
                else
                {
                    x100 = false;
                    elementValue = (ushort)value;
                }
                position = positionValue;
            }

            public bool X100
            {
                get
                {
                    return x100;
                }
            }

            public ushort GetElementValue
            {
                get
                {
                    return (ushort)(elementValue * (x100 ? 100 : 1));
                }
            }

            public ushort ElementValue
            {
                get
                {
                    return (ushort)elementValue;
                }
            }

            public byte Position
            {
                get
                {
                    return position;
                }
            }
        }

        public struct ReconnectionTableConfiguration
        {
            public bool HasReconnections
            {
                get
                {
                    bool hasReconnections = false;
                    listStages.ForEach(x => hasReconnections |= x != null);
                    return hasReconnections;
                }
            }

            public ReconnectionTableConfiguration(int reconnectionStages, ushort resetTime)
            {
                listStages = new List<ReconnectionStage?>(reconnectionStages);

                if (resetTime > 2046)
                    throw new Exception("Error al grabar el tiempo de reset para la tabla de reconexiones, el valor introducido es mayor del límite");
                ResetTime = resetTime;
            }

            public ushort TableSize
            {
                get
                {
                    ushort size = 3;
                    listStages.ForEach((x) => size += (ushort)(x.HasValue ? 2 : 0));
                    if (size > 32)
                        throw new Exception(string.Format("Error en la configuración de la tabla de reconexión, la capacidad de la tabla es de 32 elementos, mientras que se intenta introducir {0} elementos", size));
                    return size;
                }
            }

            public ushort TotalAmountOfReconnections
            {
                get
                {
                    ushort totalReconnections = 0;
                    listStages.ForEach((x) => totalReconnections += (ushort)(x.HasValue ? x.Value.AmountOfReconnections : 0));
                    if (totalReconnections > 2046)
                        throw new Exception(string.Format("Error en la configuración de la tabla de reconexión, la cantidad total de reconexiones no puede superar las 2046 reconexiones, mientras que se intenta introducir {0} reconexiones", totalReconnections));
                    return totalReconnections;
                }
            }

            public ushort ResetTime;

            public List<ReconnectionStage?> listStages;

            public struct ReconnectionStage
            {
                public ushort AmountOfReconnections;
                public ushort ReconnectionTime;

                public ReconnectionStage(string _AmountOfReconnections, string _ReconnectionTime)
                {
                    //List<string> reconnectionConfiguration = new List<string>(2);
                    //var reconnectionParameters = info.Split(';');

                    //if (reconnectionParameters.Length != 2)
                    //    throw new Exception("Error en la configuración de la etapa de reconexión, numero de parametros incorrectos");

                    //foreach (string reconnectionParameter in reconnectionParameters)
                    //{
                    //    ushort checkedValue = Convert.ToUInt16(reconnectionParameter);
                    //    if (checkedValue > 2046)
                    //        throw new Exception(string.Format("Error en la configuración de la etapa de reconexión, valor de parametro fuera de márgenes, valor máximo 2046, valor introducido: {0}", checkedValue));
                    //    reconnectionConfiguration.Add(reconnectionParameter);
                    //}

                    AmountOfReconnections = Convert.ToUInt16(_AmountOfReconnections);
                    ReconnectionTime = Convert.ToUInt16(_ReconnectionTime);
                }

                public override string ToString()
                {
                    return "{" + AmountOfReconnections.ToString() + ";" + ReconnectionTime.ToString() + "}";
                }
            }

            public string GetStageListString()
            {
                if (HasReconnections)
                {
                    var stageList = new List<string>();
                    listStages.ForEach(x => stageList.Add(x.ToString()));
                    return string.Join(", ", stageList);
                }
                else
                    return string.Empty;
            }
        }
    }
}
