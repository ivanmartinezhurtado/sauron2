﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion (1.43)]
    public class EKOR_DTC2TestBase : TestBase
    {
        private EKOR_DTC2 EkorDTC2;
        private Tower3 Tower;
        private string CAMERA_IDS;
        private string CAMERA_IDS_LED;
        private MTX3293 metrix;
        private USB5856 usb5856;

        private bool utilNuevo;
        private EKOR_DTC2.EkorModelType model;

        #region Constants

        //ACTIVATE INPUTS
        internal const byte ACTIVATE_IN1 = 42;
        internal const byte ACTIVATE_IN2 = 41;
        internal const byte ACTIVATE_IN3 = 19;
        internal const byte ACTIVATE_IN4 = 18;
        internal const byte ACTIVATE_IN5 = 17;
        internal const byte ACTIVATE_IN6 = 40;
        internal const byte ACTIVATE_IN7 = 0;
        internal const byte ACTIVATE_IN8 = 1;
        internal const byte ACTIVATE_IN9 = 2;
        internal const byte ACTIVATE_IN10 = 3;
        internal const byte SUPPLY_TO_ACTIVATE_AUX_INPUTS = 22;

        //DETECTION OUTPUTS
        internal const byte DETECTION_OUT1 = 20;
        internal const byte DETECTION_OUT2 = 19;
        internal const byte DETECTION_OUT3 = 18;
        internal const byte DETECTION_OUT4 = 17;
        internal const byte DETECTION_OUT5 = 16;
        internal const byte DETECTION_OUT6 = 15;
        internal const byte DETECTION_OUT7 = 9;

        // UTIL VIEJO
        internal const int PISTONS_REAR = 5;
        internal const int PISTONS_FRONT = 6;
        internal const int _24V = 14;
        internal const int IN_DISPARO = 9;

        // NUEVO UTIL
        internal const int ANY_BUTTON = 9999;
        internal const int OUT_SET_BUTTON_ACTUATOR = 7;
        internal const int OUT_ESC_BUTTON_ACTUATOR = 8;
        internal const int OUT_UP_BUTTON_ACTUATOR = 9; //ADVANTECH
        internal const int OUT_LEFT_BUTTON_ACTUATOR = 10;
        internal const int OUT_RIGHT_BUTTON_ACTUATOR = 11;
        internal const int OUT_DOWN_BUTTON_ACTUATOR = 12;
        internal const int OUT_RESET_BUTTON = 8;
        internal const int OUT_ACTIVATE_POWER_SUPPLY = 17;
        internal const int OUT_DIVISOR_INPUT_CURRENT = 19;
        internal const int OUT_POWER_SUPPLY_SWITCH = 20;
        internal const int OUT_FAN = 15;
        internal const int IN_EKORPLUS_LED = 16;
        internal const int IN_DETECTION_FIJACION_EQUIPO = 21;
        internal const int IN_DEVICE_PRESENCE = 23;
       
        internal const byte EV_GENERAL = 4;
        internal const byte EV_GENERAL_AMPLIACION = 9;
        internal const byte PISTON_PUNTAS = 6;
        internal const byte SIN_PASARELA = 49;
        internal const byte FIJACION_CARRO = 59;
        internal const byte PISTON_RS232 = 50;
        internal const byte PISTON_RS485 = 57;
        internal const byte PISTON_TRIGGER = 58;
        internal const byte PISTON_DOMO = 5;
        internal const byte DETECTION_DOMO_DOWN = 11;
        internal const byte DETECTION_DOMO_UP = 14;

        // DICCIONARIOS RELACION RELÉS CON ENTRADAS/SALIDAS DEL EQUIPO
        public Dictionary<byte, EKOR_DTC2.enumInputs> DictionaryRelationRelaysInputs = new Dictionary<byte, EKOR_DTC2.enumInputs>()
        {
            { ACTIVATE_IN1, EKOR_DTC2.enumInputs.IN1 },
            { ACTIVATE_IN2, EKOR_DTC2.enumInputs.IN2 },
            { ACTIVATE_IN3, EKOR_DTC2.enumInputs.IN3 },
            { ACTIVATE_IN4, EKOR_DTC2.enumInputs.IN4 },
            { ACTIVATE_IN5, EKOR_DTC2.enumInputs.IN5 },
            { ACTIVATE_IN6, EKOR_DTC2.enumInputs.IN6 },
            { ACTIVATE_IN7, EKOR_DTC2.enumInputs.IN7 },
            { ACTIVATE_IN8, EKOR_DTC2.enumInputs.IN8 },
            { ACTIVATE_IN9, EKOR_DTC2.enumInputs.IN9 },
            { ACTIVATE_IN10, EKOR_DTC2.enumInputs.IN10 }
        };

        public Dictionary<byte, EKOR_DTC2.enumInputsCI> DictionaryRelationRelaysInputsCI = new Dictionary<byte, EKOR_DTC2.enumInputsCI>()
        {
            { ACTIVATE_IN1, EKOR_DTC2.enumInputsCI.IN1 },
            { ACTIVATE_IN2, EKOR_DTC2.enumInputsCI.IN2 },
            { ACTIVATE_IN3, EKOR_DTC2.enumInputsCI.IN3 },
            { ACTIVATE_IN4, EKOR_DTC2.enumInputsCI.IN4 },
            { ACTIVATE_IN5, EKOR_DTC2.enumInputsCI.IN5 },
            { ACTIVATE_IN6, EKOR_DTC2.enumInputsCI.IN6 },
            { ACTIVATE_IN7, EKOR_DTC2.enumInputsCI.IN7 },
            { ACTIVATE_IN8, EKOR_DTC2.enumInputsCI.IN8 },
            { ACTIVATE_IN9, EKOR_DTC2.enumInputsCI.IN9 },
            { ACTIVATE_IN10, EKOR_DTC2.enumInputsCI.IN10 }
        };

        public Dictionary<EKOR_DTC2.enumOutputsRPGRPT, byte> DictionaryRelationRelaysOutputs = new Dictionary<EKOR_DTC2.enumOutputsRPGRPT, byte>()
        {
            { EKOR_DTC2.enumOutputsRPGRPT.OUT1, DETECTION_OUT1 },
            { EKOR_DTC2.enumOutputsRPGRPT.OUT2, DETECTION_OUT2 },
            { EKOR_DTC2.enumOutputsRPGRPT.OUT3, DETECTION_OUT3 },
            { EKOR_DTC2.enumOutputsRPGRPT.OUT4, DETECTION_OUT4 },
            { EKOR_DTC2.enumOutputsRPGRPT.OUT5, DETECTION_OUT5 },
            { EKOR_DTC2.enumOutputsRPGRPT.OUT6, DETECTION_OUT6 },
            { EKOR_DTC2.enumOutputsRPGRPT.OUT7, DETECTION_OUT7 }
        };

        public Dictionary<EKOR_DTC2.enumOutputsCI, byte> DictionaryRelationRelaysOutputsCI = new Dictionary<EKOR_DTC2.enumOutputsCI, byte>()
        {
            { EKOR_DTC2.enumOutputsCI.OUT1, DETECTION_OUT7 },
            { EKOR_DTC2.enumOutputsCI.OUT2, DETECTION_OUT6 },
            { EKOR_DTC2.enumOutputsCI.OUT3, DETECTION_OUT5 },
            { EKOR_DTC2.enumOutputsCI.OUT4, DETECTION_OUT4 },
            { EKOR_DTC2.enumOutputsCI.OUT5, DETECTION_OUT3 },
            { EKOR_DTC2.enumOutputsCI.OUT6, DETECTION_OUT2 },
            { EKOR_DTC2.enumOutputsCI.OUT7, DETECTION_OUT1 }
        };

        private enum Pistones
        {
            PISTON_PUNTAS = 6,
            SIN_PASARELA = 49,
            PISTON_RS232 = 50,
            PISTON_RS485 = 57,
            PISTON_TRIGGER = 58,
            FIJACION_CARRO = 59,
            NOTHING = 9999
        }

        #endregion

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out Tower))
            {
                Tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", Tower);
            }

            if (EkorDTC2 == null)
                EkorDTC2 = new EKOR_DTC2(Comunicaciones.SerialPort);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            Tower.PowerSourceIII.Tolerance = Configuracion.GetDouble("TOLERANCE_MTE", 0.1, ParamUnidad.PorCentage);
            Tower.IO.DO.On(23);   // Modificación manguera, el equipo se alimenta por la salida de la Chroma a 24VDC
            Tower.IO.DO.On(_24V);
            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V), 0.5);

            model = EKOR_DTC2.EkorModelType.UNKNOW;
            Enum.TryParse<EKOR_DTC2.EkorModelType>(Identificacion.MODELO, out model);

            utilNuevo = Model.NumUtilBien == 6534 ? false : true;
            usb5856 = new USB5856();

            if (utilNuevo)
            {
                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[IN_DEVICE_PRESENCE];
                }, "Error, no se ha detectado la prensencia del equipo", 20, 500, 500);

                TestDeviceConnection();
            }     
        }

        public void InitMetrix(string nameDevicePortVirtual)
        {
            var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            metrix = new MTX3293();
            metrix.PortCom = portMetrix;
        }

        public void TestComunicationsRS232(int porEkor232)
        {
            if (porEkor232 == 0)
                porEkor232 = Comunicaciones.SerialPort;

            EkorDTC2.Cirbus.PortCom = porEkor232;

            logger.InfoFormat("Puerto de comunicacion RS232 asignado en  COM {0}", porEkor232.ToString());

            SamplerWithCancel((p) =>
            {
                EkorDTC2.ReadFirmware();
                return true;
            }, string.Empty, 3, 500, 1000, true, true, (e) => Error().UUT.COMUNICACIONES.NO_COMUNICA("POR RS232").Throw());
            Resultado.Set("TEST_RS232", "OK", ParamUnidad.s);

            EkorDTC2.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_DEFAULT", "FF FF 00 00 0005 00 00 0000 0000 00 00 00 00 00 00 0000", ParamUnidad.SinUnidad));
            Delay(2000, "Configurando ajustes de usuario");

            EkorDTC2.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_DEFAULT", "0102 270F 04B0 0438 1964 05 00 00 00 00 0000", ParamUnidad.SinUnidad));
            Delay(2000, "Configurando ajustes de instalador");

            var version = EkorDTC2.ReadFirmware();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la versión de firmware es incorrecta"), ParamUnidad.SinUnidad);
                      
            if (model != EKOR_DTC2.EkorModelType.RPG_RPT)
            {
                var cpu = EkorDTC2.ReadCPU();
                logger.InfoFormat("CPU : {0}", porEkor232.ToString());
                Assert.AreEqual("CPU", cpu, Identificacion.VERSION_CPU, Error().SOFTWARE.CPU.VERSION_INCORRECTA("Error, la versión de cpu es incorrecta"), ParamUnidad.SinUnidad);                
            }

        }

        public void TestComunicationsRS485(int portEkor485)
        {
            if (portEkor485 == 0)
                portEkor485 = Comunicaciones.SerialPort2;

            EkorDTC2.Cirbus.PortCom = portEkor485;

            SamplerWithCancel((p) =>
            {

                EkorDTC2.ReadFirmware();
                logger.InfoFormat("Puerto de comunicacion RS485 asignado en  COM {0}", portEkor485.ToString());
                return true;
            }, "Error. No se ha podido cominunicar con el equipo por RS485", 3, 500, 1000, false, false, (e) => Error().UUT.COMUNICACIONES.NO_COMUNICA("POR RS485").Throw());

            Resultado.Set("TEST_RS485", "OK", ParamUnidad.s);
            
        }

        public void TestConsumoBateria()
        {
            Tower.IO.DO.On(20, 21);

            EkorDTC2.Cirbus.DtrEnable = EkorDTC2.Cirbus.RtsEnable = false;

            Delay(30000, "Esperando a que el equipo entre en modo Standbye y el circuito de bateria se cargue para empezar a medir correctamente");

            var param = new AdjustValueDef(Params.I_DC.Null.EnVacio.TestPoint("PILA"), ParamUnidad.A);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
            }, 0, 15, 2000);

            if (Identificacion.MODELO.Trim() == "RPG-RPT")
            {
                Tower.IO.DO.On(PISTON_DOMO);
                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[DETECTION_DOMO_UP];
                }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());
                Tower.IO.DO.PulseOn(300, OUT_ESC_BUTTON_ACTUATOR);
                Delay(500, "Retirada piston tecla");
                Tower.IO.DO.Off(PISTON_DOMO);
                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[DETECTION_DOMO_DOWN];
                }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

                var param2 = new AdjustValueDef(Params.I_DC.Null.ConCarga.TestPoint("PILA"), ParamUnidad.A);
                TestMeasureBase(param2,
                (step) =>
                {
                    return Tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
                }, 0, 20, 1500);

                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "EKOR_DTC2_DISPLAY_5V",
                    "DISPLAY_5V", "EKOR_DTC2");
            }

            EkorDTC2.Cirbus.DtrEnable = EkorDTC2.Cirbus.RtsEnable = true;

            Tower.IO.DO.Off(20, 21);

            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V), 0.5);
        }

        public void AssemblyBattery()
        {
            if (utilNuevo)
            {
                TestDeviceDisconnection();

                Shell.MsgBox("Saque el equipo del útil e introduzca la pila y vuelva a introducir el equipo al útil", "ENSAMBLE LA PILA");

                TestDeviceConnection();
            }
            else
                Shell.MsgBox("Ensamble la pila al equipo", "ENSAMBLE LA PILA");
        }

        public void TestSetupDefault()
        {            
            EkorDTC2.WriteDate(DateTime.Now);
            Delay(2000, "Escribiendo fecha y hora al equipo");
     
            var defaultFactorValue = (int)Configuracion.GetDouble(Params.GAIN_I.Other("DEFAULT").Name, 65536, ParamUnidad.SinUnidad);
            var DefaultFactorsCalibration = new EKOR_DTC2.FactorsCalibrationCurrents();
            DefaultFactorsCalibration.FactorsScalePrecision = new EKOR_DTC2.FasesEscala { L1 = defaultFactorValue, L2 = defaultFactorValue, L3 = defaultFactorValue, LH = defaultFactorValue };
            DefaultFactorsCalibration.FactorsScaleCC = new EKOR_DTC2.FasesEscala { L1 = defaultFactorValue, L2 = defaultFactorValue, L3 = defaultFactorValue, LH = defaultFactorValue };

            EkorDTC2.WriteCurrentFactorCalibration(DefaultFactorsCalibration);    
            
            if(model == EKOR_DTC2.EkorModelType.CI)
            {
                var defaultFactorsVoltageCalibration = new EKOR_DTC2.FactorsCalibrationVoltages();
                defaultFactorsVoltageCalibration.L1 = defaultFactorValue;
                defaultFactorsVoltageCalibration.L2 = defaultFactorValue;
                defaultFactorsVoltageCalibration.L3 = defaultFactorValue;

                EkorDTC2.WriteVoltageFactorCalibration(defaultFactorsVoltageCalibration);
            }                     
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var factorsCalibration = EkorDTC2.ReadCurrentFactorCalibration();

            var defaultFactorsPrecision = Identificacion.MODELO.ToUpper().Contains("CI") ? new EKOR_DTC2.FasesEscala { L1 = 161000, L2 = 161000, L3 = 161000, LH = 161000 }
                                                                                : new EKOR_DTC2.FasesEscala { L1 = 95000, L2 = 95000, L3 = 95000, LH = 95000 };
            var defaultFactorsCC = Identificacion.MODELO.ToUpper().Contains("CI") ? new EKOR_DTC2.FasesEscala { L1 = 232000, L2 = 232000, L3 = 232000, LH = 232000 }
                                                                                : new EKOR_DTC2.FasesEscala { L1 = 138500, L2 = 138500, L3 = 138500, LH = 138500 };
            factorsCalibration.FactorsScalePrecision = defaultFactorsPrecision;
            factorsCalibration.FactorsScaleCC = defaultFactorsCC;
            EkorDTC2.WriteCurrentFactorCalibration(factorsCalibration);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 1.5, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 2, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = 50, L2 = 100, L3 = 150 };                                                                           
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, 0, ParamUnidad.A);
            crucePistasI.L1.Average = ConsignasI.L1;
            crucePistasI.L2.Average = ConsignasI.L2;
            crucePistasI.L3.Average = ConsignasI.L3;
            crucePistasI.L1.Tolerance = Params.I.Null.CrucePistas.Tol();
            crucePistasI.L2.Tolerance = Params.I.Null.CrucePistas.Tol();
            crucePistasI.L3.Tolerance = Params.I.Null.CrucePistas.Tol();

            TestMeasureBase(crucePistasI,
                (step) =>
                {
                    var vars = EkorDTC2.ReadCurrentDisplayInAmpers(EKOR_DTC2.Scale.PRECISION).LINES;
                    return new double[] { vars.L1, vars.L2, vars.L3};

                }, delFirts, samples, timeInterval);        
            
            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestVacio()
        {
            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.I.L1.EnVacio.TestPoint("PRECISION"), ParamUnidad.A), true);
            defs.Neutro.Name.Replace("LN", "LH");

            TestMeasureBase(defs,
                (step) =>
                {
                    var readed = EkorDTC2.ReadMeasurementsCurrentDisplay();
                    return new double[] { readed.ScalePrecision.L1, readed.ScalePrecision.L2, readed.ScalePrecision.L3, readed.ScalePrecision.LH };
                }, 0, 10, 2000);

            var defs2 = new TriAdjustValueDef(new AdjustValueDef(Params.I.L1.EnVacio.TestPoint("CC"), ParamUnidad.A), true);
            defs.Neutro.Name.Replace("LN", "LH");

            TestMeasureBase(defs2,
                (step) =>
                {
                    var readed = EkorDTC2.ReadMeasurementsCurrentDisplay();
                    return new double[] { readed.ScaleCC.L1, readed.ScaleCC.L2, readed.ScaleCC.L3, readed.ScaleCC.LH };
                }, 0, 10, 2000);
        }

        public void TestKeyboard()
        {

            if (utilNuevo)
            {
                Tower.IO.DO.On(PISTON_DOMO);
                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[DETECTION_DOMO_UP];
                }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición superior").Throw());

                var SortButtonsDictionary = new Dictionary<EKOR_DTC2.Keyboard, byte>();
                SortButtonsDictionary.Add(EKOR_DTC2.Keyboard.SET_BUTTON, OUT_SET_BUTTON_ACTUATOR);
                SortButtonsDictionary.Add(EKOR_DTC2.Keyboard.ESC_BUTTON, OUT_ESC_BUTTON_ACTUATOR);
                SortButtonsDictionary.Add(EKOR_DTC2.Keyboard.UP_BUTTON, OUT_UP_BUTTON_ACTUATOR);
                SortButtonsDictionary.Add(EKOR_DTC2.Keyboard.RIGHT_BUTTON, OUT_RIGHT_BUTTON_ACTUATOR);
                SortButtonsDictionary.Add(EKOR_DTC2.Keyboard.LEFT_BUTTON, OUT_LEFT_BUTTON_ACTUATOR);
                SortButtonsDictionary.Add(EKOR_DTC2.Keyboard.DOWN_BUTTON, OUT_DOWN_BUTTON_ACTUATOR);

                SamplerWithCancel((step) =>
                {
                    if (EkorDTC2.ReadKeyboard() == EKOR_DTC2.Keyboard.NO_BUTTON_OR_INPUT)
                        return true;
                    return false;
                }, "Se ha detectado alguna tecla activada cuando no debería estarlo", 10, 200, 200, false, false, (e) => Error().UUT.HARDWARE.KEYBOARD(e).Throw());

                foreach (KeyValuePair<EKOR_DTC2.Keyboard, byte> key in SortButtonsDictionary)
                {
                    
                    SamplerWithCancel((p) =>
                    {
                        EkorDTC2.ReadKeyboard();

                        if (key.Value == OUT_UP_BUTTON_ACTUATOR)
                            usb5856.DO.PulseOn(500, key.Value);
                        else
                            Tower.IO.DO.PulseOn(500, key.Value);

                        if (EkorDTC2.ReadKeyboard() == key.Key)
                            return true;

                        return false;

                    }, string.Format("No se detecta la tecla {0}", key.Key.GetDescription().Replace("_", " ")), 5, 500, 500, true, true, (e) => Error().UUT.HARDWARE.KEYBOARD(e).Throw());
                   

                    Resultado.Set(key.Key.ToString(), "OK", ParamUnidad.SinUnidad);
                    
                }

                Tower.IO.DO.Off(PISTON_DOMO);
                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[DETECTION_DOMO_DOWN];
                }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());
            }

            else
            {
                var IMAGES_PATH = this.PathCameraImages + @"\EKOR_DTC2\";

                var SortButtonsList = new List<EKOR_DTC2.Keyboard>();
                SortButtonsList.Add(EKOR_DTC2.Keyboard.SET_BUTTON);
                SortButtonsList.Add(EKOR_DTC2.Keyboard.ESC_BUTTON);
                SortButtonsList.Add(EKOR_DTC2.Keyboard.UP_BUTTON);
                SortButtonsList.Add(EKOR_DTC2.Keyboard.RIGHT_BUTTON);
                SortButtonsList.Add(EKOR_DTC2.Keyboard.LEFT_BUTTON);
                SortButtonsList.Add(EKOR_DTC2.Keyboard.DOWN_BUTTON);

                foreach (EKOR_DTC2.Keyboard key in SortButtonsList)
                {
                    EkorDTC2.ReadKeyboard();
                    var frm = this.ShowForm(() => new ImageView("Test Teclado", IMAGES_PATH + key.ToString() + "_ACABADOS.jpg"), MessageBoxButtons.AbortRetryIgnore, string.Format("Pulse la tecla {0}", key.GetDescription()));

                    try
                    {
                        SamplerWithCancel((p) =>
                        {
                            if (EkorDTC2.ReadKeyboard() == key)
                                return true;
                            return false;
                        }, string.Format("No se detecta la tecla {0}", key.GetDescription().Replace("_", " ")), 40, 500, 0, true, true, (e) => Error().UUT.HARDWARE.KEYBOARD(e).Throw());
                    }
                    finally
                    {
                        this.CloseForm(frm);
                    }

                    Resultado.Set(key.ToString(), "OK", ParamUnidad.SinUnidad);
                }

                var errorKey = string.Empty;
                SamplerWithCancel((step) =>
                {
                    var key = EkorDTC2.ReadKeyboard();
                    if (key == EKOR_DTC2.Keyboard.NO_BUTTON_OR_INPUT)
                        return true;

                    errorKey = key.GetDescription();
                    return false;
                }, string.Format("Se ha detectado la tecla {0} activada cuando deberían estar todas desactivadas", errorKey), 10, 500, 200, false, false, (e) => Error().UUT.HARDWARE.KEYBOARD(e).Throw());
            }
        }

        public void TestDisplay()
        {
            if (utilNuevo)
            {
                var name = Identificacion.MODELO.Contains("CI") ? "_CI" : string.Empty;
                EkorDTC2.ReadyDisplay();
                Delay(200, "Enviando Trama");
                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, string.Format("EKOR_DTC2_DISPLAY_ALL{0}", name), string.Format("DISPLAY_ALL{0}", name), "EKOR_DTC2");
            }
            else
            {
                var frm = new Form();

                if(model == EKOR_DTC2.EkorModelType.CI)
                    frm = this.ShowForm(() => new ImageView("TEST DISPLAY", this.PathCameraImages + "\\EKOR_DTC2\\DISPLAY_ACABADOS_CI.jpg"), MessageBoxButtons.OKCancel);
                else
                    frm = this.ShowForm(() => new ImageView("TEST DISPLAY", this.PathCameraImages + "\\EKOR_DTC2\\DISPLAY_ACABADOS.jpg"), MessageBoxButtons.OKCancel);

                try
                {
                    SamplerWithCancel((step) =>
                    {
                        if (step % 3 == 0)
                            EkorDTC2.ReadyDisplay();
                        return frm.DialogResult == DialogResult.OK;
                    }, "Error display incorrecto", 50, 3000, 250, true, true, (e) => Error().UUT.HARDWARE.DISPLAY(e).Throw());
                }
                finally
                {
                    this.CloseForm(frm);
                }
            }

            Delay(4000, "Espera para que el equipo termine la rutina de mostrar por pantalla todos los segmentos(Sinó ha acabado rechaza tramas)");
        }

        public void TestLeds()
        {
            CAMERA_IDS_LED = GetVariable<string>("CAMARA_IDS_LED", CAMERA_IDS_LED);
            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LED, "EKOR_DTC2_LED", "LED", "EKOR_DTC2");
        }
          
        public void TestCover()
        {
            var caratula = Identificacion.CARATULA;
            if (caratula != "NO")
                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "EKOR_DTC2_CARATULA_" + caratula, "CARATULA_" + caratula, "EKOR_DTC2");
        }

        public void TestInputs()
        {
            var inputsNumber = Configuracion.GetDouble("NUMBER_OF_INPUTS", 2, ParamUnidad.SinUnidad);

          
            var inputsValue = new List<KeyValuePair<byte, EKOR_DTC2.enumInputs>>();

            for (int i = 0; i < inputsNumber; i++)
                inputsValue.Add(DictionaryRelationRelaysInputs.ToArray()[i]);


            foreach (KeyValuePair<byte, EKOR_DTC2.enumInputs> input in inputsValue)
            {
                this.Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                if (input.Value > EKOR_DTC2.enumInputs.IN5)
                    Tower.IO.DO.On(SUPPLY_TO_ACTIVATE_AUX_INPUTS);               

                Tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    var value = EkorDTC2.ReadInputs((int)inputsNumber);
                    return value == input.Value;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 5, 500, 200, false, false, 
                (e) => Error().UUT.HARDWARE.DIGITAL_INPUTS(e).Throw());


                Tower.IO.DO.Off(input.Key, SUPPLY_TO_ACTIVATE_AUX_INPUTS);

                SamplerWithCancel((p) =>
                {
                    var value = EkorDTC2.ReadInputs((int)inputsNumber);
                    return value == EKOR_DTC2.enumInputs.NOTHING;
                }, string.Format("Error. no se detecta la entrada {0} desactivada cuando debería estarlo", input.Value.ToString()), 5, 500, 200, false, false,
                (e) => Error().UUT.HARDWARE.DIGITAL_INPUTS(e).Throw());

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestInputsCI()
        {
            var inputsNumber = Configuracion.GetDouble("NUMBER_OF_INPUTS", 5, ParamUnidad.SinUnidad);

            var inputsValue = new List<KeyValuePair<byte, EKOR_DTC2.enumInputsCI>>();

            for (int i = 0; i < inputsNumber; i++)
                inputsValue.Add(DictionaryRelationRelaysInputsCI.ToArray()[i]);


            foreach (KeyValuePair<byte, EKOR_DTC2.enumInputsCI> input in inputsValue)
            {
                this.Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                if (input.Value > EKOR_DTC2.enumInputsCI.IN5)
                    Tower.IO.DO.On(SUPPLY_TO_ACTIVATE_AUX_INPUTS);

                Tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    var value = (EKOR_DTC2.enumInputsCI)EkorDTC2.ReadInputsCI();
                    return value == input.Value;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 5, 500, 200, false, false,
                (e) => Error().UUT.HARDWARE.DIGITAL_INPUTS(e).Throw());


                Tower.IO.DO.Off(input.Key, SUPPLY_TO_ACTIVATE_AUX_INPUTS);

                SamplerWithCancel((p) =>
                {
                    var value = (EKOR_DTC2.enumInputsCI)EkorDTC2.ReadInputsCI();
                    return value == EKOR_DTC2.enumInputsCI.NOTHING;
                }, string.Format("Error. no se detecta la entrada {0} desactivada cuando debería estarlo", input.Value.ToString()), 5, 500, 200, false, false,
                (e) => Error().UUT.HARDWARE.DIGITAL_INPUTS(e).Throw());

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestOutputs()
        {            
            var outputNumber = Configuracion.GetDouble("NUMBER_OF_OUTPUTS", 2, ParamUnidad.SinUnidad);

            var outputValue = new List<KeyValuePair<EKOR_DTC2.enumOutputsRPGRPT, byte>>();
            outputValue.Add(new KeyValuePair<EKOR_DTC2.enumOutputsRPGRPT, byte>(EKOR_DTC2.enumOutputsRPGRPT.OUT1, DETECTION_OUT1));
            outputValue.Add(new KeyValuePair<EKOR_DTC2.enumOutputsRPGRPT, byte>(EKOR_DTC2.enumOutputsRPGRPT.OUT2, DETECTION_OUT2));

            for (int i = 0; i < outputNumber; i++)
                outputValue.Add(DictionaryRelationRelaysOutputs.ToArray()[i]);
            
            foreach (KeyValuePair<EKOR_DTC2.enumOutputsRPGRPT, byte> output in outputValue)
            {
                this.Logger.InfoFormat("{0}", output.Key.ToString());

                EkorDTC2.WriteOutput(output.Key, EKOR_DTC2.stateOutputs.ON);                

                SamplerWithCancel((p) =>
                {                
                    return Tower.IO.DI[output.Value];
                }, string.Format("Error, no se ha detectado la salida {0} activada cuando deberia estarlo", output.Key.ToString()), 5, 500, 200, true, true,
                (e) => Error().UUT.HARDWARE.DIGITAL_OUTPUTS(e).Throw());

                EkorDTC2.WriteOutput(output.Key, EKOR_DTC2.stateOutputs.OFF);
               
                SamplerWithCancel((p) =>
                {                  
                    return !Tower.IO.DI[output.Value];
                }, string.Format("Error, no se ha detectado la salida {0} desactivada cuando deberia estarlo", output.Key.ToString()), 5, 500, 200, true, true,
                 (e) => Error().UUT.HARDWARE.DIGITAL_OUTPUTS(e).Throw());

                Resultado.Set(string.Format("{0}", output.Key.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestOutputsCI()
        {
            var outputNumber = Configuracion.GetDouble("NUMBER_OF_OUTPUTS", 2, ParamUnidad.SinUnidad);

            var outputValue = new List<KeyValuePair<EKOR_DTC2.enumOutputsCI, byte>>();       

            for (int i = 0; i < outputNumber; i++)
                outputValue.Add(DictionaryRelationRelaysOutputsCI.ToArray()[i]);

            foreach (KeyValuePair<EKOR_DTC2.enumOutputsCI, byte> output in outputValue)
            {
                this.Logger.InfoFormat("{0}", output.Key.ToString());

                EkorDTC2.WriteOutput(output.Key);

                if (output.Key == EKOR_DTC2.enumOutputsCI.OUT2)
                    Tower.IO.DO.On(43);

                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[output.Value];
                }, string.Format("Error, no se ha detectado la salida {0} activada cuando deberia estarlo", output.Key.ToString()), 5, 500, 200, true, true,
                 (e) => Error().UUT.HARDWARE.DIGITAL_OUTPUTS(e).Throw());

                EkorDTC2.WriteOutput(EKOR_DTC2.enumOutputsCI.NOTHING);

                if (output.Key == EKOR_DTC2.enumOutputsCI.OUT2)
                    Tower.IO.DO.Off(43);

                SamplerWithCancel((p) =>
                {
                    return !Tower.IO.DI[output.Value];
                }, string.Format("Error, no se ha detectado la salida {0} desactivada cuando deberia estarlo", output.Key.ToString()), 5, 500, 200, true, true,
                 (e) => Error().UUT.HARDWARE.DIGITAL_OUTPUTS(e).Throw());

                Resultado.Set(string.Format("{0}", output.Key.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestTrigger()
        {
            SamplerWithCancel((x) =>
            {
                Logger.InfoFormat(string.Format("Reintento : {0}", x));

                EkorDTC2.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_TRIGGER", "FF FF 00 00 0190 01 00 0000 0005 00 00 00 00 18 00 0000", ParamUnidad.SinUnidad));
                Delay(500, "");
                EkorDTC2.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_TRIGGER", "0102 270F 04B0 0438 1964 06 00 00 00 00 0000", ParamUnidad.SinUnidad));
                Delay(500, "");

                Tower.IO.DO.OffWait(500, (int)Pistones.PISTON_RS232);

                var currentTrigger = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 2, ParamUnidad.A);

                this.InParellel(
                () =>
                {
                    Tower.MUX.Reset();
                    Tower.MUX.Connect2((int)InputMuxEnum.IN9, Outputs.FreCH1, (int)InputMuxEnum.IN4, Outputs.FreCH2);

                    Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.5);

                    Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 0.8);

                },
                () =>
                {
                    Tower.ActiveCurrentCircuit(true, true, true);
                    Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(currentTrigger, 0, 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
                });

                var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

                var adjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    try
                    {
                        var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 10000, HP53131A.Channels.CH1, () =>
                        {
                            Delay(500, "");
                            Tower.IO.DO.Off(28);
                        });

                        return (timeResult.Value);
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Format("Error, el equipo no ha disparado"));
                    }
                }, 0, 1, 1000);

                Tower.ActiveCurrentCircuit(true, true, true);

                Tower.IO.DO.OnWait(1000, (int)Pistones.PISTON_RS232);

                SamplerWithCancel((p) =>
                {
                    EkorDTC2.ValidateTrigger();
                    return true;
                }, "Error, no se ha podido validar el disparo", 3, 500, 0, true, true, (e) => Error().UUT.DISPARO.NO_DISPARA(e).Throw());

                var inyectedCurrent = new AdjustValueDef(Params.I.Null.TRIGGER.Name, 0, 0, 0, Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 2, ParamUnidad.A), Params.I.Null.TRIGGER.Tol(), ParamUnidad.A);
                TestMeasureBase(inyectedCurrent, (s) =>
                {
                    return Tower.PowerSourceIII.ReadCurrent().L1;
                }, 0, 2, 500);

                Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

                var currentTriggerVerif = new AdjustValueDef(Params.I.Other("TRIGGER").Verificacion.Name, 0, 0, 0, 0, Params.I.Null.TRIGGER.Tol(), ParamUnidad.s);

                TestCalibracionBase(currentTriggerVerif,
                   () =>
                   {
                       return EkorDTC2.ReadInfoTriggers().currentTrigguer;

                   },
                   () =>
                   {
                       return inyectedCurrent.Value;
                   }, 1, 1, 500, "Corriente de verificación de disparo fuera de márgenes");


                var timeTriggerVerif = new AdjustValueDef(Params.TIME.Other("TRIGGER").Verificacion.Name, 0, 0, 0, 0, 5, ParamUnidad.s);

                TestCalibracionBase(timeTriggerVerif,
                   () =>
                   {
                       return EkorDTC2.ReadInfoTriggers().timeTrigger;
                   },
                   () =>
                   {
                       return adjustResult.Value;
                   }, 1, 1, 500, "Tiempo de verificación de disparo fuera de márgenes");

                return true;
            }, string.Empty, 2, 1000, 0, false, false);
        }

        public void TestAdjustVoltage(int delFirst, int initCount, int samples, int interval)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 70, ParamUnidad.V);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.TestPoint("V").Name, 0, ParamUnidad.Hz);

            Tower.ActiveVoltageCircuit(true, false, false, true);
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = voltage, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, frecuency, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var VRTeorica = CalculaVR(voltage);          
            
            var VRReal = new AdjustValueDef(Params.V.Null.Ajuste.TestPoint("REF").Name, 0, 0, 0, VRTeorica, Params.V.Other("R").PATRON.Tol(), ParamUnidad.V);
            TestMeasureBase(VRReal,
            (step) =>
            {
                var tester = metrix.ReadVoltage(MTX3293.measureTypes.AC); 
                return tester;
            }, 2, 5, 500);

            var inyectedCurrent = VRReal.Value / 10000D;              

            var gains = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_V.L1.Ajuste, ParamUnidad.Puntos));

            var adjustResult = EkorDTC2.AdjustVoltage(delFirst, initCount, samples, interval, VRReal.Value, VRTeorica,
                (value) =>
                {
                    gains.L1.Value = value.L1;
                    gains.L2.Value = value.L2;
                    gains.L3.Value = value.L3;

                    return !gains.HasMinMaxError();
                });

            gains.AddToResults(Resultado);

            if (!adjustResult.Item1)
                Error().UUT.AJUSTE.MARGENES("Error en el ajuste de Voltaje fuera de margenes").Throw();

            EkorDTC2.WriteVoltageFactorCalibration(adjustResult.Item2);

            Tower.ActiveVoltageCircuit(false, false, false, false);
            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestAdjustCurrents([Description("PRECISION || CORTOCIRCUITO")]EKOR_DTC2.Scale scale, int delFirst, int initCount, int samples, int interval)
        {
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scale.ToString()).Name, 0.075, ParamUnidad.A);
            double currentFondo = scale == EKOR_DTC2.Scale.PRECISION ? Configuracion.GetDouble("I_FONDO_PRECISION", 5, ParamUnidad.A) : Configuracion.GetDouble("I_FONDO_CC", 26, ParamUnidad.A);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 0, L3 = 0}, new TriLineValue { L1 = current, L2 = current, L3 = current }, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
            var inyectedCurrent = Tower.PowerSourceIII.ReadCurrent();

            var gains = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_I.L1.Ajuste.TestPoint(scale.ToString()), ParamUnidad.Puntos));

            var adjustResult = EkorDTC2.AdjustCurrents(scale, delFirst, initCount, samples, interval, inyectedCurrent, currentFondo,
                (value) =>
                {
                    gains.L1.Value = value.L1;
                    gains.L2.Value = value.L2;
                    gains.L3.Value = value.L3;
                   
                    return !gains.HasMinMaxError();
                });

            gains.AddToResults(Resultado);

            if (!adjustResult.Item1)
                Error().UUT.AJUSTE.MARGENES(string.Format("Error en el ajuste de corriente en escala {0} fuera de margenes", scale.ToString())).Throw();

            EkorDTC2.WriteCurrentFactorCalibration(scale, adjustResult.Item2);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestAdjustHomopolarsCurrents([Description("PRECISION || CORTOCIRCUITO")]EKOR_DTC2.Scale scale, int delFirst, int initCount, int samples, int interval)
        {
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scale.ToString() + "_HOMOPOLAR").Name, 0.5, ParamUnidad.A);          
            double currentFondo = scale == EKOR_DTC2.Scale.PRECISION ? Configuracion.GetDouble("I_FONDO_PRECISION", 5, ParamUnidad.A) : Configuracion.GetDouble("I_FONDO_CC", 26, ParamUnidad.A);
            int vueltasTrafoHomopolar = (int)Configuracion.GetDouble("VUELTAS_TRAFO_HOMOPOLAR", 1, ParamUnidad.SinUnidad);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = current, L2 = 0, L3 = 0 }, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
            var inyectedCurrent = Tower.PowerSourceIII.ReadCurrent().L1;

            var gains = new AdjustValueDef(Params.GAIN_I.Null.TestPoint("LH_" + scale.ToString()), ParamUnidad.Puntos);

            var adjustResult = EkorDTC2.AdjustHomopolarsCurrents(scale, vueltasTrafoHomopolar, delFirst, initCount, samples, interval, inyectedCurrent, currentFondo,
                (value) =>
                {
                    gains.Value = value.LH;
                   
                    return !HasError(gains);
                });

            gains.AddToResults(Resultado);

            if (!adjustResult.Item1)
                Error().UUT.AJUSTE.MARGENES(string.Format("Error en el ajuste de {0} fuera de margenes", gains.Name.Replace("_", " "))).Throw();

            EkorDTC2.WriteCurrentFactorCalibration(scale, adjustResult.Item2);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestVerification([Description("PRECISION || CORTOCIRCUITO")]EKOR_DTC2.Scale scale, int initCount, int samples, int timeInterval)
        {
            double current = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint(scale.ToString()).Name, 0.5, ParamUnidad.A);
            int vueltasTrafoHomopolar = (int)Configuracion.GetDouble("VUELTAS_TRAFO_HOMOPOLAR", 1, ParamUnidad.SinUnidad);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = current, L2 = current, L3 = current }, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var defs = new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, Params.I.Null.Verificacion.Tol(), Params.I.Null.Verificacion.Var(), ParamUnidad.A);

            var listDefs = new ListTriAdjustValueDef("ListTriDefsCurrentVerif");
            listDefs.Add(defs);
            var samplerConfig = new ConfigurationSampler { InitDelayTime = 200, SampleNumber = initCount, SampleMaxNumber = samples, DelayBetweenSamples = timeInterval };

            this.TestCalibrationWithVariance(listDefs,
               () =>
               {
                   var currents = EkorDTC2.ReadCurrentDisplayInAmpers(scale);

                   return new double[]
                   {
                        currents.LINES.L1 ,  currents.LINES.L2  , currents.LINES.L3
                   };
               },
               () =>
               {
                   var currentsMTE = Tower.PowerSourceIII.ReadCurrent();
                   return new double[]
                   {
                        currentsMTE.L1, currentsMTE.L2, currentsMTE.L3
                   };

               }, samplerConfig);

            double currentHomopolar = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint(scale.ToString() + "_HOMOPOLAR").Name, 2, ParamUnidad.A);
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = currentHomopolar, L2 = 0, L3 = 0 }, 0, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defsH = new AdjustValueDef(Params.I.LN.Verificacion.TestPoint(scale.ToString()).Name, 0, 0, 0, 0, Params.I.Null.Verificacion.Tol(), Params.I.Null.Verificacion.Var(), ParamUnidad.A);
            var listDefsH = new ListAdjustValueDef();
            listDefsH.Add(defsH);

            this.TestCalibrationWithVariance(listDefsH,
               () =>
               {
                   var currents = EkorDTC2.ReadCurrentDisplayInAmpers(scale);

                   return new double[] { currents.LN / vueltasTrafoHomopolar };            

               },
               () =>
               {
                   var currentsMTE = Tower.PowerSourceIII.ReadCurrent().L1;
                   return new double[] { currentsMTE };

               }, samplerConfig);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestVerificationVoltage(int initCount, int samples, int timeInterval)
        {
            double voltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 70, ParamUnidad.V);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.TestPoint("V").Name, 0, ParamUnidad.Hz);

            double Vref = CalculaVR(voltage);            

            Tower.ActiveVoltageCircuit(true, false, false, true);
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = voltage, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, frecuency, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var defsV = new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), Params.V.Null.Verificacion.Var(), ParamUnidad.V);
            var listDefs = new ListTriAdjustValueDef("ListTriDefsVoltageVerif");
            listDefs.Add(defsV);
            var samplerConfig = new ConfigurationSampler { InitDelayTime = 5000, SampleNumber = 5, SampleMaxNumber = 40, DelayBetweenSamples = 1000 };

            Delay(2000, "Estabilizando medida del equipo");

            this.TestCalibrationWithVariance(listDefs,
               () =>
               {
                   var voltages = EkorDTC2.ReadVoltageDisplayInVolts(Vref);

                   return new double[]
                   {
                    voltages.L1 ,  voltages.L2 , voltages.L3
                   };
               },
               () =>
               {
                   var voltageR = metrix.ReadVoltage(MTX3293.measureTypes.AC);

                   return new double[]
                   {
                    voltageR, voltageR, voltageR
                   };

               }, samplerConfig);
        }

        public void TestArranque()
        {
            EkorDTC2.Cirbus.PortCom = Comunicaciones.SerialPort;        
    
            Tower.LAMBDA.ApplyOffAndWaitStabilisation();
            Tower.IO.DO.Off(23);
            
            EkorDTC2.WriteStandbye();

            Delay(2500, "Apagando el equipo");

            var current = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint("ARRANQUE").Name, 0.070, ParamUnidad.A);         

            this.InParellel(
            () =>
            {
                Tower.MUX.Reset();
                Tower.MUX.Connect2((int)InputMuxEnum.IN5, Outputs.FreCH1, (int)InputMuxEnum.IN6, Outputs.FreCH2);

                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.8);

                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 2.0);
            },
            () =>
            {
                Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(24, current);
            });

            var adjustValue = new AdjustValueDef(Params.TIME.Other("ARRANQUE").Verificacion, ParamUnidad.s);

            var adjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 5000, HP53131A.Channels.CH1, () =>
                    {
                        Delay(500, "");
                        Tower.IO.DO.On(23);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw new Exception(string.Format("Error, el equipo no ha arrancado"));
                }
            }, 0, 1, 1000);
        }

        public void TestDespierta()
        {
            var IMAGES_PATH = this.PathCameraImages + @"\EKOR_DTC2\ESC_BUTTON_ACABADOS.jpg";

            EkorDTC2.WriteStandbye();
            Tower.LAMBDA.ApplyOff();

            Delay(4000, "Esperando que el equipo entre en modo Standbye");

            if (utilNuevo)
            {
                Tower.IO.DO.On(PISTON_DOMO);
                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[DETECTION_DOMO_UP];
                }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());
                Tower.IO.DO.PulseOn(300, OUT_ESC_BUTTON_ACTUATOR);
            }
            else
                Shell.MsgBox("Pulse la tecla ESC para que el equipo salga del modo Standbye", "Test Despierta");

            SamplerWithCancel((p) =>
            {
                EkorDTC2.ReadDateTime();

                return true;

            }, "No se ha podido comunicar con el equipo ", 50, 200, 1000, false, false,
            (e) => Error().UUT.HARDWARE.NO_COMUNICA("DESPUES MODO STANDBYE").Throw());


            Tower.IO.DO.Off(PISTON_DOMO);
            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            Resultado.Set("COMS_AFTER_STANDBYE", "OK", ParamUnidad.SinUnidad);
        }

        public void TestCostumize()
        {
            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V), 0.5);

            EkorDTC2.WriteRemoveCounter();

            if (model == EKOR_DTC2.EkorModelType.RPG_RPT)
            {
                EkorDTC2.WriteBastidor(TestInfo.NumBastidor.Value.ToString("D8"));
                Delay(4000, "");
            }

            Tower.LAMBDA.ApplyOffAndWaitStabilisation();
            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V), 0.5);

            SamplerWithCancel((p) =>
            {
                EkorDTC2.ReadFirmware();
                return true;
            }, "Error. No se ha podido cominunicar con el equipo después del reset", 3, 500, 1000, true, true,
            (e) => Error().UUT.HARDWARE.NO_COMUNICA("DESPUES DEL RESET").Throw());
            
            DateTime ekorDateTime = EkorDTC2.ReadDateTime();

            double timeDiff = (DateTime.Now.Subtract(ekorDateTime)).TotalSeconds;

            var secondsLimit = Margenes.GetDouble(Params.TIME.Null.Verificacion.TestPoint("OFFSET_MAX").Name, 30, ParamUnidad.s);

            Assert.AreLesser(Params.TIME.Null.Offset.Name, Math.Abs(timeDiff), secondsLimit, Error().UUT.CONFIGURACION.MARGENES("Error. El reloj del equipo ha perdido mas de 30 segundos"), ParamUnidad.s);

            if (model == EKOR_DTC2.EkorModelType.RPG_RPT)
            {
                var bastidorEkor = EkorDTC2.ReadBastidor();
                Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, Convert.ToInt32(bastidorEkor), (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, el bastidor leído del equipo no coincide con el esperado"), ParamUnidad.SinUnidad);
            }

            var userSettingsToWrite = Parametrizacion.GetString("ADJUST_USER_FAB", "FF FF 00 00 0005 00 00 0000 0000 00 00 00 00 00 00 0000", ParamUnidad.SinUnidad);
            EkorDTC2.WriteUserSettings(userSettingsToWrite);
            Delay(2000, "Configurando ajustes de fabrica del usuario");

            var HasLogicTable = Configuracion.GetString("HAS_LOGIC_TABLE", "NO", ParamUnidad.SinUnidad);
            if (!HasLogicTable.Contains("NO"))
            {
                if(HasLogicTable.Contains("1020"))
                {
                    EkorDTC2.WriteInstallerSettings("0102 270F 04B0 0438 1964 15 00 00 01 06 0000 0096 0001"); //cambiamos modelo a 15 = 1020 para que acepte las tablas lógicas
                    Delay(2000, "Configurando ajustes para enviar tablas lógicas");
                }

                SendTableLogic(HasLogicTable);

                Delay(2500, "Espera reset");

                CambiarProtocoloPrTecaldo();

            }else
            {
                var installerSettingsToWrite = Parametrizacion.GetString("ADJUST_INSTALLER_FAB", "0002 270F 04b0 0438 1964 00 00 00 00 00 0000", ParamUnidad.SinUnidad);
                EkorDTC2.WriteInstallerSettings(installerSettingsToWrite);
                Delay(2000, "Configurando ajustes de fabrica del instalador");
            }
        }

        public void CambiarProtocoloPrTecaldo(bool modbusToCirbus = false)
        {
            Tower.IO.DO.PulseOn(500, 7);
            Delay(2000, "");

            Tower.IO.DO.PulseOn(500, 7);
            Delay(2000, "");

            Tower.IO.DO.PulseOn(500, 12);
            Delay(500, "");

            Tower.IO.DO.PulseOn(500, 12);
            Delay(500, "");

            Tower.IO.DO.PulseOn(500, 12);
            Delay(500, "");

            Tower.IO.DO.PulseOn(500, 12);
            Delay(500, "");

            Tower.IO.DO.PulseOn(500, 12);
            Delay(500, "");

            Tower.IO.DO.PulseOn(500, 10);
            Delay(2000, "");

            if (modbusToCirbus)
            {
                usb5856.DO.PulseOn(500, 9);
                Delay(2000, "");
            }
            else
            {
                Tower.IO.DO.PulseOn(500, 12);
                Delay(2000, "");
            }

            Tower.IO.DO.PulseOn(500, 7);
            Delay(1000, "");

            Tower.IO.DO.PulseOn(500, 7);
            Delay(2000, "");
        }

        private void SendTableLogic(string logicalTable)
        {
            EkorDTC2.WriteInitializePointer();

            if (logicalTable.Contains("1020"))
                EkorDTC2.WriteProgrammingLogicTable(EKOR_DTC2.TiposTL.TF1020);
            else
                EkorDTC2.WriteProgrammingLogicTable(EKOR_DTC2.TiposTL.TF1000);

            EkorDTC2.RecordingLogicTable();
        }

        private void TestDesviationClock()
        {
            SamplerWithCancel((p) =>
            {
                var timeEkor = EkorDTC2.ReadDateTime();

                logger.InfoFormat("EKOR_PLUS FECHA:{0} HORA:{1}", timeEkor.ToShortDateString(), timeEkor.ToLongTimeString());
                logger.InfoFormat("PC FECHA:{0} HORA:{1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());

                double timeDiff = (DateTime.Now.Subtract(timeEkor)).TotalSeconds;

                logger.InfoFormat("DERIVA RELOJ:{0}", timeDiff);

                Assert.AreBetween(Params.TIME.Null.Offset.Name, timeDiff, Params.TIME.Null.Offset.Min(), Params.TIME.Null.Offset.Max(), Error().UUT.CONFIGURACION.MARGENES("Error. El equipo tiene una deriva de reloj fuera de margenes"), ParamUnidad.s);

                return true;
            }, string.Empty, 2, 1000, 200, false, false);
        }
        
        public void TestFinish()
        {
            if (EkorDTC2 != null)
                EkorDTC2.Dispose();

            if (metrix != null)
                metrix.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();

                if (utilNuevo)
                    TestDeviceDisconnection();

                if (Tower.IO != null)
                    Tower.IO.DO.OffRange(0, Tower.IO.DO.Count - 1);

                Tower.Dispose();
            }
        }

        //***************************** ACTIVACION DESACTIVACION PISTONES UTILLAJE NUEVO ********************************

        public void TestDeviceConnection()
        {
            var timeWait = 500;
            var inputsOuputs = new Dictionary<Pistones, byte[]>()
            {
                {Pistones.FIJACION_CARRO, new byte[]{ 21 }},
                {Pistones.SIN_PASARELA, new byte[] { 2 } },
                {Pistones.PISTON_PUNTAS, new byte[] { 25, 26 } },
                {Pistones.PISTON_RS232, new byte[] { 28 } },
                {Pistones.PISTON_RS485, new byte[] { 34 } },
                {Pistones.PISTON_TRIGGER, new byte[] { 35 } }
            };

            usb5856.DO.OffRange(0, usb5856.DO.Count - 1);

            Tower.IO.DO.OnWait(500, EV_GENERAL, EV_GENERAL_AMPLIACION);

            Tower.IO.DO.Off(PISTON_DOMO);
            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            foreach (KeyValuePair<Pistones, byte[]> inOut in inputsOuputs)
                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OnWait(timeWait, (int)inOut.Key);

                    foreach (int input in inOut.Value)
                        if (!Tower.IO.DI[input])
                            throw new Exception(string.Format("Error no se ha detectado el piston {0} en su final de carrera", inOut.Key.ToString()));
                    return true;
                }, "", cancelOnException: false, throwException: false, exception: (e) => Error().HARDWARE.UTILLAJE.PNEUMATICA(string.Format("No se ha detectado el piston {0} en su final de carrera", inOut.Key.ToString())).Throw());
        }

        public void TestDeviceDisconnection()
        {
            var timeWait = 1000;
            var inputsOuputs = new Dictionary<Pistones, int[]>()
            {
                {Pistones.FIJACION_CARRO, new int[]{ 21 }},    // Detección puntas de disparo DTC2 / EKORPLUS
                {Pistones.PISTON_RS485, new int[]{ 34 }},
                {Pistones.PISTON_TRIGGER, new int[]{ 35 }},
                {Pistones.SIN_PASARELA, new int[]{ 2 }},
                {Pistones.PISTON_PUNTAS, new int[]{ 25,26 }},
                {Pistones.PISTON_RS232, new int[]{ 28 }},                
            };

            Tower.IO.DO.OffWait(1000, OUT_SET_BUTTON_ACTUATOR, OUT_ESC_BUTTON_ACTUATOR, OUT_RIGHT_BUTTON_ACTUATOR, OUT_LEFT_BUTTON_ACTUATOR, OUT_DOWN_BUTTON_ACTUATOR);
            usb5856.DO.OffWait(1000, OUT_UP_BUTTON_ACTUATOR);

            Tower.IO.DO.Off(PISTON_DOMO);
            SamplerWithCancelWhitOutCancelToken(
            () =>
            {
                return Tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            foreach (KeyValuePair<Pistones, int[]> inOut in inputsOuputs)
                SamplerWithCancelWhitOutCancelToken(() =>
                {
                    Tower.IO.DO.OffWait(timeWait, (int)inOut.Key);

                    foreach (int input in inOut.Value)
                        if (Tower.IO.DI[input])
                            throw new Exception(string.Format("Error no se han desactivado el piston {0} de la IN{1} ", inOut.Key.ToString(), input));

                    return true;
                }, "", initialDelay: 200, cancelOnException: false, throwException: false, exception: (e) => Error().HARDWARE.UTILLAJE.PNEUMATICA(string.Format("No se ha detectado el piston {0} en su final de carrera", inOut.Key.ToString())).Throw());
        }

        private double CalculaVR(double GeneratedVoltage, double frecuency = 50)
        {
            var Rutil = Configuracion.GetDouble(Params.RES.Null.TestPoint("UTIL").Name, 10000, ParamUnidad.Ohms);          
            var RCondensadorUtil = 1 / (2.0 * Math.PI * frecuency * Configuracion.GetDouble("CAPACITANCE_UTIL", 0.00000001, ParamUnidad.F));
            var RElectronicaEquipo = 1 / (2.0 * Math.PI * frecuency * Configuracion.GetDouble("CAPACITANCE_UUT", 0.000001, ParamUnidad.F));

            var Ztotal = Rutil + (3 * RElectronicaEquipo) + RCondensadorUtil;

            var Itotal = GeneratedVoltage / Ztotal;

            return Itotal * Rutil;   //VR

        }

  
    }
}
