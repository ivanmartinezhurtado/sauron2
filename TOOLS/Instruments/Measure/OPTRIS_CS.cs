﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using System;
using System.Globalization;
using System.IO.Ports;
using System.Threading;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.01)]
    public class OPTRIS_CS : MeasureBase<OPTRIS_CS>, IDisposable
    {
        private string INITIALIZATION = "00";
        private string QUERY_TEMPERATURE = "01";
        private string UNLOCK_DEVICE = "3957414954";

        public enum PrivateCommands
        {
            INITIALIZATION = Commands.USER_DEFINED_COMMANDS + 1,
            QUERY_TEMPERATURE= Commands.USER_DEFINED_COMMANDS + 2,
            UNLOCK_DEVICE = Commands.USER_DEFINED_COMMANDS + 3,
        };

        private SerialPort sp;
        private byte _port = 5;

        public OPTRIS_CS()
        {
            InitializeComponent();

            protocol = new ASCIIProtocol();
            protocol.CaracterFinRx = string.Empty;
            protocol.CaracterFinTx = string.Empty;
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
        }

        public OPTRIS_CS(byte portName)
        {
            InitializeComponent();

            protocol = new ASCIIProtocol();
            protocol.CaracterFinRx = string.Empty;
            protocol.CaracterFinTx = string.Empty;
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
            PortCom = portName;
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }
  
        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = sp.ReadTimeout = 5000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
            sp.DtrEnable = true;
            
        }

        public void Initialization(bool? throwException = null)
        {
            string stringReaded = string.Empty;
            var samplerResult = Sampler.Run(5, 1000, (step) =>
                {
                    SendCommand(new Command<PrivateCommands>(PrivateCommands.INITIALIZATION, true));
                    Thread.Sleep(50);
                    SendCommand(new Command<PrivateCommands>(PrivateCommands.UNLOCK_DEVICE, true));

                    int reint = 0;
                    do
                    {
                        Thread.Sleep(50);                
                        stringReaded = sp.ReadExisting();
                        if (stringReaded != "WAITING")
                        {
                            sp.DtrEnable = false;
                            Thread.Sleep(50);
                            sp.DtrEnable = true;
                            SendCommand(new Command<PrivateCommands>(PrivateCommands.INITIALIZATION, true));
                            Thread.Sleep(50);
                            SendCommand(new Command<PrivateCommands>(PrivateCommands.UNLOCK_DEVICE, true));
                        }
                        _logger.InfoFormat("step:{0} Reint:{1}  RX:{2} ", step.Step, reint, stringReaded);
                        reint++;
                    } while (stringReaded.Trim() != "WAITING".Trim() && reint < 5);

                    if (reint >= 5)
                    {
                        sp.Close();
                        sp.DiscardInBuffer();
                        sp.DiscardOutBuffer();
                    }
                    
                    step.Cancel = stringReaded.Trim() == "WAITING".Trim();
                });

            if (!samplerResult.Canceled)
            {
                sp.Close();
                throw new Exception("Error de comunicaciones con el termometro OPTRIS, no se ha recibido la trama esperada");
            }
        }

        public double ReadTemperature(bool? throwException = null)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.QUERY_TEMPERATURE, true));

            var MSByte = sp.ReadByte();
            var LSByte = sp.ReadByte();
            var temp =  (MSByte * 256 + LSByte - 1000) / 10D;
            _logger.InfoFormat("Temperatura: {0} ºC", temp);
            return temp;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            HexStringMessage message, response;
            CultureInfo enUS = new CultureInfo("en-US");

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)PrivateCommands.INITIALIZATION:
                    message = HexStringMessage.Create(enUS, INITIALIZATION, "");
                    message.HasResponse = false;
                    break;

                case (int)PrivateCommands.UNLOCK_DEVICE:
                    message = HexStringMessage.Create(enUS, UNLOCK_DEVICE, "");
                    message.HasResponse = false;
                    break;

                case (int)PrivateCommands.QUERY_TEMPERATURE:
                    message = HexStringMessage.Create(enUS, QUERY_TEMPERATURE, "");
                    message.HasResponse = false;
                    break;
                default:
                    throw new NotSupportedException();
            }

            if (message.HasResponse)
                response = transport.SendMessage<HexStringMessage>(message);
            else
                transport.SendMessage<HexStringMessage>(message);
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
    }
}
