﻿using Dezac.Core.Utility;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{

    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(SaveResultsProcessActionDescription))]
    

    public class SaveResultsProcessAction : ActionBase
    {
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var numbersOfProcess = cacheSvc.Get<string>("NUMBERS_OF_PROCESS");

            try
            {
                if (numbersOfProcess == null)
                    return;

                for (int i = 0; i < Convert.ToInt32(numbersOfProcess); i++)
                    Save();
            }
            catch(Exception e)
            {
                SendMail.Send(string.Format("LASER SAVE RESULTS {0}", testBase.Model.NumProducto),
                    string.Format("Message : {0}\n StackTrace : {1}\n Source : {2}\n InnerException : {3}", e.Message, e.StackTrace, e.Source, e.InnerException),
                    "cjordan@dezac.com");
            }
        }

        private void Save()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            if (data == null)
                return;

            if (context.ExecutionResult == TestExecutionResult.Aborted)
                return;

            if (context.GetSteps(StepExecutionResult.Failed).Count() == 1)
            {
                if (context.GetSteps(StepExecutionResult.Failed).FirstOrDefault().GetErrorMessage().Contains("PARALELISMO CANCELADO"))
                    return;

                if (context.GetSteps(StepExecutionResult.Failed).FirstOrDefault().GetErrorMessage() == "sin ninguna reparación!")
                    return;
            }

            //int numBastidor = data.TestInfo.NumBastidor.Value;
            string resultado = context.ExecutionResult == TestExecutionResult.Completed ? "O" : "E";
           

            using (DezacService svc = new DezacService())
            {
                if (!data.TestInfo.NumFabricacion.HasValue)
                    data.TestInfo.NumFabricacion = null;

                var producto = svc.GetProducto(data.NumProducto, data.Version);
                var productoFase = svc.GetProductoFase(producto, data.IdFase);

                var codigoGrupo = svc.GetCodigoGrupo(data.IdFase);

                ORDEN orden = svc.GetOrden(data.NumOrden.Value);

                if (orden == null)
                    return;

                var numRegistro = svc.GetNumRegistroOrdenProcesoFase();
                data.TestInfo.NumTestFase = numRegistro;

                ORDENPROCESOFASE procesofase = new ORDENPROCESOFASE
                {
                    NUMREGISTRO = numRegistro,
                    ORDEN = orden,
                    NUMORDEN = orden.NUMORDEN,
                    IDFASE = data.IdFase,
                    SECUENCIA = productoFase.SECUENCIA,
                    IDOPERARIO = data.IdOperario,
                    FECHA = data.TestInfo.StartTime,
                    TIEMPO = Convert.ToInt32(data.TestInfo.Elapsed.TotalSeconds),
                    RESULTADO = resultado,
                    CODIGOGRUPO = codigoGrupo
                };

                svc.GrabarDatos(procesofase);
            }
        }
    }

    public class SaveResultsProcessActionDescription : ActionDescription
    {
        public override string Name { get { return "SaveResultsProcess"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }
}
