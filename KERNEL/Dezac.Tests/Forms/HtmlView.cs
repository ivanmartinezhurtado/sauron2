﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.Forms
{
    public partial class HtmlView : UserControl
    {
        public HtmlView()
        {
            InitializeComponent();
        }

        public HtmlView(string url)
            : this()
        {
            Url = url;
        }

        public string Url 
        {
            get { return browser.Url.AbsoluteUri; }
            set { browser.Navigate(value); }
        }
    }
}
