﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionHardwareLevel0
    {
        private TestExceptionHardwareLevel1 psl;

        public string[] Category { get { return psl.Category; } set { psl.Category = value; } }

        public TestExceptionHardwareLevel1 TORRE
        {
            get {  psl.SubCategory = ConstantsExceptions.HardwareLevel0.TORRE;  return psl; }
        }

        public TestExceptionHardwareLevel1 MANGUERA
        {
            get {  psl.SubCategory = ConstantsExceptions.HardwareLevel0.MANGUERA; return psl; }
        }

        public TestExceptionHardwareLevel1 UTILLAJE
        {
            get {  psl.SubCategory = ConstantsExceptions.HardwareLevel0.UTILLAJE;  return psl;  }
        }

        public TestExceptionHardwareLevel1 INSTRUMENTOS
        {
            get {  psl.SubCategory = ConstantsExceptions.HardwareLevel0.INSTRUMENTOS;   return psl;  }
        }

        public TestExceptionHardwareLevel1 PC
        {
            get { psl.SubCategory = ConstantsExceptions.HardwareLevel0.PC; return psl; }
        }

        public TestExceptionHardwareLevel0()
        {
            psl = new TestExceptionHardwareLevel1();
        }
    }
}
