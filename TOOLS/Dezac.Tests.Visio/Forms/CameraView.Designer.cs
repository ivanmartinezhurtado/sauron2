﻿namespace Dezac.Tests.Visio.Forms
{
    partial class CameraIDSView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cameraImage = new System.Windows.Forms.PictureBox();
            this.lblCamera = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.picPatron = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPatron)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.cameraImage, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCamera, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblImage, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.picPatron, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1033, 556);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // cameraImage
            // 
            this.cameraImage.BackColor = System.Drawing.Color.AliceBlue;
            this.cameraImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraImage.Location = new System.Drawing.Point(3, 26);
            this.cameraImage.Name = "cameraImage";
            this.cameraImage.Size = new System.Drawing.Size(510, 527);
            this.cameraImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cameraImage.TabIndex = 5;
            this.cameraImage.TabStop = false;
            this.cameraImage.DoubleClick += new System.EventHandler(this.cameraImage_DoubleClick);
            // 
            // lblCamera
            // 
            this.lblCamera.BackColor = System.Drawing.Color.AliceBlue;
            this.lblCamera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.ForeColor = System.Drawing.Color.Navy;
            this.lblCamera.Location = new System.Drawing.Point(3, 0);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(510, 23);
            this.lblCamera.TabIndex = 4;
            this.lblCamera.Text = "CAMARA";
            this.lblCamera.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblImage
            // 
            this.lblImage.BackColor = System.Drawing.Color.AliceBlue;
            this.lblImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImage.ForeColor = System.Drawing.Color.Navy;
            this.lblImage.Location = new System.Drawing.Point(519, 0);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(511, 23);
            this.lblImage.TabIndex = 6;
            this.lblImage.Text = "PATRÓN";
            this.lblImage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picPatron
            // 
            this.picPatron.BackColor = System.Drawing.Color.AliceBlue;
            this.picPatron.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picPatron.Location = new System.Drawing.Point(519, 26);
            this.picPatron.Name = "picPatron";
            this.picPatron.Size = new System.Drawing.Size(511, 527);
            this.picPatron.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPatron.TabIndex = 7;
            this.picPatron.TabStop = false;
            // 
            // CameraView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "CameraView";
            this.Size = new System.Drawing.Size(1033, 556);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cameraImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPatron)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox cameraImage;
        private System.Windows.Forms.Label lblCamera;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.PictureBox picPatron;

    }
}
