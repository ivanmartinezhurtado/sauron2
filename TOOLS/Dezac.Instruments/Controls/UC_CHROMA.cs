﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.PowerSource;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(ChromaDescription))]
    public partial class UC_CHROMA : UserControl, IDisposable
    {
        private static readonly string TAG = "CHROMA";

        private double Voltage;
        private double freq;
        private double currentLimit;

        private SynchronizationContext sc;

        private bool timerEnabled;
        private bool modeRead;

        private Chroma chroma;

        public Type InstrumentType
        {
            get
            {
                return typeof(Chroma);
            }
        }

        public Chroma Instrument
        {
            get
            {
                if (chroma == null)
                    chroma = new Chroma();

                return chroma;
            }
            set
            {
                if (chroma != null)
                {
                    chroma.Dispose();
                    chroma = null;
                }

                chroma = value;
            }
        }

        public UC_CHROMA()
        {
            InitializeComponent();         
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;

            string[] Adress = new string[] { "ADDR1", "ADDR2", "ADDR3" };
            SetMode(false);
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            if (ValidatePresets())
                ActivaChroma();
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            try
            {
                ToggleButtonsState((Button)sender);

                var task = Task.Factory.StartNew((Action)(() => { Instrument.ApplyOffAndWaitStabilisation(); }));
                LifeMethod("Apagando", task);
            }
            catch (Exception ex)
            {
                Logger.Error("Error al apagar el CHROMA", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = modeRead ? Color.DimGray : Color.LightGreen;
            SetMode(!modeRead);
        }

        private void ActivaChroma()
        {
            try
            {
                ToggleButtonsState(btnOn);

                var task = Task.Factory.StartNew((Action)(() => { Instrument.ApplyPresetsAndWaitStabilisation(Voltage, currentLimit, freq); }));

                LifeMethod("Encendiendo", task);              
            }
            catch (Exception ex)
            {
                Logger.Error("Error al encender el CHROMA", ex);
            }
            finally
            {
                ToggleButtonsState(btnOn);
            }
        }

        private bool ValidatePresets()
        {
            try
            {
                Instrument.GetIdentification();
                return ValidateVoltageFreq();
            } catch(Exception ex)
            {
                Logger.Error("Error comunicando con la CHROMA", ex);
            }
            
            return false;
        }

        private bool ValidateVoltageFreq()
        {

            Voltage = GetAsDouble(txtV1);
            if (Voltage > 400)
            {
                errorProvider1.SetError(txtV1, "Voltage must lower 400V");
                return false;
            }
            if (Voltage > 150)
                Instrument.SetVoltageRange(Chroma.VoltageRangeEnum.HIGH);
            freq = GetAsDouble(txtFreq);
            if (freq > 100)
            {
                errorProvider1.SetError(txtFreq, "Frequqncy must lower 100Hz");
                return false;
            }
            return true;
        }

        private void SetMode(bool read)
        {
            modeRead = read;

            if (modeRead && !timerEnabled)
                EnableTimer();
            else
                timerEnabled = false;
        }

        private void EnableTimer()
        {
            timerEnabled = true;
            Task.Factory.StartNew(OnGraphTask);
        }

        private void OnGraphTask()
        {
            while (timerEnabled)
            {
                try
                {
                    UpdateGraphs();
                }
                catch { }
                Thread.Sleep(1000);
            }
        }

        private void UpdateGraphs()
        {
            double current = 0;
            double voltage = 0;

            if (modeRead)
                current = Instrument.ReadCurrent();

            if (modeRead)
                voltage = Instrument.ReadVoltage();
            
            sc.Post(_ =>
            {
                if (current != 0)
                    DataPointHub.Add(this, "I1", current, TAG);

                if (voltage != 0)                                   
                    DataPointHub.Add(this, "V1", voltage, TAG);                              
            }, null);
        }

        private double GetAsDouble(MaskedTextBox txt)
        {
            if (string.IsNullOrEmpty(txt.Text))
                return 0;

            return Convert.ToDouble(txt.Text);
        }

        private void ToggleButtonsState(Button btn)
        {
            if (btn.Equals(btnOn))
                btnOn.Enabled = !btnOn.Enabled;
            if (btn.Equals(btnOff))
                btnOff.Enabled = !btnOff.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value+=10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))       
                components.Dispose();
            
            if(Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }

    public class ChromaDescription : ControlDescription
    {
        public override string Name { get { return "CHROMA"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.PowerSourceACDC; } }
        public override Image IconBig { get { return Properties.Resources._6404_2; } }
        public override Type SubcomponentType { get { return typeof(Chroma); } }
    }
}
