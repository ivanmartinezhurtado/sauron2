﻿using System;

namespace Instruments.Utility
{
    public class InstrumentsVersionAttribute : Attribute
    {
        public double Version { get; set; }

        public InstrumentsVersionAttribute()
        {

        }

        public InstrumentsVersionAttribute(double version)
        {
            Version = version;
        }
    }
}
