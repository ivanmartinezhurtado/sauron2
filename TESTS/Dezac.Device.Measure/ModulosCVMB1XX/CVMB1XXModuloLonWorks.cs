﻿namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class CVMB1XXModuloLonWorks : DeviceBase
    {
        public CVMB1XXModuloLonWorks()
        {
        }

        public CVMB1XXModuloLonWorks(int port)
        {
            SetPort(port);          
        }

        public void SetPort(int port, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 115200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }
        
        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public void WriteServicePinFlag()
        {
            Modbus.Write((ushort)Registers.SERVICE_PIN_FLAG, (ushort)1);
        }

        public long ReadNeuronID()
        {
            return Modbus.Read<long>((ushort)Registers.NEURON_ID);
        }

        public enum Registers
        {
            SERVICE_PIN_FLAG = 0xE678,
            NEURON_ID = 0xE67A
        }
    }
}
