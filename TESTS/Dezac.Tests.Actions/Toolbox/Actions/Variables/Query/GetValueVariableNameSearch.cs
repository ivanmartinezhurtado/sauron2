﻿using Dezac.Tests.Actions.Views;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(GetValueVariableNameSearchDescription))]
    public class GetValueVariableNameSearch : ActionBase
    {
        [Description("Nombre de la variable a buscar")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInput { get; set; }

        [Description("El tipo de búsqueda es el siguiente: empieza con ... o finaliza con ...")]
        [Category("3. Configuration")]
        [DefaultValue(TipeSearch.StartWith)]
        public TipeSearch StartsWith_EndWith { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if(DataOutput.IsKeyNullOrEmpty)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataOutput").Throw();

            if (string.IsNullOrEmpty(DataInput))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataInput").Throw();

            if (Variables != null)
            {
                var i = 1;
                foreach (var item in Variables.VariableList)
                {
                    if (StartsWith_EndWith == TipeSearch.StartWith)
                    {
                        if (item.Key.StartsWith(DataInput))
                        {
                            Logger.InfoFormat("Variable encontrada: {0} =  {1}", item.Key, item.Value);
                            var value = item.Value;
                            DataOutput.Value = value;
                        }
                    }
                    else
                    {
                        if (item.Key.EndsWith(DataInput))
                        {
                            Logger.InfoFormat("Variable encontrada: {0} =  {1}", item.Key, item.Value);
                            var value = item.Value;
                            DataOutput.Value = value;
                        }
                    }
                }
            }
        }

        public enum TipeSearch
        {
            StartWith,
            EndWith
        }

    }


    public class GetValueVariableNameSearchDescription : ActionDescription
    {
        public override string Description { get { return "Action para obtener el valor de una variable que empieza o acaba por un string ...";  } }
        public override string Name { get { return "GetValueVariableNameSearch"; }}
        public override string Category { get { return "Actions/Variables/Query"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
