﻿using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.UserControls;
using System.Windows.Forms;

namespace Dezac.Tests.Actions.Views
{
    public partial class LoginView : DialogViewBase
    {
        private string roles = "";

        public LoginView()
        {
            InitializeComponent();
        }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            string userName = UserName;
            string password = Password;

            if (string.IsNullOrEmpty(userName))
                return false;

            if (string.IsNullOrEmpty(password))
                return false;

            using (var svc = new DezacService())
            {
                Empleado = svc.Login(userName, password);
                roles = svc.GetUserParam("WebTSD", UserName, "Role");
            }

            if (Empleado == null)
            {
                lblError.Text = "Usuario inexistente o password incorrecto";
                return false;
            }
            if (roles == null)
            {
                lblError.Text = "Usuario sin roles asignados";
                return false;
            }

            lblError.Text = string.Empty;
            return true;
        }

        public string Roles
        {
            get { return roles; }
        }

        public string UserName
        {
            get { return txtUserName.Text; }
            set { txtUserName.Text = value; }
        }

        public string Password
        {
            get { return txtPassword.Text; }
            set { txtPassword.Text = value; }
        }

        public VW_EMPLEADO Empleado { get; set; }
    }
}
