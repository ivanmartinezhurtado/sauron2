﻿using Dezac.Device;
using Instruments.Utility;
using log4net;
using System;

namespace Instruments.IO
{
    [InstrumentsVersion(1.00)]
    public class MIDA20 : IOBase
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(MIDA20));
   
        private ChannelConfiguration config;

        private static readonly object syncObject = new object();

        public MidabusDeviceSerialPort Midabus { get; internal set; }

        public MIDA20()
            : base(logger)
        {
            SetPort();
            Config().SetChannels(2, 1);
            Layout();
        }

        public enum ChannelsEnum
        {
            SLOT1 = 0,
            SLOT2 = 1,
        }

        public ChannelConfiguration Config()
        {
            config = config ?? new ChannelConfiguration(this);

            return config;
        }

        public void SetPort(int port = 4, byte perifericNumber = 1, int baudRate = 9600)
        {
            if (Midabus == null)
                Midabus = new MidabusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, 7, logger);
            else
                Midabus.PortCom = port;

            Midabus.PerifericNumber = perifericNumber;
        }

        public void Layout(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                ApplyDefaultLayout();
        }

        public void ApplyDefaultLayout()
        {
            Config()
               .SetChannelAsOutput((int)ChannelsEnum.SLOT1)
               .SetChannelAsInput((int)ChannelsEnum.SLOT2)
               .Apply();
        }

        protected override T InternalRead<T>(IOType type, int num)
        {
            var port = GetPort(type);
            port.Data = Convert.ToByte(num);

            bool result = false;

            lock (syncObject)
            {
                result = InternalReadPort(port);
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        protected override void InternalWrite<T>(int num, T value)
        {
            var port = GetPort(IOType.Output);
            lock (syncObject)
            {
                port.Data = Convert.ToByte(num);
                port.Value = (bool)Convert.ChangeType(value, typeof(T));
                InternalWritePort(port);
            }
        }

        protected bool InternalReadPort(IOPort port)
        {
            var GetSlot = GetAdressSlote(port);
          
            return ReadIOSlote(GetSlot, port.Data);
        }

        protected void InternalWritePort(IOPort port)
        {
            var value = 100 + port.Data;
            var GetSlot = GetAdressSlote(port);
            WriteIOSlote(GetSlot, value, port.Value);
        }

        private void WriteIOSlote(ushort slot, int value, bool state)
        {
            var mesage = string.Format("{0}{1}{2}", slot.ToString(), value.ToString("X4"), state ?"FF" : "00");
            Midabus.Write(mesage, "00");
        }

        private bool ReadIOSlote(ushort slot, int value)
        {
            var mesage = string.Format("{0}{1}{2}", slot.ToString(), value.ToString("X4"), "01");
            var result = Midabus.Read(mesage, "960004").Replace("960004", "");
            return result == "FF00";
        }

        private ushort GetAdressSlote(IOPort port)
        {
            var register = port.IOType == IOType.Output ? 96 : 16;
            return (ushort)register;
        }

        internal IOPort GetPort(IOType type)
        {
            foreach(var channel in config.Channels)
                foreach (IOPort port in channel.Ports)
                    if (port.IOType == type)
                        return port;

            throw new IOException(IOException.Codes.INVALID_PORT);
        }

        public override void Dispose()
        {
            Midabus.Dispose();
            base.Dispose();
        }
    }
}
