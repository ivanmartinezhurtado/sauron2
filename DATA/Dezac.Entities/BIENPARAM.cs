namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class BIENPARAM
    {
        public int NUMPARAM { get; set; }
        public int NUMBIEN { get; set; }
        public string IDPARAM { get; set; }
        public string PARAM { get; set; }
    
        public virtual BIEN BIEN { get; set; }
    }
}
