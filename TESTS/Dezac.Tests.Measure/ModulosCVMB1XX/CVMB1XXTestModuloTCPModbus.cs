﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Instruments.Router;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.08)]
    public class CVMB1XXTestModuloTCPModbus : CVMB1XXTestModulosGenerico
    {
        private CVMB1XXModuloModBusTCP mod;
        private Router router;

        public void TestInitialization(byte portCVM)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            CVMDummy = new CVMB1XX(portCVM);
            CVMDummy.Modbus.PerifericNumber = Comunicaciones.Periferico;
            AddInstanceVar(CVMDummy, "UUT");

            router = Router.Default();
        }

        public void TestComunicationsTCP()
        {
            var macReading = CVMDummy.ReadMac();

            string macArray = "";

            if (macReading.ToString().Equals(Configuracion.GetString("MAC_DEFECTO", "14:A6:2C:00:00:00", ParamUnidad.SinUnidad)) || macReading.ToString().Equals("00:00:00:00:00:00"))
            {
                var macs = this.GetMacServiceDezac(1);

                Assert.IsNotNullOrEmpty(macs, Error().SOFTWARE.WEB_SERVICE.CARGA_SERVICIO("No se han obtenido MAC del Servicio Web"));

                macArray = macs.FirstOrDefault().Replace("-", "");
            }

            CVMDummy.WriteDHCPFlag(true);

            Delay(5000, "espera");

            SamplerWithCancel((p) =>
            {
                var tcpConfiguration = CVMDummy.ReadTCPConfiguration();

                if (tcpConfiguration.IP == null)
                    throw new Exception("No se ha recibido una IP para configuracion DHCP del equipo");

                return true;

            }, "No se le ha asignado una IP sel Servidor DHCP", 50, 1000);
      
            var clients = new List<ClientEntry>();

            SamplerWithCancel((p) =>
            {
                clients = router.GetConnectedClientsFromWeb(true);

                return !clients.Count.Equals(0);
            }, "No se han encontrado clientes DHCP conectados al router", 50, 1000);

            Delay(1000, "Espera conexion");

            var IpCvmDummy = clients.Where((p) => { return p.MAC.ToUpper() == macReading.ToString(); }).FirstOrDefault();
            SamplerWithCancel((p) =>
            {
                mod = new CVMB1XXModuloModBusTCP(IpCvmDummy.IP, 502, ModbusTCPProtocol.TCP);

                return true;
            }, "No se ha podido conecar con el Modulo Ethernet", 5, 1500, 0, false, false);

            if (!string.IsNullOrEmpty(macArray))
                mod.WriteMACTCP(macArray);

            Delay(1000, "espera");

            mod.Dispose();

            mod = null;

            CVMDummy.WriteDHCPFlag(true);

            Delay(1000, "espera");

            CVMDummy.Reset();

            Resultado.Set("TEST_COMUNICATIONS_TCP-MASTER", "OK", ParamUnidad.SinUnidad);

        }

        public void TestTCPtoRS485()
        {
            SamplerWithCancel((p) =>
            {
                CVMDummy.ReadMac();
                return true;
            }, "Error de comunicaciones al  con el Modulo Ethernet", 30, 1500, 0, false, false);

            var macReading = CVMDummy.ReadMac();

            this.TestInfo.NumMAC = macReading.ToString();

            var clients = new List<ClientEntry>();

            SamplerWithCancel((p) =>
            {
                clients = router.GetConnectedClientsFromWeb(true);

                return !clients.Count.Equals(0);
            }, "No se han encontrado clientes DHCP conectados al router", 50, 1000);

            Delay(1000, "Espera conexion");

            var ipMaster = clients.Where((p) => { return p.MAC.ToUpper() == macReading.ToString(); }).FirstOrDefault();

            SamplerWithCancel((p) =>
            {
                mod = new CVMB1XXModuloModBusTCP(ipMaster.IP, 502, ModbusTCPProtocol.TCP);

                return true;
            }, "No se ha podido conecar con el Modulo Ethernet", 5, 1500, 0, false, false);

            var defaultConfig = CVMDummy.ReadModuleCommunicationsConfig485();

            var configTest = defaultConfig;
            configTest.BaudRate = CVMB1XX.BaudRateModules._19200;
            CVMDummy.WriteModuleComunicationsConfig485(configTest);

            mod.tcp.PerifericNumber = 2;
            SamplerWithCancel((p) =>
            {

                var numserieSlave = mod.ReadNumSerie().CToNetString();

                Assert.IsTrue(numserieSlave.Trim() == Configuracion.GetString("SERIAL_NUMBER_CVMB100_SLAVE", "10000000001", ParamUnidad.SinUnidad).Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. el numero de serie leido del CVM-B100 Master no corresponde"));

                return true;
            }, string.Empty, 5, 1000, 0, false, false);
            defaultConfig.BaudRate = CVMB1XX.BaudRateModules._38400;
            CVMDummy.WriteModuleComunicationsConfig485(defaultConfig);

            Resultado.Set("TEST_COMUNICATIONS_TCP_RS485_SLAVER", "OK", ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (CVMDummy != null)
                TestDispose();

            if (mod != null)
                mod.Dispose();
        }
    }
}