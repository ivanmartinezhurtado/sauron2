namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ValorParametroProducto")]
    public partial class ValorParametroProducto
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("GrupoParametrizacion")]
        public int GrupoId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Parametro")]
        public int ParametroId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Producto")]
        public int ProductoId { get; set; }

        [Required]
        public string Valor { get; set; }
        public string ValorInicio { get; set; }

        public DateTime? FechaAlta { get; set; }

        [StringLength(10)]
        public string CategoriaId { get; set; }

        public virtual GrupoParametrizacion GrupoParametrizacion { get; set; }
        public virtual Producto Producto { get; set; }
        public virtual Parametro Parametro { get; set; }
        public virtual ValorParametro ValorParametro { get; set; }
    }
}
