﻿using System;
using System.IO;

namespace AssemblyResolve
{
    public class ChangeVersionAssemblyInfo
    {
        public string IncreaseFileVersionBuild(string userPath, FileAssemblyItem fileSourceChangeVersion)
        {
            var fi = Path.Combine(userPath, fileSourceChangeVersion.PathProjectAssembly, "Properties", "AssemblyInfo.cs");
            var ne = fileSourceChangeVersion.CurrentVersion.Split('.');
            var newVersion = string.Format("{0}.{1}.{2}.{3}", ne[0], ne[1], Convert.ToByte(ne[2]) + 1, ne[3]);

            string readFile;

            using (var sr = new StreamReader(fi))
            {
                readFile = sr.ReadToEnd();
                sr.Close();
            }
            var replaceFileVersion = readFile.Replace("[assembly: AssemblyFileVersion(\"" + fileSourceChangeVersion.CurrentVersion + "\")]", "[assembly: AssemblyFileVersion(\"" + newVersion + "\")]");
            var replaceFAssemblyVersion = replaceFileVersion.Replace("[assembly: AssemblyVersion(\"" + fileSourceChangeVersion.CurrentVersion + "\")]", "[assembly: AssemblyVersion(\"" + newVersion + "\")]");

            using (var sw = new StreamWriter(fi))
            {
                sw.Write(replaceFAssemblyVersion);
                sw.Close();
            }
            return newVersion;
        }
    }
}
