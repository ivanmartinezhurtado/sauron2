﻿using Dezac.Core.Model;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Services;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TaskRunner.Model;
using WinTaskRunner.ObservableCollections;
using WinTaskRunner.Services;

namespace WinTaskRunner.Views
{
    public partial class SequenceView : UserControl
    {
        private PRODUCTO producto;
        private T_FAMILIA familia;
        private T_TESTSEQUENCEFILE sequenceSeletected;
        protected readonly IShell shell;
        private VW_EMPLEADO Empleado;
        private SequenceDocument document;

        public SequenceView(IShell shell, SequenceDocument document)
        {
            InitializeComponent();

            this.shell = shell;
            this.document = document;

            var sequenceFile = FindResource("SequenceFiles") as SequenceFiles;
            sequenceFile.Clear();
            var sequenceFileVersion = FindResource("SequenceFilesVersion") as SequenceFilesVersion;
            sequenceFileVersion.Clear();
            var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
            sequenceFileProducto.Clear();

            if (document == null || !document.Properties.ContainsKey("NumProducto") || !document.Properties.ContainsKey("Version"))
            {
                buttonAddSequenceFile.IsEnabled = false;
                buttonAddSequenceFileProducto.IsEnabled = false;
                buttonDeleteSequenceFileProducto.IsEnabled = false;
                buttonAddSequenceFileVersion.IsEnabled = false;
                return;
            }

            using (var srv = new DezacService())
            {
                producto = srv.GetProducto((int)document.Properties["NumProducto"], (int)document.Properties["Version"]);
                foreach (T_TESTSEQUENCEFILE file in srv.GetFamilySequenceFiles(producto.NUMFAMILIA.Value))
                    sequenceFile.Add(file);

                familia = srv.Db.T_FAMILIA.Where(p => p.NUMFAMILIA == producto.NUMFAMILIA.Value).FirstOrDefault();
            }

            labelSequenceTitle.Content = $"Secuencias de la Familia {familia.DESCRIPCION}";

            InitialState();
        }

        private void buttonAddSequenceFileVersion_Click(object sender, RoutedEventArgs e)
        {
            System.Collections.Generic.List<Step> disabledSteps = document.Model.GetDisabledSteps();

            if (disabledSteps.Any())
                if (MessageBox.Show($"La secuencia tiene Steps deshabilitados ¿Esta seguro que quiere validarla? {Environment.NewLine}{Environment.NewLine}" +
                                    $"Steps deshabilitados: {Environment.NewLine}  -> {String.Join($"{Environment.NewLine}  -> ", disabledSteps.Select(step => step.Name))}",
                    "Steps Deshabilitados!", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;

            if (!GetEmpleado())
                return;

            var uploadDialog = new AddSequenceVersionView(sequenceSeletected.DESCRIPCION);
            uploadDialog.ShowDialog();
            if (uploadDialog.Result != MessageBoxResult.OK)
                return;

            using (var srv = new DezacContext())
            {
                var itemVer = new T_TESTSEQUENCEFILEVERSION
                {
                    NUMTSFV = srv.GetNextSerie("T_TESTSFVNUMTSFV"),
                    NUMTSF = sequenceSeletected.NUMTSF,
                    FECHAALTA = DateTime.Now,
                    USUARIOALTA = Empleado.IDOPERARIO,
                    DESCRIPCION = uploadDialog.Description,
                    FECHAVALIDACION = DateTime.Now,
                    USUARIOVALIDACION = Empleado.IDOPERARIO,
                };
                using (var ms = new MemoryStream())
                {
                    string version = string.IsNullOrEmpty(document.Model.Version) ? "0" : document.Model.Version;

                    document.Model.Version = (Convert.ToInt32(version) + 1).ToString();

                    document.Save(ms);
                    itemVer.FICHERO = ms.ToArray();
                }
                srv.T_TESTSEQUENCEFILEVERSION.Add(itemVer);
                srv.SaveChanges();
            }

            var sequenceFileVersion = FindResource("SequenceFilesVersion") as SequenceFilesVersion;
            sequenceFileVersion.Clear();
            using (var srv = new DezacService())
            {
                foreach (T_TESTSEQUENCEFILEVERSION version in srv.GetSequenceFileVersion(sequenceSeletected))
                    sequenceFileVersion.Add(version);
            }
        }

        private void buttonAddSequenceFileProducto_Click(object sender, RoutedEventArgs e)
        {
            ProductsViewModel productModel = null;
            using (var prodView = new ProductsView() { FilterByNumFamilia = familia.NUMFAMILIA, ChechBoxVersionVisible = true })
            {
                if (shell.ShowDialog("Seleccione el producto", prodView, System.Windows.Forms.MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                    return;

                productModel = prodView.Current;

                using (var srv = new DezacContext())
                {
                    var itemProducto = new T_TESTSEQUENCEFILEPRODUCTO
                    {
                        NUMTSFP = srv.GetNextSerie("T_TESTSFVNUMTSFP"),
                        NUMTSF = sequenceSeletected.NUMTSF,
                        FECHAALTA = DateTime.Now,
                        FECHABAJA = null,
                        NUMPRODUCTO = prodView.Current.NUMPRODUCTO,
                        VERSION = null,
                    };
                    if (prodView.Current.VERSION != 0)
                        itemProducto.VERSION = prodView.Current.VERSION;

                    srv.T_TESTSEQUENCEFILEPRODUCTO.Add(itemProducto);
                    srv.SaveChanges();
                }

                var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
                sequenceFileProducto.Clear();
                using (var srv = new DezacService())
                {
                    foreach (T_TESTSEQUENCEFILEPRODUCTO producto in srv.GetSequenceFileProducto(sequenceSeletected))
                        sequenceFileProducto.Add(producto);
                }
            }
        }

        private void buttonDeleteSequenceFileProducto_Click(object sender, RoutedEventArgs e)
        {
            var productoSel = (T_TESTSEQUENCEFILEPRODUCTO)gridSequenceFileProdcuto.SelectedItem;
            if (productoSel == null)
                return;

            using (var srv = new DezacContext())
            {
                srv.Database.ExecuteSqlCommand($"DELETE FROM app.T_TESTSEQUENCEFILEPRODUCTO prod WHERE prod.NUMTSFP = {productoSel.NUMTSFP}");
                srv.SaveChanges();
            }

            var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
            sequenceFileProducto.Clear();
            using (var srv = new DezacService())
            {
                foreach (T_TESTSEQUENCEFILEPRODUCTO producto in srv.GetSequenceFileProducto(sequenceSeletected))
                    sequenceFileProducto.Add(producto);
            }
        }

        private void GridSequenceFile_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            sequenceSeletected = (T_TESTSEQUENCEFILE)((DataGridRow)sender).DataContext;
            var sequenceFileVersion = FindResource("SequenceFilesVersion") as SequenceFilesVersion;
            sequenceFileVersion.Clear();
            var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
            sequenceFileProducto.Clear();
            using (var srv = new DezacService())
            {
                foreach (T_TESTSEQUENCEFILEPRODUCTO producto in srv.GetSequenceFileProducto(sequenceSeletected))
                    sequenceFileProducto.Add(producto);

                foreach (T_TESTSEQUENCEFILEVERSION version in srv.GetSequenceFileVersion(sequenceSeletected))
                    sequenceFileVersion.Add(version);
            }
            buttonAddSequenceFileProducto.IsEnabled = true;
            buttonDeleteSequenceFileProducto.IsEnabled = true;
            buttonAddSequenceFileVersion.IsEnabled = true;
        }

        private void buttonAddSequenceFile_Click(object sender, RoutedEventArgs e)
        {
            if (!GetEmpleado())
                return;

            var addSequenceView = new AddSequenceView(familia);
            addSequenceView.ShowDialog();
            if (addSequenceView.Result != MessageBoxResult.OK)
                return;

            using (var srv = new DezacContext())
            {
                var item = new T_TESTSEQUENCEFILE
                {
                    NUMTSF = srv.GetNextSerie("T_TESTSEQUENCEFILENUMTSF"),
                    NUMFAMILIA = producto.NUMFAMILIA.Value,
                    FECHAALTA = DateTime.Now,
                    USUARIOALTA = Empleado.IDOPERARIO,
                    DESCRIPCION = addSequenceView.Description,
                    NOMBREFICHERO = addSequenceView.FileName,
                    IDFASE = addSequenceView.Fase.IDFASE
                };
                srv.T_TESTSEQUENCEFILE.Add(item);
                srv.SaveChanges();
            }

            var sequenceFile = FindResource("SequenceFiles") as SequenceFiles;
            sequenceFile.Clear();
            var sequenceFileVersion = FindResource("SequenceFilesVersion") as SequenceFilesVersion;
            sequenceFileVersion.Clear();
            var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
            sequenceFileProducto.Clear();
            using (var srv = new DezacService())
            {
                foreach (T_TESTSEQUENCEFILE file in srv.GetFamilySequenceFiles(producto.NUMFAMILIA.Value))
                    sequenceFile.Add(file);
            }
        }

        private void buttonChangeFamily_Click(object sender, RoutedEventArgs e)
        {
            ProductsViewModel productModel = null;
            using (var prodView = new ProductsView())
            {
                if (shell.ShowDialog("Seleccione el producto", prodView, System.Windows.Forms.MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                    return;

                productModel = prodView.Current;

                var sequenceFile = FindResource("SequenceFiles") as SequenceFiles;
                sequenceFile.Clear();
                var sequenceFileVersion = FindResource("SequenceFilesVersion") as SequenceFilesVersion;
                sequenceFileVersion.Clear();
                var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
                sequenceFileProducto.Clear();
                using (var srv = new DezacService())
                {
                    producto = srv.GetProducto(productModel.NUMPRODUCTO, productModel.VERSION);
                    foreach (T_TESTSEQUENCEFILE file in srv.GetFamilySequenceFiles(producto.NUMFAMILIA.Value))
                        sequenceFile.Add(file);

                    familia = srv.Db.T_FAMILIA.Where(p => p.NUMFAMILIA == producto.NUMFAMILIA.Value).FirstOrDefault();
                }
            }
            labelSequenceTitle.Content = $"Secuencias de la Familia {producto.T_FAMILIA.DESCRIPCION}";

            buttonAddSequenceFile.IsEnabled = true;
            buttonAddSequenceFileProducto.IsEnabled = false;
            buttonDeleteSequenceFileProducto.IsEnabled = false;
            buttonAddSequenceFileVersion.IsEnabled = false;
        }

        private bool GetEmpleado()
        {
            if (Empleado == null)
            {
                var ctrl = new LoginView();
                if (shell.ShowDialog("IDENTIFICACIÓN", ctrl, System.Windows.Forms.MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                    return false;

                Empleado = ctrl.Empleado;
                labelUsuario.Content = $"{Empleado.IDOPERARIO} {Empleado.NOMBRE}";
            }
            return true;
        }

        private void InitialState()
        {
            var sequenceFiles = FindResource("SequenceFiles") as SequenceFiles;

            if (document.Properties.ContainsKey("Fase") && document.Properties.ContainsKey("Producto"))
                using (var srv = new DezacService())
                {
                    T_TESTSEQUENCEFILE actualSequence = sequenceFiles
                        .Where(seq =>
                            seq.IDFASE == (document.Properties["Fase"] as ProductFaseViewModel).IDFASE &&
                            srv.GetSequenceFileProducto(seq)
                                .Any(prod =>
                                    prod.NUMPRODUCTO == (document.Properties["Producto"] as ProductsViewModel).NUMPRODUCTO &&
                                    prod.VERSION == (document.Properties["Producto"] as ProductsViewModel).VERSION))
                        .FirstOrDefault();

                    if (actualSequence == null)
                        actualSequence = sequenceFiles
                            .Where(seq =>
                                seq.IDFASE == (document.Properties["Fase"] as ProductFaseViewModel).IDFASE &&
                                srv.GetSequenceFileProducto(seq)
                                    .Any(prod => prod.NUMPRODUCTO == (document.Properties["Producto"] as ProductsViewModel).NUMPRODUCTO))
                            .FirstOrDefault();

                    if (actualSequence != null)
                    {
                        gridSequenceFile.SelectedItem = actualSequence;
                        sequenceSeletected = actualSequence;
                        var sequenceFileVersion = FindResource("SequenceFilesVersion") as SequenceFilesVersion;
                        sequenceFileVersion.Clear();
                        var sequenceFileProducto = FindResource("SequenceFilesProducto") as SequenceFilesProducto;
                        sequenceFileProducto.Clear();

                        foreach (T_TESTSEQUENCEFILEPRODUCTO producto in srv.GetSequenceFileProducto(sequenceSeletected))
                            sequenceFileProducto.Add(producto);

                        foreach (T_TESTSEQUENCEFILEVERSION version in srv.GetSequenceFileVersion(sequenceSeletected))
                            sequenceFileVersion.Add(version);

                        T_TESTSEQUENCEFILEPRODUCTO actualProduct = sequenceFileProducto
                            .Where(prod =>
                                prod.NUMPRODUCTO == (document.Properties["Producto"] as ProductsViewModel).NUMPRODUCTO &&
                                prod.VERSION == (document.Properties["Producto"] as ProductsViewModel).VERSION)
                            .FirstOrDefault();
                        if (actualProduct == null)
                            actualProduct = sequenceFileProducto
                                .Where(prod => prod.NUMPRODUCTO == (document.Properties["Producto"] as ProductsViewModel).NUMPRODUCTO)
                                .FirstOrDefault();

                        if (actualProduct != null)
                            gridSequenceFileProdcuto.SelectedItem = actualProduct;

                        T_TESTSEQUENCEFILEVERSION actualVersion = sequenceFileVersion
                            .OrderByDescending(ver => ver.FECHAVALIDACION)
                            .FirstOrDefault();
                        if (actualVersion != null)
                            gridSequenceFileVersion.SelectedItem = actualVersion;
                    }
                }
        }
    }
}
