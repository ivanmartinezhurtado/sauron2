﻿using System.Collections.Generic;

namespace AssemblyResolve
{
    public class FileAssemblyItem
    {
        public bool Update { get; set; }
        public string NameAssembly { get; set; }
        public string Name { get; set; }
        public string CurrentVersion { get; set; }
        public string NewVersion { get; set; }
        public string PathProjectAssembly { get; set; }
        public bool ControlClassversion { get; set; }
        public bool IncrementVersion { get; set; }
        public List<ClassVersion> Clases { get; set; }
    }
}
