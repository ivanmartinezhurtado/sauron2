﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.AOI.Models
{
    public enum ClassificatorsEnum
    {
        _0_Generico = 0,
        _1_TestPoint = 1,
        _2_ConectorDoble = 2,
        _3_RectangularGrande = 3,
        _4_IVDS = 4,
        _5_EsquinasSinEstano = 5,
    }
}
