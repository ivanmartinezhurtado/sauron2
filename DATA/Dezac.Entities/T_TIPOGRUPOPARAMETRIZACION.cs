namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TIPOGRUPOPARAMETRIZACION
    {
        public T_TIPOGRUPOPARAMETRIZACION()
        {
            this.T_GRUPOPARAMETRIZACION = new HashSet<T_GRUPOPARAMETRIZACION>();
        }
    
        public int NUMTIPOGRUPO { get; set; }
        public string TIPO { get; set; }
        public string DESCRIPCION { get; set; }
    
        public virtual ICollection<T_GRUPOPARAMETRIZACION> T_GRUPOPARAMETRIZACION { get; set; }
    }
}
