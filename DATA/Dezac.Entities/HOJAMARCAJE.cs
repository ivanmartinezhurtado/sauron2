namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class HOJAMARCAJE
    {
        public HOJAMARCAJE()
        {
            this.DIARIOMARCAJE = new HashSet<DIARIOMARCAJE>();
        }
    
        public int NUMHOJA { get; set; }
        public string IDACTIVIDAD { get; set; }
        public Nullable<int> NUMORDEN { get; set; }
        public string IDACTIVIDADREPROCESO { get; set; }
        public string IDCAUSANTEREPROCESO { get; set; }
        public Nullable<int> NUMTRABAJO { get; set; }
        public string MODIFICACIONSINO { get; set; }
        public string PREPARACIONSINO { get; set; }
    
        public virtual ICollection<DIARIOMARCAJE> DIARIOMARCAJE { get; set; }
        public virtual ORDEN ORDEN { get; set; }
    }
}
