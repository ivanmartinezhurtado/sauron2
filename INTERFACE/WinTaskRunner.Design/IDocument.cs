﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WinTaskRunner.Design
{
    public interface IDocumentManager
    {
        IDocument AddNew();
        IDocument Open();
        IDocument Open(string fileName);
        IDocument Open(Stream stream, string fileName);
        void Save(IDocument doc);
        void SaveAs(IDocument doc);
        string DefaultExtension { get; }
    }

    public interface IDocument
    {
        string Name { get; set; }
        string FileName { get; set; }
        bool IsNew { get; }
        void Save();
        void Save(Stream stream, Encoding encoding = null);
        string DocType { get; }

        Dictionary<string, object> Properties { get; }
    }
}
