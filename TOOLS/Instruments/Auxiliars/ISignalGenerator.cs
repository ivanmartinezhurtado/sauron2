﻿using System;

namespace Instruments.Auxiliars
{
    public enum SignalFunction
    {
        SINUSOIDAL,
        SQUARE,
        TRIANGULAR,
        RAMP,
        PULSE,
        NRAMP,
        NOISE,
        PRBS,
        ARBITRARY,
        DC
    }

    public enum UnitVoltage
    {    
        VRMS,
        VPP,
        DBM
    }

    public class SignalInfo
    {
        public int Channel { get; set; }
        public SignalFunction Function { get; set; }
        public double? Frequency { get; set; }

        public double? VoltageOffset { get; set; }
        public double? Amplitude { get; set; }
        public UnitVoltage UnitVoltage { get; set; }
        public double? MinVoltage { get; set; }
        public double? MaxVoltage { get; set; }

        public double? Phase { get; set; }
        public double? DutyCycle { get; set; }
    }

    public interface ISignalGenerator : IDisposable
    {
        ISignalGenerator SetSignal(SignalInfo info, bool apply = false);

        ISignalGenerator On(int channel);
        ISignalGenerator Off(int channel);

        byte portAddress { get; set; }

        string GetIdentification();
    }

}
