﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Comunications.Utility
{
    public static class Extensions
    {  
        public static byte[] ToBytes(object value)
        {
            if (value == null) return null;

            Type valueType = value.GetType();

            if (!valueType.IsValueType) return null;

            int rawsize = Marshal.SizeOf(value);

            if (rawsize % 2 != 0) rawsize -= 1;

            byte[] rawdata = new byte[rawsize];

            GCHandle handle = GCHandle.Alloc(rawdata, GCHandleType.Pinned);

            try
            {
                Marshal.StructureToPtr(value, handle.AddrOfPinnedObject(), false);
            }
            finally
            {
                handle.Free();
            }

            AdjustEndianness(valueType, rawdata);

            return rawdata;
        }

        public static byte[] HexStringToBytes(string value)
        {
            var lenght = value.Length == 2 ? 1 : (value.Length) / 2;

            byte[] data = new byte[lenght];
            int n = 0;

            for (int i = 0; i < value.Length - 1; i += 2) //Step 2
            {
                data[n] = byte.Parse(value.Substring(i, 2), NumberStyles.HexNumber);
                n += 1;
            }
            return data;
        }

        public static string BytesToHexString(byte[] value)
        {
            string text = string.Empty;

            foreach (byte b in value)
                text += b.ToString("X2");
            return text;
        }

        public static T AsignaEstructura<T>(ushort[] data) where T : struct
        {
            using (MemoryStream ms = new MemoryStream())
            {
                foreach (ushort value in data)
                {
                    byte[] ubytes = BitConverter.GetBytes(value);
                    ms.WriteByte(ubytes[1]);
                    ms.WriteByte(ubytes[0]);
                }

                Type tp = typeof(T);

                byte[] bytes = ms.ToArray();

                AdjustEndianness(tp, bytes);

                GCHandle gch = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                try
                {
                    IntPtr ptr = gch.AddrOfPinnedObject();

                    Object obj = Marshal.PtrToStructure(ptr, tp);

                    return (T)Convert.ChangeType(obj, typeof(T));
                }
                finally
                {
                    gch.Free();
                }
            }
        }

        public static T AsignaEstructura<T>(byte[] data) where T : struct
        {
            using (MemoryStream ms = new MemoryStream())
            {
                Type tp = typeof(T);

                AdjustEndianness(tp, data);

                GCHandle gch = GCHandle.Alloc(data, GCHandleType.Pinned);
                try
                {
                    IntPtr ptr = gch.AddrOfPinnedObject();

                    Object obj = Marshal.PtrToStructure(ptr, tp);

                    return (T)Convert.ChangeType(obj, typeof(T));
                }
                finally
                {
                    gch.Free();
                }
            }
        }

        public static object AsignaEstructura(Type type, byte[] data) 
        {
            using (MemoryStream ms = new MemoryStream())
            {
                AdjustEndianness(type, data);

                GCHandle gch = GCHandle.Alloc(data, GCHandleType.Pinned);
                try
                {
                    IntPtr ptr = gch.AddrOfPinnedObject();

                    Object obj = Marshal.PtrToStructure(ptr, type);

                    return Convert.ChangeType(obj, type);
                }
                finally
                {
                    gch.Free();
                }
            }
        }

        private static void AdjustEndianness(Type type, byte[] data, int baseOffset = 0)
        {
            foreach (FieldInfo field in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.FieldType.Name == "String" || field.FieldType.Name == "Byte[]")
                    continue;

                int offset = Marshal.OffsetOf(type, field.Name).ToInt32();

                if (field.FieldType.IsValueType && !field.FieldType.IsPrimitive)
                    AdjustEndianness(field.FieldType, data, baseOffset + offset);
                else
                {
                    int size = Marshal.SizeOf(field.FieldType);
                    if (size > 1)
                        Array.Reverse(data, baseOffset + offset, size);
                }
            }
        }

        public static T AsignedTramaToStructure<T>(string trama, char separator =' ') where T : struct
        {
            var split = trama.Split(separator);
            byte i = 0;

            T toStruc = new T();
            object obj = (T)toStruc;
            FieldInfo[] fi = typeof(T).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo info in fi)
            {
                var valorEspacio = split[i];
                if (valorEspacio == "")
                    i++;

                var valor = Convert.ChangeType(split[i], info.FieldType);
                if(valor.ToString() != "")
                 info.SetValue(obj, valor);

                i++;
            }
            return (T)Convert.ChangeType(obj, typeof(T));
        }  

        public static T AsignedArrayByteToBooleanStruct<T>(byte[] resp) where T : struct
        {
            var responseBoolean = new List<bool>();
            foreach (byte b in resp)
            {
                var binary = Convert.ToString(b, 2).PadLeft(8, '0');
                var value = binary.ToCharArray();
                Array.Reverse(value);
                foreach (char c in value)
                    responseBoolean.Add(c == '0' ? false : true);
            }
            var booleanArray = responseBoolean.ToArray();
            T toStruc = new T();
            object obj = (T)toStruc;
            FieldInfo[] fi = typeof(T).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var i = 0;
            foreach (FieldInfo info in fi)
            {
                var valor = Convert.ChangeType(booleanArray[i], info.FieldType);
                if (valor.ToString() != "")
                    info.SetValue(obj, valor);
                i++;
            }
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }
}
