namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Producto")]
    public partial class Producto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [ForeignKey("Articulo")]
        public int ArticuloId { get; set; }
        public int Version { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }
        public bool Baja { get; set; }

        //[ForeignKey("Familia")]
        //public int? FamiliaId { get; set; }

        public Articulo Articulo { get; set; }
        //public Familia Familia { get; set; }
    }
}
