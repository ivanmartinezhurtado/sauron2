﻿using Comunications.Ethernet;
using System.Drawing;
using Dezac.Core.Utility;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(TftpServerDisposeDescription))]
    public class TftpServerDispose : ActionBase
    {
        public override void Execute()
        {

            var tftpServer = GetVariable<TFTPServer>("TFTPServer", () => { return null; });

            if (tftpServer == null)
                return;

            tftpServer.Dispose();
        }

        public class TftpServerDisposeDescription : ActionDescription
        {
            public override string Name { get { return "TFTPServerDispose"; } }
            public override string Category { get { return "Tools/Ethernet/TFTP"; } }
            public override string Description { get { return "Action que sirve para finalizar el servidor TFTP"; } }
            public override Image Icon { get { return Properties.Resources.ftp_icon; } }
        }
    }

}
