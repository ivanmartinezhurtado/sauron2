namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ORDEN
    {
        public ORDEN()
        {
            this.HOJAMARCAJE = new HashSet<HOJAMARCAJE>();
            this.HOJAMARCAJEFASE = new HashSet<HOJAMARCAJEFASE>();
            this.ORDENCAJA = new HashSet<ORDENCAJA>();
            this.ORDENPRODUCTO = new HashSet<ORDENPRODUCTO>();
        }
    
        public int NUMORDEN { get; set; }
        public string DESCRIPCION { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public string ABIERTA { get; set; }
        public decimal CANTIDAD { get; set; }
        public Nullable<decimal> PENDIENTE { get; set; }
        public string IDALMACEN { get; set; }
        public string UBICACION { get; set; }
        public System.DateTime FECHAALTA { get; set; }
        public short PRIORIDAD { get; set; }
        public Nullable<int> NUMPROPUESTA { get; set; }
        public string PROC_DBMS { get; set; }
        public string PROC_TIPO { get; set; }
        public string PROC_SERIE { get; set; }
        public Nullable<decimal> PROC_NUMERO { get; set; }
        public Nullable<decimal> PROC_CANTIDAD { get; set; }
        public string CONMODIFICACION { get; set; }
        public string DETRANSFORMACION { get; set; }
        public string NOTASMODIFICACION { get; set; }
        public Nullable<int> TRAPRODORG { get; set; }
        public Nullable<int> TRAVERORG { get; set; }
        public string TRAPARTESINO { get; set; }
        public string USERNAMEALTA { get; set; }
        public string USERNAMEMODIFICACION { get; set; }
        public Nullable<System.DateTime> FECHACIERRE { get; set; }
        public string CONFIRMACIONENTREGA { get; set; }
        public string CONFIRMACIONENTREGAIE { get; set; }
        public string FABRICARPORFASES { get; set; }
        public string REFERENCIATIEMPO { get; set; }
        public string NOTASIDP { get; set; }
        public Nullable<int> NROSERIE { get; set; }
        public string PRODUCCIONSINO { get; set; }
        public string PROTOTIPOSINO { get; set; }
        public string BLOQUEOSINO { get; set; }
        public string NOTASBLOQUEO { get; set; }
        public string PRESERIESINO { get; set; }
        public Nullable<int> NUMPROYECTO { get; set; }
        public Nullable<int> NUMFASE { get; set; }
        public Nullable<int> NUMTAREA { get; set; }
        public string REPROCESO { get; set; }
        public string IDTIPO { get; set; }
        public string STOCKSINO { get; set; }
        public string IDLINEAENTREGA { get; set; }
        public Nullable<int> NUMASISTENCIA { get; set; }
        public string MRP_CODI { get; set; }
        public string BLOQUEOMARCAJE { get; set; }
        public string NIVELCONTROLTEST { get; set; }
    
        public virtual ICollection<HOJAMARCAJE> HOJAMARCAJE { get; set; }
        public virtual ICollection<HOJAMARCAJEFASE> HOJAMARCAJEFASE { get; set; }
        public virtual PROYECTO PROYECTO { get; set; }
        public virtual ICollection<ORDENCAJA> ORDENCAJA { get; set; }
        public virtual ICollection<ORDENPRODUCTO> ORDENPRODUCTO { get; set; }
        public virtual ARTICULO ARTICULO { get; set; }
    }
}
