﻿using System;
using System.Globalization;
using System.Text;

namespace Comunications.Message
{
    public class StringMessage : BasicMessage, IValidateMessage
    {
        public StringMessage()
        {
            Encoding = ASCIIEncoding.ASCII;
        }

        public StringMessage(string text)
            : this()
        {
            Text = text;
        }

        public static StringMessage Create(IFormatProvider formatProvider, string format, params object[] args)
        {
            StringMessage message = new StringMessage { Format = format };

            message.SetArgs(formatProvider, args);

            return message;
        }

        public Func<StringMessage, bool> Validator { get; set; }

        public Encoding Encoding { get; set; }

        public string Format { get; set; }

        public virtual string Text
        {
            get
            {
                if (frame == null)
                    return null;

                return Encoding.GetString(frame);
            }
            set
            {
                byte[] frame = Encoding.GetBytes(value);
                Initialize(frame);
            }
        }

        public void SetArgs(params object[] args)
        {
            SetArgs((IFormatProvider)null, args);
        }

        public void SetArgs(IFormatProvider formatProvider, params object[] args)
        {
            if (string.IsNullOrEmpty(Format))
            {
                if (args.Length > 1)
                    throw new Exception("Invalid arguments for given format");

                Text = args[0].ToString();
            }
            else
                if (formatProvider != null)
                    Text = string.Format(formatProvider, Format, args);
                else
                    Text = string.Format(Format, args);
        }

        public virtual void ValidateResponse(ITransportMessage response)
        {
            if (Validator != null && !Validator((StringMessage)response))
                throw new FormatException("Error, formato del trama de respuesta del protocolo StringMesssage incorrecta");
        }

        public override string ToString()
        {
            return Text;
        }

        public double QueryNumber()
        {
            double res = double.Parse(Text, System.Globalization.NumberStyles.Float, new CultureInfo("en-US"));
            return res;
        }

        public string GetFormattedText()
        {
            int p = Text.IndexOf(',');

            if (p > 0)
                return Text.Substring(p + 1).Replace("--------", "0");

            return Text;
        }
    }
}
