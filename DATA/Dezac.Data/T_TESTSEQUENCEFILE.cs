namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTSEQUENCEFILE")]
    public partial class T_TESTSEQUENCEFILE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_TESTSEQUENCEFILE()
        {
            T_TESTSEQUENCEFILEVERSION = new HashSet<T_TESTSEQUENCEFILEVERSION>();
            T_TESTSEQUENCEFILEPRODUCTO = new HashSet<T_TESTSEQUENCEFILEPRODUCTO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTSF { get; set; }

        public int NUMFAMILIA { get; set; }

        public int? NUMPRODUCTO { get; set; }

        public int? VERSION { get; set; }

        [StringLength(255)]
        public string NOMBREFICHERO { get; set; }

        [StringLength(512)]
        public string DESCRIPCION { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(15)]
        public string USUARIOALTA { get; set; }

        public byte[] FICHERO { get; set; }

        public DateTime? FECHAVALIDACION { get; set; }

        [StringLength(15)]
        public string USUARIOVALIDACION { get; set; }

        [StringLength(10)]
        public string IDFASE { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTSEQUENCEFILEVERSION> T_TESTSEQUENCEFILEVERSION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTSEQUENCEFILEPRODUCTO> T_TESTSEQUENCEFILEPRODUCTO { get; set; }
    }
}
