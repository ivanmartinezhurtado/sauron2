﻿namespace Dezac.Tests.Actions
{
    public enum BpsEnum
    {
        Bps9600 = 9600,
        Bps19200 = 19200,
        Bps38400 = 38400,
        Bps115200 = 115200
    }
}
