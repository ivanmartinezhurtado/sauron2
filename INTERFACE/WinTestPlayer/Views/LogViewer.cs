﻿using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class LogViewer : UserControl
    {
        protected readonly AppService app;
        private SynchronizationContext sc;
        private SequenceContext context;

        public LogViewer()
        {
            InitializeComponent();

            app = DI.Resolve<AppService>();
            sc = SynchronizationContext.Current;

            app.Runner.AllTestStarting += (s, e) =>
            {
                timer1.Enabled = true;
                timer1.Start();
                Clear();
            };

            app.Runner.TestEnd += (s, e) => { context = e; };

            app.Runner.AllTestEnded += (s, e) =>
            {
                Stop();
            };
        }

        private void Clear()
        {
            radGridView1.Rows.Clear();
            LogService.Clear();
        }

        private void Stop()
        {
            timer1.Stop();
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            sc.Post(_ =>
            {
                AddUpdateRowGrid(LogService.MyEvents);
            }, null);

            timer1.Start();
        }

        public void AddUpdateRowGrid(List<LoggingEventWrapper> events)
        {
            radGridView1.Rows.Clear();

            int staticCount = LogService.MyEvents.Count;
            for (int i = staticCount - 1; i > staticCount - 11 && i >= 0; i--)
            {
                var row = LogService.MyEvents[i];
                var rowIndex = radGridView1.Rows.Add(row.NumInstance, row.TimeStamp, row.LoggerName, row.StepPath, row.RenderedMessage);
             
                if (row.Level == Level.Error)
                    radGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Red;

                if (row.Level == Level.Warn)
                    radGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Orange;

                if (row.Level == Level.Debug)
                    radGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.AliceBlue;

                radGridView1.Rows[rowIndex].Selected = true;
                radGridView1.FirstDisplayedScrollingRowIndex = rowIndex;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void btnLogger_Click(object sender, EventArgs e)
        {
            ITestContext context = this.context != null ? this.context.Services.Get<ITestContext>() : null;

            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, false, context, false);
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            ITestContext context = this.context != null ? this.context.Services.Get<ITestContext>() : null;

            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.CheckReport, false, context, false);
        }

        private void btnRaw_Click(object sender, EventArgs e)
        {
            ITestContext context = this.context != null ? this.context.Services.Get<ITestContext>() : null;

            LogService.ShowLog(FormatLogViewer.LogsTypesEnum.RawByTimeLog, false, context, false);
        }
    }
}
