﻿namespace Instruments.IO
{
    public class IOPort
    {
        internal byte Data { get; set; }

        internal bool Value { get; set; }

        internal IOChannel Channel { get; set; }

        public int BitsPerPort { get; internal set; }
        public int MinBit { get; internal set; }
        public int MaxBit { get { return MinBit + BitsPerPort - 1; } }

        public IOPort(IOChannel channel, int bitsPerPort = 8)
        {
            this.Channel = channel;
            this.BitsPerPort = bitsPerPort;
        }

        public bool this[int index]
        {
            get { return (Data & (1 << index)) > 0; }
            set
            {
                int bitInByteIndex = index % BitsPerPort;
                byte mask = (byte)(1 << bitInByteIndex);

                if (value)
                    Data |= mask;
                else
                    Data &= (byte)~mask;
            }
        }

        public IOType IOType { get; set; }

        public int NumPort
        {
            get
            {
                int numPorts = Channel.Ports.Count;
                int relativeChannel = Channel.Ports.IndexOf(this);

                return (Channel.NumChannel * numPorts) + relativeChannel;
            }
        }

        public int RelativeNumPort
        {
            get
            {
                return Channel.Ports.IndexOf(this);
            }
        }
    }
}
