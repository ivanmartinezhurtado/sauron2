﻿using System;

namespace AssemblyResolve
{
    [Serializable]
    public class ClassVersion
    {
        public string NameClass;
        public string Version;
    }
}
