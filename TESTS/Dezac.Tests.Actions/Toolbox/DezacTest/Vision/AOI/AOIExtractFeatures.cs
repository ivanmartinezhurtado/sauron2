﻿using Dezac.Core.Utility;
using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.AOI;

namespace Dezac.Tests.Actions.AOI
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AOIExtractFeaturesDescription))]
    [Browsable(false)]
    public class AOIExtractFeatures : ActionBase
    {
        public string weldingsFolder { get; set; }

        public string inFeaturesFile { get; set; }

        public string outFeaturesFile { get; set; }

        public override void Execute()
        {
            HalconInterface.AOIExtractFeatures(weldingsFolder, inFeaturesFile, outFeaturesFile);
        }
    }

    public class AOIExtractFeaturesDescription : ActionDescription
    {
        public override string Name { get { return "AOIExtractFeatures"; } }
        public override string Category { get { return "DezacTest/Vision/AOI"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
