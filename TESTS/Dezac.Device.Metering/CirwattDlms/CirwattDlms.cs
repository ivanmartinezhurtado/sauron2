﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Tests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.04)]
    public class CirwattDlms : DeviceBase
    {

        [JsonIgnore]
        public short PPMClock
        {
            get
            {
                return dlms.ClockPPM;
            }
        }

        private Dlms.DLMS dlms;

        [Flags]
        public enum enumFases
        {
            NOTHING = 0,
            [Description("0")]
            FASE_1 = (1 << 0),
            [Description("1")]
            FASE_2 = (1 << 1),
            [Description("2")]
            FASE_3 = (1 << 2),
            [Description("3")]
            FASE_4 = (1 << 3),
            [Description("4")]
            FASE_5 = (1 << 4),
            [Description("5")]
            FASE_6 = (1 << 5),
            [Description("0")]
            MONOFASICO = FASE_1,
            TRIFASICO = FASE_1 | FASE_2 | FASE_3,
            QUATRIFASICO = FASE_1 | FASE_2 | FASE_3 | FASE_4,
            SIXFASICO = FASE_1 | FASE_2 | FASE_3 | FASE_4 | FASE_5 | FASE_6,
        }


        public struct structPhaseOffset
        {
            public double anguloDesfase;
            public double errorDesfase;
            public short puntosErrorDesfase;
        }

        public List<structPhaseOffset> phaseOffset { get; set;}

        [JsonIgnore]
        public Dlms.DLMS Dlms {
            get
            {
                if (dlms == null)
                {
                    dlms = new Dlms.DLMS();
                    dlms.ClientAddress = 73;
                    dlms.Password = "qxzbravo";
                    dlms.SetTimeout(2000);
                }
                return dlms;
            }   
        }

        public void SetPort(int port, int baudrate)
        {
            Dlms.SerialPort = "COM" + port;
            Dlms.BaudRate = baudrate;
        }

        public byte PuertoCom { get { return Convert.ToByte(Dlms.SerialPort.Replace("COM", "")); } set { Dlms.SerialPort = "COM" + value; } }

        public byte Retries { set { Dlms.Retries = value; } }

        public void Retry(Action action, int numRetries = 1)
        {
            Exception last = null;
            while(numRetries-- >= 0)
                try
                {
                    action();
                    return;
                }
                catch(Exception ex)
                {
                    last = ex;
                }

            throw last;            
        }

        public T Retry<T>(Func<T> func, int numRetries = 3)
        {
            Exception last = null;
            while (numRetries-- >= 0)
                try
                {
                    T res = func();
                    return res;
                }
                catch(Exception ex)
                {
                    _logger.Warn("Exepcion capturada: " + ex.Message);
                    last = ex;
                }

            throw last;
        }

        public void IniciarSesion()
        {
            LogMethod();

            if (!Retry<bool>(() => {

                Dlms.Disconnect();

                System.Threading.Thread.Sleep(500);

               return Dlms.Associate();

            }, 3))
                throw new Exception("Error de comunicaciones al iniciar sesión en el DLMS");
        }

        public void CerrarSesion(bool throwError = true)
        {
            try
            {
                LogMethod();

                Dlms.Disconnect();
            }
            catch (Exception ex)
            {
                if (throwError)
                    throw new Exception("Error de comunicaciones al Cerrar sesión en el DLMS por " + ex.Message);
            }
        }

        #region Metodos Ajuste

        //---------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------

        public Tuple<bool, List<short>> AdjustGap(enumFases fase, TypeMeterConection.frequency freq, double MiPFref, double MiFactorDesfase, ConfigurationSampler parameters, Func<List<short>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new List<short>();

            var adjustResult = AdjustBase(parameters.InitDelayTime, parameters.SampleNumber, parameters.SampleMaxNumber, parameters.DelayBetweenSamples, 0,
                () =>
                {
                    var activePoints = this.Retry<double[]>(() => ReadPowerPoints(fase, TipoPotencia.Activa).ToArray(), 3);
                    var reactivePoints = this.Retry<double[]>(() => ReadPowerPoints(fase, TipoPotencia.React).ToArray(), 3);

                    if (fase.HasFlag(enumFases.FASE_1))
                        _logger.Debug(string.Format("KWL1 : {0}", activePoints[0]));

                    if (fase.HasFlag(enumFases.FASE_2))
                        _logger.Debug(string.Format("KWL2 : {0}", activePoints[1]));

                    if (fase.HasFlag(enumFases.FASE_3))
                        _logger.Debug(string.Format("KWL3 : {0}", activePoints[2]));

                    if (fase.HasFlag(enumFases.FASE_4))
                        _logger.Debug(string.Format("KWLN : {0}", activePoints[3]));

                    if (fase.HasFlag(enumFases.FASE_1))
                        _logger.Debug(string.Format("KVARL1 : {0}", reactivePoints[0]));

                    if (fase.HasFlag(enumFases.FASE_2))
                        _logger.Debug(string.Format("KVARL2 : {0}", reactivePoints[1]));

                    if (fase.HasFlag(enumFases.FASE_3))
                        _logger.Debug(string.Format("KVARL3 : {0}", reactivePoints[2]));

                    if (fase.HasFlag(enumFases.FASE_4))
                        _logger.Debug(string.Format("KVARLN : {0}", reactivePoints[3]));

                    var points = new List<double>();
                    points.AddRange(activePoints);
                    points.AddRange(reactivePoints);

                    return points.ToArray();
                },
                (points) =>
                {
                    var rangActive = 0;
                    var rangReactive = fase == enumFases.QUATRIFASICO ? 4 : fase == enumFases.TRIFASICO ? 3 : fase == enumFases.SIXFASICO ? 6 : 1;

                    resultadoAjuste.Clear();

                    if (fase.HasFlag(enumFases.FASE_1))
                        resultadoAjuste.Add(CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));

                    if (fase.HasFlag(enumFases.FASE_2))
                        resultadoAjuste.Add(CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));

                    if (fase.HasFlag(enumFases.FASE_3))
                        resultadoAjuste.Add(CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));

                    if (fase.HasFlag(enumFases.FASE_4))
                        resultadoAjuste.Add(CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));

                    if (fase.HasFlag(enumFases.FASE_5))
                        resultadoAjuste.Add(CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));

                    if (fase.HasFlag(enumFases.FASE_6))
                        resultadoAjuste.Add(CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));
                    
                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        private short CalculateGap(StatisticalList points, double MiPFref, double MiFactorDesfase, int positionActiva, int positionReactiva)
        {
            var phaseOffset = new structPhaseOffset();
            phaseOffset.anguloDesfase = (Math.Atan2(points.Average(positionReactiva), points.Average(positionActiva))).ToDegree();
            phaseOffset.errorDesfase = MiPFref - phaseOffset.anguloDesfase;
            phaseOffset.puntosErrorDesfase = (short)((phaseOffset.errorDesfase * MiFactorDesfase));
            return phaseOffset.puntosErrorDesfase;
        }

        public Tuple<bool, List<ushort>> AdjustVoltage(enumFases fase, double voltageReference, ConfigurationSampler parameters, Func<List<ushort>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new List<ushort>();

            var adjustResult = AdjustBase(parameters.InitDelayTime, parameters.SampleNumber, parameters.SampleMaxNumber, parameters.DelayBetweenSamples, 0,
                () =>
                {
                    var voltagePoints = this.Retry<double[]>(() => ReadVoltagePoints(fase).ToArray(), 3);

                    if (fase.HasFlag(enumFases.FASE_1))
                        _logger.Debug(string.Format("VL1 : {0}", voltagePoints[0]));

                    if (fase.HasFlag(enumFases.FASE_2))
                        _logger.Debug(string.Format("VL2 : {0}", voltagePoints[1]));

                    if (fase.HasFlag(enumFases.FASE_3))
                        _logger.Debug(string.Format("VL3 : {0}", voltagePoints[2]));

                    if (fase.HasFlag(enumFases.FASE_4))
                        _logger.Debug(string.Format("VL4 : {0}", voltagePoints[3]));

                    var points = new List<double>();
                    points.AddRange(voltagePoints);

                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    resultadoAjuste.Clear();

                    if (fase.HasFlag(enumFases.FASE_1))
                        resultadoAjuste.Add(CalculateVoltageGain(points, voltageReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_2))
                        resultadoAjuste.Add(CalculateVoltageGain(points, voltageReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_3))
                        resultadoAjuste.Add(CalculateVoltageGain(points, voltageReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_4))
                        resultadoAjuste.Add(CalculateVoltageGain(points, voltageReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_5))
                        resultadoAjuste.Add(CalculateVoltageGain(points, voltageReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_6))
                        resultadoAjuste.Add(CalculateVoltageGain(points, voltageReference, rang++));

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        private ushort CalculateVoltageGain(StatisticalList points, double voltageReference, int rang)
        {
            return (ushort)((Math.Sqrt(points.Average(rang)) * 100) / (voltageReference));
        }

        public Tuple<bool, List<ushort>> AdjustPowerActive(enumFases fase, TypeMeterConection.frequency freq, double powerReference, ConfigurationSampler parameters, Func<List<ushort>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new List<ushort>(); 

            var adjustResult = AdjustBase(parameters.InitDelayTime, parameters.SampleNumber, parameters.SampleMaxNumber, parameters.DelayBetweenSamples, 0,
                () =>
                {
                    var activePoints = this.Retry<double[]>(() => ReadPowerPoints(fase, TipoPotencia.Activa).ToArray(), 3);

                    if (fase.HasFlag(enumFases.FASE_1))
                        _logger.Debug(string.Format("KWL1 : {0}", activePoints[0]));

                    if (fase.HasFlag(enumFases.FASE_2))
                        _logger.Debug(string.Format("KWL2 : {0}", activePoints[1]));

                    if (fase.HasFlag(enumFases.FASE_3))
                        _logger.Debug(string.Format("KWL3 : {0}", activePoints[2]));

                    if (fase.HasFlag(enumFases.FASE_4))
                        _logger.Debug(string.Format("KWL4 : {0}", activePoints[3]));

                    var points = new List<double>();
                    points.AddRange(activePoints);

                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    resultadoAjuste.Clear();

                    if (fase.HasFlag(enumFases.FASE_1))
                        resultadoAjuste.Add((ushort)CalculatePowerActive(points, powerReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_2))
                        resultadoAjuste.Add((ushort)CalculatePowerActive(points, powerReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_3))
                        resultadoAjuste.Add((ushort)CalculatePowerActive(points, powerReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_4))
                        resultadoAjuste.Add( (ushort)CalculatePowerActive(points, powerReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_5))
                        resultadoAjuste.Add((ushort)CalculatePowerActive(points, powerReference, rang++));

                    if (fase.HasFlag(enumFases.FASE_6))
                        resultadoAjuste.Add((ushort)CalculatePowerActive(points, powerReference, rang++));

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        private double CalculatePowerActive(StatisticalList points, double powerReference, int rang)
        {
            return (points.Average(rang) / powerReference);
        }

        //---------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------

        public List<double> ReadPowerPoints(enumFases fase, TipoPotencia tipoPotencia)
        {
            LogMethod();

            var powerPoints = new List<double>();

            IniciarSesion();

            var actions = new Dictionary<enumFases, Func<bool>>();

            if (tipoPotencia == TipoPotencia.Activa)
            {
                actions.Add(enumFases.FASE_1, Dlms.Read_ActivePoints_L1);
                actions.Add(enumFases.FASE_2, Dlms.Read_ActivePoints_L2);
                actions.Add(enumFases.FASE_3, Dlms.Read_ActivePoints_L3);
                actions.Add(enumFases.FASE_4, Dlms.Read_ActivePoints_L4);
                actions.Add(enumFases.FASE_5, Dlms.Read_ActivePoints_L5);
                actions.Add(enumFases.FASE_6, Dlms.Read_ActivePoints_L6);
            }
            else
            {
                actions.Add(enumFases.FASE_1, Dlms.Read_ReactivePoints_L1);
                actions.Add(enumFases.FASE_2, Dlms.Read_ReactivePoints_L2);
                actions.Add(enumFases.FASE_3, Dlms.Read_ReactivePoints_L3);
                actions.Add(enumFases.FASE_4, Dlms.Read_ReactivePoints_L4);
                actions.Add(enumFases.FASE_5, Dlms.Read_ReactivePoints_L5);
                actions.Add(enumFases.FASE_6, Dlms.Read_ReactivePoints_L6);
            }

            foreach (KeyValuePair<enumFases, Func<bool>> items in actions)
                if (fase.HasFlag(items.Key))
                {
                    if (!items.Value())
                        throw new Exception(string.Format("Error de comunicaciones DLMS al al leer puntos de potencia {0} de la {1}", tipoPotencia.ToString(), items.Key.ToString()));

                    var position = Convert.ToByte(items.Key.GetDescription());

                    powerPoints.Add(tipoPotencia == TipoPotencia.Activa ? Dlms.ActivePoints[position] : Dlms.ReactivePoints[position]);
                }

            return powerPoints;
        }

        public void WriteGapGain(enumFases fase, TypeMeterConection.frequency freq, List<short> phaseOffset)
        {
            LogMethod();
          
             var descfreq = freq == TypeMeterConection.frequency._50Hz ? "50Hz" : "60Hz";

             Dlms.PhaseOffset50 = phaseOffset.ToArray();
             Dlms.PhaseOffset60 = phaseOffset.ToArray();

             var actions = new Dictionary<enumFases, Func<string, bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Write_PhaseOffset_L1);
             actions.Add(enumFases.FASE_2, Dlms.Write_PhaseOffset_L2);
             actions.Add(enumFases.FASE_3, Dlms.Write_PhaseOffset_L3);
             actions.Add(enumFases.FASE_4, Dlms.Write_PhaseOffset_L4);
             actions.Add(enumFases.FASE_5, Dlms.Write_PhaseOffset_L5);
             actions.Add(enumFases.FASE_6, Dlms.Write_PhaseOffset_L6);

            IniciarSesion();

            foreach (KeyValuePair<enumFases, Func<string, bool>> items in actions)
                if (fase.HasFlag(items.Key))
                    if (!items.Value(descfreq))
                        throw new Exception(string.Format("Error de comunicaciones DLMS  al grabar desfase de L{0} a {1}", items.Key.ToString(), descfreq));

            Thread.Sleep(2000);
        }

        //---------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------

        public List<double> ReadVoltagePoints(enumFases fase)
        {
            LogMethod();

            var voltagePoints = new List<double>();

            IniciarSesion();

            voltagePoints.Clear();

            var actions = new Dictionary<enumFases, Func<bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Read_VoltagePoints_L1);
            actions.Add(enumFases.FASE_2, Dlms.Read_VoltagePoints_L2);
            actions.Add(enumFases.FASE_3, Dlms.Read_VoltagePoints_L3);
            actions.Add(enumFases.FASE_4, Dlms.Read_VoltagePoints_L4);
            actions.Add(enumFases.FASE_5, Dlms.Read_VoltagePoints_L5);
            actions.Add(enumFases.FASE_6, Dlms.Read_VoltagePoints_L6);

            foreach (KeyValuePair<enumFases, Func<bool>> items in actions)
                if (fase.HasFlag(items.Key))
                {
                    if (!items.Value())
                        throw new Exception(string.Format("Error de comunicaciones DLMS  al leer puntos de voltage de {0}", items.Key.ToString()));

                    var position = Convert.ToByte(items.Key.GetDescription());

                    voltagePoints.Add(Dlms.VoltagePoints[position]);
                }

            return voltagePoints;
        }

        public void WriteVoltageGain(enumFases fase, List<ushort> voltageGain)
        {
            LogMethod();

            Dlms.VoltageGain = voltageGain.ToArray();

            var actions = new Dictionary<enumFases, Func<bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Write_VoltageGain_L1);
            actions.Add(enumFases.FASE_2, Dlms.Write_VoltageGain_L2);
            actions.Add(enumFases.FASE_3, Dlms.Write_VoltageGain_L3);
            actions.Add(enumFases.FASE_4, Dlms.Write_VoltageGain_L4);
            actions.Add(enumFases.FASE_5, Dlms.Write_VoltageGain_L5);
            actions.Add(enumFases.FASE_6, Dlms.Write_VoltageGain_L6);

            foreach (KeyValuePair<enumFases, Func<bool>> items in actions)
                if (fase.HasFlag(items.Key))
                    this.Retry(() =>
                    {
                        if (!items.Value())
                        {
                            IniciarSesion();
                            throw new Exception(string.Format("Error de comunicaciones DLMS  al grbar ganacia de voltage de {0}", items.Key.ToString()));
                        }
                    }, 3);

            Thread.Sleep(1000);
        }

        //---------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------

        public void WritePowerActiveGain(enumFases fase, TypeMeterConection.frequency freq, List<ushort> powerActiveGain)
        {
            LogMethod();

            var descfreq = freq == TypeMeterConection.frequency._50Hz ? "50Hz" : "60Hz";

            Dlms.ActiveGain50 = powerActiveGain.ToArray();
            Dlms.ActiveGain60 = powerActiveGain.ToArray();

            var actions = new Dictionary<enumFases, Func<string, bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Write_ActiveGain_L1);
            actions.Add(enumFases.FASE_2, Dlms.Write_ActiveGain_L2);
            actions.Add(enumFases.FASE_3, Dlms.Write_ActiveGain_L3);
            actions.Add(enumFases.FASE_4, Dlms.Write_ActiveGain_L4);
            actions.Add(enumFases.FASE_5, Dlms.Write_ActiveGain_L5);
            actions.Add(enumFases.FASE_6, Dlms.Write_ActiveGain_L6);

            foreach (KeyValuePair<enumFases, Func<string, bool>> items in actions)
                if (fase.HasFlag(items.Key))
                    this.Retry(() =>
                    {
                        if (!items.Value(descfreq))
                        {
                            IniciarSesion();
                            throw new Exception(string.Format("Error de comunicaciones DLMS al grabar ganancia de activa de la {0}", items.Key.ToString()));
                        }
                    }, 3);

            Thread.Sleep(1000);
        }

        public void WritePowerReactiveGain(enumFases fase, TypeMeterConection.frequency freq, List<ushort> powerReactiveGain)
        {
            LogMethod();

            var descfreq = freq == TypeMeterConection.frequency._50Hz ? "50Hz" : "60Hz";

            Dlms.ReactiveGain50 = powerReactiveGain.ToArray();
            Dlms.ReactiveGain60 = powerReactiveGain.ToArray(); //RELLENO LAS DOS PERO DESPUES SOLO GRABO LA QUE TOCA

            var actions = new Dictionary<enumFases, Func<string, bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Write_ReactiveGain_L1);
            actions.Add(enumFases.FASE_2, Dlms.Write_ReactiveGain_L2);
            actions.Add(enumFases.FASE_3, Dlms.Write_ReactiveGain_L3);

            foreach (KeyValuePair<enumFases, Func<string, bool>> items in actions)
                if (fase.HasFlag(items.Key))
                    this.Retry(() =>
                    {
                        if (!items.Value(descfreq))
                        {
                            IniciarSesion();
                            throw new Exception(string.Format("Error de comunicaciones DLMS al grabar ganancia de activa de la {0}", items.Key.ToString()));
                        }
                    }, 3);

            Thread.Sleep(1000);
        }

        public List<ushort> ReadPowerGain(enumFases fase, TypeMeterConection.frequency freq, TipoPotencia tipoPotencia)
        {
            LogMethod();

            var gainPower = new List<ushort>();

            var descfreq = freq == TypeMeterConection.frequency._50Hz ? "50Hz" : "60Hz";

            IniciarSesion();

            var actions = new Dictionary<enumFases, Func<string, bool>>();

            if (tipoPotencia == TipoPotencia.Activa)
            {
                actions.Add(enumFases.FASE_1, Dlms.Read_ActiveGain_L1);
                actions.Add(enumFases.FASE_2, Dlms.Read_ActiveGain_L2);
                actions.Add(enumFases.FASE_3, Dlms.Read_ActiveGain_L3);
                actions.Add(enumFases.FASE_4, Dlms.Read_ActiveGain_L4);
                actions.Add(enumFases.FASE_5, Dlms.Read_ActiveGain_L5);
                actions.Add(enumFases.FASE_6, Dlms.Read_ActiveGain_L6);
            }
            else
            {
                actions.Add(enumFases.FASE_1, Dlms.Read_ReactiveGain_L1);
                actions.Add(enumFases.FASE_2, Dlms.Read_ReactiveGain_L2);
                actions.Add(enumFases.FASE_3, Dlms.Read_ReactiveGain_L3);
                //actions.Add(enumFases.FASE_3, Dlms.Read_ReactiveGain_L4);
                //actions.Add(enumFases.FASE_3, Dlms.Read_ReactiveGain_L5);
                //actions.Add(enumFases.FASE_3, Dlms.Read_ReactiveGain_L6);
            }

            foreach (KeyValuePair<enumFases, Func<string, bool>> items in actions)
                if (fase.HasFlag(items.Key))
                {
                    if (!items.Value(descfreq))
                        throw new Exception(string.Format("Error de comunicaciones DLMS al grabar ganancia de {0} de la {1}", tipoPotencia.ToString(), items.Key.ToString()));

                    var position = Convert.ToByte(items.Key.GetDescription());

                    if (tipoPotencia == TipoPotencia.Activa)
                    {
                        var gainActive = freq == TypeMeterConection.frequency._50Hz ? Dlms.ActiveGain50[position] : Dlms.ActiveGain60[position];
                        gainPower.Add(gainActive);
                    }
                    else
                    {
                        var gainReactive = freq == TypeMeterConection.frequency._50Hz ? Dlms.ReactiveGain50[position] : Dlms.ReactiveGain60[position];
                        gainPower.Add(gainReactive);
                    }
                }

            return gainPower;
        }

        //---------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------

        public void WriteZeroOffsetActiveGain(enumFases fase)
        {
            WriteOffsetActiveGain(fase, new List<int>() { 0, 0, 0, 0 });
        }

        private void WriteOffsetActiveGain(enumFases fase, List<int> offsetGain)
        {
            LogMethod();

            Dlms.ActiveOffset = offsetGain.ToArray();

            var actions = new Dictionary<enumFases, Func<bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Write_ActiveOffset_L1);
            actions.Add(enumFases.FASE_2, Dlms.Write_ActiveOffset_L2);
            actions.Add(enumFases.FASE_3, Dlms.Write_ActiveOffset_L3);
            actions.Add(enumFases.FASE_4, Dlms.Write_ActiveOffset_L4);
            actions.Add(enumFases.FASE_5, Dlms.Write_ActiveOffset_L5);
            actions.Add(enumFases.FASE_6, Dlms.Write_ActiveOffset_L6);

            foreach (KeyValuePair<enumFases, Func<bool>> items in actions)
                if (fase.HasFlag(items.Key))
                    this.Retry(() =>
                    {
                        if (!items.Value())
                        {
                            IniciarSesion();
                            throw new Exception(string.Format("Error de comunicaciones DLMS al grabar ganancia de Offset de activa de la {0}", items.Key.ToString()));
                        }
                    
                    }, 3);

            Thread.Sleep(1000);
        }

        public void WriteZeroOffsetReacctiveGain(enumFases fase)
        {
            WriteOffsetActiveGain(fase, new List<int>() { 0, 0, 0, 0 });
        }

        private void WriteOffsetReacctiveGain(enumFases fase, List<int> offsetGain)
        {
            LogMethod();

            Dlms.ReactiveOffset = offsetGain.ToArray();

            var actions = new Dictionary<enumFases, Func<bool>>();
            actions.Add(enumFases.FASE_1, Dlms.Write_ReactiveOffset_L1);
            actions.Add(enumFases.FASE_2, Dlms.Write_ReactiveOffset_L2);
            actions.Add(enumFases.FASE_3, Dlms.Write_ReactiveOffset_L3);
           
            foreach (KeyValuePair<enumFases, Func<bool>> items in actions)
                if (fase.HasFlag(items.Key))
                    if (!items.Value())
                        throw new Exception(string.Format("Error de comunicaciones DLMS al grabar ganancia de Offset de activa de la {0}", items.Key.ToString()));
            Thread.Sleep(1000);
        }

        public List<double> Leer_Potencia(enumFases fases, TipoPotencia tipoPotencia)
        {
            LogMethod();

            IniciarSesion();

            var power = new List<double>();

            if (tipoPotencia == TipoPotencia.Activa)
            {
                if (!Dlms.ReadActivePowerHighResolution())
                    throw new Exception("Error de comunicaciones DLMS al leer potencia Actica High Resolution");

                if (fases.HasFlag(enumFases.FASE_1))
                    power.Add(Dlms.HgActivePowerL1);

                if (fases.HasFlag(enumFases.FASE_2))
                    power.Add(Dlms.HgActivePowerL2);

                if (fases.HasFlag(enumFases.FASE_3))
                    power.Add(Dlms.HgActivePowerL3);

                if (fases.HasFlag(enumFases.FASE_4))
                    power.Add(Dlms.HgActivePowerL4);

                if (fases.HasFlag(enumFases.FASE_5))
                    power.Add(Dlms.HgActivePowerL5);

                if (fases.HasFlag(enumFases.FASE_6))
                    power.Add(Dlms.HgActivePowerL6);
            }
            else
            {
                if(!Dlms.ReadReactivePowerHighResolution())
                    throw new Exception("Error de comunicaciones DLMS al leer potencia Reactica High Resolution");

                if (fases.HasFlag(enumFases.FASE_1))
                    power.Add(Dlms.HgReactivePowerL1);

                if (fases.HasFlag(enumFases.FASE_2))
                    power.Add(Dlms.HgReactivePowerL2);

                if (fases.HasFlag(enumFases.FASE_3))
                    power.Add(Dlms.HgReactivePowerL3);

                if (fases.HasFlag(enumFases.FASE_4))
                    power.Add(Dlms.HgReactivePowerL4);

                if (fases.HasFlag(enumFases.FASE_5))
                    power.Add(Dlms.HgReactivePowerL5);

                if (fases.HasFlag(enumFases.FASE_6))
                    power.Add(Dlms.HgReactivePowerL6);
            }

            return power;
        }

        public List<double> Leer_Potencia(enumFases fases, TypeMeterConection.frequency freq, TipoPotencia tipoPotencia)
        {
            LogMethod();

            var point = this.Retry<List<double>>(() => ReadPowerPoints(fases, tipoPotencia), 3);

            var gain = this.Retry<List<ushort>>(() => ReadPowerGain(fases, freq, tipoPotencia), 3);

            var power = new List<double>();

            if (fases.HasFlag(enumFases.FASE_1))
                power.Add(point[0] / gain[0]);

            if (fases.HasFlag(enumFases.FASE_2))
                power.Add(point[1] / gain[1]);

            if (fases.HasFlag(enumFases.FASE_3))
                power.Add(point[2] / gain[2]);

            if (fases.HasFlag(enumFases.FASE_4))
                power.Add(point[3] / gain[3]);

            return power;
        }

        #endregion

        public string LeerCodigoUnesa()
        {
            LogMethod();

            if (!Dlms.Read_UnesaModelCode())
                throw new Exception("Error de comunicaciones al leer codigo UNESA por DLMS");

            return Dlms.UnesaModelCode;         
        }

        public void EscribirCodigoUnesa(string codigoUnesa)
        {
            LogMethod();

            Dlms.UnesaModelCode = codigoUnesa;

            if (!Dlms.Write_UnesaModelCode())
                throw new Exception("Error de comunicaciones al escribir codigo UNESA por DLMS");
        }

        public string LeerVersionSW()
        {
            LogMethod();

            if (!Dlms.Read_FirmwareVersion())
                throw new Exception("Error de comunicaciones al leer versión de firmware por DLMS");

            return Dlms.ActiveFirmwareIdentifier;         
            
        }

        public ushort LeerVersionSWPLC()
        {
            LogMethod();

            if (!Dlms.Read_PlcVersion())
                throw new Exception("Error de comunicaciones al leer versión de firmware del PLC por DLMS");

            return Dlms.PlcVersion;

        }

        public string LeerModelo()
        {
            LogMethod();

            if (!Dlms.Read_MeterModel())
                throw new Exception("Error de comunicaciones al leer modelo por DLMS");

            return Dlms.MeterModel;  
        }

        public double LeerTemperatura()
        {
            LogMethod();

            if (!Dlms.Read_Temperature())
                throw new Exception("Error de comunicaciones al leer temperatura por DLMS");

            return Dlms.Temperature;
        }

        public void EscribirModelo(string model)
        {
            LogMethod();

            Dlms.MeterModel= model;

            if (!Dlms.Write_MeterModel())
                throw new Exception("Error de comunicaciones al escribir modelo por DLMS");
        }

        public uint LeerNumeroFabricacion()
        {
            LogMethod();

            if (!Dlms.Read_FabricationNumber())
                throw new Exception("Error de comunicaciones al leer numero de fabricación por DLMS");

            return Dlms.FabricationNumber;  
        }

        public void EscribirNumeroFabricacion(uint fabricationNumber)
        {
            LogMethod();

            Dlms.FabricationNumber = fabricationNumber;

            if (!Dlms.Write_FabricationNumber())
                throw new Exception("Error de comunicaciones al escribir numero de fabricación por DLMS");
        }

        public uint LeerNumeroSerie()
        {
            LogMethod();

            if (!Dlms.Read_SerialNumber())
                throw new Exception("Error de comunicaciones al leer numero de serie por DLMS");

            return Dlms.NumberSerial;
        }

        public string LeerMACPLC()
        {
            LogMethod();

            if (!Dlms.ReadMACAddress())
                throw new Exception("Error de comunicaciones al leer numero de serie por DLMS");

            return Dlms.MacAddress;
        }

        public double Leer_Temperatura()
        {
            LogMethod();

            if (!Dlms.Read_Temperature())
                throw new Exception("Error de comunicaciones al leer la temperatura del equipo");

            return Dlms.Temperature;
        }

        public short Leer_PPM()
        {
            LogMethod();

            if (!Dlms.Read_ClockPPM())
                throw new Exception("Error de comunicaciones al leer los PPM del equipo");

            return Dlms.ClockPPM;
        }

        public void EscribirNumeroSerie(uint serialNumber)
        {
            LogMethod();

            Dlms.NumberSerial = serialNumber;

            if (!Dlms.Write_SerialNumber())
                throw new Exception("Error de comunicaciones al escribir numero de fabricación por DLMS");
        }

        public void EscribirFechaFabricacion(DateTime fabricationDate)
        {
            LogMethod();

            Dlms.FabricationDate = fabricationDate;

            if (!Dlms.Write_FabricationDate())
                throw new Exception("Error de comunicaciones al escribir fecha de fabricación por DLMS");
        }

        public void EscribirFechaVerificacion(DateTime verificationDate)
        {
            LogMethod();

            Dlms.VerificationDate = verificationDate;

            if (!Dlms.Write_VerificationDate())
                throw new Exception("Error de comunicaciones al escribir fecha de verification por DLMS");
        }

        public void EscribirEstadoFabricacion(ushort fabricationStatus)
        {
            LogMethod();

            Dlms.FabricationStatus = fabricationStatus;

            if (!Dlms.Write_FabricationStatus())
                throw new Exception("Error de comunicaciones al escribir estado de fabricación por DLMS");
        }

        public void EscribirPPM(short PPM)
        {
            LogMethod();

            Dlms.ClockPPM = PPM;

            if (!Dlms.Write_ClockPPM())
                throw new Exception("Error de comunicaciones al escribir PPM reloj por DLMS");
        }

        public short LeerPPM()
        {
            LogMethod();

            if (!dlms.Read_ClockPPM())
                throw new Exception("Error de comunicaciones al leer PPM reloj por DLMS");

            return Dlms.ClockPPM;
        }

        public void EscribirFechaHoraActual()
        {
            LogMethod();

            DateTime MiHora = new DateTime();
            MiHora = DateTime.Now;
            Dlms.DLMS.TypeDateTimeProtocol typeHora = new Dlms.Cosem.TypeDateTimeProtocol();
            typeHora.date.any = (short)MiHora.Year;
            typeHora.date.mes = (short)MiHora.Month;
            typeHora.date.dia = (short)MiHora.Day;
            typeHora.date.diaSemana =  MiHora.DayOfWeek == 0 ? (short)7 : (short)MiHora.DayOfWeek;
            typeHora.time.hora = (short)MiHora.Hour;
            typeHora.time.minuto = (short)MiHora.Minute;
            typeHora.time.segundo = (short)MiHora.Second;
            typeHora.desviacion = 32768;
            typeHora.flagSummer = true;
            Dlms.TimeCurrent = typeHora;
            if (!Dlms.WriteClockTime())
                throw new Exception("Error de comunicaciones al escribir estado de fabricación por DLMS");
        }

        public DateTime LeerFechaHora()
        {
            LogMethod();

            if (!Dlms.ReadClockTime())
                throw new Exception("Error de comunicaciones al escribir estado de fabricación por DLMS");

            DateTime MiHora = new DateTime(Dlms.TimeCurrent.date.any, Dlms.TimeCurrent.date.mes, Dlms.TimeCurrent.date.dia, Dlms.TimeCurrent.time.hora, Dlms.TimeCurrent.time.minuto, Dlms.TimeCurrent.time.segundo);
            return MiHora;
        }

        public ushort LeerCrcBinario()
        {
            LogMethod();

            if (!Dlms.Read_IntegrityCrc())
                throw new Exception("Error de comunicaciones al leer numero de serie por DLMS");

            return Dlms.IntegrityCrc;
        }

        public void EscribirReset()
        {
            LogMethod();

            Dlms.Write_Reset();
        }

        public enum enumStateRelay
        {
            Open = 1,
            Closed = 2,
        }

        public void WriteRelayEnergyControl(byte contador, enumStateRelay state)
        {
            LogMethod();

            Dlms.DisconnectorControl = (byte)state;

            if (!Dlms.SetDisconnectorControl(contador))
                throw new Exception("Error de comunicaciones al escribir RelayEnergyControl por DLMS");
        }

        public void ReadRelayEnergyControl(byte contador)
        {
            LogMethod();

           var disconector= Dlms.ReadDisconnectorControl(contador);
           var disconectorState = Dlms.ReadDisconnectorControlState(contador);
        }

        public void WriteRelayImpulseControl(byte contador, enumStateRelay state)
        {
            LogMethod();

            Dlms.ImpulsesDisconnectorControl = (byte)state;

            if (!Dlms.SetImpulsesDisconnectorControl(contador))
                throw new Exception("Error de comunicaciones al escribir RelayImpulseControl por DLMS");
        }

        public void ReadRelayImpulseControl(byte contador)
        {
            LogMethod();

            var disconector = Dlms.ReadImpulsesDisconnectorControl(contador);
            var disconectorState = Dlms.ReadImpulsesDisconnectorControlState(contador);
        }

        public void ReadTemperature()
        {
            LogMethod();

            dlms.Read_Temperature();
        }

        public enum enumOuputs
        {
            NOTHING=0,
            RELAY1 =1,
            RELAY2 =2,
            LED_PPM =3,
        }

        public void WriteActivateOuputs(enumOuputs output)
        {
            LogMethod();

            Dlms.Ouputs = (ushort)output;

            if (!Dlms.Write_OutputTest())
                throw new Exception("Error de comunicaciones al escribir WriteActivateOuputs por DLMS");
        }

        [Flags]
        public enum enumLeds
        {
            NULL = 0,
            ENERGY_1 = (1 << 0),
            ENERGY_2 = (1 << 1),
            ENERGY_3 = (1 << 2),
            ENERGY_4 = (1 << 3),
            INPUT_1 = (1 << 4),
            INPUT_2 = (1 << 5),
            INPUT_3 = (1 << 6),
            INPUT_4 = (1 << 7),
            SWITCH_1 =(1 << 8),
            SWITCH_2 = (1 << 9),
            SWITCH_3 = (1 << 10),
            SWITCH_4 = (1 << 11),
            PLC_GREEN = (1 << 12),
            PLC_RED= (1 << 13),
            VAR_1 = (1 << 14),
            VAR_2 = (1 << 15),
            VAR_3 = (1 << 16),
            VAR_4 = (1 << 17),
            VAR_5 = (1 << 18),
            ALL_ENERGY = ENERGY_1 | ENERGY_2 | ENERGY_3 | ENERGY_4,
            ALL_INPUTS = INPUT_1 | INPUT_2 | INPUT_3 | INPUT_4,
            ALL_SWITCH = SWITCH_1 | SWITCH_2 | SWITCH_3 | SWITCH_4,
            SEPARATE_LED = ENERGY_1 | ENERGY_3 | SWITCH_2 | SWITCH_4 | INPUT_1 | INPUT_3 | VAR_2,
            REST_SEPARATE_LED = ENERGY_2 | ENERGY_4 | INPUT_2 | INPUT_4 | SWITCH_1 | SWITCH_3 | VAR_1,
        }

        public void WriteActivateLeds(enumLeds led)
        {
            LogMethod();

            Dlms.Ouputs = (ushort)led;
           // dlms.
            if (!Dlms.Write_OutputTest())
                throw new Exception("Error de comunicaciones al escribir WriteActivateOuputs por DLMS");
        }

        public void WriteActivateDisplay(byte display)
        {
            LogMethod();

            Dlms.DisplayTest = display;
            // dlms.
            if (!Dlms.Write_DisplayTest())
                throw new Exception("Error de comunicaciones al escribir WriteActivateDisplay por DLMS");
        }

        [Flags]
        public enum enumKeyInputs
        {
            NOTHING = 0,
            IN1 = (1 << 2),
            IN2 = (1 << 3),
            IN3 = (1 << 4),
            IN4 = (1 << 5),
            REARME = (1 << 6)
        }

        public enumKeyInputs ReadInputsImpulse()
        {
            LogMethod();

            if (!Dlms.Read_InputSatus())
                throw new Exception("Error de comunicaciones al escribir ReadInputsImpulse por DLMS");

            return (enumKeyInputs)Dlms.Inputs;
        }

        public enum enumKeyBoard
        {
            NOTHING=0,
            TECLA1=(1 << 0),
            TECLA2=(1 << 1),
        }

        public enumKeyBoard ReadKeyboard()
        {
            LogMethod();

            if(!Dlms.Read_InputSatus())
                throw new Exception("Error de comunicaciones al leer entradas por DLMS");

            return (enumKeyBoard)Dlms.Inputs;
        }

        public override void Dispose()
        {
            CerrarSesion(false);
            dlms = null;
        }
    }
}
