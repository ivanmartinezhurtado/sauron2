﻿using Dezac.Tests.Forms;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(InitShellActionDescription))]
    
    public class InitShell : ActionBase, IShell
    {
        public override void Execute()
        {
            if (!SequenceContext.Current.Services.Contains<IShell>())
                SequenceContext.Current.Services.Add<IShell>(this);
        }

        public DialogResult MsgBox(string text, string caption)
        {
            return MessageBox.Show(text, caption);
        }

        public DialogResult MsgBox(string text, string caption, MessageBoxButtons buttons)
        {
            return MessageBox.Show(text, caption, buttons);
        }

        public DialogResult MsgBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return MessageBox.Show(text, caption, buttons, icon);
        }

        //*******************************************************************

        public DialogResult ShowDialog(string title, Control control, MessageBoxButtons? buttons, Action<Control> onLoad = null)
        {
            return Task.Factory.StartNew<DialogResult>(() =>
            {
                var form = control is Form ? (Form)control : new MsgBox(title, control, buttons);

                if (onLoad != null)
                    form.Load += (s, e) => { onLoad(form); };

                form.ActiveControl = control;
                return form.ShowDialog();

            }, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler).Result;
        }

        public DialogResult ShowDialog(string title, Func<Control> control, MessageBoxButtons? buttons, string description = null, int waitTime = 500)
        {
            return Task.Factory.StartNew<DialogResult>(() =>
            {
                using (var ctrl = control())
                {
                    var form = ctrl as Form;
                    if (form == null)
                    {
                        form = new MsgBox(title, ctrl, buttons, description, waitTime);
                        form.ActiveControl = ctrl;
                    }
                    return form.ShowDialog();
                }

            }, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler).Result;
        }

        public T ShowDialog<T>(string title, Func<Control> control, MessageBoxButtons? buttons, Func<Control, DialogResult, T> result)
        {
            return Task.Factory.StartNew<T>(() =>
            {
                using (var ctrl = control())
                {
                    var form = ctrl as Form;
                    if (form == null)
                    {
                        form = new MsgBox(title, ctrl, buttons);
                        form.ActiveControl = ctrl;
                    }
                    var dialogResult = form.ShowDialog();

                    return result(ctrl, dialogResult);
                }

            }, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler).Result;
        }

        public Form ShowForm(Func<Control> control)
        {
            return Task.Factory.StartNew<Form>(() =>
            {
                var ctrl = control();

                var form = ctrl as Form;
                if (form == null)
                {
                    form = new MsgBox(ctrl.Text, ctrl, MessageBoxButtons.OK);
                    form.ActiveControl = ctrl;
                }
                form.Show();

                return form;
            }, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler).Result;
        }

        //**********************************************************************

        public void Run(Action action)
        {
            Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler);
        }

        public Task<T> Run<T>(Func<T> action)
        {
            return Task.Factory.StartNew<T>(() =>
            {
                return action();

            }, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler);
        }

        public void RunSync(Action action)
        {
            Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, SequenceContext.Current.InitialTaskScheduler).Wait();
        }

        public DialogResult ShowTableDialog(string title, object data, MessageBoxButtons buttons = MessageBoxButtons.OK)
        {
            return ShowDialog(title, new TableGridView(data), buttons);
        }

        public DialogResult ShowTableDialog(string title, TableGridViewConfig config, MessageBoxButtons buttons = MessageBoxButtons.OK)
        {
            return ShowDialog(title, new TableGridView(config), buttons);
        }

        public T ShowTableDialog<T>(string title, object data)
        {
            return ShowTableDialog<T>(title, new TableGridViewConfig { Data = data });
        }

        public T ShowTableDialog<T>(string title, TableGridViewConfig config)
        {
            using (var tbl = new TableGridView(config))
            {
                if (ShowDialog(title, tbl, MessageBoxButtons.OKCancel) == DialogResult.OK)
                    return tbl.GetCurrent<T>();
            }

            return default(T);
        }

        public T ShowInputKeyboard<T>(string title, string promptText, T defaultValue, string mask = null, string initValue = null)
        {
            using (var ctrl = new InputKeyBoard(promptText, mask, initValue))
            {
                if (ShowDialog(title, ctrl, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    var value = ctrl.TextValue;

                    Type type = typeof(T);

                    if (type.Name == "Nullable`1")
                    {
                        if (value == null || value.ToString() == "")
                            return default(T);

                        return (T)Convert.ChangeType(value, type.GetGenericArguments()[0]);
                    }

                    return (T)Convert.ChangeType(value, type);
                }
            }

            return defaultValue;
        }
    }

    public class InitShellActionDescription : ActionDescription
    {
        public override string Name { get { return "InitShell"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.kernel; } }
    }
}
