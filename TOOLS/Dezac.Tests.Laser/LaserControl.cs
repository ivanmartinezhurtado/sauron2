﻿using Dezac.Core.Exceptions;
using Dezac.Data.ViewModels;
using Dezac.Tests.Laser.Controls;
using Dezac.Tests.Services;
using Instruments.IO;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Laser
{
    public class LaserControl : IDisposable
    {
        public byte InputAlarm { get; set; }

        public byte InputReady { get; set; }

        public byte OutputStart { get; set; }

        public TestBase TestBase { get; set; }

        public void LaserRuning(List<LaserItemViewModel> lineas, USB4761 usbIO)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            LaserStarting.resultLaser hasAlarm = LaserStarting.resultLaser.OK;
            string ErrorMessage = string.Empty;

            var stepLaser = iShell.ShowDialog("INICIANDO MARCADO LASER", () =>
            {
                var ctrl = new LaserStarting();
                ctrl.InputAlarm = InputAlarm;
                ctrl.InputReady = InputReady;
                ctrl.OutputStart = OutputStart;
                ctrl.TestBase = TestBase;
                ctrl.IO = usbIO;
                ctrl.IO.LogEnabled = false;
                ctrl.Lineas = lineas;

                return ctrl;
            }
           , MessageBoxButtons.OKCancel,
            (c, dr) =>
            {
                hasAlarm = ((LaserStarting)c).HasAlarm;
                ErrorMessage = ((LaserStarting)c).ErrorMessage;
                return dr;
            }
           );
            if (ErrorMessage != string.Empty)
                TestException.Create().SOFTWARE.SISMA.CONFIGURACION_INCORRECTA(ErrorMessage).Throw();

            if (hasAlarm == LaserStarting.resultLaser.Alarma)
                TestException.Create().HARDWARE.INSTRUMENTOS.LASER("Alarma Detectada").Throw();

            if (hasAlarm == LaserStarting.resultLaser.Canceled)
                TestException.Create().PROCESO.OPERARIO.TEST_CANCELADO();
        }

        public void Dispose()
        {            
        }
    }
}
