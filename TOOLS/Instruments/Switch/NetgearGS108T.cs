﻿using System.Linq;
using System.Net;

namespace Instruments.Switch
{
    public class NetgearGS108T
    {
        public string Password { get; set; }
        public string IP { get; set; }
        private string cookie;
        private WebClient wc;

        public NetgearGS108T()
        {
        }

        public void SetPort(string ip = "100.0.0.8", string password = "F3rran.18")
        {
            IP = ip;
            Password = password;
            cookie = null;
            wc = GetWebRequest();

            Login();
        }

        public void ChangeStateEthernet(Ethernet eth, State state)
        {
            string url = string.Format("http://{0}/base/system/port/port_cfg_rw.html", IP);
            string data = string.Format("unit_no=1&java_port=&inputBox_interface1=&portDesc=&adminMode={0}&physicalMode=Auto&linkTrap=Enable&frameSize=1518&CBox_1=checkbox&inputBox_interface2=&submt=16&cncel=&err_flag=0&err_msg=&selectedPorts=g{1}%3B&multiple_ports=1", state.ToString(), (byte)eth);
            string result = wc.UploadString(url, data);
        }

        public void Dipose()
        {
            wc.Dispose();
        }

        private void Login()
        {
            string url = string.Format("http://{0}/base/main_login.html", IP);
            string data = string.Format("pwd={0}&login.x=28&login.y=7&err_flag=0&err_msg=", Password);
            string result = wc.UploadString(url, data);

            cookie = wc.ResponseHeaders.GetValues("Set-Cookie").FirstOrDefault().Split(';')[0];
        }

        private WebClient GetWebRequest()
        {
            var wc = new WebClient();

            wc.Headers.Add(HttpRequestHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            wc.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
            wc.Headers.Add(HttpRequestHeader.AcceptLanguage, "es-ES,es;q=0.9");
            wc.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
            wc.Headers.Add(HttpRequestHeader.Pragma, "no-cache");
            wc.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
            wc.Headers.Add(HttpRequestHeader.Cookie, cookie);
            wc.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");

            return wc;
        }

        public enum Ethernet
        {
            ETH1 = 1,
            ETH2 = 2,
            ETH3 = 3,
            ETH4 = 4,
            ETH5 = 5,
            ETH6 = 6,
            ETH7 = 7,
            ETH8 = 8
        }

        public enum State
        {
            Enable,
            Disable
        }
    }
}