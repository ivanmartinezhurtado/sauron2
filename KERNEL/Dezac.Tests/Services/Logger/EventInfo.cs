﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Dezac.Tests.Services.Logger
{
    public class EventInfo
    {
        [XmlAttribute]
        public string Id { get; set; }
        [XmlAttribute]
        public string TimeStamp { get; set; }
        [XmlAttribute]
        public string Level { get; set; }
        [XmlAttribute]
        public string LoggerName { get; set; }
        [XmlAttribute]
        public string Text { get; set; }
        [XmlAttribute]
        public string StepPath { get; set; }
        [XmlAttribute]
        public string Type { get; set; }
        [XmlAttribute]
        public int Depth { get; set; }
        [XmlAttribute]
        public string ThreadName { get; set; }
        [XmlAttribute]
        public string Result { get; set; }
        [XmlAttribute]
        public string ExecutionTime { get; set; }
        private List<EventInfo> events;
        public List<EventInfo> Events
        {
            get
            {
                if (events == null)
                    events = new List<EventInfo>();

                return events;
            }
        }
    }
}
