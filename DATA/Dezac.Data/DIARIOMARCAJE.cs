namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.DIARIOMARCAJE")]
    public partial class DIARIOMARCAJE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DIARIOMARCAJE()
        {
            DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMMARCAJE { get; set; }

        [Required]
        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public int NUMHOJA { get; set; }

        public DateTime FECHA { get; set; }

        public decimal? PRECIO { get; set; }

        public DateTime? TIME_ID { get; set; }

        [StringLength(30)]
        public string USERNAME { get; set; }

        public DateTime? FECHAREGISTRO { get; set; }

        [StringLength(10)]
        public string IDOPERARIOCONTROL { get; set; }

        [StringLength(50)]
        public string COMPUTERNAME { get; set; }

        [StringLength(1)]
        public string CARA { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }

        public virtual OPERARIO OPERARIO1 { get; set; }

        public virtual HOJAMARCAJE HOJAMARCAJE { get; set; }
    }
}
