﻿using Dezac.Device;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{

    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DisposeMidabusDeviceActionDescription))]
    public class DisposeMidabusDeviceAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM";
            var uut = GetVariable(name) as MidabusDevice;

            if (uut != null)
                uut.Dispose();
        }
    }

    public class DisposeMidabusDeviceActionDescription : ActionDescription
    {
        public override string Name { get { return "DisposeMidabusDevice"; } }
        public override string Description { get { return "Cierra el puerto midabus serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/Midabus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
    }
}
