﻿using Dezac.Core.Utility;
using System;
using System.Collections.Generic;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class G_CONTROL : DeviceBase
    {
        public ModbusDeviceSerialPort Modbus { get; set; }

        public G_CONTROL()
        {
        }

        public G_CONTROL(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public void WriteFlagTest(bool value= true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.Flag_Test, value);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            var version = Modbus.ReadRegister((ushort)Registers.soft_version)/100D;
            return version.ToString().Replace(",",".");
        }

        public int ReadErrorCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.coderror);
        }

        public void WriteErrorCode(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.coderror, value);
        }

        public void WriteVectorHardware()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.idhar, 0);
        }

        public void WriteSerialNumber(int sn)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.numserie, sn);
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.numserie); ;
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.bastidor, bastidor);
        }

        public int ReadBastidor()
        {
            LogMethod();
            return  Modbus.ReadInt32((ushort)Registers.bastidor); ;
        }
        public double ReadVL1()
        {
            LogMethod();
            var v1 = Modbus.ReadRegister((ushort)Registers.V_L1) / 10D;
            _logger.InfoFormat("Valor V: {0}", v1);
            return v1;
        }

        public ushort ReadRE()
        {
            LogMethod();
            var re= Modbus.ReadRegister((ushort)Registers.RE);
            _logger.InfoFormat("Valor RE: {0}", re);
            return re;
        }

        public void WriteGainDefault()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.SetGainDefault, true);
            System.Threading.Thread.Sleep(2000);
            Modbus.WriteSingleCoil((ushort)Registers.ApplyGain, true);
            System.Threading.Thread.Sleep(2000);
        }

        public void WriteGainVL1(ushort gain)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SetGainVL1, gain);
            System.Threading.Thread.Sleep(2000);
        }

        public void WriteGainVRE(ushort gain)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SetGainVRE, gain);
            System.Threading.Thread.Sleep(2000);
        }

        public void ApplyGain()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.ApplyGain, true);
            System.Threading.Thread.Sleep(2000);
        }

        public Leds ReadLeds()
        {
            LogMethod();
            return (Leds)Modbus.ReadRegister((ushort)Registers.Leds);
        }

        [Flags]
        public enum Leds
        {
            Off = 0,
            Rojo = 1 << 0,
            Amarillo = 1 << 1,
            Verde = 1 << 2,
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public enum Registers
        {
            V_L1 = 46,
            RE = 44,
            Leds = 128,
            SetGainVL1 = 514,
            SetGainVRE = 512,
            numserie = 256,
            bastidor = 260,
            coderror = 264,
            idhar = 266,
            soft_version = 268,
            Flag_Test = 11000,
            Reset = 2000,
            SetGainDefault = 4096,
            ApplyGain = 4369
        }

        public Tuple<bool, List<double>> AdjustVoltage(double voltageReference, ConfigurationSampler parameters, Func<List<double>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new List<double>();

            var adjustResult = AdjustBase(parameters.InitDelayTime, parameters.SampleNumber, parameters.SampleMaxNumber, parameters.DelayBetweenSamples, 100,
                  () =>
                  {
                      var v1 = ReadVL1();
                      var varlist = new List<double>() { v1 };
                      return varlist.ToArray();
                  },
                  (listValues) =>
                  {
                      var GainV1 = voltageReference * 1000 / listValues.Average(0);
                      _logger.InfoFormat("Ajust formula : {0} * 1000 / {1}", voltageReference, listValues.Average(0));
                      _logger.InfoFormat("Ajust Gain V : {0}", GainV1);
                      var varlist = new List<double>() { GainV1 };
                      return adjustValidation(varlist);
                  });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        public Tuple<bool, List<double>> AdjustResistence(double resistenceReference, ConfigurationSampler parameters, Func<List<double>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new List<double>();

            var adjustResult = AdjustBase(parameters.InitDelayTime, parameters.SampleNumber, parameters.SampleMaxNumber, parameters.DelayBetweenSamples, 100,
                  () =>
                  {
                      var v1 = ReadRE();
                      var varlist = new List<double>() { v1 };
                      return varlist.ToArray();
                  },
                  (listValues) =>
                  {
                      var GainV1 = resistenceReference * 1000 / listValues.Average(0);
                      _logger.InfoFormat("Ajust formula : {0} * 1000 / {1}", resistenceReference, listValues.Average(0));
                      _logger.InfoFormat("Ajust Gain RE: {0}", GainV1);
                      var varlist = new List<double>() { GainV1 };
                      return adjustValidation(varlist);
                  });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }
    }
}
