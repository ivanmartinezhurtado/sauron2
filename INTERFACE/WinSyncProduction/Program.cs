﻿using System;
using System.Linq;
using System.Windows.Forms;
using WinSyncProduction;

namespace WinPublisher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string application = "WinTestPlayer";

            var procs1 = System.Diagnostics.Process.GetProcessesByName("WinSyncProduction");
            if (procs1.Count() > 1)
            {
                MessageBox.Show("Este progroma solo se puede ejecutar en una solo instancia", "!Atención! la aplicación ya esta abierta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (args.Count() > 0)         
                application = args[0];

            var procs = System.Diagnostics.Process.GetProcessesByName(application);
            if (procs.Count() > 1)
            {
                MessageBox.Show("Este progroma solo se puede ejecutar en una solo instancia", "!Atención! la aplicación ya esta abierta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(application));
        }
    }
}
