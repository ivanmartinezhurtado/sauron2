﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.00)]
    public class AR6Test : TestBase
    {
        private AR6 ar6;
        private Tower3 tower;

        private const string IMAGE_PATH = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\AR6\";
        private const byte CONECT_BATTERY = 43;
        private const byte NTC_60_GRADOS = 19;
        private const byte _24V = 14;
        private const byte EXTERN_SUPPLY = 15;
        private const byte MEASURE_VOLTAGE_BATTERY = 18;

        public void TestInitializaion(int port)
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            ar6 = AddInstanceVar(new AR6(port), "UUT");
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", ar6.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", ar6.GetType().Name), ParamUnidad.SinUnidad);

        }

        public void TestComunications()
        {
            SamplerWithCancel((p) => { ar6.FlagTest(); return true; }, "No se ha podido comunicar con el equipo", 10, 1500, 2000);

            Resultado.Set("COMUNICATIONS", "OK", ParamUnidad.SinUnidad);

            var vDSP = ar6.ReadFirmwareVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_DSP, vDSP, Identificacion.VERSION_DSP, Error().SOFTWARE.ERROR_TRANSFERENCIA.VERSION_INCORRECTA("Error, version de DSP incorrecta"), ParamUnidad.SinUnidad);

            var vATMEL = ar6.ReadATMELVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_ATMEL, vATMEL, Identificacion.VERSION_ATMEL, Error().SOFTWARE.ERROR_TRANSFERENCIA.VERSION_INCORRECTA("Error, version de ATMEL incorrecta"), ParamUnidad.SinUnidad);

            var vAPLI = ar6.ReadAPLIVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_APLI, vAPLI, Identificacion.VERSION_APLI, Error().SOFTWARE.ERROR_TRANSFERENCIA.VERSION_INCORRECTA("Error, version de APLI incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestConsumptionEquipmentOff()
        {
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(5, 0.1); //TODO BBDD
            tower.IO.DO.On(CONECT_BATTERY, _24V, 23);
            tower.IO.DO.Off(NTC_60_GRADOS);

            var param = new AdjustValueDef(Params.I_DC.Null.Bateria.Vmin, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpAC);
            }, 0, 6, 1000);


            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(9, 1.5);

            param = new AdjustValueDef(Params.I_DC.Null.Bateria.Vnom, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
            }, 0, 6, 1000);
        }

        public void TestConsumptionEquipmentOn()
        {
            SamplerWithCancel(
                (p) => 
                {
                    ar6.WriteBatteryLevel(AR6.BatteryLevel.CARGADA);
                    return true;
                }, "No se ha podido enviar la carga de la batería", 10, 1500, 2000);

            Delay(25000, "Inicializando el DSP");

            var productoVersion = TestInfo.ProductoVersion; 
            ar6.WriteProductoVersion(TestInfo.ProductoVersion); //SETUPDEFAULT?

            var param = new AdjustValueDef(Params.I_DC.Null.Bateria.Vnom, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
            }, 0, 6, 1000);          
        }

        public void AdjustOffsetCurrent()
        {
            tower.IO.DO.On(EXTERN_SUPPLY);
            Delay(500, "Alimentando equipo");

            ar6.ResetOffsetsCurrent();

            ar6.WriteModeLoadBattery(AR6.LoadModeBattery.WITHOUT_LOAD);
            var expectedOffsets = 0;
            var offsetsReading = ar6.ReadOffsetsCurrent();

            Assert.AreEqual("OFFSET_CURRENT", offsetsReading, expectedOffsets, Error().UUT.CONFIGURACION.SETUP("Error. La escritura de offset de corriente a cero no se ha realizado."), ParamUnidad.Puntos);

            var offsetsPoints = ar6.ReadOffsetsCurrentPoints();

            var offsets = 485 - offsetsPoints;

            ar6.WriteOffsetsCurrent(offsets);

            var param = new AdjustValueDef(Params.I_DC.Null.Offset, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadBatteryCurrent();
            }, 0, 6, 1000);
            //entre 0 y 40.

            tower.IO.DO.Off(EXTERN_SUPPLY);
            Delay(500, "Alimentando equipo");

        }

        public void AdjustMeasureCurrent(int delFirst = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            ar6.WriteGainsCurrentMeasure(1000);//por defecto = 1000
            Delay(200, "");
            ar6.WriteBatteryLevel(AR6.BatteryLevel.CARGADA);
            ar6.WriteModeLoadBattery(AR6.LoadModeBattery.FAST_LOAD);

            double currentPattern = 0;

            SamplerWithCancel((p) => 
            {
                currentPattern = tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
                return true;
            }, "No se ha podido comunicar con el equipo", 10, 1500, 2000);

            var def = new AdjustValueDef(Params.POINTS.Null.TestPoint("AJUSTE_MEDIDA_CORRIENTE"), ParamUnidad.Puntos);
            var result = ar6.AjusteMedidaCorriente(delFirst, initCount, samples, timeInterval, currentPattern,
                (value) =>
                {
                    def.Value = value;

                    return def.IsValid();
                });

            Assert.IsTrue(result.Item1, Error().UUT.AJUSTE.MARGENES("Error. valores de ajuste de medida de corriente fuera de margenes"));

            var tolerancia = Params.I.Null.TestPoint("VERIF_MEDIDA_CORRIENTE").Tol();
            var param = new AdjustValueDef(Params.I.Null.TestPoint("VERIF_MEDIDA_CORRIENTE").Name, currentPattern, 0, 0, 0, tolerancia, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadBatteryCurrent();
            }, 1, 6, 1000);

        }

        public void AdjustBatteryVoltage(int delFirst = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            tower.IO.DO.On(MEASURE_VOLTAGE_BATTERY);

            ar6.WriteGainsBatteryVoltage(1000);//por defecto = 1000
            Delay(200, "");

            double voltagePattern = 0;

            SamplerWithCancel((p) =>
            {
                voltagePattern = tower.MeasureMultimeter(InputMuxEnum.IN7, MagnitudsMultimeter.VoltDC);
                return true;
            }, "No se ha podido comunicar con el equipo", 10, 1500, 2000);

            var def = new AdjustValueDef(Params.POINTS.Null.TestPoint("AJUSTE_TENSION_BATERIA"), ParamUnidad.Puntos);
            var result = ar6.AjusteVoltageBateria(delFirst, initCount, samples, timeInterval, voltagePattern,
                (value) =>
                {
                    def.Value = value;

                    return def.IsValid();
                });

            Assert.IsTrue(result.Item1, Error().UUT.AJUSTE.MARGENES("Error. valores de ajuste del voltage de bateria fuera de margenes"));

            var tolerancia = Params.I.Null.TestPoint("VERIF_TENSION_BATERIA").Tol();
            var param = new AdjustValueDef(Params.I.Null.TestPoint("VERIF_TENSION_BATERIA").Name, voltagePattern, 0, 0, 0, tolerancia, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadBatteryVoltage();
            }, 1, 6, 1000);

            tower.IO.DO.Off(MEASURE_VOLTAGE_BATTERY);
        }

        public void TestVerificationTemperature()
        {
            var tempTolerance = Margenes.GetDouble(Params.TEMP.Null.Verificacion.Name, 20, ParamUnidad.PorCentage);
            var tempPatron = Consignas.GetDouble(Params.TEMP.Null.PATRON.TestPoint("25_GRADOS").Name, 25, ParamUnidad.Grados);
            var param = new AdjustValueDef(Params.TEMP.Null.Verificacion.TestPoint("25_GRADOS").Name, tempPatron, 0, 0, 0, tempTolerance, ParamUnidad.Grados);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadTemperatureBattery();
            }, 0, 6, 1000);

            tower.IO.DO.On(NTC_60_GRADOS);

            tempPatron = Consignas.GetDouble(Params.TEMP.Null.PATRON.TestPoint("60_GRADOS").Name, 60, ParamUnidad.Grados);
            param = new AdjustValueDef(Params.TEMP.Null.Verificacion.TestPoint("60_GRADOS").Name, tempPatron, 0, 0, 0, tempTolerance, ParamUnidad.Grados);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadTemperatureBattery();
            }, 0, 6, 1000);

            tower.IO.DO.Off(NTC_60_GRADOS);
        }

        public void TestVerificationBatteryLoad()
        {
            ar6.WriteModeLoadBattery(AR6.LoadModeBattery.FAST_LOAD);
            Delay(10000, "Esperando al equipo");

            var param = new AdjustValueDef(Params.I.Null.Bateria.TestPoint("CARGA_RAPIDA").Name, 2000, 0, 0, 0, 10, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadBatteryCurrent();
            }, 0, 6, 1000);


            ar6.WriteModeLoadBattery(AR6.LoadModeBattery.SLOW_LOAD);
            ar6.WriteBattery150mA();
            Delay(500, "Esperando al equipo");

            param = new AdjustValueDef(Params.I.Null.Bateria.TestPoint("CARGA_LENTA").Name, 150, 0, 0, 0, 10, ParamUnidad.mA);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadBatteryCurrent();
            }, 0, 6, 1000);


        }

        public void TestSetupDefault()
        {
            var gainsDefault = new AR6.Gains();
            gainsDefault.V1 = gainsDefault.V2 = gainsDefault.V3 = gainsDefault.VN = (ushort)Configuracion.GetDouble(Params.GAIN_V.Null.TestPoint("DEFAULT").Name, 10800, ParamUnidad.Puntos);
            gainsDefault.I1 = gainsDefault.I2 = gainsDefault.I3 = gainsDefault.IN = (ushort)Configuracion.GetDouble(Params.GAIN_I.Null.TestPoint("DEFAULT").Name, 19600, ParamUnidad.Puntos);
            gainsDefault.VL12 = gainsDefault.VL23 = gainsDefault.VL31 = (ushort)Configuracion.GetDouble(Params.GAIN_V.Null.TestPoint("DEFAULT").Name, 10800, ParamUnidad.Puntos);
            gainsDefault.ILEAK = (ushort)Configuracion.GetDouble(Params.GAIN_I.Null.TestPoint("LEAK_DEFAULT").Name, 14500, ParamUnidad.Puntos);
            gainsDefault.KW1 = gainsDefault.KW2 = gainsDefault.KW3 = (ushort)Configuracion.GetDouble(Params.GAIN_KW.Null.TestPoint("DEFAULT").Name, 15600, ParamUnidad.Puntos);

            ar6.WriteDefaultGains(gainsDefault);
            var x = ar6.ReadGains(AR6.Escalas.AA);
        }

        public void TestReadEPROMS()
        {
            var DictionaryPinzasAndStatePinzas = new Dictionary<AR6.AddresPinzas, AR6.StatePinzas>();
            DictionaryPinzasAndStatePinzas.Add(AR6.AddresPinzas.PINZA_IL1, AR6.StatePinzas.PINZA_I1);
            DictionaryPinzasAndStatePinzas.Add(AR6.AddresPinzas.PINZA_IL2, AR6.StatePinzas.PINZA_I2);
            DictionaryPinzasAndStatePinzas.Add(AR6.AddresPinzas.PINZA_IL3, AR6.StatePinzas.PINZA_I3);
            DictionaryPinzasAndStatePinzas.Add(AR6.AddresPinzas.PINZA_IN, AR6.StatePinzas.PINZA_IN);
            DictionaryPinzasAndStatePinzas.Add(AR6.AddresPinzas.PINZA_IK, AR6.StatePinzas.PINZA_IK);
            
            foreach (var pinza in DictionaryPinzasAndStatePinzas)
            {
                SamplerWithCancel((p) =>
                {
                    var reading = ar6.ReadStatePinza(pinza.Key);
                    if (reading == pinza.Value)
                        return true;

                    if (reading == AR6.StatePinzas.NO_DETECTION)
                        throw new Exception(string.Format("Error, no se ha detectado la pinza {0}", pinza.Key.ToString()));
                    else
                        throw new Exception(string.Format("Error, pinza {0} mal conectada", pinza.Key.ToString()));
                }, "", 5, 1000, 1000, false, false);

                Resultado.Set(pinza.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }

            ar6.WriteSimulationPinzas();

            string[] imageName = { "PINZA_I1", "PINZA_I2", "PINZA_I3", "PINZA_IN", "PINZA_ILEAK"};

            for (int i = 0; i <= 4; i++)
            {
                if (Shell.ShowDialog("Test PINZAS", () => new ImageView("Test Alimentación" + imageName[i].Replace("_", " "), IMAGE_PATH + imageName[i] + ".jpg"), MessageBoxButtons.YesNo, "Conecte la pinza y comprueve el led") != DialogResult.Yes)
                    throw new Exception(string.Format("Error, la {0} no se ha alimentado", imageName[i].Replace("_"," ")));

                Resultado.Set(string.Format("ALIM_{0}", imageName[i]), "OK", ParamUnidad.SinUnidad);
            }        
        }

        public void TestMeasureEmpty(int delFirst = 1, int samples = 3, int timeInterval = 500)
        {
            var ConsignasV = new TriLineValue { L1 = 0, L2 = 0, L3 = 0 };
            var ConsignasI = new TriLineValue { L1 = 0, L2 = 0, L3 = 0 };
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            ar6.WriteGroundConnection(AR6.GroundConection.CONECT);
            var MeasureEmptyV = new TriAdjustValueDef(Params.V.L1.EnVacio.Name, Params.V.Null.EnVacio.Min(), Params.V.Null.EnVacio.Max(), 0, 0, ParamUnidad.mV, true);
            var MeasureEmptyI = new TriAdjustValueDef(Params.I.L1.EnVacio.Name, Params.I.Null.EnVacio.Min(), Params.I.Null.EnVacio.Max(), 0, 0, ParamUnidad.mA, true, false, true);

            var MeasuresEmptiesList = new List<TriAdjustValueDef>();
            MeasuresEmptiesList.Add(MeasureEmptyV);
            MeasuresEmptiesList.Add(MeasureEmptyI);


            TestMeasureBase(MeasuresEmptiesList,
                (step) =>
                {
                    var vars = ar6.ReadAllMeasures();
                    var IF = ar6.ReadILeak();
                    return new double[] 
                    { 
                       vars.V1, vars.V2, vars.V3, vars.VN,
                       vars.I1, vars.I2, vars.I3, vars.IN, IF 
                    };
                }, delFirst, samples, timeInterval);

            ar6.WriteGroundConnection(AR6.GroundConection.DISCONECT);
        }

        public void TestMeasureFrecuency()
        {
            var V = Consignas.GetDouble(Params.V.Null.Offset.Name, 300, ParamUnidad.Puntos);
                       
            var I = Consignas.GetDouble(Params.I.Null.Offset.Name, 5.416, ParamUnidad.Puntos);

            var ConsignasV = new TriLineValue { L1 = V, L2 = V, L3 = V };
            var ConsignasI = new TriLineValue { L1 = I, L2 = I, L3 = I };

            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });           

            var param = new AdjustValueDef(Params.FREQ.Null.Verificacion, ParamUnidad.Hz);
            TestMeasureBase(param,
            (step) =>
            {
                return ar6.ReadFrequency();
            }, 0, 6, 1000);
        }

        public void TestMeasureOffsets(int delFirst = 2, int samples = 4, int timeInterval = 500)
        {
            ar6.WriteGroundConnection(AR6.GroundConection.CONECT);

            var V = Consignas.GetDouble(Params.V.Null.Offset.Name, 300, ParamUnidad.Puntos);

            var I = Consignas.GetDouble(Params.I.Null.Offset.Name, 5.416, ParamUnidad.Puntos);
       
            var ConsignasV = new TriLineValue { L1 = V, L2 = V, L3 = V };
            var ConsignasI = new TriLineValue { L1 = I, L2 = I, L3 = I };

            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var paramOffsetV = new TriAdjustValueDef(new AdjustValueDef(Params.V.L1.Offset, ParamUnidad.Puntos), true);           

            var paramOffsetI = new TriAdjustValueDef(new AdjustValueDef(Params.I.L1.Offset, ParamUnidad.Puntos), true, false, true);

            var paramOffset = new List<TriAdjustValueDef>();
            paramOffset.Add(paramOffsetV);
            paramOffset.Add(paramOffsetI);

            TestMeasureBase(paramOffset,
                (step) =>
                {
                    var vars = ar6.ReadOffsets();
                    return new double[] { vars.V1, vars.V2, vars.V3, vars.VN, vars.I1, vars.I2, vars.I3, vars.IN, vars.IF };
                }, delFirst, samples, timeInterval);

            var paramOffsetVN = new TriAdjustValueDef(Params.V.L1.TestPoint("M_OFFSET").Name, Params.V.LN.Offset.Min(), Params.V.LN.Offset.Max(), 0, 0, ParamUnidad.Puntos, true);           

            TestMeasureBase(paramOffsetVN,
               (step) =>
               {
                   var vars = ar6.ReadOffsets();
                   return new double[] {vars.V1_M, vars.V2_M, vars.V3_M, vars.VN_M };
               }, delFirst, samples, timeInterval);

            ar6.WriteGroundConnection(AR6.GroundConection.DISCONECT);
        }

        public void TestMeasureTHD()
        {
            TriAdjustValueDef CheckingTHDV = new TriAdjustValueDef(Params.THD_V.L1.Verificacion.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage, true);
       
            TestMeasureBase(CheckingTHDV,
                (step) =>
                {
                    var vars = ar6.ReadTHD();
                    return new double[] {vars.V1, vars.V2, vars.V3, vars.VN };
                }, 2, 4, 500);

            TriAdjustValueDef CheckingTHDI = new TriAdjustValueDef(Params.THD_I.L1.Verificacion.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage, true);

            TestMeasureBase(CheckingTHDI,
                (step) =>
                {
                    var vars = ar6.ReadTHD();
                    return new double[] { vars.I1, vars.I2, vars.I3, vars.IN };
                }, 2, 4, 500);
        }

        public void TestShortCircuit(int delFirst = 2, int samples = 4, int timeInterval = 500)
        {
            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 180, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 190, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 200, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1.083, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 3.249, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 5.416, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            
            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, 20/*Params.V.Null.CrucePistas.Tol*/, ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, 20/*Params.I.Null.CrucePistas.Tol*/, ParamUnidad.A);
            crucePistasI.L1.Average = ConsignasI.L1;
            crucePistasI.L2.Average = ConsignasI.L2;
            crucePistasI.L3.Average = ConsignasI.L3;

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = ar6.ReadAllMeasures();
                    return new double[] { vars.V1, vars.V2, vars.V3, 
                        vars.I1, vars.I2, vars.I3 };

                }, delFirst, samples, timeInterval);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestFinish()
        {
            if (ar6 != null)
                ar6.Dispose();

            if (tower != null)
                tower.Dispose();
        }

        #region AJUSTAR   
        public void TestAdjust([Description("Escala = AA | AB | BA | BB")] AR6.Escalas escala = AR6.Escalas.AA, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            //if (escala.Equals(AR6.Escalas.AA) || escala.Equals(AR6.Escalas.AB))
            //    ar6.WriteLowScale(false);
            //else
            //    ar6.WriteLowScale(true);

            bool stateLowScale = escala == AR6.Escalas.AA ? false : escala == AR6.Escalas.AB ? false : true;
            ar6.WriteLowScale(stateLowScale); 

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint(escala.ToString()).Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(escala.ToString()).Name, 5.416, ParamUnidad.A);
            var adjustGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint(escala.ToString()).Name, 0, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, 0, TriLineValue.Create(0, 120, 240));

            var defsTensiones = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_V.L1.Ajuste, ParamUnidad.Puntos, true), true);

            var defsCorrientes = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_I.L1.Ajuste, ParamUnidad.Puntos, true), true, false, true);

            var defsTensionesComp = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_V.L1.Ajuste.TestPoint("COMPUESTAS"), ParamUnidad.Puntos, true));

            var defsPotencias = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_KW.L1.Ajuste, ParamUnidad.Puntos, true));


            var result = ar6.Ajuste(delFirts, initCount, samples, timeInterval, escala, adjustVoltage, adjustCurrent, adjustGap,
                (value) =>
                {
                    defsTensiones.L1.Value = value.V1;
                    defsTensiones.L2.Value = value.V2;
                    defsTensiones.L3.Value = value.V3;
                    defsTensiones.Neutro.Value = value.VN;

                    defsCorrientes.L1.Value = value.I1;
                    defsCorrientes.L2.Value = value.I2;
                    defsCorrientes.L3.Value = value.I3;
                    defsCorrientes.Neutro.Value = value.IN;
                    defsCorrientes.Fugas.Value = value.ILEAK;

                    defsTensionesComp.L1.Value = value.VL12;
                    defsTensionesComp.L2.Value = value.VL23;
                    defsTensionesComp.L3.Value = value.VL31;
                    
                    defsPotencias.L1.Value = value.KW1;
                    defsPotencias.L2.Value = value.KW2;
                    defsPotencias.L3.Value = value.KW3;

                    return !defsTensiones.HasMinMaxError() && !defsCorrientes.HasMinMaxError() && !defsTensionesComp.HasMinMaxError() && !defsPotencias.HasMinMaxError();
                });

            defsTensiones.AddToResults(Resultado);
            defsCorrientes.AddToResults(Resultado);
            defsTensionesComp.AddToResults(Resultado);
            defsPotencias.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, Error().UUT.AJUSTE.MARGENES("Error valores de ajuste fuera de margenes"));
        }

        public void TestAdjustPhase(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {          
            ar6.WriteLowScale(false);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.AjusteDesfase.Name, 5.416, ParamUnidad.A);
            var adjustGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 0, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, 0, TriLineValue.Create(0, 120, 240));

            var defsTensiones = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_V.L1.Ajuste, ParamUnidad.Puntos, true), true);         

            var result = ar6.AjusteDesfase(delFirts, initCount, samples, timeInterval, adjustVoltage, adjustCurrent, adjustGap,
                (value) =>
                {
                    defsTensiones.L1.Value = value.V1;
                    defsTensiones.L2.Value = value.V2;
                    defsTensiones.L3.Value = value.V3;                  

                    return !defsTensiones.HasMinMaxError();
                });

            defsTensiones.AddToResults(Resultado);


            Assert.IsTrue(result.Item1, Error().UUT.AJUSTE.MARGENES("Error valores de ajuste fuera de margenes"));
        }
        #endregion

        #region VERIFICAR

        public void TestVerification([Description("Escala = AA | BA | BA | BB")] AR6.Escalas Escala = AR6.Escalas.AA, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            ar6.WriteGroundConnection(AR6.GroundConection.CONECT);
            ar6.FlagTest();
            ar6.WriteLowScale(false);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint(Escala.ToString()).Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint(Escala.ToString()).Name, 5.416, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.TestPoint(Escala.ToString()).Name, 45, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(Escala.ToString()).Name, 0, 0, 0, toleranciaI, ParamUnidad.A));          

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = ar6.ReadAllVariables().Measures;

                    return new double[] { 
                    vars.KW1,  vars.KW2, vars.KW3,
                    vars.V1, vars.V2,vars.V3, 
                    vars.I1, vars.I2, vars.I3};
                },
                () =>
                {
                    var config = tower.MTEWAT.GetFunctionConfig(1);
                    var ReadWattimeter = tower.MTEWAT.ReadAll();
                    var varsList = new List<double>() 
                    { 
                        ReadWattimeter.PowerActive.L1, ReadWattimeter.PowerActive.L2, ReadWattimeter.PowerActive.L3,  
                        ReadWattimeter.Voltage.L1, ReadWattimeter.Voltage.L2, ReadWattimeter.Voltage.L3, 
                        ReadWattimeter.Current.L1,  ReadWattimeter.Current.L2 ,  ReadWattimeter.Current.L3
                    };

                    

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestVerificationGap(int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            ar6.WriteLowScale(false);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint("GAP").Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint("GAP").Name, 5.416, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint("GAP").Name, 45, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.TestPoint("GAP").Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = ar6.ReadAllVariables().Measures;

                    return new double[]
                    {
                    vars.KW1,  vars.KW2, vars.KW3, vars.V1, vars.V2, vars.V3, vars.I1, vars.I2, vars.I3
                    };
                },
                () =>
                {
                    var config = tower.MTEWAT.GetFunctionConfig(1);
                    var ReadWattimeter = tower.MTEWAT.ReadAll();
                    var varsList = new List<double>() { 
                         ReadWattimeter.PowerActive.L1 , ReadWattimeter.PowerActive.L2 , ReadWattimeter.PowerActive.L3};

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }
        #endregion

    }
}
