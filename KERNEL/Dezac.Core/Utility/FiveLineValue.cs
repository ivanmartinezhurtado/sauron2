﻿using System.Collections.Generic;

namespace Dezac.Core.Utility
{
    public struct FiveLineValue
    {
        public readonly static FiveLineValue Zero = new FiveLineValue();

        public TriLineValue LINES { get; set; }
        public double LN { get; set; }
        public double LK { get; set; }

        public static FiveLineValue Create(FourLineValue fourLines, double lK)
        {
            return new FiveLineValue { LINES = fourLines.LINES, LN = fourLines.LN, LK = lK };
        }
        public static FiveLineValue Create(TriLineValue triLines, double vN, double lK)
        {
            return new FiveLineValue { LINES = triLines, LN = vN, LK = lK };
        }
        public static FiveLineValue Create(double v1, double v2, double v3, double vN, double lK)
        {
            return new FiveLineValue { LINES = TriLineValue.Create(v1, v2, v3), LN = vN, LK= lK };
        }
        public static FiveLineValue Create(double v)
        {
            return new FiveLineValue { LINES = TriLineValue.Create(v), LN = v, LK=v };
        }

        public static bool operator ==(FiveLineValue x, FiveLineValue y)
        {
            return x.LINES == y.LINES && x.LN == y.LN && x.LK == y.LK;
        }

        public static bool operator !=(FiveLineValue x, FiveLineValue y)
        {
            return x.LINES != y.LINES || x.LN != y.LN || x.LK != y.LK;
        }

        public override bool Equals(object obj)
        {
            return (LINES == ((FiveLineValue)obj).LINES) && (LN == ((FiveLineValue)obj).LN) && (LK == ((FiveLineValue)obj).LK);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public static FiveLineValue operator *(double x, FiveLineValue y)
        {
            y.LINES *= TriLineValue.Create(x);
            y.LN *= x;
            y.LK *= x;
            return y;
        }

        public static FiveLineValue operator *(FiveLineValue y, double x)
        {
            y.LINES *= TriLineValue.Create(x);
            y.LN *= x;
            y.LK *= x;
            return y;
        }

        public static FiveLineValue operator -(FiveLineValue x, FiveLineValue y)
        {
            x.LINES -= y.LINES;
            x.LN -= y.LN;
            x.LK -= y.LK;
            return x;
        }

        public static implicit operator FiveLineValue(double value)
        {
            return new FiveLineValue { LINES = TriLineValue.Create(value), LN = value, LK=value };
        }

        public override string ToString()
        {
            return string.Format("{0},LN={1},LK={2}", LINES.ToString(), LN, LK);
        }

        public double[] ToArray()
        {
            var list = new List<double>();
            list.AddRange(TriLineValue.ToArray(LINES));
            list.Add(LN);
            list.Add(LK);
            return list.ToArray();
        }

        public static double[] ToArray(FiveLineValue value)
        {
            var list = new List<double>();
            list.AddRange(TriLineValue.ToArray(value.LINES));
            list.Add(value.LN);
            list.Add(value.LK);
            return list.ToArray();
        }

        public static bool isNotZero(FiveLineValue x)
        {
            return TriLineValue.isNotZero(x.LINES) && x.LN != 0 && x.LK != 0;
        }

        public static double vLNCALC { get; set; }
    }
}
