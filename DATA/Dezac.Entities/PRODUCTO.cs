namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PRODUCTO
    {
        public PRODUCTO()
        {
            this.PRODUCTOFASE = new HashSet<PRODUCTOFASE>();
            this.PRODUCTOCOMPONENTE = new HashSet<PRODUCTOCOMPONENTE>();
            this.T_TEST = new HashSet<T_TEST>();
            this.T_VALORPARAMETROPRODUCTO = new HashSet<T_VALORPARAMETROPRODUCTO>();
            this.PRODUCTOBINARIO = new HashSet<PRODUCTOBINARIO>();
            this.T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
            this.T_TESTSEQUENCEFILE = new HashSet<T_TESTSEQUENCEFILE>();
        }
    
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public Nullable<System.DateTime> FECHAVERSION { get; set; }
        public string SITUACION { get; set; }
        public string ACTIVO { get; set; }
        public string IDALMACENPRODUCCION { get; set; }
        public string NOTASVERSION { get; set; }
        public Nullable<int> MINIMOFABRICACION { get; set; }
        public Nullable<int> NUMSOLDADURAS { get; set; }
        public string NOTAS { get; set; }
        public string IDGFH { get; set; }
        public Nullable<decimal> TIEMPOFABRICACION { get; set; }
        public string PRESERIE { get; set; }
        public Nullable<System.DateTime> FECULTREVPRECIO { get; set; }
        public string AVISOMARCAJEFASE { get; set; }
        public string AUTOPARTE { get; set; }
        public string PRODUCTODECOMPRA { get; set; }
        public Nullable<int> UDSCUBETA { get; set; }
        public string ENTRAMADO { get; set; }
        public string TRAZABILIDAD { get; set; }
        public Nullable<int> LOTEOPTIMO { get; set; }
        public string IDFAMILIA { get; set; }
        public Nullable<int> NUMFAMILIA { get; set; }
        public string LISTADISTRIBAVISOMARC { get; set; }
        public string AVISOPREPROD { get; set; }
        public string MATRICULASHIJAS { get; set; }
        public string EMPLEADOVALIDAPRODUC { get; set; }
        public string NOTASVALIDAPRODUC { get; set; }
        public string EMPLEADORESPONSABLE { get; set; }
        public string EMPLEADOVALIDAID { get; set; }
        public string NOTASVALIDAID { get; set; }
        public string IDTEST { get; set; }
        public Nullable<int> NUMDIRECCION { get; set; }
        public string ESPEJOSMD { get; set; }
    
        public virtual ICollection<PRODUCTOFASE> PRODUCTOFASE { get; set; }
        public virtual T_FAMILIA T_FAMILIA { get; set; }
        public virtual ICollection<PRODUCTOCOMPONENTE> PRODUCTOCOMPONENTE { get; set; }
        public virtual ICollection<T_TEST> T_TEST { get; set; }
        public virtual ICollection<T_VALORPARAMETROPRODUCTO> T_VALORPARAMETROPRODUCTO { get; set; }
        public virtual ARTICULO ARTICULO { get; set; }
        public virtual ICollection<PRODUCTOBINARIO> PRODUCTOBINARIO { get; set; }
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }
        public virtual ICollection<T_TESTSEQUENCEFILE> T_TESTSEQUENCEFILE { get; set; }
    }
}
