using System;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Windows.Forms.DataVisualization.Charting;
using Instruments.Utility;
using Instruments.PowerSource;

namespace Dezac.Instruments
{
	public partial class GraphForm : Form
	{
        private FixedList<DataTimePoint> points;

        private double prevValue;

        public Func<double> ReadValue { internal get; set; }

		public GraphForm()
		{
			InitializeComponent();

            points = new FixedList<DataTimePoint>(200);

			Font = SystemInformation.MenuFont;

            chart.Series[0].Points.DataBindY(points, "Value");
            
            //m.Interval = 500;
            //m.ReadValue = () => {
            //    return ReadValue();
            //};
            //m.ValueReaded += new EventHandler<MonitorSignal.MonitorValueEventArgs>(m_ValueReaded);

		}

        public String Variable { set { lvDetails.Columns[0].Text = value; } }

        public void AddValue(double value)
        {
            AddValue(new DataTimePoint { Time = DateTime.Now, Value = value });
        }

        public void AddValue(DataTimePoint value)
        {
            points.Add(value);
            chart.Series[0].Points.DataBindY(points, "Value");

            if (prevValue != value.Value)
            {
                var item = lvDetails.Items.Add(value.Value.ToString());
                item.SubItems.Add(value.Time.ToString("HH:mm:ss"));
                prevValue = value.Value;
            }
        }

        private double GetAsDouble(TextBox txt)
        {
            if (string.IsNullOrEmpty(txt.Text))
                return 0;

            return Convert.ToDouble(txt.Text);
        }
	}

    public class DataTimePoint
    {
        public double Value { get; set; }
        public DateTime Time { get; set; }
    }

    public class FixedList<T> : List<T>
    {
        public int Size { get; private set; }

        public FixedList(int size)
        {
            Size = size;
        }

        public new void Add(T obj)
        {
            base.Add(obj);
            lock (this)
            {
                while (base.Count > Size)
                    base.RemoveAt(0);
            }
        }
    }

    public static class DataPointCollectionExtensions
    {
        public static void AddFixed(this DataPointCollection points, double value, int max)
        {
            points.Add(value);
            while (points.Count > max)
                points.RemoveAt(0);
        }

        public static double GetMedia(this IList<double> list)
        {
            double t = 0;
            int n = list.Count;

            if (n == 0)
                return 0;

            for(int i = 0; i < n; i ++)
            {
                double y = list[i];
                t += y * y;
            }

            return Math.Sqrt(t / n);
        }
    }
}
