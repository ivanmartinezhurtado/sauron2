namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PARTEORDENEQUIPO
    {
        public int NUMPARTE { get; set; }
        public int NUMORDEN { get; set; }
        public string NROSERIE { get; set; }
    }
}
