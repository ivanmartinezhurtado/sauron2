﻿using System;
using TaskRunner.Model;

namespace Dezac.Tests.Services
{
    public interface IInstanceResolveService
    {
        object Resolve(Type type);
        object Resolve(Type type, SequenceContext context);
    }
}
