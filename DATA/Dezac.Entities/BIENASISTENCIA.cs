namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class BIENASISTENCIA
    {
        public int NUMASISTENCIA { get; set; }
        public Nullable<int> NUMBIEN { get; set; }
        public string IDGFH { get; set; }
        public Nullable<System.DateTime> FECHAALTA { get; set; }
        public Nullable<int> NUMINCIDENCIA { get; set; }
        public string ABIERTA_SN { get; set; }
        public Nullable<System.DateTime> FECHACIERRE { get; set; }
        public string IDEMPLEADO { get; set; }
        public string DESCRIPCION { get; set; }
        public string NOTAS { get; set; }
        public Nullable<int> NUMPROGRAMACION { get; set; }
        public string TIPOASISTENCIA { get; set; }
        public string REPARACION { get; set; }
        public string CALIBRACION { get; set; }
        public string PRESERIE { get; set; }
    
        public virtual BIEN BIEN { get; set; }
    }
}
