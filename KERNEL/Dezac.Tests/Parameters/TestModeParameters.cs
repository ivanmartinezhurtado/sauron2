﻿using Dezac.Tests.Model;
using System;

namespace Dezac.Tests.Parameters
{
    public class TestModeParameters
    {
        ParamValueCollection context;
        private ExtensionParameters mgex;

        public string variable { get { return mgex.variable; } set { mgex.variable = value; } }

        public string testPoint { get { return mgex.testPoint; } set { mgex.testPoint = value; } }

        public string Fase { get { return mgex.Fase; } set { mgex.Fase = value; } }

        public ParamUnidad Unidad { get { return mgex.Unidad; } set { mgex.Unidad = value; } }

        public ExtensionParameters esc_200mA { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_200mA; return mgex; } }

        public ExtensionParameters esc_20mA { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_20mA; return mgex; } }

        public ExtensionParameters esc_5A { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_5A; return mgex; } }

        public ExtensionParameters esc_1A { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_1A; return mgex; } }

        public ExtensionParameters esc_MC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_MC; return mgex; } }

        public ExtensionParameters esc_1 { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_1; return mgex; } }

        public ExtensionParameters esc_2 { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_2; return mgex; } }

        public ExtensionParameters esc_3 { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_3; return mgex; } }

        public ExtensionParameters esc_4 { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_4; return mgex; } }

        public ExtensionParameters esc_1_DC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_1_DC; return mgex; } }

        public ExtensionParameters esc_2_DC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_2_DC; return mgex; } }

        public ExtensionParameters esc_3_DC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_3_DC; return mgex; } }

        public ExtensionParameters esc_4_DC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_4_DC; return mgex; } }

        public ExtensionParameters esc_1V { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_1V; return mgex; } }

        public ExtensionParameters esc_2V { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_2V; return mgex; } }

        public ExtensionParameters esc_333mV { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_333mV; return mgex; } }

        public ExtensionParameters esc_1_28V { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_1_28V; return mgex; } }

        public ExtensionParameters esc_DC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_DC; return mgex; } }

        public ExtensionParameters esc_AC { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.ESCALA_AC; return mgex; } }

        public ExtensionParameters calc { get { mgex.poweSourceLevel = ConstantsParameters.TestPoints.CALC; return mgex; } }

        public ExtensionParameters Vmin { get{ mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.VMIN;  return mgex;} }
       
        public ExtensionParameters Vmax { get{ mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.VMAX;  return mgex;} }
        
        public ExtensionParameters Vnom { get{ mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.VNOM;  return mgex;} }

        public ExtensionParameters Q2 { get { mgex.poweSourceLevel = ConstantsParameters.PoweSourceLevel.Q2; return mgex; } }


        public string Name { get { return variable + Fase + testPoint; } }


        public double Min(double defaultValue = -9999999)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + "_MIN", defaultValue, Unidad));
        }

        public double Max(double defaultValue = 9999999)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + "_MAX", defaultValue, Unidad));
        }

        public double Avg(double defaultValue = 1)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + "_AVG", defaultValue, Unidad));
        }

        public double Tol(double defaultValue = 5)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + "_TOL", defaultValue, ParamUnidad.PorCentage));
        }

        public double Var(double defaultValue = 999)
        {
            return Convert.ToDouble(context.GetDouble(variable + Fase + testPoint + "_VAR", defaultValue, ParamUnidad.SinUnidad));
        }

        public ExtensionParameters TestPoint(string value)
        {
            if (string.IsNullOrEmpty(value))
                value = "";
            else
                value = "_" + value.TrimStart('_');

            mgex.poweSourceLevel = value;
            return mgex;
        }

        public ExtensionParameters modo(string value)
        {
            if (string.IsNullOrEmpty(value))
                value = "";
            else
                value = "_" + value.TrimStart('_');

            mgex.poweSourceLevel = value;
            return mgex;
        }

        public TestModeParameters(ParamValueCollection contextmodel)
        {
            context = contextmodel;
            mgex = new ExtensionParameters(contextmodel);
        }

    }
}
