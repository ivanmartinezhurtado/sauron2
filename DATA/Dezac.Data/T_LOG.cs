namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_LOG")]
    public partial class T_LOG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMLOG { get; set; }

        public DateTime FECHA { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTIPOEVENTO { get; set; }

        public int? IDTORRE { get; set; }

        public int? IDDISPOSITIVO { get; set; }

        public int? IDSOFTWARE { get; set; }

        public int? SESION { get; set; }

        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public int? NUMMATRICULA { get; set; }

        public virtual BIEN BIEN { get; set; }

        public virtual BIEN BIEN1 { get; set; }

        public virtual BIEN BIEN2 { get; set; }

        public virtual MATRICULA MATRICULA { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        public virtual T_TIPOEVENTO T_TIPOEVENTO { get; set; }
    }
}
