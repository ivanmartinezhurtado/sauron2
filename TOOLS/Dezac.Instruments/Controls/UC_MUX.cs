﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.IO;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;


namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(MuxDescription))]
    public partial class UC_MUX : UserControl
    {
        private PCI7296 Device;
        private IOMux mux;
        private int InputA = 0;
        private int InputB = 0;
        Outputs DeviceA;
        Outputs DeviceB;
        private bool SelecInputA = false;
        private bool SelecInputB = false;
        private int OutputA = 0;
        private int OutputB = 0;

        public UC_MUX()
        {
            InitializeComponent();
            Device = new PCI7296(); ;
        }

        public UC_MUX(PCI7296 device)
        {
            InitializeComponent();
            Device = device;
        }

        public Type InstrumentType
        {
            get
            {
                return typeof(IOMux);
            }
        }
        public IOMux Instrument
        {
            get
            {
                if (mux == null)
                    mux = new IOMux(Device.DO);

                return mux;
            }
            set
            {
                if (mux != null)
                {
                    mux.Dispose();
                    mux = null;
                }
                mux = value;
            }
        }

         private void SelecEntrada(object sender, EventArgs e)
         {
             Button bt = (Button)sender;
             foreach (Control ctr in splitContainer1.Panel1.Controls)
             {
                 Button btOther = (Button)ctr;
                 if (btOther.Equals(bt))
                 {
                     if (SelecInputA == false)
                     {
                         InputA = Convert.ToInt32(ctr.Name.Replace("InMux", ""));
                         bt.ImageIndex = 0;
                    }
                     else
                     {
                         if (SelecInputB == false)
                         {
                             InputB = Convert.ToInt32(ctr.Name.Replace("InMux", ""));
                             if (InputB == InputA) return;
                             bt.ImageIndex = 3;
                        }
                         else { ResetButtons(); return; }
                     }
                     
                 }
                 else
                 {
                     if (btOther.Name != "InMux" + InputA)
                         btOther.ImageIndex = 1;
                 }
             }
         }

        private void ResetButtons()
        {
            Instrument.Reset();
            SelecInputA = false;
            SelecInputB = false;
            InputA = 0;
            InputB = 0;
            foreach (Control ctr in splitContainer1.Panel1.Controls)
            {
                Button btOther = (Button)ctr;
                //btOther.BackColor = Color.MistyRose;
                btOther.ImageIndex = 1;
            }
            foreach (Control ctr in splitContainer2.Panel2.Controls)
            {
                Button btOther = (Button)ctr;
                btOther.BackColor = Color.MistyRose;
                btOther.ImageIndex = 1;
            }
            btnApply.Enabled = true;
            lbl_CANALA.Text = "Libre";
            lbl_CANALA.BackColor = Color.Silver;
            lbl_CANALB.Text = "Libre";
            lbl_CANALB.BackColor = Color.Silver;
        }

        private void b_MUX_Reset_Click(object sender, EventArgs e)
        {
            try
            {
                ResetButtons();
            }
            catch (Exception ex)
            {
                Logger.Error("Error al reslizar Reset a la MUX", ex);
            }
        }

        private void SelecOutput(object sender, EventArgs e)
        {
            
            Button bt = (Button)sender;
            foreach (Control ctr in splitContainer2.Panel2.Controls)
            {
                Button btOther = (Button)ctr;
                if (btOther.Equals(bt))
                {
                    if (SelecInputA == false)
                    {
                        OutputA = Convert.ToInt32(ctr.Name.Replace("OutMux", ""));
                        DeviceA = SelecDeviceOutputMux(OutputA);
                        SelecInputA = true;
                        lbl_CANALA.Text = "Ocupado";
                        lbl_CANALA.BackColor = Color.Green;
                        bt.BackColor = Color.Green;
                    }
                    else
                    {
                        if (SelecInputB == false)
                        {
                            OutputB = Convert.ToInt32(ctr.Name.Replace("OutMux", ""));
                            if (OutputA == OutputB) return;
                            DeviceB = SelecDeviceOutputMux(OutputB);
                            SelecInputB = true;
                            lbl_CANALB.Text = "Ocupado";
                            lbl_CANALB.BackColor = Color.DarkGray;
                            bt.BackColor = Color.DarkGray;
                        }
                        else { ResetButtons(); return; }
                    }
                    bt.ImageIndex = 0;
                }
                else
                {
                    if (ctr.Name != "OutMux" + OutputA)
                    {
                        btOther.BackColor = Color.MistyRose;
                        btOther.ImageIndex = 1;
                    }
                }

            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try { 
            if (SelecInputB == false)
            {
                if (SelecInputA == false) return;
                else  Instrument.Connect(InputA, DeviceA);    
            }else
                Instrument.Connect2(InputA, DeviceA,InputB,DeviceB);

            btnApply.Enabled = false;
            }
            catch (Exception ex)
            {
                Logger.Error("Error al actiar salidas a la MUX", ex);
            }
        }

        private Outputs SelecDeviceOutputMux(int Ouput)
        {
            Outputs Device = new Outputs();
            switch(Ouput)
            {
                case 0:
                    Device= Outputs.Multimetro;
                    break;
                case 1:
                     Device= Outputs.FreCH1;
                    break;
                case 2:
                    Device= Outputs.FreCH2;
                    break;
                default:
                    ResetButtons();
                    break;
            }
            return Device;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if(Device != null)
                Device.Dispose();

            if (mux != null)
                mux.Dispose();

            base.Dispose(disposing);
        }

        private void btnMouseEnter(object sender, EventArgs e)
        {
            var button = (Button)sender;
            button.ImageIndex = SelecInputA || SelecInputB ?
                                button.Name.Contains("InMux" + InputA) ? 0 : button.Name.Contains("InMux" + InputB) ? 3 : 2
                                        : 2; 
        }

        private void btnMouseLeave(object sender, EventArgs e)
        {
            var button = (Button)sender;
            button.ImageIndex = button.Name.Contains("InMux" + InputA) ? 0 : button.Name.Contains("InMux" + InputB) ? 3 : 1;

        }
    }
    public class MuxDescription : ControlDescription
    {
        public override string Name { get { return "MUX"; } }
        public override string Category { get { return "In/Out"; } }
        public override Image Icon { get { return Properties.Resources.mux; } }
        public override Image IconBig { get { return null; } }
        public override Type SubcomponentType { get { return typeof(IOMux); } }
    }
}
