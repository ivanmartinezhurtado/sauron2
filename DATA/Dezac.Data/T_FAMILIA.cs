namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_FAMILIA")]
    public partial class T_FAMILIA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_FAMILIA()
        {
            PRODUCTO = new HashSet<PRODUCTO>();
            T_FAMILIAPARAM = new HashSet<T_FAMILIAPARAM>();
            T_GRUPOPARAMETRIZACION = new HashSet<T_GRUPOPARAMETRIZACION>();
            T_IRISERRORFAMILIAFASE = new HashSet<T_IRISERRORFAMILIAFASE>();
            T_TEST = new HashSet<T_TEST>();
            T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
            T_TESTSEQUENCEFILE = new HashSet<T_TESTSEQUENCEFILE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMFAMILIA { get; set; }

        [Required]
        [StringLength(20)]
        public string FAMILIA { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [Required]
        [StringLength(1)]
        public string BLOQUEOTEST { get; set; }

        [StringLength(3)]
        public string PLATAFORMA { get; set; }

        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTO> PRODUCTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_FAMILIAPARAM> T_FAMILIAPARAM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_GRUPOPARAMETRIZACION> T_GRUPOPARAMETRIZACION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_IRISERRORFAMILIAFASE> T_IRISERRORFAMILIAFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TEST> T_TEST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTSEQUENCEFILE> T_TESTSEQUENCEFILE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTCONFIGFILE> T_TESTCONFIGFILE { get; set; }
    }
}
