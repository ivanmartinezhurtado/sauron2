﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.Utility
{
    public class InstrumentException : Exception
    {
        public int Code { get; protected set; }

        public InstrumentException(int code)
            : base()
        {
            this.Code = code;
        }

        public InstrumentException(int code, string message)
            : base(message)
        {
            this.Code = code;
        }

        public InstrumentException(int code, string message, Exception innerException)
            : base(message, innerException)
        {
            this.Code = code;
        }
    }
}
