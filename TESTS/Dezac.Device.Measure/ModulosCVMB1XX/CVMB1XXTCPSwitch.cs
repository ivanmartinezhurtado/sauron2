﻿using Comunications.Utility;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure.ModulosCVMB1XX
{
    [DeviceVersion(1.00)]
    public class CVMB1XXTCPSwitch : DeviceBase
    {
        ModbusDeviceTCP tcp;

        public CVMB1XXTCPSwitch()
        {
        }

        public CVMB1XXTCPSwitch(string hostname, int tcpPort, ModbusTCPProtocol modbusTCPProtocol)
        {
            SetTCP(hostname, tcpPort, modbusTCPProtocol);
        }

        public void SetTCP(string hostname, int tcpPort, ModbusTCPProtocol modbusTCPProtocol)
        {
            tcp = new ModbusDeviceTCP(hostname, tcpPort, modbusTCPProtocol);
        }

        public MAC ReadMAC()
        {
            return tcp.Read<MAC>();
        }

        public ushort ReadLine1Voltage()
        {
            return tcp.ReadRegister((ushort)Registers.L1_VOLTAGE);
        }

        public ushort ReadSerialNumberDevice()
        {
            return tcp.ReadRegister((ushort)Registers.SERIAL_NUMBER_TCP);
        }

        public void WriteReset()
        {
            tcp.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public override void Dispose()
        {
            tcp.Dispose();
        }

        public void WriteMACTCP(MAC mac)
        {
            tcp.Write<MAC>(mac);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAC)]
        public struct MAC
        {
            public byte Field_1;
            public byte Field_2;
            public byte Field_3;
            public byte Field_4;
            public byte Field_5;
            public byte Field_6;

            public override string ToString()
            {
                return Field_1.ToString() + ":" + Field_2.ToString() + ":" + Field_3.ToString() + ":" + Field_4.ToString() + ":" + Field_5.ToString() + ":" + Field_6.ToString();
            }
        }
        
        public enum Registers
        {
            MAC = 0xF236, // 3 registros
            SERIAL_NUMBER_TCP = 0x2B5C,
            RESET = 0x07D0,
            L1_VOLTAGE = 0X0000,
        }

    }
}
