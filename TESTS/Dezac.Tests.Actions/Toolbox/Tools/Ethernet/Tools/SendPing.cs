﻿using Dezac.Tests.Actions.Kernel;
using System.ComponentModel;
using System.Drawing;
using System.Net.NetworkInformation;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(SendPingDescription))]
    public class SendPing : ActionBase
    {
        [Description("Dirección IP a donde mandar el ping")]
        [Category("3. Configuration")]
        public string IP { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            Ping p = new Ping();
            PingReply result;
            int i = 0;

            var ip = VariablesTools.ResolveValue(IP).ToString();
            
            testBase.Logger.InfoFormat("CMD : ping {0}", ip);
            result = p.Send(ip, 5000);
            testBase.Logger.InfoFormat("RESP : {0}", result.Status.ToString());          

            if (result.Status != IPStatus.Success) 
                Error().UUT.COMUNICACIONES.ETHERNET("NO HAY RESPUESTA EN EL PING").Throw();
        }
    }

    public class SendPingDescription : ActionDescription
    {
        public override string Name { get { return "SendPing"; } }
        public override string Category { get { return "Tools/Ethernet/Tools"; } }
        public override string Description { get { return "Action para mandar el ping"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
