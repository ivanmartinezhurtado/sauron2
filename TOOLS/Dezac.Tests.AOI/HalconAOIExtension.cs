﻿using Dezac.Core.Model;
using Dezac.Services;
using Dezac.Tests.AOI.Models;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TaskRunner;

namespace Dezac.Tests.AOI
{
    public static class HalconAOIExtension
    {
        private const int IN_READY_FOR_INSPECTION = 0;
        private const int OUT_INSPECTION_RUNNING = 0;
        private const int OUT_INSPECTION_KO = 1;
        private const int OUT_INSPECTION_OK = 2;
        private const int OUT_START_INSPECTION = 3;

        public static HalconInspectionResults AOIWeldingInspection(TestBase testBase)
        {
            var cachesvc = testBase.GetService<ICacheService>();

            PCIE1760 io = null;

            if (!cachesvc.TryGet("PCIE_IO", out io))
            {
                io = new PCIE1760();
                cachesvc.AddOrSet("PCIE_IO", io);
            }

            int numColumns = (int)testBase.Configuracion.GetDouble("NUM_COLUMNS", 1, ParamUnidad.Numero);
            int numRows = (int)testBase.Configuracion.GetDouble("NUM_ROWS", 1, ParamUnidad.Numero);
            int rowDistance = (int)testBase.Configuracion.GetDouble("COLUMN_DISTANCE", 1, ParamUnidad.Numero);
            int columnDistance = (int)testBase.Configuracion.GetDouble("ROW_DISTANCE", 1, ParamUnidad.Numero);
            int timeStartCapture = (int)testBase.Configuracion.GetDouble("START_CAPTURE_TIME", 1200, ParamUnidad.Numero);

            string SVMFolder = ConfigurationManager.AppSettings["PathAOISVM"];
            string ROIFolder = ConfigurationManager.AppSettings["PathAOIROI"];

            string auxString = testBase.Model.IdFase == "039" ? "_B" : "";
            string matchingFileName = ROIFolder + "MATCHING_MODEL_" + testBase.Model.NumProducto + auxString + ".shm";
            string searchFileName = ROIFolder + "MATCHING_BOARD_MODEL_" + testBase.Model.NumProducto + auxString + ".shm";
            string ROIFileName = ROIFolder + "ROI_MODEL_" + testBase.Model.NumProducto + auxString + ".hobj";
            string boardRegionFileName = ROIFolder + "MATCHING_BOARD_REGION_" + testBase.Model.NumProducto + auxString + ".hobj";
            string weldingSVM = ROIFolder + "WeldingSVM_" + testBase.Model.NumProducto + auxString + ".tup";

            string imageSaveFile = $"{ConfigurationManager.AppSettings["PathAOIImages"]}{testBase.Model.NumProducto}{auxString}" +
                $"\\{DateTime.Now.ToShortDateString().Replace("/", "")}_{DateTime.Now.ToLongTimeString().Replace(":", "")}.png";

            List<string> SVMFile = new List<string>();

            int i = 0;
            while (File.Exists(SVMFolder + "SVMClassifier_" + i + ".gsc"))
            {
                SVMFile.Add(SVMFolder + "SVMClassifier_" + i + ".gsc");
                i++;
            }

            bool hasInspection = testBase.Configuracion.GetString("HAS_AOI", "NO", ParamUnidad.Numero) == "SI";

            while (!io.DI.Read(IN_READY_FOR_INSPECTION).Value)
            {
                testBase.CheckCancellationRequested();

                Thread.Sleep(500);
            }

            Task task = new Task(
            () => io.DO.PulseOn(1000, OUT_START_INSPECTION));
            task.Start();

            Thread.Sleep(timeStartCapture);
            io.DO.On(OUT_INSPECTION_RUNNING);

            HalconInspectionResults inspectionResults = null;

            if (hasInspection)
                try
                {
                    AOIClassficationInfo info = new AOIClassficationInfo()
                    {
                        SVMFile = SVMFile,
                        matchingFileName = matchingFileName,
                        searchFileName = searchFileName,
                        ROIFileName = ROIFileName,
                        boardRegionFileName = boardRegionFileName,
                        numRows = numRows,
                        numColumns = numColumns,
                        rowDistance = rowDistance,
                        columnDistance = columnDistance,
                        barcodeRead = testBase.Model.IsAOITrace,
                        orderPath = ConfigurationManager.AppSettings["PathAOIResultsSave"] + testBase.Model.NumOrden.Value + "\\",
                        weldingSVM = weldingSVM,
                    };
                    inspectionResults = HalconInterface.AOIClassification(info, imageSaveFile); 
                    inspectionResults.NumProduct = testBase.Model.NumProducto;
                    inspectionResults.IdFase = testBase.Model.IdFase;
                    inspectionResults.NumFamilia = testBase.Model.NumFamilia.Value;
                    inspectionResults.NumOperario = testBase.Model.IdOperario;
                    inspectionResults.NumOrden = testBase.Model.NumOrden.Value;
                    inspectionResults.Date = DateTime.Now;

                    io.DO.On(OUT_INSPECTION_OK);
                    io.DO.Off(OUT_INSPECTION_KO, OUT_INSPECTION_RUNNING);

                    testBase.Resultado.Set("GRAB_IMAGE_TIME", inspectionResults.GrabImageTimeSeconds, ParamUnidad.s);
                    testBase.Resultado.Set("SAVE_IMAGE_TIME", inspectionResults.SaveImageTimeSeconds, ParamUnidad.s);
                    testBase.Resultado.Set("EXTRAC_WELDINGS_TIME", inspectionResults.ExtractWeldingsTimeSeconds, ParamUnidad.s);
                    testBase.Resultado.Set("READ_BARCODES_TIME", inspectionResults.ReadBarcodesTimeSeconds, ParamUnidad.s);
                    testBase.Resultado.Set("EXTRACT_FEATURES_TIME", inspectionResults.ExtractFeaturesTimeSeconds, ParamUnidad.s);
                    testBase.Resultado.Set("CLASSIFICATION_TIME", inspectionResults.ClassificationTimeSeconds, ParamUnidad.s);

                    testBase.SharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, inspectionResults.Barcodes.ToList());

                    var json = JsonConvert.SerializeObject(inspectionResults);
                    var path = ConfigurationManager.AppSettings["PathAOIResultsSave"] + testBase.Model.NumOrden.Value.ToString() + "\\";

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    File.WriteAllText(path + string.Join("_", inspectionResults.Barcodes) + ".txt", json);

                }
                catch (Exception e)
                {
                    io.DO.On(OUT_INSPECTION_KO);
                    io.DO.Off(OUT_INSPECTION_OK, OUT_INSPECTION_RUNNING);

                    throw e;
                }
            else
            {
                io.DO.On(OUT_INSPECTION_OK);
                io.DO.Off(OUT_INSPECTION_KO, OUT_INSPECTION_RUNNING);

                HalconInterface.AOIGrabAndSaveImage(imageSaveFile);
            }

            return inspectionResults;
        }

        public static void AOISetTestResults(TestBase testBase)
        {
            var exceptions = new List<Exception>();
            var results = testBase.GetSharedVariable<HalconInspectionResults>("AOIINSPECTION_RESULTS", null);

            if (!results.Success)
                throw results.Exception;

             object _lockobj = new object();

            lock (_lockobj)
            {
                BastidorService.BastidorValidation(false, false, false, false);
            }

            var boardResult = results.Boards[testBase.NumInstance - 1];

            testBase.Resultado.Set("BOARD_#" + boardResult.Position + "_RESULT", boardResult.Classification.ToString(), ParamUnidad.SinUnidad);
            testBase.TestInfo.NumBastidor = UInt64.Parse(boardResult.Barcode);

            foreach (WeldingResult welding in boardResult.Weldigns) 
            {
                testBase.Resultado.Set(string.Format("BOARD_#{0}_WELDING_#{1}_SCORE", boardResult.Position, welding.IDBoard), welding.Score, ParamUnidad.Numero, 1, 4);
                testBase.Resultado.Set(string.Format("BOARD_#{0}_WELDING_#{1}_CLASSIFICATION", boardResult.Position, welding.IDBoard), welding.Classification.ToString(), ParamUnidad.Numero);
                if (welding.Classification != ResultEnum.OK)
                    exceptions.Add(new Exception(string.Format("AOI KO TEST: Board {0}, Welding {1}, Score {2:0.00}", boardResult.Position, welding.IDBoard, welding.Score)));
            }

            if (testBase.TestInfo != null && !testBase.TestInfo.NumFabricacion.HasValue)
                try
                {
                    using (DezacService svc = new DezacService())
                    {
                        testBase.TestInfo.NumFabricacion = svc.GetNumFabricacion(testBase.Model.NumOrden.Value, (int)testBase.TestInfo.NumBastidor.Value);
                    }
                }
                catch(Exception e)
                {
                    testBase.Logger.Warn(e.Message);
                }

            if (exceptions.Count > 0)
                throw new AggregateException(exceptions);
        }

    }
}
