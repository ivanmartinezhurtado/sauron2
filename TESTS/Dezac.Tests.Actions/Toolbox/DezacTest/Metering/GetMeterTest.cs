﻿using Dezac.Core.Exceptions;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(GetMeterTypeDescription))]
    public class GetMeterType : ActionBase
    {
        [Category("3. Configuration")]
        [DefaultValue("412QD1A")]
        [Description("Modelo de contador a obtener")]
        public string Model { get; set; }

        [Category("4. Results")]
        [DefaultValue("METER_TYPE")]
        [Description("Nombre de la variable donde se guardará el objeto MeterType")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            Logger.InfoFormat("Modelo: {0}", Model);
            try
            {
                var meter = new MeterType(VariablesTools.ResolveValue(Model).ToString());

                if (meter == null)
                    TestException.Create().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Modelo");

                Logger.InfoFormat("Contador Tipo Equipo:{0}", meter.TipoEquipo.ToString());
                Logger.InfoFormat("Contador Tipo Conexion:{0}", meter.TipoConexion.ToString());
                Logger.InfoFormat("Contador Voltage Nom. {0}  Voltage min.{1}  Voltage Max.{2}", meter.VoltageProperty.VoltageNominal, meter.VoltageProperty.VoltageMinimo, meter.VoltageProperty.VoltageNominal);
                Logger.InfoFormat("Contador Current base. {0}  Current min.{1}  Current Max.{2}", meter.CurrentProperty.CorrienteBase, meter.CurrentProperty.CorrienteMinima, meter.CurrentProperty.CorrienteMaxima);
                Logger.InfoFormat("Contador Freq {0} Automatica {1}", meter.FrequencyProperty.Frequency, meter.FrequencyProperty.IsAutomatico);
                Logger.InfoFormat("Contador Precision Activa:{0} Precision Reactiva:{1}", meter.PrecisionProperty.PrecisionActiva, meter.PrecisionProperty.PrecisionReacctiva);
                Logger.InfoFormat("Contador Precision Imin Activa:{0} Precision Imin Reactiva:{1}", meter.PrecisionProperty.PrecisionIminActiva, meter.PrecisionProperty.PrecisionIminReactiva);

                SetVariable(DataOutput, meter);
                var tipoEquipo = string.Format("{0}_TIPO_EQUIPO", DataOutput);
                SetVariable(tipoEquipo, meter.TipoEquipo.ToString());
                tipoEquipo = string.Format("{0}_TIPO_CONEXION", DataOutput);
                SetVariable(tipoEquipo, meter.TipoConexion.ToString());

                var vnom = string.Format("{0}_VOLTAGE_NOM", DataOutput);
                SetVariable(vnom, meter.VoltageProperty.VoltageNominal);
                var vmin = string.Format("{0}_VOLTAGE_MIN", DataOutput);
                SetVariable(vmin, meter.VoltageProperty.VoltageMinimo);
                var vmax = string.Format("{0}_VOLTAGE_MAX", DataOutput);
                SetVariable(vmax, meter.VoltageProperty.VoltageMaximo);
                var freq = string.Format("{0}_FREQUENCIA", DataOutput);
                SetVariable(freq, meter.FrequencyProperty.Frequency);
                var currBase = string.Format("{0}_CORRIENTE_BASE", DataOutput);
                SetVariable(currBase, meter.CurrentProperty.CorrienteBase);
                var currMin = string.Format("{0}_CORRIENTE_MIN", DataOutput);
                SetVariable(currMin, meter.CurrentProperty.CorrienteMinima);
                var currMax = string.Format("{0}_CORRIENTE_MAX", DataOutput);
                SetVariable(currMax, meter.CurrentProperty.CorrienteMaxima);
            }
            catch (Exception ex)
            {
                Logger.WarnFormat("Modelo: {0} Incorrecto no se ha podido crear una clase MeterType por {1}", Model, ex.Message);
                Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("MODELO INCOHERENTE").Throw();
            }
        }
    }

    public class GetMeterTypeDescription : ActionDescription
    {
        public override string Name { get { return "GetMeterType"; } }
        public override string Category { get { return "DezacTest/Metering"; } }
        public override string Description { get { return "Action que sirve para crear un objeto de tipo Meter"; } }
        public override Image Icon { get { return Properties.Resources.Validation; } }
    }
}
