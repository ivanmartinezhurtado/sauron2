﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(InitTestBaseDescription))]
    public class InitTestBase : ActionBase
    {
        [Description("Versión del Test que está inicializando por defecto es la versión 1")]
        [Category("3. Version")]
        [DefaultValue("1")]
        public string Version { get; set; }
        public override void Execute()
        {
            var context = SequenceContext.Current;

            var testContext = context.Services.Get<ITestContext>();
            if (testContext == null)
            {
                if (context.RunMode == RunMode.Release)
                    return;

                var data = new TestContextModel
                {
                    IdEmpleado = "999",
                    IdOperario = "999",
                    modoPlayer = ModoTest.Normal,
                    Cantidad = 1,
                    IdFase = "120"
                };

                Context.Services.Add<ITestContext>(data);
                testContext = context.Services.Get<ITestContext>();
            }

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });

            if (testBase.TestInfo.TestVersion == "SIN VERSIONAR")
            {
                if (string.IsNullOrEmpty(testContext.SequenceVersion))
                    testBase.TestInfo.TestVersion = string.Format("{0}", Version);
                else
                    testBase.TestInfo.TestVersion = string.Format("{0}.{1}", Version, testContext.SequenceVersion);

                testBase.Resultado.Set("VERSION_TEST", string.Format("{0}", testBase.TestInfo.TestVersion), ParamUnidad.SinUnidad);
            }

            if(testBase.Resultado.GetString("CLASSE_TEST","") == "TestBase" && testContext.SequenceName != null)
                testBase.Resultado.Set("CLASSE_TEST", string.Format("{0}", testContext.SequenceName), ParamUnidad.SinUnidad);

            var reportVersion = testBase.Configuracion.GetString("TEST_REPORT_VERSION", "1", ParamUnidad.SinUnidad);
            if (reportVersion != null)
                testBase.TestInfo.ReportVersion = Convert.ToInt32(reportVersion);

            testBase.Resultado.Set("TEST_REPORT_VERSION", testBase.TestInfo.ReportVersion.Value, ParamUnidad.SinUnidad, 1);
            testBase.Resultado.Set("PC_NAME", testContext.PC, ParamUnidad.SinUnidad, "");
            testBase.Resultado.Set("SAURON_VERSION", testContext.SauronVersion, ParamUnidad.SinUnidad, "");
            testBase.Resultado.Set("SAURON_INTERFACE", testContext.SauronInterface, ParamUnidad.SinUnidad, "");
        }
    }

    public class InitTestBaseDescription : ActionDescription
    {
        public override string Name { get { return "InitTestBase"; } }
        public override string Description { get { return "Acción para inicializar Test de equipos cuando estos solo tienen Actions, se debería cada vez que se cambia algo en la secuencia cambiar la Version, Ejemplo: 1, 2, 3, 4, ..."; } }
        public override string Category { get { return "Actions/Context"; } }
        public override Image Icon { get { return Properties.Resources.kernel; } }
    }
}
