namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.BIEN")]
    public partial class BIEN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BIEN()
        {
            BIEN1 = new HashSet<BIEN>();
            BIENASISTENCIA = new HashSet<BIENASISTENCIA>();
            BIENPROGRAMACIONASISTENCIA = new HashSet<BIENPROGRAMACIONASISTENCIA>();
            BIENPARAM = new HashSet<BIENPARAM>();
            T_LOG = new HashSet<T_LOG>();
            T_LOG1 = new HashSet<T_LOG>();
            T_LOG2 = new HashSet<T_LOG>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMBIEN { get; set; }

        [Required]
        [StringLength(20)]
        public string CODIGO { get; set; }

        [StringLength(40)]
        public string NROSERIE { get; set; }

        [Required]
        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [StringLength(10)]
        public string IDPROVEEDOR { get; set; }

        public decimal? VALORADQUISICION { get; set; }

        public DateTime? FECHAADQUISICION { get; set; }

        public DateTime? FECHABAJA { get; set; }

        [StringLength(20)]
        public string DOCCONTABLE { get; set; }

        [Required]
        [StringLength(1)]
        public string AMORTIZABLE { get; set; }

        [Required]
        [StringLength(1)]
        public string USADO { get; set; }

        [StringLength(10)]
        public string UBICACION { get; set; }

        [Required]
        [StringLength(10)]
        public string IDFAMILIA { get; set; }

        [StringLength(50)]
        public string CAUSABAJA { get; set; }

        [StringLength(10)]
        public string IDSECCION { get; set; }

        public int? NUMBIENPADRE { get; set; }

        [StringLength(200)]
        public string NOTAS { get; set; }

        public byte[] IMAGEN { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACION { get; set; }

        public int? IDENTIFICACIONTEST { get; set; }

        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDTIPO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDPROPIETARIO { get; set; }

        [Required]
        [StringLength(10)]
        public string IDDEPOSITARIO { get; set; }

        public DateTime FECHAALTA { get; set; }

        public int? NUMPROYECTO { get; set; }

        [StringLength(20)]
        public string REFPROVEEDOR { get; set; }

        [StringLength(20)]
        public string UBICACIONPROVISIONAL { get; set; }

        [StringLength(1)]
        public string  CALIBRACION { get; set; }

        [StringLength(1)]
        public string AJUSTE { get; set; }

        [StringLength(1)]
        public string PREVENTIVO { get; set; }

        [StringLength(1)]
        public string CONTROLBIEN { get; set; }

        [StringLength(1)]
        public string GENERICO { get; set; }

        public virtual PROYECTO PROYECTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BIEN> BIEN1 { get; set; }

        public virtual BIEN BIEN2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BIENASISTENCIA> BIENASISTENCIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BIENPROGRAMACIONASISTENCIA> BIENPROGRAMACIONASISTENCIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BIENPARAM> BIENPARAM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_LOG> T_LOG { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_LOG> T_LOG1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_LOG> T_LOG2 { get; set; }
    }
}
