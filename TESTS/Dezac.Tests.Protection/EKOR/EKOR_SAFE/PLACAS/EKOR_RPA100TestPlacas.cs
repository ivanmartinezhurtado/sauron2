﻿using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.03)]
    public class EKOR_RPA100TestPlacas : TestBase
    {
        private const byte IN_DEVICE_PRESENCE = 17;
        private const byte OUT_FAN = 15;
        internal const int OUT_ACTIVATE_POWER_SUPPLY = 17;
        internal const int OUT_DIVISOR_INPUT_CURRENT = 19;
        internal const int OUT_POWER_SUPPLY_SWITCH = 20;

        private EKOR_RPA100 Ekor;
        private TowerBoard tower;

        public void TestInitialization(int portEkor = 5)
        {
            if (portEkor == 0)
                portEkor = Comunicaciones.SerialPort;

            Ekor = new EKOR_RPA100(portEkor);
            Ekor.Cirbus.PerifericNumber = 1;

            var towerBoard = tower as TowerBoard;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out towerBoard))
            {
                towerBoard = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", towerBoard);
            }
            if (towerBoard == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower = towerBoard;

            tower.IO.DO.On(14);
            tower.ActiveVoltageCircuit();

            tower.LAMBDA.Initialize();

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DEVICE_PRESENCE, 2000))
                throw new Exception("Error no se detecta el equipo en el util");

            tower.IO.DO.OnWait(200, 4);// Activa Valbula de aire
            tower.IO.DO.OnWait(200, 14);
            tower.IO.DO.OnWait(200, OUT_ACTIVATE_POWER_SUPPLY); //17
        }

        public void TestComunicationsTTL()
        {
            Ekor.Cirbus.PortCom = Comunicaciones.SerialPort;

            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    logger.InfoFormat("Puerto enviamos Trama Modbus de Recuperacion a CIRBUS");
                    Ekor.RecoveryDefaultSettings(Comunicaciones.SerialPort, 9600, 1);
                    Delay(2000, "");
                }
                var model = EKOR_SAFE_OLD.ModelHardware.FABPLUS;
                Ekor.WriteConfigurationDefaultFab(model);
                var version = Ekor.ReadVersion();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Versión firmware incorrecta"), ParamUnidad.SinUnidad);
                var crcBoot = Ekor.ReadCRCBoot();
                Assert.AreEqual(ConstantsParameters.Identification.CRC_BOOT, crcBoot, Identificacion.CRC_BOOT.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, crc de boot incorrecto"), ParamUnidad.SinUnidad);

                return true;
            }, "", 3, 1000, 1000, false, false,
            (p) =>
                 Error().UUT.COMUNICACIONES.INICIALIZACION().Throw()
            );
        }

        public void TestComunicationsRS232(int porEkorfront9)
        {
            if (porEkorfront9 == 0)
                porEkorfront9 = Comunicaciones.SerialPort;

            Ekor.Cirbus.PortCom = porEkorfront9;
            Ekor.Cirbus.BaudRate = 9600;
            Ekor.Cirbus.Parity = System.IO.Ports.Parity.None;
            Ekor.Cirbus.PerifericNumber = 1;

            string developerVersion = "";
            string firmwareVersion = "";

            SamplerWithCancel((p) =>
            {
                logger.InfoFormat("Puerto de comunicacion RS232 asignado en  COM {0}", porEkorfront9.ToString());

                developerVersion = Ekor.ReadVersionDesarrollo().version;
                firmwareVersion = Ekor.ReadVersion().version;

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_DEVELOPER, developerVersion.Trim(), Identificacion.VERSION_DEVELOPER.Trim(),
                Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
                Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de firmware es incorrecta"), ParamUnidad.SinUnidad);
                
                var versionMicro = Ekor.ReadMicrocontrolerType();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, (string)versionMicro.cpu, Identificacion.VERSION_HARDWARE.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, la  tipo de microcontrolador no es el correcto"), ParamUnidad.SinUnidad);

                Resultado.Set("CPU REVISION", (string)versionMicro.revision, ParamUnidad.SinUnidad);

                var versionBoot = Ekor.ReadVersionBoot();

                Assert.AreEqual(ConstantsParameters.Identification.VERSION_BOOT, (string)versionBoot, Identificacion.VERSION_BOOT.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, version de boot incorrecto"), ParamUnidad.SinUnidad);

                var crcBoot = Ekor.ReadCRCBoot();

                Assert.AreEqual(ConstantsParameters.Identification.CRC_BOOT, crcBoot, Identificacion.CRC_BOOT.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, crc de boot incorrecto"), ParamUnidad.SinUnidad);
                return true;

            }, "Error. No comunica el equipo por RS232", 3, 500, 1000, false, false);         
        }

        public void TestDisplay()
        {
            var frm = this.ShowForm(() => new ImageView("TEST DISPLAY", this.PathCameraImages + "\\EKOR_RPA100\\TEST_DISPLAY_PAR.jpg"), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    Ekor.WriteDisplayTestSegmentsEven();
                    if (frm.DialogResult == DialogResult.Cancel)
                        throw new Exception("Error display incorrecto");
                    return frm.DialogResult == DialogResult.OK;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Resultado.Set("DISPLAY_PAR", "OK", ParamUnidad.SinUnidad);


            frm = this.ShowForm(() => new ImageView("TEST DISPLAY", this.PathCameraImages + "\\EKOR_RPA100\\TEST_DISPLAY_IMPAR.jpg"), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    Ekor.WriteDisplayTestSegmentsOdd();
                    if (frm.DialogResult == DialogResult.Cancel)
                        throw new Exception("Error display incorrecto");
                    return frm.DialogResult == DialogResult.OK;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Resultado.Set("DISPLAY_INPAR", "OK", ParamUnidad.SinUnidad);
        }

        public void TestConsumo()
        {
            tower.IO.DO.OnWait(3000, 17);

            ////*************************************** RS232
            AdjustValueDef consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("ALIMENTACION_COM").Name, 0, Params.I_DC.Null.TestPoint("ALIMENTACION_COM").Min(), Params.I_DC.Null.TestPoint("ALIMENTACION_COM").Max(), 0, 0, ParamUnidad.mA);

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltDC,
                NumMuestras = 3,
                Rango = consumption.Max,
                DigitosPrecision = 4,
                NPLC = 0
            };

            //TestMeasureBase(consumption, (s) =>
            //{
            //    return tower.MeasureMultimeter(InputMuxEnum.IN2_AuxMedida1, measure, 1000) / 0.47;
            //}, 1, 5, 2000, "Error de consumo del equipo, el consumo del equipo por las comunicaciones está fuera de márgenes");

            //**************************************** FUENTE 
            tower.IO.DO.OffWait(500, 20);
            consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("ALIMENTACION").Name, 0, Params.I_DC.Null.TestPoint("ALIMENTACION").Min(), Params.I_DC.Null.TestPoint("ALIMENTACION").Max(), 0, 0, ParamUnidad.mA);
            measure.Rango = consumption.Max;

            TestMeasureBase(consumption, (s) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, measure, 1000) / 0.47;
            }, 1, 5, 2000);

            ////**************************************** BATERIA 
            ////tower.IO.DO.OnWait(500, 39);
            ////Delay(1500, "estabilizacion consumo");

            //////SetLambaInSteps(3.3D, 0.005D, 3);
            ////tower.LAMBDA.ApplyAndWaitStabilisation(3.3, 0.010);
            ////Delay(2000, "estabilizacion consumo");

            //var measureAC = new MagnitudToMeasure()
            //{
            //    Frequency = freqEnum._50Hz,
            //    Magnitud = MagnitudsMultimeter.AmpDC,
            //    NumMuestras = 3,
            //    Rango = consumption.Max,
            //    DigitosPrecision = 4,
            //    NPLC = 0
            //};

            ////****************************************BATERIA ALIMENTADA
            //tower.IO.DO.On(17);

            //Delay(500, "desconexión multiplexora");

            //consumption = new AdjustValueDef(Params.I_DC.Null.TestPoint("BATERIA_ALIMENTADA").Name, 0, Params.I_DC.Null.TestPoint("BATERIA_ALIMENTADA").Min, Params.I_DC.Null.TestPoint("BATERIA_ALIMENTADA").Max, 0, 0, ParamUnidad.mA);

            //measureAC.Rango = consumption.Max;

            //TestMeasureBase(consumption, (s) =>
            //{
            //    return tower.MeasureMultimeter(InputMuxEnum.NOTHING, measureAC, 1000);
            //}, 1, 5, 2000, "Error de consumo de la bateria, el consumo con alimentación se encuentra fuera de márgenes");

            //tower.IO.DO.OffWait(500, 39);

            //tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void TestDateTime()
        {
            Ekor.WriteDateTime();
        }

        public void TestCalibrationClock(string portCrono)
        {
            Ekor.Cirbus.PortCom = Comunicaciones.SerialPort;

            tower.Chroma.SetVoltageRange(Instruments.PowerSource.Chroma.VoltageRangeEnum.HIGH);
            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            double temperature = 0;
            //Delay(10000, "Inicio cronotermo");

            if (string.IsNullOrEmpty(portCrono))
                throw new Exception("No se ha pasado el puerto del Thermometro Optris");

            var cronoPort = Convert.ToByte(GetVariable<string>(portCrono, portCrono).Replace("COM", ""));

            tower.HP53131A
                .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
                .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 1.5)
                .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 8);

            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            tower.IO.DO.On(OUT_FAN);

            TestMeasureBase(defs,
            (step) =>
            {
                Ekor.StartSignalClok(015);

                Delay(500, "");

                var medidaFreqMed = tower.MeasureWithFrecuency(InputMuxEnum.IN3, 8000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);

                //using (var crono = new CRONOTERMOSTATO(cronoPort))
                //{
                //    crono.Modbus.BaudRate = 19200;
                //    crono.Modbus.TimeOut = 4000;

                //    SamplerWithCancel((p) =>
                //    {
                //        crono.WriteStartTemperatureSensing();
                //        Delay(2000, "");
                //        temperature = crono.ReadTemperatureAndConvertToReal();
                //        Resultado.Set("TEMPERATURA", temperature, ParamUnidad.Grados);
                //        return true;
                //    }, "", 8, 1000, 1000, false, true, (p) => { Error().HARDWARE.UTILLAJE.MEDIDA_INSTRUMENTO("Comunicaciones CRONOTERMOSTATO").Throw(); });
                //}
                temperature = 22;

                SamplerWithCancel((p) =>
                {
                    Ekor.ReadVersion();
                    return true;
                }, "Error de comunicaciones despues de esperar 10seg. al enviar CLK_DE2ZACFAB", 8, 1000, 2000, true, true);

                var calibrationClk = Ekor.CalibrationClock(medidaFreqMed, temperature);

                return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
            }, 0, 2, 1000);

            Delay(2000, "Wait grabacion reloj");

            tower.IO.DO.Off(OUT_FAN);
        }

        public void TestFinish()
        {
            if (Ekor != null)
                Ekor.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                Delay(2000, "Espera apagar MTE");
                if (tower.IO != null)
                {
                    tower.IO.DO.Off(OUT_FAN);
                    tower.IO.DO.Off(14, OUT_ACTIVATE_POWER_SUPPLY, OUT_DIVISOR_INPUT_CURRENT, OUT_POWER_SUPPLY_SWITCH);
                }
                tower.Dispose();
            }
        }
    }
}
