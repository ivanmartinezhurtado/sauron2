﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionUUTLevel1
    {
        public string[] Category { get; set; }

        public string[] SubCategory { get; set; }

        public string ClassName { get; set; }

        public SauronException MARGENES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.MARGENES, parameter).Exception();
        }

        public SauronException MARGENES_SUPERIORES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.MARGENES_SUPERIORES, parameter).Exception();
        }

        public SauronException MARGENES_INFERIORES(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.MARGENES_INFERIORES, parameter).Exception();
        }

        public SauronException SETUP(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.SETUP, parameter).Exception();
        }

        public SauronException NO_COMUNICA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.NO_COMUNICA, parameter).Exception();
        }

        public SauronException VECTOR_HARDWARE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.VECTOR_HARDWARE, parameter).Exception();
        }

        public SauronException DISPLAY(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.DISPLAY, parameter).Exception();
        }

        public SauronException NO_DISPARA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.NO_DISPARA, parameter).Exception();
        }

        public SauronException YA_DISPARADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.YA_DISPARADO, parameter).Exception();
        }

        public SauronException INICIALIZACION(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.INICIALIZACION, parameter).Exception();
        }

        public SauronException UPGRADE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.UPGRADE, parameter).Exception();
        }
        public SauronException RECUPERACION_MODO_FABRICA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.RECUPERACION_MODO_FABRICA, parameter).Exception();
        }

        public SauronException FLAG_TEST(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.FLAG_TEST, parameter).Exception();
        }

        public SauronException INICIO_SESION(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.INICIO_SESION, parameter).Exception();
        }

        public SauronException TIME_OUT(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.TIMEOUT, parameter).Exception();
        }

        public SauronException LEDS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.LEDS, parameter).Exception();
        }

        public SauronException KEYBOARD(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.KEYBOARD, parameter).Exception();
        }

        public SauronException TOROIDAL(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.TOROIDAL, parameter).Exception();
        }

        public SauronException RELE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.RELE, parameter).Exception();
        }

        public SauronException DIGITAL_INPUTS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.DIGITAL_INPUTS, parameter).Exception();
        }

        public SauronException NO_ENCIENDE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.NO_ENCIENDE, parameter).Exception();
        }

        public SauronException DIGITAL_OUTPUTS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.DIGITAL_OUTPUTS, parameter).Exception();
        }

        public SauronException USB(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.USB, parameter).Exception();
        }

        public SauronException ETHERNET(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.ETHERNET, parameter).Exception();
        }

        public SauronException SOAP_SERVICE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.SOAP_SERVICE, parameter).Exception();
        }

        public SauronException RTC(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.RTC, parameter).Exception();
        }

        public SauronException ALARMA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.ALARMA, parameter).Exception();
        }

        public SauronException EXTERNAL_MEMORY(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.EXTERNAL_MEMORY, parameter).Exception();
        }

        public SauronException BORRADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.BORRADO, parameter).Exception();
        }

        public SauronException PARAMETROS_NO_GRABADOS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.PARAMETROS_NO_GRABADOS, parameter).Exception();
        }
        
        public SauronException ERROR_UPLOAD(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.ERROR_UPLOAD, parameter).Exception();
        }
        public SauronException ERROR_DOWNLOAD(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.ERROR_DOWNLOAD, parameter).Exception();
        }
        public SauronException NO_CORRESPONDE_CON_ENVIO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.NO_CORRESPONDE_CON_ENVIO, parameter).Exception();
        }
        public SauronException BLUETOOTH(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.BLUETOOTH, parameter).Exception();
        }
        public SauronException GRABACION_SINCRONISMO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.GRABACION_SINCRONISMO, parameter).Exception();
        }
        public SauronException BAUDRATE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.BAUDRATE, parameter).Exception();
        }
        public SauronException NO_COINCIDE(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.NO_COINCIDE, parameter).Exception();
        }
        public SauronException DESCRIPCION_INCORRECTA(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.DESCRIPCION_INCORRECTA, parameter).Exception();
        }
        public SauronException NO_GRABADO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.NO_GRABADO, parameter).Exception();
        }
        public SauronException GPS(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.GPS, parameter).Exception();
        }
        public SauronException RELOJ(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.RELOJ, parameter).Exception();
        }
        public SauronException CANCELADO_POR_USUARIO(string parameter = null)
        {
            return SauronMessageException.Create(Category, SubCategory, ConstantsExceptions.UUTLevel1.CANCELADO_POR_USUARIO, parameter).Exception();
        }
    }
}
