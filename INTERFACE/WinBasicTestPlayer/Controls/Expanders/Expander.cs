﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls;
using System.ComponentModel;
using System.Drawing;
using Telerik.WinControls.UI;

namespace WinTestPlayer.Controls.Expanders
{
    [ToolboxItem(true)]
    public class Expander : RadControl
    {
        ExpanderElement expanderElement;

        public Expander()
        {
            this.Orientation = ExpanderOrientation.Left;
        }

        [DefaultValue(ExpanderOrientation.Left)]
        public ExpanderOrientation Orientation
        {
            get
            {
                return this.expanderElement.Orientation;
            }
            set
            {
                this.expanderElement.Orientation = value;
            }
        }

        protected override void CreateChildItems(RadElement parent)
        {
            this.expanderElement = CreateExpanderElement();
            parent.Children.Add(expanderElement);
        }

        public ExpanderElement ExpanderElement
        {
            get
            {
                return expanderElement;
            }
        }

        public void Expand()
        {
            this.ExpanderElement.Expand();
        }

        public void Collapse()
        {
            this.ExpanderElement.Collapse();
        }

        protected virtual ExpanderElement CreateExpanderElement()
        {
            return new ExpanderElement();
        }

        protected override System.Drawing.Size DefaultSize
        {
            get
            {
                return new Size(80, 700);
            }
        }
    }
}
