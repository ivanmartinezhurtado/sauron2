﻿using TaskRunner.Model;

namespace TaskRunner.Tools
{
    public interface IActionBase
    {
        bool IsGroupAction { get; }
        string Name { get; }
        void PreExecute();
        void Execute();
        void PostExecute(StepResult stepResult);
    }
}
