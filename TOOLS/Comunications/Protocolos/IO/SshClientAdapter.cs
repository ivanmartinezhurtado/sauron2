﻿using Comunications.Ethernet;
using Comunications.Message;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.Threading;

namespace Comunications.Message
{
    public class SshClientAdapter : IStreamResource
    {
        private SSHClient ssh;
        private string NewLine = "\r\n";

        public SshClientAdapter(SSHClient sshClient, string newLine = "\r\n")
        {
            Debug.Assert(sshClient != null, "Argument sshClient cannot be null.");

            NewLine = newLine;

            ssh = sshClient;
        }
        
        public int InfiniteTimeout
        {         
            get { return Timeout.Infinite; }
        }

        public int ReadBufferSize
        {           
            get
            {
                return ReadBufferSize;
            }
            set
            {
                ReadBufferSize = value;
            }
        }

        private int readTimeout = 500;

        public int ReadTimeout
        {
            get
            {
                return readTimeout;
            }

            set
            {
                readTimeout = value;
            }
        }

        private int writeTimeout = 500;
        public int WriteTimeout
        {
            get
            {
                return writeTimeout;
            }

            set
            {
                writeTimeout = value;
            }
        }

        public string PortName
        {
            get
            {
                return ssh.Shell.GetType().FullName;
            }

            set
            {
                PortName = value;
            }
        }
       
        public void DiscardInBuffer()
        {
            ssh.Shell.Flush();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            if (ssh != null)
                ssh.Dispose();
        }

        public void HandleMessageException(Exception ex)
        {
            try
            {
                var e = ex.InnerException ?? ex;

                if (e is System.Net.Sockets.SocketException)
                {
                    var host = ssh.ConnectionInfo.Host;
                    var user = ssh.ConnectionInfo.Username;
                    var password = ssh.ConnectionInfo.ProxyPassword;

                    if (ssh.Shell != null)
                        ssh.Shell.Close();

                    if (ssh != null)
                        ssh.Dispose();

                    ssh = new SSHClient(host, user, password);                   
                }
            }
            catch
            {

            }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            return ssh.Shell.Read(buffer, offset, count);
        }

        public string ReadLineString()
        {
            var response = ssh.Shell.ReadLine(TimeSpan.FromMilliseconds(ReadTimeout));

            if (string.IsNullOrEmpty(response))
                return string.Empty;

            return response;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            ssh.Shell.Write(buffer, offset, count);
        }

        public void WriteLineString(string tx)
        {
            string resp;

            ssh.Shell.WriteLine(tx);
            do
            {
                resp = ReadLineString();
            }
            while (!resp .Contains(tx) && !string.IsNullOrEmpty(resp));
        }

        public void WriteLineStringAsBytes(string tx, int byteInterval = 50, bool writeCR = true, bool writeLF = false)
        {
            tx += NewLine;
            byte[] byteTx = System.Text.Encoding.ASCII.GetBytes(tx);
            Write(byteTx, 0, byteTx.Length);
        }
    }
}
