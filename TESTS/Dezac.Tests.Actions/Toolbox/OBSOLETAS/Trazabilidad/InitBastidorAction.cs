﻿using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(InitBastidorActionDescription))]
    [Obsolete("Usar la nueva action InitBastidorMultipleAction")]
    
    public class InitBastidorAction : ActionBase
    {
        public bool Trazabilidad { get; set; }

        public bool SortIntroComponents { get; set; }

        public bool AddSubconjuntosSinRegistrar { get; set; }

        public bool SearchNumSerieTestFaseBefore { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del TestBase");

            BastidorService.BastidorController(testBase);

            BastidorService.BastidorValidation(Trazabilidad, SortIntroComponents, SearchNumSerieTestFaseBefore, AddSubconjuntosSinRegistrar);

            System.Threading.Thread.Sleep(500);
        }
        public override void PostExecute(StepResult stepResult)
        {
            if (stepResult.ExecutionResult == StepExecutionResult.Failed)
                stepResult.ExecutionResult = StepExecutionResult.Aborted;
        }
    }

    public class InitBastidorActionDescription : ActionDescription
    {
        public override string Name { get { return "InitBastidor"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
    }
}
