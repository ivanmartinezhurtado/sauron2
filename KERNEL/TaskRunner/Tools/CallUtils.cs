﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TaskRunner.Tools
{
    public static class CallUtils
    {
        public static MethodInfo GetMethod(Object item, String methodName)
        {
            return GetMethod(item, methodName, null);
        }

        public static MethodInfo GetMethod(Object item, String methodName, params Type[] types)
        {
            MethodInfo m = null;

            if (item != null)
                m = GetMethod(item.GetType(), methodName, types);

            return m;
        }

        public static MethodInfo GetMethod(Type type, String methodName)
        {
            return GetMethod(type, methodName, null);
        }

        public static MethodInfo GetMethod(Type type, String methodName, params Type[] types)
        {
            MethodInfo m = null;

            if (type != null && !String.IsNullOrEmpty(methodName))
            {
                m = type.GetMethod(methodName, (types ?? Type.EmptyTypes));
                if (m == null && types != null && types.Length > 0)
                    m = type.GetMethods().Where(p => p.Name == methodName && p.GetParameters().Length == types.Length).FirstOrDefault();
            }

            return m;
        }

        public static Object InvokeMethod(Object entity, String methodName)
        {
            return InvokeMethod(entity, methodName, null, null);
        }

        public static Object InvokeMethod(Object entity, String methodName, Object[] args)
        {
            return InvokeMethod(entity, methodName, args, GetTypes(args));
        }

        public static Object InvokeMethod(Object entity, String methodName, Object[] args, Type[] types)
        {
            MethodInfo m = GetMethod(entity, methodName, types);

            if (m == null)
            {
                string format = "The method '{0}' with arguments '{1}' could not be located on the specified entity.";
                string typesValue = (types == null) ? "()" : "(" + GetTypeNames(types) + ")";
                throw new ArgumentException(string.Format(format, methodName, typesValue));
            }

            object[] callArgs = args;

            if (args != null && args.Length > 0)
            {
                ParameterInfo[] parameters = m.GetParameters();

                callArgs = new object[args.Length];

                for (int i = 0; i < args.Length; i++)
                {
                    object arg = args[i];
                    if (arg != null)
                    {
                        if (parameters[i].ParameterType.IsEnum)
                        {

                            if (int.TryParse(arg.ToString(), out int value))
                                arg = value;
                            else
                                arg = Enum.Parse(parameters[i].ParameterType, arg.ToString());
                        }
                        else
                            arg = Convert.ChangeType(arg, parameters[i].ParameterType);
                    }

                    callArgs[i] = arg;
                }
            }

            return m.Invoke(entity, callArgs);
        }

        public static string GetTypeNames(params Type[] types)
        {
            StringBuilder builder = new StringBuilder();

            foreach (Type type in types)
            {
                if (builder.Length > 0)
                    builder.Append(", ");

                builder.Append(type.Name);
            }

            return builder.ToString();
        }

        public static Type[] GetTypes(params Object[] args)
        {
            Type[] types = Type.EmptyTypes;

            if (args != null)
                types = Type.GetTypeArray(args);

            return types;
        }
    }
}
