﻿using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Services;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Xml;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.Laser
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(GetLaserFilesActionDescription))]
    public class GetLaserFilesAction : ActionBase
    {
        private string PathFilesLaser
        {
            get { return ConfigurationManager.AppSettings["PathFilesLaser"]; }
        }

        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null)
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA("Falta iniciar una función de carga de datos de contexto del test (InitProduct)").Throw();

            if (data.NumOrden != null && data.NumProducto == 0)
                Error().PROCESO.PARAMETROS_ERROR.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test").Throw();

            DeleteFilesTemp();

            BinaryItemViewModel fileXML;
            BinaryItemViewModel fileSVG;
            BinaryItemViewModel filePDF;

            using (DezacService svc = new DezacService())
            {
                fileXML = svc.GetBinario(data.NumProducto, data.Version, data.IdFase, "961");
                fileSVG = svc.GetBinario(data.NumProducto, data.Version, data.IdFase, "960");
                filePDF = svc.GetBinario(data.NumProducto, data.Version, data.IdFase, "12");
            }

            if (fileXML == null)
                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("No se ha podido obtener el fichero XML para este producto").Throw();
            if (fileSVG == null)
                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("No se ha podido obtener el fichero SVG para este producto").Throw();
            if (filePDF == null)
                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("No se ha podido obtener el fichero PDF para este producto").Throw();

            fileXML.PathFicheroTest = CreateFilesLaser(fileXML);
            ConfigurationXMLFile(fileXML);
            fileSVG.PathFicheroTest = CreateFilesLaser(fileSVG);
            filePDF.PathFicheroTest = CreateFilesLaser(filePDF);

            Process.Start("file:///" + filePDF.PathFicheroTest);
        }

        public void DeleteFilesTemp()
        {
            string url = PathFilesLaser;

            if (Directory.Exists(url))
                Directory.Delete(url, true);

            System.Threading.Thread.Sleep(1000);

            if (!Directory.Exists(url))
                Directory.CreateDirectory(url);
        }

        private void ConfigurationXMLFile(BinaryItemViewModel fileXML)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(fileXML.PathFicheroTest);

            XmlNodeList nodesList = xml.DocumentElement.ChildNodes[0].ChildNodes;

            foreach(XmlNode node in nodesList)
            {
                if (node.Name == "contatore")
                    node.RemoveAll();

                if(node.Name == "file")
                {
                    var attributes = node.Attributes;
                    foreach(XmlAttribute att in attributes)
                        if(att.Name == "nome_file")
                            att.Value = fileXML.PathFicheroTest + ".svg";
                }
            }

            xml.Save(fileXML.PathFicheroTest);
        }

        private string CreateFilesLaser(BinaryItemViewModel item)
        {
            if (!Directory.Exists(PathFilesLaser))
                Directory.CreateDirectory(PathFilesLaser);

            string name = string.Format("{0}", item.NombreFichero);

            string fileName = Path.Combine(PathFilesLaser, name);

            if (!File.Exists(fileName))
                File.WriteAllBytes(fileName, item.Fichero);

            return fileName;
        }
    }

    public class GetLaserFilesActionDescription : ActionDescription
    {
        public override string Name { get { return "GetLaserFilesPGI"; } }
        public override string Category { get { return "DezacTest/Oracle/FilesPGI"; } }
        public override string Description { get { return "Action que sirve para cargar la base de datos de los docuntos de láser que existen en el PGI a la secuencia"; } }
        public override Image Icon { get { return Properties.Resources.DatabaseFiles; } }
    }
}
