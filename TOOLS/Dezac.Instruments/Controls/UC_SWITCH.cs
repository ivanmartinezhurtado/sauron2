﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dezac.Core.Utility;
using Instruments.Switch;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(NetgearGS108TDescription))]
    public partial class UC_SWITCH : UserControl
    {
        private NetgearGS108T netgear;

        public Type InstrumentType
        {
            get
            {
                return typeof(NetgearGS108T);
            }
        }

        public NetgearGS108T Instrument
        {
            get
            {
                if (netgear == null)
                    netgear = new NetgearGS108T();

                return netgear;
            }
            set
            {
                netgear = value;
            }
        }

        public UC_SWITCH()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }

    public class NetgearGS108TDescription : ControlDescription
    {
        public override string Name { get { return "NetgearGS108T"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.router_icon; } }
        public override Image IconBig { get { return Properties.Resources.router_icon; } }
        public override Type SubcomponentType { get { return typeof(NetgearGS108T); } }
        public override bool Visible => false;
    }
}