﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using TaskRunner.Tools;

namespace TaskRunner.Model
{
    public class SharedVariablesList
    {
        private static readonly object _syncObject = new Object();
        private static readonly ILog logger = LogManager.GetLogger("VariablesList");

        public EventHandler<VariableLogEntry> EntryAdded;
        private void EntryAddedInvoke(object sender, VariableLogEntry entry)
        {
            if (EntryAdded != null)
                EntryAdded(this, entry);
        }

        private ConcurrentDictionary<string, object> variablesList = new ConcurrentDictionary<string, object>();
        public ReadOnlyDictionary<string, object> VariableList
        {
            get
            {
                return new ReadOnlyDictionary<string, object>(variablesList);
            }
        }

        private VariableLog variableLogger = new VariableLog();
        public VariableLog VariableLogger
        {
            get
            {
                return variableLogger;
            }
        }

        public int NumInstance { get; private set; }
        public SharedVariablesList()
        {
            variableLogger.EntryAdded += EntryAddedInvoke;
        }
        public SharedVariablesList(int numInstance)
        {
            NumInstance = numInstance;
            variableLogger.EntryAdded += EntryAddedInvoke;
        }

        public bool TryGetValue(string key, out object value)
        {
            value = Get(key);
            if (key != null)
                return true;

            return false;
        }
        public object Get(string key)
        {
            if (variablesList.ContainsKey(key))
                return variablesList[key];

            //Get Properties
            key = key.Replace("-", ".");
           
            if (Regex.IsMatch(key, VariablesResolver.REGEX_VARIABLE_INDEX))
            {
                var indexVars = Regex.Matches(key, VariablesResolver.REGEX_VARIABLE_INDEX);
                foreach (Match indexVar in indexVars)
                {
                    var valueIndexVar = Get(indexVar.Value.Replace("[", "").Replace("]", "").Replace("$", "")).ToString();
                    key = key.Replace(indexVar.Value.Replace("[", "").Replace("]", ""), valueIndexVar);
                }
            }           

            object value = null;
            string[] tokens = key.Split('.');
            string tokenWithOutIndex = Regex.Replace(tokens[0], VariablesResolver.REGEX_ENUMERABLE_INDEX, "");
            if (tokenWithOutIndex != tokens[0] || tokens.Length > 1)
            {
                value = Get(tokenWithOutIndex);
                value = VariablesResolver.GetEnumerableElement(value, tokens[0]);
            }

            if (value == null)
                return null;

            for (int i = 1; i < tokens.Length; i++)
            {
                tokenWithOutIndex = Regex.Replace(tokens[i], VariablesResolver.REGEX_ENUMERABLE_INDEX, "");
                value = VariablesResolver.GetPropertyValue(value, tokenWithOutIndex);
                value = VariablesResolver.GetEnumerableElement(value, tokens[i]);
            }
            return value;
        }
        public T Get<T>(string key)
        {
            object result = Get(key);
            if (result == null)
                return default;
            if (result is T)
                return (T)result;
            else
                return RunnerHelper.ChangeType<T>(result);
        }
        public T Get<T>(string key, Func<T> def)
        {
            object result = Get(key);
            if (result != null)
            {
                if (result is T)
                    return (T)result;
                else
                    return RunnerHelper.ChangeType<T>(result);
            }

            if (def != null)
                return def();

            return default;
        }
        public T Get<T>(string key, T def) => Get(key, () => def);
        public T Get<T>()
        {
            Type type = typeof(T);

            object variable = variablesList.Where(p => p.Value != null && p.Value.GetType() == type)
                .Select(p => p.Value)
                .FirstOrDefault();

            if (variable != null)
                return (T)variable;

            return default(T);
        }
   
        public void AddOrUpdate(string key, object value)
        {
            if (string.IsNullOrEmpty(key) || value == null)
                return;

            VariablesResolver.VaribleKeyValidation(ref key);

            if (!key.Contains("."))
            {
                variablesList.AddOrUpdate(key, value, (k, curValue) => value);
                logger.Debug($"Variable added or updated: {key} = {variablesList[key]}");
                variableLogger.AddEntry(NumInstance, key, variablesList[key]);
                return;
            }

            //Looking For Fields and Properties
            string[] tokens = key.Split('.');
            object instance = Get(tokens[0]);
            if (instance == null)
                throw new Exception($"La variable {key} parece referirse a una propiedad de una estructura pero la variable {tokens[0]} no existe.");

            VariablesResolver.SetVariableProperty(instance, key, value);
            logger.Debug($"Modificacion de una propiedad de la variable {tokens[0]}: {key} = {value}");
            variableLogger.AddEntry(NumInstance, tokens[0], variablesList[tokens[0]]);
        }

        public T AddOrGet<T>(string key, Func<T> func)
        {
            if (string.IsNullOrEmpty(key))
                throw new Exception($"La funcion AddOrGet no admite variable con key == null");

            if (key.Contains("."))
                throw new Exception($"La funcion AddOrGet no admite referencias a propiedades, para ello usar AddOrUpdate");

            VariablesResolver.VaribleKeyValidation(ref key);
            string refKey = "_referenceCounter_" + key;
            lock (_syncObject)
            {
                object result = variablesList.GetOrAdd(key, (k) =>
                {
                    variablesList[refKey] = 0;
                    return func();
                });

                variablesList[refKey] = (int)variablesList[refKey] + 1;

                logger.Debug($"Variable added or updated: {key} = {variablesList[key]}");
                variableLogger.AddEntry(NumInstance, key, variablesList[key]);

                return RunnerHelper.ChangeType<T>(result);
            }
        }

        public bool ContainsKey(string key)
        {
            if (variablesList.ContainsKey(key))
                return true;
            
            key = key.Replace("-", ".");

            if (key.Contains("."))
                if (variablesList.ContainsKey(key.Split('.')[0]))
                    return true;
            return false;
        }

        public bool RemoveInstance(object instance)
        {
            if (instance == null)
                return false;

            return Remove(variablesList.Where(p => p.Value == instance).Select(p => p.Key).FirstOrDefault());
        }

        public bool Remove(string key)
        {
            if (string.IsNullOrEmpty(key))
                return false;
            
            lock (_syncObject)
            {
                if (variablesList.ContainsKey(key))
                {
                    variablesList.TryRemove(key, out object value);

                    //if (VariableChangued != null)
                    //    VariableChangued(this, new VariableChanguedEventArgs() { NumInstance = NumInstance, Key = key, Value = null });
                    return true;
                }
            }

            return false;
        }
    }

    public class VariableChanguedEventArgs : EventArgs
    {
        public int NumInstance { get; set; }
        public string Key { get; set; }
        public Object Value { get; set; }
    }
}
