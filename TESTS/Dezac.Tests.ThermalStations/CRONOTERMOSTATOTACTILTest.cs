﻿using Dezac.Device.ThermalStations;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.ThermalStations
{
    [TestVersion(1.12)]
    public class CRONOTERMOSTATOTACTILTest : TestBase 
    {
        CRONOTERMOSTATOTACTIL crono;
        CENTRALKSP central;
        Tower3 tower;
        string CAMERA_IDS;
        bool hasRS485 = false;

        public void TestInitialization()
        {
            crono = new CRONOTERMOSTATOTACTIL(Comunicaciones.SerialPort);
            central = new CENTRALKSP(Comunicaciones.SerialPortModulo);

            hasRS485 = Configuracion.GetString("TIENE_RS_485", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI";

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.Chroma.ApplyOffAndWaitStabilisation();

            tower.IO.DO.PulseOn(500, 17, 18);
        }

        public void TestSupplyPoints()
        {
            var voltageBateria = Consignas.GetDouble(Params.V.Null.Bateria.Name, 3.3, ParamUnidad.V);
            var currentBateria = Consignas.GetDouble(Params.I.Null.Bateria.Name, 0.5, ParamUnidad.A);
            var voltageVCCExt = Consignas.GetDouble(Params.V.Null.TestPoint("VCC_EXT(RS485)").Name, 12, ParamUnidad.V);
            var currentVCCExt = Consignas.GetDouble(Params.I.Null.TestPoint("VCC_EXT(RS485)").Name, 12, ParamUnidad.A);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.Off(17, 18);

            Delay(500, "Espera apagado equipo");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltageBateria, currentBateria);

            Delay(3000, "Esperando encendido del equipo");

            ushort version = 0;

            try
            {
                version = crono.ReadSoftwareVersion();
            }
            catch (TimeoutException)
            {
                Resultado.Set("SUPPLY_POINTS", "KO", ParamUnidad.SinUnidad);
                Resultado.Set("COMM_BOARD", "KO", ParamUnidad.SinUnidad);

                throw new Exception("La alimentación del equipo por los terminales estandar o las comunicaciones por test points no funciona correctamente");
            }
 
            Resultado.Set("SUPPLY_POINTS", "OK", ParamUnidad.SinUnidad);

            if (Comunicaciones.SerialPort == 7)
                Resultado.Set("COMM_BOARD", "OK", ParamUnidad.SinUnidad);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.On(17);

            if (hasRS485)
            {

                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltageVCCExt, currentVCCExt);

                Delay(3000, "Esperando encendido del equipo");

                try
                {
                    version = crono.ReadSoftwareVersion();
                }
                catch (TimeoutException)
                {
                    Resultado.Set("SUPPLY_POINTS_VCCEXT", "KO", ParamUnidad.SinUnidad);
                    throw new Exception("La alimentación del equipo por terminales externos no funciona correctamente");
                }

                Resultado.Set("SUPPLY_POINTS_VCCEXT", "OK", ParamUnidad.SinUnidad);

                tower.LAMBDA.ApplyOffAndWaitStabilisation();
            }

            tower.IO.DO.On(18);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltageBateria, currentBateria);

            Delay(3000, "Esperando encendido del equipo");

            SamplerWithCancel((step) =>
            {
                try
                {
                    version = crono.ReadSoftwareVersion();
                }
                catch (Exception)
                {
                    if (step < 2)
                    Shell.MsgBox("Compruebe que los cables que van al tester han sido conectados correctamente", "Interacción del usuario - Revisar conexionado", MessageBoxButtons.OK);
                    else
                        Resultado.Set("SUPPLY_POINTS_BATTERY_TERMINALS", "KO", ParamUnidad.SinUnidad);

                    return false;
                }

                return true;
            }, "La alimentación por batería no funciona correctamente, compruebe que las chapas del portapilas estan correctamente montadas y soldadas", cancelOnException: false, throwException: false);

            Resultado.Set("SUPPLY_POINTS_BATTERY_TERMINALS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestCommunications()
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.Off(17, 18);

            Delay(500, "Espera apagado equipo");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3.3, 0.5);

            Delay(3000, "Esperando encendido del equipo");

            ushort version = 0;

            SamplerWithCancel((p) =>
            {
                version = crono.ReadSoftwareVersion();

                return true;

            }, "Error de comunicación, el equipo no comunica", throwException:false, cancelOnException:false);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la version de firmware del equipo es incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestDisplay()
        {
            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            var voltageBateria = Consignas.GetDouble(Params.V.Null.Bateria.Name, 3.3, ParamUnidad.V);
            var currentBateria = Consignas.GetDouble(Params.I.Null.Bateria.Name, 0.5, ParamUnidad.A);
            var voltageDisplay = Consignas.GetDouble(Params.V.Null.TestPoint("DISPLAY").Name, 2.6, ParamUnidad.V);
            var voltageVCCExt = Consignas.GetDouble(Params.V.Null.TestPoint("VCC_EXT(RS485)").Name, 12, ParamUnidad.V);
            var currentVCCExt = Consignas.GetDouble(Params.I.Null.TestPoint("VCC_EXT(RS485)").Name, 12, ParamUnidad.A);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltageDisplay, currentBateria);

            Delay(2000, "Esperando que se encienda el display");

            crono.WriteBacklightAndDisplay(true);

            Delay(2000, "Esperando que se encienda el display");
            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "CRONO_ALL", "ALL", "CRONO");

            crono.WriteBacklightAndDisplay(false);

            Resultado.Set("DISPLAY", "OK", ParamUnidad.SinUnidad);

            if (hasRS485)
            {
                tower.IO.DO.Off(18);
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltageVCCExt, currentVCCExt);
            }
            else
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltageBateria, currentBateria);
        }

        public void TestKeyboard()
        {
            tower.IO.DO.On(4);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(0, 0);
            Delay(2000, "Espera Apagado - Encendido");
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3.3, 0.5);
            crono.WriteKeyboardTest();
            Delay(2000, "Enviando trama modo test teclado");
            var key = crono.ReadKeyboard();
            Assert.AreEqual("NO_KEY", key.ToString(), CRONOTERMOSTATOTACTIL.Keys.NO_KEY_OR_MORE.ToString(), Error().UUT.HARDWARE.DISPLAY("Error, se detecta tecla pulsada cuando no se debería haber pulsado ninguna"), ParamUnidad.SinUnidad);
                                    
            foreach (CRONOTERMOSTATOTACTIL.Keys expectedKey in Enum.GetValues(typeof(CRONOTERMOSTATOTACTIL.Keys)))
            {
                if (expectedKey == CRONOTERMOSTATOTACTIL.Keys.NO_KEY_OR_MORE)
                    continue;

                SamplerWithCancel((p) =>
                    {                       

                        switch(expectedKey) 
                        {
                            case CRONOTERMOSTATOTACTIL.Keys.INFO:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_INFO);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_INFO);
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.SET:
                                tower.Chroma.SetVoltageRange();
                                tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);
                                tower.Chroma.ApplyOff();
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.WORK_MODE:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_WORK_MODE);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_WORK_MODE);
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.PLUS:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_PLUS);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_PLUS);
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.ENTER:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_ENTER);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_ENTER);
                                break;
                           case CRONOTERMOSTATOTACTIL.Keys.MINUS:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_MINUS);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_MINUS);
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.FAN:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_FAN);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_FAN);
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.PROGRAM:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_PROGRAM);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_PROGRAM);
                                break;
                            case CRONOTERMOSTATOTACTIL.Keys.ECO:
                                tower.IO.DO.On((ushort)PISTONES.ACTIVA_ECO);
                                Delay(1000, "Pulsando la tecla");
                                tower.IO.DO.Off((ushort)PISTONES.ACTIVA_ECO);
                                break;
                            default:
                                break;
                        }                  

                        var lastKey = crono.ReadKeyboard();
                        if (lastKey != expectedKey)
                            return false;
                        
                        Resultado.Set("KEY_" + expectedKey.ToString(), "OK", ParamUnidad.SinUnidad);
                        return true;

                    }, string.Format("Error de teclado, no se ha detectado la tecla {0}", expectedKey.ToString()), 6);
            }

            var keysToPress = CheckKeysToPress();
            if (keysToPress.Count > 0)
                throw new Exception("Error de teclado, el equipo no ha detectado la pulsacion de todas las teclas");
            
            tower.IO.DO.Off(4);
        }

        public void TestRadioCommunications()
        {
            central.WriteMessageFrameClear();
            central.WriteFrequencySelection(CENTRALKSP.RadioFrequency._434MHz);

            crono.Write434MHzFrequencyTest();

            TestMeasureBase(new AdjustValueDef(Params.FREQ.Null.TestPoint(CENTRALKSP.RadioFrequency._434MHz.ToString().ToUpper().Replace("_", string.Empty)), ParamUnidad.Hz),
                (step) =>
                {
                    return central.ReadMessageFramesReceived();
                }, 30, 45, 1000);

            central.WriteMessageFrameClear();
            central.WriteFrequencySelection(CENTRALKSP.RadioFrequency._433MHz);

            crono.Write433MHzFrequencyTest();

            TestMeasureBase(new AdjustValueDef(Params.FREQ.Null.TestPoint(CENTRALKSP.RadioFrequency._433MHz.ToString().ToUpper().Replace("_", string.Empty)), ParamUnidad.Hz),
                (step) =>
                {
                    return central.ReadMessageFramesReceived();
                }, 30, 45, 1000);


            tower.HP53131A.PresetAndConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_OFF, Instruments.Measure.HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            tower.HP53131A.PresetAndConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.LOW, 1.8);
            tower.HP53131A.PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 10);

            TestMeasureBase(new AdjustValueDef(Params.FREQ.Null.TestPoint("TRANSCEIVER"), ParamUnidad.Hz),
               (step) =>
               {
                   return tower.HP53131A.RunMeasure(HP53131A.Variables.FREQ, 5000).Value;

               }, 0, 5, 1000);

        }

        public void TestTemperature(string portOptris)
        {
            SamplerWithCancel((p) =>
                {
                    var reading = 0.0D;

                    if (Configuracion.GetString("TEMPERATURA_AUTOMATIZADA", "NO", ParamUnidad.SinUnidad) == "NO")
                        reading = 25;
                    else
                    //var optrisPort = Convert.ToByte(GetVariable<string>(portOptris, portOptris).Replace("COM", ""));

                        using (var optris = new OPTRIS_CS(16)) //NO FUNCIONA WMI SE HARDCODEA EL PUERTO MOMENTANEAMENTE
                        {
                            optris.Initialization();
                            reading = optris.ReadTemperature();
                        }

                    var temp = new AdjustValueDef(Params.TEMP.Null.RTC.Name, 0, reading - Params.TEMP.Null.RTC.Tol(), reading + Params.TEMP.Null.RTC.Tol(), 0, 0, ParamUnidad.Grados);

                    var result = TestMeasureBase(temp,
                     (step) =>
                     {
                         crono.WriteTemperatureTest();

                         Delay(3000, "Esperando la lectura de temperatura");

                         return crono.ReadAndCheckTemperatureProbe();

                     }, 0, 3, 2000);

                    Resultado.Set(Params.TEMP.Null.PATRON.Name, reading, ParamUnidad.Grados);

                    return true;

                }, "Error, temperatura medida por el equipo fuera de margenes del patron", 2);

            crono.WriteEndTest();
        }

        public void TestConsumo()
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.Off(17, 18);

            tower.IO.DO.On(17, 18);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3.3, 0.5);

            Delay(8000, "Espera encendido equipo");

            var consumo = new AdjustValueDef(Params.I_DC.Null.TestPoint("NOMINAL").Name, 0, Params.I_DC.Null.TestPoint("NOMINAL").Min(), Params.I_DC.Null.TestPoint("NOMINAL").Max(), 0, 0, ParamUnidad.A);

            TestMeasureBase(consumo, (step) =>
                {
                    if (Configuracion.GetString("AUTOMATIZADO", "NO", ParamUnidad.SinUnidad) == "NO")
                        return Shell.ShowDialog<double>("Lectura de corriente del tester",
                            () => new InputKeyBoard("Introduce la lectura del multímetro en mA (miliamperios)", "9.99"), MessageBoxButtons.OKCancel,
                            (c, r) => { return r == DialogResult.OK ? Convert.ToDouble(((InputKeyBoard)c).TextValue) : -1; });
                    else
                        return tower.HP34401A.SCPI.MEASure.CURRent.DC().QueryNumber().Value * 1000;

                }, 0, 2, 500);

            crono.WriteConsumptionMeasure();

            Delay(5000, "Espera encendido equipo");

            consumo = new AdjustValueDef(Params.I_DC.Null.TestPoint("SUSPENSION").Name, 0, Params.I_DC.Null.TestPoint("SUSPENSION").Min(), Params.I_DC.Null.TestPoint("SUSPENSION").Max(), 0, 0, ParamUnidad.A);

            TestMeasureBase(consumo, (step) =>
            {
                    if (Configuracion.GetString("AUTOMATIZADO", "NO", ParamUnidad.SinUnidad) == "NO")
                        return Shell.ShowDialog<double>("Lectura de corriente del tester",
                            () => new InputKeyBoard("Introduce la lectura del multímetro en mA (miliamperios)", "9.99"), MessageBoxButtons.OKCancel,
                            (c, r) => { return r == DialogResult.OK ? Convert.ToDouble(((InputKeyBoard)c).TextValue) : -1; });
                    else
                        return tower.HP34401A.SCPI.MEASure.CURRent.DC().QueryNumber().Value * 1000000;

             }, 0, 2, 500);
        }

        public void TestFinish()
        {
            if (crono != null)
                crono.Dispose();

            if (central != null)
                central.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                tower.IO.DO.Off(15, 17, 18, 22);

                tower.Dispose();
            }
        }

        private CRONOTERMOSTATOTACTIL.Keys KeyToCheck(CRONOTERMOSTATOTACTIL.Keys key)
        {
            CRONOTERMOSTATOTACTIL.Keys keyPressed = CRONOTERMOSTATOTACTIL.Keys.NO_KEY_OR_MORE;
            keyPressed = crono.ReadKeyboard();

            if (keyPressed == CRONOTERMOSTATOTACTIL.Keys.NO_KEY_OR_MORE)
            {
                Shell.MsgBox(string.Format("No se ha detectado tecla pulsada o se ha pulsado mas de una tecla, porfavor, pulse solamente la tecla {0}", key.ToString()), "TEST TECLADO");
                throw new Exception("Tecla pulsada incorrecta");
            }

            return keyPressed;
        }

        private List<CRONOTERMOSTATOTACTIL.MaskedKeys> CheckKeysToPress()
        {
            var keysToPress = Enum.GetValues(typeof(CRONOTERMOSTATOTACTIL.MaskedKeys)).Cast<CRONOTERMOSTATOTACTIL.MaskedKeys>().ToList();

            var keysPressed = crono.ReadKeyboardMask();

            keysPressed.ForEach(x=> keysToPress.Remove(x));

            return keysToPress;
        }

        public enum PISTONES
        {
            ACTIVA_INFO = 5,
            ACTIVA_WORK_MODE = 6,
            ACTIVA_PLUS = 7,
            ACTIVA_ENTER = 8,
            ACTIVA_MINUS = 9,
            ACTIVA_FAN = 10,
            ACTIVA_PROGRAM = 11,
            ACTIVA_ECO = 12
        }
    }
}
