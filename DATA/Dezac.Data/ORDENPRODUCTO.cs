namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ORDENPRODUCTO")]
    public partial class ORDENPRODUCTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDENPRODUCTO()
        {
            ORDENPRODUCTOERROR = new HashSet<ORDENPRODUCTOERROR>();
            REPROCESOFAB = new HashSet<REPROCESOFAB>();
            T_TEST = new HashSet<T_TEST>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMFABRICACION { get; set; }

        public int NUMORDEN { get; set; }

        public int NUMPRODUCTO { get; set; }

        public DateTime FECHA { get; set; }

        public int? NUMMATRICULA { get; set; }

        [StringLength(20)]
        public string NROSERIE { get; set; }

        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        [StringLength(1)]
        public string PRODUCTOVALIDO { get; set; }

        public DateTime? FECHAMAT { get; set; }

        [StringLength(1)]
        public string SITUACION { get; set; }

        public DateTime? FECHASIT { get; set; }

        public int? NUMCAJA { get; set; }

        public virtual MATRICULA MATRICULA { get; set; }

        public virtual ORDEN ORDEN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENPRODUCTOERROR> ORDENPRODUCTOERROR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPROCESOFAB> REPROCESOFAB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TEST> T_TEST { get; set; }
    }
}
