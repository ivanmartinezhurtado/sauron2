﻿using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public class ResultsList : List<ResultItem>
    {
        public event EventHandler<ResultItem> ItemAdded;

        public void Add(object value)
        {
            Add(null, value);
        }

        public void Add(string name, object value)
        {
            SequenceContext context = SequenceContext.Current;
            ResultItem item = new ResultItem
            {
                Name = name,
                NumInstance = context.NumInstance,
                Step = context.Step,
                Timestamp = DateTime.Now,
                Value = value
            };

            base.Add(item);

            if (ItemAdded != null)
                ItemAdded(this, item);
        }
    }
}
