﻿namespace WinTestPlayer.Views
{
    partial class IndicatorsView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblIndicadores = new System.Windows.Forms.Label();
            this.pnlIndicadores = new Telerik.WinControls.UI.RadPanel();
            this.rgFPY = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.lblTotal = new Telerik.WinControls.UI.RadLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.radialGaugeArc11 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc12 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.lblFPY = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.rgKO = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.label4 = new System.Windows.Forms.Label();
            this.radialGaugeArc5 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc6 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.lblKO = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.rgOK = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.label2 = new System.Windows.Forms.Label();
            this.radialGaugeArc1 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc2 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.lblOK = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            this.pnlTiempos = new Telerik.WinControls.UI.RadPanel();
            this.radLabelTiempoMedio = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempoCiclo = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempoTest = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.lblHoraActual = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempoCicloPrevisto = new Telerik.WinControls.UI.RadLabel();
            this.lblHour = new Telerik.WinControls.UI.RadLabel();
            this.lblTiempos = new Telerik.WinControls.UI.RadLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lblEmbalaje = new System.Windows.Forms.Label();
            this.pnlEmbalaje = new Telerik.WinControls.UI.RadPanel();
            this.rgEquiposCaja = new Telerik.WinControls.UI.Gauges.RadRadialGauge();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lblUnidadesCaja = new Telerik.WinControls.UI.RadLabel();
            this.bdBoxReport = new System.Windows.Forms.BindingSource(this.components);
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.lblCajaActual = new Telerik.WinControls.UI.RadLabel();
            this.radialGaugeArc9 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeArc10 = new Telerik.WinControls.UI.Gauges.RadialGaugeArc();
            this.radialGaugeSingleLabel3 = new Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pnlIndicadores)).BeginInit();
            this.pnlIndicadores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgFPY)).BeginInit();
            this.rgFPY.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgKO)).BeginInit();
            this.rgKO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgOK)).BeginInit();
            this.rgOK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTiempos)).BeginInit();
            this.pnlTiempos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTiempoMedio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCiclo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHoraActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCicloPrevisto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlEmbalaje)).BeginInit();
            this.pnlEmbalaje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgEquiposCaja)).BeginInit();
            this.rgEquiposCaja.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidadesCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdBoxReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCajaActual)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIndicadores
            // 
            this.lblIndicadores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIndicadores.AutoSize = true;
            this.lblIndicadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIndicadores.ForeColor = System.Drawing.Color.Navy;
            this.lblIndicadores.Location = new System.Drawing.Point(4, 169);
            this.lblIndicadores.Name = "lblIndicadores";
            this.lblIndicadores.Size = new System.Drawing.Size(79, 16);
            this.lblIndicadores.TabIndex = 15;
            this.lblIndicadores.Text = "Indicadores";
            // 
            // pnlIndicadores
            // 
            this.pnlIndicadores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIndicadores.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlIndicadores.Controls.Add(this.rgFPY);
            this.pnlIndicadores.Controls.Add(this.rgKO);
            this.pnlIndicadores.Controls.Add(this.rgOK);
            this.pnlIndicadores.Location = new System.Drawing.Point(3, 189);
            this.pnlIndicadores.Name = "pnlIndicadores";
            this.pnlIndicadores.Size = new System.Drawing.Size(154, 354);
            this.pnlIndicadores.TabIndex = 9;
            // 
            // rgFPY
            // 
            this.rgFPY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgFPY.BackColor = System.Drawing.Color.AliceBlue;
            this.rgFPY.CausesValidation = false;
            this.rgFPY.Controls.Add(this.radLabel7);
            this.rgFPY.Controls.Add(this.lblTotal);
            this.rgFPY.Controls.Add(this.label7);
            this.rgFPY.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc11,
            this.radialGaugeArc12,
            this.lblFPY});
            this.rgFPY.Location = new System.Drawing.Point(2, 215);
            this.rgFPY.Name = "rgFPY";
            this.rgFPY.Size = new System.Drawing.Size(147, 135);
            this.rgFPY.StartAngle = 180D;
            this.rgFPY.SweepAngle = 180D;
            this.rgFPY.TabIndex = 17;
            this.rgFPY.Text = "radRadialGauge6";
            this.rgFPY.Value = 50F;
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.ForeColor = System.Drawing.Color.Navy;
            this.radLabel7.Location = new System.Drawing.Point(16, 112);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(63, 18);
            this.radLabel7.TabIndex = 23;
            this.radLabel7.Text = "Total OF:";
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = false;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTotal.Location = new System.Drawing.Point(78, 107);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(57, 28);
            this.lblTotal.TabIndex = 22;
            this.lblTotal.Text = "100";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(56, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "FPY";
            // 
            // radialGaugeArc11
            // 
            this.radialGaugeArc11.BackColor = System.Drawing.Color.Navy;
            this.radialGaugeArc11.BackColor2 = System.Drawing.Color.Navy;
            this.radialGaugeArc11.BindEndRange = true;
            this.radialGaugeArc11.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc11.Name = "radialGaugeArc11";
            this.radialGaugeArc11.RangeEnd = 50D;
            this.radialGaugeArc11.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc11.Width = 40D;
            // 
            // radialGaugeArc12
            // 
            this.radialGaugeArc12.BackColor = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc12.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc12.BackColor3 = System.Drawing.Color.Black;
            this.radialGaugeArc12.BindStartRange = true;
            this.radialGaugeArc12.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc12.Name = "radialGaugeArc12";
            this.radialGaugeArc12.RangeEnd = 100D;
            this.radialGaugeArc12.RangeStart = 50D;
            this.radialGaugeArc12.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc12.Width = 40D;
            // 
            // lblFPY
            // 
            this.lblFPY.AccessibleDescription = "(%)";
            this.lblFPY.AccessibleName = "(%)";
            this.lblFPY.BindValue = true;
            this.lblFPY.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblFPY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFPY.ForeColor = System.Drawing.Color.Black;
            this.lblFPY.LabelFontSize = 12F;
            this.lblFPY.LabelFormat = "##,#";
            this.lblFPY.LabelText = "";
            this.lblFPY.LocationPercentage = new System.Drawing.SizeF(0F, -0.1F);
            this.lblFPY.Name = "lblFPY";
            this.lblFPY.Text = "";
            this.lblFPY.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // rgKO
            // 
            this.rgKO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgKO.BackColor = System.Drawing.Color.AliceBlue;
            this.rgKO.CausesValidation = false;
            this.rgKO.Controls.Add(this.label4);
            this.rgKO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rgKO.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc5,
            this.radialGaugeArc6,
            this.lblKO});
            this.rgKO.Location = new System.Drawing.Point(2, 109);
            this.rgKO.Name = "rgKO";
            this.rgKO.RangeEnd = 10D;
            this.rgKO.Size = new System.Drawing.Size(147, 135);
            this.rgKO.StartAngle = 180D;
            this.rgKO.SweepAngle = 180D;
            this.rgKO.TabIndex = 16;
            this.rgKO.Text = "radRadialGauge3";
            this.rgKO.Value = 0F;
            this.rgKO.DoubleClick += new System.EventHandler(this.rgKO_DoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Firebrick;
            this.label4.Location = new System.Drawing.Point(60, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "NG";
            // 
            // radialGaugeArc5
            // 
            this.radialGaugeArc5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(66)))), ((int)(((byte)(68)))));
            this.radialGaugeArc5.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(88)))), ((int)(((byte)(88)))));
            this.radialGaugeArc5.BindEndRange = true;
            this.radialGaugeArc5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc5.Name = "radialGaugeArc5";
            this.radialGaugeArc5.RangeEnd = 0D;
            this.radialGaugeArc5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc5.Width = 40D;
            // 
            // radialGaugeArc6
            // 
            this.radialGaugeArc6.BackColor = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc6.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc6.BackColor3 = System.Drawing.Color.Black;
            this.radialGaugeArc6.BindStartRange = true;
            this.radialGaugeArc6.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc6.Name = "radialGaugeArc6";
            this.radialGaugeArc6.RangeEnd = 50D;
            this.radialGaugeArc6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc6.Width = 40D;
            // 
            // lblKO
            // 
            this.lblKO.BindValue = true;
            this.lblKO.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblKO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKO.ForeColor = System.Drawing.Color.Black;
            this.lblKO.LabelFontSize = 12F;
            this.lblKO.LabelText = "Text";
            this.lblKO.LocationPercentage = new System.Drawing.SizeF(0F, -0.1F);
            this.lblKO.Name = "lblKO";
            this.lblKO.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // rgOK
            // 
            this.rgOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgOK.BackColor = System.Drawing.Color.AliceBlue;
            this.rgOK.CausesValidation = false;
            this.rgOK.Controls.Add(this.label2);
            this.rgOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rgOK.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc1,
            this.radialGaugeArc2,
            this.lblOK});
            this.rgOK.Location = new System.Drawing.Point(2, 5);
            this.rgOK.Name = "rgOK";
            this.rgOK.RangeEnd = 800D;
            this.rgOK.Size = new System.Drawing.Size(147, 135);
            this.rgOK.StartAngle = 180D;
            this.rgOK.SweepAngle = 180D;
            this.rgOK.TabIndex = 15;
            this.rgOK.Text = "radRadialGauge1";
            this.rgOK.Value = 300F;
            this.rgOK.DoubleClick += new System.EventHandler(this.rgOK_DoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
            this.label2.Location = new System.Drawing.Point(61, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "OK";
            // 
            // radialGaugeArc1
            // 
            this.radialGaugeArc1.AngleTransform = 0F;
            this.radialGaugeArc1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(190)))), ((int)(((byte)(79)))));
            this.radialGaugeArc1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(191)))), ((int)(((byte)(80)))));
            this.radialGaugeArc1.BindEndRange = true;
            this.radialGaugeArc1.BindEndRangeOffset = 180D;
            this.radialGaugeArc1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc1.Name = "radialGaugeArc1";
            this.radialGaugeArc1.RangeEnd = 480D;
            this.radialGaugeArc1.Text = "º";
            this.radialGaugeArc1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc1.Width = 40D;
            // 
            // radialGaugeArc2
            // 
            this.radialGaugeArc2.AngleTransform = 0F;
            this.radialGaugeArc2.BackColor = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc2.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc2.BackColor3 = System.Drawing.Color.AliceBlue;
            this.radialGaugeArc2.BindStartRange = true;
            this.radialGaugeArc2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc2.Name = "radialGaugeArc2";
            this.radialGaugeArc2.RangeEnd = 800D;
            this.radialGaugeArc2.RangeStart = 300D;
            this.radialGaugeArc2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc2.Width = 40D;
            // 
            // lblOK
            // 
            this.lblOK.BindValue = true;
            this.lblOK.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOK.ForeColor = System.Drawing.Color.Black;
            this.lblOK.LabelFontSize = 12F;
            this.lblOK.LabelText = "Text";
            this.lblOK.LocationPercentage = new System.Drawing.SizeF(0F, -0.1F);
            this.lblOK.Name = "lblOK";
            this.lblOK.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // pnlTiempos
            // 
            this.pnlTiempos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTiempos.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlTiempos.Controls.Add(this.radLabelTiempoMedio);
            this.pnlTiempos.Controls.Add(this.radLabel6);
            this.pnlTiempos.Controls.Add(this.lblTiempoCiclo);
            this.pnlTiempos.Controls.Add(this.lblTiempoTest);
            this.pnlTiempos.Controls.Add(this.radLabel2);
            this.pnlTiempos.Controls.Add(this.lblHoraActual);
            this.pnlTiempos.Controls.Add(this.lblTiempoCicloPrevisto);
            this.pnlTiempos.Controls.Add(this.lblHour);
            this.pnlTiempos.Location = new System.Drawing.Point(3, 568);
            this.pnlTiempos.MinimumSize = new System.Drawing.Size(150, 260);
            this.pnlTiempos.Name = "pnlTiempos";
            // 
            // 
            // 
            this.pnlTiempos.RootElement.MinSize = new System.Drawing.Size(150, 260);
            this.pnlTiempos.Size = new System.Drawing.Size(154, 262);
            this.pnlTiempos.TabIndex = 22;
            // 
            // radLabelTiempoMedio
            // 
            this.radLabelTiempoMedio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabelTiempoMedio.AutoSize = false;
            this.radLabelTiempoMedio.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelTiempoMedio.ForeColor = System.Drawing.Color.Navy;
            this.radLabelTiempoMedio.Location = new System.Drawing.Point(5, 31);
            this.radLabelTiempoMedio.Name = "radLabelTiempoMedio";
            this.radLabelTiempoMedio.Size = new System.Drawing.Size(141, 36);
            this.radLabelTiempoMedio.TabIndex = 23;
            this.radLabelTiempoMedio.Text = "00:00";
            this.radLabelTiempoMedio.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.ForeColor = System.Drawing.Color.Navy;
            this.radLabel6.Location = new System.Drawing.Point(29, 10);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(92, 18);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "Tiempo medio";
            // 
            // lblTiempoCiclo
            // 
            this.lblTiempoCiclo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoCiclo.AutoSize = false;
            this.lblTiempoCiclo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoCiclo.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblTiempoCiclo.Location = new System.Drawing.Point(5, 91);
            this.lblTiempoCiclo.Name = "lblTiempoCiclo";
            this.lblTiempoCiclo.Size = new System.Drawing.Size(141, 36);
            this.lblTiempoCiclo.TabIndex = 21;
            this.lblTiempoCiclo.Text = "00:00";
            this.lblTiempoCiclo.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTiempoTest
            // 
            this.lblTiempoTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoTest.AutoSize = false;
            this.lblTiempoTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoTest.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblTiempoTest.Location = new System.Drawing.Point(5, 151);
            this.lblTiempoTest.Name = "lblTiempoTest";
            this.lblTiempoTest.Size = new System.Drawing.Size(141, 36);
            this.lblTiempoTest.TabIndex = 20;
            this.lblTiempoTest.Text = "00:00";
            this.lblTiempoTest.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.Navy;
            this.radLabel2.Location = new System.Drawing.Point(25, 70);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(101, 18);
            this.radLabel2.TabIndex = 8;
            this.radLabel2.Text = "Tiempo de ciclo";
            // 
            // lblHoraActual
            // 
            this.lblHoraActual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHoraActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraActual.ForeColor = System.Drawing.Color.Navy;
            this.lblHoraActual.Location = new System.Drawing.Point(38, 190);
            this.lblHoraActual.Name = "lblHoraActual";
            this.lblHoraActual.Size = new System.Drawing.Size(75, 18);
            this.lblHoraActual.TabIndex = 16;
            this.lblHoraActual.Text = "Hora actual";
            // 
            // lblTiempoCicloPrevisto
            // 
            this.lblTiempoCicloPrevisto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempoCicloPrevisto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempoCicloPrevisto.ForeColor = System.Drawing.Color.Navy;
            this.lblTiempoCicloPrevisto.Location = new System.Drawing.Point(26, 130);
            this.lblTiempoCicloPrevisto.Name = "lblTiempoCicloPrevisto";
            this.lblTiempoCicloPrevisto.Size = new System.Drawing.Size(98, 18);
            this.lblTiempoCicloPrevisto.TabIndex = 14;
            this.lblTiempoCicloPrevisto.Text = "Tiempo del test";
            // 
            // lblHour
            // 
            this.lblHour.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHour.AutoSize = false;
            this.lblHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHour.ForeColor = System.Drawing.Color.Black;
            this.lblHour.Location = new System.Drawing.Point(5, 211);
            this.lblHour.Name = "lblHour";
            this.lblHour.Size = new System.Drawing.Size(141, 39);
            this.lblHour.TabIndex = 13;
            this.lblHour.Text = "00:00";
            this.lblHour.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTiempos
            // 
            this.lblTiempos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTiempos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempos.ForeColor = System.Drawing.Color.Navy;
            this.lblTiempos.Location = new System.Drawing.Point(4, 546);
            this.lblTiempos.Name = "lblTiempos";
            this.lblTiempos.Size = new System.Drawing.Size(58, 18);
            this.lblTiempos.TabIndex = 21;
            this.lblTiempos.Text = "Tiempos";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lblEmbalaje
            // 
            this.lblEmbalaje.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmbalaje.AutoSize = true;
            this.lblEmbalaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmbalaje.ForeColor = System.Drawing.Color.Navy;
            this.lblEmbalaje.Location = new System.Drawing.Point(4, 3);
            this.lblEmbalaje.Name = "lblEmbalaje";
            this.lblEmbalaje.Size = new System.Drawing.Size(66, 16);
            this.lblEmbalaje.TabIndex = 23;
            this.lblEmbalaje.Text = "Embalaje";
            // 
            // pnlEmbalaje
            // 
            this.pnlEmbalaje.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlEmbalaje.Controls.Add(this.rgEquiposCaja);
            this.pnlEmbalaje.Location = new System.Drawing.Point(3, 22);
            this.pnlEmbalaje.Name = "pnlEmbalaje";
            this.pnlEmbalaje.Size = new System.Drawing.Size(154, 143);
            this.pnlEmbalaje.TabIndex = 24;
            // 
            // rgEquiposCaja
            // 
            this.rgEquiposCaja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgEquiposCaja.BackColor = System.Drawing.Color.AliceBlue;
            this.rgEquiposCaja.CausesValidation = false;
            this.rgEquiposCaja.Controls.Add(this.radLabel3);
            this.rgEquiposCaja.Controls.Add(this.lblUnidadesCaja);
            this.rgEquiposCaja.Controls.Add(this.radLabel4);
            this.rgEquiposCaja.Controls.Add(this.lblCajaActual);
            this.rgEquiposCaja.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rgEquiposCaja.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bdBoxReport, "EquiposEntradosCajaActual", true));
            this.rgEquiposCaja.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radialGaugeArc9,
            this.radialGaugeArc10,
            this.radialGaugeSingleLabel3});
            this.rgEquiposCaja.Location = new System.Drawing.Point(2, 5);
            this.rgEquiposCaja.Name = "rgEquiposCaja";
            this.rgEquiposCaja.RangeEnd = 800D;
            this.rgEquiposCaja.Size = new System.Drawing.Size(147, 135);
            this.rgEquiposCaja.StartAngle = 180D;
            this.rgEquiposCaja.SweepAngle = 180D;
            this.rgEquiposCaja.TabIndex = 15;
            this.rgEquiposCaja.Text = "radRadialGauge1";
            this.rgEquiposCaja.Value = 0F;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.Navy;
            this.radLabel3.Location = new System.Drawing.Point(3, 76);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(98, 19);
            this.radLabel3.TabIndex = 27;
            this.radLabel3.Text = "Unidades caja:";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUnidadesCaja
            // 
            this.lblUnidadesCaja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUnidadesCaja.AutoSize = false;
            this.lblUnidadesCaja.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdBoxReport, "EquiposPorCaja", true));
            this.lblUnidadesCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidadesCaja.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblUnidadesCaja.Location = new System.Drawing.Point(94, 71);
            this.lblUnidadesCaja.Name = "lblUnidadesCaja";
            this.lblUnidadesCaja.Size = new System.Drawing.Size(51, 28);
            this.lblUnidadesCaja.TabIndex = 26;
            this.lblUnidadesCaja.Text = "1";
            this.lblUnidadesCaja.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bdBoxReport
            // 
            this.bdBoxReport.DataSource = typeof(WinTestPlayer.Model.TestModel);
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.ForeColor = System.Drawing.Color.Navy;
            this.radLabel4.Location = new System.Drawing.Point(22, 105);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(79, 19);
            this.radLabel4.TabIndex = 25;
            this.radLabel4.Text = "Caja actual:";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCajaActual
            // 
            this.lblCajaActual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCajaActual.AutoSize = false;
            this.lblCajaActual.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdBoxReport, "CajaActual", true));
            this.lblCajaActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaActual.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblCajaActual.Location = new System.Drawing.Point(95, 100);
            this.lblCajaActual.Name = "lblCajaActual";
            this.lblCajaActual.Size = new System.Drawing.Size(51, 28);
            this.lblCajaActual.TabIndex = 24;
            this.lblCajaActual.Text = "1";
            this.lblCajaActual.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radialGaugeArc9
            // 
            this.radialGaugeArc9.AngleTransform = 0F;
            this.radialGaugeArc9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(190)))), ((int)(((byte)(79)))));
            this.radialGaugeArc9.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(191)))), ((int)(((byte)(80)))));
            this.radialGaugeArc9.BindEndRange = true;
            this.radialGaugeArc9.BindEndRangeOffset = 180D;
            this.radialGaugeArc9.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc9.Name = "radialGaugeArc9";
            this.radialGaugeArc9.RangeEnd = 180D;
            this.radialGaugeArc9.Text = "º";
            this.radialGaugeArc9.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc9.Width = 40D;
            // 
            // radialGaugeArc10
            // 
            this.radialGaugeArc10.AngleTransform = 0F;
            this.radialGaugeArc10.BackColor = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc10.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.radialGaugeArc10.BackColor3 = System.Drawing.Color.AliceBlue;
            this.radialGaugeArc10.BindStartRange = true;
            this.radialGaugeArc10.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc10.Name = "radialGaugeArc10";
            this.radialGaugeArc10.RangeEnd = 800D;
            this.radialGaugeArc10.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeArc10.Width = 40D;
            // 
            // radialGaugeSingleLabel3
            // 
            this.radialGaugeSingleLabel3.BindValue = true;
            this.radialGaugeSingleLabel3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.radialGaugeSingleLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radialGaugeSingleLabel3.ForeColor = System.Drawing.Color.Black;
            this.radialGaugeSingleLabel3.LabelFontSize = 12F;
            this.radialGaugeSingleLabel3.LabelText = "Text";
            this.radialGaugeSingleLabel3.LocationPercentage = new System.Drawing.SizeF(0F, -0.1F);
            this.radialGaugeSingleLabel3.Name = "radialGaugeSingleLabel3";
            this.radialGaugeSingleLabel3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // IndicatorsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.pnlEmbalaje);
            this.Controls.Add(this.lblEmbalaje);
            this.Controls.Add(this.pnlTiempos);
            this.Controls.Add(this.lblTiempos);
            this.Controls.Add(this.pnlIndicadores);
            this.Controls.Add(this.lblIndicadores);
            this.Name = "IndicatorsView";
            this.Size = new System.Drawing.Size(160, 837);
            ((System.ComponentModel.ISupportInitialize)(this.pnlIndicadores)).EndInit();
            this.pnlIndicadores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgFPY)).EndInit();
            this.rgFPY.ResumeLayout(false);
            this.rgFPY.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgKO)).EndInit();
            this.rgKO.ResumeLayout(false);
            this.rgKO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgOK)).EndInit();
            this.rgOK.ResumeLayout(false);
            this.rgOK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTiempos)).EndInit();
            this.pnlTiempos.ResumeLayout(false);
            this.pnlTiempos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTiempoMedio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCiclo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHoraActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempoCicloPrevisto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTiempos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlEmbalaje)).EndInit();
            this.pnlEmbalaje.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgEquiposCaja)).EndInit();
            this.rgEquiposCaja.ResumeLayout(false);
            this.rgEquiposCaja.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnidadesCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdBoxReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCajaActual)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIndicadores;
        private Telerik.WinControls.UI.RadPanel pnlIndicadores;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge rgFPY;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc11;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc12;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel lblFPY;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge rgKO;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc5;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc6;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel lblKO;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge rgOK;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc1;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc2;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel lblOK;
        private Telerik.WinControls.UI.RadPanel pnlTiempos;
        private Telerik.WinControls.UI.RadLabel lblTiempoCicloPrevisto;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel lblTiempos;
        private System.Windows.Forms.Timer timer;
        private Telerik.WinControls.UI.RadLabel lblHoraActual;
        private Telerik.WinControls.UI.RadLabel lblTiempoTest;
        private Telerik.WinControls.UI.RadLabel lblTiempoCiclo;
        private System.Windows.Forms.Label lblEmbalaje;
        private Telerik.WinControls.UI.RadPanel pnlEmbalaje;
        private Telerik.WinControls.UI.Gauges.RadRadialGauge rgEquiposCaja;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel lblCajaActual;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc9;
        private Telerik.WinControls.UI.Gauges.RadialGaugeArc radialGaugeArc10;
        private Telerik.WinControls.UI.Gauges.RadialGaugeSingleLabel radialGaugeSingleLabel3;
        private Telerik.WinControls.UI.RadLabel lblHour;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel lblTotal;
        private Telerik.WinControls.UI.RadLabel lblUnidadesCaja;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.BindingSource bdBoxReport;
        private Telerik.WinControls.UI.RadLabel radLabelTiempoMedio;
        private Telerik.WinControls.UI.RadLabel radLabel6;
    }
}
