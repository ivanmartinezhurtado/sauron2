﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(RunMethodActionDescription))]
    [Editor(typeof(UIMethodEditor), typeof(UITypeEditor))]
    
    public class RunMethodAction : ActionBase
    {
        [Category("Identificacion")]
        [Description("Path con la ubicación y el nombre del assembly cargado")]
        [ReadOnly(true)]
        public string AssemblyFile { get; set; }

        [Category("Identificacion")]
        [Description("Nombre y namespace de la clase instanciada del assembly cargado")]
        [ReadOnly(true)]
        public string TypeName { get; set; }

        [Category("Identificacion")]
        [Description("Nombre del método que se va a ejecutar de la clase instanciada")]
        [ReadOnly(true)]
        public string Method { get; set; }

        [Category("Instancia")]
        [Description("Nombre de la instancia de la clase (null si usamos solo una instancia)")]
        public string InstanceContext { get; set; }

        [Category("Instancia")]
        [Description("Flag para guardar la instancia en cache y reutilizarla en diferentes hilos o tests")]
        public bool UseCache { get; set; }

        [Category("Resultados")]
        [Description("Nombre de la variable donde se guarda el valor devuelto por el método ejecutado")]
        public string VariableName { get; set; } //ToDo Miki: Cambiar a VariableOutPut

        [Category("Parametros")]
        [TypeConverter(typeof(ArgumentsListConverter))]
        [Description("Parámetros de entrada que necesita el método a ejecutar")]
        public ArgumentsList Args { get; set; }

        public RunMethodAction()
        {
            Args = new ArgumentsList();
        }

        public override void Execute()
        {
            object target = GetInstance();

            try
            {
                object result = CallUtils.InvokeMethod(target, Method, Args.Select(p => VariablesTools.ResolveValue(p)).ToArray());

                if (result == null)
                    return;

                var context = SequenceContext.Current;

                var nameVar = VariableName ?? Method;
                context.Variables.AddOrUpdate(nameVar, result);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw ex.InnerException;
                else
                    throw ex;
            }
        }

        private string FullTypeName
        {
            get
            {
                if (AssemblyFile == null)
                    return TypeName;

                return string.Format("{0}, {1}", TypeName, Path.GetFileNameWithoutExtension(AssemblyFile));
            }
        }

        private string ObjectName
        {
            get
            {
                return TypeName.Split('.').LastOrDefault();
            }
        }
        private object GetInstance()
        {
            object target = null;
            string key = string.IsNullOrEmpty(InstanceContext) ? ObjectName : string.Format("{0}_{1}", InstanceContext, ObjectName);

            var cacheSvc = SequenceContext.Current.Services.Get<ICacheService>();
            if (cacheSvc != null)
                cacheSvc.TryGet(key, out target);

            if (target == null)
                SequenceContext.Current.Variables.TryGetValue(key, out target);

            if (target == null)
            {
                Type type = GetMethodType();

                if (string.IsNullOrEmpty(InstanceContext))
                {
                    var instanceResolveService = SequenceContext.Current.Services.Get<IInstanceResolveService>();
                    if (instanceResolveService != null)
                        target = instanceResolveService.Resolve(type);
                    if (target != null)
                        return target;
                }

                target = Activator.CreateInstance(type);

                if (UseCache)
                {
                    if (cacheSvc != null)
                        cacheSvc.Add(key, target);
                }
                else
                    SequenceContext.Current.Variables.AddOrUpdate(key, target);
            }

            return target;
        }

        public Type GetMethodType()
        {
            Type type = Type.GetType(FullTypeName, false, true);
            if (type == null)
            {
                Assembly asm = null;

                if (File.Exists(AssemblyFile))
                    asm = Assembly.LoadFrom(AssemblyFile);
                else
                {
                    var assemblyResolveService = SequenceContext.Current.Services.Get<IAssemblyResolveService>();
                    asm = assemblyResolveService.Resolve(AssemblyFile);
                }

                type = asm.GetType(TypeName, true, true);
            }

            return type;
        }

        [TypeConverter(typeof(ArgumentConverter))]
        public class Argument
        {
            [Browsable(false)]
            public string Name { get; set; }

            [ReadOnly(true)]
            public string Description { get; set; }

            [ReadOnly(true)]
            public string Type { get; set; }

            [Editor(typeof(ParamArgValueEditor), typeof(UITypeEditor))]
            public string Value { get; set; }

            [Category("Parametrización")]
            [DefaultValue(null)]
            public ParamUnidad? Unidades { get; set; }

            [Category("Parametrización")]
            [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(UITypeEditor))]
            public string DefaultValue { get; set; }

            [Browsable(false)]
            public bool IsJSON { get; set; }

            public Type ResolveType()
            {
                var tokens = Type.Split('.');
                var typeName = Type;
                Type type = null;
                int i = 0;

                do
                {
                    type = System.Type.GetType(typeName);
                    if (type == null && i < tokens.Length)
                        typeName += (typeName.Contains(",") ? "." : ", ") + tokens[i++];

                } while (type == null && i < tokens.Length);

                return type;
            }

            public override string ToString()
            {
                return Value;
            }
        }

        public class ArgumentsList : List<Argument>
        {
            public override string ToString()
            {
                return this.Select(p => p.Value).Aggregate((a, b) => { return a + ", " + b; });
            }
        }
        public class ArgumentConverter : ExpandableObjectConverter
        {
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
            {
                if (destinationType == typeof(string))
                    return false;
                return base.CanConvertTo(context, destinationType);
            }

            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                if (sourceType == typeof(string))
                    return false;

                return base.CanConvertFrom(context, sourceType);
            }
        }

        public override string ToString()
        {
            return string.Format("({0}.{1})", TypeName, Method);
        }
    }

    public class RunMethodActionDescription : ActionDescription
    {
        public override string Name { get { return "RunMethodAction"; } }
        public override string Category { get { return "Actions/Developer"; } }
        public override string Description { get { return "Ejecuta un método desde un Equipo o desde los Instrumentos"; } }
        public override Image Icon { get { return Properties.Resources.Other_xml_icon; } }
    }

    public class ParamArgValueEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            object result = value;

            var arg = context.Instance as RunMethodAction.Argument;
            Type type = arg.ResolveType();

            if (type.Name != "String" &&
                (type.IsClass || type.IsInterface || (type.IsValueType && !type.IsPrimitive)))
                using (var editor = new FieldsTypeEditor(type, value != null ? value.ToString() : null))
                {
                    if (editor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        result = editor.GetAsJSON();
                        arg.IsJSON = !type.IsEnum;
                    }
                }
            else
            {
                arg.IsJSON = false;
                //using (var editor = new TextAreaEditor(value != null ? value.ToString() : null))
                using (var editor = new VariableSelectorView())
                {
                    editor.Value = value != null ? value.ToString() : null;
                    if (editor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        result = editor.Value;
                }
            }

            return result;
        }
    }

    public class ExpandableCollectionPropertyDescriptor : PropertyDescriptor
    {
        private IList _collection;

        private readonly int _index = -1;

        public ExpandableCollectionPropertyDescriptor(IList coll, int idx)
            : base(GetDisplayName(coll, idx), null)
        {
            _collection = coll;
            _index = idx;
        }

        public override bool SupportsChangeEvents
        {
            get { return true; }
        }

        private static string GetDisplayName(IList list, int index)
        {
            return ((RunMethodAction.ArgumentsList)list)[index].Name;
        }

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return _collection.GetType();
            }
        }

        public override object GetValue(object component)
        {
            return _collection[_index];
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override string Name
        {
            get { return _index.ToString(); }
        }

        public override Type PropertyType
        {
            get { return _collection[_index].GetType(); }
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }

        public override void SetValue(object component, object value)
        {
            _collection[_index] = value;
        }
    }

    internal class ArgumentsListConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            if (destType == typeof(string))
            {
                string result = string.Empty;

                var action = context.Instance as RunMethodAction;
                if (action == null || !action.Args.Any())
                    return result;

                foreach (var a in action.Args)
                    result += string.Format(",{0}={1}", a.Name, a.Value);

                return result.Substring(1);
            }

            return base.ConvertTo(context, culture, value, destType);
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            var collection = value as IList;
            var pds = new PropertyDescriptorCollection(null);

            for (int i = 0; i < collection.Count; i++)
                pds.Add(new ExpandableCollectionPropertyDescriptor(collection, i));

            return pds;
        }
    }


}
