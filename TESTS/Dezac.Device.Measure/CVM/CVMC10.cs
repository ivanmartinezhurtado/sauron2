﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.07)]
    public class CVMC10 : DeviceBase
    {
        public CVMC10()
        {
        }

        public CVMC10(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, int periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        #region Rutinas de control del equipo

        public void Reset()
        {
            LogMethod();
            Modbus.Retries = 0;
            Modbus.WithTimeOut((m) => { m.WriteSingleCoil((ushort)Registers.RESET, true); });
            Modbus.Retries = 3;
        }

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        #endregion

        #region Lectura de registros

        public AllVariables ReadAllVariables()
        {
            LogMethod();

            var config = Modbus.Read<AllVariables>();

            return config;
        }

        public PhasesVariables ReadPhaseVariables()
        {
            LogMethod();

            var config = Modbus.Read<PhasesVariables>();

            return config;
        }

        public ThreePhaseVariables ReadThreePhaseVariables()
        {
            LogMethod();

            var config = Modbus.Read<ThreePhaseVariables>();

            return config;
        }

        public ushort ReadNeutralIntensity()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)ElectricalVariablesRegisters.NEUTRO);
        }

        public TotalHarmonicDistorsion ReadTotalHarmonicDistorsion()
        {
            LogMethod();

            var config = Modbus.Read<TotalHarmonicDistorsion>((ushort)Registers.DISTORSION_HARMONICA_TOTAL);

            return config;
        }

        public Frequency ReadFrequency()
        {
            LogMethod();

            var config = new Frequency();

            config.Instantaneous = (uint)Modbus.ReadInt32((ushort)FrequencyRegisters.FRECUENCIA_INSTANTANEA) ;
            config.Maximum = (uint)Modbus.ReadInt32((ushort)FrequencyRegisters.FRECUENCIA_MAXIMA);
            config.Minimum = (uint)Modbus.ReadInt32((ushort)FrequencyRegisters.FRECUENCIA_MINIMA);

            return config;
        }

        public MeasureGains ReadMeasureGains()
        {
            LogMethod();

            MeasureGains measureGains = new MeasureGains();

            measureGains.Voltage = ReadVoltageGains();
            measureGains.Current = ReadCurrentGains();
            measureGains.Compound = ReadCompoundGains();
            measureGains.Power = ReadPowerGains();
            measureGains.neutralCurrent = ReadNeutralCurrentGain();

            return measureGains;
        }

        public TriUShort ReadVoltageGains()
        {
            LogMethod();
            return Modbus.Read<TriUShort>((ushort)GainFactorsRegisters.TODOS);
        }

        public TriUShort ReadCurrentGains()
        {
            LogMethod();
            return Modbus.Read<TriUShort>((ushort)GainFactorsRegisters.CORRIENTE);
        }

        public CompoundGains ReadCompoundGains()
        {
            LogMethod();
            return Modbus.Read<CompoundGains>((ushort)GainFactorsRegisters.TENSIONES_COMPUESTAS);
        }

        public TriUShort ReadPowerGains()
        {
            LogMethod();
            return Modbus.Read<TriUShort>((ushort)GainFactorsRegisters.KW);
        }

        public TriUShort ReadOffsetGains()
        {
            LogMethod();
            return Modbus.Read<TriUShort>((ushort)GainFactorsRegisters.DESFASE);
        }

        public ushort ReadNeutralCurrentGain()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)GainFactorsRegisters.IN);
        }

        public int ReadFare()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TARIFA);
        }

        public ushort ReadInputs()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ENTRADAS);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.VERSION_FIRMWARE, (ushort)2);
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.NUMERO_SERIE);
        }

        public string ReadSerialNumberLong()
        {
            LogMethod();
            var high = Modbus.ReadInt32((ushort)Registers.NUM_SERIE_LONG_HIGH);
            var low = Modbus.ReadInt32((ushort)Registers.NUM_SERIE_LONG_LOW);

            return string.Format("{0:0000000}{1:0000000}", high, low);
        }

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();

            var data = Modbus.ReadMultipleCoil((ushort)Registers.VECTOR_HARDWARE, 96);
            var hardwareVector = new HardwareVector(data);
            return hardwareVector;
        }

        public KeyboardKeys ReadKeys()
        {
            LogMethod();

            var keys= Modbus.ReadMultipleCoil((ushort)Registers.TECLAS,8);
            var value= (ushort)keys[0];
            return (KeyboardKeys)value;
        }

        public ushort ReadKeyboardConfiguration()
        {
            LogMethod();

            var data = Modbus.ReadMultipleCoil((ushort)Registers.CONFIG_TECLAS);
            return (ushort)data[0];
        }

        public TriInt ReadTarifa(TarifasRegisters tarifa)
        {
            LogMethod();
            return Modbus.Read<TriInt>((ushort)tarifa);
        }

        public ushort ReadSensibilityKeyboard()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.ENABLED_READ_SENSIBILITY, true);
            Thread.Sleep(50);
            return Modbus.ReadRegister((ushort)Registers.READ_SENSIBILITY);
        }

        public MemoriesMaps ReadModbusMapSelection()
        {
            LogMethod();
            return (MemoriesMaps)Modbus.ReadRegister((ushort)Registers.MEMORY_MAP_SELECTION);
        }

        public Comunications ReadComunications()
        {
            LogMethod();
            return Modbus.Read<Comunications>();
        }

        #endregion

        #region Escritura de registros

        public void WriteTransformationRatio(uint primarioTension = 1, ushort secundarioTension = 1, ushort primarioCorriente = 5,
            ushort secundarioCorriente = 5, ushort primarioCorrienteNeutro = 5, ushort secundarioCorrienteNeutro = 5)
        {
            LogMethod();

            TransformationRatioConfiguration relTransf = new TransformationRatioConfiguration
            {
                TensionPrimary = primarioTension,
                TensionSecondary = secundarioTension,
                CurrentPrimary = primarioCorriente,
                CurrentSecondary = secundarioCorriente
            };

            Modbus.Write<TransformationRatioConfiguration>(relTransf);

            NeutralTransformationRatioConfiguration relTransfNeutro = new NeutralTransformationRatioConfiguration
            {
                NeutralCurrentPrimary = primarioCorrienteNeutro,
                NeutralCurrentSecondary = secundarioCorrienteNeutro
            };

            Modbus.Write<NeutralTransformationRatioConfiguration>(relTransfNeutro);

        }

        public void WriteHardwareVector(HardwareVector hardwareVector)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.VECTOR_HARDWARE, hardwareVector.ToSingleCoil());
        }

        public void WriteComunications()
        {
            LogMethod();
            var comunications = new Comunications() { Protocolo = 0, NumeroPeriferico = 1, Velocidad = 0, Paridad = 0, BitsDatos = 0, BitsStop = 0 };
            WriteComunications(comunications);
        }

        public void WriteComunications(Comunications comunications)
        {
            LogMethod();

            int retries = Modbus.Retries;
            Modbus.Retries = 0;
            try
            {
                Modbus.Write<Comunications>((ushort)Registers.COMUNICACIONES, comunications);
            }
            catch
            {
            }
            Modbus.Retries = retries;
        }

        public void WriteInitialMessage(String mensaje)
        {
            LogMethod();
            mensaje = mensaje.PadRight(8, ' ');
            Modbus.WriteString((ushort)Registers.MENSAJE_INICIAL, mensaje.StringSwap());
        }

        public void WritePassword(ushort password)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PASSWORD, password);
        }

        public void WriteSetupLock(bool bloqueo)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.BLOQUEO_SETUP, Convert.ToUInt16(bloqueo));
        }

        public void WriteMaximumDemandConfiguration(byte minutos = 15, bool tipoVentana = false)
        {
            LogMethod();
            MaximumDemand md = new MaximumDemand();

            md.WindowPeriod = minutos;
            md.WindowType = Convert.ToUInt16(tipoVentana);

            Modbus.Write<MaximumDemand>(md);
        }

        public void WriteSerialNumber(UInt32 numeroSerie)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.NUMERO_SERIE, (int)numeroSerie);
        }

        public void WriteScreenSelection(int configuracionPantallas)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SELECCION_PANTALLA, configuracionPantallas);
        }

        public void WriteSynchronism(ushort Sincronismo)
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)GainFactorsRegisters.SINCRONISMO, (ushort)Sincronismo);
        }

        public ushort ReadSynchronism()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)GainFactorsRegisters.SINCRONISMO);
        }


        public void WriteDefaultGains(ushort voltagegain, ushort currentgain, ushort activepowergain, ushort phaseGapDeafault)
        {
            LogMethod();
            TriUShort voltageGains = new TriUShort { L1 = voltagegain, L2 = voltagegain, L3 = voltagegain };
            TriUShort currentGains = new TriUShort { L1 = currentgain, L2 = currentgain, L3 = currentgain };
            TriUShort activePowerGains = new TriUShort { L1 = activepowergain, L2 = activepowergain, L3 = activepowergain };
            CompoundGains compoundVoltageGains = new CompoundGains { L12 = voltagegain, L23 = voltagegain, L31 = voltagegain };
            var measureGains = new MeasureGains() { Voltage = voltageGains, Current = currentGains, Compound = compoundVoltageGains, Power = activePowerGains, neutralCurrent= currentgain };
            this.WriteMeasureGains(measureGains);

            TriUShort phaseGapGains = new TriUShort { L1 = phaseGapDeafault, L2 = phaseGapDeafault, L3 = phaseGapDeafault };
            this.WritePhaseGapGains(phaseGapGains);
        }

        public void WriteMeasureGains(MeasureGains measureGains)
        {
            LogMethod();

            Modbus.Write((ushort)GainFactorsRegisters.TODOS, measureGains.Voltage);
            Modbus.Write((ushort)GainFactorsRegisters.CORRIENTE, measureGains.Current);
            Modbus.Write((ushort)GainFactorsRegisters.TENSIONES_COMPUESTAS, measureGains.Compound);
            Modbus.Write((ushort)GainFactorsRegisters.KW,measureGains.Power);
            if (measureGains.neutralCurrent.HasValue)
                Modbus.Write((ushort)GainFactorsRegisters.IN, measureGains.neutralCurrent.Value);
        }

        public void WritePhaseGapGains()
        {
            LogMethod();

            ushort FactorFase = 1000;
            this.WritePhaseGapGains(new TriUShort() { L1 = FactorFase, L2 = FactorFase, L3 = FactorFase });
        }

        public void WritePhaseGapGains(TriUShort phaseGapGains)
        {
            LogMethod();
            Modbus.Write<TriUShort>((ushort)GainFactorsRegisters.DESFASE, phaseGapGains);
        }

        public void CleanRegisters(ClearRegisters borrar)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)borrar, true);
        }

        public void DisplaySwitch(DisplayRegisters display)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)display, true);
        }

        public void WriteLeds(LedRegisters led, bool encender)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)led, encender);
        }

        public void WriteOutputs(OutputRegisters salida, bool encender)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)salida, encender);
        }

        public void WriteSingleCoil(ushort register, bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil(register, state);
        }

        public void WriteKeyboardConfiguration()
        {
            LogMethod();
            int retriesModbus = Modbus.Retries;
            Modbus.Retries = 0;
            Modbus.WithTimeOut((p) => { p.WriteSingleCoil((ushort)Registers.CONFIG_TECLAS, true); });
            Modbus.Retries = retriesModbus;
        }

        public void WriteAlarm(CVMC10.AlarmRegisters alarm, CVMC10.AlarmConfiguration alarmConfiguration)
        {
            LogMethod();
            Modbus.Write<CVMC10.AlarmConfiguration>((ushort)alarm, alarmConfiguration);
        }

        public void WriteBacNetID(int bacNetID)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.BACNET_ID, bacNetID); 
        }

        public void WriteBacNetMAC(int bacNetMAC)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.BACNET_MAC, (ushort)bacNetMAC);
        }

        public void WriteAllAlarmsDefault()
        {
            LogMethod();

            CVMC10.AlarmConfiguration alarmConfig = new CVMC10.AlarmConfiguration();
            alarmConfig.maxValue = 0;
            alarmConfig.minValue = 0;
            alarmConfig.variableNumber = 0;
            alarmConfig.alarmDelay = 0;
            alarmConfig.alarmOffDelay = 0;
            alarmConfig.alarmLogic = 0;
            alarmConfig.histeresis = 0;
            alarmConfig.latching = 0;
            foreach (CVMC10.AlarmRegisters alarm in Enum.GetValues(typeof(CVMC10.AlarmRegisters)))
                WriteAlarm(alarm, alarmConfig);
        }

        public void WriteAlarmConfig(CVMC10.AlarmRegisters alarm, CVMC10.AlarmConfiguration alarmConfig)
        {
           LogMethod();
           WriteAlarm(alarm, alarmConfig);     
        }

        public void WriteMedidaConveni(ushort convenio)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONENIO_MEDIDO, convenio);
        }

        public void WriteDisabledMasterCommunications(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DISABLED_COMUNICATIONS_MASTER, state);
        }

        public void WriteAlarmMeasureVI(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.ALARM_VI, state);
        }

        public void WriteSerialNumberLong(string serialNumber)
        {
            LogMethod();

            var high = Convert.ToInt32(serialNumber.Substring(0, 7));
            var low = Convert.ToInt32(serialNumber.Substring(7, 7));

            FlagTest();
            Modbus.WriteInt32((ushort)Registers.NUM_SERIE_LONG_HIGH, high);
            Modbus.WriteInt32((ushort)Registers.NUM_SERIE_LONG_LOW, low);
        }

        public void WriteTipoInstalacion(TipoInstalacion value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TIPO_INSTALACION, (ushort)value);
        }

        public void WriteModbusMapSelection(MemoriesMaps value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MEMORY_MAP_SELECTION, (ushort)value);
        }

        #endregion

        #region Adjust methods

        public Tuple<bool, MeasureGains> CalculateAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, double voltage, TriDouble current, double transformationRelatio, Func<MeasureGains, bool> adjustValidation, Func<TriDouble, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = ReadMeasureGains();

            var result = new MeasureGains();

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;
            current.LN = current.L1;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    var allVariables = ReadAllVariables();

                    var varlist = new List<double>(){ 
                       allVariables.Phases.L1.Voltage, allVariables.Phases.L2.Voltage, allVariables.Phases.L3.Voltage,
                       allVariables.Phases.L1.Current, allVariables.Phases.L2.Current, allVariables.Phases.L3.Current,
                       allVariables.Phases.L1.ActivePower, allVariables.Phases.L2.ActivePower, allVariables.Phases.L3.ActivePower,
                       allVariables.ThreePhase.CompoundVoltageL12, allVariables.ThreePhase.CompoundVoltageL23, allVariables.ThreePhase.CompoundVoltageL31};


                    if (ReadHardwareVector().In_Medida)
                        varlist.Add(allVariables.ThreePhase.NeutralCurrent);
                    
                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    result.Voltage.L1 = Convert.ToUInt16(gains.Voltage.L1 * Math.Pow(voltage / listValues.Average(0), 2));
                    result.Voltage.L2 = Convert.ToUInt16(gains.Voltage.L2 * Math.Pow(voltage / listValues.Average(1), 2));
                    result.Voltage.L3 = Convert.ToUInt16(gains.Voltage.L3 * Math.Pow(voltage / listValues.Average(2), 2));
                    result.Current.L1 = Convert.ToUInt16(gains.Current.L1 * Math.Pow(current.L1 / listValues.Average(3), 2));
                    result.Current.L2 = Convert.ToUInt16(gains.Current.L2 * Math.Pow(current.L2 / listValues.Average(4), 2));
                    result.Current.L3 = Convert.ToUInt16(gains.Current.L3 * Math.Pow(current.L3 / listValues.Average(5), 2));
                    result.Power.L1 = Convert.ToUInt16(gains.Power.L1 * (voltage * current.L1 / listValues.Average(6)));
                    result.Power.L2 = Convert.ToUInt16(gains.Power.L2 * (voltage * current.L2 / listValues.Average(7)));
                    result.Power.L3 = Convert.ToUInt16(gains.Power.L3 * (voltage * current.L3 / listValues.Average(8)));
                    result.Compound.L12 = Convert.ToUInt16(gains.Compound.L12 * Math.Pow((voltage * Math.Sqrt(3)) / listValues.Average(9), 2));
                    result.Compound.L23 = Convert.ToUInt16(gains.Compound.L23 * Math.Pow((voltage * Math.Sqrt(3)) / listValues.Average(10), 2));
                    result.Compound.L31 = Convert.ToUInt16(gains.Compound.L31 * Math.Pow((voltage * Math.Sqrt(3)) / listValues.Average(11), 2));

                    if (ReadHardwareVector().In_Medida)
                        result.neutralCurrent = Convert.ToUInt16(gains.neutralCurrent * Math.Pow(current.LN / listValues.Average(12), 2));

                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }
        public Tuple<bool, MeasureGains> CalculateAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, double voltage, double current, double transformationRelatio, Func<MeasureGains, bool> adjustValidation, Func<TriDouble, bool> sampleValidation = null)
        {           
            FlagTest();

            var gains = ReadMeasureGains();

            var result = new MeasureGains();

            current *= transformationRelatio;


            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    var allVariables = ReadAllVariables();

                    var varlist = new List<double>()
                    {
                       allVariables.Phases.L1.Voltage, allVariables.Phases.L1.Current, allVariables.Phases.L1.ActivePower
                    };


                    if (ReadHardwareVector().In_Medida)
                        varlist.Add(allVariables.ThreePhase.NeutralCurrent);

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    result.Voltage.L1 = Convert.ToUInt16(gains.Voltage.L1 * Math.Pow(voltage / listValues.Average(0), 2));
                    result.Current.L1 = Convert.ToUInt16(gains.Current.L1 * Math.Pow(current / listValues.Average(1), 2));
                    result.Power.L1 = Convert.ToUInt16(gains.Power.L1 * (voltage * current / listValues.Average(2)));

                    return adjustValidation(result);
                });

            //gains.Voltage.L1 = result.Voltage.L1;
            //gains.Current.L1 = result.Current.L1;
            //gains.Power.L1 = result.Power.L1;
            //result = gains;

            return Tuple.Create(adjustResult.Item1, result);
        }

        public Tuple<bool, TriUShort> CalculateGapAdjustFactors(int delFirst, int initCount, int samples, int interval, TriDouble powerReference, double offsetReference, double transformationRelation, Func<TriUShort, bool> adjustValidation, Func<TriDouble, bool> sampleValidation = null)
        {
            FlagTest();

            var offsetGains = ReadOffsetGains();

            powerReference.L1 *= transformationRelation * Math.Cos(offsetReference * Math.PI / 180);
            powerReference.L2 *= transformationRelation * Math.Cos(offsetReference * Math.PI / 180);
            powerReference.L3 *= transformationRelation * Math.Cos(offsetReference * Math.PI / 180);

            double angle = 0.0D;

            var result = new TriUShort();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 15,
                () =>
                {
                    var powers = Modbus.Read<PhasesVariables>();
                    return new double[] { powers.L1.ActivePower, powers.L2.ActivePower, powers.L3.ActivePower };
                },
                (listValues) =>
                {
                    angle = (Math.Acos((1 - (listValues.Average(0) - powerReference.L1) / powerReference.L1) * 0.5) * 180 / Math.PI) - 60;
                    result.L1 = Convert.ToUInt16(1000 - ((angle * 64000) / 360));
                    angle = (Math.Acos((1 - (listValues.Average(1) - powerReference.L2) / powerReference.L2) * 0.5) * 180 / Math.PI) - 60;
                    result.L2 = Convert.ToUInt16(1000 - ((angle * 64000) / 360));
                    angle = (Math.Acos((1 - (listValues.Average(2) - powerReference.L3) / powerReference.L3) * 0.5) * 180 / Math.PI) - 60;
                    result.L3 = Convert.ToUInt16(1000 - ((angle * 64000) / 360));
                    return adjustValidation(result);
                },
                (samplesToValidate) =>
                {
                    TriDouble toValidate = new TriDouble { L1 = samplesToValidate[0], L2 = samplesToValidate[1], L3 = samplesToValidate[2] };
                    return sampleValidation(toValidate);
                });
    
            return Tuple.Create(adjustResult.Item1, result);
        }
        public Tuple<bool, TriUShort> CalculateGapAdjustFactors(int delFirst, int initCount, int samples, int interval, double powerReference, double offsetReference, double transformationRelation, Func<TriUShort, bool> adjustValidation, Func<double, bool> sampleValidation = null)
        {
            FlagTest();

            var offsetGains = ReadOffsetGains();

            powerReference *= transformationRelation * Math.Cos(offsetReference * Math.PI / 180);

            double angle = 0.0D;

            var result = new TriUShort();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 15,
                () =>
                {
                    var powers = Modbus.Read<PhasesVariables>();
                    return new double[] { powers.L1.ActivePower};
                },
                (listValues) =>
                {
                    angle = (Math.Acos((1 - (listValues.Average(0) - powerReference) / powerReference) * 0.5) * 180 / Math.PI) - 60;
                    result.L1 = Convert.ToUInt16(1000 - ((angle * 64000) / 360));                    
                    return adjustValidation(result);
                });           

            return Tuple.Create(adjustResult.Item1, result);
        }

        #endregion        

        #region Estructuras

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.L1)]
        public struct AllVariables
        {
            public PhasesVariables Phases;
            public ThreePhaseVariables ThreePhase;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.L1)]
        public struct PhasesVariables
        {
            public PhaseVariables L1;
            public PhaseVariables L2;
            public PhaseVariables L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PhaseVariables
        {
            private Int32 voltage;
            private Int32 current;
            private Int32 activePower;
            private Int32 inductiveReactivePower;
            private Int32 capacitiveReactivePower;
            private Int32 apparentPower;
            private Int32 powerFactor;
            private Int32 phiCosine;

            public double Voltage { get { return Convert.ToDouble(voltage) / 10; } }
            public double Current { get { return Convert.ToDouble(current) / 1000; } }
            public double ActivePower { get { return Convert.ToDouble(activePower); } }
            public double InductiveReactivePower { get { return Convert.ToDouble(inductiveReactivePower); } }
            public double CapacitiveReactivePower { get { return Convert.ToDouble(capacitiveReactivePower); } }
            public double ApparentPower { get { return Convert.ToDouble(apparentPower); } }
            public double PowerFactor { get { return Convert.ToDouble(powerFactor) / 100; } }
            public double PhiCosine { get { return Convert.ToDouble(phiCosine) / 100; } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.COMPUESTA)]
        public struct ThreePhaseVariables
        {
            private Int32 threePhaseActivePower;
            private Int32 threePhaseInductivePower;
            private Int32 threePhaseCapacitivePower;
            private Int32 threePhaseApparentPower;
            private Int32 threePhasePowerFactor;
            private Int32 threePhasePhiCosine;
            private Int32 frequency;
            private Int32 compoundVoltageL12;
            private Int32 compoundVoltageL23;
            private Int32 compoundVoltageL31;
            private Int32 neutralCurrent;

            public double ThreePhaseActivePower { get { return Convert.ToDouble(threePhaseActivePower); } }
            public double ThreePhaseInductivePower { get { return Convert.ToDouble(threePhaseInductivePower); } }
            public double ThreePhaseCapacitivePower { get { return Convert.ToDouble(threePhaseCapacitivePower); } }
            public double ThreePhaseApparentPower { get { return Convert.ToDouble(threePhaseApparentPower); } }
            public double ThreePhasePowerFactor { get { return Convert.ToDouble(threePhasePowerFactor); } }
            public double ThreePhasePhiCosine { get { return Convert.ToDouble(threePhasePhiCosine) / 100; } }
            public double Frequency { get { return Convert.ToDouble(frequency) / 100; } }
            public double CompoundVoltageL12 { get { return Convert.ToDouble(compoundVoltageL12) / 10; } }
            public double CompoundVoltageL23 { get { return Convert.ToDouble(compoundVoltageL23) / 10; } }
            public double CompoundVoltageL31 { get { return Convert.ToDouble(compoundVoltageL31) / 10; } }
            public double NeutralCurrent { get { return Convert.ToDouble(neutralCurrent) / 1000; } }

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.DISTORSION_HARMONICA_TOTAL)]
        public struct TotalHarmonicDistorsion
        {
            private Int32 voltageL1;
            private Int32 voltageL2;
            private Int32 voltageL3;
            private Int32 currentL1;
            private Int32 currentL2;
            private Int32 currentL3;

            public double VoltageL1 { get { return Convert.ToDouble(voltageL1) / 100; } }
            public double VoltageL2 { get { return Convert.ToDouble(voltageL2) / 100; } }
            public double VoltageL3 { get { return Convert.ToDouble(voltageL3) / 100; } }
            public double CurrentL1 { get { return Convert.ToDouble(currentL1) / 100; } }
            public double CurrentL2 { get { return Convert.ToDouble(currentL2) / 100; } }
            public double CurrentL3 { get { return Convert.ToDouble(currentL3) / 100; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.CONSUMIDA)]
        public struct ConsumedEnergyVariables
        {
            public Int32 ActiveEnergyGenerated_kWh;
            public Int32 ActiveEnergyGenerated_Wh;
            public Int32 InductiveEnergyGenerated_KvarLwh;
            public Int32 InductiveEnergyGenerated_varLh;
            public Int32 CapacitiveEnergyGenerated_KvarCwh;
            public Int32 CapacitiveEnergyGenerated_varCh;
            public Int32 ApparentEnergyGenerated_Kvah;
            public Int32 ApparentEnergyGenerated_vah;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.GENERADA)]
        public struct GeneratedEnergyVariables
        {
            public Int32 ActiveEnergyGenerated_kWh;
            public Int32 ActiveEnergyGenerated_Wh;
            public Int32 InductiveEnergyGenerated_KvarLwh;
            public Int32 InductiveEnergyGenerated_varLh;
            public Int32 CapacitiveEnergyGenerated_KvarCwh;
            public Int32 CapacitiveEnergyGenerated_varCh;
            public Int32 ApparentEnergyGenerated_Kvah;
            public Int32 ApparentEnergyGenerated_vah;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIG_RELACION_TRANSFORMACION)]
        public struct TransformationRatioConfiguration
        {
            public uint TensionPrimary;
            public ushort TensionSecondary;
            public ushort CurrentPrimary;
            public ushort CurrentSecondary;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COMUNICACIONES)]
        public struct Comunications
        {
            public ushort Protocolo;
            public ushort NumeroPeriferico;
            public ushort Velocidad;
            public ushort Paridad;
            public ushort BitsDatos;
            public ushort BitsStop;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIG_RELACION_TRANSFORMACION_NEUTRO)]
        public struct NeutralTransformationRatioConfiguration
        {
            public ushort NeutralCurrentPrimary;
            public ushort NeutralCurrentSecondary;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)GainFactorsRegisters.TODOS)]
        public struct MeasureGains
        {
            public TriUShort Voltage;
            public TriUShort Current;
            public CompoundGains Compound;
            public TriUShort Power;
            public ushort? neutralCurrent;
        }

        

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TECLAS)]
        public struct KeyboardVariables
        {
            public bool Key1;
            public bool Key2;
            public bool Key3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AlarmConfiguration
        {
            public int maxValue;
            public int minValue;
            public ushort variableNumber;
            public ushort alarmDelay;
            public ushort histeresis;
            public ushort latching;
            public ushort alarmOffDelay;
            public ushort alarmLogic;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriUShort
        {
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriInt
        {
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        public struct TriDouble
        {
            public double L1;
            public double L2;
            public double L3;
            public double LN;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CompoundGains
        {
            public ushort L12;
            public ushort L23;
            public ushort L31;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAXIMA_DEMANDA)]
        public struct MaximumDemand
        {
            public ushort WindowPeriod;
            public ushort WindowType;
        }

        public struct Frequency
        {
            public uint Instantaneous;
            public uint Maximum;
            public uint Minimum;
        }

        #endregion

        #region Registros

        public enum BaudRate
        {
            _9600 = 0,
            _19200 = 1,
            _38400 = 2,
            NULL = 999
        }

        public enum KeyboardKeys
        {
            NO_KEY = 0x0000,
            KEY_1 = 0X0001,
            KEY_2 = 0X0002,
            KEY_3 = 0X0004,
        }

        public enum Tarifa
        {
            TARIFA_1,
            TARIFA_2,
            TARIFA_3,
        }

        public enum GainFactorsRegisters
        {
            TODOS = 0x9C40,
            CORRIENTE = 0x9C43,
            TENSIONES_COMPUESTAS = 0x9C46,
            KW = 0x9C49,
            IN = 0x9C54,
            DESFASE = 0x9CA4,
            SINCRONISMO = 0x9CAE,
        }
       

        public enum TarifasRegisters
        {
            HORAS_TARIFA = 0x283C,
        }

        public enum LedRegisters
        {
            LED_CPU = 0x000A,
            LED_TECLA = 0x000B,
            LED_ALARMA = 0x000C,
        }

        public enum OutputRegisters
        {
            OUT_1 = 0x000F,
            OUT_2 = 0x0010,
            OUT_3 = 0x0011,
            OUT_4 = 0x0012,
            LECTURA_OUTS = 0x4321,
        }

        public enum ClearRegisters
        {
            BORRADO_ENERGIA = 0x0834,
            BORRADO_MAXMIN = 0x0838,
            BORRAR_CONTADORES = 0x083D,
            BORRAR_MAXIMO_MAXIMADEMANDA = 0x083F,
            BORRAR_TODO = 0x0848,
        }

        public enum DisplayRegisters
        {
            DISPLAY_1 = 0x138B,
            DISPLAY_2 = 0x138C,
            DISPLAY_3 = 0x138D,
            DISPLAY_4 = 0x138E,
            DISPLAY_ALL = 0x1389,
            //DISPLAYBACKLIGHT = 0x1389,
        }

        public enum ElectricalVariablesRegisters
        {
            L1 = 0x0000,
            L2 = 0x0010,
            L3 = 0x0020,
            NEUTRO = 0x0044,
            COMPUESTA = 0x0030,
            CONSUMIDA = 0x005E,
            GENERADA = 0x0072,
        }

        public enum KeyboardValues
        {
            TECLA_1 = 1,
            TECLA_2 = 2,
            TECLA_3 = 4,
        }

        public enum FrequencyRegisters
        {
            FRECUENCIA_INSTANTANEA = 0x003C,
            FRECUENCIA_MAXIMA = 0x0142,
            FRECUENCIA_MINIMA = 0x01A0,
        }

        public enum AlarmRegisters
        {
            ALARM_1 = 0x2AF8,
            ALARM_2 = 0x2B02,
            ALARM_3 = 0x2B0C,
            ALARM_4 = 0x2B16,
        }

        public enum TipoInstalacion
        {
            TRIFASICA = 0,
            MONOFASICA = 5,
            NULL = 100
        }

        public enum MemoriesMaps
        {
            C10,
            MINI
        }

        public enum Registers
        {
            ALARM_VI = 0X0011,
            CONFIG_RELACION_TRANSFORMACION = 0x2710,
            CONFIG_RELACION_TRANSFORMACION_NEUTRO = 0x271A,
            CONFIG_TECLAS = 0x3A9B,
            DISTORSION_HARMONICA_TOTAL = 0x0046,
            ESTADO_POTENCIA = 0x07D1,
            ENTRADAS = 0x4E20,
            FLAG_TEST = 0x2AF8,
            INIT_MAXIMA_DEMANDA = 0x0839,
            TECLAS = 0x0000,
            COMUNICACIONES=0x2742,
            MAXIMA_DEMANDA = 0x274C,
            MAXIMA_DEMANDA_TIPO_VENTANA = 0x274D,
            MENSAJE_INICIAL = 0x0514,
            NUMERO_SERIE = 0x0578,
            PASSWORD = 0x2B70,
            BLOQUEO_SETUP = 0x2B71,
            RESET = 0x07D0,
            SELECCION_PANTALLA = 0x2B7A,
            SELECCIONAR_PERFIL = 0x2B60,
            TARIFA = 0x4E20,
            VECTOR_HARDWARE = 0x2710,
            VERSION_FIRMWARE = 0x05DC,
            BACNET_ID = 0x2EE0,
            BACNET_MAC = 0x2EE2,
            CONENIO_MEDIDO = 0x2B86,
            ENABLED_READ_SENSIBILITY = 0x3A9A,
            READ_SENSIBILITY = 0x4E23,
            DISABLED_COMUNICATIONS_MASTER = 0X4A38,
            NUM_SERIE_LONG_HIGH = 13000,
            NUM_SERIE_LONG_LOW = 13002,
            TIPO_INSTALACION = 11100,
            MEMORY_MAP_SELECTION = 0x2B60
        };

        #endregion

        #region Vector de Wardware

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele4 { get; set; }
            public bool Rele3 { get; set; }
            public bool Rele2 { get; set; }
            public bool Rele1 { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Shunt { get; set; }
            public bool In_Medida { get; set; }
            public bool Flex { get; set; }

            public string Rele1String { get { return Rele1 == true ? "SI" : "NO"; } }
            public string Rele2String { get { return Rele2 == true ? "SI" : "NO"; } }
            public string Rele3String { get { return Rele3 == true ? "SI" : "NO"; } }
            public string Rele4String { get { return Rele4 == true ? "SI" : "NO"; } }
            public string Rs232String { get { return Rs232 == true ? "SI" : "NO"; } }
            public string ComunicationString { get { return Comunication == true ? "SI" : "NO"; } }
            public string ShuntString { get { return Shunt == true ? "SI" : "NO"; } }
            public string In_MedidaString { get { return In_Medida == true ? "SI" : "NO"; } }
            public string FlexString { get { return Flex == true ? "SI" : "NO"; } }

            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value;
                    if (powerSupplyString.ToUpper().Contains("AC"))
                    {
                        if (powerSupplyString.Contains("230"))
                        {
                            PowerSupply = VectorHardwareSupply._230Vac;
                            return;
                        }
                        else
                        {
                            if (powerSupplyString.Contains("400"))
                            {
                                PowerSupply = VectorHardwareSupply._400Vac;
                                return;
                            }
                            else
                            {
                                if (powerSupplyString.Contains("480"))
                                {
                                    PowerSupply = VectorHardwareSupply._480Vac;
                                    return;
                                }
                                else
                                {
                                    if (powerSupplyString.Contains("85") || powerSupplyString.Contains("265"))
                                    {
                                        PowerSupply = VectorHardwareSupply._85_265Vac;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (powerSupplyString.Contains("20") || powerSupplyString.Contains("21") || powerSupplyString.Contains("90") || powerSupplyString.Contains("120"))
                        {
                            PowerSupply = VectorHardwareSupply._20_120Vdc;
                            return;
                        }
                    }
                    throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else
                    {
                        if (inputVoltageString.Contains("110"))
                        {
                            InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                            return;
                        }
                        else
                        {
                            if (inputVoltageString.Contains("500"))
                            {
                                InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                                return;
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("5"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                        return;
                    }
                    else
                    {
                        if (inputCurrentString.Contains("2"))
                        {
                            InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                            return;
                        }
                        else
                        {
                            if (inputCurrentString.Contains("1"))
                            {
                                InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                                return;
                            }
                            else
                            {
                                if (inputCurrentString.ToUpper().Contains("ITF"))
                                {
                                    InputCurrent = VectorHardwareCurrentInputs.ITFext;
                                    return;
                                }
                            }
                        }
                    }
                    throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString.Contains("CVM-C10"))
                    {
                        Modelo = VectorHardwareModelo.CVM_C10;
                        return;
                    }
                    throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele1 = (vectorHardware[0] & 0x10) == 0x10;
                Rele2 = (vectorHardware[0] & 0x20) == 0x20;
                Rele3 = (vectorHardware[0] & 0x40) == 0x40;
                Rele4 = (vectorHardware[0] & 0x80) == 0x80;    
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                In_Medida = (vectorHardware[3] & 0x01) == 0x01;
                Flex = (vectorHardware[3] & 0x02) == 0x02;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele1;
                vector[5] = Rele2;
                vector[6] = Rele3;
                vector[7] = Rele4;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;

                vector[11] = Shunt;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[24] = In_Medida;
                vector[25] = Flex;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2,
            ITFext = 7,
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _20_120Vdc = 3,
            _85_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            CVM_C10 = 1
        }

        #endregion

        #region Lectura registros Escala2 CVMC10_ROGOWSKI

        public TriUShort ReadCurrentGainsEsc2()
        {
            LogMethod();
            return Modbus.Read<TriUShort>((ushort)GainFactorsRegistersEsc2.CORRIENTE_ROGOWSKI);
        }

        public ushort ReadNeutralCurrentGainEsc2()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)GainFactorsRegistersEsc2.IN_ROGOWSKI);
        }

        public TriUShort ReadPowerGainsEsc2()
        {
            LogMethod();
            return Modbus.Read<TriUShort>((ushort)GainFactorsRegistersEsc2.KW_ROGOWSKI);
        }

        public MeasureGainsEsc2 ReadMeasureGainsEsc2()
        {
            LogMethod();

            MeasureGainsEsc2 measureGains = new MeasureGainsEsc2();

            measureGains.Current = ReadCurrentGainsEsc2();
            measureGains.neutralCurrent = ReadNeutralCurrentGainEsc2();
            measureGains.Power = ReadPowerGainsEsc2();

            return measureGains;
        }

        #endregion

        #region Escritura registros Escala2 CVMC10_ROGOWSKI

        public void WriteMeasureGainsEscala2(MeasureGainsEsc2 measureGains)
        {
            LogMethod();

            Modbus.Write((ushort)GainFactorsRegistersEsc2.CORRIENTE_ROGOWSKI, measureGains.Current);
            Modbus.Write((ushort)GainFactorsRegistersEsc2.IN_ROGOWSKI, measureGains.neutralCurrent);
            Modbus.Write((ushort)GainFactorsRegistersEsc2.KW_ROGOWSKI, measureGains.Power);
        }

        #endregion

        #region Adjust method Escala2 CVMC10_ROGOWSKI

        public Tuple<bool, MeasureGainsEsc2> CalculateAdjustMeasureFactorsEscala2(int delFirst, int initCount, int samples, int interval, double voltage, TriDouble current, double transformationRelatio, Func<MeasureGainsEsc2, bool> adjustValidation, Func<TriDouble, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = ReadMeasureGainsEsc2();

            var result = new MeasureGainsEsc2();

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;
            current.LN = current.L1;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    var allVariables = ReadAllVariables();

                    var varlist = new List<double>(){
                       allVariables.Phases.L1.Current, allVariables.Phases.L2.Current, allVariables.Phases.L3.Current,
                       allVariables.ThreePhase.NeutralCurrent,
                       allVariables.Phases.L1.ActivePower, allVariables.Phases.L2.ActivePower, allVariables.Phases.L3.ActivePower,
                    };

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    result.Current.L1 = Convert.ToUInt16(gains.Current.L1 * Math.Pow(current.L1 / listValues.Average(0), 2));
                    result.Current.L2 = Convert.ToUInt16(gains.Current.L2 * Math.Pow(current.L2 / listValues.Average(1), 2));
                    result.Current.L3 = Convert.ToUInt16(gains.Current.L3 * Math.Pow(current.L3 / listValues.Average(2), 2));
                    result.neutralCurrent = Convert.ToUInt16(gains.neutralCurrent * Math.Pow(current.LN / listValues.Average(3), 2));
                    //result.neutralCurrent = Convert.ToUInt16(gains.neutralCurrent * Math.Pow(current.LN / listValues.Average(3), 2));

                    result.Power.L1 = Convert.ToUInt16(gains.Power.L1 * (voltage * current.L1 / listValues.Average(4)));
                    result.Power.L2 = Convert.ToUInt16(gains.Power.L2 * (voltage * current.L2 / listValues.Average(5)));
                    result.Power.L3 = Convert.ToUInt16(gains.Power.L3 * (voltage * current.L3 / listValues.Average(6)));


                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }
        #endregion


        #region Estructuras and Enums CVMC10_ROGOWSKI
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)GainFactorsRegistersEsc2.CORRIENTE_ROGOWSKI)]
        public struct MeasureGainsEsc2
        {
            public TriUShort Current;
            public ushort neutralCurrent;
            public TriUShort Power;

        }

        public enum GainFactorsRegistersEsc2
        {
            CORRIENTE_ROGOWSKI = 0x9C4C,
            IN_ROGOWSKI = 0x9C4F,
            KW_ROGOWSKI = 0x9C50,
        }
        #endregion

        public override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}
