﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Tests;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class DeviceDlmsTrifasic : DeviceDlmsBase
    {

        public DeviceDlmsTrifasic()
        {
            Fases = TypeMeterConection.enumTypeMeterConection.TRIFASICO;

            var parameters = new controlAdjust()
            {
                delFirst = 0,
                initCount = 3,
                interval = 1100,
                samples = 5,
            };

            adjustVariables = parameters;
        }

        public TriLineValue ReadPower(TypeMeterConection.frequency freq, TipoPotencia tipoPotencia)
        {
            LogMethod();

            var point = ReadPowerPoints<int>(tipoPotencia);
            var gain = ReadPowerGain<int>(freq, tipoPotencia);

            var w1 = 0D;
            if (point.L1 != 0 && gain.L1 != 0)
                w1 = (double)point.L1 / (double)gain.L1;

            var w2 = 0D;
            if (point.L2 != 0 && gain.L2 != 0)
                w2 = (double)point.L2 / (double)gain.L2;

            var w3 = 0D;
            if (point.L3 != 0 && gain.L3 != 0)
                w3 = (double)point.L3 / (double)gain.L3;

            return TriLineValue.Create(w1, w2, w3);
        }

        public Tuple<bool, TriLineGenericValue<short>> AdjustGap(TypeMeterConection.frequency freq, double MiPFref, double MiFactorDesfase, Func<TriLineGenericValue<short>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<short>();
            var i = 0;

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var activePoints = ReadPowerPoints<double>(TipoPotencia.Activa);
                    var reactivePoints = ReadPowerPoints<double>(TipoPotencia.React);
                    var points = new List<double>();
                    _logger.InfoFormat("Active Points: {0}   Sample={1} / {2}", activePoints.ToString(), i, adjustVariables.initCount);
                    _logger.InfoFormat("Reactive Points: {0}   Sample={1} / {2}", reactivePoints.ToString(), i, adjustVariables.initCount);
                    points.AddRange(activePoints.ToArray());
                    points.AddRange(reactivePoints.ToArray());
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rangActive = 0;
                    var rangReactive = 3;

                    _logger.InfoFormat("Factor Desfase: {0}", MiFactorDesfase);

                    foreach (var list in points.series)
                        _logger.InfoFormat("StadisticList Series {0} -> Data: {1}", list.Name, list);

                    resultadoAjuste.L1 = (CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));
                    _logger.InfoFormat("Gain Desfase L1: {0}", resultadoAjuste.L1);

                    resultadoAjuste.L2 = (CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));
                    _logger.InfoFormat("Gain Desfase L2: {0}", resultadoAjuste.L2);

                    resultadoAjuste.L3 = (CalculateGap(points, MiPFref, MiFactorDesfase, rangActive++, rangReactive++));
                    _logger.InfoFormat("Gain Desfase L3: {0}", resultadoAjuste.L3);

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        public Tuple<bool, TriLineGenericValue<ushort>> AdjustVoltage(double voltageReference, Func<TriLineGenericValue<ushort>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>();
            var i = 0;

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var voltagePoints = ReadVoltage<double>();
                    var points = new List<double>();
                    _logger.InfoFormat("Voltage: {0}   Sample={1} / {2}", voltagePoints.ToString(), i, adjustVariables.initCount);
                    points.AddRange(voltagePoints.ToArray());
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    _logger.InfoFormat("StadisticList Voltage: {0}", points);

                    resultadoAjuste.L1 = CalculateVoltageGain(points, voltageReference, rang++);
                    _logger.InfoFormat("Gain Voltage L1: {0}", resultadoAjuste.L1);

                    resultadoAjuste.L2 = CalculateVoltageGain(points, voltageReference, rang++);
                    _logger.InfoFormat("Gain Voltage L2: {0}", resultadoAjuste.L2);

                    resultadoAjuste.L3 = CalculateVoltageGain(points, voltageReference, rang++);
                    _logger.InfoFormat("Gain Voltage L3: {0}", resultadoAjuste.L3);

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        public Tuple<bool, TriLineGenericValue<ushort>> AdjustPowerActive(TypeMeterConection.frequency freq, double powerReference, Func<TriLineGenericValue<ushort>, bool> adjustValidation)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>();
            var i = 0;

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var activePoints = ReadPowerPoints<double>(TipoPotencia.Activa);
                    var points = new List<double>();
                    _logger.InfoFormat("Active Points: {0}   Sample={1} / {2}", activePoints.ToString(), i, adjustVariables.initCount);
                    points.AddRange(activePoints.ToArray());
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    _logger.InfoFormat("StadisticList Power Active Points: {0}", points);

                    resultadoAjuste.L1 = ((ushort)CalculatePowerActive(points, powerReference, rang++));
                    _logger.InfoFormat("Gain Power Active L1: {0}", resultadoAjuste.L1);

                    resultadoAjuste.L2 = ((ushort)CalculatePowerActive(points, powerReference, rang++));
                    _logger.InfoFormat("Gain Power Active L2: {0}", resultadoAjuste.L2);

                    resultadoAjuste.L3 = ((ushort)CalculatePowerActive(points, powerReference, rang++));
                    _logger.InfoFormat("Gain Power Active L3: {0}", resultadoAjuste.L3);

                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        //**************************************************************************

        public void WriteVoltageGain<T>(TriLineGenericValue<T> voltageGain)
        {
            LogMethod();
            WriteFasesObisData<T>(OBIS_VOLTAGE_GAIN, voltageGain);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<T> ReadVoltageGain<T>()
        {
            LogMethod();
            return ReadFasesObisData<T>(OBIS_VOLTAGE_GAIN);
        }

        public TriLineGenericValue<T> ReadPowerPoints<T>(TipoPotencia tipoPotencia)
        {
            LogMethod();
            if (tipoPotencia == TipoPotencia.Activa)
                return ReadFasesObisData<T>(OBIS_ACTIVE_POINTS);
            else
                return ReadFasesObisData<T>(OBIS_REACTIVE_POINTS);
        }

        public TriLineGenericValue<T> ReadPowerGain<T>(TypeMeterConection.frequency freq, TipoPotencia tipoPotencia)
        {
            LogMethod();

            if (freq == TypeMeterConection.frequency._50Hz)
            {
                if (tipoPotencia == TipoPotencia.Activa)
                    return ReadFasesObisData<T>(OBIS_ACTIVE_GAIN_50Hz);
                else
                    return ReadFasesObisData<T>(OBIS_REACTIVE_GAIN_50Hz);
            }
            else
            {
                if (tipoPotencia == TipoPotencia.Activa)
                    return ReadFasesObisData<T>(OBIS_ACTIVE_GAIN_60Hz);
                else
                    return ReadFasesObisData<T>(OBIS_REACTIVE_GAIN_60Hz);
            }
        }

        public void WritePowerGain<T>(TypeMeterConection.frequency freq, TipoPotencia tipoPotencia, TriLineGenericValue<T> powerPoints)
        {
            LogMethod();
            if (freq == TypeMeterConection.frequency._50Hz)
            {
                if (tipoPotencia == TipoPotencia.Activa)
                    WriteFasesObisData<T>(OBIS_ACTIVE_GAIN_50Hz, powerPoints);
                else
                    WriteFasesObisData<T>(OBIS_REACTIVE_GAIN_50Hz, powerPoints);
            }
            else
            {
                if (tipoPotencia == TipoPotencia.Activa)
                    WriteFasesObisData<T>(OBIS_ACTIVE_GAIN_60Hz, powerPoints);
                else
                    WriteFasesObisData<T>(OBIS_REACTIVE_GAIN_60Hz, powerPoints);
            }

            Thread.Sleep(3000);
        }

        public TriLineGenericValue<T> ReadGapGain<T>(TypeMeterConection.frequency freq)
        {
            LogMethod();

            if (freq == TypeMeterConection.frequency._50Hz)
                return ReadFasesObisData<T>(OBIS_PHASE_OFFSET_GAIN_50Hz);
            else
                return ReadFasesObisData<T>(OBIS_PHASE_OFFSET_GAIN_60Hz);
        }

        public void WriteGapGain<T>(TypeMeterConection.frequency freq, TriLineGenericValue<T> phaseOffset)
        {
            LogMethod();

            if (freq == TypeMeterConection.frequency._50Hz)
                WriteFasesObisData<T>(OBIS_PHASE_OFFSET_GAIN_50Hz, phaseOffset);
            else
                WriteFasesObisData<T>(OBIS_PHASE_OFFSET_GAIN_60Hz, phaseOffset);

            Thread.Sleep(3000);
        }

        public void WriteOffset<T>(TipoPotencia tipoPotencia, TriLineGenericValue<T> phaseOffset)
        {
            LogMethod();

            if (tipoPotencia == TipoPotencia.Activa)
                WriteFasesObisData<T>(OBIS_ACTIVE_OFFSET_GAIN, phaseOffset);
            else
                WriteFasesObisData<T>(OBIS_REACTIVE_OFFSET_GAIN, phaseOffset);

            Thread.Sleep(2000);
        }

        public TriLineGenericValue<T> ReadVoltage<T>()
        {
            LogMethod();
            return ReadFasesObisData<T>(OBIS_VOLTAGE_POINTS);
        }

        public TriLineGenericValue<T> ReadVoltage<T>(string[] obis)
        {
            LogMethod();
            return ReadFasesObisData<T>(obis);
        }


        public TriLineGenericValue<T> ReadCurrent<T>()
        {
            LogMethod();
            return ReadFasesObisData<T>(OBIS_CURRENT);
        }

        public void WriteCurrentGain<T>(TriLineGenericValue<T> currentGain)
        {
            LogMethod();
            WriteFasesObisData<T>(OBIS_CURRENT_GAIN, currentGain);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<T> ReadCurrentGain<T>()
        {
            LogMethod();
            return ReadFasesObisData<T>(OBIS_CURRENT_GAIN);
        }


        public TriLineGenericValue<T> ReadBetweenVoltage<T>()
        {
            LogMethod();
            return ReadFasesObisData<T>(OBIS_BETWEEN_VOLTAGE);
        }

        public void WriteBetweenVoltageGain<T>(TriLineGenericValue<T> betweenVoltageGain)
        {
            LogMethod();
            WriteFasesObisData<T>(OBIS_BETWEEN_VOLTAGE_GAIN, betweenVoltageGain);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<T> ReadBetweenVoltageGain<T>()
        {
            LogMethod();
            return ReadFasesObisData<T>(OBIS_BETWEEN_VOLTAGE_GAIN);
        }

        //************************************************************************************
        //************************************************************************************

        public TriLineGenericValue<T> ReadFasesObisData<T>(string[] obis)
        {
            LogMethod();
            return TriLineGenericValue<T>.Create(Dlms.ReadData<T>(obis[0], 2), Dlms.ReadData<T>(obis[1], 2), Dlms.ReadData<T>(obis[2], 2));
        }

        public TriLineGenericValue<T> ReadFasesObisRegister<T>(string[] obis)
        {
            LogMethod();
            return TriLineGenericValue<T>.Create(Dlms.ReadRegister<T>(obis[0], 2), Dlms.ReadRegister<T>(obis[1], 2), Dlms.ReadRegister<T>(obis[2], 2));
        }

        public void WriteFasesObisData<T>(string[] obis, TriLineGenericValue<T> value)
        {
            LogMethod();

            Dlms.WriteData<T>(obis[0], 2, value.L1);
            Dlms.WriteData<T>(obis[1], 2, value.L2);
            Dlms.WriteData<T>(obis[2], 2, value.L3);
        }

        public void WriteFasesObisRegister<T>(string[] obis, TriLineGenericValue<T> value)
        {
            LogMethod();

            Dlms.WriteRegister<T>(obis[0], 2, value.L1);
            Dlms.WriteRegister<T>(obis[1], 2, value.L2);
            Dlms.WriteRegister<T>(obis[2], 2, value.L3);
        }

        public void WriteData<T>(string obis, T value)
        {
            LogMethod();
            Dlms.WriteData<T>(obis, 2, value);
        }

        public T ReadData<T>(string obis)
        {
            LogMethod();
            return Dlms.ReadData<T>(obis, 2);
        }
    }
}
