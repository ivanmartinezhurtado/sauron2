﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace WinTestPlayer.Utils
{
    public static class Tools
    {
        public static string CrearUrlFileInstruccion(byte[] pdf)
        {
            string fileName = Path.GetTempFileName() + ".pdf";

            File.WriteAllBytes(fileName, pdf);

            return fileName;
        }

        public static void DelayCall(int msec, Action fn)
        {
            SynchronizationContext sc = SynchronizationContext.Current;

            Task.Run(() =>
            {
                Thread.Sleep(msec);
                sc.Post(_ => { fn(); }, null);
            });
        }
    }
}
