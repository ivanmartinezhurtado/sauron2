namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TIPOVALORPARAMETRO
    {
        public T_TIPOVALORPARAMETRO()
        {
            this.T_PARAMETRO = new HashSet<T_PARAMETRO>();
        }
    
        public int NUMTIPOVALOR { get; set; }
        public string TIPOVALOR { get; set; }
    
        public virtual ICollection<T_PARAMETRO> T_PARAMETRO { get; set; }
    }
}
