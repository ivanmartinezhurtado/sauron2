namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.INSTRUCCIONTECNICA")]
    public partial class INSTRUCCIONTECNICA
    {
        [Key]
        [StringLength(20)]
        public string REFINSTRUCCION { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        public byte[] DOCPDF { get; set; }

        public DateTime? FECHAPDF { get; set; }
    }
}
