﻿using Instruments.Towers;
using System;
using System.Linq;
using System.Reflection;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Services
{
    public class InstanceResolveService : IInstanceResolveService
    {
        private static InstanceResolveService shared;

        public static InstanceResolveService Default
        {
            get
            {
                if (shared == null)
                    shared = new InstanceResolveService();

                return shared;
            }
        }

        public object Resolve(Type type)
        {
            return Resolve(type, SequenceContext.Current);
        }

        public object Resolve(Type type, SequenceContext context)
        {
            if (type == null)
                return null;

            if (context == null)
                return null;

            ITower tower = null;

            var cacheSvc = context.Services.Get<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                if (type.FullName == "Instruments.Towers.Tower3")
                    tower = new Tower3();
                else if (type.FullName == "Instruments.Towers.Tower1")
                    tower = new Tower1();
                else if (type.FullName == "Instruments.Towers.TowerBoard")
                    tower = new TowerBoard();
                else
                    return null;

                cacheSvc.Add("TOWER", tower);
                return tower;
            }
            else
            {
                if ((type.FullName == "Instruments.Towers.Tower3") ||  (type.FullName == "Instruments.Towers.Tower1") || (type.FullName == "Instruments.Towers.TowerBoard"))
                    return tower;
            }

            var towerType = tower.GetType();

            var prop = towerType
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.PropertyType == type)
                .FirstOrDefault();

            if (prop == null)
                return null;

            return prop.GetValue(tower);
        }
    }
}
