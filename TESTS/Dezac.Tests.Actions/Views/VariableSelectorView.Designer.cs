﻿namespace Dezac.Tests.Actions.Views
{
    partial class VariableSelectorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.cards = new System.Windows.Forms.TabControl();
            this.Variables = new System.Windows.Forms.TabPage();
            this.bsVariables = new Telerik.WinControls.UI.RadGridView();
            this.bsList = new System.Windows.Forms.BindingSource(this.components);
            this.Parametrizacion = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnFase = new System.Windows.Forms.Button();
            this.btnProducto = new System.Windows.Forms.Button();
            this.btnFamilia = new System.Windows.Forms.Button();
            this.lblIdFase = new System.Windows.Forms.Label();
            this.lblNumProducto = new System.Windows.Forms.Label();
            this.lblFamilia = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddParam = new Telerik.WinControls.UI.RadButton();
            this.btnFiltrar = new Telerik.WinControls.UI.RadButton();
            this.ConfigurationFiles = new System.Windows.Forms.TabPage();
            this.viewConfigFiles = new Telerik.WinControls.UI.RadGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnFaseFiles = new System.Windows.Forms.Button();
            this.btnProductoFiles = new System.Windows.Forms.Button();
            this.btnFamiliaFiles = new System.Windows.Forms.Button();
            this.lblIdFaseFiles = new System.Windows.Forms.Label();
            this.lblNumProductoFiles = new System.Windows.Forms.Label();
            this.lblFamiliaFiles = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnAddFiles = new Telerik.WinControls.UI.RadButton();
            this.btnDownload = new Telerik.WinControls.UI.RadButton();
            this.Device = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Info = new System.Windows.Forms.TabPage();
            this.Context = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new Telerik.WinControls.UI.RadButton();
            this.btnOk = new Telerik.WinControls.UI.RadButton();
            this.paramsView = new Dezac.Tests.Actions.Views.ParamsView();
            this.list = new Dezac.Tests.Actions.Views.TreeTypeViewer();
            this.testInfo = new Dezac.Tests.Actions.Views.TreeTypeViewer();
            this.testContext = new Dezac.Tests.Actions.Views.TreeTypeViewer();
            this.cards.SuspendLayout();
            this.Variables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsVariables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsVariables.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).BeginInit();
            this.Parametrizacion.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddParam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFiltrar)).BeginInit();
            this.ConfigurationFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewConfigFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewConfigFiles.MasterTemplate)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).BeginInit();
            this.Device.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Info.SuspendLayout();
            this.Context.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            this.SuspendLayout();
            // 
            // cards
            // 
            this.cards.Controls.Add(this.Variables);
            this.cards.Controls.Add(this.Parametrizacion);
            this.cards.Controls.Add(this.ConfigurationFiles);
            this.cards.Controls.Add(this.Device);
            this.cards.Controls.Add(this.Info);
            this.cards.Controls.Add(this.Context);
            this.cards.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cards.Location = new System.Drawing.Point(0, 0);
            this.cards.Name = "cards";
            this.cards.SelectedIndex = 0;
            this.cards.Size = new System.Drawing.Size(880, 738);
            this.cards.TabIndex = 5;
            // 
            // Variables
            // 
            this.Variables.Controls.Add(this.bsVariables);
            this.Variables.Location = new System.Drawing.Point(4, 22);
            this.Variables.Name = "Variables";
            this.Variables.Size = new System.Drawing.Size(872, 712);
            this.Variables.TabIndex = 4;
            this.Variables.Text = "Variables";
            this.Variables.UseVisualStyleBackColor = true;
            // 
            // bsVariables
            // 
            this.bsVariables.BackColor = System.Drawing.SystemColors.Control;
            this.bsVariables.Cursor = System.Windows.Forms.Cursors.Default;
            this.bsVariables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bsVariables.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.bsVariables.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bsVariables.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bsVariables.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.bsVariables.MasterTemplate.AllowAddNewRow = false;
            this.bsVariables.MasterTemplate.AllowDeleteRow = false;
            this.bsVariables.MasterTemplate.AllowEditRow = false;
            this.bsVariables.MasterTemplate.AutoGenerateColumns = false;
            this.bsVariables.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "Key";
            gridViewTextBoxColumn1.HeaderText = "Nombre";
            gridViewTextBoxColumn1.IsAutoGenerated = true;
            gridViewTextBoxColumn1.Name = "Key";
            gridViewTextBoxColumn1.Width = 525;
            gridViewTextBoxColumn2.AllowFiltering = false;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "Value";
            gridViewTextBoxColumn2.HeaderText = "Valor";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.Name = "Value";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.Width = 345;
            this.bsVariables.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.bsVariables.MasterTemplate.DataSource = this.bsList;
            this.bsVariables.MasterTemplate.EnableFiltering = true;
            this.bsVariables.MasterTemplate.ShowRowHeaderColumn = false;
            this.bsVariables.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.bsVariables.Name = "bsVariables";
            this.bsVariables.ReadOnly = true;
            this.bsVariables.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bsVariables.Size = new System.Drawing.Size(872, 712);
            this.bsVariables.TabIndex = 1;
            this.bsVariables.Text = "radGridView1";
            // 
            // Parametrizacion
            // 
            this.Parametrizacion.Controls.Add(this.paramsView);
            this.Parametrizacion.Controls.Add(this.panel3);
            this.Parametrizacion.Location = new System.Drawing.Point(4, 22);
            this.Parametrizacion.Name = "Parametrizacion";
            this.Parametrizacion.Padding = new System.Windows.Forms.Padding(3);
            this.Parametrizacion.Size = new System.Drawing.Size(872, 712);
            this.Parametrizacion.TabIndex = 3;
            this.Parametrizacion.Text = "Parametrización";
            this.Parametrizacion.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnFase);
            this.panel3.Controls.Add(this.btnProducto);
            this.panel3.Controls.Add(this.btnFamilia);
            this.panel3.Controls.Add(this.lblIdFase);
            this.panel3.Controls.Add(this.lblNumProducto);
            this.panel3.Controls.Add(this.lblFamilia);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.btnAddParam);
            this.panel3.Controls.Add(this.btnFiltrar);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(866, 100);
            this.panel3.TabIndex = 0;
            // 
            // btnFase
            // 
            this.btnFase.Location = new System.Drawing.Point(585, 65);
            this.btnFase.Name = "btnFase";
            this.btnFase.Size = new System.Drawing.Size(75, 23);
            this.btnFase.TabIndex = 18;
            this.btnFase.Text = "...";
            this.btnFase.UseVisualStyleBackColor = true;
            this.btnFase.Click += new System.EventHandler(this.btnFase_Click);
            // 
            // btnProducto
            // 
            this.btnProducto.Location = new System.Drawing.Point(586, 37);
            this.btnProducto.Name = "btnProducto";
            this.btnProducto.Size = new System.Drawing.Size(75, 23);
            this.btnProducto.TabIndex = 15;
            this.btnProducto.Text = "...";
            this.btnProducto.UseVisualStyleBackColor = true;
            this.btnProducto.Click += new System.EventHandler(this.btnProducto_Click);
            // 
            // btnFamilia
            // 
            this.btnFamilia.Location = new System.Drawing.Point(586, 9);
            this.btnFamilia.Name = "btnFamilia";
            this.btnFamilia.Size = new System.Drawing.Size(75, 23);
            this.btnFamilia.TabIndex = 12;
            this.btnFamilia.Text = "...";
            this.btnFamilia.UseVisualStyleBackColor = true;
            this.btnFamilia.Click += new System.EventHandler(this.btnFamilia_Click);
            // 
            // lblIdFase
            // 
            this.lblIdFase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdFase.Location = new System.Drawing.Point(124, 65);
            this.lblIdFase.Name = "lblIdFase";
            this.lblIdFase.Size = new System.Drawing.Size(441, 23);
            this.lblIdFase.TabIndex = 17;
            this.lblIdFase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumProducto
            // 
            this.lblNumProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumProducto.Location = new System.Drawing.Point(124, 37);
            this.lblNumProducto.Name = "lblNumProducto";
            this.lblNumProducto.Size = new System.Drawing.Size(441, 23);
            this.lblNumProducto.TabIndex = 14;
            this.lblNumProducto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFamilia
            // 
            this.lblFamilia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFamilia.Location = new System.Drawing.Point(124, 9);
            this.lblFamilia.Name = "lblFamilia";
            this.lblFamilia.Size = new System.Drawing.Size(441, 23);
            this.lblFamilia.TabIndex = 11;
            this.lblFamilia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(61, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "FASE:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(44, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "FAMILIA:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "PRODUCTO:";
            // 
            // btnAddParam
            // 
            this.btnAddParam.Location = new System.Drawing.Point(699, 9);
            this.btnAddParam.Name = "btnAddParam";
            this.btnAddParam.Size = new System.Drawing.Size(127, 35);
            this.btnAddParam.TabIndex = 9;
            this.btnAddParam.Text = "Añadir";
            this.btnAddParam.Click += new System.EventHandler(this.btnAddParam_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Location = new System.Drawing.Point(699, 53);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(127, 35);
            this.btnFiltrar.TabIndex = 8;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.Click += new System.EventHandler(this.btnParams_Click);
            // 
            // ConfigurationFiles
            // 
            this.ConfigurationFiles.Controls.Add(this.viewConfigFiles);
            this.ConfigurationFiles.Controls.Add(this.panel4);
            this.ConfigurationFiles.Location = new System.Drawing.Point(4, 22);
            this.ConfigurationFiles.Name = "ConfigurationFiles";
            this.ConfigurationFiles.Padding = new System.Windows.Forms.Padding(3);
            this.ConfigurationFiles.Size = new System.Drawing.Size(872, 712);
            this.ConfigurationFiles.TabIndex = 5;
            this.ConfigurationFiles.Text = "ConfigurationFiles";
            this.ConfigurationFiles.UseVisualStyleBackColor = true;
            // 
            // viewConfigFiles
            // 
            this.viewConfigFiles.BackColor = System.Drawing.Color.Transparent;
            this.viewConfigFiles.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewConfigFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewConfigFiles.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.viewConfigFiles.ForeColor = System.Drawing.SystemColors.ControlText;
            this.viewConfigFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.viewConfigFiles.Location = new System.Drawing.Point(3, 103);
            // 
            // 
            // 
            this.viewConfigFiles.MasterTemplate.AllowColumnReorder = false;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Description";
            gridViewTextBoxColumn3.HeaderText = "Description";
            gridViewTextBoxColumn3.Name = "Description";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 386;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "FileName";
            gridViewTextBoxColumn4.HeaderText = "FileName";
            gridViewTextBoxColumn4.Name = "FileName";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 385;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "IdTipoDoc";
            gridViewTextBoxColumn5.HeaderText = "IdTipoDoc";
            gridViewTextBoxColumn5.Name = "IdTipoDoc";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 78;
            this.viewConfigFiles.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.viewConfigFiles.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.viewConfigFiles.Name = "viewConfigFiles";
            this.viewConfigFiles.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.viewConfigFiles.Size = new System.Drawing.Size(866, 606);
            this.viewConfigFiles.TabIndex = 2;
            this.viewConfigFiles.Text = "radGridView1";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnFaseFiles);
            this.panel4.Controls.Add(this.btnProductoFiles);
            this.panel4.Controls.Add(this.btnFamiliaFiles);
            this.panel4.Controls.Add(this.lblIdFaseFiles);
            this.panel4.Controls.Add(this.lblNumProductoFiles);
            this.panel4.Controls.Add(this.lblFamiliaFiles);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.btnAddFiles);
            this.panel4.Controls.Add(this.btnDownload);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(866, 100);
            this.panel4.TabIndex = 1;
            // 
            // btnFaseFiles
            // 
            this.btnFaseFiles.Location = new System.Drawing.Point(585, 65);
            this.btnFaseFiles.Name = "btnFaseFiles";
            this.btnFaseFiles.Size = new System.Drawing.Size(75, 23);
            this.btnFaseFiles.TabIndex = 18;
            this.btnFaseFiles.Text = "...";
            this.btnFaseFiles.UseVisualStyleBackColor = true;
            this.btnFaseFiles.Click += new System.EventHandler(this.btnFaseFiles_Click);
            // 
            // btnProductoFiles
            // 
            this.btnProductoFiles.Location = new System.Drawing.Point(586, 37);
            this.btnProductoFiles.Name = "btnProductoFiles";
            this.btnProductoFiles.Size = new System.Drawing.Size(75, 23);
            this.btnProductoFiles.TabIndex = 15;
            this.btnProductoFiles.Text = "...";
            this.btnProductoFiles.UseVisualStyleBackColor = true;
            this.btnProductoFiles.Click += new System.EventHandler(this.btnProducto_Click);
            // 
            // btnFamiliaFiles
            // 
            this.btnFamiliaFiles.Location = new System.Drawing.Point(586, 9);
            this.btnFamiliaFiles.Name = "btnFamiliaFiles";
            this.btnFamiliaFiles.Size = new System.Drawing.Size(75, 23);
            this.btnFamiliaFiles.TabIndex = 12;
            this.btnFamiliaFiles.Text = "...";
            this.btnFamiliaFiles.UseVisualStyleBackColor = true;
            this.btnFamiliaFiles.Click += new System.EventHandler(this.btnFamilia_Click);
            // 
            // lblIdFaseFiles
            // 
            this.lblIdFaseFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdFaseFiles.Location = new System.Drawing.Point(124, 65);
            this.lblIdFaseFiles.Name = "lblIdFaseFiles";
            this.lblIdFaseFiles.Size = new System.Drawing.Size(441, 23);
            this.lblIdFaseFiles.TabIndex = 17;
            this.lblIdFaseFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumProductoFiles
            // 
            this.lblNumProductoFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumProductoFiles.Location = new System.Drawing.Point(124, 37);
            this.lblNumProductoFiles.Name = "lblNumProductoFiles";
            this.lblNumProductoFiles.Size = new System.Drawing.Size(441, 23);
            this.lblNumProductoFiles.TabIndex = 14;
            this.lblNumProductoFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFamiliaFiles
            // 
            this.lblFamiliaFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFamiliaFiles.Location = new System.Drawing.Point(124, 9);
            this.lblFamiliaFiles.Name = "lblFamiliaFiles";
            this.lblFamiliaFiles.Size = new System.Drawing.Size(441, 23);
            this.lblFamiliaFiles.TabIndex = 11;
            this.lblFamiliaFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(61, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "FASE:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(44, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "FAMILIA:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "PRODUCTO:";
            // 
            // btnAddFiles
            // 
            this.btnAddFiles.Location = new System.Drawing.Point(699, 9);
            this.btnAddFiles.Name = "btnAddFiles";
            this.btnAddFiles.Size = new System.Drawing.Size(127, 35);
            this.btnAddFiles.TabIndex = 9;
            this.btnAddFiles.Text = "Añadir";
            this.btnAddFiles.Visible = false;
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(699, 53);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(127, 35);
            this.btnDownload.TabIndex = 8;
            this.btnDownload.Text = "Descargar";
            this.btnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // Device
            // 
            this.Device.Controls.Add(this.list);
            this.Device.Controls.Add(this.panel2);
            this.Device.Location = new System.Drawing.Point(4, 22);
            this.Device.Name = "Device";
            this.Device.Padding = new System.Windows.Forms.Padding(3);
            this.Device.Size = new System.Drawing.Size(872, 712);
            this.Device.TabIndex = 0;
            this.Device.Text = "Device";
            this.Device.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtPrefix);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(866, 38);
            this.panel2.TabIndex = 0;
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(86, 10);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(169, 20);
            this.txtPrefix.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prefix (Step):";
            // 
            // Info
            // 
            this.Info.Controls.Add(this.testInfo);
            this.Info.Location = new System.Drawing.Point(4, 22);
            this.Info.Name = "Info";
            this.Info.Padding = new System.Windows.Forms.Padding(3);
            this.Info.Size = new System.Drawing.Size(872, 712);
            this.Info.TabIndex = 1;
            this.Info.Text = "Test Info";
            this.Info.UseVisualStyleBackColor = true;
            // 
            // Context
            // 
            this.Context.Controls.Add(this.testContext);
            this.Context.Location = new System.Drawing.Point(4, 22);
            this.Context.Name = "Context";
            this.Context.Padding = new System.Windows.Forms.Padding(3);
            this.Context.Size = new System.Drawing.Size(872, 712);
            this.Context.TabIndex = 2;
            this.Context.Text = "Test Context";
            this.Context.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 738);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(880, 56);
            this.panel1.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(754, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 31);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "CANCEL";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(619, 13);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(110, 31);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // paramsView
            // 
            this.paramsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paramsView.Location = new System.Drawing.Point(3, 103);
            this.paramsView.Name = "paramsView";
            this.paramsView.Size = new System.Drawing.Size(866, 606);
            this.paramsView.TabIndex = 1;
            // 
            // list
            // 
            this.list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list.Location = new System.Drawing.Point(3, 41);
            this.list.Name = "list";
            this.list.Size = new System.Drawing.Size(866, 668);
            this.list.TabIndex = 0;
            // 
            // testInfo
            // 
            this.testInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testInfo.Location = new System.Drawing.Point(3, 3);
            this.testInfo.Name = "testInfo";
            this.testInfo.Size = new System.Drawing.Size(866, 706);
            this.testInfo.TabIndex = 1;
            // 
            // testContext
            // 
            this.testContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testContext.Location = new System.Drawing.Point(3, 3);
            this.testContext.Name = "testContext";
            this.testContext.Size = new System.Drawing.Size(866, 706);
            this.testContext.TabIndex = 1;
            // 
            // VariableSelectorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 794);
            this.Controls.Add(this.cards);
            this.Controls.Add(this.panel1);
            this.Name = "VariableSelectorView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Variable Selector";
            this.cards.ResumeLayout(false);
            this.Variables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bsVariables.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsVariables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).EndInit();
            this.Parametrizacion.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddParam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFiltrar)).EndInit();
            this.ConfigurationFiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.viewConfigFiles.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewConfigFiles)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).EndInit();
            this.Device.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Info.ResumeLayout(false);
            this.Context.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl cards;
        private System.Windows.Forms.TabPage Device;
        private System.Windows.Forms.TabPage Info;
        private System.Windows.Forms.TabPage Context;
        private System.Windows.Forms.TabPage Parametrizacion;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton btnCancel;
        private Telerik.WinControls.UI.RadButton btnOk;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadButton btnFiltrar;
        private Dezac.Tests.Actions.Views.TreeTypeViewer list;
        private TreeTypeViewer testInfo;
        private TreeTypeViewer testContext;
        private ParamsView paramsView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton btnAddParam;
        private System.Windows.Forms.Button btnFase;
        private System.Windows.Forms.Button btnProducto;
        private System.Windows.Forms.Button btnFamilia;
        private System.Windows.Forms.Label lblIdFase;
        private System.Windows.Forms.Label lblNumProducto;
        private System.Windows.Forms.Label lblFamilia;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage Variables;
        private Telerik.WinControls.UI.RadGridView bsVariables;
        private System.Windows.Forms.BindingSource bsList;
        private System.Windows.Forms.TabPage ConfigurationFiles;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnFaseFiles;
        private System.Windows.Forms.Button btnProductoFiles;
        private System.Windows.Forms.Button btnFamiliaFiles;
        private System.Windows.Forms.Label lblIdFaseFiles;
        private System.Windows.Forms.Label lblNumProductoFiles;
        private System.Windows.Forms.Label lblFamiliaFiles;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadButton btnAddFiles;
        private Telerik.WinControls.UI.RadButton btnDownload;
        private Telerik.WinControls.UI.RadGridView viewConfigFiles;
    }
}