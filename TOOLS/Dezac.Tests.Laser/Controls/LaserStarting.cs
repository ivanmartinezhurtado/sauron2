﻿using Dezac.Data.ViewModels;
using Instruments.IO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.Laser.Controls
{
    public partial class LaserStarting : Form
    {
        private int steps = 5;

        public byte InputAlarm { get; set; }

        public byte InputReady { get; set; }

        public byte OutputStart { get; set; }

        public TestBase TestBase { get; set; }

        public resultLaser HasAlarm { get; private set; }

        public string ErrorMessage { get; private set; }

        private bool checkAlarm = false;

        public enum resultLaser
        {
            Alarma,
            Canceled,
            OK
        }

        public List<LaserItemViewModel> Lineas { get; set; }

        private SynchronizationContext sc;
        private CancellationTokenSource cts;
        private Task task;

        public USB4761 IO { get; set; }
    
        public LaserStarting()
        {
            InitializeComponent();
            lblStatuas.Text = "Espere, por favor ...";
            progress.Value = 0;
            progress.Maximum = steps;
            HasAlarm = resultLaser.OK;
            ErrorMessage = string.Empty;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;
            cts = new CancellationTokenSource();

            LaserItemViewModel item = Lineas.First();

            lblItem.Text = item.CodigoBcn;
            lblDescripcion.Text = item.DescripcionArticulo;
            lblStatuas.Text = string.Empty;

            timer.Enabled = true;
            timer.Start();

            task = Task.Factory.StartNew(() => ProcessItem(item), cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public void ProcessItem(LaserItemViewModel item)
        {
            checkAlarm = false;
            ShowProgress(string.Format("Girando puerta ...", PathIntercambio), true);
            
            EsperarGiroLaser(EstadoLaser.PARADO);

            this.TestBase.Logger.DebugFormat("Giro de la puerta OK");

            CreateFileLaser(item);

            var token = cts.Token;           

            if (token.IsCancellationRequested && HasAlarm == resultLaser.Alarma)
                return;

            if (token.IsCancellationRequested && HasAlarm == resultLaser.Alarma)
                return;          

            EsperarLaser(EstadoLaser.PARADO);

            ShowProgress("Cargando fichero " + item.CodigoBcn + ".SVG ... ", true);           

            EsperarLaser(EstadoLaser.MARCANDO);
            this.TestBase.SetVariable("LASER_PRINTING", true);

            //checkAlarm = true;
            ShowProgress("... Laser marcando ...", true);
            this.TestBase.Logger.DebugFormat("Laser Marcando");

            EsperarLaser(EstadoLaser.PARADO);
            this.TestBase.Logger.DebugFormat("Marcado Laser Finalizado");
            checkAlarm = false;

            if (token.IsCancellationRequested && HasAlarm == resultLaser.Alarma)
                return;

            if (token.IsCancellationRequested && HasAlarm == resultLaser.Alarma)
                return;

            EsperarLaser(EstadoLaser.PARADO);

            if (token.IsCancellationRequested && HasAlarm == resultLaser.Alarma)
                return;

            ShowProgress("Laser finalizado correctamente", true);

        }

        private void ShowProgress(string msg, bool step)
        {
            sc.Post(_ => {
                if (step)
                    progress.Value++;
                lblStatuas.Text = msg;

            }, null);
        }

        private string PathFilesLaser
        {
            get { return ConfigurationManager.AppSettings["PathFilesLaser"]; }
        }

        private string PathIntercambio
        {
            get { return ConfigurationManager.AppSettings["PathIntercambio"]; }
        }

        private void CreateFileLaser(LaserItemViewModel item)
        {
            string name = string.Format("{0}", item.CodigoBcn.ToUpper().Replace(".XML", string.Empty));

            string fileName = Path.Combine(PathFilesLaser, name + ".svg");

            if (!File.Exists(fileName))
                throw new Exception(string.Format("Archivo {0} inexistente!", fileName));

            if (!Directory.Exists(PathIntercambio))
                Directory.CreateDirectory(PathIntercambio);

            string targetFileName = Path.Combine(PathFilesLaser, "dati.txt");

            //string content = string.Format("{0};", name);

            string content;

            using (var laserSvc = new LaserService())
            {
                content = laserSvc.BuildLaserFile(name);
            }

            this.TestBase.Logger.InfoFormat(string.Format("Contenido fichero dati.txt :\n {0}", content));
            File.WriteAllText(targetFileName, content);

            if (!File.Exists(targetFileName))
                throw new Exception("No se ha podido generar el archivo dati.txt");
            
            this.TestBase.SamplerWithCancel((s) =>
            {
                if(s % 10 == 0)
                    File.Move(targetFileName, Path.Combine(PathIntercambio, "dati.txt"));

                if (File.Exists(Path.Combine(PathIntercambio, "dati.txt")))
                    return true;
                return false;
            }, "No se ha podido copiar el fichero dati.txt", 50, 100, 0, false, true);
            this.TestBase.Logger.DebugFormat(string.Format("Fichero dati.txt copiado en {0} --> Archivo SVG : {1}", PathIntercambio, name ));
        }    

        public enum EstadoLaser
        {
            MARCANDO = 0,
            PARADO = 1,
            GIRANDO = 0,
        }

        public void IniciarSeñalExternaLaser()
        {
            IO.DO.PulseOn(200, OutputStart);
        }

        public void EsperarLaser(EstadoLaser Estado)
        {
            do
            {
                Thread.Sleep(500);
            } while (IO.DI[InputReady] != Convert.ToBoolean(Estado) && !cts.IsCancellationRequested);
        }

        public void EsperarGiroLaser(EstadoLaser Estado)
        {
            do
            {
                Thread.Sleep(500);
            } while (IO.DI[InputAlarm] != Convert.ToBoolean(Estado) && !cts.IsCancellationRequested);

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            CheckAlarm();
            ReadLastLineLog();
            CheckFinish();
        }

        private void ReadLastLineLog()
        {            
            File.Copy(@"\\PC-14244\Sisma\SVG2RTC\Parametri\log.txt", @"\\PC-14244\Sisma\SVG2RTC\Parametri\logSAURON.txt", true);                
            
            var lastLine = File.ReadAllLines(@"\\PC-14244\Sisma\SVG2RTC\Parametri\logSAURON.txt").LastOrDefault();

            //this.TestBase.Logger.InfoFormat("LOG SISMA --> {0}", lastLine);

            if (lastLine.Contains("Error") || lastLine.Contains("non trovato nel filesystem"))
            {
                sc.Post(_ => { splitter.Panel1.BackColor = Color.Red; }, null);
                ErrorMessage = lastLine;
                cts.Cancel();
            }
        }

        private void CheckAlarm()
        {
            if (!IO.DI[InputAlarm] && checkAlarm)
            {
                HasAlarm = resultLaser.Alarma;
                sc.Post(_ => { splitter.Panel1.BackColor = Color.Red; }, null);
                cts.Cancel();
            }
        }

        private void CheckFinish()
        {
            if (HasAlarm != resultLaser.OK || (task != null && task.IsCompleted))
            {
                timer.Stop();
                timer.Enabled = false;
                this.Close();
            }
        }

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = false;
            splitter.Panel1.BackColor = Color.Red;
            lblStatuas.Text = "Laser cancelado por el operario";
            HasAlarm = resultLaser.Canceled;
        }

    }
}
