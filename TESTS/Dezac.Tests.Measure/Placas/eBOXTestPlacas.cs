﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.00)]
    public class eBOXTestPlacas : TestBase
    {
        private TowerBoard tower;
        private eBOX ebox;

        public void TestInitialization(int portCVM = 0)
        {
            var sec = Identificacion.SEQUENCE_FILE;

            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            logger.InfoFormat("Create new Instance eBOX class with PortCom {0}", portCVM);

            ebox = AddInstanceVar(new eBOX(portCVM, Comunicaciones.Periferico), "UUT");
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", ebox.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", ebox.GetType().Name), ParamUnidad.SinUnidad);

            tower = GetVariable<TowerBoard>("TOWER");

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

        }

        public void TestBatteryConsumption()
        {
            var dialogResult = Shell.MsgBox("La lectura del multímetro es inferior a 1uA?", "Test de consumo de pila", MessageBoxButtons.YesNo);

            Assert.AreEqual(ConstantsParameters.TestPoints.BATERIA, dialogResult.ToString(), DialogResult.Yes.ToString(), Error().UUT.CONSUMO.MARGENES_SUPERIORES("Error de batería, el consumo es mayor del esperado"), ParamUnidad.SinUnidad);

        }

        public void TestComunications()
        {
            SamplerWithCancel((p) => { ebox.FlagTest(); return true; }, "Error Version de Firmware incorrecta", 20, 1000);

            var version = ebox.ReadSoftwareVersion().CToNetString();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error del equipo, version de software incorrecta"), ParamUnidad.SinUnidad);

            var model = ebox.ReadModel();
            Assert.AreEqual(ConstantsParameters.Identification.MODELO, model.ToString(), Identificacion.MODELO, Error().UUT.CONFIGURACION.NO_COINCIDE("Error modelo incorrecto"), ParamUnidad.SinUnidad);

            ebox.WriteBastidor((int)TestInfo.NumBastidor);
        }

        public void TestInputOutputs()
        {
            tower.IO.DO.OnWait(1000, 19);

            eBOX.ResulTestInOut resultIO = eBOX.ResulTestInOut.RUNNING;

            ebox.WriteTestInOutStart();

            SamplerWithCancel((p) =>
            {
                resultIO = ebox.ReadTestInOut();
                return resultIO != eBOX.ResulTestInOut.RUNNING;

            }, "");

            tower.IO.DO.OffWait(1000, 19);

            Assert.IsTrue(resultIO == eBOX.ResulTestInOut.OK, Error().UUT.CONFIGURACION.NO_CORRESPONDE_CON_ENVIO(string.Format("Error en el test de entradas y salidas por resultado {0}", resultIO.ToString())));
        }

        public void TestLeds()
        {
            ebox.WriteLeds(eBOX.Leds.ALL_OFF);

            var cameraResult = Shell.ShowDialog("Interaccion del usuario", () =>
            {
                var image = new ImageView("Prueba de leds", string.Format("{0}\\{1}\\{2}.png", PathCameraImages.TrimEnd('\\'), ebox.GetType().Name, eBOX.Leds.ALL_OFF.ToString()));
                return image;
            },
                MessageBoxButtons.YesNo, "Comprobar que todos los leds estan apagados como en la imagen patrón", 2000);

            Assert.AreEqual(cameraResult, DialogResult.Yes, Error().UUT.CONSUMO.LEDS("Error. hay leds encendidos cuando deberían estar apagados"));

            List<eBOX.Leds> leds = new List<eBOX.Leds>();

            leds.Add(eBOX.Leds.KEYB_VOLT_CURR_BAT_SD_RED);
            leds.Add(eBOX.Leds.REC_IO_BAT_SD_GREEN);
            leds.Add(eBOX.Leds.WIFI_3G_USB_BAT_SD_BLUE);

            foreach (eBOX.Leds led in leds)
            {
                ebox.WriteLeds(led);

                cameraResult = Shell.ShowDialog("Interaccion del usuario", () =>
                {
                    var image = new ImageView("Prueba de leds", string.Format("{0}\\{1}\\{2}.png", PathCameraImages.TrimEnd('\\'), ebox.GetType().Name, led.ToString()));
                    return image;
                },
                    MessageBoxButtons.YesNo, "Comprobar que se encienden los leds destacados en la imagen patrón", 2000);

                Assert.AreEqual(cameraResult, DialogResult.Yes, Error().UUT.CONSUMO.LEDS(string.Format("Error. mal funcionamiento del LED {0} incorrecto", led.ToString())));

                Resultado.Set(led.ToString().Replace(".png", " "), "OK", ParamUnidad.SinUnidad);
            }

            ebox.WriteLeds(eBOX.Leds.ALL_OFF);
        }

        public void TestTeclat()
        {
            //var resultTeclat = eBOX.KeyboardKeys.RUNNING;

            //SamplerWithCancel((p) =>
            //{
            //    resultTeclat = ebox.ReadTestTeclat();
            //    return resultTeclat != eBOX.KeyboardKeys.RUNNING;

            //}, "");

            //Assert.AreEqual("ESTADO_TECLADO", resultTeclat.ToString(), eBOX.KeyboardKeys.OK.ToString(), string.Format("Error en el test de teclado, el equipo notifica del siguiente o siguientes fallos {0}", resultTeclat.ToString()), ParamUnidad.SinUnidad);

        }

        public void TestDisplay()
        {
            //ebox.WriteTestDisplay();

            //var cameraResult = Shell.ShowDialog("Interaccion del usuario", () =>
            //{
            //    var image = new ImageView("Prueba de display", string.Format("{0}\\{1}\\{2}.png", PathCameraImages.TrimEnd('\\'), ebox.GetType().Name, "DISPLAY"));

            //    return image;
            //},
            //    MessageBoxButtons.YesNo, "Comprobar que el display se muestra como en la imagen patrón", 2000);

            //Assert.AreEqual(cameraResult, DialogResult.Yes, "Error. mal funcionamiento del display incorrecto");
        }

        public void TestADC()
        {
            eBOX.ResulTest result = eBOX.ResulTest.RUNNING;

            SamplerWithCancel((p) =>
            {
                result = ebox.ReadTestADC();
                return result != eBOX.ResulTest.RUNNING;

            }, "");

            Assert.IsTrue(result == eBOX.ResulTest.OK, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error en el test ACDC por resultado {0}", result.ToString())));

            SamplerWithCancel((p) =>
            {
                result = ebox.ReadTestADC1();
                return result != eBOX.ResulTest.RUNNING;

            }, "");

            Assert.IsTrue(result == eBOX.ResulTest.OK, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error en el test ACDC1 por resultado {0}", result.ToString())));
        }

        public void TestEmbeddedInitialization()
        {
            ebox.FlagTest();

            SamplerWithCancel((p) =>
            {
                return ebox.ReadEmbeddedStatus() != 0;
            }, "Error, el embedded ha tardado mas tiempo del esperado en arrancar", 65, 3000, 0);

            var imxStatus = ebox.ReadIMXStatus();

            Assert.AreEqual("CODIGO_ERROR_IMX", imxStatus.ToString(), eBOX.IMXErrorCode.NO_ERROR.ToString(), Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error, el registro IMX notifica del siguiente o siguientes problemas {0}", imxStatus)), ParamUnidad.SinUnidad);
        }

        public void TestUART()
        {
            //eBOX.ResulTest result = eBOX.ResulTest.RUNNING;

            //SamplerWithCancel((p) =>
            //{
            //    result = ebox.ReadTestUART1();
            //    return result != eBOX.ResulTest.RUNNING;

            //}, "");

            //Assert.IsTrue(result == eBOX.ResulTest.OK, string.Format("Error en el test UART1(DMA) por resultado {0}", result.ToString()));

            //SamplerWithCancel((p) =>
            //{
            //    result = ebox.ReadTestUART2();
            //    return result != eBOX.ResulTest.RUNNING;

            //}, "");

            //Assert.IsTrue(result == eBOX.ResulTest.OK, string.Format("Error en el test UART2(Modbus) por resultado {0}", result.ToString()));
        }

        public void TestSD()
        {
            eBOX.ResulTest result = eBOX.ResulTest.RUNNING;

            SamplerWithCancel((p) =>
            {
                result = ebox.ReadDetectionSD1();
                return result != eBOX.ResulTest.RUNNING;

            }, "");

            Assert.IsTrue(result == eBOX.ResulTest.OK, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error en la detección SD1 por resultado {0}", result.ToString())));

            SamplerWithCancel((p) =>
            {
                result = ebox.ReadDetectionSD2();
                return result != eBOX.ResulTest.RUNNING;

            }, "");

            Assert.IsTrue(result == eBOX.ResulTest.OK, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error en la detección SD2 por resultado {0}", result.ToString())));
        }

        public void TestNandFlash()
        {
            //eBOX.ResulTest result = eBOX.ResulTest.RUNNING;

            //SamplerWithCancel((p) =>
            //{
            //    result = ebox.ReadTestNandFlash();
            //    return result != eBOX.ResulTest.RUNNING;

            //}, "");

            //Assert.IsTrue(result == eBOX.ResulTest.OK, string.Format("Error en el test NandFlash por resultado {0}", result.ToString()));

            eBOX.NandFlashSizeEnum resultSize = eBOX.NandFlashSizeEnum.RUNNING;

            SamplerWithCancel((p) =>
            {
                resultSize = ebox.ReadNandFlashSize();
                return resultSize != eBOX.NandFlashSizeEnum.RUNNING;

            }, "");

            Assert.IsTrue(resultSize == eBOX.NandFlashSizeEnum.SIZE_256MB, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error en el tamaño de la NandFlash por resultado {0}", resultSize.ToString())));
        }

        public void Test3G()
        {
            eBOX.ResulTest result = eBOX.ResulTest.RUNNING;

            SamplerWithCancel((p) =>
            {
                result = ebox.ReadTest3G();
                return result != eBOX.ResulTest.RUNNING;

            }, "");

            Assert.IsTrue(result == eBOX.ResulTest.OK, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO(string.Format("Error en el test 3G con SIM y Setup por resultado {0}", result.ToString())));
        }

        public void TestWifi()
        {
            eBOX.ResulTest result = eBOX.ResulTest.RUNNING;

            SamplerWithCancel((p) =>
            {
                result = ebox.ReadTestWifi();
                return result != eBOX.ResulTest.RUNNING;

            }, "");

            Assert.IsTrue(result == eBOX.ResulTest.OK, Error().SOFTWARE.ERROR_TRANSFERENCIA.WIFI(string.Format("Error en el test Wifi con Setup por resultado {0}", result.ToString())));
        }

        private static readonly Dictionary<eBOX.Registersforceps, InputMuxEnum> forceps = new Dictionary<eBOX.Registersforceps, InputMuxEnum> 
        { 
             { eBOX.Registersforceps.VOLT_H, InputMuxEnum.IN9 },  // OUT 28 + CHROMA
            { eBOX.Registersforceps.VOLT_N, InputMuxEnum.IN13 },    // OUT 28
            //{ eBOX.Registersforceps.CLAMP_F1, InputMuxEnum.IN12_V3 }, // OUT 28 + CHROMA
            { eBOX.Registersforceps.CLAMP_F2, InputMuxEnum.IN13 }, // OUT 28 + CHROMA 
            { eBOX.Registersforceps.CLAMP_F3, InputMuxEnum.IN9 }, // OUT 28 + CHROMA + OUT 15
            //{ eBOX.Registersforceps.CLAMP_N, InputMuxEnum.IN10_AuxMedida9 },  // OUT 28 + CHROMA + OUT 15
            //{ eBOX.Registersforceps.CLAMP_K, InputMuxEnum.IN11_AuxMedida10 },  // OUT 28 + CHROMA + OUT 15
        };

        public enum SignalsForceps
        {
            VOLT_H = 9,
            VOLT_N = 13,
            CLAMP_F1 = 12,
            CLAMP_F2 = 13,
            CLAMP_F3 = 9,
            CLAMP_N = 10,
            CLAMP_K = 11,
        }

        public void TestControlSignalsForceps(int DeleteFirst, int NumberSamples, int WaitTimeReadMeasure)
        {
            tower.IO.DO.On(18);

            foreach (KeyValuePair<eBOX.Registersforceps, InputMuxEnum> forcep in forceps)
            {
                switch (forcep.Key)
                {
                    case eBOX.Registersforceps.VOLT_N:
                        tower.Chroma.ApplyOff();
                        tower.IO.DO.Off(15);
                        break;
                    case eBOX.Registersforceps.CLAMP_N:
                    case eBOX.Registersforceps.CLAMP_K:
                    case eBOX.Registersforceps.CLAMP_F3:
                        tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50); //?? cuanto hay que consignar
                        tower.IO.DO.On(15);
                        break;
                    default:
                        tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50); //?? cuanto hay que consignar
                        tower.IO.DO.Off(15);
                        break;
                }

                var adjustValueTestPoint = new AdjustValueDef(Params.V.Other(forcep.Key.ToString()).EnVacio.Name, 0, Params.V.Other(forcep.Key.ToString()).EnVacio.Min(), Params.V.Other(forcep.Key.ToString()).EnVacio.Max(), 0, 0, ParamUnidad.V);

                ebox.WriteControlSignalForceps(forcep.Key, 0);

                TestMeasureBase(adjustValueTestPoint, (step) =>
                {
                    return tower.MeasureMultimeter(forcep.Value, MagnitudsMultimeter.VoltDC, WaitTimeReadMeasure); ;

                }, DeleteFirst, NumberSamples, WaitTimeReadMeasure);

                adjustValueTestPoint = new AdjustValueDef(Params.V.Other(forcep.Key.ToString()).ConCarga.Name, 0, Params.V.Other(forcep.Key.ToString()).ConCarga.Min(), Params.V.Other(forcep.Key.ToString()).ConCarga.Max(), 0, 0, ParamUnidad.V);

                ebox.WriteControlSignalForceps(forcep.Key, 1);

                TestMeasureBase(adjustValueTestPoint, (step) =>
                {
                    return tower.MeasureMultimeter(forcep.Value, MagnitudsMultimeter.VoltDC, WaitTimeReadMeasure); ;

                }, DeleteFirst, NumberSamples, WaitTimeReadMeasure);

            }

            tower.Chroma.ApplyOff();

            tower.IO.DO.Off(18);
        }

        public void TestRTC()
        {
            var date = ebox.ReadDatetime();

            Delay(4000, "Wait to test the clock");

            DateTime fecha = DateTime.UtcNow;

            int timeDiff = (fecha - date).Seconds;

            Assert.AreGreater(timeDiff, 2, Error().UUT.CONFIGURACION.RELOJ("Error. El equipo no ha incrementado el reloj"));

            ebox.WriteTestRTC();

            tower.HP53131A
            .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_50)
            .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.LOW, 2)
            .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 9);

            Assert.IsValidMaxMin(Params.FREQ.Null.RTC.Name, tower.MeasureWithFrecuency(InputMuxEnum.IN5, 10000), ParamUnidad.Hz, Params.FREQ.Null.RTC.Min(), Params.FREQ.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("RTC")); 

            double Temperatura = ebox.ReadTemperature();

            double CalcFactor = 32768 * (0.034 * Math.Pow(Temperatura - 25, 2));
            double FactorPPMcalc = (32768 + 0.9) - (CalcFactor / 1000000);

            Assert.IsValidMaxMin(Params.FREQ.Null.RTC.calc.Name, tower.MeasureWithFrecuency(InputMuxEnum.IN1, 10000), ParamUnidad.Hz, Params.FREQ.Null.RTC.calc.Min(), Params.FREQ.Null.RTC.calc.Max(), Error().UUT.MEDIDA.MARGENES("RTC Calculada"));

        }

        public void Customization()
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(4000, "Encendemos el equipo");

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(9, 1);

            SamplerWithCancel((p) => { ebox.FlagTest(); return true; }, "Error de comunicaciones al volver a enceder el equipo", 20, 1000);

            var numbastidor = ebox.ReadBastidor();

            Assert.AreEqual(numbastidor.ToString(), TestInfo.NumBastidor.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, valor de numero de bastidor leido diferente al grabado"));

            ebox.WriteReleDefault();
        }

        public void TestFinish()
        {
            if (ebox != null)
                ebox.Dispose();

            if (tower == null)
            {
                tower = GetVariable<TowerBoard>("TOWER");
                if (tower == null)
                    return;
            }

            tower.ShutdownSources();
            tower.IO.DO.Off(19);
            tower.IO.DO.Off(18);

            tower.Dispose();
        }
    }
}
