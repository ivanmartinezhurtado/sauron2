﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Visio.DTO
{
    public class HalconCallResult
    {
        public static HalconCallResult OK = new HalconCallResult { Success = true };
        public bool Success { get; set; }
        public Exception Exception { get; set; }
        public string ErrorMessage { get; set; }
        public Dictionary<string, string> TupleResults { get; set; }
        public Dictionary<string, List<string>> VectorResults { get; set; }
        public Dictionary<string, byte[]> IconicResults { get; set; }
        public HalconCallResult()
        {
            TupleResults = new Dictionary<string, string>();
            IconicResults = new Dictionary<string, byte[]>();
            VectorResults = new Dictionary<string, List<string>>();
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
