﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskRunner;
using TaskRunner.Model;
using WinTaskRunner.Utils;

namespace WinTaskRunner.Services
{
    public class SequenceService
    {
        public List<SequenceDocument> Sequences { get; private set;}
        
        //public event void Play();

        public SequenceDocument Current { get; set; }

        public SequenceService()
        {
            Sequences = new List<SequenceDocument>();
        }

        public void Remove(SequenceDocument model)
        {
            if (model != null)
                Sequences.Remove(model);
        }

        public SequenceDocument AddNew()
        {
            var doc = new SequenceDocument();
            Sequences.Add(doc);

            if (Current == null)
                Current = doc;

            return doc;
        }
    }
}
