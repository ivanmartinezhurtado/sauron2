﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(InitHexSerialPortDescription))]
    public class InitHexSerialPort : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Puerto en el que se quiere trabajar")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Port { get; set; }

        [Category("3. Configuration")]
        [Description("Velocidad de transmsion del puerto")]
        [DefaultValue(BaudRateEnum.Bps9600)]
        public BaudRateEnum Bps { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_HEX")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de espera a recibir la trama amtes de dar error")]
        [DefaultValue(1000)]
        public int TimeOut { get; set; }

        [Category("3. Configuration")]
        [Description("Último carácter de la linea")]
        public EndLine EndLineCaracter { get; set; }

        [Category("3. Configuration")]
        [Description("DTR habilitado")]
        [DefaultValue(false)]
        public bool DTREnable { get; set; }

        [Category("3. Configuration")]
        [Description("RTS habilitado")]
        [DefaultValue(false)]
        public bool RTSEnable { get; set; }

        [Category("3. Configuration")]
        [Description("Forma de colaboración de Hardware")]
        [DefaultValue(Handshake.None)]
        public Handshake Handshake { get; set; }

        public enum EndLine
        {
            CRLF,
            CR,
            LF,
            PuntoComa,
            Null,
        }

        public override void Execute()
        {
            var portDevice = Convert.ToByte(GetVariable<string>(Port, Port).Replace("COM", ""));

            var sp = new SerialPort("COM" + portDevice, (int)Bps, Parity.None, 8, StopBits.One);
            sp.ReadTimeout = TimeOut;
            sp.WriteTimeout = TimeOut;
            sp.DtrEnable = DTREnable;
            sp.RtsEnable = RTSEnable;
            sp.Handshake = Handshake;
            if (EndLineCaracter != EndLine.Null)
                sp.NewLine = GetStringEndLine(EndLineCaracter);

            var protocol = new ASCIIProtocol
            {
                CaracterFinRx = GetStringEndLine(EndLineCaracter),
                CaracterFinTx = GetStringEndLine(EndLineCaracter)
            };
            var transport = new MessageTransport(protocol, new SerialPortAdapter(sp), Logger);

            SetVariable(DeviceName ?? ("COM" + portDevice), transport);
        }

        private string GetStringEndLine(EndLine endLine)
        {
            switch (endLine)
            {
                case EndLine.CR:
                    return "\r";
                case EndLine.LF:
                    return "\n";
                case EndLine.CRLF:
                    return "\r\n";
                case EndLine.PuntoComa:
                    return ";";
                case EndLine.Null:
                    return "";
                default:
                    return "\r\n";
            }
         }
    }


    public class InitHexSerialPortDescription : ActionDescription
    {
        public override string Name { get { return "InitHEXSerialPort"; } }
        public override string Description { get { return "Crea una instancia de un puerto de comunicación Serie"; } }
        public override string Category { get { return "Tools/Comunications/HEX"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
