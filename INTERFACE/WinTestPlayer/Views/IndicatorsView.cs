﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinTestPlayer.Services;
using WinTestPlayer.Model;
using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.UserControls;
using TaskRunner.Enumerators;

namespace WinTestPlayer.Views
{
    public partial class IndicatorsView : UserControl
    {
        protected AppService app;
        protected ShellService shell;
        protected TestModel model;

        private DateTime? testStart;
        private double? tiempoPrevisto;

        private int numOK;
        private int numNG;

        public IndicatorsView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            shell = DI.Resolve<ShellService>();
            app = DI.Resolve<AppService>();
            //app.ModelChanged += (s, ee) => { UpdateUI(); };
            app.Runner.AllTestStarting += (s, ee) => { testStart = DateTime.Now; UpdateTimes(); };
            app.Runner.AllTestEnded += (s, ee) => { UpdateUI(); };

            model = app.Model;

            UpdateUI();

            if (model.ModoPlayer == ModoPlayer.Normal || model.ModoPlayer == ModoPlayer.TIC)  //app.Model.Offline)
            {
                rgOK.DoubleClick += new System.EventHandler(rgOK_DoubleClick);
                rgKO.DoubleClick += new System.EventHandler(rgKO_DoubleClick);
            }
            else
            {
                lblEmbalaje.Visible = false;
                pnlEmbalaje.Visible = false;
                rgFPY.Visible = false;
                lblIndicadores.Top = lblEmbalaje.Top;
                pnlIndicadores.Top = pnlEmbalaje.Top;
                pnlIndicadores.Height -= rgFPY.Height - 40;
                lblTiempos.Top = pnlIndicadores.Bottom + 10;
                pnlTiempos.Top = lblTiempos.Top + 20;
                radLabel6.Visible = false;
                radLabelTiempoMedio.Visible = false;

                app.Runner.TestEnd += (s, ee) =>
                {
                    if (ee.ExecutionResult == TestExecutionResult.Completed)
                        numOK++;
                    else if (ee.ExecutionResult == TestExecutionResult.Faulted)
                        numNG++;
                };
            }

            timer.Enabled = true;

            bdBoxReport.DataSource = app.Model;
        }

        private async void UpdateUI()
        {
            if (model.ModoPlayer == ModoPlayer.Normal || model.ModoPlayer == ModoPlayer.TIC)
            {
                double total = Convert.ToDouble(model.Orden != null ? model.Orden.CANTIDAD : 1);
                lblTotal.Text = total.ToString();

                rgOK.RangeEnd = total;
                radialGaugeArc1.RangeEnd = total;
                radialGaugeArc2.RangeEnd = total;

                rgKO.RangeEnd = total;
                radialGaugeArc5.RangeEnd = total;
                radialGaugeArc6.RangeEnd = total;

                rgFPY.RangeEnd = 1;
                radialGaugeArc11.RangeEnd = 1;
                radialGaugeArc12.RangeEnd = 1;
                lblFPY.LabelFormat = "##,#%";

                rgEquiposCaja.RangeEnd = model.EquiposPorCaja;
                radialGaugeArc10.RangeEnd = model.EquiposPorCaja;
                radialGaugeArc9.RangeEnd = model.EquiposPorCaja;

                if (model.Orden != null)
                {
                    TestIndicators indicators = await GetTestIndicatorsAsync(model.Orden.NUMORDEN, model.IdFase);
                    rgOK.Value = Convert.ToSingle(indicators.NumOK);
                    rgKO.Value = Convert.ToSingle(indicators.NumError);
                    rgFPY.Value = Convert.ToSingle(indicators.NumFPY / 100);
                    var time = new TimeSpan(0, 0, (int)indicators.NumTiempoMedio);
                    tiempoPrevisto = time.TotalSeconds;
                    radLabelTiempoMedio.Text = time.ToString(@"mm\:ss"); //string.Format("{0}:{1}:{2}", time.Hours, time.Minutes, time.Seconds);
                }
            }
            else
            {
                rgOK.Value = numOK;
                rgKO.Value = numNG;
                rgFPY.Value = 0;
            }
        }

        private Task<TestIndicators> GetTestIndicatorsAsync(int orden, string idFase)
        {
            return Task.Run<TestIndicators>(() =>
            {
                using (IDezacService svc = DI.Resolve<IDezacService>())
                {
                    return svc.GetTestIndicators(orden, idFase);
                }
            });
        }

        private void UpdateTimes()
        {
            DateTime now = DateTime.Now;

            lblHour.Text = now.ToString("HH:mm:ss");

            if (testStart.HasValue)
            {
                if (app.IsRunning)
                    lblTiempoTest.Text = (now - testStart.Value).ToString(@"mm\:ss");

                TimeSpan span = (now - testStart.Value);

                lblTiempoCiclo.Text = span.ToString(@"mm\:ss");
                lblTiempoTest.ForeColor = tiempoPrevisto.HasValue && span.TotalSeconds > tiempoPrevisto.Value ? Color.Red : Color.Black;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            UpdateTimes();
        }

        private void rgOK_DoubleClick(object sender, EventArgs e)
        {
            if (app.Model.NumOrden <= 0 || app.IsRunning)
                return;

            List<DeviceDataView> list = null;

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                list = svc.GetAllDeviceTestOK(app.Model.NumOrden, app.Model.IdFase);
            }

            var table = new TableGridViewConfig { Data = list, Fields = "FECHAMAT;NUMMATRICULA;NROSERIE;NUMCAJA", GroupBy = "NUMCAJA" };

            shell.ShowTableDialog("TESTS OK", table);
        }

        private void rgKO_DoubleClick(object sender, EventArgs e)
        {
            if (app.Model.NumOrden <= 0 || app.IsRunning)
                return;

            List<TestOrdenProductoResult> list = null;

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                list = svc.GetTestResults(app.Model.NumOrden, app.Model.IdFase, false);
            }

            shell.ShowTableDialog("TESTS NO GOOD", new TableGridViewConfig { Data = list, Fields = "Fecha;IdBastidor;PuntoError;MsgError,NumCaja" });
        }

    }
}
