﻿using Dezac.Core.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using TaskRunner.Tools;

namespace Dezac.Tests.Actions.Views
{
    public partial class FieldsTypeEditor : Form
    {
        public Type type;
        public List<DataBindNameValuePair> Items { get; internal set; }

        public bool IsPrimitiveArray { get; set; }

        public FieldsTypeEditor()
        {
            InitializeComponent();
            
            this.Icon = Application.OpenForms[0].Icon;
        }

        public FieldsTypeEditor(Type type, string json)
            : this()
        {
            this.type = type;
            if (type == null)
                return;

            this.Text = string.Format("Editar {0}", type.Name);

            if (type.IsEnum)
            {
                var names = Enum.GetNames(type);
                var values = Enum.GetValues(type);

                Items = new List<DataBindNameValuePair>();

                foreach(var v in values)
                    Items.Add(new DataBindNameValuePair { Name = names[Items.Count], Value = v.ToString() });

                grid.ReadOnly = true;
            }
            else if (type.IsArray && type.GetElementType().IsPrimitive)
            {
                IsPrimitiveArray = true;
                this.grid.Columns[0].IsVisible = false;
                this.grid.Columns[2].IsVisible = false;

                Items = new List<DataBindNameValuePair>();

                grid.AllowAddNewRow = true;

                if (!string.IsNullOrEmpty(json))
                {
                    var list = JsonConvert.DeserializeObject<List<object>>(json);
                    for(int i = 0; i < list.Count; i++)
                        Items.Add(new DataBindNameValuePair { Name = i.ToString(), Value = string.Format("{0}", list[i]) });
                }
            }
            else
            {
                Items = JsonHelper.DeserializeObjectAndFlattenList(Activator.CreateInstance(type))
                    .Select(p => new DataBindNameValuePair { Name = p.Name, Value = p.Value != null ? p.Value.ToString() : null })
                    .ToList();

                if (!string.IsNullOrEmpty(json))
                    JsonConvert.DeserializeObject<List<DataBindNameValuePair>>(json)
                        .ForEach(p =>
                        {
                            var item = Items.Find(pc => pc.Name == p.Name);
                            if (item != null)
                                item.Value = p.Value;
                        });
            }

            grid.DataSource = Items;
        }

        public string GetAsJSON()
        {
            if (grid.ReadOnly)
            {
                if (grid.SelectedRows.Count != 1)
                    return null;

                return grid.SelectedRows[0].Cells[1].Value.ToString();
            }

            if (IsPrimitiveArray)
            {
                var array = Items.Select(p => Convert.ChangeType(p.Value, type.GetElementType())).ToArray();
                return JsonConvert.SerializeObject(array);
            }

            return JsonConvert.SerializeObject(Items);
        }
  
        private void MasterTemplate_CommandCellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            var value = e.Row.Cells[1].Value;

            var pair = e.Row.DataBoundItem as DataBindNameValuePair;
            var property = type.GetProperty(pair?.Name);
            var propertyType = property?.PropertyType;
            if ((propertyType != null) && propertyType.IsEnum)
                using (var editor = new FieldsTypeEditor(propertyType, value != null ? value.ToString() : null))
                {
                    if (editor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        e.Row.Cells[1].Value = Enum.Parse(propertyType, editor.GetAsJSON());
                }
            else
                using (var editor = new VariableSelectorView())
                {
                    if (value != null)
                        editor.Value = value.ToString();

                    if (editor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        e.Row.Cells[1].Value = editor.Value;
                }
        }
    }

    public class DataBindNameValuePair : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [JsonIgnore]
        public string value;

        public string Name { get; set; }
        public string Value
        {
            get
            {
                return value;
            }
            set
            {
                if (value != Value)
                {
                    this.value = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Value"));
                }
            }
        }
    }
}
