﻿using Dezac.Core.Utility;
using Instruments.Router;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.Ethernet
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(GetHostAndRouterIpDescription))]
    public class GetHostAndRouterIp : ActionBase
    {
        public override void Execute()
        {
            var ip = Router.Default().RouterIP;

            if (SequenceContext.Current.Variables.ContainsKey("RouterIP"))
                SequenceContext.Current.Variables.AddOrUpdate("RouterIP", ip);
            else
                SequenceContext.Current.Variables.AddOrUpdate("RouterIP", ip);

            SequenceContext.Current.ResultList.Add("RouterIP" + ip);

            var HostIp = NetUtils.GetMyIP();
            if (SequenceContext.Current.Variables.ContainsKey("HostIP"))
                SequenceContext.Current.Variables.AddOrUpdate("HostIP", HostIp);
            else
                SequenceContext.Current.Variables.AddOrUpdate("HostIP", HostIp);

            SequenceContext.Current.ResultList.Add("HostIP" + HostIp);
        }
    }

    public class GetHostAndRouterIpDescription : ActionDescription
    {
        public override string Name { get { return "GetHostAndRouterIP"; } }
        public override string Category { get { return "Tools/Ethernet/Tools"; } }
        public override string Description { get { return "Action que sirve para coger la IP del Host y la IP del Router"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
