﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Visio.DTO
{
    public struct ImageStruct
    {
        public Bitmap Image;
        public byte[] ImageError;
        public string ImageName;

        public ImageStruct(Bitmap image, string imageName)
        {
            Image = image;
            ImageName = imageName;
            ImageError = null;
        }
    }
}
