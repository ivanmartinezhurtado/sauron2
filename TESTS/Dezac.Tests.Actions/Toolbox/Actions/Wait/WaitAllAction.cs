﻿using System;
using System.Drawing;
using System.Threading;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WaitAllActionDescription))]
    public class WaitAllAction : ActionBase
    {
        private static readonly object _syncObject = new Object();

        public override void Execute()
        {
            string key = "_signal_waitallaction_" + SequenceContext.Current.Step.Name;

            int runningInstances = 0;
            int numSignals = 0;

            lock (_syncObject)
            {
                SetSharedVariable(key, GetSharedVariable<int>(key, 0) + 1);
            }

            do
            {
                if (numSignals < runningInstances)
                    Thread.Sleep(200);

                runningInstances = SequenceContext.Current.RunningInstances;
                numSignals = GetSharedVariable<int>(key, 0);

            } while (numSignals < runningInstances);
        }
    }

    public class WaitAllActionDescription : ActionDescription
    {
        public override string Name { get { return "WaitAll"; }}
        public override string Category { get { return "Actions/Wait"; } }
        public override string Description { get { return "Este action sirve por si se está haciendo test en paralelos, en un punto, estos vuelvan a trabajar desde el mismo, esperando así para empezar a la vez"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
    }
}
