﻿using Dezac.Device;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WriteCirbusDescription))]
    public class WriteCirbus : ActionBase
    {
        [Description("Trama Cirbus a mandar")]
        [Category("3. Configuration")]
        public string Message { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("CirbusDevice")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "CirbusDevice";
            var uut = GetVariable(name) as CirbusDevice;

            uut.Write(Message);

            Context.ResultList.Add(string.Format("{0} - TX:{1} ", name, Message));
        }
    }

    public class WriteCirbusDescription : ActionDescription
    {
        public override string Name { get { return "WriteCirbus"; } }
        public override string Description { get { return "Escribir por protocolo Cirbus"; } }
        public override string Category { get { return "Tools/Comunications/Cirbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
    }
}
