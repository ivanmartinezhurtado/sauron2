﻿using System.ComponentModel;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class EKOR_DIDO : EKOR_SAFE
    {
        public EKOR_DIDO()
        {
        }

        public EKOR_DIDO(int port)
        {
            SetPort(port);
        }

        #region Enumerados

        public enum enumInputsDIDO
        {
            [Description("Entrada digital 1")]
            IN1 = 0,
            [Description("Entrada digital 2")]
            IN2 = 1,
            [Description("Entrada digital 3")]
            IN3 = 2,
            [Description("Entrada digital 4")]
            IN4 = 3,
            [Description("Entrada digital 5")]
            IN5 = 4,
            [Description("Entrada digital 6")]
            IN6 = 5,
            [Description("Entrada digital 7")]
            IN7 = 6,
            [Description("Entrada digital 8")]
            IN8 = 7,
            [Description("Entrada digital 9")]
            IN9 = 8,
            [Description("Entrada digital 10")]
            IN10 = 9,
        }

        public enum enumLEDSDIDO
        {
            [Description("LED COM")]
            COM = 1,
            [Description("LED IN1")]
            IN1 = 2,
            [Description("LED IN2")]
            IN2 = 3,
            [Description("LED IN3")]
            IN3 = 4,
            [Description("LED IN4")]
            IN4 = 5,
            [Description("LED IN5")]
            IN5 = 6,
            [Description("LED IN6")]
            IN6 = 7,
            [Description("LED IN7")]
            IN7 = 8,
            [Description("LED IN8")]
            IN8 = 9,
            [Description("LED IN9")]
            IN9 = 10,
            [Description("LED IN10")]
            IN10 = 11,
            [Description("LED OUT1")]
            OUT1 = 12,
            [Description("LED OUT2")]
            OUT2 = 13,
            [Description("LED OUT3")]
            OUT3 = 14,
            [Description("LED OUT4")]
            OUT4 = 15,
            [Description("LED ALARM1")]
            ALARM1 = 16,
            [Description("LED ALARM2")]
            ALARM2 = 17,
            [Description("LED ALARM3")]
            ALARM3 = 18,
            [Description("LED ALARM4")]
            ALARM4 = 19
        }

        #endregion

    }
}
