namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTSEQUENCEFILEPRODUCTO")]
    public partial class T_TESTSEQUENCEFILEPRODUCTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTSFP { get; set; }

        public int NUMTSF { get; set; }

        public int NUMPRODUCTO { get; set; }

        public int? VERSION { get; set; }

        public DateTime FECHAALTA { get; set; }

        public DateTime? FECHABAJA { get; set; }

        public virtual T_TESTSEQUENCEFILE T_TESTSEQUENCEFILE { get; set; }
    }
}
