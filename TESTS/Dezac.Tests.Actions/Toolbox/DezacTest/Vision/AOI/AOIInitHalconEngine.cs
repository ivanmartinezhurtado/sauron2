﻿using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.AOI;

namespace Dezac.Tests.Actions.AOI
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AOIInitHalconEngineDescription))]
    [Browsable(false)]
    public class AOIInitHalconEngine : ActionBase
    {
        public override void Execute()
        {
            HalconInterface.InitEngine();
        }
    }

    public class AOIInitHalconEngineDescription : ActionDescription
    {
        public override string Name { get { return "AOIInitHalconEngine"; } }
        public override string Category { get { return "DezacTest/Vision/AOI"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
