using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Instruments.Router;
using System;
using System.Collections.Generic;
using System.Linq;
using Dezac.Core.Utility;

namespace Dezac.Tests.Measure
{
    [TestVersion(2.04)]
    public class CVMMINI_ETHTest : CVMMINITest
    {
        CVMMINI_Cirbus cvmMiniCirbus;
        private ClientEntry ip;

        public CVMMINI_Cirbus CVMMiniCirbus
        {
            get
            {
                if (cvmMiniCirbus == null)
                    cvmMiniCirbus = new CVMMINI_Cirbus(Comunicaciones.SerialPort);

                return cvmMiniCirbus;
            }
        }

        public override void TestCommunications()
        {
            ResetPowerSource(false, true);

            base.TestCommunications();
        }

        public override void TestCustomization()
        {
            ResetPowerSource(false, true);

            base.TestCustomization();
        }

        public void TestCommunicationEthernetBoard()
        {
            Tower.Chroma.ApplyOffAndWaitStabilisation();

            //ChangeHardwareCirbus();

            Tower.IO.DO.Off(OUT_TTL_PISTON);
            var version = "";
            Tower.Chroma.ApplyAndWaitStabilisation();
            ChangeHardwareCirbus();
            SamplerWithCancel((p) => { version = CVMMiniCirbus.ReadVersion(); return true; }, "Error de comunicaciones", 9, 300, 500, false, false);

            //this.InParellel(()=>
            //{
            //    Tower.Chroma.ApplyAndWaitStabilisation();
            //},() =>
            //{
            //    ChangeHardwareCirbus();
            //    SamplerWithCancel((p) => { version = CVMMiniCirbus.ReadVersion(); return true; }, "Error de comunicaciones", 9, 300, 500, false, false);
            //});

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_PLACA_COM, version, Identificacion.VERSION_FIRMWARE_PLACA_COM, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware de la placa COM incorrecta"),ParamUnidad.SinUnidad);       
        }

        public void TestEthernetBoardSetup()
        {
            if (!cvmMiniCirbus.ReadFlashFormat())
            {
                CVMMiniCirbus.WriteToFormatFlash();
                Delay(20000, "Esperando al formateo de la flash");
                Assert.IsTrue("FORMATEO FLASH ETHERNET", CVMMiniCirbus.ReadFlashFormat(), Error().UUT.CONFIGURACION.SETUP("Error: No se ha podido formatear la flash de la placa Ethernet"));
            }
            if (CVMMiniCirbus.ReadMAC() == "ACK FF-FF-FF-FF-FF-FF")
            {
                TestInfo.NumMAC = this.GetMacServiceDezac(1).FirstOrDefault();
                CVMMiniCirbus.WriteMAC(TestInfo.NumMAC);
                Assert.AreEqual("MAC", CVMMiniCirbus.ReadMAC().Replace("ACK ",""), TestInfo.NumMAC, Error().UUT.CONFIGURACION.SETUP("Error: No se ha podido grabar la MAC en la placa Ethernet"), ParamUnidad.SinUnidad);
            }
            else
                Resultado.Set("MAC", CVMMiniCirbus.ReadMAC().Replace("ACK ", ""), ParamUnidad.SinUnidad);

            CVMMiniCirbus.Cirbus.ClosePort();

            ChangeHardwareCirbus(false);
        }

        public void TestComunicationTCP()
        {
            byte Periferico = Comunicaciones.Periferico;

            var clients = new List<ClientEntry>();

            Tower.IO.DO.Off(OUT_COM_RS485_POSITION);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_COM_PISTON_ETH] && !Tower.IO.DI[IN_COM_PISTON_RS485]; }, "Error: No se ha detectado el piston de seleccion de COM en posicion ETHERNET",
                20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            Tower.IO.DO.On(OUT_EV_COM);
            Tower.IO.DO.On(OUT_ETH_PISTON);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_ETH_DETECTION]; }, "Error: No se ha detectado el piston de ETHERNET",
                20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            ResetPowerSource(false, false);
            Delay(5000, "Espera arranque placa ethernet");

            SamplerWithCancel((p) =>
            {
                clients = Router.Default().GetConnectedClientsFromWeb(true);

                return !clients.Count.Equals(0);
            }, "No se han encontrado clientes DHCP conectados al router", 100, 1000, 1000);

            var ipClient = clients.Where((p) => p.Name.Contains("CVM"));
            if (ipClient == null)
                throw new Exception("No se ha encontrado en la tabla DHCP del router ningun equipo CVM-Mini conectado");

            ip = ipClient.FirstOrDefault();
            TestInfo.NumMAC = ip.MAC.ToString();

            SetVariable("IPClienTFTPtDevice", ip.IP);

            using (var cvmEth = new CVMMINI(502, ip.IP, Periferico))
            {
                SamplerWithCancel((p) => { cvmEth.ReadAllVariables(); return true; }, "Error de comunicaciones por Ethernet", 3, 500, 500, false, false);
                var version = cvmEth.ReadSoftwareVersion().Trim();
            }
        }

        public override void TestFinish()
        {
            CVMMiniCirbus.Dispose();
            base.TestFinish();
        }

        public override void ResetPowerSource(bool test = true)
        {
            ResetPowerSource(test, true);
        }

        private void ResetPowerSource(bool test, bool disableTCP)
        {
            if (disableTCP)
            {
                Logger.InfoFormat("Deshabilitando Comunicaciones Placa Ethernet");
                CVMMini.Modbus.ClosePort();

                ChangeHardwareCirbus();
            }

            base.ResetPowerSource(false);

            if (disableTCP)
            {
                CVMMiniCirbus.Cirbus.Retries = 8;

                CVMMiniCirbus.WriteDisableComunications();

                CVMMiniCirbus.Cirbus.ClosePort();

                ChangeHardwareCirbus(false);

                if (test == true)
                    SamplerWithCancel((p) => { CVMMini.FlagTest(); return true; }, "Error. No se puede establecer comunicación con el equipo despues del reset", 5, 500);
            }

        }

        private void ChangeHardwareCirbus(bool on = true)
        {
            Tower.IO.DO.Off(OUT_TTL_PISTON);
            Delay(300, "Se quitan puntas TTL para cambiar RX por TX");

            if (on)
                Tower.IO.DO.On(OUT_COM_CIRBUS);
            else
                Tower.IO.DO.Off(OUT_COM_CIRBUS);

            Delay(300, "Se ponen puntas TTL con RX por TX cambiados");
            Tower.IO.DO.On(OUT_TTL_PISTON);
        }
    }
}

