﻿using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;

namespace Dezac.Tests.Protection
{
    [TestVersion(0.00)]
    public class VBTiTest : TestBase
    {
        #region Constants

        #endregion

        private Tower3 tower;
        private VBTi vbti;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            var port = Comunicaciones.SerialPort;
            var baudrate = Comunicaciones.BaudRate;

            vbti = new VBTi(port, baudrate);

            tower.ShutdownSources();
            tower.Active24VDC();
            tower.ActiveElectroValvule();
        }

        public void TestConsumption(int TimeaWait=2000, int TimeInterval=1500, int NumSamples=3)
        {
            throw new NotImplementedException();
        }

        public void TestCommunications()
        {
            var version = "0";
            this.SamplerWithCancel((p) => { version = vbti.ReadFirmware(); return true; }, "Error: No se ha podido comunciar con el equipo");
            Assert.AreEqual("VERSION_FIRMWARE", version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("La version de firmware del equipo no coincide con la de la BBDD"), ParamUnidad.SinUnidad);
        }

        public void TestSetupDefault()
        {
            //var ip = vbti.ReadIP();

            //if(!ip.Contains("172.16.157.72"))
            //{
            //    Logger.InfoFormat("Grabando la IP por defecto: 172.16.157.72");

            //    vbti.WriteIP("172.16.157.72 255.255.240.0 172.16.150.99 172.16.144.0");
            //    Delay(500, "");
            //    vbti.SendXML();
            //    Delay(500, "");

            //    ip = vbti.ReadIP();
            //    Assert.Contains("IP_DEFAULT", ip, "172.16.157.72", "Error: No se ha podido grabar de IP por defecto", ParamUnidad.SinUnidad);             
            //}

            //Delay(3000, "");

            //Assert.AreEqual("I2C", vbti.ReadI2C(), "I2C OK", "Error: Comunicaciones I2C KO", ParamUnidad.SinUnidad);
        }

        public void TestEthernet()
        {
            // Envia ping a la "172.16.157.72"
        }

        public void TestLeds()
        {
            throw new NotImplementedException();
        }

        public void TestButtons()
        {
            throw new NotImplementedException();
        }

        public void TestAdjust()
        {
            throw new NotImplementedException();
        }
    
        public void Customization()
        {
            throw new NotImplementedException();
        }

        public void TestFinish()
        {
            if (vbti != null)
                vbti.Dispose();
            if (tower != null)
            {
                tower.ShutdownSources();
                tower.IO.DO.OffWait(500, 0); 
                tower.IO.DO.Off(4);
                tower.Dispose();
            }
        }

        private void ResetPowerSource()
        {
            tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(1000, "Apagado del equipo");
            tower.Chroma.ApplyAndWaitStabilisation();
            Delay(9000, "Arraqnue equipo");

        }
    }
}
