namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ESTRUCTURAMATRICULA
    {
        public ESTRUCTURAMATRICULA()
        {
            this.MATRICULA1 = new HashSet<MATRICULA>();
        }
    
        public int NUMESTRUCTURA { get; set; }
        public string IDOPERARIO { get; set; }
        public int NUMMATRICULA { get; set; }
        public System.DateTime FECHA { get; set; }
    
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual MATRICULA MATRICULA { get; set; }
        public virtual ICollection<MATRICULA> MATRICULA1 { get; set; }
    }
}
