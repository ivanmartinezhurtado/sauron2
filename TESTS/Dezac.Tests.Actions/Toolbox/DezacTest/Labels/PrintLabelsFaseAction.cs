﻿using Dezac.Core.Enumerate;
using Dezac.Data;
using Dezac.Data.ViewModels;
using Dezac.Labels;
using Dezac.Services;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.17)]
    [DesignerAction(Type = typeof(PrintLabelsFaseActionDescription))]
    public class PrintLabelsFaseAction : ActionBase
    {    
        [Category("3. Configuration")]
        [Description("Tipo de etiqueta")]
        public LabelTamplete.LabelTemplate Template { get; set; }

        [Category("3. Configuration")]
        [Description("Fichero de etiqueta a usar, si se deja en blanco se obtiene de AOT Doc.")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Fase de la que se obtiene el fichero de etiqueta, rellenar en caso que la etiqueta este en una fase diferente a la del contexto del test")]
        public TestInfo.FasesEnum Fase { get; set; }

        [Category("3. Configuration")]
        [Description("Impresora con la que se desea imprimir")]
        public EnumPrinters.Printers Printer { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;

            if (context.ExecutionResult != TestExecutionResult.Completed)
                return;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            var cacheSvc = testBase.GetService<ICacheService>();

            var data = context.Services.Get<ITestContext>();
            var labelFileName = GetLabelFileName();

            if (string.IsNullOrEmpty(labelFileName))
            {
                Logger.WarnFormat("No se ha definido ningún archivo de etiqueta o no esta validado por IDP");

                if (Template != LabelTamplete.LabelTemplate.EmbalajeIndividual && Template != LabelTamplete.LabelTemplate.EmbalajeConjunto)
                    Error().PROCESO.PARAMETROS_ERROR.FALTA_ARCHIVO_ETIQUETA(Template.ToString()).Throw();

                return;
            }

            if (!File.Exists(labelFileName))
                Error().PROCESO.PARAMETROS_ERROR.FALTA_ARCHIVO_ETIQUETA(string.Format("El archivo de etiqueta {0} no existe", labelFileName)).Throw();

            Printer = context.RunMode == RunMode.Release ? EnumPrinters.Printers.NINGUNA : Printer;

            ParamValueCollection labelParams = null;
            if (Fase != 0)
                labelParams = GetLabelParamValueCollection(data, Template);

            var btr = cacheSvc.Get<BarTenderReportService>("BARTENDER_SERVICE");
            if (btr == null)
            {
                btr = new BarTenderReportService(labelFileName);
                cacheSvc.AddOrSet("BARTENDER_SERVICE", btr);
            }
            else
                btr.FileName = labelFileName;

            var signal = string.Format("_PRINTLABEL_{0}_",Template.ToString());

            testBase.RunTestSequentiallyOrdered(signal, () =>
            {
                var dictionary = btr.Print(data, LabelTamplete.mapTipoGrupos[Template], Printer, labelParams);
                context.Variables.AddOrUpdate("PRINT_LABEL_" + Template.ToString(), dictionary);
            });
        }

        private string GetLabelFileName()
        {
            if (DataInput != null && !DataInput.IsKeyNullOrEmpty)
                return DataInput.Key;
            
            var context = SequenceContext.Current;
            var modelContext = context.Services.Get<ITestContext>();

            if (modelContext == null)
                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("Falta iniciar una función de carga de datos de contexto del test (InitProduct)").Throw();

            var cacheSvc = context.Services.Get<ICacheService>();
            string key = string.Format("Label_{0}_{1}_{2}.btw", Template, modelContext.NumProducto, modelContext.Version);
            string fileName = null;

            if (cacheSvc.TryGet(key, out fileName))
                return fileName;

            LabelItemViewModel item = GetEtiqueta(modelContext.NumProducto, modelContext.Version, Fase == 0 ? modelContext.IdFase : ((int)Fase).ToString("000"), Template);

            if (item == null || item.Data == null)
            {
                Logger.ErrorFormat("No se ha podido obtener ninguna etiqueta asociada para este producto");
                return null;
            }

            fileName = SaveBinaryFile(key, item.Data);

            cacheSvc.Add(key, fileName);


            return fileName;
        }

        public LabelItemViewModel GetEtiqueta(int numProducto, int version, string idFase, LabelTamplete.LabelTemplate template)
        {
            LabelItemViewModel item = null;
            bool NewLabel = false;

            using (var svc = new DezacService())
            {
                item = svc.GetLabels(numProducto, version, idFase, ((int)Template).ToString()).FirstOrDefault();

                if (item != null)
                    item.Data = svc.GetBinario(item.NumArticulo, "950");

                var grupos = svc.GetGruposParametrizacion(numProducto, version, idFase);
                NewLabel = grupos.Where(p => p.NUMTIPOGRUPO == LabelTamplete.mapTipoGrupos[Template]).Any();

                if(NewLabel)
                {
                    var valueLabesl = grupos.Where(p => p.NUMTIPOGRUPO == LabelTamplete.mapTipoGrupos[Template]).FirstOrDefault().T_VALORPARAMETRO.ToList();
                    var validate = valueLabesl.Where((p) => p.T_PARAMETRO.PARAM.Contains("IDP_VALIDATED")).Select(p => p.VALOR).FirstOrDefault();
                    if (validate == null)
                        NewLabel = false;
                    else
                        NewLabel = validate.Trim().ToUpper() == "SI" || validate.Trim().ToUpper() == "PRUEBA";
                }
            }

            return NewLabel ? item : null;
        }

        private ParamValueCollection GetLabelParamValueCollection(ITestContext data, LabelTamplete.LabelTemplate Template)
        {
            ParamValueCollection paramsList = null;

            List<T_GRUPOPARAMETRIZACION> groups;
            using (var svc = new DezacService())
            {                   
                groups = svc.GetGruposParametrizacion(data.NumProducto, data.Version, Fase == 0 ? data.IdFase : ((byte)Fase).ToString("000"));
            }

            if (groups == null)
                Error().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO(string.Format("No hay grupos de parametrización en la fase {0}", Fase == 0 ? data.IdFase : ((byte)Fase).ToString("000"))).Throw();

            var groupLabel = groups.Where(p => p.NUMTIPOGRUPO == LabelTamplete.mapTipoGrupos[Template]).FirstOrDefault();
            paramsList = data.GetGroupList(LabelTamplete.mapTipoGrupos[Template]);
            if (paramsList != null)
                foreach (var vp in groupLabel.T_VALORPARAMETRO)
                {
                    ParamValue pv = new ParamValue
                    {
                        IdGrupo = vp.NUMGRUPO,
                        IdTipoGrupo = (TipoGrupo)LabelTamplete.mapTipoGrupos[Template],
                        Grupo = paramsList.Name,
                        IdParam = vp.NUMPARAM,
                        ValorInicio = vp.VALORINICIO,
                        Valor = vp.VALOR,
                        IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                        IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                        Unidad = vp.T_PARAMETRO.T_UNIDAD.UNIDAD,
                        Name = vp.T_PARAMETRO.PARAM,
                        IdCategoria = vp.IDCATEGORIA
                    };

                    paramsList.Add(pv);
                }

            return paramsList;
        }

        public bool HasLabel()
        {
            var context = SequenceContext.Current;
            var modelContext = context.Services.Get<ITestContext>();

            LabelItemViewModel item = GetEtiqueta(modelContext.NumProducto, modelContext.Version, modelContext.IdFase, Template);

            return item != null && item.Data != null;
        }

        private string PathFileLabelTemplate
        {
            get { return ConfigurationManager.AppSettings["PathFileLabelTemplate"]; }
        }

        private string SaveBinaryFile(string name, byte[] data)
        {
            if (!Directory.Exists(PathFileLabelTemplate))
                Directory.CreateDirectory(PathFileLabelTemplate);

            string fileName = Path.Combine(PathFileLabelTemplate, name);

            File.WriteAllBytes(fileName, data);

            return fileName;
        }               
    }

    public class PrintLabelsFaseActionDescription : ActionDescription
    {
        public override string Name { get { return "PrintLabelsPhase"; } }
        public override string Category { get { return "DezacTest/Labels"; } }
        public override string Description { get { return "Este Action hace la impresión de Etiquetas del Test"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
    }
}
