﻿using Dezac.Tests.Actions.Kernel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.Toolbox.Actions.Variables.Add
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AddVariableListDescription))]
    public class AddVariableList : ActionBase
    {
        [Description("Lista de nombres de variables que contienen los objetos que se desean añadir a la lista")]
        [Category("3. Configuration")]
        public string[] DataInputList { get; set; } 
        //TODO Revisar para cambiar la variable DataInputList como Lista

        [Description("Instancia/ hilo que ejecutará el Action")]
        [Category("3. Configuration")]
        public ActionVariable Instance { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            int instance;
            var NumInstance = SequenceContext.Current.NumInstance;
            if (Instance == null || Instance.IsKeyNullOrEmpty)
                instance = NumInstance;
            else
                instance = Instance.GetValue<int>();

            if (NumInstance == instance)
            {
                if (DataInputList != null)
                {
                    var list = GetVariable<List<object>>(DataOutput.Key.Replace("$", ""), new List<object>());
                    foreach (var item in DataInputList)
                        list.Add(VariablesTools.ResolveValue(item.ToString()));

                    DataOutput.Value = list;
                }
            }
        }        
    }

    public class AddVariableListDescription : ActionDescription
    {
        public override string Description { get { return "Añade los elementos a una variable del tipo lista, si no existe, la crea"; } }
        public override string Name { get { return "AddVariableList"; } }
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
