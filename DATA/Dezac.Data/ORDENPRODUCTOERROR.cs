namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ORDENPRODUCTOERROR")]
    public partial class ORDENPRODUCTOERROR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NROERROR { get; set; }

        public int NUMFABRICACION { get; set; }

        public int? NUMCAUSA { get; set; }

        public int NUMERROR { get; set; }

        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public DateTime FECHA { get; set; }

        [StringLength(50)]
        public string OBSERVACIONES { get; set; }

        public virtual OPE_ERROR OPE_ERROR { get; set; }

        public virtual OPERARIO OPERARIO { get; set; }

        public virtual ORDENPRODUCTO ORDENPRODUCTO { get; set; }
    }
}
