﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Measure
{
    [TestVersion(2.19)]
    public class eBOXTest : TestBase
    {
        private Tower3 tower;
        private eBOX ebox;
        private eBOX.ModelosMyeBOX model;
        private USB4761 usbIO;
        private double INESTABLE_MEASURE = 168430.09;
        private double PHASE_GAP_OUT_OF_MARGINS = 336860.18;
        private int timeWaitAdjustFactor = 3000;
        private string CAMERA_IDS;

        public USB4761 UsbIO
        {
            get
            {
                try
                {
                    if (usbIO == null)
                    {
                        logger.InfoFormat("Inicializamos modulo USB4761");
                        usbIO = new USB4761();
                    }
                }catch(Exception ex)
                {
                    throw new Exception("Error al Inicializar el modulo USB4761 MOXA por " + ex.Message);
                }
                return usbIO;
            }
        }

        //************************************************************************************************************

        public void TestInitialization(int portCVM = 0)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            ebox = AddInstanceVar(new eBOX(portCVM, Comunicaciones.Periferico), "UUT");
            logger.InfoFormat("Create new Instance MYeBOX class with PortCom {0}", portCVM);
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", ebox.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", ebox.GetType().Name), ParamUnidad.SinUnidad);

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            tower.FLUKE.Tolerance = 0.1;
            tower.FLUKE.DeviceInputImpedance = 8; //Impedacia del eBOX  10KOhms

            timeWaitAdjustFactor =(int)Configuracion.GetDouble("WAITTIME_AFTER_WRITEGAINS", timeWaitAdjustFactor, ParamUnidad.ms);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            UsbIO.DO.Off(0, 1, 2, 3, 4, 5);

            SetVariable("STOP_DEBUGG", false);
        }

        public void TestConsumption()
        {
            tower.IO.DO.On(15);

            Delay(2000, "Espera al arranque del equipo");

            var defs = new AdjustValueDef(Params.I_DC.Null.EnVacio.Name, 0, Params.I_DC.Null.EnVacio.Min(), Params.I_DC.Null.EnVacio.Max(), 0, Params.I_DC.Null.EnVacio.Tol(), ParamUnidad.A);

            TestMeasureBase(defs,
            (step) =>
            {
                var voltageReading = tower.MeasureMultimeter(InputMuxEnum.IN7, MagnitudsMultimeter.VoltDC);
                return voltageReading / 0.1D;
            }, 0, 10, 1000);

        }

        public void TestComunications()
        {
            SamplerWithCancel((p) =>
            {
                ebox.Disable3GEmbeddedConfiguration();
                return true;
            }, "Error, fallo de comunicaciones el equipo no responde", 6, 1000, 1000, false, false);
          
            ebox.FlagTest();

            var modeloEBOX = Identificacion.MODELO == "eBOX_150" ? 1 : 2;
            model = (eBOX.ModelosMyeBOX)modeloEBOX;
            var cliente = 1; //CIRCUTOR
            var modelClient = string.Format("{0:00}{1:00}", modeloEBOX, cliente);  
            ebox.WriteModel(new eBOX.ModelCustomer() { ModelAndCustomer = Convert.ToUInt16(modelClient, 16) });

            Delay(2000, "Grabar modelo equipo Circutor");

            var firmwareVersion = ebox.ReadSoftwareVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La version de firmware del equipo no es la correcta."), ParamUnidad.SinUnidad);

            var deviceVersion = ebox.ReadGenericVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, deviceVersion, Identificacion.VERSION_HARDWARE, Error().UUT.HARDWARE.NO_COINCIDE("Error. La version del conjunto del equipo no es la correcta"), ParamUnidad.SinUnidad);

            Delay(1000, "");

            var bootVersion = ebox.ReadBootVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_BOOT, bootVersion, Identificacion.VERSION_BOOT, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La version de firmware del equipo no es la correcta."), ParamUnidad.SinUnidad);

            var Temperatura = ebox.ReadTemperature();
            Assert.IsValidMaxMin(Params.TEMP.Null.RTC.Name, Temperatura, ParamUnidad.Grados, Params.TEMP.Null.RTC.Min(), Params.TEMP.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("Temperatura"));

            DateTime fecha = DateTime.UtcNow;
            ebox.WriteDateTime(fecha);

            Delay(4000, "Wait to test the clock");
            logger.InfoFormat("Wait to test the clock {0}", "4000 ms");

            var date = ebox.ReadDatetime();
            if (date.Year != fecha.Year && date.Month != fecha.Month && date.Day != fecha.Day)
                throw new Exception("Error Fecha grabada incorrecta");

            SamplerWithCancel(
            (p) =>
            {
                date = ebox.ReadDatetime();
                return (date - fecha).Seconds > 2;
            }, "Error de reloj del equipo, no se detecta el incremento del mismo", 5, 1000);

            if (TestInfo.NumBastidor == null)
                throw new Exception("Error, no se ha introducido numero de bastidor, es necesario leer el número de bastidor del equipo");

            ebox.WriteBastidor((int)TestInfo.NumBastidor);

            var current = ebox.ReadCurrentBatery();

            var voltage = ebox.ReadVoltageBatery();

            var status = ebox.ReadPowerChargeStatus();

        }

        public void TestSetupDefault()
        {
            ebox.FlagTest();

            ebox.WriteReleDefault();

            var gainVoltageDefault = Configuracion.GetDouble("GAIN_VOLTGE_DEFAULT", 292334, ParamUnidad.Puntos);
            ebox.WriteGainVoltage(new eBOX.FourIntStruct() { Value = FourLineValue.Create(gainVoltageDefault) });

            var gainVoltageCmpoundDefault = Configuracion.GetDouble("GAIN_VOLTGE_COMPOUND_DEFAULT", 293451, ParamUnidad.Puntos);
            ebox.WriteGainVoltageCompound(new eBOX.TriIntStruct { Value = TriLineValue.Create(gainVoltageCmpoundDefault) });

            var gainCurrentDefault = Configuracion.GetDouble("GAIN_CURRENT_DEFAULT", 60148, ParamUnidad.Puntos);
            var gainCurrentDefaultLK = Configuracion.GetDouble("GAIN_CURRENT_LK_DEFAULT", 60579, ParamUnidad.Puntos);

            foreach (var value in Enum.GetValues(typeof(eBOX.CircuitsCurrent)))
            {
                var circuit = (eBOX.CircuitsCurrent)value;

                if (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA || circuit == eBOX.CircuitsCurrent._3000mA)
                {
                    if (model == eBOX.ModelosMyeBOX.eBOX_1500)
                    ebox.WriteGainCurrentLk(circuit, (int)gainCurrentDefaultLK);
                    
                    continue;
                }
                ebox.WriteGainCurrents(circuit, new eBOX.FourIntStruct() { Value = FourLineValue.Create(gainCurrentDefault) });
                Delay(1000, "Enviando configuración por defecto del equipo");
            }

            var gainCurrentDefaultL128V_L1 = Configuracion.GetDouble("GAIN_CURRENT_DEFAULT_128V_L1", 60347, ParamUnidad.Puntos);
            var gainCurrentDefaultL128V_L2 = Configuracion.GetDouble("GAIN_CURRENT_DEFAULT_128V_L2", 60450, ParamUnidad.Puntos);
            var gainCurrentDefaultL128V_L3 = Configuracion.GetDouble("GAIN_CURRENT_DEFAULT_128V_L3", 60377, ParamUnidad.Puntos);
            var gainCurrentDefaultL128V_LN = Configuracion.GetDouble("GAIN_CURRENT_DEFAULT_128V_LN", 60382, ParamUnidad.Puntos);
            ebox.WriteGainCurrents(eBOX.CircuitsCurrent._1_28V, new eBOX.FourIntStruct() { Value = FourLineValue.Create(gainCurrentDefaultL128V_L1, gainCurrentDefaultL128V_L2, gainCurrentDefaultL128V_L3, gainCurrentDefaultL128V_LN) });

            Delay(2000, "Enviando configuración por defecto del equipo");

            ConfigWifiConfigurationTest();

            if (model == eBOX.ModelosMyeBOX.eBOX_1500)
                Config3GConfigurationTest();          
        }

        public void TestTeclat()
        {
            var lastKeyPressed = eBOX.KeyboardKeys.NONE;

            ebox.WriteKeyboardTestStart();

            foreach (eBOX.KeyboardKeys key in Enum.GetValues(typeof(eBOX.KeyboardKeys)))
            {
                if (key == eBOX.KeyboardKeys.NONE)
                    continue;

                var imageView = new ImageView(string.Format("Pulse la tecla {0} y a continuación pulse aceptar", key.GetDescription()), string.Format("{0}\\{1}\\TECLADO_{2}.png", PathCameraImages.TrimEnd('\\'), ebox.GetType().Name, key.ToString()));

                var frm = this.ShowForm(() => imageView, MessageBoxButtons.RetryCancel);
                try
                {
                    SamplerWithCancel((s) =>
                    {
                        if (s == 40)
                        {
                            var diagResult = Shell.MsgBox("¿DESEA CONTINUAR CON EL TEST DE TECLADO?", "TEST DE TECLADO", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (diagResult == DialogResult.No)
                                throw new Exception("Error en la comprobación de tecla {0}");
                        }

                        lastKeyPressed = ebox.ReadTestTeclat();

                        var teclaCorrecta = lastKeyPressed == key;

                        Resultado.Set(string.Format("TECLA_{0}", key.ToString()), teclaCorrecta ? "OK" : "KO", ParamUnidad.SinUnidad);

                        return teclaCorrecta;
                    }, string.Format("Error en la comprobación de tecla {0}, se detecta el pulsado de la tecla {1}", key.ToString(), lastKeyPressed.ToString()), 100, 500, 1000);     
                }
                finally
                {
                    this.CloseForm(frm);
                }
            }

            Resultado.Set("TECLADO", "OK", ParamUnidad.SinUnidad);

            ebox.WriteKeyboardTestFinish();
        }

        public void TestDisplay()
        {
            ebox.WriteTestDisplayStart();

            Delay(2000, "Esperando activación display");

            this.TestHalconMatchingProcedure(CAMERA_IDS, "EBOX_DISPLAY", "EBOX_DISPLAY", "EBOX");

            ebox.WriteTestDisplayFinish();
        }

        public void TestLeds()
        {
            List<eBOX.Leds> leds = new List<eBOX.Leds>();

            leds.Add(eBOX.Leds.KEYB_VOLT_CURR_BAT_SD_RED);
            leds.Add(eBOX.Leds.REC_IO_BAT_SD_GREEN);
            leds.Add(eBOX.Leds.WIFI_3G_USB_BAT_SD_BLUE);
           
            foreach (eBOX.Leds led in leds)
            {
                ebox.WriteLeds(led);

                Delay(2000, "Esperando activación leds");

                this.TestHalconFindLedsProcedure(CAMERA_IDS, string.Format("{0}_{1}", model.ToString().ToUpper(), led.ToString()), string.Format("{0}_{1}", model.ToString().ToUpper(), led.ToString()), ebox.GetType().Name.ToUpper());
            }

            ebox.WriteLeds(eBOX.Leds.ALL_OFF);
        }

        public void TestInputOutputs()
        {
            eBOX.ResulTestInOut resultIO = eBOX.ResulTestInOut.RUNNING;

            ebox.FlagTest();

            ebox.WriteTestInOutStart();

            SamplerWithCancel((p) =>
            {
                resultIO = ebox.ReadTestInOut();
                return resultIO != eBOX.ResulTestInOut.RUNNING;

            }, string.Format("Error en el test de entradas y salidas por resultado {0}", resultIO.ToString()), 5, 1000, 2000);

            ebox.WriteTestInOutFinish();

            Assert.AreEqual("ENTRADAS_SALIDAS", resultIO.ToString(), eBOX.ResulTestInOut.OK.ToString(), Error().UUT.CONFIGURACION.NO_COMUNICA(string.Format("Error en el test de entradas y salidas digitales, el equipo notifica del siguiente o siguientes fallos {0}", resultIO.ToString())), ParamUnidad.SinUnidad);
        }

        public void TestEEPROMpinces()
        {
            ebox.FlagTest();

            ebox.WriteEepromClampsTestStart();

            SamplerWithCancel((p) =>
            {
                return ebox.ReadTestEepromPinces() != eBOX.ResultadoTestPinzas.RUNNING;
            }, "se ha terminado el tiempo de espera en el test EEPROM pinzas", 5, 2000, 1000);

            var clampStatus = ebox.ReadTestEepromPinces();

            ebox.WriteEepromClampsTestFinish();

            Assert.AreEqual("TEST_PINZAS", clampStatus.ToString(), eBOX.ResultadoTestPinzas.OK.ToString(), Error().UUT.EEPROM.SETUP(string.Format("Error en el test EEPROM Pinzas, el equipo notifica del siguiente o siguientes problemas {0}", clampStatus.ToString())), ParamUnidad.SinUnidad);
        }

        //************************************************************************************************************
        //    EMBEDDED TEST
        //************************************************************************************************************

        public void TestEmbeddedInitialization()
        {
            ebox.FlagTest();

            SamplerWithCancel((p) =>
            {
                return ebox.ReadEmbeddedStatus() != 0;
            }, "Error, el embedded ha tardado mas tiempo del esperado en arrancar", 65, 3000, 0);

            var imxStatus = ebox.ReadIMXStatus();

            Assert.AreEqual("CODIGO_ERROR_IMX", imxStatus.FirstOrDefault().ToString(), eBOX.IMXErrorCode.NO_ERROR.ToString(), Error().UUT.CONFIGURACION.SETUP(string.Format("Error, el registro IMX notifica del siguiente o siguientes problemas {0}", imxStatus.FirstOrDefault().ToString())), ParamUnidad.SinUnidad);
        }

        public void TestNandFlash()
        {
            eBOX.NandFlashSizeEnum resultSize = eBOX.NandFlashSizeEnum.RUNNING;

            SamplerWithCancel((p) =>
            {
                resultSize = ebox.ReadNandFlashSize();
                return resultSize != eBOX.NandFlashSizeEnum.RUNNING;

            }, "Error se ha terminado el tiempo de espera al preguntar el tamaño de la NandFlash");

            Assert.AreEqual("TAMAÑO_NANDFLASH", resultSize.ToString(), eBOX.NandFlashSizeEnum.SIZE_256MB.ToString(), Error().UUT.MEDIDA.SETUP("Error en el tamaño de la NandFlash"), ParamUnidad.SinUnidad);
        } 

        public void TestPeripheralCompletionCheck()
        {
            SamplerWithCancel((p) =>
                {
                    return ebox.ReadSDWifi3GTestResult();
                }, "Error de test del IMX28, no ha terminado las pruebas en el tiempo esperado", 255, 2500);
        }

        public void TestSD()
        {
            var sdStatus = ebox.ReadSDStatus();

            Assert.AreEqual("ESTADO_SD", sdStatus.ToString(), eBOX.StatusSD.OK.ToString(), Error().UUT.CONFIGURACION.SETUP(string.Format("El estado de la SD no es el esperado, el embedded informa del siguiente error: {0}", sdStatus.GetDescription())), ParamUnidad.SinUnidad);

            var sdSize = ebox.ReadSD1Size();

            Assert.AreGreater("SIZE_SD1", sdSize, Configuracion.GetDouble("SIZE_SD1", 953, ParamUnidad.MByte), Error().UUT.CONFIGURACION.SETUP("Error, el tamaño de la SD1 es incorrecto"), ParamUnidad.MByte);

            sdSize = ebox.ReadSD2Size();

            Assert.AreGreater("SIZE_SD2", sdSize, Configuracion.GetDouble("SIZE_SD2", 7579, ParamUnidad.MByte), Error().UUT.CONFIGURACION.SETUP("Error, el tamaño de la SD2 es incorrecto"), ParamUnidad.MByte);
        }

        public void TestWifi()
        {
            var wifiStatus = ebox.ReadWifiStatus();
            TestInfo.NumMACWifi = ebox.ReadWiFiMAC();
            Resultado.Set("MAC_WIFI", TestInfo.NumMACWifi, ParamUnidad.SinUnidad);
            Assert.AreEqual("ESTADO_WIFI", wifiStatus.ToString(), eBOX.StatusWifi.OK.ToString(), Error().SOFTWARE.FALLO_RED.WIFI(string.Format("El estado del wifi no es el esperado, el embedded informa del siguiente error: {0}", wifiStatus.GetDescription())), ParamUnidad.SinUnidad);

            if (TestInfo.NumMACWifi.Trim() == "00:00:00:00:00:00")
                throw new Exception("Error equipo Ok en el test de WIFI pero se ha leido la MAC a 0");
        }

        public void Test3G()
        {
            SamplerWithCancel((p) =>
            {
                var _3GStatus = ebox.Read3GStatus();

                TestInfo.NumMAC3G = ebox.Read3GMAC();
                Resultado.Set("MAC_3G", TestInfo.NumMAC3G, ParamUnidad.SinUnidad);
                Assert.AreEqual("ESTADO_3G", _3GStatus.ToString(), eBOX.Status3G.OK.ToString(), Error().SOFTWARE.ERROR_TRANSFERENCIA.TRESG(string.Format("El estado del 3G no es el esperado, el embedded informa del siguiente error: {0}", _3GStatus.GetDescription())), ParamUnidad.SinUnidad);

                TestInfo.IMEI = ebox.ReadIMEI3G();
                Resultado.Set("IMEI_3G", TestInfo.IMEI, ParamUnidad.SinUnidad);

                if (TestInfo.IMEI.Trim() == "000000000000000")
                    throw new Exception("Error equipo Ok en el test 3G pero se ha leido el IMEI a 0");

                if (TestInfo.NumMAC3G.Trim() == "00:00:00:00:00:00")
                    throw new Exception("Error equipo Ok en el test 3G pero se ha leido la MAC a 0");

                return true;

            }, string.Empty, 5, 2000, 0, false, true);
        }

        public void TestUSB()
        {
           // tower.Chroma.ApplyAndWaitStabilisation(230, 0, 50);

            DriveInfo usbDrive = null;

            SamplerWithCancel((p) =>
                {
                    foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Fixed))
                        if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DRIVE_NAME", "MYEBOX", ParamUnidad.SinUnidad).ToUpper())
                        {
                            usbDrive = drive;
                            break;
                        }

                    return usbDrive != null;
                }, "Error No se ha encontrado el dispositivo USB del MYeBOX", 8, 5000);

            var destinationFilename = Path.Combine(usbDrive.RootDirectory.ToString(), "test.txt");
            File.WriteAllText(destinationFilename, "Fichero de prueba para el test USB del equipo MYeBOX");

            var md5String = string.Empty;
            using (var md5 = MD5.Create())
            {
                md5String = md5.ComputeHash(File.ReadAllBytes(destinationFilename)).BytesToHexString();
            }

            CodeUtils.Safe(() => File.Delete(destinationFilename), "Imposible eliminar el archivo");

            Assert.AreEqual("MD5_CODE", md5String, Configuracion.GetString("MD5_CRC", "21BAE788694393267CBE71514C3EF132", ParamUnidad.SinUnidad), Error().UUT.CONFIGURACION.SETUP("Error, el MD5 no coinciden!"), ParamUnidad.SinUnidad);

            //tower.Chroma.ApplyOff();
        }

        //************************************************************************************************************
        //    AJUSTE VERIFICACION TEST PINZAS EEPROM NORMALES TENSIONES
        //************************************************************************************************************

        public void TestAdjustAndVerification_2V()
        {
            TriLineValue phaseGap = TriLineValue.Create(45); // Consignas.GetDouble(Params.PHASE.L1.Ajuste.TestPoint(eBOX.CircuitsCurrent._2V.ToString()).Name, 45, ParamUnidad.Grados), Consignas.GetDouble(Params.PHASE.L2.Ajuste.TestPoint(eBOX.CircuitsCurrent._2V.ToString()).Name, 45, ParamUnidad.Grados), Consignas.GetDouble(Params.PHASE.L3.Ajuste.TestPoint(eBOX.CircuitsCurrent._2V.ToString()).Name, 45, ParamUnidad.Grados));

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._2V, phaseGap, outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            internalAdjustVoltage(apply.Item1);

            internalAdjustCurrent(eBOX.CircuitsCurrent._2V);

            internalVerificationVoltage(apply.Item1,  apply.Item2, 45);

            tower.FLUKE.ApplyOffAndWaitStabilisation();
            tower.FLUKE.SetOuputSynchronism(FLUKE6003A.OutputFrequencySynchronizationModes.INT);
            var apply60Hz = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._2V, phaseGap, outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT, frec: eBOX.Frecuency._60Hz);

            internalAdjustVoltage(apply60Hz.Item1, eBOX.Frecuency._60Hz);

            internalVerificationVoltage(apply60Hz.Item1, apply60Hz.Item2, 45, eBOX.Frecuency._60Hz);

            tower.FLUKE.ApplyOffAndWaitStabilisation();
            tower.FLUKE.SetOuputSynchronism(FLUKE6003A.OutputFrequencySynchronizationModes.LINE);

            if (model == eBOX.ModelosMyeBOX.eBOX_1500)
            {
                apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._2V, phaseGap, outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

                internalAdjustCurrentLeak(eBOX.CircuitsCurrent._2V);
             
                internalVerificationCurrentLeak(eBOX.CircuitsCurrent._2V, apply.Item2);
            }

            tower.FLUKE.ApplyOffAndWaitStabilisation();
        }

        public void TestAdjustAndVerification_1_28V()
        {
            TriLineValue phaseGap = TriLineValue.Create(45); // Consignas.GetDouble(Params.PHASE.L1.Ajuste.TestPoint(eBOX.CircuitsCurrent._1_28V.ToString()).Name, 45, ParamUnidad.Grados), Consignas.GetDouble(Params.PHASE.L2.Ajuste.TestPoint(eBOX.CircuitsCurrent._1_28V.ToString()).Name, 45, ParamUnidad.Grados), Consignas.GetDouble(Params.PHASE.L3.Ajuste.TestPoint(eBOX.CircuitsCurrent._1_28V.ToString()).Name, 45, ParamUnidad.Grados));

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._1_28V, phaseGap, outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            Delay(10000, "Espera estabilización fuente");
        
            internalAdjustCurrent(eBOX.CircuitsCurrent._1_28V);

            internalVerificationCurrent(eBOX.CircuitsCurrent._1_28V, apply.Item2);

            tower.FLUKE.ApplyOffAndWaitStabilisation();
        }

        public void TestAdjustAndVerification_333mV()
        {
            TriLineValue phaseGap = TriLineValue.Create(45); // Consignas.GetDouble(Params.PHASE.L1.Ajuste.TestPoint(eBOX.CircuitsCurrent._333mV.ToString()).Name, 45, ParamUnidad.Grados), Consignas.GetDouble(Params.PHASE.L2.Ajuste.TestPoint(eBOX.CircuitsCurrent._333mV.ToString()).Name, 45, ParamUnidad.Grados), Consignas.GetDouble(Params.PHASE.L3.Ajuste.TestPoint(eBOX.CircuitsCurrent._333mV.ToString()).Name, 45, ParamUnidad.Grados));

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._333mV, phaseGap, outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            Delay(10000, "Espera estabilización fuente");

            internalAdjustCurrent(eBOX.CircuitsCurrent._333mV);

            internalVerificationCurrent(eBOX.CircuitsCurrent._333mV, apply.Item2);

            tower.FLUKE.ApplyOffAndWaitStabilisation();
        }
        //************************************************************************************************************
        //    AJUSTE VERIFICACION TEST PINZAS ROWOSKY
        //************************************************************************************************************

        public void TestAdjustAndVerification_10mV()
        {
            UsbIO.DO.OnWait(1000, 4, 5);

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._10mV, Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados), outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            internalAdjustCurrent(eBOX.CircuitsCurrent._10mV);

            internalVerificationCurrent(eBOX.CircuitsCurrent._10mV, apply.Item2);
        }

        public void TestAdjustAndVerification_100mV()
        {
            UsbIO.DO.OnWait(1000, 4, 5);

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._100mV, Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados), outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            internalAdjustCurrent(eBOX.CircuitsCurrent._100mV);

            internalVerificationCurrent(eBOX.CircuitsCurrent._100mV, 100);
        }

        public void TestAdjustAndVerification_1000mV()
        {
            UsbIO.DO.OnWait(1000, 4, 5);

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._1000mV, Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados), outputCurrentMode: OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            internalAdjustCurrent(eBOX.CircuitsCurrent._1000mV);

            internalVerificationCurrent(eBOX.CircuitsCurrent._1000mV, 100);

            tower.FLUKE.ApplyOffAndWaitStabilisation();
        }

        //************************************************************************************************************
        //    AJUSTE VERIFICACION TEST PINZAS DIRECTAS CORRIENTES
        //************************************************************************************************************

        public void TestAdjustAndVerification_250mA()
        {
            UsbIO.DO.OnWait(1000, 0, 1, 4, 5);

            ebox.WriteTestAdjust(eBOX.CircuitsCurrent._250mA);

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._250mA, Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados));

            internalAdjustCurrent(eBOX.CircuitsCurrent._250mA);

            internalVerificationCurrent(eBOX.CircuitsCurrent._250mA, apply.Item2);

            tower.FLUKE.ApplyOffAndWaitStabilisation();

        }

        //************************************************************************************************************
        //    AJUSTE VERIFICACION TEST PINZAS DIRECTAS CORRIENTES FUGAS Y NEUTRO
        //************************************************************************************************************

        public void TestAdjustAndVerification_3A()
        {
            UsbIO.DO.OffWait(1000, 0, 1, 4, 5);

            UsbIO.DO.OnWait(1000, 2, 3);

            ebox.WriteTestAdjust(eBOX.CircuitsCurrent._3000mA);

            var apply = ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent._3000mA, Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados), hasL1: false, hasL3: false);

            internalAdjustCurrentLeak(eBOX.CircuitsCurrent._3000mA);

            internalVerificationCurrentLeak(eBOX.CircuitsCurrent._3000mA, apply.Item2);

            Delay(timeWaitAdjustFactor, "Espera grabacion de factores");

            var ganaciaEscal_3000mV = ebox.ReadGainCurrentsLk(eBOX.CircuitsCurrent._30mA);
            var ganacia_EscvalLow_30mA = ganaciaEscal_3000mV - (0.6710422272 * ganaciaEscal_3000mV) / 100;
            Resultado.Set(Params.GAIN_I.LK.Ajuste.TestPoint(eBOX.CircuitsCurrent._30mA.ToString()).Name, ganacia_EscvalLow_30mA, ParamUnidad.Puntos);
            ebox.WriteGainCurrentLk(eBOX.CircuitsCurrent._30mA, (int)ganacia_EscvalLow_30mA);

            Delay(timeWaitAdjustFactor, "Espera grabacion de factores");

            var ganacia_EscvalLow_300mA = ganaciaEscal_3000mV - (0.4464457417 * ganaciaEscal_3000mV) / 100;
            Resultado.Set(Params.GAIN_I.LK.Ajuste.TestPoint(eBOX.CircuitsCurrent._300mA.ToString()).Name, ganacia_EscvalLow_300mA, ParamUnidad.Puntos);
            ebox.WriteGainCurrentLk(eBOX.CircuitsCurrent._300mA, (int)ganacia_EscvalLow_300mA);

            Delay(timeWaitAdjustFactor, "Espera grabacion de factores");

            tower.FLUKE.ApplyOffAndWaitStabilisation();

            UsbIO.DO.OffWait(1000, 2, 3);
        }

        //************************************************************************************************************

        public void Customization()
        {
            ebox.WriteSerialNumber(TestInfo.NumSerie);

            Delay(1000, "Esrcibimos Numero de Serie");

            var modeloEBOX = Identificacion.MODELO == "eBOX_150" ? 1 : 2;
            model = (eBOX.ModelosMyeBOX)modeloEBOX;
            var cliente = Convert.ToByte(Identificacion.COSTUMER == "CIRCUTOR" ? 1 : Identificacion.COSTUMER == "ENDESA" ? 0 : 2);
            var modelClient = string.Format("{0:00}{1:00}", modeloEBOX, cliente);
            ebox.WriteModel(new eBOX.ModelCustomer() { ModelAndCustomer = Convert.ToUInt16(modelClient, 16) });

            Delay(2000, "Esrcibimos Modelo");

            tower.IO.DO.OffWait(10000, 15);

            tower.IO.DO.OnWait(4000, 15);

            SamplerWithCancel((p) => { ebox.FlagTest(); return true; }, "Error de comunicaciones al volver a enceder el equipo", 20, 1000, 500, false, false);

            foreach (var value in Enum.GetValues(typeof(eBOX.CircuitsCurrent)))
            {
                var circuit = (eBOX.CircuitsCurrent)value;
                if (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA || circuit == eBOX.CircuitsCurrent._3000mA)
                {
                    logger.DebugFormat("Read Gain Current circuit:{0}, Gain LK:{0}", circuit, ebox.ReadGainCurrentsLk(circuit));
                    continue;
                }
                logger.DebugFormat("Read Gain Current ciruit:{0} Gain current:{1}", circuit, ebox.ReadGainCurrents(circuit).Value);
            }

            var numserie = ebox.ReadSerialNumber();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numserie, TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, valor de numero de serie leido diferente al grabado"), ParamUnidad.SinUnidad);

            TestInfo.SSID = "MYeBOX_" + numserie;
            Resultado.Set("SSID", TestInfo.SSID, ParamUnidad.SinUnidad);

            var modeloCustomer = ebox.ReadModel();
            Assert.AreEqual(ConstantsParameters.Identification.MODELO, Convert.ToInt32(modeloCustomer.ModelMyeBOX), modeloEBOX, Error().PROCESO.PRODUCTO.NO_COINCIDE("Error modelo de MyeBOX incorrecto"), ParamUnidad.SinUnidad);
            Assert.AreEqual(ConstantsParameters.Identification.COSTUMER,Convert.ToByte(modeloCustomer.CustomerMyeBOX), cliente, Error().PROCESO.PRODUCTO.NO_COINCIDE("Error cliente del MyeBOX incorrecto"), ParamUnidad.SinUnidad);

            DateTime fecha = DateTime.UtcNow;
            var date = ebox.Modbus.Read<eBOX.FechaHora>((ushort)eBOX.Registers.FECHA_HORA).ToDateTime();
            int timeDiff = (fecha - date).Seconds;
            Assert.AreGreater(10, timeDiff, Error().UUT.MEDIDA.MARGENES("Error. El equipo ha perdido la hora del reloj en mas de 10seg."));

            var code = ebox.ReadRegisterCode();
            TestInfo.PSW = code;
            Resultado.Set("REGISTER_CODE", code, ParamUnidad.SinUnidad);
            Resultado.Set("PSW", code, ParamUnidad.SinUnidad);
            
            ebox.WriteReleDefault();
       
            SetVariable("STOP_DEBUGG", true);
        }

        public void TestFinish()
        {
            try
            {
                ebox.WriteResetControlEmbedded();
                Delay(10000, "Reset control");

                if (SequenceContext.Current.TestResult.ExecutionResult == TestExecutionResult.Completed)
                    Delay(50000, "Reset control");
            }
            finally
            {
                if (ebox != null)
                    ebox.Dispose();

                if (tower != null)
                {
                    if (tower.IO != null)
                        tower.IO.DO.Off(15, 17);

                    tower.ShutdownSources();

                    tower.Dispose();
                }

                if (UsbIO != null)
                    UsbIO.Dispose();
            }
        }

        //************************************************************************************************************
        // CONSIGNAR
        //************************************************************************************************************

        private Tuple<double,double> ApplyVoltageCurrentByCircuit(eBOX.CircuitsCurrent circuit, TriLineValue phaseGap, bool hasL1 = true, bool hasL2 = true, bool hasL3 = true, OutputCurrentModes outputCurrentMode = OutputCurrentModes.CURRENT, eBOX.Frecuency frec = eBOX.Frecuency._50Hz)
        {
            var tP = circuit.ToString().Trim('_');
            double voltage = Consignas.GetDouble(Params.V_AC.Null.Ajuste.TestPoint(tP).Name, 280, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I_AC.Null.Ajuste.TestPoint(tP).Name, 0.8, ParamUnidad.A);

            tower.FLUKE.SetOutputCurrentMode(outputCurrentMode);
            tower.FLUKE.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(voltage), TriLineValue.Create(hasL1 ? current : 0, hasL2 ? current : 0, hasL3 ? current : 0), (double)frec, phaseGap, TriLineValue.Create(0, 240, 120));

            return Tuple.Create(voltage, current);
        }

        //************************************************************************************************************
        // AJUSTE 
        //************************************************************************************************************

        private void internalAdjustVoltage(double voltage, eBOX.Frecuency frecuency = eBOX.Frecuency._50Hz)
        {
            var tP = eBOX.CircuitsCurrent._2V.ToString().Trim('_');
            if (frecuency == eBOX.Frecuency._60Hz)
                tP += frecuency.ToString();

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.TestPoint(tP).Name, Params.GAIN_V.Null.Ajuste.TestPoint(tP).Min(), Params.GAIN_V.Null.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.Puntos, model == eBOX.ModelosMyeBOX.eBOX_1500 ? true : false));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.TestPoint(tP).Name, Params.GAIN_V.L12.Ajuste.TestPoint(tP).Min(), Params.GAIN_V.L12.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.Puntos));

            var gain = TestMeasureBase(triGains,
            (step) =>
            {
                var gainList = new List<double>();

                var vars = ReadMeasureVoltageCurrent(eBOX.CircuitsCurrent._2V, false, true, frecuency);
                var patternDeviceReadings = tower.FLUKE.ReadVoltage();
                var Voltage_Between_Phases = TriLineValue.Create(Math.Sqrt(3) * tower.FLUKE.ReadVoltage().L1);

                logger.DebugFormat("Voltage {0}", vars.Voltage.Value);

                var newGainVoltage = ebox.CalculGain(FourLineValue.Create(patternDeviceReadings, patternDeviceReadings.L1), ebox.ReadGainVoltage().Value, vars.Voltage.Value);
                logger.DebugFormat("Gain Voltage {0}", newGainVoltage);
                gainList.Add(model == eBOX.ModelosMyeBOX.eBOX_1500 ? newGainVoltage.ToArray() : newGainVoltage.LINES.ToArray());

                var newGainVoltageCompound = ebox.CalculGain(Voltage_Between_Phases, ebox.ReadGainVVoltageCompound().Value, vars.VoltageCompound.Value);
                logger.DebugFormat("Gain Voltage compound L12={0}  L23={1}  L31={2}", newGainVoltageCompound.L1, newGainVoltageCompound.L2, newGainVoltageCompound.L3);
                gainList.Add(newGainVoltageCompound.ToArray());

                return gainList.ToArray();

            }, 0, 1, 500);

            ebox.FlagTest();

            ebox.WriteGainVoltage(new eBOX.FourIntStruct() { Value = FourLineValue.Create(gain[0].L1.Value, gain[0].L2.Value, gain[0].L3.Value, model == eBOX.ModelosMyeBOX.eBOX_150 ? ebox.ReadGainVoltage().Value.LN : gain[0].Neutro.Value) }, frecuency);

            if (frecuency == eBOX.Frecuency._50Hz)
                ebox.WriteGainVoltageCompound(new eBOX.TriIntStruct { Value = TriLineValue.Create(gain[1].L1.Value, gain[1].L2.Value, gain[1].L3.Value) });

            Delay(timeWaitAdjustFactor, "Espera estabilizacion al enviar ganancias");
        }

        private void internalAdjustCurrent(eBOX.CircuitsCurrent circuit)
        {
            var tP = circuit.ToString().Trim('_');

            var triGains = new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.TestPoint(tP).Name, Params.GAIN_I.Null.Ajuste.TestPoint(tP).Min(), Params.GAIN_I.Null.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.Puntos, circuit != eBOX.CircuitsCurrent._250mA, false);

            var gainCurrentRead = ebox.ReadGainCurrents(circuit).Value;

            var gain = TestMeasureBase(triGains,
                 (step) =>
                 {
                     var gainList = new List<double>();

                     var vars = ReadMeasureVoltageCurrent(circuit);
                     logger.DebugFormat("Current {0} ", vars.Currents.Value);

                     var currentPatern = realtionVoltageFromCurrent[circuit]; 

                     var factorEscal_1V = circuit == eBOX.CircuitsCurrent._1000mV ? 10 : 1;

                     var newGainCurrent = ebox.CalculGain(currentPatern, gainCurrentRead, vars.Currents.Value * factorEscal_1V);
                     logger.DebugFormat("Gain Current {0}", newGainCurrent);

                     gainList.Add(circuit == eBOX.CircuitsCurrent._250mA ? newGainCurrent.LINES.ToArray() : newGainCurrent.ToArray());

                     return gainList.ToArray();

                 }, 0, 1, 500);

            ebox.FlagTest();

            ebox.WriteGainCurrents(circuit, new eBOX.FourIntStruct() { Value = FourLineValue.Create(gain.L1.Value, gain.L2.Value, gain.L3.Value, circuit != eBOX.CircuitsCurrent._250mA ? gain.Neutro.Value : gainCurrentRead.LN) });

            Delay(timeWaitAdjustFactor, "Espera estabilizacion al enviar ganancias");

            gainCurrentRead = ebox.ReadGainCurrents(circuit).Value;
        
        }

        private void internalAdjustCurrentNeutral(eBOX.CircuitsCurrent circuit)
        {
            var tP = circuit == eBOX.CircuitsCurrent._300mA ? eBOX.CircuitsCurrent._250mA.ToString().Trim('_') : circuit.ToString().Trim('_');
            var neutralGain = new AdjustValueDef(Params.GAIN_I.LN.Ajuste.TestPoint(tP).Name, 0, Params.GAIN_I.LN.Ajuste.TestPoint(tP).Min(), Params.GAIN_I.LN.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.Puntos);


            var gain = TestMeasureBase(neutralGain,
                 (step) =>
                 {
                     var vars = ReadMeasureVoltageCurrent(circuit);
                     logger.DebugFormat("Current LN{0} ", vars.Currents.Value.LN);

                     var currentPatern = realtionVoltageFromCurrent[circuit];

                     var newGainCurrent = ebox.CalculGain(currentPatern, ebox.ReadGainCurrents(circuit).Value.LN, vars.Currents.Value.LN);
                     logger.DebugFormat("Gain Current LN{0}", newGainCurrent);

                     return newGainCurrent.LINES.L1;

                 }, 0, 1, 500);

            ebox.FlagTest();

            ebox.WriteGainCurrents(circuit, new eBOX.FourIntStruct() { Value = FourLineValue.Create(ebox.ReadGainCurrents(circuit).Value.LINES, gain.Value) });

            Delay(timeWaitAdjustFactor, "Espera estabilizacion al enviar ganancias");
        }

        private void internalAdjustCurrentLeak(eBOX.CircuitsCurrent circuit)
        {
            var tP = circuit.ToString().Trim('_');
            var adjustFugas = new AdjustValueDef(Params.GAIN_I.LK.Ajuste.TestPoint(tP).Name, 0, Params.GAIN_I.LK.Ajuste.TestPoint(tP).Min(), Params.GAIN_I.LK.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.Puntos);


            var gain = TestMeasureBase(adjustFugas,
                 (step) =>
                 {
                     var lkCurrent = ReadMeasureCurrentLeak(circuit) / (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA ? 100 : 1);// Dividimos entre 100 por la modificacion para tener mas cifras significativas en el circuito de 30mV para poder tener mas precisión tanto en ajuste como en verificacion

                     var currentPatern = realtionVoltageFromCurrent[circuit].LINES.L2;
                     if (lkCurrent == 0)
                         throw new Exception("Error en la medida de corriente, el equipo no mide corriente la línea lk");

                     var newGainCurrentLK = (double)ebox.CalculGain(currentPatern, (double)ebox.ReadGainCurrentsLk(circuit), lkCurrent);
                     logger.DebugFormat("Gain Current LK={0}", newGainCurrentLK);

                     return newGainCurrentLK;

                 }, 0, 1, 500);

            ebox.FlagTest();

            ebox.WriteGainCurrentLk(circuit, (int)gain.Value);

            Delay(timeWaitAdjustFactor, "Espera estabilizacion al enviar ganancias");
        }

        //************************************************************************************************************
        // VERIFICACION  
        //************************************************************************************************************

        private void internalVerificationVoltage(double voltage, double current, double gap, eBOX.Frecuency frec = eBOX.Frecuency._50Hz)
        {
            var tP = eBOX.CircuitsCurrent._2V.ToString().Trim('_');
            if (frec == eBOX.Frecuency._60Hz)
                tP += frec.ToString();

            var toleranciaV = Params.V.Null.Verificacion.TestPoint(tP).Tol();
            var toleranciaI = Params.I.Null.Verificacion.TestPoint(tP).Tol();
            var toleranciaKW = Params.KW.Null.Verificacion.TestPoint(tP).Tol();
            var toleranciaKVAR = Params.KVAR.Null.Verificacion.TestPoint(tP).Tol();

            UsbIO.DO.OnWait(2000, 6, 7);

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(tP).Name, 0, 0, voltage, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.V.L12.Verificacion.TestPoint(tP).Name, 0, 0, voltage * Math.Sqrt(3), toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(tP).Name, 0, 0, current, toleranciaI, ParamUnidad.A, true, false));
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.TestPoint(tP).Name, 0, 0, current, toleranciaKW, ParamUnidad.W, false, false));
            defs.Add(new TriAdjustValueDef(Params.KVAR.L1.Verificacion.TestPoint(tP).Name, 0, 0, current, toleranciaKVAR, ParamUnidad.Var, false, false));

            if (model == eBOX.ModelosMyeBOX.eBOX_1500)
                defs[0].Neutro = new AdjustValueDef(Params.V.LN.Verificacion.TestPoint(tP).Name, 0, 0, 0, voltage, toleranciaV, ParamUnidad.V);

            TestCalibracionBase(defs,
                () =>
                {
                    var varsList = new List<double>();
                   // var vars = ReadMeasureVoltageCurrent(eBOX.CircuitsCurrent._2V, true, true); //, true);

                    var vars = ebox.ReadVariablesInstantaneas();
                    var measureIII = ebox.ReadVariablesInstantaneasIII();

                    varsList.Add(vars.L1.Tension);
                    varsList.Add(vars.L2.Tension);
                    varsList.Add(vars.L3.Tension);
                    logger.DebugFormat("Voltage L1={0} L2={1} L3={2}", vars.L1.Tension, vars.L2.Tension, vars.L3.Tension);

                    if (model == eBOX.ModelosMyeBOX.eBOX_1500)
                        varsList.Add(measureIII.TensionNeutro);

                    varsList.Add(measureIII.TensionLineaL1L2);
                    varsList.Add(measureIII.TensionLineaL2L3);
                    varsList.Add(measureIII.TensionLineaL3L1);
                    logger.DebugFormat("Voltage Compound L12={0} L23={1} L31={2}", measureIII.TensionLineaL1L2, measureIII.TensionLineaL2L3, measureIII.TensionLineaL3L1);

                    varsList.Add(vars.L1.Corriente);
                    varsList.Add(vars.L2.Corriente);
                    varsList.Add(vars.L3.Corriente);
                    logger.DebugFormat("Current L1={0} L2={1} L3={2}", vars.L1.Corriente, vars.L2.Corriente, vars.L3.Corriente);
                    varsList.Add(measureIII.CorrienteNeutro);
                    logger.DebugFormat("Current LN={0}", measureIII.CorrienteNeutro);

                    //var powers = ebox.ReadPowers();
                    varsList.Add(vars.L1.PotenciaActiva);
                    varsList.Add(vars.L2.PotenciaActiva);
                    varsList.Add(vars.L3.PotenciaActiva);
                    logger.DebugFormat("Pot.Activa L1={0} L2={1} L3={2}", vars.L1.PotenciaActiva, vars.L2.PotenciaActiva, vars.L3.PotenciaActiva);

                    varsList.Add(vars.L1.PotenciaReactivaInductiva);
                    varsList.Add(vars.L2.PotenciaReactivaInductiva);
                    varsList.Add(vars.L3.PotenciaReactivaInductiva);
                    logger.DebugFormat("Pot.Reactiva L1={0} L2={1} L3={2}", vars.L1.PotenciaReactivaInductiva, vars.L2.PotenciaReactivaInductiva, vars.L3.PotenciaReactivaInductiva);

                    return varsList.ToArray();
                },
                () =>
                {
                    var voltageRef = tower.FLUKE.ReadVoltage();
                    var Voltage_Between_Phases = TriLineValue.Create(Math.Sqrt(3) * tower.FLUKE.ReadVoltage().L1);
                    var currentPatern = realtionVoltageFromCurrent[eBOX.CircuitsCurrent._2V];
                    var power = TriLinePower.Create(voltageRef, currentPatern.LINES, TriLineValue.Create(gap));
                    var varsList = new List<double>();

                    varsList.AddRange(voltageRef.ToArray());

                    if (model == eBOX.ModelosMyeBOX.eBOX_1500)
                        varsList.Add(voltageRef.L1);

                    varsList.AddRange(Voltage_Between_Phases.ToArray());

                    varsList.AddRange(currentPatern.ToArray());

                    varsList.AddRange(power.KW.ToArray());

                    varsList.AddRange(power.KVAR.ToArray());

                    return varsList.ToArray();

                }, 1, 2, 500);
        }

        private void internalVerificationCurrent(eBOX.CircuitsCurrent circuit, double current)
        {
            var tP = circuit.ToString().Trim('_');
            var phaseGap = Consignas.GetDouble(Params.PHASE.Null.Verificacion.Name, 45, ParamUnidad.Grados);

            var toleranciaI = Params.I.Null.Verificacion.TestPoint(tP).Tol();

            var defs = new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(tP).Name, 0, 0, current, toleranciaI, ParamUnidad.A, circuit != eBOX.CircuitsCurrent._250mA, false);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = ReadMeasureVoltageCurrent(circuit); //, true);
                    logger.DebugFormat("Current eBOX {0}", vars.Currents.Value);

                    var factorEscal_1V = circuit == eBOX.CircuitsCurrent._1000mV ? 10 : 1;
                    var measure = circuit == eBOX.CircuitsCurrent._250mA ? (vars.Currents.Value * factorEscal_1V).LINES.ToArray() : (vars.Currents.Value * factorEscal_1V).ToArray();

                    var varsList = new List<double>();
                    varsList.AddRange(measure);

                    return varsList.ToArray();
                },
                () =>
                {
                    var currentPatern = circuit == eBOX.CircuitsCurrent._250mA ? realtionVoltageFromCurrent[circuit].LINES.ToArray() : realtionVoltageFromCurrent[circuit].ToArray();
                    return currentPatern.ToArray();
              
                }, 1, 2, 500, string.Format("Error en la verificación de las corrientes del circuito {0}", tP));
        }

        private void internalVerificationCurrentNeutral(eBOX.CircuitsCurrent circuit, double current)
        {
            var tP = circuit == eBOX.CircuitsCurrent._300mA ? eBOX.CircuitsCurrent._250mA.ToString().Trim('_') : circuit.ToString().Trim('_');
            var toleranciaI = Params.I.Null.Verificacion.TestPoint(tP).Tol();
            var defs = new AdjustValueDef(Params.I.LN.Verificacion.TestPoint(tP).Name, 0, 0, 0, current, toleranciaI, ParamUnidad.A);

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = ReadMeasureVoltageCurrent(circuit, true);
                    logger.DebugFormat("Current LN{0}", vars.Currents.Value.LN);
                    return vars.Currents.Value.LN;
                },
                () =>
                {
                    var currentPatern = realtionVoltageFromCurrent[circuit].LINES.L1;
                    return currentPatern;

                }, 1, 2, 500, string.Format("Error en la verificación de la corriente neutro del circuito {0}", tP));
        }

        private void internalVerificationCurrentLeak(eBOX.CircuitsCurrent circuit, double current)
        {
            var tP = circuit.ToString().Trim('_');
            var toleranciaI = Params.I.Null.Verificacion.TestPoint(tP).Tol();
            var defsFugas = new AdjustValueDef(Params.I.LK.Verificacion.TestPoint(tP).Name, 0, 0, 0, current, toleranciaI, ParamUnidad.A);

            TestCalibracionBase(defsFugas,
                () =>
                {
                    var currentLk = ReadMeasureCurrentLeak(circuit, true) / (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA ? 100 : 1); // Dividimos entre 100 por la modificacion para tener mas cifras significativas en el circuito de 30mV para poder tener mas precisión tanto en ajuste como en verificacion;
                    logger.DebugFormat("Current LK{0}", currentLk);
                    return currentLk;
                },
                () =>
                {
                    var currentPatern = realtionVoltageFromCurrent[circuit].LINES.L2;
                    return currentPatern;

                }, 1, 2, 500, string.Format("Error en la verificación de la corriente fugas del circuito {0}", tP));
        }

        //************************************************************************************************************
        //************************************************************************************************************

        private eBOX.MeasureVoltageCurrent ReadMeasureVoltageCurrent(eBOX.CircuitsCurrent circuit, bool verification = false, bool VoltageTest = false, eBOX.Frecuency frec = eBOX.Frecuency._50Hz)
        {
            var vars = new eBOX.MeasureVoltageCurrent();

            ebox.FlagTest();

            if (VoltageTest)
            {
                var tP = circuit.ToString().Trim('_');
                if (frec == eBOX.Frecuency._60Hz)
                    tP += frec.ToString();

                var triMeasure = new List<TriAdjustValueDef>();
                triMeasure.Add(new TriAdjustValueDef(Params.V.L1.Ajuste.TestPoint(tP).Name, Params.V.Null.Ajuste.TestPoint(tP).Min(), Params.V.Null.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.V, model == eBOX.ModelosMyeBOX.eBOX_1500 ? true : false));
                triMeasure.Add(new TriAdjustValueDef(Params.V.L12.Ajuste.TestPoint(tP).Name, Params.V.L12.Ajuste.TestPoint(tP).Min(), Params.V.L12.Ajuste.TestPoint(tP).Max(), 0, 0, ParamUnidad.V));

                var result = TestMeasureBase(triMeasure,
                (step) =>
                {
                    var measureAverage = new List<double>();
                    var measure = ebox.ReadVariablesInstantaneas();
                    var measureIII = ebox.ReadVariablesInstantaneasIII();

                    Logger.InfoFormat("Voltage:  L1={0} L2={1} L3={2} LN={3}", measure.L1.Tension, measure.L2.Tension, measure.L3.Tension, measureIII.TensionNeutro);
                    Logger.InfoFormat("Voltage Compound:  L12={0} L23={1} L31={2}", measureIII.TensionLineaL1L2, measureIII.TensionLineaL2L3, measureIII.TensionLineaL3L1);

                    measureAverage.Add(measure.L1.Tension, measure.L2.Tension, measure.L3.Tension);

                    if (model == eBOX.ModelosMyeBOX.eBOX_1500)
                        measureAverage.Add(measureIII.TensionNeutro);

                    measureAverage.Add(measureIII.TensionLineaL1L2, measureIII.TensionLineaL2L3, measureIII.TensionLineaL3L1);
                    return measureAverage.ToArray();

                }, 2, 5, 1050, null, true);

                vars.Voltage.Value = FourLineValue.Create(result[0].L1.Value, result[0].L2.Value, result[0].L3.Value, model == eBOX.ModelosMyeBOX.eBOX_1500 ? result[0].Neutro.Value :0);
                vars.VoltageCompound.Value = TriLineValue.Create(result[1].L1.Value, result[1].L2.Value, result[1].L3.Value);
            }
            else
            {
                if (verification)
                    ebox.WriteTestVerification(circuit);
                else
                    ebox.WriteTestAdjust(circuit);

                SamplerWithCancel((p) =>
                {
                    vars = ebox.ReadVoltageCurrent();

                    Logger.InfoFormat("ReadVoltageCurrent -> Voltage:{0}", vars.Voltage.Value.ToString());

                    Logger.InfoFormat("ReadVoltageCurrent -> Current:{0}", vars.Currents.Value.ToString());

                    return vars.Voltage.Value.LINES.L1 != -0.01 && vars.Currents.Value.LINES.L1 != -0.001;

                }, "Error no se ha podido obtener una medida estable del eBOX", 80, 2000, 2000);

                if (vars.Voltage.Value.LINES.L1 == INESTABLE_MEASURE)
                    throw new Exception("El Ebox notifica que no se puede medir debido a inestabilidad de la señal");

                if (vars.Voltage.Value.LINES.L1 == PHASE_GAP_OUT_OF_MARGINS)
                    throw new Exception("El Ebox notifica de un error en el desfase de la señal aplicada");

                if (!FourLineValue.isNotZero(vars.Currents.Value))
                    throw new Exception("Error en la medida de corriente, el equipo no mide corriente en alguna de las lineas L1, L2, L3, LN");

            }
            return vars;
        }

        private double ReadMeasureCurrentLeak(eBOX.CircuitsCurrent circuit, bool verification = false)
        {
            var leakCurrentMeasure = 0.0D;

            ebox.FlagTest();

            if (verification)
                ebox.WriteTestVerification(circuit);
            else
                ebox.WriteTestAdjust(circuit);

            SamplerWithCancel((p) =>
            {
                leakCurrentMeasure = ebox.ReadCurrentLK();

                return leakCurrentMeasure != -0.001;

            }, "Error no se ha podido obtener una medida estable del MYeBOX", 80, 2000, 2000);

            if (leakCurrentMeasure == INESTABLE_MEASURE)
                throw new Exception("El MYeBOX notifica que no se puede medir debido a inestabilidad de la señal");

            if (leakCurrentMeasure == PHASE_GAP_OUT_OF_MARGINS)
                throw new Exception("El MYeBOX notifica de un error en el desfase de la señal aplicada");

            if (leakCurrentMeasure == 0)
                throw new Exception("Error en la medida de corriente, el equipo no mide corriente en la línea LK");

            return leakCurrentMeasure;
        }

        //************************************************************************************************************
        //************************************************************************************************************

        private void ConfigWifiConfigurationTest()
        {
            var ssid = Configuracion.GetString("SSID_WIFI", "dd-wrt-12319", ParamUnidad.SinUnidad);
            var password = Configuracion.GetString("PASSWORD_WIFI", "dezac2309", ParamUnidad.SinUnidad);
            var setupWifi = new eBOX.SetupWifi();
            setupWifi.SetASSID(ssid);
            setupWifi.SetPasswod(password);
            setupWifi.Connection = 1;
            setupWifi.WPS = 0;
            ebox.WriteSetupWifi(setupWifi);

            Delay(500, "grabando configuración wifi");

            ebox.DisableWifiEmbeddedConnection();
        }

        private void ConfigWifiConfigurationFactory()
        {
            var setupWifi = new eBOX.SetupWifi();
            setupWifi.SetASSID(string.Empty);
            setupWifi.SetPasswod(string.Empty);
            setupWifi.Connection = 0;
            setupWifi.WPS = 0;
            ebox.WriteSetupWifi(setupWifi);
        }

        private void Config3GConfigurationTest()
        {
            var apn = Configuracion.GetString("APN_SIM", "ipfija.vodafone.es", ParamUnidad.SinUnidad);
            var pin = Configuracion.GetString("PIN_SIM", "8691", ParamUnidad.SinUnidad);
            var user = Configuracion.GetString("USER_SIM", "vodafone", ParamUnidad.SinUnidad);
            var password = Configuracion.GetString("PASSWORD_SIM", "vodafone", ParamUnidad.SinUnidad);

            var setup3G = new eBOX.Setup3G();
            setup3G.Connection = 1;
            setup3G.PinEnable = 1;
            setup3G.SetAPNName(apn);
            setup3G.SetAPNUser(user);
            setup3G.SetAPNPasswod(password);
            setup3G.SetPIN(pin);
            ebox.WriteSetup3G(setup3G);

            Delay(500, "grabando configuración 3G");

            //ebox.Disable3GEmbeddedConfiguration();
        }

        private void Config3GConfigurationFactory()
        {
            var setup3G = new eBOX.Setup3G();
            setup3G.Connection = 0;
            setup3G.PinEnable = 0;
            setup3G.SetAPNName(string.Empty);
            setup3G.SetAPNUser(string.Empty);
            setup3G.SetAPNPasswod(string.Empty);
            setup3G.SetPIN("0000");
            ebox.WriteSetup3G(setup3G);
        }

        //************************************************************************************************************
        //************************************************************************************************************

        private Dictionary<eBOX.CircuitsCurrent, FourLineValue> realtionVoltageFromCurrent = new Dictionary<eBOX.CircuitsCurrent, FourLineValue>()
        {
            {   eBOX.CircuitsCurrent._2V,  FourLineValue.Create(5) },
            {   eBOX.CircuitsCurrent._1_28V, FourLineValue.Create(100) },
            {   eBOX.CircuitsCurrent._333mV, FourLineValue.Create(50) },
            {   eBOX.CircuitsCurrent._100mV, FourLineValue.Create(1000) },
            {   eBOX.CircuitsCurrent._10mV, FourLineValue.Create(100)},
            {   eBOX.CircuitsCurrent._1000mV, FourLineValue.Create(10000) },
            {   eBOX.CircuitsCurrent._250mA, FourLineValue.Create(50)},
            {   eBOX.CircuitsCurrent._30mA, FourLineValue.Create(30)},
            {   eBOX.CircuitsCurrent._300mA, FourLineValue.Create(300) },
            {   eBOX.CircuitsCurrent._3000mA, FourLineValue.Create(4) },
        };

        public void TestWriteGain()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.IO.DO.OnWait(4000, 15);

            ebox = AddInstanceVar(new eBOX(5, 1), "UUT");

            SamplerWithCancel((p) => { ebox.FlagTest(); return true; }, "Error de comunicaciones al volver a enceder el equipo", 20, 1000, 500, false, false);

            foreach (var value in Enum.GetValues(typeof(eBOX.CircuitsCurrent)))
            {
                var circuit = (eBOX.CircuitsCurrent)value;

                if (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA || circuit == eBOX.CircuitsCurrent._3000mA)
                {
                    logger.DebugFormat("Write Gain Current LK Default 65000");
                    ebox.WriteGainCurrentLk(circuit, (int)65000);               
                    continue;
                }
                var gainCurrent = new eBOX.FourIntStruct() { Value = FourLineValue.Create(65000, 65000, 65000, 65000) };
                logger.DebugFormat("Write Gain Current ciruit {0} Default {1}", circuit, gainCurrent.Value);
                ebox.WriteGainCurrents(circuit, gainCurrent);
            }


            Delay(2000, "Esrcibimos Numero de Serie");

            foreach (var value in Enum.GetValues(typeof(eBOX.CircuitsCurrent)))
            {
                var circuit = (eBOX.CircuitsCurrent)value;

                ReadMeasureCurrentLeak(circuit, false);

                if (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA || circuit == eBOX.CircuitsCurrent._3000mA)
                {
                    logger.DebugFormat("Write Gain Current LK Default 60000");
                    ebox.WriteGainCurrentLk(circuit, (int)60000);
                    continue;
                }
                var gainCurrent = new eBOX.FourIntStruct() { Value = FourLineValue.Create(60000, 60000, 60000, 60000) };
                logger.DebugFormat("Write Gain Current ciruit {0} Default {1}", circuit, gainCurrent.Value);
                ebox.WriteGainCurrents(circuit, gainCurrent);

                ReadMeasureCurrentLeak(circuit, true);
            }

            ebox.WriteSerialNumber("123456789");;

            Delay(1000, "Esrcibimos Numero de Serie");

            tower.IO.DO.OffWait(4000, 15);

            tower.IO.DO.OnWait(4000, 15);

            foreach (var value in Enum.GetValues(typeof(eBOX.CircuitsCurrent)))
            {
                var circuit = (eBOX.CircuitsCurrent)value;
                if (circuit == eBOX.CircuitsCurrent._30mA || circuit == eBOX.CircuitsCurrent._300mA || circuit == eBOX.CircuitsCurrent._3000mA)
                {               
                    logger.DebugFormat("Write Gain Current circuit:{0}, LK:{0}", circuit, ebox.ReadGainCurrentsLk(circuit));
                    continue;
                }
                var gainCurrent = new eBOX.FourIntStruct() { Value = FourLineValue.Create(65000, 65000, 65000, 65000) };
                logger.DebugFormat("Write Gain Current ciruit:{0} Gain current:{1}", circuit, ebox.ReadGainCurrents(circuit).Value);
            }
        }
    }
}
