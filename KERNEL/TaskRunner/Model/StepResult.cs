﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TaskRunner.Enumerators;

namespace TaskRunner.Model
{
    public class StepResult
    {
        public int ThreadId { get; internal set; }
        public string Name { get { return Step.Name; } }
        public int NumInstance { get; internal set; }
        [JsonIgnore]
        public Step Step { get; internal set; }
        public DateTime StartTime { get; private set; } = DateTime.Now;
        public DateTime EndTime { get; internal set; }
        public TimeSpan Duration { get { return EndTime - StartTime; } }
        public SectionEnum Section { get; internal set; }
        public int LoopIndex { get; internal set; }
        public Exception Exception { get; set; }
        public StepExecutionResult ExecutionResult { get; set; }
        [JsonIgnore]
        private ConcurrentBag<StepResult> childResults = new ConcurrentBag<StepResult>();
        [JsonIgnore]
        public ReadOnlyCollection<StepResult> ChildResults { get { return childResults.ToList().AsReadOnly(); } }
        public void AddChildResult(StepResult stepResult)
        {
            childResults.Add(stepResult);
        }

        [JsonIgnore]
        public StepResult ParentResult { get; set; }
        public string Path
        {
            get
            {
                if (ParentResult != null)
                    return $"{ParentResult.Path} > {Name.Replace(" ", "_")}_#{LoopIndex}";

                return Name.Replace(" ", "_");
            }
        }
        public bool CancelationRequest { get; set; }

        public StepResult(int numInstance, Step step, int threadId, SectionEnum section, StepResult parentResult, int loopIndex)
        {
            NumInstance = numInstance;
            Step = step;
            ThreadId = threadId;
            Section = section;
            ParentResult = parentResult;
            LoopIndex = loopIndex;
            ExecutionResult = StepExecutionResult.Pending;
        }

        public string GetErrorMessage()
        {
            return Exception == null ? "" : Exception.Message;
        }

        public bool CheckCancelationRequest()
        {
            if (CancelationRequest)
                return true;
            else if (ParentResult == null)
                return false;
            else
                return ParentResult.CheckCancelationRequest();
        }

        public IEnumerable<StepResult> Flatten(Func<StepResult, bool> include, Func<StepResult, bool> includeChilds = null)
        {
            List<StepResult> list = new List<StepResult>();
            AddSteps(list, this, include, includeChilds);
            return list;
        }

        public IEnumerable<StepResult> GetSteps(StepExecutionResult executionResult)
        {
            List<StepResult> faultedSteps = new List<StepResult>();
            ChildResults
                .Where(step => step.ExecutionResult == executionResult)
                .ToList()
                .ForEach(faulted => { faultedSteps.Add(faulted); faultedSteps.AddRange(faulted.GetSteps(executionResult)); });
            return faultedSteps;
        }

        public IEnumerable<Exception> GetChildsExceptions()
        {
            IEnumerable<StepResult> faultedSteps = GetSteps(StepExecutionResult.Failed);
            List<Exception> exceptions = new List<Exception>();
            if (Exception != null)
                exceptions.Add(Exception);

            foreach (StepResult step in faultedSteps)
                if (step.Exception != null)
                    exceptions.Add(step.Exception);

            return exceptions;
        }

        private void AddSteps(List<StepResult> list, StepResult step, Func<StepResult, bool> include, Func<StepResult, bool> includeChilds)
        {
            if (step == null)
                return;

            if (include == null || include(step))
                list.Add(step);

            if (includeChilds == null || includeChilds(step))
                foreach (StepResult item in step.ChildResults)
                    AddSteps(list, item, include, includeChilds);
        }
    }
}
