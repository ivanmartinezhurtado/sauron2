﻿using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using Dezac.Tests.Services;
using System.ComponentModel;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.10)]
    public class EDMKTest : TestBase
    {
        private MeterType meter;

        private EDMK edmk;
        public EDMK Edmk
        {
            get
            {
                if (edmk == null)
                    edmk = AddInstanceVar(new EDMK(), "UUT");

                return edmk;
            }
        }

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        private string CAMARA_IDS;

        #region IN&OUT
        private const byte IN_RELAY_1 = 15;
        private const byte IN_RELAY_2 = 16;
        private const byte IN_DEVICE_PRESENCE = 26; //PRUEBA ANCLA 
        private const byte IN_RAIL_FIXED = 32;
        private const byte IN_CONTACTS = 30; //1 - 9
        private const byte IN_CONTACTS_2 = 31;//10 - 15
        private const byte IN_TTL_BLOCK = 29;
        private const byte IN_POSITION_ETHERNET = 28;
        private const byte IN_POSITION_RS485 = 27;
        private const byte IN_RS485 = 24;
        private const byte IN_PISTON_PREPAGO = 35;
        private const byte IN_ETHERNET = 25;
        private const byte IN_DOOR = 34;
        private const byte IN_TAPA = 33;

        private const byte OUT_RAIL_FIXED = 5;
        private const byte OUT_CONTACTS = 6; //1 - 9 Y 10 - 15
        private const byte OUT_TTL_BLOCK = 7;
        private const byte OUT_SWITCH_COMUNICATIONS = 8; //REPOSO ETH ACTIVO 485
        private const byte OUT_SUPPLY_AUX_PISTONS = 9;
        private const byte OUT_LED_ERROR = 51;

        private const byte OUT_DISCONNECT_TTL = 17;
        private const byte INVERT_TTL = 18;
        /******LONWORKS*////
        private const byte MODEL_LONWORKS = 19;
        private const byte DISCONNECT_GROUND_RS485 = 20;

        private const byte OUT_PREPAGO = 21;
        private const byte OUT_PISTON_PREPAGO = 48;
        private const byte OUT_RS485 = 49;
        private const byte OUT_ETHERNET = 50;

        private const byte KEY_ODD = 10;
        private const byte KEY_EVEN = 11;        
        #endregion

        public void TestInitialization()
        {
            byte Periferico = Comunicaciones.Periferico;
            int BaudRate = Comunicaciones.BaudRate;                       
            int SerialPort = Comunicaciones.SerialPort;

            Edmk.SetPort(SerialPort, BaudRate, Periferico);

            Tower.IO.DO.Off(OUT_LED_ERROR);
            Tower.ActiveElectroValvule();
            Tower.Active24VDC();
            Tower.IO.DO.On(OUT_SUPPLY_AUX_PISTONS);

            SamplerWithCancel((p) => { return Tower.IO.DI[IN_DOOR]; }, "Error No se ha detectado la puerta del útil cerrada", 20, 500, 500, false, false);

            Tower.IO.DO.On(OUT_RAIL_FIXED);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_RAIL_FIXED]; }, "Error no se ha detectado la fijacion del carro", 10, 500, 500, false, false);

            Tower.IO.DO.On(OUT_TTL_BLOCK);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_TTL_BLOCK]; }, "Error, no se ha detectado el bloque de comunicaciones TTL", 10, 500, 500, false, false);

            Tower.IO.DO.On(OUT_CONTACTS);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_CONTACTS]; }, "Error, no se ha detectado la bornera(1 - 9)", 10, 500, 500, false, false);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_CONTACTS_2]; }, "Error, no se ha detectado la bornera(10 - 15)", 10, 500, 500, false, false);

            SamplerWithCancel((p) => { return Tower.IO.DI[IN_DEVICE_PRESENCE]; }, "Error No se ha detectado el equipo", 20, 500, 500, false, false);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_TAPA]; }, "Error No se ha detectado la tapa del equipo", 10, 500, 500, false, false);

            if (VectorHardware.GetString("COMUNICACIONES", "NO", ParamUnidad.SinUnidad) == "SI")
            {
                Tower.IO.DO.On(OUT_SWITCH_COMUNICATIONS);
                SamplerWithCancel((p) => { return Tower.IO.DI[IN_POSITION_RS485]; }, "Error, no se ha detectado el piston de comunicaciones en la posicion correcta", 10, 500, 500, false, false);
                Tower.IO.DO.On(OUT_RS485);
                SamplerWithCancel((p) => { return Tower.IO.DI[IN_RS485]; }, "Error, no se ha detectado la conexión de las puntas de comunicación RS485", 10, 500, 500, false, false);
            }

            Tower.ActiveVoltageCircuit();

            meter = new MeterType(Identificacion.MODELO);
        }

        public void TestCommunications()
        {
            SamplerWithCancel((p) => { Edmk.FlagTest(); return true; }, "Error de comunicaciones", 3, 500, 500, false, false);

            var version = Edmk.ReadSoftwareVersion().Trim();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware incorrecta, por favor actualice el equipo"), ParamUnidad.SinUnidad);
        }

        public void TestSetupDefault()
        {
            Edmk.FlagTest();

            Edmk.WriteSetupDefaultCalibration();
            Edmk.WriteRecordSetupCalibration();

            Edmk.WriteSetupDefault();
            Edmk.WriteRecordSetup();

            Edmk.WritePrimaryCurrentRelationDefault();

            Edmk.WritePulseActiveOut(1000);    
            Edmk.WritePulseReactiveOut(1000);
            
            Delay(1000, "Grabando parametros de defecto al equipo");

            var msgToWrite = Configuracion.GetString("MENSAJE_INICIAL", "CIR", ParamUnidad.SinUnidad);
            Edmk.WriteInitialMessage(msgToWrite);

            SamplerWithCancel((s) =>
            {
                return WriteHardwareVector();
            }, "No se ha podido escribir el vector de hardware", 3, 1000, 0, false, false,
            (e) =>
            {
                Error().UUT.FIRMWARE.SETUP(e).Throw();
            });

            Delay(500, "Grabando parametros de defecto al equipo");

            var dateToWrite = new EDMK.Date(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            Edmk.WriteDateManufactured(dateToWrite);
                        
            Edmk.WriteRecordSetup();
            Delay(2000, "Grabando parametros de defecto al equipo");

            Tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(2000, "Reseteando equipo");
            Tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);
            Delay(3000, "Espera proceso interno");

            Edmk.FlagTest();          

            var dateReaded = Edmk.ReadDateManufactured();
            Assert.AreEqual(dateReaded, dateToWrite, Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("Error, la fecha de fabricación no coincide con la grabada"));

            var hardwareVectorToWrite = GetVariable<EDMK.HardwareVector>("VectorHardware");

            var vectorHardware = Edmk.ReadHardwareVector();
            vectorHardware.ToSingleCoil();
            Assert.AreEqual(vectorHardware.VectorHardwareBoolean, hardwareVectorToWrite.VectorHardwareBoolean, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error, el vector de hardware leido no coincide con el grabado"));
        }

        public void TestKeyboard()
        {
            var keyboardOutputInformation = new Dictionary<List<EDMK.Key>, int>()
            {
                {new List<EDMK.Key> { EDMK.Key.RIGHT }, KEY_ODD},
                {new List<EDMK.Key> { EDMK.Key.SETUP, EDMK.Key.SET_A, EDMK.Key.CLEAR, EDMK.Key.UP_DOWN, EDMK.Key.UP }, KEY_EVEN}
            };

            Edmk.FlagTest();

            foreach (KeyValuePair<List<EDMK.Key>, int> keyboard in keyboardOutputInformation)
            {
                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OnWait(150, keyboard.Value);
                    foreach (var key in keyboard.Key)
                    {                       
                        var result = Edmk.ReadKeyboard().HasFlag(key);

                        Resultado.Set(key.ToString(), result == true ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                        if (!result)
                        {
                            Tower.IO.DO.OffWait(150, keyboard.Value);
                            throw new Exception(string.Format("No se ha detectado la tecla {0}", key.ToString()));
                        }
                    }
                    return true;

                }, string.Empty, 3, 200, 200, false, true,
                (e)=> 
                {
                    Error().UUT.HARDWARE.KEYBOARD(e).Throw();
                });
                Tower.IO.DO.OffWait(150, keyboard.Value);
            }

            SamplerWithCancel((p) =>
            {
                var key = Edmk.ReadKeyboard();

                if (key != EDMK.Key.NO_KEY)
                    throw new Exception(string.Format("Se ha detectado la tecla {0} activada cuando no debería detectarse ninguna", key.ToString()));

                return true;
            }, string.Empty, 3, 200, 50, true, true,
            (e) =>
            {
                Error().UUT.HARDWARE.KEYBOARD(e).Throw();
            });
        }

        public void TestDisplay()
        {
            CAMARA_IDS = GetVariable<string>("CAMARA_IDS", CAMARA_IDS);

            Edmk.FlagTest(true);

            EDMK.LedsInformation leds = new EDMK.LedsInformation(EDMK.State.OFF);

            leds.BACKLIGHT = EDMK.State.ON;

            Edmk.WriteLedState(leds);

            var halconWorking = true;
            this.InParellel(
            () =>
            {
                do
                {
                    Edmk.SendDisplayInfo(EDMK.DisplayState.EVEN);
                }
                while (halconWorking);
            },

            () =>
            {
                try
                {
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMARA_IDS, "EDMK_EVEN", "EVEN", "EDMK", 4);
                }
                finally
                {
                    halconWorking = false;
                }
            });

            halconWorking = true;
            this.InParellel(
            () =>
            {
                do
                {
                    Edmk.SendDisplayInfo(EDMK.DisplayState.ODD);
                }
                while (halconWorking);
            },

            () =>
            {
                try
                {
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMARA_IDS, "EDMK_ODD", "ODD", "EDMK", 4);
                }
                finally
                {
                    halconWorking = false;
                }
            });           
        }

        public void TestLeds()
        {
            var timeWait = 400;

            CAMARA_IDS = GetVariable<string>("CAMARA_IDS", CAMARA_IDS);

            Edmk.FlagTest();

            Edmk.WriteLedState(new EDMK.LedsInformation(EDMK.State.ON, EDMK.State.OFF, EDMK.State.OFF));

            Delay(timeWait, "Espera condicionamiento de los leds");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA_IDS, "EDMK_LED_CPU", "LED_CPU", "EDMK");

            Edmk.WriteLedState(new EDMK.LedsInformation(EDMK.State.OFF, EDMK.State.ON, EDMK.State.OFF));

            Delay(timeWait, "Espera condicionamiento de los leds");

            this.TestHalconCaptureAsync( TestHalconExtension.TypeProcedure.FindLeds, CAMARA_IDS, "EDMK_LED_COMM", "LED_COMM", "EDMK");
        }

        public void TestRelays()
        {
            Edmk.FlagTest();
            var vh = Edmk.ReadHardwareVector();
            var deviceOutputInputInformation = new Dictionary<EDMK.Outputs, int>();

            if (vh.Rele1)
                deviceOutputInputInformation.Add(EDMK.Outputs.RELAY1, IN_RELAY_1);
            if (vh.Rele2)
                deviceOutputInputInformation.Add(EDMK.Outputs.RELAY2, IN_RELAY_2);

            foreach (var output in deviceOutputInputInformation)
            {
                SamplerWithCancel((p) =>
                {
                    Edmk.WriteOutputState(output.Key, EDMK.State.ON);

                    Delay(150, "Esperando activacion de la salida");

                    return Tower.IO.DI[output.Value];                                                          
                }, string.Empty, 3, 500, 0, true, true,
                (e)=>
                {
                    Error().UUT.HARDWARE.RELE(output.Key.GetDescription() + "ACTIVADO").Throw();
                });

                SamplerWithCancel((p) =>
                {
                    Edmk.WriteOutputState(output.Key, EDMK.State.OFF);

                    Delay(150, "Esperando desactivacion de la salida");

                    return !this.Tower.IO.DI[output.Value];
                }, string.Empty, 3, 500, 0, true, true,
                (e) =>
                {
                    Error().UUT.HARDWARE.RELE(output.Key.GetDescription() + "DESACTIVADO").Throw();
                });

                Resultado.Set(output.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestAdjustActivePower(EDMK.Escalas scale, int delFirst, int initCount, int samples, int interval)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scale.ToString()).Name, 3, ParamUnidad.A);
            var angle = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint("ACTIVA").Name, 0, ParamUnidad.Grados);
            
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, angle, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);
            
            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_KW.L1.Ajuste.TestPoint(scale.ToString()), ParamUnidad.Puntos));

            var mteVoltage = Tower.PowerSourceIII.ReadVoltage() * Math.Pow(10, 6);
            var mteCurrent = Tower.PowerSourceIII.ReadCurrent();
            var mtePowers = TriLinePower.Create(mteVoltage, mteCurrent, angle);

            var rel = Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);
            var dispersion = (int)Configuracion.GetDouble(Params.KW.Null.Ajuste.TestPoint("DISPERSION_" + scale.ToString()).Name, 1000000, ParamUnidad.SinUnidad);

            var result = Edmk.AdjustActivePower(delFirst, initCount, samples, interval, scale, dispersion, mtePowers, rel,
                (value) =>
                {
                    defs.L1.Value = value.Factors.L1;
                    defs.L2.Value = value.Factors.L2;
                    defs.L3.Value = value.Factors.L3;

                    return !defs.HasMinMaxError();
                },
                (potencias) => (potencias.L1 > 0 && potencias.L2 > 0 && potencias.L3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            var typeGains = scale == EDMK.Escalas.E5A ? EDMK.GainsType.ACTIVE_E5A : EDMK.GainsType.ACTIVE_E1A;
            if (result.Item1)
                Edmk.WriteGains(typeGains, result.Item2);
            else
                Error().UUT.AJUSTE.MARGENES("KW").Throw();

            Edmk.WriteRecordSetupCalibration();
        }

        public void TestAdjustReactivePower(EDMK.Escalas scale, int delFirst, int initCount, int samples, int interval)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scale.ToString()).Name, 3, ParamUnidad.A);
            var angle = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint("REACTIVA").Name, 0, ParamUnidad.Grados);

            var rel = Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);
            var dispersion = (int)Configuracion.GetDouble(Params.KVAR.Null.Ajuste.TestPoint("DISPERSION_" + scale.ToString()).Name, 1000000, ParamUnidad.SinUnidad);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, angle, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_KVAR.L1.Ajuste.TestPoint(scale.ToString()), ParamUnidad.Puntos));

            var mteVoltage = Tower.PowerSourceIII.ReadVoltage() * Math.Pow(10, 6);
            var mteCurrent = Tower.PowerSourceIII.ReadCurrent();
            var mtePowers = TriLinePower.Create(mteVoltage, mteCurrent, angle);

            var result = Edmk.AdjustReactivePower(delFirst, initCount, samples, interval, EDMK.Escalas.E5A, dispersion, mtePowers, rel,
                (value) =>
                {
                    defs.L1.Value = value.Factors.L1;
                    defs.L2.Value = value.Factors.L2;
                    defs.L3.Value = value.Factors.L3;

                    return !defs.HasMinMaxError();
                },
                (potencias) => (potencias.L1 > 0 && potencias.L2 > 0 && potencias.L3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            var typeGains = scale == EDMK.Escalas.E5A ? EDMK.GainsType.REACTIVE_E5A : EDMK.GainsType.REACTIVE_E1A;
            if (result.Item1)
                Edmk.WriteGains(typeGains, result.Item2);
            else
                Error().UUT.AJUSTE.MARGENES("KVAR").Throw();

            Edmk.WriteRecordSetupCalibration();
        }

        public void TestAdjustPhase(EDMK.Escalas scale, int delFirst, int initCount, int samples, int interval)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 3, ParamUnidad.A);
            var angle = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 60, ParamUnidad.Grados);

            var rel = Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);
            var dispersion = (int)Configuracion.GetDouble(Params.PHASE.Null.AjusteDesfase.TestPoint("DISPERSION").Name, 1000000, ParamUnidad.SinUnidad);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, angle, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste, ParamUnidad.Puntos));

            var mteVoltage = Tower.PowerSourceIII.ReadVoltage() * Math.Pow(10, 6);
            var mteCurrent = Tower.PowerSourceIII.ReadCurrent();
            var mtePowers = TriLinePower.Create(mteVoltage, mteCurrent, angle);

            var result = Edmk.AdjustPhase(delFirst, initCount, samples, interval, mtePowers, rel, dispersion,
                (value) =>
                {
                    defs.L1.Value = (short)value.Phase.L1;
                    defs.L2.Value = (short)value.Phase.L2;
                    defs.L3.Value = (short)value.Phase.L3;

                    return !defs.HasMinMaxError();
                },
                (potencias) => (potencias.L1 > 0 && potencias.L2 > 0 && potencias.L3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            if (result.Item1)
                Edmk.WritePhaseGains(result.Item2);
            else
                Error().UUT.AJUSTE.MARGENES("DESFASE").Throw();

            Edmk.WriteRecordSetupCalibration();
        }

        public void TestAdjustOffsetActivePower(EDMK.Escalas scale, int delFirst, int initCount, int samples, int interval)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint("OFFSET_ACTIVA").Name, 110, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint("OFFSET_ACTIVA_" + scale.ToString()).Name, 0.1, ParamUnidad.A);
            var angle = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint("OFFSET_ACTIVA").Name, 0, ParamUnidad.Grados);

            var rel = Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, angle, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_OFFSET.L1.Ajuste.TestPoint("KW_" + scale.ToString()), ParamUnidad.Puntos));

            var mteVoltage = Tower.PowerSourceIII.ReadVoltage();
            var mteCurrent = Tower.PowerSourceIII.ReadCurrent();
            var mtePowers = TriLinePower.Create(mteVoltage, mteCurrent, angle);
            var dispersion = (int)Configuracion.GetDouble(Params.KW.Null.Offset.TestPoint("DISPERSION_" + scale.ToString()).Name, 1000000, ParamUnidad.SinUnidad);

            var result = Edmk.AdjustOffsetActivePower(delFirst, initCount, samples, interval, scale, dispersion, mtePowers, rel,
                (value) =>
                {
                    defs.L1.Value = (short)value.OffsetsFactors.L1;
                    defs.L2.Value = (short)value.OffsetsFactors.L2;
                    defs.L3.Value = (short)value.OffsetsFactors.L3;

                    return !defs.HasMinMaxError();
                },
                (offsets) => (offsets.L1 > 0 && offsets.L2 > 0 && offsets.L3 > 0)); 

            defs.AddToResults(Resultado);

            var typeGains = scale == EDMK.Escalas.E5A ? EDMK.OffsetsType.ACTIVE_E5A : EDMK.OffsetsType.ACTIVE_E1A;
            if (result.Item1)
                Edmk.WriteOffsetsGains(typeGains, result.Item2);
            else
                Error().UUT.AJUSTE.MARGENES("OFFSET").Throw();

            Edmk.WriteRecordSetupCalibration();
        }

        public void TestAdjustOffsetReactivePower(EDMK.Escalas scale, int delFirst, int initCount, int samples, int interval)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint("OFFSET_REACTIVA").Name, 110, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint("OFFSET_REACTIVA_" + scale.ToString()).Name, 1, ParamUnidad.A);
            var angle = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.TestPoint("OFFSET_REACTIVA").Name, 90, ParamUnidad.Grados);

            var rel = Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, angle, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new TriAdjustValueDef(new AdjustValueDef(Params.GAIN_OFFSET.L1.Ajuste.TestPoint("KVAR_" + scale.ToString()), ParamUnidad.Puntos));

            var mteVoltage = Tower.PowerSourceIII.ReadVoltage();
            var mteCurrent = Tower.PowerSourceIII.ReadCurrent();
            var mtePowers = TriLinePower.Create(mteVoltage, mteCurrent, angle);
            var dispersion = (int)Configuracion.GetDouble(Params.KVAR.Null.Offset.TestPoint("DISPERSION_" + scale.ToString()).Name, 1000000, ParamUnidad.SinUnidad);

            var result = Edmk.AdjustOffsetReactivePower(delFirst, initCount, samples, interval, scale,dispersion,  mtePowers, rel,
                (value) =>
                {
                    defs.L1.Value = (short)value.OffsetsFactors.L1;
                    defs.L2.Value = (short)value.OffsetsFactors.L2;
                    defs.L3.Value = (short)value.OffsetsFactors.L3;

                    return !defs.HasMinMaxError();
                },
                (offsets) => (offsets.L1 > 0 && offsets.L2 > 0 && offsets.L3 > 0));  // Aceptamos o descartamos muestra

            defs.AddToResults(Resultado);

            var typeOffsets = scale == EDMK.Escalas.E5A ? EDMK.OffsetsType.REACTIVE_E5A : EDMK.OffsetsType.REACTIVE_E1A;
            if (result.Item1)
                Edmk.WriteOffsetsGains(typeOffsets, result.Item2);
            else
                Error().UUT.AJUSTE.MARGENES("OFFSET").Throw();

            Edmk.WriteRecordSetupCalibration();
        }

        public void TestPoint_Imin([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia, int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            Edmk.WriteEscala(EDMK.Escalas.E1A);
            TestPointCalibration(TestPointsName.TP_IMIN, tipoPotencia, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);

            if (meter.VoltageProperty.IsMultiRangeVoltage)
                TestPointCalibration(TestPointsName.TP_IMIN, tipoPotencia, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime, meter.VoltageProperty.IsMultiRangeVoltage);
        }

        public void TestPoint_IbCosPhi(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IbCOS05, TipoPotencia.Activa, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IbSenPhi(int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            TestPointCalibration(TestPointsName.TP_IbSEN05, TipoPotencia.React, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestPoint_IMax([Description("0=Activa 1=Reactiva")]TipoPotencia tipoPotencia, int NumSamplesToVerification = 3, int NumMaxSamplesToAbort = 6, int SamplingTime = 1100)
        {
            Edmk.WriteEscala(EDMK.Escalas.E5A);
            TestPointCalibration(TestPointsName.TP_IMAX, tipoPotencia, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        public void TestCustomize()
        {
            Edmk.FlagTest();

            Edmk.WriteEscala(EDMK.Escalas.E5A);

            Edmk.WriteDeleteAllEnergies();
            Delay(200, "Borrando energias del equipo");

            var serialNumber = Convert.ToUInt32(TestInfo.NumSerie);
            Edmk.WriteSerialNumber((int)serialNumber);
            Edmk.WriteRecordSetup();
            Delay(1000, "Escribiendo numero de serie");

            Tower.Chroma.ApplyOff();
            Delay(2000, "Reiniciando equipo");
            Tower.Chroma.ApplyAndWaitStabilisation();
            Delay(2000, "Iniciando equipo");

            Edmk.FlagTest();

            foreach (EDMK.EnergiesType energy in Enum.GetValues(typeof(EDMK.EnergiesType)))
                if (!Edmk.ReadEnergies(energy).CheckEnergiesDelete())
                    Error().UUT.MEDIDA.BORRADO(energy.ToString()).Throw();

            var serialNumberReaded = (uint)Edmk.ReadSerialNumber();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, serialNumberReaded.ToString(), serialNumber.ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, el numero de serie leído no coincide con el grabado"), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (Edmk != null)
                Edmk.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();

                if (Tower.IO != null)
                {
                    Tower.IO.DO.OffWait(1000, OUT_ETHERNET, OUT_RS485);

                    Tower.IO.DO.OffWait(1000, OUT_SUPPLY_AUX_PISTONS);

                    Tower.IO.DO.Off(OUT_SWITCH_COMUNICATIONS);

                    SamplerWithCancelWhitOutCancelToken(() => { return Tower.IO.DI[IN_POSITION_ETHERNET] && !Tower.IO.DI[IN_POSITION_RS485]; }, "Error: No se ha detectado el piston de seleccion de COM en posicion RS485",
                    40, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

                    Tower.IO.DO.OffWait(1000, OUT_TTL_BLOCK, OUT_CONTACTS, 14);

                    Tower.IO.DO.OffWait(1000, OUT_RAIL_FIXED);

                    Tower.IO.DO.OffWait(1000, 4);

                }

                Tower.Dispose();

                if (IsTestError)
                    Tower.IO.DO.On(OUT_LED_ERROR);
            }
          
        }

        private bool WriteHardwareVector()
        {
            var hardwareVectorToWrite = GetHardwareVectorConfiguration();
            Edmk.WriteHardwareVector(hardwareVectorToWrite);
            Edmk.WriteRecordSetup();
            Delay(500, "Grabando parametros de defecto al equipo");
            var vectorHardware = Edmk.ReadHardwareVector();
            vectorHardware.ToSingleCoil();

            return vectorHardware.VectorHardwareBoolean == hardwareVectorToWrite.VectorHardwareBoolean;
        }

        private EDMK.HardwareVector GetHardwareVectorConfiguration()
        {
            var hardwareVector = new EDMK.HardwareVector();

            hardwareVector.Comunication = VectorHardware.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rs232 = VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele1 = VectorHardware.GetString("RELE1", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele2 = VectorHardware.GetString("RELE2", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "5A", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "300V", ParamUnidad.SinUnidad);
            hardwareVector.Tarifas = VectorHardware.GetString("TARIFAS", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Bateria = VectorHardware.GetString("BATERIA", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "EDMK", ParamUnidad.SinUnidad);
            
            SetVariable("VectorHardware", hardwareVector);

            return hardwareVector;
        }

        private void TestPointCalibration(TestPointsName testPointName, TipoPotencia tipoPotencia, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200, bool isMultiRangeVoltage = false)
        {           
            TestPoint testPoint = new TestPoint(meter, testPointName, tipoPotencia, isMultiRangeVoltage);

            ConsignarMTE(testPoint);

            internalVerification(testPoint, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime);
        }

        private void internalVerification(TestPoint testPoint, int NumSamplesToVerification = 5, int NumMaxSamplesToAbort = 6, int SamplingTime = 1200)
        {
            var rel = Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);
            var potenciaAparente = testPoint.Voltage * testPoint.Current * rel;
            var potenciaActiva = potenciaAparente * Math.Cos(testPoint.Desfase.ToRadians());
            var potenciaReactiva = potenciaAparente * Math.Sin(testPoint.Desfase.ToRadians());

            var Tolerance = testPoint.Tolerance;
            var Average = testPoint.TipoPotencia == TipoPotencia.Activa ? potenciaActiva : potenciaReactiva;
            var Unidad = testPoint.TipoPotencia == TipoPotencia.Activa ? ParamUnidad.W : ParamUnidad.Var;

            var margen = Margenes.GetDouble(string.Format("PRECISION_{0}", testPoint.Name), Tolerance, ParamUnidad.PorCentage);

            var defs = new TriAdjustValueDef(testPoint.Name + "_L1", 0, 0, Average, margen, Unidad);
            TestCalibracionBase(defs,
            () =>
            {
                var powers = Edmk.ReadInstantPowers();
                var lecturas = new List<double>();

                if (testPoint.TipoPotencia == TipoPotencia.Activa)
                {
                    lecturas.Add((double)powers.ActivePowers.L1);
                    lecturas.Add((double)powers.ActivePowers.L2);
                    lecturas.Add((double)powers.ActivePowers.L3);
                    logger.InfoFormat("Potencia Activa EDMK L1: {0}", powers.ActivePowers.L1);
                    logger.InfoFormat("Potencia Activa EDMK L2: {0}", powers.ActivePowers.L2);
                    logger.InfoFormat("Potencia Activa EDMK L3: {0}", powers.ActivePowers.L3);
                }
                else
                {
                    lecturas.Add((double)powers.ReactivePowers.L1);
                    lecturas.Add((double)powers.ReactivePowers.L2);
                    lecturas.Add((double)powers.ReactivePowers.L3);
                    logger.InfoFormat("Potencia Reactiva EDMK L1: {0}", powers.ReactivePowers.L1);
                    logger.InfoFormat("Potencia Reactiva EDMK L2: {0}", powers.ReactivePowers.L2);
                    logger.InfoFormat("Potencia Reactiva EDMK L3: {0}", powers.ReactivePowers.L3);
                }
                return lecturas.ToArray();
            },
            () =>
            {
                var lecturas = new List<double>();
                var voltageRef = tower.PowerSourceIII.ReadVoltage();
                var currentRef = tower.PowerSourceIII.ReadCurrent() * rel;
                var power = TriLinePower.Create(voltageRef, currentRef, testPoint.Desfase);

                if (testPoint.TipoPotencia == TipoPotencia.React)
                {
                    lecturas.AddRange(power.KVAR.ToArray()); 
                    logger.InfoFormat("Potencia Reactiva MTE  {0}", power.KVAR.ToString());
                }
                else
                {
                    lecturas.AddRange(power.KW.ToArray());
                    logger.InfoFormat("Potencia Activa MTE  {0}", power.KW.ToString());
                }
                return lecturas.ToArray();
            }, NumSamplesToVerification, NumMaxSamplesToAbort, SamplingTime,
            string.Format("Error calculado en la calibración en {0} fuera de margenes", testPoint.Name));

            logger.InfoFormat("L1 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L1.Value, defs.L1.Error, defs.L1.Tolerance);
            logger.InfoFormat("L2 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L2.Value, defs.L2.Error, defs.L2.Tolerance);
            logger.InfoFormat("L3 -> Valor= {0}  Error= {1} Tolerance= {2}", defs.L3.Value, defs.L3.Error, defs.L3.Tolerance);

            var ErrorTri = Math.Round((defs.L1.Error + defs.L2.Error + defs.L3.Error) / 3, 2);
            var defsError = new AdjustValueDef(testPoint.Name + "_III_ERROR", ErrorTri, -1 * Tolerance, Tolerance, 0, 0, ParamUnidad.PorCentage);
            Resultado.Set(defsError.Name, defsError.Value, ParamUnidad.PorCentage, -1 * Tolerance, Tolerance);

            Assert.IsTrue(defsError.IsValid() == true, Error().UUT.CONFIGURACION.MARGENES(string.Format("Error calculado trifasico en la calibración en {0} fuera de margenes", defsError.Name)));
        }

        private void ConsignarMTE(TestPoint testPoint)
        { 
            var current = testPoint.Current / Configuracion.GetDouble("RELATION_TRAFO", 1, ParamUnidad.SinUnidad);
            if (testPoint.testPointName == TestPointsName.TP_IMIN)
                current = 0.05;
            
            var config = new PowerSourceIIIStabilizationExtension.PowerSourceIIIConfiguration()
            {
                V_L1 = testPoint.Voltage,
                V_L2 = testPoint.Voltage,
                V_L3 = testPoint.Voltage,
                I_L1 = current,
                I_L2 = current,
                I_L3 = current,
                PF_L1 = testPoint.Desfase,
                PF_L2 = testPoint.Desfase,
                PF_L3 = testPoint.Desfase,
                Gap_L1 = 0,
                Gap_L2 = 120,
                Gap_L3 = 240,
                Frequency = testPoint.Frequency,
                InternalStabilization = false,
                Samples = 4,
                MaxSamples = 20,
                Timeout = 1100,
                TimeSamples = 1100,
                TestPoint = testPoint.Name,
                Tolerance = testPoint.Tolerance,
                VoltageTolerance = testPoint.Tolerance,
                VoltageMaxVariance = 100,
                CurrentTolerance = testPoint.Tolerance,
                CurrentMaxVariance = 100,
                PFDegreeTolerance = 1,
                PFDegreeMaxVariance = 100,
                FrequencyMaxVaraince = testPoint.Tolerance,
                FrequencyTolerance = 100,
                GapDegreeMaxVariance = 1,
                GapDegreeTolerence = 100,
                UseWatimetro = false,
            };

            this.PowerSourceIIIStabilization(tower, config);
        }
    }
}
