﻿using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace Instruments.Router
{
    public class RouterDLink615 : Router
    {
        private string cookie;
        private XElement xElement;

        public override string UserName { get { return "Admin"; } }

        public RouterDLink615()
            : base()
        {
            UserName = "Admin";
            Password = "dezac2309";

            cookie = null;
        }

        public override RouterConfig ReadConfig()
        {
            var config = new RouterConfig();

           var xml = GetLanXML();
        
           var modules = xElement.Elements("module").ToList();
          
            foreach (var module in modules)
            {
                var nameServie = module.Elements("service").FirstOrDefault().Value;
                switch (nameServie)
                {
                    case "DEVICE.HOSTNAME":
                        config.RouterName = module.Elements("device").FirstOrDefault().Value;
                        break;
                    case "INET.LAN-1":
                        config.LanIP = module.Elements("inet").Elements("entry").Elements("ipv4").Elements("ipaddr").FirstOrDefault().Value;
                        break;
                    case "DHCPS4.LAN-1":
                        config.StartDHCP = Convert.ToInt32(module.Elements("dhcps4").Elements("entry").Elements("start").FirstOrDefault().Value);
                        config.Domain = module.Elements("dhcps4").Elements("entry").Elements("domain").FirstOrDefault().Value;
                        config.LanIP = module.Elements("inet").Elements("entry").Elements("ipv4").Elements("ipaddr").FirstOrDefault().Value;
                        break;
                }
            }      
           
            return config;
        }

        public override RouterConfig ApplyConfig(RouterConfig config)
        {
            var currentConfig = ReadConfig();
            var ChangeIP = false;
            var modules = xElement.Descendants("module").ToList();
            foreach(var module in modules)
            {
                var nameServie = module.Elements("service").FirstOrDefault().Value;
                switch (nameServie)
                {
                    case "DEVICE.HOSTNAME":
                        bool changeModuleHost = string.IsNullOrEmpty(config.RouterName) ? false : currentConfig.RouterName != config.RouterName;
                        if (changeModuleHost)
                            module.Elements("device").FirstOrDefault().Value = config.RouterName;
                        else
                        {
                            module.Add(XElement.Parse("<FATLADY>ignore</FATLADY>"));
                            module.Add(XElement.Parse("<SETCFG>ignore</SETCFG>"));
                            module.Add(XElement.Parse("<ACTIVATE>ignore</ACTIVATE>"));
                        }
                        break;

                    case "INET.LAN-1":
                        bool changeModuleHINET = string.IsNullOrEmpty(config.LanIP) ? false : currentConfig.LanIP != config.LanIP;
                        if (changeModuleHINET)
                        {
                            string maskIP = config.LanMaskIP == "255.255.255.0" ? "24" : config.LanMaskIP == "255.255.0.0" ? "16" : "8"; 
                            module.Elements("inet").Elements("entry").Elements("ipv4").Elements("ipaddr").FirstOrDefault().Value = config.LanIP;
                            module.Elements("inet").Elements("entry").Elements("ipv4").Elements("mask").FirstOrDefault().Value = maskIP;
                            ChangeIP = true;
                        }
                        else
                        {
                            module.Add(XElement.Parse("<FATLADY>ignore</FATLADY>"));
                            module.Add(XElement.Parse("<SETCFG>ignore</SETCFG>"));
                            module.Add(XElement.Parse("<ACTIVATE>ignore</ACTIVATE>"));
                        }
                        break;

                    case "DHCPS4.LAN-1":
                        bool changemoduleDHCPS4 = (config.StartDHCP != currentConfig.StartDHCP);
                        if (changemoduleDHCPS4)
                        {
                            string ip = string.IsNullOrEmpty(config.LanIP) ? currentConfig.LanIP : config.LanIP;
                            string start = config.StartDHCP.ToString();
                            string end = (config.StartDHCP + 200).ToString();
                            if (Convert.ToInt32(end) > 255)
                                end ="250";
                           
                            string gatewayIP = string.IsNullOrEmpty(config.LanGatewayIP) ? currentConfig.LanGatewayIP : config.LanGatewayIP;

                            module.Elements("dhcps4").Elements("entry").Elements("start").FirstOrDefault().Value = start;
                            module.Elements("dhcps4").Elements("entry").Elements("end").FirstOrDefault().Value = end;
                            module.Elements("dhcps4").Elements("entry").Elements("domain").FirstOrDefault().Value = currentConfig.Domain;
                            module.Elements("inet").Elements("entry").Elements("ipv4").Elements("ipaddr").FirstOrDefault().Value = config.LanIP;
                        }
                        else
                        {
                            module.Add(XElement.Parse("<FATLADY>ignore</FATLADY>"));
                            module.Add(XElement.Parse("<SETCFG>ignore</SETCFG>"));
                            module.Add(XElement.Parse("<ACTIVATE>ignore</ACTIVATE>"));
                        }
                        break;
                    default:
                        module.Add(XElement.Parse("<FATLADY>ignore</FATLADY>"));
                        module.Add(XElement.Parse("<SETCFG>ignore</SETCFG>"));
                        module.Add(XElement.Parse("<ACTIVATE>ignore</ACTIVATE>"));
                        break;
                }
            }

            using (var wc = GetWebRequest(true))
            {
                wc.Headers.Add(HttpRequestHeader.Referer, string.Format("http://{0}/bsc_lan.php", RouterIP));
                wc.Headers.Add("Origin", string.Format("http://{0}", RouterIP));
                wc.Headers[HttpRequestHeader.ContentType] = "text/xml";

                var url = string.Format("http://{0}/hedwig.cgi", RouterIP);
                var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xElement.ToString();

                var result = wc.UploadString(url, content);
                if (result.IndexOf("<result>OK</result>") < 0)
                    throw new Exception("No se ha podido enviar la configuración al router");
            }

            using (var wc = GetWebRequest(true))
            {
                wc.Headers.Add(HttpRequestHeader.Referer, string.Format("http://{0}/bsc_lan.php", RouterIP));
                wc.Headers.Add("Origin", string.Format("http://{0}", RouterIP));

                var url = string.Format("http://{0}/pigwidgeon.cgi", RouterIP);
                var content = "ACTIONS=SETCFG%2CSAVE%2CACTIVATE";

                var result = wc.UploadString(url, content);
                if (result.IndexOf("<result>OK</result>") < 0)
                    throw new Exception("No se ha podido grabar y activar la configuración al router");
            }

            if (ChangeIP)
            {
                using (var wc = GetWebRequest(true))
                {
                    wc.Headers.Add(HttpRequestHeader.Referer, string.Format("http://{0}/bsc_lan.php", RouterIP));
                    wc.Headers.Add("Origin", string.Format("http://{0}", RouterIP));

                    var url = string.Format("http://{0}/service.cgi", RouterIP);
                    var content = "EVENT=REBOOT";

                    var result = wc.UploadString(url, content);
                    if (result.IndexOf("<result>OK</result>") < 0)
                        throw new Exception("No se ha podido grabar y activar la configuración al router");
                }

                Thread.Sleep(170000);
                var newIP = NetUtils.GetDefaultGateway();
                RouterIP = string.IsNullOrEmpty(config.LanIP) ? currentConfig.LanIP : config.LanIP;
            }

            return ReadConfig();
        }

        public override RouterConfigWifi ReadConfigWifi()
        {
            var config = new RouterConfigWifi();

            var xml = GetLanXML(true);

            var modules = xElement.Elements("module").ToList();
            foreach (var module in modules)
            {
                var nameServie = module.Elements("service").FirstOrDefault().Value;
                switch (nameServie)
                {
                    case "WIFI.PHYINF":
                        config.SSID = module.Elements("wifi").Elements("entry").Elements("ssid").FirstOrDefault().Value;
                        config.Enabled = module.Elements("phyinf").Elements("active").FirstOrDefault().Value == "1" ? true : false;
                        break;
                 
                }
            }

            return config;
        }

        public override RouterConfigWifi ApplyConfigWifi(RouterConfigWifi config)
        {
            var currentConfig = ReadConfigWifi();

            var modules = xElement.Descendants("module").ToList();
            foreach (var module in modules)
            {
                var nameServie = module.Elements("service").FirstOrDefault().Value;
                switch (nameServie)
                {
                    case "WIFI.PHYINF":
                        if (config.SSID != currentConfig.SSID || config.Enabled != currentConfig.Enabled)
                        {
                            module.Elements("wifi").Elements("entry").Elements("ssid").FirstOrDefault().Value = config.SSID;
                            module.Elements("phyinf").Elements("active").FirstOrDefault().Value = config.Enabled ? "1" : "0";
                        }else
                        {
                            module.Add(XElement.Parse("<FATLADY>ignore</FATLADY>"));
                            module.Add(XElement.Parse("<SETCFG>ignore</SETCFG>"));
                            module.Add(XElement.Parse("<ACTIVATE>ignore</ACTIVATE>"));
                        }
                        break;
                    default:
                        module.Add(XElement.Parse("<FATLADY>ignore</FATLADY>"));
                        module.Add(XElement.Parse("<SETCFG>ignore</SETCFG>"));
                        module.Add(XElement.Parse("<ACTIVATE>ignore</ACTIVATE>"));
                        break;
                }
            }

            using (var wc = GetWebRequest(true))
            {
                wc.Headers.Add(HttpRequestHeader.Referer, string.Format("http://{0}/bsc_wlan.php", RouterIP));
                wc.Headers.Add("Origin", string.Format("http://{0}", RouterIP));
                wc.Headers[HttpRequestHeader.ContentType] = "text/xml";

                var url = string.Format("http://{0}/hedwig.cgi", RouterIP);
                var content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xElement.ToString();

                var result = wc.UploadString(url, content);
                if (result.IndexOf("<result>OK</result>") < 0)
                    throw new Exception("No se ha podido enviar la configuración al router");
            }

            using (var wc = GetWebRequest(true))
            {
                wc.Headers.Add(HttpRequestHeader.Referer, string.Format("http://{0}/bsc_wlan.php", RouterIP));
                wc.Headers.Add("Origin", string.Format("http://{0}", RouterIP));

                var url = string.Format("http://{0}/pigwidgeon.cgi", RouterIP);
                var content = "ACTIONS=SETCFG%2CSAVE%2CACTIVATE";

                var result = wc.UploadString(url, content);
                if (result.IndexOf("<result>OK</result>") < 0)
                    throw new Exception("No se ha podido grabar y activar la configuración al router");
            }

            return ReadConfigWifi();
        }

        private void Login()
        {
            cookie = null;
            string challenge = null;

            using (var wc = GetWebRequest(true))
            {
                string url = string.Format("http://{0}/authentication.cgi?captcha=&dummy={1}", RouterIP, GetTime());

                string result = wc.DownloadString(url);

                int p = result.IndexOf("\"uid\": \"");
                if (p >= 0)
                {
                    p += 8;
                    int pf = result.IndexOf("\"", p);
                    cookie = "uid=" + result.Substring(p, pf - p);

                    p = result.IndexOf("\"challenge\": \"");
                    p += 14;
                    pf = result.IndexOf("\"", p);
                    challenge = UserName + result.Substring(p, pf - p);
                }
            }

            var hmacData = Encoding.UTF8.GetBytes(challenge);
            var key = Encoding.UTF8.GetBytes(Password);

            var hmac = new HMACMD5(key);
            var hashBytes = hmac.ComputeHash(hmacData);
            var digest = BitConverter.ToString(hashBytes).Replace("-", "");

            using (var wc = GetWebRequest(true))
            { 
                var url = string.Format("http://{0}/authentication.cgi", RouterIP);
                string data = string.Format("id={0}&password={1}", UserName, digest);

                var result = wc.UploadString(url, data);
            }
        }

        public XElement GetLanXML(bool wifi= false)
        {
            xElement = null;
            Login();

            using (var wc = GetWebRequest(true))
            {
                string url = string.Format("http://{0}/getcfg.php", RouterIP);
                string data = wifi == false ? "SERVICES=DEVICE.HOSTNAME%2CINET.LAN-1%2CDHCPS4.LAN-1%2CDHCPS4.LAN-2%2CRUNTIME.INF.LAN-1%2CURLCTRL%2CWAN" : "SERVICES=WIFI.PHYINF%2CPHYINF.WIFI%2CDEVICE.LAYOUT";

                string refer = wifi == false ? "bsc_lan.php" : "bsc_wlan.php";
                wc.Headers.Add(HttpRequestHeader.Referer, string.Format("http://{0}/{1}", RouterIP, refer));
                wc.Headers.Add("Origin", string.Format("http://{0}/{1}", RouterIP, refer));

                string result = wc.UploadString(url, data);

                xElement = XElement.Parse(result);

                return xElement;
            }
        }

        private string GetTime()
        {
            return Convert.ToInt64(DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds).ToString();
        }

        public override List<ClientEntry> GetConnectedClientsFromWeb(bool excludeMe, bool PRS600 = true)
        {
            var list = new List<ClientEntry>();

            var xml = GetLanXML();

            var modules = xElement.Elements("module").ToList();

            foreach (var module in modules)
            {
                var nameServie = module.Elements("service").FirstOrDefault().Value;
                switch (nameServie)
                {
                    case "RUNTIME.INF.LAN-1":
                        var dhcpClients = module.Elements("runtime").Elements("inf").Elements("dhcps4").Elements("leases").Elements("entry").ToList();
                        foreach (var clients in dhcpClients)
                            if (clients.Element("hostname") != null)
                            {
                                var item = new ClientEntry();
                                item.Name = clients.Element("hostname").Value;
                                item.IP = clients.Element("ipaddr").Value;
                                item.MAC = clients.Element("macaddr").Value;
                                item.Expire = clients.Element("expire").Value;

                                var exists = list.Find(p => p.MAC == item.MAC);
                                if (exists != null && string.Compare(exists.Expire, item.Expire) > 0)
                                {
                                    list.Remove(exists);
                                    list.Add(item);
                                }
                                else if (exists == null)
                                    list.Add(item);
                            }

                        break;
                }
            }

            if (excludeMe)
                list = list.Where(p => !p.Name.Contains("PC-")).ToList();

            if (PRS600)
                list = list.Where(p => !p.Name.Contains("PRS600")).ToList();

            return list;
        }

        public override void DeleteDHCPEntry(string ip, string mac)
        {
            //string data = string.Format("ccp_act=revokeHost&ip={0}&mac={1}", ip, mac);

            //string url = string.Format("http://{0}/get_set.ccp", RouterIP);

            //var wc = GetWebRequest(true);
            //wc.UploadString(url, data);
        }

        private WebClient GetWebRequest(bool post = false)
        {
            var wc = new WebClient();

            wc.Headers.Add(HttpRequestHeader.Accept, "*/*");
            wc.Headers.Add(HttpRequestHeader.AcceptLanguage, "es-ES,es;q=0.8");
            wc.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
            wc.Headers.Add(HttpRequestHeader.Pragma, "no-cache");
            wc.Headers.Add(HttpRequestHeader.UserAgent, "User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36");

            if (cookie != null)
                wc.Headers.Add(HttpRequestHeader.Cookie, cookie);

            if (post)
                wc.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");

            return wc;
        }

        public override void Disposed()
        {
            throw new NotImplementedException();
        }
    }
}
