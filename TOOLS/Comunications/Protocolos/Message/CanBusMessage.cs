﻿using System;
using System.Globalization;

namespace Comunications.Message
{
    public class CanBusMessage : HexStringMessage
    {
        private static CultureInfo enUS = new CultureInfo("en-US");

        public static CanBusMessage Create(string format, params object[] args)
        {
            CanBusMessage message = new CanBusMessage { Format = format };

            message.SetArgs(enUS, args);

            return message;
        }

        public override void ValidateResponse(ITransportMessage response)
        {          
            if (response.MessageFrame[0] == 170)
                return;

            throw new Exception("Error validacion trama CanBus");
        }
    }
}
