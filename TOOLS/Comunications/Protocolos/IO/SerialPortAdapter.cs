﻿using System;
using System.Diagnostics;
using System.IO.Ports;

namespace Comunications.Message
{
    public class SerialPortAdapter : IStreamResource
    {
        private SerialPort _serialPort;

        public SerialPortAdapter(SerialPort serialPort)
        {
            _serialPort = serialPort;

            Debug.Assert(serialPort != null, "Argument serialPort cannot be null.");
            if (serialPort.WriteTimeout == InfiniteTimeout)
                serialPort.WriteTimeout = 10000;
        }

        public SerialPortAdapter(SerialPort serialPort, string NewLine)
        {
            _serialPort = serialPort;

            Debug.Assert(NewLine != null, "Argument NewLine cannot be null.");
            _serialPort.NewLine = NewLine;

            Debug.Assert(serialPort != null, "Argument serialPort cannot be null.");
            _serialPort = serialPort;
            if (serialPort.WriteTimeout == InfiniteTimeout)
                serialPort.WriteTimeout = 10000;
        }

        public int?  WaitTimeOpenPort { get; set; }

        public SerialPort SerialPort
        {
            get { return _serialPort; }
        }

        public string NewLine
        {
            get { return _serialPort.NewLine; }
            set { _serialPort.NewLine = value; }
        }

        public int InfiniteTimeout
        {
            get { return SerialPort.InfiniteTimeout; }
        }

        public int ReadBufferSize
        {
            get { return _serialPort.ReadBufferSize; }
            set
            {
                _serialPort.ReadBufferSize = value;
            }
        }

        public int ReadTimeout
        {
            get { return _serialPort.ReadTimeout; }
            set
            {
                _serialPort.ReadTimeout = value;
                _serialPort.WriteTimeout = value;
            }
        }

        public int WriteTimeout
        {
            get { return _serialPort.WriteTimeout; }
            set { _serialPort.WriteTimeout = value; }
        }

        public int BaudRate
        {
            get { return _serialPort.BaudRate; }
            set { _serialPort.BaudRate = value; }
        }

        public void DiscardInBuffer()
        {
            if (_serialPort.IsOpen) 
                _serialPort.DiscardInBuffer();
        }

        public bool OpenPort()
        {
            bool isOpen = _serialPort.IsOpen;

            if (!isOpen)
                _serialPort.Open();

            if (WaitTimeOpenPort.HasValue)
                System.Threading.Thread.Sleep(WaitTimeOpenPort.Value);

            return isOpen;
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            bool isOpen = OpenPort();
            int readed = 0;
            try
            {
                readed = _serialPort.Read(buffer, offset, count);
            }
            catch(Exception e)
            {
                if (e is TimeoutException)
                    throw new TimeoutException(string.Format("Error de comunicaciones por TimeOut, no hay respuesta por el puerto serie {0} ", _serialPort.PortName));
            }
            finally
            {
                if (!isOpen)
                    _serialPort.Close();
            }

            return readed;
        }

        public string ReadLineString()
        {
            bool isOpen = OpenPort();
            string readed = "";
            try
            {
                readed =  _serialPort.ReadLine();
            }
            catch (Exception e)
            {
                if (e is TimeoutException)
                    throw new TimeoutException(string.Format("Error de comunicaciones por TimeOut, no hay respuesta por el puerto serie {0} ", _serialPort.PortName));
            }
            finally
            {
                if (!isOpen)
                    _serialPort.Close();
            }
            return readed;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            bool isOpen = OpenPort();
            _serialPort.Write(buffer, offset, count);
        }

        public void HandleMessageException(Exception ex)
        {

        }

        public void Dispose()
        {
            if (_serialPort != null)
            {
                if (_serialPort.IsOpen)
                {
                    _serialPort.Close();
                    System.Threading.Thread.Sleep(300);
                }
                _serialPort.Dispose();
            }

            _serialPort = null;

            GC.SuppressFinalize(this);
        }

        public string PortName
        {
            get
            {
                return _serialPort.PortName;
            }
            set
            {
                _serialPort.PortName=value;
            }
        }

        public void WriteLineString(string tx)
        {
            bool isOpen = OpenPort();

            _serialPort.WriteLine(tx);
        }

        public void WriteLineStringAsBytes(string tx, int byteInterval = 50, bool writeCR = true, bool writeLF = false)
        {
            bool isOpen = OpenPort();

            var data = System.Text.UTF8Encoding.UTF8.GetBytes(tx);

            foreach (var b in data)
            {
                _serialPort.Write(new byte[] { b }, 0, 1);
                System.Threading.Thread.Sleep(byteInterval);
            }

            if (writeCR)
            {
                System.Threading.Thread.Sleep(byteInterval);
                _serialPort.Write(new byte[] { 0x0d }, 0, 1);
            }

            if (writeLF)
            {
                System.Threading.Thread.Sleep(byteInterval);
                _serialPort.Write(new byte[] { 0x0a }, 0, 1);
            }
        }
    }
}
