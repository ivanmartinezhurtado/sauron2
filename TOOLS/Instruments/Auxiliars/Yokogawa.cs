﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using Ivi.Visa.Interop;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Instruments.Auxiliars
{
    [InstrumentsVersion(1.00)]
    public class Yokogawa : ISignalGenerator, IDisposable, IConsoleDevice
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Yokogawa));
        protected MessageTransport transport;
        protected IProtocol protocol;

        private CultureInfo enUS = new CultureInfo("en-US");

        private FormattedIO488 _Gpib;
        private ResourceManager _mgr;
        private byte _adrr = 4;
        private bool _IsOpen = false;

        public Yokogawa()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinRx = "\r\n", CaracterFinTx = "\r\n" };
            transport = new MessageTransport(protocol, new GpibPortAdapter(_Gpib), logger);
            _mgr = new ResourceManager();
        }

        public byte portAddress
        {
            get { return _adrr; }
            set
            {
                Close();
                _adrr = value;
            }
        }

        private void InitializeComponent()
        {
            if (_Gpib == null)
                _Gpib = new FormattedIO488();
        }

        private void Close()
        {
            if (_IsOpen && _Gpib.IO != null)
                _Gpib.IO.Close();

            _IsOpen = false;
        }

        private void Open()
        {
            if (_IsOpen && _Gpib.IO != null)
                _Gpib.IO.Close();

            _Gpib.IO = (IMessage)_mgr.Open("GPIB0::" + _adrr.ToString(), AccessMode.NO_LOCK, 2000, "");
            _Gpib.IO.TerminationCharacterEnabled = true;
            _IsOpen = true;
        }

        private bool IsOpen()
        {
            return _Gpib != null && _Gpib.IO != null;
        }

        public string GetIdentification()
        {
            var resp = WriteCommand("*IDN?").Value.Split(',');
            return "Yokowaga";
        }

        public ISignalGenerator On(int channel)
        {
            return SetOutput(State.ON);
        }

        public ISignalGenerator Off(int channel)
        {
            return SetOutput(State.OFF);
        }

        public ISignalGenerator SetSignal(int channel, SignalFunction function, double frequency, double amplitude, double voltageOffset, double phase, bool apply = false)
        {
            return SetSignal(new SignalInfo { Channel = channel, Function = function, Frequency = frequency, Amplitude = amplitude, VoltageOffset = voltageOffset, Phase = phase }, apply);
        }

        public ISignalGenerator SetSignal(int channel, SignalFunction function, double frequency, double minVoltage, double maxVoltage, double voltageOffset, double phase, bool apply = false)
        {
            return SetSignal(new SignalInfo { Channel = channel, Function = function, Frequency = frequency, MinVoltage = minVoltage, MaxVoltage = maxVoltage, VoltageOffset = voltageOffset, Phase = phase }, apply);
        }

        public ISignalGenerator SetSignal(SignalInfo info, bool apply = false)
        {
            try
            {
                Initialize();
            }catch
            {
                Initialize(); //Parche reintento ..
            }
         
            SelectChannel((Channels)info.Channel);
           
            SetWaveform((WaveformFunction)info.Function);

            if (info.Frequency.HasValue)
                SetFreq(info.Frequency.Value);

            if (info.Amplitude.HasValue)
                SetAmplitude((double)info.Amplitude.Value);

            if (info.MinVoltage != null && info.MaxVoltage != null)
            {
                SetLowLevelVoltage((double)info.MinVoltage);
                SetHighLevelVoltage((double)info.MaxVoltage);
            }

            SetOutputMode(OutputMode.CONTINOUS);

            if (info.VoltageOffset != null)
                SetVoltageOffset((double)info.VoltageOffset, DCVoltageUnits.V);

            SetPhaseAngle((double)info.Phase);

            On(info.Channel);

            return this;
        }

        public Yokogawa SelectChannel(Channels channel)
        {
            string argument = channel == Channels.CHANNEL_1 ? "1" : "2";
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_CHANNEL_SELECT, true, argument));
            return this;
        }

        public Yokogawa SetOutput(State state)
        {
            string argument = state == State.ON ? "ON" : "OFF";
            try
            {
                InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_CHANNEL, true, argument));
            }
            catch (Exception ex)
            {
                if (ex.Message == "Error de comando. El comando especificado no se encuentra definido")
                {
                    InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_CHANNEL, true, argument));
                    return this;
                }

                throw;
            }
            return this;
        }

        public Yokogawa SetAmplitude(double amplitude, ACVoltageUnits unit)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_AMPLITUDE, true, amplitude, unit.ToString()));
            return this;
        }

        public Yokogawa SetAmplitude(double amplitude)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_AMPLITUDE, true, amplitude, ACVoltageUnits.Vrms));
            return this;
        }

        public Yokogawa SetFreq(double frequency, FrequencyUnits unit)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_FREQUENCY, true, frequency, unit.ToString()));
            return this;
        }

        public Yokogawa SetFreq(double frequency)
        {
            FrequencyUnits frequencyUnits = FrequencyUnits.Hz;

            while (frequency > 1000)
            {
                frequency /= 1000;
                frequencyUnits++;
                if (!Enum.IsDefined(typeof(FrequencyUnits), frequencyUnits))
                    throw new ArgumentOutOfRangeException("La frequencia requerida esta fuera de los márgenes de trabajo del equipo");
            }

            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_FREQUENCY, true, frequency, frequencyUnits.ToString()));
            return this;
        }

        public Yokogawa SetWaveform(WaveformFunction wave)
        {
            if (!Enum.IsDefined(typeof(WaveformFunction), wave))
                throw new NotImplementedException("La forma de onda requerida no esta implementada en el instrumento actual");

            string argument = string.Empty;

            switch (wave)
            {
                case WaveformFunction.PULSE:
                    argument = "PLS";
                    break;
                case WaveformFunction.RAMP:
                    argument = "RAMP";
                    break;
                case WaveformFunction.SINUSOIDAL:
                    argument = "SIN";
                    break;
                case WaveformFunction.SQUARE:
                    argument = "SQR";
                    break;
                case WaveformFunction.TRIANGULAR:
                    argument = "TRI";
                    break;
            }

            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_WAVEFORM, true, argument));
            return this;
        }

        public Yokogawa SetOutputMode(OutputMode output)
        {
            string argument = string.Empty;

            switch (output)
            {
                case OutputMode.CONTINOUS:
                    argument = "CONT";
                    break;

                case OutputMode.DC:
                    argument = "DC";
                    break;

                case OutputMode.GATE:
                    argument = "GATE";
                    break;

                case OutputMode.TRIGGER:
                    argument = "TRIG";
                    break;
            }

            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_MODE, true, argument));
            return this;
        }

        public Yokogawa SetLowLevelVoltage(double voltageLevel, DCVoltageUnits unit)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_LOW_LEVEL, true, voltageLevel, unit.ToString()));
            return this;
        }

        public Yokogawa SetLowLevelVoltage(double voltageLevel)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_LOW_LEVEL, true, voltageLevel, DCVoltageUnits.V));
            return this;
        }

        public Yokogawa SetHighLevelVoltage(double voltageLevel, DCVoltageUnits unit)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_HIGH_LEVEL, true, voltageLevel, unit.ToString()));
            return this;
        }

        public Yokogawa SetHighLevelVoltage(double voltageLevel)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_HIGH_LEVEL, true, voltageLevel, DCVoltageUnits.V));
            return this;
        }

        public Yokogawa SetPhaseAngle(double phase)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_PHASE_ANGLE, true, phase.ToString()));
            return this;
        }

        public Yokogawa SetVoltageOffset(double offset, DCVoltageUnits unit)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OFFSET, true, offset.ToString(), unit.ToString()));
            return this;
        }

        //***********************************************

        public Yokogawa SetVoltageRange(OutputRange range)
        {
            var argument = range == OutputRange._1V ? "1V" : "10V";
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE_RANGE, true, argument));
            return this;
        }

        public Yokogawa SetTriggerPolarity(TriggerLogic trigPolarity)
        {
            var arguments = trigPolarity == TriggerLogic.POSITIVE ? "POS" : "NEG";
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_TRIGGER_POLARITY, true, arguments));
            return this;
        }

        public Yokogawa SetSeparateMode(State state)
        {
            var argument = state == State.ON ? "ON" : "OFF";
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_SEPARATE_MODE, true, argument));
            return this;
        }

        public Yokogawa SetGateStatus(State state)
        {
            string argument = state == State.ON ? "ON" : "OFF";
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_GATE_STATUS, true, argument));
            return this;
        }

        public Yokogawa SetDCVoltage(double voltage, DCVoltageUnits unit)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_OUTPUT_VOLTAGE_DC_MODE, true, voltage, unit.ToString()));
            return this;
        }

        public Yokogawa SetNumberOfBursts(int numberOfBursts)
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.SET_NUMBER_OF_BURSTS, true, numberOfBursts.ToString()));
            return this;
        }

        public Yokogawa FireTrigger()
        {
            InternalSendCommand<PrivateCommands>(new Command<PrivateCommands>(PrivateCommands.GENERATE_MANUAL_TRIGGER, true));
            return this;
        }

        public Yokogawa Initialize()
        {
            WriteCommand("INIT");
            return this;
        }

        //***********************************************

        private Result<string> InternalSendCommand<T>(Command<T> command)
        {
            CheckCommandSupported(command.Commands);

            var temporalPrivateCommand = (PrivateCommands)command.Commands;

            var privateCommand = commandDictionary[temporalPrivateCommand];

            int count = CountFormatStringParameters(privateCommand);

            if (count != command.Args.Length)
                throw new Exception(string.Format("El numero de parametros introducido no es suficiente para el comando {0}, requiere {1} parametros pero se introducen {2}", temporalPrivateCommand, count, command.Args.Length));

            var response = WriteCommand(StringMessage.Create(enUS, privateCommand, command.Args).Text);

            return response;
        }

        public Result<string> WriteCommand(string text)
        {
            try
            {
                if (!IsOpen())
                    Open();

                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                response = transport.SendMessage<SCPIMessage>(new SCPIMessage { Text = "ERR?" });
                int result = Convert.ToInt32(response.Text);
                if (result > 0)
                {
                    if (Enum.IsDefined(typeof(ErrorCodes), result))
                        throw new Exception(errorCodesDictionary[result]);
                    throw new Exception(string.Format("El yokogawa informa de un error no especificado. Codigo de error: {0}", result));
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            Close();
            _Gpib = null;
            _mgr = null;
            transport.Dispose();
        }

        private int CountFormatStringParameters(string formatString)
        {
            return Regex.Matches(Regex.Replace(formatString,
                @"(\{{2}|\}{2})", ""), // removes escaped curly brackets
                @"\{(\d+)(?:\:?[^}]*)\}").OfType<Match>()
                .SelectMany(match => match.Groups.OfType<Group>().Skip(1))
                .Select(index => Int32.Parse(index.Value))
                .Max() + 1;
        }

        private void CheckCommandSupported(int command)
        {
            if (!Enum.IsDefined(typeof(PrivateCommands), command))
                throw new NotSupportedException("Error. El comando pedido no esta soportado por el equipo");

            if (!commandDictionary.ContainsKey((PrivateCommands)command))
                throw new NotSupportedException("Error. El comando pedido no esta soportado por el equipo");
        }

        private Dictionary<PrivateCommands, string> commandDictionary = new Dictionary<PrivateCommands, string>()
        {
            {PrivateCommands.GENERATE_MANUAL_TRIGGER,"TRIG"},
            {PrivateCommands.SET_CHANNEL_SELECT, "CHAN {0}"},
            {PrivateCommands.SET_GATE_STATUS, "GATE {0}"},
            {PrivateCommands.SET_HIGH_LEVEL, "HLEV {0}{1}"},
            {PrivateCommands.SET_LOW_LEVEL, "LLEV {0}{1}"},
            {PrivateCommands.SET_NUMBER_OF_BURSTS, "BURS {0}"},
            {PrivateCommands.SET_OFFSET, "OFFS {0}{1}"},
            {PrivateCommands.SET_OUTPUT_AMPLITUDE, "AMPL {0}{1}"},
            {PrivateCommands.SET_OUTPUT_CHANNEL, "OUTP {0}"},
            {PrivateCommands.SET_OUTPUT_FREQUENCY, "FREQ {0}{1}"},
            {PrivateCommands.SET_OUTPUT_MODE, "MODE {0}"},
            {PrivateCommands.SET_OUTPUT_PERIOD, "PER {0}"},
            {PrivateCommands.SET_OUTPUT_VOLTAGE_DC_MODE, "DC {0}{1}"},
            {PrivateCommands.SET_OUTPUT_WAVEFORM, "FUNC {0}"},
            {PrivateCommands.SET_PHASE_ANGLE, "PHAS {0}"},
            {PrivateCommands.SET_SEPARATE_MODE, "SEP {0}"},
            {PrivateCommands.SET_TRIGGER_POLARITY, "SLOP {0}"},
            {PrivateCommands.SET_TTL_LEVEL_OUTPUT, "TTL {0}"},
            {PrivateCommands.SET_VOLTAGE_RANGE, "RANG {0}"},
            {PrivateCommands.SET_WAVE_DUTY, "DUTY {0}"},
        };

        private Dictionary<int, string> errorCodesDictionary = new Dictionary<int, string>()
        {
            {113, "Error de comando. El comando especificado no se encuentra definido"},
            {120, "Error de comando. El valor del parametro es incorrecto"},
            {140, "Error de comando. El caracter del parametro es incorrecto"},
            {221, "Error de ejecución. Imposible de ejecutar el comando en el estado actual, compruebe las restricciones del modo de trabajo"},
            {222, "Error de ejecución. Parametro fuera de márgenes, compruebe el rango válido"},
            {224, "Error de ejecución. Comando no disponible en el FG110"},
            {501, "Error de equipo. La memoria ROM no funciona correctamente, requerido servicio técnico"},
            {502, "Error de equipo. La memoria RAM no funciona correctamente, requerido servicio técnico"},
            {503, "Error de equipo. El ventilador de refrigeración se ha parado, apague el generador de funciones yokogawa. Si tras encenderlo el problema persiste, se requiere de servicio técnico"},
        };

        public enum PrivateCommands
        {
            SET_CHANNEL_SELECT,
            SET_OUTPUT_MODE,
            /// <summary>Determina la logica para la señal de trigger, solo se puede usar si el generador de funciones se encuentra en modo de trigger</summary>
            SET_TRIGGER_POLARITY,
            /// <summary>Produce una señal de trigger, solo es útil si el generador de funciones se encuentra en modo TRIGGER</summary>
            GENERATE_MANUAL_TRIGGER,
            /// <summary>Configura manualmente el estado de la puerta para el canal actual y devuelve su estado, solo se puede usar si el generador de funciones se encuentra en modo de GATE o TRIGGER</summary>
            SET_GATE_STATUS,
            /// <summary>Determina el numero de pulsos o ciclos que se producen tras la señal de trigger, solo se puede usar si el generador de funciones se encuentra en modo de TRIGGER</summary>
            SET_NUMBER_OF_BURSTS,
            SET_OUTPUT_CHANNEL,
            /// <summary>Configura la forma de onda para la salida actual y si la onda se invierte o no (Esto último no funciona cuando el modo de salida es DC)</summary>
            SET_OUTPUT_WAVEFORM,
            /// <summary>Configura la frequencia para la salida actual, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_OUTPUT_FREQUENCY,
            /// <summary>Configura el período para la salida actual, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_OUTPUT_PERIOD,
            SET_SEPARATE_MODE,
            /// <summary>Configura el duty cycle de la señal, solo se puede usar si la señal de salida es de tipo PULSE y el modo del generador no es DC</summary>
            SET_WAVE_DUTY,
            /// <summary>Configura el rango de voltajes para la salida</summary>
            SET_VOLTAGE_RANGE,
            /// <summary>Determina la amplitud de la forma de onda, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_OUTPUT_AMPLITUDE,
            /// <summary>Configura el valor de voltaje de offset para la salida actual, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_OFFSET,
            /// <summary>Configura el valor de voltaje para la salida actual cuando se encuentra en estado alto, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_HIGH_LEVEL,
            /// <summary>Configura el valor de voltaje para la salida actual cuando se encuentra en estado bajo, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_LOW_LEVEL,
            /// <summary>Configura los valores de voltaje para los estaods bajo y alto a logica TTL (0 - 5V), no se puede usar si el generador se encuentra en modo DC</summary>
            SET_TTL_LEVEL_OUTPUT,
            /// <summary>Fuerza una tensión de salida para el canal actual, si se encuentra en cualquier otro modo que no sea DC, cambia a éste</summary>
            SET_OUTPUT_VOLTAGE_DC_MODE,
            /// <summary>Configura el desfase para el canal actual, no se puede usar si el generador se encuentra en modo DC</summary>
            SET_PHASE_ANGLE,
        }

        public enum ErrorCodes
        {
            NOT_DEFINED = 113,
            PARAMETER_VALUE_INCORRECT = 120,
            PARAMETER_CHARACTER_INCORRECT = 140,
            NOT_POSSIBLE_TO_EXECUTE = 221,
            PARAMETER_OUT_OF_RANGE = 222,
            COMMAND_NOT_AVAILABLE_FG110 = 224,
            ROM_ERROR = 501,
            RAM_ERROR = 502,
            COOLING_FAN_MALFUNCTION = 503
        }

        public enum TriggerLogic
        {
            POSITIVE,
            NEGATIVE
        }

        public enum OutputMode
        {
            CONTINOUS,
            TRIGGER,
            GATE,
            DC
        }

        public enum WaveformFunction
        {
            SINUSOIDAL,
            SQUARE,
            TRIANGULAR,
            RAMP,
            PULSE
        }

        public enum State
        {
            ON,
            OFF
        }

        public enum Terminator
        {
            CRLF,
            LF
        }

        public enum Channels
        {
            CHANNEL_1,
            CHANNEL_2
        }

        public enum OutputRange
        {
            _1V,
            _10V
        }

        public enum DCVoltageUnits
        {
            mV,
            V,
        }

        public enum ACVoltageUnits
        {
            mV,
            V,
            mVrms,
            Vrms
        }

        public enum FrequencyUnits
        {
            mHz, Hz, kHz, MHz,
        }
    }
}
