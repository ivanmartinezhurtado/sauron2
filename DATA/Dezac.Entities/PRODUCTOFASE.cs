namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PRODUCTOFASE
    {
        public PRODUCTOFASE()
        {
            this.BIEN = new HashSet<BIEN>();
        }
    
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public int SECUENCIA { get; set; }
        public string IDFASE { get; set; }
        public Nullable<decimal> TIEMPOTEORICO { get; set; }
        public Nullable<decimal> TIEMPOSTANDARD { get; set; }
        public string NOTAS { get; set; }
        public Nullable<decimal> TIEMPOPREPARACION { get; set; }
        public Nullable<int> OPERARIOS { get; set; }
        public string FASETEST { get; set; }
        public Nullable<decimal> TACKTIME { get; set; }
    
        public virtual FASE FASE { get; set; }
        public virtual PRODUCTO PRODUCTO { get; set; }
        public virtual ICollection<BIEN> BIEN { get; set; }
    }
}
