﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.Drawing;
using Instruments.Actuators;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(LAC25HomingDescription))]
    

    public class LAC25Homing : ActionBase
    { 
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");

            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            tower.Actuator_LAC25.SetPIDConstants(SMAC_LAC25.AxisActuator.LINEAL, 39, 25, 2970, -285, 10, 8000);
            tower.Actuator_LAC25.SetPIDConstants(SMAC_LAC25.AxisActuator.ROTATIVO, 11, 1, 222, 0, 10, 5000);

            tower.Actuator_LAC25.WriteAndRunHomingLineal(false,true);
            tower.Actuator_LAC25.WriteAndRunHomingRotative(false, true);

            tower.Actuator_LAC25.MotorOff();
        }
    }

    public class LAC25HomingDescription : ActionDescription
    {
        public override string Name { get { return "LAC25 Homing"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Transport_Engine_icon; } }
        public override string Description { get { return "Funcion para programar el Moros LAC25 en su posicion inicial"; } }
    }
}

