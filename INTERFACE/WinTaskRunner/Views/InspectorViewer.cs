﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions;
using Dezac.Tests.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using TaskRunner.Model;
using TaskRunner.Tools;
using WinTaskRunner.Services;
using Dezac.Tests.Actions.Kernel;

namespace WinTaskRunner.Views
{
    public partial class InspectorViewer : UserControl
    {
        protected readonly AppService app;
        protected readonly AssemblyResolverService assemblyResolver;
        protected readonly IInstanceResolveService instanceResolveService;
        protected readonly IShell shell;

        private SynchronizationContext sc;
        private SequenceContext lastContext;

        private BindingList<InspectorItem> Items { get; set; }

        private List<Type> ListTypes { get; set; }

        private Type type;

        private object target;

        private object Target
        {
            get
            {
                if (target == null)
                    target = Activator.CreateInstance(type);

                return target;
            }
            set
            {
                ClearInstance();
                target = value;
            }
        }

        private bool running;

        private StringBuilder sbLog = new StringBuilder();

        public InspectorViewer(AppService appService
          , AssemblyResolverService assemblyResolver
          , InstanceResolveService instanceResolveService
          , IShell shell)
          : this()
        {
            app = appService;
            this.assemblyResolver = assemblyResolver;
            this.shell = shell;
            this.instanceResolveService = instanceResolveService;
            sc = SynchronizationContext.Current;

            app.Runner.TestStart += (s, e) =>
            {
                lastContext = SequenceContext.Current;
            };

            radButtonClose.Visible = true;
            radButtonClose.Enabled = false;
            radButtonLoad.Visible = true;
            radButtonLoad.Enabled = false;
            label1.Visible = true;
            label2.Visible = true;
            comboBoxDevice.Visible = true;
            comboBoxDevice.Enabled = false;
            comboBoxDivision.Visible = true;
        }


        public InspectorViewer()
        {
            InitializeComponent();

            Items = new BindingList<InspectorItem>();

            gridMethods.EnableCustomSorting = true;
            gridMethods.CustomSorting += GridMethods_CustomSorting;

            UpdateUI();
        }

        public void LoadType(Type type)
        {
            ClearInstance();

            this.type = type;

            Items.Clear();

            IOrderedEnumerable<MethodInfo> methods = type
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => !m.IsSpecialName && !m.DeclaringType.FullName.StartsWith("System.") && m.DeclaringType.Name != "TestBase" && m.Name != "Dispose")
                .OrderBy(p => p.Name.StartsWith("SetPort") ? string.Empty : p.Name);

            foreach (MethodInfo method in methods)
            {
                var item = new InspectorItem
                {
                    Method = method,
                    Name = method.Name
                };

                method.GetParameters()
                    .ToList()
                    .ForEach(p =>
                    {
                        var value = new RunMethodAction.Argument { Name = p.Name, Type = p.ParameterType.FullName };

                        var description = p.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
                        if (description != null)
                            value.Description = description.Description;

                        if (p.DefaultValue != null && p.HasDefaultValue)
                            value.Value = p.ParameterType.IsEnum ? ((int)p.DefaultValue).ToString() : p.DefaultValue.ToString();

                        item.Arguments.Add(value);
                    });

                item.UpdateArgs();

                if (item.Name == "SetPort")
                {
                    item.Selected = true;
                    item.Orden = 1;
                }
                else if (item.Name == "Dispose")
                {
                    item.Selected = true;
                    item.Orden = 10000;
                }

                item.PropertyChanged += OnItemPropertyChanged;

                Items.Add(item);
            }

            bsList.DataSource = Items;
            gridMethods.DataSource = bsList;
            gridMethods.Refresh();
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Selected")
                return;

            var item = sender as InspectorItem;
            if (item == null || !item.Selected || item.Orden < 9999)
                return;

            item.Orden = Items.Where(p => p.Name != "Dispose" && p.Selected).Max(p => p.Orden) + 1;
        }

        private InspectorItem Current
        {
            get { return bsList.Current as InspectorItem; }
        }

        private void Start()
        {
            sbLog.Clear();
            Items
                .Where(p => p.Selected)
                .OrderBy(p => p.Orden.GetValueOrDefault(Int32.MaxValue))
                .ToList()
                .ForEach(p => sbLog.AppendFormat("{0};", p.Name));

            sbLog.AppendLine();
            UpdateUI();

            Run();
            UpdateUI();
        }

        private void Stop()
        {
            timer.Enabled = false;
            UpdateUI();
            ClearInstance();
        }

        private void ClearInstance()
        {
            if (target == null)
                return;

            try
            {
                if (target is IDisposable)
                    ((IDisposable)Target).Dispose();
            }
            catch { }

            target = null;
        }

        private void UpdateUI()
        {
            if (IsDisposed)
                return;

            btStart.Text = timer.Enabled ? "Stop" : "Start";
            if (btStart.Text == "Start")
                btStart.Image = Properties.Resources.Play_icon__1_;
            else
                btStart.Image = Properties.Resources.Stop_Normal_Red_icon__1_;

            InspectorItem current = Current;
            gridResults.DataSource = current != null && current.Result != null ? current.Result.Properties : null;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!running)
                RunAsync();
        }

        private async void RunAsync()
        {
            await Task.Run(() => Run());
        }

        private void Run()
        {
            running = true;

            Items
                .Where(p => p.Selected)
                .OrderBy(p => p.Orden.GetValueOrDefault(Int32.MaxValue))
                .ToList()
                .ForEach(p => RunItem(p));

            sbLog.AppendLine();

            running = false;
        }

        private void RunItem(InspectorItem item)
        {
            try
            {
                object value = CallUtils.InvokeMethod(Target, item.Name, item.Arguments.Select(p => VariablesTools.ResolveValue(p)).ToArray());
                item.Result = new InspectorResultValue(value);
                item.Error = false;
            }
            catch (Exception ex)
            {
                item.Error = true;
                if (ex.InnerException != null)
                    item.Result = new InspectorResultValue(string.Format("ERROR: {0}", ex.InnerException.Message));
                else
                    item.Result = new InspectorResultValue(string.Format("ERROR: {0}", ex.Message));
            }

            if (item.Error == true)
                return;

            if (true || item.Plot)
            {
                if (item.Result.Properties != null)
                    foreach (NameValuePair<object> kvp in item.Result.Properties)
                        DataPointHub.TryAdd(this, kvp.Name, kvp.Value, type.Name);
                else
                    DataPointHub.TryAdd(this, item.Name, item.Result.Value, type.Name);
            }

            UpdateUI();
        }


        private void bsList_CurrentChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void GridMethods_CustomSorting(object sender, Telerik.WinControls.UI.GridViewCustomSortingEventArgs e)
        {
            int v1 = (int)(e.Row1.Cells["SOrden"].Value ?? 9999);
            int v2 = (int)(e.Row2.Cells["SOrden"].Value ?? 9999);

            if (v1 > v2)
                e.SortResult = 1;
            else if (v1 < v2)
                e.SortResult = -1;
            else
                e.SortResult = 0;
        }

        private void gridMethods_CommandCellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            Telerik.WinControls.UI.GridViewColumn column = e.Column;
            var item = e.Row.DataBoundItem as InspectorItem;

            if (item == null)
                return;

            if (column.Name != "Send")
            {
                if (item.Args != null)
                {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(item)["Arguments"];
                    var editor = (UITypeEditor)pd.GetEditor(typeof(UITypeEditor));
                    var serviceProvider = new RuntimeServiceProvider();
                    editor.EditValue(serviceProvider, serviceProvider, item.Arguments);
                    item.UpdateArgs();
                }
            }
            else
                RunItem(item);
        }

        private void gridMethods_RowFormatting(object sender, Telerik.WinControls.UI.RowFormattingEventArgs e)
        {
            var item = e.RowElement.RowInfo.DataBoundItem as InspectorItem;
            if (item != null)
                e.RowElement.BackColor = item.Error ? Color.Red : Color.Empty;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Stop();

            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        private void RadButtonLoad_Click(object sender, EventArgs e)
        {
            object device = comboBoxDevice.SelectedItem;
            Type type = ListTypes.Where(p => p.Name == device.ToString()).FirstOrDefault();
            radButtonClose.Enabled = true;
            LoadType(type);
        }

        private void ComboBoxDivision_SelectedValueChanged(object sender, EventArgs e)
        {
            string division = ((ComboBox)sender).SelectedItem as string;
            string library = "";
            switch (division)
            {
                case "MEDIDA":
                    library = "Dezac.Device.Measure.dll";
                    break;
                case "PROTECCION":
                    library = "Dezac.Device.Protection.dll";
                    break;
                case "METERING":
                    library = "Dezac.Device.Metering.dll";
                    break;
                case "REACTIVA":
                    library = "Dezac.Device.Reactive.dll";
                    break;
                case "MADEL":
                    library = "Dezac.Device.ThermalStations.dll";
                    break;
                case "RENOVABLES":
                    library = "Dezac.Device.Renewable.dll";
                    break;
                case "DEVICE GENERATED":
                    library = "Devcie.Generated.";
                    break;
                case "EXAMINAR ...":
                    loadDeviceLibraryExaminar();
                    library = "Devcie.Generated.";
                    break;
            }

            if (library.StartsWith("Devcie.Generated."))
            {
                comboBoxDevice.Items.Clear();
                IEnumerable<string> files = Directory.EnumerateFiles(Application.StartupPath, "Device.Generated.*", SearchOption.TopDirectoryOnly);
                foreach (string file in files)
                {
                    var assembly = Assembly.LoadFrom(file);

                    IEnumerable<Type> ListTypesTmp = from t in assembly.GetTypes()
                                                     where t.IsClass && t.IsPublic && !t.IsAbstract
                                                     select t;

                    if (ListTypes == null)
                        ListTypes = ListTypesTmp.ToList();
                    else
                        ListTypes.AddRange(ListTypesTmp.ToList());

                    foreach (Type type in ListTypesTmp)
                        comboBoxDevice.Items.Add(type.Name);
                }

                if (comboBoxDevice.Items != null)
                    comboBoxDevice.Enabled = true;
            }
            else
            {
                Assembly assembly = assemblyResolver.Resolve(app.Config.PathCurrentAsssembly, library);
                if (assembly != null)
                {
                    IEnumerable<Type> ListTypesTmp = from t in assembly.GetTypes()
                                                     where t.IsClass && t.IsPublic && !t.IsAbstract
                                                     select t;

                    comboBoxDevice.Items.Clear();

                    ListTypes = ListTypesTmp.ToList();

                    foreach (Type type in ListTypes)
                        comboBoxDevice.Items.Add(type.Name);

                    if (comboBoxDevice.Items != null)
                        comboBoxDevice.Enabled = true;
                }
            }
        }

        private void loadDeviceLibraryExaminar()
        {
            var ofd = new OpenFileDialog();

            ofd.ValidateNames = false;
            ofd.CheckFileExists = false;
            ofd.Filter = "Device.Generated.* (*.dll)|*.dll";
            ofd.FilterIndex = 1;
            ofd.DefaultExt = ".dll";
            ofd.FileName = "Device.Generated.*";
            string currentPath = ConfigurationManager.AppSettings["PathDeviceLibrary"];

            if (!string.IsNullOrEmpty(currentPath))
                ofd.InitialDirectory = currentPath;

            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                string pathName = Path.GetFullPath(ofd.FileName);
                string destFileName = string.Format(@"{0}\{1}", Application.StartupPath, ofd.SafeFileName);
                File.Copy(pathName, destFileName, true);
                Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                if (ex.InnerException != null)
                    message = ex.InnerException.Message;

                MessageBox.Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ComboBoxDevice_SelectedValueChanged(object sender, EventArgs e)
        {
            radButtonLoad.Enabled = true;
        }

        private void BtStart_Click_1(object sender, EventArgs e)
        {
            if (btStart.Text == "Stop")
            {
                Stop();
                timer.Enabled = false;
            }
            else
            {
                Start();
                timer.Interval = Convert.ToInt32(nInterval.Value);
                timer.Enabled = chkRepetir.Checked;
            }
        }

        private void RadButtonClose_Click(object sender, EventArgs e)
        {
            gridMethods.DataSource = null;
            gridMethods.Rows.Clear();
            comboBoxDevice.Items.Clear();
            ClearInstance();
        }

        public object ResolveValue(RunMethodAction.Argument arg)
        {
            if (!string.IsNullOrEmpty(arg.Value) && arg.IsJSON)
            {
                Type type = arg.ResolveType();

                object value = Activator.CreateInstance(type);

                JsonConvert.DeserializeObject<List<NameValuePair>>(arg.Value)
                    .ForEach(p =>
                    {
                        string argValue = p.Value;

                        if (string.IsNullOrEmpty(p.Name))
                            value = argValue;
                        else
                        {
                            string[] paths = p.Name.Split('.');
                            object current = value;

                            FieldInfo field = null;
                            PropertyInfo prop = null;

                            for (int i = 0; i < paths.Length; i++)
                            {
                                bool last = i == paths.Length - 1;

                                field = current.GetType().GetField(paths[i]);
                                if (field != null)
                                {
                                    if (last)
                                        field.SetValue(current, Convert.ChangeType(argValue, field.FieldType));
                                    else
                                        current = field.GetValue(current);
                                }
                                else
                                {
                                    prop = current.GetType().GetProperty(paths[i]);
                                    if (prop != null)
                                    {
                                        if (last)
                                            prop.SetValue(current, Convert.ChangeType(argValue, prop.PropertyType));
                                        else
                                            current = prop.GetValue(current);
                                    }
                                }
                            }
                        }
                    });

                return value;
            }

            return arg.Value;
        }

    }

    internal class InspectorItem : INotifyPropertyChanged
    {
        private InspectorResultValue result;
        private bool selected { get; set; }
        private int? orden { get; set; }
        private string args { get; set; }
        private bool plot { get; set; }

        public MethodInfo Method { get; set; }

        public InspectorItem()
        {
            Arguments = new RunMethodAction.ArgumentsList();
        }

        public bool Selected
        {
            get { return selected; }
            set
            {
                selected = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Selected"));
            }
        }

        public string Name { get; set; }

        public string Value
        {
            get
            {
                if (Result != null)
                    return Result.Text;
                else
                    return null;
            }
        }

        public InspectorResultValue Result
        {
            get { return result; }
            set { result = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Result")); }
        }

        public int? Orden
        {
            get { return orden; }
            set { orden = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Orden")); }
        }

        public bool Plot
        {
            get { return plot; }
            set
            {
                plot = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Plot"));
            }
        }

        public string Args
        {
            get { return args; }
        }

        public bool Error { get; set; }

        public RunMethodAction.ArgumentsList Arguments { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void UpdateArgs()
        {
            foreach (RunMethodAction.Argument item in Arguments)
            {
                if (args != null)
                    args += ", ";

                args += string.Format("{0}={1}", item.Name, item.Value);
            }
        }
    }

    internal class InspectorResultValue
    {
        public InspectorResultValue(object value)
        {
            Value = value;

            if (Value != null)
            {
                if (value is string || Value.GetType().IsPrimitive)
                {
                    Text = value.ToString();
                    FormattedText = Text;
                    var list = new List<NameValuePair<object>>();
                    list.Add(new NameValuePair<object>() { Name = "Value", Value = Text });
                    Properties = list;
                }
                else
                {
                    Text = JsonConvert.SerializeObject(Value);
                    FormattedText = JsonConvert.SerializeObject(Value, Formatting.Indented);
                    Properties = JsonHelper.DeserializeAndFlattenList(Text);
                }
            }
        }

        public object Value { get; private set; }

        public string Text { get; private set; }

        public string FormattedText { get; private set; }

        public List<NameValuePair<object>> Properties { get; private set; }

        public override string ToString()
        {
            return Text;
        }
    }

    internal class RuntimeServiceProvider : IServiceProvider, ITypeDescriptorContext
    {
        #region IServiceProvider Members

        object IServiceProvider.GetService(Type serviceType)
        {
            if (serviceType == typeof(IWindowsFormsEditorService))
                return new WindowsFormsEditorService();

            return null;
        }

        class WindowsFormsEditorService : IWindowsFormsEditorService
        {
            #region IWindowsFormsEditorService Members

            public void DropDownControl(Control control)
            {
            }

            public void CloseDropDown()
            {
            }

            public System.Windows.Forms.DialogResult ShowDialog(Form dialog)
            {
                return dialog.ShowDialog();
            }

            #endregion
        }

        #endregion

        #region ITypeDescriptorContext Members

        public void OnComponentChanged()
        {
        }

        public IContainer Container
        {
            get { return null; }
        }

        public bool OnComponentChanging()
        {
            return true; // true to keep changes, otherwise false
        }

        public object Instance
        {
            get { return null; }
        }

        public PropertyDescriptor PropertyDescriptor
        {
            get { return null; }
        }

        #endregion
    }
}
