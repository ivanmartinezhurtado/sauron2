﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(LambdaPowerSourceActivationActionDescription))]
    [Browsable(false)]

    public class LambdaPowerSourceActivation : ActionBase
    {
        [DefaultValue(null)]
        [Description("Puerto Serie")]
        public string SerialPort { get; set; }

        [DefaultValue("12")]
        [Description("La tensión de consigna para la fuente LAMBDA")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Voltage { get; set; }

        [DefaultValue("0.5")]
        [Description("Limitador de corriente a la salida Lambda, un valor de 0 deshabilita esta función")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string CurrentLimit { get; set; }

        [DefaultValue(false)]
        [Description("No Wait stabilization")]
        public bool NoWaitStabilization { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            if (!string.IsNullOrEmpty(SerialPort))
            {
                var portDevice = Convert.ToByte(GetVariable<string>(SerialPort, SerialPort).Replace("COM", ""));   
                tower.PortLambda = portDevice;
                tower.LAMBDA.PortCom = portDevice;
            }           

            if (!NoWaitStabilization)
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(ResolveVariable(Voltage), ResolveVariable(CurrentLimit));
            else
                tower.LAMBDA.ApplyPresets(ResolveVariable(Voltage), ResolveVariable(CurrentLimit));
        }

        private double ResolveVariable(string variableName)
        {
            return Convert.ToDouble(VariablesTools.ResolveValue(variableName).ToString().Replace(".", ","));
        }
    }

    public class LambdaPowerSourceActivationActionDescription : ActionDescription
    {
        public override string Name { get { return "ActivateLambda"; } }
        public override string Description { get { return "Enciende la fuente lambda de la torre con las consignas especificadas"; } }
        public override string Category { get { return "Hardware/Tower/PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;PowerSourcesShutdown:2;TowerDispose:2"; } }
    }
}
