﻿namespace TaskRunner.Tools
{
    public class NameValuePair : NameValuePair<string>
    {
    }

    public class NameValuePair<T>
    {
        public string Name { get; set; }
        public T Value { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public static NameValuePair<T> Create(string name, T value)
        {
            NameValuePair<T> NamedValue = new NameValuePair<T>()
            {
                Name = name,
                Value = value
            };

            return NamedValue;
        }
    }

}
