﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device.Metering;
using Dezac.Device.Metering.SOAP;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Router;
using Instruments.Towers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.25)]
    public class CDCTestBase : TestBase
    {
        private COMPACT_DC compacdc;
        private Tower3 tower3;
        private CirwattDlms sbt;
        private int TiempoEspera3G;
        private DateTime TiempoInicialConfig3G;

        private const int OUT_ALARM_IN_1_3 = 20;
        private const int OUT_ALARM_IN_2_4 = 21;

        internal DateTime dateTimeLocalRestart { get; set; }
        internal List<CompactDctSoapService.ConfigurationModules> modulesCheck { get; set; }
        internal string CAMERA_CPU_PSU { get; set; }
        internal string CAMERA_SBT { get; set; }
        internal string CAMERA_AUX { get; set; }

        internal int InstanceNumber { get; set; }
        internal bool DoubleSecondary { get; set; }
        internal bool onlySBT { get; set; }
        internal string METER_PLC { get; set; }
        internal byte SBT_NUMBER { get; set; }
        internal string has3G { get; set; }
        internal int PULSADOR_RESET { get; set; }
        internal string ZonaHoraria { get; set; }

        internal UtilEnum Util
        {
            get
            {
                return (UtilEnum)Model.NumUtilBien;
            }
        }

        internal Dictionary<int, string> PositionCameraDictionary;

        [JsonIgnore]
        internal COMPACT_DC cdc
        {
            get
            {
                if (compacdc == null)
                {

                    compacdc = AddInstanceVar(new COMPACT_DC(Identificacion.VERSION_STG), "UUT");
                    Resultado.Set("VERSION_DEVICE", string.Format("{0}: {1}", compacdc.GetType().Name, compacdc.GetDeviceVersion()), ParamUnidad.SinUnidad);

                    var keyFileSSH = Configuracion.GetString("HAS_KEY_FILE_SSH", "NO", ParamUnidad.SinUnidad);
                    if (keyFileSSH.ToUpper().Trim() == "SI")
                    {
                        var dirPath = ConfigurationManager.AppSettings["PathCustomizationFile"];
                        var keyFile = Path.Combine(dirPath, "CDC", "id_rsa");
                        cdc.PrivateKeyFile = keyFile;
                    }
                }

                return compacdc;
            }
        }

        [JsonIgnore]
        internal Tower3 tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower3))
                {
                    tower3 = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower3);
                }
                return tower3;
            }
        }

        [JsonIgnore]
        internal CirwattDlms cirwatt
        {
            get
            {
                if (sbt == null)
                {
                    sbt = AddInstanceVar<CirwattDlms>(new CirwattDlms(), "CIRWATT");
                    Resultado.Set("VERSION_DEVICE2", string.Format("{0}: {1}", sbt.GetType().Name, sbt.GetDeviceVersion()), ParamUnidad.SinUnidad);
                }

                return sbt;
            }
        }


        private static readonly object _syncSBT = new Object();

        // SBT  //
        //**************************************

        public void VerificationParametersSBT()
        {
            VerificationParametersSBT(SBT_NUMBER);
        }

        internal void VerificationParametersSBT(byte numSBT)
        {
            var meter = AddInstanceVar(new CirwattDlms(), string.Format("SBT{0}", numSBT));

            if (numSBT == 1)
                meter.PuertoCom = Comunicaciones.SerialPort;
            else
                meter.PuertoCom = Comunicaciones.SerialPort2;

            logger.InfoFormat("New SBT{0} by  COM PORT:{1}", numSBT, meter.PuertoCom);

            meter.Retries = 8;

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                
                meter.EscribirModelo(Identificacion.MODELO_SBT);
                
                return true;
            }, "", 5, 1500, 500, false);

            SamplerWithCancel((p) =>
            {
                var codunesa = meter.LeerCodigoUnesa();
                Assert.AreEqual(ConstantsParameters.Identification.CODIGO_UNESA, codunesa, Identificacion.CODIGO_UNESA, Error().UUT.CONFIGURACION.SETUP("Error. el codigo unesa es incorrecto."), ParamUnidad.SinUnidad);

                var versionSW = meter.LeerVersionSW();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, versionSW, Identificacion.VERSION_FIRMWARE_SBT, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. la versión de firmware es incorrecto."), ParamUnidad.SinUnidad);

                var modelo = meter.LeerModelo();
                Assert.AreEqual(ConstantsParameters.Identification.MODELO_SBT, modelo, Identificacion.MODELO_SBT, Error().UUT.CONFIGURACION.SETUP("Error. el modelo del equipo es incorrecto."), ParamUnidad.SinUnidad);

                return true;
            }, "", 5, 1500, 500, false);

            meter.CerrarSesion();
        }

        public void WriteFactorsDefaultSBT()
        {
            WriteFactorsDefaultSBT(SBT_NUMBER);
        }

        internal void WriteFactorsDefaultSBT(byte numSBT)
        {
            var producto = Identificacion.NUM_SUBCONJUNTO;
            if (producto == 0)
                throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

            var ListSBT = TestInfo.ListComponents.Where((p) =>
            {
                var result = p.descripcion.Contains("SBT") && p.numproducto == producto;
                return result;
            }).ToList();

            if (ListSBT == null && ListSBT.Count == 0)
                throw new Exception(string.Format("Error no se encuentra un subconjunto SBT{0} en la estructura para poder grabar su matricula", numSBT));

            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                var numeroFabricacion = meter.LeerNumeroFabricacion();

                var SBT = ListSBT.Where(t => t.matricula == numeroFabricacion).FirstOrDefault();
                if (SBT == null)
                    throw new Exception(string.Format("Error no se encuentra un subconjunto SBT{0} en la estructura con su matricula correspondiente", numSBT));

                if (!onlySBT)
                    lock (_syncSBT)
                    {
                        if (!SBT.descripcion.Contains("_SUB" + numSBT.ToString()))
                            foreach (var item in ListSBT)
                                if (item.descripcion.Contains("_SUB1"))
                                    item.descripcion = item.descripcion.Replace("_SUB1", "_SUB2");
                                else
                                    item.descripcion = item.descripcion.Replace("_SUB2", "_SUB1");
                    }

                Resultado.Set(string.Format("SERIAL_NUMBER_SBT{0}", numSBT), TestInfo.NumSerieSubconjunto[SBT.descripcion], ParamUnidad.SinUnidad);

                meter.EscribirNumeroSerie(Convert.ToUInt32(TestInfo.NumSerieSubconjunto[SBT.descripcion]));

                meter.EscribirFechaFabricacion(DateTime.Now);

                meter.EscribirEstadoFabricacion(999);

                meter.EscribirFechaHoraActual();

                meter.EscribirFechaVerificacion(DateTime.Now);

                return true;

            }, "", 5, 1500, 500, false, false);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                meter.WriteZeroOffsetActiveGain(CirwattDlms.enumFases.TRIFASICO);

                meter.WriteVoltageGain(CirwattDlms.enumFases.TRIFASICO, new List<ushort>() { 3018, 3018, 3018 });

                meter.WriteGapGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, new List<short>() { 0, 0, 0 });

                meter.WriteGapGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._60Hz, new List<short>() { 0, 0, 0 });

                meter.WritePowerActiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, new List<ushort>() { 35594, 35594, 35594 });

              meter.WritePowerActiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._60Hz, new List<ushort>() { 35594, 35594, 35594 });

                meter.WritePowerReactiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._50Hz, new List<ushort>() { 35594, 35594, 35594 });

               meter.WritePowerReactiveGain(CirwattDlms.enumFases.TRIFASICO, TypeMeterConection.frequency._60Hz, new List<ushort>() { 35594, 35594, 35594 });

                return true;

            }, "", 5, 1500, 500, false, false);

            meter.CerrarSesion();

        }

        public void TestVoltageAdjustSBT()
        {
            TestVoltageAdjustSBT(SBT_NUMBER);
        }

        internal void TestVoltageAdjustSBT(byte numSBT)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            WaitAllTasksAndRunOnlyOnce(InstanceNumber, "MTE_TEST_VOLTAGE_ADJUST_SBT", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }); });

            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);
            
            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1100, SampleMaxNumber = 3 };

            var defs = new TriAdjustValueDef(string.Format("{0}_SBT{1}", Params.GAIN_V.L1.Name, numSBT), Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                return true;
            },"",3,500,100, false);

            var result = meter.AdjustVoltage(CirwattDlms.enumFases.TRIFASICO, adjustVoltage, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0];
                defs.L2.Value = value[1];
                defs.L3.Value = value[2];
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de voltage valores fuera de margenes");

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                meter.WriteVoltageGain(CirwattDlms.enumFases.TRIFASICO, result.Item2);
                return true;
            }, "", 3, 500, 100, false);

            meter.CerrarSesion();

        }

        public void TestGainPhaseOffsetAdjustSBT()
        {
            TestGainPhaseOffsetAdjustSBT(SBT_NUMBER);
        }

        public void TestGainPhaseOffsetAdjustSBT_60Hz()
        {
            TestGainPhaseOffsetAdjustSBT(SBT_NUMBER, TypeMeterConection.frequency._60Hz);
        }

        internal void TestGainPhaseOffsetAdjustSBT(byte numSBT, TypeMeterConection.frequency frecuency = TypeMeterConection.frequency._50Hz)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 60, ParamUnidad.Grados);

            WaitAllTasksAndRunOnlyOnce(InstanceNumber, string.Format("MTE_TEST_GAIN_ADJUST_SBT{0}", frecuency.ToString()), () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, (double)frecuency, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true); });

            Delay(2000, "Espera estabilización para calculo de Ganacia");

            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);

            double desfaseFactor = frecuency == TypeMeterConection.frequency._50Hz ? -22.2 : -18.5;

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1100, SampleMaxNumber = 3 };

            var defs = frecuency == TypeMeterConection.frequency._50Hz
                ? new TriAdjustValueDef(string.Format("{0}_SBT{1}", Params.GAIN_DESFASE.L1.Name, numSBT), Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos)
                : new TriAdjustValueDef(string.Format("{0}_SBT{1}{2}", Params.GAIN_DESFASE.L1.Name, numSBT, frecuency.ToString()), Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                return true;
            }, "", 3, 500, 100, false);

            var result = meter.AdjustGap(CirwattDlms.enumFases.TRIFASICO, frecuency, powerFactor, desfaseFactor, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0];
                defs.L2.Value = value[1];
                defs.L3.Value = value[2];
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de desfase valor fuera de margenes");

            meter.WriteGapGain(CirwattDlms.enumFases.TRIFASICO, frecuency, result.Item2);

            meter.CerrarSesion();
        }

        public void TestGainPowerAdjustSBT()
        {
            TestGainPowerAdjustSBT(SBT_NUMBER);
        }

        public void TestGainPowerAdjustSBT_60Hz()
        {
            TestGainPowerAdjustSBT(SBT_NUMBER, TypeMeterConection.frequency._60Hz);
        }

        internal void TestGainPowerAdjustSBT(byte numSBT, TypeMeterConection.frequency frecuency = TypeMeterConection.frequency._50Hz)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);
            var powerReference = adjustVoltage * adjustCurrent * Math.Cos(powerFactor.ToRadians());

            WaitAllTasksAndRunOnlyOnce(InstanceNumber, string.Format("MTE_TEST_POWER_ADJUST_SBT{0}", frecuency.ToString()), () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, (double)frecuency, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true); });

            Delay(2000, "Espera estabilización para calculo de Ganacia");

            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1100, SampleMaxNumber = 3 };

            var defs = frecuency == TypeMeterConection.frequency._50Hz
                ? new TriAdjustValueDef(string.Format("{0}_SBT{1}", Params.GAIN_KW.L1.Name, numSBT), Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos)
                : new TriAdjustValueDef(string.Format("{0}_SBT{1}{2}", Params.GAIN_KW.L1.Name, numSBT, frecuency.ToString()), Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                return true;
            }, "", 3, 500, 100, false);

            var result = meter.AdjustPowerActive(CirwattDlms.enumFases.TRIFASICO, frecuency, powerReference, controlAdjust, (value) =>
            {
                defs.L1.Value = value[0];
                defs.L2.Value = value[1];
                defs.L3.Value = value[2];
                return !defs.HasMinMaxError();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error en el ajuste de potencia  valor fuera de margenes");

            meter.WritePowerActiveGain(CirwattDlms.enumFases.TRIFASICO, frecuency, result.Item2);
            meter.WritePowerReactiveGain(CirwattDlms.enumFases.TRIFASICO, frecuency, result.Item2);

            WaitAllTasksAndRunOnlyOnce(InstanceNumber, "MTE_TEST_FINISH_POWER_ADJUST_SBT", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, 0, 60, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true); });

            meter.CerrarSesion();
        }

        public void TestCRCIntegritySBT()
        {
            TestCRCIntegritySBT(SBT_NUMBER);
        }

        internal void TestCRCIntegritySBT(byte numSBT)
        {
            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                var Crc_Binario = meter.LeerCrcBinario();

                Assert.AreEqual(string.Format("{0}_SBT{1}", ConstantsParameters.Identification.CRC_FIRMWARE, numSBT), Crc_Binario.ToString(), Identificacion.CRC_FIRMWARE.ToString(), Error().UUT.CONFIGURACION.SETUP(string.Format("Error. el CRC de integridad del SBT{0} es incorrecto.", numSBT)), ParamUnidad.SinUnidad);

                return true;
            }, "", 15, 5000, 5000, false);

            meter.CerrarSesion();

        }

        public virtual void TestVerificationSBT(int initCount = 1, int samples = 2, int timeInterval = 1100)
        {
            TestVerificationSBT(initCount, samples, timeInterval, SBT_NUMBER);
        }

        public void TestVerificationSBT_60Hz(int initCount = 1, int samples = 2, int timeInterval = 1100)
        {
            TestVerificationSBT(initCount, samples, timeInterval, SBT_NUMBER, TypeMeterConection.frequency._60Hz);
        }

        internal virtual void TestVerificationSBT(int initCount = 1, int samples = 2, int timeInterval = 1100, byte numSBT = 1, TypeMeterConection.frequency freq = TypeMeterConection.frequency._50Hz)
        {
            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);

            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, 60, ParamUnidad.Grados);

            WaitAllTasksAndRunOnlyOnce(InstanceNumber, string.Format("MTE_TEST_POWER_VERIFICATION_ON_SBT{0}", freq.ToString()), () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, (double)freq, angleGap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }); });

            var average = adjustVoltage * adjustCurrent * Math.Cos((angleGap.ToRadians()));

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            string defsKWName = freq == TypeMeterConection.frequency._50Hz
                ? string.Format("{0}_SBT{1}", Params.KW.L1.Verificacion.Name, numSBT)
                : string.Format("{0}_SBT{1}{2}", Params.KW.L1.Verificacion.Name, numSBT, freq.ToString());

            string defsKVARName = freq == TypeMeterConection.frequency._50Hz
                ? string.Format("{0}_SBT{1}", Params.KVAR.L1.Verificacion.Name, numSBT)
                : string.Format("{0}_SBT{1}{2}", Params.KVAR.L1.Verificacion.Name, numSBT, freq.ToString());

            var defs = new List<TriAdjustValueDef>();
             
            defs.Add(new TriAdjustValueDef(defsKWName, 0, 0, average, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(defsKVARName, 0, 0, average, toleranciaKw, ParamUnidad.Var));

            Delay(4000, "Espera arranque del SBT");

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();
                return true;
            }, "", 3, 500, 100, false);

            int count = 1;

            TestCalibracionBase(defs,
                () =>
                {
                    logger.DebugFormat("TestCalibracionBase SBT{0} Reint:{1}", numSBT, count);

                    var powerActive = meter.Leer_Potencia(CirwattDlms.enumFases.TRIFASICO,
                       TypeMeterConection.frequency._50Hz, TipoPotencia.Activa);
                    logger.DebugFormat("PowerActive SBT{0} Potencia Activa: {1}", numSBT, powerActive);

                    var powerReactive = meter.Leer_Potencia(CirwattDlms.enumFases.TRIFASICO,
                        TypeMeterConection.frequency._50Hz, TipoPotencia.React);
                    logger.DebugFormat("PowerActive SBT{0} Potencia Reactiva: {1}", numSBT, powerReactive);

                    var varsList = new List<double>();
                    varsList.AddRange(powerActive);
                    varsList.AddRange(powerReactive);

                    count++;

                    return varsList.ToArray();
                },
                () =>
                {
                    var result = WaitAllTasksAndRunOnlyOnce(InstanceNumber, string.Format("MTE_TEST_POWER_VERIFICATION_SBT{0}", freq.ToString()), () =>
                    {
                        List<TriLineValue> value = new List<TriLineValue>();
                        value.Add(tower.PowerSourceIII.ReadVoltage());
                        value.Add(tower.PowerSourceIII.ReadCurrent());
                        return value;
                    }, new List<TriLineValue>());

                    var Power = TriLinePower.Create(result[0], result[1], TriLineValue.Create(angleGap));
                    logger.DebugFormat("with SBT{0} -> Powers MTE PPS {1}", numSBT, Power);

                    var varsList = new List<double>();
                    varsList.AddRange(Power.KW.ToArray());
                    varsList.AddRange(Power.KVAR.ToArray());

                    return varsList.ToArray();

                }, initCount, samples, timeInterval);

            meter.CerrarSesion();
        }

        // PLC  //
        //**************************************
        public void TestPLCfromSBT()
        {
            TestPLCfromSBT(SBT_NUMBER);
        }

        internal void TestPLCfromSBT(byte numSBT)
        {
            var meter = GetVariable<CirwattDlms>(string.Format("SBT{0}", numSBT), cirwatt);

            var producto = Identificacion.NUM_SUBCONJUNTO;
            if (producto == 0)
                throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

            var SBT = TestInfo.ListComponents.Where(p => p.descripcion.Contains(onlySBT == false ? string.Format("_SUB{0}", numSBT) : "SBT") && p.numproducto == producto).ToList().FirstOrDefault();
            if (SBT == null)
                throw new Exception(string.Format("Error no se encuentra un subconjunto SBT{0} en la estructura para poder grabar su matricula", numSBT));

            uint numeroSerie = 0;

            if (numSBT == 1)
                Delay(5000, "Espera detection PLC");

            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                numeroSerie = meter.LeerNumeroSerie();

                Assert.IsTrue(numeroSerie.ToString() == TestInfo.NumSerieSubconjunto[SBT.descripcion].ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE(string.Format("Error. El número de serie grabado en el SBT{0} no corresponde con el leido", numSBT)));

                meter.CerrarSesion();

                return true;

            }, "", 5, 1500, 1000, false, false);

            SamplerWithCancel((p) =>
            {
                var deviceNotConected = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Registrado);
                if (deviceNotConected.Count() != 0)
                    throw new Exception("Error. Se ha detectado un contador conectado al concentrador después de haberse borrado la lista de nodos");

                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);

                var sbtSoap = device.Where((t) => t.IsSBT == true);

                if (sbtSoap.Count() == 0)
                    throw new Exception("Error. no se ha detectado ningun SBT conectado al concentrador ");

                var deviceSBT = sbtSoap.Where((t) => t.MeterID == "CIR" + numeroSerie.ToString()).FirstOrDefault();

                if (deviceSBT == null)
                    throw new Exception("Error. no se ha encontrado el SBT esperado");

                return true;
            }, "", 10, 4000, 1000, false, false);


            SamplerWithCancel((p) =>
            {
                meter.IniciarSesion();

                meter.EscribirEstadoFabricacion(0);

                meter.EscribirReset();

                meter.CerrarSesion();

                return true;

            }, "", 5, 1500, 500, false, false);

        }

        internal void internalTestPLCfromMeter(string fase, byte numSBT, byte retries, int timeDetectionSeg, bool noDetection, string METER_ID)
        {
            SamplerWithCancel((p) =>
            {
                cdc.DeleteSnifferFile();
                return true;
            }, "Error al limpiar el fichero sniffer.csv por SSH", 2, 2000, 0, false, false);

            Delay(1000, "");
            var timeCurrentStart = DateTime.Now;
            var qualityResult = numSBT == 1 ? "QUALITY_LEVEL_PLC_" + fase : "QUALITY_LEVEL_PLC_" + fase + "_PLC_S";
            var snrResult = numSBT == 1 ? "SNR_LEVEL_PLC_" + fase : "SNR_LEVEL_PLC_" + fase + "_PLC_S";
            var typePLC = numSBT == 1 ? "" : "_S";

            SamplerWithCancel((p) =>
            {
                var diffCurrentTime = DateTime.Now.Subtract(timeCurrentStart).TotalSeconds;
                logger.InfoFormat("Tiempo en (s) transcurrido: {0}", diffCurrentTime);

                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);
                var meter = device.Where((r) => r.IsSBT == false && r.MeterID == METER_ID).FirstOrDefault();
                if (meter == null && noDetection && diffCurrentTime > timeDetectionSeg)
                {
                    logger.InfoFormat("No se ha leido la trama del sniffer despues del tiempo indicado en la fase {0} despues de {1} reintento", fase, retries);
                    Resultado.Set(qualityResult, 0, ParamUnidad.SinUnidad);
                    return true;
                }

                if (meter != null)
                {
                    try
                  {
                        cdc.Soap.ReadDeviceInformation(meter.MeterID);
                 }catch(Exception)
                 {
                   if(!noDetection)
                        throw new Exception(string.Format("Error al intentar comunicar por PLC con el Contador {0}", METER_ID));
                    }

                    logger.InfoFormat("Reading PLC{0} Communication Quality by FASE {1} -- Retry {2} to {3}", typePLC, fase, p, retries);
                    var listSniffer = cdc.ReadPLCCommunicationsQuality();
                    if (listSniffer != null && listSniffer.Count != 0)
                    {
                        logger.InfoFormat("Respuesta PLC{0} count: {1}", typePLC, listSniffer.LastOrDefault());

                        var readingPLCQuality = Convert.ToInt32(listSniffer.LastOrDefault().PHYLevel);
                        var readingPLCNoise = Convert.ToInt32(listSniffer.LastOrDefault().PHYSNR);

                        var qualityGood = Assert.GetIsValidMaxMin(Params.PARAMETER(qualityResult, ParamUnidad.SinUnidad).Null.Name, readingPLCQuality, ParamUnidad.SinUnidad,
                         Params.PARAMETER("QUALITY_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                         Params.PARAMETER("QUALITY_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES(qualityResult.ToString()), !noDetection);

                        var snrGood = Assert.GetIsValidMaxMin(Params.PARAMETER(snrResult, ParamUnidad.SinUnidad).Null.Name, readingPLCNoise, ParamUnidad.SinUnidad,
                         Params.PARAMETER("SNR_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                         Params.PARAMETER("SNR_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES(snrResult.ToString()), !noDetection);

                        if (noDetection)
                            if (!qualityGood && !snrGood)
                                throw new Exception("Error en la señal ruido o calidad e señal en el PLC");

                        logger.InfoFormat("Tiempo en (s) PLC{0} Meter OK: {1}", typePLC, diffCurrentTime);
                        return true;
                    }
                }

                if (noDetection && diffCurrentTime > timeDetectionSeg)
                {
                    logger.InfoFormat("No se ha leido la trama del sniffer despues del tiempo indicado en la fase {0} despues de {1} reintento", fase, retries);
                    Resultado.Set(qualityResult, 0, ParamUnidad.SinUnidad);
                    return true;
                }
                else if (!noDetection && diffCurrentTime > timeDetectionSeg)
                    throw new Exception(string.Format("Error no se detecta la señal de PLC{0} en la linea", typePLC, fase));

                return false;

            }, "", retries, 5000, 5000, false, false);
        }

        internal void internalTestPLCfromMeter(string fase, byte retries, string METER_ID, bool PLC_S = false)
        {
            var qualityResult = PLC_S ? string.Format("QUALITY_LEVEL_PLC_S_{0}", fase) : string.Format("QUALITY_LEVEL_PLC_{0}", fase);
            var snrResult = PLC_S ? string.Format("SNR_LEVEL_PLC_S_{0}", fase) : string.Format("SNR_LEVEL_PLC_{0}", fase);

            SamplerWithCancel((p) =>
            {
                cdc.DeleteSnifferFile();
                return true;
            }, "Error al limpiar el fichero sniffer.csv por SSH", 2, 2000, 0, false, false);

            SamplerWithCancel((p) =>
            {
                logger.InfoFormat("Deteccion del contador en la fase {0} reintento {1}", fase, retries);

                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);
                var meter = device.Where((r) => r.IsSBT == false && r.MeterID == METER_ID).FirstOrDefault();
                if (meter == null)
                    throw new Exception(string.Format("Error. no se ha detectado un contador conectado al concentrador en la fase {0}", fase));
                
                cdc.Soap.ReadDeviceInformation(meter.MeterID);

                logger.InfoFormat("Reading PLC Communication Quality by PLC {0} -- Retry {1} to {0}", fase, p, retries);
                var listSniffer = cdc.ReadPLCCommunicationsQuality();
                if (listSniffer != null && listSniffer.Count() != 0)
                {
                    logger.InfoFormat("Respuesta PLC{ count: {0}", listSniffer.LastOrDefault());
                    var readingPLCQuality = Convert.ToInt32(listSniffer.LastOrDefault().PHYLevel);
                    var readingPLCNoise = Convert.ToInt32(listSniffer.LastOrDefault().PHYSNR);

                    var qualityGood = Assert.GetIsValidMaxMin(Params.PARAMETER(qualityResult, ParamUnidad.SinUnidad).Null.Name, readingPLCQuality, ParamUnidad.SinUnidad,
                     Params.PARAMETER("QUALITY_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                     Params.PARAMETER("QUALITY_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES(qualityResult.ToString()));

                    var snrGood = Assert.GetIsValidMaxMin(Params.PARAMETER(snrResult, ParamUnidad.SinUnidad).Null.Name, readingPLCNoise, ParamUnidad.SinUnidad,
                     Params.PARAMETER("SNR_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Min(),
                     Params.PARAMETER("SNR_LEVEL_PLC_" + fase, ParamUnidad.SinUnidad).Null.Verificacion.Max(), Error().UUT.MEDIDA.MARGENES(snrResult.ToString()));

                    return true;
                }

                throw new Exception(string.Format("Error. no se ha detectado un fichero sniffer para poder obtener la calidad de la señal en la fase {0}", fase));

            }, "", retries, 5000, 5000, false);
        }

        // CPU  //
        //**************************************
        public virtual void TestMemoryCPU()
        {
            int memoryRam = 0;

            SamplerWithCancel((p) => { memoryRam = cdc.GetSizeMemoryRAM(); return true; }, "", 8, 2000, 10000, false, false);

            Assert.AreBetween(Params.MEMORY_RAM.Null.TestPoint("TOTAL").Name, memoryRam, Params.MEMORY_RAM.Null.TestPoint("TOTAL").Min(), Params.MEMORY_RAM.Null.TestPoint("TOTAL").Max(), Error().UUT.MEMORIA.MARGENES("Error. tamaño de la memoria RAM total fuera de margenes"), ParamUnidad.MByte);

            internalCheckTestPartitionMemory(cdc.StatusInternalMemory());
        }

        public virtual void TestLedsCPU()
        {
            //ActivateLedsCPU();
            this.TestHalconFindLedsProcedure(CAMERA_CPU_PSU, "CDC_CPU", "CPU", "CDC", 3, (step) =>
                {
                    ActivateLedsCPU();
                    //Delay(3000, "");
                });
        }       

        public virtual void TestWebService()
        {
            CompactDctSoapService.configuration config = null;

            SamplerWithCancel((p) => { config = cdc.Soap.ReadConfiguration(); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 10000, false, false);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, config.info.Vf, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_MODEM_PLC, config.info.VfComm, Identificacion.VERSION_FIRMWARE_MODEM_PLC, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware del modem instalada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            var stgRead = config.stg.StgVersion;
            Resultado.Set("VERSION_STG_LEIDO", stgRead, ParamUnidad.SinUnidad);

            var stg = new CompactDctSoapService.ConfigurationStg();
            stg.StgVersion = Identificacion.VERSION_STG;
            cdc.Soap.VersionXml = stgRead;
            cdc.Soap.WriteStgVersion(stg);
            Resultado.Set("VERSION_STG_GRABADO", stg.StgVersion, ParamUnidad.SinUnidad);
            cdc.Soap.VersionXml = stg.StgVersion;

            TestInfo.NumMAC_PLC = config.info.Macplc;
            Resultado.Set(ConstantsParameters.TestInfo.MAC_PLC, config.info.Macplc, ParamUnidad.SinUnidad);

            Delay(1000, "Espera al enviar el STG version");

            cdc.Soap.WriteFrameLogMode(CompactDctSoapService.FrameLogMode.Capture);

            Delay(1000, "Espera al enviar el WriteFrameLogMode mode Capture");

            var producto = Identificacion.NUM_SUBCONJUNTO;
            if (producto == 0)
                throw new Exception("Error no se a creado parametro NUM_SUBCONJUNTO  en los parametros de Identificacion de la familia del producto");

            var PSU = TestInfo.ListComponents.Where(p => p.descripcion.Contains("FUENTE")).ToList().FirstOrDefault();
            if (PSU == null)
                throw new Exception("Error no se encuentra un subconjunto PSU en la estructura para poder grabar su matricula");

            var PLC = TestInfo.ListComponents.Where(p => p.descripcion.Contains("PLC")).ToList().FirstOrDefault();
            if (PLC == null)
                throw new Exception("Error no se encuentra un subconjunto PLC en la estructura para poder grabar su matricula");

            var LOGICA = TestInfo.ListComponents.Where(p => p.descripcion.Contains("LOGICA")).ToList().FirstOrDefault();
            if (LOGICA == null)
                throw new Exception("Error no se encuentra un subconjunto LOGICA en la estructura para poder grabar su matricula");

            var positionSlot = (int)Configuracion.GetDouble("POSICION_PSU", 1, ParamUnidad.SinUnidad);
            modulesCheck = new List<CompactDctSoapService.ConfigurationModules>();
            modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + PSU.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "Supply", Mod = "PSU-STD" });
            Resultado.Set(PSU.descripcion.ToUpper(), PSU.matricula, ParamUnidad.SinUnidad);

            positionSlot = (int)Configuracion.GetDouble("POSICION_PLC", 4, ParamUnidad.SinUnidad);
            modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + PLC.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "PLC", Mod = "PLC1", });
            Resultado.Set(PLC.descripcion.ToUpper(), PLC.matricula, ParamUnidad.SinUnidad);

            if (Configuracion.GetString("HAS_MODULE_PLC_S", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_PLC_S", 6, ParamUnidad.SinUnidad);
                var PLC_S = TestInfo.ListComponents.Where(p => p.descripcion.Contains("PLC-S")).ToList().FirstOrDefault();
                if (PLC_S == null)
                    throw new Exception("Error no se encuentra un subconjunto PLC_S en la estructura para poder grabar su matricula");
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + PLC_S.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "PLC", Mod = "PLC2", });
                Resultado.Set(PLC_S.descripcion.ToUpper(), PLC_S.matricula, ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_SBT", "SI", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_SBT", 3, ParamUnidad.SinUnidad);
                var SBT = TestInfo.ListComponents.Where(p =>
                {
                    var result = p.descripcion.Contains(onlySBT == false ? string.Format("_SUB{0}", 1) : "SBT") && p.numproducto == producto;
                    return result;
                }).ToList().FirstOrDefault();
                if (SBT == null)
                    throw new Exception(string.Format("Error no se encuentra un subconjunto SBT{0} en la estructura para poder grabar su matricula", 1));
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + TestInfo.NumSerieSubconjunto[SBT.descripcion].ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "SBT", Mod = "SBT", });
                Resultado.Set(SBT.descripcion.ToUpper(), TestInfo.NumSerieSubconjunto[SBT.descripcion], ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_SBT2", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_SBT_S", 5, ParamUnidad.SinUnidad);
                var SBT = TestInfo.ListComponents.Where(p => p.descripcion.Contains(string.Format("_SUB{0}", 2)) && p.numproducto == producto).ToList().FirstOrDefault();
                if (SBT == null)
                    throw new Exception(string.Format("Error no se encuentra un subconjunto SBT{0} en la estructura para poder grabar su matricula", 2));
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + TestInfo.NumSerieSubconjunto[SBT.descripcion].ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "SBT", Mod = "SBT", });
                Resultado.Set(SBT.descripcion.ToUpper(), TestInfo.NumSerieSubconjunto[SBT.descripcion], ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_MODEM", "SI", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_MODEM", 6, ParamUnidad.SinUnidad);
                var MODEM = TestInfo.ListComponents.Where(p => p.descripcion.Contains("MODEM")).ToList().FirstOrDefault();
                if (MODEM == null)
                    throw new Exception("Error no se encuentra un subconjunto MODEM en la estructura para poder grabar su matricula");

                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + MODEM.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "Modem", Mod = "Modem", });
                Resultado.Set(MODEM.descripcion.ToUpper(), MODEM.matricula, ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_ALARM", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_ALARM", 7, ParamUnidad.SinUnidad);
                var ALARM = TestInfo.ListComponents.Where(p => p.descripcion.Contains("ALARM")).ToList().FirstOrDefault();
                if (ALARM == null)
                    throw new Exception("Error no se encuentra un subconjunto ALARM en la estructura para poder grabar su matricula");
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + ALARM.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "Inputs", Mod = "Inputs-4", });
                Resultado.Set(ALARM.descripcion.ToUpper(), ALARM.matricula, ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_SPECTRUM", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_SPECTRUM", 4, ParamUnidad.SinUnidad);
                var ALARM = TestInfo.ListComponents.Where(p => p.descripcion.Contains("SPECTRUM")).ToList().FirstOrDefault();
                if (ALARM == null)
                    throw new Exception("Error no se encuentra un subconjunto SPECTRUM en la estructura para poder grabar su matricula");
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + ALARM.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "Spectrum", Mod = "Spectrum", });
                Resultado.Set(ALARM.descripcion.ToUpper(), ALARM.matricula, ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_BATTERY", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_BAT", 5, ParamUnidad.SinUnidad);
                var BAT = TestInfo.ListComponents.Where(p => p.descripcion.Contains("PLACA BAT")).ToList().FirstOrDefault();
                if (BAT == null)
                    throw new Exception("Error no se encuentra un subconjunto PLACA BAT en la estructura para poder grabar su matricula");
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + BAT.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "Battery", Mod = "Battery", });
                Resultado.Set(BAT.descripcion.ToUpper(), BAT.matricula, ParamUnidad.SinUnidad);
            }

            if (Configuracion.GetString("HAS_MODULE_RS485", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
            {
                positionSlot = (int)Configuracion.GetDouble("POSICION_RS485", 4, ParamUnidad.SinUnidad);
                var ALARM = TestInfo.ListComponents.Where(p => p.descripcion.Contains("RS485")).ToList().FirstOrDefault();
                if (ALARM == null)
                    throw new Exception("Error no se encuentra un subconjunto RS485 en la estructura para poder grabar su matricula");
                modulesCheck.Add(new CompactDctSoapService.ConfigurationModules() { Id = "CIR" + ALARM.matricula.ToString().PadLeft(10, '0'), Slot = positionSlot.ToString(), Type = "RS485", Mod = "RS485", });
                Resultado.Set(ALARM.descripcion.ToUpper(), ALARM.matricula, ParamUnidad.SinUnidad);
            }

            SamplerWithCancel((p) => 
            { 
                config = cdc.Soap.ReadConfiguration();
                if (modulesCheck.Count != config.modulos.Count)
                    throw new Exception("Error. no se han detectado el número de modulos conectados correctamente");

                return true;
            }, "", 10, 1000, 5000, false, false);

        }

        public virtual void TestPulsadorCPU()
        {
            if (cdc.ReadStatusResetButton() == COMPACT_DC.statusPort.Off)
                throw new Exception("Error estado de la entrada de reset incorrecto");

            tower.IO.DO.On(PULSADOR_RESET);

            Delay(500, "Espera");

            if (cdc.ReadStatusResetButton() == COMPACT_DC.statusPort.On)
                throw new Exception("Error estado de la entrada de reset incorrecto");

            tower.IO.DO.Off(PULSADOR_RESET);

            Delay(500, "Espera");

            if (cdc.ReadStatusResetButton() == COMPACT_DC.statusPort.Off)
                throw new Exception("Error estado de la entrada de reset incorrecto");

            Resultado.Set("TEST_PULSADOR_RESET", "OK", ParamUnidad.SinUnidad);
        }

        public virtual void TestConfigurationCPU()
        {

            cdc.Soap.WriteAdditionalInfo(new CompactDctSoapService.AdditionalInfo() { Group = "Others", Key = "DoubleSecondary", Value = DoubleSecondary == true ? "Y" : "N" });

            var client = Configuracion.GetString("CLIENTE", "GENERAL", ParamUnidad.SinUnidad).ToUpper();

            if (Enum.IsDefined(typeof(COMPACT_DC.Client), client))
                cdc.Soap.WriteOthers(new CompactDctSoapService.ConfigurationOthers() { Client = ((int)Enum.Parse(typeof(COMPACT_DC.Client), client)).ToString() });
            else
                throw new Exception("El cliente definido no existe, por favor introduzca uno de los clientes válidos");

            Delay(2000, "Espera la grbación de los modulos");

            cdc.Soap.WriteModuleConfiguration(modulesCheck);

            Delay(2000, "Espera la grbación de los modulos");

            var modelo = Identificacion.MODELO;
            if (modelo.Trim() == "TGB" || modelo.Trim() == "Compact DC")
                modelo = string.Format("{0}/{1}", Model.NumProducto, Model.Version);

            var config = new CompactDctSoapService.ConfigurationInfo();
            config.Cnc = Identificacion.SUBMODELO + TestInfo.NumSerie;
            config.Model = modelo;
            config.YearManufacture = DateTime.Now.Year.ToString();
            config.Te = Configuracion.GetString("TeInfo", "Concentrator", ParamUnidad.SinUnidad);
            cdc.Soap.WriteConfiguration(config);

            var password = new CompactDctSoapService.ConfigurationPassword();
            password.ManagementPassword = Configuracion.GetString("CLAVE_GESTION", "00000001", ParamUnidad.SinUnidad);
            password.WritePassword = Configuracion.GetString("CLAVE_ESCRITURA", "qxzbravo", ParamUnidad.SinUnidad);
            password.UpdatePassword = Configuracion.GetString("CLAVE_ACTUALIZACION", "uflu84i7", ParamUnidad.SinUnidad);
            password.SearchPassword = Configuracion.GetString("BUSQUEDA_AUTOMATICA_PWD", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            cdc.Soap.WritePassword(password);

            ZonaHoraria = "Europe/Madrid";
            var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
            if (timeZoneInfo == null)
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE(ZonaHoraria).Throw();
            var timeZoneLocal = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            cdc.Soap.WriteDateTimeToTimeZone(ZonaHoraria, timeZoneLocal);

            Model.TestInfo.FechaFabricacion = string.Format("{0:0000}/{1:00}", DateTime.Now.Year, DateTime.Now.Month);
            Resultado.Set("FECHA_FABRICACION", Model.TestInfo.FechaFabricacion, ParamUnidad.SinUnidad);

            if (Configuracion.GetString("HAS_MODULE_MODEM", "SI", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
                TestConfigurationModem3G();
        }

        // VERIFICATION CPU //
        //**************************************
        public virtual void DeleteNodesRestart()
        {
            bool resetModem = false;
            SamplerWithCancel((p) => { cdc.Soap.WriteConfigurationWhitOutInfoAndModuleFromXML(Identificacion.CUSTOMIZATION_FILE, true, out resetModem); return true; }, "Error, al escribir fichero XML de configuracion por SOAP ", 15, 5000, 5000, false, false);

            Delay(2000, "Esperamos grabacion parametros Modem cliente");

            if (resetModem)
                cdc.DeleteModemConfiguration();

            Delay(2000, "Espera borrar configuracion modem");

            ZonaHoraria = Configuracion.GetString("TIME_ZONE", "NO", ParamUnidad.SinUnidad).Trim();

            if (ZonaHoraria == "NO")
                ZonaHoraria = "Europe/Madrid";

            var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
            if (timeZoneInfo == null)
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE(ZonaHoraria).Throw();

            var timeZoneLocal = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            cdc.Soap.WriteDateTimeToTimeZone(ZonaHoraria, timeZoneLocal);

            Delay(10000, "");

            SamplerWithCancel((p) => { cdc.KillConcentratorAplication(); return true; }, "Error, no se ha podido borrar la BBDD por SSH en el equipo", 5, 2000, 1000, false, false);

            string nodesPaths = Configuracion.GetString("RUTA_LISTA_NODOS", "db", ParamUnidad.SinUnidad);
            cdc.DeleteListNodes(nodesPaths);

            Delay(2500, "Esperamos se desconecte el contador y borre la BD");

            cdc.CloseConectionSsH();
        }

        public virtual void TestExternalMemory()
        {
            string status = "";

            SamplerWithCancel((p) => { status = cdc.StatusExternalMemory(Configuracion.GetString("NAME_FILE_LOG", "log.txt", ParamUnidad.SinUnidad)); return true; }, "Error, no se ha podido establecer comunicacion SSH con el equipo despues de reiniciar", 10, 2000, 15000, false, false);

            var dateDevice = Convert.ToDateTime(status);

            var diferenceTime = dateDevice.Subtract(dateTimeLocalRestart);

            Resultado.Set("LAST_SHUT_DOWN_TIME", status, ParamUnidad.SinUnidad);
            Resultado.Set("LAST_SHUT_DOWN_DIFERENCE_TIME", diferenceTime.TotalSeconds.ToString(), ParamUnidad.SinUnidad);

            if ((diferenceTime.TotalSeconds > Margenes.GetDouble("DIFERENCE_TIME_SHUT_DOWN_HI", 120, ParamUnidad.s)) || (diferenceTime.TotalSeconds < Margenes.GetDouble("DIFERENCE_TIME_SHUT_DOWN_LOW", -120, ParamUnidad.s)))
                throw new Exception("Error. memoria externa no ha registrado el ultimo reset ");

            var temperature = cdc.VerificationTemperature();
            Assert.AreBetween(Params.TEMP.Null.Verificacion.Name, temperature, Params.TEMP.Null.Verificacion.Min(), Params.TEMP.Null.Verificacion.Max(), Error().UUT.CONFIGURACION.MARGENES("Error valor de temperatura fuera de márgenes"), ParamUnidad.Grados);
        }

        public virtual void TestConfigVerification()
        {
            CompactDctSoapService.configuration config = null;

            SamplerWithCancel((p) => { config = cdc.Soap.ReadConfiguration(); return true; }, "Error, no se ha podido establecer comunicacion con el servicio SOAP del equipo ", 10, 5000, 5000, false, false);

            Assert.IsTrue(config.Cnc == Identificacion.SUBMODELO + TestInfo.NumSerie.ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El número de serie grabado no corresponde con el leido"));

            var dateTimeLocal = DateTime.Now;

            if (ZonaHoraria != "NO")
            {
                var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
                if (timeZoneInfo == null)
                    Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE(ZonaHoraria).Throw();
                dateTimeLocal = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            }

            var fechaEquipo = config.Fecha;

            var diferenceTime = fechaEquipo.Subtract(dateTimeLocal);
            if ((diferenceTime.TotalSeconds > Margenes.GetDouble("TIME_DESVIATION_MAX", 120, ParamUnidad.s)) || (diferenceTime.TotalSeconds < Margenes.GetDouble("TIME_DESVIATION_MIN", -120, ParamUnidad.s)))
                throw new Exception("Error. fecha hora del equipos fuera de margenes");

            var tareas = config.tareas;
            var tareasBBDD = Configuracion.GetDouble("TAREAS_PROGRAMADAS", 3, ParamUnidad.SinUnidad);
            if (tareas.Count != tareasBBDD)
                throw new Exception("Error no se han detectado las misnas tareas programadas en el Concentrador que en la BBDD");

            if (Configuracion.GetString("HAS_MODULE_ALARM", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
                TestAlarms();
        }

        public virtual void TestAnyNodeConected()
        {
            SamplerWithCancel((p) =>
            {
                var config = cdc.Soap.ReadConfiguration();

                internalTestModule(config.modulos);

                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);

                var meter = device.Where((t) => t.IsSBT == false);

                if (meter.Count() != 0)
                    throw new Exception("Error. Se ha detectado un contador conectado al concentrador después de haberse borrado la lista de nodos");

                Resultado.Set("ANY_METER_CONNECTED", "OK", ParamUnidad.SinUnidad);

                if (Configuracion.GetString("HAS_MODULE_SBT", "NO", ParamUnidad.SinUnidad).Trim().ToUpper() == "SI")
                {
                    var sbt = device.Where((t) => t.IsSBT == true);

                    if (sbt.Count() != InstanceNumber)
                        throw new Exception("Error. no se ha detectado SBT conectado al concentrador después del reset");

                    Resultado.Set("SBT_CONNECTED", "OK", ParamUnidad.SinUnidad);
                }

                return true;

            }, "", 3, 5000, 5000, false, false);
        }

        public virtual void TestFinish()
        {
            var meter = GetVariable<CirwattDlms>(string.Format("SBT1"), cirwatt);
            if (meter != null)
                meter.Dispose();

            var meter2 = GetVariable<CirwattDlms>(string.Format("SBT2"), cirwatt);
            if (meter2 != null)
                meter2.Dispose();

            if (cirwatt != null)
                cirwatt.Dispose();

            if (cdc != null)
                cdc.Dispose();

            if (tower != null)
            {
                tower.IO.DO.OffWait(200, OUT_ALARM_IN_1_3, OUT_ALARM_IN_2_4);
                tower.ShutdownSources();
                tower.Dispose();
            }
        }

        // 3G  //
        //**************************************

        public virtual void TestConfigurationModem3G()
        {
            var apn = Configuracion.GetString("APN_SIM", "ipfija.vodafone.es", ParamUnidad.SinUnidad);
            var pin = Configuracion.GetString("PIN_SIM", "8691", ParamUnidad.SinUnidad);
            var user = Configuracion.GetString("USER_SIM", "vodafone", ParamUnidad.SinUnidad);
            var password = Configuracion.GetString("PASSWORD_SIM", "vodafone", ParamUnidad.SinUnidad);
            var authentification = Configuracion.GetString("AUTHENTICATION_SIM", "0", ParamUnidad.SinUnidad);
            var mode = Configuracion.GetString("MODE_SIM", "1", ParamUnidad.SinUnidad);

            var configurationModem3G = new CompactDctSoapService.ConfigurationModem3G()
            {
                apn1 = apn,
                authentication1 = authentification,
                user1 = user,
                password1 = password,
                pin1 = null,
                apn2 = apn,
                authentication2 = authentification,
                user2 = user,
                password2 = password,
                pin2 = pin,
                logFrames = "N",
                pingEnable = "Y",
                pingHost = "www.google.es",
                pingThreshold = "90",
                pingLength = "50",
                pingTimeout = "50",
                pingMeasureInterval = "10",
                pingInterval = "1",
                pingAction = "2",
                sim2mode = "2",
                sim2interval = "2100",
                sim2retries = "3",
                externalICMP = "2",
                externalSSH = "Y",
                ddnsEnable = "N",
                ddnsHost = "88.28.197.24",
                ddnsUser = "",
                ddnsPassword = "",
                dualSim = "2"
            };

            cdc.Soap.WriteConfigurationModem3G(configurationModem3G);

            TiempoInicialConfig3G = DateTime.Now;
            TiempoEspera3G = (int)Configuracion.GetDouble("TIEMPO_ESPERA_3G", 10000, ParamUnidad.s);

            Delay(TiempoEspera3G, "Esperando configuracion modem");
        }

        public virtual void TestVerificationModem3G()
        {
            Dictionary<COMPACT_DC.Modem.SimStatus, string> SimStatusDictionary = new Dictionary<COMPACT_DC.Modem.SimStatus, string>()
            {
                {COMPACT_DC.Modem.SimStatus.SIM_STATUS_NOT_DETECTED, "no se detecta la sim en la ranura"},
                {COMPACT_DC.Modem.SimStatus.SIM_STATUS_PIN_INVALID, "el pin introducido para la tarjeta sim es inválido"},
                {COMPACT_DC.Modem.SimStatus.SIM_STATUS_PUK_REQUIRED, "se requiere introducir el PUK para desbloquear la tarjeta"},
                {COMPACT_DC.Modem.SimStatus.SIM_STATUS_READY, "la tarjeta sim se encuentra lista para su uso"},
                {COMPACT_DC.Modem.SimStatus.SIM_STATUS_UNKNOWN, "estado de la sim desconocido"},
            };

            COMPACT_DC.Modem.modemStatus modemStatus = null;

            SamplerWithCancel((step) =>
            {
                if (step > 0 && (step % 30) == 0)
                    TestConfigurationModem3G();

                modemStatus = cdc.Modem3G.ReadModemStatus();

                Resultado.Set("MODEM_3G_STATUS_SIM1", modemStatus.statusSim1.ToString(), ParamUnidad.SinUnidad);
                Resultado.Set("MODEM_3G_STATUS_SIM2", modemStatus.statusSim2.ToString(), ParamUnidad.SinUnidad);
                Resultado.Set("MODEM_3G_IP_ASSIGNED", modemStatus.ipAssigned, ParamUnidad.SinUnidad);

                if (modemStatus.statusSim2 != COMPACT_DC.Modem.SimStatus.SIM_STATUS_READY)
                {
                    if (SimStatusDictionary.ContainsKey(modemStatus.statusSim2))
                        throw new Exception(string.Format("Error, el estado de la SIM 2 incorrecto, se esperaba encontrar que: {0}, mientras que se notifica de: {1}", SimStatusDictionary[COMPACT_DC.Modem.SimStatus.SIM_STATUS_PIN_INVALID], SimStatusDictionary[modemStatus.statusSim2]));
                    else
                        throw new Exception(string.Format("Error, el estado de la SIM 2 incorrecto, se notifica del siguiente estado {0}", modemStatus.statusSim2.ToString()));
                }

                if (modemStatus.statusSim1 != COMPACT_DC.Modem.SimStatus.SIM_STATUS_PIN_INVALID)
                {
                    if (SimStatusDictionary.ContainsKey(modemStatus.statusSim1))
                        throw new Exception(string.Format("Error, el estado de la SIM 1 incorrecto, se esperaba encontrar que: {0}, mientras que se notifica de: {1}", SimStatusDictionary[COMPACT_DC.Modem.SimStatus.SIM_STATUS_PIN_INVALID], SimStatusDictionary[modemStatus.statusSim1]));
                    else
                        throw new Exception(string.Format("Error, el estado de la SIM 1 incorrecto, se notifica del siguiente estado {0}", modemStatus.statusSim1.ToString()));
                }

                if (string.IsNullOrEmpty(modemStatus.ipAssigned) || modemStatus.ipAssigned.Length < 10)
                    throw new Exception("Error no se ha obtenido una IpAssigned");

                return true;
            }, "", 60, 6000, 1000, false, false);

            var TiempoTotalConfig3G = DateTime.Now.Subtract(TiempoInicialConfig3G).TotalSeconds;
            Resultado.Set("TIME_3G", TiempoTotalConfig3G, ParamUnidad.s);

            TestInfo.IMEI = modemStatus.imei;
            Resultado.Set("MODEM_3G_IMEI", modemStatus.imei, ParamUnidad.SinUnidad);
            Assert.AreEqual("MODEM_3G_MODEL", modemStatus.moddel, Identificacion.MODELO_3G, Error().SOFTWARE.FALLO_RED.TRESG("Error, modelo 3G incorrecto"), ParamUnidad.SinUnidad);
            Resultado.Set("MODEM_3G_MANUFACTURER", modemStatus.manufacturer, ParamUnidad.SinUnidad);
            Resultado.Set("MODEM_3G_VERSION", modemStatus.version, ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                modemStatus = cdc.Modem3G.ReadModemStatus();

                Assert.IsValidMaxMin(Params.PARAMETER("MODEM_3G_COVERAGE", ParamUnidad.dB).Null.Verificacion.Name, 
                    Convert.ToDouble(modemStatus.csq), 
                    ParamUnidad.dB,
                    Params.PARAMETER("MODEM_3G_COVERAGE", ParamUnidad.dB).Null.Verificacion.Min(),
                    Params.PARAMETER("MODEM_3G_COVERAGE", ParamUnidad.dB).Null.Verificacion.Max(),
                    Error().UUT.MEDIDA.MARGENES("Cobertura"));

                return true;
            }, "", 15, 5000, 1000, false, false);

            var router = Router.Default();

            SamplerWithCancel((p) =>
            {
                if (!router.PingIP(modemStatus.IP))
                    throw new Exception("Error al realizar un Ping a la ipAssigned de la targeta sim configurada");

                return true;
            }, "", 15, 5000, 1000, false, false);
        }

        public virtual void TestLeds3G()
        {
            Delay(2500, "Espera leds");

            this.TestHalconFindLedsProcedure(CAMERA_AUX, "CDC_3G", "3G", "CDC", 3);
        }

        // ALARMAS  //
        //**************************************
        public void TestAlarms()
        {
            var alarms = new List<CompactDctSoapService.AlarmInfo>();

            var camera = Configuracion.GetDouble("POSICION_ALARM", 5) == 5 ? CAMERA_SBT : CAMERA_AUX; 

            this.InParellel(() => this.TestHalconFindLedsProcedure(camera, "CDC_Alarmas", "Alarmas_PAR", "CDC", 3), () =>
            {
                SamplerWithCancel((p) =>
                {
                    alarms = cdc.Soap.ReadAlarms();

                    Assert.IsTrue(alarms.Count == Configuracion.GetDouble("ALARMS_NUMBER", 4), Error().UUT.CONFIGURACION.ALARMA("Error numero de alarmas detectadas incorrecto"));

                    return true;
                }, "", 10, 500, 500, false, false);

                tower.IO.DO.Off(OUT_ALARM_IN_2_4);
                tower.IO.DO.On(OUT_ALARM_IN_1_3);

                SamplerWithCancel((p) =>
                {
                    alarms = cdc.Soap.ReadAlarms();

                    foreach (CompactDctSoapService.AlarmInfo alarm in alarms)
                        if (Math.IEEERemainder(alarm.id, 2) != 0)
                            Assert.IsTrue(alarm.value.Equals("1"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 1, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                        else
                            Assert.IsTrue(alarm.value.Equals("0"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 0, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));

                    return true;
                }, "", 10, 1000, 1000, false, false);

            });

            tower.IO.DO.Off(OUT_ALARM_IN_1_3);
            tower.IO.DO.On(OUT_ALARM_IN_2_4);

            this.InParellel(() => this.TestHalconFindLedsProcedure(camera, "CDC_Alarmas", "Alarmas_IMPAR", "CDC", 3), () =>
            {
                SamplerWithCancel((p) =>
                {
                    alarms = cdc.Soap.ReadAlarms();

                    foreach (CompactDctSoapService.AlarmInfo alarm in alarms)
                        if (Math.IEEERemainder(alarm.id, 2) != 0)
                            Assert.IsTrue(alarm.value.Equals("0"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 0, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                        else
                            Assert.IsTrue(alarm.value.Equals("1"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 1, valor encontrado {0}", alarm.value.ToString())));

                    return true;
                }, "", 10, 500, 3000, false, false);
            });

            tower.IO.DO.On(OUT_ALARM_IN_1_3, OUT_ALARM_IN_2_4);
        }

        public void TestAlarmsNew(byte capturasVision = 1)
        {
            var alarms = new List<CompactDctSoapService.AlarmInfo>();

            SamplerWithCancel((p) =>
            {
                alarms = cdc.Soap.ReadAlarms();

                Assert.IsTrue(alarms.Count == Configuracion.GetDouble("ALARMS_NUMBER", 4), Error().UUT.CONFIGURACION.ALARMA("Error numero de alarmas detectadas incorrecto"));

                return true;
            }, "", 10, 500, 500, false, false);

            tower.IO.DO.Off(OUT_ALARM_IN_2_4);
            tower.IO.DO.On(OUT_ALARM_IN_1_3);
            SamplerWithCancel((p) =>
            {
                alarms = cdc.Soap.ReadAlarms();
                foreach (CompactDctSoapService.AlarmInfo alarm in alarms)
                    if (Math.IEEERemainder(alarm.id, 2) != 0)
                    {
                        Assert.IsTrue(alarm.value.Equals("1"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 1, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                        Resultado.Set(string.Format("{0}_ON", alarm.name.ToString().ToUpper()), "OK", ParamUnidad.SinUnidad);
                    }
                    else
                    {
                        Assert.IsTrue(alarm.value.Equals("0"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 0, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                        Resultado.Set(string.Format("{0}_OFF", alarm.name.ToString().ToUpper()), "OK", ParamUnidad.SinUnidad);
                    }

                return true;
            }, "", 10, 1000, 1000, false, false);
            TestLeds(Leds.ALARMA_PAR, capturasVision);

            tower.IO.DO.Off(OUT_ALARM_IN_1_3);
            tower.IO.DO.On(OUT_ALARM_IN_2_4);
            SamplerWithCancel((p) =>
            {
                alarms = cdc.Soap.ReadAlarms();
                foreach (CompactDctSoapService.AlarmInfo alarm in alarms)
                    if (Math.IEEERemainder(alarm.id, 2) != 0)
                    {
                        Assert.IsTrue(alarm.value.Equals("0"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 0, valor encontrado {0}, remanente {1}", alarm.value.ToString(), Math.IEEERemainder(alarm.id, 2))));
                        Resultado.Set(string.Format("{0}_OFF", alarm.name.ToString().ToUpper()), "OK", ParamUnidad.SinUnidad);
                    }
                    else
                    {
                        Assert.IsTrue(alarm.value.Equals("1"), Error().UUT.CONFIGURACION.ALARMA(string.Format("Error en la entrada de alarma " + alarm.id + ", estado incorrecto, valor esperado 1, valor encontrado {0}", alarm.value.ToString())));
                        Resultado.Set(string.Format("{0}_ON", alarm.name.ToString().ToUpper()), "OK", ParamUnidad.SinUnidad);
                    }

                return true;
            }, "", 10, 500, 3000, false, false);
            TestLeds(Leds.ALARMA_IMPAR, capturasVision);

            tower.IO.DO.On(OUT_ALARM_IN_1_3, OUT_ALARM_IN_2_4);
        }
        //************************************

        internal void ActivateLedsCPU()
        {
            var ledsValue = new Dictionary<string, COMPACT_DC.Ports.DefinePortsOutputs>();

            ledsValue.Add("OUT_LED_POWER", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_POWER);
            ledsValue.Add("OUT_LED_PLC_LINK", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_PLC_LINK);
            ledsValue.Add("OUT_LED_SBT_LINK", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_SBT_LINK);
            ledsValue.Add("OUT_LED_MODEM_LINK", COMPACT_DC.Ports.DefinePortsOutputs.OUT_LED_MODEM_LINK);

            foreach (KeyValuePair<string, COMPACT_DC.Ports.DefinePortsOutputs> led in ledsValue)
                cdc.Puertos.On(led.Value);

            Delay(2500, "Espera leds");

            cdc.CloseConectionSsH();
        }

        private void internalTestModule(List<CompactDctSoapService.Module> modulesRead)
        {
            if (modulesCheck.Count != modulesRead.Count)
                throw new Exception("Error. no se han detectado el número de modulos conectados correctamente");

            foreach (CompactDctSoapService.ConfigurationModules modulo in modulesCheck)
            {
                var mod = modulesRead.Where((p) => p.Mod.Contains(modulo.Mod));
                if (mod == null)
                    throw new Exception(string.Format("Error. no se han detectado el modulo {0} conectado", modulo.Mod));

                if (mod.Count() == 0)
                    throw new Exception(string.Format("Error. no se han detectado el modulo {0} conectado", modulo.Mod));

                Resultado.Set("MODULO_" + modulo.Mod.ToUpper(), string.Format("Id={0} Slot={1}", mod.FirstOrDefault().Id, mod.FirstOrDefault().Slot), ParamUnidad.SinUnidad);
            }
        }

        private void internalCheckTestPartitionMemory(List<COMPACT_DC.Memory> memory)
        {
            if (memory.Count() < 4)
                throw new Exception("Error. no se han detectado el número de particiones correctamente");

            var ArrayPartitions = new List<string> { "ubi0", "ubi1", "ubi2" };

            CultureInfo enUS = new CultureInfo("en-US");

            foreach (string partition in ArrayPartitions)
            {
                var mod = memory.Where((p) => p.Filesystem.Contains(partition));
                if (mod.Count() == 0)
                    throw new Exception(string.Format("Error. no se ha encontrado la partición {0} ", partition));

                var size = Convert.ToDouble(mod.FirstOrDefault().Size.Replace("M", ""), enUS);

                var partitionName = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Name;
                var partitionSizeMin = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Min();
                var partitionSizeMax = Params.PARAMETER("FILESYSTEM_" + partition.ToUpper(), ParamUnidad.MByte).Null.TestPoint("SIZE").Max();

                Assert.AreBetween(partitionName, size, partitionSizeMin, partitionSizeMax, Error().UUT.MEMORIA.MARGENES(string.Format("Error. tamaño de la partición{0} de la memoria interna fuera de margenes", partition)), ParamUnidad.MByte);
            }
        }


        // LEDS //
        //**************************************
        public void TestLeds(Leds leds = Leds.PSU, byte capturas = 1)
        {
            int posicion = 0;
            if (leds == Leds.ALARMA_IMPAR || leds == Leds.ALARMA_PAR)
                posicion = (int)Configuracion.GetDouble("POSICION_ALARM", 0, ParamUnidad.SinUnidad);
            else if (leds == Leds.BAT || leds == Leds.BAT_SAI || leds == Leds.BAT_STATUS)
                posicion = (int)Configuracion.GetDouble("POSICION_BAT", 0, ParamUnidad.SinUnidad);
            else
                posicion = (int)Configuracion.GetDouble(string.Format("POSICION_{0}", leds), 0, ParamUnidad.SinUnidad);

            var camera = PositionCameraDictionary[posicion];

            if (posicion != 0)
            {
                if (leds == Leds.CPU)
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, camera, String.Format("CDC_{0}_{1}_{2}", Util, posicion, leds), String.Format("CPU_{0}_{1}", Util, posicion), "CDC", capturas,
                        (step) =>
                        {
                            ActivateLedsCPU();
                        }, leds.ToString());
                else
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, camera, String.Format("CDC_{0}_{1}_{2}", Util, posicion, leds), String.Format("{0}_{1}_{2}", leds, Util, posicion), "CDC", capturas, null, leds.ToString());
            }
            else
                throw new Exception(string.Format("Error: No se ha parametrizado la posicion de la placa {0}", leds));
        }

        internal enum UtilEnum
        {
            AUTOMATICO = 12620,
            MODULAR = 12772,
            IBERDROLA = 15300,
        }

        public enum Leds
        {
            PSU,
            CPU,
            PLC,
            PLC_S,
            SBT,
            SBT_S,
            MODEM,
            ALARMA_PAR,
            ALARMA_IMPAR,
            BAT,
            BAT_SAI,
            BAT_STATUS,
            RS485_INITIALIZING, 
            RS485_CONNECTED,
            BRIGHT,
        }
    }
}
