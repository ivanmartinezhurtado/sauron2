﻿namespace Dezac.Tests.Parameters
{
    public static class ConstantsParameters
    {
        public static class LabelTest
        {
            public const string LABEL_WARNING = "LABEL_WARNING";
            public const string LABEL_SERIAL_NUMBER = "LABEL_NUMSERIE";
            public const string LABEL_SERIAL_NUMBER_SUBCONJUNTO = "LABEL_NUMSERIE_SUBCONJUNTO";
            public const string LABEL_TEST = "LABEL_TEST";
            public const string LABEL_SECURITY_NORMATIVE = "LABEL_SECURITY_NORMATIVE";
            public const string LABEL_SINGLE_BOX = "LABEL_SINGLE_BOX";
            public const string LABEL_OTHERS = "LABEL_OTHERS";
            public const string LABEL_NUMBER_COPIES = "LABEL_NUMBER_COPIES";
            public const string HAS_TEST_REPORT = "HAS_TEST_REPORT";
            public const string PRINT_TEST_REPORT = "PRINT_TEST_REPORT";
            public const string TEST_REPORT = "TEST_REPORT";
        }

        public static class TestInfo
        {
            public const string NUM_SERIE = "NUM_SERIE";
            public const string NUM_BASTIDOR = "NUM_BASTIDOR";
            public const string MAC = "MAC";
            public const string MAC2 = "MAC2";
            public const string MAC_PLC = "MAC_PLC";
            public const string DEVICE_IP = "DEVICE_IP";
            public const string COD_CIRCUTOR = "CODCIR";
            public const string BACNET_ID = "BACNET_ID";
            public const string BASTIDORES = "BASTIDORES";
        }

        public static class Comunications
        {
            public const string SERIALPORT = "SERIALPORT";
            public const string SERIALPORT2 = "SERIALPORT2";
            public const string SERIALPORT3 = "SERIALPORT3";
            public const string PERIFERICO = "PERIFERICO";
            public const string PERIFERICO2 = "PERIFERICO2";
            public const string PERIFERICO3 = "PERIFERICO3";
            public const string BAUDRATE = "BAUDRATE";
            public const string BITSSTOP = "BITSSTOP";
            public const string PARIDAD = "PARIDAD";
            public const string PERIFERICO_MODULO = "PERIFERICO_MODULO";
            public const string SERIALPORT_MODULO = "SERIALPORT_MODULO";
            public const string SERIALPORT_INSTRUMENT = "SERIALPORT_INSTRUMENT";
            public const string SERIALPORT_DEVICE = "SERIALPORT_DEVICE";
            public const string PERIFERICO_DEVICE = "PERIFERICO_DEVICE";
            public const string SP0 = "SP0";
            public const string SP1 = "SP1";
            public const string SP2 = "SP2";
            public const string SP3 = "SP3";
            public const string PROTOCOLO = "PROTOCOLO";
            public const string TIPO = "TIPO_COMUNICACION";
            public const string IP = "IP";
        }

        public static class Identification
        {
            public const string RDNI_MAX = "RDNI_MAX";
            public const string RDNI_MIN = "RDNI_MIN";
            public const string VECTOR_HARDWARE = "VECTOR_HARDWARE";
            public const string VERSION_BOOT_MODULO = "VERSION_BOOT_MODULO";
            public const string VERSION_BOOT = "VERSION_BOOT";
            public const string CRC_BOOT = "CRC_BOOT";
            public const string VERSION_FIRMWARE = "VERSION_FIRMWARE";
            public const string VERSION_FIRMWARE_BLUETOOTH = "VERSION_FIRMWARE_BLUETOOTH";
            public const string VERSION_FIRMWARE_2 = "VERSION_FIRMWARE_2";
            public const string VERSION_FIRMWARE_PLACA_COM = "VERSION_FIRMWARE_PLACA_COM";
            public const string VERSION_FIRMWARE_SBT = "VERSION_FIRMWARE_SBT";
            public const string VERSION_FIRMWARE_MODEM_PLC = "VERSION_FIRMWARE_MODEM_PLC";
            public const string VERSION_HARDWARE = "VERSION_HARDWARE";
            public const string VERSION_PRODUCTO = "VERSION_PRODUCTO";
            public const string VER_SUBCONJUNTO = "VER_SUBCONJUNTO";
            public const string VERSION_STG = "VERSION_STG";
            public const string VERSION_PLC = "VERSION_PLC";
            public const string VERSION_EMBEDDED = "VERSION_EMBEDDED";
            public const string VERSION_EMBEDDED_FINALIZADO = "VERSION_EMBEDDED_FINALIZADO";
            public const string VERSION_DEVELOPER = "VERSION_DEVELOPER";
            public const string VERSION_POWER_STUDIO = "VERSION_POWER_STUDIO";
            public const string VERSION_TEST_PASARELA = "VERSION_TEST_PASARELA";
            public const string CRC_FIRMWARE = "CRC_FIRMWARE";
            public const string CRC_FIRMWARE_FAB = "CRC_FIRMWARE_FAB";
            public const string MODELO = "MODELO";
            public const string MODELO_FAB = "MODELO_FAB";
            public const string MODELO_3G = "MODELO_3G";
            public const string MODELO_CLIENTE = "MODELO_CLIENTE";
            public const string MODELO_HARDWARE = "MODELO_HARDWARE";
            public const string MODELO_FABRICA = "MODELO_FABRICA";
            public const string MODELO_SBT = "MODELO_SBT";
            public const string SUBMODELO = "SUBMODELO";
            public const string SUBMODELO_FAB = "SUBMODELO_FAB";
            public const string IS_MID = "IS_MID";
            public const string SEQUENCE_FILE = "SEQUENCE_FILE";
            public const string MODELO_DIFERENCIAL = "MODELO_DIFERENCIAL";
            public const string MODELO_MAGNETOTERMICO = "MODELO_MAGNETOTERMICO";
            public const string VERSION_TEST = "VERSION_TEST";
            public const string MESSAGE_DISPLAY = "MESSAGE_DISPLAY";
            public const string CODE_ERROR = "CODE_ERROR";
            public const string ROM_CODE = "ROM_CODE";
            public const string CODIGO_UNESA = "CODIGO_UNESA";
            public const string CUSTOMIZATION_FILE = "CUSTOMIZATION_FILE";
            public const string CUSTOMIZATION_FILE_TEST = "CUSTOMIZATION_FILE_TEST";
            public const string NUM_SUBCONJUNTO = "NUM_SUBCONJUNTO";
            public const string ESTRUCTURA_SUBCONJUNTOS = "ESTRUCTURA_SUBCONJUNTOS";
            public const string CABECERA_NUMERO_SERIE = "CABECERA_NUMERO_SERIE";
            public const string LONGITUD_NUMERO_SERIE = "LONGITUD_NUMERO_SERIE";
            public const string COSTUMER = "COSTUMER";
            public const string MAC_CUSTOMER = "MAC_CUSTOMER";
            public const string VERSION_DSP = "VERSION_DSP";
            public const string VERSION_ATMEL = "VERSION_ATMEL";
            public const string VERSION_APLI = "VERSION_APLI";
            public const string SN_PATRON = "SN_PATRON";
            public const string NUMERO_SERIE_CLIENTE = "NUMERO_SERIE_CLIENTE";
            public const string VERSION_CPU = "VERSION_CPU";
            public const string CARATULA = "CARATULA";
            public const string MID = "MID";
            public const string PASARELA = "PASARELA";
            public const string ID = "ID";
        }

        public static class Electrics
        {
            public const string V = "V";
            public const string VF = "VF";
            public const string V_DC = "V_DC";
            public const string V_AC = "V_AC";
            public const string VRMS = "VRMS";
            public const string I = "I";
            public const string I_DC = "I_DC";
            public const string I_AC = "I_AC";
            public const string I_N = "I_NEUTRO";
            public const string I_LK = "I_FUGAS";
            public const string KW = "KW";
            public const string W = "W";
            public const string KVAR = "KVAR";
            public const string VAR = "VAR";
            public const string KVA = "KVA";
            public const string VA = "VA";
            public const string FREQ = "FREQ";
            public const string TEMP = "TEMP";
            public const string TIME = "TIME";
            public const string THD_V = "THD_V";
            public const string THD_I = "THD_I";
            public const string RES = "RES";
            public const string RES_FUGA = "RES_FUGA";
            public const string RES_RDO = "RES_RDO";
            public const string RES_RCC = "RES_RCC";
            public const string POINTS = "POINTS";
            public const string CUADRATICPOINTS = "CUADRATIC_POINTS";
            public const string V_LINE = "V_LINE";
            public const string SYNC = "SYNC";
            public const string PF = "PF";
            public const string COS_PHI = "COS_PHI";
            public const string ANGLE_GAP = "ANGLE_GAP";
            public const string ANGLE_PF = "ANGLE_PF";
            public const string NWID = "NWID";
            public const string PWID = "PWID";
            public const string PER = "PER";
            public const string VPEAK_MAX = "VPEAK_MAX";
            public const string VPEAK_MIN = "VPEAK_MIN";
            public const string TINT = "TINT";
            public const string PHASE = "PHASE";
            public const string DCYCLE = "DCYCLE";
            public const string PPM = "PPM";
            public const string LUX = "LUX";
            public const string POWER_SIGNAL = "POWER_SIGNAL";
            public const string SNR = "SNR";
            public const string WEIGHT_LED = "WEIGHT_LED";
            public const string OFFSET_V = "OFFSET_V";
            public const string OFFSET_I = "OFFSET_I";
            public const string NUM_MUESTRAS = "NUMERO_MUESTRAS";
            public const string CURRENT_FACTOR = "CURRENT_FACTOR";
        }

        public static class DeviceParameters
        {
            public const string MEMORY_RAM = "MEMORY_RAM";
            public const string MEMORY_INTERNAL = "MEMORY_INTERNAL";
            public const string VALOR = "VALOR";
        }

        public static class SharedVars
        {
            public const string BASTIDORES = "_BASTIDORES_";
        }

        public static class FactorGains
        {
            public const string GAIN_DESFASE = "GAIN_DESFASE";
            public const string GAIN_FACTOR = "GAIN_FACTOR";
            public const string GAIN_KW = "GAIN_KW";
            public const string GAIN_KVAR = "GAIN_KVAR";
            public const string GAIN_V = "GAIN_V";
            public const string GAIN_I = "GAIN_I";
            public const string GAIN_I_N = "GAIN_NEUTRO";
            public const string GAIN_I_LK = "GAIN_FUGAS";
            public const string GAIN_FREC = "GAIN_FREC";
            public const string GAIN_OFFSET = "GAIN_OFFSET";
            public const string GAIN_OFFSET_RDO = "GAIN_OFFSET_RDO";
            public const string GAIN_FACTOR_RDO = "GAIN_FACTOR_RDO";
            public const string GAIN_OFFSET_RCC = "GAIN_OFFSET_RCC";
            public const string GAIN_FACTOR_RCC = "GAIN_FACTOR_RCC";
            public const string SUM_V = "SUM_V";
            public const string SUM_I = "SUM_I";
            public const string SUM_KW = "SUM_KW";
            public const string SUM_KVAR = "SUM_KVAR";
            public const string SUM_IN = "SUM_IN";
            public const string SUM_ILK = "SUM_ILK";
        }

        public static class Fases
         {
             public const string L1 = "_L1";
             public const string L2 = "_L2";
             public const string L3 = "_L3";
             public const string L4 = "_L4";
             public const string L5 = "_L5";
             public const string L6 = "_L6";
             public const string LN = "_LN";
             public const string LK = "_LK";
             public const string L12 = "_L12";
             public const string L23 = "_L23";
             public const string L31 = "_L31";
             public const string LN_CALC = "_LN_CALC";
            public const string III = "_III";
        }

        public static class TestPoints
        {
            public const string OFFSET = "_OFFSET";
            public const string CRUCE_PISTAS = "_CRUCE_PISTAS";
            public const string CARGA = "_CARGA";
            public const string VACIO = "_VACIO";
            public const string AJUSTE = "_AJUSTE";
            public const string AJUSTE_DESFASE = "_AJUSTE_DESFASE";
            public const string AJUSTE_POTENCIA = "_AJUSTE_POTENCIA";
            public const string VERIF = "_VERIF";
            public const string RTC = "_RTC";
            public const string CALC = "_CALC";
            public const string INFINITO = "_INFIN";
            public const string TRIGGER = "_TRIGGER";
            public const string TESTER = "_TESTER";
            public const string PATRON = "_PATRON";
            public const string BATERIA = "_BATERIA";
        }

        public static class PoweSourceLevel
        {
            public const string VMIN = "_VMIN";
            public const string VMAX = "_VMAX";
            public const string VNOM = "_VNOM";
            public const string ESCALA_5A = "_5A";
            public const string ESCALA_1A = "_1A";
            public const string ESCALA_MC = "_MC";
            public const string ESCALA_200mA = "_200mA";
            public const string ESCALA_20mA = "_20mA";
            public const string ESCALA_1 = "_ESC1";
            public const string ESCALA_2 = "_ESC2";
            public const string ESCALA_3 = "_ESC3";
            public const string ESCALA_4 = "_ESC4";
            public const string ESCALA_1_DC = "_ESC1_DC";
            public const string ESCALA_2_DC = "_ESC2_DC";
            public const string ESCALA_3_DC = "_ESC3_DC";
            public const string ESCALA_4_DC = "_ESC4_DC";
            public const string ESCALA_1V = "_ESC_1V";
            public const string ESCALA_1_28V = "_ESC1_28V";
            public const string ESCALA_2V = "_ESC_2V";
            public const string ESCALA_333mV = "_ESC_333mV";
            public const string ESCALA_DC = "_ESC_DC";
            public const string ESCALA_AC = "_ESC_AC";
            public const string Q1 = "Q1";
            public const string Q2 = "Q2";
            public const string Q3 = "Q3";
            public const string Q4 = "Q4";
        }
        public static class MargenDesc
        {
            public const string MIN = "_MIN";
            public const string MAX = "_MAX";
            public const string TOL = "_TOL";
            public const string AVG = "_AVG";
        }

    }
}
