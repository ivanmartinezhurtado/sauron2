﻿using Comunications.Utility;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.03)]
    public class TR16 : DeviceBase
    {
        public TR16()
        {
        }

        public TR16(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, int periferic = 1, int timeout = 1000)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)periferic;
            Modbus.TimeOut = timeout;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.SOFTWARE_VERSION, 3).Trim('\0');
        }

        public ushort ReadCodeRom()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.CODE_ROM);
        }

        public ushort ReadSlavesNumber()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.SLAVES_NUMBER);
        }        

        public void ReadErrorCodeEEPROM()
        {
            LogMethod();
            var code = Modbus.ReadRegister((ushort)Registers.ERROR_CODE_EEPROM);

            switch (code)
            {
                case 0:
                    break;
                case 776:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("SETUP PRINCIPAL, FACTORES DE CALIBRACION Y NUMERO SERIE").Throw();
                    break;
                case 8:
                    TestException.Create().UUT.EEPROM.SETUP("SETUP PRINCIPAL").Throw();
                    break;
                case 256:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("FACTORES DE CALIBRACIÓN").Throw();
                    break;
                case 512:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("NUMERO DE SERIE").Throw();
                    break;
                case 264:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("SETUP PRINCIPAL Y FACTORES DE CALIBRACIÓN").Throw();
                    break;
                case 520:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("SETUP PRINCIPAL Y NUMERO DE SERIE").Throw();
                    break;
                case 768:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("FACTORES DE CALIBRACIÓN Y NUMERO DE SERIE").Throw();
                    break;
                default:
                    TestException.Create().UUT.EEPROM.PARAMETROS_NO_GRABADOS("CODI ERROR EEPROM DESCONOCIDO").Throw();
                    break;
            }
        }

        public byte ReadCommutator(Commutators commutator)
        {
            ushort addres;
            switch(commutator)
            {
                case Commutators.COMMUTATOR_LOW :
                    addres = (ushort)Registers.COMMUTATOR1;
                    break;
                case Commutators.COMMUTATOR_HIGH:
                    addres = (ushort)Registers.COMMUTATOR2;
                    break;
                default:
                    addres = (ushort)Commutators.COMMUTATOR_LOW;
                    break;
            }

            LogMethod();
            return Modbus.ReadMultipleCoil(addres, 8)[0];
        }

        public byte ReadAllDIP()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Registers.DIP, 8)[0];
        }

        public bool ReadDIP(DIPS dip)
        {
            LogMethod();
            return (Modbus.ReadMultipleCoil((ushort)Registers.DIP, 8)[0] & (ushort)dip) == (ushort)dip;
        }

        public Gains ReadFactorsCalibration()
        {
            LogMethod();
            return Modbus.Read<Gains>();
        }

        public ElectricalVariables ReadVariables()
        {
            LogMethod();
            return Modbus.Read<ElectricalVariables>();
        }

        public ushort ReadInputs()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public ushort ReadAnalogInput()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ANALOG_INPUT);
        }

        public double AdjustAnalogInput(double medidaIn, ConfigurationSampler config)
        {
            double Gain_AN_IN = 0;

            var med_HP_AN_IN = (1024 * medidaIn) / 0.02;

            Thread.Sleep(config.InitDelayTime);
            var result = AdjustBase(1, config.SampleNumber, config.SampleMaxNumber, config.DelayBetweenSamples, 0,
            () =>
            {
                var points = ReadAnalogInput();

                var value = new List<double>(){
                    points
                };
                return value.ToArray();
            },
            (listValues) =>
            {
                _logger.InfoFormat("Measure In Analog {0}", listValues.ToString());
                Gain_AN_IN = (1024 * med_HP_AN_IN) / listValues.Average(0);
                return true;
            });

            WriteGainAnalogInputs((ushort)Gain_AN_IN);

            return Gain_AN_IN;
        }

        public void WriteGainAnalogInputs(ushort value)
        {
            Modbus.Write((ushort)Registers.FACTOR_CALIBRATION_ANALOG, value);
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public double ReadTemperature()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TEMPERATURE);
        }

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();

            var hardwareVectorReading = Modbus.ReadMultipleCoil((ushort)Registers.HARDWARE_VECTOR, 96);

            return new HardwareVector(hardwareVectorReading);
        }

        public Setup ReadSetup()
        {
            LogMethod();
            return Modbus.Read<Setup>((ushort)Registers.SETUP);
        }

        public void WriteHardwareVector(HardwareVector hardwareVector)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_VECTOR, hardwareVector.ToSingleCoil());
        }

        public void WriteSetup(Setup setup)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SETUP, setup);
        }

        public void WriteFactorsCalibration(Gains gains)
        {
            LogMethod();
            Modbus.Write<Gains>(gains);
        }

        public void WriteSerialNumber(string sn)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SERIAL_NUMBER, Int32.Parse(sn));
        }

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void WriteLeds(bool stateLedCPU, bool stateLedCOMM)
        {
            LogMethod();
            var ledsInfo = new bool[] { stateLedCPU, stateLedCOMM };
            Modbus.WriteMultipleCoil((ushort)Registers.LEDS, ledsInfo);
        }

        public void WriteActiveSensorMeasure()
        {
            LogMethod();
            var sensorActive = new SENSOR_MEASURE()
            {
                s1 = 0,
                s2 = 0,
                s3 = 0,
                s4 = 0,
                s5 = 0,
                s6 = 0,
                s7 = 0,
                s8 = 0
            };
            Modbus.Write<SENSOR_MEASURE>(sensorActive);
        }

        public void WriteReset()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public void WriteApplyPointsCalibration()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.INPUTS_POINTS_CALIBRATION, true);
        }

        public void WriteDelay12(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DELAY12, value);
        }

        public void CalibrationCurrents(double media4Vdc, double consignaVDC, ConfigurationSampler config)
        {
            var puntos4Vdc = (media4Vdc * 1140) / consignaVDC;
            var puntos4Vdc_convert = puntos4Vdc * 1024;

            var gains = ReadGains();

            var result = AdjustBase(0, config.SampleNumber, config.SampleMaxNumber, config.DelayBetweenSamples, 0,
            () =>
            {
                var res = ReadPointsCalibration();
                var points = new List<double>()
                    {
                        res.i1, res.i12, res.i13, res.i4, res.i5, res.i6, res.i7, res.i8, res.i9, res.i10, res.i11, res.i12, res.i13, res.i14, res.i15, res.i16
                    };
                return points.ToArray();
            },
            (listValues) =>
            {
                _logger.InfoFormat("Measure Points {0}", listValues.ToString());
                gains.i1 = (ushort)(puntos4Vdc_convert / listValues.Average(0));
                _logger.InfoFormat("Gain I1 {0}", gains.i1);
                gains.i2 = (ushort)(puntos4Vdc_convert / listValues.Average(1));
                _logger.InfoFormat("Gain I2 {0}", gains.i2);
                gains.i3 = (ushort)(puntos4Vdc_convert / listValues.Average(2));
                _logger.InfoFormat("Gain I3 {0}", gains.i3);
                gains.i4 = (ushort)(puntos4Vdc_convert / listValues.Average(3));
                _logger.InfoFormat("Gain I4 {0}", gains.i4);
                gains.i5 = (ushort)(puntos4Vdc_convert / listValues.Average(4));
                _logger.InfoFormat("Gain I5 {0}", gains.i5);
                gains.i6 = (ushort)(puntos4Vdc_convert / listValues.Average(5));
                _logger.InfoFormat("Gain I6 {0}", gains.i6);
                gains.i7 = (ushort)(puntos4Vdc_convert / listValues.Average(6));
                _logger.InfoFormat("Gain I7 {0}", gains.i7);
                gains.i8 = (ushort)(puntos4Vdc_convert / listValues.Average(7));
                _logger.InfoFormat("Gain I8 {0}", gains.i8);
                gains.i9 = (ushort)(puntos4Vdc_convert / listValues.Average(8));
                _logger.InfoFormat("Gain I9 {0}", gains.i9);
                gains.i10 = (ushort)(puntos4Vdc_convert / listValues.Average(9));
                _logger.InfoFormat("Gain I10 {0}", gains.i10);
                gains.i11 = (ushort)(puntos4Vdc_convert / listValues.Average(10));
                _logger.InfoFormat("Gain I11 {0}", gains.i11);
                gains.i12 = (ushort)(puntos4Vdc_convert / listValues.Average(11));
                _logger.InfoFormat("Gain I12 {0}", gains.i12);
                gains.i13 = (ushort)(puntos4Vdc_convert / listValues.Average(12));
                _logger.InfoFormat("Gain I13 {0}", gains.i13);
                gains.i14 = (ushort)(puntos4Vdc_convert / listValues.Average(13));
                _logger.InfoFormat("Gain I14 {0}", gains.i14);
                gains.i15 = (ushort)(puntos4Vdc_convert / listValues.Average(14));
                _logger.InfoFormat("Gain I15 {0}", gains.i15);
                gains.i16 = (ushort)(puntos4Vdc_convert / listValues.Average(15));
                _logger.InfoFormat("Gain I16 {0}", gains.i16);
                return true;
            //}//,
            //(muestras)=>
            //{
            //    if(muestras)
            //    return true;
            });
            
           WriteGains(gains);
        }

        public void CalibrationDiferencialVoltage(double medida300Vdc, double consignaVDC, ConfigurationSampler config)
        {
            var puntos300Vdc_convert = (medida300Vdc * 202) / consignaVDC;

            var gains = ReadGains();

            var result = AdjustBase(0, config.SampleNumber, config.SampleMaxNumber, config.DelayBetweenSamples, 0,
            () =>
            {
                var res = ReadPointVDCalibration();
                var points = new List<double>()
                    {
                        res
                    };
                return points.ToArray();
            },
            (listValues) =>
            {
                _logger.InfoFormat("Measure Points VD {0}", listValues.ToString());
                gains.VD = (ushort)((puntos300Vdc_convert * 1024)/ listValues.Average(0));
                _logger.InfoFormat("Gain VD {0}", gains.VD);
                return true;
            });

            WriteGains(gains);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SENSOR_MESURE_ACTIVE)]
        public struct SENSOR_MEASURE
        {
            public ushort s1;
            public ushort s2;
            public ushort s3;
            public ushort s4;
            public ushort s5;
            public ushort s6;
            public ushort s7;
            public ushort s8;
        }

        public ushort ReadPointVDCalibration()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.INPUTS_POINTS_DIFERENCIAL_VOLTAGE);
        }

        public POINTS_CALIBRATION ReadPointsCalibration()
        {
            LogMethod();
            return Modbus.Read<POINTS_CALIBRATION>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.INPUTS_POINTS_CALIBRATION)]
        public struct POINTS_CALIBRATION
        {
            public ushort i1;
            public ushort i2;
            public ushort i3;
            public ushort i4;
            public ushort i5;
            public ushort i6;
            public ushort i7;
            public ushort i8;
            public ushort i9;
            public ushort i10;
            public ushort i11;
            public ushort i12;
            public ushort i13;
            public ushort i14;
            public ushort i15;
            public ushort i16;
         }

        public Gains ReadGains()
        {
            LogMethod();
            return Modbus.Read<Gains>();
        }

        public void WriteGains(Gains gains)
        {
            LogMethod();
            Modbus.Write<Gains>(gains);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.FACTORS_CALIBRATION)]
        public struct Gains
        {
            public ushort i1;
            public ushort i2;
            public ushort i3;
            public ushort i4;
            public ushort i5;
            public ushort i6;
            public ushort i7;
            public ushort i8;
            public ushort i9;
            public ushort i10;
            public ushort i11;
            public ushort i12;
            public ushort i13;
            public ushort i14;
            public ushort i15;
            public ushort i16;
            public ushort VD;
            public ushort PT100;
            public ushort Analog;
        }

        public void WriteOffsets(Offsets offsets)
        { 
            LogMethod();
            Modbus.Write<Offsets>(offsets);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.OFFSETS)]
        public struct Offsets
        {
            public ushort Offset1;
            public ushort Offset2;
            public ushort Offset3;
            public ushort Offset4;
            public ushort Offset5;
            public ushort Offset6;
            public ushort Offset7;
            public ushort Offset8;
            public ushort Offset9;
            public ushort Offset10;
            public ushort Offset11;
            public ushort Offset12;
            public ushort Offset13;
            public ushort Offset14;
            public ushort Offset15;
            public ushort Offset16;
            public ushort OffsetVD;
            public ushort OffsetPT100;
            public ushort OffsetAnalog;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VARIABLES)]
        public struct ElectricalVariables
        {
            private ushort i1;
            private ushort i2;
            private ushort i3;
            private ushort i4;
            private ushort i5;
            private ushort i6;
            private ushort i7;
            private ushort i8;
            private ushort i9;
            private ushort i10;
            private ushort i11;
            private ushort i12;
            private ushort i13;
            private ushort i14;
            private ushort i15;
            private ushort i16;
            private ushort vd;
            private ushort pt100_0;
            private ushort analogInput;
            private ushort inp;
            private ushort umbral;
            private ushort perifericNumber;
            
            //Todos los valores estaban divididos por 100D, esa division no hace falta, la he quitado. (Rbutt)

            public double I1 { get { return i1; } }
            public double I2 { get { return i2; } }
            public double I3 { get { return i3; } }
            public double I4 { get { return i4; } }
            public double I5 { get { return i5; } }
            public double I6 { get { return i6; } }
            public double I7 { get { return i7; } }
            public double I8 { get { return i8; } }
            public double I9 { get { return i9; } }
            public double I10 { get { return i10; } }
            public double I11 { get { return i11; } }
            public double I12 { get { return i12; } }
            public double I13 { get { return i13; } }
            public double I14 { get { return i14; } }
            public double I15 { get { return i15; } }
            public double I16 { get { return i16; } }
            public double VD { get { return vd; } }
            public double PT100 { get { return pt100_0; } }
            public double AnalogInput { get { return analogInput; } }
            public double DigitalInput { get { return inp; } }
            public double Umbral { get { return umbral; } }
            public double Periferic { get { return perifericNumber; } }
        }  

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP)]
        public struct Setup
        {
            public ushort S1PrimaryCurrent;
            public ushort S2PrimaryCurrent;
            public ushort S3PrimaryCurrent;
            public ushort S4PrimaryCurrent;
            public ushort S5PrimaryCurrent;
            public ushort S6PrimaryCurrent;
            public ushort S7PrimaryCurrent;
            public ushort S8PrimaryCurrent;
            public ushort S9PrimaryCurrent;
            public ushort S10PrimaryCurrent;
            public ushort S11PrimaryCurrent;
            public ushort S12PrimaryCurrent;
            public ushort S13PrimaryCurrent;
            public ushort S14PrimaryCurrent;
            public ushort S15PrimaryCurrent;
            public ushort S16PrimaryCurrent;
        }

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool FibraOptica { get; set; }
            public bool Rs232 { get; set; }
            private bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool InConfig { get; set; }

            private string FibraOpticaString { get { return FibraOptica == true ? "SI" : "NO"; } }
            private string Rs232String { get { return Rs232 == true ? "SI" : "NO"; } }
            private string ComunicationString { get { return Comunication == true ? "SI" : "NO"; } }
            private string ShuntString { get { return InConfig == true ? "SI" : "NO"; } }
 
            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    VectorHardwareSupply powerSupplyParsed;
                    powerSupplyString = value.Trim();

                    if (!Enum.TryParse(powerSupplyString.Trim(), out powerSupplyParsed))
                        TestException.Create().UUT.CONFIGURACION.VECTOR_HARDWARE(powerSupplyString).Throw();

                    PowerSupply = powerSupplyParsed;
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {

                    VectorHardwareVoltageInputs inputVoltageParsed;
                    inputVoltageString = value;

                    if (!Enum.TryParse(inputVoltageString.Trim(), out inputVoltageParsed))
                        TestException.Create().UUT.CONFIGURACION.VECTOR_HARDWARE(inputVoltageString).Throw();

                    InputVoltage = inputVoltageParsed;
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    VectorHardwareCurrentInputs inputCurrentParsed;
                    inputCurrentString = value;

                    if (!Enum.TryParse(inputCurrentString.Trim(), out inputCurrentParsed))
                        TestException.Create().UUT.CONFIGURACION.VECTOR_HARDWARE(inputCurrentString).Throw();

                    InputCurrent = inputCurrentParsed;
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    VectorHardwareModelo modeloParsed;
                    modeloString = value;

                    if (!Enum.TryParse(modeloString.Trim(), out modeloParsed))
                        TestException.Create().UUT.CONFIGURACION.VECTOR_HARDWARE(modeloString).Throw();

                    Modelo = modeloParsed;
                }
            }

            private VectorHardwareCurrentInputs InputCurrent { get; set; }
            private VectorHardwareVoltageInputs InputVoltage { get; set; }
            private VectorHardwareSupply PowerSupply { get; set; }
            private VectorHardwareModelo Modelo { get; set; }

            private string VectorHardwareTrama { get { return vectorHardwareString; } }
            private string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                FibraOptica = (vectorHardware[0] & 0x04) == 0x04;
                InConfig = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);                
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
                InputCurrentString = InputCurrent.ToString();
                InputVoltageString = InputVoltage.ToString();
                PowerSupplyString = PowerSupply.ToString();
                ModeloString = Modelo.ToString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[3] = FibraOptica;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;
                vector[11] = InConfig;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            _25A = 0,
            _100A_200A = 1,
        }

        public enum VectorHardwareVoltageInputs
        {
            _1000Vdc =0,
        }

        public enum VectorHardwareSupply
        {
            _230Vac_24Vdc = 0,
        }

        public enum VectorHardwareModelo
        {
            TR8_485 = 1,
            TR16_RS485_25A = 2,
        }

        public enum Commutators
        {
            COMMUTATOR_LOW,
            COMMUTATOR_HIGH
        }

        public enum DIPS
        {
            BAUD_HIGH = 0x01,
            BAUD_LOW = 0x02,
            MASTER_SLAVE = 0x04,
            PT = 0x08
        }

        public enum Registers
        {
            SOFTWARE_VERSION = 0x0578,
            CODE_ROM = 0x526C,
            SLAVES_NUMBER = 0x0834,
            SETUP = 0x044C,
            HARDWARE_VECTOR = 0x2710,
            FACTORS_CALIBRATION = 0x2AF8,
            FACTOR_CALIBRATION_ANALOG = 0x2B0A,
            OFFSETS = 0x2BC0,
            SERIAL_NUMBER = 0x2710,
            FLAG_TEST = 0x2AF8,
            ERROR_CODE_EEPROM = 0x05DC,
            COMMUTATOR1 = 0x0018,
            COMMUTATOR2 = 0x0020,
            DIP = 0x0008,
            LEDS = 0x0000,
            VARIABLES = 0x0000,
            INPUTS = 0x0013,
            ANALOG_INPUT = 0x0012,
            RESET = 0x07D0,
            INPUTS_POINTS_CALIBRATION = 0x2B5C,
            INPUTS_POINTS_DIFERENCIAL_VOLTAGE = 0x2B6C,
            SENSOR_MESURE_ACTIVE = 0x4EE8,
            TEMPERATURE = 0x0011,
            DELAY12=0x55F0      //variable para añadir un delay al iniciar el equipo.
        }

        public void ClosePort()
        {
            Modbus.ClosePort();              
        }

        public override void Dispose()
        {
            Modbus.Dispose();
            Modbus = null;
        }
    }
}
