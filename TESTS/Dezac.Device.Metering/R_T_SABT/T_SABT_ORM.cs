﻿
using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

/// Autogenerated - 12/12/2019 12:45:29
namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class T_SABT_ORM : DeviceBase
    {
        public T_SABT_ORM()
        {

        }

        #region DLMS

        private DeviceDlmsTrifasic meter;

        internal DeviceDlmsTrifasic Meter
        {
            get
            {
                if (meter == null)
                {
                    meter = new DeviceDlmsTrifasic();
                    meter.SetPort(modbus.PortCom, modbus.BaudRate);
                    _logger.InfoFormat("New T-SABT ORM Dlms  by  COM PORT:{0}", modbus.PortCom);
                    meter.Retries = 3;
                }

                if (modbus.IsOpen)
                {
                    modbus.ClosePort();
                    meter.StartSession();
                }

                return meter;
            }
        }

        public void StartSession()
        {
            Meter.StartSession();
        }

        internal string[] OBIS_LINE_VOLTAGE_GAIN = { "1.128.128.10.17.255", "1.128.128.10.18.255", "1.128.128.10.19.255" };
        internal string[] OBIS_FUSE_VOLTAGE_OFFSET = { "1.128.128.13.1.255", "1.128.128.13.2.255", "1.128.128.13.3.255" };
        internal string[] OBIS_FUSE_VOLTAGE_GAIN = { "1.128.128.10.33.255", "1.128.128.10.34.255", "1.128.128.10.35.255" };
        internal string[] OBIS_FUSE_POWER_GAIN = { "1.128.128.12.33.255", "1.128.128.12.34.255", "1.128.128.12.35.255" };
        internal string OBIS_NEUTRAL_CURRENT_GAIN = "1.128.128.11.1.255";

        public void WriteAdjustFactorsDefualtORM()
        {
            StartSession();
            WriteLineVoltageGain(3450, 3450, 3450);
            WriteFuseVoltageOffset(0,0,0);
            WriteFuseVoltageGain(11400,11400,11400);
            WriteFusePowerGain(12760, 12760, 12760);
            WriteNeutralCurrentGain(11772);
        }

        public void WriteLineVoltageGain(ushort gainL1, ushort gainL2, ushort gainL3)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };

            Meter.WriteFasesObisData<ushort>(OBIS_LINE_VOLTAGE_GAIN, resultadoAjuste);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<ushort> ReadLineVoltageGain()
        {
            LogMethod();
            return Meter.ReadFasesObisData<ushort>(OBIS_LINE_VOLTAGE_GAIN);
        }

        public void WriteFuseVoltageOffset(short gainL1, short gainL2, short gainL3)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<short>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };

            Meter.WriteFasesObisData<short>(OBIS_FUSE_VOLTAGE_OFFSET, resultadoAjuste);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<ushort> ReadFuseVoltageOffset()
        {
            LogMethod();
            return Meter.ReadFasesObisData<ushort>(OBIS_FUSE_VOLTAGE_OFFSET);
        }

        public void WriteFuseVoltageGain(ushort gainL1, ushort gainL2, ushort gainL3)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };

            Meter.WriteFasesObisData<ushort>(OBIS_FUSE_VOLTAGE_GAIN, resultadoAjuste);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<ushort> ReadFuseVoltageGain()
        {
            LogMethod();
            return Meter.ReadFasesObisData<ushort>(OBIS_FUSE_VOLTAGE_GAIN);
        }

        public void WriteFusePowerGain(ushort gainL1, ushort gainL2, ushort gainL3)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };

            Meter.WriteFasesObisData<ushort>(OBIS_FUSE_POWER_GAIN, resultadoAjuste);
            Thread.Sleep(3000);
        }

        public TriLineGenericValue<ushort> ReadFusePowerGain()
        {
            LogMethod();
            return Meter.ReadFasesObisData<ushort>(OBIS_FUSE_POWER_GAIN);
        }

        public void WriteNeutralCurrentGain(ushort gainNeutro)
        {
            LogMethod();
            Meter.WriteData<ushort>(OBIS_NEUTRAL_CURRENT_GAIN, gainNeutro);
        }

        public ushort ReadNeutralCurrentGain()
        {
            LogMethod();
            return Meter.ReadData<ushort>(OBIS_NEUTRAL_CURRENT_GAIN);
        }

        #endregion

        #region Modbus

        public void SetPort(string port, int baudRate = 115200, byte periferic = 1)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;

            if (!byte.TryParse(portTmp, out result))
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");

            if (modbus == null)
                modbus = new ModbusDeviceSerialPort(result, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                modbus.PortCom = result;

            //modbus.UseFunction06 = true;
            modbus.PerifericNumber = periferic;
            modbus.BaudRate = baudRate;
            _logger.InfoFormat("New T-SABT ORM Modbus by  COM PORT:{0}", result);
        }

        private ModbusDeviceSerialPort modbus;

        public ModbusDeviceSerialPort Modbus
        {
            get
            {
                if (modbus == null)
                    modbus = new ModbusDeviceSerialPort(1, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);

                if (meter != null)
                    if (meter.IsOpen)
                        meter.CloseSession();

                return modbus;
            }
        }

        #region All Register Methods

        // Description: N�mero de perif�rico seleccionado con los selectores
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadPERIPH_NUM_SELECT()
        {
            LogMethod();
            return (UInt16)Modbus.ReadHoldingRegister((ushort)Registers.PERIPH_NUM_SELECT);
        }


        // Description: N�mero de perif�rico guardado en la configuraci�n
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadPERIPH_NUM_CONFIG()
        {
            LogMethod();
            return (UInt16)Modbus.ReadHoldingRegister((ushort)Registers.PERIPH_NUM_CONFIG);
        }


        // Description: N�mero de perif�rico guardado en la configuraci�n
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WritePERIPH_NUM_CONFIG(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PERIPH_NUM_CONFIG, value);
        }


        // Description: Baudrate del puerto serie (ver tabla)
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadBAUDRATE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadHoldingRegister((ushort)Registers.BAUDRATE);
        }


        // Description: Baudrate del puerto serie (ver tabla)
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteBAUDRATE(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.BAUDRATE, value);
        }


        // Description: Tipo de sensor de temperatura externo (0-NTC 1-PT100)
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadTYPE_TEMP_SENSOR()
        {
            LogMethod();
            return (UInt16)Modbus.ReadHoldingRegister((ushort)Registers.TYPE_TEMP_SENSOR);
        }


        // Description: Tipo de sensor de temperatura externo (0-NTC 1-PT100)
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTYPE_TEMP_SENSOR(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TYPE_TEMP_SENSOR, value);
        }


        // Description: Versi�n de firmware
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadFIRMWARE_VERSION()
        {
            LogMethod();
            return (UInt32)Modbus.ReadInt32HoldingRegisters((ushort)Registers.FIRMWARE_VERSION);
        }


        // Description: N�mero de serie
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadSERIAL_NUMBER()
        {
            LogMethod();
            return (UInt32)Modbus.ReadInt32HoldingRegisters((ushort)Registers.SERIAL_NUMBER);
        }


        // Description: Relaci�n de transformaci�n de los toros de corriente de fase
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadTRANS_RELATION_I()
        {
            LogMethod();
            return (UInt32)Modbus.ReadInt32HoldingRegisters((ushort)Registers.TRANS_RELATION_I);
        }


        // Description: Relaci�n de transformaci�n del toro de corriente de neutro
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadTRANS_RELATION_V()
        {
            LogMethod();
            return (UInt32)Modbus.ReadInt32HoldingRegisters((ushort)Registers.TRANS_RELATION_V);
        }


        // Description: Estados
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadSTATES()
        {
            LogMethod();
            return (UInt32)Modbus.ReadInt32HoldingRegisters((ushort)Registers.STATES);
        }


        // Description: Alarmas
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadALARMS()
        {
            LogMethod();
            return (UInt32)Modbus.ReadInt32HoldingRegisters((ushort)Registers.ALARMS);
        }


        #endregion

        #region All Register

        public enum Registers
        {
            PERIPH_NUM_SELECT = 0x0007,
            PERIPH_NUM_CONFIG = 0x0008,
            BAUDRATE = 0x0009,
            TYPE_TEMP_SENSOR = 0x000A,
            FIRMWARE_VERSION = 0x0050,
            SERIAL_NUMBER = 0x0300,
            TRANS_RELATION_I = 0x07D0,
            TRANS_RELATION_V = 0x07D2,
            STATES = 0x0834,
            ALARMS = 0x0836,
        }

        #endregion

        #region Struct Methods and Enums


        public VOLTAGE_DOWN ReadVOLTAGE_DOWN()
        {
            LogMethod();
            return Modbus.ReadHolding<VOLTAGE_DOWN>();
        }


        public VOLTAGE_UP ReadVOLTAGE_UP()
        {
            LogMethod();
            return Modbus.ReadHolding<VOLTAGE_UP>();
        }


        public CURRENT ReadCURRENT()
        {
            LogMethod();
            return Modbus.ReadHolding<CURRENT>();
        }


        public ACTIVE_POWER ReadACTIVE_POWER()
        {
            LogMethod();
            return Modbus.ReadHolding<ACTIVE_POWER>();
        }


        public REACTIVE_POWER ReadREACTIVE_POWER()
        {
            LogMethod();
            return Modbus.ReadHolding<REACTIVE_POWER>
                ();
        }


        public APARENT_POWER ReadAPARENT_POWER()
        {
            LogMethod();
            return Modbus.ReadHolding<APARENT_POWER>
                ();
        }


        public POWER_FACTOR ReadPOWER_FACTOR()
        {
            LogMethod();
            return Modbus.ReadHolding<POWER_FACTOR>
                ();
        }


        public PHASE_VOLTAGE ReadPHASE_VOLTAGE()
        {
            LogMethod();
            return Modbus.ReadHolding<PHASE_VOLTAGE>
                ();
        }


        public PHASE_CURRENT ReadPHASE_CURRENT()
        {
            LogMethod();
            return Modbus.ReadHolding<PHASE_CURRENT>
                ();
        }


        public ACTIVE_QUADRANT ReadACTIVE_QUADRANT()
        {
            LogMethod();
            return Modbus.ReadHolding<ACTIVE_QUADRANT>
                ();
        }


        public ACTIVE_ENERGY ReadACTIVE_ENERGY()
        {
            LogMethod();
            return Modbus.ReadHolding<ACTIVE_ENERGY>
                ();
        }


        public REACTIVE_ENERGY ReadREACTIVE_ENERGY()
        {
            LogMethod();
            return Modbus.ReadHolding<REACTIVE_ENERGY>
                ();
        }


        public TEMPERATURA ReadTEMPERATURA()
        {
            LogMethod();
            return Modbus.ReadHolding<TEMPERATURA>
                ();
        }


        public FUSE ReadFUSE()
        {
            LogMethod();
            return Modbus.ReadHolding<FUSE>();
        }


        public HARMONIC ReadHARMONIC()
        {
            LogMethod();
            return Modbus.ReadHolding<HARMONIC>();
        }


        public ADC_TEMP ReadADC_TEMP()
        {
            LogMethod();
            return Modbus.ReadHolding<ADC_TEMP>();
        }

        public AC_OFFSET ReadAC_OFFSET()
        {
            LogMethod();
            return Modbus.ReadHolding<AC_OFFSET>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0708)]
        public struct VOLTAGE_DOWN
        {
            private UInt32 voltage_down_l1;
            private UInt32 voltage_down_l2;
            private UInt32 voltage_down_l3;
            private UInt32 voltage_down_n;

            public double VOLTAGE_DOWN_L1 { get { return voltage_down_l1 / 10D; } }
            public double VOLTAGE_DOWN_L2 { get { return voltage_down_l2 / 10D; } }
            public double VOLTAGE_DOWN_L3 { get { return voltage_down_l3 / 10D; } }
            public double VOLTAGE_DOWN_N { get { return voltage_down_n / 10D; } }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0710)]
        public struct VOLTAGE_UP
        {
            private UInt32 voltage_up_l1;
            private UInt32 voltage_up_l2;
            private UInt32 voltage_up_l3;
            private UInt32 voltage_up_n;

            public double VOLTAGE_UP_L1 { get { return voltage_up_l1 / 10D; } }
            public double VOLTAGE_UP_L2 { get { return voltage_up_l2 / 10D; } }
            public double VOLTAGE_UP_L3 { get { return voltage_up_l3 / 10D; } }
            public double VOLTAGE_UP_N { get { return voltage_up_n / 10D; } }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0718)]
        public struct CURRENT
        {
            private UInt32 current_l1;
            private UInt32 current_l2;
            private UInt32 current_l3;
            private UInt32 current_sum;
            private UInt32 current_n;

            public double CURRENT_L1 { get { return current_l1 / 100D; } }
            public double CURRENT_L2 { get { return current_l2 / 100D; } }
            public double CURRENT_L3 { get { return current_l3 / 100D; } }
            public double CURRENT_SUM { get { return current_sum / 100D; } }
            public double CURRENT_N { get { return current_n / 100D; } }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0722)]
        public struct ACTIVE_POWER
        {
            public UInt32 ACTIVE_POWER_L1;
            public UInt32 ACTIVE_POWER_L2;
            public UInt32 ACTIVE_POWER_L3;
            public UInt32 ACTIVE_POWER_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x072A)]
        public struct REACTIVE_POWER
        {
            public UInt32 REACTIVE_POWER_L1;
            public UInt32 REACTIVE_POWER_L2;
            public UInt32 REACTIVE_POWER_L3;
            public UInt32 REACTIVE_POWER_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0732)]
        public struct APARENT_POWER
        {
            public UInt32 APPARENT_POWER_L1;
            public UInt32 APPARENT_POWER_L2;
            public UInt32 APPARENT_POWER_L3;
            public UInt32 APPARENT_POWER_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x073A)]
        public struct POWER_FACTOR
        {
            public UInt32 POWER_FACTOR_L1;
            public UInt32 POWER_FACTOR_L2;
            public UInt32 POWER_FACTOR_L3;
            public UInt32 POWER_FACTOR_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0742)]
        public struct PHASE_VOLTAGE
        {
            public UInt32 PHASE_V1;
            public UInt32 PHASE_V2;
            public UInt32 PHASE_V3;
            public UInt32 PHASE_VN;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x074A)]
        public struct PHASE_CURRENT
        {
            public UInt32 PHASE_I1;
            public UInt32 PHASE_I2;
            public UInt32 PHASE_I3;
            public UInt32 PHASE_IN;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0752)]
        public struct ACTIVE_QUADRANT
        {
            public UInt32 ACTIVE_QUADRANT_L1;
            public UInt32 ACTIVE_QUADRANT_L2;
            public UInt32 ACTIVE_QUADRANT_L3;
            public UInt32 ACTIVE_QUADRANT_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x076C)]
        public struct ACTIVE_ENERGY
        {
            public UInt32 IMP_ACTIVE_ENERGY_L1;
            public UInt32 IMP_ACTIVE_ENERGY_L2;
            public UInt32 IMP_ACTIVE_ENERGY_L3;
            public UInt32 IMP_ACTIVE_ENERGY_TOTAL;
            public UInt32 EXP_ACTIVE_ENERGY_L1;
            public UInt32 EXP_ACTIVE_ENERGY_L2;
            public UInt32 EXP_ACTIVE_ENERGY_L3;
            public UInt32 EXP_ACTIVE_ENERGY_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x077C)]
        public struct REACTIVE_ENERGY
        {
            public UInt32 Q1_REACTIVE_ENERGY_L1;
            public UInt32 Q1_REACTIVE_ENERGY_L2;
            public UInt32 Q1_REACTIVE_ENERGY_L3;
            public UInt32 Q1_REACTIVE_ENERGY_TOTAL;
            public UInt32 Q2_REACTIVE_ENERGY_L1;
            public UInt32 Q2_REACTIVE_ENERGY_L2;
            public UInt32 Q2_REACTIVE_ENERGY_L3;
            public UInt32 Q2_REACTIVE_ENERGY_TOTAL;
            public UInt32 Q3_REACTIVE_ENERGY_L1;
            public UInt32 Q3_REACTIVE_ENERGY_L2;
            public UInt32 Q3_REACTIVE_ENERGY_L3;
            public UInt32 Q3_REACTIVE_ENERGY_TOTAL;
            public UInt32 Q4_REACTIVE_ENERGY_L1;
            public UInt32 Q4_REACTIVE_ENERGY_L2;
            public UInt32 Q4_REACTIVE_ENERGY_L3;
            public UInt32 Q4_REACTIVE_ENERGY_TOTAL;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x079C)]
        public struct TEMPERATURA
        {
            public UInt32 TEMP_INTERNAL;
            public UInt32 TEMP_F1;
            public UInt32 TEMP_F2;
            public UInt32 TEMP_F3;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x07A4)]
        public struct FUSE
        {
            private UInt32 fuse_voltage_l1;
            private UInt32 fuse_voltage_l2;
            private UInt32 fuse_voltage_l3;
            private UInt32 power_fuse_l1;
            private UInt32 power_fuse_l2;
            private UInt32 power_fuse_l3;

            public double FUSE_VOLTAGE_L1 { get { return fuse_voltage_l1 / 1000D; } }
            public double FUSE_VOLTAGE_L2 { get { return fuse_voltage_l2 / 1000D; } }
            public double FUSE_VOLTAGE_L3 { get { return fuse_voltage_l3 / 1000D; } }

            public double POWER_FUSE_L1 { get { return power_fuse_l1 / 1000D; } }
            public double POWER_FUSE_L2 { get { return power_fuse_l2 / 1000D; } }
            public double POWER_FUSE_L3 { get { return power_fuse_l3 / 1000D; } }
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x07B0)]
        public struct HARMONIC
        {
            public UInt16 HARMONIC_DISTORSION_V1;
            public UInt16 HARMONIC_DISTORSION_V2;
            public UInt16 HARMONIC_DISTORSION_V3;
            public UInt16 HARMONIC_DISTORSION_I1;
            public UInt16 HARMONIC_DISTORSION_I2;
            public UInt16 HARMONIC_DISTORSION_I3;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x2040)]
        public struct ADC_TEMP
        {
            public UInt16 ADC_TEMP_L1;
            public UInt16 ADC_TEMP_L2;
            public UInt16 ADC_TEMP_L3;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x204E)]
        public struct AC_OFFSET
        {
            public Int32 AC_OFFSET_VF1;
            public Int32 AC_OFFSET_VF2;
            public Int32 AC_OFFSET_VF3;
        }


        #endregion

        public override void Dispose()
        {
            if (modbus != null)
                modbus.Dispose();

            if (meter != null)
            {
                if (meter.IsOpen)
                    meter.CloseSession();

                meter.Dispose();
            }
        }

        #endregion

        #region Ajuste 

        public TriLineGenericValue<ushort> AdjustVoltage(double voltageReference)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>();
            var i = 0;

            var adjustVariables = new controlAdjust()
            {
                delFirst = 0,
                initCount = 3,
                interval = 1100,
                samples = 5,
            };


            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var voltagePoints = ReadVOLTAGE_UP();
                    var points = new List<double>();
                    _logger.InfoFormat("Voltage Line L1: {0}   Sample={1} / {2}", voltagePoints.VOLTAGE_UP_L1, i, adjustVariables.initCount);
                    points.Add(voltagePoints.VOLTAGE_UP_L1);
                    _logger.InfoFormat("Voltage Line L2: {0}   Sample={1} / {2}", voltagePoints.VOLTAGE_UP_L2, i, adjustVariables.initCount);
                    points.Add(voltagePoints.VOLTAGE_UP_L2);
                    _logger.InfoFormat("Voltage Line L3: {0}   Sample={1} / {2}", voltagePoints.VOLTAGE_UP_L3, i, adjustVariables.initCount);
                    points.Add(voltagePoints.VOLTAGE_UP_L3);
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    _logger.InfoFormat("StadisticList Line Voltage: {0}", points);

                    var gainDlms = ReadLineVoltageGain();
   
                    resultadoAjuste.L1 = CalculateVoltageGain(points, voltageReference, rang++, gainDlms.L1);
                    _logger.InfoFormat("Gain Line Voltage L1: {0}", resultadoAjuste.L1);

                    resultadoAjuste.L2 = CalculateVoltageGain(points, voltageReference, rang++, gainDlms.L2);
                    _logger.InfoFormat("Gain Line Voltage L2: {0}", resultadoAjuste.L2);

                    resultadoAjuste.L3 = CalculateVoltageGain(points, voltageReference, rang++, gainDlms.L3);
                    _logger.InfoFormat("Gain Line Voltage L3: {0}", resultadoAjuste.L3);

                    return true; 
                });

            return  resultadoAjuste;
        }

        private ushort CalculateVoltageGain(StatisticalList points, double voltageReference, int rang, ushort gain)
        {
            return (ushort)((points.Average(rang) / voltageReference) * gain);
        }

        public TriLineValue AdjustFuseOffset()
        {
            LogMethod();

            var resultadoAjuste = new TriLineValue();
            var i = 0;

            var adjustVariables = new controlAdjust()
            {
                delFirst = 0,
                initCount = 3,
                interval = 1100,
                samples = 5,
            };

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var acOffset = ReadAC_OFFSET();
                    var points = new List<double>();
                    _logger.InfoFormat("AC_OFFSET L1: {0}   Sample={1} / {2}", acOffset.AC_OFFSET_VF1, i, adjustVariables.initCount);
                    points.Add(acOffset.AC_OFFSET_VF1);
                    _logger.InfoFormat("AC_OFFSET L2: {0}   Sample={1} / {2}", acOffset.AC_OFFSET_VF2, i, adjustVariables.initCount);
                    points.Add(acOffset.AC_OFFSET_VF2);
                    _logger.InfoFormat("AC_OFFSET L3: {0}   Sample={1} / {2}", acOffset.AC_OFFSET_VF3, i, adjustVariables.initCount);
                    points.Add(acOffset.AC_OFFSET_VF3);
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    _logger.InfoFormat("StadisticList AC Fuse: {0}", points);
                    resultadoAjuste.L1 = points.Average(rang++);
                    _logger.InfoFormat("Fuse AC Average L1: {0}", resultadoAjuste.L1);
                    resultadoAjuste.L2 = points.Average(rang++);
                    _logger.InfoFormat("Fuse Ac Average L2: {0}", resultadoAjuste.L2);
                    resultadoAjuste.L3 = points.Average(rang++);
                    _logger.InfoFormat("Fuse Ac Average L3: {0}", resultadoAjuste.L3);
                    return true;
                });

            return resultadoAjuste;
        }

        public TriLineGenericValue<ushort> AdjustVoltageFuseGain(double voltageReference)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>();
            var i = 0;

            var adjustVariables = new controlAdjust()
            {
                delFirst = 0,
                initCount = 3,
                interval = 1100,
                samples = 5,
            };

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var fuse = ReadFUSE();
                    var points = new List<double>();
                    _logger.InfoFormat("FUSE_VOLTAGE_L1: {0}   Sample={1} / {2}", fuse.FUSE_VOLTAGE_L1, i, adjustVariables.initCount);
                    points.Add(fuse.FUSE_VOLTAGE_L1);
                    _logger.InfoFormat("FUSE_VOLTAGE_L2: {0}   Sample={1} / {2}", fuse.FUSE_VOLTAGE_L2, i, adjustVariables.initCount);
                    points.Add(fuse.FUSE_VOLTAGE_L2);
                    _logger.InfoFormat("FUSE_VOLTAGE_L3: {0}   Sample={1} / {2}", fuse.FUSE_VOLTAGE_L3, i, adjustVariables.initCount);
                    points.Add(fuse.FUSE_VOLTAGE_L3);
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;

                    var gainDlms = ReadFuseVoltageGain();

                    _logger.InfoFormat("StadisticList Fuse voltage: {0}", points);
                    resultadoAjuste.L1 = CalculateFuseGain(points, voltageReference, rang++, gainDlms.L1);
                    _logger.InfoFormat("Fuse Voltage Gain L1: {0}", resultadoAjuste.L1);
                    resultadoAjuste.L2 = CalculateFuseGain(points, voltageReference, rang++, gainDlms.L2);
                    _logger.InfoFormat("Fuse Voltage Gain L2: {0}", resultadoAjuste.L2);
                    resultadoAjuste.L3 = CalculateFuseGain(points, voltageReference, rang++, gainDlms.L3);
                    _logger.InfoFormat("Fuse Voltage Gain L3: {0}", resultadoAjuste.L3);
                    return true;
                });

            return resultadoAjuste;
        }

        private ushort CalculateFuseGain(StatisticalList points, double reference, int rang, ushort gain)
        {
            return (ushort)((points.Average(rang) / reference) * gain);
        }

        public TriLineGenericValue<ushort> AdjustPowerFuseGain(double powerReference)
        {
            LogMethod();

            var resultadoAjuste = new TriLineGenericValue<ushort>();
            var i = 0;

            var adjustVariables = new controlAdjust()
            {
                delFirst = 0,
                initCount = 3,
                interval = 1100,
                samples = 5,
            };

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var fuse = ReadFUSE();
                    var points = new List<double>();
                    _logger.InfoFormat("POWER_FUSE_L1: {0}   Sample={1} / {2}", fuse.POWER_FUSE_L1, i, adjustVariables.initCount);
                    points.Add(fuse.POWER_FUSE_L1);
                    _logger.InfoFormat("POWER_FUSE_L2: {0}   Sample={1} / {2}", fuse.POWER_FUSE_L2, i, adjustVariables.initCount);
                    points.Add(fuse.POWER_FUSE_L2);
                    _logger.InfoFormat("POWER_FUSE_L3: {0}   Sample={1} / {2}", fuse.POWER_FUSE_L3, i, adjustVariables.initCount);
                    points.Add(fuse.POWER_FUSE_L3);
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;

                    var gainDlms = ReadFusePowerGain();

                    _logger.InfoFormat("StadisticList Fuse voltage: {0}", points);
                    resultadoAjuste.L1 = CalculateFuseGain(points, powerReference, rang++, gainDlms.L1);
                    _logger.InfoFormat("Fuse power Gain L1: {0}", resultadoAjuste.L1);
                    resultadoAjuste.L2 = CalculateFuseGain(points, powerReference, rang++, gainDlms.L2);
                    _logger.InfoFormat("Fuse power Gain L2: {0}", resultadoAjuste.L2);
                    resultadoAjuste.L3 = CalculateFuseGain(points, powerReference, rang++, gainDlms.L3);
                    _logger.InfoFormat("Fuse power Gain L3: {0}", resultadoAjuste.L3);
                    return true;
                });

            return resultadoAjuste;
        }

        public ushort AdjustNeutralCurrentGain(double referenceCurrent)
        {
            LogMethod();

            var resultadoAjuste = (ushort)0;
            var i = 0;

            var adjustVariables = new controlAdjust()
            {
                delFirst = 0,
                initCount = 3,
                interval = 1100,
                samples = 5,
            };

            var adjustResult = AdjustBase(adjustVariables.delFirst, adjustVariables.initCount, adjustVariables.samples, adjustVariables.interval, 0,
                () =>
                {
                    var fuse = ReadCURRENT();
                    var points = new List<double>();
                    _logger.InfoFormat("NEUTRAL_CURRENT: {0}   Sample={1} / {2}", fuse.CURRENT_N, i, adjustVariables.initCount);
                    points.Add(fuse.CURRENT_N);
                    i++;
                    return points.ToArray();
                },
                (points) =>
                {
                    var rang = 0;
                    var gainDlms = ReadNeutralCurrentGain();

                    _logger.InfoFormat("StadisticList Neutral current: {0}", points);
                    resultadoAjuste = CalculateFuseGain(points, referenceCurrent, rang++, gainDlms);
                    return true;
                });

            return resultadoAjuste;
        }

        #endregion

    }
}
