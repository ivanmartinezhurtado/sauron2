﻿namespace Dezac.Instruments.Controls
{
    partial class UC_CVM_EMULATOR
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label factorPotenciaL1Label;
            System.Windows.Forms.Label potenciaActivaL1Label;
            System.Windows.Forms.Label tensionL1Label;
            System.Windows.Forms.Label corrienteL1Label;
            System.Windows.Forms.Label cosenoPhiL1Label;
            System.Windows.Forms.Label potenciaAparenteL1Label;
            System.Windows.Forms.Label potenciaReactivaInductivaL1Label;
            System.Windows.Forms.Label frecuenciaLabel;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL1Label;
            this.bsAllVars = new System.Windows.Forms.BindingSource(this.components);
            this.cosenoPhiL1TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL1TextBox = new System.Windows.Forms.TextBox();
            this.tensionL1TextBox = new System.Windows.Forms.TextBox();
            this.btnOn = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.factorPotenciaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaInductivaL1TextBox = new System.Windows.Forms.TextBox();
            this.frecuenciaTextBox = new System.Windows.Forms.TextBox();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmb_Periferic = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBoxBaudRate = new System.Windows.Forms.ComboBox();
            factorPotenciaL1Label = new System.Windows.Forms.Label();
            potenciaActivaL1Label = new System.Windows.Forms.Label();
            tensionL1Label = new System.Windows.Forms.Label();
            corrienteL1Label = new System.Windows.Forms.Label();
            cosenoPhiL1Label = new System.Windows.Forms.Label();
            potenciaAparenteL1Label = new System.Windows.Forms.Label();
            potenciaReactivaInductivaL1Label = new System.Windows.Forms.Label();
            frecuenciaLabel = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL1Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // factorPotenciaL1Label
            // 
            factorPotenciaL1Label.AutoSize = true;
            factorPotenciaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL1Label.Location = new System.Drawing.Point(18, 71);
            factorPotenciaL1Label.Name = "factorPotenciaL1Label";
            factorPotenciaL1Label.Size = new System.Drawing.Size(44, 13);
            factorPotenciaL1Label.TabIndex = 0;
            factorPotenciaL1Label.Text = "PF L1:";
            // 
            // potenciaActivaL1Label
            // 
            potenciaActivaL1Label.AutoSize = true;
            potenciaActivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL1Label.Location = new System.Drawing.Point(158, 15);
            potenciaActivaL1Label.Name = "potenciaActivaL1Label";
            potenciaActivaL1Label.Size = new System.Drawing.Size(75, 13);
            potenciaActivaL1Label.TabIndex = 0;
            potenciaActivaL1Label.Text = "Pot. Act L1:";
            // 
            // tensionL1Label
            // 
            tensionL1Label.AutoSize = true;
            tensionL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL1Label.Location = new System.Drawing.Point(25, 15);
            tensionL1Label.Name = "tensionL1Label";
            tensionL1Label.Size = new System.Drawing.Size(37, 13);
            tensionL1Label.TabIndex = 0;
            tensionL1Label.Text = "V L1:";
            // 
            // corrienteL1Label
            // 
            corrienteL1Label.AutoSize = true;
            corrienteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL1Label.Location = new System.Drawing.Point(29, 41);
            corrienteL1Label.Name = "corrienteL1Label";
            corrienteL1Label.Size = new System.Drawing.Size(33, 13);
            corrienteL1Label.TabIndex = 0;
            corrienteL1Label.Text = "I L1:";
            // 
            // cosenoPhiL1Label
            // 
            cosenoPhiL1Label.AutoSize = true;
            cosenoPhiL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL1Label.Location = new System.Drawing.Point(336, 41);
            cosenoPhiL1Label.Name = "cosenoPhiL1Label";
            cosenoPhiL1Label.Size = new System.Drawing.Size(72, 13);
            cosenoPhiL1Label.TabIndex = 0;
            cosenoPhiL1Label.Text = "Cos Phi L1:";
            // 
            // potenciaAparenteL1Label
            // 
            potenciaAparenteL1Label.AutoSize = true;
            potenciaAparenteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL1Label.Location = new System.Drawing.Point(326, 15);
            potenciaAparenteL1Label.Name = "potenciaAparenteL1Label";
            potenciaAparenteL1Label.Size = new System.Drawing.Size(82, 13);
            potenciaAparenteL1Label.TabIndex = 0;
            potenciaAparenteL1Label.Text = "Pot. Apar L1:";
            // 
            // potenciaReactivaInductivaL1Label
            // 
            potenciaReactivaInductivaL1Label.AutoSize = true;
            potenciaReactivaInductivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaInductivaL1Label.Location = new System.Drawing.Point(159, 41);
            potenciaReactivaInductivaL1Label.Name = "potenciaReactivaInductivaL1Label";
            potenciaReactivaInductivaL1Label.Size = new System.Drawing.Size(74, 13);
            potenciaReactivaInductivaL1Label.TabIndex = 0;
            potenciaReactivaInductivaL1Label.Text = "Pot. Ind L1:";
            // 
            // frecuenciaLabel
            // 
            frecuenciaLabel.AutoSize = true;
            frecuenciaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            frecuenciaLabel.Location = new System.Drawing.Point(333, 71);
            frecuenciaLabel.Name = "frecuenciaLabel";
            frecuenciaLabel.Size = new System.Drawing.Size(74, 13);
            frecuenciaLabel.TabIndex = 0;
            frecuenciaLabel.Text = "Frecuencia:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.BackColor = System.Drawing.Color.Transparent;
            label8.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.ForeColor = System.Drawing.Color.Black;
            label8.Location = new System.Drawing.Point(5, 68);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(84, 19);
            label8.TabIndex = 54;
            label8.Text = "CVM-B150";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(12, 95);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(70, 13);
            label1.TabIndex = 55;
            label1.Text = "Serial Port:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(24, 150);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(58, 13);
            label4.TabIndex = 58;
            label4.Text = "Periferic:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(15, 203);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(67, 13);
            label3.TabIndex = 62;
            label3.Text = "BaudRate:";
            // 
            // cosenoPhiL1TextBox
            // 
            this.cosenoPhiL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL1", true));
            this.cosenoPhiL1TextBox.Location = new System.Drawing.Point(413, 37);
            this.cosenoPhiL1TextBox.Name = "cosenoPhiL1TextBox";
            this.cosenoPhiL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.cosenoPhiL1TextBox.TabIndex = 1;
            // 
            // corrienteL1TextBox
            // 
            this.corrienteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL1", true));
            this.corrienteL1TextBox.Location = new System.Drawing.Point(68, 37);
            this.corrienteL1TextBox.Name = "corrienteL1TextBox";
            this.corrienteL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.corrienteL1TextBox.TabIndex = 1;
            // 
            // tensionL1TextBox
            // 
            this.tensionL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL1", true));
            this.tensionL1TextBox.Location = new System.Drawing.Point(68, 11);
            this.tensionL1TextBox.Name = "tensionL1TextBox";
            this.tensionL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.tensionL1TextBox.TabIndex = 1;
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(8, 318);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(79, 42);
            this.btnOn.TabIndex = 4;
            this.btnOn.Text = "Read";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(10, 111);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(75, 21);
            this.cmb_Port.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // factorPotenciaL1TextBox
            // 
            this.factorPotenciaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL1", true));
            this.factorPotenciaL1TextBox.Location = new System.Drawing.Point(68, 69);
            this.factorPotenciaL1TextBox.Name = "factorPotenciaL1TextBox";
            this.factorPotenciaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.factorPotenciaL1TextBox.TabIndex = 1;
            // 
            // potenciaActivaL1TextBox
            // 
            this.potenciaActivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL1", true));
            this.potenciaActivaL1TextBox.Location = new System.Drawing.Point(238, 11);
            this.potenciaActivaL1TextBox.Name = "potenciaActivaL1TextBox";
            this.potenciaActivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaActivaL1TextBox.TabIndex = 1;
            // 
            // potenciaAparenteL1TextBox
            // 
            this.potenciaAparenteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL1", true));
            this.potenciaAparenteL1TextBox.Location = new System.Drawing.Point(413, 11);
            this.potenciaAparenteL1TextBox.Name = "potenciaAparenteL1TextBox";
            this.potenciaAparenteL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaAparenteL1TextBox.TabIndex = 1;
            // 
            // potenciaReactivaCapicitivaL1TextBox
            // 
            this.potenciaReactivaCapicitivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL1", true));
            this.potenciaReactivaCapicitivaL1TextBox.Location = new System.Drawing.Point(243, 69);
            this.potenciaReactivaCapicitivaL1TextBox.Name = "potenciaReactivaCapicitivaL1TextBox";
            this.potenciaReactivaCapicitivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaCapicitivaL1TextBox.TabIndex = 1;
            // 
            // potenciaReactivaInductivaL1TextBox
            // 
            this.potenciaReactivaInductivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaInductivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaInductivaL1", true));
            this.potenciaReactivaInductivaL1TextBox.Location = new System.Drawing.Point(238, 37);
            this.potenciaReactivaInductivaL1TextBox.Name = "potenciaReactivaInductivaL1TextBox";
            this.potenciaReactivaInductivaL1TextBox.Size = new System.Drawing.Size(79, 20);
            this.potenciaReactivaInductivaL1TextBox.TabIndex = 1;
            // 
            // frecuenciaTextBox
            // 
            this.frecuenciaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frecuenciaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "Frecuencia", true));
            this.frecuenciaTextBox.Location = new System.Drawing.Point(413, 69);
            this.frecuenciaTextBox.Name = "frecuenciaTextBox";
            this.frecuenciaTextBox.Size = new System.Drawing.Size(79, 20);
            this.frecuenciaTextBox.TabIndex = 1;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(95, 364);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(502, 13);
            this.pbStatus.TabIndex = 41;
            this.pbStatus.Visible = false;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(corrienteL1Label);
            this.panel1.Controls.Add(this.corrienteL1TextBox);
            this.panel1.Controls.Add(this.cosenoPhiL1TextBox);
            this.panel1.Controls.Add(cosenoPhiL1Label);
            this.panel1.Controls.Add(potenciaReactivaCapicitivaL1Label);
            this.panel1.Controls.Add(this.potenciaReactivaCapicitivaL1TextBox);
            this.panel1.Controls.Add(potenciaAparenteL1Label);
            this.panel1.Controls.Add(this.potenciaAparenteL1TextBox);
            this.panel1.Controls.Add(factorPotenciaL1Label);
            this.panel1.Controls.Add(this.factorPotenciaL1TextBox);
            this.panel1.Controls.Add(frecuenciaLabel);
            this.panel1.Controls.Add(potenciaReactivaInductivaL1Label);
            this.panel1.Controls.Add(this.potenciaReactivaInductivaL1TextBox);
            this.panel1.Controls.Add(potenciaActivaL1Label);
            this.panel1.Controls.Add(this.potenciaActivaL1TextBox);
            this.panel1.Controls.Add(this.frecuenciaTextBox);
            this.panel1.Controls.Add(tensionL1Label);
            this.panel1.Controls.Add(this.tensionL1TextBox);
            this.panel1.Location = new System.Drawing.Point(93, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(507, 352);
            this.panel1.TabIndex = 50;
            // 
            // cmb_Periferic
            // 
            this.cmb_Periferic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Periferic.FormattingEnabled = true;
            this.cmb_Periferic.Location = new System.Drawing.Point(10, 166);
            this.cmb_Periferic.Name = "cmb_Periferic";
            this.cmb_Periferic.Size = new System.Drawing.Size(75, 21);
            this.cmb_Periferic.TabIndex = 57;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.circutor1;
            this.pictureBox1.Location = new System.Drawing.Point(11, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(74, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 60;
            this.pictureBox1.TabStop = false;
            // 
            // comboBoxBaudRate
            // 
            this.comboBoxBaudRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBaudRate.FormattingEnabled = true;
            this.comboBoxBaudRate.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.comboBoxBaudRate.Location = new System.Drawing.Point(11, 219);
            this.comboBoxBaudRate.Name = "comboBoxBaudRate";
            this.comboBoxBaudRate.Size = new System.Drawing.Size(75, 21);
            this.comboBoxBaudRate.TabIndex = 61;
            // 
            // potenciaReactivaCapicitivaL1Label
            // 
            potenciaReactivaCapicitivaL1Label.AutoSize = true;
            potenciaReactivaCapicitivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL1Label.Location = new System.Drawing.Point(159, 71);
            potenciaReactivaCapicitivaL1Label.Name = "potenciaReactivaCapicitivaL1Label";
            potenciaReactivaCapicitivaL1Label.Size = new System.Drawing.Size(78, 13);
            potenciaReactivaCapicitivaL1Label.TabIndex = 0;
            potenciaReactivaCapicitivaL1Label.Text = "Pot. Cap L1:";
            // 
            // UC_CVM_EMULATOR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(label3);
            this.Controls.Add(this.comboBoxBaudRate);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(label4);
            this.Controls.Add(this.cmb_Periferic);
            this.Controls.Add(label1);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(label8);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.btnOn);
            this.Controls.Add(this.panel1);
            this.Name = "UC_CVM_EMULATOR";
            this.Size = new System.Drawing.Size(611, 385);
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.BindingSource bsAllVars;
        private System.Windows.Forms.TextBox tensionL1TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL1TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL1TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL1TextBox;
        private System.Windows.Forms.TextBox corrienteL1TextBox;
        private System.Windows.Forms.TextBox frecuenciaTextBox;
        private System.Windows.Forms.TextBox potenciaReactivaInductivaL1TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL1TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL1TextBox;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmb_Periferic;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBoxBaudRate;
    }
}
