﻿using System.ComponentModel;
using System.Drawing;
using WinDHCP.Library;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DHCPDisposedDescription))]
    public class DHCPDisposed : ActionBase
    {
        [DefaultValue("DHCPServer")]
        [Category("3. Instance")]
        [Description("Nombre de la instancia del objeto DHCP Server")]
        public string DHCPServerName { get; set; }

        public override void Execute()
        {
            var DHCPserver = GetSharedVariable(DHCPServerName) as DhcpServer;
            if (DHCPserver != null)
            {
                DHCPserver.Stop();
                DHCPserver = null;
            }
        }
    }

    public class DHCPDisposedDescription : ActionDescription
    {
        public override string Name { get { return "DHCPDisposed"; } }
        public override string Description { get { return "Finaliza y libera los recursos de un Servidor DHCP"; } }
        public override string Category { get { return "Tools/Ethernet/DHCP"; } }
        public override Image Icon { get { return Properties.Resources.DHCP; } }
    }
}
