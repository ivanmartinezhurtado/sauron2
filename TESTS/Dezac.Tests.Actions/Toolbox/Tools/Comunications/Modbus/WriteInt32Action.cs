﻿using Dezac.Device;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WriteInt32ActionDescription))]
    public class WriteInt32Action : ActionBase
    {
        [Description("Dirección Modbus en decimal")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ushort Address { get; set; }

        [Description("Tipo registro Modbus HoldingRegister (03) InputRegister (04)")]
        [Category("3. Configuration")]
        [DefaultValue(TipoRegisterModbus.InputRegister)]
        public TipoRegisterModbus RegisterType { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        public ActionVariable SendValue { get; set; } = new ActionVariable();

        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDevice;
            var value = Convert.ToInt32(SendValue.Value);
            Context.ResultList.Add(string.Format("{0} - WriteInt32  2 Registers Address {1} -> {2}", name, Address, value));
            Logger.InfoFormat("{0} - WriteInt32 2 Registers Address {1} -> {2}", name, Address, value);
            uut.WriteInt32(Address, value);
        }
    }

    public class WriteInt32ActionDescription : ActionDescription
    {
        public override string Name { get { return "WriteInt32"; } }
        public override string Description { get { return "Escribe un entero de 32 bits (dos registros modbus)"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitModbusDeviceAction;DisposedModbusDeviceAction:2"; } }
    }
}
