﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.17)]
    public class CVMC5Test : TestBase
    {
        private CVMC5 cvm;
        public CVMC5 Cvm
        {
            get
            {
                if (cvm == null)
                    cvm = new CVMC5(Comunicaciones.SerialPort);

                return cvm;
            }
        }

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }


        }

        string CAMERA_IDS;

        private double transformationRelation;

        public void TestInitialization(int port)
        {
            Cvm.Modbus.PortCom = port == 0 ? Comunicaciones.SerialPort : port;
            Cvm.Modbus.PerifericNumber = Comunicaciones.Periferico;

            Tower.Active24VDC();
            Tower.ActiveElectroValvule();

            Tower.PowerSourceIII.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.5, ParamUnidad.PorCentage);
            Tower.PowerSourceIII.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 35000, ParamUnidad.ms);
        }

        public void TestConsumption()
        {
            var supplyvoltage = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            var IsDC = supplyvoltage.Contains("Vdc");

            var configuration = new TestPointConfiguration()
            {
                Current = 0.25,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = IsDC == true ? TypePowerSource.LAMBDA : TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(Tower, configuration);
        }

        public void TestCommunications()
        {
            try
            {
                SamplerWithCancel((p) =>
                {
                    Cvm.FlagTest();
                    return true;
                }, "Error: No se puede comunicar con el equipo.", 3, 500, 0, false, false);
            }
            catch
            {
                Logger.Warn("Cambio de comunicaciones a 19200, recuperación equipo.");
                Cvm.Modbus.BaudRate = 19200;
                Delay(1000, "");

                SamplerWithCancel((p) =>
                {
                    Cvm.FlagTest();
                    return true;
                }, "Error: No se puede comunicar con el equipo.", 3, 500, 0, false, false);
            }

            var firmwareVersion = Cvm.ReadFirmwareVersion().CToNetString().Trim(); 

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de firmware del equipo incorrecta"), ParamUnidad.SinUnidad);

            var codeROM = Cvm.ReadROMCode();

            Assert.AreEqual(ConstantsParameters.Identification.ROM_CODE, codeROM, Identificacion.ROM_CODE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de firmware del equipo incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestDefaultParameters()
        {
            Cvm.FlagTest(true);

            if (Comunicaciones.SerialPort == 7)
            {
                Cvm.WriteCommunicationConfiguration(1, 19200);
                Cvm.Modbus.BaudRate = 19200;

                ResetPowerSource();

                Cvm.FlagTest(true);
            }
                
            transformationRelation = Configuracion.GetDouble("RELACION_TRANSFORMACION", 1, ParamUnidad.SinUnidad);

            CVMC5.SetupConfiguration setupConfiguration = new CVMC5.SetupConfiguration((int)Configuracion.GetDouble("TENSION_PRIMARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES", "SIMPLE", ParamUnidad.SinUnidad),
                (CVMC5.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO", 0, ParamUnidad.SinUnidad),
                (CVMC5.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            Cvm.WriteSetupConfiguration(setupConfiguration);

            CVMC5.MaximumDemandConfiguration maximumDemandConfiguration = new CVMC5.MaximumDemandConfiguration()
            {
                VariabletoCalculate = Configuracion.GetString("VARIABLE_MAXIMA_DEMANDA", "NO_PD", ParamUnidad.SinUnidad),
                RegisterTime = (ushort)Configuracion.GetDouble("TIEMPO_REGISTRO_MAXIMA_DEMANDA", 15, ParamUnidad.SinUnidad)
            };

            Cvm.WriteMaximumDemandConfiguration(maximumDemandConfiguration);

            var hardwareVector = new CVMC5.HardwareVector();
            hardwareVector.Comunication = VectorHardware.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            hardwareVector.Rs232 = VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.In_Medida = VectorHardware.GetString("MEDIDA_NEUTRO", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Rele = VectorHardware.GetString("RELE", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "5A", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "300V", ParamUnidad.SinUnidad);
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "CVM-C5", ParamUnidad.SinUnidad);
            hardwareVector.CalculoHarmonicos = VectorHardware.GetString("CALCULO_HARMONICOS", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            SetVariable("VectorHardware", hardwareVector);

            Cvm.WriteHardwareVector(hardwareVector);

            string presetHW = VectorHardware.GetString("PRECONFIGURACION_VECTOR_HARDWARE", "NO", ParamUnidad.SinUnidad).ToUpper().Trim();
            if (presetHW != "NO")
                Cvm.WritePresetHardware(UInt16.Parse(presetHW));
            Resultado.Set("PRECONFIGURACION_VECTOR_HARDWARE", presetHW, ParamUnidad.SinUnidad);

            var bloqueoSetup = Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad) == "NO" ? false : true;

            transformationRelation = hardwareVector.InputCurrent == CVMC5.VectorHardwareCurrentInputs.MC ? 20 : 1;

            CVMC5.CalibrationFactors DefaultGain = new CVMC5.CalibrationFactors((ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO", 25004, ParamUnidad.Puntos), (ushort)Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 13000, ParamUnidad.Puntos));
            Cvm.WriteCalibrationFactors(DefaultGain);

            Cvm.WritePhaseGapCalibrationFactors(new CVMC5.PhaseGapFactors((ushort)Configuracion.GetDouble(Params.GAIN_DESFASE.Null.TestPoint("DEFECTO").Name, 0, ParamUnidad.SinUnidad)));

            CVMC5.PasswordConfiguration passwordConfiguration = new CVMC5.PasswordConfiguration()
            {
                Password = (ushort)Configuracion.GetDouble("CONTRASEÑA", 1234, ParamUnidad.SinUnidad),
                SetupLocking = (Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI") ? (byte)1 : (byte)0
            };
            Cvm.WritePasswordInformation(passwordConfiguration);

            Cvm.WriteInitialMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));

            Cvm.WriteSerialNumber((uint)Configuracion.GetDouble("NUMERO_SERIE_TEST", 12345678, ParamUnidad.SinUnidad));

            CVMC5.AlarmConfiguration alarmConfiguration = new CVMC5.AlarmConfiguration()
            {
                Delay = (ushort)Configuracion.GetDouble("RETRASO_ALARMA", 0, ParamUnidad.SinUnidad),
                MaximumValue = (int)Configuracion.GetDouble("VALOR_MAXIMO_ALARMA", 0, ParamUnidad.SinUnidad),
                MinimumValue = (int)Configuracion.GetDouble("VALOR_MINIMO_ALARMA", 0, ParamUnidad.SinUnidad),
                VariableNumber = (byte)Configuracion.GetDouble("VARIABLE_ALARMA", 0, ParamUnidad.SinUnidad),
            };

            Cvm.WriteAlarmConfiguration(alarmConfiguration);

            Cvm.WriteClearAll();

            Cvm.WriteClearEnergy();

            Cvm.WriteClearMaximumMinimums();

            ResetPowerSource();

            var eepromError = Cvm.ReadEEPROMErrorCodes();

            Assert.AreEqual(eepromError, CVMC5.EEPROMErrorCodes.NO_ERROR, Error().UUT.EEPROM.EXTERNAL_MEMORY(string.Format("Error de EEPROM, el equipo informa del siguiente error {0}", eepromError.ToString())));

            var energyReadings = Cvm.ReadEnergies().ComprobarBorrado();

            Assert.IsTrue(energyReadings, Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error de energias, se ha registrado energia despues de borrarlas y reiniciar el equipo"));

            var vector = Cvm.ReadHardwareVector();
            var vectorHardwareBBDD = GetVariable<CVMC5.HardwareVector>("VectorHardware", vector);

            vector.ToSingleCoil();
            vectorHardwareBBDD.ToSingleCoil();

            Assert.AreEqual(vector.VectorHardwareBoolean, vectorHardwareBBDD.VectorHardwareBoolean, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error. El vector de hardware grabado no coincide con el grabado"));

            CVMC5.PasswordConfiguration ReadPasswordConfiguration = Cvm.ReadPasswordInformation();

            Assert.IsTrue(passwordConfiguration.Equals(ReadPasswordConfiguration), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion de password leida no es la misma que la de BBDD"));

            Resultado.Set("CONTRASEÑA", ReadPasswordConfiguration.Password.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("BLOQUEO_SETUP", ReadPasswordConfiguration.SetupLocking.ToString(), ParamUnidad.SinUnidad);

            CVMC5.MaximumDemandConfiguration ReadMaximumDemandConfiguration = Cvm.ReadMaximumDemandConfig();

            Assert.IsTrue(maximumDemandConfiguration.Equals(ReadMaximumDemandConfiguration), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion de password leida no es la misma que la de BBDD"));

            Resultado.Set("TIEMPO_REGISTRO_MAXIMA_DEMANDA", ReadMaximumDemandConfiguration.RegisterTime.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("VARIABLE_MAXIMA_DEMANDA", ReadMaximumDemandConfiguration.VariabletoCalculate, ParamUnidad.SinUnidad);

            CVMC5.SetupConfiguration ReadSetupConfiguration = Cvm.ReadSetupConfiguration();

            Assert.IsTrue(setupConfiguration.Equals(ReadSetupConfiguration), Error().SOFTWARE.BBDD.CONFIGURACION_INCORRECTA("La configuracion de password leida no es la misma que la de BBDD"));

            Resultado.Set("TENSION_PRIMARIO", ReadSetupConfiguration.VoltagePrimary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TENSION_SECUNDARIO", ReadSetupConfiguration.VoltageSecondary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CORRIENTE_PRIMARIO", ReadSetupConfiguration.CurrentPrimary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TIPO_TENSIONES", ReadSetupConfiguration.VoltageType.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_PRINCIPAL_DEFECTO", ReadSetupConfiguration.DefaultPage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CALCULO_HARMONICOS_SETUP_DEFECTO", ReadSetupConfiguration.CalculateHarmonics.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TIEMPO_DESCONEXION_BACKLIGHT", ReadSetupConfiguration.BacklightShutdownTime.ToString(), ParamUnidad.SinUnidad);

            CVMC5.HardwareVector ReadHardwareVector = Cvm.ReadHardwareVector();

            Resultado.Set("COMUNICACIONES", ReadHardwareVector.ComunicationString, ParamUnidad.SinUnidad);
            Resultado.Set("RS232", ReadHardwareVector.Rs232String, ParamUnidad.SinUnidad);
            Resultado.Set("SHUNT", ReadHardwareVector.ShuntString, ParamUnidad.SinUnidad);
            Resultado.Set("MEDIDA_NEUTRO", ReadHardwareVector.In_MedidaString, ParamUnidad.SinUnidad);
            Resultado.Set("RELE", ReadHardwareVector.ReleString, ParamUnidad.SinUnidad);
            Resultado.Set("ALIMENTACION", ReadHardwareVector.PowerSupply.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_CORRIENTE", ReadHardwareVector.InputCurrent.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("ENTRADA_TENSION", ReadHardwareVector.InputVoltage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("MODELO", ReadHardwareVector.Modelo.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CALCULO_HARMONICOS", ReadHardwareVector.CalculoHarmonicosString, ParamUnidad.SinUnidad);

            Resultado.Set(ConstantsParameters.Identification.VECTOR_HARDWARE, ReadHardwareVector.VectorHardwareTrama, ParamUnidad.SinUnidad);

            Cvm.WriteClearAll();
        }

        public void TestDisplay()
        {
            var timeWait = 800;

            var version = Configuracion.GetString("VERSION_DISPLAY", "V2", ParamUnidad.SinUnidad);

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            Cvm.FlagTest(true);

            Cvm.WriteBacklightState(true, true);

            Cvm.SendDisplayInfo(CVMC5.DisplayState.EVEN_SEGMENTS, version);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, string.Format("CVMC5_EVEN_{0}", version), string.Format("EVEN_{0}", version), "CVMC5");

            Cvm.SendDisplayInfo(CVMC5.DisplayState.ODD_SEGMENTS, version);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, string.Format("CVMC5_ODD_{0}", version), string.Format("ODD_{0}", version), "CVMC5");
        }

        public void TestKeyboard()
        {
            Dictionary<CVMC5.KeyboardKeys, int> keysActuators = new Dictionary<CVMC5.KeyboardKeys,int>()
            {
                {CVMC5.KeyboardKeys.DISPLAY,11},
                {CVMC5.KeyboardKeys.MAX, 10},
                {CVMC5.KeyboardKeys.MIN, 9},
                {CVMC5.KeyboardKeys.RESET,12}
            };

            Tower.IO.DO.Off(keysActuators[CVMC5.KeyboardKeys.DISPLAY], keysActuators[CVMC5.KeyboardKeys.MAX], keysActuators[CVMC5.KeyboardKeys.MIN], keysActuators[CVMC5.KeyboardKeys.RESET]);

            Assert.IsTrue(Cvm.ReadKeyboard().Equals(CVMC5.KeyboardKeys.NONE),Error().UUT.CALIBRACION.YA_DISPARADO("Error en el teclado: Se detecta tecla pulsada antes de empezar el test"));

            foreach (KeyValuePair<CVMC5.KeyboardKeys, int> keyboard in keysActuators)
            {
                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OnWait(150, keyboard.Value);

                    var result = keyboard.Key == Cvm.ReadKeyboard();

                    Resultado.Set(keyboard.Key.ToString(), result == true ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                    return result;

                }, string.Format("Error tecla {0} no detectada al pulsarla", keyboard.Key.GetDescription()));

                SamplerWithCancel((p) =>
                {
                    Tower.IO.DO.OffWait(150, keyboard.Value);

                    var result = Cvm.ReadKeyboard() == CVMC5.KeyboardKeys.NONE;

                    Resultado.Set(keyboard.Key.ToString(), result == true ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                    return result ;

                }, string.Format("Error tecla {0} encallada al desactivarla", keyboard.Key.GetDescription()));

            }
        }

        public void TestTarifa()
        {
            if (VectorHardware.GetString("RS232", "NO", ParamUnidad.SinUnidad).ToUpper() == "NO")
            {
                Cvm.WriteTariffSelection(CVMC5.Tariff.FARE_1);

                Delay(1000, "");

                var tariff = (ushort)Cvm.ReadActiveTariff();

                Assert.AreEqual(tariff, (ushort)CVMC5.Tariff.FARE_1, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error, no se ha grabado bien la tarifa 1"));

                Resultado.Set("TARIFA_1", "OK", ParamUnidad.SinUnidad);

                Cvm.WriteTariffSelection(CVMC5.Tariff.FARE_2);

                Delay(1000, "");

                tariff = (ushort)Cvm.ReadActiveTariff();

                Assert.AreEqual(tariff, (ushort)CVMC5.Tariff.FARE_2, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error, no se ha grabado bien la tarifa 2"));                

                Resultado.Set("TARIFA_2", "OK", ParamUnidad.SinUnidad);
            }
            else
            {
                Tower.IO.DO.Write(18, false);

                Delay(1000, "");

                var tariff = (ushort)Cvm.ReadActiveTariff();

                Assert.AreEqual(tariff.ToString(), CVMC5.Tariff.FARE_1, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error, no se ha grabado bien la tarifa 1"));

                Resultado.Set("TARIFA_1", "OK", ParamUnidad.SinUnidad);

                Tower.IO.DO.Write(18, true);

                Delay(1000, "");

                tariff = (ushort)Cvm.ReadActiveTariff();

                Assert.AreEqual(tariff, CVMC5.Tariff.FARE_2, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error, no se ha grabado bien la tarifa 2"));

                Resultado.Set("TARIFA_2", "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestInputOutput()
        {
            var peripheral = VectorHardware.GetString("RELE", "SI", ParamUnidad.SinUnidad).ToUpper();
            if (peripheral == "SI")
            {
                Cvm.WriteOutput(CVMC5.State.ON);

                Assert.IsTrue(Tower.IO.DI.Read(9).Value, Error().UUT.HARDWARE.NO_COMUNICA("Error de salida del equipo, no se detecta la activacion de la salida del equipo"));

                Cvm.WriteOutput(CVMC5.State.OFF);

                Assert.IsFalse(Tower.IO.DI.Read(9).Value, Error().UUT.HARDWARE.NO_COMUNICA("Error de salida del equipo, no se detecta la desactivacion de la salida del equipo"));

                Resultado.Set("RELE_SALIDA", "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            Tower.IO.DO.On(24);
            Tower.IO.DO.On(25);
            Tower.IO.DO.On(26);
            Tower.IO.DO.On(27);
            Cvm.FlagTest(true);

            var offsetsV = new TriAdjustValueDef(Params.V.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.V.Null.CrucePistas.TestPoint("OFFSETS").Min(), Params.V.Null.CrucePistas.TestPoint("OFFSETS").Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.I.Null.CrucePistas.TestPoint("OFFSETS").Min(), Params.I.Null.CrucePistas.TestPoint("OFFSETS").Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = Cvm.ReadPhaseVariables();
                    return new double[] { vars.L1.Tension / 10D, vars.L2.Tension / 10D, vars.L3.Tension / 10D,
                        vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D};

                }, delFirts, samples, timeInterval);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = Cvm.ReadPhaseVariables();

                    return new double[] { vars.L1.Tension / 10D, vars.L2.Tension/ 10D, vars.L3.Tension/ 10D,
                        vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D};
                }, delFirts, samples, timeInterval);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestAdjustVoltageCurrent(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var measureGains = Cvm.CalculateVoltageCurrentCalibrationFactors(delFirts, initCount, samples, timeInterval, realVoltageCurrent.Voltage, realVoltageCurrent.Current, transformationRelation,
                (value) =>
                {
                    triGains[0].L1.Value = value.VoltageL1;
                    triGains[0].L2.Value = value.VoltageL2;
                    triGains[0].L3.Value = value.VoltageL3;

                    triGains[1].L1.Value = value.CurrentL1;
                    triGains[1].L2.Value = value.CurrentL2;
                    triGains[1].L3.Value = value.CurrentL3;

                    return !HasError(triGains, false).Any();
                });

            triGains.ForEach(x => x.AddToResults(Resultado));

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            Cvm.FlagTest(true);
            Cvm.WriteCalibrationFactors(measureGains.Item2);

        }

        public void TestPhaseGapAdjust(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            Cvm.FlagTest(true);

            Cvm.WriteRecoverCalibrationValuesFromEEPROM();

            var realVoltageCurrent = internalTestAdjustVerificationPhaseGap();

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
            var power = TriLinePower.Create(realVoltageCurrent.Voltage, realVoltageCurrent.Current * transformationRelation, realVoltageCurrent.powerFactor);

            var offsetGains = Cvm.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, power.KW, realVoltageCurrent.powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                });

            triDefs.AddToResults(Resultado);

            if (!offsetGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de desfase fuera de margenes"));

            Cvm.FlagTest(true);
            Cvm.WritePhaseGapCalibrationFactors(offsetGains.Item2);
        }

        public void TestVerification(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
             Cvm.FlagTest(true);

            Cvm.WriteRecoverCalibrationValuesFromEEPROM();

            var realVoltageCurrent = internalTestAdjustVerificationPhaseGap();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            // Frec
            var param = new AdjustValueDef(Params.FREQ.L1.Verificacion.Name, 0, Params.FREQ.L1.Verificacion.Min(), Params.FREQ.L1.Verificacion.Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(param,
            (step) =>
            {
                return Cvm.ReadFrecuencia().frecuencia / 10D;
            }, countSamples, maxSamples, timeInterval);

            // VCompuestas
            var defs2 = new TriAdjustValueDef(Params.V.L12.Verificacion.Name, 0, 0, realVoltageCurrent.Voltage.L1*Math.Sqrt(3), Params.V.L12.Verificacion.Tol(), ParamUnidad.V);

            TestMeasureBase(defs2,
            (step) =>
            {
                var vars = Cvm.ReadTensionesCompuestas();
                var varsRead = new List<double>()
                        {
                            vars.V1V2 / 10D, vars.V2V3 / 10D, vars.V3V1 / 10D
                        };
                return varsRead.ToArray();
            }, countSamples, maxSamples, timeInterval);

            //I,V y Pot
            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A));
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W));

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = Cvm.ReadPhaseVariables();
                    var varsRead = new List<double>()
                            {
                               vars.L1.Tension / 10D, vars.L2.Tension/ 10D, vars.L3.Tension/ 10D,
                               vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D,
                               vars.L1.PotenciaActiva, vars.L2.PotenciaActiva, vars.L3.PotenciaActiva,
                            };
                    return varsRead.ToArray();
                },
                () =>
                {
                    var voltageRef = Tower.PowerSourceIII.ReadVoltage();
                    var currentRef = Tower.PowerSourceIII.ReadCurrent();

                    var varsList = new List<double>()
                    {
                       voltageRef.L1, voltageRef.L2, voltageRef.L3,
                       currentRef.L1 * transformationRelation, currentRef.L2 * transformationRelation, currentRef.L3 *transformationRelation
                    };

                    var power = TriLinePower.Create(voltageRef, currentRef * transformationRelation, realVoltageCurrent.powerFactor);
                    varsList.AddRange(power.KW.ToArray());
                    return varsList.ToArray();

                }, countSamples, maxSamples, timeInterval);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public void TestCustomization()
        {
            Cvm.FlagTest();

            CVMC5.SetupConfiguration setupConfiguration = new CVMC5.SetupConfiguration((int)Configuracion.GetDouble("TENSION_PRIMARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES", "SIMPLE", ParamUnidad.SinUnidad),
                (CVMC5.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO", 0, ParamUnidad.SinUnidad),
                (CVMC5.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            CVMC5.SetupConfiguration setupConfigurationCliente = new CVMC5.SetupConfiguration
                ((int)Configuracion.GetDouble("TENSION_PRIMARIO_CLIENTE", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO_CLIENTE", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO_CLIENTE", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES_CLIENTE", "SIMPLE", ParamUnidad.SinUnidad),
                (CVMC5.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO_CLIENTE", 0, ParamUnidad.SinUnidad),
                (CVMC5.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO_CLIENTE", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO_CLIENTE", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            if (!setupConfiguration.Equals(setupConfigurationCliente))
                Cvm.WriteSetupConfiguration(setupConfiguration);

            Resultado.Set("TENSION_PRIMARIO", setupConfigurationCliente.VoltagePrimary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TENSION_SECUNDARIO", setupConfigurationCliente.VoltageSecondary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CORRIENTE_PRIMARIO", setupConfigurationCliente.CurrentPrimary.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TIPO_TENSIONES", setupConfigurationCliente.VoltageType.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("PANTALLA_PRINCIPAL_DEFECTO", setupConfigurationCliente.DefaultPage.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("CALCULO_HARMONICOS_SETUP_DEFECTO", setupConfigurationCliente.CalculateHarmonics.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("TIEMPO_DESCONEXION_BACKLIGHT", setupConfigurationCliente.BacklightShutdownTime.ToString(), ParamUnidad.SinUnidad);

            Cvm.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));

            Cvm.WriteTariffSelection(CVMC5.Tariff.FARE_1);

            Cvm.WriteClearEnergy();

            SamplerWithCancel((p) =>
            {
                Cvm.WriteClearEnergy();
                CVMC5.Energies energies = Cvm.ReadEnergies();
                return energies.ComprobarBorrado();
            }, "Error: No se han podido borrar las energias", 3, 200);


            int baudRate = (int)Configuracion.GetDouble("BAUDRATE_CLIENTE", 9600, ParamUnidad.Numero);
            byte periferic = (byte)Configuracion.GetDouble("PERIFERICO_CLIENTE", 1, ParamUnidad.Numero);

            Cvm.WriteCommunicationConfiguration(periferic, baudRate);

            Cvm.Modbus.BaudRate = baudRate;
            Cvm.Modbus.PerifericNumber = periferic;

            ResetPowerSource();

            var serialReading = Cvm.ReadSerialNumber();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, serialReading.ToString(), TestInfo.NumSerie, Error().UUT.CONFIGURACION.PARAMETROS_NO_GRABADOS("Error en la memoria, el numero de serie no se ha grabado correctamente"), ParamUnidad.SinUnidad);

            var tariff = (ushort)Cvm.ReadActiveTariff();

            Assert.AreEqual(tariff, (ushort)CVMC5.Tariff.FARE_1, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error, no se ha detectado el equipo con la TARIFA 1"));

            Resultado.Set("TARIFA_FINAL", tariff.ToString(), ParamUnidad.SinUnidad);

            var energyReading = Cvm.ReadEnergies();

            Assert.IsTrue(energyReading.ComprobarBorrado(), Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error: las energias no son 0 despues del reset"));

            Resultado.Set("BORRADO_DE_ENERGIAS", "OK", ParamUnidad.SinUnidad);

            CVMC5.EEPROMErrorCodes eepromError = CVMC5.EEPROMErrorCodes.ALARM_NOT_INITIALIZED;

            SamplerWithCancel((p) =>
            {
                eepromError = Cvm.ReadEEPROMErrorCodes();
                return eepromError == CVMC5.EEPROMErrorCodes.NO_ERROR;
         
            }, string.Format("Error de EEPROM, el equipo informa del siguiente error {0}", eepromError.ToString()), 3, 1000, 1000, false, false);

        }

        public void TestFinish()
        {
            if (Cvm != null)
                Cvm.Dispose();

            if (Tower != null)
                Tower.Dispose();          
        }

        internal void ResetPowerSource()
        {          
            Delay(1000, "Esperando finalizacion de tareas del equipo");

            Tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(1000, "esperando apagado del equipo");

            Tower.Chroma.ApplyAndWaitStabilisation();

            Delay(3000, "Estabilización comunicaciones del equipo");

            SamplerWithCancel((p) => { Cvm.FlagTest(); return true; }, "Error. No se puede establecer comunicación con el equipo despues del reset", 10, 500);
        }

        private struct VoltageCurrentReading
        {
            public TriLineValue Voltage { get; set; }
            public TriLineValue Current { get; set; }
            public double powerFactor { get; set; }
        };

        private VoltageCurrentReading internalTestAdjustVerificationVoltageCurrent()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A),
                L2 = Consignas.GetDouble(Params.I.L2.Ajuste.Name, 5, ParamUnidad.A),
                L3 = Consignas.GetDouble(Params.I.L3.Ajuste.Name, 5, ParamUnidad.A),
            };

            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var currentReadings = Tower.PowerSourceIII.ReadCurrent();
            var voltageReadings = Tower.PowerSourceIII.ReadVoltage();

            var realAdjustCurrents = new TriLineValue()
            {
                L1 = currentReadings.L1 ,
                L2 = currentReadings.L2 ,
                L3 = currentReadings.L3 ,
            };

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(realAdjustCurrents.L1, realAdjustCurrents.L2, realAdjustCurrents.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        private VoltageCurrentReading internalTestAdjustVerificationPhaseGap()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 250, ParamUnidad.V);
            var adjustCurrent = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.AjusteDesfase.Name, 5, ParamUnidad.A),
                L2 = Consignas.GetDouble(Params.I.L2.AjusteDesfase.Name, 5, ParamUnidad.A),
                L3 = Consignas.GetDouble(Params.I.L3.AjusteDesfase.Name, 5, ParamUnidad.A)
            };

            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 45, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var currentReadings = Tower.PowerSourceIII.ReadCurrent();
            var voltageReadings = Tower.PowerSourceIII.ReadVoltage();

            var realAdjustCurrents = new TriLineValue()
            {
                L1 = currentReadings.L1,
                L2 = currentReadings.L2,
                L3 = currentReadings.L3,
            };

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(realAdjustCurrents.L1, realAdjustCurrents.L2, realAdjustCurrents.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        public void BorradoEnergia()
        {
            Cvm.FlagTest();

            Cvm.WriteClearEnergy();

            Delay(500, "");

            Assert.IsTrue(Cvm.ReadEnergies().ComprobarBorrado(), Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error en el borrado de energias"));

            Resultado.Set("BORRADO_ENREGIA", "OK", ParamUnidad.SinUnidad);
        }
    }
}
