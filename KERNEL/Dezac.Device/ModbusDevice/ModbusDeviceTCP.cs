﻿using Comunications.Message;
using log4net;
using System.Net.Sockets;

namespace Dezac.Device
{
    public class ModbusDeviceTCP : ModbusDevice
    {
        private string hostName;
        private int port;

        private TcpClient tcp;

        public ModbusDeviceTCP(string hostName, int port, ModbusTCPProtocol modbusTCPProtocol, ILog _logger = null)
            : base()
        {
            this.hostName = hostName;
            this.port = port;

            tcp = new TcpClient(hostName, port);

            var adapter = new TcpClientAdapter(tcp);

            Transport = modbusTCPProtocol == ModbusTCPProtocol.RTU ?
                new ModbusTransport(adapter, _logger) :
                new TcpModbusTransport(adapter, _logger);
        }

        public int Port
        {
            get { return port; }
            set
            {
                if (port == value)
                    return;

                port = value;

                tcp.Close();
                tcp = new TcpClient(hostName, port);
            }
        }

        public bool IsConnected
        {
            get { 
                if(tcp!=null)
                    return tcp.Connected;

                return false;
            }
        }

        public void Connect()
        {
            if (tcp != null)
            {
                tcp.Close();
                tcp.Connect(hostName, port);
            }
            else
                tcp = new TcpClient(hostName, port);
        }

        public void Close()
        {
            if (tcp != null)
                try
                {
                    tcp.Close();
                    tcp = null;
                }
                catch
                {

                }
        }

        public string HostName
        {
            get
            {
                return hostName;
            }
            set
            {
                if (hostName != value)
                {
                    hostName = value;
                    Close();
                    tcp = new TcpClient(hostName, port);
                }
            }
        }

        public override void ClosePort()
        {
            Close();
        }

        public override void Dispose()
        {
            Close();
        }
    }

    public enum TipoTransporte
    {
        SERIAL_PORT = 0,
        ETHERNET = 1,
    }

    public enum ModbusTCPProtocol
    {
        RTU = 0,
        TCP = 1
    }
}
