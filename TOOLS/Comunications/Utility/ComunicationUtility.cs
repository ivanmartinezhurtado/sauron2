﻿using System;
using System.Net;
using System.Text;

namespace Comunications.Utility
{
    public static class ComunicationUtility
    {
      public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

      public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

      public  static T[] Redim<T>(T[] arr, bool preserved, int nbRows)
        {
            T[] arrRedimed = new T[nbRows];
            if (preserved)
                for (int i = 0; i < arr.Length; i++)
                    arrRedimed[i] = arr[i];
            return arrRedimed;
        }

      public static T[] Redim<T>(T[] arr, bool preserved, T[] arr2)
      {
          int LonSum = (arr.Length + arr2.Length);
          T[] arrRedimed = new T[LonSum];
          if (preserved)
              for (int i = 0; i < LonSum; i++)
                  if (i < arr.Length)
                      arrRedimed[i] = arr[i];
                  else
                      arrRedimed[i] = arr2[(i - arr.Length)];
          return arrRedimed;
      }

      public static byte[] RunCrc_crc16Calc(byte[] data)
      {
          if (data == null)
              throw new ArgumentNullException("data");

          ushort crc = ushort.MaxValue;

          foreach (byte b in data)
              crc = crc_1021(crc, b);
          ;

          return BitConverter.GetBytes(IPAddress.NetworkToHostOrder((short)crc));
      }

      private static ushort crc_1021(ushort old_crc, byte data)
      {
          int crc;
          int x;

          x = (((old_crc >> 8) ^ data) & 0xFF);
          x ^= x >> 4;
          crc = (old_crc << 8) ^ (x << 12) ^ (x << 5) ^ x;
          crc &= 0xFFFF;
          return (ushort)crc;
      }

        public static byte CheckSum(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            int crc = 0;

            foreach (byte b in data)
                crc += b;
            ;

            crc = (byte)Math.IEEERemainder(crc, 256);

            return (byte)crc;
        }


        public static byte CheckSumMidabus(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            int crc = 0;

            var asc = ASCIIEncoding.ASCII.GetChars(data);

            for (byte b = 0; b < data.Length - 1; b += 2)
            {
                var crc_aux = 0;
                crc_aux <<= 4;
                int b_aux = Convert.ToInt32(asc[b].ToString(), 16);
                crc_aux += b_aux;
                crc_aux <<= 4;
                b_aux = Convert.ToInt32(asc[b + 1].ToString(), 16);
                crc_aux += b_aux;
                crc += crc_aux;
            }

            return (byte)crc;
        }

       
    }
}
