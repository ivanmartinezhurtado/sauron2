﻿using Dezac.Tests.Model;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Protection
{  

    public class UtillajeViejoEkorPlus : IUtillajeEkor
    {
        internal const int PISTONS_REAR = 5;
        internal const int PISTONS_FRONT = 6;

        private Tower3 tower { get; set; }
        private TestBase testBase { get; set; }

        public UtillajeViejoEkorPlus(TestBase testBase, ITower tower)
        {
            this.tower = (Tower3)tower;
            this.testBase = testBase;
        }

        public void ActivatePistonRS232()
        {
            tower.IO.DO.OnWait(200, PISTONS_FRONT);
        }

        public void DesactivatePistonRS232()
        {
            tower.IO.DO.OffWait(200, PISTONS_FRONT);
        }

        public void TestDeviceConnection()
        {
            tower.IO.DO.On(4);
            tower.IO.DO.OnWait(500, PISTONS_REAR);
        }

        public void TestDeviceDisconnection()
        {
            tower.IO.DO.OffWait(500, PISTONS_REAR);
            tower.IO.DO.Off(4);
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairInputs()
        {
            throw new NotImplementedException();
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairOutputs()
        {
            throw new NotImplementedException();
        }

        public byte GetOutActivateVL()
        {
            throw new NotImplementedException();
        }

        public byte GetOutActivateVF()
        {
            throw new NotImplementedException();
        }

        public List<KeyValuePair<byte, byte>> GetKeyValuePairKeyBoard()
        {
            throw new NotImplementedException();
        }

        public void SupplyUSB(bool state = true)
        {           
        }

        public int GetInputDetectionTrigger()
        {
            throw new NotImplementedException();
        }

        public int GetOutputMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public int GetOutputMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public int GetOutputMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public int GetOutCLK()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public void ActivateMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL1()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL2()
        {
            throw new NotImplementedException();
        }

        public void DesactivateMeasurePasatapasL3()
        {
            throw new NotImplementedException();
        }

        public void ResetUSB()
        {
            throw new NotImplementedException();
        }

        public void ActivatePistonTestKeyboard()
        {
        }

        public void DesactivatePistonTestKeyboard()
        {
        }
    }
}
