﻿using Dezac.Device;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.UserControls;
using System;
using System.Windows.Forms;
using Dezac.Tests.Services;
using Instruments.Towers;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.05)]
    public class READWATTTest : TestBase
    {
        private READWATT readwatt;
        private Tower3 tower;

        public void TestInitialization()
        {
            readwatt = new READWATT(Comunicaciones.SerialPort);
            readwatt.Modbus.PerifericNumber = 0; 
            ((ModbusDeviceSerialPort)readwatt.Modbus).RtsEnable = true;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
        }

        public void TestComunicationsRS232()
        {
            if (readwatt.Modbus.IsOpen)
                readwatt.Modbus.ClosePort();

            Shell.MsgBox(string.Format("TEST CONFIGURADO CON EL COM {0} PARA EL RS232", Comunicaciones.SerialPort), "PUERTO RS232", MessageBoxButtons.OK);
              
            byte portRs232;
            try
            {
                portRs232 = Comunicaciones.SerialPort;
                readwatt.Modbus.PortCom = portRs232;
                SamplerWithCancel((p) =>
                {
                    try
                    {
                        readwatt.WriteModbusAddress(Comunicaciones.Periferico);
                        readwatt.Modbus.PerifericNumber = Comunicaciones.Periferico;
                        return true;
                    }
                    catch
                    {
                        readwatt.Modbus.PerifericNumber = Comunicaciones.Periferico;
                        readwatt.ReadSoftwareVersion();
                        return true;
                    }
                }, "Error comunicaciones, no hemos recibido respuesta del equipo", 2, 500, 500, false, false);
            }
            catch (Exception)
            {         
                    var usercontrolFluke = new InputKeyBoard("Introduzca el puerto del RS232 al que esta conectado el util del READWATT", "99");
                    Shell.ShowDialog("PUERTO RS232", () => usercontrolFluke, MessageBoxButtons.OKCancel);
                    portRs232 = Convert.ToByte(usercontrolFluke.TextValue);
            }

            readwatt.Modbus.PortCom = (byte)portRs232;
            TestComunications();
        }

        public void TestComunicationsRS485()
        {
            if (readwatt.Modbus.IsOpen)
                readwatt.Modbus.ClosePort();

            Shell.MsgBox(string.Format("TEST CONFIGURADO CON EL COM {0} PARA EL RS485", Comunicaciones.SerialPort2), "PUERTO RS485", MessageBoxButtons.OK);

            byte portRs485;
            try
            {
                portRs485 = Comunicaciones.SerialPort2;
                readwatt.Modbus.PortCom = portRs485;
                SamplerWithCancel((p) =>
                {
                    readwatt.Modbus.PerifericNumber = Comunicaciones.Periferico;
                    readwatt.ReadSoftwareVersion();
                    return true;
                }, "Error comunicaciones, no hemos recibido respuesta del equipo", 2, 500, 500, false, false);
            }
            catch (Exception)
            {
                var usercontrolFluke = new InputKeyBoard("Introduzca el puerto del RS485 al que esta conectado el util del READWATT", "99");
                Shell.ShowDialog("PUERTO RS485", () => usercontrolFluke, MessageBoxButtons.OKCancel);
                portRs485 = Convert.ToByte(usercontrolFluke.TextValue);
            }

            readwatt.Modbus.PortCom = (byte)portRs485;
            TestComunications();
        }

        private void TestComunications()
        {
            SamplerWithCancel((p) =>
            {
                try
                {
                    readwatt.WriteModbusAddress(Comunicaciones.Periferico);
                    readwatt.Modbus.PerifericNumber = Comunicaciones.Periferico;
                    return true;
                }
                catch
                {
                    readwatt.Modbus.PerifericNumber = Comunicaciones.Periferico;
                    readwatt.ReadSoftwareVersion();
                    return true;
                }
            }, "Error comunicaciones, no hemos recibido respuesta del equipo", 2, 500, 500, false, false);

            var version = readwatt.ReadSoftwareVersion();

            string[] versionBBDD = Identificacion.VERSION_FIRMWARE.Split('.');

            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Revision.ToString(), ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

            Assert.IsTrue(version.Major == Convert.ToUInt16(versionBBDD[0]) && version.Minor == Convert.ToUInt16(versionBBDD[1]) && version.Revision == Convert.ToUInt16(versionBBDD[2]), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, Versión de Firmware del equipo diferente a la de la BBDD"));

            readwatt.WriteBastidor((int)TestInfo.NumBastidor.Value);

            readwatt.WriteCodeError(9999);

            readwatt.Modbus.ClosePort();

            readwatt.WriteMaxPulseTime(1500);

            readwatt.WriteMinPulseTime(2);

            readwatt.WriteDivPulse(2);

            readwatt.WriteRatioPulse(1);

            readwatt.WriteTransformRatioPulse(1);
        }

        public void TestCostumize()
        {
            readwatt.WriteNumSerie(Convert.ToInt32(TestInfo.NumSerie));

            readwatt.WriteMaxPulseTime((ushort)Parametrizacion.GetDouble("MAX_PULSE_TIME", 250, ParamUnidad.s));

            readwatt.WriteMinPulseTime((ushort)Parametrizacion.GetDouble("MIN_PULSE_TIME", 2, ParamUnidad.s));

            readwatt.WriteDivPulse((ushort)Parametrizacion.GetDouble("DIV_PULSE", 10, ParamUnidad.SinUnidad));

            readwatt.WriteRatioPulse((ushort)Parametrizacion.GetDouble("RATIO_PULSE", 1000, ParamUnidad.SinUnidad));

            readwatt.WriteTransformRatioPulse((ushort)Parametrizacion.GetDouble("TRANSFORM_RATIO_PULSE", 1, ParamUnidad.SinUnidad));

            var numserie = readwatt.ReadNumSerie();

            Assert.IsTrue(numserie.ToString() == TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. no se ha grabado el número de serie"));

            var bastidor = readwatt.ReadBastidor();

            Assert.IsTrue(bastidor.ToString() == TestInfo.NumBastidor.Value.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. no se ha grabado el número de bastidor"));

            readwatt.WriteCodeError(0);

            Resultado.Set("COD_ERROR", 0, ParamUnidad.SinUnidad);

            readwatt.WriteResetEnergyPulse();

            var pulsos = readwatt.ReadNumberPulse();

            Resultado.Set("NUMBER_PULSE", pulsos, ParamUnidad.SinUnidad);

            var energy= readwatt.ReadEnergyPulsos();

            Resultado.Set("ENERGY_PULSE", energy.Energy.ToString(), ParamUnidad.SinUnidad);

            Assert.IsTrue(pulsos == 0, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. no se ha grabado el número de bastidor"));
        }

        public void TestFinish()
        {
            if (readwatt != null)
                readwatt.Dispose();
        }
    }
}
