﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Comunications.Message
{
    public class LAC25Message : StringMessage
    {
        private static CultureInfo enUS = new CultureInfo("en-US");

        public int NumCommands { get; set; }
        public bool IsChecking { get; set; }
    }
}
