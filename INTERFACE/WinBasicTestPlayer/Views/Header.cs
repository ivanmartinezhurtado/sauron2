﻿using System;
using System.Configuration;
using System.IO;
using System.Windows.Forms;
using WinTestPlayer.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class Header : UserControl
    {
        protected AppService app;
        protected ShellService shell;
        protected int idPorcess = -1;

        public Header()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            app = DI.Resolve<AppService>();
            shell = DI.Resolve<ShellService>();
            app.TestInfoChanged += (s, ee) => { UpdateUI(); };
            app.ModelChanged += (s, ee) => { UpdateUI(); };
            app.Runner.AllTestEnded += (s, ee) => 
            { 
                UpdateUI();
            };

            UpdateUI();
        }

        private void UpdateUI()
        {
            TestModel model = app.Model;

            btPlay.Enabled = true; ;
            btPlay.ImageIndex = !app.IsRunning ? 2 : 3;

            lblOperario.Text = model.IdOperario ?? model.IdEmpleado;
            var ruta = app.Sequence?.Name;
            var name = Path.GetFileName(ruta);
            lblDesc.Text = name;
      
            lblNumOrden.Visible = string.IsNullOrEmpty(model.Lote) ? false : true;
            lblNumOrden.Text = model.Lote;
            lblOrden.Visible = lblNumOrden.Visible;
            lblOrden.Text = ConfigurationManager.AppSettings["LabelOrden"]; 

            lblProducto.Visible = string.IsNullOrEmpty(model.Codigo) ? false : true;
            lblProducto.Text = model.Codigo;
            labelProducto.Visible = lblProducto.Visible;
            labelProducto.Text = ConfigurationManager.AppSettings["LabelProducto"];

            labelTitulo.Text = ConfigurationManager.AppSettings["LabelTitulo"];
        }

        private void btPlay_Click(object sender, EventArgs e)
        {
            if (!app.IsRunning)
                app.Play();
            else
            {
                btPlay.Enabled = false;
                app.Runner.Stop();
            }
        }
    }
}
