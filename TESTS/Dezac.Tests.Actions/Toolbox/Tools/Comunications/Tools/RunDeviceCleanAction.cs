﻿using Dezac.Core.Utility;
using System.Diagnostics;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(RunDeviceCleanActionDescription))]
    public class RunDeviceCleanAction : ActionBase
    {
        public override void Execute()
        {
            var ps = new ProcessStartInfo("C:\\DeviceCleanupCmd.exe");
            ps.Verb = "runas";

            var proc = new Process();
            proc.StartInfo = ps;
            proc.Start();
            proc.WaitForExit();
        }
    }

    public class RunDeviceCleanActionDescription : ActionDescription
    {
        public override string Name { get { return "RunDeviceClean"; } }
        public override string Description { get{return "Libera los puertos del dispositivo"; } }
        public override string Category { get { return "Tools/Comunications/Tools"; } }
        public override Image Icon { get { return Properties.Resources.Tools_icon; } }
    }
}
