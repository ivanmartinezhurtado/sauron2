﻿using Dezac.Core.Model;
using Dezac.Data;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WinTaskRunner.Services;

namespace WinTaskRunner.Views
{
    public partial class UploadSequenceView : UserControl, IDialogView
    {
        private SequenceDocument document;

        public int? IdFamilia { get; set; }
        public string Familia { get; set; }
        public int? NumProducto { get; set; }
        public int? Version { get; set; }
        public string Descripcion { get; set; }
        public string IdFase { get; set; }
        public string Fase { get; set; }
        public string IdUser { get; set; }
        public int TipoSecuencia { get; set; }

        public List<T_TESTSEQUENCEFILE> seqs;
        public List<T_TESTSEQUENCEFILE> seqsProd;

        protected readonly IShell shell;

        public UploadSequenceView()
        {
            InitializeComponent();

            cbSeq.SelectedIndexChanged += CbSeq_SelectedIndexChanged;
        }

        public UploadSequenceView(IShell shell, SequenceDocument document)
            : this()
        {
            this.shell = shell;
            this.document = document;

            if (document != null)
            {
                if (document.Properties.ContainsKey("Producto"))
                {
                    ProductsViewModel prod = document.Properties["Producto"] as ProductsViewModel;
                    IdFamilia = prod.NUMFAMILIA;
                    Familia = prod.FAMILIA;
                    NumProducto = prod.NUMPRODUCTO;
                    Version = prod.VERSION;
                    Descripcion = prod.DESCRIPCION;
                }

                if (document.Properties.ContainsKey("Fase"))
                {
                    ProductFaseViewModel fase = document.Properties["Fase"] as ProductFaseViewModel;
                    IdFase = fase.IDFASE;
                    Fase = fase.FASE;
                }

                if (document.Properties.ContainsKey("FileName"))
                {
                    txtNombre.Text = document.Properties["FileName"].ToString();
                }

                if (document.Properties.ContainsKey("TipoSecuencia"))
                {
                    TipoSecuencia = (int)document.Properties["TipoSecuencia"];
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            LoadSequences();
            UpdateUI();
        }

        private void UpdateUI()
        {
            if (IdFamilia.HasValue)
            {
                lblFamilia.Text = string.Format("{0}-{1}", IdFamilia, Familia);
            }
            else
            {
                lblFamilia.Text = null;
            }

            if (NumProducto.HasValue)
            {
                lblProducto.Text = string.Format("{0}/{1}-{2}", NumProducto, Version, Descripcion);
            }
            else
            {
                lblProducto.Text = null;
            }

            if (!string.IsNullOrEmpty(IdFase))
            {
                lblFase.Text = string.Format("{0}-{1}", IdFase, Fase);
            }
            else
            {
                lblFase.Text = null;
            }

            rbSameSeq.Checked = TipoSecuencia == 1;
            rbNewSeq.Checked = TipoSecuencia == 2;
            lblUsuario.Text = IdUser;

            T_TESTSEQUENCEFILE item = cbSeq.SelectedItem as T_TESTSEQUENCEFILE;

            txtNombre.Enabled = item == null || item.NUMTSF == -1;
            txtDescripcionSecuencia.Enabled = txtNombre.Enabled;
            if (item != null && item.NUMTSF != -1)
            {
                txtNombre.Text = item.NOMBREFICHERO;
                txtDescripcionSecuencia.Text = item.DESCRIPCION;
            }
        }

        public event DialgoResultEventHandler CloseDialog;

        public bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.OK)
            {
                return Upload();
            }

            return true;
        }

        private bool ValidateForm()
        {
            string msg = string.Empty;

            if (IdFamilia <= 0)
            {
                msg += "Falta la familia\n";
            }

            if (TipoSecuencia == 0)
            {
                msg += "Falta indicar el tipo de secuencia\n";
            }

            if (NumProducto.GetValueOrDefault() <= 0 && TipoSecuencia != 0)
            {
                msg += "Falta el producto\n";
            }

            if (Version.GetValueOrDefault() <= 0 && TipoSecuencia != 2)
            {
                msg += "Falta la versión\n";
            }

            if (string.IsNullOrEmpty(IdFase))
            {
                msg += "Falta la fase\n";
            }

            if (string.IsNullOrEmpty(IdUser))
            {
                msg += "Falta el usuario\n";
            }

            if (string.IsNullOrEmpty(txtDescripcion.Text))
            {
                msg += "Falta la descripcion\n";
            }

            if (TipoSecuencia <= 0)
            {
                msg += "Falta indicar el tipo de asociación de la secuencia\n";
            }

            if (msg != string.Empty)
            {
                MessageBox.Show(msg, "Atención");
            }

            return msg == string.Empty;
        }

        // TODO: No permetre vincular nomes producte
        // TODO: Resolució sequencia, si hi ha una nova no anar al sistema antic
        private bool Upload()
        {
            if (!ValidateForm())
            {
                return false;
            }

            T_TESTSEQUENCEFILE item = cbSeq.SelectedItem as T_TESTSEQUENCEFILE;
            T_TESTSEQUENCEFILEPRODUCTO itemProd = null;

            using (DezacContext db = new DezacContext())
            {
                if (item == null || item.NUMTSF == -1 || TipoSecuencia == 2)
                {
                    item = new T_TESTSEQUENCEFILE
                    {
                        NUMTSF = db.GetNextSerie("T_TESTSEQUENCEFILENUMTSF"),
                        NUMFAMILIA = IdFamilia.Value,
                        FECHAALTA = DateTime.Now,
                        USUARIOALTA = IdUser,
                        DESCRIPCION = txtDescripcionSecuencia.Text,
                        NOMBREFICHERO = txtNombre.Text,
                        IDFASE = IdFase
                    };

                    db.T_TESTSEQUENCEFILE.Add(item);
                }

                itemProd = db.T_TESTSEQUENCEFILEPRODUCTO.Where(pp => pp.NUMTSF == item.NUMTSF && pp.NUMPRODUCTO == NumProducto && pp.VERSION == null && pp.FECHABAJA == null).FirstOrDefault();

                if (itemProd == null)
                {
                    itemProd = new T_TESTSEQUENCEFILEPRODUCTO
                    {
                        NUMTSFP = db.GetNextSerie("T_TESTSFVNUMTSFP"),
                        NUMTSF = item.NUMTSF,
                        FECHAALTA = DateTime.Now,
                        NUMPRODUCTO = NumProducto.Value
                    };

                    db.T_TESTSEQUENCEFILEPRODUCTO.Add(itemProd);
                }

                T_TESTSEQUENCEFILEVERSION itemVer = new T_TESTSEQUENCEFILEVERSION
                {
                    NUMTSFV = db.GetNextSerie("T_TESTSFVNUMTSFV"),
                    NUMTSF = item.NUMTSF,
                    FECHAALTA = DateTime.Now,
                    USUARIOALTA = IdUser,
                    DESCRIPCION = txtDescripcion.Text,
                    FECHAVALIDACION = DateTime.Now,
                    USUARIOVALIDACION = IdUser
                };

                using (MemoryStream ms = new MemoryStream())
                {
                    string version = string.IsNullOrEmpty(document.Model.Version) ? "0" : document.Model.Version;

                    document.Model.Version = (Convert.ToInt32(version) + 1).ToString();

                    document.Save(ms);
                    itemVer.FICHERO = ms.ToArray();
                }

                db.T_TESTSEQUENCEFILEVERSION.Add(itemVer);

                db.SaveChanges();
            }

            return true;
        }

        private void btnFamilia_Click(object sender, EventArgs e)
        {
            List<T_FAMILIA> fams = null;

            using (DezacContext db = new DezacContext())
            {
                fams = db.T_FAMILIA.Where(p => p.PLATAFORMA == "TS2").OrderBy(p => p.NUMFAMILIA).ToList();
            }

            T_FAMILIA item = shell.ShowTableDialog<T_FAMILIA>("FAMILIAS", new TableGridViewConfig { Data = fams, Fields = "NUMFAMILIA;FAMILIA" });
            if (item == null || item.NUMFAMILIA == IdFamilia)
            {
                return;
            }

            IdFamilia = item.NUMFAMILIA;
            Familia = item.FAMILIA;
            NumProducto = null;
            Version = 0;
            Descripcion = null;

            UpdateUI();

            LoadSequences();
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            ProductsViewModel item = null;

            using (ProductsView prodView = new ProductsView())
            {
                prodView.HidePanels();
                prodView.FilterByNumFamilia = IdFamilia;

                if (shell.ShowDialog("Seleccione el producto", prodView, MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    item = prodView.Current;
                }
            }

            if (item == null)
            {
                return;
            }

            NumProducto = item.NUMPRODUCTO;
            Version = item.VERSION;
            Descripcion = item.DESCRIPCION;

            UpdateUI();

            LoadSequences();
        }

        private void btnFase_Click(object sender, EventArgs e)
        {
            List<ProductFaseViewModel> fases;

            using (DezacContext db = new DezacContext())
            {
                if (radCheckBoxReproceso.Checked)
                {
                    IQueryable<FASE> query = db.FASE.AsQueryable();
                    fases = fases = query.Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.DESCRIPCION, TIEMPOPREPARACION = 0, TIEMPOSTANDARD = 0, TIEMPOTEORICO = 0 })
                        .ToList();
                }
                else
                {
                    IQueryable<PRODUCTOFASE> query = db.PRODUCTOFASE.Include("FASE").AsQueryable();

                    if (NumProducto.HasValue)
                    {
                        query = query.Where(p => p.NUMPRODUCTO == NumProducto);
                    }

                    if (Version.HasValue)
                    {
                        query = query.Where(p => p.VERSION == Version);
                    }

                    fases = query.Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                        .ToList();
                }
            }

            ProductFaseViewModel fase = shell.ShowTableDialog<ProductFaseViewModel>("FASE", fases);
            if (fase == null)
            {
                return;
            }

            IdFase = fase.IDFASE;
            Fase = fase.FASE;

            LoadSequences();
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            LoginView ctrl = new LoginView();

            if (shell.ShowDialog("IDENTIFICACIÓN", ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                return;
            }

            IdUser = ctrl.Empleado.IDEMPLEADO;
            UpdateUI();
        }

        private void rbTipoSecuencia_Click(object sender, EventArgs e)
        {
            if (rbSameSeq.Checked)
            {
                TipoSecuencia = 1;
                LoadSequences();
            }
            else if (rbNewSeq.Checked)
            {
                TipoSecuencia = 2;
                LoadNewSequences();
            }
        }

        private void LoadSequences()
        {
            if (!IdFamilia.HasValue || string.IsNullOrEmpty(IdFase) || !NumProducto.HasValue)
            {
                seqs = null;
            }

            using (DezacContext db = new DezacContext())
            {
                seqs = db.T_TESTSEQUENCEFILE
                    .Include("T_TESTSEQUENCEFILEPRODUCTO")
                    .Where(p => p.NUMFAMILIA == IdFamilia && p.IDFASE == IdFase && p.FICHERO == null && p.NUMTSF > 1704)
                    .ToList();
            }

            seqsProd = seqs.Where(p => p.T_TESTSEQUENCEFILEPRODUCTO.Any(pp => pp.NUMPRODUCTO == NumProducto && pp.VERSION == null && pp.FECHABAJA == null)).ToList();
            if (seqsProd != null && seqsProd.Any())
            {
                cbSeq.DataSource = seqsProd;
                cbSeq.DisplayMember = "DESCRIPCION";
                cbSeq.Enabled = false;
                TipoSecuencia = 1;
                rbSameSeq.Enabled = true;
            }
            else
            {
                if (seqs != null)
                {
                    seqs.Clear();
                    seqs = null;
                }
                seqs = new List<T_TESTSEQUENCEFILE>();
                seqs.Add(new T_TESTSEQUENCEFILE { NUMTSF = -1, DESCRIPCION = "--- NUEVA ---" });
                cbSeq.DataSource = seqs;
                cbSeq.Enabled = true;
                TipoSecuencia = 2;
                rbSameSeq.Enabled = false;
            }

            cbSeq.ValueMember = "NUMTSF";
            cbSeq.DisplayMember = "DESCRIPCION";
            cbSeq.SelectedIndex = 0;

            UpdateUI();
        }

        private void LoadNewSequences()
        {
            if (seqs != null)
            {
                seqs.Clear();
                seqs = null;
            }

            seqs = new List<T_TESTSEQUENCEFILE>();
            seqs.Add(new T_TESTSEQUENCEFILE { NUMTSF = -1, DESCRIPCION = "--- NUEVA ---" });
            cbSeq.DataSource = seqs;
            cbSeq.Enabled = true;
            TipoSecuencia = 2;

            cbSeq.ValueMember = "NUMTSF";
            cbSeq.DisplayMember = "DESCRIPCION";
            cbSeq.SelectedIndex = 0;

            UpdateUI();
        }

        private void CbSeq_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}
