namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class INSTRUCCIONTECNICA
    {
        public string REFINSTRUCCION { get; set; }
        public string DESCRIPCION { get; set; }
        public byte[] DOCPDF { get; set; }
        public Nullable<System.DateTime> FECHAPDF { get; set; }
    }
}
