﻿using System;
using System.Collections.Generic;
using System.Threading;
using Instruments.IO;

namespace Instruments.Utility
{
    public class MonitorInputs
    {
        public int Interval { get; set; }

        public Dictionary<byte, bool> Inputs { get; set; }

        public IOBase IO { get; set; }
      
        private Timer timer;

        public event EventHandler<InputsEventArgs> ValueCahnged;

        public void Start()
        {
            timer = new Timer(DoWork, this, Interval, Interval);
        }

        public void Stop()
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
        }

        private void DoWork(object state)
        {
            for (byte i = 0; i < Inputs.Count; i++)
            {
                bool value = IO.DI[i];
                if (Inputs[i] != value)
                {
                    Inputs[i] = value;
                    if (ValueCahnged != null)
                        ValueCahnged(this, new InputsEventArgs(i, value));
                }
            }

        }
    }

    public class InputsEventArgs : EventArgs
    {
        public InputsEventArgs(byte value, bool state)
        {
            this.Value = value;
            this.State = state;
        }

        public byte Value { get; set; }
        public bool State { get; set; }
    }
}
