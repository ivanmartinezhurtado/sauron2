﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Metering;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.02)]
    public class MK_DCTest : TestBase
    {
        private const int OUT_ERROR_LIGHT = 43;
        private const int IN_BLOQ_UUT = 23;
        private const int IN_PRECINTO_UUT = 9;
        private const int IN_DETECT_UUT = 10;

        private const int OUT_IMPULSE_UUT = 18;
        private const int INPUT_MUX_IMPULSE_OUT = 2; //MX
        private const string IMAGE_PATH = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\MK_DCTest\";

        private Tower3 tower;
        private MK_DC mk_dc;
        private double EnergyRead;
    
        public bool IsDC { get; set; }
        public bool IsShunt { get; set; }
        public double RelationTransform { get; set; }

        public void TestInitialization(int portCEM)
        {
            if (portCEM == 0)
                portCEM = Comunicaciones.SerialPort;

            mk_dc = AddInstanceVar(new MK_DC(portCEM), "UUT");
            mk_dc.Modbus.PerifericNumber = Comunicaciones.Periferico;
            mk_dc.Modbus.TimeOut = 2000;
            ((ModbusDeviceSerialPort)mk_dc.Modbus).RtsEnable = true;
            ((ModbusDeviceSerialPort)mk_dc.Modbus).DtrEnable = true;

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.IO.DO.Off(OUT_ERROR_LIGHT);

            IsDC = Configuracion.GetString("SUPPLY_TYPE", "DC", ParamUnidad.SinUnidad).Trim().ToUpper() =="DC";
            if (IsDC)
                tower.IO.DO.On(23);

            IsShunt = Configuracion.GetString("HAS_SHUNT", "SI", ParamUnidad.SinUnidad).Trim().ToUpper() =="SI";

            tower.FLUKE.DeviceInputImpedance = 8; //Impedacia del eBOX  10KOhms

            Shell.MsgBox("INTRODUZCA EL EQUIPO EN EL UTIL Y PULSE ACEPTAR", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tower.WaitBloqSecurity(2000);

            if (!tower.IO.DI.WaitOn(IN_BLOQ_UUT, 2000))
                Shell.MsgBox("ATENCION PUERTA DEL UTIL ABIERTA", "INICIO TEST", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!tower.IO.DI.WaitOn(IN_DETECT_UUT, 2000))
                throw new Exception("Error  puerta del util abierta");

            if (!tower.IO.DI.WaitOn(IN_PRECINTO_UUT, 2000))
                throw new Exception("Error  puerta del util abierta");

            tower.FLUKE.AC_DC = AC_DC.DC;
        }

        public void TestConsmuption()
        {
            var configuration = new TestPointConfiguration()
            {
                Voltage = IsDC == true ? 36:  230,
                Current = 0.1,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = IsDC == true ? TypePowerSource.LAMBDA : TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.OnlyActive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(tower, configuration);
        }

        public void TestComunicaciones()
        {
            SamplerWithCancel((p) => { mk_dc.WriteErrorCode(9999); return true; }, "Error comunicaciones, no hemos recibido respuesta del equipo");

            var version = mk_dc.ReadSoftwareVersion();

            string[] versionBBDD = Identificacion.VERSION_FIRMWARE.Split('.');

            Assert.IsTrue(version.Major == Convert.ToUInt16(versionBBDD[0]) && version.Minor == Convert.ToUInt16(versionBBDD[1]) && version.Revision == Convert.ToUInt16(versionBBDD[2]), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, Versión de Firmware del equipo diferenete a la de la BBDD"));

            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Revision.ToString(), ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

            var crc = mk_dc.ReadCRCFirmware();

            Resultado.Set(ConstantsParameters.Identification.CRC_FIRMWARE, crc.ToString(), ParamUnidad.SinUnidad, Identificacion.CRC_FIRMWARE.ToString());

            Assert.IsTrue(crc == Identificacion.CRC_FIRMWARE, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, CRC del Firmware del equipo diferenete a la de la BBDD"));

        }

        public void TestConfiguracionInicial()
        {
           // var model = string.IsNullOrEmpty(Identificacion.MODELO) ? "10" : Identificacion.MODELO;
           // mk_dc.WriteModelo(Convert.ToInt32(model));

            mk_dc.WriteBastidor((int)TestInfo.NumBastidor.Value);

            EnergyRead = mk_dc.ReadEnergy();

            if (IsShunt)
            {
                mk_dc.WriteShuntType(false);

                var relationTransform = 600; //Relacio del equip 1 a 1
                mk_dc.WriteShuntRelation((ushort)relationTransform);
                logger.Info("Realcion transfo,marmacion que grabamos al Equipo del SHUNT 600");

                RelationTransform = relationTransform / 0.06;
                logger.Info("Realcion con la consgina de medida de 600/0.06 = 10000");
            }
            else
                RelationTransform = 1;

            //Valores defecto
            mk_dc.WriteGainVoltage(2058);
            mk_dc.WriteGainCurrent(30813);
            mk_dc.WriteOffsetCurrent(0);
            mk_dc.WriteOffsetVoltage(0);
        }

        public void TestDisplay()
        {
            //mk_dc.WriteDisplay(MK_DC.DisplayConfigurations.TODOS_SEGMENTOS_DESACTIVADOS);

            //if (Shell.ShowDialog("Test DISPLAY", () => new ImageView("TODOS SEGMENTOS DESACTIVADOS", IMAGE_PATH + "TODOS_SEGMENTOS_DESACTIVADOS.png"), MessageBoxButtons.YesNo, "Se ve el Display como en la imagen patron") != DialogResult.Yes)
            //    throw new Exception("Error Display incorrecto");

            mk_dc.WriteDisplay(MK_DC.DisplayConfigurations.TODOS_SEGMENTOS_ACTIVADOS);

            if (Shell.ShowDialog("Test DISPLAY", () => new ImageView("TODOS LOS SEGMENTOS ACTIVADOS", IMAGE_PATH + "TODOS_LOS_SEGMENTOS_ACTIVADOS.png"), MessageBoxButtons.YesNo, "Se ve el Display como en la imagen patron") != DialogResult.Yes)
                throw new Exception("Error Display incorrecto");
        }


        public void TestKeyboard()
        {
            foreach (MK_DC.Keys key in Enum.GetValues(typeof(MK_DC.Keys)))
            {
                SamplerWithCancel((p) =>
                {
                    if (mk_dc.ReadDigitallInput() == 3)
                        return true;
                    return false;
                }
              , string.Format("Error se detecta alguna tecla cuando no debe haber ninguna pulsada"), 40, 500);

                var frm = this.ShowForm(() => new ImageView("Test Teclado", IMAGE_PATH + key.GetDescription() + ".png"), MessageBoxButtons.AbortRetryIgnore, "Pulse la tecla mostrada en la imagen");

                try
                {
                    SamplerWithCancel((p) =>
                    {
                        if (mk_dc.ReadDigitallInput() == (ushort)key)
                            return true;
                        return false;
                    }
                    , string.Format("No se detecta la tecla {0}", key.GetDescription().Replace("_", " ")), 40, 500);
                }
                finally
                {
                    this.CloseForm(frm);
                }

                SamplerWithCancel((p) =>
                {
                    if (mk_dc.ReadDigitallInput() == 3)
                        return true;
                    return false;
                }
            , string.Format("Error se detecta alguna tecla cuando no debe haber ninguna pulsada"), 40, 500);
            }
        }
    
        public void TestSalidaImpulsos()
        {
            var tension = Consignas.GetDouble(Params.V.Null.TestPoint("IMPULSE_OUTPUT").Name, 280, ParamUnidad.V);
            var corriente = Consignas.GetDouble(Params.I.Null.TestPoint("IMPULSE_OUTPUT").Name, 0.06, ParamUnidad.A);

            var voltage = TriLineValue.Create(tension, 0, 0);
            var current = TriLineValue.Create(corriente, 0, 0);

            tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, current, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
            Delay(2000, "");

            tower.IO.DO.On(OUT_IMPULSE_UUT);

            mk_dc.WriteDigitalOutputs(0);

            var param = new AdjustValueDef(Params.V.Null.TestPoint("IMPULSE_OUTPUT_ACTIVE").Name,0, Params.V.Null.TestPoint("IMPULSE_OUTPUT_ACTIVE").Min(), Params.V.Null.TestPoint("IMPULSE_OUTPUT_ACTIVE").Max(),0,0, ParamUnidad.Hz);

            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
            }, 0, 3, 2000);

            mk_dc.WriteDigitalOutputs(1);

            param = new AdjustValueDef(Params.V.Null.TestPoint("IMPULSE_OUTPUT_INACTIVE").Name, 0, Params.V.Null.TestPoint("IMPULSE_OUTPUT_INACTIVE").Min(), Params.V.Null.TestPoint("IMPULSE_OUTPUT_INACTIVE").Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(param,
            (step) =>
            {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
            }, 0, 3, 2000);

            tower.IO.DO.Off(OUT_IMPULSE_UUT);
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var measureConsigna = new List<AdjustValueDef>();
            measureConsigna.Add(new AdjustValueDef(Params.V.Null.Offset.Name, 0, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V));
            measureConsigna.Add(new AdjustValueDef(Params.I.Null.Offset.Name, 0, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.A));

            tower.FLUKE.ApplyOff();

            TestMeasureBase(measureConsigna,
            (step) =>
            {
                var voltage = mk_dc.ReadVoltage();
                var current = mk_dc.ReadCurrent();
                var result = new List<double>() { voltage, current };
                return result.ToArray();
            }, delFirts, samples, timeInterval);

            var ConsignasV = new TriLineValue { L1 = Consignas.GetDouble(Params.V.L1.Ajuste.Name, 250, ParamUnidad.V), L2 = 0, L3 = 0 };
            var ConsignasI = new TriLineValue { L1 = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 0.06, ParamUnidad.A), L2 = 0, L3 = 0 };

            if (IsShunt)
                tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            tower.FLUKE.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
            Delay(2000, "");

            measureConsigna.Clear();
            measureConsigna.Add(new AdjustValueDef(Params.V.Null.CrucePistas.Name, 0, Params.V.Null.CrucePistas.Min(), Params.V.Null.CrucePistas.Max(), 0, 0, ParamUnidad.V));
            measureConsigna.Add(new AdjustValueDef(Params.I.Null.CrucePistas.Name, 0, Params.I.Null.CrucePistas.Min(), Params.I.Null.CrucePistas.Max(), 0, 0, ParamUnidad.A));

            TestMeasureBase(measureConsigna,
             (step) =>
             {
                 var voltage = mk_dc.ReadVoltage();
                 var current = mk_dc.ReadCurrent();
                 var result = new List<double>() { voltage, current };
                 return result.ToArray();
             }, delFirts, samples, timeInterval);
        }

        public void TestAjusteVoltage(int delFirst = 2, int samples = 3, int interval = 1100)
        {
            var valoresAjuste0V = new AdjustValueDef(Params.POINTS.Other("0V").Ajuste.Name, 0, Params.POINTS.Other("0V").Ajuste.Min(), Params.POINTS.Other("0V").Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            tower.FLUKE.ApplyOff();
            Delay(2000, "");

            var points0V = TestMeasureBase(valoresAjuste0V,
            (step) =>
             {
                 var point = mk_dc.ReadPointsVoltage();
                 return point;
             }, delFirst, samples, interval, true);

            var tension = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 280, ParamUnidad.V);
            var corriente = 0D;
            var tesnsionString = tension.ToString() + "V";

            var valoresAjuste230V = new AdjustValueDef(Params.POINTS.Other(tesnsionString).Ajuste.Name, 0, Params.POINTS.Other(tesnsionString).Ajuste.Min(), Params.POINTS.Other(tesnsionString).Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
            var voltage = TriLineValue.Create(tension, 0, 0);

            tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, corriente, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
            Delay(2000, "");

            var points280V = TestMeasureBase(valoresAjuste230V,
            (step) =>
            {
                var point = mk_dc.ReadPointsVoltage();
                return point;
            }, delFirst, samples, interval, true);

            var adjGainVoltage = new AdjustValueDef(Params.GAIN_V.Null.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
            var adjGainOffset = new AdjustValueDef(Params.GAIN_OFFSET.Other("V").Name, 0, Params.GAIN_OFFSET.Null.TestPoint("V").Min(), Params.GAIN_OFFSET.Null.TestPoint("V").Max(), 0, 0, ParamUnidad.Puntos);

            var ajusteVoltage0 = new MK_DC.PuntosVoltageGanancia()
            {
                puntosVoltage = points0V.Value,
                Voltage = 0
            };
            var ajusteVoltage280 = new MK_DC.PuntosVoltageGanancia()
            {
                puntosVoltage = points280V.Value,
                Voltage = tension
            };

            var valoresAjusteVoltage = mk_dc.CalculateVoltageAdjust(ajusteVoltage0, ajusteVoltage280,
            (value) =>
            {
                adjGainVoltage.Value = value.Variable;
                adjGainOffset.Value = value.VariableOffset;

                adjGainVoltage.AddToResults(Resultado);
                if(HasError(adjGainVoltage, false))
                    throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", adjGainVoltage.Name));

                adjGainOffset.AddToResults(Resultado);
                if(HasError(adjGainOffset, false))
                    throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", adjGainOffset.Name));

                return true;
            });
        }

        public void TestAjusteCorriente(int delFirst = 2, int samples = 4, int interval = 1100)
        {
            var tension = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 280, ParamUnidad.V);
            var corriente = 0D;
            var voltage = TriLineValue.Create(tension, 0, 0);
            var current = TriLineValue.Create(corriente, 0, 0);
            tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, current, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
            Delay(2000, "");

            //****************************************************************

            var valoresAjuste0A = new AdjustValueDef(Params.POINTS.Other("0A").Ajuste.Name, 0, Params.POINTS.Other("0A").Ajuste.Min(), Params.POINTS.Other("0A").Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var points0A = TestMeasureBase(valoresAjuste0A,
            (step) =>
            {
                var point = mk_dc.ReadPointsCurrent();
                return point;
            }, delFirst, samples, interval, true);

            var ajusteCurrent0A = new MK_DC.PuntosCorrienteGanancia()
            {
                puntosCorriente = points0A.Value,
                Voltage = tension,
                Corriente = corriente
            };

            //**************************************************************
            tension = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 280, ParamUnidad.V);
            corriente = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.06, ParamUnidad.A);
            var currentString = corriente.ToString() + "A";

            voltage = TriLineValue.Create(tension, 0, 0);
            current = TriLineValue.Create(corriente, 0, 0);

            if (IsShunt)
                tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, current, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
            Delay(2000, "");

            //*****************************************************************

            var valoresCorriente10A = new AdjustValueDef(Params.POINTS.Other(currentString).Ajuste.Name, 0, Params.POINTS.Other(currentString).Ajuste.Min(), Params.POINTS.Other(currentString).Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var points10A = TestMeasureBase(valoresCorriente10A,
            (step) =>
            {
                var point = mk_dc.ReadPointsCurrent();
                return point;
            }, delFirst, samples, interval, true);

            var ajusteCurrent10A = new MK_DC.PuntosCorrienteGanancia()
            {
                puntosCorriente = points10A.Value,
                Voltage = tension,
                Corriente = corriente
            };

            //*********************************************************************

            var adjGainCurrent = new AdjustValueDef(Params.GAIN_I.Null.Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
            var adjGainOffsetCurrent = new AdjustValueDef(Params.GAIN_OFFSET.Other("I").Name, 0, Params.GAIN_OFFSET.Other("I").Ajuste.Min(), Params.GAIN_OFFSET.Other("I").Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
          
            var valoresAjusteVoltage = mk_dc.CalculateCorrienteAdjust(ajusteCurrent0A, ajusteCurrent10A,
            (value) =>
            {
                adjGainCurrent.Value = value.Variable;
                adjGainOffsetCurrent.Value = value.VariableOffset;

                adjGainCurrent.AddToResults(Resultado);
                if (HasError(adjGainCurrent, false))
                    throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", adjGainCurrent.Name));

                adjGainOffsetCurrent.AddToResults(Resultado);
                if (HasError(adjGainOffsetCurrent, false))
                    throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", adjGainOffsetCurrent.Name));

                return true;
            }, IsShunt);
        }    

        public void TestVerificationIMIN()
        {
            var testPointName = "10xFS";
            var voltage = Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint(testPointName).Name, 50, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint(testPointName).Name, 0.006, ParamUnidad.A); //0.5 Directe
            var tolerance = Margenes.GetDouble(Params.PARAMETER("V_I", ParamUnidad.PorCentage).Null.Verificacion.TestPoint(testPointName).Name, 20, ParamUnidad.PorCentage);
            InternalTestVerification(testPointName, voltage, current, tolerance);
        }

        public void TestVerificationIMAX()
        {
            var testPointName = "100xFS";
            var voltage = Consignas.GetDouble(Params.V.Null.Verificacion.TestPoint(testPointName).Name, 280, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Verificacion.TestPoint(testPointName).Name, 0.06, ParamUnidad.A); //10A Directe
            var tolerance = Margenes.GetDouble(Params.PARAMETER("V_I", ParamUnidad.PorCentage).Null.Verificacion.TestPoint(testPointName).Name, 1, ParamUnidad.PorCentage);
            InternalTestVerification(testPointName, voltage, current, tolerance);
        }

        public void TestCustomize()
        {
            //mk_dc.WriteNumSerie(Convert.ToUInt32(TestInfo.NumSerie));

            if (IsDC)
            {
                tower.LAMBDA.ApplyOffAndWaitStabilisation();
                Delay(2000, "");
                tower.LAMBDA.ApplyPresetsAndWaitStabilisation(36, 0.1);
            }else
            {
                tower.Chroma.ApplyOffAndWaitStabilisation();
                Delay(2000, "");
                tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0,50);
            }

            SamplerWithCancel((p) => { mk_dc.ReadBastidor(); return true; }, "Error comunicaciones despues del reset de hardware para verificar la grabacion en memoria ");

            var Bastidor = mk_dc.ReadBastidor();
            Assert.IsTrue(Bastidor == (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error el numero de Bastidor grabado no coincide con el esperado"));

            var Energy = mk_dc.ReadEnergy();
            if (Energy <= EnergyRead)
                throw new Exception("Error, no se ha incrementado la energia activa exportada del equipo");

            mk_dc.ResetEnergiesSetup();

            Delay(1000, "Espera reset energias");

            var codError = Convert.ToUInt16(Identificacion.CODE_ERROR);
            mk_dc.WriteErrorCode(codError);

            if (IsShunt)
            {
                var shuntDirection = Parametrizacion.GetString("CONEXION_SHUNT", "POSITIVO", ParamUnidad.SinUnidad).Trim().ToUpper() == "POSITIVO";
                mk_dc.WriteShuntType(shuntDirection);

                var relationTransform = (ushort)Parametrizacion.GetDouble("RELACION_TRANSFORMACION", 1200, ParamUnidad.SinUnidad);
                mk_dc.WriteShuntRelation(relationTransform);
            }

            var Energy0 = mk_dc.ReadEnergy();
            logger.InfoFormat("Energia final del test: {0}", Energy0);
            if (Energy0 > 10)
                throw new Exception("Error, no se ha borrado la energia del equipo");

            Delay(3000, "Espera para grabar código de error");
        }

        public void TestFinish()
        {
            if (mk_dc != null)
                mk_dc.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.IO.DO.Off(OUT_IMPULSE_UUT);
                tower.Dispose();

                if (IsTestError && tower != null)
                    tower.IO.DO.On(OUT_ERROR_LIGHT);
            }
        }

        //**********************************
        //**********************************

        public void InternalTestVerification(string testPointName, double tension, double corriente, double tolerance)
        {
            var voltage = TriLineValue.Create(tension, 0, 0);
            var current = TriLineValue.Create(corriente, 0, 0);

            if (IsShunt)
                tower.FLUKE.SetOutputCurrentMode(OutputCurrentModes.VOLTAGE_FROM_CURRENT);

            tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, current, 0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
            Delay(2000, "");

            var power = tension * corriente;

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.V.Null.Verificacion.TestPoint(testPointName).Name, 0, 0, 0, tension, tolerance, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.Null.Verificacion.TestPoint(testPointName).Name, 0, 0, 0, corriente, tolerance, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.KW.Null.Verificacion.TestPoint(testPointName).Name, 0, 0, 0, power, tolerance, ParamUnidad.Kw));

            TestCalibracionBase(defs,
            () =>
            {
                var voltageRead = (double)mk_dc.ReadVoltage();
                var currentRead = (double)mk_dc.ReadCurrent();
                var powerRead = (double)mk_dc.ReadPowerActive();
                var result = new List<double>() { voltageRead, currentRead, powerRead };
                return result.ToArray();
            },
            () =>
            {
                var resultParton = new List<double>() { tension, corriente * RelationTransform , power * RelationTransform };
                return resultParton.ToArray();
            }, 3, 5, 1200);
        }
    }
}
 