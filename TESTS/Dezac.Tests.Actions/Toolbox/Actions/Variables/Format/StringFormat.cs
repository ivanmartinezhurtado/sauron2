﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(StringFormatDescription))]
    public class StringFormat : ActionBase
    {
        [Category("3.1 Format")]
        [Description("Devuelve una nueva cadena que alinea a la derecha los caracteres en esta instancia rellenándolos con espacios a la izquierda, para una longitud total especificada.")]
        public Pad[] PadLeft { get; set; }

        [Category("3.1 Format")]
        [Description("Devuelve una nueva cadena que alinea a la izquierda los caracteres en esta instancia rellenándolos con espacios a la derecha, para una longitud total especificada.")]
        public Pad[] PadRight { get; set; }

        [DefaultValue(true)]
        [Category("3.2 Remove")]
        [Description("Limpia los caracteres que no se pueden imprimir de la cadena")]
        public bool RemoveNoPrintableCharacters { get; set; }

        [DefaultValue(false)]
        [Category("3.2 Remove")]
        [Description("Limpia los espacios en blanco del principio y del final de la cadena")]
        public bool Trim { get; set; }

        [DefaultValue(false)]
        [Category("3.2 Remove")]
        [Description("Limpia los espacios en blanco del principio de la cadena")]
        public bool TrimStart { get; set; }

        [DefaultValue(false)]
        [Category("3.2 Remove")]
        [Description("Limpia los espacios en blanco del final de la cadena")]
        public bool TrimEnd { get; set; }

        [DefaultValue(format.Normal)]
        [Category("3.3 LetterType")]
        [Description("Cambiar toda la cadena a mayúsculas o a minúsculas")]
        public format ToUpperToLower { get; set; }

        [Category("3.4 Replace")]
        [Description("Reemplaza la cadena con una nueva")]
        public RepalceText[] Replace { get; set; }

        [Category("3.5 Concatenate")]
        [Description("Descripción del nombre de la variable a validar, poniéndolo delante.")]
        public ConcatenaString[] Prefix { get; set; }
        
        [Category("3.5 Concatenate")]
        [Description("Descripción del nombre de la variable a validar, poniéndolo detrás")]
        public ConcatenaString[] Sufix { get; set; }

        [Category("3.6 Substring")]
        [Description("posicion inicial del caracter a coger de la cadena")]
        public int? StartSubString { get; set; }

        [Category("3.6 Substring")]
        [Description("Cantidad de caracteres a coger de la cadena")]
        public int? LenghtSubString { get; set; }

        [Category("3.7 Insert")]
        [Description("Posición donde insertar la cadena")]
        public int? PositionInsert { get; set; } 

        [Category("3.7 Insert")]
        [Description("Valor de la cadena que vamos a insertar")]
        public string StringInsert { get; set; }

        [Category("3.8 PatternExpresion")]
        [Description("Expresión regular que se evalua reemplazando en el valor de VariableName las coincidencias por el valor de ReplaceExpresion devolviéndolas en la OutputVarName")]
        public string PatternExpresion { get; set; }

        [Category("3.8 PatternExpresion")]
        [Description("Expresión regular que se evalua en el valor de VariableName devolviendo las coincidencias en la OutputVarName")]
        public string GetPatternExpresion { get; set; }

        [Category("3.8 PatternExpresion")]
        [Description("Expresión regular para sustituir una expresión por otra")]
        public string ReplaceExpresion { get; set; }

        [Category("3.9 Split")]
        [Description("Separa una cadena de texto en varias cadenas tomando como referencia el carácter especificado")]
        public string SplitterChar { get; set; }

        [Category("3. Configuration")]
        [Description("Variable de entrada dode estarán los datos con los que trabajar")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        //ToDo Miki: A lo mejor podriamos tener los enumerados fuera para poder reutilizarlos en algun momento o por si falla en algún momento poder verlo al momento
        public enum format
        {
            Normal,
            ToUpper,
            ToLower
        }

        public class RepalceText
        {
            public string OldText { get; set; }
            public string NewText { get; set; }
        }

        public class Pad
        {
            public char AddText { get; set; }
            public int Lenght { get; set; }
        }

        public class ConcatenaString
        {
            [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
            public string Concatena { get; set; }
        }

        public override void Execute()
        {
            if (DataOutput.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DataOutput.Key).Throw();

            var variable = DataInput.Value.ToString();                       

            if(RemoveNoPrintableCharacters)
                variable = Regex.Replace(variable, "[^ -~]+", "");

            if (Trim)
                variable = variable.Trim();

            if (TrimStart)
                variable = variable.TrimStart();

            if (TrimEnd)
                variable = variable.TrimEnd();

            if (StartSubString.HasValue && LenghtSubString.HasValue)
                variable = variable.Substring(StartSubString.Value, LenghtSubString.Value);
            
            if (Replace != null)
            {
                foreach (var replace in Replace)
                {
                    variable = variable.Replace(replace.OldText, replace.NewText);
                }
            }

            if (Prefix != null)
            {
                foreach (var value in Prefix)
                {
                    var valueResult = VariablesTools.ResolveValue(value.Concatena).ToString();
                    variable = valueResult + variable;
                }
            }

            if (Sufix != null)
            {
                foreach (var value in Sufix)
                {
                    var valueResult = VariablesTools.ResolveValue(value.Concatena).ToString();
                    variable += valueResult;
                }
            }

            if (PadLeft != null)
            {
                foreach (var value in PadLeft)
                {
                    var variablePadLeft = variable.PadLeft(value.Lenght, value.AddText);
                    variable = variablePadLeft;
                }
            }            

            if (PadRight != null)
            {
                foreach (var value in PadRight)
                {
                    var variablePadRight = variable.PadRight(value.Lenght, value.AddText);
                    variable = variablePadRight;
                }
            }

            if(PositionInsert.HasValue && !string.IsNullOrEmpty(StringInsert))
                variable = variable.Insert(PositionInsert.Value, StringInsert);

            if (!string.IsNullOrEmpty(PatternExpresion) && !string.IsNullOrEmpty(ReplaceExpresion))
                variable = Regex.Replace(variable, PatternExpresion, ReplaceExpresion);

            if (!string.IsNullOrEmpty(GetPatternExpresion))
                variable = Regex.Match(variable, GetPatternExpresion).Value;

            if (ToUpperToLower == format.ToUpper)
                variable = variable.ToUpper();
            else if (ToUpperToLower == format.ToLower)
                variable = variable.ToLower();

            if (!string.IsNullOrEmpty(SplitterChar))
            {
                var separator = SplitterChar.Replace("\\n", "\n").Replace("\\r", "\r").ToCharArray();
                var splitVar = variable.Split(separator);
                for (int i = 0; i < splitVar.Length; i++)
                {
                    var splitValue = Regex.Replace(splitVar[i], "[^ -~]+", "");
                    if (!string.IsNullOrEmpty(splitValue))
                    {
                        if (!Context.Variables.ContainsKey(DataOutput + "_" + i))
                            Context.Variables.AddOrUpdate(DataOutput + "_" + i, splitVar[i]);
                        else
                            Context.Variables.AddOrUpdate(DataOutput + "_" + i, splitVar[i]);

                        Logger.InfoFormat("{0} -> {1}", DataOutput + "_" + i, splitVar[i]);
                    }
                }
            }
            else
            {
                Logger.InfoFormat("{0} -> {1}", DataOutput, variable);
                DataOutput.Value = variable;         
            }
        }
    }

    public class StringFormatDescription : ActionDescription
    {
        public override string Description { get { return "Este action sirve para modificar cadenas y crear nuevas"; } }
        public override string Name { get { return "StringFormat"; }}
        public override string Category { get { return "Actions/Variables/Format"; } }
        public override Image Icon { get { return Properties.Resources.format; } }
    }
}
