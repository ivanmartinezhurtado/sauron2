﻿namespace WinTestPlayer.Views
{
    partial class TestResultView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picLock = new Telerik.WinControls.UI.RadPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblResultDescription = new System.Windows.Forms.RichTextBox();
            this.btChekSteps = new System.Windows.Forms.Button();
            this.btloggerNew = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.pboxResult = new System.Windows.Forms.PictureBox();
            this.commandBarButton3 = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).BeginInit();
            this.picLock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxResult)).BeginInit();
            this.SuspendLayout();
            // 
            // picLock
            // 
            this.picLock.BackColor = System.Drawing.Color.AliceBlue;
            this.picLock.Controls.Add(this.pictureBox1);
            this.picLock.Controls.Add(this.lblResultDescription);
            this.picLock.Controls.Add(this.btChekSteps);
            this.picLock.Controls.Add(this.btloggerNew);
            this.picLock.Controls.Add(this.lblResult);
            this.picLock.Controls.Add(this.pboxResult);
            this.picLock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLock.Location = new System.Drawing.Point(0, 0);
            this.picLock.Name = "picLock";
            this.picLock.Size = new System.Drawing.Size(685, 347);
            this.picLock.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinTestPlayer.Properties.Resources.LogoInicio;
            this.pictureBox1.Location = new System.Drawing.Point(17, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 43);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // lblResultDescription
            // 
            this.lblResultDescription.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultDescription.Location = new System.Drawing.Point(105, 90);
            this.lblResultDescription.Name = "lblResultDescription";
            this.lblResultDescription.Size = new System.Drawing.Size(566, 243);
            this.lblResultDescription.TabIndex = 25;
            this.lblResultDescription.Text = "";
            // 
            // btChekSteps
            // 
            this.btChekSteps.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btChekSteps.Image = global::WinTestPlayer.Properties.Resources.Document_Flow_Chart_icon;
            this.btChekSteps.Location = new System.Drawing.Point(58, 297);
            this.btChekSteps.Name = "btChekSteps";
            this.btChekSteps.Size = new System.Drawing.Size(35, 36);
            this.btChekSteps.TabIndex = 24;
            this.btChekSteps.UseVisualStyleBackColor = true;
            this.btChekSteps.Click += new System.EventHandler(this.btChekSteps_Click);
            // 
            // btloggerNew
            // 
            this.btloggerNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btloggerNew.Image = global::WinTestPlayer.Properties.Resources.log_icon;
            this.btloggerNew.Location = new System.Drawing.Point(17, 297);
            this.btloggerNew.Name = "btloggerNew";
            this.btloggerNew.Size = new System.Drawing.Size(35, 36);
            this.btloggerNew.TabIndex = 23;
            this.btloggerNew.UseVisualStyleBackColor = true;
            this.btloggerNew.Click += new System.EventHandler(this.btloggerNew_Click);
            // 
            // lblResult
            // 
            this.lblResult.BackColor = System.Drawing.Color.AliceBlue;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(105, 55);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(566, 32);
            this.lblResult.TabIndex = 20;
            this.lblResult.Text = "TEST ERROR";
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pboxResult
            // 
            this.pboxResult.Image = global::WinTestPlayer.Properties.Resources.ErrorIcon;
            this.pboxResult.Location = new System.Drawing.Point(17, 90);
            this.pboxResult.Name = "pboxResult";
            this.pboxResult.Size = new System.Drawing.Size(42, 42);
            this.pboxResult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pboxResult.TabIndex = 19;
            this.pboxResult.TabStop = false;
            // 
            // commandBarButton3
            // 
            this.commandBarButton3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarButton3.DisplayName = "commandBarButton3";
            this.commandBarButton3.Image = global::WinTestPlayer.Properties.Resources.log_icon;
            this.commandBarButton3.Name = "commandBarButton3";
            this.commandBarButton3.Text = "commandBarButton3";
            this.commandBarButton3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarButton3.ToolTipText = "Report";
            // 
            // TestResultView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picLock);
            this.Name = "TestResultView";
            this.Size = new System.Drawing.Size(685, 347);
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).EndInit();
            this.picLock.ResumeLayout(false);
            this.picLock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel picLock;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pboxResult;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btloggerNew;
        private Telerik.WinControls.UI.CommandBarButton commandBarButton3;
        private System.Windows.Forms.Button btChekSteps;
        private System.Windows.Forms.RichTextBox lblResultDescription;
    }
}
