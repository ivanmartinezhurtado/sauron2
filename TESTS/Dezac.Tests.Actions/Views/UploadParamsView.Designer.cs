﻿namespace Dezac.Tests.Actions.Views
{
    partial class UploadParamsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadParamsView));
            this.label4 = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.btnUsuario = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblParam = new System.Windows.Forms.Label();
            this.lblUnidad = new System.Windows.Forms.Label();
            this.cmbParamUnidad = new System.Windows.Forms.ComboBox();
            this.rdbParamExistente = new System.Windows.Forms.RadioButton();
            this.rdbParamNuevo = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblParamExist = new System.Windows.Forms.Label();
            this.cmbTipoParametro = new System.Windows.Forms.ComboBox();
            this.lblTipoParam = new System.Windows.Forms.Label();
            this.cmbTipoGrupo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnParametros = new System.Windows.Forms.Button();
            this.cmbTipoValor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(96, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "VALOR:";
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(155, 197);
            this.txtValor.Multiline = true;
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(403, 23);
            this.txtValor.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(81, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "USUARIO:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(155, 243);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(307, 23);
            this.lblUsuario.TabIndex = 20;
            this.lblUsuario.Text = "USUARIO";
            this.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUsuario
            // 
            this.btnUsuario.Location = new System.Drawing.Point(487, 243);
            this.btnUsuario.Name = "btnUsuario";
            this.btnUsuario.Size = new System.Drawing.Size(75, 23);
            this.btnUsuario.TabIndex = 21;
            this.btnUsuario.Text = "...";
            this.btnUsuario.UseVisualStyleBackColor = true;
            this.btnUsuario.Click += new System.EventHandler(this.btnUsuario_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(155, 60);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(326, 20);
            this.txtNombre.TabIndex = 18;
            // 
            // lblParam
            // 
            this.lblParam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParam.AutoSize = true;
            this.lblParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParam.Location = new System.Drawing.Point(13, 64);
            this.lblParam.Name = "lblParam";
            this.lblParam.Size = new System.Drawing.Size(135, 13);
            this.lblParam.TabIndex = 17;
            this.lblParam.Text = "NUEVO PARAMETRO:";
            this.lblParam.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUnidad
            // 
            this.lblUnidad.AutoSize = true;
            this.lblUnidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnidad.Location = new System.Drawing.Point(373, 110);
            this.lblUnidad.Name = "lblUnidad";
            this.lblUnidad.Size = new System.Drawing.Size(59, 13);
            this.lblUnidad.TabIndex = 24;
            this.lblUnidad.Text = "UNIDAD:";
            // 
            // cmbParamUnidad
            // 
            this.cmbParamUnidad.FormattingEnabled = true;
            this.cmbParamUnidad.Location = new System.Drawing.Point(436, 106);
            this.cmbParamUnidad.Name = "cmbParamUnidad";
            this.cmbParamUnidad.Size = new System.Drawing.Size(122, 21);
            this.cmbParamUnidad.TabIndex = 25;
            // 
            // rdbParamExistente
            // 
            this.rdbParamExistente.AutoSize = true;
            this.rdbParamExistente.Checked = true;
            this.rdbParamExistente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbParamExistente.Location = new System.Drawing.Point(196, 21);
            this.rdbParamExistente.Name = "rdbParamExistente";
            this.rdbParamExistente.Size = new System.Drawing.Size(165, 20);
            this.rdbParamExistente.TabIndex = 28;
            this.rdbParamExistente.TabStop = true;
            this.rdbParamExistente.Text = "Parametro Existente";
            this.rdbParamExistente.UseVisualStyleBackColor = true;
            this.rdbParamExistente.CheckedChanged += new System.EventHandler(this.rdbParamExistente_CheckedChanged);
            // 
            // rdbParamNuevo
            // 
            this.rdbParamNuevo.AutoSize = true;
            this.rdbParamNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbParamNuevo.Location = new System.Drawing.Point(406, 21);
            this.rdbParamNuevo.Name = "rdbParamNuevo";
            this.rdbParamNuevo.Size = new System.Drawing.Size(147, 20);
            this.rdbParamNuevo.TabIndex = 29;
            this.rdbParamNuevo.Text = "Nuevo Parametro";
            this.rdbParamNuevo.UseVisualStyleBackColor = true;
            this.rdbParamNuevo.CheckedChanged += new System.EventHandler(this.rdbParamNuevo_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(360, 289);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 34);
            this.button1.TabIndex = 30;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(468, 289);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 34);
            this.button2.TabIndex = 31;
            this.button2.Text = "CANCEL";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblParamExist
            // 
            this.lblParamExist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParamExist.AutoSize = true;
            this.lblParamExist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParamExist.Location = new System.Drawing.Point(60, 64);
            this.lblParamExist.Name = "lblParamExist";
            this.lblParamExist.Size = new System.Drawing.Size(88, 13);
            this.lblParamExist.TabIndex = 32;
            this.lblParamExist.Text = "PARAMETRO:";
            this.lblParamExist.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbTipoParametro
            // 
            this.cmbTipoParametro.FormattingEnabled = true;
            this.cmbTipoParametro.Location = new System.Drawing.Point(155, 106);
            this.cmbTipoParametro.Name = "cmbTipoParametro";
            this.cmbTipoParametro.Size = new System.Drawing.Size(165, 21);
            this.cmbTipoParametro.TabIndex = 34;
            // 
            // lblTipoParam
            // 
            this.lblTipoParam.AutoSize = true;
            this.lblTipoParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoParam.Location = new System.Drawing.Point(27, 110);
            this.lblTipoParam.Name = "lblTipoParam";
            this.lblTipoParam.Size = new System.Drawing.Size(121, 13);
            this.lblTipoParam.TabIndex = 33;
            this.lblTipoParam.Text = "TIPO PARAMETRO:";
            // 
            // cmbTipoGrupo
            // 
            this.cmbTipoGrupo.FormattingEnabled = true;
            this.cmbTipoGrupo.Location = new System.Drawing.Point(155, 152);
            this.cmbTipoGrupo.Name = "cmbTipoGrupo";
            this.cmbTipoGrupo.Size = new System.Drawing.Size(165, 21);
            this.cmbTipoGrupo.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(60, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "TIPO GRUPO:";
            // 
            // btnParametros
            // 
            this.btnParametros.Location = new System.Drawing.Point(487, 59);
            this.btnParametros.Name = "btnParametros";
            this.btnParametros.Size = new System.Drawing.Size(75, 23);
            this.btnParametros.TabIndex = 37;
            this.btnParametros.Text = "...";
            this.btnParametros.UseVisualStyleBackColor = true;
            this.btnParametros.Click += new System.EventHandler(this.btnParametros_Click);
            // 
            // cmbTipoValor
            // 
            this.cmbTipoValor.FormattingEnabled = true;
            this.cmbTipoValor.Location = new System.Drawing.Point(436, 152);
            this.cmbTipoValor.Name = "cmbTipoValor";
            this.cmbTipoValor.Size = new System.Drawing.Size(122, 21);
            this.cmbTipoValor.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(344, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "TIPO VALOR:";
            // 
            // UploadParamsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(590, 335);
            this.Controls.Add(this.cmbTipoValor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnParametros);
            this.Controls.Add(this.cmbTipoGrupo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbTipoParametro);
            this.Controls.Add(this.lblTipoParam);
            this.Controls.Add(this.lblParamExist);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rdbParamNuevo);
            this.Controls.Add(this.rdbParamExistente);
            this.Controls.Add(this.cmbParamUnidad);
            this.Controls.Add(this.lblUnidad);
            this.Controls.Add(this.btnUsuario);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblParam);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UploadParamsView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Button btnUsuario;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblParam;
        private System.Windows.Forms.Label lblUnidad;
        private System.Windows.Forms.ComboBox cmbParamUnidad;
        private System.Windows.Forms.RadioButton rdbParamExistente;
        private System.Windows.Forms.RadioButton rdbParamNuevo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblParamExist;
        private System.Windows.Forms.ComboBox cmbTipoParametro;
        private System.Windows.Forms.Label lblTipoParam;
        private System.Windows.Forms.ComboBox cmbTipoGrupo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnParametros;
        private System.Windows.Forms.ComboBox cmbTipoValor;
        private System.Windows.Forms.Label label1;
    }
}
