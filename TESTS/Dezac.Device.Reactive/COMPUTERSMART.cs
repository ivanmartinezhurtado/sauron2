﻿using Comunications.Utility;
using Dezac.Core.Utility;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Dezac.Device.Reactive
{
    [DeviceVersion(1.04)]
    public class COMPUTERSMART : DeviceBase
    {
        #region Device initialization and disposal
        private int time = 1000;

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public COMPUTERSMART()
        {
          
        }

        public COMPUTERSMART(int port)
        {
            Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Modbus.PerifericNumber = 1;
        }

        public override void Dispose()
        {
            Modbus.Dispose();
            base.Dispose();
        }

        #endregion

        #region Modbus Reading operations

        public IdpInformation ReadIdpInformation()
        {
            LogMethod();
            return Modbus.Read<IdpInformation>();
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            var value =Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION).ToBytes().BytesToHexString();
            var value2 = value[1] + "." + value[2] + "." + value[3];
            return value2;
        }

        public SetupInfo ReadSetupInformation()
        {
            LogMethod();
            return Modbus.Read<SetupInfo>();
        }

        public SetupInfoFAST ReadSetupInformationFAST()
        {
            LogMethod();
            return Modbus.Read<SetupInfoFAST>();
        }

        public AlarmsInfo ReadAlarmsInformation()
        {
            LogMethod();
            return Modbus.Read<AlarmsInfo>();
        }

        public SetupLimitsInfo ReadSetupLimitsInformation()
        {
            LogMethod();
            return Modbus.Read<SetupLimitsInfo>();
        }

        public KeyboardKeys ReadKeyboard()
        {
            LogMethod();
            return (KeyboardKeys)Modbus.ReadRegister((ushort)Registers.KEYBOARD);
        }

        public MeasurePoints ReadPointsVariables()
        {
            LogMethod();
            return Modbus.Read<MeasurePoints>();
        }

        public Measure ReadVariables()
        {
            LogMethod();

            var measure = new Measure();
            measure.VoltageL1 = ReadVoltageL1Measure();
            System.Threading.Thread.Sleep(time);
            measure.VoltageL2 = ReadVoltageL2Measure();
            System.Threading.Thread.Sleep(time);
            measure.VoltageL3 = ReadVoltageL3Measure();
            System.Threading.Thread.Sleep(time);
            measure.Current = ReadCurrentMeasure();
            System.Threading.Thread.Sleep(time);
            measure.CurrentLeak = ReadLeakCurrentMeasure();
            System.Threading.Thread.Sleep(time);
            measure.PowerActive = ReadKWMeasure();
            System.Threading.Thread.Sleep(time);
            measure.PowerReactive = ReadKVARMeasure();
            System.Threading.Thread.Sleep(time);
            measure.PowerApparent = ReadKVAMeasure();
            System.Threading.Thread.Sleep(time);
            measure.PF = ReadPowerFactorMeasure();
            System.Threading.Thread.Sleep(time);
            measure.Temp = ReadTemperatureMeasurement();
            System.Threading.Thread.Sleep(time);
            measure.THDV = ReadTHDVoltageMeasure();
            System.Threading.Thread.Sleep(time);
            measure.THDI = ReadTHDCurrentMeasure();

            return measure;
        }

        public uint ReadCurrentMeasure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.CURRENT_MEASURE) ;
        }

        public uint ReadLeakCurrentMeasure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.LEAK_CURRENT_MEASURE);
        }

        public uint ReadVoltageL1Measure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.VOLTAGE_L1_MEASURE)  ;
        }

        public uint ReadVoltageL2Measure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.VOLTAGE_L2_MEASURE) ;
        }

        public uint ReadVoltageL3Measure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.VOLTAGE_L3_MEASURE) ;
        }

        public uint ReadKWMeasure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.KW_MEASURE);
        }

        public uint ReadKVARMeasure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.KVAR_MEASURE);
        }

        public uint ReadKVAMeasure()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32((ushort)Registers.KVA_MEASURE);
        }

        public ushort ReadPowerFactorMeasure()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.POWER_FACTOR_MEASURE);
        }

        public ushort ReadTemperatureMeasurement()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TEMPERATURE_MEASURE);
        }

        public int ReadTHDVoltageMeasure()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.THD_V_MEASURE);
        }

        public int ReadTHDCurrentMeasure()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.THD_I_MEASURE);
        }

        public HarmonicMeasure ReadHarmonicMeasures()
        {
            LogMethod();
            return Modbus.Read<HarmonicMeasure>();
        }

        public ushort ReadInternalFreq()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.INTERNAL_FREQ);
        }

        #endregion

        #region ModbusWritting operations

        public void WriteIdpInformation(IdpInformation idpInfo)
        {
            LogMethod();
            Modbus.Write<IdpInformation>(idpInfo);
        }

        public void WriteSetupInformation(SetupInfo setupInformation)
        {
            LogMethod();
            Modbus.Write<SetupInfo>(setupInformation);
        }

        public void WriteSetupInformation(SetupInfoFAST setupInformation)
        {
            LogMethod();
            Modbus.Write<SetupInfoFAST>(setupInformation);
        }

        public void WriteAlarmInformation(AlarmsInfo alarmInformation)
        {
            LogMethod();
            Modbus.Write<AlarmsInfo>( alarmInformation);
        }

        public void WriteSetupLimitsInformation(SetupLimitsInfo setupLimitsInformation)
        {
            LogMethod();
            Modbus.Write<SetupLimitsInfo>(setupLimitsInformation);
        }

        public void WriteFlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void WriteRelaysInformation(RelayInformation relayInformation)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.RELAYS, relayInformation.ToBoolArray());
        }

        public void WriteLeds(bool led1, bool led2)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.LEDS, new bool[] { false, false, false, false, led1, led2, false, false });

        }

        public void WriteDisplay(DisplayConfigurations display)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DISPLAY, (ushort)display); 
        }

        public void WriteDeleteMaximum()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DELETE_MAX, (ushort)0x0001);
        }

        #endregion

        #region Calibration Functions

        public double CalculateFreqGain(ushort internalFreq, int minSamples, int maxSamples, int timeSamples, Func<double, bool> adjustValidator)
        {
            double factors = 0;

            var adjustResult = AdjustBase(0, minSamples, maxSamples, timeSamples, int.MaxValue,
                () =>
                {
                    double[] freq = { ReadPointsVariables().Frequency };
                    return freq;
                }, (listValues) =>
                {
                    factors = internalFreq + ((500 - listValues.Average(0)) * 1.26);

                    return adjustValidator(factors);
                });

            return factors;
        }

        public Tuple<bool, CalibrationFactors, string> CalculateAdjustFactorsVoltageCurrentDesfase(int delFirst, int initCount, int samples, int interval, double adjustVoltage, double adjustCurrent, VoltageConfiguration voltageConfig, Func<double[]> measureVariables, Func<CalibrationFactors, List<AdjustValueDef>> adjustValidation, Func<double[], List<AdjustValueDef>> sampleValidation = null)
        {
            var factors = new CalibrationFactors();
            var Vx = 0;

            switch (voltageConfig)
            {
                case VoltageConfiguration._110V:
                    Vx = 2212; break;
                case VoltageConfiguration._230V:
                    Vx = 5202; break;
                case VoltageConfiguration._400V:
                    Vx = 8366; break;
                case VoltageConfiguration._480V:
                    Vx = 9700; break;
            }

            var errorString = "";

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    factors.CalibrationVL1 = (ushort)((adjustVoltage / listValues.Average(13)) * Vx);
                    factors.CalibrationVL2 = (ushort)((adjustVoltage / listValues.Average(14)) * Vx);          
                    factors.CalibrationVL3 = (ushort)((adjustVoltage / listValues.Average(15)) * Vx);

                    factors.CalibrationI = (ushort)(((adjustCurrent) / listValues.Average(16)) * 17642);

                    factors.CalibrationIFactor = (ushort)(Math.Sqrt(listValues.Average(3) / listValues.Average(6)) * (5 / adjustCurrent));

                    factors.CalibrationDesfase = (ushort)(((Math.Atan((listValues.Average(4) / listValues.Average(5))) * (180.0 / Math.PI)) - 45.0) * 21.9);

                    var errorList = adjustValidation(factors);

                    if (errorList.Count != 0)
                        errorString = string.Format("Error, fuera de margenes de {0}", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                },
                (samplesToValidate) =>
                {
                    var errorList = sampleValidation(samplesToValidate);

                    if (errorList.Count != 0)
                        errorString = string.Format("{0} fuera de margenes", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                });

            if (!string.IsNullOrEmpty(adjustResult.Item3))
                errorString = adjustResult.Item3;

            return Tuple.Create(adjustResult.Item1, factors, errorString);
        }

        public Tuple<bool, CalibrationFactors, string> CalculateAdjustFactorsCurrentLeak(int delFirst, int initCount, int samples, int interval, double adjustCurrent, Func<double[]> measureVariables, Func<CalibrationFactors, List<AdjustValueDef>> adjustValidation, Func<double[], List<AdjustValueDef>> sampleValidation = null)
        {
            var factors = new CalibrationFactors();

            adjustCurrent *= 500;

            var errorString = "";

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    factors.CalibrationILCK = (ushort)(((adjustCurrent) / listValues.Average(1)) * 4830);

                    var errorList = adjustValidation(factors);

                    if (errorList.Count != 0)
                        errorString = string.Format("Error, fuera de margenes de {0}", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                },
                (samplesToValidate) =>
                {
                    var errorList = sampleValidation(samplesToValidate);

                    if (errorList.Count != 0)
                        errorString = string.Format("{0} fuera de margenes", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                });

            if (!string.IsNullOrEmpty(adjustResult.Item3))
                errorString = adjustResult.Item3;

            return Tuple.Create(adjustResult.Item1, factors, errorString);
        }

        public Tuple<bool, CalibrationFactors, string> CalculateAdjustFactorsPower(int delFirst, int initCount, int samples, int interval, double adjustVoltage, double adjustCurrent, Func<double[]> measureVariables, Func<CalibrationFactors, List<AdjustValueDef>> adjustValidation, Func<double[], List<AdjustValueDef>> sampleValidation = null)
        {
            var factors = new CalibrationFactors();

            var errorString = "";

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    var aux = adjustVoltage * adjustCurrent * Math.Cos(Math.PI / 4) * 100000;
                    factors.CalibrationPowerReactive = (ushort)(aux / (listValues.Average(0) / listValues.Average(2)));
                    factors.CalibrationPowerActive = (ushort)(aux / (listValues.Average(1) / listValues.Average(2)));

                    var errorList = adjustValidation(factors);

                    if (errorList.Count != 0)
                        errorString = string.Format("Error, fuera de margenes de {0}", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                },
                (samplesToValidate) =>
                {
                    var errorList = sampleValidation(samplesToValidate);

                    if (errorList.Count != 0)
                        errorString = string.Format("{0} fuera de margenes", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                });

            if (!string.IsNullOrEmpty(adjustResult.Item3))
                errorString = adjustResult.Item3;

            return Tuple.Create(adjustResult.Item1, factors, errorString);
        }


        #endregion

        #region Structures

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.IDP_REGISTERS)]
        public struct IdpInformation
        {
            public long SerialNumber;
            public long BastidorNumber;
            public ushort ErrorCode;
            public ushort HardwareVector;

            public override string ToString()
            {
                return this.ToBytes().BytesToHexString();
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP)]
        public struct SetupInfo
        {
            public ushort TipoCoseno;
            public ushort CosenoFiObjetivo;
            public ushort CK;
            public ushort ProgramaManiobra;
            public ushort TiempoRetraso;
            public ushort Paso;
            public ushort Fase;
            public ushort PrimarioCorriente;
            public ushort EscalaPrimarioCorriente;
            public ushort ManiobraCondensador1;
            public ushort ManiobraCondensador2;
            public ushort ManiobraCondensador3;
            public ushort ManiobraCondensador4;
            public ushort ManiobraCondensador5;
            public ushort ManiobraCondensador6;
            public ushort ManiobraCondensador7;
            public ushort ManiobraCondensador8;
            public ushort ManiobraCondensador9;
            public ushort ManiobraCondensador10;
            public ushort ManiobraCondensador11;
            public ushort ManiobraCondensador12;
            public ushort BaudRate;
            public ushort Paridad;
            public ushort BitsStop;
            public ushort Periferico;
            public ushort Display;
            public ushort Backlight; 

            public SetupInfo(SmartModels model)
            {
                TipoCoseno = 0x0001;
                CosenoFiObjetivo = 0x0064;
                CK = 0x0064;
                ProgramaManiobra = 0x0457;
                TiempoRetraso = 0x000A;
                Paso = model == SmartModels.Smart6 ? (ushort)0x06 : (ushort)0x0C;
                Fase = 0x0000;
                PrimarioCorriente = 0x0005;
                EscalaPrimarioCorriente = 0x0001;
                ManiobraCondensador1 = 0x0000;
                ManiobraCondensador2 = 0x0000;
                ManiobraCondensador3 = 0x0000;
                ManiobraCondensador4 = 0x0000;
                ManiobraCondensador5 = 0x0000;
                ManiobraCondensador6 = 0x0000;
                ManiobraCondensador7 = 0x0000;
                ManiobraCondensador8 = 0x0000;
                ManiobraCondensador9 = 0x0000;
                ManiobraCondensador10 = 0x0000;
                ManiobraCondensador11 = 0x0000;
                ManiobraCondensador12 = 0x0000;
                BaudRate = 0x0000;
                Paridad = 0x0000;
                BitsStop = 0x0000;
                Periferico = 0x0001;
                Display = 0x0000;
                Backlight = 0x0006;
            }

            public override string ToString()
            {
                return this.ToBytes().BytesToHexString();
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP)]
        public struct SetupInfoFAST
        {
            public ushort TipoCoseno;
            public ushort CosenoFiObjetivo;
            public ushort CK;
            public ushort ProgramaManiobra;
            public ushort TiempoRetraso;
            public ushort Paso;
            public ushort Fase;
            public ushort PrimarioCorriente;
            public ushort EscalaPrimarioCorriente;
            public ushort ManiobraCondensador1;
            public ushort ManiobraCondensador2;
            public ushort ManiobraCondensador3;
            public ushort ManiobraCondensador4;
            public ushort ManiobraCondensador5;
            public ushort ManiobraCondensador6;
            public ushort ManiobraCondensador7;
            public ushort ManiobraCondensador8;
            public ushort ManiobraCondensador9;
            public ushort ManiobraCondensador10;
            public ushort ManiobraCondensador11;
            public ushort ManiobraCondensador12;
            public ushort BaudRate;
            public ushort Paridad;
            public ushort BitsStop;
            public ushort Periferico;
            public ushort Display;
            public ushort Backlight;
            public ushort ProgramaEstandar;
            public ushort TipoManiobra;
            public ushort MarchaParo;

            public SetupInfoFAST(SmartModels model)
            {
                TipoCoseno = 0x0001;
                CosenoFiObjetivo = 0x0064;
                CK = 0x0064;
                ProgramaManiobra = 0x0457;
                TiempoRetraso = 0x000A;
                Paso = model == SmartModels.Smart6 ? (ushort)0x06 : (ushort)0x0C;
                Fase = 0x0000;
                PrimarioCorriente = 0x0005;
                EscalaPrimarioCorriente = 0x0001;
                ManiobraCondensador1 = 0x0000;
                ManiobraCondensador2 = 0x0000;
                ManiobraCondensador3 = 0x0000;
                ManiobraCondensador4 = 0x0000;
                ManiobraCondensador5 = 0x0000;
                ManiobraCondensador6 = 0x0000;
                ManiobraCondensador7 = 0x0000;
                ManiobraCondensador8 = 0x0000;
                ManiobraCondensador9 = 0x0000;
                ManiobraCondensador10 = 0x0000;
                ManiobraCondensador11 = 0x0000;
                ManiobraCondensador12 = 0x0000;
                BaudRate = 0x0000;
                Paridad = 0x0000;
                BitsStop = 0x0000;
                Periferico = 0x0001;
                Display = 0x0000;
                Backlight = 0x0006;
                ProgramaEstandar = 0x0000;
                TipoManiobra = 0x0000;
                MarchaParo = 0x0001;
            }

            public override string ToString()
            {
                return this.ToBytes().BytesToHexString();
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ALARMS)]
        public struct AlarmsInfo
        {
            public ushort LimiteTHDTension;
            public ushort LimiteTHDCorriente;
            public ushort HabilitacionCorrienteFugas;
            public ushort LimiteCorrienteFugas;
            public ushort ResetCorrienteFugas;
            public ushort LimiteCoseno;
            public ushort LimiteCorrienteCoseno;
            public ushort TipoCoseno;
            public ushort LimiteTemperatura;
            public ushort HabilitacionAlarma1;
            public ushort HabilitacionAlarma2;
            public ushort HabilitacionAlarma3;
            public ushort HabilitacionAlarma4;
            public ushort HabilitacionAlarma5;
            public ushort HabilitacionAlarma6;
            public ushort HabilitacionAlarma7;
            public ushort HabilitacionAlarma8;
            public ushort HabilitacionAlarma9;
            public ushort HabilitacionAlarma10;
            public ushort HabilitacionAlarma11;
            public ushort HabilitacionAlarma12;
            public ushort HabilitacionAlarma13;
            public ushort HabilitacionAlarma14;
            public ushort HabilitacionAlarma15;
            public ushort HabilitacionAlarma16;
            public ushort HabilitacionReleAlarma1;
            public ushort HabilitacionReleAlarma2;
            public ushort HabilitacionReleAlarma3;
            public ushort HabilitacionReleAlarma4;
            public ushort HabilitacionReleAlarma5;
            public ushort HabilitacionReleAlarma6;
            public ushort HabilitacionReleAlarma7;
            public ushort HabilitacionReleAlarma8;
            public ushort HabilitacionReleAlarma9;
            public ushort HabilitacionReleAlarma10;
            public ushort HabilitacionReleAlarma11;
            public ushort HabilitacionReleAlarma12;
            public ushort HabilitacionReleAlarma13;
            public ushort HabilitacionReleAlarma14;
            public ushort HabilitacionReleAlarma15;
            public ushort HabilitacionReleAlarma16;

            public AlarmsInfo(SmartModels model)
            {
                LimiteTHDTension = 0x0005;
                LimiteTHDCorriente = 0x0032;
                HabilitacionCorrienteFugas = 0x0000;
                LimiteCorrienteFugas = 0x012C;
                ResetCorrienteFugas = 0x0000;
                LimiteCoseno = 0x005F;
                LimiteCorrienteCoseno = 0x0014;
                TipoCoseno = 0x0001;
                LimiteTemperatura = 0x003C;
                HabilitacionAlarma1 = 0x0001;
                HabilitacionAlarma2 = 0x0001;
                HabilitacionAlarma3 = 0x0001;
                HabilitacionAlarma4 = 0x0001;
                HabilitacionAlarma5 = 0x0001;
                HabilitacionAlarma6 = 0x0001;
                HabilitacionAlarma7 = 0x0000;
                HabilitacionAlarma8 = 0x0000;
                HabilitacionAlarma9 = 0x0000;
                HabilitacionAlarma10 = 0x0000;
                HabilitacionAlarma11 = 0x0000;
                HabilitacionAlarma12 = 0x0000;
                HabilitacionAlarma13 = 0x0000;
                HabilitacionAlarma14 = 0x0000;
                HabilitacionAlarma15 = 0x0000;
                HabilitacionAlarma16 = 0x0000;
                HabilitacionReleAlarma1 = 0x0000;
                HabilitacionReleAlarma2 = 0x0000;
                HabilitacionReleAlarma3 = 0x0000;
                HabilitacionReleAlarma4 = 0x0000;
                HabilitacionReleAlarma5 = 0x0000;
                HabilitacionReleAlarma6 = 0x0000;
                HabilitacionReleAlarma7 = 0x0000;
                HabilitacionReleAlarma8 = 0x0000;
                HabilitacionReleAlarma9 = 0x0000;
                HabilitacionReleAlarma10 = 0x0000;
                HabilitacionReleAlarma11 = 0x0000;
                HabilitacionReleAlarma12 = 0x0000;
                HabilitacionReleAlarma13 = 0x0000;
                HabilitacionReleAlarma14 = 0x0000;
                HabilitacionReleAlarma15 = 0x0000;
                HabilitacionReleAlarma16 = 0x0000;
            }

            public override string ToString()
            {
                return this.ToBytes().BytesToHexString();
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP_LIMITS)]
        public struct SetupLimitsInfo
        {
            public byte CosenoInductivo;
            public byte CosenoCapacitivo;
            public byte CKMaximo;
            public byte CKMinimo;
            public ushort TiempoRetrasoMaximo;
            public byte TiempoRetrasoMinimo;
            public byte PasoMaximo;
            public byte IntensidadMinima;
            public byte FactorReconexion;
            public ushort FactorCalibracionCorriente;
            public ushort AnguloDesfase;
            public ushort CalibracionMedidaI;
            public ushort CalibracionMedidaIFugas;
            public ushort CalibracionMedidaV1;
            public ushort CalibracionMedidaV2;
            public ushort CalibracionMedidaV3;
            public ushort CalibracionMedidaPotenciaActiva;
            public ushort CalibracionMedidaPotenciaReactiva;
            public ushort CalibracionFrecuenciaInterna;

            public SetupLimitsInfo(SmartModels model, VoltageConfiguration voltage, ushort freq, bool fast)
            {
                ushort calibrationVoltage = 0;
                ushort calibrationPower = 0;
                switch (voltage)
                {
                    case VoltageConfiguration._110V:
                        calibrationVoltage = 0x08A4;
                        calibrationPower = 0x018B;
                        break;
                    case VoltageConfiguration._230V:
                        calibrationVoltage = 0x1452;
                        calibrationPower = 0x036B;
                        break;
                    case VoltageConfiguration._400V:
                        calibrationVoltage = 0x20AE;
                        calibrationPower = 0x05DC;
                        break;
                    case VoltageConfiguration._480V:
                        calibrationVoltage = 0x25E4;
                        calibrationPower = 0x06A4;
                        break;
                }

                CosenoInductivo = 0x55;
                CosenoCapacitivo = 0x5F;
                CKMaximo = 0x64;
                CKMinimo = 0x02;
                TiempoRetrasoMaximo = 0x03E7;  //Segun TSD2013
                TiempoRetrasoMinimo = fast ? (byte)0x02 : (byte)0x04;
                PasoMaximo = model == SmartModels.Smart6 ? (byte)0x06 : (byte)0x0C; 
                IntensidadMinima = 0x08;
                FactorReconexion = 0x05;
                FactorCalibracionCorriente = 0x011A;
                AnguloDesfase = 0x0000;
                CalibracionMedidaI = 0x44EA;
                CalibracionMedidaIFugas = 0x12DE;             
                CalibracionMedidaV1 = calibrationVoltage;
                CalibracionMedidaV2 = calibrationVoltage;
                CalibracionMedidaV3 = calibrationVoltage;
                CalibracionMedidaPotenciaActiva = calibrationPower;
                CalibracionMedidaPotenciaReactiva = calibrationPower;
                CalibracionFrecuenciaInterna = freq;
            }

            public override string ToString()
            {
                return this.ToBytes().BytesToHexString();
            }
        }

        public struct RelayInformation
        {
            private bool Relay1;
            private bool Relay2;
            private bool Relay3;
            private bool Relay4;
            private bool Relay5;
            private bool Relay6;
            private bool Relay7;
            private bool Relay8;
            private bool Relay9;
            private bool Relay10;
            private bool Relay11;
            private bool Relay12;
            private bool Relay13;
            private bool Relay14;
            public Configurations Config;

            public RelayInformation(Configurations config)
            {
                Config = config;
                switch (config)
                {
                    case Configurations.EVEN:
                        Relay2 = true;
                        Relay4 = true;
                        Relay6 = true;
                        Relay8 = true;
                        Relay10 = true;
                        Relay12 = true;
                        Relay14 = true;
                        Relay1 = false;
                        Relay3 = false;
                        Relay5 = false;
                        Relay7 = false;
                        Relay9 = false;
                        Relay11 = false;
                        Relay13 = false;
                        break;
                    case Configurations.ODDS:
                        Relay1 = true;
                        Relay3 = true;
                        Relay5 = true;
                        Relay7 = true;
                        Relay9 = true;
                        Relay11 = true;
                        Relay13 = true;
                        Relay2 = false;
                        Relay4 = false;
                        Relay6 = false;
                        Relay8 = false;
                        Relay10 = false;
                        Relay12 = false;
                        Relay14 = false;
                        break;
                    case Configurations.ALL:
                        Relay1 = true;
                        Relay2 = true;
                        Relay3 = true;
                        Relay4 = true;
                        Relay5 = true;
                        Relay6 = true;
                        Relay7 = true;
                        Relay8 = true;
                        Relay9 = true;
                        Relay10 = true;
                        Relay11 = true;
                        Relay12 = true;
                        Relay13 = true;
                        Relay14 = true;
                       break;
                    case Configurations.NONE:
                    default:
                        Relay1 = false;
                        Relay2 = false;
                        Relay3 = false;
                        Relay4 = false;
                        Relay5 = false;
                        Relay6 = false;
                        Relay7 = false;
                        Relay8 = false;
                        Relay9 = false;
                        Relay10 = false;
                        Relay11 = false;
                        Relay12 = false;
                        Relay13 = false;
                        Relay14 = false;
                        break;
                }
            }

            public bool[] ToBoolArray()
            {
                return new bool[] { Relay9, Relay10, Relay11, Relay12, Relay13, Relay14, false, false, Relay1, Relay2, Relay3, Relay4, Relay5, Relay6, Relay7, Relay8};
            }
            public enum Configurations
            {
                NONE,
                ODDS,
                EVEN,
                ALL
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.HARMONICS_MEASURE)]
        public struct HarmonicMeasure
        {
            public ushort THV3;
            public ushort THV5;
            public ushort THV7;
            public ushort THV9;
            public ushort TH11;
            public ushort THV13;
            public ushort THI3;
            public ushort THI5;
            public ushort THI7;
            public ushort THI9;
            public ushort THI11;
            public ushort THI13;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MEASURES_POINTS)]
        public struct MeasurePoints
        {
            public Int32 VoltageL1;
            public Int32 VoltageL2;
            public Int32 VoltageL3;
            public Int32 Current;
            public Int32 LeakCurrent;
            public Int32 ReactivePower;
            public Int32 ActivePower;
            public Int32 Nmos;
            public Int16 OffsetL1;
            public Int16 OffsetL2;
            public Int16 OffsetL3;
            public Int16 Dummy;
            public Int16 OffsetCurrent;
            public Int16 OffsetLeakCurrent;
            public Int16 CosPhi;
            public Int16 Frequency;
        }

        public struct Measure
        {
            private uint voltageL1;
            private uint voltageL2;
            private uint voltageL3;
            private uint current;
            private uint currentLeak;
            private uint powerActive;
            private uint powerReactive;
            private uint powerApparent;
            private ushort pf;
            private ushort temp;
            private ushort thdv;
            private ushort thdi;

            public double VoltageL1 { get { return voltageL1 / 1000D; } set { voltageL1 = (uint)value; } }
            public double VoltageL2 { get { return voltageL2 / 1000D; } set { voltageL2 = (uint)value; } }
            public double VoltageL3 { get { return voltageL3 / 1000D; } set { voltageL3 = (uint)value; } }
            public double Current { get { return current / 1000D; } set { current = (uint)value; } }
            public double CurrentLeak { get { return currentLeak / 1000D; } set { currentLeak = (uint)value; } }
            public double PowerActive { get { return powerActive; } set { powerActive = (uint)value; } }
            public double PowerReactive { get { return powerReactive; } set { powerReactive = (uint)value; } }
            public double PowerApparent { get { return powerApparent; } set { powerApparent = (uint)value; } }
            public double PF { get { return pf; } set { pf = (ushort)value; } }
            public double Temp { get { return temp; } set { temp = (ushort)value; } }
            public double THDV { get { return thdv; } set { thdv = (ushort)value; } }
            public double THDI { get { return thdi; } set { thdi = (ushort)value; } }
        }

        public struct CalibrationFactors
        {
            public ushort CalibrationVL1;
            public ushort CalibrationVL2;
            public ushort CalibrationVL3;
            public ushort CalibrationI;
            public ushort CalibrationILCK;
            public ushort CalibrationIFactor;
            public ushort CalibrationDesfase;
            public ushort CalibrationPowerActive;
            public ushort CalibrationPowerReactive;
        };

        #endregion

        #region Enumerators

        public enum VoltageConfiguration
        {
            _110V = 0,
            _230V = 1,
            _400V = 2,
            _480V = 3
        }

        public enum SmartModels
        {
            Smart6 = 0,
            Smart12 = 8,
        }

        [Flags]
        public enum KeyboardKeys
        {
            UP = 0x1700,
            LEFT = 0x1D00,
            SETUP = 0x0F00,
            DOWN = 0x1B00,
            RIGHT = 0x1E00,
            NONE = 0x1F00
        }

        public enum DisplayConfigurations
        {
            NONE = 0x0000,
            LEFT_PART = 0x0001,
            RIGHT_PART = 0x0002
        }

        public enum Registers
        {
            IDP_REGISTERS = 0x0514,
            LEDS = 0x0018,
            DISPLAY = 0x03F5,
            FLAG_TEST = 0x2AF8,
            FIRMWARE_VERSION = 0x03E8,
            SETUP = 0x044C,
            ALARMS = 0x0578,
            SETUP_LIMITS = 0x04B0,
            RELAYS = 0x0000,
            KEYBOARD = 0x03F2,
            MEASURES_POINTS = 0x0384,
            CURRENT_MEASURE = 0x039E,
            LEAK_CURRENT_MEASURE = 0x03A0,
            VOLTAGE_L1_MEASURE = 0x03A2,
            VOLTAGE_L2_MEASURE = 0x03A4,
            VOLTAGE_L3_MEASURE = 0x03A6,
            KW_MEASURE = 0x03AA,
            KVAR_MEASURE = 0x03AC,
            KVA_MEASURE = 0x03AE,
            POWER_FACTOR_MEASURE = 0x025A,
            TEMPERATURE_MEASURE = 0x0284,
            THD_V_MEASURE = 0x026A,
            THD_I_MEASURE = 0x026B,
            HARMONICS_MEASURE = 0x026C,
            INTERNAL_FREQ = 0x03F6,
            DELETE_MAX = 0x03F4,
        }

        #endregion
    }
}