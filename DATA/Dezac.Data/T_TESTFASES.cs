namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_TESTFASES")]
    public partial class T_TESTFASES
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMTEST { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string IDFASE { get; set; }

        public int SECUENCIA { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual T_TEST T_TEST { get; set; }
    }
}
