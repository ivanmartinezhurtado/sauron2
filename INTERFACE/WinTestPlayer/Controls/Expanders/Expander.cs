﻿using Telerik.WinControls;
using System.ComponentModel;
using System.Drawing;

namespace WinTestPlayer.Controls.Expanders
{
    [ToolboxItem(true)]
    public class Expander : RadControl
    {
        ExpanderElement expanderElement;

        public Expander()
        {
            Orientation = ExpanderOrientation.Left;
        }

        [DefaultValue(ExpanderOrientation.Left)]
        public ExpanderOrientation Orientation
        {
            get
            {
                return expanderElement.Orientation;
            }
            set
            {
                expanderElement.Orientation = value;
            }
        }

        protected override void CreateChildItems(RadElement parent)
        {
            expanderElement = CreateExpanderElement();
            parent.Children.Add(expanderElement);
        }

        public ExpanderElement ExpanderElement
        {
            get
            {
                return expanderElement;
            }
        }

        public void Expand()
        {
            ExpanderElement.Expand();
        }

        public void Collapse()
        {
            ExpanderElement.Collapse();
        }

        protected virtual ExpanderElement CreateExpanderElement()
        {
            return new ExpanderElement();
        }

        protected override System.Drawing.Size DefaultSize
        {
            get
            {
                return new Size(80, 700);
            }
        }
    }
}
