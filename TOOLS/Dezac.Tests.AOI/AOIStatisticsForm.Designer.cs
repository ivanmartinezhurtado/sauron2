﻿namespace Dezac.Tests.AOI
{
    partial class AOIStatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.hWindow1 = new HalconDotNet.HSmartWindowControl();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonLoad = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalCircuitos = new Telerik.WinControls.UI.RadLabel();
            this.labelFormaDefectuosa = new Telerik.WinControls.UI.RadLabel();
            this.labelOtrosFallos = new Telerik.WinControls.UI.RadLabel();
            this.labelLevantado = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.labelPatasLargas = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.labelFaltasComponentes = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.labelFaltasEstaño = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.labelFalsosFallos = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalFalsosFallos = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalFaltaEstano = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalFaltaComponente = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalPatasLargas = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalFormaDefectuosa = new Telerik.WinControls.UI.RadLabel();
            this.labelCruces = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalCruces = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalLevantado = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalOtros = new Telerik.WinControls.UI.RadLabel();
            this.labelTotalFallos = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTotalSoldaduras = new Telerik.WinControls.UI.RadLabel();
            this.labelDPMO = new Telerik.WinControls.UI.RadLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxTipoFallo = new System.Windows.Forms.ComboBox();
            this.labelOrdenProducto = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonLoad)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalCircuitos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFormaDefectuosa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOtrosFallos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLevantado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPatasLargas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFaltasComponentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFaltasEstaño)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFalsosFallos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFalsosFallos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFaltaEstano)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFaltaComponente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalPatasLargas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFormaDefectuosa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCruces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalCruces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalLevantado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalOtros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFallos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalSoldaduras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDPMO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1503F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 361F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelOrdenProducto, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chart1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1904, 1001);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel2.Controls.Add(this.hWindow1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(20, 340);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1503, 641);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // hWindow1
            // 
            this.hWindow1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.hWindow1.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.hWindow1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hWindow1.HDoubleClickToFitContent = true;
            this.hWindow1.HDrawingObjectsModifier = HalconDotNet.HSmartWindowControl.DrawingObjectsModifier.None;
            this.hWindow1.HImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindow1.HKeepAspectRatio = true;
            this.hWindow1.HMoveContent = true;
            this.hWindow1.HZoomContent = HalconDotNet.HSmartWindowControl.ZoomContent.WheelForwardZoomsIn;
            this.hWindow1.Location = new System.Drawing.Point(0, 0);
            this.hWindow1.Margin = new System.Windows.Forms.Padding(0);
            this.hWindow1.Name = "hWindow1";
            this.hWindow1.Size = new System.Drawing.Size(1503, 689);
            this.hWindow1.TabIndex = 9;
            this.hWindow1.WindowSize = new System.Drawing.Size(1503, 689);
            this.hWindow1.HMouseUp += new HalconDotNet.HMouseEventHandler(this.HWindow1_HMouseUp);
            this.hWindow1.Load += new System.EventHandler(this.HWindow1_Load);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.buttonLoad, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(1523, 60);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel5, 3);
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 600F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(361, 921);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // buttonLoad
            // 
            this.buttonLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonLoad.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoad.Location = new System.Drawing.Point(3, 3);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(174, 34);
            this.buttonLoad.TabIndex = 0;
            this.buttonLoad.Text = "Cargar Orden";
            this.buttonLoad.Click += new System.EventHandler(this.ButtonLoad_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel5.SetColumnSpan(this.tableLayoutPanel6, 2);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel6.Controls.Add(this.radLabel15, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalCircuitos, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.labelFormaDefectuosa, 2, 9);
            this.tableLayoutPanel6.Controls.Add(this.labelOtrosFallos, 2, 12);
            this.tableLayoutPanel6.Controls.Add(this.labelLevantado, 2, 11);
            this.tableLayoutPanel6.Controls.Add(this.radLabel4, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.labelPatasLargas, 2, 8);
            this.tableLayoutPanel6.Controls.Add(this.radLabel8, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.labelFaltasComponentes, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.radLabel6, 0, 8);
            this.tableLayoutPanel6.Controls.Add(this.labelFaltasEstaño, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.radLabel12, 0, 10);
            this.tableLayoutPanel6.Controls.Add(this.labelFalsosFallos, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.radLabel18, 0, 9);
            this.tableLayoutPanel6.Controls.Add(this.radLabel3, 0, 11);
            this.tableLayoutPanel6.Controls.Add(this.radLabel16, 0, 12);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalFalsosFallos, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalFaltaEstano, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalFaltaComponente, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalPatasLargas, 1, 8);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalFormaDefectuosa, 1, 9);
            this.tableLayoutPanel6.Controls.Add(this.labelCruces, 2, 10);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalCruces, 1, 10);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalLevantado, 1, 11);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalOtros, 1, 12);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalFallos, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.labelTotalSoldaduras, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.labelDPMO, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.comboBoxTipoFallo, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 13;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(361, 600);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // radLabel15
            // 
            this.radLabel15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel15.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.Location = new System.Drawing.Point(4, 215);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(117, 22);
            this.radLabel15.TabIndex = 0;
            this.radLabel15.Text = "Falsos Fallos:";
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalCircuitos
            // 
            this.labelTotalCircuitos.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalCircuitos.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalCircuitos.Location = new System.Drawing.Point(220, 92);
            this.labelTotalCircuitos.Name = "labelTotalCircuitos";
            this.labelTotalCircuitos.Size = new System.Drawing.Size(19, 22);
            this.labelTotalCircuitos.TabIndex = 32;
            this.labelTotalCircuitos.Text = "0";
            this.labelTotalCircuitos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFormaDefectuosa
            // 
            this.labelFormaDefectuosa.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelFormaDefectuosa.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFormaDefectuosa.Location = new System.Drawing.Point(299, 379);
            this.labelFormaDefectuosa.Name = "labelFormaDefectuosa";
            this.labelFormaDefectuosa.Size = new System.Drawing.Size(19, 22);
            this.labelFormaDefectuosa.TabIndex = 0;
            this.labelFormaDefectuosa.Text = "0";
            this.labelFormaDefectuosa.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelOtrosFallos
            // 
            this.labelOtrosFallos.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOtrosFallos.Location = new System.Drawing.Point(299, 496);
            this.labelOtrosFallos.Name = "labelOtrosFallos";
            this.labelOtrosFallos.Size = new System.Drawing.Size(19, 22);
            this.labelOtrosFallos.TabIndex = 0;
            this.labelOtrosFallos.Text = "0";
            this.labelOtrosFallos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLevantado
            // 
            this.labelLevantado.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelLevantado.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLevantado.Location = new System.Drawing.Point(299, 461);
            this.labelLevantado.Name = "labelLevantado";
            this.labelLevantado.Size = new System.Drawing.Size(19, 22);
            this.labelLevantado.TabIndex = 0;
            this.labelLevantado.Text = "0";
            this.labelLevantado.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(4, 256);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(146, 22);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Faltas de Estaño:";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPatasLargas
            // 
            this.labelPatasLargas.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelPatasLargas.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatasLargas.Location = new System.Drawing.Point(299, 338);
            this.labelPatasLargas.Name = "labelPatasLargas";
            this.labelPatasLargas.Size = new System.Drawing.Size(19, 22);
            this.labelPatasLargas.TabIndex = 0;
            this.labelPatasLargas.Text = "0";
            this.labelPatasLargas.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel8
            // 
            this.radLabel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(4, 297);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(196, 22);
            this.radLabel8.TabIndex = 0;
            this.radLabel8.Text = "Faltas de Componente:";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFaltasComponentes
            // 
            this.labelFaltasComponentes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelFaltasComponentes.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFaltasComponentes.Location = new System.Drawing.Point(299, 297);
            this.labelFaltasComponentes.Name = "labelFaltasComponentes";
            this.labelFaltasComponentes.Size = new System.Drawing.Size(19, 22);
            this.labelFaltasComponentes.TabIndex = 0;
            this.labelFaltasComponentes.Text = "0";
            this.labelFaltasComponentes.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(4, 338);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(116, 22);
            this.radLabel6.TabIndex = 0;
            this.radLabel6.Text = "Patas Largas:";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFaltasEstaño
            // 
            this.labelFaltasEstaño.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelFaltasEstaño.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFaltasEstaño.Location = new System.Drawing.Point(299, 256);
            this.labelFaltasEstaño.Name = "labelFaltasEstaño";
            this.labelFaltasEstaño.Size = new System.Drawing.Size(19, 22);
            this.labelFaltasEstaño.TabIndex = 0;
            this.labelFaltasEstaño.Text = "0";
            this.labelFaltasEstaño.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel12
            // 
            this.radLabel12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel12.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(4, 420);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(66, 22);
            this.radLabel12.TabIndex = 0;
            this.radLabel12.Text = "Cruces:";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFalsosFallos
            // 
            this.labelFalsosFallos.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelFalsosFallos.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFalsosFallos.Location = new System.Drawing.Point(299, 215);
            this.labelFalsosFallos.Name = "labelFalsosFallos";
            this.labelFalsosFallos.Size = new System.Drawing.Size(19, 22);
            this.labelFalsosFallos.TabIndex = 0;
            this.labelFalsosFallos.Text = "0";
            this.labelFalsosFallos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel18
            // 
            this.radLabel18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel18.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel18.Location = new System.Drawing.Point(4, 379);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(163, 22);
            this.radLabel18.TabIndex = 0;
            this.radLabel18.Text = "Forma Defectuosa:";
            this.radLabel18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(4, 461);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(209, 22);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Componente Levantado:";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel16.Location = new System.Drawing.Point(4, 496);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(111, 22);
            this.radLabel16.TabIndex = 0;
            this.radLabel16.Text = "Otros Fallos:";
            this.radLabel16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalFalsosFallos
            // 
            this.labelTotalFalsosFallos.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalFalsosFallos.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalFalsosFallos.Location = new System.Drawing.Point(220, 215);
            this.labelTotalFalsosFallos.Name = "labelTotalFalsosFallos";
            this.labelTotalFalsosFallos.Size = new System.Drawing.Size(19, 22);
            this.labelTotalFalsosFallos.TabIndex = 0;
            this.labelTotalFalsosFallos.Text = "0";
            this.labelTotalFalsosFallos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalFaltaEstano
            // 
            this.labelTotalFaltaEstano.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalFaltaEstano.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalFaltaEstano.Location = new System.Drawing.Point(220, 256);
            this.labelTotalFaltaEstano.Name = "labelTotalFaltaEstano";
            this.labelTotalFaltaEstano.Size = new System.Drawing.Size(19, 22);
            this.labelTotalFaltaEstano.TabIndex = 0;
            this.labelTotalFaltaEstano.Text = "0";
            this.labelTotalFaltaEstano.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalFaltaComponente
            // 
            this.labelTotalFaltaComponente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalFaltaComponente.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalFaltaComponente.Location = new System.Drawing.Point(220, 297);
            this.labelTotalFaltaComponente.Name = "labelTotalFaltaComponente";
            this.labelTotalFaltaComponente.Size = new System.Drawing.Size(19, 22);
            this.labelTotalFaltaComponente.TabIndex = 0;
            this.labelTotalFaltaComponente.Text = "0";
            this.labelTotalFaltaComponente.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalPatasLargas
            // 
            this.labelTotalPatasLargas.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalPatasLargas.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalPatasLargas.Location = new System.Drawing.Point(220, 338);
            this.labelTotalPatasLargas.Name = "labelTotalPatasLargas";
            this.labelTotalPatasLargas.Size = new System.Drawing.Size(19, 22);
            this.labelTotalPatasLargas.TabIndex = 0;
            this.labelTotalPatasLargas.Text = "0";
            this.labelTotalPatasLargas.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalFormaDefectuosa
            // 
            this.labelTotalFormaDefectuosa.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalFormaDefectuosa.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalFormaDefectuosa.Location = new System.Drawing.Point(220, 379);
            this.labelTotalFormaDefectuosa.Name = "labelTotalFormaDefectuosa";
            this.labelTotalFormaDefectuosa.Size = new System.Drawing.Size(19, 22);
            this.labelTotalFormaDefectuosa.TabIndex = 0;
            this.labelTotalFormaDefectuosa.Text = "0";
            this.labelTotalFormaDefectuosa.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCruces
            // 
            this.labelCruces.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCruces.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCruces.Location = new System.Drawing.Point(299, 420);
            this.labelCruces.Name = "labelCruces";
            this.labelCruces.Size = new System.Drawing.Size(19, 22);
            this.labelCruces.TabIndex = 0;
            this.labelCruces.Text = "0";
            this.labelCruces.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalCruces
            // 
            this.labelTotalCruces.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalCruces.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalCruces.Location = new System.Drawing.Point(220, 420);
            this.labelTotalCruces.Name = "labelTotalCruces";
            this.labelTotalCruces.Size = new System.Drawing.Size(19, 22);
            this.labelTotalCruces.TabIndex = 0;
            this.labelTotalCruces.Text = "0";
            this.labelTotalCruces.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalLevantado
            // 
            this.labelTotalLevantado.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalLevantado.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalLevantado.Location = new System.Drawing.Point(220, 461);
            this.labelTotalLevantado.Name = "labelTotalLevantado";
            this.labelTotalLevantado.Size = new System.Drawing.Size(19, 22);
            this.labelTotalLevantado.TabIndex = 0;
            this.labelTotalLevantado.Text = "0";
            this.labelTotalLevantado.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalOtros
            // 
            this.labelTotalOtros.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalOtros.Location = new System.Drawing.Point(220, 496);
            this.labelTotalOtros.Name = "labelTotalOtros";
            this.labelTotalOtros.Size = new System.Drawing.Size(19, 22);
            this.labelTotalOtros.TabIndex = 0;
            this.labelTotalOtros.Text = "0";
            this.labelTotalOtros.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalFallos
            // 
            this.labelTotalFallos.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalFallos.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalFallos.Location = new System.Drawing.Point(220, 51);
            this.labelTotalFallos.Name = "labelTotalFallos";
            this.labelTotalFallos.Size = new System.Drawing.Size(19, 22);
            this.labelTotalFallos.TabIndex = 32;
            this.labelTotalFallos.Text = "0";
            this.labelTotalFallos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Total Fallos:";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Total Circuitos:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Total Soldaduras:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "DPMO:";
            // 
            // labelTotalSoldaduras
            // 
            this.labelTotalSoldaduras.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelTotalSoldaduras.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalSoldaduras.Location = new System.Drawing.Point(220, 133);
            this.labelTotalSoldaduras.Name = "labelTotalSoldaduras";
            this.labelTotalSoldaduras.Size = new System.Drawing.Size(19, 22);
            this.labelTotalSoldaduras.TabIndex = 32;
            this.labelTotalSoldaduras.Text = "0";
            this.labelTotalSoldaduras.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDPMO
            // 
            this.labelDPMO.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDPMO.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDPMO.Location = new System.Drawing.Point(220, 174);
            this.labelDPMO.Name = "labelDPMO";
            this.labelDPMO.Size = new System.Drawing.Size(19, 22);
            this.labelDPMO.TabIndex = 32;
            this.labelDPMO.Text = "0";
            this.labelDPMO.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Filtrar por Tipo:";
            // 
            // comboBoxTipoFallo
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.comboBoxTipoFallo, 3);
            this.comboBoxTipoFallo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxTipoFallo.FormattingEnabled = true;
            this.comboBoxTipoFallo.Location = new System.Drawing.Point(220, 4);
            this.comboBoxTipoFallo.Name = "comboBoxTipoFallo";
            this.comboBoxTipoFallo.Size = new System.Drawing.Size(145, 21);
            this.comboBoxTipoFallo.TabIndex = 33;
            this.comboBoxTipoFallo.SelectedValueChanged += new System.EventHandler(this.ComboBoxTipoFallo_SelectedValueChanged);
            // 
            // labelOrdenProducto
            // 
            this.labelOrdenProducto.AutoSize = true;
            this.labelOrdenProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOrdenProducto.Location = new System.Drawing.Point(23, 20);
            this.labelOrdenProducto.Name = "labelOrdenProducto";
            this.labelOrdenProducto.Size = new System.Drawing.Size(282, 24);
            this.labelOrdenProducto.TabIndex = 4;
            this.labelOrdenProducto.Text = "Nº Orden: X - Nº Producto: Y";
            // 
            // chart1
            // 
            chartArea1.AxisY2.Maximum = 100D;
            chartArea1.AxisY2.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(23, 63);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Weldings";
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Percentage";
            series2.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(1497, 254);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            this.chart1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Chart1_MouseUp);
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radLabel5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(4, 92);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(133, 22);
            this.radLabel5.TabIndex = 31;
            this.radLabel5.Text = "Total Circuitos";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AOIStatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 1001);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AOIStatisticsForm";
            this.Text = "AOIStatistics";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonLoad)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalCircuitos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFormaDefectuosa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOtrosFallos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLevantado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPatasLargas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFaltasComponentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFaltasEstaño)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFalsosFallos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFalsosFallos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFaltaEstano)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFaltaComponente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalPatasLargas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFormaDefectuosa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCruces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalCruces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalLevantado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalOtros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalFallos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTotalSoldaduras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDPMO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel labelCruces;
        private Telerik.WinControls.UI.RadLabel labelFormaDefectuosa;
        private Telerik.WinControls.UI.RadLabel labelPatasLargas;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel labelFaltasComponentes;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel labelFaltasEstaño;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel labelFalsosFallos;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel labelOtrosFallos;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel labelLevantado;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private Telerik.WinControls.UI.RadLabel labelTotalCircuitos;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private System.Windows.Forms.Label labelOrdenProducto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private Telerik.WinControls.UI.RadButton buttonLoad;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private HalconDotNet.HSmartWindowControl hWindow1;
        private Telerik.WinControls.UI.RadLabel labelTotalFalsosFallos;
        private Telerik.WinControls.UI.RadLabel labelTotalFaltaEstano;
        private Telerik.WinControls.UI.RadLabel labelTotalFaltaComponente;
        private Telerik.WinControls.UI.RadLabel labelTotalPatasLargas;
        private Telerik.WinControls.UI.RadLabel labelTotalFormaDefectuosa;
        private Telerik.WinControls.UI.RadLabel labelTotalCruces;
        private Telerik.WinControls.UI.RadLabel labelTotalLevantado;
        private Telerik.WinControls.UI.RadLabel labelTotalOtros;
        private Telerik.WinControls.UI.RadLabel labelTotalFallos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadLabel labelTotalSoldaduras;
        private Telerik.WinControls.UI.RadLabel labelDPMO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxTipoFallo;
    }
}