﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Dezac.Tests.Visio
{
    public abstract class Camera : IDisposable
    {
        protected abstract void CreateCamera();
        public abstract void Start();
        public abstract void Stop();
        public abstract Bitmap GetImage();
        public Bitmap GetImage(int row1, int column1, int row2, int column2, double resizeFactor)
        {
            Bitmap image = GetImage();

            Bitmap imageCroped = image;
            if (row2 != 0 && column2 != 0)
                imageCroped = CropImage(image, row1, column1, row2, column2);

            Bitmap imageResized = imageCroped;
            if (resizeFactor != 0 && resizeFactor != 1)
                imageResized = ResizeImage(imageCroped, resizeFactor);

            return imageResized;
        }
        private Bitmap ResizeImage(Bitmap image, double factor)
        {
            var destRect = new Rectangle(0, 0, (int)(image.Width * factor), (int)(image.Height * factor));
            var destImage = new Bitmap((int)(image.Width * factor), (int)(image.Height * factor));

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
        private Bitmap CropImage(Bitmap img, int row1, int column1, int row2, int column2)
        {
            Bitmap bmpImage = new Bitmap(img);
            var cropArea = new Rectangle(column1, row1, column2 - column1, row2 - row1);
            return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        }
        public abstract void Dispose();
    }
}
