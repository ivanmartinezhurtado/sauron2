﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.00)]
    public abstract class MeasureBase<T> : IMeasure
    {
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(T));

        public class FunctionConfig
        {
            public string Range { get; set; }
            public string Resolution { get; set; }
        }

        public class MeasureStatus
        {
            public bool IsBusy { get; set; }
            public bool HasWarning { get; set; }
            public bool HasError { get; set; }

            public bool IsOK { get { return !IsBusy && !HasWarning && !HasError; } }
        }

        public string GetInstrumentVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(InstrumentsVersionAttribute), true).FirstOrDefault() as InstrumentsVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }

        internal MeasureStatus Status { get; set; }

        protected Dictionary<int, FunctionConfig> functionParams;

        public bool ThrowException { get; set; }

        protected MessageTransport transport;

        protected IProtocol protocol;

        public enum Commands
        {
            RESET,
            READ_VARIABLE,
            READ_STATUS,
            READ_IDN,

            USER_DEFINED_COMMANDS = 9999
        };

        public MeasureBase()
        {
            Status = new MeasureStatus();
        }

        public virtual FunctionConfig SetFunctionConfig(int function, string range, string resolution)
        {
            FunctionConfig config = GetFunctionConfig(function);
            config.Range = range;
            config.Resolution = resolution;

            return config;
        }

        public virtual FunctionConfig GetFunctionConfig(int function)
        {
            if (functionParams == null)
                functionParams = new Dictionary<int, FunctionConfig>();

            FunctionConfig config = null;

            if (!functionParams.TryGetValue((int)function, out config))
            {
                config = new FunctionConfig();
                functionParams.Add((int)function, config);
            }

            return config;
        }

        public virtual Result<T> Read<T>(int variable, bool? throwException = null)
        {
            T result = default(T);

            var cmd = new Command<Commands>(Commands.READ_VARIABLE, throwException, variable, result);

            if (SendCommand(cmd))
                return new Result<T>((T)Convert.ChangeType(cmd.Args[1], typeof(T)));

            return new Result<T>(cmd.Exception);
        }

        public virtual Result<MeasureStatus> ReadStatus(bool? throwException = null)
        {
            var cmd = new Command<Commands>(Commands.READ_STATUS, throwException);

            if (SendCommand(cmd))
                return new Result<MeasureStatus>(Status);

            return new Result<MeasureStatus>(cmd.Exception);
        }

        public virtual Result<bool> Reset(bool? throwException = null)
        {
            var cmd = new Command<Commands>(Commands.RESET, throwException);
            if (SendCommand(cmd))
                return true;

            return new Result<bool>(cmd.Exception);
        }

        protected bool SendCommand<T>(Command<T> command)
        {
            try
            {
                _logger.InfoFormat("Command {0}", command);
                ExecuteCommand(command, 3);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                command.Exception = ex;
                if (command.ThrowException.GetValueOrDefault(ThrowException))
                    throw;

                return false;
            }
        }

        private void ExecuteCommand<T>(Command<T> command, int numRetries)
        {
            bool error = false;
            do
            {
                try
                {
                    error = false;
                    InternalSendCommand(command);
                }
                catch (Exception ex)
                {
                    error = true;
                    numRetries--;

                    _logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                    if (numRetries <= 0 || ex is NotSupportedException)
                        throw;
                }
            } while (error);
        }

        protected abstract void InternalSendCommand<T>(Command<T> command);
    }
}
