﻿namespace Dezac.Tests.Services
{
    public interface ITestReportsService
    {
        void ShowBoxReport(int numOrden, int numCaja, int unidadesCaja, bool print, bool preview, string printerReport);
        byte[] ShowTestReport(string reportName, int numTestFase, bool preview = true, bool pdf = false, bool print = false, string printerReport="");
        byte[] ExportPdf(string reportName, object data);
    }
}
