﻿namespace Comunications.Message
{
    public interface IValidateMessage
    {
        void ValidateResponse(ITransportMessage response);
    }
}
