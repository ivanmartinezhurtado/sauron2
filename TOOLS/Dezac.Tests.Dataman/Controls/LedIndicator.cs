﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.Dataman.Controls
{
    public partial class LedIndicator : UserControl
    {
        private bool isOn;

        public LedIndicator()
        {
            InitializeComponent();
        }

        public Color ColorOn { get; set; }
        public Color ColorOff { get; set; }

        public string CaptionText
        {
            get { return this.LabelText.Text; }
            set { this.LabelText.Text = value; }
        }

        public bool IsOn
        {
            get { return isOn; }
            set
            {
                isOn = value;
                this.Refresh();
            }
        }

        private void Led_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                e.Graphics.FillEllipse(new SolidBrush(isOn ? ColorOn : ColorOff), 0, 0, Led.Width - 1, Led.Height - 1);
                e.Graphics.DrawEllipse(new Pen(Color.Black), 0, 0, Led.Width - 1, Led.Height - 1);
            } catch { }
        }

        private void LedIndicator_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                Led.Height = this.Height - 4;
                Led.Width = Led.Height;
                LabelText.Left = Led.Width + 8;
                LabelText.Top = ((this.Height - LabelText.Height) / 2) + 2;
                LabelText.Width = this.Width - LabelText.Left - 4;
            } catch { }
        }
    }
}
