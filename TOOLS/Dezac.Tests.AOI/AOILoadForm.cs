﻿using Dezac.Tests.AOI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Tests.AOI
{
    public partial class AOILoadForm : Form
    {
        public List<AOIStatistics> StatisticsList { get; set; }
        public AOILoadForm()
        {
            InitializeComponent();
            GridFiles.LoadLayout(ConfigurationManager.AppSettings["GridFilesCongfig"]);
            StatisticsList = new List<AOIStatistics>();
        }

        public AOILoadForm(List<AOIStatistics> statisticsList)
            : this()
        {
            GridFiles.DataSource = statisticsList;
        }

        private void AOILoadForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            GridFiles.SaveLayout(ConfigurationManager.AppSettings["GridFilesCongfig"]);
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            GridFiles.SelectedRows.ToList().ForEach((row) => StatisticsList.Add(row.DataBoundItem as AOIStatistics));
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
