﻿using Dezac.Tests.Model;
using System;

namespace Dezac.Tests.Parameters
{
    public class IdentificationParameters
    {
        ParamValueCollection ident;

        public int RDNI_MAX { get { return Convert.ToInt32(ident.GetDouble(ConstantsParameters.Identification.RDNI_MAX, 9999999, ParamUnidad.Ohms)); } }
        public int RDNI_MIN { get { return Convert.ToInt32(ident.GetDouble(ConstantsParameters.Identification.RDNI_MIN,-9999999, ParamUnidad.Ohms)); } }
        public ushort ROM_CODE { get { return Convert.ToUInt16(ident.GetDouble(ConstantsParameters.Identification.ROM_CODE, 0, ParamUnidad.SinUnidad)); } }
        public string VERSION_FIRMWARE { get { return ident.GetString(ConstantsParameters.Identification.VERSION_FIRMWARE, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_FIRMWARE_BLUETOOTH { get { return ident.GetString(ConstantsParameters.Identification.VERSION_FIRMWARE_BLUETOOTH, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_FIRMWARE_PLACA_COM { get { return ident.GetString(ConstantsParameters.Identification.VERSION_FIRMWARE_PLACA_COM, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_FIRMWARE_2 { get { return ident.GetString(ConstantsParameters.Identification.VERSION_FIRMWARE_2, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_BOOT { get { return ident.GetString(ConstantsParameters.Identification.VERSION_BOOT, "0.0.0", ParamUnidad.SinUnidad); } }
        public string CRC_BOOT { get { return ident.GetString(ConstantsParameters.Identification.CRC_BOOT, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_FIRMWARE_SBT { get { return ident.GetString(ConstantsParameters.Identification.VERSION_FIRMWARE_SBT, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_FIRMWARE_MODEM_PLC { get { return ident.GetString(ConstantsParameters.Identification.VERSION_FIRMWARE_MODEM_PLC, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_HARDWARE { get { return ident.GetString(ConstantsParameters.Identification.VERSION_HARDWARE, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_PRODUCTO { get { return ident.GetString(ConstantsParameters.Identification.VERSION_PRODUCTO, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_EMBEDDED { get { return ident.GetString(ConstantsParameters.Identification.VERSION_EMBEDDED, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_EMBEDDED_FINALIZADO { get { return ident.GetString(ConstantsParameters.Identification.VERSION_EMBEDDED, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_DEVELOPER { get { return ident.GetString(ConstantsParameters.Identification.VERSION_DEVELOPER, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_STG { get { return ident.GetString(ConstantsParameters.Identification.VERSION_STG, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_PLC { get { return ident.GetString(ConstantsParameters.Identification.VERSION_PLC, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_POWER_STUDIO { get { return ident.GetString(ConstantsParameters.Identification.VERSION_POWER_STUDIO, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VERSION_TEST_PASARELA { get { return ident.GetString(ConstantsParameters.Identification.VERSION_TEST_PASARELA, "0.0.0", ParamUnidad.SinUnidad); } }
        public string VECTOR_HARDWARE { get { return ident.GetString(ConstantsParameters.Identification.VECTOR_HARDWARE, "0000", ParamUnidad.SinUnidad); } }
        public long CRC_FIRMWARE { get { return Convert.ToInt64(ident.GetDouble(ConstantsParameters.Identification.CRC_FIRMWARE, 0, ParamUnidad.SinUnidad)); } }
        public long CRC_FIRMWARE_FAB { get { return Convert.ToInt64(ident.GetDouble(ConstantsParameters.Identification.CRC_FIRMWARE_FAB, 0, ParamUnidad.SinUnidad)); } }
        public string MODELO { get { return ident.GetString(ConstantsParameters.Identification.MODELO, "0000", ParamUnidad.SinUnidad); } }
        public string MODELO_FAB { get { return ident.GetString(ConstantsParameters.Identification.MODELO_FAB, "0000", ParamUnidad.SinUnidad); } }
        public string MODELO_3G { get { return ident.GetString(ConstantsParameters.Identification.MODELO_3G, "0000", ParamUnidad.SinUnidad); } }
        public string MODELO_CLIENTE { get { return ident.GetString(ConstantsParameters.Identification.MODELO_CLIENTE, "0000", ParamUnidad.SinUnidad); } }
        public string MODELO_HARDWARE { get { return ident.GetString(ConstantsParameters.Identification.MODELO_HARDWARE, "0000", ParamUnidad.SinUnidad); } }
        public string MODELO_FABRICA { get { return ident.GetString(ConstantsParameters.Identification.MODELO_FABRICA, "0000", ParamUnidad.SinUnidad); } }
        public string MODELO_SBT { get { return ident.GetString(ConstantsParameters.Identification.MODELO_SBT, "0000", ParamUnidad.SinUnidad); } }
        public string SUBMODELO { get { return ident.GetString(ConstantsParameters.Identification.SUBMODELO, "CIR", ParamUnidad.SinUnidad); } }
        public string SUBMODELO_FAB { get { return ident.GetString(ConstantsParameters.Identification.SUBMODELO_FAB, "CIR", ParamUnidad.SinUnidad); } }

        public string VERSION_BOOT_MODULO { get { return ident.GetString(ConstantsParameters.Identification.VERSION_BOOT_MODULO, "1.1.0", ParamUnidad.SinUnidad); } }
        public string SEQUENCE_FILE { get { return ident.GetString(ConstantsParameters.Identification.SEQUENCE_FILE, "XXXXXX.SEQ", ParamUnidad.SinUnidad); } }
        public string MODELO_DIFERENCIAL { get { return ident.GetString(ConstantsParameters.Identification.MODELO_DIFERENCIAL, "0000000", ParamUnidad.SinUnidad); } }
        public string MODELO_MAGNETOTERMICO { get { return ident.GetString(ConstantsParameters.Identification.MODELO_MAGNETOTERMICO, "0000000", ParamUnidad.SinUnidad); } }
        public string MESSAGE_DISPLAY { get { return ident.GetString(ConstantsParameters.Identification.MESSAGE_DISPLAY, "XXXXXXXX", ParamUnidad.SinUnidad); } }
        public string CODIGO_UNESA { get { return ident.GetString(ConstantsParameters.Identification.CODIGO_UNESA, "0000", ParamUnidad.SinUnidad); } }
        public string CODE_ERROR { get { return ident.GetString(ConstantsParameters.Identification.CODE_ERROR, "0000", ParamUnidad.SinUnidad); } }
        public string CUSTOMIZATION_FILE { get { return ident.GetString(ConstantsParameters.Identification.CUSTOMIZATION_FILE, "DEFAULT", ParamUnidad.SinUnidad); } }
        public string CUSTOMIZATION_FILE_TEST { get { return ident.GetString(ConstantsParameters.Identification.CUSTOMIZATION_FILE_TEST, "DEFAULT", ParamUnidad.SinUnidad); } }
        public int NUM_SUBCONJUNTO { get { return (int)ident.GetDouble(ConstantsParameters.Identification.NUM_SUBCONJUNTO, 0, ParamUnidad.SinUnidad); } }
        public string COSTUMER { get { return ident.GetString(ConstantsParameters.Identification.COSTUMER, "CIRCUTOR", ParamUnidad.SinUnidad); } }
        public string MAC_CUSTOMER { get { return ident.GetString(ConstantsParameters.Identification.MAC_CUSTOMER, "MARCA", ParamUnidad.SinUnidad); } }
        public string ESTRUCTURA_SUBCONJUNTOS { get { return ident.GetString(ConstantsParameters.Identification.ESTRUCTURA_SUBCONJUNTOS, "-", ParamUnidad.SinUnidad); } }
        public int CABECERA_NUMERO_SERIE { get { return (int)ident.GetDouble(ConstantsParameters.Identification.CABECERA_NUMERO_SERIE, 0, ParamUnidad.SinUnidad); } }
        public int LONGITUD_NUMERO_SERIE { get { return (int)ident.GetDouble(ConstantsParameters.Identification.LONGITUD_NUMERO_SERIE, 0, ParamUnidad.SinUnidad); } }
        public string VERSION_DSP { get { return ident.GetString(ConstantsParameters.Identification.VERSION_DSP, "CPU-0.0.0.00", ParamUnidad.SinUnidad); } }
        public string VERSION_ATMEL { get { return ident.GetString(ConstantsParameters.Identification.VERSION_ATMEL, "PWS-0.0.0.00", ParamUnidad.SinUnidad); } }
        public string VERSION_APLI { get { return ident.GetString(ConstantsParameters.Identification.VERSION_APLI, "0.0.0", ParamUnidad.SinUnidad); } }
        public string SN_PATRON { get { return ident.GetString(ConstantsParameters.Identification.SN_PATRON, "0000000000", ParamUnidad.SinUnidad); } }
        public string VERSION_CPU { get { return ident.GetString(ConstantsParameters.Identification.VERSION_CPU, "00", ParamUnidad.SinUnidad); } }
        public string CARATULA { get { return ident.GetString(ConstantsParameters.Identification.CARATULA, "NO", ParamUnidad.SinUnidad); } }

        public string MID { get { return ident.GetString(ConstantsParameters.Identification.MID, "SI", ParamUnidad.SinUnidad); } }
        public string PASARELA { get { return ident.GetString(ConstantsParameters.Identification.PASARELA, "NO", ParamUnidad.SinUnidad); } }
        public string ID { get { return ident.GetString(ConstantsParameters.Identification.ID, "0000000000", ParamUnidad.SinUnidad); } }
        public IdentificationParameters(ParamValueCollection identificacion)
        {
            ident = identificacion;
        }
    }
}
