﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Services;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using WinTaskRunner.Design;
using WinTaskRunner.Design.Documents;
using WinTaskRunner.Design.Services;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(RazorTemplateActionDescription))]
    public class RazorTemplateAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Campo donde se debe escribir el código plantilla para razor")]
        [Editor(typeof(TemplateEditor), typeof(UITypeEditor))]
        public string Code { get; set; }

        [Category("3. Configuration")]
        [Description("Path del fichero donde cargar el código de la plantilla para razor")]
        public ActionVariable FileName { get; set; } = new ActionVariable();
        
        
        [Category("4. Results")]
        [Description("Plantilla resultante para usar en la secuencia")]
        public string DataOutput { get; set; }

        public override void Execute()
        {
            var context = Context;
            var config = new TemplateServiceConfiguration();
            
            config.BaseTemplateType = typeof(TestTemplateBase<>);
            using (var service = RazorEngineService.Create(config))
            {
                var template = Code;

                if (FileName != null  && !FileName.IsKeyNullOrEmpty)
                    template = File.ReadAllText(FileName.Value.ToString());

                var key = context.Step.Name;

                if (context.RunMode != RunMode.Release)
                    key += "_" + DateTime.Now.Ticks;

                string result = service.RunCompile(template, key, typeof(ActionBase), this);

                Logger.InfoFormat("RazorTemplate -> {0}", DataOutput);
                Logger.InfoFormat("{0}", result);

                if (!string.IsNullOrEmpty(DataOutput))
                    SetVariable(DataOutput, result);

                context.ResultList.Add(DataOutput ?? key, result);
            }
        }
    }

    public abstract class TestTemplateBase<T> : TemplateBase<T>
    {
        public TestTemplateBase()
        {
            Test = new TestScriptHost();
        }

        public TestScriptHost Test { get; set; }

        public SequenceContext Context { get { return SequenceContext.Current; } }

        public ITestContext TestContext { get { return Context?.Services.Get<ITestContext>(); } }
    }

    public class RazorTemplateActionDescription : ActionDescription
    {
        public override string Name { get { return "CodeGenerator"; }}
        public override string Category { get { return "Actions/Developer"; } }
        public override string Description { get { return "Crea una plantilla de código con el contenido de la propiedad 'Code' (Código donde se escribe en cualquier tipo de código, por ejemplo XML, por ejemplo sirve para grabar el número de serie en las diferentes placas de un equipo)."; } }
        public override Image Icon { get { return Properties.Resources.Other_xml_icon; } }
    }

    class TemplateEditor : UITypeEditor
    {
        private Dictionary<object, SourceCodeDocument> docs = new Dictionary<object, SourceCodeDocument>();

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            SourceCodeDocument doc = null;

            if (!docs.TryGetValue(context.Instance, out doc))
            {
                var action = (RazorTemplateAction)context.Instance;

                var docService = ServiceContainer.Resolve<IDocumentService>();

                doc = docService.AddNew(".cshtml") as SourceCodeDocument;
                doc.Text = action.Code;
                doc.TextChanged += (s, e) =>
                {
                    action.Code = e;
                };

                docs.Add(context.Instance, doc);
            }

            return doc.Text;
        }
    }
}
