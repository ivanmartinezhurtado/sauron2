using System;
using System.Collections.Generic;

namespace TaskRunner.Model
{
    public sealed class ServiceCollection : Dictionary<Type, object>
    {
        public event EventHandler<object> ServiceAdded;

        public ServiceCollection()
        {
        }

        public void Add<TService>(TService serviceInstance)
        {
            Add(typeof(TService), serviceInstance);
        }

        public new void Add(Type serviceType, object serviceInstance)
        {
            base.Add(serviceType, serviceInstance);

            if (ServiceAdded != null)
                ServiceAdded(this, serviceInstance);
        }

        public TService AddNew<TService>()
        {
            return AddNew<TService, TService>();
        }

        public TService AddNew<TService, TRegisterAs>()
            where TService : TRegisterAs
        {
            return (TService)AddNew(typeof(TService), typeof(TRegisterAs));
        }

        public object AddNew(Type serviceType)
        {
            return AddNew(serviceType, serviceType);
        }

        public object AddNew(Type serviceType, Type registerAs)
        {
            object instance = Activator.CreateInstance(serviceType);

            Add(registerAs, instance);

            return instance;
        }

        public bool Contains<TService>()
        {
            return Contains(typeof(TService));
        }

        public bool Contains(Type serviceType)
        {
            return ContainsKey(serviceType);
        }

        public TService Get<TService>()
        {
            return (TService)Get(typeof(TService));
        }

        public object Get(Type serviceType)
        {


            if (TryGetValue(serviceType, out object result))
                return result;

            return null;
        }

        public void Remove<TService>()
        {
            Remove(typeof(TService));
        }

        public TService Create<TService>()
        {
            return (TService)Create(typeof(TService));
        }

        public object Create(Type serviceType)
        {

            if (TryGetValue(serviceType, out object serviceInstance))
                return Activator.CreateInstance(serviceInstance.GetType());

            return null;
        }

        //public void InitializeServices()
        //{
        //    foreach (object item in Values)
        //    {
        //        if (item is IInitializeService)
        //            ((IInitializeService)item).Initialize();
        //    }
        //}
    }
}
