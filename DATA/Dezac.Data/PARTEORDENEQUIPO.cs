namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PARTEORDENEQUIPO")]
    public partial class PARTEORDENEQUIPO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARTE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMORDEN { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string NROSERIE { get; set; }
    }
}
