﻿using FastColoredTextBoxNS;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WinTaskRunner.Design.Documents;

namespace WinTaskRunner.Design.Services
{
    public class SourceCodeDocumentManager : IDocumentManager
    {
        private Style sameWordsStyle = new MarkerStyle(new SolidBrush(Color.FromArgb(50, Color.Gray)));
        string[] keywords = { "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char", "checked", "class", "const", "continue", "decimal", "default", "delegate", "do", "double", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "foreach", "goto", "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace", "new", "null", "object", "operator", "out", "override", "params", "private", "protected", "public", "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string", "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe", "ushort", "using", "virtual", "void", "volatile", "while", "add", "alias", "ascending", "descending", "dynamic", "from", "get", "global", "group", "into", "join", "let", "orderby", "partial", "remove", "select", "set", "value", "var", "where", "yield" };
        string[] methods = { "Equals()", "GetHashCode()", "GetType()", "ToString()" };
        string[] snippets = { "if(^)\n{\n;\n}", "if(^)\n{\n;\n}\nelse\n{\n;\n}", "for(^;;)\n{\n;\n}", "while(^)\n{\n;\n}", "do\n{\n^;\n}while();", "switch(^)\n{\ncase : break;\n}" };
        string[] declarationSnippets = {
               "public class ^\n{\n}", "private class ^\n{\n}", "internal class ^\n{\n}",
               "public struct ^\n{\n;\n}", "private struct ^\n{\n;\n}", "internal struct ^\n{\n;\n}",
               "public void ^()\n{\n;\n}", "private void ^()\n{\n;\n}", "internal void ^()\n{\n;\n}", "protected void ^()\n{\n;\n}",
               "public ^{ get; set; }", "private ^{ get; set; }", "internal ^{ get; set; }", "protected ^{ get; set; }"
               };
        //Style invisibleCharsStyle = new InvisibleCharsRenderer(Pens.Gray);
        Color currentLineColor = Color.FromArgb(100, 210, 210, 255);
        Color changedLineColor = Color.FromArgb(255, 230, 230, 255);

        public IDocument AddNew()
        {
            return OpenDocument(null);
        }

        public IDocument Open()
        {
            var ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK)
                return Open(ofd.FileName);

            return null;
        }

        public IDocument Open(string fileName)
        {
            return OpenDocument(fileName);
        }

        public IDocument Open(Stream stream, string fileName)
        {
            throw new NotImplementedException();
        }

        public void Save(IDocument document)
        {
            var doc = document as SourceCodeDocument;
            if (doc == null)
                return;

            if (string.IsNullOrEmpty(doc.FileName))
                SaveAs(document);
            else
                doc.Save();
        }

        public void SaveAs(IDocument document)
        {
            var doc = document as SourceCodeDocument;
            if (doc == null)
                return;

            var sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
                doc.Save(sfd.FileName);
        }
        public string DefaultExtension
        {
            get { return ".cs"; }
        }

        private IDocument OpenDocument(string fileName)
        {
            var doc = new SourceCodeDocument { FileName = fileName };

            var tb = new FastColoredTextBox();
            tb.Font = new Font("Consolas", 9.75f);
            //tb.ContextMenuStrip = cmMain;
            tb.Dock = DockStyle.Fill;
            tb.BorderStyle = BorderStyle.Fixed3D;
            //tb.VirtualSpace = true;
            tb.LeftPadding = 17;
            tb.Language = Language.CSharp;
            tb.AddStyle(sameWordsStyle);//same words style
            if (fileName != null)
                tb.OpenFile(fileName);
            tb.Tag = new TbInfo();
            tb.Focus();
            tb.DelayedTextChangedInterval = 1000;
            tb.DelayedEventsInterval = 500;

            tb.TextChangedDelayed += (s, e) =>
            {
                doc.Text = tb.Text;
            };
            doc.TextChanged += (s, e) =>
            {
                tb.Text = doc.Text;
            };

            //tb.SelectionChangedDelayed += new EventHandler(tb_SelectionChangedDelayed);
            //tb.KeyDown += new KeyEventHandler(tb_KeyDown);
            //tb.MouseMove += new MouseEventHandler(tb_MouseMove);
            tb.ChangedLineColor = changedLineColor;
            tb.ShowFoldingLines = true;
            tb.HighlightingRangeType = HighlightingRangeType.VisibleRange;
            //create autocomplete popup menu
            //AutocompleteMenu popupMenu = new AutocompleteMenu(tb);
            //popupMenu.Items.ImageList = ilAutocomplete;
            //popupMenu.Opening += new EventHandler<CancelEventArgs>(popupMenu_Opening);
            //BuildAutocompleteMenu(popupMenu);
            //(tb.Tag as TbInfo).popupMenu = popupMenu;

            ServiceContainer.Resolve<IDocumentService>().AddDocument(tb, doc, 200);

            return doc;
        }
    }

    public class TbInfo
    {
        public AutocompleteMenu popupMenu;
    }

}
