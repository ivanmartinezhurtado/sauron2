﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.42)]
    public abstract class RGRNTestBase : TestBase
    {
        #region Entradas y salidas
        // Salidas de la manguera
        internal const byte OUT_ABRE_TOROIDAL = 18;
        internal const byte OUT_CORTO_TOROIDAL = 17;

        internal const byte OUT_24V = 14;
        internal const byte OUT_230V = 15;
        internal const byte OUT_PILOTO_ERROR = 48;
        internal const byte OUT_ILUMINACION = 51;
        internal const byte OUT_DESCONECTA_DATAMAN = 52;

        // Salidas EV
        internal const byte OUT_EV_GENERAL = 4;
        internal const byte OUT_EV_ALIMENTACION = 5;
        internal const byte OUT_EV_TTL = 6;
        internal const byte OUT_EV_ACTUADOR = 7;
        internal const byte OUT_EV_FIJACION_CARRO = 8;
        internal const byte OUT_EV_TECLA_RESET = 10;
        internal const byte OUT_EV_TECLA_TEST = 11;

        // Entradas de la manguera
        internal const byte IN_RELAY_NO = 9;
        internal const byte IN_RELAY_NC = 10;

        //Entradas presencia equipo 
        internal const byte IN_DETECT_UUT = 24;
        internal const byte IN_SEGURIDAD = 23;
        internal const byte IN_DETECT_TAPA = 25;
        internal const byte IN_DETECT_ANCLA = 26;
        internal const byte IN_DETECT_FIJACION_1 = 28;
        internal const byte IN_DETECT_FIJACION_2 = 29;
        internal const byte IN_DETECT_BORNERA_1_4 = 30;
        internal const byte IN_DETECT_BORNERA_5_8 = 31;
        internal const byte IN_DETECT_TTL = 32;
        internal const byte IN_POSICION_ACTUADOR_TIEMPO = 33;        
        internal const byte IN_POSICION_ACTUADOR_CORRIENTE = 34;

        #endregion

        public Tower3 Tower;
        public object Rgrn;
        public string CAMERA_IDS;

        public virtual void TestInitialization()
        { 
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out Tower))
            {
                Tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", Tower);
            }

            Tower.MUX.Reset();
            Tower.Active24VDC();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);
            Tower.IO.DO.On(OUT_24V);
            Tower.IO.DO.On(OUT_230V);
            Tower.IO.DO.Off(OUT_PILOTO_ERROR);

            this.InParellel(() =>
            {
                Tower.PTE.ApplyOff();
            },
            () =>
            {
                Tower.LAMBDA.ApplyOff();
            });

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[IN_SEGURIDAD])           
                    return false;
                return true;
            }, "Error: No se ha detectado el bloque de seguridad", 50, 500, 0, false, false, (s) =>
            {
                Error().HARDWARE.UTILLAJE.BLOQUE_SEGURIDAD().Throw();
            });

            SamplerWithCancel((p) => { return (Tower.IO.DI[IN_DETECT_UUT]); }, "Error: No se detecta el equipo",
                2, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES(s).Throw(); });

            Tower.IO.DO.On(OUT_EV_GENERAL);

            Tower.IO.DO.On(OUT_EV_FIJACION_CARRO);
            SamplerWithCancel((p) => { return (Tower.IO.DI[IN_DETECT_FIJACION_1] && Tower.IO.DI[IN_DETECT_FIJACION_2]) ; }, "Error: No se han detectado los pistones de fijacion",
                20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });   
        }

        public void TestInitialization2()
        {
            Tower.IO.DO.On(OUT_EV_ALIMENTACION);

            SamplerWithCancel((p) => { return (Tower.IO.DI[IN_DETECT_BORNERA_1_4]); }, "Error: No se ha detectado el piston de la bornera superior",
                10, 300, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            SamplerWithCancel((p) => { return (Tower.IO.DI[IN_DETECT_BORNERA_5_8]); }, "Error: No se han detectado el piston de la bornera inferior",
                10, 300, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

            SamplerWithCancel((p) => { return (Tower.IO.DI[IN_DETECT_ANCLA]); }, "Error: No se ha detectado el ancla",
                2, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES(s).Throw(); });

            Tower.IO.DO.On(OUT_EV_TTL);
            SamplerWithCancel((p) => { return Tower.IO.DI[IN_DETECT_TTL]; }, "Error: No se ha detectado el piston de la bornera de comunicaciones",
                20, 100, 0, true, true, (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });
        }

        public abstract void TestInitLAC25Macros(bool rewrite = false);

        public void TestConsumption([Description("Tiempo espera estabilización consumo")]int timeWait = 1000, int timeInterval = 100, int iterations = 5 )
        {
            var typeSupply = VectorHardware.GetString("TIPO_ALIMENTACION", "AC", ParamUnidad.SinUnidad);

            if (typeSupply == "AC")
            {
                Tower.PTE.InitConfig();
                Tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;
                Tower.PTE.Presets.Voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 0, ParamUnidad.V);
                Tower.PTE.ApplyAndWaitStabilisation();

                Delay(timeWait, "Espera a la estabilización del consumo del equipo");

                TestConsumo(Params.W.Null.EnVacio.Vnom.Name, Params.W.Null.EnVacio.Vnom.Min(), Params.W.Null.EnVacio.Vnom.Max(), () => (double)(Tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva) / 10.0, "Error Consumo Pot. Activa en vacio a Vnom fuera de margenes", timeInterval, iterations);

                TestConsumo(Params.VA.Null.EnVacio.Vnom.Name, Params.VA.Null.EnVacio.Vnom.Min(), Params.VA.Null.EnVacio.Vnom.Max(), () => Tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaAparente / 10, "Error Consumo Pot. Reactiva en vacio a Vnom fuera de margenes", timeInterval, iterations);
            }
            else if (typeSupply == "DC")
            {
                var configuration = new TestPointConfiguration()
                {
                    Current = 0,
                    Frecuency = 50,
                    Iteraciones = iterations,
                    ResultName = "",
                    TiempoIntervalo = timeInterval,
                    typePowerSource = TypePowerSource.LAMBDA,
                    typeTestExecute = TypeTestExecute.ActiveAndReactive,
                    typeTestPoint = TypeTestPoint.EnVacio,
                    typeTestVoltage = TypeTestVoltage.VoltageNominal,
                    Voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V),
                    WaitTimeReadMeasure = timeWait
                };

                Tower.IO.DO.On(23);

                this.TestConsumo(Tower, configuration);
            }
            else
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error: El parametro 'TIPO_ALIMENTACION' no esta bien configurado: Solo permite 'AC' y 'DC' como valores").Throw();
        }

        public abstract void TestCommunications();

        public abstract void TestToroidal();

        public abstract void TestAdjust();

        protected void TestVerification(Func<double> readCurrent)
        {
            var verificationCurrent = Consignas.GetDouble(Params.I.Other("TRIGGER").Verificacion.Name, 0, ParamUnidad.A);

            if (Tower.IO.DI[IN_RELAY_NO] || !Tower.IO.DI[IN_RELAY_NC])
                Error().UUT.DISPARO.YA_DISPARADO();

            Tower.PTE.ResetTimerTripMonitor();

            Tower.PTE.ApplyConfigMonitor(3, PTE.Monitor.EventType.TRIP, PTE.Monitor.EventTrigger.ActivarseDesactivarse);

            Tower.PTE.DesActivaTimer(108);

            Tower.PTE.ApplyCurrentAndStartTimer(verificationCurrent);

            Delay(300, "Esperando el disparo del equipo");

            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[IN_RELAY_NO] && !Tower.IO.DI[IN_RELAY_NC];
            }, "Error No se ha producido el disparo del equipo", 3, 150, 0, true, true,
            (s) =>
            {
                Error().UUT.DISPARO.NO_DISPARA(s).Throw();
            });

            AdjustValueDef adjustValueCurrent = new AdjustValueDef(Params.I.Null.TRIGGER.Name, 0, Params.I.Null.TRIGGER.Min(), Params.I.Null.TRIGGER.Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(adjustValueCurrent,
            (step) =>
            {
                var currentRead = readCurrent();
                return currentRead;
            }, 0, 1, 0);

            var timeBBDD = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

            var timeResult = TestMeasureBase(timeBBDD, (s) =>
            {
                var time = Tower.PTE.ReadTime();
                return time;
            }, 0, 1, 1000);

            Tower.PTE.ApplyOff();
        }

        public void TestSelectorsPositions(byte retries = 1)
        {
            var caratula = Identificacion.CARATULA;
            if (caratula != "NO")
            {
                Tower.IO.DO.On(OUT_EV_ACTUADOR, OUT_ILUMINACION);
                Delay(1000, "Encendido iluminacion");
                try
                {
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "RG_RN_SELECTOR_" + caratula, "SELECTOR_" + caratula, "RG_RN", retries);
                }
                finally
                {
                    Tower.IO.DO.Off(OUT_EV_ACTUADOR, OUT_ILUMINACION);
                }
            }
            else
                Logger.WarnFormat("No se ha parametrizado Identificacion.CARATULA en BBDD");
        }

        public abstract void TestCostumization();

        public virtual void TestFinish()
        {
            bool Macros;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc.TryGet("LAC25_MACROS", out Macros))
                try
                {
                    Tower.Actuator_LAC25.Home(false, true);
                }
                catch { }

            this.InParellel(() =>
            {
                Tower.PTE.Reset();
                Tower.PTE.ApplyOffAllAndWaitStabilisation();
            },
            () =>
            {
                Tower.LAMBDA.ApplyOff();
            });

            Tower.IO.DO.Off(OUT_EV_ALIMENTACION, OUT_EV_TTL, OUT_EV_ACTUADOR);
            Delay(1600, "");

            Tower.IO.DO.Off(OUT_EV_FIJACION_CARRO);
            Delay(800, "");

            Tower.IO.DO.Off(OUT_EV_GENERAL);

            Tower.IO.DO.Off(OUT_24V);

            if (Tower != null)
            {
                Tower.ShutdownSources();
                Tower.Dispose();
            }

            if (IsTestError)
                Tower.IO.DO.On(OUT_PILOTO_ERROR);
        }
  
        #region Auxiliar functions

        protected void PowerSourceResetAndApplyPrsetes(Func<bool> ValidationFunc)
        {
            var typeSupply = VectorHardware.GetString("TIPO_ALIMENTACION", "AC", ParamUnidad.SinUnidad);
            var voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.EnVacio.Vnom.Name, 0, ParamUnidad.A);

            if (typeSupply == "AC")
            {
                Tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;
                Tower.PTE.ApplyOff();

                Delay(2000, "Esperando apagado del equipo");
                Tower.PTE.ApplyPresets(voltage, 0, 50);
                Delay(2000, "Esperando encendido del equipo");
            }
            else
            {
                Tower.LAMBDA.ApplyOff();
                Delay(2000, "Esperando apagado del equipo");
                Tower.IO.DO.On(23);
                Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltage, current);
                Delay(2000, "Esperando encendido del equipo");
            }

            ValidationFunc();
        }

        protected string GetEEPROMErrorCodesTranslation(RG_RN_LC.ErrorCode errorCodes)
        {
            var errorList = new List<string>();

            foreach (RG_RN_LC.ErrorCode item in Enum.GetValues(typeof(RG_RN_LC.ErrorCode)))
                if ((errorCodes.HasFlag(item)) && (item != RG_RN_LC.ErrorCode.NO_ERROR))
                    errorList.Add(item.GetDescription());
            return string.Join(", ", errorList.ToArray());
        }

        protected void InitAdjust(bool stepsByTorque)
        {
            var tolerance = Configuracion.GetDouble("PTE_TOLERANCIA", 0.5, ParamUnidad.PorCentage);

            var typeSupply = VectorHardware.GetString("TIPO_ALIMENTACION", "AC", ParamUnidad.SinUnidad);

            var current = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0, ParamUnidad.A);

            Tower.IO.DO.On(OUT_ILUMINACION);

            this.InParellel(() =>
            {
                Tower.IO.DO.On(OUT_EV_ACTUADOR);
                SamplerWithCancel((p) => { return (Tower.IO.DI[IN_POSICION_ACTUADOR_CORRIENTE] && !Tower.IO.DI[IN_POSICION_ACTUADOR_TIEMPO]); }, 
                    "Error: No se ha detectado el sensor del actuador en posicion del selector de corriente", 20, 100, 0, true, true, 
                    (s) => { Error().HARDWARE.UTILLAJE.PNEUMATICA(s).Throw(); });

                Tower.Actuator_LAC25.WriteAndRunFindSelector();

                byte pos = (byte)Configuracion.GetDouble("POSICION_SELECTOR_AJUSTE", 5, ParamUnidad.Numero);

                if (stepsByTorque)
                    while (pos > 0)
                    {
                        pos--;
                        Tower.Actuator_LAC25.GoNextTorqueStep();
                        Delay(300, "");
                    }
                else
                    Tower.Actuator_LAC25.RunGoPosition(pos);

            }, () =>
            {
                Tower.PTE.ApplyPresetsAndWaitStabilisation(Tower.PTE.Presets.Voltage, current, 50);
            });
        }

        #endregion
    }
}