﻿using Instruments.Utility;

namespace Instruments.SCPI
{
    public class SCPIDevice
    {
        protected SCPIContext context;

        public SCPIDevice(IConsoleDevice device)
        {
            this.context = new SCPIContext(device);
        }
        public SCPICommand Reset { get { return new SCPICommand(context, "*RST"); } }
        public SCPICommand Clear { get { return new SCPICommand(context, "*CLS"); }} //Clear registers and errors 
        public SCPICommand ClearSRE { get { return new SCPICommand(context, "*SRE 0"); } } // Clear service request enable register
        public SCPICommand ClearESE { get { return new SCPICommand(context, "*ESE 0"); } } // Clear event status enable register
        public SCPICommand OperationComplete { get { return new SCPIQueryCommand<int>(context, "*OPC"); } }
        public SCPIQueryCommand<string> Identification { get { return new SCPIQueryCommand<string>(context, "*IDN"); } }
        public SCPIQueryCommand<string> StandardEventStatus { get { return new SCPIQueryCommand<string>(context, "*ESE"); } }
        public SCPIQueryCommand<string> OperationCompleteQuery { get { return new SCPIQueryCommand<string>(context, "*OPC"); } }
        public SCPIQueryCommand<string> StatusByte { get { return new SCPIQueryCommand<string>(context, "*STB"); } }
    }
}
