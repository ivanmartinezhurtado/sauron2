﻿using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Extensions
{
    public static class TestControlTrazabilidad
    {
        public static void ControlTrazabilidad(this TestBase testBase, bool sorted)
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();
            if (data == null)
                throw new Exception("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            var bastidor = 12345;

            using (DezacService svc = new DezacService())
            {
                if (sorted)
                    SubsetTrazabilityController(testBase, data, svc, bastidor);
                else
                    SubsetTrazabilityControllerUnsorted(testBase, data, svc, bastidor);
            }
        }

        private static List<ComponentsEstructureInProduct> SubsetTrazabilityController(TestBase testBase, ITestContext data, DezacService svc, int bastidor, bool AddSubconjuntosSinRegistrar = false)
        {
            var components = svc.GetComponentsEstructureInProductByTrazabilidad(data.NumProducto, data.Version);

            if (components.Count > 1)
            {
                var numEstructura = svc.GetNumEstructura(bastidor, data.IdOperario.ToString());

                foreach (ComponentsEstructureInProduct component in components)
                {
                    var subconjuntoMatricula = testBase.GetIntroBastiorByUser(component.descripcion);

                    var Matricula = svc.GetMatricula(subconjuntoMatricula);
                    if (Matricula == null)
                        throw new Exception(string.Format("Bastidor del equipo {0} no registrado!", component.descripcion));

                    if (!AddSubconjuntosSinRegistrar)
                    {
                        var op = svc.GetOrdenProductoByBastidor(subconjuntoMatricula);
                        if (op == null)
                            throw new Exception(string.Format("El bastidor {0} no esta registrado en ninguna orden!", component.descripcion));

                        var ordenSubconjunto = svc.GetOrden(op.NUMORDEN);
                        if (ordenSubconjunto == null)
                            throw new Exception(string.Format("El bastidor {0} no esta registrado en ninguna orden", component.descripcion));

                        if (data.modoPlayer == ModoTest.Normal)
                        {
                            if (component.numproducto != ordenSubconjunto.NUMPRODUCTO || component.version != ordenSubconjunto.VERSION)
                                throw new Exception(string.Format("Equipo {0} esta registrado como otro producto {1} o con otra versión {2}!", component.descripcion, op.ORDEN.NUMPRODUCTO, op.ORDEN.VERSION));
                        }
                        else
                        {
                            if (component.numproducto != ordenSubconjunto.NUMPRODUCTO)
                                throw new Exception(string.Format("Equipo {0} esta registrado como otro producto {1} !", component.descripcion, op.ORDEN.NUMPRODUCTO));
                        }

                        svc.GetResultadoNumTestIsOK(subconjuntoMatricula);
                    }

                    svc.GrabarEstructuraMatricula(numEstructura, subconjuntoMatricula);

                    component.matricula = subconjuntoMatricula;
                }
            }
            return components;
        }

        private static List<ComponentsEstructureInProduct> SubsetTrazabilityControllerUnsorted(TestBase testBase, ITestContext data, DezacService svc, int bastidor)
        {
            var estruct = svc.GetComponentsEstructureInProductByTrazabilidad(data.NumProducto, data.Version);

            var components = new List<ComponentsEstructureInProduct>();
            components.AddRange(estruct);

            foreach (ComponentsEstructureInProduct comp in estruct)
                if (comp.cantidad > 1)
                {
                    for (byte i = 1; i <= comp.cantidad - 1; i++)
                    {
                        var comp2 = new ComponentsEstructureInProduct();
                        comp2.matricula = 0;
                        comp2.cantidad = 1;
                        comp2.descripcion = string.Format("{0}_SUB{1}", comp.descripcion, i + 1);
                        comp2.numproducto = comp.numproducto;
                        comp2.version = comp.version;
                        components.Add(comp2);
                    }
                    comp.cantidad = 1;
                    comp.descripcion = string.Format("{0}_SUB{1}", comp.descripcion, 1);
                }

            if (components.Count > 1)
            {
                var numEstructura = svc.GetNumEstructura(bastidor, data.IdOperario.ToString());

                int numIntro = 0;

                do
                {
                    var subconjuntoMatricula = testBase.GetIntroBastiorByUser(string.Format("SUBCONJUNTO {0} / {1}", numIntro + 1, components.Count + 1));

                    var Matricula = svc.GetMatricula(subconjuntoMatricula);
                    if (Matricula == null)
                        throw new Exception("Bastidor del subconjunto no registrado!");

                    var op = svc.GetOrdenProductoByBastidor(subconjuntoMatricula);
                    if (op == null)
                        throw new Exception("El bastidor del subconjunto no esta registrado en ninguna orden!");

                    var ordenSubconjunto = svc.GetOrden(op.NUMORDEN);
                    if (ordenSubconjunto == null)
                        throw new Exception("El bastidor del subconjunto no esta registrado en ninguna orden");

                    if (components.Where((p) => p.matricula == subconjuntoMatricula).Count() == 0)
                    {
                        bool trobat = false;

                        foreach (ComponentsEstructureInProduct component in components)
                            if (component.numproducto == ordenSubconjunto.NUMPRODUCTO && component.matricula == 0)
                            {
                                svc.GetResultadoNumTestIsOK(subconjuntoMatricula);

                                component.matricula = subconjuntoMatricula;

                                svc.GrabarEstructuraMatricula(numEstructura, subconjuntoMatricula);

                                trobat = true;
                                break;
                            }

                        if (!trobat)
                            throw new Exception("Bastidor del subconjunto esta registrado como otro producto y no existe en la estructura de este equipo");
                    }

                    numIntro = components.Where((p) => { return p.matricula != 0; }).Count();

                } while (components.Where((p) => { return p.matricula == 0; }).Count() != 0);
            }
            return components;
        }      
    }
}
