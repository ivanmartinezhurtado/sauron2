﻿using System;

namespace Dezac.Tests.Model
{
    public class ParamValue
    {
        public int IdGrupo { get; set; }
        public TipoGrupo IdTipoGrupo { get; set; }
        public string Grupo { get; set; }
        public int IdParam { get; set; }
        public string Valor { get; set; }
        public string ValorInicio { get; set; }
        public int IdTipoValor { get; set; }
        public string TipoValor { get; set; }
        public ParamUnidad IdUnidad { get; set; }
        public string Unidad { get; set; }
        public string Name { get; set; }        // Parametro
        public string IdCategoria { get; set; }

        public double? Min { get; set; }
        public double? Max { get; set; }
        public string ValorEsperado { get; set; }
        
        public DateTime? Modified { get; set; }
        public string StepName { get; set; }
        public int NumInstance { get; set; }

        public override string ToString()
        {
            return string.Format("{0} = {1} ({2})", Name, Valor, IdUnidad);
        }

        public bool IsValid
        {
            get
            {
                double valor;

                if (!Min.HasValue || !Max.HasValue || !double.TryParse(Valor, out valor))
                    return true;

                return Min <= valor && valor <= Max;
            }
        }

        public int GetTipoValorFromUnidad()
        {
            switch(IdUnidad)
            {
                case ParamUnidad.V:
                case ParamUnidad.mV:
                case ParamUnidad.bps:
                case ParamUnidad.W:
                case ParamUnidad.Kw:
                case ParamUnidad.Var:
                case ParamUnidad.mA:
                case ParamUnidad.A:
                case ParamUnidad.ppm:
                case ParamUnidad.Puntos:
                case ParamUnidad.Hz:
                case ParamUnidad.ms:
                case ParamUnidad.s:
                case ParamUnidad.Ohms:
                case ParamUnidad.Ganancia:
                case ParamUnidad.Grados:
                case ParamUnidad.PorCentage:
                case ParamUnidad.Numero:
                case ParamUnidad.Kvar:
                case ParamUnidad.MByte:
                case ParamUnidad.dB:
                case ParamUnidad.uA:
                case ParamUnidad.us:
                case ParamUnidad.dVAR:
                case ParamUnidad.dW:
                case ParamUnidad.F:
                case ParamUnidad.uF:
                case ParamUnidad.nF:
                case ParamUnidad.KOhms:
                case ParamUnidad.VA:
                case ParamUnidad.LUX:
                case ParamUnidad.dBm:
                case ParamUnidad.kVA:
                case ParamUnidad.Whimp:
                case ParamUnidad.m:
                case ParamUnidad.cm:
                case ParamUnidad.Pulse:
                    return 2; //Numeric
                case ParamUnidad.SinUnidad:
                    return 1; //String
                case ParamUnidad.Fecha:
                    return 3;
            }

            return -1;
        }
    }

    public enum ParamUnidad
    {
        SinDefinir = 0, 
        SinUnidad=1,
        V = 2,
        mV = 3,
        bps=4,
        W=5,
        Kw=6,
        Var=7,
        mA=8,
        A=9,
        ppm=10,
        Puntos=12,
        Hz=13,
        ms=14,
        s=15,
        Ohms=16,
        Ganancia=17,
        PorCentage=18,
        Grados=19,
        Fecha=20,
        Numero=23,
        Kvar=24,
        MByte=25,
        dB=26,
        us=27,
        uA=28,
        dW=29,
        dVAR=30,
        F=31,
        uF=32,
        nF=33,
        KOhms = 34,
        VA=35,
        LUX = 36,
        dBm = 37,
        kVA = 38,
        Whimp = 39,
        m = 40,
        cm=41,
        Pulse = 42
    }

    public enum TipoGrupo
    {
        CONFIGURACION = 1,
        MARGENES = 2,
        IDENTIFICACION = 5,
        COMUNICACIONES = 6,
        CONSIGNAS = 7,
        RESULTADOS = 8,
    }
}
