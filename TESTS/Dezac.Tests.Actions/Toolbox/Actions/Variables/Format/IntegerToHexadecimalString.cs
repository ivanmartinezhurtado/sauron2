﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(IntegerToHexadecimalStringDescription))]
    public class IntegerToHexadecimalString : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número Hexadecimal a convertir")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if (DataOutput.IsKeyNullOrEmpty)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DataOutput.Key).Throw();

            long intValue = Convert.ToInt64(DataInput.Value);

            var hexValue = intValue.ToString("X");

            DataOutput.Value = hexValue;
        }
    }

    public class IntegerToHexadecimalStringDescription : ActionDescription
    { 
        public override string Name { get { return "IntegerToHexadecimalString"; }}
        public override string Category { get { return "Actions/Variables/Format"; } }
        public override string Description { get { return "Este action sirve para pasar un número o una cadena de Decimal a Hexadecimal"; } }
        public override Image Icon { get { return Properties.Resources.format; } }
    }
}
