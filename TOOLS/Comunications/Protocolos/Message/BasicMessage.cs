﻿using System;

namespace Comunications.Message
{
    public class BasicMessage : ITransportMessage
    {
        protected bool onlyRead = false;
        protected bool hasResponse = true;
        protected byte[] frame;
        protected int searchLastEndLine = 1;
        protected int? answerLen = null;

        public int TransactionId { get; set; }

        public byte[] MessageFrame
        {
            get { return frame; }
        }

        public virtual void Initialize(byte[] frame)
        {
            this.frame = new byte[frame.Length];

            frame.CopyTo(this.frame, 0);
        }

        public bool HasResponse 
        { 
            get { return hasResponse; } 
            set { hasResponse = value; }
        }

        public bool OnlyRead
        {
            get { return onlyRead; }
            set { onlyRead = value; }
        }

        public int SearchLastEndLine
        {
            get { return searchLastEndLine; }
            set { searchLastEndLine = value; }
        }

        public int? AnswerLen
        {
            get { return answerLen; }
            set { answerLen = value; }
        }
    }
}
