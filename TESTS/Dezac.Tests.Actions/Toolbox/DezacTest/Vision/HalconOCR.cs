﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions;
using Dezac.Tests.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(HalconOCRDescription))]
    public class HalconOCR : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del test de visión artificial")]
        [DefaultValue("OCR")]
        public string TestName { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre para guardar la imagen")]
        [DefaultValue("")]
        public string DataInput { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo de test")]
        [DefaultValue("_OCR")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Número de capturas (reintentos)")]
        [DefaultValue((byte)1)]
        public byte Retry { get; set; }

        [Category("3.1 Camera Selection")]
        [Description("Nombre de la cámara si se ha definido como una variable con el action AddVariable")]
        [DefaultValue("")]
        public string DataInputCamera { get; set; }

        [Category("3.1 Camera Selection")]
        [Description("Número de serie de la cámara (no necesario si tiene DataInputCamera)")]
        [DefaultValue("")]
        public string SerialNumber { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Ancho del carácter en píxeles")]
        [DefaultValue(0)]
        public int CharWidth { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Altura del carácter en píxeles")]
        [DefaultValue(0)]
        public int CharHeight { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Trazo del carácter en píxeles")]
        [DefaultValue(0)]
        public int CharStroke { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Fragmento mínimo para tener en cuenta en píxeles")]
        [DefaultValue(0)]
        public int MinFragmentSize { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Polaridad, Caracteres blancos con fondo negro o caracteres negros en fondo blanco")]
        [DefaultValue(TestHalconExtension.PolaritySelectionEnum.Dark_On_Light)]
        public TestHalconExtension.PolaritySelectionEnum Polarity { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Número de líneas a leer")]
        [DefaultValue(0)]
        public int NumberOfLines { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Orientación del texto en grados")]
        [DefaultValue(0)]
        public double OrientationOffset { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Tolerancia de la orientación del texto")]
        [DefaultValue(0)]
        public double OrientationTolerance { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Los caracteres reconocidos deben coincidir con la RegularExpresion.")]
        [DefaultValue("")]
        public string RegularExpresion { get; set; }

        [Category("3.2 OCR Parametrization")]
        [Description("Clasificador que determina el carácter")]
        [DefaultValue(TestHalconExtension.OCRClassificatorEnum.Universal_AllCharacters_WithRejection)]
        public TestHalconExtension.OCRClassificatorEnum Classificator { get; set; }

        [Category("3.3 Crop and Resize image")]
        [Description("Fila 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Row1 { get; set; }

        [Category("3.3 Crop and Resize image")]
        [Description("Columna 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Column1 { get; set; }

        [Category("3.3 Crop and Resize image")]
        [Description("Fila 2 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Row2 { get; set; }

        [Category("3.3 Crop and Resize image")]
        [Description("Columna 2 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Column2 { get; set; }

        [Category("3.3 Crop and Resize image")]
        [Description("Factor de redimensión para una imagen con menos resolución (1 significa sin reducción de resolución)")]
        [DefaultValue(1)]
        public double ResizeFactor { get; set; }

        [Category("4. Results")]
        [Description("Variable de salida con la lista de caracteres leídos")]
        [DefaultValue("")]
        public string CharactersRead { get; set; }

        [Category("4. Results")]
        [Description("Variable de salida con la lista de valores de confianza")]
        [DefaultValue("")]
        public string Confidences { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            if (!string.IsNullOrEmpty(DataInputCamera))
                SerialNumber = GetVariable<string>(DataInputCamera);

            if (RegularExpresion == null)
                RegularExpresion = "";

            string charactersRead = testBase.TestHalconOCRProcedure(SerialNumber, TestName, DataInput, DeviceName, Retry,
                Row1, Column1, Row2, Column2, ResizeFactor, CharWidth, CharHeight, CharStroke, MinFragmentSize, Polarity, NumberOfLines,
                OrientationOffset, OrientationTolerance, RegularExpresion, Classificator);

            Context.Variables.AddOrUpdate(CharactersRead, charactersRead);
        }

    }

    public class HalconOCRDescription : ActionDescription
    {
        public override string Name { get { return "HalconOCR"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override string Description { get { return "Action que sirve para leer en OCR (Reconocimiento óptico de caracteres)"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
