﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AssemblyResolve
{
    public static class Tools
    {
        public static ProjectItem LoadProject(string applicationName, string appliactionPath)
        {
            string fileName = Path.Combine(appliactionPath, "app.json");
            var json = File.ReadAllText(fileName);
            var Projects = JsonConvert.DeserializeObject<List<ProjectItem>>(json);

            var Project = Projects.Where((p) => p.Name == applicationName).FirstOrDefault();
            if (Project == null)      
                return null;
           
            return Project;
        }

        public static void Save(string fileName, List<FileAssemblyItem> listFileAssembly)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            settings.Formatting = Formatting.Indented;

            var json = JsonConvert.SerializeObject(listFileAssembly, settings);

            if (fileName.IndexOf(".") < 0)
                fileName += ".json";

            File.WriteAllText(fileName, json);
        }

        public static List<FileAssemblyItem> LoadFile(string fileName)
        {      
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                settings.Formatting = Formatting.Indented;
                string json = File.ReadAllText(fileName);
                return JsonConvert.DeserializeObject<List<FileAssemblyItem>>(json, settings);
        }

        public static  FileAssemblyItem AddAssemblyTarget(string PathAssembly, FileAssemblyItem assemblie, bool Update = true, string currentVersion = "", List<ClassVersion> classesVersion = null, bool IncrementVersion = true)
        {
            var assemblyPath = Path.Combine(PathAssembly, assemblie.NameAssembly);

            var fileAssembly = new FileAssemblyItem();
            var asm = AssemblyName.GetAssemblyName(assemblyPath);
            fileAssembly.NewVersion = asm.Version.ToString();
            fileAssembly.CurrentVersion = currentVersion;
            fileAssembly.Name = asm.Name;
            fileAssembly.NameAssembly = assemblie.NameAssembly;
            fileAssembly.PathProjectAssembly = assemblie.PathProjectAssembly;
            fileAssembly.Update = Update;
            fileAssembly.IncrementVersion = IncrementVersion;
            fileAssembly.ControlClassversion = assemblie.ControlClassversion;
            if (classesVersion != null)
                fileAssembly.Clases = classesVersion;
            return fileAssembly;
        }
    }
}
