using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Comunications.Message
{
    public class CirbusMessage : ITransportMessage, IValidateMessage
	{
        internal byte startSymbolCode = Encoding.ASCII.GetBytes("$").First();

		public CirbusMessage()
		{
		}

        public CirbusMessage(byte[] slaveAddress, string message)
		{
            SlaveAddress = slaveAddress;
            Data = ASCIIEncoding.ASCII.GetBytes(message);
		}

        public Func<CirbusMessage, bool> Validator { get; set; }

        public byte StartSymbolCode { get; set; }

        public byte[] SlaveAddress { get; set; }

        public byte[] Data { get; set; }

        public ushort CheckSumCalculate { get; set; }

        public ushort CheckSumRead { get; set; }

        public byte[] MessageFrame
        {
            get
            {
                List<byte> frame = new List<byte>();
                frame.Add(startSymbolCode);
                frame.AddRange(SlaveAddress);
                frame.AddRange(Data);
                CheckSumCalculate = ComunicationUtility.CheckSum(frame.ToArray());
                frame.AddRange(ASCIIEncoding.ASCII.GetBytes(CheckSumCalculate.ToString("X2")));
                return frame.ToArray();
            }
        }

		public void Initialize(byte[] frame)
		{
            if (frame == null)
                throw new ArgumentNullException("frame", "Argument frame cannot be null.");

            StartSymbolCode = frame[0];
            var address = new List<byte>();
            address.Add(frame[1]);
            address.Add(frame[2]);
            SlaveAddress = address.ToArray();
            CheckSumCalculate = ComunicationUtility.CheckSum(frame.Take(frame.Length-2).ToArray());
            var checkSumChar = string.Format("{0}{1}", Convert.ToChar(frame[frame.Length - 2]), Convert.ToChar(frame[frame.Length - 1]));
            CheckSumRead = byte.Parse(checkSumChar, NumberStyles.HexNumber);
            Data = frame.Skip(3).ToArray();
            Data = Data.Take(Data.Length - 2).ToArray();
		}
     
        public void ValidateResponse(ITransportMessage response)
        {
            var typedResponse = response as CirbusMessage;

            if (startSymbolCode != typedResponse.StartSymbolCode)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Received response with unexpected Start SymbolCode  $  Expected {0}, received {1}.", StartSymbolCode, typedResponse.StartSymbolCode));

            if ((SlaveAddress[0] != typedResponse.SlaveAddress[0]) && (SlaveAddress[1] != typedResponse.SlaveAddress[1]))
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Response slave address does not match request. Expected {0}, received {1}.", SlaveAddress, typedResponse.SlaveAddress));

            if (typedResponse.CheckSumRead != typedResponse.CheckSumCalculate)
                throw new FormatException(String.Format(CultureInfo.InvariantCulture, "Response Checksum  does not match request Expected {0}, received {1}.", SlaveAddress, typedResponse.SlaveAddress));

            if (Validator != null)
                if (!Validator((CirbusMessage)response))
                    throw new FormatException(string.Format("Error en la validación de la respuesta Cirbus no controlada"));
        }

        private bool hasResponse = true;
        public bool HasResponse
        {
            get { return hasResponse; }
            set { hasResponse = value; }
        }

        public int TransactionId
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int SearchLastEndLine
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int? AnswerLen
        {
            get
            {
                return null;
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "Address {0},  Data: {1}", SlaveAddress, Data != null ? Extensions.BytesToHexString(Data) : null);
        }

    }
}
