﻿using System;

namespace Dezac.Tests.Actions
{
    public class DesignerActionAttribute : Attribute
    {
        public Type Type { get; set; }

    }
}