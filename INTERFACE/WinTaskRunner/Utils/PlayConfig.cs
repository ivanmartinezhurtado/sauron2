﻿using System.Collections.Generic;
using System.Linq;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using WinTaskRunner.Services;

namespace WinTaskRunner.Utils
{
    public class PlayConfig
    {
        private Dictionary<int, List<SequenceContext>> groups;

        public PlayConfig()
        {
            groups = new Dictionary<int, List<SequenceContext>>();
        }

        public SequenceDocument Sequence { get; set; }
        public RunMode RunMode { get; set; }
        public int? NumOrden { get; set; }
        public int NumTests { get; set; }
        public int NumTestsParalel { get; set; }
        public int RunTestNumber { get; set; }
        public int NumSequentialExecuted { get; set; }
        public bool Canceled { get; set; }
        public PlayConfig PreviousPlayConfig { get; set; }

        public void Add(int runTestNumber, SequenceContext context)
        {
            GetTestsGroup(runTestNumber).Add(context);
        }

        public List<SequenceContext> GetTestsGroup(int id)
        {
            lock (this)
            {

                if (!groups.TryGetValue(id, out List<SequenceContext> list))
                {
                    list = new List<SequenceContext>();
                    groups.Add(id, list);
                }

                return list;
            }
        }

        public SequenceContext GetTest(int runTestNumber, int numInstance = 1, bool firstInstanceIfNotFound = false)
        {
            if (runTestNumber <= 0)
                return null;

            List<SequenceContext> list = GetTestsGroup(runTestNumber);
            SequenceContext context = list.Where(p => p.NumInstance == numInstance).FirstOrDefault();

            if (context == null && firstInstanceIfNotFound)
                context = list.FirstOrDefault();

            return context;
        }
    }
}
