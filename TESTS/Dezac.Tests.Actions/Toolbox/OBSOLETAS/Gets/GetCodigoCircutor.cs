﻿using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(GetCodigoCircutorDescription))]
   

    public class GetCodigoCircutor : ActionBase
    {
        private string codCircutor = "";

        public string CodCircutor { get { return codCircutor; } }

        public int OrdenFabricacion { get; set; }
        public int NumProducto { get; set; }
        public int Version { get; set; }

        public override void Execute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            using (DezacService svc = new DezacService())
            {
                if (data == null || (data.NumOrden == null && data.NumProducto == 0))
                {
                    if (OrdenFabricacion != 0)
                    {
                        ORDEN orden = svc.GetOrden(OrdenFabricacion);

                        Assert.IsNotNull(orden, Error().PROCESO.PRODUCTO.NO_ORDEN_FABRICACION_NO_PRODUCTO("Orden de fabricación inexistente"));

                        codCircutor = svc.GetCodigoCircutor(orden.NUMPRODUCTO);                      
                    }
                    else
                        codCircutor = svc.GetCodigoCircutor(NumProducto);
                }
                else
                    codCircutor = svc.GetCodigoCircutor(data.NumProducto);
            }

            SequenceContext.Current.Variables.AddOrUpdate(ConstantsParameters.TestInfo.COD_CIRCUTOR, codCircutor);

            if (data != null && data.TestInfo != null)
                data.TestInfo.CodCircutor = codCircutor;
        }
    }

    public class GetCodigoCircutorDescription : ActionDescription
    {
        public override string Name { get { return "GetCodigoCircutor"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.database_process_icon; } }
    }
}
