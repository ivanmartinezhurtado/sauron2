﻿
using Instruments.Utility;

namespace Instruments.IO
{
    public enum Outputs
    {
        Multimetro,
        FreCH1,
        FreCH2,
        Aux,
    };

    public interface IMux
    {
        void Reset();
        bool Connect(InputMuxEnum input, Outputs output);
        bool Connect(int input, Outputs output);
        bool Connect2(int input, Outputs output, int? input2, Outputs? output2);
        bool Connect2(InputMuxEnum input, Outputs output, InputMuxEnum input2, Outputs output2);
        void Disconnect(int input, Outputs output);
        void Disconnect2(int input, Outputs output, int? input2, IO.Outputs? output2);
        void Disconnect2(InputMuxEnum input, Outputs output, InputMuxEnum input2, IO.Outputs output2);


        void Dispose();
    }
}
