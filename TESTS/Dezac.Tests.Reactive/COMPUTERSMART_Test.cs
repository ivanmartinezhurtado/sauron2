﻿using Dezac.Core.Utility;
using Dezac.Device.Reactive;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Reactive
{
    [TestVersion(1.05)]
    public class COMPUTERSMART_Test : TestBase
    {
        private Tower3 tower;
        private COMPUTERSMART compSmart;

        private const byte OUT_EV_GENERAL = 4;
        private const byte OUT_EV_PUNTAS_1 = 5;
        private const byte OUT_EV_KEY_UP = 8;
        private const byte OUT_EV_KEY_LEFT = 9;
        private const byte OUT_EV_KEY_SETUP = 10;
        private const byte OUT_EV_KEY_RIGHT = 11;
        private const byte OUT_EV_KEY_DOWN = 12;

        private const byte OUT_PILOTO_ROJO = 0;
        private const byte OUT_PILOTO_VERDE = 1;
        private const byte OUT_RELE_ALARMA = 17;
        private const byte OUT_230VAux = 15;


        private const byte IN_PRESENCIA_EQUIPO = 3;
        private const byte IN_RELE_ALAR_NO = 7;
        private const byte IN_RELE_ALAR_NC = 8;
        private const byte IN_RELE_1 = 9;
        private const byte IN_RELE_2 = 10;
        private const byte IN_RELE_3 = 11;
        private const byte IN_RELE_4 = 12;
        private const byte IN_RELE_5 = 13;
        private const byte IN_RELE_6 = 14;
        private const byte IN_RELE_7 = 15;
        private const byte IN_RELE_8 = 16;
        private const byte IN_RELE_9 = 17;
        private const byte IN_RELE_10 = 18;
        private const byte IN_RELE_11 = 19;
        private const byte IN_RELE_12 = 20;
        private const byte IN_LED = 22;

        private bool fast= false;

        COMPUTERSMART.SmartModels model;
        COMPUTERSMART.VoltageConfiguration voltageConfig;

        public void TestInitialization()
        {
            compSmart = AddInstanceVar(new COMPUTERSMART(Comunicaciones.SerialPort), "UUT");

            fast = Identificacion.MODELO.ToUpper().Trim() == "FAST";

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.IO.DO.Off(OUT_PILOTO_ROJO, OUT_PILOTO_VERDE); 

            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.IO.DO.On(OUT_EV_GENERAL);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI.WaitOn(IN_PRESENCIA_EQUIPO,10000);
            }, "Error: No se ha cerrado el utillaje.", 6, 1000, 1000, false, false);

            Delay(1000, "Esperando que entre el equipo");

            tower.IO.DO.OnWait(500, OUT_EV_PUNTAS_1, OUT_230VAux);

            tower.MTEPS.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.2, ParamUnidad.PorCentage);
            tower.MTEPS.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 35000, ParamUnidad.ms);
        }

        public void TestConsumption()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            CVMMINITower cvmminiConsumo;
            cvmminiConsumo = new CVMMINITower(Comunicaciones.SerialPortDevice);
            cvmminiConsumo.Modbus.PerifericNumber = Comunicaciones.PerifericoDevice;

            TestConsumo(Params.KW.Null.EnVacio.Vmin.Name, Params.KW.Null.EnVacio.Vmin.Min(), Params.KW.Null.EnVacio.Vmin.Max(),
            () => cvmminiConsumo.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vmin fuera de margenes");

            TestConsumo(Params.KVAR.Null.EnVacio.Vmin.Name, Params.KVAR.Null.EnVacio.Vmin.Min(), Params.KVAR.Null.EnVacio.Vmin.Max(),
            () => cvmminiConsumo.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vmin fuera de margenes");

            cvmminiConsumo.Dispose();
        }

        public void TestCommunications()
        {
            var version = "0.0.0";
            SamplerWithCancel((p) =>
            {
                version = compSmart.ReadFirmwareVersion();
                return true;
            }, "Error: El equipo no comunica", 5, 200, 2500, false, false);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error: la version de firmware del equipo no coincide con la de la BBDD"), ParamUnidad.SinUnidad);
        }

        public void TestSetupDefault()
        {
            compSmart.WriteFlagTest();

            model = new COMPUTERSMART.SmartModels();
            switch ((int)VectorHardware.GetDouble("RELES", 6, ParamUnidad.SinUnidad))
            {
                case 6:
                    model = COMPUTERSMART.SmartModels.Smart6;
                    break;
                case 12:
                    model = COMPUTERSMART.SmartModels.Smart12;
                    break;
                default:
                    throw new Exception("Error: Modelo en BBDD erroneo, valores aceptados: 6 y 12");
            }

            switch ((int)VectorHardware.GetDouble("SETUP_VOLTAJE", 400, ParamUnidad.V))
            {
                case 110:
                    voltageConfig = COMPUTERSMART.VoltageConfiguration._110V;
                    break;
                case 230:
                    voltageConfig = COMPUTERSMART.VoltageConfiguration._230V;
                    break;
                case 400:
                    voltageConfig = COMPUTERSMART.VoltageConfiguration._400V;
                    break;
                case 480:
                    voltageConfig = COMPUTERSMART.VoltageConfiguration._480V;
                    break;
                default:
                    throw new Exception("Error: Modelo en BBDD erroneo, valores aceptados: 110, 230, 400, 480");
            }

            if (fast)
                compSmart.WriteSetupInformation(new COMPUTERSMART.SetupInfoFAST(model));
            else
                compSmart.WriteSetupInformation(new COMPUTERSMART.SetupInfo(model));

            compSmart.WriteAlarmInformation(new COMPUTERSMART.AlarmsInfo(model));

            var freq = compSmart.ReadInternalFreq();
            compSmart.WriteSetupLimitsInformation(new COMPUTERSMART.SetupLimitsInfo(model, voltageConfig, freq, fast));

            ResetPowerSource();
            if (fast)
                Assert.AreEqual("SETUP", compSmart.ReadSetupInformationFAST().ToString(), new COMPUTERSMART.SetupInfoFAST(model).ToString(), Error().UUT.CONFIGURACION.NO_GRABADO("Error: No se ha grabado bien el configuracion del equipo"),ParamUnidad.SinUnidad);
            else
                Assert.AreEqual("SETUP", compSmart.ReadSetupInformation().ToString(), new COMPUTERSMART.SetupInfo(model).ToString(), Error().UUT.CONFIGURACION.NO_GRABADO("Error: No se ha grabado bien el configuracion del equipo"), ParamUnidad.SinUnidad);

            Assert.AreEqual("SETUP_ALARMS", compSmart.ReadAlarmsInformation().ToString(), new COMPUTERSMART.AlarmsInfo(model).ToString(), Error().UUT.CONFIGURACION.ALARMA("Error: No se han grabado bien las alarmas equipo"), ParamUnidad.SinUnidad);
            
            Assert.AreEqual("SETUP_LIMITS", compSmart.ReadSetupLimitsInformation().ToString(), new COMPUTERSMART.SetupLimitsInfo(model, voltageConfig, freq, fast).ToString(), Error().UUT.CONFIGURACION.NO_GRABADO("Error: No se han grabado bien los limites del equipo"), ParamUnidad.SinUnidad);
        }

        public void TestDisplay()
        {
            compSmart.WriteFlagTest();

            compSmart.WriteLeds(true, true);

            compSmart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.LEFT_PART);

            var frm = this.ShowForm(() => new ImageView("TEST DISPLAY LEFT", this.PathCameraImages + "\\COMPUTER_SMART\\IZQUIERDA.jpg"), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    compSmart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.LEFT_PART);
                    return frm.DialogResult == DialogResult.OK;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Resultado.Set("DISPLAY_LEFT", "OK", ParamUnidad.SinUnidad);

            compSmart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.RIGHT_PART);

            var frm2 = this.ShowForm(() => new ImageView("TEST DISPLAY RIGHT", this.PathCameraImages + "\\COMPUTER_SMART\\DERECHA.jpg"), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    compSmart.WriteDisplay(COMPUTERSMART.DisplayConfigurations.RIGHT_PART);
                    return frm2.DialogResult == DialogResult.OK;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                this.CloseForm(frm2);
            }

            Resultado.Set("DISPLAY_RIGHT", "OK", ParamUnidad.SinUnidad);
        }

        public void TestLeds()
        {
            compSmart.WriteFlagTest();

            compSmart.WriteLeds(true, false);

            var frm = this.ShowForm(() => new ImageView("TEST LED1", this.PathCameraImages + "\\COMPUTER_SMART\\LED_DERECHA.jpg"), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    compSmart.WriteLeds(true, false);
                    return frm.DialogResult == DialogResult.OK;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Resultado.Set("LED_DERECHA", "OK", ParamUnidad.SinUnidad);

            compSmart.WriteLeds(false, true);

            var frm2 = this.ShowForm(() => new ImageView("TEST LED2", this.PathCameraImages + "\\COMPUTER_SMART\\LED_IZQUIERDA.jpg"), MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((p) =>
                {
                    compSmart.WriteLeds(false, true);
                    return frm2.DialogResult == DialogResult.OK;
                }, "Error display incorrecto", 50, 1000, 100);
            }
            finally
            {
                this.CloseForm(frm2);
            }

            Resultado.Set("LED_IZQUIERDA", "OK", ParamUnidad.SinUnidad);
        }

        public void TestRelay()
        {
            var relesPares = new List<int> { IN_RELE_2, IN_RELE_4, IN_RELE_6 };
            var relesImpares = new List<int> { IN_RELE_1, IN_RELE_3, IN_RELE_5 };

            if (model == COMPUTERSMART.SmartModels.Smart12)
            {
                relesPares.Add(IN_RELE_8, IN_RELE_10, IN_RELE_12);
                relesImpares.Add(IN_RELE_7, IN_RELE_9, IN_RELE_11);
            }

            var relesTodos = new List<int>();
            relesTodos = relesTodos.Concat(relesImpares).Concat(relesPares).ToList();
            relesPares.Add(IN_RELE_ALAR_NC);
            relesImpares.Add(IN_RELE_ALAR_NO);

            Dictionary <COMPUTERSMART.RelayInformation, Func<bool>> RelayDictionary = new Dictionary<COMPUTERSMART.RelayInformation, Func<bool>>()
            {
                { new COMPUTERSMART.RelayInformation(COMPUTERSMART.RelayInformation.Configurations.ODDS), new Func<bool>(()=> { return tower.IO.DI.WaitAllOn(500, relesImpares.ToArray()) && tower.IO.DI.WaitAllOff(500, relesPares.ToArray()); }) },
                { new COMPUTERSMART.RelayInformation(COMPUTERSMART.RelayInformation.Configurations.EVEN), new Func<bool>(()=> { return tower.IO.DI.WaitAllOn(500, relesPares.ToArray()) && tower.IO.DI.WaitAllOff(500, relesImpares.ToArray()); })},
                { new COMPUTERSMART.RelayInformation(COMPUTERSMART.RelayInformation.Configurations.ALL), new Func<bool>(()=> { return tower.IO.DI.WaitAllOn(500, relesTodos.ToArray()); }) },
                { new COMPUTERSMART.RelayInformation(COMPUTERSMART.RelayInformation.Configurations.NONE), new Func<bool>(()=> { return tower.IO.DI.WaitAllOff(500, relesTodos.ToArray()); }) },
            };

            compSmart.WriteFlagTest();

            tower.IO.DO.On(OUT_RELE_ALARMA);

            foreach (KeyValuePair<COMPUTERSMART.RelayInformation, Func<bool>> relay in RelayDictionary)
                this.SamplerWithCancel((p) =>
                {
                    compSmart.WriteRelaysInformation(relay.Key);
                    Assert.IsTrue(string.Format("RELES_{0}", relay.Key.Config.ToString()), relay.Value(), Error().UUT.CONFIGURACION.RELE(string.Format("Error en la prueba de reles {0}", relay.Key.Config.ToString())));
                    return true;
                }, "", 2, 500, 100,false, false);

            tower.IO.DO.Off(OUT_RELE_ALARMA);
        }

        public void TestKeyborad()
        {
            compSmart.WriteFlagTest();

            Dictionary<COMPUTERSMART.KeyboardKeys, byte> KeyBoardDictionary = new Dictionary<COMPUTERSMART.KeyboardKeys, byte>()
            {
                { COMPUTERSMART.KeyboardKeys.DOWN, OUT_EV_KEY_DOWN },
                { COMPUTERSMART.KeyboardKeys.UP, OUT_EV_KEY_UP },
                { COMPUTERSMART.KeyboardKeys.LEFT, OUT_EV_KEY_LEFT },
                { COMPUTERSMART.KeyboardKeys.RIGHT, OUT_EV_KEY_RIGHT },
                { COMPUTERSMART.KeyboardKeys.SETUP, OUT_EV_KEY_SETUP },
            };

            foreach (KeyValuePair<COMPUTERSMART.KeyboardKeys, byte> key in KeyBoardDictionary)
            {
                tower.IO.DO.On(key.Value);
                this.SamplerWithCancel((p) =>
                {
                    Assert.IsTrue(string.Format("TECLA_{0}", key.Key.ToString()), compSmart.ReadKeyboard() == key.Key, Error().UUT.CONFIGURACION.SETUP(string.Format("Error: No se ha podido leer la tecla {0}", key.Key.ToString())));
                    return true;
                }, "", 3, 200, 200, false, false);

                tower.IO.DO.Off(key.Value);
            }
        }

        public void TestAdjustFrequency(ushort minsamples = 5, ushort maxsamples = 10, ushort timesamples = 1100)
        {
            AdjustValueDef GainFreqBBDD = new AdjustValueDef(Params.GAIN_FREC.Null.Ajuste.Name, 0, Params.GAIN_FREC.Null.Ajuste.Min(), Params.GAIN_FREC.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            ushort InternalFreq = (ushort)compSmart.ReadInternalFreq();

            var gainFreq = compSmart.CalculateFreqGain(InternalFreq, minsamples, maxsamples, timesamples,
                (factor) =>
                {
                    GainFreqBBDD.Value = factor;
                    GainFreqBBDD.AddToResults(Resultado);
                    return !HasError(GainFreqBBDD, false);
                }
                );

            compSmart.WriteSetupLimitsInformation(new COMPUTERSMART.SetupLimitsInfo(model, voltageConfig, (ushort)gainFreq, fast));

            ResetPowerSource();

            Assert.AreEqual(compSmart.ReadSetupLimitsInformation().CalibracionFrecuenciaInterna, (ushort)gainFreq, Error().UUT.MEDIDA.NO_GRABADO("Error: No se ha grabado correctamente el valor de ganancia de frecuencia"));

            AdjustValueDef FreqBBDD = new AdjustValueDef(Params.FREQ.Null.TestPoint("AJUSTE_FRECUENCIA").Name, 0, Params.FREQ.Null.TestPoint("AJUSTE_FRECUENCIA").Min(), Params.FREQ.Null.TestPoint("AJUSTE_FRECUENCIA").Max(),0,0, ParamUnidad.Hz);

            TestMeasureBase(FreqBBDD,
                (step) =>
                {
                    return (double)compSmart.ReadPointsVariables().Frequency;
                }, 0, 3, 1100);
        }

        public void TestAdjustVoltageCurrent(ushort delsamples = 0, ushort minsamples = 2, ushort maxsamples = 5,  ushort timesamples = 1100)
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var DesfaseDatabase = Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue { L1 = voltageDatabase, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = currentDatabase, L2 = 0, L3 = 0 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, DesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.SUM_V.L1.Ajuste.Name, 0,  Params.SUM_V.Null.Ajuste.Min(), Params.SUM_V.Null.Ajuste.Max(), 0, 0,ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.SUM_V.L2.Ajuste.Name, 0,  Params.SUM_V.Null.Ajuste.Min(), Params.SUM_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.SUM_V.L3.Ajuste.Name, 0,  Params.SUM_V.Null.Ajuste.Min(), Params.SUM_V.Null.Ajuste.Max(), 0, 0,ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.SUM_I.Null.Ajuste.Name, 0,  Params.SUM_I.Null.Ajuste.Min(), Params.SUM_I.Null.Ajuste.Max(), 0, 0,ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.SUM_KW.Null.Ajuste.Name, 0, Params.SUM_KW.Null.Ajuste.Min(), Params.SUM_KW.Null.Ajuste.Max(), 0, 0,ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.SUM_KVAR.Null.Ajuste.Name, 0, Params.SUM_KVAR.Null.Ajuste.Min(), Params.SUM_KVAR.Null.Ajuste.Max(), 0, 0,ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.NUM_MUESTRAS.Null.Ajuste.Name, 0, Params.NUM_MUESTRAS.Null.Ajuste.Min(), Params.NUM_MUESTRAS.Null.Ajuste.Max(), 0,0, ParamUnidad.Numero));
            defs.Add(new AdjustValueDef(Params.OFFSET_V.L1.Ajuste.Name, 0, Params.OFFSET_V.Null.Ajuste.Min(), Params.OFFSET_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.OFFSET_V.L2.Ajuste.Name, 0, Params.OFFSET_V.Null.Ajuste.Min(), Params.OFFSET_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.OFFSET_V.L3.Ajuste.Name, 0, Params.OFFSET_V.Null.Ajuste.Min(), Params.OFFSET_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.OFFSET_I.Null.Ajuste.Name, 0, Params.OFFSET_I.Null.Ajuste.Min(), Params.OFFSET_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.COS_PHI.Null.Ajuste.Name, 0, Params.COS_PHI.Null.Ajuste.Min(), Params.COS_PHI.Null.Ajuste.Max(), 0, 0, ParamUnidad.Grados));
            defs.Add(new AdjustValueDef(Params.FREQ.Null.Ajuste.Name, 0, Params.FREQ.Null.Ajuste.Min(), Params.FREQ.Null.Ajuste.Max(), 0, 0, ParamUnidad.Hz));
            defs.Add(new AdjustValueDef(Params.V.L1.Ajuste.Name,0, 0, 0, Params.V.Null.Ajuste.Avg(), Params.V.Null.Ajuste.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.L2.Ajuste.Name, 0, 0, 0, Params.V.Null.Ajuste.Avg(), Params.V.Null.Ajuste.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.L3.Ajuste.Name, 0, 0, 0, Params.V.Null.Ajuste.Avg(), Params.V.Null.Ajuste.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.Null.Ajuste.Name, 0, 0, 0, Params.I.Null.Ajuste.Avg(), Params.I.Null.Ajuste.Tol(), ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.PF.Null.Ajuste.Name, 0, Params.PF.Null.Ajuste.Min(), Params.PF.Null.Ajuste.Max(), 0, 0, ParamUnidad.Numero));
            defs.Add(new AdjustValueDef(Params.TEMP.Null.Ajuste.Name, 0, Params.TEMP.Null.Ajuste.Min(), Params.TEMP.Null.Ajuste.Max(), 0, 0, ParamUnidad.Grados));
            defs.Add(new AdjustValueDef(Params.THD_V.Null.Ajuste.Name, 0, Params.THD_V.Null.Ajuste.Min(), Params.THD_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new AdjustValueDef(Params.THD_I.Null.Ajuste.Name, 0, Params.THD_I.Null.Ajuste.Min(), Params.THD_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.PorCentage));

            var defsCalibration = new List<AdjustValueDef>();
            defsCalibration.Add( new AdjustValueDef(Params.GAIN_V.L1.Ajuste.Name,0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_V.L2.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_V.L3.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_I.Null.Ajuste.Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new AdjustValueDef(Params.CURRENT_FACTOR.Null.Ajuste.Name, 0, Params.CURRENT_FACTOR.Null.Ajuste.Min(), Params.CURRENT_FACTOR.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_DESFASE.Null.Ajuste.Name, 0, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            compSmart.WriteFlagTest();

            var result = compSmart.CalculateAdjustFactorsVoltageCurrentDesfase(delsamples, minsamples, maxsamples, timesamples, tower.PowerSourceIII.ReadVoltage().L1, tower.PowerSourceIII.ReadCurrent().L1, voltageConfig,
            () =>
            {
                var pointMeasures = compSmart.ReadPointsVariables();
                var measures = compSmart.ReadVariables();
                var THDMeasures = compSmart.ReadHarmonicMeasures();

                return new double[]
                {
                    pointMeasures.VoltageL1, pointMeasures.VoltageL2, pointMeasures.VoltageL3,
                    pointMeasures.Current,
                    pointMeasures.ReactivePower, pointMeasures.ActivePower, pointMeasures.Nmos,
                    pointMeasures.OffsetL1, pointMeasures.OffsetL2, pointMeasures.OffsetL3,
                    pointMeasures.OffsetCurrent, pointMeasures.CosPhi, pointMeasures.Frequency,
                    measures.VoltageL1, measures.VoltageL2, measures.VoltageL3,
                    measures.Current, measures.PF,
                    measures.Temp, measures.THDV, measures.THDI,
                    };
            },
            (adjustValues) =>
            {
                defsCalibration[0].Value = adjustValues.CalibrationVL1; defsCalibration[1].Value = adjustValues.CalibrationVL2; defsCalibration[2].Value = adjustValues.CalibrationVL3;
                defsCalibration[3].Value = adjustValues.CalibrationI; defsCalibration[4].Value = adjustValues.CalibrationIFactor; defsCalibration[5].Value = adjustValues.CalibrationDesfase;

                foreach (var res in defsCalibration)
                    res.AddToResults(Resultado);

                return HasError(defsCalibration, false);
            },
            (samples) =>
            {
                for (int i = 0; i < defs.ToArray().Length; i++)
                    defs[i].Value = samples[i];

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                return HasError(defs, false);
            });

            if (!result.Item1)
                throw new Exception(string.Format("Error en Ajuste de volatge y corriente por {0} ", result.Item3));

            var limits = compSmart.ReadSetupLimitsInformation();
            limits.CalibracionMedidaV1 = result.Item2.CalibrationVL1;
            limits.CalibracionMedidaV2 = result.Item2.CalibrationVL2;
            limits.CalibracionMedidaV3 = result.Item2.CalibrationVL3;
            limits.CalibracionMedidaI = result.Item2.CalibrationI;
            limits.FactorCalibracionCorriente = result.Item2.CalibrationIFactor;
            limits.AnguloDesfase = (byte)result.Item2.CalibrationDesfase;

            compSmart.WriteSetupLimitsInformation(limits);

            Delay(2000, "");

            ResetPowerSource();

            Assert.AreEqual(compSmart.ReadSetupLimitsInformation(), limits, Error().UUT.MEDIDA.SETUP("Error: No se han guardado bien las ganacias"));
        }

        public void TestAdjustLeakCurrent(ushort delsamples = 0, ushort minsamples = 2, ushort maxsamples = 5, ushort timesamples = 1100)
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I_LK.Null.Ajuste.Name, 0.002, ParamUnidad.A);
            var DesfaseDatabase = Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue { L1 = voltageDatabase, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = 0, L2 = currentDatabase, L3 = 0 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, DesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.SUM_ILK.Null.Ajuste.Name, 0, Params.SUM_ILK.Null.Ajuste.Min(), Params.SUM_ILK.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.I_LK.Null.Ajuste.Name, 0, 0, 0, Params.I_LK.Null.Ajuste.Avg(), Params.I_LK.Null.Ajuste.Tol(), ParamUnidad.A));

            var defsCalibration = new List<AdjustValueDef>();
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_I_LK.L1.Ajuste.Name, 0, Params.GAIN_I_LK.Null.Ajuste.Min(), Params.GAIN_I_LK.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            compSmart.WriteFlagTest();

            Delay(2000, "");

            var result = compSmart.CalculateAdjustFactorsCurrentLeak(delsamples, minsamples, maxsamples, timesamples, tower.PowerSourceIII.ReadCurrent().L2,
            () =>
            {
                var pointMeasures = compSmart.ReadPointsVariables();
                Delay(1000, "");
                var poinLeak = compSmart.ReadLeakCurrentMeasure() / 1000D;

                return new double[]
                {
                    pointMeasures.LeakCurrent, poinLeak
                };
            },
            (adjustValues) =>
            {
                defsCalibration[0].Value = adjustValues.CalibrationILCK; 

                foreach (var res in defsCalibration)
                    res.AddToResults(Resultado);

                return HasError(defsCalibration, false);
            },
            (samples) =>
            {
                for (int i = 0; i < defs.ToArray().Length; i++)
                    defs[i].Value = samples[i];

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                return HasError(defs, false);
            });

            if (!result.Item1)
                throw new Exception(string.Format("Error en Ajuste de corriente de fugas por {0} ", result.Item3));

            var limits = compSmart.ReadSetupLimitsInformation();
            limits.CalibracionMedidaIFugas = result.Item2.CalibrationILCK;

            compSmart.WriteSetupLimitsInformation(limits);

            Delay(2000, "");

            ResetPowerSource();

            Assert.AreEqual(compSmart.ReadSetupLimitsInformation(), limits, Error().UUT.MEDIDA.SETUP("Error: No se ha guardado bien la ganancia de corriente de fugas"));
        }

        public void TestAdjustPower(ushort delsamples = 0, ushort minsamples = 2, ushort maxsamples = 5, ushort timesamples = 1100)
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var DesfaseDatabase = Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue { L1 = voltageDatabase, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = currentDatabase, L2 = 0, L3 = 0 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, DesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.SUM_KW.Null.Ajuste.Name, 0, Params.SUM_KW.Null.Ajuste.Min(), Params.SUM_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.SUM_KVAR.Null.Ajuste.Name, 0, Params.SUM_KVAR.Null.Ajuste.Min(), Params.SUM_KVAR.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.NUM_MUESTRAS.Null.Ajuste.Name, 0, Params.NUM_MUESTRAS.Null.Ajuste.Min(), Params.NUM_MUESTRAS.Null.Ajuste.Max(), 0,0, ParamUnidad.Puntos));

            var defsCalibration = new List<AdjustValueDef>();
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_KVAR.Null.Ajuste.Name, 0, Params.GAIN_KVAR.Null.Ajuste.Min(), Params.GAIN_KVAR.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new AdjustValueDef(Params.GAIN_KW.Null.Ajuste.Name, 0, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            compSmart.WriteFlagTest();

            var result = compSmart.CalculateAdjustFactorsPower(delsamples, minsamples, maxsamples, timesamples, tower.PowerSourceIII.ReadVoltage().L1, tower.PowerSourceIII.ReadCurrent().L1,
            () =>
            {
                var pointMeasures = compSmart.ReadPointsVariables();
                return new double[]
                {
                    pointMeasures.ReactivePower, pointMeasures.ActivePower, pointMeasures.Nmos
                };
            },
            (adjustValues) =>
            {
                defsCalibration[0].Value = adjustValues.CalibrationPowerReactive; defsCalibration[1].Value = adjustValues.CalibrationPowerActive; 

                foreach (var res in defsCalibration)
                    res.AddToResults(Resultado);

                return HasError(defsCalibration, false);
            },
            (samples) =>
            {
                for (int i = 0; i < defs.ToArray().Length; i++)
                    defs[i].Value = samples[i];

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                return HasError(defs, false);
            });

            if (!result.Item1)
                throw new Exception(string.Format("Error en Ajuste de potencia por {0} ", result.Item3));

            var limits = compSmart.ReadSetupLimitsInformation();
            limits.CalibracionMedidaPotenciaReactiva = result.Item2.CalibrationPowerReactive;
            limits.CalibracionMedidaPotenciaActiva = result.Item2.CalibrationPowerActive;

            compSmart.WriteSetupLimitsInformation(limits);

            Delay(2000, "");

            ResetPowerSource();

            Assert.AreEqual(compSmart.ReadSetupLimitsInformation(), limits, Error().UUT.MEDIDA.SETUP("Error: No se han guardado bien la ganancias de potencia"));
        }

        public void TestVerificacion(ushort minsamples = 2, ushort maxsamples = 5, ushort timesamples = 1100)
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 5, ParamUnidad.A);
            var DesfaseDatabase = Consignas.GetDouble(Params.PHASE.Null.Verificacion.Name, 45, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue { L1 = voltageDatabase, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = currentDatabase, L2 = 0, L3 = 0 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, DesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Avg(), Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.L2.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Avg(), Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.V.L3.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Avg(), Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.Null.Verificacion.Name, 0, 0, 0, Params.I.Null.Verificacion.Avg(), Params.I.Null.Verificacion.Tol(), ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.PF.Null.Verificacion.Name, 0, Params.PF.Null.Verificacion.Min(), Params.PF.Null.Verificacion.Max(), Params.PF.Null.Verificacion.Avg(), 0, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.TEMP.Null.Verificacion.Name, 0, Params.TEMP.Null.Verificacion.Min(), Params.TEMP.Null.Verificacion.Max(), Params.TEMP.Null.Verificacion.Avg(), 0, ParamUnidad.Grados));
            defs.Add(new AdjustValueDef(Params.THD_V.Null.Verificacion.Name, 0, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new AdjustValueDef(Params.THD_I.Null.Verificacion.Name, 0,  Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            defs.Add(new AdjustValueDef(Params.KW.Null.Verificacion.Name, 0, 0, 0, Params.KW.Null.Verificacion.Avg(), Params.KW.Null.Verificacion.Tol(), ParamUnidad.W));
            defs.Add(new AdjustValueDef(Params.KVAR.Null.Verificacion.Name, 0, 0, 0, Params.KVAR.Null.Verificacion.Avg(), Params.KVAR.Null.Verificacion.Tol(),ParamUnidad.Var));
            defs.Add(new AdjustValueDef(Params.KVA.Null.Verificacion.Name, 0, 0, 0, Params.KVA.Null.Verificacion.Avg(), Params.KVA.Null.Verificacion.Tol(), ParamUnidad.VA));


            TestCalibracionBase(defs,
                () =>
                {
                    var measures = compSmart.ReadVariables();

                    return new double[] { measures.VoltageL1, measures.VoltageL2, measures.VoltageL3,
                                    measures.Current, measures.PF, measures.Temp,
                                    measures.THDV, measures.THDI,
                                    measures.PowerActive, measures.PowerReactive, measures.PowerApparent
                                    };
                },
                () =>
                {
                    var average = new List<double>();

                    foreach (AdjustValueDef def in defs)
                        average.Add(def.Average);
                    return average.ToArray();
                }, minsamples, maxsamples, timesamples);
        }

        public void TestVerificacionCurrentLeak(ushort minsamples = 2, ushort maxsamples = 5, ushort timesamples = 1100)
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I_LK.Null.Verificacion.Name, 0.002, ParamUnidad.A);
            var DesfaseDatabase = Consignas.GetDouble(Params.PHASE.Null.Verificacion.Name, 45, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue { L1 = voltageDatabase, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = 0, L2 = currentDatabase, L3 = 0 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, DesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.I_LK.Null.Verificacion.Name, 0, 0, 0, Params.I_LK.Null.Verificacion.Avg(), Params.I_LK.Null.Verificacion.Tol(), ParamUnidad.A));

            Delay(2000, "");

            TestCalibracionBase(defs,
                () =>
                {
                    return new double[] { compSmart.ReadLeakCurrentMeasure()/1000D };
                },
                () =>
                {
                    var average = new List<double>();

                    foreach (AdjustValueDef def in defs)
                        average.Add(def.Average);
                    return average.ToArray();
                }, minsamples, maxsamples, timesamples);
        }

        public void TestCostumization()
        {
            var idpInfo = new COMPUTERSMART.IdpInformation();
            idpInfo.SerialNumber = Int64.Parse(TestInfo.NumSerie);
            idpInfo.BastidorNumber = (long)TestInfo.NumBastidor;
            idpInfo.ErrorCode = 0;
            idpInfo.HardwareVector = (ushort)((ushort)model + (ushort)voltageConfig);
            compSmart.WriteIdpInformation(idpInfo);

            compSmart.WriteDeleteMaximum();

            Delay(2000, "");

            ResetPowerSource();

            var idpInfo2 = compSmart.ReadIdpInformation();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, idpInfo2.SerialNumber.ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_GRABADO("Error: El nuemro de serie no se ha grabado correctamente"), ParamUnidad.SinUnidad);
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, idpInfo2.BastidorNumber, (long)TestInfo.NumBastidor, Error().UUT.NUMERO_DE_BASTIDOR.NO_GRABADO("Error: El nuemro de bastidor no se ha grabado correctamente"), ParamUnidad.SinUnidad);
            Assert.AreEqual(ConstantsParameters.Identification.VECTOR_HARDWARE, idpInfo2.HardwareVector, (ushort)((ushort)model + (ushort)voltageConfig), Error().UUT.HARDWARE.VECTOR_HARDWARE("Error: El vector de Hardware, no se ha grabado correctamente"), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (compSmart != null)
                compSmart.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.IO.DO.OffWait(500, OUT_230VAux, OUT_EV_PUNTAS_1);
                tower.IO.DO.Off(OUT_EV_GENERAL);
                tower.Dispose();

                if (IsTestError && tower != null)
                    tower.IO.DO.On(OUT_PILOTO_ROJO);
                if (!IsTestError && tower != null)
                    tower.IO.DO.On(OUT_PILOTO_VERDE);
            }
        }

        /////////////////////////////////////////

        private void ResetPowerSource(bool test = true)
        {
            var voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V);

            Delay(2000, "Esperando finalizacion de tareas del equipo");

            tower.ActiveVoltageCircuit(false, false, false, false);
            //tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            Delay(2000, "Esperando apagado del equipo");

            tower.ActiveVoltageCircuit();
            //tower.PowerSourceIII.ApplyAndWaitStabilisation(voltage, 0, 50);

            if (test == true)
                SamplerWithCancel((p) => { compSmart.WriteFlagTest(); return true; }, "Error. No se puede establecer comunicación con el equipo despues del reset", 8, 1000, 4000);
        }

        public Tuple<double, double, double> ApplyAndWaitStabilisation(double desfase = 45, string descriptionPoint = "")
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var anguloDesfaseDatabase = Consignas.GetDouble(Params.PHASE.Other(descriptionPoint).Ajuste.Name, desfase, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue { L1 = voltageDatabase, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = currentDatabase, L2 = currentDatabase, L3 = currentDatabase };

            tower.PowerSourceIII.Tolerance = Consignas.GetDouble("TOLERANCIA_ESTABILIZACION_MTE", 0.01, ParamUnidad.PorCentage);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, anguloDesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var voltageReference = tower.PowerSourceIII.ReadVoltage().L1;
            Resultado.Set(Params.V.Null.Ajuste.TestPoint("PATRON").Name, voltageReference, ParamUnidad.V);

            var currentReference = tower.PowerSourceIII.ReadCurrent().L1;
            Resultado.Set(Params.I.Null.Ajuste.TestPoint("PATRON").Name, currentReference, ParamUnidad.A);

            return Tuple.Create(voltageReference, currentReference, anguloDesfaseDatabase);
        }
    }
}
