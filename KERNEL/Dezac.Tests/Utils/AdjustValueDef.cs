﻿using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Dezac.Tests.Utils
{
    public class AdjustValueDef : ICloneable
    {
        private double value;
        private double max;
        private double min;
        private double average;
        private double tolerance;
        private double varianza;
        private double maxVarianza;

        public string Name { get; set; }

        public string VarNamesDevice { get; set; }

        public string VarNamesPattern { get; set; }

        public ParamUnidad Unidad { get; set; }

        [Browsable(false)]
        public bool IsMaxMin { get; set; }

        [Browsable(false)]
        public virtual double Max
        {
            get
            {
                return Math.Round(max, 12);
            }
            set
            {
                this.max = value;
            }
        }

        [Browsable(false)]
        public virtual double Min
        {
            get
            {
                return Math.Round(min, 12);
            }
            set
            {
                this.min = value;
            }
        }

        [Browsable(false)]
        public virtual double Average
        {
            get
            {
                return Math.Round(average, 12);
            }
            set
            {
                this.average = value;
            }
        }

        [Browsable(false)]
        public virtual double Tolerance
        {
            get
            {
                return Math.Round(tolerance, 12);
            }
            set
            {
                this.tolerance = value;
            }
        }

        [Browsable(false)]
        public virtual double MaxVarianza
        {
            get
            {
                return Math.Round(maxVarianza, 12);
            }
            set
            {
                this.maxVarianza = value;
            }
        }

        [Browsable(false)]
        public double Varianza
        {
            get
            {
                return Math.Round(varianza, 12);
            }
            set
            {
                this.varianza = value;
            }
        }

        [Browsable(false)]
        public double Value
        {
            get
            {
                return Math.Round(value, 12);
            }
            set
            {
                this.value = value;
            }
        }

        [Browsable(false)]
        public double Error
        {
            get
            {
                if (Average == 0)
                    return IsValid() ? 0 : 100;
                else
                    return Math.Round(((Value - Average) / Average) * 100, 12);
            }
        }

        //--------------------------------------------------------------------

        public AdjustValueDef()
        {

        }

        public AdjustValueDef(TestModeParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null)
        {
            IsMaxMin = true;

            Name = paramater.Name;
     
            min = !Min.HasValue ? paramater.Min() : paramater.Min(Min.Value);
            max = !Max.HasValue ? paramater.Max() : paramater.Max(Max.Value);
            
            maxVarianza = MaxVarianza.HasValue ? MaxVarianza.Value : 0;
            Unidad = unidad;
        }

        public AdjustValueDef(TestModeParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol= null, double? MaxVarianza = null)
        {
            IsMaxMin = isMaxMin;

            Name = paramater.Name;
            if (isMaxMin)
            {
                if (Avg.HasValue && Tol.HasValue)
                {
                    min = paramater.Min(Avg.Value - Tol.Value);
                    max = paramater.Max(Avg.Value + Tol.Value);
                }
                else
                {
                    min = paramater.Min();
                    max = paramater.Max();
                }
                average = (Math.Abs(max - min) / 2) + Min;
                tolerance = Tol.Value;
            }
            else
            {
                average = !Avg.HasValue ? paramater.Avg() : paramater.Avg(Avg.Value);
                tolerance = !Tol.HasValue ? paramater.Tol() : paramater.Tol(Tol.Value);
                max = average + Math.Abs(average * (tolerance / 100));
                min = average - Math.Abs(average * (tolerance / 100));
            }

            maxVarianza = MaxVarianza.HasValue ? MaxVarianza.Value : 0;
            Unidad = unidad;
        }

        public AdjustValueDef(ExtensionParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null)
        {
            IsMaxMin = true;

            Name = paramater.Name;

            min = !Min.HasValue ? paramater.Min() : paramater.Min(Min.Value);
            max = !Max.HasValue ? paramater.Max() : paramater.Max(Max.Value);

            maxVarianza = MaxVarianza.HasValue ? MaxVarianza.Value : 0;
            Unidad = unidad;
        }

        public AdjustValueDef(ExtensionParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null)
        {
            IsMaxMin = isMaxMin;

            Name = paramater.Name;
            if (isMaxMin)
            {
                if (Avg.HasValue && Tol.HasValue)
                {
                    min = paramater.Min(Avg.Value - Tol.Value);
                    max = paramater.Max(Avg.Value + Tol.Value);
                }
                else
                {
                    min = paramater.Min();
                    max = paramater.Max();
                }
                average = (Math.Abs(max - min) / 2) + Min;
                tolerance = Tol.Value;
            }
            else
            {
                average = !Avg.HasValue ? paramater.Avg() : paramater.Avg(Avg.Value);
                tolerance = !Tol.HasValue ? paramater.Tol() : paramater.Tol(Tol.Value);
                max = average + Math.Abs(average * (tolerance / 100));
                min = average - Math.Abs(average * (tolerance / 100));
            }

            maxVarianza = MaxVarianza.HasValue ? MaxVarianza.Value : 0;
            Unidad = unidad;
        }

        //--------------------------------------------------------------------

        public AdjustValueDef(string name, double value, double min, double max, double average, double tolerance, ParamUnidad unidad)
        {
            Name = name;
            this.value = value;
            this.min = min;
            this.max = max;
            this.average = average;
            this.tolerance = tolerance;
            Unidad = unidad;

            if (Min != 0 || Max != 0)
                IsMaxMin = true;
        }

        public AdjustValueDef(string name, double min, double max, ParamUnidad unidad)
        {
            IsMaxMin = true;
            Name = name;
            this.min = min;
            this.max = max;
            Unidad = unidad;
        }

        public AdjustValueDef(string name, double average, double tolerance, double varianza, ParamUnidad unidad)
        {
            IsMaxMin = false;
            Name = name;
            this.average = average;
            this.tolerance = tolerance;
            this.maxVarianza = varianza;
            Unidad = unidad;
            calculateMaxMin();
        }

        public AdjustValueDef(string name, double value, double min, double max, double average, double tolerance, double varianza, ParamUnidad unidad)
        {
            Name = name;
            this.value = value;
            this.min = min;
            this.max = max;
            this.average = average;
            this.tolerance = tolerance;
            this.maxVarianza = varianza;
            Unidad = unidad;

            if (Min != 0 || Max != 0)
                IsMaxMin = true;
        }

        //*************************************************************************************************************

        public bool IsValid()
        {
            return IsValid(Value);
        }

        public bool IsValid(double value)
        {
            calculateMaxMin();
            return Min <= value && Max >= value;
        }

        public bool IsValidAverageError()
        {
            return IsValidAverageError(Value);
        }

        public bool IsValidAverageError(double value)
        {
            Value = value;

            if (Average == 0)
                return IsValid(Value);

            calculateMaxMin();
            return Math.Abs(Error) <= Tolerance;
        }

        public void calculateMaxMin()
        {
            if(IsMaxMin)
            {
                average = Average != 0 ? Average : (Math.Abs(Max - Min) / 2) + Min;
                tolerance = Tolerance != 0 ? Tolerance : Math.Abs((((Max - Min) / 2) / Average) * 100);
            }
            else
            {
                if (Average != 0)
                {
                    max = Average + Math.Abs(Average * (Tolerance / 100));
                    min = Average - Math.Abs(Average * (Tolerance / 100));
                }
                else if (Average == 0 && Tolerance != 0)
                {
                    max = Tolerance;
                    min = -Tolerance;
                }
            }           
        }

        public bool IsCorrect()
        {
            return IsCorrect(Value);
        }

        public bool IsCorrect(double value)
        {
            if (IsMaxMin)
                return IsValid(value);        
            else
                return IsValidAverageError(value);      
        }

        public bool IsValidVariance()
        {
            return IsValidVariance(Value);
        }

        public bool IsValidVariance(double value)
        {
            if (MaxVarianza == 0)
                return true;

            return MaxVarianza >= Varianza;
        }

        public override string ToString()
        {
            return string.Format("{0} => {1} ( Error: {2}, Avg: {3}, Tol.: {4}, Max: {5}, Min: {6}, Varianza: {7}, MaxVarianza: {8} )",
                Name, Value, Error, Average, Tolerance, Max, Min, Varianza, MaxVarianza);
        }

        public object Clone()
        {
            return new AdjustValueDef(this.Name, this.Value, this.Min, this.Max, this.Average, this.Tolerance, this.Unidad);
        }
    }

    public class TriAdjustValueDef
    {
        public AdjustValueDef L1 { get; set; }
        public AdjustValueDef L2 { get; set; }
        public AdjustValueDef L3 { get; set; }
        public AdjustValueDef Neutro { get; set; }
        public AdjustValueDef NeutroCalc { get; set; }
        public AdjustValueDef Fugas { get; set; }

        public TriAdjustValueDef()
        {
        }

        public TriAdjustValueDef(AdjustValueDef value, bool hasNeutro = false,  bool hasNeutroCalc=false, bool hasFugas=false)
        {
            CrealteAdjustOtherLines(value, hasNeutro, hasNeutroCalc, hasFugas);
        }

        public TriAdjustValueDef(string nameL1, double min, double max, double average, double tolerance, ParamUnidad unidad = ParamUnidad.Puntos, bool hasNeutro=false, bool hasNeutroCalc=false, bool hasFugas=false)
        {
            if (nameL1.Contains("_L12"))
            {
                L1 = new AdjustValueDef(nameL1, 0, min, max, average, tolerance, unidad);
                L2 = new AdjustValueDef(nameL1.Replace("_L12", "_L23"), 0, min, max, average, tolerance, unidad);
                L3 = new AdjustValueDef(nameL1.Replace("_L12", "_L31"), 0, min, max, average, tolerance, unidad);
            }
            else
            {
                L1 = new AdjustValueDef(nameL1, 0, min, max, average, tolerance, unidad);
                L2 = new AdjustValueDef(nameL1.Replace("_L1", "_L2"), 0, min, max, average, tolerance, unidad);
                L3 = new AdjustValueDef(nameL1.Replace("_L1", "_L3"), 0, min, max, average, tolerance, unidad);
            }

            if (hasNeutro)
                Neutro = new AdjustValueDef(nameL1.Replace("_L1", "_LN"), 0, min, max, average, tolerance, unidad);

            if (hasNeutroCalc)
                NeutroCalc = new AdjustValueDef(nameL1.Replace("_L1", "_LN_Calc"), 0, min, max, average, tolerance, unidad);

            if (hasFugas)
                Fugas = new AdjustValueDef(nameL1.Replace("_L1", "_LK"), 0, min, max, average, tolerance, unidad);
        }

        public TriAdjustValueDef(string nameL1, double min, double max, double average, double tolerance, double varianza, ParamUnidad unidad = ParamUnidad.Puntos, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            if (nameL1.Contains("_L12"))
            {
                L1 = new AdjustValueDef(nameL1, 0, min, max, average, tolerance, varianza, unidad);
                L2 = new AdjustValueDef(nameL1.Replace("_L12", "_L23"), 0, min, max, average, tolerance, varianza, unidad);
                L3 = new AdjustValueDef(nameL1.Replace("_L12", "_L31"), 0, min, max, average, tolerance, varianza, unidad);
            }
            else
            {
                L1 = new AdjustValueDef(nameL1, 0, min, max, average, tolerance, varianza, unidad);
                L2 = new AdjustValueDef(nameL1.Replace("_L1", "_L2"), 0, min, max, average, tolerance, varianza, unidad);
                L3 = new AdjustValueDef(nameL1.Replace("_L1", "_L3"), 0, min, max, average, tolerance, varianza, unidad);
            }

            if (hasNeutro)
                Neutro = new AdjustValueDef(nameL1.Replace("_L1", "_LN"), 0, min, max, average, tolerance, varianza, unidad);

            if (hasNeutroCalc)
                NeutroCalc = new AdjustValueDef(nameL1.Replace("_L1", "_LN_Calc"), 0, min, max, average, tolerance, varianza,  unidad);

            if (hasFugas)
                Fugas = new AdjustValueDef(nameL1.Replace("_L1", "_LK"), 0, min, max, average, tolerance, varianza, unidad);
        }

        public TriAdjustValueDef(string nameL1, string nameL2, string nameL3, double value, double min, double max, double average, double tolerance, ParamUnidad unidad = ParamUnidad.Puntos)
        {
            L1 = new AdjustValueDef(nameL1, value, min, max, average, tolerance, unidad);
            L2 = new AdjustValueDef(nameL2, value, min, max, average, tolerance, unidad);
            L3 = new AdjustValueDef(nameL3, value, min, max, average, tolerance, unidad);
        }

        //*********************************************************************************************

        public TriAdjustValueDef(TestModeParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            CrealteAdjustOtherLines(new AdjustValueDef(paramater, unidad, Min, Max, MaxVarianza), hasNeutro, hasNeutroCalc, hasFugas);
        }

        public TriAdjustValueDef(TestModeParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            CrealteAdjustOtherLines(new AdjustValueDef(paramater, unidad, isMaxMin,  Avg, Tol, MaxVarianza), hasNeutro, hasNeutroCalc, hasFugas); 
        }

        public TriAdjustValueDef(ExtensionParameters paramater, ParamUnidad unidad, double? Min = null, double? Max = null, double? MaxVarianza = null, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            CrealteAdjustOtherLines(new AdjustValueDef(paramater, unidad, Min, Max, MaxVarianza), hasNeutro, hasNeutroCalc, hasFugas);
        }

        public TriAdjustValueDef(ExtensionParameters paramater, ParamUnidad unidad, bool isMaxMin, double? Avg = null, double? Tol = null, double? MaxVarianza = null, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            CrealteAdjustOtherLines(new AdjustValueDef(paramater, unidad, isMaxMin, Avg, Tol, MaxVarianza), hasNeutro, hasNeutroCalc, hasFugas);
        }

        //*********************************************************************************************

        public bool HasMinMaxError()
        {

            var l1Valid = L1.IsValid();
            var l2Valid = L2.IsValid();
            var l3Valid = L3.IsValid();
            var NeutroValid = (Neutro != null && !Neutro.IsValid());
            var NeutroCalcValid = (NeutroCalc != null && !NeutroCalc.IsValid());
            var FugasValid = (Fugas != null && !Fugas.IsValid());

            return !l1Valid || !l2Valid || !l3Valid || NeutroValid || NeutroCalcValid || FugasValid;
        }

        public bool HasAverageError()
        {
            var l1Valid = L1.IsValidAverageError();
            var l2Valid = L2.IsValidAverageError();
            var l3Valid = L3.IsValidAverageError();
            var NeutroValid = (Neutro != null && !Neutro.IsValidAverageError());
            var NeutroCalcValid = (NeutroCalc != null && !NeutroCalc.IsValidAverageError());
            var FugasValid = (Fugas != null && !Fugas.IsValidAverageError());

            return !l1Valid || !l2Valid || !l3Valid || NeutroValid || NeutroCalcValid || FugasValid;
        }

        public List<AdjustValueDef> GetMinMaxError()
        {
            var result = new List<AdjustValueDef>();

            if (!L1.IsValid())
                result.Add(L1);

            if (!L2.IsValid())
                result.Add(L2);
            
            if (!L3.IsValid())
                result.Add(L3);

            if (Neutro != null && !Neutro.IsValid())
                result.Add(Neutro);

            if (NeutroCalc != null && !NeutroCalc.IsValid())
                result.Add(NeutroCalc);

            if (Fugas != null && !Fugas.IsValid())
                result.Add(Fugas);

            return result;
        }

        public List<AdjustValueDef> GetAverageError()
        {
            var result = new List<AdjustValueDef>();

            if (!L1.IsValidAverageError())
                result.Add(L1);

            if (!L2.IsValidAverageError())
                result.Add(L2);

            if (!L3.IsValidAverageError())
                result.Add(L3);

            if (Neutro != null && !Neutro.IsValidAverageError())
                result.Add(Neutro);

            if (NeutroCalc != null && !NeutroCalc.IsValidAverageError())
                result.Add(NeutroCalc);

            if (Fugas != null && !Fugas.IsValidAverageError())
                result.Add(Fugas);

            return result;
        }

        public double[] AverageToArray()
        {
            var average = new double[]
            {
             L1.Average, L2.Average, L3.Average
            };
            return average; 
        }

        public List<string> NamesToList()
        {
            var names = new List<string>()
            {          
              L1.Name, L2.Name, L3.Name
            };

            if (Neutro != null)
                names.Add(Neutro.Name);

            if (NeutroCalc != null)
                names.Add(NeutroCalc.Name);

            if (Fugas != null)
                names.Add(Fugas.Name);

            return names;
        }

        public List<AdjustValueDef> GetListAdjusValueDef()
        {
            var result = new List<AdjustValueDef>();

            if (L1!= null)
                result.Add(L1);

            if (L2 != null)
                result.Add(L2);

            if (L3 != null)
                result.Add(L3);

            if (Neutro != null)
                result.Add(Neutro);

            if (NeutroCalc != null)
                result.Add(NeutroCalc);

            if (Fugas != null)
                result.Add(Fugas);

            return result;
        }


        //*********************************************************************************************

        private void CrealteAdjustOtherLines(AdjustValueDef value, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            L1 = (AdjustValueDef)value.Clone();
            L2 = new AdjustValueDef(L1.Name.Replace("_L1", "_L2"), 0, L1.Min, L1.Max, L1.Average, L1.Tolerance, L1.Varianza, L1.Unidad);
            L3 = new AdjustValueDef(L1.Name.Replace("_L1", "_L3"), 0, L1.Min, L1.Max, L1.Average, L1.Tolerance, L1.Varianza, L1.Unidad);

            if (hasNeutro)
                Neutro = new AdjustValueDef(L1.Name.Replace("_L1", "_LN"), 0, L1.Min, L1.Max, L1.Average, L1.Tolerance, L1.Varianza, L1.Unidad);

            if (hasNeutroCalc)
                NeutroCalc = new AdjustValueDef(L1.Name.Replace("_L1", "_LN_Calc"), 0, L1.Min, L1.Max, L1.Average, L1.Tolerance, L1.Varianza, L1.Unidad);

            if (hasFugas)
                Fugas = new AdjustValueDef(L1.Name.Replace("_L1", "_LK"), 0, L1.Min, L1.Max, L1.Average, L1.Tolerance, L1.Varianza, L1.Unidad);
        }
    }
}
