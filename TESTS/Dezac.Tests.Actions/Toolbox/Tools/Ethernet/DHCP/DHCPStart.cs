﻿using System.ComponentModel;
using System.Drawing;
using WinDHCP.Library;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DHCPStartDescription))]
    public class DHCPStart : ActionBase
    {
        [DefaultValue("DHCPServer")]
        [Category("3. Instance")]
        [Description("Nombre de la instancia del objeto DHCP Server")]
        public string DHCPServerName { get; set; }

        public override void Execute()
        {
            var DHCPserver = GetSharedVariable(DHCPServerName) as DhcpServer;
            if (DHCPserver != null)
                DHCPserver.Start();
            else
                Error().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE("DHCPServerName no encontrado").Throw();
            
        }
    }

    public class DHCPStartDescription : ActionDescription
    {
        public override string Name { get { return "DHCPStart"; } }
        public override string Description { get { return "Inicia el servicio de un Servidor DHCP"; } }
        public override string Category { get { return "Tools/Ethernet/DHCP"; } }
        public override Image Icon { get { return Properties.Resources.DHCP; } }
    }
}
