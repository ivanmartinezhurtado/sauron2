﻿using Dezac.Core.Utility;
using Dezac.Device.Reactive;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Reactive
{
    [TestVersion(1.04)]
    public class COMPUTERMAXTest : TestBase
    {
        private byte OUTPUT_PILOTO_TEST_EQUIPO_1 = 0;
        private byte OUTPUT_PILOTO_TEST_EQUIPO_2 = 2;
        private byte OUTPUT_PUNTAS_EQUIPO_1 = 5;
        private byte OUTPUT_PUNTAS_EQUIPO_2 = 6;
        private byte OUTPUT_PUNTAS_COMUNICACIONES_EQUIPO_1 = 7;
        private byte OUTPUT_PUNTAS_COMUNICACIONES_EQUIPO_2 = 8;
        private byte OUTPUT_TECLA_ARRIBA = 9;
        private byte OUTPUT_TECLA_ABAJO = 10;
        private byte OUTPUT_TECLA_SETUP = 11;
        private byte OUTPUT_SELECCION_RELES_EQUIPO_1_EQUIPO_2 = 20;
        private byte OUTPUT_ACTIVACION_MINI_CONVERSORES = 15;
        private byte OUTPUT_SELECCION_EQUIPOS_A_VERIFICAR = 19;

        private byte INPUT_PRESENCIA_EQUIPO_1 = 21;
        private byte INPUT_PRESENCIA_EQUIPO_2 = 3;

        private byte INPUT_RELE_1 = 9;
        private byte INPUT_RELE_2 = 10;
        private byte INPUT_RELE_3 = 11;
        private byte INPUT_RELE_4 = 12;
        private byte INPUT_RELE_5 = 13;
        private byte INPUT_RELE_6 = 14;
        private byte INPUT_RELE_7 = 15;
        private byte INPUT_RELE_8 = 16;
        private byte INPUT_RELE_9 = 17;
        private byte INPUT_RELE_10 = 18;
        private byte INPUT_RELE_11 = 19;
        private byte INPUT_RELE_12 = 20;
        private byte INPUT_LEDS_1 = 22;
        private byte INPUT_LEDS_2 = 23;

        private byte COMPORT_MINI_EQUIPO_1 = 7;
        private byte COMPORT_MINI_EQUIPO_2 = 11;
        private byte COMPORT_EQUIPO_1 = 10;
        private byte COMPORT_EQUIPO_2 = 9;

        private byte NUMERO_PERIFERICO_CVM_MINI_EQUIPO_1 = 1;
        private byte NUMERO_PERIFERICO_CVM_MINI_EQUIPO_2 = 2;

        private bool esSegundoEquipo = false;

        private class DeviceInfo
        {
            public byte puertoComunicaciones;
            public byte presencia;
            public byte puntas;
            public byte puntasComunicaciones;
            public byte pilotoTest;
            public byte numeroPerifericoMiniUtillaje;
            public byte entradaLeds;
        }
        private DeviceInfo deviceInfo = new DeviceInfo();

        private Tower3 tower;
        private COMPUTERMAX cmax;

        private static readonly object syncObject = new object();

        public void TestInitialization()
        {
            esSegundoEquipo = NumInstance > 1;
            if (esSegundoEquipo)
            {
                deviceInfo.numeroPerifericoMiniUtillaje = NUMERO_PERIFERICO_CVM_MINI_EQUIPO_2;
                deviceInfo.pilotoTest = OUTPUT_PILOTO_TEST_EQUIPO_2;
                deviceInfo.presencia = INPUT_PRESENCIA_EQUIPO_2;
                deviceInfo.puertoComunicaciones = COMPORT_EQUIPO_2;
                deviceInfo.puntas = OUTPUT_PUNTAS_EQUIPO_2;
                deviceInfo.puntasComunicaciones = OUTPUT_PUNTAS_COMUNICACIONES_EQUIPO_2;
                deviceInfo.entradaLeds = INPUT_LEDS_2;
            }
            else
            {
                deviceInfo.numeroPerifericoMiniUtillaje = NUMERO_PERIFERICO_CVM_MINI_EQUIPO_1;
                deviceInfo.pilotoTest = OUTPUT_PILOTO_TEST_EQUIPO_1;
                deviceInfo.presencia = INPUT_PRESENCIA_EQUIPO_1;
                deviceInfo.puertoComunicaciones = COMPORT_EQUIPO_1;
                deviceInfo.puntas = OUTPUT_PUNTAS_EQUIPO_1;
                deviceInfo.puntasComunicaciones = OUTPUT_PUNTAS_COMUNICACIONES_EQUIPO_1;
                deviceInfo.entradaLeds = INPUT_LEDS_1;
            }

            cmax = AddInstanceVar(new COMPUTERMAX(deviceInfo.puertoComunicaciones), "UUT");

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.Active24VDC();
            WaitAllTestsAndRunOnlyOnce("MTE_TEST_OFF", () =>
             {
                 tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
             });
            tower.ActiveElectroValvule();
            tower.ActiveVoltageCircuit();
            tower.IO.DO.OnWait(400, OUTPUT_ACTIVACION_MINI_CONVERSORES);
            tower.IO.DO.Off(deviceInfo.pilotoTest);

            Delay(500, "Espera activación reles");
        }

        public void TestSupplyConsumption()
        {
            using (var cvmMiniUtillaje = new CVMMINITower(!esSegundoEquipo ? COMPORT_MINI_EQUIPO_1 : COMPORT_MINI_EQUIPO_2))
            {
                cvmMiniUtillaje.Modbus.PerifericNumber = deviceInfo.numeroPerifericoMiniUtillaje;

                var vMin = Consignas.GetDouble(Params.V.L23.ConCarga.Vmin.Name, 210, ParamUnidad.V);
                var vMax = Consignas.GetDouble(Params.V.L23.ConCarga.Vmax.Name, 260, ParamUnidad.V);
                var vNom = Consignas.GetDouble(Params.V.L23.ConCarga.Vnom.Name, 230, ParamUnidad.V);

                var minimumSupplyVoltage = new TriLineValue() { L1 = 0, L2 = vMin, L3 = vMin };
                var maximumSupplyVoltage = new TriLineValue() { L1 = 0, L2 = vMax, L3 = vMax };
                var nominalSupplyVoltage = new TriLineValue() { L1 = 0, L2 = vNom, L3 = vNom };

                WaitAllTestsAndRunOnlyOnce("MTE_TEST_SUPPLY_MIN", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(minimumSupplyVoltage, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }); });

                cmax.WriteFlagTest();

                cmax.WriteAllOutputs(true);

                Delay(2000, "Esperando estabilización del consumo");

                TestConsumo(Params.KVAR.Null.ConCarga.Vmin.Name, Params.KVAR.Null.ConCarga.Vmin.Min(), Params.KVAR.Null.ConCarga.Vmin.Max(),
                () =>
                {
                    var vars = cvmMiniUtillaje.ReadAllVariables();
                    return vars.Trifasicas.TensionLineaL2L3 * vars.Phases.L3.Corriente / 1000.0D;
                });

                cmax.WriteAllOutputs(false);

                WaitAllTestsAndRunOnlyOnce("MTE_TEST_SUPPLY_MAX", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(maximumSupplyVoltage, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }); });

                Delay(2000, "Esperando estabilización del consumo");

                cmax.WriteFlagTest();

                TestConsumo(Params.KVAR.Null.EnVacio.Vmax.Name, Params.KVAR.Null.EnVacio.Vmax.Min(), Params.KVAR.Null.EnVacio.Vmax.Max(),
                () =>
                {
                    var vars = cvmMiniUtillaje.ReadAllVariables();

                    return vars.Trifasicas.TensionLineaL2L3  * vars.Phases.L3.Corriente / 1000.0D;
                });

                WaitAllTestsAndRunOnlyOnce("MTE_TEST_SUPPLY_NOM", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(nominalSupplyVoltage, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }); });

                Delay(2000, "Esperando estabilización del consumo");

                TestConsumo(Params.KVAR.Null.EnVacio.Vnom.Name, Params.KVAR.Null.EnVacio.Vnom.Min(), Params.KVAR.Null.EnVacio.Vnom.Max(),
                () =>
                {
                    var vars = cvmMiniUtillaje.ReadAllVariables();

                    return vars.Trifasicas.TensionLineaL2L3 * vars.Phases.L3.Corriente / 1000.0D;
                });
            }
        }

        public void TestCommunications()
        {
            cmax.WriteFlagTest();
            var identification = new COMPUTERMAX.IdentificationParameters();
            identification.serialNumber = 0;
            identification.frameNumber = (uint)(TestInfo.NumBastidor == null ? 0 : TestInfo.NumBastidor);
            identification.errorCode = 0x9999;
            cmax.WriteIdentificationParameters(identification);
        }

        public void TestSetupDefault()
        {
            string estadoConfiguracion = "Error. Configuración del equipo inválida. Características inválidas:";

            var modelo = VectorHardware.GetString("MODELO", "MAX12", ParamUnidad.SinUnidad);
            cmax.ModeloHardware = modelo;
            if (cmax.flagReles)   
                estadoConfiguracion += "    Configuración de reles del equipo \r";
            
            var tensionAlimentacion = VectorHardware.GetDouble("TENSION_ALIMENTACION", 110, ParamUnidad.V);
            cmax.VoltageSupplyHardware = (int)tensionAlimentacion;

            if (cmax.flagAlimentacion)
                estadoConfiguracion += "    Configuración de tensión de alimentación del equipo \r";
            
            var versionHardware = Identificacion.VERSION_FIRMWARE;
            cmax.VersionHardware = versionHardware;

            if (cmax.flagReles || cmax.flagAlimentacion)
                throw new Exception(estadoConfiguracion);

            cmax.WriteFlagTest();

            COMPUTERMAX.SetupInfo setupInfo = new COMPUTERMAX.SetupInfo()
            {
                tipoCoseno = (byte)Configuracion.GetDouble("TIPO_COSENO", 1, ParamUnidad.SinUnidad),
                cosenoFiObjetivo = (byte)Configuracion.GetDouble("COSENO_FI_OBJETIVO", 100, ParamUnidad.SinUnidad),
                CK = (byte)Configuracion.GetDouble("CK", 100, ParamUnidad.SinUnidad),
                programaManiobra = (byte)Configuracion.GetDouble("PROGRAMA_MANIOBRA", 0, ParamUnidad.SinUnidad),
                tiempoRetraso = (ushort)Configuracion.GetDouble("TIEMPO_RETRASO", 10, ParamUnidad.SinUnidad),
                paso = cmax.SetupInfoPaso,
                cuadrantes = (byte)Configuracion.GetDouble("CUADRANTES", 0, ParamUnidad.SinUnidad),
                fase = (byte)Configuracion.GetDouble("FASE", 1, ParamUnidad.SinUnidad),
                escalaPrimarioCorriente = (byte)Configuracion.GetDouble("ESCALA_PRIMARIO_CORRIENTE", 1, ParamUnidad.SinUnidad),
                primarioCorriente = (ushort)Configuracion.GetDouble("PRIMARIO_CORRIENTE", 5, ParamUnidad.SinUnidad),
                hardware = cmax.SetupInfoHardware,
            };

            Resultado.Set("SETUP_INFO_HARDWARE", setupInfo.hardware, ParamUnidad.SinUnidad);
            Resultado.Set("SETUP_INFO_PASO", setupInfo.paso, ParamUnidad.SinUnidad);

            cmax.WriteSetupInfoVariables(setupInfo);
                        
            var isComputerFast = Configuracion.GetString("MODELO_FAST", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI";
            if (isComputerFast)
            {
                COMPUTERMAX.SetupLimitsInfoFast setupLimitsInfo = new COMPUTERMAX.SetupLimitsInfoFast()
                {
                    cosenoInductivo = (byte)Configuracion.GetDouble("COSENO_FI_INDUCTIVO", 85, ParamUnidad.SinUnidad),
                    cosenoCapacitivo = (byte)Configuracion.GetDouble("COSENO_FI_CAPACITIVO", 95, ParamUnidad.SinUnidad),
                    CKMaximo = (byte)Configuracion.GetDouble("CK_MAXIMO", 100, ParamUnidad.SinUnidad),
                    CKMinimo = (byte)Configuracion.GetDouble("CK_MINIMO", 2, ParamUnidad.SinUnidad),
                    tiempoRetrasoMaximo = (ushort)Configuracion.GetDouble("TIEMPO_RETRASO_MAXIMO", 999, ParamUnidad.SinUnidad),
                    tiempoRetrasoMinimo = (ushort)Configuracion.GetDouble("TIEMPO_RETRASO_MINIMO", 4, ParamUnidad.SinUnidad),
                    pasoMaximo = cmax.SetupInfoPaso,
                    intensidadMinima = (byte)Configuracion.GetDouble("INTENSIDAD_MINIMA", 8, ParamUnidad.SinUnidad),
                    factorReconexion = (byte)Configuracion.GetDouble("FACTOR_RECONEXION", 5, ParamUnidad.SinUnidad),
                    factorCalibracionCorriente = (ushort)Configuracion.GetDouble("FACTOR_CALIBRACION_CORRIENTE", 283, ParamUnidad.SinUnidad),
                    anguloDesfase = (ushort)Configuracion.GetDouble("ANGULO_DESFASE", 0, ParamUnidad.SinUnidad),
                    calibracionMedidaI = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_I", 17642, ParamUnidad.SinUnidad),
                    calibracionMedidaV = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_V", 13810, ParamUnidad.SinUnidad),
                    valorMaximoI = (uint)Configuracion.GetDouble("VALOR_MAXIMO_I", 0, ParamUnidad.SinUnidad),
                    valorMaximoV = (ushort)Configuracion.GetDouble("VALOR_MAXIMO_V", 0, ParamUnidad.SinUnidad)
                };

                cmax.WriteSetupLimitFastVariables(setupLimitsInfo);
            }
            else
            {
                COMPUTERMAX.SetupLimitsInfo setupLimitsInfo = new COMPUTERMAX.SetupLimitsInfo()
                {
                    cosenoInductivo = (byte)Configuracion.GetDouble("COSENO_FI_INDUCTIVO", 85, ParamUnidad.SinUnidad),
                    cosenoCapacitivo = (byte)Configuracion.GetDouble("COSENO_FI_CAPACITIVO", 95, ParamUnidad.SinUnidad),
                    CKMaximo = (byte)Configuracion.GetDouble("CK_MAXIMO", 100, ParamUnidad.SinUnidad),
                    CKMinimo = (byte)Configuracion.GetDouble("CK_MINIMO", 2, ParamUnidad.SinUnidad),
                    tiempoRetrasoMaximo = (ushort)Configuracion.GetDouble("TIEMPO_RETRASO_MAXIMO", 999, ParamUnidad.SinUnidad),
                    tiempoRetrasoMinimo = (ushort)Configuracion.GetDouble("TIEMPO_RETRASO_MINIMO", 4, ParamUnidad.SinUnidad),
                    pasoMaximo = cmax.SetupInfoPaso,
                    intensidadMinima = (byte)Configuracion.GetDouble("INTENSIDAD_MINIMA", 8, ParamUnidad.SinUnidad),
                    factorReconexion = (byte)Configuracion.GetDouble("FACTOR_RECONEXION", 5, ParamUnidad.SinUnidad),
                    factorCalibracionCorriente = (ushort)Configuracion.GetDouble("FACTOR_CALIBRACION_CORRIENTE", 283, ParamUnidad.SinUnidad),
                    anguloDesfase = (byte)Configuracion.GetDouble("ANGULO_DESFASE", 0, ParamUnidad.SinUnidad),
                    calibracionMedidaI = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_I", 17642, ParamUnidad.SinUnidad),
                    calibracionMedidaV = (ushort)Configuracion.GetDouble("CALIBRACION_MEDIDA_V", 13810, ParamUnidad.SinUnidad),
                    valorMaximoI = (uint)Configuracion.GetDouble("VALOR_MAXIMO_I", 0, ParamUnidad.SinUnidad),
                    valorMaximoV = (ushort)Configuracion.GetDouble("VALOR_MAXIMO_V", 0, ParamUnidad.SinUnidad)
                };
                
                cmax.WriteSetupLimitVariables(setupLimitsInfo);
            }

            cmax.WriteMaximums(new COMPUTERMAX.Maximums(0, 0));
            
            WaitAllTestsAndRunOnlyOnce("TestSetupDefault_MTE_OFF", () => { tower.PowerSourceIII.ApplyOffAndWaitStabilisation(); });

            Delay(2000, "Espera apagado equipo");

            var vNom = Consignas.GetDouble(Params.V.L23.ConCarga.Vnom.Name, 230, ParamUnidad.V);

            TriLineValue nominalSupplyVoltage = new TriLineValue() { L1 = 0, L2 = vNom, L3 = vNom };

            WaitAllTestsAndRunOnlyOnce("TestSetupDefault_MTE_ON", () => { tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(nominalSupplyVoltage, TriLineValue.Create(0),0, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }); });
        }

        public void TestLeds()
        {
            cmax.WriteFlagTest();

            cmax.WriteLeds(true, COMPUTERMAX.Leds.RUN);
            cmax.WriteLeds(true, COMPUTERMAX.Leds.X10_IP);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[deviceInfo.entradaLeds];
            }, string.Format("Error. No se detectan la activación de los leds del equipo {0}.", esSegundoEquipo ? 2 : 1), 10, 200);

            cmax.WriteLeds(false, COMPUTERMAX.Leds.RUN);
            cmax.WriteLeds(false, COMPUTERMAX.Leds.X10_IP);

            SamplerWithCancel((p) =>
            {
                return !tower.IO.DI[deviceInfo.entradaLeds];
            }, string.Format("Error. No se detectan el apagado de los leds del equipo {0}.", esSegundoEquipo ? 2 : 1), 10, 200);

        }

        public void TestRelays()
        {
            var RelaysListImpar = new Dictionary<int, COMPUTERMAX.Relays>();
            RelaysListImpar.Add(INPUT_RELE_1, COMPUTERMAX.Relays.OUTPUT_1);
            RelaysListImpar.Add(INPUT_RELE_3, COMPUTERMAX.Relays.OUTPUT_3);

            var RelaysListPar = new Dictionary<int, COMPUTERMAX.Relays>();
            RelaysListPar.Add(INPUT_RELE_2, COMPUTERMAX.Relays.OUTPUT_2);
            RelaysListPar.Add(INPUT_RELE_4, COMPUTERMAX.Relays.OUTPUT_4);

            switch (cmax.SetupInfoNumReles)
            {
                case 6:
                    RelaysListImpar.Add(INPUT_RELE_5, COMPUTERMAX.Relays.OUTPUT_5);
                    RelaysListPar.Add(INPUT_RELE_6, COMPUTERMAX.Relays.OUTPUT_6);
                    break;
                case 8:
                    RelaysListImpar.Add(INPUT_RELE_5, COMPUTERMAX.Relays.OUTPUT_5);
                    RelaysListPar.Add(INPUT_RELE_6, COMPUTERMAX.Relays.OUTPUT_6);
                    RelaysListImpar.Add(INPUT_RELE_7, COMPUTERMAX.Relays.OUTPUT_7);
                    RelaysListPar.Add(INPUT_RELE_8, COMPUTERMAX.Relays.OUTPUT_8);
                    break;
                case 12:
                    RelaysListImpar.Add(INPUT_RELE_5, COMPUTERMAX.Relays.OUTPUT_5);
                    RelaysListPar.Add(INPUT_RELE_6, COMPUTERMAX.Relays.OUTPUT_6);
                    RelaysListImpar.Add(INPUT_RELE_7, COMPUTERMAX.Relays.OUTPUT_7);
                    RelaysListPar.Add(INPUT_RELE_8, COMPUTERMAX.Relays.OUTPUT_8);
                    RelaysListImpar.Add(INPUT_RELE_9, COMPUTERMAX.Relays.OUTPUT_9);
                    RelaysListPar.Add(INPUT_RELE_10, COMPUTERMAX.Relays.OUTPUT_10);
                    RelaysListImpar.Add(INPUT_RELE_11, COMPUTERMAX.Relays.OUTPUT_11);
                    RelaysListPar.Add(INPUT_RELE_12, COMPUTERMAX.Relays.OUTPUT_12);
                    break;
            }

            // OJO: S'ha de canviar a nivell de Hardware
            /*if (tower.IO.DI[INPUT_PRESENCIA_EQUIPO_2])
                tower.IO.DO.On(OUTPUT_SELECCION_RELES_EQUIPO_1_EQUIPO_2);
            else
                tower.IO.DO.Off(OUTPUT_SELECCION_RELES_EQUIPO_1_EQUIPO_2);
             * */

            lock (syncObject)
            {
                if (esSegundoEquipo)
                    tower.IO.DO.On(OUTPUT_SELECCION_RELES_EQUIPO_1_EQUIPO_2);
                else
                    tower.IO.DO.Off(OUTPUT_SELECCION_RELES_EQUIPO_1_EQUIPO_2);

                cmax.WriteFlagTest();

                Sampler.Run(5, 200,
                (step) =>
                {
                    cmax.WriteAllOutputs(true);
                    Delay(200, "Esperando estabilización de los relés...");
                    cmax.WriteAllOutputs(false);
                });

                SamplerWithCancel((p) =>
                {
                    cmax.WriteOutputs(true, RelaysListImpar.Values.ToArray());

                    return tower.IO.DI.ReadAllOn(RelaysListImpar.Keys.ToArray()) && tower.IO.DI.ReadAllOff(RelaysListPar.Keys.ToArray());
                }, "Error. Error en la detección del encendido de los relés impares.");

                SamplerWithCancel((p) =>
                {
                    cmax.WriteOutputs(true, RelaysListPar.Values.ToArray());

                    return tower.IO.DI.ReadAllOff(RelaysListImpar.Keys.ToArray()) && tower.IO.DI.ReadAllOn(RelaysListPar.Keys.ToArray());
                }, "Error. Error en la detección del encendido de los relés pares.");

                SamplerWithCancel((p) =>
                {
                    cmax.WriteOutputs(false, RelaysListPar.Values.ToArray());

                    return tower.IO.DI.ReadAllOff(RelaysListImpar.Keys.ToArray()) && tower.IO.DI.ReadAllOff(RelaysListPar.Keys.ToArray());
                }, "Error. Error en la detección del apagado de los relés");
            }
        }

        public void TestKeyboard()
        {
            var keyboard = new List<KeyValuePair<byte, COMPUTERMAX.Keyboard>>();
            keyboard.Add(new KeyValuePair<byte, COMPUTERMAX.Keyboard>(OUTPUT_TECLA_ABAJO, COMPUTERMAX.Keyboard.KEY_DOWN));
            keyboard.Add(new KeyValuePair<byte, COMPUTERMAX.Keyboard>(OUTPUT_TECLA_ARRIBA, COMPUTERMAX.Keyboard.KEY_UP));
            keyboard.Add(new KeyValuePair<byte, COMPUTERMAX.Keyboard>(OUTPUT_TECLA_SETUP, COMPUTERMAX.Keyboard.KEY_SETUP));

            WaitAllTests("TestKeyboard_Init");

            // Desactivamos paralelismo
            lock (syncObject)
            {
                tower.IO.DO.Off(OUTPUT_TECLA_ABAJO, OUTPUT_TECLA_ARRIBA, OUTPUT_TECLA_SETUP);

                cmax.WriteFlagTest();

                SamplerWithCancel(
                (p) =>
                {
                    return cmax.ReadKeyboard() == COMPUTERMAX.Keyboard.NO_KEYS;
                }, "Error. Se detectan teclas activadas en la desactivacion de las teclas", 5, 50);

                foreach (KeyValuePair<byte, COMPUTERMAX.Keyboard> currentKey in keyboard)
                {
                    tower.IO.DO.On(currentKey.Key);
                    SamplerWithCancel((p) =>
                    {
                        return cmax.ReadKeyboard() == currentKey.Value;
                    }, "Error. No se detecta la tecla " + currentKey.Value.ToString() + " o hay mas teclas activas", 5, 50);
                    tower.IO.DO.Off(currentKey.Key);
                }

                SamplerWithCancel(
                (p) =>
                {
                    return cmax.ReadKeyboard() == COMPUTERMAX.Keyboard.NO_KEYS;
                }, "Error. Se detectan teclas activadas en la desactivacion de las teclas", 5, 50);
            }

            WaitAllTests("TestKeyboard_End");
        }

        public void TestAdjust()
        {
            if (tower.IO.DI[INPUT_PRESENCIA_EQUIPO_2] && IsInstanceRunning(2))
                tower.IO.DO.Off(OUTPUT_SELECCION_EQUIPOS_A_VERIFICAR);
            else
                tower.IO.DO.On(OUTPUT_SELECCION_EQUIPOS_A_VERIFICAR);

            var voltageDatabase = Consignas.GetDouble(Params.V.L23.Ajuste.Name, 230.94D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A);
            var anguloDesfaseDatabase = Consignas.GetDouble(Params.PHASE.L12.Ajuste.Name, 45, ParamUnidad.Grados);
            var voltageOffset = Consignas.GetDouble(Params.V.Null.TestPoint("OFFSET_COMM").Name, 0, ParamUnidad.V);

            TriLineValue voltage = new TriLineValue { L1 = 0, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = currentDatabase, L2 = 0, L3 = 0 };

            List<AdjustValueDef> defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.POINTS.Null.TestPoint("SUM_V").Name, 0, Params.POINTS.Null.TestPoint("SUM_V").Min(), Params.POINTS.Null.TestPoint("SUM_V").Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.POINTS.Null.TestPoint("SUM_I").Name, 0, Params.POINTS.Null.TestPoint("SUM_I").Min(), Params.POINTS.Null.TestPoint("SUM_I").Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.POINTS.Null.TestPoint("SUM_KVAR").Name, 0, Params.POINTS.Null.TestPoint("SUM_KVAR").Min(), Params.POINTS.Null.TestPoint("SUM_KVAR").Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.POINTS.Null.TestPoint("SUM_KW").Name, 0, Params.POINTS.Null.TestPoint("SUM_KW").Min(), Params.POINTS.Null.TestPoint("SUM_KW").Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.POINTS.Null.TestPoint("NMOS").Name, 0, Params.POINTS.Null.TestPoint("NMOS").Min(), Params.POINTS.Null.TestPoint("NMOS").Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new AdjustValueDef(Params.V.Null.Offset.Name, 0, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.Null.Offset.Name, 0, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.ANGLE_GAP.Null.Ajuste.Name, 0, Params.ANGLE_GAP.Null.Ajuste.Min(), Params.ANGLE_GAP.Null.Ajuste.Max(), 0, 0, ParamUnidad.Grados));
            defs.Add(new AdjustValueDef(Params.FREQ.Null.Ajuste.Name, 0, Params.FREQ.Null.Ajuste.Min(), Params.FREQ.Null.Ajuste.Max(), 0, 0, ParamUnidad.Hz));
            defs.Add(new AdjustValueDef(Params.V.Null.Ajuste.Name, 0, Params.V.Null.Ajuste.Min(), Params.V.Null.Ajuste.Max(), 0, 0, ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.Null.Ajuste.Name, 0, Params.I.Null.Ajuste.Min(), Params.I.Null.Ajuste.Max(), 0, 0, ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.THD_I.Null.Ajuste.Name, 0, Params.THD_I.Null.Ajuste.Min(), Params.THD_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.PorCentage));

            tower.PowerSourceIII.Tolerance = Consignas.GetDouble("TOLERANCIA_ESTABILIZACION_MTE", 0.01, ParamUnidad.PorCentage);

            WaitAllTestsAndRunOnlyOnce("TestAdjust_MTE_Init", () => tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, anguloDesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }));

            cmax.WriteFlagTest();

            var isComputerFast = Configuracion.GetString("MODELO_FAST", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI";

                var voltageReference = tower.PowerSourceIII.ReadVoltage().L2 * Math.Sqrt(3);
                Resultado.Set(Params.V.Null.Ajuste.TestPoint("PATRON").Name, voltageReference, ParamUnidad.V);

                var currentReference = tower.PowerSourceIII.ReadCurrent().L1;
                Resultado.Set(Params.I.Null.Ajuste.TestPoint("PATRON").Name, currentReference, ParamUnidad.A);

            if (isComputerFast)
            {
                var result = cmax.CalculateAdjustFactorsFast(2, 10, 15, 1100, voltageReference, currentReference, voltageOffset, () =>
                {
                    var allVariables = cmax.ReadMeasureAdjustVariables();
                    return new double[] 
                    { 
                        allVariables.Measures.SumTV, allVariables.Measures.SumTI, 
                        allVariables.Measures.SumTKVar, allVariables.Measures.SumTKW, 
                        allVariables.Measures.Nmos, allVariables.Measures.OffsetTension, 
                        allVariables.Measures.OffsetCorriente, allVariables.Measures.CosenoFi, 
                        allVariables.Measures.Hertz, allVariables.Voltage, 
                        allVariables.Current, allVariables.CurrentTHD
                    };
                },
                (adjustValues) =>
                {
                    return true;
                },
                (samples) =>
                {
                    defs[0].Value = samples.Measures.SumTV;
                    defs[1].Value = samples.Measures.SumTI;
                    defs[2].Value = samples.Measures.SumTKVar;
                    defs[3].Value = samples.Measures.SumTKW;
                    defs[4].Value = samples.Measures.Nmos;
                    defs[5].Value = samples.Measures.OffsetTension;
                    defs[6].Value = samples.Measures.OffsetCorriente;
                    defs[7].Value = samples.Measures.CosenoFi * Math.Pow(10, -3);
                    defs[8].Value = samples.Measures.Hertz * Math.Pow(10, -1);
                    defs[9].Value = samples.Voltage * Math.Pow(10, -5);
                    defs[10].Value = samples.Current * Math.Pow(10, -8);
                    defs[11].Value = samples.CurrentTHD;
                    return !HasError(defs, false).Any();
                });

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                if (HasError(defs, false).Any())
                    throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(defs, false).FirstOrDefault().Name));

                cmax.WriteSetupLimitFastVariables(result.Item2);

                SetVariable("FactoresCalibracion", result.Item2);
            }
            else
            {
                var result = cmax.CalculateAdjustFactors(2, 10, 15, 1100, voltageReference, currentReference, voltageOffset, () =>
                    {
                        var allVariables = cmax.ReadMeasureAdjustVariables();
                        return new double[] 
                    { 
                        allVariables.Measures.SumTV, allVariables.Measures.SumTI, 
                        allVariables.Measures.SumTKVar, allVariables.Measures.SumTKW, 
                        allVariables.Measures.Nmos, allVariables.Measures.OffsetTension, 
                        allVariables.Measures.OffsetCorriente, allVariables.Measures.CosenoFi, 
                        allVariables.Measures.Hertz, allVariables.Voltage, 
                        allVariables.Current, allVariables.CurrentTHD                     
                    };
                    },
                    (adjustValues) =>
                    {
                        return true;
                    },
                    (samples) =>
                    {
                        defs[0].Value = samples.Measures.SumTV;
                        defs[1].Value = samples.Measures.SumTI;
                        defs[2].Value = samples.Measures.SumTKVar;
                        defs[3].Value = samples.Measures.SumTKW;
                        defs[4].Value = samples.Measures.Nmos;
                        defs[5].Value = samples.Measures.OffsetTension;
                        defs[6].Value = samples.Measures.OffsetCorriente;
                        defs[7].Value = samples.Measures.CosenoFi * Math.Pow(10, -3);
                        defs[8].Value = samples.Measures.Hertz * Math.Pow(10, -1);
                        defs[9].Value = samples.Voltage * Math.Pow(10, -5);
                        defs[10].Value = samples.Current * Math.Pow(10, -8);
                        defs[11].Value = samples.CurrentTHD;
                        return !HasError(defs, false).Any();
                    });

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                Resultado.Set(Params.GAIN_I.Null.Name, result.Item2.calibracionMedidaI, ParamUnidad.Puntos);
                Resultado.Set(Params.GAIN_V.Null.Name, result.Item2.calibracionMedidaV, ParamUnidad.Puntos);
                Resultado.Set(Params.GAIN_I.Other("FACTOR").Name, result.Item2.factorCalibracionCorriente, ParamUnidad.Puntos);
                Resultado.Set(Params.GAIN_DESFASE.Null.Name, result.Item2.anguloDesfase, ParamUnidad.Puntos);

                if (HasError(defs, false).Any())
                    throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(defs, false).FirstOrDefault().Name));

                logger.Info(result.Item2);
                cmax.WriteSetupLimitVariables(result.Item2);

                SetVariable("FactoresCalibracion", result.Item2);
            }

            WaitAllTestsAndRunOnlyOnce("TestAdjust_MTE_Off", () => tower.PowerSourceIII.ApplyOffAndWaitStabilisation());
        }

        public void TestVerification()
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.L23.Ajuste.Name, 230.94D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(Params.I.L1.Ajuste.Name, 5, ParamUnidad.A);
            var anguloDesfaseDatabase = Consignas.GetDouble(Params.PHASE.L12.Ajuste.Name, 45, ParamUnidad.Grados);
            var voltageOffset = Consignas.GetDouble(Params.V.Null.TestPoint("OFFSET_COMM").Name, 0, ParamUnidad.V);
            double powerFactor = Math.Cos(anguloDesfaseDatabase * Math.PI / 180);

            TriLineValue voltage = new TriLineValue { L1 = 0, L2 = voltageDatabase, L3 = voltageDatabase };
            TriLineValue current = new TriLineValue { L1 = currentDatabase, L2 = 0, L3 = 0 };

            List<AdjustValueDef> defs = new List<AdjustValueDef>();
            defs.Add(new AdjustValueDef(Params.V.L23.Verificacion.Name, 0, 0, 0, voltage.L2 * Math.Sqrt(3), Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new AdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, current.L1, Params.I.Null.Verificacion.Tol(), ParamUnidad.A));
            defs.Add(new AdjustValueDef(Params.PF.L12.Verificacion.Name, 0, 0, 0, powerFactor, Params.PF.Null.Verificacion.Tol(), ParamUnidad.SinUnidad));

            WaitAllTestsAndRunOnlyOnce("TestVerification_MTE_Init", () => tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, anguloDesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }));

            TestCalibracionBase(defs,
                () =>
                {
                    var voltageReading = cmax.ReadVoltageVariables() * Math.Pow(10, -5) + voltageOffset;
                    var currentReading = cmax.ReadCurrentVariables() * Math.Pow(10, -8);
                    var factorPotencia = cmax.ReadMeasureVariables().CosenoFi * Math.Pow(10, -3);
                    return new double[] { voltageReading , currentReading ,
                    factorPotencia };
                },
                () =>
                {
                    lock (syncObject)
                    {
                        var voltageValue = tower.PowerSourceIII.ReadVoltage().L2;
                        var currentValue = tower.PowerSourceIII.ReadCurrent().L1;
                        return new double[] {
                        voltageValue * Math.Sqrt(3), 
                        currentValue,
                        powerFactor
                    };
                    }
                }, 5, 10, 1000);
        }

        public void TestCustomization()
        {
            cmax.WriteFlagTest();

            COMPUTERMAX.IdentificationParameters identification = new COMPUTERMAX.IdentificationParameters();
            identification.serialNumber = (uint)TestInfo.NumBastidor;
            identification.frameNumber = (uint)TestInfo.NumBastidor;
            identification.errorCode = 0;
            cmax.WriteIdentificationParameters(identification);

            var version = VectorHardware.GetString("VERSION", "09", ParamUnidad.SinUnidad);
            cmax.VersionHardware = version;

            var setupInfo = cmax.ReadSetupInfo();

            setupInfo.hardware = cmax.SetupInfoHardware;

            cmax.WriteSetupInfoVariables(setupInfo);

            var vNom = Consignas.GetDouble(Params.V.L23.ConCarga.Vnom.Name, 230, ParamUnidad.V);

            TriLineValue nominalSupplyVoltage = new TriLineValue() { L1 = 0, L2 = vNom, L3 = vNom };

            WaitAllTestsAndRunOnlyOnce("TestCustomization_MTE_Off", () => {
                tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(nominalSupplyVoltage, 0, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
            });

            var isComputerFast = Configuracion.GetString("MODELO_FAST", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI";
            if (isComputerFast)
            {
                COMPUTERMAX.SetupLimitsInfoFast result = new COMPUTERMAX.SetupLimitsInfoFast();
                result = GetVariable("FactoresCalibracion", result);

                var setupLimitsInfo = cmax.ReadSetupLimitsFast();

                Assert.AreEqual(setupLimitsInfo.calibracionMedidaI, result.calibracionMedidaI, Error().UUT.MEDIDA.SETUP("Error. El valor de calibración de la corriente leido no coincide con el grabado."));

                Assert.AreEqual(setupLimitsInfo.calibracionMedidaV, result.calibracionMedidaV, Error().UUT.MEDIDA.SETUP("Error. El valor de calibración de la tension leido no coincide con el grabado."));

                Assert.AreEqual(setupLimitsInfo.factorCalibracionCorriente, result.factorCalibracionCorriente, Error().UUT.MEDIDA.SETUP("Error. El valor del factor de calibración de la corriente leido no coincide con el grabado."));

                Assert.AreEqual(setupLimitsInfo.anguloDesfase, result.anguloDesfase, Error().UUT.MEDIDA.SETUP("Error. El valor del desfase leido no coincide con el grabado."));
            }
            else
            {
                COMPUTERMAX.SetupLimitsInfo result = new COMPUTERMAX.SetupLimitsInfo();
                result = GetVariable("FactoresCalibracion", result);

                var setupLimitsInfo = cmax.ReadSetupLimits();

                Assert.AreEqual(setupLimitsInfo.calibracionMedidaI, result.calibracionMedidaI, Error().UUT.MEDIDA.SETUP("Error. El valor de calibración de la corriente leido no coincide con el grabado."));

                Assert.AreEqual(setupLimitsInfo.calibracionMedidaV, result.calibracionMedidaV, Error().UUT.MEDIDA.SETUP("Error. El valor de calibración de la tension leido no coincide con el grabado."));

                Assert.AreEqual(setupLimitsInfo.factorCalibracionCorriente, result.factorCalibracionCorriente, Error().UUT.MEDIDA.SETUP("Error. El valor del factor de calibración de la corriente leido no coincide con el grabado."));

                Assert.AreEqual(setupLimitsInfo.anguloDesfase, result.anguloDesfase, Error().UUT.MEDIDA.SETUP("Error. El valor del desfase leido no coincide con el grabado."));
            }

            var firmwareVersion = cmax.ReadSoftwareVersion();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, version, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de firmware Incorrecta"), ParamUnidad.SinUnidad);

            var setupVariablesInfo = cmax.ReadSetupInfo();

            Assert.AreEqual(setupVariablesInfo.hardware, setupInfo.hardware, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error. No se ha grabado correctamente el vector de hardware"));

            var identificationReading = cmax.ReadIdentificationParameters();

            Assert.AreEqual(identificationReading.errorCode, identification.errorCode, Error().UUT.CONFIGURACION.NO_GRABADO("Error. No se ha grabado correctamente el codigo de error."));

            Assert.AreEqual(identificationReading.frameNumber, identification.frameNumber, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. No se ha grabado correctamente el número de bastidor."));

            Assert.AreEqual(identificationReading.serialNumber, identification.serialNumber, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. No se ha grabado correctamente el número de serie."));
        }
        
        public void TestFinish()
        {
            if (cmax != null)
                cmax.Dispose();

            this.RunOnlyLastTest("OFF_TOWER",
            () =>
            {
                if (tower != null)
                {
                    tower.ShutdownSources();
                    tower.IO.DO.Off(14, OUTPUT_ACTIVACION_MINI_CONVERSORES, OUTPUT_SELECCION_EQUIPOS_A_VERIFICAR, deviceInfo.puntasComunicaciones, deviceInfo.puntas);
                    tower.IO.DO.Off(OUTPUT_PUNTAS_EQUIPO_2,OUTPUT_PILOTO_TEST_EQUIPO_1);
                    tower.Dispose();

                    if (IsTestError)
                        tower.IO.DO.On(deviceInfo.pilotoTest);
                }
            });
        }
    }
}
