﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Dezac.Tests.Forms
{
    public partial class InputMatrixImageItemView : UserControl
    {
        private string imageFileName;
        private bool? isValid;
        private bool isVisibleLogger;


        public Action ViewLogger { get; set; }

        public InputMatrixImageItemView()
        {
            InitializeComponent();
        }

        public bool IsVisibleLogger
        {
            get { return isVisibleLogger; }
            set
            {
                isVisibleLogger = value;
                UpdateUI();
            }
        }

        public string ImageFileName
        {
            get { return imageFileName; }
            set
            {
                imageFileName = value;
                if (value != null && File.Exists(value))
                    Image = Image.FromFile(value);
                else
                    Image = null;
            }
        }

        public Image Image
        {
            get { return image.Image; }
            set { image.Image = value; }
        }

        public string Title
        {
            get { return lbl.Text; }
            set { lbl.Text = value; }
        }

        public string InputText
        {
            get { return textBox.Text; }
            set { textBox.Text = value; }
        }

        public string InfoText
        {
            get { return infoText.Text; }
            set { infoText.Text = value; }
        }

        public string InfoRichText
        {
            get { return infoText.Rtf; }
            set { infoText.Rtf = value; }
        }

        public void ShowImage()
        {
            infoText.Visible = false;
            image.Visible = true;
        }

        public void ShowText()
        {
            infoText.Visible = true;
            image.Visible = false;
        }

        public bool? IsValid
        {
            get { return isValid; }
            set
            {
                isValid = value;
                UpdateUI();
            }
        }

        public bool IsDisabled
        {
            get { return chkDisabed.Checked; }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox.Text))
                IsValid = null;
            else
                IsValid = true;
        }

        private void chkDisabed_CheckedChanged(object sender, EventArgs e)
        {
            textBox.Enabled = !chkDisabed.Checked;

            if (chkDisabed.Checked)
                textBox.Text = null;

            UpdateUI();
        }

        private void UpdateUI()
        {
            if (IsDisabled)
                BackColor = SystemColors.ControlDark;
            else if (isValid.HasValue)
                BackColor = IsValid.Value ? Color.Green : Color.Red;
            else
                BackColor = SystemColors.Control;

             btnLogView.Visible = IsVisibleLogger;
        }

        private void btnLogView_Click(object sender, EventArgs e)
        {
            ViewLogger();
        }

        public new void Dispose()
        {
            Image.Dispose();

            base.Dispose();
        }
    }
}
