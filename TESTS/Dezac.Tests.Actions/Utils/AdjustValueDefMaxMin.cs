﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    public class AdjustValueDefMaxMin : AdjustValueDefWrapper
    {
        private VariableItem[] varNamesDevice;
    
        [Description("Control variable name")]
        [CategoryAttribute("1.- Identification")]
        public new string Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        [Description("Control variable unit type")]
        [CategoryAttribute("1.- Identification")]
        public new ParamUnidad Unidad
        {
            get { return base.Unidad; }
            set { base.Unidad = value; }
        }

        [Description("Control variable Maximum margin")]
        [CategoryAttribute("2.- Margin Maximum and Minimum")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Browsable(true)]
        public new string Max
        {
            get { return base.Max; }
            set { base.Max = value; }
        }

        [Description("Control variable Minimum margin")]
        [CategoryAttribute("2.- Margin Maximum and Minimum")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Browsable(true)]
        public new string Min
        {
            get { return base.Min; }
            set { base.Min = value; }
        }

        [Description("Control variable Varianza margin")]
        [CategoryAttribute("3.- Margin Varianza")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Browsable(true)]
        public new string MaxVarianza
        {
            get { return base.MaxVarianza; }
            set { base.MaxVarianza = value; }
        }

        [Description("Varaible name to resolve value in list device result variables")]
        [CategoryAttribute("4.- Variable to load value from device")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public new string VarNamesDevice
        {
            get { return base.VarNamesDevice; }
            set { base.VarNamesDevice = value; }
        }

        [Description("Varaible name to resolve value in list pattern result variables")]
        [CategoryAttribute("5.- Variable to load value from pattern")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public new string VarNamesPattern
        {
            get { return base.VarNamesPattern; }
            set { base.VarNamesPattern = value; }
        }
    }
}
