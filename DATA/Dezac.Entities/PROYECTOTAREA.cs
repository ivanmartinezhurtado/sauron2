namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PROYECTOTAREA
    {
        public int NUMPROYECTO { get; set; }
        public int NUMFASE { get; set; }
        public int NUMTAREA { get; set; }
        public string IDTAREA { get; set; }
        public string DESCRIPCION { get; set; }
        public string DESCRIPCIONDETALLADA { get; set; }
        public string IDEMPLEADO { get; set; }
    }
}
