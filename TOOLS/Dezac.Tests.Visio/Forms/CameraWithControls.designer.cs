namespace Dezac.Tests.Visio.Forms
{
    partial class CameraWithControls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flipVerticalButton = new System.Windows.Forms.Button();
            this.flipHorizontalButton = new System.Windows.Forms.Button();
            this.captureButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Setting_lbl = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblBarCode = new System.Windows.Forms.Label();
            this.btScanBarCode = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtGain = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtExposureCamera = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtExposure = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.sliderHeight = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.labelWidth = new System.Windows.Forms.Label();
            this.sliderWidth = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.Sharpness_SLD = new System.Windows.Forms.TrackBar();
            this.Sharpness_LBL = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Brigtness_SLD = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.Brigthness_LBL = new System.Windows.Forms.Label();
            this.Contrast_LBL = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Contrast_SLD = new System.Windows.Forms.TrackBar();
            this.Reset_Cam_Settings = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Refresh_BTN = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RetrieveGrayFrame = new System.Windows.Forms.CheckBox();
            this.RetrieveBgrFrame = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Camera_Selection = new System.Windows.Forms.ComboBox();
            this.Cam_lbl = new System.Windows.Forms.Label();
            this.captureBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sharpness_SLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Brigtness_SLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contrast_SLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.captureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // flipVerticalButton
            // 
            this.flipVerticalButton.Location = new System.Drawing.Point(18, 131);
            this.flipVerticalButton.Name = "flipVerticalButton";
            this.flipVerticalButton.Size = new System.Drawing.Size(102, 23);
            this.flipVerticalButton.TabIndex = 2;
            this.flipVerticalButton.Text = "Espejo Vertical";
            this.flipVerticalButton.UseVisualStyleBackColor = true;
            this.flipVerticalButton.Click += new System.EventHandler(this.FlipVerticalButtonClick);
            // 
            // flipHorizontalButton
            // 
            this.flipHorizontalButton.Location = new System.Drawing.Point(126, 131);
            this.flipHorizontalButton.Name = "flipHorizontalButton";
            this.flipHorizontalButton.Size = new System.Drawing.Size(102, 23);
            this.flipHorizontalButton.TabIndex = 1;
            this.flipHorizontalButton.Text = "Espejo Horizontal";
            this.flipHorizontalButton.UseVisualStyleBackColor = true;
            this.flipHorizontalButton.Click += new System.EventHandler(this.FlipHorizontalButtonClick);
            // 
            // captureButton
            // 
            this.captureButton.Enabled = false;
            this.captureButton.Location = new System.Drawing.Point(18, 63);
            this.captureButton.Name = "captureButton";
            this.captureButton.Size = new System.Drawing.Size(102, 23);
            this.captureButton.TabIndex = 0;
            this.captureButton.Text = "Iniciar Camera";
            this.captureButton.UseVisualStyleBackColor = true;
            this.captureButton.Click += new System.EventHandler(this.captureButtonClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 425F));
            this.tableLayoutPanel1.Controls.Add(this.Setting_lbl, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Cam_lbl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.captureBox, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(905, 799);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // Setting_lbl
            // 
            this.Setting_lbl.AutoSize = true;
            this.Setting_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Setting_lbl.Location = new System.Drawing.Point(483, 0);
            this.Setting_lbl.Name = "Setting_lbl";
            this.Setting_lbl.Size = new System.Drawing.Size(132, 16);
            this.Setting_lbl.TabIndex = 2;
            this.Setting_lbl.Text = "CONFIGURACI�N";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.lblBarCode);
            this.panel1.Controls.Add(this.btScanBarCode);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.Reset_Cam_Settings);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.Refresh_BTN);
            this.panel1.Controls.Add(this.richTextBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.RetrieveGrayFrame);
            this.panel1.Controls.Add(this.RetrieveBgrFrame);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Camera_Selection);
            this.panel1.Controls.Add(this.captureButton);
            this.panel1.Controls.Add(this.flipHorizontalButton);
            this.panel1.Controls.Add(this.flipVerticalButton);
            this.panel1.Location = new System.Drawing.Point(483, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 773);
            this.panel1.TabIndex = 0;
            // 
            // lblBarCode
            // 
            this.lblBarCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarCode.Location = new System.Drawing.Point(218, 198);
            this.lblBarCode.Name = "lblBarCode";
            this.lblBarCode.Size = new System.Drawing.Size(170, 36);
            this.lblBarCode.TabIndex = 27;
            this.lblBarCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btScanBarCode
            // 
            this.btScanBarCode.Location = new System.Drawing.Point(278, 160);
            this.btScanBarCode.Name = "btScanBarCode";
            this.btScanBarCode.Size = new System.Drawing.Size(110, 23);
            this.btScanBarCode.TabIndex = 26;
            this.btScanBarCode.Text = "Leer C�digo Barras";
            this.btScanBarCode.UseVisualStyleBackColor = true;
            this.btScanBarCode.Click += new System.EventHandler(this.btScanBarCode_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(278, 131);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Guardar Imagen";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtGain);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtExposureCamera);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtExposure);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.labelHeight);
            this.panel2.Controls.Add(this.sliderHeight);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.labelWidth);
            this.panel2.Controls.Add(this.sliderWidth);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.Sharpness_SLD);
            this.panel2.Controls.Add(this.Sharpness_LBL);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.Brigtness_SLD);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.Brigthness_LBL);
            this.panel2.Controls.Add(this.Contrast_LBL);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.Contrast_SLD);
            this.panel2.Location = new System.Drawing.Point(3, 251);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(397, 331);
            this.panel2.TabIndex = 24;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(208, 266);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 16);
            this.label13.TabIndex = 36;
            this.label13.Text = "(Camera)";
            // 
            // txtGain
            // 
            this.txtGain.Location = new System.Drawing.Point(102, 292);
            this.txtGain.Name = "txtGain";
            this.txtGain.Size = new System.Drawing.Size(100, 20);
            this.txtGain.TabIndex = 35;
            this.txtGain.TextChanged += new System.EventHandler(this.txtGain_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(57, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 16);
            this.label12.TabIndex = 34;
            this.label12.Text = "Gain:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtExposureCamera
            // 
            this.txtExposureCamera.Location = new System.Drawing.Point(102, 266);
            this.txtExposureCamera.Name = "txtExposureCamera";
            this.txtExposureCamera.Size = new System.Drawing.Size(100, 20);
            this.txtExposureCamera.TabIndex = 33;
            this.txtExposureCamera.TextChanged += new System.EventHandler(this.txtExposureCamera_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(28, 267);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 16);
            this.label10.TabIndex = 32;
            this.label10.Text = "Exposure:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtExposure
            // 
            this.txtExposure.Location = new System.Drawing.Point(102, 240);
            this.txtExposure.Name = "txtExposure";
            this.txtExposure.Size = new System.Drawing.Size(100, 20);
            this.txtExposure.TabIndex = 31;
            this.txtExposure.TextChanged += new System.EventHandler(this.txtExposure_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(28, 241);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 30;
            this.label9.Text = "Exposure:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeight.Location = new System.Drawing.Point(346, 199);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(16, 16);
            this.labelHeight.TabIndex = 29;
            this.labelHeight.Text = "0";
            // 
            // sliderHeight
            // 
            this.sliderHeight.Location = new System.Drawing.Point(102, 199);
            this.sliderHeight.Maximum = 4000;
            this.sliderHeight.Minimum = 480;
            this.sliderHeight.Name = "sliderHeight";
            this.sliderHeight.Size = new System.Drawing.Size(238, 45);
            this.sliderHeight.SmallChange = 50;
            this.sliderHeight.TabIndex = 28;
            this.sliderHeight.TickFrequency = 200;
            this.sliderHeight.Value = 480;
            this.sliderHeight.Scroll += new System.EventHandler(this.sliderHeight_Scroll);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(51, 199);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 16);
            this.label11.TabIndex = 27;
            this.label11.Text = "Altura:";
            // 
            // labelWidth
            // 
            this.labelWidth.AutoSize = true;
            this.labelWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWidth.Location = new System.Drawing.Point(346, 151);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(16, 16);
            this.labelWidth.TabIndex = 26;
            this.labelWidth.Text = "0";
            // 
            // sliderWidth
            // 
            this.sliderWidth.LargeChange = 400;
            this.sliderWidth.Location = new System.Drawing.Point(102, 151);
            this.sliderWidth.Maximum = 4000;
            this.sliderWidth.Minimum = 640;
            this.sliderWidth.Name = "sliderWidth";
            this.sliderWidth.Size = new System.Drawing.Size(238, 45);
            this.sliderWidth.SmallChange = 200;
            this.sliderWidth.TabIndex = 25;
            this.sliderWidth.TickFrequency = 200;
            this.sliderWidth.Value = 640;
            this.sliderWidth.Scroll += new System.EventHandler(this.sliderWidth_Scroll);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(36, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 16);
            this.label7.TabIndex = 24;
            this.label7.Text = "Anchura:";
            // 
            // Sharpness_SLD
            // 
            this.Sharpness_SLD.Location = new System.Drawing.Point(102, 105);
            this.Sharpness_SLD.Maximum = 200;
            this.Sharpness_SLD.Name = "Sharpness_SLD";
            this.Sharpness_SLD.Size = new System.Drawing.Size(238, 45);
            this.Sharpness_SLD.TabIndex = 21;
            this.Sharpness_SLD.TickFrequency = 5;
            this.Sharpness_SLD.Scroll += new System.EventHandler(this.Sharpness_SLD_Scroll);
            // 
            // Sharpness_LBL
            // 
            this.Sharpness_LBL.AutoSize = true;
            this.Sharpness_LBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sharpness_LBL.Location = new System.Drawing.Point(346, 105);
            this.Sharpness_LBL.Name = "Sharpness_LBL";
            this.Sharpness_LBL.Size = new System.Drawing.Size(16, 16);
            this.Sharpness_LBL.TabIndex = 22;
            this.Sharpness_LBL.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(39, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Contorno:";
            // 
            // Brigtness_SLD
            // 
            this.Brigtness_SLD.Location = new System.Drawing.Point(102, 3);
            this.Brigtness_SLD.Maximum = 100;
            this.Brigtness_SLD.Name = "Brigtness_SLD";
            this.Brigtness_SLD.Size = new System.Drawing.Size(238, 45);
            this.Brigtness_SLD.TabIndex = 14;
            this.Brigtness_SLD.TickFrequency = 5;
            this.Brigtness_SLD.Scroll += new System.EventHandler(this.Brigtness_SLD_Scroll);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 20;
            this.label5.Text = "Contraste:";
            // 
            // Brigthness_LBL
            // 
            this.Brigthness_LBL.AutoSize = true;
            this.Brigthness_LBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Brigthness_LBL.Location = new System.Drawing.Point(346, 3);
            this.Brigthness_LBL.Name = "Brigthness_LBL";
            this.Brigthness_LBL.Size = new System.Drawing.Size(16, 16);
            this.Brigthness_LBL.TabIndex = 15;
            this.Brigthness_LBL.Text = "0";
            // 
            // Contrast_LBL
            // 
            this.Contrast_LBL.AutoSize = true;
            this.Contrast_LBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contrast_LBL.Location = new System.Drawing.Point(346, 54);
            this.Contrast_LBL.Name = "Contrast_LBL";
            this.Contrast_LBL.Size = new System.Drawing.Size(16, 16);
            this.Contrast_LBL.TabIndex = 19;
            this.Contrast_LBL.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Brillo:";
            // 
            // Contrast_SLD
            // 
            this.Contrast_SLD.Location = new System.Drawing.Point(102, 54);
            this.Contrast_SLD.Maximum = 200;
            this.Contrast_SLD.Name = "Contrast_SLD";
            this.Contrast_SLD.Size = new System.Drawing.Size(238, 45);
            this.Contrast_SLD.TabIndex = 18;
            this.Contrast_SLD.TickFrequency = 5;
            this.Contrast_SLD.Scroll += new System.EventHandler(this.Contrast_SLD_Scroll);
            // 
            // Reset_Cam_Settings
            // 
            this.Reset_Cam_Settings.Location = new System.Drawing.Point(298, 601);
            this.Reset_Cam_Settings.Name = "Reset_Cam_Settings";
            this.Reset_Cam_Settings.Size = new System.Drawing.Size(102, 23);
            this.Reset_Cam_Settings.TabIndex = 17;
            this.Reset_Cam_Settings.Text = "Reset";
            this.Reset_Cam_Settings.UseVisualStyleBackColor = true;
            this.Reset_Cam_Settings.Click += new System.EventHandler(this.Reset_Cam_Settings_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Par�metros";
            // 
            // Refresh_BTN
            // 
            this.Refresh_BTN.Location = new System.Drawing.Point(298, 736);
            this.Refresh_BTN.Name = "Refresh_BTN";
            this.Refresh_BTN.Size = new System.Drawing.Size(102, 23);
            this.Refresh_BTN.TabIndex = 12;
            this.Refresh_BTN.Text = "Refresh";
            this.Refresh_BTN.UseVisualStyleBackColor = true;
            this.Refresh_BTN.Click += new System.EventHandler(this.Refresh_BTN_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(18, 630);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(382, 100);
            this.richTextBox1.TabIndex = 11;
            this.richTextBox1.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 601);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "Info";
            // 
            // RetrieveGrayFrame
            // 
            this.RetrieveGrayFrame.AutoSize = true;
            this.RetrieveGrayFrame.Location = new System.Drawing.Point(40, 197);
            this.RetrieveGrayFrame.Name = "RetrieveGrayFrame";
            this.RetrieveGrayFrame.Size = new System.Drawing.Size(145, 17);
            this.RetrieveGrayFrame.TabIndex = 9;
            this.RetrieveGrayFrame.Text = "Obtener imagen en GRIS";
            this.RetrieveGrayFrame.UseVisualStyleBackColor = true;
            this.RetrieveGrayFrame.CheckedChanged += new System.EventHandler(this.RetrieveGrayFrame_CheckedChanged);
            // 
            // RetrieveBgrFrame
            // 
            this.RetrieveBgrFrame.AutoSize = true;
            this.RetrieveBgrFrame.Checked = true;
            this.RetrieveBgrFrame.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RetrieveBgrFrame.Location = new System.Drawing.Point(40, 174);
            this.RetrieveBgrFrame.Name = "RetrieveBgrFrame";
            this.RetrieveBgrFrame.Size = new System.Drawing.Size(127, 17);
            this.RetrieveBgrFrame.TabIndex = 8;
            this.RetrieveBgrFrame.Text = "Obtener imagen RGB";
            this.RetrieveBgrFrame.UseVisualStyleBackColor = true;
            this.RetrieveBgrFrame.CheckedChanged += new System.EventHandler(this.RetrieveBgrFrame_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Vista";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Camera";
            // 
            // Camera_Selection
            // 
            this.Camera_Selection.FormattingEnabled = true;
            this.Camera_Selection.Location = new System.Drawing.Point(18, 36);
            this.Camera_Selection.Name = "Camera_Selection";
            this.Camera_Selection.Size = new System.Drawing.Size(370, 21);
            this.Camera_Selection.TabIndex = 3;
            // 
            // Cam_lbl
            // 
            this.Cam_lbl.AutoSize = true;
            this.Cam_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cam_lbl.Location = new System.Drawing.Point(3, 0);
            this.Cam_lbl.Name = "Cam_lbl";
            this.Cam_lbl.Size = new System.Drawing.Size(71, 16);
            this.Cam_lbl.TabIndex = 1;
            this.Cam_lbl.Text = "CAMERA";
            // 
            // captureBox
            // 
            this.captureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.captureBox.Location = new System.Drawing.Point(3, 23);
            this.captureBox.Name = "captureBox";
            this.captureBox.Size = new System.Drawing.Size(474, 773);
            this.captureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.captureBox.TabIndex = 3;
            this.captureBox.TabStop = false;
            // 
            // CameraWithControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 823);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CameraWithControls";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Camera View";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sharpness_SLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Brigtness_SLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contrast_SLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.captureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button captureButton;
        private System.Windows.Forms.Button flipHorizontalButton;
        private System.Windows.Forms.Button flipVerticalButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label Setting_lbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Cam_lbl;
        private System.Windows.Forms.ComboBox Camera_Selection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox captureBox;
        private System.Windows.Forms.CheckBox RetrieveBgrFrame;
        private System.Windows.Forms.CheckBox RetrieveGrayFrame;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button Refresh_BTN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Brigthness_LBL;
        private System.Windows.Forms.TrackBar Brigtness_SLD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Reset_Cam_Settings;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Contrast_LBL;
        private System.Windows.Forms.TrackBar Contrast_SLD;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TrackBar Sharpness_SLD;
        private System.Windows.Forms.Label Sharpness_LBL;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.TrackBar sliderHeight;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblBarCode;
        private System.Windows.Forms.Button btScanBarCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar sliderWidth;
        private System.Windows.Forms.TextBox txtExposure;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtGain;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtExposureCamera;
        private System.Windows.Forms.Label label10;
    }
}

