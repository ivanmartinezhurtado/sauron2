﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Kernel;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Linq;
using System.Xml.XPath;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(XmlSearchNodeDescription))]
    
    public class XmlSearchNode : ActionBase
    {
        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Nodo a buscar en el XML")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string FilterXmlNode { get; set; }

        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Nombre del atributo a buscar su valor en el FilterXmlNode")]
        public string ValueXmlNodeAttribute { get; set; }

        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Nombre del atributo a filtrar su valor")]
        public string FilterXmlNodeAttribute { get; set; }

        [DefaultValue("")]
        [Category("3.1 Filter")]
        [Description("Valor del atributo a filtrar con el nombre en FilterXmlNodeAttribute")]
        public string FilterValueXmlNodeAttribute { get; set; }

        [DefaultValue("")]
        [Category("3. Configuration")]
        [Description("variable con los datos XML")]
        public string DataInput { get; set; } //ToDo Miki: Cambiado de VariableName a DataInput

        [Category("4. Results")]
        public string DataOutput { get; set; } //ToDo Miki: Cambiado de OutputVarName a DataOutput

        [Category("4. Results")]
        public string DataOutputXml { get; set; } //ToDo Miki: Cambiado de OutputVarNameXmlNode a DataOutputXml

        public override void Execute()
        {
            if (string.IsNullOrEmpty(DataOutput))
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DataOutput).Throw();

            var node = XmlVar();

            Logger.InfoFormat("{0} -> {1}", DataOutput, node.Item1);

            Logger.InfoFormat("{0} -> {1}", DataOutputXml ?? DataOutput + "_Node", node.Item2);

            if (Context.Variables.ContainsKey(DataOutput))
                Context.Variables.AddOrUpdate(DataOutput, node.Item1);
            else
                Context.Variables.AddOrUpdate(DataOutput, node.Item1);

            if (!string.IsNullOrEmpty(DataOutputXml))
                Context.Variables.AddOrUpdate(DataOutputXml, node.Item2);
        }

        public Tuple<string, XElement> XmlVar()
        {
            var result = "";
            XDocument xDoc = null;

            if (!DataInput.StartsWith("$"))
                DataInput = "$" + DataInput;

            var xml = VariablesTools.ResolveValue(DataInput);
            if (xml is string)
                xDoc = XDocument.Parse((string)xml);
            else
            {
                var sxml = xml as XElement;
                xDoc = XDocument.Parse(sxml.ToString());
            }

            var xpath = xDoc.XPathSelectElements(FilterXmlNode);
            XElement nodeSelected = null;
            if (xpath != null)
            {
                foreach (XElement xl in xpath)
                    if (xl.HasAttributes)
                    {
                        if (string.IsNullOrEmpty(FilterXmlNodeAttribute))
                        {
                            var att = xl.Attribute(ValueXmlNodeAttribute);
                            if (att != null)
                            {
                                nodeSelected = xl;
                                result = att.Value;
                                Context.ResultList.Add(string.Format("{0}={1}", att.Name, att.Value));
                                Context.ResultList.Add(string.Format("{0}_Node={1}", att.Name, nodeSelected.ToString()));
                                break;
                            }
                            else
                                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0} atributo {1}", FilterXmlNode, ValueXmlNodeAttribute)).Throw();
                        }
                        else
                        {
                            var att = xl.Attribute(FilterXmlNodeAttribute);
                            if (att != null)
                            {
                                if (att.Value == FilterValueXmlNodeAttribute)
                                {
                                    var att2 = xl.Attribute(ValueXmlNodeAttribute);
                                    if (att2 != null)
                                    {
                                        nodeSelected = xl;
                                        result = att2.Value;
                                        Context.ResultList.Add(string.Format("{0}={1}", att2.Name, att2.Value));
                                        Context.ResultList.Add(string.Format("{0}_Node={1}", att2.Name, nodeSelected.ToString()));
                                        break;
                                    }
                                    else
                                        Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0} atributo {1} con filtro en el atributo {2} con valor {3}", FilterXmlNode, ValueXmlNodeAttribute, FilterXmlNodeAttribute, FilterValueXmlNodeAttribute)).Throw();
                                }
                                else
                                {
                                    nodeSelected = xl;
                                    result = att.Value;
                                    Context.ResultList.Add(string.Format("{0}={1}", att.Name, att.Value));
                                    Context.ResultList.Add(string.Format("{0}_Node={1}", att.Name, nodeSelected.ToString()));
                                    break;
                                }
                            }
                            else
                                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0} atributo {1}", FilterXmlNode, ValueXmlNodeAttribute)).Throw();
                        }
                    }
                    else
                    {
                        nodeSelected = xl;
                        result = xl.Value;
                        Context.ResultList.Add(string.Format("{0}={1}", xl.Name, xl.Value));
                        Context.ResultList.Add(string.Format("{0}_Node={1}", xl.Name, nodeSelected.ToString()));
                        break;
                    }

                if (string.IsNullOrEmpty(result))
                    Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0} atributo {1} valor {2}", FilterXmlNode, ValueXmlNodeAttribute)).Throw();
            }
            else
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(string.Format("XML {0}", FilterXmlNode)).Throw();

            return Tuple.Create<string, XElement>(result, nodeSelected);
        }
    }

    public class XmlSearchNodeDescription : ActionDescription
    { 
        public override string Name { get { return "XmlSearchNode"; }}
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Other_xml_icon; } }
    }
}
