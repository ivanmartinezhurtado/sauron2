﻿using System.ComponentModel;
using System.Drawing.Design;

namespace Dezac.Tests.Actions.Views
{
    public class UIVariableEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            object result = value;

            using (var editor = new VariableSelectorView())
            {
                if (value != null)
                    editor.Value = value.ToString();

                if (editor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    result = editor.Value;
            }

            return result;
        }
    }
}
