﻿using Comunications.Message;
using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Dezac.Device
{
    public class CirbusDevice : IDisposable
    {
        protected CirbusTransport transport;
        private byte[] perifericNumber;

        public CirbusDevice()
        {
            Cultura = new CultureInfo("en-US");
        }

        public CirbusDevice(CirbusTransport Transport)
        {
            transport = Transport;
            Cultura = new CultureInfo("en-US");
        }

        public CirbusTransport Transport
        {
            get { return transport; }
            set { transport = value; }
        }

        public CultureInfo Cultura { get; set; }

        public byte PerifericNumber { set { perifericNumber = ASCIIEncoding.ASCII.GetBytes(value.ToString("00")); } get { return Convert.ToByte(ASCIIEncoding.ASCII.GetString(perifericNumber)); } }

        public int TimeOut { set { transport.ReadTimeout = value; } get { return transport.ReadTimeout; } }

        public int Retries { set { transport.Retries = value; } get { return transport.Retries; } }

        public int WaitToRetryMilliseconds { set { transport.WaitToRetryMilliseconds = value; } get { return transport.WaitToRetryMilliseconds; } }

        public string Read(string message)
        {
            byte[] data = transport.Read(perifericNumber, message);
           return ASCIIEncoding.ASCII.GetString(data);
        }

        public string Read(string message, bool validator)
        {
            byte[] data = transport.Read(perifericNumber, message, validator);
            return ASCIIEncoding.ASCII.GetString(data);
        }

        public string Read(string message, params object[] param)
        {
            return Read(string.Format(Cultura, message, param));
        }

        public string Read(string message,  Dictionary<byte,string> listErrors)
        {
            byte[] data = transport.Read(perifericNumber, message, listErrors);
            return ASCIIEncoding.ASCII.GetString(data).Trim();
        }

        public string Read(string message, Dictionary<byte, string> listErrors, params object[] param)
        {
            return Read(string.Format(Cultura, message, param), listErrors);
        }
       
        public T Read<T>(string message) where T : struct
        {
            byte[] data = transport.Read(perifericNumber, message);
            return Extensions.AsignedTramaToStructure<T>(ASCIIEncoding.ASCII.GetString(data).Trim());
        }

        public T Read<T>(string message, bool validator) where T : struct
        {
            byte[] data = transport.Read(perifericNumber, message, validator);
            return Extensions.AsignedTramaToStructure<T>(ASCIIEncoding.ASCII.GetString(data).Trim());
        }

        public T Read<T>(string message, Dictionary<byte, string> listErrors) where T : struct
        {
            byte[] data = transport.Read(perifericNumber, message, listErrors);
            return Extensions.AsignedTramaToStructure<T>(ASCIIEncoding.ASCII.GetString(data).Trim());
        }

        public T Read<T>(string message, Dictionary<byte, string> listErrors, params object[] param) where T : struct
        {
            return Read<T>(string.Format(Cultura, message, param), listErrors);
        }

        public void WriteWhitoutResponse(string message)
        {
            transport.WriteWithoutResponse(perifericNumber, message);
        }

        public void Write(string message)
        {
            transport.Write(perifericNumber, message);
        }

        public void Write(string message, params object[] param)
        {
            var msg = string.Format(Cultura, message, param);
            transport.Write(perifericNumber, msg);
        }

        public void Write(string message, Dictionary<byte,string> listErrors = null)
        {
            transport.Write(perifericNumber, message, listErrors);
        }

        public void Write(string message, Dictionary<byte,string> listErrors = null,  params object[] param)
        {
            var msg = string.Format(Cultura, message, param);
            transport.Write(perifericNumber, msg, listErrors);
        }

        public void WithTimeOut(Action<CirbusDevice> action, int? timeOut = null, bool throwTimeOutException = false)
        {
            int currentTimeOut = this.TimeOut;
                
            TimeOut = timeOut.GetValueOrDefault(currentTimeOut);
            try
            {
                action(this);
            }
            catch (TimeoutException)
            {
                if (throwTimeOutException)
                    throw;
            }
            finally
            {
                TimeOut = currentTimeOut;
            }
        }

        public virtual void Dispose()
        {
            Transport.Dispose();
        }

        public virtual void ClosePort()
        {

        }
    }
}
