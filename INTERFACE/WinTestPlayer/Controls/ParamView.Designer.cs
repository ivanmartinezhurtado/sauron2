﻿namespace WinTestPlayer.Controls
{
    partial class ParamView
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridParams = new MetroFramework.Controls.MetroGrid();
            this.bsParams = new System.Windows.Forms.BindingSource(this.components);
            this.metroRadioButton1 = new MetroFramework.Controls.MetroRadioButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.rbRes = new MetroFramework.Controls.MetroRadioButton();
            this.rbMarg = new MetroFramework.Controls.MetroRadioButton();
            this.rbIden = new MetroFramework.Controls.MetroRadioButton();
            this.rbCons = new MetroFramework.Controls.MetroRadioButton();
            this.rbConfs = new MetroFramework.Controls.MetroRadioButton();
            this.rbComs = new MetroFramework.Controls.MetroRadioButton();
            this.TipoGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorInicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsParams)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridParams
            // 
            this.gridParams.AllowUserToAddRows = false;
            this.gridParams.AllowUserToResizeRows = false;
            this.gridParams.AutoGenerateColumns = false;
            this.gridParams.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridParams.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridParams.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridParams.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridParams.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridParams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridParams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoGrupo,
            this.nameDataGridViewTextBoxColumn,
            this.ValorInicio,
            this.Unidad});
            this.gridParams.DataSource = this.bsParams;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridParams.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridParams.EnableHeadersVisualStyles = false;
            this.gridParams.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridParams.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridParams.Location = new System.Drawing.Point(0, 50);
            this.gridParams.Name = "gridParams";
            this.gridParams.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridParams.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gridParams.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridParams.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridParams.Size = new System.Drawing.Size(747, 384);
            this.gridParams.TabIndex = 0;
            // 
            // bsParams
            // 
            this.bsParams.DataSource = typeof(Dezac.Tests.Core.Model.ParamValue);
            // 
            // metroRadioButton1
            // 
            this.metroRadioButton1.AutoSize = true;
            this.metroRadioButton1.Location = new System.Drawing.Point(18, 18);
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Size = new System.Drawing.Size(56, 15);
            this.metroRadioButton1.TabIndex = 1;
            this.metroRadioButton1.Text = "Todos";
            this.metroRadioButton1.UseSelectable = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.rbRes);
            this.metroPanel1.Controls.Add(this.rbMarg);
            this.metroPanel1.Controls.Add(this.rbIden);
            this.metroPanel1.Controls.Add(this.rbCons);
            this.metroPanel1.Controls.Add(this.rbConfs);
            this.metroPanel1.Controls.Add(this.rbComs);
            this.metroPanel1.Controls.Add(this.metroRadioButton1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(747, 50);
            this.metroPanel1.TabIndex = 2;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // rbRes
            // 
            this.rbRes.AutoSize = true;
            this.rbRes.Location = new System.Drawing.Point(651, 18);
            this.rbRes.Name = "rbRes";
            this.rbRes.Size = new System.Drawing.Size(80, 15);
            this.rbRes.TabIndex = 7;
            this.rbRes.Text = "Resultados";
            this.rbRes.UseSelectable = true;
            this.rbRes.CheckedChanged += new System.EventHandler(this.OnRefreshData);
            // 
            // rbMarg
            // 
            this.rbMarg.AutoSize = true;
            this.rbMarg.Location = new System.Drawing.Point(557, 18);
            this.rbMarg.Name = "rbMarg";
            this.rbMarg.Size = new System.Drawing.Size(75, 15);
            this.rbMarg.TabIndex = 6;
            this.rbMarg.Text = "Márgenes";
            this.rbMarg.UseSelectable = true;
            this.rbMarg.CheckedChanged += new System.EventHandler(this.OnRefreshData);
            // 
            // rbIden
            // 
            this.rbIden.AutoSize = true;
            this.rbIden.Location = new System.Drawing.Point(445, 18);
            this.rbIden.Name = "rbIden";
            this.rbIden.Size = new System.Drawing.Size(95, 15);
            this.rbIden.TabIndex = 5;
            this.rbIden.Text = "Identificación";
            this.rbIden.UseSelectable = true;
            this.rbIden.CheckedChanged += new System.EventHandler(this.OnRefreshData);
            // 
            // rbCons
            // 
            this.rbCons.AutoSize = true;
            this.rbCons.Location = new System.Drawing.Point(346, 18);
            this.rbCons.Name = "rbCons";
            this.rbCons.Size = new System.Drawing.Size(78, 15);
            this.rbCons.TabIndex = 4;
            this.rbCons.Text = "Consignas";
            this.rbCons.UseSelectable = true;
            this.rbCons.CheckedChanged += new System.EventHandler(this.OnRefreshData);
            // 
            // rbConfs
            // 
            this.rbConfs.AutoSize = true;
            this.rbConfs.Location = new System.Drawing.Point(223, 18);
            this.rbConfs.Name = "rbConfs";
            this.rbConfs.Size = new System.Drawing.Size(99, 15);
            this.rbConfs.TabIndex = 3;
            this.rbConfs.Text = "Configuración";
            this.rbConfs.UseSelectable = true;
            this.rbConfs.CheckedChanged += new System.EventHandler(this.OnRefreshData);
            // 
            // rbComs
            // 
            this.rbComs.AutoSize = true;
            this.rbComs.Checked = true;
            this.rbComs.Location = new System.Drawing.Point(92, 18);
            this.rbComs.Name = "rbComs";
            this.rbComs.Size = new System.Drawing.Size(112, 15);
            this.rbComs.TabIndex = 2;
            this.rbComs.TabStop = true;
            this.rbComs.Text = "Comunicaciones";
            this.rbComs.UseSelectable = true;
            this.rbComs.CheckedChanged += new System.EventHandler(this.OnRefreshData);
            // 
            // TipoGrupo
            // 
            this.TipoGrupo.DataPropertyName = "TipoGrupo";
            this.TipoGrupo.HeaderText = "TipoGrupo";
            this.TipoGrupo.Name = "TipoGrupo";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // ValorInicio
            // 
            this.ValorInicio.DataPropertyName = "Valor";
            this.ValorInicio.HeaderText = "Valor";
            this.ValorInicio.Name = "ValorInicio";
            // 
            // Unidad
            // 
            this.Unidad.DataPropertyName = "Unidad";
            this.Unidad.HeaderText = "Unidad";
            this.Unidad.Name = "Unidad";
            // 
            // ParamView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridParams);
            this.Controls.Add(this.metroPanel1);
            this.Name = "ParamView";
            this.Size = new System.Drawing.Size(747, 434);
            ((System.ComponentModel.ISupportInitialize)(this.gridParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsParams)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroGrid gridParams;
        private System.Windows.Forms.BindingSource bsParams;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroRadioButton rbIden;
        private MetroFramework.Controls.MetroRadioButton rbCons;
        private MetroFramework.Controls.MetroRadioButton rbConfs;
        private MetroFramework.Controls.MetroRadioButton rbComs;
        private MetroFramework.Controls.MetroRadioButton rbRes;
        private MetroFramework.Controls.MetroRadioButton rbMarg;
        private System.Windows.Forms.DataGridViewTextBoxColumn textDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoGrupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorInicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unidad;
    }
}
