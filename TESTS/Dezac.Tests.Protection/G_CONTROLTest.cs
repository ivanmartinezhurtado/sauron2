﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.ComponentModel;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.05)]
    public class G_CONTROLTest : TestBase
    {
        TowerBoard tower;
        G_CONTROL gControl;

        private const byte PILOTO_ERROR = 48;
        private const byte BLOQUE_SEGURIDAD = 23;
        private const byte DEVICE_PRESENCE =9;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            gControl = new G_CONTROL(5); // Comunicaciones.SerialPort);
            gControl.Modbus.PerifericNumber = Comunicaciones.Periferico;

            tower.IO.DO.Off(PILOTO_ERROR);
            tower.Active24VDC();

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[BLOQUE_SEGURIDAD];
            }, "Error. No se ha detectado la presencia del equipo", 10, 1500, 1000);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DEVICE_PRESENCE];
            }, "Error. No se ha detectado la presencia del equipo", 10, 1500, 1000);

        }

        public void TestComunications()
        {            
            SamplerWithCancel((p) =>
            {
                var version = gControl.ReadFirmwareVersion();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la versión de firmware es incorrecta"), ParamUnidad.SinUnidad);             
                return true;
            }, "Error. No se ha podido cominunicar con el equipo", 3, 500, 1000, false, false);

            gControl.WriteFlagTest();

            Resultado.Set("COMUNICATIONS", "OK", ParamUnidad.SinUnidad);

            gControl.WriteErrorCode(999);

            gControl.WriteGainDefault();

            Delay(2000, "");
        }

        public void TestAdjustVL1()
        {
            var defs = new AdjustValueDef(Params.GAIN_V.Null.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var voltageReference = Consignas.GetDouble(Params.V.Null.TestPoint("NOM").Name, 230, ParamUnidad.V);

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 2, DelayBetweenSamples = 1100, SampleMaxNumber = 3 };

            var result = gControl.AdjustVoltage(voltageReference, controlAdjust, (value) =>
            {
                defs.Value = value[0];
                return defs.IsValid();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES("GAIN_V_NOM").Throw();

            gControl.WriteGainVL1(Convert.ToUInt16(defs.Value));

            gControl.ApplyGain();

            var verif = new AdjustValueDef(Params.V.Null.TestPoint("NOM").Name, 0, 0,0, voltageReference, Params.V.Null.Verificacion.Tol(), ParamUnidad.PorCentage);
            TestCalibracionBase(verif,
            () =>
            {
                return gControl.ReadVL1();
            },
            () =>
            {
                return voltageReference;
            }, 1,5, 500, "V");
        }

        public void TestAdjustVRE()
        {
            var defs = new AdjustValueDef(Params.GAIN_FACTOR.Other("RE").Name,0, Params.GAIN_FACTOR.Other("RE").Ajuste.Min(), Params.GAIN_FACTOR.Other("RE").Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var resistenceReference =  Configuracion.GetDouble(Params.RES.Null.Ajuste.Name, 50, ParamUnidad.Ohms);

            tower.IO.DO.On((byte)Enum_RE._50R);

            Delay(6000, "Espera On Ressitencia antes de Ajustr");

            SamplerWithCancel((p) =>
            {
                var re = gControl.ReadRE();
                logger.DebugFormat("Adjust RE {0}", re);
                Assert.AreBetween(re, 42, 55, Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Error. No se ha podido obtener un valor de RE aproximado a 50 Ohmios"));
                return true;
            }, "Error. No se ha podido obtener un valor de RE aproximado a 50 Ohmios", 50, 2000, 1000, false, false);

            var controlAdjust = new ConfigurationSampler() { InitDelayTime = 1, SampleNumber = 4, DelayBetweenSamples = 1100, SampleMaxNumber = 6 };

            var result = gControl.AdjustResistence(resistenceReference, controlAdjust, (value) =>
            {
                defs.Value = value[0];
                return defs.IsValid();
            });

            defs.AddToResults(Resultado);

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES("GAIN_RE").Throw();

            gControl.WriteGainVRE(Convert.ToUInt16(defs.Value));

            gControl.ApplyGain();

            Delay(2000, "Espera del ApplyGain antes de verificar"); 

            var verif = new AdjustValueDef(Params.RES.Other("RE").Name, 0, 0, 0, resistenceReference, Params.RES.Other("RE").Verificacion.Tol(), ParamUnidad.Ohms);
            TestCalibracionBase(verif,
            () =>
            {
                var re = gControl.ReadRE();
                logger.DebugFormat("Verif RE {0}", re);
                return re;
            },
            () =>
            {
                return resistenceReference;
            }, 2, 5, 1100, "RE");

            tower.IO.DO.OffWait(2000, (byte)Enum_RE._50R);
        }

        public void TestMedidaLeds([Description("_5R, _1K, CA")]Enum_RE testPoint)
        {
            tower.IO.DO.OnWait(6000, (byte)testPoint);

            var valor = testPoint.ToString().Replace("_", "").Replace("R", "").Replace("K", "000").Replace("CA", "1350");
            var resistenceReference = Convert.ToDouble(valor);

            var desc = testPoint.ToString().Replace("_", "");

            var verif = new AdjustValueDef(Params.RES.Other(desc).Name, 0, 0, 0, resistenceReference, Params.RES.Other(desc).Verificacion.Tol(), ParamUnidad.Ohms);
            TestCalibracionBase(verif,
            () =>
            {
                return gControl.ReadRE();
            },
            () =>
            {
                return resistenceReference;
            }, 2, 10, 1100, testPoint.ToString());

         
            tower.IO.DO.OffWait(100, (byte)testPoint);
        }

        public void TestSupplyVoltageMin()
        {
            var voltageReference = Consignas.GetDouble(Params.V.Null.TestPoint("MIN").Name, 110, ParamUnidad.V);
            InternalTestSupplyVoltage(voltageReference, "MIN");
        }

        public void TestSupplyVoltageMax()
        {
            var voltageReference = Consignas.GetDouble(Params.V.Null.TestPoint("MAX").Name, 274, ParamUnidad.V);
            InternalTestSupplyVoltage(voltageReference, "MAX");
        }

        private void InternalTestSupplyVoltage(double voltageReference, string testPoint)
        {
            tower.Chroma.ApplyPresetsAndWaitStabilisation(voltageReference, 0.5, 50);

            SamplerWithCancel((p) =>
            {
                gControl.WriteFlagTest(false);
                return true;
            }, "Error. No se ha podido cominunicar con el equipo", 10, 100, 0, false, false);

            var verif = new AdjustValueDef(Params.V.Null.TestPoint(testPoint).Name, 0, 0, 0, voltageReference, Params.V.Null.Verificacion.Tol(), ParamUnidad.PorCentage);
            TestCalibracionBase(verif,
            () =>
            {
                return gControl.ReadVL1();
            },
            () =>
            {
                return voltageReference;
            }, 1, 5, 500, "V_" + testPoint);  
        }

        public void TestCustomization()
        {
            gControl.WriteFlagTest(false);
            Delay(200, "Wait ... WriteFlagTest");

            gControl.WriteVectorHardware();
            Delay(200, "Wait ... WriteVectorHardware");

            gControl.WriteBastidor(Convert.ToInt32(TestInfo.NumBastidor));
            Delay(200, "Wait ... WriteBastidor");

            gControl.WriteSerialNumber(Convert.ToInt32(TestInfo.NumSerie));
            Delay(200, "Wait ... WriteSerialNumber");

            gControl.WriteErrorCode(0);
            Delay(200, "Wait ... WriteErrorCode");

            gControl.ApplyGain();
            Delay(1000, "Escribiendo Apply Gain");

            tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(1000, "Reseteando equipo");
            var voltageReference = Consignas.GetDouble(Params.V.Null.TestPoint("NOM").Name, 230, ParamUnidad.V);
            tower.Chroma.ApplyPresetsAndWaitStabilisation(voltageReference, 0.5, 50);

            var bastidorReading = 0;
            SamplerWithCancel((p) =>
            {
                bastidorReading = gControl.ReadBastidor();
                return true;
            }, "Error. No se ha podido comununicar con el equipo después del Reset", 5, 2000, 0, false, false);

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidorReading.ToString(), TestInfo.NumBastidor.ToString(), Error().UUT.NUMERO_DE_BASTIDOR.NO_GRABADO("Error, numero de bastidor no grabado correctamente"), ParamUnidad.SinUnidad);

            var numSerie = gControl.ReadSerialNumber().ToString();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numSerie, TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_GRABADO("Error, numero de serie no grabado correctamente"), ParamUnidad.SinUnidad);

            var codError = gControl.ReadErrorCode();

            Assert.AreEqual("CODERROR", codError, 0, Error().UUT.CONFIGURACION.NO_GRABADO("Error, coderror no grabado correctamente"), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (gControl != null)
                gControl.Dispose();

            if (tower != null)
            {                
                tower.IO.DO.Off(4);
                tower.ShutdownSources();
                tower.Dispose();
            }

            if (IsTestError)
                tower.IO.DO.On(PILOTO_ERROR);
        }

        public enum Enum_RE
        {
            _50R = 20,
            _5R = 17,
            _1K = 28,
            CA = 0,
        }

    }
}
