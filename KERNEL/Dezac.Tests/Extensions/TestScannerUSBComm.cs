﻿using Instruments.Auxiliars;
using System;

namespace Dezac.Tests.Extensions
{
    public static class TestScannerUSBComm
    {
        public static string ScannerUSBComm(this TestBase testBase, string scannerPort)
        {
            var portScanner = Convert.ToByte(scannerPort.Replace("COM", ""));

            using (HoneywellScanner scanner = new HoneywellScanner(portScanner))
            {
                var result = scanner.Read();
                testBase.Logger.InfoFormat("Honeywell Reading: {0}", result);
                return result;
            }
        }          
    }
}
