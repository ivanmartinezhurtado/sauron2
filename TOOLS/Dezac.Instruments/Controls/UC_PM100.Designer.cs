﻿namespace Dezac.Instruments.Controls
{
    partial class UC_PM100
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label potenciaActivaL1Label;
            System.Windows.Forms.Label tensionL1Label;
            System.Windows.Forms.Label corrienteL1Label;
            System.Windows.Forms.Label cosenoPhiL1Label;
            System.Windows.Forms.Label potenciaAparenteL1Label;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL1Label;
            System.Windows.Forms.Label frecuenciaLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label factorPotenciaL1Label;
            this.cosenoPhiL1TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL1TextBox = new System.Windows.Forms.TextBox();
            this.tensionL1TextBox = new System.Windows.Forms.TextBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.potenciaActivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL1TextBox = new System.Windows.Forms.TextBox();
            this.frecuenciaTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnIDN = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.rb120A = new System.Windows.Forms.RadioButton();
            this.rb12A = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.factorPotenciaL1TextBox = new System.Windows.Forms.TextBox();
            this.txtIDN = new System.Windows.Forms.TextBox();
            this.btmSetConfigure = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnAVG = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btnShunt = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.btnClearSel = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.btnSelection = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            potenciaActivaL1Label = new System.Windows.Forms.Label();
            tensionL1Label = new System.Windows.Forms.Label();
            corrienteL1Label = new System.Windows.Forms.Label();
            cosenoPhiL1Label = new System.Windows.Forms.Label();
            potenciaAparenteL1Label = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL1Label = new System.Windows.Forms.Label();
            frecuenciaLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            factorPotenciaL1Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // potenciaActivaL1Label
            // 
            potenciaActivaL1Label.AutoSize = true;
            potenciaActivaL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL1Label.Location = new System.Drawing.Point(18, 126);
            potenciaActivaL1Label.Name = "potenciaActivaL1Label";
            potenciaActivaL1Label.Size = new System.Drawing.Size(59, 15);
            potenciaActivaL1Label.TabIndex = 0;
            potenciaActivaL1Label.Text = "Pot. Act:";
            // 
            // tensionL1Label
            // 
            tensionL1Label.AutoSize = true;
            tensionL1Label.BackColor = System.Drawing.Color.Transparent;
            tensionL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL1Label.ForeColor = System.Drawing.Color.Black;
            tensionL1Label.Location = new System.Drawing.Point(18, 6);
            tensionL1Label.Name = "tensionL1Label";
            tensionL1Label.Size = new System.Drawing.Size(59, 15);
            tensionL1Label.TabIndex = 0;
            tensionL1Label.Text = "Voltage:";
            // 
            // corrienteL1Label
            // 
            corrienteL1Label.AutoSize = true;
            corrienteL1Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL1Label.ForeColor = System.Drawing.Color.Black;
            corrienteL1Label.Location = new System.Drawing.Point(7, 36);
            corrienteL1Label.Name = "corrienteL1Label";
            corrienteL1Label.Size = new System.Drawing.Size(70, 15);
            corrienteL1Label.TabIndex = 0;
            corrienteL1Label.Text = "Corriente:";
            // 
            // cosenoPhiL1Label
            // 
            cosenoPhiL1Label.AutoSize = true;
            cosenoPhiL1Label.BackColor = System.Drawing.Color.Transparent;
            cosenoPhiL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL1Label.ForeColor = System.Drawing.Color.Black;
            cosenoPhiL1Label.Location = new System.Drawing.Point(17, 66);
            cosenoPhiL1Label.Name = "cosenoPhiL1Label";
            cosenoPhiL1Label.Size = new System.Drawing.Size(60, 15);
            cosenoPhiL1Label.TabIndex = 0;
            cosenoPhiL1Label.Text = "Cos Phi:";
            // 
            // potenciaAparenteL1Label
            // 
            potenciaAparenteL1Label.AutoSize = true;
            potenciaAparenteL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaAparenteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaAparenteL1Label.Location = new System.Drawing.Point(8, 186);
            potenciaAparenteL1Label.Name = "potenciaAparenteL1Label";
            potenciaAparenteL1Label.Size = new System.Drawing.Size(69, 15);
            potenciaAparenteL1Label.TabIndex = 0;
            potenciaAparenteL1Label.Text = "Pot. Apar:";
            // 
            // potenciaReactivaCapicitivaL1Label
            // 
            potenciaReactivaCapicitivaL1Label.AutoSize = true;
            potenciaReactivaCapicitivaL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaCapicitivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaCapicitivaL1Label.Location = new System.Drawing.Point(4, 156);
            potenciaReactivaCapicitivaL1Label.Name = "potenciaReactivaCapicitivaL1Label";
            potenciaReactivaCapicitivaL1Label.Size = new System.Drawing.Size(73, 15);
            potenciaReactivaCapicitivaL1Label.TabIndex = 0;
            potenciaReactivaCapicitivaL1Label.Text = "Pot. Reac:";
            // 
            // frecuenciaLabel
            // 
            frecuenciaLabel.AutoSize = true;
            frecuenciaLabel.BackColor = System.Drawing.Color.Transparent;
            frecuenciaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            frecuenciaLabel.ForeColor = System.Drawing.Color.Black;
            frecuenciaLabel.Location = new System.Drawing.Point(365, 235);
            frecuenciaLabel.Name = "frecuenciaLabel";
            frecuenciaLabel.Size = new System.Drawing.Size(39, 15);
            frecuenciaLabel.TabIndex = 0;
            frecuenciaLabel.Text = "Frec:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.Black;
            label2.Location = new System.Drawing.Point(19, 73);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(70, 13);
            label2.TabIndex = 54;
            label2.Text = "Serial Port:";
            // 
            // factorPotenciaL1Label
            // 
            factorPotenciaL1Label.AutoSize = true;
            factorPotenciaL1Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL1Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL1Label.Location = new System.Drawing.Point(49, 96);
            factorPotenciaL1Label.Name = "factorPotenciaL1Label";
            factorPotenciaL1Label.Size = new System.Drawing.Size(28, 15);
            factorPotenciaL1Label.TabIndex = 0;
            factorPotenciaL1Label.Text = "PF:";
            // 
            // cosenoPhiL1TextBox
            // 
            this.cosenoPhiL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cosenoPhiL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cosenoPhiL1TextBox.Location = new System.Drawing.Point(79, 63);
            this.cosenoPhiL1TextBox.Name = "cosenoPhiL1TextBox";
            this.cosenoPhiL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.cosenoPhiL1TextBox.TabIndex = 1;
            // 
            // corrienteL1TextBox
            // 
            this.corrienteL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.corrienteL1TextBox.Location = new System.Drawing.Point(79, 33);
            this.corrienteL1TextBox.Name = "corrienteL1TextBox";
            this.corrienteL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.corrienteL1TextBox.TabIndex = 1;
            // 
            // tensionL1TextBox
            // 
            this.tensionL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tensionL1TextBox.Location = new System.Drawing.Point(79, 3);
            this.tensionL1TextBox.Name = "tensionL1TextBox";
            this.tensionL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.tensionL1TextBox.TabIndex = 1;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.BackgroundImage = global::Dezac.Instruments.Properties.Resources.Actions_exit_icon;
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(25, 323);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(44, 38);
            this.btnReset.TabIndex = 6;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(13, 89);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(77, 21);
            this.cmb_Port.TabIndex = 0;
            this.cmb_Port.SelectedIndexChanged += new System.EventHandler(this.cmb_Port_SelectedIndexChanged);
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(103, 363);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(497, 11);
            this.pbStatus.TabIndex = 39;
            this.pbStatus.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // potenciaActivaL1TextBox
            // 
            this.potenciaActivaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaActivaL1TextBox.Location = new System.Drawing.Point(79, 123);
            this.potenciaActivaL1TextBox.Name = "potenciaActivaL1TextBox";
            this.potenciaActivaL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaActivaL1TextBox.TabIndex = 1;
            // 
            // potenciaAparenteL1TextBox
            // 
            this.potenciaAparenteL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaAparenteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaAparenteL1TextBox.Location = new System.Drawing.Point(79, 183);
            this.potenciaAparenteL1TextBox.Name = "potenciaAparenteL1TextBox";
            this.potenciaAparenteL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaAparenteL1TextBox.TabIndex = 1;
            // 
            // potenciaReactivaCapicitivaL1TextBox
            // 
            this.potenciaReactivaCapicitivaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaCapicitivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaReactivaCapicitivaL1TextBox.Location = new System.Drawing.Point(79, 153);
            this.potenciaReactivaCapicitivaL1TextBox.Name = "potenciaReactivaCapicitivaL1TextBox";
            this.potenciaReactivaCapicitivaL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaReactivaCapicitivaL1TextBox.TabIndex = 1;
            // 
            // frecuenciaTextBox
            // 
            this.frecuenciaTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.frecuenciaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frecuenciaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frecuenciaTextBox.Location = new System.Drawing.Point(409, 232);
            this.frecuenciaTextBox.Name = "frecuenciaTextBox";
            this.frecuenciaTextBox.Size = new System.Drawing.Size(71, 21);
            this.frecuenciaTextBox.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnRead);
            this.panel1.Controls.Add(this.btnSelection);
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.btnClearSel);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.btnShunt);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.btnAVG);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.btmSetConfigure);
            this.panel1.Controls.Add(this.txtIDN);
            this.panel1.Controls.Add(this.btnIDN);
            this.panel1.Controls.Add(this.groupBox12);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.frecuenciaTextBox);
            this.panel1.Controls.Add(frecuenciaLabel);
            this.panel1.Location = new System.Drawing.Point(103, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 343);
            this.panel1.TabIndex = 53;
            // 
            // btnIDN
            // 
            this.btnIDN.BackColor = System.Drawing.Color.Transparent;
            this.btnIDN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnIDN.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIDN.ForeColor = System.Drawing.Color.Black;
            this.btnIDN.Location = new System.Drawing.Point(3, 20);
            this.btnIDN.Name = "btnIDN";
            this.btnIDN.Size = new System.Drawing.Size(63, 27);
            this.btnIDN.TabIndex = 55;
            this.btnIDN.Text = "IDN";
            this.btnIDN.UseVisualStyleBackColor = false;
            this.btnIDN.Click += new System.EventHandler(this.btnIDN_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Transparent;
            this.groupBox12.Controls.Add(this.rb120A);
            this.groupBox12.Controls.Add(this.rb12A);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(351, 268);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(129, 48);
            this.groupBox12.TabIndex = 50;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Output Current";
            // 
            // rb120A
            // 
            this.rb120A.AutoSize = true;
            this.rb120A.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb120A.ForeColor = System.Drawing.Color.Black;
            this.rb120A.Location = new System.Drawing.Point(67, 20);
            this.rb120A.Name = "rb120A";
            this.rb120A.Size = new System.Drawing.Size(60, 20);
            this.rb120A.TabIndex = 1;
            this.rb120A.TabStop = true;
            this.rb120A.Text = "120A";
            this.rb120A.UseVisualStyleBackColor = true;
            // 
            // rb12A
            // 
            this.rb12A.AutoSize = true;
            this.rb12A.Checked = true;
            this.rb12A.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb12A.ForeColor = System.Drawing.Color.Black;
            this.rb12A.Location = new System.Drawing.Point(11, 20);
            this.rb12A.Name = "rb12A";
            this.rb12A.Size = new System.Drawing.Size(52, 20);
            this.rb12A.TabIndex = 0;
            this.rb12A.TabStop = true;
            this.rb12A.Text = "12A";
            this.rb12A.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(tensionL1Label);
            this.panel2.Controls.Add(this.factorPotenciaL1TextBox);
            this.panel2.Controls.Add(factorPotenciaL1Label);
            this.panel2.Controls.Add(this.potenciaAparenteL1TextBox);
            this.panel2.Controls.Add(potenciaAparenteL1Label);
            this.panel2.Controls.Add(this.potenciaReactivaCapicitivaL1TextBox);
            this.panel2.Controls.Add(potenciaReactivaCapicitivaL1Label);
            this.panel2.Controls.Add(this.cosenoPhiL1TextBox);
            this.panel2.Controls.Add(cosenoPhiL1Label);
            this.panel2.Controls.Add(this.corrienteL1TextBox);
            this.panel2.Controls.Add(corrienteL1Label);
            this.panel2.Controls.Add(this.tensionL1TextBox);
            this.panel2.Controls.Add(potenciaActivaL1Label);
            this.panel2.Controls.Add(this.potenciaActivaL1TextBox);
            this.panel2.Location = new System.Drawing.Point(329, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(157, 212);
            this.panel2.TabIndex = 8;
            // 
            // factorPotenciaL1TextBox
            // 
            this.factorPotenciaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factorPotenciaL1TextBox.Location = new System.Drawing.Point(79, 93);
            this.factorPotenciaL1TextBox.Name = "factorPotenciaL1TextBox";
            this.factorPotenciaL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.factorPotenciaL1TextBox.TabIndex = 1;
            // 
            // txtIDN
            // 
            this.txtIDN.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtIDN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIDN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDN.Location = new System.Drawing.Point(72, 26);
            this.txtIDN.Name = "txtIDN";
            this.txtIDN.ReadOnly = true;
            this.txtIDN.Size = new System.Drawing.Size(181, 21);
            this.txtIDN.TabIndex = 2;
            // 
            // btmSetConfigure
            // 
            this.btmSetConfigure.BackColor = System.Drawing.Color.Transparent;
            this.btmSetConfigure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmSetConfigure.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetConfigure.ForeColor = System.Drawing.Color.Black;
            this.btmSetConfigure.Location = new System.Drawing.Point(3, 67);
            this.btmSetConfigure.Name = "btmSetConfigure";
            this.btmSetConfigure.Size = new System.Drawing.Size(63, 27);
            this.btmSetConfigure.TabIndex = 56;
            this.btmSetConfigure.Text = "CFG";
            this.btmSetConfigure.UseVisualStyleBackColor = false;
            this.btmSetConfigure.Click += new System.EventHandler(this.btmSetConfigure_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(72, 72);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(85, 21);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(163, 72);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(90, 21);
            this.textBox2.TabIndex = 57;
            // 
            // btnAVG
            // 
            this.btnAVG.BackColor = System.Drawing.Color.Transparent;
            this.btnAVG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAVG.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAVG.ForeColor = System.Drawing.Color.Black;
            this.btnAVG.Location = new System.Drawing.Point(3, 111);
            this.btnAVG.Name = "btnAVG";
            this.btnAVG.Size = new System.Drawing.Size(63, 27);
            this.btnAVG.TabIndex = 58;
            this.btnAVG.Text = "AVG";
            this.btnAVG.UseVisualStyleBackColor = false;
            this.btnAVG.Click += new System.EventHandler(this.btnAVG_Click);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(72, 114);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(85, 21);
            this.textBox3.TabIndex = 59;
            // 
            // btnShunt
            // 
            this.btnShunt.BackColor = System.Drawing.Color.Transparent;
            this.btnShunt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnShunt.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShunt.ForeColor = System.Drawing.Color.Black;
            this.btnShunt.Location = new System.Drawing.Point(3, 159);
            this.btnShunt.Name = "btnShunt";
            this.btnShunt.Size = new System.Drawing.Size(63, 27);
            this.btnShunt.TabIndex = 60;
            this.btnShunt.Text = "SHUNT";
            this.btnShunt.UseVisualStyleBackColor = false;
            this.btnShunt.Click += new System.EventHandler(this.btnShunt_Click);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(72, 162);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(85, 21);
            this.textBox4.TabIndex = 61;
            // 
            // btnClearSel
            // 
            this.btnClearSel.BackColor = System.Drawing.Color.Transparent;
            this.btnClearSel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClearSel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearSel.ForeColor = System.Drawing.Color.Black;
            this.btnClearSel.Location = new System.Drawing.Point(3, 201);
            this.btnClearSel.Name = "btnClearSel";
            this.btnClearSel.Size = new System.Drawing.Size(63, 27);
            this.btnClearSel.TabIndex = 62;
            this.btnClearSel.Text = "CLEAR";
            this.btnClearSel.UseVisualStyleBackColor = false;
            this.btnClearSel.Click += new System.EventHandler(this.btnClearSel_Click);
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(72, 204);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(85, 21);
            this.textBox5.TabIndex = 63;
            // 
            // btnSelection
            // 
            this.btnSelection.BackColor = System.Drawing.Color.Transparent;
            this.btnSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSelection.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelection.ForeColor = System.Drawing.Color.Black;
            this.btnSelection.Location = new System.Drawing.Point(3, 244);
            this.btnSelection.Name = "btnSelection";
            this.btnSelection.Size = new System.Drawing.Size(63, 27);
            this.btnSelection.TabIndex = 64;
            this.btnSelection.Text = "SELECT";
            this.btnSelection.UseVisualStyleBackColor = false;
            this.btnSelection.Click += new System.EventHandler(this.btnSelection_Click);
            // 
            // btnRead
            // 
            this.btnRead.BackColor = System.Drawing.Color.Transparent;
            this.btnRead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRead.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRead.ForeColor = System.Drawing.Color.Black;
            this.btnRead.Location = new System.Drawing.Point(3, 285);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(63, 27);
            this.btnRead.TabIndex = 65;
            this.btnRead.Text = "READ";
            this.btnRead.UseVisualStyleBackColor = false;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // UC_PM100
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(label2);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnReset);
            this.Name = "UC_PM100";
            this.Size = new System.Drawing.Size(611, 383);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox tensionL1TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL1TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL1TextBox;
        private System.Windows.Forms.TextBox corrienteL1TextBox;
        private System.Windows.Forms.TextBox frecuenciaTextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL1TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL1TextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox factorPotenciaL1TextBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton rb120A;
        private System.Windows.Forms.RadioButton rb12A;
        private System.Windows.Forms.Button btnIDN;
        private System.Windows.Forms.TextBox txtIDN;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btmSetConfigure;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btnAVG;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button btnShunt;
        private System.Windows.Forms.Button btnClearSel;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button btnSelection;
        private System.Windows.Forms.Button btnRead;
    }
}
