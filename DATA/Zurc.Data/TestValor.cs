namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TestValor")]
    public partial class TestValor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TestFase")]
        public int TestFaseId { get; set; }
        [ForeignKey("Parametro")]
        public int? ParametroId { get; set; }

        [Required]
        public string Valor { get; set; }
        public string StepName { get; set; }
        public decimal? ValMax { get; set; }
        public decimal? ValMin { get; set; }
        public string ValorEsperado { get; set; }
        public int? UnidadId { get; set; }
        [ForeignKey("Unidad")]

        public virtual Parametro Parametro { get; set; }
        public virtual TestFase TestFase { get; set; }
        public virtual Unidad Unidad { get; set; }
    }
}
