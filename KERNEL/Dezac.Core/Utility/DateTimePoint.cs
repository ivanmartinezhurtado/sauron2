﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Core.Utility
{
    public class DataTimePoint
    {
        public string Source { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public DateTime Time { get; set; }

        public string Key { get { return string.Format("{0}/{1}", Source, Name); } }
    }

}
