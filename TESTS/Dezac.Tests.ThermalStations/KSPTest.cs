﻿using Dezac.Core.Utility;
using Dezac.Device.ThermalStations;
using Dezac.Services;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.ThermalStations
{
    [TestVersion(1.00)]
    public class KSPTest : TestBase
    {
        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        private KSP ksp;
        public KSP Ksp
        {
            get
            {
                if (ksp == null)
                    ksp  = new KSP(Comunicaciones.SerialPort);

                return ksp;
            }
        }


        private const string IMAGE_PATH = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\KSP\";

        private int idnNumberTest = 25;

        private KSP.TemperaturePatterns temperaturePatterns;
        private KSP.Temporizators temporizators;

        private const byte DETECTION_RELAY_Y = 11;
        private const byte DETECTION_RELAY_G = 12;
        private const byte _24V = 14;
        private const byte ALIM_CRONO_434MHz = 17;
        private const byte ALIM_CRONO_433MHz = 18;
        private const byte SIMULATION_ALARM = 0;
        private const byte SELECT_NTC_0C = 20;


        public void TestInitialization(byte comPort = 0)       
        {   

            Tower.MUX.Reset();
        }

        public void TestCommunications()
        {
            Tower.IO.DO.On(_24V, 18);
            Tower.Chroma.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V_AC.Null.Name, 230, ParamUnidad.V), 0, 50);
            
            SamplerWithCancel(
                (p) =>
                {
                    Ksp.FlagTest();
                    Delay(200, "Escribiendo Flag de Test");
                    return Ksp.ReadFlagTest();
                }, "Error, no se ha podido comunicar con el equipo", 5, 500, 1500, false, false);           

            if (Ksp.ReadIdentificationNumber() != idnNumberTest)
            {
                Ksp.WriteIdentificationNumber(idnNumberTest);
                Delay(500, "Escribiendo el identificador de test");
                Ksp.WriteIdentificationNumber(idnNumberTest);
                Delay(500, "Escribiendo el identificador de test");
                ResetHardware();

                SamplerWithCancel(
                (p) =>
                {
                    Ksp.FlagTest();
                    Delay(200, "Escribiendo Flag de Test");
                    return Ksp.ReadFlagTest();
                }, "Error, el equipo no ha entrado en FlagTest", 5, 500, 1500, false, false);
            }
                           
            var idTest = Ksp.ReadIdentificationNumber();
            Assert.AreEqual("ID_NUMBER_TEST", idTest, idnNumberTest, Error().UUT.CONFIGURACION.NO_GRABADO("Error, no se ha podido escribir el numero de Identificación de Test"), ParamUnidad.SinUnidad);      
                      
            TestInfo.FirmwareVersion = Ksp.ReadSoftwareVersion();
            var fwBBDD = Identificacion.VERSION_FIRMWARE; 
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, TestInfo.FirmwareVersion, fwBBDD, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, versión de firmware incorrecta"), ParamUnidad.SinUnidad);

            Ksp.WriteMotorMode(KSP.MotorModes.REPOSE);
            Ksp.DesactivateRelays();
            Ksp.InitGrids();
            Ksp.InitLeds();
        }

        public void TestRelays()
        {
            Ksp.FlagTest();

            var RelaysAndInputs = new Dictionary<KSP.Relays, byte>();
            RelaysAndInputs.Add(KSP.Relays.RELAY_Y, DETECTION_RELAY_Y);
            RelaysAndInputs.Add(KSP.Relays.RELAY_G, DETECTION_RELAY_G);

            foreach (KeyValuePair<KSP.Relays, byte> CurrentRelay in RelaysAndInputs)
            {
                Ksp.DesactivateRelays();

                SamplerWithCancel(
                    (p) =>
                    {
                        Ksp.ActivateRelay(CurrentRelay.Key);
                        Delay(200, "Activando Relé Actual");

                        if (!Tower.IO.DI[CurrentRelay.Value])
                            return false;  
                                     
                        return true;
                    }, string.Format("Error, no se ha detectado el {0} activado", CurrentRelay.Key.GetDescription()));

                Resultado.Set(CurrentRelay.Key.ToString() +"_ON", "OK", ParamUnidad.SinUnidad);

                SamplerWithCancel(
                  (p) =>
                  {
                      Ksp.DesactivateRelays();
                      Delay(200, "Desactivando relés");

                      if (Tower.IO.DI[CurrentRelay.Value])
                          return false;

                      return true;
                  }, string.Format("Error, no se ha detectado el {0} desactivado", CurrentRelay.Key.GetDescription()));

                Resultado.Set(CurrentRelay.Key.ToString()+"_OFF", "OK", ParamUnidad.SinUnidad);
            }          
        }

        public void TestControlRejas()
        {
            List<KSP.MotorModes> ModesList = new List<KSP.MotorModes>();
            ModesList.Add(KSP.MotorModes.OPENING);
            ModesList.Add(KSP.MotorModes.CLOSING);

            foreach (KSP.MotorModes mode in ModesList)
            {
                Ksp.WriteMotorMode(mode);

                var gridsValues = new Dictionary<KSP.AddresGrids, InputMuxEnum>();
                {
                    gridsValues.Add(KSP.AddresGrids.ADDR_GRID1, InputMuxEnum.IN5);
                    gridsValues.Add(KSP.AddresGrids.ADDR_GRID2, InputMuxEnum.IN4);
                    gridsValues.Add(KSP.AddresGrids.ADDR_GRID3, InputMuxEnum.IN3);
                    gridsValues.Add(KSP.AddresGrids.ADDR_GRID4, InputMuxEnum.IN2);
                    gridsValues.Add(KSP.AddresGrids.ADDR_GRID5, InputMuxEnum.IN5);
                    gridsValues.Add(KSP.AddresGrids.ADDR_GRID6, InputMuxEnum.IN4);
                }

                foreach (KeyValuePair<KSP.AddresGrids, InputMuxEnum> CurrentGrid in gridsValues)
                {
                    Ksp.ActivateGrid(CurrentGrid.Key);

                    var defs = new AdjustValueDef(Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_" + mode.GetDescription() + "_ACT").Name, 0, Params.V_DC.Null.TestPoint(mode.GetDescription() + "_ACT").Min(), Params.V_DC.Null.TestPoint(mode.GetDescription() + "_ACT").Max(), 0, 0, ParamUnidad.V);

                    if (CurrentGrid.Key == KSP.AddresGrids.ADDR_GRID5 || CurrentGrid.Key == KSP.AddresGrids.ADDR_GRID6)
                        Tower.IO.DO.On(43);

                    TestMeasureBase(defs, (step) =>
                    {
                        Ksp.ActivateGrid(CurrentGrid.Key);                  
                        return Tower.MeasureMultimeter(CurrentGrid.Value, MagnitudsMultimeter.VoltDC);
                    }, 0, 6, 500);

                    var defs2 = new AdjustValueDef(Params.V_DC.Null.TestPoint(CurrentGrid.Key.GetDescription() + "_" + mode.GetDescription() + "_DES").Name, 0, Params.V_DC.Null.TestPoint(mode.GetDescription() + "_DES").Min(), Params.V_DC.Null.TestPoint(mode.GetDescription() + "_DES").Max(), 0, 0, ParamUnidad.V);

                    TestMeasureBase(defs2, (step) =>
                    {
                        Ksp.DesactivateGrid(CurrentGrid.Key);
                        
                        return Tower.MeasureMultimeter(CurrentGrid.Value, MagnitudsMultimeter.VoltDC);
                    }, 0, 6, 500);

                    Tower.IO.DO.Off(43);
                }
            }

            Ksp.WriteMotorMode(KSP.MotorModes.REPOSE);
        }

        public void TestLeds()
        {
            var PATH = IMAGE_PATH + @"LEDS\LEDS.jpg";

            SamplerWithCancel(
                  (p) =>
                  {
                      foreach (KSP.LedsGridsMode ledsMode in Enum.GetValues(typeof(KSP.LedsGridsMode)))
                      {
                          Ksp.WriteLedsMode(ledsMode);

                          foreach (KSP.Leds led in Enum.GetValues(typeof(KSP.Leds)))
                          {
                              if (led == KSP.Leds.LED_RELAY_Y)
                              {
                                  Ksp.ActivateLed(KSP.Leds.ANY_LED);
                                  Ksp.ActivateRelay(KSP.Relays.RELAY_Y);
                              }
                              else
                                  Ksp.ActivateLed(led);
                              Delay(600, "Tiempo que permanecera el mismo led encendido");

                              if (led == KSP.Leds.LED_RELAY_Y)
                                  Ksp.DesactivateRelays();
                          }
                      }

                      if (Shell.ShowDialog("TEST DE LEDS", () => new ImageView("Funcionan todos los LEDS?", PATH), MessageBoxButtons.YesNo, "Funcionan todos los leds?") != DialogResult.Yes)
                          if (Shell.MsgBox("Quieres repetir para volver a comprobar la seqüencia de leds?", "Test LEDS", MessageBoxButtons.YesNo) == DialogResult.Yes)
                              return false;
                          else
                              throw new Exception("Error, secuencia de encendido/apagado de los leds incorrecta, falla algun led");
                      
                      return true;
                  }, "Error, secuencia de encendido/apagado de los leds incorrecta, falla algun led");

            Resultado.Set("LEDS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestSwitchMaquina()
        {
            var PATH = IMAGE_PATH + @"SWITCH\";

            foreach (KSP.SwitchPositions position in Enum.GetValues(typeof(KSP.SwitchPositions)))
            {
                var frm = this.ShowForm(() => new ImageView("Test Switch", PATH + position.ToString() + ".jpg"), MessageBoxButtons.AbortRetryIgnore, "Colocar switch como en la imagen");

                try
                {
                    SamplerWithCancel((p) =>
                    {
                        if (Ksp.ReadPositionSwitch() == (ushort)position)
                            return true;
                        return false;
                    }
                    , string.Format("Error, no se han detectado todos los switches en la posición {0}", position.GetDescription().Replace("_", " ")), 100, 500);
                }
                finally
                {
                    this.CloseForm(frm);
                }

                Resultado.Set(position.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestSwitchRotativo()
        {
            var PATH = IMAGE_PATH + @"ROTATIVE_SWITCH\";

            foreach (KSP.SwitchRotationPositions position in Enum.GetValues(typeof(KSP.SwitchRotationPositions)))
            {
                var frm = this.ShowForm(() => new ImageView("Test Teclado", PATH + position.GetDescription() + ".jpg"), MessageBoxButtons.AbortRetryIgnore, "Coloque el switch en la posicion indicada");

                try
                {
                    SamplerWithCancel((p) =>
                    {
                        if (Ksp.ReadPositionSwitchRotative() == (ushort)position)
                            return true;
                        return false;
                    }
                    , string.Format("No se detecta el switch rotativo en la posición {0}", position.GetDescription().Replace("_", " ")), 40, 500);
                }
                finally
                {
                    this.CloseForm(frm);
                }
                Resultado.Set(string.Format("ROTATIVE_SWITCH_{0}", position.ToString()), "OK", ParamUnidad.SinUnidad);
            }


            var frmEstadoInicial = this.ShowForm(() => new ImageView("Posicion Inicial", PATH + KSP.SwitchRotationPositions.POSITION6.GetDescription() + ".jpg"), MessageBoxButtons.AbortRetryIgnore, "Coloque el switch en la posicion indicada");
            try
            {
                SamplerWithCancel((p) =>
                {
                    if (Ksp.ReadPositionSwitchRotative() == (ushort)KSP.SwitchRotationPositions.POSITION6)
                        return true;
                    return false;
                }
                , "Error, no se ha detectado el switch rotativo en la posición 6 (Posicion inicial)", 40, 500);
            }
            finally
            {
                this.CloseForm(frmEstadoInicial);
            }
        }

        public void TestCalibrationSonda(int delFirst = 2, int initCount = 3, int samples = 6, int interval = 1100)
        {
            Tower.IO.DO.Off(SELECT_NTC_0C);

            var Rntc25 = (int)Configuracion.GetDouble(Params.RES.Null.Ajuste.TestPoint("25C").Name, 10000, ParamUnidad.Ohms);
            var Rntc0 = (int)Configuracion.GetDouble(Params.RES.Null.Ajuste.TestPoint("0C").Name, 33900, ParamUnidad.Ohms);

            var defsPatron25 = new AdjustValueDef(Params.TEMP.Null.TestPoint("NTC_25C").Name, 0, Params.TEMP.Null.TestPoint("NTC_25C").Min(), Params.TEMP.Null.TestPoint("NTC_25C").Max(), 0, 0, ParamUnidad.Puntos);
            Delay(5000, "Esperando que la ksp estabilize la medida");
            var ResPatron25 = Ksp.Calibration(Rntc25, delFirst, initCount, samples, interval,
                (value) =>
                {
                    TestMeasureBase(defsPatron25,
                    (step) =>
                    {
                        return value;
                    }, 0, 3, 2000);

                    defsPatron25.Value = value;
                    return defsPatron25.IsValid();
                });

            Assert.IsTrue(ResPatron25.Item1, Error().UUT.CALIBRACION.MARGENES("Error calibracion del equipo con el patron de 25 Grados fuera de margenes"));//medida de temperatura del equipo


            Tower.IO.DO.On(SELECT_NTC_0C);

            var defsPatron0 = new AdjustValueDef(Params.TEMP.Null.TestPoint("NTC_0C").Name, 0, Params.TEMP.Null.TestPoint("NTC_0C").Min(), Params.TEMP.Null.TestPoint("NTC_0C").Max(), 0, 0, ParamUnidad.Puntos);

            Delay(5000, "Esperando que la ksp estabilize la medida");
            var ResPatron0 = Ksp.Calibration(Rntc0, delFirst, initCount, samples, interval,
                (value) =>
                {
                    TestMeasureBase(defsPatron0,
                    (step) =>
                    {
                        return value;
                    }, 0, 3, 2000);

                    defsPatron0.Value = value;
                    return defsPatron0.IsValid();
                });

            Assert.IsTrue(ResPatron0.Item1, Error().UUT.CALIBRACION.MARGENES("Error calibracion del equipo con el patron de 0 Grados fuera de margenes"));

            Tower.IO.DO.On(SELECT_NTC_0C);

            var valorR1 = (ResPatron25.Item2 + ResPatron0.Item2) / 2;

            var ListPointsValues = new List<ushort>();
            
            foreach (KSP.PatternsValues patron in Enum.GetValues(typeof(KSP.PatternsValues)))
            {
                var defsPatrones = new AdjustValueDef(Params.TEMP.Null.TestPoint("PATRON" + patron.ToString()).Name, 0, Params.TEMP.Null.TestPoint("PATRON" + patron.ToString()).Min(), Params.TEMP.Null.TestPoint("PATRON" + patron.ToString()).Max(), 0, 0, ParamUnidad.Puntos);               
                double valuePoints = 0;

                TestMeasureBase(defsPatrones,
                    (step) =>
                    {
                        valuePoints = 1024 * ((int)patron / (valorR1 + (int)patron));
                        return valuePoints;
                    }, 0, 3, 2000);

                ListPointsValues.Add((ushort)valuePoints);
            }

            temperaturePatterns = new KSP.TemperaturePatterns();
            temperaturePatterns.Pattern_4C = ListPointsValues[9]; temperaturePatterns.Pattern60C = ListPointsValues[0]; temperaturePatterns.Pattern0C = ListPointsValues[8];
            temperaturePatterns.Pattern55C = ListPointsValues[1]; temperaturePatterns.Pattern27C = ListPointsValues[3]; temperaturePatterns.Pattern27C2 = ListPointsValues[3];
            temperaturePatterns.Pattern26C = ListPointsValues[4]; temperaturePatterns.Pattern26C2 = ListPointsValues[4]; temperaturePatterns.Pattern33C = ListPointsValues[2];
            temperaturePatterns.Pattern20C = ListPointsValues[6]; temperaturePatterns.Pattern7C = ListPointsValues[7];

            Ksp.WriteTemperaturePatterns(temperaturePatterns);

        }

        public void GrabarTemporizadores()
        {
            temporizators = new KSP.Temporizators();
            temporizators.TEMP0 = (ushort)Parametrizacion.GetDouble("TEMP0", 100, ParamUnidad.SinUnidad); 
            temporizators.TEMP1 = (ushort)Parametrizacion.GetDouble("TEMP1", 200, ParamUnidad.SinUnidad); 
            temporizators.TEMP2 = (ushort)Parametrizacion.GetDouble("TEMP2", 24000, ParamUnidad.SinUnidad); 
            temporizators.TEMP3 = (ushort)Parametrizacion.GetDouble("TEMP3", 100, ParamUnidad.SinUnidad);
            temporizators.TEMP4 = (ushort)Parametrizacion.GetDouble("TEMP4", 100, ParamUnidad.SinUnidad);
            temporizators.TEMP5 = (ushort)Parametrizacion.GetDouble("TEMP5", 10, ParamUnidad.SinUnidad); 
            temporizators.TEMP6 = (ushort)Parametrizacion.GetDouble("TEMP6", 10, ParamUnidad.SinUnidad);
            temporizators.TEMP7 = (ushort)Parametrizacion.GetDouble("TEMP7", 300, ParamUnidad.SinUnidad);
            temporizators.TEMP8 = (ushort)Parametrizacion.GetDouble("TEMP8", 300, ParamUnidad.SinUnidad); 
            temporizators.TEMP9 = (ushort)Parametrizacion.GetDouble("TEMP9", 300, ParamUnidad.SinUnidad);

            Resultado.Set("TEMP0", temporizators.TEMP0, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP1", temporizators.TEMP1, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP2", temporizators.TEMP2, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP3", temporizators.TEMP3, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP4", temporizators.TEMP4, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP5", temporizators.TEMP5, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP6", temporizators.TEMP6, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP7", temporizators.TEMP7, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP8", temporizators.TEMP8, ParamUnidad.SinUnidad);
            Resultado.Set("TEMP9", temporizators.TEMP9, ParamUnidad.SinUnidad);

            Ksp.WriteTemporizators(temporizators);
        }

        public void TestRadioFrecuency()
        {
            foreach (KSP.RadioFrequency crono in Enum.GetValues(typeof(KSP.RadioFrequency)))
            {
                Tower.IO.DO.Off(ALIM_CRONO_433MHz, ALIM_CRONO_434MHz);
                var Out = crono == KSP.RadioFrequency._433MHz ? ALIM_CRONO_433MHz : ALIM_CRONO_434MHz;
                Tower.IO.DO.On(ALIM_CRONO_433MHz);
                Tower.IO.DO.On(Out);

                Ksp.ResetCounters();
                Ksp.WriteFrecuencyWorker(crono);

                Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V_DC.Null.Name, 3, ParamUnidad.V), Consignas.GetDouble(Params.I_DC.Null.Name, 0.2, ParamUnidad.A));

                Delay(15000, "Espera CRONO");

                int numberFrames = Ksp.ReadCounters()[0];
                int numberFramesEspecified = (int)Margenes.GetDouble("NUMERO_TRAMAS_MIN", 250, ParamUnidad.F);

                if (crono == KSP.RadioFrequency._433MHz)
                    TestTunerTension();
                else
                    TestFrecuencyReference();

                Resultado.Set(string.Format("TRAMAS_RECIBIDAS{0}", crono.ToString()), numberFrames.ToString(), ParamUnidad.SinUnidad, numberFramesEspecified.ToString());

                Assert.AreGreater(numberFrames, numberFramesEspecified, Error().UUT.MEDIDA.NO_COINCIDE(string.Format("Error, el numero de tramas recibido a {0} es inferior al esperado", crono.GetDescription())));

                Resultado.Set(string.Format("COMUNICATIONS{0}", crono.GetDescription()), "OK", ParamUnidad.SinUnidad);

                Tower.LAMBDA.ApplyOffAndWaitStabilisation();
            }
        }
       
        public void TestWriteIdentifierNumber()
        {
            var idnNumberBBDD = 0;

            using (DezacService svc = new DezacService())
            {
                try
                {
                    idnNumberBBDD = (int)svc.GetIdentifierNumberKSP();
                }
                catch (Exception)
                {
                    throw new Exception("Error al intentar recuperar el identificador de la Base de Datos");
                }
            }
   
            Ksp.WriteIdentificationNumber(idnNumberBBDD);
            TestInfo.DeviceID = idnNumberBBDD.ToString();

            Resultado.Set("ID_NUMBER", TestInfo.DeviceID, ParamUnidad.SinUnidad);
            Delay(1000, "Escribiendo numero de identificador");
        }

        public void TestAlarms()
        {
            KSP.Alarms alarm = KSP.Alarms.ALARMA_CRUCE;
            
            Tower.IO.DO.On(SIMULATION_ALARM);
            Delay(500, "Uniendo terminales entrada de alarma");

            SamplerWithCancel((p) =>
            {
                alarm = Ksp.ReadAlarm();
                return alarm == KSP.Alarms.ALARMA_EXTERIOR;
            }, string.Format("Error, se ha detectado la {0} activada", alarm), 40, 500);


            Tower.IO.DO.Off(SIMULATION_ALARM);
            Delay(500, "Separando terminales entrada de alarma");

            SamplerWithCancel((p) =>
            {
                alarm = Ksp.ReadAlarm();
                return alarm == KSP.Alarms.SIN_ALARMA;
            }, string.Format("Error, se ha detectado la {0} activada", alarm), 40, 500);

            Resultado.Set("ALARMS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestSupervisionFinal()
        {
            Ksp.Reset();
            Delay(15000, "Resetenado el equipo");


            if (Shell.ShowDialog("Test Final", () => new ImageView("SUPERVISION FINAL", IMAGE_PATH + "LED_CPU.jpg"), MessageBoxButtons.YesNo, "Ha parpadeado alguno de los leds?") != DialogResult.No)
                throw new Exception("Error, grabación bloques de la EEPROM incorrecta");

            Resultado.Set("SUPERVISION_FINAL", "OK", ParamUnidad.SinUnidad);
        }

        public void TestCustomize()
        {
            Ksp.WriteSerialNumber(Convert.ToInt32(TestInfo.NumSerie));
            Delay(500, "Escribiendo el numero de serie");

            Ksp.Reset();
            Delay(15000, "Esperando que la KSP arranque y cargue la EEPROM");

            var idReaded = Ksp.ReadIdentificationNumber();
            Assert.AreEqual("ID_NUMBER", idReaded, Convert.ToInt32(TestInfo.DeviceID), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, el identificador leído es diferente al grabado"), ParamUnidad.SinUnidad);

            var temperarurePattersReading = Ksp.ReadTemperaturePatterns();
            Assert.AreEqual(temperaturePatterns, temperarurePattersReading, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, patrones de temperatura leídos diferentes a los grabados"));

            var temporizatorsReaded = Ksp.ReadTemporizators();
            Assert.AreEqual(temporizators, temporizatorsReaded, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, temporizadores leídos diferentes a los grabados"));

            var serialNumber = Ksp.ReadSerialNumber();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, serialNumber, TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, el numero de serie no coincide con el grabado"), ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (Ksp != null)
                Ksp.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();
                Tower.IO.DO.Off(_24V, ALIM_CRONO_434MHz, ALIM_CRONO_433MHz, SIMULATION_ALARM, SELECT_NTC_0C);
                Tower.Dispose();
            }
        }


        private void TestTunerTension()
        {
            var PATH = IMAGE_PATH + @"TESTPOINT\V_SINTONIA.jpg";
            var defs = new AdjustValueDef(Params.V.Null.TestPoint("SINTONIA").Name, 0, Params.V.Null.TestPoint("SINTONIA").Min(), Params.V.Null.TestPoint("SINTONIA").Max(), 0, 0, ParamUnidad.Puntos);
            var frm = this.ShowForm(() => new ImageView("Test Tension Sintonia", PATH), MessageBoxButtons.AbortRetryIgnore, "Colocar punta del útil en el TestPoint indicado");
            try
            {
                TestMeasureBase(defs, (step) =>
                {
                    return Tower.MeasureMultimeter(InputMuxEnum.IN6, MagnitudsMultimeter.VoltDC);
                }, 2, 40, 500);
            }
            finally
            {
                this.CloseForm(frm);
            }
        }

        private void TestFrecuencyReference()
        {
            var PATH = IMAGE_PATH + @"TESTPOINT\FREC_REF.jpg";
            Tower.IO.DO.Off(ALIM_CRONO_433MHz, ALIM_CRONO_434MHz);

            Ksp.WriteFrecuencyWorker(KSP.RadioFrequency._433MHz);

            Tower.IO.DO.On(ALIM_CRONO_433MHz);

            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V_DC.Null.Name, 3, ParamUnidad.V), Consignas.GetDouble(Params.I_DC.Null.Name, 0.2, ParamUnidad.A));

            var param = new AdjustValueDef(Params.FREQ.Null.TestPoint("REFERENCIA"), ParamUnidad.Hz);

            var frm = this.ShowForm(() => new ImageView("Test Frecuencia Referencia", PATH), MessageBoxButtons.AbortRetryIgnore, "Colocar punta del útil en el TestPoint indicado");
            try
            {

                TestMeasureBase(param, (step) =>
                {
                    return Tower.MeasureWithFrecuency(InputMuxEnum.IN6);
                }, 2, 40, 500);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        private void ResetHardware()
        {
            Tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(500, "");
            Tower.Chroma.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V_AC.Null.Name, 230, ParamUnidad.V), 0, 50);
            Delay(6000, "Espera que la ksp cargue la EEPROM");
        }
    }
}
