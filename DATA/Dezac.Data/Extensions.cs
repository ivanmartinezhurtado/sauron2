﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace Dezac.Data
{
    public static class Extensions
    {
        public static DbCommand CreateCommand(this DbContext db, string sql)
        {
            var cmd = db.Database.Connection.CreateCommand();

            cmd.CommandText = sql;
            cmd.CommandType = CommandType.StoredProcedure;

            return cmd;
        }

        public static int ExecuteCommand(this DbCommand cmd)
        {
            bool isOpen = cmd.Connection.State == ConnectionState.Open;

            try
            {
                if (!isOpen)
                    cmd.Connection.Open();

                return cmd.ExecuteNonQuery();
            } finally
            {
                if (!isOpen)
                    cmd.Connection.Close();
            }
        }

        public static DbParameter AddParameter(this DbCommand cmd, string name, DbType type, object value = null, ParameterDirection direction = ParameterDirection.Input, int size = 0)
        {
            if (type == DbType.Binary)
            {
                OracleParameter parameter2 = (OracleParameter)cmd.CreateParameter();
                parameter2.ParameterName = name;
                parameter2.OracleDbType = OracleDbType.Blob;
                parameter2.Value = value;
                cmd.Parameters.Add(parameter2);
                return (DbParameter)parameter2;
            }
            else
            {
                var p = cmd.CreateParameter();
                p.ParameterName = name;
                p.DbType = type;
                p.Value = value;
                p.Direction = direction;
                if (size > 0)
                    p.Size = size;

                cmd.Parameters.Add(p);

                return p;
            }
        }


        public static IQueryable<T> OrderByField<T>(this IQueryable<T> q, string SortField, bool Ascending)
        {
            var param = Expression.Parameter(typeof(T), "p");
            var prop = Expression.Property(param, SortField);
            var exp = Expression.Lambda(prop, param);
            string method = Ascending ? "OrderBy" : "OrderByDescending";
            Type[] types = new Type[] { q.ElementType, exp.Body.Type };
            var mce = Expression.Call(typeof(Queryable), method, types, q.Expression, exp);
            return q.Provider.CreateQuery<T>(mce);
        }

        public static bool IsAttachedTo(this ObjectContext context, object entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            ObjectStateEntry entry;
            if (context.ObjectStateManager.TryGetObjectStateEntry(entity, out entry))
                return (entry.State != EntityState.Detached);
            return false;
        }

        public static IQueryable<T> Fetch<T>(this IQueryable<T> q, string propertyName) where T : class
        {
            return q.Include(propertyName);
        }

        public static DataSet DataQuery(this DezacContext db, string sql, params object[] args)
        {
            try
            {
                using (var cn = new OracleConnection(db.Database.Connection.ConnectionString))
                {
                    cn.Open();
                    using (var cmd = cn.CreateCommand())
                    {
                        if (args != null && args.Length > 0)
                            sql = string.Format(sql, args);

                        cmd.CommandText = sql;

                        var adapter = new OracleDataAdapter((OracleCommand)cmd);
                        DataSet ds = new DataSet();

                        adapter.Fill(ds);

                        return ds;
                    }
                }
            }
            catch (Exception)
            {
                //return null;
                throw;
            }
        }
    }

    public partial class FASE
    {
        public const string VERIFICAR = "120";
    }

    public partial class T_PARAMETRO
    {
        public string TestPoint
        {
            get
            {
                return string.IsNullOrEmpty(DESCRIPCION) ? PARAM : DESCRIPCION;
            }
        }
    }
}
