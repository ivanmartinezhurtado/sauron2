﻿using Instruments.Cameras.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Instruments.Cameras.DTO
{
    public class CameraLucidSettings
    {
        public string SerialNumber { get; set; }
        [JsonIgnore]
        public int Width { get { if (Column2 != 0) return Column2 - Column1; else return 0; } }
        [JsonIgnore]
        public int Height { get { if (Row2 != 0) return Row2 - Row1; else return 0; } }
        [JsonIgnore]
        public int OffsetX { get { return Column1; } }
        [JsonIgnore]
        public int OffsetY { get { return Row1; } }
        public int Column1 { get; set; }
        public int Column2 { get; set; }
        public int Row1 { get; set; }
        public int Row2 { get; set; }
        public int ExposureTime { get; set; }
        public double FrameRate { get; set; }
        public bool GainAuto { get; set; }
        public double Gain { get; set; }
        public double RedOffset { get; set; }
        public double GreenOffset { get; set; }
        public double BlueOffset { get; set; }
        public int StreamPacketSize { get; set; }
    }
}
