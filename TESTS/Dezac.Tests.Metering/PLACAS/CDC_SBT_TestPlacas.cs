﻿using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;

namespace Dezac.Tests.Metering.Placas
{
    [TestVersion(1.02)]
    public class CDC_SBT_TestPlacas : TestBase
    {
        private TowerBoard tower;
        private CirwattDlms meter;

        private const int PILOTO_ERROR = 20;

        internal CirwattDlms Meter
        {
            get
            {
                if (meter == null)
                {
                    meter = AddInstanceVar(new CirwattDlms(), "CDC_SBT_PLACAS");
                    Resultado.Set("VERSION_DEVICE2", string.Format("{0}: {1}", Meter.GetType().Name, Meter.GetDeviceVersion()), ParamUnidad.SinUnidad);
                }

                return meter;
            }
        }

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }              
            tower.Active24VDC();
          
            tower.IO.DO.Off(PILOTO_ERROR);
        }

        public void TestCommunications(int comPort = 0)
        {
            if (comPort == 0)
                comPort = Comunicaciones.SerialPort;

            tower.IO.DO.On(17);

            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3, 0.3);

            Meter.SetPort(comPort, 115200);

            logger.InfoFormat("New CDC_SBT_TestPlacas by  COM PORT:{0}", comPort);

            Meter.Retries = 3;

            SamplerWithCancel((p) => { Meter.IniciarSesion(); return true; }, "Error en las comunicaciones DLMS con el equipo por el puerto OPTICO (TTL)", 2, 500); 

            var firmwareVersion = Meter.LeerVersionSW();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de firmware incorrecta"), ParamUnidad.SinUnidad);

            Meter.EscribirEstadoFabricacion(799);

            Meter.EscribirNumeroFabricacion((uint)TestInfo.NumBastidor.Value);
        }

        public void TestAjustePPM()
        {
            Meter.IniciarSesion();

            Meter.WriteActivateOuputs( CirwattDlms.enumOuputs.LED_PPM);
         
            tower.HP53131A
            .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
            .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.1) 
            .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 9);

            var ppm = tower.MeasureWithFrecuency(InputMuxEnum.IN10, 20000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);

            Assert.IsValidMaxMin(Params.FREQ.Null.RTC.Name, ppm, ParamUnidad.Hz, Params.FREQ.Null.RTC.Min(), Params.FREQ.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("RTC"));

            var ppmCalc = Math.Round(1  * ((1 /ppm) * 512), 9);
            var ppmRead = Meter.LeerPPM();
            var TempEquipo = Meter.LeerTemperatura();
            var PPM_Calc_Temp = (ppmCalc - (Math.Pow((TempEquipo - 25), 2) * (-39)) / 1000);

            Meter.EscribirPPM((short)PPM_Calc_Temp);

            ppmRead = Meter.LeerPPM();

            Assert.IsValidMaxMin(Params.PPM.Null.RTC.Name, ppmRead, ParamUnidad.PorCentage, Params.PPM.Null.RTC.Min(), Params.PPM.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("PPM"));
        }

        public void TestFinish()
        {
            if (Meter != null)
                Meter.CerrarSesion();

            if (tower != null)
            {
                tower.ShutdownSources();

                if (tower.IO != null)
                    tower.IO.DO.OffWait(21, 7);

                tower.Dispose();
            }
        }
    }

}
