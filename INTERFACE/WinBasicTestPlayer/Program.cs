﻿using log4net;
using System;
using System.Linq;
using System.Windows.Forms;
using WinTestPlayer.Services;

namespace WinTestPlayer
{
    static class Program
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        [STAThread]
        static void Main()
        {
            _logger.Info("Starting WinTaskPlayer...");

            var procs = System.Diagnostics.Process.GetProcessesByName("WinTestPlayer");
            if (procs.Count() > 1)
            {
               MessageBox.Show("Este progroma solo se puede ejecutar en solo dos instancia","!Atención! la aplicación ya esta abierta",  MessageBoxButtons.OK, MessageBoxIcon.Warning);
               return;
            }

            DI.Build();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(DI.Resolve<MainForm>());
        }
    }
}
