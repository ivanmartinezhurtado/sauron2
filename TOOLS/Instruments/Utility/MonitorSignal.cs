﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Instruments.Utility
{
    public class MonitorSignal
    {
        public int Interval { get; set; }
        public double? Min { get; set; }
        public double? Max { get; set; }
        public Func<double, bool> IsValid { get; set; }

        public Func<double> ReadValue { get; set; }

        private Timer timer;

        public delegate void MonitorSignalValueDelegate(object sender, MonitorValueEventArgs e);

        public event EventHandler<MonitorValueEventArgs> ValueReaded;
        public event EventHandler<MonitorValueEventArgs> ValueOutOfRange;

        private SynchronizationContext sc;

        public void Start()
        {
            sc = SynchronizationContext.Current;
            timer = new Timer(DoWork, this, Interval, Interval);
        }

        public void Stop()
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
        }

        private static void DoWork(object state)
        {
            MonitorSignal m = state as MonitorSignal;

            double value = m.ReadValue();

            m.OnValueReaded(value);
            m.ValidateValue(value);
        }

        protected void OnValueReaded(double value)
        {
            if (ValueReaded != null)
                sc.Post((s) => { ValueReaded(this, new MonitorValueEventArgs(value)); }, null);
        }

        protected void ValidateValue(double value)
        {
            if (ValueOutOfRange == null && !Min.HasValue && !Max.HasValue && IsValid == null)
                return;

            if (Min.GetValueOrDefault(value) > value ||
                Max.GetValueOrDefault(value) < value ||
                (IsValid != null && !IsValid(value)))
                ValueOutOfRange(this, new MonitorValueEventArgs(value));
        }

        public class MonitorValueEventArgs : EventArgs
        {
            public MonitorValueEventArgs(double value)
            {
                this.Value = value;
            }

            public double Value { get; set; }
        }
    }
}
