﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(HexadecimalStringToIntegerDescription))]
    public class HexadecimalStringToInteger : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número Hexadecimal a convertir")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if (DataOutput.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DataOutput.Key).Throw();
            
            string hexValue = DataInput.Value.ToString();

            int intValue = Convert.ToInt32(hexValue, 16);

            DataOutput.Value = intValue;
        }
    }

    public class HexadecimalStringToIntegerDescription : ActionDescription
    { 
        public override string Name { get { return "HexadecimalStringToInteger"; }}
        public override string Category { get { return "Actions/Variables/Format"; } }
        public override string Description { get { return "Este action sirve para pasar un número o una cadena de Hexadecimal a Decimal"; } }
        public override Image Icon { get { return Properties.Resources.format; } }
    }
}
