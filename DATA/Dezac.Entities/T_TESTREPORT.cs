namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTREPORT
    {
        public int NUMTEST { get; set; }
        public byte[] IMAGEDOC { get; set; }
    
        public virtual T_TEST T_TEST { get; set; }
    }
}
