﻿namespace Dezac.Tests.Forms
{
    partial class BienInputlView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.txtBien = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbUtil = new System.Windows.Forms.PictureBox();
            this.lblCommand = new System.Windows.Forms.Label();
            this.lblTipoBien = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUtil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoBien)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.radPanel1.Controls.Add(this.lblDescripcion);
            this.radPanel1.Controls.Add(this.txtBien);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.pbUtil);
            this.radPanel1.Controls.Add(this.lblCommand);
            this.radPanel1.Controls.Add(this.lblTipoBien);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(627, 707);
            this.radPanel1.TabIndex = 6;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.Location = new System.Drawing.Point(21, 72);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(592, 36);
            this.lblDescripcion.TabIndex = 30;
            // 
            // txtBien
            // 
            this.txtBien.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBien.Location = new System.Drawing.Point(190, 646);
            this.txtBien.Name = "txtBien";
            this.txtBien.Size = new System.Drawing.Size(245, 35);
            this.txtBien.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dezac.Tests.Properties.Resources.dezac;
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 43);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // pbUtil
            // 
            this.pbUtil.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbUtil.Location = new System.Drawing.Point(19, 116);
            this.pbUtil.Name = "pbUtil";
            this.pbUtil.Size = new System.Drawing.Size(592, 463);
            this.pbUtil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUtil.TabIndex = 26;
            this.pbUtil.TabStop = false;
            // 
            // lblCommand
            // 
            this.lblCommand.BackColor = System.Drawing.Color.Transparent;
            this.lblCommand.CausesValidation = false;
            this.lblCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCommand.Location = new System.Drawing.Point(81, 600);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(471, 32);
            this.lblCommand.TabIndex = 23;
            this.lblCommand.Text = "Introduzca el  número de bien solicitado";
            this.lblCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTipoBien
            // 
            this.lblTipoBien.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoBien.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.lblTipoBien.Location = new System.Drawing.Point(280, 39);
            this.lblTipoBien.Name = "lblTipoBien";
            this.lblTipoBien.Size = new System.Drawing.Size(68, 26);
            this.lblTipoBien.TabIndex = 7;
            this.lblTipoBien.Text = "Utillaje";
            // 
            // BienInputlView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Name = "BienInputlView";
            this.Size = new System.Drawing.Size(627, 707);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUtil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoBien)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel lblTipoBien;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.PictureBox pbUtil;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.TextBox txtBien;
    }
}
