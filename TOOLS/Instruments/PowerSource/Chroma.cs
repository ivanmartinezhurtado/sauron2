﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Instruments.Utility;
using Ivi.Visa.Interop;
using log4net;
using System;
using System.Globalization;
using System.Threading;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.17)]
    public class Chroma : PowerSourceACI, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Chroma));

        public enum PrivateCommands
        {
            SET_VOLTAGE_RANGE = Commands.USER_DEFINED_COMMANDS + 1,
            READ_BUSY = Commands.USER_DEFINED_COMMANDS + 2,
            SET_VOLTAGE_LIMIT = Commands.USER_DEFINED_COMMANDS + 3,
            SET_VOLTAGE_RANGE_AUTO = Commands.USER_DEFINED_COMMANDS + 4,
            SET_VOLTAGE_RANGE_61601 = Commands.USER_DEFINED_COMMANDS + 5,
        };

        public enum VoltageRangeEnum
        {
            LOW = 150,
            HIGH = 300,
            AUTO = 1,
        };

        public enum typeSourceEnum
        {
            _6404,
            _61601
        }

        public typeSourceEnum TypeSource { get; private set; }

        private FormattedIO488 _Gpib;
        private ResourceManager mgr;
        private string _alias = "CHROMA";
        private bool _IsOpen = false;
        private string idn = "";
        private const string APPLY_ON_OFF_COMMAND = "OUTP {0:0}";
        private const string PRESET_CONFIG_FRECUENCY_COMMAND = "FREQ {0:00.0}";
        private const string PRESET_CONFIG_VOLTAGE_COMMAND = "VOLT {0:000.0}";
        private const string PRESET_CONFIG_VOLTAGE_RANGE_COMMAND = "VOLT:RANG {0:000}";
        private const string PRESET_CONFIG_VOLTAGE_RANGE_AUTO = "VOLT:RANG:AUTO ON";
        private const string PRESET_CONFIG_VOLTAGE_RANGE_61601 = "VOLT:RANG {0}";
        private const string PRESET_CONFIG_CURRENT_PEAK_COMMAND = "CURR:PEAK {0:00.00}";
        private const string PRESET_CONFIG_CURRENT_LIMIT_COMMAND = "CURR:LIM {0:0.00}";
        private const string READ_CURRENT_COMMAND = "MEAS:CURR:AC?";
        private const string READ_VOLTAGE_COMMAND = "MEAS:VOLT:AC{0}?";
        private const string READ_FREQ_COMMAND = "MEAS:FREQ?";
        private const string READ_POWERFACTOR_COMMAND = "MEAS:POW:AC:PFAC?";
        private const string READ_POWER_COMMAND = "MEAS:POW:AC?";
        private const string READ_STATUS = "STAT:QUES?";
        private const string READ_SYSTEM_ERROR = ":SYST:ERR?";
        private const string CLEAR_COMMAND = "*CLS";
        private const string CLEAR_PROTECTION = "OUTP:PROT:CLE";
        private const string READ_BUSY_COMMAND = "*OPC?";
        private const string READ_IDN_COMMAND = "*IDN?";

        private CultureInfo enUS = new CultureInfo("en-US");

        //*****************************************************************

        private bool sendRange = false;

        public Chroma()
            : base(logger)
        {
            RetryStabilisation = 1;
            TimeOutStabilitation = 5000;

            if (_Gpib == null)
                _Gpib = new FormattedIO488(); 

            protocol = new StringProtocol();
            protocol.CaracterFinRx = "\n";
            protocol.CaracterFinTx = "\n";
            transport = new MessageTransport(protocol, new GpibPortAdapter(_Gpib), logger);
            mgr = new ResourceManager();
        }

        private void Open()
        {
            try
            {
                if ((_IsOpen == true) && (_Gpib.IO != null))
                {
                    Logger.Info("CHROMA Gpib.IO Is Open --> Close ");
                    _Gpib.IO.Close();
                }

                Logger.InfoFormat("CHROMA Gpib.IO with alias: {0}", _alias);
                _Gpib.IO = (IMessage)mgr.Open(_alias, AccessMode.NO_LOCK, 4000, "");
                _Gpib.IO.TerminationCharacterEnabled = true;
                _IsOpen = true;
            }
            catch(Exception ex)
            {
                Logger.ErrorFormat("CHROMA Gpib.IO Eception {0}", ex.Message);
                TestException.Create().HARDWARE.INSTRUMENTOS.DETECCION_INSTRUMENTO("Open GPIB.IO Failed").Throw();
            }

            GetIdentification();
            
            if (HasErrorReadSystemError())
                TestException.Create().HARDWARE.INSTRUMENTOS.DETECCION_INSTRUMENTO($"ERROR INTERNO FUENTE {Status.ErrorDescription}").Throw();
        }

        private void Close()
        {
            if ((_IsOpen == true) && (_Gpib.IO != null)) _Gpib.IO.Close();
            _IsOpen = false;
        }

        //*****************************************************************

        public void SetVoltageRange(VoltageRangeEnum Range = VoltageRangeEnum.AUTO)
        {
            if (TypeSource == typeSourceEnum._6404)
            {
                if (Range == VoltageRangeEnum.AUTO)
                    SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE_RANGE_AUTO, true));
                else
                    SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE_RANGE, true, (int)Range));
            }
            else
                SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE_RANGE_61601, true, Range.ToString("G")));

            sendRange = true;
        }
        
        public string GetIdentification()
        {
            bool succes = false;
            byte reint = 0;
            var message = StringMessage.Create(enUS, READ_IDN_COMMAND);
            do
            {
                try
                {
                    Logger.Info("CHROMA Read Identification");
                    var response = transport.SendMessage<StringMessage>(message);
                    var resp = response.Text.Split(',');
                    idn = string.Format("{0}, MODEL:{1}, SN={2}", resp[0], resp[1], resp[2]);
                    if (idn.Contains("61601"))
                        TypeSource = typeSourceEnum._61601;
                    else
                        TypeSource = typeSourceEnum._6404;

                    succes = true;
                }
                catch (Exception ex)
                {
                    reint++;
                    Logger.WarnFormat("Excepcion CHROMA por {0}", ex.Message);
                    Reset();
                }

            } while (!succes && reint < 2);

            if (!succes)
                TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(READ_IDN_COMMAND).Throw();

            return idn;

        }

        public void ApplyOn()
        {
            SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
            this.IsOn = true;
            SendCommand(new Command<Commands>(Commands.READ_STATUS, true));
        }

        protected override void PostActionWhenStabilizationError()
        {
            SendCommand(new Command<Commands>(Commands.RESET, true));
            base.PostActionWhenStabilizationError();
        }

        protected override void PostActionWhenInternalError()
        {
            SendCommand(new Command<Commands>(Commands.RESET, true));
            base.PostActionWhenInternalError();
        }

        //*******************************************************************

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            if (!_IsOpen)
                Open();

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:

                    if (!sendRange)
                        SetVoltageRange();

                    message = StringMessage.Create(enUS, PRESET_CONFIG_VOLTAGE_COMMAND, Presets.Voltage);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Voltage = Presets.Voltage;
                    break;
                case (int)Commands.SET_CURRENT:
                    if (TypeSource == typeSourceEnum._6404)
                    {
                        Presets.Current = 10;
                        Logger.Warn("El limite de corriente de pico maximo se configura a 10A para la fuente 6404.");
                        message = StringMessage.Create(enUS, PRESET_CONFIG_CURRENT_PEAK_COMMAND, Presets.Current);
                    }
                    else
                    {
                        Presets.Current = 4;
                        Logger.Warn("El limite de corriente se configura a 4A para la fuente 61601.");
                        message = StringMessage.Create(enUS, PRESET_CONFIG_CURRENT_LIMIT_COMMAND, Presets.Current);
                    }
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    break;
                case (int)Commands.SET_FREQ:
                    message = StringMessage.Create(enUS,PRESET_CONFIG_FRECUENCY_COMMAND, Presets.Freq);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Freq = Presets.Freq;
                    break;
                case (int)Commands.APPLY_CONFIG:
                    message = StringMessage.Create(enUS,APPLY_ON_OFF_COMMAND, "ON");//1
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)Commands.APPLY_OFF:
                    message = StringMessage.Create(enUS,APPLY_ON_OFF_COMMAND, "OFF");//0
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Voltage = 0;
                    DevicePreset.Freq = 0;
                    break;
                case (int)Commands.RESET:
                    message = StringMessage.Create(enUS, CLEAR_COMMAND);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    message = StringMessage.Create(enUS, CLEAR_PROTECTION);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)Commands.READ_VOLTAGE:
                    if (TypeSource.Equals(typeSourceEnum._61601))
                        message = StringMessage.Create(enUS, READ_VOLTAGE_COMMAND, "DC");
                    else
                        message = StringMessage.Create(enUS, READ_VOLTAGE_COMMAND, "");
                    response = transport.SendMessage<StringMessage>(message);
                    DeviceData.Voltage = Convert.ToDouble(response.Text,enUS);
                    break;
                case (int)Commands.READ_CURRENT:
                    message = new StringMessage { Text = READ_CURRENT_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);
                    DeviceData.Current = Convert.ToDouble(response.Text, enUS);
                    break;
                case (int)Commands.READ_POWER:
                    message = new StringMessage { Text = READ_POWER_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);
                    DeviceData.Power = Convert.ToDouble(response.Text, enUS);
                    break;
                case (int)Commands.READ_FREQ:
                    message = new StringMessage { Text = READ_FREQ_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);
                    DeviceData.Freq = Convert.ToDouble(response.Text, enUS);
                    break;
                case (int)Commands.READ_STATUS:
                    message = new StringMessage { Text = READ_STATUS };
                    response = transport.SendMessage<StringMessage>(message);
                    Status.HasError = response.Text.Replace("\r", "") != "0";
                    if (Status.HasError)
                    {
                        Enum.TryParse<QuestionableStatusRegister>(response.Text, out QuestionableStatusRegister errors);
                        if (errors == QuestionableStatusRegister.Peak_Current_Limit_Protection)
                        {
                            Logger.Warn("Error de corriente de pico maxima detectado");
                            Status.HasError = false;
                            Status.ErrorDescription = "";
                        }
                        else
                        {
                            Status.ErrorDescription = errors.ToString("G");
                            Thread.Sleep(1000);
                            SendCommand(new Command<Commands>(Commands.RESET, true));
                            break;
                        }
                    }
                    Status.HasError = HasErrorReadSystemError();
                    break;
                case (int)PrivateCommands.READ_BUSY:
                    message = StringMessage.Create(enUS, READ_BUSY_COMMAND);
                    response = transport.SendMessage<StringMessage>(message);
                    Status.IsBusy = Convert.ToBoolean(response.Text, enUS);
                    break;
                case (int)PrivateCommands.SET_VOLTAGE_RANGE:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_VOLTAGE_RANGE_COMMAND, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)PrivateCommands.SET_VOLTAGE_RANGE_AUTO:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_VOLTAGE_RANGE_AUTO);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)PrivateCommands.SET_VOLTAGE_RANGE_61601:
                    message = StringMessage.Create(enUS, PRESET_CONFIG_VOLTAGE_RANGE_61601, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                default:
                    logger.WarnFormat("COMMAND CHROMA {0} NOT IMPLEMENTED", command.ToString());
                    break;
            }
        }

        private bool HasErrorReadSystemError()
        {
            StringMessage message, response;
            message = new StringMessage { Text = READ_SYSTEM_ERROR };
            bool hasError = false;
            byte reint = 0;
            do
            {
                Logger.Info("CHROMA Read SYSTEM ERROR");
                response = transport.SendMessage<StringMessage>(message);
                var responseSplited = response.Text.Split(',');
                Status.HasError = responseSplited[0] != "0";
                if (Status.HasError)
                {
                    Status.ErrorDescription = responseSplited[1];
                    Logger.Warn(Status.ErrorDescription);
                    if (responseSplited[0] == "-410")
                        Reset();

                    Status.HasError = false;
                    Status.ErrorDescription = "";
                    reint++;
                    hasError = true;
                }
                else
                    hasError =  false;

            } while (hasError && reint < 2);

            return hasError;
        }

        protected void Reset()
        {
            logger.Warn("Error Detected. Trying to recover the Chroma.");
            SendCommand(new Command<Commands>(Commands.RESET, true));
            Logger.Info("Delay: Wainting for reset");
            Thread.Sleep(1000);
        }

        protected override bool ValidateEstabilization(double tolerance)
        {
            ReadVoltage();

            if (DevicePreset.Voltage == 0)
                return DeviceData.Voltage < VoltageOffMin;

            var Max = Math.Round(DevicePreset.Voltage + (DevicePreset.Voltage * (tolerance / 100)), 12);

            var Min = Math.Round(DevicePreset.Voltage - (DevicePreset.Voltage * (tolerance / 100)), 12);

            return (DeviceData.Voltage > Min) && (DeviceData.Voltage < Max);
        }

        public void Dispose()
        {
            Close();
            _Gpib = null;
            mgr = null;
            transport.Dispose();
        }

        [Flags]
        private enum QuestionableStatusRegister
        {
            Under_Voltage_Protection = 1,
            Short_Circuit_Protection = 2,
            Over_Temperature_Protection = 4 ,
            Over_Current_Protection = 256,
            Fan_Failure = 512,
            Over_Power_Protection = 1024,
            Peak_Current_Limit_Protection = 2048,
        }
    }
}
