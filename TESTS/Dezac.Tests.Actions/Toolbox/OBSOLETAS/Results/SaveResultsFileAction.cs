﻿using Dezac.Core.Utility;
using Dezac.Data;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using TaskRunner.Model;
using TaskRunner.Enumerators;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(SaveResultsFileActionDescription))]
    

    public class SaveResultsFileAction : ActionBase
    {
        public string PathResults { get; set; }

        public override void Execute()
        {
            if(string.IsNullOrEmpty(PathResults))
                PathResults = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            if (!data.TestInfo.NumBastidor.HasValue)
                return;

            if (context.ExecutionResult == TestExecutionResult.Aborted)
                return;

            if (context.GetSteps(StepExecutionResult.Failed).Count()==1)
                if (context.GetSteps(StepExecutionResult.Failed).FirstOrDefault().GetErrorMessage().Contains("PARALELISMO CANCELADO"))
                    return;

            data.TestInfo.SaveBBDD = false;

            if (!data.TestInfo.NumFabricacion.HasValue)
                data.TestInfo.NumFabricacion = null;

            int numBastidor = (int)data.TestInfo.NumBastidor.Value;
            string resultado = context.ExecutionResult == TestExecutionResult.Completed ? "O" : "E";

            T_TEST test = new T_TEST
            {
                NUMFABRICACION = data.TestInfo.NumFabricacion,
                NUMMATRICULA = numBastidor,
                NUMMATRICULATEST = numBastidor,
                FECHA = data.TestInfo.StartTime,
                NUMPRODUCTOTEST = data.NumProducto,
                VERSIONPRODUCTOTEST = data.Version,
                TIEMPO = Convert.ToInt32(data.TestInfo.Elapsed.TotalSeconds),
                RESULTADO = resultado,
            };  

            if (resultado == "O")
            {
                test.NROSERIE = data.TestInfo.NumSerie;
                test.NROSERIE = test.NROSERIE == "0" ? null : test.NROSERIE;
                test.NUMMATRICULA = numBastidor;
            }
            else
                test.NROSERIE = null;
                
            T_TESTFASE fase = new T_TESTFASE { 
                T_TEST = test, 
                IDFASE = data.IdFase, 
                IDOPERARIO = data.IdOperario,
                FECHA = data.TestInfo.StartTime,
                TIEMPO = Convert.ToInt32(data.TestInfo.Elapsed.TotalSeconds),
                RESULTADO = resultado,
                NUMCAJA = data.NumCajaActual,
                NOMBREPC = data.PC
            };

            foreach (var error in context.GetSteps(StepExecutionResult.Failed))
            {
                var errMsg = error.GetErrorMessage().Left(200);
                if (!errMsg.Contains("PARALELISMO CANCELADO"))
                    fase.T_TESTERROR.Add(new T_TESTERROR { PUNTOERROR = error.Step.Name.Left(50), EXTENSION = errMsg, NUMERROR = 87 });
            }

            data.Resultados.ForEach(p =>
            {
                if (p.Modified != null)
                    fase.T_TESTVALOR.Add(new T_TESTVALOR { NUMPARAM = p.IdParam, VALOR = p.Valor, VALMAX = p.Max.ToDecimal(), VALMIN = p.Min.ToDecimal(), EXPECTEDVALUE = p.ValorEsperado, STEPNAME = p.StepName, NUMUNIDAD = (int)p.IdUnidad });
            });

            foreach (var step in context.TestResult.StepResults)
                fase.T_TESTSTEP.Add(new T_TESTSTEP { 
                    STEPNAME = step.Name, 
                    TIMESTART = step.StartTime, 
                    TIMEEND = step.EndTime,
                    RESULT = step.ExecutionResult == StepExecutionResult.Passed ? "O" : "E",
                    EXCEPTION = step.Exception == null ? null : step.GetErrorMessage().Left(500),
                    GROUPNAME = step.Step.ReportGroupName
                });

            test.T_TESTFASE.Add(fase);

            GrabarDatos(test);

            context.ResultList.Add(string.Format("BD => Nº TEST: {0}, Nº FASE: {1}, Nª FABRICACIÓN: {2}, Nº RESULTS: {3}", 
                test.NUMTEST, fase.NUMTESTFASE, test.NUMFABRICACION, fase.T_TESTVALOR.Count));

            data.TestInfo.NumTest = test.NUMTEST;
            data.TestInfo.NumTestFase = fase.NUMTESTFASE;
        }

        public bool GrabarDatos(T_TEST test)
        {
            var fase = test.T_TESTFASE.FirstOrDefault();

            if (test.NUMTEST == 0)
                test.NUMTEST = Convert.ToInt32(DateTime.Now.ToString("yyMMddHHmm"));

            fase.NUMTESTFASE = test.NUMTEST;
            fase.NUMTEST = test.NUMTEST;

            int i = 0;
            foreach (var error in fase.T_TESTERROR)
                error.NROERROR = i++;

            string name = string.Format("{0}_{1}_{2:yyyyMMdd}_{2:HHmmss}.json",
                i > 0 ? "ERROR" : "OK",
                test.NUMMATRICULATEST,
                DateTime.Now);
            string fileName = Path.Combine(PathResults, name);

            var data = Serialize(test);

            File.WriteAllText(fileName, data);

            return true;
        }

        public static string Serialize(object data)
        {
            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            return JsonConvert.SerializeObject(data, settings);
        }
    }

    public class SaveResultsFileActionDescription : ActionDescription
    {
        public override string Name { get { return "SaveResultsFile"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.page_save_icon; } }
    }
}
