﻿using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class MK_DC : DeviceBase
    {
        public MK_DC()
        {
        }

        public MK_DC(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDevice Modbus { get; internal set; }

        public double Offset { get; set; }

        //**************************************************

        public ushort ReadDigitallInput()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.DIGITAL_INPUT);
        }

        public void WriteDigitalOutputs(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DIGITAL_OUTPUTS, value);
        }
        public enum Keys
        {
            TECLA_IZQUIERDA = 1,
            TECLA_DERECHA = 2,
        }

        public ushort ReadDigitalOutputs()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.DIGITAL_OUTPUTS);
        }

        public FirmwareVersion ReadSoftwareVersion()
        {
            LogMethod();
            return Modbus.Read<FirmwareVersion>();
        }

        public int ReadNumSerie()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.NUM_SERIE);
        }

        public void WriteNumSerie(UInt32 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.NUM_SERIE, (int)value);
        }

        public int ReadModelo()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.MODEL);
        }

        public void WriteModelo(int value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.MODEL, value);
        }

        public void WriteShuntType(bool value)
        {
            LogMethod();
            var valor = value ? 0 : 1;
            Modbus.Write((ushort)Registers.SHUNT_TYPE, (ushort)valor);
        }

        public void WriteShuntRelation(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SHUNT_RELATION, value);
        }

        public int ReadEnergy()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.ENERGY);
        }

        public int ReadBastidor()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public void WriteBastidor(int value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.BASTIDOR, value);
        }

        public ushort ReadErrorCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        public void WriteErrorCode(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ERROR_CODE, value);
        }

        public int ReadCRCFirmware()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.CRC);
        }

        public void WriteDisplay(DisplayConfigurations value)
        {
            LogMethod();
            Modbus.Write<Display>(displayConfigurations[value]);
        }

        //**************************************************
        //**************************************************

        public double ReadVoltage()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.VOLTAGE_L1) / 10D;
        }

        public double ReadCurrent()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.CURRENT_L1) / 100D;
        }

        public double ReadPowerActive()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.POWER_ACTIVE_L1);
        }

        public int ReadPointsVoltage()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.POINTS_VOLTAGE_L1);
        }

        public int ReadPointsCurrent()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.POINTS_CURRENT_L1);
        }

        public int ReadPointsPowerActive()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.POINTS_POWER_ACTIVE_L1);
        }


        public void WriteGainVoltage(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.GAIN_VOLTAGE, value);
        }

        public ushort ReadGainVoltage()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.GAIN_VOLTAGE);
        }


        public void WriteGainPowerActive(ushort value)
        {
            Modbus.Write((ushort)Registers.GAIN_POWER_ACTIVE, value);
        }

        public ushort ReadGainPowerActive()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.GAIN_POWER_ACTIVE);
        }


        public void WriteGainCurrent(ushort value)
        {
            Modbus.Write((ushort)Registers.GAIN_CURRENT, value);
        }

        public ushort ReadGainCurrent()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.GAIN_CURRENT);
        }


        public void WriteOffsetPowerActive(int value)
        {
            Modbus.WriteInt32((ushort)Registers.OFFSET_POWER_ACTIVE, value);
        }

        public int ReadOffsetPowerActive()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.OFFSET_POWER_ACTIVE);
        }


        public void WriteOffsetVoltage(int value)
        {
            Modbus.WriteInt32((ushort)Registers.OFFSET_VOLTAGE, value);
        }

        public int ReadOffsetVoltagee()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.OFFSET_VOLTAGE);
        }


        public void WriteOffsetCurrent(int value)
        {
            Modbus.WriteInt32((ushort)Registers.OFFSET_CURRENT, value);
        }

        public int ReadOffsetCurrent()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.OFFSET_CURRENT);
        }


        public void ResetEnergiesSetup()
        {
            Modbus.WriteSingleCoil((ushort)Registers.RESET_ENERGIES, true);
        }

        //**************************************************
        //**************************************************
        public struct Ganancias
        {
            public ushort Variable;
            public int VariableOffset;
        }

        public struct PuntosVoltageGanancia
        {
            public double Voltage;
            public double puntosVoltage;
        }

        public struct PuntosCorrienteGanancia
        {
            public double Voltage;
            public double Corriente;
            public double puntosCorriente;
        }

        public Tuple<bool, Ganancias> CalculateVoltageAdjust(PuntosVoltageGanancia voltage0, PuntosVoltageGanancia voltage280, Func<Ganancias, bool> gainVoltageValidation)
        {
            //GananciaV = (PuntosV - OffsetPuntosV) / (Tension - Offset)
            var gananciaV = (voltage280.puntosVoltage - voltage0.puntosVoltage) / (voltage280.Voltage - voltage0.Voltage);

            var ganaciasVOffset = new Ganancias();
            ganaciasVOffset.Variable = Convert.ToUInt16(gananciaV);

            //Offset = PuntosV - (GanaciaV * Tension)
            var offsetVoltage = voltage280.puntosVoltage - (ganaciasVOffset.Variable * voltage280.Voltage);
            ganaciasVOffset.VariableOffset = Convert.ToInt32(offsetVoltage);

            var result = gainVoltageValidation(ganaciasVOffset);
            if (result)
            {
                WriteGainVoltage(ganaciasVOffset.Variable);
                WriteOffsetVoltage(ganaciasVOffset.VariableOffset);
            }

            return Tuple.Create(result, ganaciasVOffset);
        }

        public Tuple<bool, Ganancias> CalculateCorrienteAdjust(PuntosCorrienteGanancia Corriente0, PuntosCorrienteGanancia Corriente280, Func<Ganancias, bool> gainCurrnetValidation, bool isShunt)
        {
            double gananciaCorriente = 0D;
            double offsetCorriente = 0D;

            if (isShunt)
                gananciaCorriente = ((Corriente280.puntosCorriente - Corriente0.puntosCorriente) * 0.6) / ((Corriente280.Corriente - Corriente0.Corriente) * 1000);
            else
                gananciaCorriente = (Corriente280.puntosCorriente - Corriente0.puntosCorriente) / (Corriente280.Corriente - Corriente0.Corriente);

            var ganaciasCorrienteOffset = new Ganancias();
            ganaciasCorrienteOffset.Variable = Convert.ToUInt16(gananciaCorriente);

            if (isShunt)
                offsetCorriente = Corriente280.puntosCorriente - ((ganaciasCorrienteOffset.Variable * Corriente280.Corriente *1000) / 0.6);
            else
                offsetCorriente = Corriente280.puntosCorriente - (ganaciasCorrienteOffset.Variable * Corriente280.Corriente);

            ganaciasCorrienteOffset.VariableOffset = Convert.ToInt32(offsetCorriente);

            var result = gainCurrnetValidation(ganaciasCorrienteOffset);
            if (result)
            {
                WriteGainCurrent(ganaciasCorrienteOffset.Variable);
                WriteOffsetCurrent(ganaciasCorrienteOffset.VariableOffset);
            }

            return Tuple.Create(result, ganaciasCorrienteOffset);
        }

        public Dictionary<DisplayConfigurations, Display> displayConfigurations = new Dictionary<DisplayConfigurations, Display> 
        { 
            { DisplayConfigurations.TODOS_SEGMENTOS_DESACTIVADOS, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 =0x0000} },
            { DisplayConfigurations.TODOS_SEGMENTOS_ACTIVADOS, new Display{ Parte1 = 0xFFFF, Parte2 = 0xFFFF, Parte3 = 0xFFFF } },
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.DISPLAY)]
        public struct Display
        {
            public UInt16 Parte1;
            public UInt16 Parte2;
            public UInt16 Parte3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VERSION_FIRMWARE)]
        public struct FirmwareVersion
        {
            public UInt16 Major;
            public UInt16 Minor;
            public UInt16 Revision;
        };

        public struct TriLinePower
        {
            public Double L1;
            public Double L2;
            public Double L3;
        }

        public struct TriLineGain
        {
            public UInt16 L1;
            public UInt16 L2;
            public UInt16 L3;
        }

        public enum Registers
        {
            DIGITAL_INPUT = 0x0020,
            DIGITAL_OUTPUTS = 0x0030,
            VERSION_FIRMWARE = 0x0050,
            NUM_SERIE = 0xF000,
            MODEL = 0xF010,
            BASTIDOR = 0xF040,
            ERROR_CODE = 0xF060,
            DISPLAY = 0xF090,
            CRC = 0xF120,

            //***************
            SHUNT_TYPE = 0x2000,
            SHUNT_RELATION = 0x2001,
            ENERGY = 0x0708,
            VOLTAGE_L1 = 0x0732,
            CURRENT_L1 = 0x0738,
            POWER_ACTIVE_L1 = 0x0746,

            POINTS_VOLTAGE_L1 = 0xF311,
            POINTS_POWER_ACTIVE_L1 = 0xF321,
            POINTS_CURRENT_L1 = 0xF341,

            GAIN_VOLTAGE = 0xF411,
            GAIN_POWER_ACTIVE = 0xF421,
            OFFSET_POWER_ACTIVE = 0xF441,
            OFFSET_VOLTAGE = 0xF4A1,
   
            GAIN_CURRENT = 0xF4B1,
            OFFSET_CURRENT = 0xF4C1,

            RESET_ENERGIES = 0xF000,
        };

        public enum DisplayConfigurations
        {
            TODOS_SEGMENTOS_ACTIVADOS,
            TODOS_SEGMENTOS_DESACTIVADOS,
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}
