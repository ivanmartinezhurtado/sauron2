﻿using Dezac.Core.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Management;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WMI_BluetoothDescription))]
    public class WMI_Bluetooth : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Habilitamos o deshabilitamos Bluetooth")]
        public bool Status { get; set; }

        public override void Execute()
        {
            string comportInfo = string.Empty;

            var wmiQuery = new SelectQuery("SELECT * FROM Win32_NetworkProtocol");
            var searchProcedure = new ManagementObjectSearcher(wmiQuery);

            foreach (ManagementObject item in searchProcedure.Get())
                if (item["Description"].ToString().ToUpper().Contains("BLUETOOTH"))
                    return;

            Error().UUT.HARDWARE.BLUETOOTH("No se encuentra ningun dispositivo Bluetooth").Throw();
        }
    }

    public class WMI_BluetoothDescription : ActionDescription
    {
        public override string Name { get { return "WMIBluetooth"; } }
        public override string Category { get { return "Tools/Comunications/Bluetooth"; } }
        public override string Description { get { return "Action que sirve para habilitar el bluetooth o deshabilitarlo"; } }
        public override Image Icon { get { return Properties.Resources.bluetooth_icon; } }
    }
}