﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(SerialPortCheckBridgeCommunicationActionDescription))]
    public class SerialPortCheckBridgeCommunicationAction : ActionBase
    {
        [Description("Texto a enviar")]
        [Category("3. Configuration")]
        public string TextToSend { get; set; }

        [Description("Nombre del dispositivo que transmite")]
        [Category("3. Configuration")]
        public string SendingDeviceName { get; set; }

        [Description("Nombre del dispositivo que recibe")]
        [Category("3. Configuration")]
        public string ReceivingDeviceName { get; set; }

        [Description("Nombre del puerto del dispositivo que transmite")]
        [Category("3. Configuration")]
        public ActionVariable SendingDevicePort { get; set; } = new ActionVariable();

        [Description("Nombre del puerto del dispositivo que recibe")]
        [Category("3. Configuration")]
        public ActionVariable ReceivingDevicePort { get; set; } = new ActionVariable();


        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });

            try
            {
                using (SerialPort sender = new SerialPort())
                {
                    sender.PortName = "COM" + SendingDevicePort.Value.ToString();
                    sender.WriteTimeout = 1000;

                    using (var receiver = new SerialPort())
                    {
                        receiver.PortName = "COM" + ReceivingDevicePort.Value.ToString();
                        receiver.ReadTimeout = 5000;

                        sender.Open();
                        receiver.Open();

                        sender.WriteLine(TextToSend);

                        var receivedString = receiver.ReadLine();

                        if (receivedString != TextToSend)
                            Error().UUT.COMUNICACIONES.NO_CORRESPONDE_CON_ENVIO("La trama leída no se corresponde con la enviada").Throw();

                        testBase.Resultado.Set(string.Format("{0}  to {1}", SendingDeviceName, ReceivingDeviceName), "OK", ParamUnidad.SinUnidad);
                    }
                }
            }catch(Exception e)
            {
                testBase.Resultado.Set(string.Format("{0}  to {1}", SendingDeviceName, ReceivingDeviceName), "ERROR", ParamUnidad.SinUnidad);
                Error().PROCESO.ACTION_EXECUTE.EXCEPTION_NOT_CONTROLLED(e.Message).Throw();
                
            }
        }
    }

    public class SerialPortCheckBridgeCommunicationActionDescription : ActionDescription
    {
        public override string Name { get { return "CheckBridgeCommunication"; } }
        public override string Description { get { return "Envia una línea de texto por un puerto serie y comprueba que otro puerto serie reciba la misma línea"; } }
        public override string Category { get { return "Tools/Comunications/ASCII"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
