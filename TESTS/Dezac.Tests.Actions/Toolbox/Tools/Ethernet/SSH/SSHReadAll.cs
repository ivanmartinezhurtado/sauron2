﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Kernel;
using Renci.SshNet;
using System;
using System.ComponentModel;
using System.Drawing;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(SSHReadAllActionDescription))]
    public class SSHReadAll : ActionBase
    {
        [DefaultValue("SSH_Device")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }


        [Category("3. Configuration")]
        [Description("TimeOut en milisegundos")]
        public ActionVariable TimeOut { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var ssh = GetVariable(DeviceName.Replace('-','_')) as SSHClient;
            
            if (ssh == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("SSHClient").Throw();

            testBase.SamplerWithCancel((p) =>
            {
                var response = ssh.Shell.Expect(string.Empty, TimeSpan.FromMilliseconds(TimeOut.GetValue<int>()));

                if (response == null)
                    return false;

                Context.Variables.AddOrUpdate(DataOutput.Key.ToString(), response);
                Context.ResultList.Add(response);
                testBase.Logger.DebugFormat(response);

                return true;

            }, "No se ha encontrado el texto esperado en la respuesta por SSH", 5, 100, 500);
        }
    }    

    public class SSHReadAllActionDescription : ActionDescription
    {
        public override string Name { get { return "SSHShellReadAll"; } }
        public override string Description { get { return "Lee toda la respuesta por una tty de la sesión SSH y almacena el resultado en una variable"; } }
        public override string Category { get { return "Tools/Ethernet/SSH"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "SSHInitialization;SSHFinish:2"; } }
    }
}
