namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ASISTENCIATECNICA
    {
        public int NUMASISTENCIA { get; set; }
        public int NUMPRODUCTO { get; set; }
        public int NUMLIN { get; set; }
        public string IDOPERARIO { get; set; }
        public Nullable<int> NUMPEDIDO { get; set; }
        public Nullable<int> NUMLINPEDIDO { get; set; }
        public string NUMEROSERIE { get; set; }
        public System.DateTime FECHAALTA { get; set; }
        public Nullable<System.DateTime> FECHAFIN { get; set; }
        public Nullable<System.DateTime> FECHAFINPREV { get; set; }
        public string ABIERTA { get; set; }
        public string GARANTIA { get; set; }
        public string CARGO { get; set; }
        public string CAUSA_1 { get; set; }
        public string CAUSA_2 { get; set; }
        public string TRABAJO_1 { get; set; }
        public string TRABAJO_2 { get; set; }
        public Nullable<decimal> HORAS { get; set; }
        public Nullable<decimal> PRECIO { get; set; }
        public string NOTAS { get; set; }
        public string AVERIACOMUNICADA { get; set; }
        public Nullable<System.DateTime> TIME_ID { get; set; }
        public string OFERTA_SINO { get; set; }
        public Nullable<System.DateTime> FECHAOFERTA { get; set; }
        public Nullable<int> NUMDOC { get; set; }
        public string ESTADOEQUIPO { get; set; }
        public string DEFECTUOSOEN { get; set; }
        public string IDDEFECTO { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
        public Nullable<int> NUMTEST { get; set; }
        public string NROREPARACION { get; set; }
        public Nullable<int> NS_ALBARAN { get; set; }
        public Nullable<int> NS_PEDIDO { get; set; }
        public Nullable<int> NS_LINEA { get; set; }
        public Nullable<int> NS_PARTE { get; set; }
        public Nullable<int> NS_ORDEN { get; set; }
        public Nullable<int> NS_NUMFABRICACION { get; set; }
        public string UBICACION { get; set; }
        public string ESTADO { get; set; }
        public string NUEVONROSERIE { get; set; }
        public string NOTASLOG { get; set; }
        public string RESPONSABLE { get; set; }
        public Nullable<int> NUMREPROCESO { get; set; }
    
        public virtual OPERARIO OPERARIO { get; set; }
        public virtual MATRICULA MATRICULA { get; set; }
        public virtual T_TEST T_TEST { get; set; }
        public virtual ARTICULO ARTICULO { get; set; }
    }
}
