﻿namespace WinTestPlayer.Views
{
    partial class PrinterConfigView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.ddlPrintersCaracteristicas = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.ddlPrintersIndividual = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ddlPrintersConjunto = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlPrintersLabel = new Telerik.WinControls.UI.RadDropDownList();
            this.lblCommand = new System.Windows.Forms.Label();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersCaracteristicas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersIndividual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersConjunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.radPanel1.Controls.Add(this.ddlPrintersCaracteristicas);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Controls.Add(this.ddlPrintersIndividual);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.pictureBox2);
            this.radPanel1.Controls.Add(this.ddlPrintersConjunto);
            this.radPanel1.Controls.Add(this.ddlPrintersLabel);
            this.radPanel1.Controls.Add(this.lblCommand);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(687, 425);
            this.radPanel1.TabIndex = 6;
            // 
            // ddlPrintersCaracteristicas
            // 
            this.ddlPrintersCaracteristicas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPrintersCaracteristicas.Location = new System.Drawing.Point(239, 235);
            this.ddlPrintersCaracteristicas.Margin = new System.Windows.Forms.Padding(4);
            this.ddlPrintersCaracteristicas.Name = "ddlPrintersCaracteristicas";
            this.ddlPrintersCaracteristicas.Size = new System.Drawing.Size(377, 24);
            this.ddlPrintersCaracteristicas.TabIndex = 26;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel4.Location = new System.Drawing.Point(65, 230);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(146, 27);
            this.radLabel4.TabIndex = 25;
            this.radLabel4.Text = "Características";
            // 
            // ddlPrintersIndividual
            // 
            this.ddlPrintersIndividual.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPrintersIndividual.Location = new System.Drawing.Point(239, 287);
            this.ddlPrintersIndividual.Margin = new System.Windows.Forms.Padding(4);
            this.ddlPrintersIndividual.Name = "ddlPrintersIndividual";
            this.ddlPrintersIndividual.Size = new System.Drawing.Size(377, 24);
            this.ddlPrintersIndividual.TabIndex = 31;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel2.Location = new System.Drawing.Point(23, 282);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(188, 27);
            this.radLabel2.TabIndex = 30;
            this.radLabel2.Text = "Embalaje Individual";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WinTestPlayer.Properties.Resources._2_Hot_Printer_icon;
            this.pictureBox2.Location = new System.Drawing.Point(625, 18);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 43);
            this.pictureBox2.TabIndex = 29;
            this.pictureBox2.TabStop = false;
            // 
            // ddlPrintersConjunto
            // 
            this.ddlPrintersConjunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPrintersConjunto.Location = new System.Drawing.Point(239, 346);
            this.ddlPrintersConjunto.Margin = new System.Windows.Forms.Padding(4);
            this.ddlPrintersConjunto.Name = "ddlPrintersConjunto";
            this.ddlPrintersConjunto.Size = new System.Drawing.Size(377, 24);
            this.ddlPrintersConjunto.TabIndex = 25;
            // 
            // ddlPrintersLabel
            // 
            this.ddlPrintersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPrintersLabel.Location = new System.Drawing.Point(239, 183);
            this.ddlPrintersLabel.Margin = new System.Windows.Forms.Padding(4);
            this.ddlPrintersLabel.Name = "ddlPrintersLabel";
            this.ddlPrintersLabel.Size = new System.Drawing.Size(377, 24);
            this.ddlPrintersLabel.TabIndex = 24;
            // 
            // lblCommand
            // 
            this.lblCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCommand.BackColor = System.Drawing.Color.Transparent;
            this.lblCommand.CausesValidation = false;
            this.lblCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCommand.Location = new System.Drawing.Point(8, 96);
            this.lblCommand.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(670, 39);
            this.lblCommand.TabIndex = 23;
            this.lblCommand.Text = "Configurar impresoras";
            this.lblCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel1.Location = new System.Drawing.Point(28, 341);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(183, 27);
            this.radLabel1.TabIndex = 19;
            this.radLabel1.Text = "Embalaje Conjunto";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinTestPlayer.Properties.Resources.dezac;
            this.pictureBox1.Location = new System.Drawing.Point(28, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(196, 53);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel3.Location = new System.Drawing.Point(47, 178);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(164, 27);
            this.radLabel3.TabIndex = 7;
            this.radLabel3.Text = "Número de Serie";
            // 
            // PrinterConfigView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PrinterConfigView";
            this.Size = new System.Drawing.Size(687, 425);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersCaracteristicas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersIndividual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersConjunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPrintersLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCommand;
        private Telerik.WinControls.UI.RadDropDownList ddlPrintersConjunto;
        private Telerik.WinControls.UI.RadDropDownList ddlPrintersLabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Telerik.WinControls.UI.RadDropDownList ddlPrintersIndividual;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddlPrintersCaracteristicas;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}
