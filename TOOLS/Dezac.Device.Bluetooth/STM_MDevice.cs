﻿using Dezac.Device.Bluetooth.Utils;
using System;

namespace Dezac.Device.Bluetooth
{
    public class STM_MDevice : BluetoothDevice
    {
        public static Guid UNKNOWN_SERVICE_RS485_UUID = new Guid("93A9CAE0-D07A-11E8-BDCB-0800275AA6E5");
        public static Guid CHARACTERISTIC_RTD_SELECTED = new Guid("AA2E2C3E-DB79-11E8-9F8B-F2801F1B9FD1");

        public static Guid UNKNOWN_SERVICE_CIRLORA_UUID = new Guid("BE7E1E82-D846-11E8-9F8B-F2801F1B9FD1");

        public static string DEFAULT_PIN = "010101";

        public static STM_MDevice Find(string serialNumber, int timeOut = 10000)
        {
            var result = BluetoothHelper.Scan(string.Format("STM-{0}", serialNumber), timeOut);
            if (result != null)
                return new STM_MDevice { BluetoothAddress = result.BluetoothAddress, PIN = DEFAULT_PIN };

            return null;
        }

        public STM_MDevice()
        {
        }

        public STM_MDevice(string name)
            : base(name)
        {
        }

        public byte ReadRtdSlectedAsync()
        {
            return ReadByte(UNKNOWN_SERVICE_RS485_UUID, CHARACTERISTIC_RTD_SELECTED);
        }

        public bool WriteRtdSlected(byte value)
        {
            return Write(UNKNOWN_SERVICE_RS485_UUID, CHARACTERISTIC_RTD_SELECTED, value);
        }     
    }
}
