﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(LAC25InitDescription))]
    

    public class LAC25Init : ActionBase
    {
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();;

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");

            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            tower.Actuator_LAC25.InitLAC25();
        }
    }

    public class LAC25InitDescription : ActionDescription
    {
        public override string Name { get { return "LAC25 Init"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Transport_Engine_icon; } }
    }
}
