﻿using Dezac.Data.ViewModels;
using Dezac.Tests.Laser.Controls;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Laser
{
    public class LaserPreparation 
    {
        public void InformationShow(List<LaserItemViewModel> lineas)
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            var stepLaser = iShell.ShowDialog("MARCADO LASER", () =>
            {
                var ctrl = new LaserStep();
                ctrl.Validate(lineas[0]);

                return ctrl;
            }
            , MessageBoxButtons.OKCancel);

            if (stepLaser != DialogResult.OK)
                throw new Exception("Test cancelado porque el marcado laser no corresponde con el requerido");
        }

    }
}
