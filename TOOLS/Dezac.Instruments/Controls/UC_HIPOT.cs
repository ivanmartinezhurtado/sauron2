﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Instruments.Auxiliars;
using Instruments.Utility;
using Instruments.IO;
using Dezac.Instruments.Utils;
using Dezac.Core.Utility;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(HipotDescription))]
    public partial class UC_HIPOT : UserControl
    {
        private DielectricStrengthTester tester;
        private MyComputer computer = new MyComputer();

        double Voltage;
        double Maximum;
        double Minimum;
        double Time;
        double low;
        double arc;
        double real;
        double ramp;
        double fall;

        int step;

        public Type InstrumentType
        {
            get
            {
                return typeof(DielectricStrengthTester);
            }
        }

        public UC_HIPOT()
        {
                InitializeComponent();
                tester = null;
                foreach (var item in Enum.GetValues(typeof(supplyType)))
                    cmbMode.Items.Add(item);
                cmbMode.SelectedIndex = 0;

                cmbCOM.Items.AddRange(computer.SerialPortNames.ToArray());
                cmbCOM.SelectedIndex = cmbCOM.FindStringExact("COM" + 2);
              
                txtStep.Text = step.ToString();
                txtVoltage.Text = "50";
                txtMaxCurrent.Text = "0,001";
                txtMinCurrent.Text = "0";
                txtTime.Text = "2";
                txtLow.Text = "0";
                txtArc.Text = "0";
                txtRamp.Text = "0";
                txtFall.Text = "0";
                txtReal.Text = "0";            
        }

        private void btnAddStep_Click(object sender, EventArgs e)
        {
            try
            {
                if (tester == null)
                    tester = new DielectricStrengthTester(Convert.ToByte(cmbCOM.Text.Replace("COM", "")));

                step = tester.ReadAmountOfSteps() + 1;
                txtStep.Text = step.ToString();

                if (txtVoltage.Text == string.Empty)
                    Voltage = 50;
                else
                    Voltage = Convert.ToDouble(txtVoltage.Text);

                if (txtMaxCurrent.Text == string.Empty)
                    Maximum = 0.0001D;
                else
                    Maximum = Convert.ToDouble(txtMaxCurrent.Text);

                if (txtMinCurrent.Text == string.Empty)
                    Minimum = 0;
                else
                    Minimum = Convert.ToDouble(txtMinCurrent.Text);

                if (txtTime.Text == string.Empty)
                    Time = 0;
                else
                    Time = Convert.ToDouble(txtTime.Text);

                if (txtLow.Text == string.Empty)
                    low = 0;
                else
                    low = Convert.ToDouble(txtLow.Text);

                if (txtArc.Text == string.Empty)
                    arc = 0;
                else
                    arc = Convert.ToDouble(txtArc.Text);

                if (txtReal.Text == string.Empty)
                    real = 0;
                else
                    real = Convert.ToDouble(txtReal.Text);

                if (txtRamp.Text == string.Empty)
                    ramp = 0;
                else
                    ramp = Convert.ToDouble(txtRamp.Text);

                if (txtFall.Text == string.Empty)
                    fall = 0;
                else
                    fall = Convert.ToInt32(txtFall.Text);

                string ChannelsHigh = FillChannels("H");
                string ChannelsLow = FillChannels("L");

                switch (cmbMode.Text)
                {
                    case "AC":
                        tester.SetACTestParameters(step, Voltage, Maximum, Time);
                        tester.SetAcLimitLow(step, low);
                        tester.SetAcLimitArc(step, arc);
                        tester.SetAcLimitReal(step, real);
                        tester.SetAcTimeRamp(step, ramp);
                        tester.SetAcTimeFall(step, fall);
                        if (ChannelsHigh != string.Empty)
                            tester.SetACOutputChannelHigh(step, ChannelsHigh);
                        if (ChannelsLow != string.Empty)
                            tester.SetACOutputChannelLow(step, ChannelsLow);
                        break;
                    case "DC":
                        tester.SetDCTestParameters(step, Voltage, Maximum, Time);
                        tester.SetDcLimitLow(step, low);
                        tester.SetDcLimitArc(step, arc);
                        tester.SetDcTimeRamp(step, ramp);
                        tester.SetDcTimeFall(step, fall);
                        if (ChannelsHigh != string.Empty)
                            tester.SetDCOutputChannelHigh(step, ChannelsHigh);
                        if (ChannelsLow != string.Empty)
                            tester.SetDCOutputChannelLow(step, ChannelsHigh);
                        break;
                    case "IR":
                        //var resistance = 5000000;
                        tester.SetIRTestParameters(step, Voltage, Minimum, Time);
                        tester.SetIRMaximumResistance(step, Maximum);
                        tester.SetIRTimeRamp(step, arc);
                        tester.SetIRTimeFall(step, fall);
                        if (ChannelsHigh != string.Empty)
                            tester.SetIROutputChannelHigh(step, ChannelsHigh);
                        if (ChannelsLow != string.Empty)
                            tester.SetIROutputChannelLow(step, ChannelsHigh);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("No se encuentra la Chroma en el puerto {0}", cmbCOM.Text), ex);
            }
        }


        private string FillChannels(string mode)
        {
            string channels = string.Empty;

            if (btnCh1.Text.Equals(mode))
                if (channels == string.Empty)
                    channels = "1";
                else
                    channels += ",1";

            if (btnCh2.Text.Equals(mode))
                if (channels == string.Empty)
                    channels = "2";
                else
                    channels += ",2";

            if (btnCh3.Text.Equals(mode))
                if (channels == string.Empty)
                    channels = "3";
                else
                    channels += ",3";

            if (btnCh4.Text.Equals(mode))
                if (channels == string.Empty)
                    channels = "4";
                else
                    channels += ",4";

            return channels;
        }
        

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (tester == null)
                    tester = new DielectricStrengthTester(Convert.ToByte(cmbCOM.Text.Replace("COM", "")));

                tester.StartTest();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("No se encuentra la Chroma en el puerto {0}", cmbCOM.Text), ex);
            }
        }


        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                if (tester == null)
                    tester = new DielectricStrengthTester(Convert.ToByte(cmbCOM.Text.Replace("COM", "")));

                tester.StopTest();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("No se encuentra la Chroma en el puerto {0}", cmbCOM.Text), ex);
            }

        }

        private void btnDeleteStep_Click(object sender, EventArgs e)
        {
            try
            {
                if (tester == null)
                    tester = new DielectricStrengthTester(Convert.ToByte(cmbCOM.Text.Replace("COM", "")));

                int step = tester.ReadAmountOfSteps();
                tester.DeleteTestParameters(step);
                txtStep.Text = step.ToString();

            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("No se encuentra la Chroma en el puerto {0}", cmbCOM.Text), ex);
            }
        }

        private void btnChannel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch(btn.Text)
            {
                case "X":
                    btn.Text = "H";
                    break;
                case "H":
                    btn.Text = "L";
                    break;
                case "L":
                    btn.Text = "X";
                    break;
            }
        }

        private void cmbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((supplyType)cmbMode.SelectedIndex)
            {
                case supplyType.AC:
                    lblMax.Text = "MaxCurrent:";
                    lblMin.Text = "MinCurrent:";
                    break;
                case supplyType.DC:
                    lblMax.Text = "MaxCurrent:";
                    lblMin.Text = "MinCurrent:";
                    break;
                case supplyType.IR:
                    lblMax.Text = "MaxResistance:";
                    lblMin.Text = "MinResistance:";
                    break;
            }
        }


        public enum supplyType
        {
            AC,
            DC,
            IR
        }
    }
    public class HipotDescription : ControlDescription
    {
        public override string Name { get { return "HIPOT"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Flash_Updater_icon; } }
        public override Image IconBig { get { return Properties.Resources.hipotTester; } }
        public override Type SubcomponentType { get { return typeof(DielectricStrengthTester); } }
        public override bool Visible { get { return false; } }
    }
}
