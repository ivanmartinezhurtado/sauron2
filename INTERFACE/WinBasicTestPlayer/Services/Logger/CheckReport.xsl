<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="IdEmpleado"></xsl:param>
  <xsl:param name="NumProducto"></xsl:param>
  <xsl:param name="Version"></xsl:param>
  <xsl:param name="DescripcionProducto"></xsl:param>
  <xsl:param name="IdFase"></xsl:param>
  <xsl:param name="PC"></xsl:param>
  <xsl:param name="NumSerie"></xsl:param>
  <xsl:param name="NumBastidor"></xsl:param>
  <xsl:param name="Fecha"></xsl:param>

  <xsl:template match="/">
        <html>
            <head>
              <style>
                body
                {
                font-family: monospace;
                font-size: 11pt;
                font-weight: normal;
                }

                table {
                table-layout: fixed;
                word-wrap: break-word;
                empty-cells: show;
                font-size: 1em;
                }

                .statistics {
                width: 65em;
                border-collapse: collapse;
                empty-cells: show;
                margin-bottom: 1em;
                }

                th {
                background: #ddd;
                color: black;
                vertical-align: top;
                font-size: 1.5em;
                }

                td {
                vertical-align: top;
                }

                .col {
                width: 4.5em;
                text-align: center;
                padding: 5px 10px;
                }

                h2 {
                display: block;
                font-size: 1.5em;
                -webkit-margin-before: 0.83em;
                -webkit-margin-after: 0.83em;
                -webkit-margin-start: 0px;
                -webkit-margin-end: 0px;
                font-weight: bold;
                }

                h1 {
                margin: 0 0 0.5em 0;
                width: 75%;
                }

                .label.ok{
                background-color: #47bd47;
                }

                .label.ko{
                background-color: #c53e3e;
                }

                .label{
                color: #fff !important;
                font-weight: bold;
                padding: 2px 10px;
                letter-spacing: 1px;
                white-space: nowrap;
                color: White;
                border-radius: 3px;
                }

                .tablesorter_header
                {
                font-size: 18px;

                }

              </style>
            </head>
          <body>

            <div id="header">
              <h1>Test Report</h1>
            </div>
            
            <div class ="Summary_container">
              <h2>Summary Information</h2>
              <table>
                <tbody>
                  <tr>
                    <td>
                      <b>Producto: </b>
                    </td>
                    <td>
                      <xsl:value-of select="$NumProducto"/>/<xsl:value-of select="$Version"/> - <xsl:value-of select="$DescripcionProducto"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Fecha: </b>
                    </td>
                    <td>
                      <xsl:value-of select="$Fecha"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Bastidor: </b>
                    </td>
                    <td>
                      <xsl:value-of select="$NumBastidor"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>Nº Serie: </b>
                    </td>
                    <td>
                      <xsl:value-of select="$NumSerie"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class ="Results_container">
              <h2>Test Results</h2>          
              <xsl:apply-templates/>
            </div>
                      
          </body>                          
        </html>
      </xsl:template>
  
  
      <xsl:template match="EventInfo[@Type='step']">
        <tbody>
          <tr class="row">
            <td class ="col">
              <xsl:value-of select="@Text"/>
            </td>

            <td class ="col">
              <xsl:value-of select="@ExecutionTime"/> ms
            </td>
            <td class ="col">
              <span>
                <xsl:attribute name="class">
                  label
                  <xsl:if test="@Result = 'Completed'">
                    ok
                  </xsl:if>
                  <xsl:if test="@Result = 'Faulted'">
                    ko
                  </xsl:if>
                </xsl:attribute>
                <xsl:if test="@Result = 'Completed'">
                  PASS
                </xsl:if>
                <xsl:if test="@Result = 'Faulted'">
                  FAIL
                </xsl:if>
              </span>
            </td>
          
          </tr>
        </tbody>
        <xsl:apply-templates select="Events"/>

      </xsl:template>
  
      <xsl:template match="EventInfo[@Type='step' and (@Id='Main' or @Id='Init' or @Id='End')]">
        <table class ="statistics">
                <xsl:value-of select="@Text"/>                            
          <thead>
            <tr class="tablesorter_row">
              <th class ="tablesorter_header" data-column="1">
                <div class="tablesorter_header_inner">Step</div>
              </th>

              <th class="tablesorter_header" title="Total execution time of these test cases. Excludes suite setups and teardowns." data-column="2">
                <div class="tablesorter_header_inner">Elapsed</div>
              </th>

              <th class="tablesorter_header" data-column="3">
                <div class="tablesorter_header_inner">Pass / Fail</div>
              </th>
              
            </tr>
          </thead>

          <tbody class="step-content">
            <xsl:apply-templates select="Events"/>
          </tbody>

        </table>
    </xsl:template>

</xsl:stylesheet>
