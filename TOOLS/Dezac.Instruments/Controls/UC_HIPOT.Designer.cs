﻿namespace Dezac.Instruments.Controls
{
    partial class UC_HIPOT
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label19;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.Label label22;
            System.Windows.Forms.Label label10;
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnCh1 = new System.Windows.Forms.Button();
            this.btnCh2 = new System.Windows.Forms.Button();
            this.btnCh3 = new System.Windows.Forms.Button();
            this.btnCh4 = new System.Windows.Forms.Button();
            this.btnAddStep = new System.Windows.Forms.Button();
            this.btnDeleteStep = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblMax = new System.Windows.Forms.Label();
            this.txtStep = new System.Windows.Forms.MaskedTextBox();
            this.cmbMode = new System.Windows.Forms.ComboBox();
            this.txtReal = new System.Windows.Forms.MaskedTextBox();
            this.txtRamp = new System.Windows.Forms.MaskedTextBox();
            this.txtTime = new System.Windows.Forms.MaskedTextBox();
            this.txtVoltage = new System.Windows.Forms.MaskedTextBox();
            this.txtLow = new System.Windows.Forms.MaskedTextBox();
            this.txtFall = new System.Windows.Forms.MaskedTextBox();
            this.txtMaxCurrent = new System.Windows.Forms.MaskedTextBox();
            this.txtArc = new System.Windows.Forms.MaskedTextBox();
            this.txtMinCurrent = new System.Windows.Forms.MaskedTextBox();
            this.cmbCOM = new System.Windows.Forms.ComboBox();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label19 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            label22 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.BackColor = System.Drawing.Color.Transparent;
            label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label13.Location = new System.Drawing.Point(9, 8);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(37, 13);
            label13.TabIndex = 118;
            label13.Text = "Step:";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.BackColor = System.Drawing.Color.Transparent;
            label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label14.Location = new System.Drawing.Point(9, 76);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(54, 13);
            label14.TabIndex = 117;
            label14.Text = "Voltage:";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.BackColor = System.Drawing.Color.Transparent;
            label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label15.Location = new System.Drawing.Point(8, 108);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(36, 13);
            label15.TabIndex = 116;
            label15.Text = "Time:";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.BackColor = System.Drawing.Color.Transparent;
            label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label16.Location = new System.Drawing.Point(276, 89);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(40, 13);
            label16.TabIndex = 108;
            label16.Text = "FALL:";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.BackColor = System.Drawing.Color.Transparent;
            label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label17.Location = new System.Drawing.Point(277, 8);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(37, 13);
            label17.TabIndex = 102;
            label17.Text = "LOW:";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.BackColor = System.Drawing.Color.Transparent;
            label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label19.Location = new System.Drawing.Point(275, 116);
            label19.Name = "label19";
            label19.Size = new System.Drawing.Size(42, 13);
            label19.TabIndex = 110;
            label19.Text = "REAL:";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.BackColor = System.Drawing.Color.Transparent;
            label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label21.Location = new System.Drawing.Point(276, 62);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(45, 13);
            label21.TabIndex = 106;
            label21.Text = "RAMP:";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.BackColor = System.Drawing.Color.Transparent;
            label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label22.Location = new System.Drawing.Point(280, 35);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(35, 13);
            label22.TabIndex = 104;
            label22.Text = "ARC:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.BackColor = System.Drawing.Color.Transparent;
            label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label10.Location = new System.Drawing.Point(9, 43);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(42, 13);
            label10.TabIndex = 119;
            label10.Text = "Mode:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 121;
            this.label1.Text = "Serial Port";
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Transparent;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Transparent;
            this.btnStart.Location = new System.Drawing.Point(19, 96);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(71, 49);
            this.btnStart.TabIndex = 117;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Transparent;
            this.btnStop.FlatAppearance.BorderSize = 0;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.Transparent;
            this.btnStop.Location = new System.Drawing.Point(18, 21);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 62);
            this.btnStop.TabIndex = 118;
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnCh1
            // 
            this.btnCh1.BackColor = System.Drawing.Color.Transparent;
            this.btnCh1.FlatAppearance.BorderSize = 0;
            this.btnCh1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCh1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnCh1.Location = new System.Drawing.Point(99, 157);
            this.btnCh1.Name = "btnCh1";
            this.btnCh1.Size = new System.Drawing.Size(50, 50);
            this.btnCh1.TabIndex = 122;
            this.btnCh1.UseVisualStyleBackColor = false;
            this.btnCh1.Click += new System.EventHandler(this.btnChannel_Click);
            // 
            // btnCh2
            // 
            this.btnCh2.BackColor = System.Drawing.Color.Transparent;
            this.btnCh2.FlatAppearance.BorderSize = 0;
            this.btnCh2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCh2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnCh2.Location = new System.Drawing.Point(150, 157);
            this.btnCh2.Name = "btnCh2";
            this.btnCh2.Size = new System.Drawing.Size(50, 50);
            this.btnCh2.TabIndex = 123;
            this.btnCh2.UseVisualStyleBackColor = false;
            this.btnCh2.Click += new System.EventHandler(this.btnChannel_Click);
            // 
            // btnCh3
            // 
            this.btnCh3.BackColor = System.Drawing.Color.Transparent;
            this.btnCh3.FlatAppearance.BorderSize = 0;
            this.btnCh3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCh3.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCh3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnCh3.Location = new System.Drawing.Point(203, 156);
            this.btnCh3.Name = "btnCh3";
            this.btnCh3.Size = new System.Drawing.Size(50, 50);
            this.btnCh3.TabIndex = 124;
            this.btnCh3.UseVisualStyleBackColor = false;
            this.btnCh3.Click += new System.EventHandler(this.btnChannel_Click);
            // 
            // btnCh4
            // 
            this.btnCh4.BackColor = System.Drawing.Color.Transparent;
            this.btnCh4.FlatAppearance.BorderSize = 0;
            this.btnCh4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCh4.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCh4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnCh4.Location = new System.Drawing.Point(252, 156);
            this.btnCh4.Name = "btnCh4";
            this.btnCh4.Size = new System.Drawing.Size(50, 50);
            this.btnCh4.TabIndex = 125;
            this.btnCh4.UseVisualStyleBackColor = false;
            this.btnCh4.Click += new System.EventHandler(this.btnChannel_Click);
            // 
            // btnAddStep
            // 
            this.btnAddStep.BackColor = System.Drawing.Color.DimGray;
            this.btnAddStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStep.ForeColor = System.Drawing.Color.White;
            this.btnAddStep.Location = new System.Drawing.Point(310, 159);
            this.btnAddStep.Name = "btnAddStep";
            this.btnAddStep.Size = new System.Drawing.Size(91, 51);
            this.btnAddStep.TabIndex = 108;
            this.btnAddStep.Text = "ADD STEP";
            this.btnAddStep.UseVisualStyleBackColor = false;
            this.btnAddStep.Click += new System.EventHandler(this.btnAddStep_Click);
            // 
            // btnDeleteStep
            // 
            this.btnDeleteStep.BackColor = System.Drawing.Color.DimGray;
            this.btnDeleteStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteStep.ForeColor = System.Drawing.Color.White;
            this.btnDeleteStep.Location = new System.Drawing.Point(417, 159);
            this.btnDeleteStep.Name = "btnDeleteStep";
            this.btnDeleteStep.Size = new System.Drawing.Size(91, 51);
            this.btnDeleteStep.TabIndex = 119;
            this.btnDeleteStep.Text = "DELETE STEP";
            this.btnDeleteStep.UseVisualStyleBackColor = false;
            this.btnDeleteStep.Click += new System.EventHandler(this.btnDeleteStep_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.lblMin);
            this.panel2.Controls.Add(this.lblMax);
            this.panel2.Controls.Add(label10);
            this.panel2.Controls.Add(label13);
            this.panel2.Controls.Add(label14);
            this.panel2.Controls.Add(label15);
            this.panel2.Controls.Add(this.txtStep);
            this.panel2.Controls.Add(this.cmbMode);
            this.panel2.Controls.Add(this.txtReal);
            this.panel2.Controls.Add(this.txtRamp);
            this.panel2.Controls.Add(this.txtTime);
            this.panel2.Controls.Add(label16);
            this.panel2.Controls.Add(label17);
            this.panel2.Controls.Add(label19);
            this.panel2.Controls.Add(this.txtVoltage);
            this.panel2.Controls.Add(label21);
            this.panel2.Controls.Add(this.txtLow);
            this.panel2.Controls.Add(this.txtFall);
            this.panel2.Controls.Add(this.txtMaxCurrent);
            this.panel2.Controls.Add(this.txtArc);
            this.panel2.Controls.Add(label22);
            this.panel2.Controls.Add(this.txtMinCurrent);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(95, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(404, 136);
            this.panel2.TabIndex = 121;
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMin.Location = new System.Drawing.Point(130, 43);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(72, 13);
            this.lblMin.TabIndex = 121;
            this.lblMin.Text = "MinCurrent:";
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMax.Location = new System.Drawing.Point(126, 8);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(75, 13);
            this.lblMax.TabIndex = 120;
            this.lblMax.Text = "MaxCurrent:";
            // 
            // txtStep
            // 
            this.txtStep.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtStep.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStep.Location = new System.Drawing.Point(53, 6);
            this.txtStep.Name = "txtStep";
            this.txtStep.ReadOnly = true;
            this.txtStep.Size = new System.Drawing.Size(63, 16);
            this.txtStep.TabIndex = 106;
            this.txtStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbMode
            // 
            this.cmbMode.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMode.FormattingEnabled = true;
            this.cmbMode.Location = new System.Drawing.Point(58, 39);
            this.cmbMode.Name = "cmbMode";
            this.cmbMode.Size = new System.Drawing.Size(64, 21);
            this.cmbMode.TabIndex = 115;
            this.cmbMode.SelectedIndexChanged += new System.EventHandler(this.cmbMode_SelectedIndexChanged);
            // 
            // txtReal
            // 
            this.txtReal.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtReal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReal.Location = new System.Drawing.Point(324, 114);
            this.txtReal.Name = "txtReal";
            this.txtReal.Size = new System.Drawing.Size(75, 16);
            this.txtReal.TabIndex = 111;
            this.txtReal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtRamp
            // 
            this.txtRamp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtRamp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRamp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRamp.Location = new System.Drawing.Point(324, 60);
            this.txtRamp.Name = "txtRamp";
            this.txtRamp.Size = new System.Drawing.Size(75, 16);
            this.txtRamp.TabIndex = 107;
            this.txtRamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime
            // 
            this.txtTime.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTime.Location = new System.Drawing.Point(52, 106);
            this.txtTime.Mask = "9.9";
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(65, 16);
            this.txtTime.TabIndex = 88;
            this.txtTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVoltage
            // 
            this.txtVoltage.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtVoltage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVoltage.Location = new System.Drawing.Point(62, 74);
            this.txtVoltage.Mask = "9999";
            this.txtVoltage.Name = "txtVoltage";
            this.txtVoltage.Size = new System.Drawing.Size(62, 16);
            this.txtVoltage.TabIndex = 87;
            this.txtVoltage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLow
            // 
            this.txtLow.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtLow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLow.Location = new System.Drawing.Point(321, 6);
            this.txtLow.Name = "txtLow";
            this.txtLow.Size = new System.Drawing.Size(75, 16);
            this.txtLow.TabIndex = 103;
            this.txtLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtFall
            // 
            this.txtFall.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtFall.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFall.Location = new System.Drawing.Point(324, 87);
            this.txtFall.Name = "txtFall";
            this.txtFall.Size = new System.Drawing.Size(75, 16);
            this.txtFall.TabIndex = 109;
            this.txtFall.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtMaxCurrent
            // 
            this.txtMaxCurrent.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtMaxCurrent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMaxCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxCurrent.Location = new System.Drawing.Point(203, 6);
            this.txtMaxCurrent.Mask = "9.999999";
            this.txtMaxCurrent.Name = "txtMaxCurrent";
            this.txtMaxCurrent.Size = new System.Drawing.Size(67, 16);
            this.txtMaxCurrent.TabIndex = 89;
            this.txtMaxCurrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtArc
            // 
            this.txtArc.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtArc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtArc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArc.Location = new System.Drawing.Point(322, 33);
            this.txtArc.Name = "txtArc";
            this.txtArc.Size = new System.Drawing.Size(75, 16);
            this.txtArc.TabIndex = 105;
            this.txtArc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtMinCurrent
            // 
            this.txtMinCurrent.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtMinCurrent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMinCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinCurrent.Location = new System.Drawing.Point(203, 41);
            this.txtMinCurrent.Mask = "9.999999";
            this.txtMinCurrent.Name = "txtMinCurrent";
            this.txtMinCurrent.Size = new System.Drawing.Size(65, 16);
            this.txtMinCurrent.TabIndex = 90;
            this.txtMinCurrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbCOM
            // 
            this.cmbCOM.BackColor = System.Drawing.SystemColors.HotTrack;
            this.cmbCOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCOM.FormattingEnabled = true;
            this.cmbCOM.Location = new System.Drawing.Point(18, 178);
            this.cmbCOM.Name = "cmbCOM";
            this.cmbCOM.Size = new System.Drawing.Size(76, 21);
            this.cmbCOM.TabIndex = 120;
            // 
            // UC_HIPOT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::Dezac.Instruments.Properties.Resources.hipotTester;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCh4);
            this.Controls.Add(this.cmbCOM);
            this.Controls.Add(this.btnCh3);
            this.Controls.Add(this.btnCh2);
            this.Controls.Add(this.btnCh1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnDeleteStep);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnAddStep);
            this.Controls.Add(this.btnStart);
            this.DoubleBuffered = true;
            this.Name = "UC_HIPOT";
            this.Size = new System.Drawing.Size(608, 227);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnCh1;
        private System.Windows.Forms.Button btnCh2;
        private System.Windows.Forms.Button btnCh3;
        private System.Windows.Forms.Button btnCh4;
        private System.Windows.Forms.Button btnAddStep;
        private System.Windows.Forms.Button btnDeleteStep;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MaskedTextBox txtStep;
        private System.Windows.Forms.MaskedTextBox txtReal;
        private System.Windows.Forms.MaskedTextBox txtRamp;
        private System.Windows.Forms.MaskedTextBox txtTime;
        private System.Windows.Forms.MaskedTextBox txtVoltage;
        private System.Windows.Forms.MaskedTextBox txtLow;
        private System.Windows.Forms.MaskedTextBox txtFall;
        private System.Windows.Forms.MaskedTextBox txtMaxCurrent;
        private System.Windows.Forms.MaskedTextBox txtArc;
        private System.Windows.Forms.MaskedTextBox txtMinCurrent;
        private System.Windows.Forms.ComboBox cmbMode;
        private System.Windows.Forms.ComboBox cmbCOM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label lblMin;
    }
}
