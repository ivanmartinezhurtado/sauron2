﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;

namespace Dezac.Device.ThermalStations
{
    [DeviceVersion(1.03)]
    public class NOVACENTRALKSP : DeviceBase
    {
        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public NOVACENTRALKSP()
        {
        }

        public NOVACENTRALKSP(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = false;
            Modbus.PerifericNumber = periferic;
        }

        public NOVACENTRALKSP(SerialPort SerialPort)
        {
            SetSerialPort(SerialPort);
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public void SetSerialPort(SerialPort SerialPort)
        {
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Write Functions

        public void FlagTest()
        {
            LogMethod();
            //Modbus.Write((ushort)Registers.FLAG_TEST, true);
            Modbus.Write((ushort)Registers.FLAG_TEST, (ushort)0X0001);
        }

        public void AuthorizationStartTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.AUTHORIZATION_START_TEST, (ushort)0x0001);
            Modbus.Write((ushort)Registers.CLOSE_OFF_LEDS_AND_RELAYS, (ushort)0x0000);
        }

        public void ActivateMaskControlLedsGrids()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MASK_CONTROL_LEDS_GRIDS, (ushort)0x003F);
        }

        public void DesactivateMaskControlLedsGrids()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MASK_CONTROL_LEDS_GRIDS, (ushort)0x0000);
        }

        public void prueba()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MASK_CONTROL_LEDS_GRIDS, (ushort)0x003b);
        }

        public void SelectGrid(Grids grid)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SELECTION_GRID, (ushort)grid);
        }

        public void OpenGrid(Grids grid)
        {
            LogMethod();
            SelectGrid(grid);
            Modbus.Write((ushort)Registers.SELECTION_MANIOBRE_GRID, (ushort)0x0001);
        }

        public void CloseGrid(Grids grid)
        {
            LogMethod();
            SelectGrid(grid);
            Modbus.Write((ushort)Registers.SELECTION_MANIOBRE_GRID, (ushort)0x0002);
        }

        public void OnLed(Leds led)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LED_GRID, (ushort)led);
        }

        public void OnLedsAllGreen()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LED_GRID, (ushort)0x003F);
        }

        public void OffLed()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LED_GRID, 0x0000);
        }

        public void OnLedsAllRed()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LED_GRID, (ushort)0x0000);
        }

        public void ActivateRelay(Relays value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RELAY_AND_LED, (ushort)value);
        }

        public void ActivateRelayCombination(RelaysCombination value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RELAY_AND_LED, (ushort)value);
        }

        public void DesactivateRelays()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RELAY_AND_LED, (ushort)0x0000);
        }

        public void ResetCounters()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.COUNTERS_AREAS, 0);
        }

        public void WriteIdentificationNumber(int? value)
        {
            if (!value.HasValue)
                throw new Exception("El Numero de Identificación recuperado es NULL");

            Modbus.Write((ushort)Registers.IDENTIFICATION_NUMBER, (ushort)value);
        }

        public void WritePerifericNumber(int? value)
        {
            LogMethod();

            if(!value.HasValue)
                throw new Exception("El Numero de Identificación recuperado es NULL");

            Modbus.Write((ushort)Registers.PERIFERIC_NUMBER, (ushort)value);
        }

        public void WriteTemperaturePatterns(TemperaturePatterns value)
        {
            FlagTest();

            LogMethod();

            var PatternAndRegistersRelation = new Dictionary<ushort, ushort>()
            {
                {(ushort)TablePatternsRegisters.Pattern10C, value.Patern_10C},
                {(ushort)TablePatternsRegisters.Pattern5C, value.Patern_5C},
                {(ushort)TablePatternsRegisters.Pattern30C, value.Patern_30C},
                {(ushort)TablePatternsRegisters.Pattern18C, value.Patern_18C},
                {(ushort)TablePatternsRegisters.Pattern85C, value.Patern_85C},
                {(ushort)TablePatternsRegisters.Pattern40C, value.Patern_40C},
                {(ushort)TablePatternsRegisters.Pattern6C, value.Patern_6C}
            };

            foreach (KeyValuePair<ushort, ushort> tablePatterns in PatternAndRegistersRelation)
                Modbus.Write((ushort)tablePatterns.Key, (ushort)tablePatterns.Value);
        }

        public void WriteTemporizators(Temporizators value)
        {
            FlagTest();

            LogMethod();

            var TemporizatorsAndRegistersRelation = new Dictionary<ushort, ushort>()
            {
                {(ushort)TableTemporizatorsRegisters.DT1, value.DT1_freecol},
                {(ushort)TableTemporizatorsRegisters.DT2, value.DT2_freecol},
                {(ushort)TableTemporizatorsRegisters.HisTmpFrio, value.His_tmpfrio},
                {(ushort)TableTemporizatorsRegisters.HisTmpCalor, value.His_tmpcalo},
                {(ushort)TableTemporizatorsRegisters.TimOffAnti, value.Tim_offanti},
                {(ushort)TableTemporizatorsRegisters.TimOnFreeCol, value.Tim_onfreecol},
                {(ushort)TableTemporizatorsRegisters.TimProtocol, value.Tim_protocol}
            };

            foreach (KeyValuePair<ushort, ushort> temporizators in TemporizatorsAndRegistersRelation)
                Modbus.Write((ushort)temporizators.Key, (ushort)temporizators.Value);
        }       

        public void WriteParameters(Parameters value)
        {
            FlagTest();

            LogMethod();

            var ParametersAndRegistersRelation = new Dictionary<ushort, ushort>()
            {
                {(ushort)TableParametersRegisters.DT1, value.DT1Freecol},
                {(ushort)TableParametersRegisters.DT2, value.DT2Freecol},
                {(ushort)TableParametersRegisters.HisTmpFrio, value.HisTmpCold},
                {(ushort)TableParametersRegisters.HisTmpCalor, value.HisTempHot},
                {(ushort)TableParametersRegisters.TimeMoveDamper, value.TimeMoveDamper},
                {(ushort)TableParametersRegisters.TempAntiHielo, value.TempAntiHielo},
                {(ushort)TableParametersRegisters.LimitWaterTemperatureHot, value.LimitWaterTemperatureHot},
                {(ushort)TableParametersRegisters.LimitWaterTemperatureCold, value.LimitWaterTemperatureCold},
                {(ushort)TableParametersRegisters.TimOffAnti, value.TimeOffCheckAntiHielo},
                {(ushort)TableParametersRegisters.LimitTempCaldera, value.LimitTempCaldera},
                {(ushort)TableParametersRegisters.TimeActivateFreeCooling, value.TimeActivateFreeCooling},
                {(ushort)TableParametersRegisters.ReturnTempAirMax, value.ReturnTempAirMax},
                {(ushort)TableParametersRegisters.ReturnTempAirMin, value.ReturnTempAirMin},
                {(ushort)TableParametersRegisters.TimeProtocol, value.TimeProtocolDO},
                {(ushort)TableParametersRegisters.IntensisVelocity, value.IntensisVelocity},
                {(ushort)TableParametersRegisters.AlarmAntiHielo, value.AlarmAntiHielo},
                {(ushort)TableParametersRegisters.ConfigFreeCooling, value.ConfigFreeCooling},
                {(ushort)TableParametersRegisters.TimerStartMotor, value.TimerStartMotor},
                {(ushort)TableParametersRegisters.TimerStopMotor, value.TimerStopMotor},
                {(ushort)TableParametersRegisters.ValueRestaFactorVelocidad, value.ValueRestaFactorVelocidad},
                {(ushort)TableParametersRegisters.ValueSuma1FactorVelocidad, value.ValueSuma1FactorVelocidad},
                {(ushort)TableParametersRegisters.ValueSuma2FactorVelocidad, value.ValueSuma2FactorVelocidad},
                {(ushort)TableParametersRegisters.ApplyTableCompatibility, value.ApplyTableCompatibility},
                {(ushort)TableParametersRegisters.TimerOffFan,  value.TimerOffFan},
                {(ushort)TableParametersRegisters.TempProtectionRadiant,  value.TempProtectionRadiant},
                {(ushort)TableParametersRegisters.TempProtectionRefresh,  value.TempProtectionRefresh},
                {(ushort)TableParametersRegisters.HisTmpFrioUbac,  value.HisTempColdUbac},
                {(ushort)TableParametersRegisters.HisTmpCalorUbac,  value.HisTempHotUbac},
        };

            foreach (KeyValuePair<ushort, ushort> parameters in ParametersAndRegistersRelation)
                Modbus.Write((ushort)parameters.Key, (ushort)parameters.Value);
        }

        public void WriteSerialNumber(int serialNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, serialNumber);
        }

        public void SaveToFlash()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FLASH, (ushort)0x0001);
        }

        public void SelectMoveGrid()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TIME_MOVE_GRID, (ushort)0x0005);
        }

        public void SelectFrecuency433MHz()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FREQUENCY_SELECTION, (ushort)0x0002);
        }

        #endregion

        #region Read Functions

        public bool ReadFlagTest()
        {
            LogMethod();
            var flagTest = Modbus.ReadRegister((ushort)Registers.FLAG_TEST);
            return flagTest == 0x0001;
        }

        public ushort ReadSwitch()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.READING_SWITCH);
        }

        public ushort ReadDigitalInputs(DigitalInputs input)
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)input);
        }

        public ushort[] ReadCounters()
        {
            LogMethod();
            return Modbus.ReadMultipleRegister((ushort)Registers.COUNTERS_AREAS, 6);
        }

        public double ReadProbeTemperature(Probes probe)
        {
            LogMethod();
            return (double) Modbus.ReadRegister((ushort)probe) / 10;
        }

        public ushort ReadProbeTemperaturePoints(Probes probe)
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)probe);
        }

        public ushort ReadIdentificationNumber()
        {
            LogMethod();
            return (Modbus.ReadRegister((ushort)Registers.IDENTIFICATION_NUMBER));
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            
            return Modbus.ReadString((ushort)Registers.VERSION_FIRMWARE, 3).Substring(0, 5); 
        }

        public TemperaturePatterns ReadTemperaturePatterns()
        {
            LogMethod();

            var patterns = new TemperaturePatterns();
           
            patterns.Patern_10C = Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern10C);
            patterns.Patern_5C = Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern5C);
            patterns.Patern_30C = Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern30C);
            patterns.Patern_18C =  Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern18C);
            patterns.Patern_85C =  Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern85C);
            patterns.Patern_40C =  Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern40C);
            patterns.Patern_6C = Modbus.ReadRegister((ushort)TablePatternsRegisters.Pattern6C);

            return patterns;
        }

        public Temporizators ReadTemporizators()
        {
            LogMethod();

            var temp = new Temporizators();

            temp.DT1_freecol = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.DT1);
            temp.DT2_freecol = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.DT2);
            temp.His_tmpcalo = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.HisTmpCalor);
            temp.His_tmpfrio = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.HisTmpFrio);
            temp.Tim_offanti = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.TimOffAnti);
            temp.Tim_onfreecol = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.TimOnFreeCol);
            temp.Tim_protocol = Modbus.ReadRegister((ushort)TableTemporizatorsRegisters.TimProtocol);

            return temp;
        }

        public Parameters ReadParameters()
        {
            LogMethod();

            var parameters = new Parameters();
            parameters.DT1Freecol = Modbus.ReadRegister((ushort)TableParametersRegisters.DT1);
            parameters.DT2Freecol = Modbus.ReadRegister((ushort)TableParametersRegisters.DT2);
            parameters.HisTmpCold = Modbus.ReadRegister((ushort)TableParametersRegisters.HisTmpFrio);
            parameters.HisTempHot = Modbus.ReadRegister((ushort)TableParametersRegisters.HisTmpCalor);
            parameters.TimeMoveDamper = Modbus.ReadRegister((ushort)TableParametersRegisters.TimeMoveDamper);
            parameters.TempAntiHielo = Modbus.ReadRegister((ushort)TableParametersRegisters.TempAntiHielo);
            parameters.LimitWaterTemperatureHot = Modbus.ReadRegister((ushort)TableParametersRegisters.LimitWaterTemperatureHot);
            parameters.LimitWaterTemperatureCold = Modbus.ReadRegister((ushort)TableParametersRegisters.LimitWaterTemperatureCold);
            parameters.TimeOffCheckAntiHielo = Modbus.ReadRegister((ushort)TableParametersRegisters.TimOffAnti);
            parameters.LimitTempCaldera = Modbus.ReadRegister((ushort)TableParametersRegisters.LimitTempCaldera);
            parameters.TimeActivateFreeCooling = Modbus.ReadRegister((ushort)TableParametersRegisters.TimeActivateFreeCooling);
            parameters.ReturnTempAirMax = Modbus.ReadRegister((ushort)TableParametersRegisters.ReturnTempAirMax);
            parameters.ReturnTempAirMin = Modbus.ReadRegister((ushort)TableParametersRegisters.ReturnTempAirMin);
            parameters.TimeProtocolDO = Modbus.ReadRegister((ushort)TableParametersRegisters.TimeProtocol);
            parameters.IntensisVelocity = Modbus.ReadRegister((ushort)TableParametersRegisters.IntensisVelocity);
            parameters.AlarmAntiHielo = Modbus.ReadRegister((ushort)TableParametersRegisters.AlarmAntiHielo);
            parameters.ConfigFreeCooling = Modbus.ReadRegister((ushort)TableParametersRegisters.ConfigFreeCooling);
            parameters.TimerStartMotor = Modbus.ReadRegister((ushort)TableParametersRegisters.TimerStartMotor);
            parameters.TimerStopMotor = Modbus.ReadRegister((ushort)TableParametersRegisters.TimerStopMotor);
            parameters.ValueRestaFactorVelocidad = Modbus.ReadRegister((ushort)TableParametersRegisters.ValueRestaFactorVelocidad);
            parameters.ValueSuma1FactorVelocidad = Modbus.ReadRegister((ushort)TableParametersRegisters.ValueSuma1FactorVelocidad);
            parameters.ValueSuma2FactorVelocidad = Modbus.ReadRegister((ushort)TableParametersRegisters.ValueSuma2FactorVelocidad);
            parameters.ApplyTableCompatibility = Modbus.ReadRegister((ushort)TableParametersRegisters.ApplyTableCompatibility);
            parameters.TimerOffFan = Modbus.ReadRegister((ushort)TableParametersRegisters.TimerOffFan);
            parameters.TempProtectionRadiant = Modbus.ReadRegister((ushort)TableParametersRegisters.TempProtectionRadiant);
            parameters.TempProtectionRefresh = Modbus.ReadRegister((ushort)TableParametersRegisters.TempProtectionRefresh);
            parameters.HisTempColdUbac = Modbus.ReadRegister((ushort)TableParametersRegisters.HisTmpFrioUbac);
            parameters.HisTempHotUbac = Modbus.ReadRegister((ushort)TableParametersRegisters.HisTmpCalorUbac);

            return parameters;
        }

        #endregion

        #region Calibration

        public Tuple<bool, double> Calibration(Probes Sonda, int valueRntc,int delFirst, int initCount, int samples, int interval, Func<double, bool> adjustValidation)
        {
            FlagTest(); 
            var result = new double();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 30,
                () =>
                {
                    var temperatureReading = ReadProbeTemperaturePoints(Sonda);
                    return new double[] { temperatureReading };
                },
                (listValues) =>
                {
                    result = (ushort)(valueRntc * (4096 - listValues.Average(0)) / listValues.Average(0));
                    return adjustValidation(result);
                });
                
            return Tuple.Create(adjustResult.Item1, result);
        }
        #endregion

        #region Enumerators and Structs

        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //[ModbusLayout(Address = (ushort)Registers.TABLE_OF_PATERNS)]
        public struct TemperaturePatterns
        {
            public ushort Patern_10C;
            public ushort Patern_5C;
            public ushort Patern_30C;
            public ushort Patern_18C;
            public ushort Patern_85C;
            public ushort Patern_40C;
            public ushort Patern_6C;
        }
        
        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        //[ModbusLayout(Address = (ushort)Registers.TABLE_OF_TEMPORIZATORS)]
        public struct Temporizators
        {
            public ushort DT1_freecol;
            public ushort DT2_freecol;
            public ushort His_tmpfrio;
            public ushort His_tmpcalo;
            public ushort Tim_offanti;
            public ushort Tim_onfreecol;
            public ushort Tim_protocol;
        }

        public struct Parameters
        {
            public ushort DT1Freecol;
            public ushort DT2Freecol;
            public ushort HisTmpCold;
            public ushort HisTempHot;
            public ushort TimeMoveDamper;
            public ushort TempAntiHielo;
            public ushort LimitWaterTemperatureHot;
            public ushort LimitWaterTemperatureCold;
            public ushort TimeOffCheckAntiHielo;
            public ushort LimitTempCaldera;
            public ushort TimeActivateFreeCooling;
            public ushort ReturnTempAirMax;
            public ushort ReturnTempAirMin;
            public ushort TimeProtocolDO;
            public ushort IntensisVelocity;
            public ushort AlarmAntiHielo;
            public ushort ConfigFreeCooling;
            public ushort TimerStartMotor;
            public ushort TimerStopMotor;
            public ushort ValueRestaFactorVelocidad;
            public ushort ValueSuma1FactorVelocidad;
            public ushort ValueSuma2FactorVelocidad;
            public ushort ApplyTableCompatibility;
            public ushort TimerOffFan;
            public ushort TempProtectionRadiant;
            public ushort TempProtectionRefresh;
            public ushort HisTempColdUbac;
            public ushort HisTempHotUbac;

        }

        [Flags]
        public enum TablePatternsRegisters
        {
            Pattern10C = 0X0801,
            Pattern5C = 0X0802,
            Pattern30C = 0X0803,
            Pattern18C = 0X0804,
            Pattern85C = 0X0806,
            Pattern40C = 0X0808,
            Pattern6C = 0X0809,
        }

        [Flags]
        public enum TableTemporizatorsRegisters
        {
            DT1 = 0X07FD,
            DT2 = 0X07FE,
            HisTmpFrio = 0X07FF,
            HisTmpCalor = 0X0800,
            TimOffAnti = 0X0805,
            TimOnFreeCol = 0X0807,
            TimProtocol = 0X080A
        }

        [Flags]
        public enum TableParametersRegisters
        {
            DT1 = 0x07FD,
            DT2 = 0x07FE,
            HisTmpFrio = 0x07FF,
            HisTmpCalor = 0x0800,
            TimeMoveDamper = 0x0801,
            TempAntiHielo = 0x0802,
            LimitWaterTemperatureHot = 0x0803,
            LimitWaterTemperatureCold = 0x0804,
            TimOffAnti = 0x0805,
            LimitTempCaldera = 0x0806,
            TimeActivateFreeCooling = 0x0807,
            ReturnTempAirMax = 0x0808,
            ReturnTempAirMin = 0x0809,
            TimeProtocol = 0x080A,
            IntensisVelocity = 0x080B,
            AlarmAntiHielo = 0x080C,
            ConfigFreeCooling = 0x080D,
            TimerStartMotor = 0x080E,
            TimerStopMotor = 0x080F,
            ValueRestaFactorVelocidad = 0x0810,
            ValueSuma1FactorVelocidad = 0x0811,
            ValueSuma2FactorVelocidad = 0x0812,
            ApplyTableCompatibility = 0x0813,
            TimerOffFan = 0x0814,
            TempProtectionRadiant = 0x0815,
            TempProtectionRefresh = 0x0816,
            HisTmpFrioUbac = 0x0817,
            HisTmpCalorUbac = 0x0818
        }

        [Flags]
        public enum RadioFrequency
        {
            _433MHz,
            _434MHz
        }

        [Flags]
        public enum Relays
        {
            RELAY1 = 0X0001,
            RELAY2 = 0X0002,
            RELAY3 = 0X0004,
            RELAY4 = 0X0008,
            RELAY5 = 0X0010,
            RELAY6 = 0X0020,
            RELAY7 = 0X0040,
        }

        public enum RelaysCombination
        {
            [Description ("relés impares")]
            RELAY_ODDS = 0x0055,
            [Description("relés pares")]
            RELAY_PAIRS = 0X002A 
        }

        [Flags]
        public enum Switches
        {
            ALL_SWITCHES_ON = 0X0000,
            ALL_SWITCHES_OFF = 0X00FF
        }

        [Flags]
        public enum Grids
        {
            GRID1 = 0X0000,
            GRID2 = 0X0001,
            GRID3 = 0X0002,
            GRID4 = 0X0003,
            GRID5 = 0X0004,
            GRID6 = 0X0005
        }

        [Flags]
        public enum Leds
        {
            LED_GRID1 = 0X0001,
            LED_GRID2 = 0X0002,
            LED_GRID3 = 0X0004,
            LED_GRID4 = 0X0008,
            LED_GRID5 = 0X0010,
            LED_GRID6 = 0X0020,
            ALL_LEDS = 0X003F
        }

        [Flags]
        public enum DigitalInputs
        {
            DIGITAL_INPUT1 = 0X03F6,
            DIGITAL_INPUT2 = 0X03F7
        }

        [Flags]
        public enum Probes
        {
            [Description("SONDA1")]
            PROBE1_C = 0X03ED,
            [Description("SONDA2")]
            PROBE2_C = 0X03EE,
            [Description("NTC1")]
            PROBE1_POINTS = 0X03F8,
            [Description("NTC2")]
            PROBE2_POINTS = 0X03F9
        }

        [Flags]
        public enum Patrones
        {
            _10C = 20272,
            _5C = 26094,
            _30C = 8025,
            _18C = 13781,
            _85C = 1070,
            _40C = 5279,
            _6C = 24791
        }

        [Flags]
        public enum Registers
        {        
            READING_SWITCH = 0X03F1,
            FLAG_TEST = 0X03E9,
            AUTHORIZATION_START_TEST = 0X0415,
            MASK_CONTROL_LEDS_GRIDS = 0X0403,
            CLOSE_OFF_LEDS_AND_RELAYS = 0X03F5,
            STATE_GRID = 0X03F2,
            IDENTIFICATION_NUMBER = 0X03EB,
            PERIFERIC_NUMBER = 0x0414,
            
            //IN_DIGITAL1 = 0X03F6,
            //IN_DIGITAL2 = 0X03F7,

            COUNTERS_AREAS = 0X03FB,
            TIMER_GRID = 0X0801,
            FREQUENCY_SELECTION = 0x0401,
            VERSION_FIRMWARE = 0X0416,

            FLASH = 0X03EA,
            SELECTION_GRID = 0X03F4,
            SELECTION_MANIOBRE_GRID = 0X03F3,
            LED_GRID = 0X03F2,
            RELAY_AND_LED = 0X03F5,


            TIME_MOVE_GRID = 0X03FA,
            TABLE_OF_TEMPORIZATORS = 0X07FD,
            SERIAL_NUMBER = 0X041A
        }
        #endregion
    }
}
