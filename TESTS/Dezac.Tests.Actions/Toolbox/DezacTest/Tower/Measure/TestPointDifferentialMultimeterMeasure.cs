﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(TestPointDifferentialMultimeterMeasureActionDescription))]
    [Browsable(true)]
    public class TestPointDifferentialMultimeterMeasure : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Numero de de las primeras muestras que descartamos")]
        [DefaultValue(0)]
        public int DeleteFirst { get; set; }

        [Category("3. Configuration")]
        [Description("Numero de muestras")]
        [DefaultValue(3)]
        public int NumberSamples { get; set; }

        [Category("3.3 Time")]
        [Description("Tiempo de espera entre muestras en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeBetweenSamples { get; set; }

        [Category("3.3 Time")]
        [Description("Tiempo de espera antes de realizar la medida en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeReadMeasure { get; set; }

        [Category("3. Configuration")]
        [Description("Magnitud electrica a medir")]
        [DefaultValue(MagnitudsMultimeter.VoltDC)]
        public MagnitudsMultimeter MagnitudToMeasure { get; set; }

        [Category("3.2 MUX")]
        [Description("La segunda entrada de la multimplexora a seleccionar")]
        [DefaultValue(InputMuxEnum.NOTHING)]
        public InputMuxEnum PrimaryInputMultiplexora { get; set; }

        [Category("3.2 MUX")]
        [Description("La primera entrada de la multimplexora a seleccionar")]
        [DefaultValue(InputMuxEnum.NOTHING)]
        public InputMuxEnum SecondaryInputMultiplexora { get; set; }

        [Category("3.1 Output")]
        [Description("Salida digital de la torre a activar para realizar la medida")]
        public int? ActiveOutput { get; set; }

        [Category("3.1 Output")]
        [Description("Salida digital de la torre a desactivar para realizar la medida")]
        public int? DesactiveOutput { get; set; }

        [Category("3. Configuration")]
        [Description("Descripción del nombre con el que se registrara en la BBDD los resultados de este test point")]
        public string TestPointDescription { get; set; }

        public override void Execute()
        {
            double primaryReading = 0;
            double secondaryReading = 0;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw(); ;

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw(); ;

            string name = "";
            double min = 0;
            double max = 0;
            ParamUnidad unidad = ParamUnidad.SinUnidad;

            switch (MagnitudToMeasure)
            {
                case MagnitudsMultimeter.AmpAC:
                    name = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.A;
                    break;

                case MagnitudsMultimeter.AmpDC:
                    name = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.A;
                    break;

                case MagnitudsMultimeter.Resistencia:
                    name = testBase.Params.RES.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.RES.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.RES.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.Ohms;
                    break;

                case MagnitudsMultimeter.VoltAC:
                    name = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;

                case MagnitudsMultimeter.VoltDC:
                    name = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;
            }

            var adjustValueTestPoint = new AdjustValueDef("DIFF_" + name, 0, min, max, 0, 0, unidad);

            if (ActiveOutput.HasValue)
                tower.IO.DO.On(ActiveOutput.Value);

            if (DesactiveOutput.HasValue)
                tower.IO.DO.Off(DesactiveOutput.Value);

            testBase.TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                primaryReading = tower.MeasureMultimeter(PrimaryInputMultiplexora, MagnitudToMeasure, WaitTimeReadMeasure);
                secondaryReading = tower.MeasureMultimeter(SecondaryInputMultiplexora, MagnitudToMeasure, WaitTimeReadMeasure);
                return primaryReading - secondaryReading;

            }, DeleteFirst, NumberSamples, WaitTimeBetweenSamples,false);

            if (ActiveOutput.HasValue)
                tower.IO.DO.Off(ActiveOutput.Value);

            if (DesactiveOutput.HasValue)
                tower.IO.DO.On(DesactiveOutput.Value);
        }

    }

    public class TestPointDifferentialMultimeterMeasureActionDescription : ActionDescription
    {
        public override string Name { get { return "TestPointDifferentialMeasure"; } }
        public override string Description { get { return "Acción genérica para medir diferencias entre medidas en la torre"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }

}
