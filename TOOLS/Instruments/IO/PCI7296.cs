﻿using Instruments.Utility;
using log4net;
using Microsoft.Win32;
using System;
using System.Runtime.InteropServices;

namespace Instruments.IO
{
    [InstrumentsVersion(1.02)]
    public class PCI7296 : IOBase
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(PCI7296));

        private const ushort PCI_7296 = 13;

        [DllImport("Pci-Dask.dll")]
        private static extern ushort Register_Card(ushort cardType, ushort card_num);

        [DllImport("Pci-Dask.dll")]
        private static extern ushort Release_Card(ushort cardNumber);

        [DllImport("Pci-Dask.dll")]
        private static extern ushort DIO_PortConfig(ushort CardNumber, ushort Port, ushort Direction);

        [DllImport("Pci-Dask.dll")]
        private static extern ushort DO_WritePort(ushort CardNumber, ushort Port, int Value);

        [DllImport("Pci-Dask.dll")]
        private static extern ushort DO_ReadPort(ushort CardNumber, ushort Port, ref int Value);

        private ushort cardNumber;
        private ChannelConfiguration config;

        private static readonly object syncObject = new object();

        public enum ChannelsEnum
        {
            CH0 = 0,
            CH1 = 1,
            CH2 = 2,
            CH3 = 3,
        }

        public enum PortsEnum
        {
            A = 0,
            B = 1,
            CL = 2,
            CU = 3,
        }

        public PCI7296()
            : base(logger)
        {
            RegistryKey rk1 = Registry.LocalMachine;
            var rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\PCI7296", false);
            var rk3 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\PCI7200", false);
            if (rk2 == null && rk3 == null)
                throw new IOException(IOException.Codes.DEVICE_NOT_INSTALLED);

            Config().SetChannels(4, 4, (c) =>
            {
                c.Ports[(int)PortsEnum.CU].BitsPerPort = 4;
                c.Ports[(int)PortsEnum.CL].BitsPerPort = 4;
            });

            Layout();
        }

        #region Public method

        public void Active(int output, int waitTime = 0)
        {
            DO.OnWait(waitTime, output);
        }

        public void PulseActive(int pulseTime, int output)
        {
            DO.PulseOn(pulseTime, output);
        }

        public void PulseDesactive(int pulseTime, int output)
        {
            DO.PulseOff(pulseTime, output);
        }

        public void Desactive(int output, int waitime = 0)
        {
            DO.OffWait(waitime, output);
        }

        public bool InputDetection(int input)
        {
            return DI[input];
        }

        public bool InputsDetection(bool state, params int[] inputs)
        {
            if (state)
                return DI.ReadAllOn(inputs);
            else
                return DI.ReadAllOff(inputs);
        }

        public bool InputWaitOn(int input, int timeOut)
        {
            return DI.WaitOn(input, timeOut);
        }

        public bool InputWaitOff(int input, int timeOut)
        {
            return DI.WaitOff(input, timeOut);
        }

        #endregion

        private ChannelConfiguration Config()
        {
            config = config ?? new ChannelConfiguration(this);

            return config;
        }

        private void Layout(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                ApplyDefaultLayout();
        }

        private void ApplyDefaultLayout()
        {
            Config()
                .SetChannelAsInput((int)ChannelsEnum.CH0)
                .SetChannelAsOutput((int)ChannelsEnum.CH1)
                .SetChannelAsOutput((int)ChannelsEnum.CH2)
                .SetPortAsOutput((int)ChannelsEnum.CH3, (int)PortsEnum.A)
                .SetPortAsInput((int)ChannelsEnum.CH3, (int)PortsEnum.B)
                .SetPortAsInput((int)ChannelsEnum.CH3, (int)PortsEnum.CU)
                .SetPortAsOutput((int)ChannelsEnum.CH3, (int)PortsEnum.CL)
                .Apply();
        }

        protected override void InternalApplyConfig(IIOBaseConfiguration config)
        {
            this.config = config as ChannelConfiguration;

            cardNumber = Register_Card(PCI_7296, 0);
            if (cardNumber < 0)
                throw new IOException(IOException.Codes.CANNOT_CONFIG);

            ushort result;

            foreach(IOChannel channel in this.config.Channels)
                foreach (var port in channel.Ports)
                {
                    result = DIO_PortConfig(cardNumber, GetCardPort(port), (ushort)port.IOType);
                    if (result < 0)
                        throw new IOException(IOException.Codes.CANNOT_CONFIG);

                    port.Data = 0;

                    if (port.IOType == IOType.Output)
                        InternalWritePort(port);
                    else
                        InternalReadPort(port);
                }
        }

        protected override T InternalRead<T>(IOType type, int num)
        {
            var port = GetPort(type, num);
            int bit = ((num - port.MinBit) % port.BitsPerPort);
            bool result = false;

            lock (syncObject)
            {
                var value = InternalReadPort(port);

                result = (value & (1 << bit)) > 0;

                port[bit] = result;
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        protected override void InternalWrite<T>(int num, T value)
        {
            var port = GetPort(IOType.Output, num);
            int bit = ((num - port.MinBit) % port.BitsPerPort);

            lock (syncObject)
            {
                port[bit] = Convert.ToBoolean(value);

                InternalWritePort(port);
            }
        }

        protected byte InternalReadPort(IOPort port)
        {
            int value = 0;

            var result = DO_ReadPort(cardNumber, GetCardPort(port), ref value);
            if (result < 0)
                throw new InstrumentException(111);

            if (port.IOType == IOType.Output)
                value = ~value;

            return (byte)(0xFF & value);
        }

        protected void InternalWritePort(IOPort port)
        {
            var value = 0xFF & ~port.Data;

            var result = DO_WritePort(cardNumber, GetCardPort(port), value);

            if (result < 0)
                throw new InstrumentException(111);
        }

        private ushort GetCardPort(IOPort port)
        {
            int idx = port.Channel.Ports.IndexOf(port);
            if (idx > 1)
                idx++;

            ushort num = Convert.ToUInt16((port.Channel.NumChannel * 5) + idx);

            return num;
        }

        internal IOPort GetPort(IOType type, int num)
        {
            foreach(var channel in config.Channels)
                foreach (IOPort port in channel.Ports)
                    if (port.IOType == type && num >= port.MinBit && num <= port.MaxBit)
                        return port;

            throw new IOException(IOException.Codes.INVALID_PORT);
        }

        public override void Dispose()
        {
            if (cardNumber > 0)
                Release_Card(cardNumber);

            base.Dispose();
        }
    }

}
