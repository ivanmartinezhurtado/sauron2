﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace WinTestPlayer.Controls.Expanders
{
    public class SettingsExpander : Expander
    {
        public SettingsExpander()
        {
            //this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
        }

        protected override ExpanderElement CreateExpanderElement()
        {
            return new SettingsExpanderElement();
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);

            if (this.DesignMode || this.Disposing)
            {
                return;
            }

            if (e.Control is RadPanel && e.Control.Name == "settingsPanel")
            {
                ((SettingsExpanderElement)this.ExpanderElement).ThemesAndSettingsContainer.Controls.Add(e.Control);
            }

            if (e.Control is RadPanel && e.Control.Name == "themesPanel")
            {
                ((SettingsExpanderElement)this.ExpanderElement).ThemesAndSettingsContainer.Controls.Add(e.Control);
            }
        }

        public RadPanel ThemesAndSettingsContainer
        {
            get
            {
                return ((SettingsExpanderElement)this.ExpanderElement).ThemesAndSettingsContainer;
            }
        }

        public RadPanel ThemesPanel
        {
            get
            {
                return ((SettingsExpanderElement)this.ExpanderElement).ThemesPanel;
            }
        }

        public RadPanel SettingsPanel
        {
            get
            {
                return ((SettingsExpanderElement)this.ExpanderElement).SettingsPanel;
            }
        }   

        protected override System.Drawing.Size DefaultSize
        {
            get
            {
                return new Size(200, 200);
            }
        }
    }
}
