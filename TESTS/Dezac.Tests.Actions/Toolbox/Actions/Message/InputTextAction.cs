﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(InputTextActionDescription))]
    public class InputTextAction : ActionBase
    {
        protected ConfigurationSamplerWithCancel configuration = new ConfigurationSamplerWithCancel();

        [Description("Mensaje sobre la entrada de datos")]
        [Category("3. Configuration")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable Message { get; set; } = new ActionVariable();

        [Description("Título del formulario")]
        [Category("3. Configuration")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ActionVariable Title { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var context = SequenceContext.Current;
            if (context == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el contexto").Throw();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var iShell = context.Services.Get<IShell>();

            if (DataOutput.IsKeyNullOrEmpty)
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA("DataOutput").Throw();

            var inputTextbox = iShell.ShowDialog<string>(Title.Value.ToString(), () =>
            {
                return new InputKeyBoard(Message.Value.ToString());
            }
            , null, (c, d) =>
            {
                if (d == DialogResult.OK)
                    return ((InputKeyBoard)c).TextValue;

                return string.Empty;
            });
            DataOutput.Value = inputTextbox;
            testBase.AddResult(string.Format("Entrada de datos a la variable {0} = {1}", DataOutput, inputTextbox));
            Logger.InfoFormat("Entrada de datos a la variable {0} = {1}", DataOutput, inputTextbox);
        }
    }

    public class InputTextActionDescription :  ActionDescription
    {
        public override string Name { get { return "InputText"; } }
        public override string Description { get { return "Permite entrar un texto al operario mostrando una imagen"; } }
        public override string Category { get { return "Actions/Message"; } }
        public override Image Icon { get { return Properties.Resources.Message; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}