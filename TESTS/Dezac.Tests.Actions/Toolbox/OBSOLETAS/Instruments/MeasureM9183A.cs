﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(MeasureM9183ADescription))]
   
    public class MeasureM9183A : ActionBase
    {    
        [Description("NPLC")]
        [DefaultValue(1)]
        public double NPLC { get; set; }

        [Description("Rango")]
        [DefaultValue(2)]
        public double Range { get; set; }

        [Description("digitosPrecision")]
        [DefaultValue(128)]
        public double digitosPrecision { get; set; }

        [Description("Numero de de las primeras muestras que descartamos")]
        [DefaultValue(0)]
        public int DeleteFirst { get; set; }

        [Description("Numero de muestras")]
        [DefaultValue(3)]
        public int NumberSamples { get; set; }

        [Description("Tiempo de espera entre muestras en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeBetweenSamples { get; set; }

        [Description("Tiempo de espera antes de realizar la medida en milisegundos")]
        [DefaultValue(500)]
        public int WaitTimeReadMeasure { get; set; }

        [Description("Magnitud electrica a medir")]
        [DefaultValue(MagnitudsMultimeter.VoltDC)]
        public MagnitudsMultimeter MagnitudToMeasure { get; set; }

        [Description("Descripción del nombre con el que se registrara en la BBDD los resultados de este test point")]
        public string TestPointDescription { get; set; }

        [Description("ChanalaMUX a medir")]
        [DefaultValue(null)]
        public int? Channel { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            var context = SequenceContext.Current;
            if (context == null)
                throw new Exception("Error. No se ha inicializado el contexto");

            var Shell = context.Services.Get<IShell>();
            if (Shell == null)
                throw new Exception("Error. No se ha inicializado el contexto de Shell");

            string name = "";
            double min = 0;
            double max = 0;
            ParamUnidad unidad = ParamUnidad.SinUnidad;

            switch (MagnitudToMeasure)
            {
                case MagnitudsMultimeter.AmpAC:
                    name = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.I_AC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.A;
                    break;

                case MagnitudsMultimeter.AmpDC:
                    name = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.I_DC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.A;
                    break;

                case MagnitudsMultimeter.Resistencia:
                    name = testBase.Params.RES.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.RES.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.RES.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.Ohms;
                    break;

                case MagnitudsMultimeter.VoltAC:
                    name = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.V_AC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;

                case MagnitudsMultimeter.VoltDC:
                    name = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Name;
                    min = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Min();
                    max = testBase.Params.V_DC.Null.TestPoint(TestPointDescription).Max();
                    unidad = ParamUnidad.V;
                    break;
            }

            var adjustValueTestPoint = new AdjustValueDef(name, 0, min, max, 0, 0, unidad);

            tower.MultimeterM9183A.ConfigurePowerLineFrequency(freqEnum._50Hz);
            tower.MultimeterM9183A.ConfigureNPLC(MagnitudToMeasure.ToString(), NPLC);
            tower.MultimeterM9183A.ConfigureFunction(MagnitudToMeasure.ToString(), Range, digitosPrecision);

            if (Channel.HasValue)
                tower.MUX.Connect(Channel.Value, Outputs.Multimetro);

            testBase.TestMeasureBase(adjustValueTestPoint, (step) =>
            { 
                var result = tower.MultimeterM9183A.ReadMeasure(WaitTimeReadMeasure);
                return result;

            }, DeleteFirst, NumberSamples, WaitTimeBetweenSamples);
        }
    }

    public class MeasureM9183ADescription : ActionDescription
    {
        public override string Name { get { return "MeasureM9183A"; } }
        public override string Description { get { return "Acción para medir con el Multimetro M9183A"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.ksicon16; } }
    }
}
