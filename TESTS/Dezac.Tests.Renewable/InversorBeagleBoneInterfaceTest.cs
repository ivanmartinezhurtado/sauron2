using Dezac.Tests.Model;
using System;
using System.IO.Ports;

namespace Dezac.Tests.Renewable
{
    [TestVersion(1.00)]    
    public class InversorBeagleBoneInterfaceTest : TestBase
    {
        public void TestBridge(byte SendingPortNumber, byte ReceivingPortNumber, string SendingPortName, string ReceivingPortName, string textToSend)
        {
            using (SerialPort sender = new SerialPort())
            {
                sender.PortName = "COM" + SendingPortNumber;
                sender.WriteTimeout = 1000;

                using (var receiver = new SerialPort())
                {
                    receiver.PortName = "COM" + ReceivingPortNumber;
                    receiver.ReadTimeout = 5000;
                     
                    sender.Open();
                    System.Threading.Thread.Sleep(100);

                    receiver.Open();
                    System.Threading.Thread.Sleep(100);

                    sender.WriteLine(textToSend);

                    System.Threading.Thread.Sleep(100);

                    var receivedString = receiver.ReadLine();

                    if (receivedString != textToSend)
                        throw new Exception("Error. La trama leida no se corresponde con la enviada");

                    Resultado.Set("BRIDGE_" + SendingPortName + ReceivingPortName, "OK", ParamUnidad.SinUnidad);

                }
            }
        }
    }
}
