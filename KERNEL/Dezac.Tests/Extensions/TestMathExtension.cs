﻿using Dezac.Tests.Utils;
using System;

namespace Dezac.Tests.Extensions
{
    public static class TestMathExtension
    {
        public static GainOffsetOfLine CalculateGainOffsetOfLine(this TestBase testBase, PointOfLine point1, PointOfLine point2)
        {
            var gain = 0D;
            var offset = 0D;

            var dx = point2.x - point1.x;
            var dy = point2.y - point1.y;

            gain = dy / dx;
            offset = point1.y - gain * point1.x;

            return new GainOffsetOfLine() { gain = gain, offset = offset };
        }
    }
}
