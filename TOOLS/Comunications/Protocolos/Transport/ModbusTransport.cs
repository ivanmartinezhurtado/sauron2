using Comunications.Protocolo;
using Comunications.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Comunications.Message
{
    public class ModbusTransport : MessageTransport
	{
        public bool UseFunction06 { get; set; }
        public ModbusTransport(IStreamResource streamResource)
            : base(new ModbusProtocol(), streamResource)
		{
            this.ReadTimeout = Modbus.DefaultTimeout;
            LogRawMessage = true;
            LogMessage = false;
		}
        public ModbusTransport(IStreamResource streamResource, ILog logger)
            : base(new ModbusProtocol(), streamResource, logger)
        {
            this.ReadTimeout = Modbus.DefaultTimeout;
            LogRawMessage = true;
            LogMessage = false;
        }
        public override T CreateMessage<T>(byte[] frame)
        {
            byte functionCode = frame[1];
            ITransportMessage message;

            // check for slave exception response else create message from frame
            if (functionCode > Modbus.ExceptionOffset)
                message = new SlaveExceptionResponse();
            else
                message = new T();

            message.Initialize(frame);

            return (T)message;
        }

        protected override string GetLogFrameString(byte[] frame)
        {
            return Extensions.BytesToHexString(frame);
        }
        protected override void ValidateResponse(ITransportMessage request, ITransportMessage response)
		{
            if (request is SlaveExceptionResponse)
                throw new ModbusException((SlaveExceptionResponse)request);

            var typedRequest = request as ModbusMessage;
            var typedResponse = response as ModbusMessage;

            if (typedRequest.FunctionCode != typedResponse.FunctionCode)
                throw new IOException(String.Format(CultureInfo.InvariantCulture, "Received response with unexpected Function Code. Expected {0}, received {1}.", typedRequest.FunctionCode, typedResponse.FunctionCode));

            if (typedRequest.SlaveAddress != typedResponse.SlaveAddress)
                throw new IOException(String.Format(CultureInfo.InvariantCulture, "Response slave address does not match request. Expected {0}, received {1}.", typedRequest.SlaveAddress, typedResponse.SlaveAddress));

            typedRequest.ValidateResponse(response);
		}

        // HOLDING REGISTERS FUNCION 03 *****************************

        public ReadHoldingInputRegistersRequest ReadHoldingRegisters(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            var message = new ReadHoldingInputRegistersRequest(Modbus.ReadHoldingRegisters, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadHoldingInputRegistersRequest>(message);

            return response;
        }
        public T ReadHoldingRegisters<T>(byte slaveAddress) where T : struct
        {
            Type type = typeof(T);
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            ushort numRegisters = (ushort)(Marshal.SizeOf(type) / 2);

            return (T)ReadHoldingRegisters<T>(slaveAddress, mType.Address, numRegisters);
        }
        public T ReadHoldingRegisters<T>(byte slaveAddress, ushort startAddress) where T : struct
        {
            Type type = typeof(T);
            ushort numRegisters = (ushort)(Marshal.SizeOf(type) / 2);
            return (T)ReadHoldingRegisters<T>(slaveAddress, startAddress, numRegisters);
        }
        public T ReadHoldingRegisters<T>(byte slaveAddress, ushort startAddress, ushort numberOfPoints) where T : struct
        {
            var message = new ReadHoldingInputRegistersRequest(Modbus.ReadHoldingRegisters, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadHoldingInputRegistersRequest>(message);

            return Extensions.AsignaEstructura<T>(response.Data);
        }
        public ushort[] ReadMultipleHoldingRegister(byte slaveAddress, ushort startAddress, ushort numberOfPoints = 1)
        {
            var data = ReadHoldingRegisters(slaveAddress, startAddress, numberOfPoints);

            ushort[] Response = new ushort[numberOfPoints];

            for (int i = 0; i < numberOfPoints; i++)
                Response[i] = data.GetRegister(i * 2);

            return Response;
        }
        public int ReadInt32HoldingRegister(byte slaveAddress, ushort startAddress)
        {
            var response = ReadHoldingRegisters(slaveAddress, startAddress, 2);
            int value = BitConverter.ToInt32(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            return value;
        }
        public long ReadInt64HoldingRegister(byte slaveAddress, ushort startAddress)
        {
            var response = ReadHoldingRegisters(slaveAddress, startAddress, 4);
            var value = BitConverter.ToInt64(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            return value;
        }
        public string ReadStringHoldingRegister(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            byte[] data = ReadHoldingRegisters(slaveAddress, startAddress, numberOfPoints).Data;
            return ASCIIEncoding.ASCII.GetString(data);
        }
        public string ReadHexStringHoldingRegister(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            byte[] data = ReadHoldingRegisters(slaveAddress, startAddress, numberOfPoints).Data;
            return Extensions.BytesToHexString(data);
        }
        public float ReadFloat32HoldingRegister(byte slaveAddress, ushort startAddress)
        {
            var bytes = ReadHoldingRegisters(slaveAddress, startAddress, 2).Data;
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            return BitConverter.ToSingle(bytes, 0);
        }
        public Dezac.Core.Utility.float16 ReadFloat16HoldingRegister(byte slaveAddress, ushort startAddress)
        {
            var bytes = ReadHoldingRegisters(slaveAddress, startAddress, 1).Data;
            Array.Resize(ref bytes, 4);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            return (Dezac.Core.Utility.float16)BitConverter.ToSingle(bytes, 0);
        }
        public DateTime ReadInt32EpochHoldingRegister(byte slaveAddress, ushort startAddress)
        {
            var response = ReadHoldingRegisters(slaveAddress, startAddress, 2);
            int value = BitConverter.ToInt32(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            var localDateTime = DateTimeOffset.FromUnixTimeSeconds(value).DateTime.ToLocalTime();
            return localDateTime;
        }
        public DateTime ReadInt64EpochHoldingRegister(byte slaveAddress, ushort startAddress)
        {
            var response = ReadHoldingRegisters(slaveAddress, startAddress, 2);
            long value = BitConverter.ToInt64(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            var localDateTime = DateTimeOffset.FromUnixTimeSeconds(value).DateTime.ToLocalTime();
            return localDateTime;
        }

        // INPUT REGISTERS FUNCION 04 *******************************
        public ReadHoldingInputRegistersRequest ReadInputRegisters(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            var message = new ReadHoldingInputRegistersRequest(Modbus.ReadInputRegisters, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadHoldingInputRegistersRequest>(message);

            return response;
        }
        public T ReadInputRegisters<T>(byte slaveAddress) where T : struct
        {
            Type type = typeof(T);
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            ushort numRegisters = (ushort)(Marshal.SizeOf(type) / 2);

            return (T) ReadInputRegisters<T>(slaveAddress, mType.Address, numRegisters);
        }
        public T ReadInputRegisters<T>(byte slaveAddress, ushort startAddress) where T : struct
        {
            Type type = typeof(T);
            ushort numRegisters = (ushort)(Marshal.SizeOf(type) / 2);
            return (T)ReadInputRegisters<T>(slaveAddress, startAddress, numRegisters);
        }
        public T ReadInputRegisters<T>(byte slaveAddress, ushort startAddress, ushort numberOfPoints) where T : struct
        {
            var message = new ReadHoldingInputRegistersRequest(Modbus.ReadInputRegisters, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadHoldingInputRegistersRequest>(message);

            return Extensions.AsignaEstructura<T>(response.Data);
        }
        public object ReadInputRegisters(Type type, byte slaveAddress) 
        {
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            ushort numRegisters = (ushort)(Marshal.SizeOf(type) / 2);

            return ReadInputRegisters(type, slaveAddress, mType.Address, numRegisters);
        }
        public object ReadInputRegisters(Type type, byte slaveAddress, ushort startAddress)
        {
            ushort numRegisters = (ushort)(Marshal.SizeOf(type) / 2);
            return ReadInputRegisters(type, slaveAddress, startAddress, numRegisters);
        }
        public object ReadInputRegisters(Type type, byte slaveAddress, ushort startAddress, ushort numberOfPoints) 
        {
            var message = new ReadHoldingInputRegistersRequest(Modbus.ReadInputRegisters, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadHoldingInputRegistersRequest>(message);

            return Extensions.AsignaEstructura(type, response.Data);
        }
        public ushort[] ReadMultipleRegister(byte slaveAddress, ushort startAddress, ushort numberOfPoints = 1)
        {
            var data = ReadInputRegisters(slaveAddress, startAddress, numberOfPoints);

            ushort[] Response = new ushort[numberOfPoints];

            for (int i = 0; i < numberOfPoints; i++)
                Response[i] = data.GetRegister(i * 2);

            return Response;
        }
        public int ReadInt32(byte slaveAddress, ushort startAddress)
        {
            var response = ReadInputRegisters(slaveAddress, startAddress, 2);
            int value = BitConverter.ToInt32(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            return value;
        }
        public long ReadInt64(byte slaveAddress, ushort startAddress)
        {
            var response = ReadInputRegisters(slaveAddress, startAddress, 4);
            var value = BitConverter.ToInt64(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            return value;
        }
        public string ReadString(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            byte[] data = ReadInputRegisters(slaveAddress, startAddress, numberOfPoints).Data;
            return ASCIIEncoding.ASCII.GetString(data);
        }

        public string ReadHexString(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            byte[] data = ReadInputRegisters(slaveAddress, startAddress, numberOfPoints).Data;
            return Extensions.BytesToHexString(data);
        }

        public float ReadFloat32(byte slaveAddress, ushort startAddress)
        {
            var bytes = ReadInputRegisters(slaveAddress, startAddress, 2).Data;
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            return BitConverter.ToSingle(bytes, 0);
        }
        public Dezac.Core.Utility.float16 ReadFloat16(byte slaveAddress, ushort startAddress)
        {
            var bytes = ReadInputRegisters(slaveAddress, startAddress, 1).Data;
            Array.Resize(ref bytes, 4);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            return (Dezac.Core.Utility.float16)BitConverter.ToSingle(bytes, 0);
        }
        public DateTime ReadInt32Epoch(byte slaveAddress, ushort startAddress)
        {
            var response = ReadInputRegisters(slaveAddress, startAddress, 2);
            int value = BitConverter.ToInt32(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            var localDateTime = DateTimeOffset.FromUnixTimeSeconds(value).DateTime.ToLocalTime();
            return localDateTime;
        }
        public DateTime ReadInt64Epoch(byte slaveAddress, ushort startAddress)
        {
            var response = ReadInputRegisters(slaveAddress, startAddress, 2);
            long value = BitConverter.ToInt64(response.Data, 0);
            value = IPAddress.NetworkToHostOrder(value);
            var localDateTime = DateTimeOffset.FromUnixTimeSeconds(value).DateTime.ToLocalTime();
            return localDateTime;
        }

        // COIL REGISTERS FUNCION 01 *******************************
        public ReadCoilsInputsRequest ReadCoils(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            var message = new ReadCoilsInputsRequest(Modbus.ReadCoils, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadCoilsInputsRequest>(message);

            return response;
        }

        public byte[] ReadMultipleCoils(byte slaveAddress, ushort startAddress, ushort numberOfPoints = 1)
        {
            return ReadCoils(slaveAddress, startAddress, numberOfPoints).Data;
        }

        public T ReadMultipleCoils<T>(byte slaveAddress) where T : struct
        {
            Type type = typeof(T);
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            var numberOfPoints = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Length;

            var response = ReadCoils(slaveAddress, mType.Address, (ushort)numberOfPoints).Data;

            return Extensions.AsignedArrayByteToBooleanStruct<T>(response);
        }

        public byte ReadCoil(byte slaveAddress, ushort startAddress)
        {
            return ReadCoils(slaveAddress, startAddress, 1).Data[0];
        }

        // DISCRETE INPUTS REGISTERS FUNCION 02 *******************************
        public ReadCoilsInputsRequest ReadInputs(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            var message = new ReadCoilsInputsRequest(Modbus.ReadInputs, slaveAddress, startAddress, numberOfPoints);
            var response = SendMessage<ReadCoilsInputsRequest>(message);

            return response;
        }
        public byte[] ReadMultipleInputs(byte slaveAddress, ushort startAddress, ushort numberOfPoints = 1)
        {
            return ReadInputs(slaveAddress, startAddress, numberOfPoints).Data;
        }
        public byte ReadSinlgeInputs(byte slaveAddress, ushort startAddress)
        {
            var resp= ReadInputs(slaveAddress, startAddress, 1).Data;
            return resp[0];
        }

        public T ReadSinlgeInputs<T>(byte slaveAddress) where T : struct
        {
            Type type = typeof(T);
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            var numberOfPoints = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Length;

            var response = ReadMultipleInputs(slaveAddress, mType.Address, (ushort)numberOfPoints);

            return Extensions.AsignedArrayByteToBooleanStruct<T>(response);
        }

        // WRITE SIGLE COIL FUNCTION 05 *******************************
        public void WriteSingleCoil(byte slaveAddress, ushort startAddress, bool coilState)
        {
            var message = new WriteSingleCoilRequest(slaveAddress, startAddress, coilState);
            var response = SendMessage<WriteSingleCoilRequest>(message);
        }

        public void WriteMultipleCoil<T>(byte slaveAddress, T value) where T : struct
        {
            Type type = typeof(T);
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            var fields = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

            foreach (FieldInfo  val in fields)
                WriteSingleCoil(slaveAddress, mType.Address++, Convert.ToBoolean(val.GetValue(value)));
        }

        // WRITE MULTIPLE COIL FUNCTION 15 *******************************
        public void WriteMultipleCoil(byte slaveAddress, ushort startAddress, bool[] coilState)
        {
            var message = new WriteMultipleCoilsRequest(slaveAddress, startAddress, coilState);
            var response = SendMessage<WriteMultipleCoilsRequest>(message);
        }

        // WRITE MULTIPLE REGISTER FUNCTION 16 *******************************
        // WRITE SIGLE REGISTER FUNCTION 06 (UseFunction06) *******************
        public void WriteSingleRegister(byte slaveAddress, ushort startAddress, ushort value)
        {
            if (UseFunction06)
            {
                var message = new WriteSingleRegisterRequest(slaveAddress, startAddress, value);
                var response = SendMessage<WriteSingleRegisterRequest>(message);
            }
            else
            {
                byte[] data = BitConverter.GetBytes(value);
                var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, new byte[] { data[1], data[0] });
                var response = SendMessage<WriteMultipleRegistersRequest>(message);
            }
        }
        public void WriteInt32(byte slaveAddress, ushort startAddress, int value)
        {
            if (UseFunction06)
            {
                byte[] data = BitConverter.GetBytes(value);
                
                //Array.Reverse(data, 0, 2);
                ushort lo = BitConverter.ToUInt16(data, 0);
                //Array.Reverse(data, 2, 2);
                ushort hi = BitConverter.ToUInt16(data, 2);

                WriteSingleRegister(slaveAddress, startAddress, hi);
                WriteSingleRegister(slaveAddress, Convert.ToUInt16(startAddress + 1), lo);
            }
            else
            {
                byte[] data = BitConverter.GetBytes(IPAddress.NetworkToHostOrder(value));
                var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, data);
                var response = SendMessage<WriteMultipleRegistersRequest>(message);
            }
        }
        public void WriteInt64(byte slaveAddress, ushort startAddress, long value)
        {
            if (UseFunction06)
            {
                byte[] data = BitConverter.GetBytes(value);

                //Array.Reverse(data, 0, 2);
                ushort lo = BitConverter.ToUInt16(data, 0);
                //Array.Reverse(data, 2, 2);
                ushort hi = BitConverter.ToUInt16(data, 2);

                WriteSingleRegister(slaveAddress, startAddress, hi);
                WriteSingleRegister(slaveAddress, Convert.ToUInt16(startAddress + 1), lo);


                //Array.Reverse(data, 0, 2);
                lo = BitConverter.ToUInt16(data, 4);
                //Array.Reverse(data, 2, 2);
                hi = BitConverter.ToUInt16(data, 6);

                WriteSingleRegister(slaveAddress, Convert.ToUInt16(startAddress + 2), hi);
                WriteSingleRegister(slaveAddress, Convert.ToUInt16(startAddress + 3), lo);
            }
            else
            {
                byte[] data = BitConverter.GetBytes(IPAddress.NetworkToHostOrder(value));
                var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, data);
                var response = SendMessage<WriteMultipleRegistersRequest>(message);
            }
        }
        public void WriteMultipleInt32(byte slaveAddress, ushort startAddress, params int[] values)
        {
            byte[] data = new byte[values.Length * 4];

            for (int i = 0; i < values.Length; i++)
            {
                byte[] itemData = BitConverter.GetBytes(IPAddress.NetworkToHostOrder(values[i]));
                itemData.CopyTo(data, i * 4);
            }

            var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, data);
            var response = SendMessage<WriteMultipleRegistersRequest>(message);
        }
        public void WriteFloat32(byte slaveAddress, ushort startAddress, float value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, bytes);
            var response = SendMessage<WriteMultipleRegistersRequest>(message);
        }
        public void WriteFloat16(byte slaveAddress, ushort startAddress, Dezac.Core.Utility.float16 value)
        {
            throw new NotImplementedException();
        }
        public void WriteString(byte slaveAddress, ushort startAddress, string text)
        {
            byte[] data = ASCIIEncoding.ASCII.GetBytes(text);
            WriteInputRegisters(slaveAddress, startAddress, data);
        }
        public void WriteStringByRegister(byte slaveAddress, ushort startAddress, string text)
        {
            var data = new List<byte>();

            foreach (var s in text)
            {
               data.Add(0);
               data.AddRange(ASCIIEncoding.ASCII.GetBytes(s.ToString()));
            }
            
            WriteInputRegisters(slaveAddress, startAddress, data.ToArray());
        }
        public void WriteHexString(byte slaveAddress, ushort startAddress, string text)
        {
            byte[] data =  Extensions.HexStringToBytes(text);
            WriteInputRegisters(slaveAddress, startAddress, data);
        }
        public void WriteFileRecord(byte slaveAddress, byte referenceType, ushort fileNumber, ushort recordNumber, byte[] data)
        {
            var message = new WriteFileRecordRequest(slaveAddress, referenceType, fileNumber, recordNumber, data);
            var response = SendMessage<WriteFileRecordRequest>(message);
        }
        public void WriteFileRecord(byte slaveAddress, ushort fileNumber, byte[] data)
        {
            var message = new WriteFileRecordRequest(slaveAddress, fileNumber, data);
            var response = SendMessage<WriteFileRecordRequest>(message);
        }
        public void WriteInputRegisters<T>(byte slaveAddress, T value) where T : struct
        {
            Type type = typeof(T);
            ModbusLayout mType = type.GetCustomAttributes(typeof(ModbusLayout), true).FirstOrDefault() as ModbusLayout;

            if (mType == null)
                throw new Exception("Impossible obtain modbus attribute info!");

            WriteInputRegisters<T>(slaveAddress, mType.Address, value);
        }
        public void WriteInputRegisters<T>(byte slaveAddress, ushort startAddress, T value) where T : struct
        {
            byte[] data = Extensions.ToBytes(value);

            WriteInputRegisters(slaveAddress, startAddress, data);
        }
        public void WriteObject(byte slaveAddress, ushort startAddress, object value) 
        {
            byte[] data = Extensions.ToBytes(value);

            WriteInputRegisters(slaveAddress, startAddress, data);
        }
        public void WriteInputRegisters(byte slaveAddress, ushort startAddress, byte[] data)
        {

            if (UseFunction06)
            {
                if (data.Length % 2 != 0)
                    throw new Exception(string.Format("Write Invalid data length: => {0}", data.Length));

                for (int i = 0; i < (data.Length / 2); i++)
                {
                    Array.Reverse(data, (i * 2), 2);
                    ushort value = BitConverter.ToUInt16(data, (i * 2));
                    WriteSingleRegister(slaveAddress, (ushort)(startAddress + i), value);
                }
            }
            else
            {
                var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, data);
                var response = SendMessage<WriteMultipleRegistersRequest>(message);
            }
        }
        public void WriteInt32Epoch(byte slaveAddress, ushort startAddress, DateTime dateTime)
        {
            var dateTimeOffset = new DateTimeOffset(dateTime);
            int unixDateTime = (int)dateTimeOffset.ToUnixTimeSeconds();

            byte[] data = BitConverter.GetBytes(IPAddress.NetworkToHostOrder(unixDateTime));

            var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, data);
            SendMessage<WriteMultipleRegistersRequest>(message);
        }
        public void WriteInt64Epoch(byte slaveAddress, ushort startAddress, DateTime dateTime)
        {
            var dateTimeOffset = new DateTimeOffset(dateTime);
            long unixDateTime = dateTimeOffset.ToUnixTimeSeconds();

            byte[] data = BitConverter.GetBytes(IPAddress.NetworkToHostOrder(unixDateTime));

            var message = new WriteMultipleRegistersRequest(slaveAddress, startAddress, data);
            SendMessage<WriteMultipleRegistersRequest>(message);
        }

        // SLAVE MODE 
        public ModbusMessage ReadRequest()
        {
            var frame = ReadRequestMessage(new ModbusMessage());
            ModbusMessage message = ModbusMessageFactory.CreateModbusRequestMessage(frame);
            return message;
        }

        public void WriteRequest(ModbusMessage message)
        {
            Write(message);
        }
    }
}
