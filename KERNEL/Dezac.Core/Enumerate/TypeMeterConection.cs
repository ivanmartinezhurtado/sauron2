﻿using System;

namespace Dezac.Core.Enumerate
{
    public static class TypeMeterConection
    {
        public enum frequency
        {
            _50Hz = 50,
            _60Hz = 60,
        }

        [Flags]
        public enum enumTypeMeterConection
        {
            MONOFASICO = enumFases.FASE_1,
            TRIFASICO = enumFases.FASE_1 | enumFases.FASE_2 | enumFases.FASE_3,
            QUATRIFASICO = enumFases.FASE_1 | enumFases.FASE_2 | enumFases.FASE_3 | enumFases.FASE_4,
            SIXFASICO = enumFases.FASE_1 | enumFases.FASE_2 | enumFases.FASE_3 | enumFases.FASE_4 | enumFases.FASE_5 | enumFases.FASE_6,
        }

        [Flags]
        public enum enumFases
        {
            FASE_1 = (1 << 0),
            FASE_2 = (1 << 1),
            FASE_3 = (1 << 2),
            FASE_4 = (1 << 3),
            FASE_5 = (1 << 4),
            FASE_6 = (1 << 5),
        }
    }
}
