﻿using Dezac.Tests.Model;
using HalconDotNet;
using log4net;
using System.Diagnostics;

namespace Dezac.Tests.AOI
{
    public static class HalconFrameGrabber
    {
        private static readonly ILog logger = LogManager.GetLogger("TEST_UUT");
        private static HTuple adqHandle = null;
        private static readonly object _syncObject = new object();

        public static void InitFrameGrabber()
        {
            lock (_syncObject)
            {
                Stopwatch stopwatch = new Stopwatch();

                if (adqHandle == null)
                {
                    stopwatch.Start();

                    HOperatorSet.OpenFramegrabber("GigEVision", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1,
                        "false", "default", "00111cf72695_TeledyneDALSAInc_SpyderGigESG14", -1, -1, out adqHandle);
                    HOperatorSet.SetFramegrabberParam(adqHandle, "DeviceUserID", "AOI_1");
                    HOperatorSet.SetFramegrabberParam(adqHandle, "FrameActiveTriggerMode", "Off");
                    HOperatorSet.SetFramegrabberParam(adqHandle, "rotaryEncoderInputASource", "Line0");
                    HOperatorSet.SetFramegrabberParam(adqHandle, "rotaryEncoderInputBSource", "Line2");
                    HOperatorSet.SetFramegrabberParam(adqHandle, "grab_timeout", 30000);
                    HOperatorSet.SetFramegrabberParam(adqHandle, "UserSetSelector", "UserSet1");

                    stopwatch.Stop();
                    logger.DebugFormat("FrameGrabber Initialized in {0} ms", stopwatch.ElapsedMilliseconds);
                }
            }
        }

        private static void StopFrameGrabber()
        {
            lock (_syncObject)
            {
                if (adqHandle != null)
                    HOperatorSet.CloseFramegrabber(adqHandle);
            }
        }

        public static HObject GrabImage()
        {
            HObject image = null;

            if (adqHandle == null)
                InitFrameGrabber();

            HOperatorSet.GrabImage(out image, adqHandle);

            return image;
        }
    }
}
