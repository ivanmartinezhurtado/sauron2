﻿using System;
using System.Linq;
using TaskRunner.Enumerators;

namespace TaskRunner.Model
{
    public class ScriptHostBase
    {
        public string Var(string name)
        {
            if (!SequenceContext.Current.Variables.ContainsKey(name))
                throw new Exception($"Variable {name} no existe.");
            return Convert.ToString(SequenceContext.Current.Variables.Get(name));
        }

        public string VarString(string name)
        {
            if (!SequenceContext.Current.Variables.ContainsKey(name))
                throw new Exception($"Variable {name} no existe");
            return Convert.ToString(SequenceContext.Current.Variables.Get(name));
        }

        public bool VarBoolean(string name)
        {
            if (!SequenceContext.Current.Variables.ContainsKey(name))
                throw new Exception($"Variable {name} no existe.");
            return Convert.ToBoolean(SequenceContext.Current.Variables.Get(name));
        }

        public double VarNumber(string name)
        {
            if (!SequenceContext.Current.Variables.ContainsKey(name))
                throw new Exception($"Variable {name} no existe.");
            return Convert.ToDouble(SequenceContext.Current.Variables.Get(name));
        }

        public DateTime VarDateTime(string name)
        {
            if (!SequenceContext.Current.Variables.ContainsKey(name))
                throw new Exception($"Variable {name} no existe.");
            return Convert.ToDateTime(SequenceContext.Current.Variables.Get(name));
        }

        public string var(string name) => Var(name);

        public string Shared(string name)
        {
            if (!SequenceContext.Current.Variables.ContainsKey(name))
                throw new Exception($"Variable {name} no existe");
            return (string)SequenceContext.Current.SharedVariables.Get(name);
        }

        public string shared(string name) => Shared(name);

        public bool HasErrors()
        {
            bool result = SequenceContext.Current.GetSteps(StepExecutionResult.Failed).Any();
            return result;
        }

        public bool StepOK(string name)
        {
            StepResult result = SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .OrderBy(step => step.LoopIndex)
                .LastOrDefault();

            return result != null && result.ExecutionResult == StepExecutionResult.Passed;
        }
        public bool StepKO(string name)
        {
            StepResult result = SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .OrderBy(step => step.LoopIndex)
                .LastOrDefault();

            return result != null && result.ExecutionResult == StepExecutionResult.Failed;
        }
        public bool StepContinued(string name)
        {
            StepResult result = SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .OrderBy(step => step.LoopIndex)
                .LastOrDefault();

            return result != null && result.ExecutionResult == StepExecutionResult.Continued;
        }
        public bool StepAborted(string name)
        {
            StepResult result = SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .OrderBy(step => step.LoopIndex)
                .LastOrDefault();

            return result != null && result.ExecutionResult == StepExecutionResult.Aborted;
        }
        public bool StepOmitted(string name)
        {
            StepResult result = SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .OrderBy(step => step.LoopIndex)
                .LastOrDefault();

            return result != null && result.ExecutionResult == StepExecutionResult.Omitted;
        }
        public bool StepDisabled(string name)
        {
            StepResult result = SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .OrderBy(step => step.LoopIndex)
                .LastOrDefault();

            return result != null && result.ExecutionResult == StepExecutionResult.Disabled;
        }
        public bool StepRetried(string name)
        {
            return SequenceContext.Current.TestResult.StepResults
                .Where(step => step.Step.Name == name)
                .Where(step => step.ExecutionResult == StepExecutionResult.Repeated)
                .Any();
        }
    }
}
