namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTVALOR
    {
        public int NUMTESTFASE { get; set; }
        public int NUMPARAM { get; set; }
        public string VALOR { get; set; }
        public string STEPNAME { get; set; }
        public Nullable<decimal> VALMAX { get; set; }
        public Nullable<decimal> VALMIN { get; set; }
        public string EXPECTEDVALUE { get; set; }
        public Nullable<int> NUMUNIDAD { get; set; }
    
        public virtual T_PARAMETRO T_PARAMETRO { get; set; }
        public virtual T_TESTFASE T_TESTFASE { get; set; }
        public virtual T_UNIDAD T_UNIDAD { get; set; }
    }
}
