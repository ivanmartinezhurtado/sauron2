﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.07)]
    public class NRG96Test : TestBase
    {
        public const int IN_ESTADO_SALIDA_EQ1 = 9;
        //public const int IN_ESTADO_SALIDA_EQ2 = 10;
        //public const int IN_ESTADO_SALIDA_EQ3 = 11;

        public const int IN_DETECCION_INICIO_EQ1 = 24;
        public const int IN_DETECCION_FINAL_EQ1 = 25;
        //public const int IN_DETECCION_INICIO_EQ2 = 26;
        //public const int IN_DETECCION_FINAL_EQ2 = 27;
        //public const int IN_DETECCION_INICIO_EQ3 = 28;
        //public const int IN_DETECCION_FINAL_EQ3 = 29;

        public const int IN_DETECCION_LATERALES_EQ1 = 30;
        //public const int IN_DETECCION_LATERALES_EQ2 = 31;
        //public const int IN_DETECCION_LATERALES_EQ3 = 32;

        public const int OUT_LED_ERROR_EQ1 = 48;
        //public const int OUT_LED_ERROR_EQ2 = 49;
        //public const int OUT_LED_ERROR_EQ3 = 50;

        public const int OUT_OK_EQ1 = 6;
        //public const int OUT_OK_EQ2 = 7;
        //public const int OUT_OK_EQ3 = 8;

        public const int OUT_BLOQUEO_ENTRADA = 7; // EV3;
        public const int OUT_PUNTAS_TEST_EQUIPO = 5; // EV1;
        public const int OUT_RETENER_EQUIPOS = 11;   // EV7;
        public const int OUT_LIBERAR_EQUIPOS = 12;   // EV8;
        public const int OUT_TECLAS_RESET_MAX = 9;   // EV5;
        public const int OUT_TECLAS_SUB_BAJ = 10;    // EV6;
        public const int OUT_PUNTAS_COMUNICACIONES = 57; // EVUTIL;

        //public const int OUT_ANULA_PUESTO_EQ2 = 53;
        //public const int OUT_ANULA_PUESTO_EQ3 = 54;

        internal double transformationRelation;

        private static readonly object _syncObject = new Object();

        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }
        private NRG96 nrg;
        public NRG96 Nrg
        {
            get
            {
                if (nrg == null)
                    nrg = new NRG96(Comunicaciones.SerialPort);

                return nrg;
            }
        }

        internal DeviceInstance NrgUtil { get; set; }
        internal double VoltageSupply { get; set; }
        internal bool tipoAlimentacionDC { get; set; }

        public void TestInitialization(int portNrgEq1 = 0/*, int portNrgEq2 = 0, int portNrgEq3 = 0*/)
        
{
            NrgUtil = GetVariablesByInstance(portNrgEq1/*, portNrgEq2, portNrgEq3*/);

            Nrg.Modbus.PortCom = NrgUtil.Port_TTL;
            Nrg.Modbus.PerifericNumber = NrgUtil.Periferic;
            VoltageSupply = Consignas.GetDouble("VOLTAGE_SUPPLY", 230, ParamUnidad.V);

            

            transformationRelation = Configuracion.GetDouble("RELACION_TRANSFORMACION", 1, ParamUnidad.SinUnidad);

            this.WaitAllTestsAndRunOnlyOnce("TEST_INIT", () =>
            {
                Tower.Active24VDC();
                Tower.ActiveElectroValvule();
                //Tower.IO.DO.OnWait(200, OUT_PUNTAS_TEST_EQUIPO);
                Tower.IO.DO.Off(OUT_LED_ERROR_EQ1);
                Tower.IO.DO.On(15);
                Tower.IO.DO.OnWait(500, OUT_BLOQUEO_ENTRADA);

                if (Nrg.Modbus.PortCom == 3)
                    Tower.IO.DO.On(43);
                //Tower.IO.DO.Off(OUT_LIBERAR_EQUIPOS, OUT_PUNTAS_COMUNICACIONES, OUT_PUNTAS_TEST_EQUIPO, 48, 49, 50);
                //Tower.IO.DO.On(OUT_RETENER_EQUIPOS, 15);
                Tower.ActiveVoltageCircuit();
                Tower.ActiveCurrentCircuit(false, false, false);

                tipoAlimentacionDC = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad).Contains("Vdc");
                if (tipoAlimentacionDC)
                    SamplerWithCancel((p) =>
                    {
                        Tower.LAMBDA.Initialize();
                        Tower.LAMBDA.ApplyOff();
                        return true;
                    }, "Error. No se puede establecer comunicación con la fuente DC LAMBDA", 2, 1500, 0, false, false);
                else
                    Tower.Chroma.ApplyOff();

                Tower.PowerSourceIII.ApplyOff();
                Tower.PowerSourceIII.Tolerance = Configuracion.GetDouble("TOLERANCIA_MTE", 0.1, ParamUnidad.PorCentage);
                Tower.PowerSourceIII.TimeOutStabilitation = (int)Configuracion.GetDouble("TIMEOUT_ESTABILIZACION_MTE", 20000, ParamUnidad.ms);

                if (!IsInstanceRunning(1))
                    Error().PROCESO.OPERARIO.COLOCACION_INCORRECTA_EQUIPO().Throw();

                //if (!IsInstanceRunning(2))
                //    Tower.IO.DO.On(OUT_ANULA_PUESTO_EQ2);
                //else
                //    Tower.IO.DO.Off(OUT_ANULA_PUESTO_EQ2);

                //if (!IsInstanceRunning(3))
                //    Tower.IO.DO.On(OUT_ANULA_PUESTO_EQ3);
                //else
                //    Tower.IO.DO.Off(OUT_ANULA_PUESTO_EQ3);
            });

            Tower.IO.DO.On(NrgUtil.OUT_LiberarEquipo);
        }

        public void TestConsumo()
        {
            this.WaitAllTestsAndRunOnlyOnce("TEST_CONSUMO_INIT", () =>
            {
                Tower.IO.DO.OnWait(500,OUT_PUNTAS_TEST_EQUIPO);
                Tower.IO.DO.OnWait(500, OUT_PUNTAS_COMUNICACIONES);

            if (tipoAlimentacionDC)
                Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(VoltageSupply, 1);
            else
                Tower.Chroma.ApplyPresetsAndWaitStabilisation(VoltageSupply, 1, 50);
            });

            SamplerWithCancel((p) =>
            {
              // var SatusFinal=  Tower.IO.DI[NrgUtil.IN_DeteccionFinal];
               var SatusInicio = Tower.IO.DI[NrgUtil.IN_DeteccionInicio];
               var SatusLateral = Tower.IO.DI[NrgUtil.IN_PistonesLarales];

               if (!SatusInicio)
                    Error().HARDWARE.UTILLAJE.DETECCION_INSTRUMENTO(string.Format("POS EQ{0}", NumInstance)).Throw();

                if (!SatusLateral)
                    Error().HARDWARE.UTILLAJE.DETECCION_INSTRUMENTO(string.Format("PISTON LATERAL EQ{0}", NumInstance)).Throw();

                return true;

            }, "", exception: (p)=> { Error().HARDWARE.UTILLAJE.DETECCION_INSTRUMENTO(p).Throw(); });

            this.WaitAllTestsAndRunOnlyOnce("TEST_CONSUMO_FIN", () =>
            {
                using (var cvmMiniUtillaje = new CVMMINITower(Comunicaciones.SerialPortDevice))
                {
                    cvmMiniUtillaje.Modbus.PerifericNumber = Comunicaciones.PerifericoDevice;

                    TestConsumo(Params.KW.Null.EnVacio.Vmin.Name, Params.KW.Null.EnVacio.Vmin.Min(), Params.KW.Null.EnVacio.Vmin.Max(),
                    () =>
                    {
                        var vars = cvmMiniUtillaje.ReadAllVariables();
                        double powerActive = 0;

                        if (NumInstance == 1)
                            powerActive = vars.Phases.L1.PotenciaActiva;
                        if (NumInstance == 2)
                            powerActive = vars.Phases.L2.PotenciaActiva;
                        if (NumInstance == 3)
                            powerActive = vars.Phases.L3.PotenciaActiva;

                        return powerActive;
                    });

                    TestConsumo(Params.KVAR.Null.EnVacio.Vmin.Name, Params.KVAR.Null.EnVacio.Vmin.Min(), Params.KVAR.Null.EnVacio.Vmin.Max(),
                    () =>
                    {
                        var vars = cvmMiniUtillaje.ReadAllVariables();
                        double powerReactive = 0;

                        if (NumInstance == 1)
                            powerReactive = vars.Phases.L1.PotenciaReactiva;
                        if (NumInstance == 2)
                            powerReactive = vars.Phases.L2.PotenciaReactiva;
                        if (NumInstance == 3)
                            powerReactive = vars.Phases.L3.PotenciaReactiva;

                        return powerReactive;
                    });
                }
            });
        }

        public void TestCommunications()
        {
            SamplerWithCancel((p) =>
            {
                Nrg.WriteFlagTest();
                return true;
            }, "", exception: (p)=> { Error().UUT.COMUNICACIONES.FLAG_TEST().Throw(); });


            var softwareVersion = Nrg.ReadSoftwareVersion();

            Assert.AreEqual("VERSION_FIRMWARE", softwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error de version de firmware, la versión grabada en el equipo es incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestSetupDefault()
        {
            Nrg.WriteFlagTest();

            Nrg.WriteDefaultCommunicationConfiguration();

            NRG96.SetupConfiguration setupConfiguration = new NRG96.SetupConfiguration((int)Configuracion.GetDouble("TENSION_PRIMARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES", "SIMPLE", ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO", 0, ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            Nrg.WriteSetupConfiguration(setupConfiguration);

            NRG96.MaximumDemandConfiguration maximumDemandConfiguration = new NRG96.MaximumDemandConfiguration()
            {
                VariabletoCalculate = Configuracion.GetString("VARIABLE_MAXIMA_DEMANDA", "NO_PD", ParamUnidad.SinUnidad),
                RegisterTime = (ushort)Configuracion.GetDouble("TIEMPO_REGISTRO_MAXIMA_DEMANDA", 15, ParamUnidad.SinUnidad)
            };

            Nrg.WriteMaximumDemandConfiguration(maximumDemandConfiguration);

            NRG96.HardwareVector hardwareVector = new NRG96.HardwareVector()
                {
                    CalculoHarmonicos = Configuracion.GetString("CALCULO_HARMONICOS_VECTOR_HARDWARE", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                    Comunication = Configuracion.GetString("COMUNICACIONES", "SI", ParamUnidad.SinUnidad) == "SI",
                    Rs232 = Configuracion.GetString("RS232/RS485", "RS485", ParamUnidad.SinUnidad) == "RS232",
                    InputCurrentString = Configuracion.GetString("CORRIENTE_ENTRADA", "5", ParamUnidad.SinUnidad),
                    InputVoltageString = Configuracion.GetString("TENSION_ENTRADA", "300", ParamUnidad.SinUnidad),
                    ModeloString = Configuracion.GetString("MODELO", "CVM_NRG_96", ParamUnidad.SinUnidad),
                    PowerSupplyString = Configuracion.GetString("TENSION_ALIMENTACION", "230Vac", ParamUnidad.SinUnidad),
                    Rele = Configuracion.GetString("RELE", "SI", ParamUnidad.SinUnidad) == "SI",
                    Shunt = Configuracion.GetString("SHUNT", "NO", ParamUnidad.SinUnidad) == "SI",
                    PaginasDePotencia = Configuracion.GetString("PAGINA_POTENCIA", "NO", ParamUnidad.SinUnidad) == "SI"
                };

            Nrg.WriteHardwareVector(hardwareVector);

            Nrg.WriteCalibrationFactors(new NRG96.CalibrationFactors((ushort)Configuracion.GetDouble(Params.GAIN_I.Null.TestPoint("DEFECTO").Name, 23815, ParamUnidad.SinUnidad), (ushort)Configuracion.GetDouble(Params.GAIN_V.Null.TestPoint("DEFECTO").Name, 11400, ParamUnidad.SinUnidad)));

            Nrg.WriteITFCalibrationFactors(new NRG96.TriCalibrationFactors((ushort)Configuracion.GetDouble(Params.GAIN_DESFASE.Null.TestPoint("DEFECTO").Name, 0, ParamUnidad.SinUnidad)));

            NRG96.PasswordConfiguration passwordConfiguration = new NRG96.PasswordConfiguration()
                {
                    Password = (ushort)Configuracion.GetDouble("CONTRASEÑA", 1234, ParamUnidad.SinUnidad),
                    SetupLocking = (Configuracion.GetString("BLOQUEO_SETUP", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI") ? (byte)1 : (byte)0
                };

            Nrg.WritePasswordInformation(passwordConfiguration);

            Nrg.WriteInitialScreenMessage(Configuracion.GetString("MENSAJE_INICIAL", "Circutor", ParamUnidad.SinUnidad));

            Nrg.WriteSerialNumber((uint)Configuracion.GetDouble("NUMERO_SERIE_TEST", 12345678, ParamUnidad.SinUnidad));

            NRG96.ScreenConfiguration screenConfiguration = new NRG96.ScreenConfiguration()
            {
                ActivePower = Configuracion.GetString("PANTALLA_POTENCIA_ACTIVA", "SI", ParamUnidad.SinUnidad) == "SI",
                Current = Configuracion.GetString("PANTALLA_CORRIENTE", "SI", ParamUnidad.SinUnidad) == "SI",
                CurrentTHD = Configuracion.GetString("PANTALLA_THD_CORRIENTE", "SI", ParamUnidad.SinUnidad) == "SI",
                PdHzCos = Configuracion.GetString("PANTALLA_PD_HZ_COS", "SI", ParamUnidad.SinUnidad) == "SI",
                PdHzPF = Configuracion.GetString("PANTALLA_PD_HZ_PF", "SI", ParamUnidad.SinUnidad) == "SI",
                PhasesPd = Configuracion.GetString("PANTALLA_PD_FASES", "SI", ParamUnidad.SinUnidad) == "SI",
                PowerFactor = Configuracion.GetString("PANTALLA_FACTOR_POTENCIA", "SI", ParamUnidad.SinUnidad) == "SI",
                ReactivePower = Configuracion.GetString("PANTALLA_POTENCIA_REACTIVA", "SI", ParamUnidad.SinUnidad) == "SI",
                ThreePhaseActiveInductiveCapacitivePower = Configuracion.GetString("PANTALLA_ACTIVA_INDUCTIVA_CAPACITIVA", "SI", ParamUnidad.SinUnidad) == "SI",
                ThreePhaseActiveReactiveApparentPower = Configuracion.GetString("PANTALLA_ACTIVA_REACTIVA_APARENTE", "SI", ParamUnidad.SinUnidad) == "SI",
                VoltagePhaseNeuter = Configuracion.GetString("PANTALLA_TENSION", "SI", ParamUnidad.SinUnidad) == "SI",
                VoltagePhasePhase = Configuracion.GetString("PANTALLA_TENSION_ENTRE_FASES", "SI", ParamUnidad.SinUnidad) == "SI",
                VoltageTHD = Configuracion.GetString("PANTALLA_THD_TENSION", "SI", ParamUnidad.SinUnidad) == "SI",
            };

            //nrg.WriteScreenConfiguration(screenConfiguration);

            Nrg.WriteAllInformationClear();

            NRG96.AlarmConfiguration alarmConfiguration = new NRG96.AlarmConfiguration()
            {
                Delay = (ushort)Configuracion.GetDouble("RETRASO_ALARMA", 0, ParamUnidad.SinUnidad),
                MaximumValue = (int)Configuracion.GetDouble("VALOR_MAXIMO_ALARMA", 0, ParamUnidad.SinUnidad),
                MinimumValue = (int)Configuracion.GetDouble("VALOR_MINIMO_ALARMA", 0, ParamUnidad.SinUnidad),
                VariableNumber = (byte)Configuracion.GetDouble("VARIABLE_ALARMA", 0, ParamUnidad.SinUnidad),
            };

            Nrg.WriteAlarmConfiguration(alarmConfiguration);
            NRG96.EnergyConsumptionInformation energyInformation = new NRG96.EnergyConsumptionInformation
            {
                Active = 0x01010101,
                Inductive = 0x12121212,
                Capacitive = 0x23232323
            };

            Nrg.WriteEnergyInformation(energyInformation);

            Delay(2000, "Asegurando grabado de energias");

            ResetPowersSource();

            Nrg.WriteFlagTest();

            var eepromErrors = Nrg.ReadEEPROMErrorCodes();

            Assert.AreEqual(eepromErrors, NRG96.EEPROMErrorCodes.NO_ERROR, Error().UUT.EEPROM.PARAMETROS_NO_GRABADOS(string.Format("Error en la eeprom del equipo, tras el reinicio se notifica del siguiente error", eepromErrors.GetDescription())));

            //var romCode = Nrg.ReadROMCode();

            //Assert.AreEqual("ESTADO_EEPROM", Identificacion.ROM_CODE, romCode, "Error en la memoria ROM del equipo, el codigo ROM no coincide con el de la BDD", ParamUnidad.SinUnidad);

            var energyReadings = Nrg.ReadEnergyInformation();

            Assert.AreEqual(energyInformation.Active, energyReadings.Active, Error().UUT.CONFIGURACION.SETUP("Error en la grabación de energias, la energia activa recuperada no coincide con la grabada"));
            Assert.AreEqual(energyInformation.Capacitive, energyReadings.Capacitive, Error().UUT.CONFIGURACION.SETUP("Error en la grabación de energias, la energia capacitiva recuperada no coincide con la grabada"));
            Assert.AreEqual(energyInformation.Inductive, energyReadings.Inductive, Error().UUT.CONFIGURACION.SETUP("Error en la grabación de energias, la energia inductiva recuperada no coincide con la grabada"));

            Nrg.WriteAllInformationClear();
        } 

        public void TestDisplay()
        {
            this.RunTestSequentiallyOrdered("TEST-DISPLAY", () =>
            {
                Nrg.WriteFlagTest();

                Nrg.SendDisplayInfo(NRG96.DisplayState.NO_SEGMENTS);

                Nrg.WriteBacklightsState(false, false);

                var dialogResult = Shell.ShowDialog(string.Format("TEST DE DISPLAY EQUIPO {0}", NumInstance), () => { return new ImageView("Se muestra el display como en la imagen patrón?", @"\\SFSERVER01\idp\Idp Público\IMAGESOURCE\NRG96_023\01\02301005.jpg") { Width = 540, Height = 600 }; }, MessageBoxButtons.YesNo, waitTime: 500);

                Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.CONFIGURACION.DISPLAY("Error de display al comprobar el apagado del backlight"));

                Nrg.WriteBacklightsState(true, true);

                Nrg.SendDisplayInfo(NRG96.DisplayState.HORIZONTAL_SEGMENTS);

                dialogResult = Shell.ShowDialog(string.Format("TEST DE DISPLAY EQUIPO {0}", NumInstance), () => { return new ImageView("Se muestra el display como en la imagen patrón?", @"\\SFSERVER01\idp\Idp Público\IMAGESOURCE\NRG96_023\01\02301002.jpg") { Width = 540, Height = 600 }; }, MessageBoxButtons.YesNo, waitTime: 500);

                Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.CONFIGURACION.DISPLAY("Error de display al comprobar el backlight o los segmentos horizontales"));

                Resultado.Set("BACKLIGHT", "OK", ParamUnidad.SinUnidad);

                Nrg.SendDisplayInfo(NRG96.DisplayState.VERTICAL_SEGMENTS);

                dialogResult = Shell.ShowDialog(string.Format("TEST DE DISPLAY EQUIPO {0}", NumInstance), () => { return new ImageView("Se muestra el display como en la imagen patrón?", @"\\SFSERVER01\idp\Idp Público\IMAGESOURCE\NRG96_023\01\02301003.jpg") { Width = 540, Height = 600 }; }, MessageBoxButtons.YesNo, waitTime: 500);

                Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.CONFIGURACION.DISPLAY("Error de display al comprobar los segmentos verticales"));

                Nrg.SendDisplayInfo(NRG96.DisplayState.SYMBOL_SEGMENTS);

                dialogResult = Shell.ShowDialog(string.Format("TEST DE DISPLAY EQUIPO {0}", NumInstance), () => { return new ImageView("Se muestra el display como en la imagen patrón?", @"\\SFSERVER01\idp\Idp Público\IMAGESOURCE\NRG96_023\01\02301004.jpg") { Width = 540, Height = 600 }; }, MessageBoxButtons.YesNo, waitTime: 500);

                Assert.AreEqual(dialogResult, DialogResult.Yes, Error().UUT.CONFIGURACION.DISPLAY("Error de display al comprobar los segmentos de los símbolos"));

                Resultado.Set("DISPLAY", "OK", ParamUnidad.SinUnidad);

                Nrg.SendDisplayInfo(NRG96.DisplayState.NO_SEGMENTS);
            });
        }

        public void TestKeyboard()
        {
            Nrg.WriteFlagTest();

            Dictionary<NRG96.KeyboardKeysRegisters, byte> imageDictionary = new Dictionary<NRG96.KeyboardKeysRegisters, byte>()
            {
                {NRG96.KeyboardKeysRegisters.RESET_KEY_MAX_KEY, OUT_TECLAS_RESET_MAX},
                {NRG96.KeyboardKeysRegisters.DISPLAY_KEY_MIN_KEY, OUT_TECLAS_SUB_BAJ },
            };

            this.RunTestSequentiallyOrdered("TEST_KEYBOARD", () =>
            {
                foreach (var item in imageDictionary)
                    SamplerWithCancel((p) =>
                    {
                        Tower.IO.DO.On(item.Value);
                        Delay(300, "");

                        var keyReading = (byte)Nrg.ReadKey();
                        var keyboardReading = (byte)keyReading == (byte)item.Key;

                        Resultado.Set(item.ToString(), keyboardReading ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                        if (!keyboardReading)
                            Error().UUT.HARDWARE.KEYBOARD(item.Key.ToString()).Throw();

                        Tower.IO.DO.Off(item.Value);
                        Delay(300, "");

                        keyReading = (byte)Nrg.ReadKey();
                        keyboardReading = (byte)keyReading == 0;

                        Resultado.Set(item.ToString(), keyboardReading ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                        if (!keyboardReading)
                            Error().UUT.HARDWARE.KEYBOARD(item.Key.ToString()).Throw();


                        return true;
                    }, "", exception: (t) => { Error().UUT.HARDWARE.KEYBOARD().Throw(); });
            });
        }

        public void TestRele()
        {
            Nrg.WriteFlagTest();

            SamplerWithCancel((p) =>
            {
                Nrg.WriteDigitalOutput(true);

                var estadoOut = Tower.IO.DI[NrgUtil.IN_EstadoSalida];

                Resultado.Set("DIGITAL_OUT_ON", estadoOut ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                if (!estadoOut)
                    Error().UUT.HARDWARE.RELE().Throw();

                Nrg.WriteDigitalOutput(false);
                Delay(300, "");

                estadoOut = !Tower.IO.DI[NrgUtil.IN_EstadoSalida];

                Resultado.Set("DIGITAL_OUT_OFF", estadoOut ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                if (!estadoOut)
                    Error().UUT.HARDWARE.RELE().Throw();

                return true;
            }, "");
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            //if (cvm.ReadHardwareVector().In_Medida)
            //    tower.IO.DO.On(OUTPUT_NEUTRO);

            var offsetsV = new TriAdjustValueDef(Params.V.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.V.Null.CrucePistas.Min(), Params.V.Null.CrucePistas.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.CrucePistas.TestPoint("OFFSETS").Name, Params.I.Null.CrucePistas.Min(), Params.I.Null.CrucePistas.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            WaitAllTestsAndRunOnlyOnce("MTE_TEST_OFF", () =>
            {
                tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
            });

            TestMeasureBase(OffsetsList,(Func<int, double[]>)((step) =>
                {
                    var vars = this.Nrg.ReadPhaseVariables();
                    return new double[] { vars.L1.Tension / 10D, vars.L2.Tension / 10D, vars.L3.Tension / 10D, 
                        vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D};

                }), delFirts, samples, timeInterval);

            var V_L1 = Consignas.GetDouble(Params.V.L1.CrucePistas.Name, 150, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.V.L2.CrucePistas.Name, 200, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.V.L3.CrucePistas.Name, 250, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 2, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 3, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            WaitAllTestsAndRunOnlyOnce("MTE_TEST_ON_SHORT_CIRCUIT", () =>
            {
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            });

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            //if (cvm.ReadHardwareVector().In_Medida)
            //    crucePistasI.Neutro = new AdjustValueDef(Params.I.LN.CrucePistas.Name, 0, 0, 0, ConsignasI.L1, Params.I.Null.CrucePistas.Tol, ParamUnidad.A);

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            TestMeasureBase(ShortCircuitList,(Func<int, double[]>)((step) =>
                {
                    var vars = this.Nrg.ReadPhaseVariables();

                    return new double[] { vars.L1.Tension / 10D, vars.L2.Tension/ 10D, vars.L3.Tension/ 10D, 
                        vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D};
                }), delFirts, samples, timeInterval);

            WaitAllTestsAndRunOnlyOnce("MTE_TEST_OFF_SHORT_CIRCUIT", () =>
            {
                tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
            });
        }

        public void TestAdjustVoltageCurrent(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var measureGains = Nrg.CalculateVoltageCurrentCalibrationFactors(delFirts, initCount, samples, timeInterval, realVoltageCurrent.Voltage, realVoltageCurrent.Current, transformationRelation,
                (value) =>
                {
                    triGains[0].L1.Value = value.VoltageL1;
                    triGains[0].L2.Value = value.VoltageL2;
                    triGains[0].L3.Value = value.VoltageL3;

                    triGains[1].L1.Value = value.CurrentL1;
                    triGains[1].L2.Value = value.CurrentL2;
                    triGains[1].L3.Value = value.CurrentL3;

                    return !HasError(triGains, false).Any();
                });

            triGains.ForEach(x => x.AddToResults(Resultado));

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            Nrg.WriteFlagTest();
            Nrg.WriteCalibrationFactors(measureGains.Item2);

            Delay(2000, "");
            Nrg.WriteRecoverCalibrationValuesFromEEPROM();
        }

        public void TestVerificationVoltageCurrent(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, 0, Params.V.Null.Verificacion.Tol(), ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, 0, Params.I.Null.Verificacion.Tol(), ParamUnidad.A));

            TestCalibracionBase(defs, (Func<double[]>)(() =>
                 {
                     var vars = this.Nrg.ReadPhaseVariables();
                     var varsList = new List<double>()
                             {
                                vars.L1.Tension / 10D, vars.L2.Tension/ 10D, vars.L3.Tension/ 10D,
                                vars.L1.Corriente / 1000D, vars.L2.Corriente / 1000D, vars.L3.Corriente / 1000D
                             };
                     return varsList.ToArray();
                 }),
                () =>
                {
                    lock (_syncObject)
                    {
                        var voltageRef = tower.PowerSourceIII.ReadVoltage();
                        var currentRef = tower.PowerSourceIII.ReadCurrent();

                        var varsList = new List<double>()
                            {
                                voltageRef.L1, voltageRef.L2, voltageRef.L3,
                                currentRef.L1 * transformationRelation, currentRef.L2 * transformationRelation, currentRef.L3 *transformationRelation
                            };
                        return varsList.ToArray();
                    }
                }, countSamples, maxSamples, timeInterval);
        }

        public void TestAdjustCompoundVoltages(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationVoltageCurrent();

            var triGains = new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var adjustVoltageValues = TriLineValue.Create(realVoltageCurrent.Voltage.L1, realVoltageCurrent.Voltage.L2, realVoltageCurrent.Voltage.L3);

            var measureGains = Nrg.CalculateCompoundMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltageValues, transformationRelation,
                (value) =>
                {
                    triGains.L1.Value = value.L1;
                    triGains.L2.Value = value.L2;
                    triGains.L3.Value = value.L3;

                    return !triGains.HasMinMaxError();
                });

            triGains.AddToResults(Resultado);

            if (!measureGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de tensiones compuestas fuera de margenes"));

            Nrg.WriteCompoundvoltageFactors(measureGains.Item2);
        }

        public void TestPhaseGapAdjust(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationPhaseGap();

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var power = TriLinePower.Create(realVoltageCurrent.Voltage, realVoltageCurrent.Current * transformationRelation, realVoltageCurrent.powerFactor);

            var offsetGains = Nrg.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, power.KW, realVoltageCurrent.powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                });

            triDefs.AddToResults(Resultado);

            if (!offsetGains.Item1)
                throw new Exception(string.Format("Error en el ajuste de desfase fuera de margenes"));

            Nrg.WriteFlagTest();
            Nrg.WriteITFCalibrationFactors(offsetGains.Item2);

            Delay(2000, "");
            Nrg.WriteRecoverCalibrationValuesFromEEPROM();
        }

        public void TestVerificationPhaseGap(int countSamples = 3, int maxSamples = 6, int timeInterval = 1100)
        {
            var realVoltageCurrent = internalTestAdjustVerificationPhaseGap();

            var toleranciaKw = Params.KW.Null.Verificacion.Tol();
            var defs = new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, 0, toleranciaKw, ParamUnidad.W);

            TestCalibracionBase(defs,(Func<double[]>)(() =>
                {
                    var vars = this.Nrg.ReadPhaseVariables();
                    var varsList = new List<double>() 
                            { 
                               vars.L1.PotenciaActiva, vars.L2.PotenciaActiva, vars.L3.PotenciaActiva,
                            };
                    return varsList.ToArray();
                }),
                () =>
                {
                    lock (_syncObject)
                    {
                        var voltageRef = tower.PowerSourceIII.ReadVoltage();
                        var currentRef = tower.PowerSourceIII.ReadCurrent();

                        var power = TriLinePower.Create(voltageRef, currentRef * transformationRelation, realVoltageCurrent.powerFactor);
                        return power.KW.ToArray();

                    }
                }, countSamples, maxSamples, timeInterval, "Error en la verificación del desfase fuera de margenes");
        }

        public void TestVerification()
        {
            //Comprobar tensiones de fase, compuestas, corrientes, medida de frecuencia, THD del equipo

            List<TriAdjustValueDef> thdMargins = new List<TriAdjustValueDef>();

            thdMargins.Add(new TriAdjustValueDef(Params.THD_V.L1.Verificacion.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));
            thdMargins.Add(new TriAdjustValueDef(Params.THD_I.L1.Verificacion.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.PorCentage));

            TestMeasureBase(thdMargins, (Func<int, double[]>)((step) =>
                {
                    var thdReadings = this.Nrg.ReadTHD();

                    return new double[] { thdReadings.VoltageL1, thdReadings.VoltageL2, thdReadings.VoltageL3, 
                                thdReadings.CurrentL1, thdReadings.CurrentL2, thdReadings.CurrentL3};
                }), 0, 10, 500);

            thdMargins.ForEach(x => x.AddToResults(Resultado));

            if (thdMargins.Any(x => x.HasMinMaxError()))
                throw new Exception("Error en la verificación del equipo, valores de THD fuera de márgenes");

            WaitAllTestsAndRunOnlyOnce("MTE_TEST_OFF_VERIFICATION", () =>
            {
                tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
            });

            Delay(3000, "Aseguramos apagado fuente MTE para evitar lecturas de energias en el equipo");

            Nrg.WriteAllInformationClear();
        }

        public void TestCustomize()
        {
            Nrg.WriteFlagTest();

            NRG96.SetupConfiguration setupConfiguration = new NRG96.SetupConfiguration
                ((int)Configuracion.GetDouble("TENSION_PRIMARIO_CLIENTE", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("TENSION_SECUNDARIO_CLIENTE", 1, ParamUnidad.SinUnidad),
                (ushort)Configuracion.GetDouble("CORRIENTE_PRIMARIO_CLIENTE", 5, ParamUnidad.SinUnidad),
                Configuracion.GetString("TIPO_TENSIONES_CLIENTE", "SIMPLE", ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.MainDefaultScreens)Configuracion.GetDouble("PANTALLA_PRINCIPAL_DEFECTO_CLIENTE", 0, ParamUnidad.SinUnidad),
                (NRG96.SetupConfiguration.EnergyDefaultScreens)Configuracion.GetDouble("PANTALLA_ENERGIA_DEFECTO_CLIENTE", 0, ParamUnidad.SinUnidad),
                Configuracion.GetString("CALCULO_HARMONICOS_SETUP_DEFECTO_CLIENTE", "THD", ParamUnidad.SinUnidad).ToUpper() != "THD",
                (byte)Configuracion.GetDouble("TIEMPO_DESCONEXION_BACKLIGHT", 10, ParamUnidad.SinUnidad));

            Nrg.WriteSetupConfiguration(setupConfiguration);

            Nrg.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));
        }

        public void TestFinish()
        {
            if (Nrg != null)
                Nrg.Dispose();

           this.RunOnlyLastTest("OFF_TOWER",
           () =>
           {
               if (Tower != null)
               {
                   try
                   {
                       Tower.ShutdownSources();
                   }
                   catch (Exception ex)
                   {
                       logger.WarnFormat("Excepcion Tower Shutdown in TestFinish by {0}", ex.Message);
                       Tower.Dispose();
                   }

                   if (Tower.IO != null)
                   {
                       Tower.IO.DO.OffWait(300, OUT_RETENER_EQUIPOS, OUT_PUNTAS_COMUNICACIONES, OUT_TECLAS_RESET_MAX, OUT_TECLAS_SUB_BAJ);
                       Tower.IO.DO.OffWait(300, OUT_PUNTAS_TEST_EQUIPO);
                       Tower.IO.DO.OnWait(500, OUT_LIBERAR_EQUIPOS, OUT_OK_EQ1);
                       Tower.IO.DO.OffWait(500, OUT_LIBERAR_EQUIPOS);
                       Tower.IO.DO.OnWait(500, OUT_RETENER_EQUIPOS);
                   }

                   Tower.Dispose();

                   if (NrgUtil != null)
                   {
                       if (IsTestError)
                           Tower.IO.DO.On(NrgUtil.OutErrorLed);
                   }else
                       Tower.IO.DO.On(48,49,50);
               }
           });
        }


        //*****************************************************************
        //*****************************************************************

        private struct VoltageCurrentReading
        {
            public TriLineValue Voltage { get; set; }
            public TriLineValue Current { get; set; }
            public double powerFactor { get; set; }
        };

        private VoltageCurrentReading internalTestAdjustVerificationVoltageCurrent()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L2 = Consignas.GetDouble(Params.I.L2.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L3 = Consignas.GetDouble(Params.I.L3.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA)
            };
            var consigAdjustCurrents = new TriLineValue()
            {
                L1 = adjustCurrent + adjustCurrentOffsets.L1,
                L2 = adjustCurrent + adjustCurrentOffsets.L2,
                L3 = adjustCurrent + adjustCurrentOffsets.L3
            };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 0, ParamUnidad.Grados);


            WaitAllTestsAndRunOnlyOnce("MTE_TEST_ON_AdjustVerificationVoltageCurrent", () =>
            {
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, consigAdjustCurrents, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            });

            var currentReadings = tower.PowerSourceIII.ReadCurrent();
            var voltageReadings = tower.PowerSourceIII.ReadVoltage();

            var realAdjustCurrents = new TriLineValue()
            {
                L1 = currentReadings.L1 - adjustCurrentOffsets.L1,
                L2 = currentReadings.L2 - adjustCurrentOffsets.L2,
                L3 = currentReadings.L3 - adjustCurrentOffsets.L3
            };

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(realAdjustCurrents.L1, realAdjustCurrents.L2, realAdjustCurrents.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        private VoltageCurrentReading internalTestAdjustVerificationPhaseGap()
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 5, ParamUnidad.A);
            var adjustCurrentOffsets = new TriLineValue()
            {
                L1 = Consignas.GetDouble(Params.I.L1.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L2 = Consignas.GetDouble(Params.I.L2.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA),
                L3 = Consignas.GetDouble(Params.I.L3.Ajuste.TestPoint("OFFSET").Name, 0, ParamUnidad.mA)
            };
            var consigAdjustCurrents = new TriLineValue()
            {
                L1 = adjustCurrent + adjustCurrentOffsets.L1,
                L2 = adjustCurrent + adjustCurrentOffsets.L2,
                L3 = adjustCurrent + adjustCurrentOffsets.L3
            };
            var powerFactor = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, 45, ParamUnidad.Grados);

            WaitAllTestsAndRunOnlyOnce("MTE_TEST_ON_AdjustVerificationPhaseGap", () =>
            {
                tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, consigAdjustCurrents, 50, powerFactor, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            });

            var currentReadings = tower.PowerSourceIII.ReadCurrent();
            var voltageReadings = tower.PowerSourceIII.ReadVoltage();

            var realAdjustCurrents = new TriLineValue()
            {
                L1 = currentReadings.L1 - adjustCurrentOffsets.L1,
                L2 = currentReadings.L2 - adjustCurrentOffsets.L2,
                L3 = currentReadings.L3 - adjustCurrentOffsets.L3
            };

            var adjustVoltageValues = TriLineValue.Create(voltageReadings.L1, voltageReadings.L2, voltageReadings.L3);
            var adjustCurrentValues = TriLineValue.Create(realAdjustCurrents.L1, realAdjustCurrents.L2, realAdjustCurrents.L3);

            return new VoltageCurrentReading() { Voltage = adjustVoltageValues, Current = adjustCurrentValues, powerFactor = powerFactor };
        }

        //*****************************************************************
        //*****************************************************************

        private void ResetPowersSource()
        {
            this.WaitAllTestsAndRunOnlyOnce("TEST_RESET", () =>
            {
                tipoAlimentacionDC = VectorHardware.GetString("ALIMENTACION", "230Vac", ParamUnidad.SinUnidad).Contains("Vdc");
                if (tipoAlimentacionDC)
                    Tower.LAMBDA.ApplyOff();
                else
                    Tower.Chroma.ApplyOff();

                Delay(5000, "");

                if (tipoAlimentacionDC)
                    Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(VoltageSupply, 1);
                else
                    Tower.Chroma.ApplyPresetsAndWaitStabilisation(VoltageSupply, 1, 50);

            });

            Delay(5000, "");
        }

        private DeviceInstance GetVariablesByInstance(int portNrgEq1 = 0/*, int portNrgEq2 = 0, int portNrgEq3 = 0*/)
        {
            var nrgTest = new DeviceInstance();

            if (NumInstance == 1)
            {
                if (portNrgEq1 == 0)
                    portNrgEq1 = Comunicaciones.SerialPort;

                nrgTest.Port_TTL = portNrgEq1;
                nrgTest.Port_RS485 = Comunicaciones.SerialPort2;
                nrgTest.Periferic = Comunicaciones.Periferico;
                nrgTest.IN_DeteccionInicio = 24;
                nrgTest.IN_DeteccionFinal = 25;
                nrgTest.IN_PistonesLarales = 30;
                nrgTest.IN_EstadoSalida = 9;
                nrgTest.OUT_LiberarEquipo = 6;
                nrgTest.OutErrorLed = 48;
            }
            //else if (NumInstance == 2)
            //{
            //    if (portNrgEq2 == 0)
            //        portNrgEq2 = Comunicaciones.SerialPort2;

            //    nrgTest.Port_TTL = portNrgEq2;
            //    nrgTest.Port_RS485 = Comunicaciones.SerialPort2;
            //    nrgTest.Periferic = Comunicaciones.Periferico;
            //    nrgTest.IN_DeteccionInicio = 26;
            //    nrgTest.IN_DeteccionFinal = 27;
            //    nrgTest.IN_PistonesLarales = 31;
            //    nrgTest.IN_EstadoSalida = 10;
            //    nrgTest.OUT_LiberarEquipo = 7;
            //    nrgTest.OutErrorLed = 49;
            //}
            //else if (NumInstance == 3)
            //{
            //    if (portNrgEq3 == 0)
            //        portNrgEq3 = Comunicaciones.SerialPort3;

            //    nrgTest.Port_TTL = portNrgEq3;
            //    nrgTest.Port_RS485 = Comunicaciones.SerialPort3;
            //    nrgTest.Periferic = Comunicaciones.Periferico;
            //    nrgTest.IN_DeteccionInicio = 28;
            //    nrgTest.IN_DeteccionFinal = 29;
            //    nrgTest.IN_PistonesLarales = 32;
            //    nrgTest.IN_EstadoSalida = 11;
            //    nrgTest.OUT_LiberarEquipo = 8;
            //    nrgTest.OutErrorLed = 50;
            //}

            return nrgTest;
        }

        public class DeviceInstance
        {
            public int Port_TTL { get; set; }
            public byte Periferic { get; set; }
            public int Port_RS485 { get; set; }
            public byte IN_DeteccionInicio { get; set; }
            public byte IN_DeteccionFinal { get; set; }
            public byte IN_PistonesLarales { get; set; }
            public byte IN_EstadoSalida { get; set; }
            public byte OUT_LiberarEquipo { get; set; }
            public byte OutErrorLed { get; set; }
        }
    }
}

