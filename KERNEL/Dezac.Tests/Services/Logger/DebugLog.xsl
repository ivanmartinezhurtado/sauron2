<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="IdEmpleado"></xsl:param>
  <xsl:param name="NumProducto"></xsl:param>
  <xsl:param name="Version"></xsl:param>
  <xsl:param name="DescripcionProducto"></xsl:param>
  <xsl:param name="IdFase"></xsl:param>
  <xsl:param name="PC"></xsl:param>
  <xsl:param name="NumSerie"></xsl:param>
  <xsl:param name="NumBastidor"></xsl:param>
  <xsl:param name="Fecha"></xsl:param>
  <xsl:param name="VersionSauron"></xsl:param>
  <xsl:param name="InterfazSauron"></xsl:param>

  <xsl:template match="/">
        <html>
            <head>
              <style>
                body
                {
                font-family: monospace;
                font-size: 11pt;
                font-weight: normal;
                }

                .step
                {
                padding: 5px;
                border: 1px solid #afafaf;
                border-radius: 15px;
                }

                .step-notes
                {
                background-color: #f9ff9f;
                border: 2px solid black;
                border-radius: 10px;
                }

                .step-description
                {
                background-color: #dedede;
                border: 2px solid black;
                border-radius: 10px;
                }

                .step-title
                {
                width: 100%;
                border: 5px solid lightgray;
                text-align: left;
                outline: none;
                font-size: 20px;
                font-weight: bold;
                border: 2px solid #a2a2a2;
                color: black;
                border-radius: 10px;
                padding: 1px;
                background-color: #b5dce8;
                font-family: monospace;
                }

                .step-title2
                {
                width: 100%;
                border: 5px solid lightgray;
                text-align: left;
                outline: none;
                font-size: 20px;
                font-weight: bold;
                border: 2px solid #a2a2a2;
                color: black;
                border-radius: 10px;
                padding: 1px;
                background-color: #b5dce8;
                font-family: monospace;
                }

                .step-content
                {
                border-color:black;
                border: 1px solid transparent;
                padding-left: 25px;
                }

                .step-1
                {
                background-color: #a2a2a2;
                border: 3px solid #2b2b2b;
                box-sizing: border-box;
                padding: 5;
                font-size: 22px;
                }

                .msg
                {
                padding: 0px;
                color: Darkblue;
                }

                .test-uut
                {
                color: Darkblue;
                }

                .results
                {
                color: DarkGreen;
                font-weight: bold;
                }

                .level-WARN
                {
                color: DarkOrange;
                }

                .level-ERROR
                {
                color: red;
                }

                .instruments
                {
                color: #424242;
                }

                .logger-Instruments-IO-PCI7296
                {
                color: SaddleBrown;
                }

                .logger-UUT
                {
                color: Black;
                }

                table
                {
                width: 100%;
                text-align: left;
                border-radius: 15px;
                padding: 10px;
                font-size: 15pt;
                }

                .title
                {
                background-color: #3a3633;
                font-size: 30px;
                font-weight: bold;
                color: white;
                text-align: center;
                border-radius: 15px;
                padding: 10;
                }

                button
                {
                background-color: #a2a2a2;
                border: 3px solid #2b2b2b;
                box-sizing: border-box;
                padding-left: 6pt;
                padding-right: 6pt;
                font-size: 20px;
                border-radius: 10px;
                font-family: monospace;
                }

                button.accordion
                {
                cursor: pointer;
                transition: 0.4s;
                }

                button.accordion.active:hover
                {
                background-color: #93bbe2;
                }

                button.accordion:hover
                {
                background-color: #93bbe2;
                }

                button.accordion:after
                {
                content: '\002B';
                color: #2b2b2b;
                float: left;
                margin-right: 5px;
                margin-left: 5px
                }

                button.accordion.active:after
                {
                content: "\2212";
                }

                button.accordion-ok
                {
                background-color: #94c18c;
                }
                button.accordion-ok.active:hover
                {
                background-color: #4fd853;
                }
                button.accordion-ok:hover
                {
                background-color: #4fd853;
                }

                button.accordion-fail
                {
                background-color: #ee5f5f;
                }
                button.accordion-fail.active:hover
                {
                background-color: #e93a3a;
                }
                button.accordion-fail:hover
                {
                background-color: #e93a3a;
                }

                button.accordion-aborted
                {
                background-color: #c97474;
                }
                button.accordion-aborted.active:hover
                {
                background-color: #ac4848;
                }
                button.accordion-aborted:hover
                {
                background-color: #ac4848;
                }

                button.accordion-repeated
                {
                background-color: #f6d484;
                }
                button.accordion-repeated.active:hover
                {
                background-color: #f7c23b;
                }
                button.accordion-repeated:hover
                {
                background-color: #f7c23b;
                }

                button.accordion-next
                {
                background-color: #f5a97a;
                }
                button.accordion-next.active:hover
                {
                background-color: #f7803b;
                }
                button.accordion-next:hover
                {
                background-color: #f7803b;
                }

                button.accordion-omitted
                {
                background-color: #c2c2c2;
                }
                button.accordion-omitted.active:hover
                {
                background-color: #a3a3a3;
                }
                button.accordion-omitted:hover
                {
                background-color: #a3a3a3;
                }

                button.accordion-disabled
                {
                background-color: #E1E1E1;
                }
                button.accordion-disabled.active:hover
                {
                background-color: #D8D8D8;
                }
                button.accordion-disabled:hover
                {
                background-color: #D8D8D8;
                }

                button.accordion-description
                {
                background-color: #dedede;
                color: black;
                font-style: italic;
                border:0px;
                font-size:13pt;
                }
                button.accordion-description:hover
                {
                background-color: #bfbfbf;
                }
                button.accordion-notes
                {
                background-color: #f9ff9f;
                color: black;
                font-style: italic;
                border:0px;
                font-size:13pt;
                }
                button.accordion-notes:hover
                {
                background-color: #fbff7a;
                }

                .panel
                {
                background-color: white;
                overflow: hidden;
                transition: max-height 0.2s ease-out;
                display: block;
                }

                .panel-notes
                {
                background-color: #f9ff9f;
                border-radius: 10px;
                }

                .panel-description
                {
                background-color: #dedede;
                border-radius: 10px;
                }

                .hidden
                {
                display: none;
                }

              </style>
            </head>
          <body>
         
            <div class="title ">
              TEST LOG
            </div>

            <xsl:element name="button">
              <xsl:attribute name="class">
                btnCollapse 
              </xsl:attribute>
              Collapse
            </xsl:element>

            <xsl:element name="button">
              <xsl:attribute name="class">
                btnExpand
              </xsl:attribute>
              Expand
            </xsl:element>

            <table>
              <tr>
                <td>
                  <b>Producto: </b>
                  <xsl:value-of select="$NumProducto"/>/<xsl:value-of select="$Version"/> - <xsl:value-of select="$DescripcionProducto"/>
                </td>
                <td>
                  <b>Version Software: </b>
                  <xsl:value-of select="$InterfazSauron"/> v<xsl:value-of select="$VersionSauron"/>
                </td>
              </tr>
               <tr>
                 <td>
                  <b>Fecha: </b>
                  <xsl:value-of select="$Fecha"/>
                 </td>
                 <td>
                  <b>Bastidor: </b>
                  <xsl:value-of select="$NumBastidor"/>
                </td>
                <td>
                  <b>Nº Serie: </b>
                  <xsl:value-of select="$NumSerie"/>
                </td>

              </tr>
            </table>
            <xsl:apply-templates/>
          </body>
                          
          <script type="text/javascript" >
            <xsl:text disable-output-escaping="yes">
              <![CDATA[
                var acc = document.getElementsByClassName("accordion");
                var i;

                for (i = 0; i < acc.length; i++) 
                {
                  acc[i].onclick = function() 
                  {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    panel.classList.toggle("hidden");
                  }
                }
              ]]>
            </xsl:text>
          </script>

          <script type="text/javascript" >
            <xsl:text disable-output-escaping="yes">
              <![CDATA[
                var acc = document.getElementsByClassName("accordion");
                var coll = document.getElementsByClassName("btnCollapse");
                var j;
                var i;                

                for (j = 0; j < coll.length ; j++)
                {
                  coll[j].onclick = function()
                  {
                    for (i = 0; i < acc.length; i++) 
                    {
                      acc[i].classList.add("active");
                      var panel =  acc[i].nextElementSibling;
                      panel.classList.add("hidden");
                    }
                  } 
                }

              ]]>
            </xsl:text>
          </script>

          <script type="text/javascript" >
            <xsl:text disable-output-escaping="yes">
              <![CDATA[
                var acc = document.getElementsByClassName("accordion");
                var exp = document.getElementsByClassName("btnExpand");
                var j;
                var i;                

                for (j = 0; j < coll.length ; j++)
                {
                  exp[j].onclick = function()
                  {
                    for (i = 0; i < acc.length; i++) 
                    {
                      acc[i].classList.remove("active");
                      var panel =  acc[i].nextElementSibling;
                      panel.classList.remove("hidden");
                    }
                  } 
                }

              ]]>
            </xsl:text>
          </script>
         
        </html>
      </xsl:template>

      <xsl:template match="EventInfo[@Type='msg' and @LoggerName='PARAMS_UUT']">
          <div class ="table_title">
            Test Results
            <table>
              <xsl:element name="div">
                <xsl:attribute name="class">
                  result
                </xsl:attribute>
                <tr>
                  <td>
                    <xsl:value-of select="@Text"/>
                    <xsl:apply-templates select="Events"/>
                  </td>
                </tr>
              </xsl:element>
            </table>
          </div>
        </xsl:template>

      <!--Todos los steps, se ponen como clase 'step', 'step-title', 'step-@nombre'-->
      <xsl:template match="EventInfo[@Type='step']">
        <div class="step">
            <xsl:element name="button">
              <xsl:attribute name="class">
                  accordion step-title 
                  <xsl:if test="@Result = 'Passed'">
                    accordion-ok
                  </xsl:if>
                  <xsl:if test="@Result = 'Failed'">
                    accordion-fail
                  </xsl:if>
                  <xsl:if test="@Result = 'Aborted'">
                    accordion-aborted
                  </xsl:if>
                  <xsl:if test="@Result = 'Continued'">
                    accordion-next
                  </xsl:if>
                  <xsl:if test="@Result = 'Repeated'">
                    accordion-repeated
                  </xsl:if>
                  <xsl:if test="@Result = 'Omitted'">
                    accordion-omitted
                  </xsl:if>
                  <xsl:if test="@Result = 'Disabled'">
                    accordion-disabled
                  </xsl:if>
              </xsl:attribute>
              <xsl:choose>
                <xsl:when test="@Result = 'Failed'">
                  <xsl:value-of select="@Text"/> - (<xsl:value-of select="@ExecutionTime"/>) ms - (Failed)
                </xsl:when>
                <xsl:when test="@Result = 'Aborted'">
                  <xsl:value-of select="@Text"/> - (<xsl:value-of select="@ExecutionTime"/>) ms - (Aborted)
                </xsl:when>
                <xsl:when test="@Result = 'Continued'">
                  <xsl:value-of select="@Text"/> - (<xsl:value-of select="@ExecutionTime"/>) ms - (Continued)
                </xsl:when>
                <xsl:when test="@Result = 'Repeated'">
                  <xsl:value-of select="@Text"/> - (<xsl:value-of select="@ExecutionTime"/>) ms - (Repeated)
                </xsl:when>
                <xsl:when test="@Result = 'Omitted'">
                  <xsl:value-of select="@Text"/> - (Omitted)
                </xsl:when>
                <xsl:when test="@Result = 'Disabled'">
                  <xsl:value-of select="@Text"/> - (Disabled)
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="@Text"/> - (<xsl:value-of select="@ExecutionTime"/>) ms
                </xsl:otherwise>
              </xsl:choose>       
            </xsl:element>
            <div class="step-content panel hidden">
                <xsl:apply-templates select="Events"/>
            </div>
        </div>
    </xsl:template>
   
    <!--Panel de notas-->
    <xsl:template match="EventInfo[@Type='msg' and @Level='TRACE']">
      <div class="step-notes">
          <xsl:element name="button">
              <xsl:attribute name="class">
                accordion step-title2 accordion-notes
              </xsl:attribute>          
                Notas:
          </xsl:element>
        <div class="step-content panel panel-notes hidden">
          <xsl:value-of select="@Text"/>
        </div>
      </div>
    </xsl:template>
  
    <!--Panel de descripción-->
    <xsl:template match="EventInfo[@Type='msg' and @Level='VERBOSE']">
       <div class="step-description">
          <xsl:element name="button">
            <xsl:attribute name="class">
                accordion step-title2 accordion-description
            </xsl:attribute>       
                Descripción: 
          </xsl:element>
          <div class="step-content panel panel-description hidden">
            <xsl:value-of select="@Text"/>
          </div>
      </div>
    </xsl:template>
  
        <!--los steps init main y end se ponen como step-1-->
      <xsl:template match="EventInfo[@Type='step' and (@Id='Main' or @Id='Init' or @Id='End')]">
        <div class="step">
          <xsl:element name="div">
              <xsl:attribute name="class">
                step-1 step-title
              </xsl:attribute>
                <xsl:value-of select="@Text"/>
            </xsl:element>
            <div class="step-content">
                <xsl:apply-templates select="Events"/>
            </div>
        </div>
    </xsl:template>

    <!--Todos msg'-->
    <xsl:template match="EventInfo[@Type='msg' and not(@Level='VERBOSE' or @Level='TRACE')]">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/>
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> - <xsl:value-of select="@LoggerName"/> - <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <!--msg con LoggerName TEST_UUT se añaden a la clase test-uut-->
    <xsl:template match="EventInfo[@Type='msg' and starts-with(@LoggerName, 'TEST_UUT')]">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/> test-uut
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="@LoggerName" />
                <xsl:with-param name="replace" select="'TEST_UUT'" />
                <xsl:with-param name="by" select="''" />
            </xsl:call-template>            
            <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>
 
    <!--msg con LoggerName PARAMS_UUT se añaden a la clase results-->
    <xsl:template match="EventInfo[@Type='msg' and @LoggerName='PARAMS_UUT']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/> results
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -  <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>
  
    <!--msg que sean instruments se añaden a la clase instruments-->
    <xsl:template match="EventInfo[@Type='msg' and starts-with(@LoggerName, 'Instruments')]">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/> instruments
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> - <xsl:value-of select="@LoggerName"/> - <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <!--? modificar el texto de taskrunner, para que no salga 'TaskRunner-SequenceRunner' sino 'TaskRunner' -->
    <xsl:template match="EventInfo[@Type='msg' and @LoggerName='TaskRunner-SequenceRunner']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/>
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> - TaskRunner - <xsl:value-of select="@Text"/>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <!--Para que los mensajes de soap se vean con la estructura original -->
    <xsl:template match="EventInfo[@LoggerName='Dezac-Device-Metering-Web_References-TransportSoap']">
        <xsl:element name="div">
            <xsl:attribute name="class">
                msg level-<xsl:value-of select="@Level"/> logger-<xsl:value-of select="@LoggerName"/>
            </xsl:attribute>
            <xsl:value-of select="@TimeStamp"/> -
            <pre>
                <xsl:value-of select="@Text"/>
            </pre>
            <xsl:apply-templates select="Events"/>
        </xsl:element>
    </xsl:template>

    <!--Funcion para hacer un string replace-->  
    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


</xsl:stylesheet>
