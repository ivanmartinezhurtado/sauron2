﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Instruments.Auxiliars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.InstrumentsTools
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(ExecuteTestDielectricStrengthActionDescription))]


    public class ExecuteTestDielectricStrength : ActionBase
    {
        [DefaultValue((byte)2)]
        public byte Port { get; set; }

        [DefaultValue(DielectricStrengthTester.TestResult.PASS)]
        public DielectricStrengthTester.TestResult ExpectedResult { get; set; }

        [DefaultValue(0)]
        public int TimeOut { get; set; }

        [DefaultValue(null)]
        public string resultName { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();;

            var context = SequenceContext.Current;
               if (context == null)
                   throw new Exception("Error. No se ha inicializado el contexto");

            bool paralelTest = context.RunningInstances > 1;

            try
            {
                var wmi = new WMISerialPortInitialize();
                wmi.Execute("MoxaPort", "MOXA USB Serial Port");
                var moxa = GetVariable<string>("MoxaPort");
                Port = Convert.ToByte(moxa.Replace("COM", ""));
            }
            catch (Exception)
            {
                Port = Port == 0 ? testBase.Comunicaciones.SerialPort : Port;
            }
                        
            using (var tester = new DielectricStrengthTester(Port))
            {
                tester.SetRemoteMode();
                tester.StopTest();
                tester.StartTest();

                testBase.SamplerWithCancel(
                    (p) =>
                    {
                        var state = tester.ReadTestState();
                        return state != DielectricStrengthTester.TestStatus.RUNNING;

                    }, "Error. El equipo lleva mas tiempo de test del esperado", 60, TimeOut * 10);

                var numberOfSteps = tester.ReadAmountOfSteps();
                if (numberOfSteps > 0)
                    for (int actualStep = 1; actualStep < numberOfSteps + 1; actualStep++)
                    {                      
                        var readingCurrent = tester.ReadTestCurrentResultOfStep(actualStep) * 1000;
                        var readingVoltage = tester.ReadTestVoltageResultOfStep(actualStep);

                        if (resultName != null)
                        {
                            if(paralelTest)
                                context.SharedVariables.AddOrUpdate(resultName + testBase.Params.V.Null.TestPoint("STEP_" + actualStep).Name, readingVoltage);
                            else
                                testBase.Resultado.Set(resultName + testBase.Params.V.Null.TestPoint("STEP_" + actualStep).Name, readingVoltage, ParamUnidad.V);
                           

                            if (tester.ReadtestStepMode(actualStep) != "IR")
                            {
                                var MaximumCurrent = testBase.Margenes.GetDouble(testBase.Params.I.Null.TestPoint("MAX_STEP_" + actualStep).Name, 0.00006, ParamUnidad.mA);
                                var MinimumCurrent = testBase.Margenes.GetDouble(testBase.Params.I.Null.TestPoint("MIN_STEP_" + actualStep).Name, 0.00006, ParamUnidad.mA);
                                if (paralelTest)
                                {
                                    context.SharedVariables.AddOrUpdate(resultName + testBase.Params.I.Null.TestPoint("STEP_" + actualStep).Name, readingCurrent);
                                    context.SharedVariables.AddOrUpdate("CURRENT_MAX", MaximumCurrent);
                                    context.SharedVariables.AddOrUpdate("CURRENT_MIN", MinimumCurrent);
                                }
                                else
                                    testBase.Resultado.Set(resultName + testBase.Params.I.Null.TestPoint("STEP_" + actualStep).Name, readingCurrent, ParamUnidad.mA, MinimumCurrent, MaximumCurrent);                           
                            }
                            else
                            {
                                var MaximumResistance = testBase.Margenes.GetDouble(testBase.Params.RES.Null.TestPoint("STEP_" + actualStep + "_MAX").Name, 1000000000, ParamUnidad.Ohms);
                                var MinimumResistance = testBase.Margenes.GetDouble(testBase.Params.RES.Null.TestPoint("STEP_" + actualStep + "_MIN").Name, 1000000, ParamUnidad.Ohms);
                                if (MaximumResistance != 0)
                                {
                                    if (paralelTest)
                                    {
                                        context.SharedVariables.AddOrUpdate(resultName + testBase.Params.RES.Null.TestPoint("STEP_" + actualStep).Name, readingCurrent / 1000);
                                        context.SharedVariables.AddOrUpdate("RESISTANCE_MAX", MaximumResistance);
                                        context.SharedVariables.AddOrUpdate("RESISTANCE_MIN", MinimumResistance);
                                    }
                                    else
                                        testBase.Resultado.Set(resultName + testBase.Params.RES.Null.TestPoint("STEP_" + actualStep).Name, readingCurrent / 1000, ParamUnidad.Ohms, MinimumResistance, MaximumResistance);                     
                                }
                                else
                                {
                                    if(paralelTest)
                                        context.SharedVariables.AddOrUpdate(resultName + testBase.Params.RES.Null.TestPoint("STEP_" + actualStep).Name, (readingCurrent / 1000).ToString());
                                    else
                                        testBase.Resultado.Set(resultName + testBase.Params.RES.Null.TestPoint("STEP_" + actualStep).Name, (readingCurrent / 1000).ToString(), ParamUnidad.Ohms, "9.9E+37");                            
                                }
                            }
                        }                           
                    }

                tester.StopTest();
                 var result = tester.ReadTestResult();

                if (resultName != null)
                {
                    if(paralelTest)
                        context.SharedVariables.AddOrUpdate(resultName.Replace("_", ""), result.ToString());
                    else
                        testBase.Resultado.Set(resultName.Replace("_", ""), result.ToString(), ParamUnidad.SinUnidad);
                }

                Assert.AreEqual(result, ExpectedResult, Error().PROCESO.PRODUCTO.FASES_TEST_INCORRECTAS("El equipo no ha superado el test, RESULTADO : " + result.ToString().Replace("_"," ")));

            }
        }
    }

    public class ExecuteTestDielectricStrengthActionDescription : ActionDescription
    {
        public override string Name { get { return "ExecuteTestDielectricStrength"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Flash_Updater_icon; } }
    }
}
