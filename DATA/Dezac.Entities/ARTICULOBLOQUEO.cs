namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ARTICULOBLOQUEO
    {
        public int NUMARTICULO { get; set; }
        public System.DateTime FECHA { get; set; }
        public string PROCESO { get; set; }
        public string USERNAME { get; set; }
        public string MOTIVO { get; set; }
        public string IDMOTIVO { get; set; }
        public Nullable<int> VERSIONPRODUCTO { get; set; }
    
        public virtual ARTICULO ARTICULO { get; set; }
    }
}
