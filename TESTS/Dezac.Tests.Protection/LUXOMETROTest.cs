﻿using Dezac.Device.Protection;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.03)]
    public class LUXOMETROTest : TestBase
    {
        private Tower3 tower;
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }
        private LUXOMETRO luxometro;
        private LUXOMETRO luxometroPatron;
        private LUXOMETRO luxometroPatronEstatico;
        //private Lambda lambda;
        //private MTX3293 Metrix3293;
        private LUXOMETRO.GPSInfo infoGPSPatron;
        private double valorCurrent;
        private double valorLuxPatron;
        private int timeEstabilization = 5000;

        private const byte UTIL1 = 17;
        private const byte UTIL2 = 18;
        private const byte ALIMENTACION_LX485 = 19;
        private const byte MEDIDA_LX485 = 43;


        private ICacheService cacheSvc;

        public LUXOMETRO.EscalaLux Escala
        {
            get
            {
                if (Model.IdFase == "120")
                    return LUXOMETRO.EscalaLux.Escala200;

                if (Model.IdFase == "121")
                    return LUXOMETRO.EscalaLux.Escala2000;

                if (Model.IdFase == "122")
                    return LUXOMETRO.EscalaLux.Escala8000;

                if (Model.IdFase == "100")
                    return LUXOMETRO.EscalaLux.Ajuste;

                throw new Exception("Erro no se esta realizando el test en una fase incorrecta");
            }
        }

        public ushort ValorGainCalculate { get; set; }

        public LUXOMETRO.GPSInfo InfoGPSPatron
        {
            get
            {
                cacheSvc.TryGet("InfoGPSPatron", out infoGPSPatron);
                return infoGPSPatron;
            }
            set
            {
                infoGPSPatron = value;

                if (cacheSvc != null)
                    cacheSvc.Add("InfoGPSPatron", infoGPSPatron);
            }
        }

        public double ValorLuxPatron
        {
            get
            {
                cacheSvc.TryGet("ValorLuxPatron", out valorLuxPatron);
                return valorLuxPatron;
            }
            set
            {
                valorLuxPatron = value;

                if (cacheSvc != null)
                    cacheSvc.Add("ValorLuxPatron", valorLuxPatron);
            }
        }
 
        public double ValorCurrent
        {
            get
            {
                var res = cacheSvc.TryGet("ValorCurrent", out valorCurrent);
                return valorCurrent;
            }
            set
            {
                valorCurrent = value;
                if (cacheSvc != null)
                    cacheSvc.Add("ValorCurrent", valorCurrent);
            }
        }

        public LUXOMETRO LuxometroPatron
        {
            get
            {
                if(luxometroPatron == null)
                    luxometroPatron = new LUXOMETRO();
                
                return luxometroPatron;
            }
        }

        public LUXOMETRO LuxometroPatronEstatico
        {
            get
            {
                if (luxometroPatronEstatico == null)
                    luxometroPatronEstatico = new LUXOMETRO(Comunicaciones.SerialPort, 2);

                return luxometroPatronEstatico;
            }
        }

        public void TestInitialization(int port = 0, int puerto485= 0)
        {
            cacheSvc = GetService<ICacheService>();

            if (port == 0)
                port = Comunicaciones.SerialPort;

            if (puerto485 == 0)
                puerto485 = Comunicaciones.SerialPort2;

            Tower.Active24VDC();

            luxometro = new LUXOMETRO();

            if (Configuracion.GetString("IS_GPS", "SI", ParamUnidad.SinUnidad) == "NO")
            {              
                Tower.IO.DO.On(ALIMENTACION_LX485);
                Tower.IO.DO.On(MEDIDA_LX485);
                luxometro.SetPort(puerto485, 1);
            }

            if (Model.IdFase != "100")
            Tower.LAMBDA.Initialize();
            //if (puertoLambda == 0)
            //    puertoLambda = Comunicaciones.SerialPortDevice;

            //lambda = new Lambda();
            //lambda.PortCom = (byte)puertoLambda;
            //lambda.Initialize();
        }

        public void TestCalibracionPatron(string nameDevicePortVirtual)
        {
            var portPatron = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            LuxometroPatron.SetPort(portPatron, 1);

            SamplerWithCancel((p) =>
            {
                var numSerie = LuxometroPatron.ReadSerialNumber();
                Assert.AreEqual(numSerie, (uint)Configuracion.GetDouble("NUMSERIE_LUXOMETRO_PATRON", 222222222), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el numero de serie leido no corresponde con la sonda patron"));

                var firmwareVersion = LuxometroPatron.ReadFirmwareVersion();
                Assert.AreEqual(firmwareVersion, Convert.ToUInt16(Identificacion.VERSION_FIRMWARE_2), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error Version de Firmware incorrecta de la sonda patron"));
                return true;
            }, "");

            AjusteLuxometroPatronByPowerSource();

            LuxometroPatron.Modbus.ClosePort();
        }

        public void TestCalibracionPatronEstatico()
        {
            if (Configuracion.GetString("IS_GPS", "SI", ParamUnidad.SinUnidad) == "NO")
            {
                luxometro.Modbus.PerifericNumber = 2;
                luxometroPatronEstatico = luxometro;
            }

            SamplerWithCancel((p) =>
            {
                var numSerie = LuxometroPatronEstatico.ReadSerialNumber();
                Assert.AreEqual(numSerie, (uint)Configuracion.GetDouble("NUMSERIE_LUXOMETRO_PATRON_ESTATICO", 2863315899), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el numero de serie leido no corresponde con la sonda patron"));

                var firmwareVersion = LuxometroPatronEstatico.ReadFirmwareVersion();
                Assert.AreEqual(firmwareVersion, Convert.ToUInt16(Identificacion.VERSION_FIRMWARE_2), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error Version de Firmware incorrecta de la sonda patron estatico"));
                return true;
            }, "");

            TestPatronEstaticoByEscala();
        }

        public void TestComunicationUSB(string nameDevicePortVirtual)
        {
            var portPatron = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            luxometro.SetPort(portPatron, 1);
            TestComunication();
        }

        public void TestComunication()
        {
            luxometro.Modbus.PerifericNumber = 1;

            SamplerWithCancel((p) =>
            {
                var firmwareVersion = luxometro.ReadFirmwareVersion();
                Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString(), ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);
                Assert.IsTrue(firmwareVersion == Convert.ToUInt16(Identificacion.VERSION_FIRMWARE), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error Version de Firmware incorrecta"));
                return true;
            }, "", 4, 500, 500, false, false);
        }

        public void TestConsumo(string nameDevicePortVirtual)
        {
            //var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            //Metrix3293 = new MTX3293();
            //Metrix3293.PortCom = portMetrix;

            var consumo = new AdjustValueDef { Name = Params.I_DC.Null.Ajuste.TestPoint(Escala.ToString()).Name, Min = Params.I_DC.Null.Ajuste.TestPoint(Escala.ToString()).Min(), Max = Params.I_DC.Null.Ajuste.TestPoint(Escala.ToString()).Max(), Unidad = ParamUnidad.mA };

            TestMeasureBase(consumo, (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.IN8, MagnitudsMultimeter.AmpDC);
            }, 1, 4, 500);
        }

        public void TestAjuste()
        {
            var adjust = new AdjustValueDef { Name = Params.GAIN_FACTOR.Null.Ajuste.TestPoint(Escala.ToString()).Name, Min = Params.GAIN_FACTOR.Null.Ajuste.TestPoint(Escala.ToString()).Min(), Max = Params.GAIN_FACTOR.Null.Ajuste.TestPoint(Escala.ToString()).Max(), Unidad = ParamUnidad.mA };

            luxometro.Modbus.PerifericNumber = 1;

            var result = luxometro.AdjustFactors(Escala, 1, 5, 10, 500, ValorLuxPatron,
            (p) =>
            {
                adjust.Value = p;
                return adjust.IsValid();
            });

            adjust.AddToResults(Resultado);

            if (!result.Item1)
                throw new Exception("Error ajuste fuera de margenes");

            ValorGainCalculate = (ushort)result.Item2;
            luxometro.WriteGainFactor(Escala, ValorGainCalculate);
            Delay(2000, "");
            luxometro.WriteValidationChange();
            Delay(2000, "");

            if (Model.IdFase == "122")
            {
                if (TestInfo.NumSerie == null)
                {
                    using (DezacService svc = new DezacService())
                    {
                        if (Identificacion.MODELO == "RECALIBRACION")
                            TestInfo.NumSerie = svc.GetNumSerieByBeforeTest((int)TestInfo.NumBastidor.Value);
                        else
                        {
                            if (!Model.NumOrden.HasValue && Model.NumProducto != 0)
                                TestInfo.NumSerie = svc.GetNumSerie(Model.NumProducto, Model.Version, 0);
                            else
                                TestInfo.NumSerie = svc.GetNumSerie(Model.NumProducto, Model.Version, Model.NumOrden.Value);
                        }
                    }

                    if (TestInfo.NumSerie == null)
                        throw new Exception("No se ha obtenido de la BBDD un numero de serie");
                }

                luxometro.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));
                Delay(2000, "");
                luxometro.WriteValidationChange();
                Delay(2000, "");
            }
        }

        public void TestVerificacion()
        {
            luxometro.Modbus.PerifericNumber = 1;

            var adjustSave = luxometro.ReadGainFactor(Escala);
            Assert.IsTrue(adjustSave == ValorGainCalculate, Error().UUT.CONFIGURACION.NO_GRABADO("Error el valor de ganacia no ses ha grabado en el equipo despues del reset"));

            if (Model.IdFase == "122")
            {
                var numSerie = luxometro.ReadSerialNumber();
                Assert.IsTrue(numSerie.ToString().Trim() == TestInfo.NumSerie.Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el numero de serie grabado no es el mismo que el leido"));
            }

            var luxAdjust = new AdjustValueDef { Name = Params.LUX.Null.Verificacion.TestPoint(Escala.ToString()).Name, Average = Params.LUX.Null.Verificacion.TestPoint(Escala.ToString()).Avg(), Tolerance = Params.LUX.Null.Verificacion.TestPoint(Escala.ToString()).Tol(), Unidad = ParamUnidad.LUX };

            var retries = 5;

            TestCalibracionBase(luxAdjust,
             () =>
             {
                 return (double)luxometro.ReadMedidaLuxometro();
             },
             () =>
             {
                 return ValorLuxPatron;
             }, retries, retries * 2, 500, "Error lectrua estabilizacion de lux fuera de margenes");
        }

        //******************************************

        public void VerificacionGPSPatron(string nameDevicePortVirtual)
        {
            var portPatron = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            LuxometroPatron.SetPort(portPatron, 1);

            var ListInfoGPS = new List<AdjustValueDef>();
            var satelites = new AdjustValueDef { Name = "SATELITES_PATRON", Min = Margenes.GetDouble("SATELITES_MIN", 10,ParamUnidad.SinUnidad), Max = Margenes.GetDouble("SATELITES_MAX", 50, ParamUnidad.SinUnidad), Unidad = ParamUnidad.Numero };
            var mediaSatelites = new AdjustValueDef { Name = "MEDIA_SATELITES_PATRON", Min = Margenes.GetDouble("MEDIA_SATELITES_MIN", 5, ParamUnidad.SinUnidad), Max = Margenes.GetDouble("MEDIA_SATELITES_MAX", 10, ParamUnidad.SinUnidad), Unidad = ParamUnidad.Numero };
            var latitud = new AdjustValueDef { Name = "LATITUD_PATRON", Min = Margenes.GetDouble("LATITUD_MIN", 4000, ParamUnidad.SinUnidad), Max = Margenes.GetDouble("LATITUD_MAX", 4500, ParamUnidad.SinUnidad), Unidad = ParamUnidad.Grados };
            var longitud = new AdjustValueDef { Name = "LONGITUD_PATRON", Min = Margenes.GetDouble("LONGITUD_MIN", 100, ParamUnidad.SinUnidad), Max = Margenes.GetDouble("LONGITUD_MAX", 200, ParamUnidad.SinUnidad), Unidad = ParamUnidad.Grados };

            ListInfoGPS.Add(satelites);
            ListInfoGPS.Add(mediaSatelites);
            ListInfoGPS.Add(latitud);
            ListInfoGPS.Add(longitud);

            SamplerWithCancel((p) =>
            {

                if (p > 0)
                    ResetUSB(portPatron);

                TestMeasureBase(ListInfoGPS, (step) =>
                {
                    var infoGPSPatron = LuxometroPatron.ReadGPSInformation();
                    var result = new List<double>()
                    {
                    infoGPSPatron.StatlitsNumber, infoGPSPatron.StalitsAverage, infoGPSPatron.latitud, infoGPSPatron.longitud // diftime
                    };

                    return result.ToArray();

                }, 0, 10, 2000);

                return true;
            }, "Error no se ha podido Ajustar GPS del PATRON", 2, 2000, 0, false, false);


            SamplerWithCancel((p) =>
            {
                var infoGPSPatron = LuxometroPatron.ReadGPSInformation();
                Assert.AreEqual(infoGPSPatron.status.ToString(), Margenes.GetDouble("STATUS", 65, ParamUnidad.SinUnidad).ToString(), Error().UUT.CONFIGURACION.GPS("Error el estado del GPS Patron patron no es esperado"));
                InfoGPSPatron = infoGPSPatron;
                return true;
            }, "Error el estado del GPS Patron no es esperado", 15, 2000, 1000, false, false);


            LuxometroPatron.Dispose();
            luxometroPatron = null;
        }

        public void VerificacionGPS(string nameDevicePortVirtual)
        {
            var portPatron = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
            luxometro.SetPort(portPatron, 1);

            SamplerWithCancel((p) =>
            {
                var firmwareVersion = luxometro.ReadFirmwareVersion();
                Assert.IsTrue(firmwareVersion == Convert.ToUInt16(Identificacion.VERSION_FIRMWARE), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error Version de Firmware incorrecta"));
                return true;
            }, "", 4, 500, 500, false, false);

            var ListInfoGPS = new List<AdjustValueDef>();
            var satelites = new AdjustValueDef { Name = "SATELITES", Min = InfoGPSPatron.StatlitsNumber / 2, Max = InfoGPSPatron.StatlitsNumber * 2, Unidad = ParamUnidad.Numero };
            var mediaSatelites = new AdjustValueDef { Name = "MEDIA_SATELITES", Min =InfoGPSPatron.StalitsAverage / 2, Max = InfoGPSPatron.StalitsAverage  * 2, Unidad = ParamUnidad.Numero };
            var latitud = new AdjustValueDef { Name = "LATITUD", Min = Margenes.GetDouble("LATITUD_MIN", 4000, ParamUnidad.SinUnidad), Max = Margenes.GetDouble("LATITUD_MAX", 4500, ParamUnidad.SinUnidad), Unidad = ParamUnidad.Grados };
            var longitud = new AdjustValueDef { Name = "LONGITUD", Min = Margenes.GetDouble("LONGITUD_MIN", 100, ParamUnidad.SinUnidad), Max = Margenes.GetDouble("LONGITUD_MAX", 200, ParamUnidad.SinUnidad), Unidad = ParamUnidad.Grados };
             var diffTime = new AdjustValueDef { Name = "TIME_OFFSET", Min = Margenes.GetDouble("TIME_OFFSET_MIN", 0, ParamUnidad.s), Max = Margenes.GetDouble("TIME_OFFSET_MAX", 30, ParamUnidad.s), Unidad = ParamUnidad.s };

            ListInfoGPS.Add(satelites);
            ListInfoGPS.Add(mediaSatelites);
            ListInfoGPS.Add(latitud);
            ListInfoGPS.Add(longitud);
             ListInfoGPS.Add(diffTime);

            var retries = Convert.ToInt32(Configuracion.GetDouble("REINTENTOS_VERIFICACION", 10, ParamUnidad.SinUnidad));

            TestMeasureBase(ListInfoGPS, (step) =>
            {
                var InfoGPS= luxometro.ReadGPSInformation();
                logger.DebugFormat("FECHA: {0}  HORA: {1}", InfoGPS.datetime.ToShortDateString(), InfoGPS.datetime.ToShortTimeString());
                var diftime = Math.Abs(InfoGPS.datetime.Subtract(DateTime.UtcNow).TotalDays);
                var result = new List<double>()
                {
                    InfoGPS.StatlitsNumber, InfoGPS.StalitsAverage, InfoGPS.latitud, InfoGPS.longitud, diftime
                };

                return result.ToArray();
            }, 0, retries, 2000);


            SamplerWithCancel((p) =>
            {
                var InfoGPSstatus = luxometro.ReadGPSInformation();
                Assert.AreEqual(InfoGPSstatus.status.ToString(), Margenes.GetDouble("STATUS", 65, ParamUnidad.SinUnidad).ToString(), Error().UUT.CONFIGURACION.GPS("Error el estado del GPS patron no es esperado"));
                return true;
            }, "Error el estado del GPS no es esperado", 15, 2000, 1000, false, false);          

        }

        //********************************************

        public void TestFinish()
        {

            //try
            //{
            //    if (lambda != null)
            //    {
            //        lambda.ApplyOff();
            //        lambda.Dispose();
            //    }
            //}
            //catch { }

            //try
            //{
            //    if (Metrix3293 != null)
            //        Metrix3293.Dispose();
            //}
            //catch { }

            try
            {
                if (luxometroPatron != null)
                    luxometroPatron.Dispose();
            }
            catch { }

            try
            {
                if (luxometroPatronEstatico != null)
                    luxometroPatronEstatico.Dispose();
            }
            catch { }

            try
            {
                if (luxometro != null)
                    luxometro.Dispose();
            }
            catch { }

            if (Tower != null)
            {
                Tower.ShutdownSources();
                Tower.IO.DO.Off(UTIL1, UTIL2, ALIMENTACION_LX485, MEDIDA_LX485);
                Tower.Dispose();
            }

        }

        //****************************************************************
        //****************************************************************

        private void AjusteLuxometroPatronByPowerSource()
        {
            var RangToChangeVoltageReference = Consignas.GetDouble(Params.I.Other("INCREMENT").Ajuste.TestPoint(Escala.ToString()).Name, 0.001, ParamUnidad.V);
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint(Escala.ToString()).Name, 11.5, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I_DC.Null.Ajuste.TestPoint(Escala.ToString()).Name, 4.4, ParamUnidad.A);

            Tower.LAMBDA.ApplyPresets(voltage, current);
            Delay(timeEstabilization, "");

            var luxAdjust = new AdjustValueDef { Name = Params.LUX.Other("PATRON").Ajuste.TestPoint(Escala.ToString()).Name, Average = Params.LUX.Other("PATRON").Ajuste.TestPoint(Escala.ToString()).Avg(), Tolerance = Params.LUX.Other("PATRON").Ajuste.TestPoint(Escala.ToString()).Tol(), Unidad = ParamUnidad.LUX };

            TestPatronByEscala(luxAdjust, () => LuxometroPatron.ReadMedidaLuxometro(), voltage, current, RangToChangeVoltageReference, 5);

            ValorCurrent = Tower.LAMBDA.ReadCurrent();
            Resultado.Set(Params.V.Null.Ajuste.TestPoint(Escala.ToString()).Name, voltage, ParamUnidad.V);
            Resultado.Set(Params.I_DC.Null.Ajuste.TestPoint(Escala.ToString()).Name, ValorCurrent, ParamUnidad.mA);

            ValorLuxPatron = LuxometroPatron.ReadMedidaLuxometro();
            Resultado.Set(Params.LUX.Other("PATRON").Ajuste.TestPoint(Escala.ToString()).Name, valorLuxPatron, ParamUnidad.LUX);
        }

        private void TestPatronEstaticoByEscala()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.TestPoint(Escala.ToString()).Name, 11.5, ParamUnidad.V);

            var luxAdjust = new AdjustValueDef { Name = Params.LUX.Other("ESTATICO").Ajuste.TestPoint(Escala.ToString()).Name, Average = Params.LUX.Other("ESTATICO").Ajuste.TestPoint(Escala.ToString()).Avg(), Tolerance = Params.LUX.Other("ESTATICO").Ajuste.TestPoint(Escala.ToString()).Tol(), Unidad = ParamUnidad.LUX };

            if (ValorCurrent == 0 | ValorLuxPatron == 0)
                throw new Exception("Error no se ha realizado correctamente el TEST SONDA PATRON, salga del programa y vuelva a entrar para que le pida el test de la SONDA PATRON");

            var cache = this.GetService<ICacheService>();
            if (cache.TryGet<bool>("LAMBDA_CONFIGURED", out bool lambdaConfigured))
            {
                var valorCurrentTemp = Tower.LAMBDA.ReadCurrent();
                if (ValorCurrent != valorCurrentTemp)
                {
                    Tower.LAMBDA.ApplyPresets(voltage, valorCurrent);
                    Delay(timeEstabilization, "");
                }
                cache.AddOrSet("LAMBDA_CONFIGURED", true);
            }

            SamplerWithCancel(
            (p) =>
            {
                return TestPatronByEscalaEstabilization(luxAdjust, () => luxometroPatronEstatico.ReadMedidaLuxometro(), 5, 1500)== StateAdjust.OK;
            }, "", cancelOnException: false, throwException: false);

            voltage = Tower.LAMBDA.ReadVoltage();
            ValorCurrent = Tower.LAMBDA.ReadCurrent();

            Resultado.Set(Params.V.Null.Ajuste.TestPoint(Escala.ToString()).Name, voltage, ParamUnidad.V);
            Resultado.Set(Params.I_DC.Null.Ajuste.TestPoint(Escala.ToString()).Name, ValorCurrent, ParamUnidad.mA);
        }

        private void TestPatronByEscala(AdjustValueDef luxAdjust, Func<double> funcLuxometro, double voltage, double current, double incrementVoltage, byte retries = 5, int timeWait = 1000)
        {
            var retriesRead = retries;
            var respAdjust = StateAdjust.OK;
            SamplerWithCancel(
            (p) =>
            {          
                respAdjust = TestPatronByEscalaEstabilization(luxAdjust, funcLuxometro, retries, timeWait);
                if (respAdjust == StateAdjust.OK)
                    return true;
              
                if (p > 0)
                {
                    if (respAdjust == StateAdjust.PorDEbajo)
                        current -= incrementVoltage;
                    else
                        current += incrementVoltage;

                    logger.InfoFormat("Aumentamos valo de la fuente a {0}V en el reintento {1} de {2}", voltage, p, retries);
                    Tower.LAMBDA.ApplyPresets(voltage, current);
                    //lambda.ApplyPresets(voltage, current);
                    Delay(timeEstabilization, "");
                }
                return false;

            }, "Error no se ha podido obtener un valor de voltage para un valor de lux estable", retries, timeWait, throwException: false, cancelOnException: false);
        }

        public enum StateAdjust
        {
            PorEncima,
            PorDEbajo,
            OK,
        }

        private StateAdjust TestPatronByEscalaEstabilization(AdjustValueDef luxAdjust, Func<double> funcLuxometro, byte retries = 5, int timeWait = 1000)
        {
            var retriesRead = retries;

            try
            {
                TestCalibracionBase(luxAdjust,
                () =>
                {
                    return funcLuxometro();
                },
                () =>
                {
                    return luxAdjust.Average;
                }, retriesRead, retriesRead + 2, timeWait, "Error lectrua estabilizacion de lux fuera de margenes");
            }
            catch (AssertFailedException)
            {
                logger.Debug("Excepcion capturada en el Assert de la Calibracion Base");
            }

            var result= luxAdjust.IsValidAverageError();
            if (result)
                return StateAdjust.OK;
            else
            {
                if (luxAdjust.Error > 0)
                    return StateAdjust.PorDEbajo;
                else
                    return StateAdjust.PorEncima;
            }
        }

        private void ResetUSB(byte port)
        {
            LuxometroPatron.Dispose();
            luxometroPatron = null;

            Shell.MsgBox("NO SE CALIBRA BIEN LA SONDA PATRON DESCONECTELA EL USB DOS SEGUNDOS Y CONECTELO OTRA VEZ", "ERROR SONDA PATRON",
              MessageBoxButtons.OK);

            Delay(3000, "");

            LuxometroPatron.SetPort(port, 1);
        }
    }
}
