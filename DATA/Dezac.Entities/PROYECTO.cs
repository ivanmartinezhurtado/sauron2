namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PROYECTO
    {
        public PROYECTO()
        {
            this.BIEN = new HashSet<BIEN>();
            this.ORDEN = new HashSet<ORDEN>();
            this.PROYECTO1 = new HashSet<PROYECTO>();
            this.ARTICULO = new HashSet<ARTICULO>();
        }
    
        public int NUMPROYECTO { get; set; }
        public System.DateTime FECHAALTA { get; set; }
        public string DESCRIPCION { get; set; }
        public string CODIGO { get; set; }
        public string ABIERTO_SN { get; set; }
        public string IDEMPLEADO { get; set; }
        public string IDSITUACION { get; set; }
        public string IDGFH { get; set; }
        public string DESCRIPCIONDETALLADA { get; set; }
        public Nullable<System.DateTime> FECHACIERRE { get; set; }
        public System.DateTime FECHAFINPREVISTA { get; set; }
        public decimal COSTEPREVISTO { get; set; }
        public decimal HORASPREVISTAS { get; set; }
        public string IDCLIENTE { get; set; }
        public Nullable<decimal> MAHORASPREVISTAS { get; set; }
        public Nullable<decimal> IMPORTEVENTA { get; set; }
        public Nullable<int> NUMARTICULO { get; set; }
        public string USUARIO { get; set; }
        public string AMBITO { get; set; }
        public Nullable<int> NUMPROYECTOPADRE { get; set; }
        public string IDTIPO { get; set; }
        public string IDCLASE { get; set; }
        public string DESCRIPCIONCLIENTE { get; set; }
        public string REFCLIENTE { get; set; }
    
        public virtual ICollection<BIEN> BIEN { get; set; }
        public virtual ICollection<ORDEN> ORDEN { get; set; }
        public virtual ICollection<PROYECTO> PROYECTO1 { get; set; }
        public virtual PROYECTO PROYECTO2 { get; set; }
        public virtual ICollection<ARTICULO> ARTICULO { get; set; }
        public virtual ARTICULO ARTICULO1 { get; set; }
    }
}
