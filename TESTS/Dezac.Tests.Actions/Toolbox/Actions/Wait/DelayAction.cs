﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;
using NUnrar;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(DelayActionDescription))]
    public class DelayAction : ActionBase
    {
        [Description("Tiempo de espera en milisegundos")]
        [Category("3. Configuration")]
        public ActionVariable Milliseconds { get; set; } = new ActionVariable();

        [Description("Tiempo de espera en segundos")]
        [Category("3. Configuration")]
        public ActionVariable Seconds { get; set; } = new ActionVariable();

        [Description("Barra donde se enseña la progresión del tiempo de espera")]
        [Category("3. Configuration")]
        public bool? ShowProgress { get; set; }

        private System.Threading.Timer timer;
        private Form form;
        private ProgressBar pb;
        private int steps;
        private Exception exception;

        public override void Execute()
        {
            int milliSeconds = 0;
       
            if (Milliseconds != null && !Milliseconds.IsKeyNullOrEmpty)
                milliSeconds = Convert.ToInt32(Milliseconds.Value);
            else if (Seconds != null && !Seconds.IsKeyNullOrEmpty)
                milliSeconds = Convert.ToInt32(Seconds.Value) * 1000;

            if (milliSeconds == 0)
                return;

            steps = (int)Math.Ceiling(milliSeconds / 500D) + 1;
            int interval = milliSeconds / steps;

            var autoEvent = new AutoResetEvent(false);
            timer = new System.Threading.Timer(Timer_Tick, autoEvent, interval, interval);

            if (ShowProgress.GetValueOrDefault())
            {
                var context = SequenceContext.Current;
                var iShell = context.Services.Get<IShell>();

                form = iShell.ShowForm(() =>
                {
                    pb = new ProgressBar();
                    pb.Dock = DockStyle.Fill;
                    pb.Maximum = milliSeconds;
                    pb.Step = interval;
                    pb.PerformStep();

                    Form frm = new Form();
                    frm.Size = new Size(300, 80);

                    if (Seconds != null && !Seconds.IsKeyNullOrEmpty)
                        frm.Text = string.Format("Delay {0} ms", milliSeconds);
                    else
                    	frm.Text = string.Format("Delay {0} s", (milliSeconds / 1000));


                    frm.StartPosition = FormStartPosition.CenterScreen;
                    frm.ControlBox = false;
                    frm.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                    frm.TopMost = true;

                    frm.Controls.Add(pb);

                    return frm;
                });
            }
            autoEvent.WaitOne();
            if (exception != null)
                throw exception;         
        }

        private void Timer_Tick(object stateInfo)
        {
            AutoResetEvent autoEvent = (AutoResetEvent)stateInfo;

            try
            {
                if (ShowProgress.GetValueOrDefault() && (form == null || pb == null || pb.IsDisposed))
                    return;

                var cancel = SequenceContext.Current.CancellationToken;
                if (cancel.IsCancellationRequested)
                {
                    DisposeForm();
                    TestException.Create().PROCESO.OPERARIO.TEST_CANCELADO().Throw();
                }

                if (--steps == 0)
                {
                    autoEvent.Set();
                    pb?.Invoke((MethodInvoker)(() => pb?.PerformStep()));
                    DisposeForm();
                    timer = null;
                }
                else if (steps > 0)
                    pb?.Invoke((MethodInvoker)(() => pb?.PerformStep()));
            }
            catch (Exception ex)
            {
                exception = ex;
                autoEvent.Set();
            }
        }

        private void DisposeForm()
        {
            pb?.Invoke((MethodInvoker)(() => pb?.Dispose()));
            pb = null;
            form?.Invoke((MethodInvoker)(() => form?.Dispose()));
            form = null;
            timer?.Dispose();
            timer = null;
        }

        public override void PostExecute(StepResult stepResult)
        {
            DisposeForm();
        }
    }

    public class DelayActionDescription : ActionDescription
    {
        public override string Name { get { return "Delay"; }}
        public override string Category { get { return "Actions/Wait"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
        public override string Description { get { return "Action para crear una espera en milisegundos"; } }
    }
}

