﻿using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.SCPI
{
    public class SCPIContext
    {
        public string Cmd { get; set; }
        public IConsoleDevice Device { get; set; }
        internal SCPIContext parent;

        public SCPIContext()
        {
            this.Cmd = string.Empty;
        }

        public SCPIContext(IConsoleDevice device)
            : this()
        {
            this.Device = device;
        }

        public string BuildCommand()
        {
            if (Parent != null)
                return Parent.BuildCommand() + Cmd;

            return Cmd;
        }

        public SCPIContext Parent
        {
            get { return parent; }
            set { parent = value; Device = value.Device; }
        }
    }
}
