﻿namespace Dezac.Tests.Dataman.Controls
{
    partial class LedIndicator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Led = new System.Windows.Forms.Label();
            this.LabelText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Led
            // 
            this.Led.BackColor = System.Drawing.Color.Gray;
            this.Led.ForeColor = System.Drawing.Color.Black;
            this.Led.Location = new System.Drawing.Point(2, 2);
            this.Led.Name = "Led";
            this.Led.Size = new System.Drawing.Size(49, 41);
            this.Led.TabIndex = 8;
            this.Led.Paint += new System.Windows.Forms.PaintEventHandler(this.Led_Paint);
            // 
            // LabelText
            // 
            this.LabelText.BackColor = System.Drawing.Color.Gray;
            this.LabelText.Cursor = System.Windows.Forms.Cursors.Default;
            this.LabelText.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelText.ForeColor = System.Drawing.Color.White;
            this.LabelText.Location = new System.Drawing.Point(64, 16);
            this.LabelText.Name = "LabelText";
            this.LabelText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelText.Size = new System.Drawing.Size(183, 17);
            this.LabelText.TabIndex = 7;
            this.LabelText.Text = "POWER";
            // 
            // LedIndicator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(5F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.Led);
            this.Controls.Add(this.LabelText);
            this.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "LedIndicator";
            this.Size = new System.Drawing.Size(260, 46);
            this.SizeChanged += new System.EventHandler(this.LedIndicator_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label Led;
        internal System.Windows.Forms.Label LabelText;
    }
}
