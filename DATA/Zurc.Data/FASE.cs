namespace Zurc.Data
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Fase")]
    public partial class Fase
    {
        [Key]
        [StringLength(10)]
        public string Id { get; set; }
        [StringLength(100)]
        public string Descripcion { get; set; }
        [DefaultValue(false)]
        public bool FaseTest { get; set; }
    }
}
