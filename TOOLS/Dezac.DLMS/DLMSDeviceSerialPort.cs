﻿using Gurux.Serial;
using System.IO.Ports;

namespace Dezac.DLMS
{
    public class DLMSDeviceSerialPort : DLMSDevice
    {
        private int portCom;
        private GXSerial sp;
        
        private GXSerial Sp
        {
            get
            {
                if (sp == null)
                {
                    sp = new GXSerial("COM" + portCom);
                    this.Media = sp;
                }
                else this.Media = sp;

                return sp;
            }
        }

        public DLMSDeviceSerialPort()
           : base()
        {
            Sp.ReadTimeout = 5000;
            Sp.WriteTimeout = 3000;
            Sp.BaudRate = 115200;
            Sp.Parity = Parity.None;
            Sp.DataBits = 8;
            Sp.StopBits = StopBits.One;
            Sp.Trace = System.Diagnostics.TraceLevel.Verbose;
        }

        public DLMSDeviceSerialPort(int portCom, int bps = 115200)
            : base()
        {
            this.portCom = portCom;
            Sp.ReadTimeout = 5000;
            Sp.WriteTimeout = 3000;
            Sp.BaudRate = bps;
            Sp.Parity = Parity.None;
            Sp.DataBits = 8;
            Sp.StopBits = StopBits.One;
        }

        public int PortCom
        {
            get { return portCom; }
            set
            {
                if (portCom == value)
                    return;

                if (sp.IsOpen)
                    sp.Close();

                portCom = value;
                sp.PortName = "COM" + portCom;
            }
        }

        public int BaudRate
        {
            get
            {
                return sp.BaudRate;
            }
            set
            {
                sp.BaudRate = value;
            }
        }

        public int DataBits
        {
            get
            {
                return sp.DataBits;
            }
            set
            {
                sp.DataBits = value;
            }
        }

        public Parity Parity
        {
            get
            {
                return sp.Parity;
            }
            set
            {
                sp.Parity = value;
            }
        }

        public StopBits StopBits
        {
            get
            {
                return sp.StopBits;
            }
            set
            {
                sp.StopBits = value;
            }
        }

        public bool RtsEnable
        {
            get { return sp.RtsEnable; }
            set { sp.RtsEnable = value; }
        }

        public bool DtrEnable
        {
            get { return sp.DtrEnable; }
            set { sp.DtrEnable = value; }
        }

        public bool IsOpen
        {
            get
            {
                return sp.IsOpen;
            }
        }

        public void OpenPort()
        {
            if (!sp.IsOpen)
                sp.Open();
        }

        public void ClosePort()
        {
            if (sp.IsOpen)
                sp.Close();
        }
    }
}
