﻿using Dezac.Core.Exceptions;
using Dezac.Services;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;
namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(GetMACDescription))]

    public class GetMAC : ActionBase
    {
        [Description("Número de orden de fabricación")]
        [Category("3. Configuration")]
        public int? FabricationOrder { get; set; }

        [Description("Número de Identificación del operario")]
        [Category("3. Configuration")]
        public string IDOperator { get; set; }

        [Description("Número MAC del equipo")]
        [Category("3. Configuration")]
        [DefaultValue(1)]
        public int NumMACS { get; set; }

        [Description("Descripción del nombre de la variable a guardar la MAC")]
        [Category("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();


        public override void Execute()
        {
            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            string idOperario = IDOperator ?? "230";

            if(DataInput == null)
                DataInput = new ActionVariable("");

            if (data != null && !string.IsNullOrEmpty(data.IdOperario))
                idOperario = data.IdOperario;

            if (data != null)
                FabricationOrder = data.NumOrden;

            Assert.IsNotNull(FabricationOrder, Error().PROCESO.PRODUCTO.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha especificado una orden de producción"));

            if (SequenceContext.Current.Variables.ContainsKey(ConstantsParameters.TestInfo.MAC))
                return;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

           testBase.GetMacServiceDezac(NumMACS, DataInput.Value.ToString());
        }

        public class GetMACDescription : ActionDescription
        {
            public override string Name { get { return "GetMAC"; } }
            public override string Category { get { return "DezacTest/Oracle/Parameters"; } }
            public override string Description { get { return "Action que sirve para guardar el número de MAC del equipo al cual se le está realizando la secuencia"; } }
            public override Image Icon { get { return Properties.Resources.database_process_icon; } }
            public override string Dependencies { get { return "InitTestBase;InitProductAction;InitBastidorMultipleAction"; } }
        }
    }
}
