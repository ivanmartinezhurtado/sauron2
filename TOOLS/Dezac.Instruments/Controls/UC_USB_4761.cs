﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.IO;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(Usb4761Description))]
    public partial class UC_USB_4761 : UserControl
    {
        private MonitorInputs m = new MonitorInputs();
        private delegate void ReadInputsDelegate(byte input, bool state);

        private Dictionary<byte, bool> Inputs = new Dictionary<byte, bool>();
        private Dictionary<byte, InputControl> ControlInputs = new Dictionary<byte, InputControl>();

        private USB4761 usb;
        private Exception instrumentException;

        public Type InstrumentType
        {
            get
            {
                return typeof(USB4761);
            }
        }

        public USB4761 Instrument
        {
            get
            {
                if (usb == null)
                {
                    instrumentException = null;
                    try
                    {      
                        usb = new USB4761();                    
                    }
                    catch (Exception ex)
                    {
                        instrumentException = ex;
                    }
                }
                return usb;
            }
            set
            {
                if (usb != null)
                {
                    usb.Dispose();
                    usb = null;
                }
                usb = value;
            }
        }

        public UC_USB_4761()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                Instrument.LogEnabled = false;

                if(instrumentException != null)
                {
                    Logger.Error("Error cargando la targeta USB-4761", instrumentException);
                    MessageBox.Show(instrumentException.Message, "Error IO USB-4761", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                InitCard();
            }
            catch (IOException ex)
            {
                Logger.Error("Error cargando la targeta la tarjeta USB-4761", ex);
                MessageBox.Show(ex.DecodedMessage, "Error IO USB-4761", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InitCard()
        {
            for (byte i = 0; i < Instrument.DI.Count; i++)
            {
                InputControl item = new InputControl();
                //item.IO = pci;
                item.NumIO = i;
                item.UpdateUI(Instrument.DI[i]);
                Inputs.Add(i, Instrument.DI[i]);
                ControlInputs.Add(i, item);
                InputPanel.Controls.Add(item);
            }

            for (byte i = 0; i < Instrument.DO.Count; i++)
            {
                OutputControl item = new OutputControl();
                item.IO = Instrument;
                item.NumIO = i;
                item.Width = 60;
                item.Height = 28;
                panelOutputs.Controls.Add(item);
            }
            m.Inputs = Inputs;
            m.IO = Instrument;
            m.Interval = 1000;
            m.ValueCahnged += new EventHandler<InputsEventArgs>(m_ValueReaded);
            m.Start();
        }

        void m_ValueReaded(object sender, InputsEventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new ReadInputsDelegate(UpdatePanel), (byte)e.Value, (bool)e.State);
            else
                UpdatePanel((byte)e.Value, (bool)e.State);
        }

        private void UpdatePanel(byte input, bool state)
        {
            ControlInputs[input].UpdateUI(state);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            m.Stop();

            if(usb != null)
                usb.Dispose();

            base.Dispose(disposing);
        }
    }

    public class Usb4761Description : ControlDescription
    {
        public override string Name { get { return "USB4761"; } }
        public override string Category { get { return "In/Out"; } }
        public override Image Icon { get { return Properties.Resources.input; } }
        public override Image IconBig { get { return Properties.Resources.USB4761; } }
        public override Type SubcomponentType { get { return typeof(USB4761); } }
    }
}
