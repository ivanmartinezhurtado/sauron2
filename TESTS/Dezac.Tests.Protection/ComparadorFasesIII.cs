﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.03)]
    public class ComparadorFasesIII : TestBase
    {   
        private TowerBoard tower;

        internal const int _24V = 14;
        internal const int OUT_PILOTO_ERROR_PLACA = 20;
        internal const int OUT_PILOTO_ERROR_EQUIPO = 21;
        internal const int IN_PRESENCIA_EQUIPO = 10;
        internal const int IN_PRESENCIA_PLACA = 9;
        internal const int IN_LED_PLACA = 15;
        internal const int IN_LED_EQUIPO = 16;
        internal const int BLOQUE_SEGURIDAD = 23;

        private bool IsMonofasic { get; set; }

        private InputMuxEnum InputLed { get; set; }

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[BLOQUE_SEGURIDAD];
            }, "Error. No se detecta el CONNECTOR DE SEGURIDAD", 5, 500, 1000);

            SamplerWithCancel((p) =>
            {
                if (Model.IdFase == "120")
                {
                    if (!tower.IO.DI[IN_PRESENCIA_EQUIPO])
                        throw new Exception("Error no se detecta la presencia del equipo");
                }
                else
                {
                    if (!tower.IO.DI[IN_PRESENCIA_PLACA])
                        throw new Exception("Error no se detecta la presencia de la placa");
                }

                return true;

            }, "No se detecta la PRESENCIA del equipo y/o de la placa", 5, 500, 1000, false, false);

            tower.ActiveVoltageCircuit();

            IsMonofasic = Identificacion.MODELO.Trim().ToUpper() == "MONOFASIC";

            InputLed = InputMuxEnum.IN2;

            if (Model.IdFase == "120")
                InputLed = InputMuxEnum.IN3;

            tower.HP53131A
            .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
            .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 2.5D)
            .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.AutoStop, HP53131A.Gate.SlopeEnum.NEG, 4);

            tower.MUX.Connect((byte)InputLed, Outputs.FreCH1);
        }

        public void TestFrequncyLED(double voltage, double desfase, string stepName, bool aceptedZero = false)
        {
            var voltageL1L2 = Consignas.GetDouble(Params.V.Null.TestPoint(stepName).Name, voltage, ParamUnidad.V);
            var desfaseL1L2 = Consignas.GetDouble(Params.PHASE.Null.TestPoint(stepName).Name, desfase, ParamUnidad.Grados);

            var testVoltajes = new TriLineValue { L1 = voltageL1L2, L2 = IsMonofasic ? 0 : voltageL1L2, L3 = 0 };
            var testDesfases = new TriLineValue { L1 = 0, L2 = desfaseL1L2, L3 = 0 };

            tower.PowerSourceIII.Tolerance = 5;
            tower.MTEPS.ApplyPresetsAndWaitStabilisation(testVoltajes, 0, 50, 0, IsMonofasic ? 0 : testDesfases, true);

            var adjustValueTestPoint = new AdjustValueDef(Params.FREQ.Other("LEDS").TestPoint(stepName).Name, 0, Params.FREQ.Other("LEDS").TestPoint(stepName).Min(), Params.FREQ.Other("LEDS").TestPoint(stepName).Max(), 0, 0, ParamUnidad.Hz);

            TestMeasureBase(adjustValueTestPoint, (step) =>
            {
                try
                {
                    return tower.MeasureWithFrecuency(3000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("VI_ERROR_TMO: A timeout occurred") & aceptedZero )
                        return 0D;

                    throw new Exception(ex.Message);
                }
            }, 0, 3, 500);
        }

        public void TestFinish()
        {
            if (tower != null)
            {
                tower.MUX.Disconnect((byte)InputLed, Outputs.FreCH1);
                tower.ShutdownSources();
                tower.Dispose();
            }
        }
    }
}
