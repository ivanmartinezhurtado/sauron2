﻿using Dezac.Core.Utility;
using Dezac.Tests.AOI;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using WinTestPlayer.Model;
using WinTestPlayer.Services;
using WinTestPlayer.Utils;
using WinTestPlayer.Views;

namespace WinTestPlayer
{
    public partial class MainForm : RadForm
    {
        protected readonly AppService app;
        protected readonly ShellService shell;

        private TestView testView;
        private Control currentView;
        private AOIResultViewerForm AOIForm;

        private string Caption = "";

        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(AppService appService, ShellService shell)
            : this()
        {
            app = appService;
            this.shell = shell;
            Text = string.Format("TSD -Test Studio Dezac  v.{0}", Application.ProductVersion);
            Caption = string.Format("TSD -Test Studio Dezac  v.{0}", Application.ProductVersion);

            app.SC = SynchronizationContext.Current;
            app.Runner.AllTestStarting += OnTestStart;
            app.Runner.TestEnd += OnTestEnded;
            app.Runner.AllTestEnded += OnAllTestEnded;
            app.CloseForm += (s, ee) => { OnCloseForm(); };
            sbTxtAutoTest.Text = app.NotifyAutoTest();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            IEnumerable<Process> processes = Process.GetProcesses().Where(p => p.ProcessName.Contains("bartend"));
            foreach (Process process in processes)
                process.Kill();

            if (app.Model.WhitOutLine == true)
            {
                ExitTest();
                return;
            }

            shell.Init(this);
            slideMenu.Collapse();

            ThemeResolutionService.LoadPackageResource("WinTestPlayer.Resources.ExamplesBlue.tssp");
            ThemeResolutionService.RegisterThemeFromStorage(ThemeStorageType.Resource, "WinTestPlayer.Resources.ExamplesBlue_QuickStart_ControlsExpander.xml");
            ThemeResolutionService.ApplyThemeToControlTree(slideMenu, "ExamplesBlue");

            slideMenu.ControlsList.SelectedIndexChanged += ControlsList_SelectedIndexChanged;

            DoSelectOrden(true);

            if (app.Model.IsAOI)
                ShowAOIViewResults();
        }

        public void DoSelectOrden(bool exit)
        {
            slideMenu.Collapse();

            var ctrl = new LoginEmpleadoView();
            if (shell.ShowDialog(Caption, ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                if (exit)
                    Close();

                return;
            }

            if (ctrl.Unlocked)
            {
                DoLogin();
                return;
            }
            else
                app.Model.ModoPlayer = ModoPlayer.Normal;

            try
            {
                app.Login(ctrl.Codigo);
                PopulateControlsList();
            }
            catch (Exception ex)
            {
                shell.MsgBox(ex.Message, "Excepción al cargar los datos de la orden de fabricación", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                DoSelectOrden(true);
            }
        }

        private void DoLogin()
        {
            slideMenu.Collapse();

            var ctrl = new LoginView();

            if (shell.ShowDialog(Caption, ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                DoSelectOrden(true);
                return;
            }

            app.Model.IdUsuario = ctrl.empleado.IDOPERARIO;
            app.Model.IdOperario = ctrl.empleado.IDOPERARIO;
            app.Model.IdEmpleado = ctrl.empleado.DEPT;
            app.Model.Operario = ctrl.empleado.NOMBRE;
            app.Model.ModoPlayer = ctrl.ModoTest;

            Text = string.Format("TSD -Test Studio Dezac  v.{0}  *** MODO {1} ***", Application.ProductVersion, ctrl.ModoTest.ToString().ToUpper());

            if (ctrl.ModoTest == ModoPlayer.SinPermisos)
            {
                MessageBox.Show("No tienes privilegios para realizar esta operación", "ERROR DE AUTORIZACION", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                DoSelectOrden(true);
            }

            PopulateControlsList();
        }

        public void PopulateControlsList()
        {
            slideMenu.ControlsList.Items.Clear();
            RadListElement listElement = slideMenu.ControlsList;
            int Index = 0;

            switch (app.Model.ModoPlayer)
            {
                case ModoPlayer.Normal:
                    listElement.Items.Add(new RadListDataItem { Text = "Inicio", Image = Properties.Resources.Play_Normal_icon__1_, Tag = new Action(DoTestOrden) });
                    listElement.Items.Add(new RadListDataItem { Text = "Historial", Image = Properties.Resources.ControlsListImages_ListView, Tag = new Action(DoHistory) });
                    break;
                case ModoPlayer.PostVenta:
                case ModoPlayer.SAT:
                case ModoPlayer.Laser:
                    listElement.Items.Add(new RadListDataItem { Text = "Inicio", Image = Properties.Resources.Play_Normal_icon__1_, Tag = new Action(DoTest) });
                    listElement.Items.Add(new RadListDataItem { Text = "Productos", Image = Properties.Resources.App_kexi_database_icon, Tag = new Action(DoProducts) });
                    listElement.Items.Add(new RadListDataItem { Text = "Historial", Image = Properties.Resources.ControlsListImages_ListView, Tag = new Action(DoHistory) });
                    Index = 1;
                    break;
                case ModoPlayer.TIC:
                    listElement.Items.Add(new RadListDataItem { Text = "Inicio", Image = Properties.Resources.Play_Normal_icon__1_, Tag = new Action(DoLoadBastidorTIC) });
                    break;
                case ModoPlayer.AutoTest:
                    listElement.Items.Add(new RadListDataItem { Text = "Inicio", Image = Properties.Resources.Play_Normal_icon__1_, Tag = new Action(DoAutoTest) });
                    break;
                case ModoPlayer.PrintLabel:
                    listElement.Items.Add(new RadListDataItem { Text = "Print Label", Image = Properties.Resources.Play_Normal_icon__1_, Tag = new Action(DoPrintLabel) });
                    break;
            }

            listElement.Items.Add(new RadListDataItem { Text = "Configurar impresoras", Image = Properties.Resources._2_Hot_Printer_icon, Tag = new Action(DoConfigPrinters) });
            listElement.Items.Add(new RadListDataItem { Text = "Cambiar Modo Test", Image = Properties.Resources.Barcode_icon, Tag = new Action<bool>(DoSelectOrden) });
            listElement.Items.Add(new RadListDataItem { Text = "Salir", Image = Properties.Resources.Action_exit_icon, Tag = new Action(ExitTest) });

            slideMenu.ControlsList.SelectedIndex = Index;
            slideMenu.AllowExpand = true;
        }

        public void DoTestOrden()
        {
            app.LoadOrden();
            DoTest();
        }

        public void DoProducts()
        {
            slideMenu.Collapse();
            LoadView(DI.Resolve<ProductsView>());
        }

        public void DoTest()
        {
            slideMenu.Collapse();

            if (app.Model.IdEmpleado == null)
                DoSelectOrden(true);
            else
            {
                if (testView == null)
                    testView = DI.Resolve<TestView>();

                LoadView(testView);
            }
        }

        //******************** MODO TEST **********************************

        public void DoLoadBastidorTIC()
        {
            try
            {
                int bastidor = shell.ShowInputKeyboard<int>("ENTRADA DE NUMERO DE BASTIDOR", "Introduce el nº de bastidor del equipo a testear", 0);
                if (bastidor == 0)
                {
                    DoSelectOrden(true);
                    return;
                }

                app.LoadTIC(bastidor);

                if (app.Model.Orden == null)
                {
                    shell.MsgBox("Error, este bastidor no esta registrado en ninguna Orden de fabricación", "Error al cargar los datos de la orden de fabricación de este bastidor", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    DoSelectOrden(true);
                    return;
                }

                if (String.IsNullOrEmpty(app.Model.IdFase))
                {
                    shell.MsgBox("Error al cargar los datos de la orden de fabricación de este bastidor por TIC por No tiene fase Asignada", "Error, cargar Orden en MODO TIC ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DoSelectOrden(true);
                    return;
                }

                DoTest();

            }
            catch (Exception ex)
            {
                shell.MsgBox(string.Format("Error al cargar los datos de la orden de fabricación de este bastidor por TIC por Excepcion:{0}", ex.Message), "Error, cargar Orden en MODO TIC ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DoSelectOrden(true);
                return;
            }
        }

        private void DoConfigPrinters()
        {
            slideMenu.Collapse();
            if (shell.ShowDialog(Caption, new PrinterConfigView(), MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
        }

        private void DoAutoTest()
        {
            try
            {
                slideMenu.Collapse();
                app.LoadAutoTest();
                DoTest();
                app.Play();
            }
            catch (Exception ex)
            {
                shell.MsgBox(ex.Message, "ATENCIÓN");
                DoSelectOrden(true);
            }
        }

        private void DoPrintLabel()
        {
            slideMenu.Collapse();

            if (string.IsNullOrEmpty(app.Model.PrinterLabel) || app.Model.PrinterLabel == "NINGUNA")
                if (shell.ShowDialog("Configuración de las impresoras", new PrinterConfigView(), MessageBoxButtons.OKCancel) != DialogResult.OK)
                    return;

            if (app.Model.PrinterLabel == "NINGUNA" && app.Model.PrinterCharacteristics == "NINGUNA" && app.Model.PrinterIndividualPackage == "NINGUNA" && app.Model.PrinterPackingPackage == "NINGUNA")
                DoSelectOrden(true);
            else
                DoHistory();
        }

        private void DoHistory()
        {
            slideMenu.Collapse();

            LoadView(DI.Resolve<HistorialView>());
        }

        //******************************************************

        private void LoadView(Control view)
        {
            if (view.Name == "TestView")
            {
                WindowState = FormWindowState.Maximized;
                header1.Visible = true;
            }
            else
                header1.Visible = false;

            if (view == currentView)
                return;

            slideMenu.Collapse();

            if (currentView != null)
            {
                mainPanel.Controls.Remove(currentView);
                if (currentView != testView)
                    currentView.Dispose();
            }

            view.Dock = DockStyle.Fill;
            mainPanel.Controls.Add(view);

            currentView = view;

            header1.Enabled = mainPanel.Controls.Contains(testView);
        }

        private void ExitTest()
        {
            Close();
            return;
        }

        //******************** EVENTS **********************************

        private void ControlsList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            RadListDataItem item = slideMenu.ControlsList.SelectedItem;
            if (item == null)
                return;

            header1.Enabled = mainPanel.Controls.Contains(testView);

            var action = item.Tag as Action;
            if (action != null)
                action();
        }

        private void OnTestStart(object sender, RunnerTestsInfo e)
        {
            ControlBox = false;
            slideMenu.AllowExpand = false;
        }

        private void OnTestEnded(object sender, SequenceContext sequenceContext)
        {
            if (sequenceContext.NumInstance != 0)
                try
                {
                    sequenceContext.SaveResultsMethod?.Invoke();
                    try
                    {

                        LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, true, sequenceContext.NumInstance, sequenceContext.Services.Get<ITestContext>(), app.Model.IsProcess);

                        Dictionary<string, byte[]> images = sequenceContext.Variables.Get("IMAGES_FILES", new Dictionary<string, byte[]>());
                        int? numTestFase = sequenceContext.Services.Get<ITestContext>().TestInfo.NumTestFase;
                        if (numTestFase.HasValue)
                        {
                            foreach (KeyValuePair<string, byte[]> image in images)
                                DataUtils.SaveFileVANumTestFase(numTestFase.Value, image.Key, "591", image.Value);

                            sequenceContext.Variables.VariableList.Where(p => p.Key.StartsWith("PRINT_LABEL_"))
                                .ToList()
                                .ForEach(p =>
                                {
                                    string json = AppProfile.Serialize(p.Value);
                                    DataUtils.SaveFileNumTestFase(numTestFase.Value, p.Key, "501", UTF8Encoding.UTF8.GetBytes(json));
                                });
                        }
                    }
                    catch (Exception ex)
                    {
                        var body = new StringBuilder();
                        body.AppendLine("Error en el guarado de archivos en la BBDD");
                        body.AppendLine("----------------------------");
                        body.AppendLine(" ");
                        body.AppendLine($"Error: {ex.Message}");
                        body.AppendLine($"StackTrace {ex.StackTrace}");
                        SendMail.Send("Sauron Error: Error en el guarado de archivos en la BBDD", body.ToString(), false, false, true, false);
                        MessageBox.Show(ex.Message + ex.StackTrace, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    sequenceContext.TestResult.MainStepResult.ExecutionResult = StepExecutionResult.Failed;
                    sequenceContext.TestResult.MainStepResult.Exception = ex;
                }

            slideMenu.AllowExpand = true;
        }

        private void OnAllTestEnded(object sender, RunnerTestsInfo runnerTestsInfo)
        {
            bool HasError = true;
            bool AOIWeldingErrors = true;

            foreach (SequenceContext sequenceContext in runnerTestsInfo.Contexts)
                if (sequenceContext.ExecutionResult == TestExecutionResult.Completed)
                {
                    HasError = sequenceContext.TestResult.StepResults.Where(p => p.Exception != null).Any();
                    AOIWeldingErrors = sequenceContext.TestResult.StepResults.Where(p => (p.Exception != null) && (p.Exception.InnerException != null) && (p.Exception.InnerException.Message.Contains("AOI KO TEST"))).Any();
                }
                else
                {
                    HasError = true;
                    AOIWeldingErrors = sequenceContext.TestResult.StepResults.Where(p => (p.Exception != null) && (p.Exception.InnerException != null) && (p.Exception.InnerException.Message.Contains("AOI KO TEST"))).Any();
                }

            bool hasBastidorAutoreader = false;
            ICacheService cache = DI.Resolve<ICacheService>();
            hasBastidorAutoreader = Convert.ToBoolean(cache.Get("BASTIDOR_AUTOREADER"));

            Tools.DelayCall(500, () =>
            {
                string resultBoxReport = app.FinishUpdateBoxCurrent(runnerTestsInfo);

                if ((!HasError && (app.Model.IsProcess || app.Model.IsAOI)) || (app.Model.IsAOI && AOIWeldingErrors))
                    app.Play();
                else
                {
                    ResultsView resultsView;
                    List<string> bastidores = null;
                    if (app.Model.IsProcess || app.Model.IsAOI || hasBastidorAutoreader)
                        resultsView = new ResultsView(runnerTestsInfo, false);
                    else
                        resultsView = new ResultsView(runnerTestsInfo, true);
                    resultsView.ShowDialog();
                    ControlBox = true;
                    if (app.Model.ModoPlayer == Model.ModoPlayer.TIC)
                        DoLoadBastidorTIC();
                    else
                    {
                        if (resultsView.Result)
                        {
                            if (resultsView.Bastidores != null)
                                bastidores = resultsView.Bastidores.ConvertAll(bastidor => { if (bastidor.HasValue)
                                        return bastidor.Value.ToString();
                                    return null; });

                            app.Play(bastidores);
                        }
                    }
                }
            });
        }

        private void OnCloseForm()
        {
            ExitTest();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            e.Cancel = app.IsRunning;
        }

        private void ShowAOIViewResults()
        {
            AOIForm = new AOIResultViewerForm(app.Model.Orden.NUMORDEN.ToString(), app.Model.IdFase);
            AOIForm.Show();

            if (Screen.AllScreens.Length > 1)
            {
                WindowState = FormWindowState.Normal;
                System.Drawing.Rectangle screen = Screen.AllScreens[1].WorkingArea;
                SetDesktopLocation(screen.Left, screen.Top);
                WindowState = FormWindowState.Maximized;
            }

        }
    }
}
