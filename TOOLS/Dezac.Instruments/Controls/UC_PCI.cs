﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.IO;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(PciDescription))]
    public partial class UC_PCI : UserControl
    {
        private MonitorInputs m = new MonitorInputs();
        private delegate void ReadInputsDelegate(byte input, bool state);


        private delegate void DesactiveOutDelegate(byte output, bool outState);
        private delegate void ActivatedDesactivatedOut45Delegate(byte output, bool state);

        private PCI7296 pci;

        public Type InstrumentType
        {
            get
            {
                return typeof(PCI7296);
            }
        }


        public PCI7296 Instrument
        {
            get
            {
                if (pci == null)
                    pci = new PCI7296();

                return pci;
            }
            set
            {
                if (pci != null)
                {
                    pci.Dispose();
                    pci = null;
                }
                pci = value;
            }
        }

        public UC_PCI()
        {
            InitializeComponent();        
        }
        

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                Instrument.LogEnabled = false;
                InitCard();
                panelMux.Controls.Add(new UC_MUX(Instrument));
            }
            catch(IOException ex)
            {
                MessageBox.Show(ex.DecodedMessage, "Error PCI", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }                                
        }

        private void InitCard()
        {
            try
            {
                for (byte i = 0; i < Instrument.DI.Count; i++)
                {
                    InputControl item = new InputControl();
                    item.NumIO = i;
                    Inputs.Add(i, false);
                    ControlInputs.Add(i, item);
                    item.text = InputsName.ContainsKey(i) ? InputsName[i] : i.ToString();
                    InputPanel.Controls.Add(item);
                }

                for (byte i = 0; i < Instrument.DO.Count; i++)
                {
                    OutputControl item = new OutputControl();

                    if (!ControlOutputs.ContainsKey(i))
                        ControlOutputs.Add(i, item);

                    item.IO = Instrument;
                    item.NumIO = i;
                    item.text = OutputsName.ContainsKey(i) ? OutputsName[i] : i.ToString();                   
                    item.outType = typeOut[i];

                    if(!ProhibitedOutputs.Contains(i))
                        panelOutputs.Controls.Add(item);                                       
                }

                m.Inputs = Inputs;
                m.IO = Instrument;
                m.Interval = 1000;
                m.ValueCahnged += new EventHandler<InputsEventArgs>(m_ValueReaded);
                m.Start();

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);                        
            }
        }

        void m_ValueReaded(object sender, InputsEventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke(new ReadInputsDelegate(UpdatePanel), (byte)e.Value, (bool)e.State);
            else
                UpdatePanel((byte)e.Value,(bool)e.State);
        }


        public void PowerSourceChange(object sender, OutputsEventArgs e)
        {          
                this.Invoke(new DesactiveOutDelegate(UpdatePanelLogic), (byte)e.Value, e.State);
        }     

        public void ActivatedDesactivatedOut45(object sender, OutputsEventArgs e)
        {
            this.Invoke(new ActivatedDesactivatedOut45Delegate(UpdatePanelLogic), (byte)e.Value, e.State);
        }


        public void UpdatePanel(byte input, bool state)
        {
            ControlInputs[input].UpdateUI(state);
        }


        public void UpdatePanelLogic(byte output , bool Outenabled = false)
        {
            switch (output)
            {
                case 23:
                    ControlOutputs[output].UpdateUI(true, false, Outenabled);
                    break;  
                case 45:
                    ControlOutputs[output].UpdateUI45(Outenabled);
                    break;
                default:
                    ControlOutputs[output].UpdateUI(true, true, Outenabled);
                    break;
            }
                      
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            m.Stop();

            if(pci != null)
                pci.Dispose();           

            if (ControlOutputs != null)
                ControlOutputs.Clear();
            
            base.Dispose(disposing);
        }

        
#region Dictionaris and Lists

        //Inputs
        private Dictionary<byte, bool> Inputs = new Dictionary<byte, bool>();
        private Dictionary<byte, InputControl> ControlInputs = new Dictionary<byte, InputControl>();

        private Dictionary<byte, String> InputsName = new Dictionary<byte, String>();


       //Outputs
        private static Dictionary<byte, OutputControl> ControlOutputs = new Dictionary<byte, OutputControl>();
        private Dictionary<byte, String> OutputsName = new Dictionary<byte, String>()
        {
            {4,"Electrovalvula General"},
            {5,"Electrovalvula 1"}, 
            {6,"Electrovalvula 2"}, 
            {7,"Electrovalvula 3"}, 
            {8,"Electrovalvula 4"},
            {9,"Electrovalvula 5"}, 
            {10,"Electrovalvula 6"},
            {11,"Electrovalvula 7"},
            {12,"Electrovalvula 8"}, 
            {13,"2 x VChroma"},
            {14,"24 V"},
            {15,"230VAC AUX"}, 
            {16,"Reset torre"}, 
            {23,"Chroma / Lambda"}, 
            {24,"VL1 Fuente Trifásica"},
            {25,"VL2 Fuente Trifásica"}, 
            {26,"VL3 Fuente Trifásica"}, 
            {27,"VLN Fuente Trifásica"},
            {28,"IL1 Fuente Trifásica"},
            {29,"IL2 Fuente Trifásica"},
            {30,"IL3 Fuente Trifásica"},
            {31,"MUX OUT1"},
            {32,"MUX OUT2"},
            {33,"MUX OUT3"},
            {34,"MUX OUT4"},
            {35,"MUX OUT5"},
            {36,"MUX OUT6"},
            {37,"MUX OUT7"}, 
            {38,"MUX OUT8"}, 
            {39,"Medida multímetro I"}, 
            {44,"Medida multímetro V"},
            {45,"12A/120A"}
        };
        private List<byte> ProhibitedOutputs = new List<byte>() { 31, 32, 33, 34, 35, 36, 37, 38 };

        private Dictionary<byte, byte> typeOut = new Dictionary<byte, byte>()
        {
            {0, (byte)OutputControl.OutputType.others},
            {1, (byte)OutputControl.OutputType.others},
            {2, (byte)OutputControl.OutputType.others},
            {3, (byte)OutputControl.OutputType.others},
            {4, (byte)OutputControl.OutputType.valvulas},
            {5, (byte)OutputControl.OutputType.valvulas},
            {6, (byte)OutputControl.OutputType.valvulas},
            {7, (byte)OutputControl.OutputType.valvulas},
            {8, (byte)OutputControl.OutputType.valvulas},
            {9, (byte)OutputControl.OutputType.valvulas},
            {10, (byte)OutputControl.OutputType.valvulas},
            {11, (byte)OutputControl.OutputType.valvulas},
            {12, (byte)OutputControl.OutputType.valvulas},
            
            {13, (byte)OutputControl.OutputType.reles},
            {14, (byte)OutputControl.OutputType.reles},
            {15, (byte)OutputControl.OutputType.reles},
            {16, (byte)OutputControl.OutputType.reles},
            {17, (byte)OutputControl.OutputType.reles},
            {18, (byte)OutputControl.OutputType.reles},
            {19, (byte)OutputControl.OutputType.reles},
            {20, (byte)OutputControl.OutputType.reles},
            {21, (byte)OutputControl.OutputType.reles},
            {22, (byte)OutputControl.OutputType.reles},
            {23, (byte)OutputControl.OutputType.reles},
            {24, (byte)OutputControl.OutputType.reles},
            {25, (byte)OutputControl.OutputType.reles},
            {26, (byte)OutputControl.OutputType.reles},
            {27, (byte)OutputControl.OutputType.reles},
            {28, (byte)OutputControl.OutputType.reles},
            {29, (byte)OutputControl.OutputType.reles},
            {30, (byte)OutputControl.OutputType.reles},

            {31, (byte)OutputControl.OutputType.mux},
            {32, (byte)OutputControl.OutputType.mux},
            {33, (byte)OutputControl.OutputType.mux},
            {34, (byte)OutputControl.OutputType.mux},
            {35, (byte)OutputControl.OutputType.mux},
            {36, (byte)OutputControl.OutputType.mux},
            {37, (byte)OutputControl.OutputType.mux}, 
            {38, (byte)OutputControl.OutputType.mux},
 
            {39, (byte)OutputControl.OutputType.reles},
            {40, (byte)OutputControl.OutputType.others},
            {41, (byte)OutputControl.OutputType.others},
            {42, (byte)OutputControl.OutputType.others},
            {43, (byte)OutputControl.OutputType.others},
            {44, (byte)OutputControl.OutputType.reles},
            {45, (byte)OutputControl.OutputType.reles},
            {46, (byte)OutputControl.OutputType.others},
            {47, (byte)OutputControl.OutputType.others},
            {48, (byte)OutputControl.OutputType.others},
            {49, (byte)OutputControl.OutputType.others},
            {50, (byte)OutputControl.OutputType.others},
            {51, (byte)OutputControl.OutputType.others},
            {52, (byte)OutputControl.OutputType.others},
            {53, (byte)OutputControl.OutputType.others},
            {54, (byte)OutputControl.OutputType.others},
            {55, (byte)OutputControl.OutputType.others},
            {56, (byte)OutputControl.OutputType.others},
            {57, (byte)OutputControl.OutputType.others},
            {58, (byte)OutputControl.OutputType.others},
            {59, (byte)OutputControl.OutputType.others},
            {60, (byte)OutputControl.OutputType.others}
                  
        };


#endregion

    }
    public class PciDescription : ControlDescription
    {
        public override string Name { get { return "PCI7296"; } }
        public override string Category { get { return "In/Out"; } }
        public override Image Icon { get { return Properties.Resources.input; } }
        public override Image IconBig { get { return Properties.Resources.PCI_7296; } }
        public override Type SubcomponentType { get { return typeof(PCI7296); } }
    }
 

}
