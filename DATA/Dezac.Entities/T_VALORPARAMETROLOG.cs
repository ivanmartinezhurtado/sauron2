namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_VALORPARAMETROLOG
    {
        public System.DateTime FECHA { get; set; }
        public int NUMGRUPO { get; set; }
        public int NUMPARAM { get; set; }
        public Nullable<int> NUMPRODUCTO { get; set; }
        public Nullable<int> VERSION { get; set; }
        public string OLDVALOR { get; set; }
        public string NEWVALOR { get; set; }
        public string USUARIO { get; set; }
        public string NOTAS { get; set; }
    }
}
