﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using TaskRunner.Enumerators;
using TaskRunner.Tools;

namespace TaskRunner.Model
{
    public class Step
    {
        [Category("Report")]
        [Description("Orden y nombre del grupo donde los resultados del Step se añadiran a un TestReport si se ha diseñado, (Ejemplo: 01,Verificación)")]
        public string ReportGroupName { get; set; }

        [Category("Step")]
        [ReadOnly(true)]
        [Description("Nombre del Step")]
        public string Name { get; set; }
        [Category("Step")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        [Description("Comentarios que se puden añadir al Step")]
        public string Comments { get; set; }
        [Category("Step")]
        [Description("Para habilitar o deshabilitar el Action que no se ejecute (no recomendable mejor realizarlo por el Checbox de la sequencia de test del editor)")]
        public bool Enabled { get; set; }

        [Category("Run")]
        [Description("Ejecuta este Action solamente en el hilo que corresponda a la instancia del test, solo srive para multiples test en paralelismo")]
        public int? RunOnNumInstance { get; set; }
        [Category("Run")]
        [Description("Ejecuta todos los Actions hijos a la vez (en paralelo)")]
        public bool RunChildStepsInParallel { get; set; }


        [Category("Loop")]
        [Description("Tipo de bucle de repeticiones que se habilitara en el Action")]
        public LoopType LoopType { get; set; }
        [Category("Loop")]
        [Description("Numero de repeticiones que realizara el bucle antes de lanzar la excepcion")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string LoopMaxIterations { get; set; }
        [Category("Loop")]
        [Description("Tiempo en (ms) que pasará entre los reintentos del bucle")]
        public int TimeIntervalLoop { get; set; }
        [Category("Loop")]
        [Description("Expresion lógica booleana que se debe cumplir para seguir ejecutando el loop")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string LoopConditionExpr { get; set; }

        [Category("Condition")]
        [Description("Expresion lógica booleana que se debe cumplir para ejecutar el Action")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string RunConditionExpr { get; set; }

        [Browsable(false)]
        public IActionBase Action { get; set; }

        [Category("Action Behavior")]
        [Description("Acción que realizará el test si falla este Action")]
        public PostActionStep FailAction { get; set; }
        [Category("Action Behavior")]
        [Description("Expresion lógica booleana que al cumplirse da el step como OK y aborta los demas hijos")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string StepOKOnCondition { get; set; }
        [Category("Action Behavior")]
        [Description("Expresion lógica booleana que al cumplirse da el step como KO y aborta los demas hijos")]
        [Editor(typeof(MultilineStringEditor), typeof(UITypeEditor))]
        public string StepKOOnCondition { get; set; }

        [Category("Multi Test Synchronization")]
        [Description("Propiedad para poder sincronizar test en paralelo")]
        public TestParallelismBehaviour TestParallelismBehaviour { get; set; }

        [Browsable(false)]
        public List<Step> Steps { get; private set; } = new List<Step>();
        [JsonIgnore]
        [Browsable(false)]
        public bool BreakPoint { get; set; }
        [Browsable(false)]
        public bool Expanded { get; set; } = true;

        public Step()
        {
            Enabled = true;
            FailAction = PostActionStep.Terminate;
        }

        public bool HasStepName(string stepName)
        {
            foreach (Step child in Steps)
                if (child.Action != null)
                {
                    if (child.Action.Name == stepName)
                        return true;
                    else
                    {
                        if (child.HasStepName(stepName))
                            return true;
                    }
                }

            return false;
        }
    }
}
