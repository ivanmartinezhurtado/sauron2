﻿
using Comunications.Message;
using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using Dezac.Device.Metering.SOAP;
using System.Linq;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.06)]
    public class CompactIECO : CDCTestBase
    {
        private const int PILOTO_ERROR = 48;
        private const int PISTON_GENERAL = 4;
        private const int OFF_ALIMENTACION_CONTADORES =56;
        private const int SALTAR_TRAFOS = 55;

        private string Camera123 = "4102856937" ;
        private string Camera456 = "4102855389";
        private string Camera789 = "4102857525";

        public void TestInitialization()
        {
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.ActiveElectroValvule();

            //******************************************
            tower.IO.DO.OffWait(1000, PILOTO_ERROR, OFF_ALIMENTACION_CONTADORES, SALTAR_TRAFOS);

            has3G = Configuracion.GetString("HAS_MODULE_MODEM", "NO").Trim().ToUpper() == "SI" ? "3G_" : "";
            onlySBT = true;
            DoubleSecondary = !onlySBT;
            InstanceNumber = 1;
            SBT_NUMBER = 1;
            PULSADOR_RESET = 9;

            Camera123 = GetVariable<string>("Camera123", Camera123);
            Camera456 = GetVariable<string>("Camera456", Camera456);
            Camera789 = GetVariable<string>("Camera789", Camera789);

            PositionCameraDictionary = new Dictionary<int, string>();
            PositionCameraDictionary.Add(1, Camera123);
            PositionCameraDictionary.Add(2, Camera123);
            PositionCameraDictionary.Add(3, Camera123);
            PositionCameraDictionary.Add(4, Camera456);
            PositionCameraDictionary.Add(5, Camera456);
            PositionCameraDictionary.Add(6, Camera456);
        }

        public void TestConsumption()
        {
            List<TriAdjustValueDef> consumptionValues = new List<TriAdjustValueDef>();

            consumptionValues.Add(new TriAdjustValueDef(Params.W.L1.EnVacio.Name, Params.W.Null.EnVacio.Min(), Params.W.Null.EnVacio.Max(), 0, 0, ParamUnidad.W));
            consumptionValues.Add(new TriAdjustValueDef(Params.VAR.L1.EnVacio.Name, Params.VAR.Null.EnVacio.Min(), Params.VAR.Null.EnVacio.Max(), 0, 0, ParamUnidad.Var));

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(230, 0, 50, 0, TriLineValue.Create(0, 120, 240));

            TestMeasureBase(consumptionValues, (step) =>
            {
                var cvmMteReadings = tower.CvmminiMTE.ReadAllVariables().Phases;

                return new double[] { cvmMteReadings.L1.PotenciaActiva, cvmMteReadings.L1.PotenciaReactiva,
                                      cvmMteReadings.L2.PotenciaActiva, cvmMteReadings.L2.PotenciaReactiva,
                                      cvmMteReadings.L3.PotenciaActiva, cvmMteReadings.L3.PotenciaReactiva};
            }, 2, 5, 1000, null, true);
        }

        public void TestSpectrum(byte portSpectrum = 0)
        {     
            var positionBandaPLCInit = (int)Configuracion.GetDouble("POSITION_INIT_FREQ_MAX_BANDA_PLC", 86, ParamUnidad.SinUnidad);
            var positionBandaPLCFin = (int)Configuracion.GetDouble("POSITION_END_FREQ_MAX_BANDA_PLC", 182, ParamUnidad.SinUnidad);
            var positionBandaPLCFueraIzquierdaFin = (int)Configuracion.GetDouble("POSITION_END_FREQ_OUT_LEFT_MAX_BANDA_PLC", 60, ParamUnidad.SinUnidad);
            var positionBandaPLCFueraDerechaInit = (int)Configuracion.GetDouble("POSITION_INIT_FREQ_OUT_RIGHT_MAX_BANDA_PLC", 200, ParamUnidad.SinUnidad);

            var margenesMaxBandaPLC = new AdjustValueDef(Params.VALOR.Other("MAX_BANDA_PLC").Name, 0, Params.VALOR.Other("MAX_BANDA_PLC").TestPoint("").Min(), Params.VALOR.Other("MAX_BANDA_PLC").TestPoint("").Max(), 0, 0, ParamUnidad.SinUnidad);
            var margenesMaxFueraBandaLC = new AdjustValueDef(Params.VALOR.Other("MAX_OUT_BANDA_PLC").Name, 0, Params.VALOR.Other("MAX_OUT_BANDA_PLC").TestPoint("").Min(), Params.VALOR.Other("MAX_OUT_BANDA_PLC").TestPoint("").Max(), 0, 0, ParamUnidad.SinUnidad);

            var spectrum = cdc.Soap.ReadPlacaSpectrum();
            var MaxBetweenValues = spectrum.Max.GetRange(positionBandaPLCInit, positionBandaPLCFin - positionBandaPLCInit); //86 - 182
            var MaxValuesBefore   = spectrum.Max.GetRange(1, positionBandaPLCFueraIzquierdaFin); // 0- 60
            var MaxValuesAfter = spectrum.Max.GetRange(positionBandaPLCFueraDerechaInit, Math.Abs(spectrum.Max.Count - positionBandaPLCFueraDerechaInit)); // 200 -- final

            foreach (int value in MaxBetweenValues)
            {
                margenesMaxBandaPLC.Value = value;
                if (!margenesMaxBandaPLC.IsValid())
                    Error().UUT.MEDIDA.MARGENES(margenesMaxBandaPLC.Name).Throw();
            }
            foreach (int value in MaxValuesAfter)
            {
                margenesMaxFueraBandaLC.Value = value;
                if (!margenesMaxFueraBandaLC.IsValid())
                    Error().UUT.MEDIDA.MARGENES(margenesMaxFueraBandaLC.Name + " LEFT").Throw();
            }
            foreach (int value in MaxValuesBefore)
            {
                margenesMaxFueraBandaLC.Value = value;
                if (!margenesMaxFueraBandaLC.IsValid())
                    Error().UUT.MEDIDA.MARGENES(margenesMaxFueraBandaLC.Name + " RIGHT").Throw();
            }

            var margenesAverage = new AdjustValueDef(Params.VALOR.Other("AVERAGE").Name, 0, Params.VALOR.Other("AVERAGE").TestPoint("").Min(), Params.VALOR.Other("AVERAGE").TestPoint("").Max(), 0, 0, ParamUnidad.SinUnidad);

            var Average = spectrum.Average;

            foreach (int value in Average)
            {
                margenesAverage.Value = value;
                if (!margenesAverage.IsValid())
                    Error().UUT.MEDIDA.MARGENES(margenesAverage.Name).Throw();
            }

            if (portSpectrum == 0)
                portSpectrum = Comunicaciones.SerialPortModulo;

            var serialPort = new SerialPort("COM" + portSpectrum, 115200, Parity.None, 8, StopBits.One);
            serialPort.DtrEnable = false;
            serialPort.RtsEnable = false;
            serialPort.Handshake = Handshake.XOnXOff;
            var spDebug = new SerialPortAdapter(serialPort, "\n");
            spDebug.ReadTimeout = 500;
            spDebug.WriteTimeout = 500;

            var user = this.WriteAndReadText((SerialPortAdapter)spDebug, 100, 10, 10, 10, "root", "root");
            if (string.IsNullOrEmpty(user))
                throw new Exception("Error no existe el proceso de la placa spetcrum activado");

            Delay(1000, "");
            CheckCancellationRequested();

            spDebug.WriteLineString("j7awg54q");
            CheckCancellationRequested();

            var serviceRun = this.WriteAndReadText((SerialPortAdapter)spDebug, 200, 1000, 150, 35, "ps", @"/dev/ttyO1");
            CheckCancellationRequested();
            if (string.IsNullOrEmpty(serviceRun))
                throw new Exception("Error no existe el proceso de la placa spetcrum activado");
        }

        public override void TestConfigurationCPU()
        {
            base.TestConfigurationCPU();

            //Enviamos contraseña de loa ultimos digitos del numero de serie
            var passwordAdm = TestInfo.NumSerie.Substring(TestInfo.NumSerie.Length - 5);

            Resultado.Set("PASSWORD_WEB", passwordAdm, ParamUnidad.SinUnidad);

            cdc.Soap.WritePasswordAccesAdm(passwordAdm);
        }

        public void TestPLCfromMeter()
        {
            //Enviamos contraseña para la encriptacion PLC de los contadores
            cdc.Soap.WriteKeyEncryptationMeters("CIR0000000001");
            Delay(500, "");
            cdc.Soap.WriteKeyEncryptationMeters("CIR0000000002");
            Delay(500, "");
            cdc.Soap.WriteKeyEncryptationMeters("CIR0000000003");
            Delay(500, "");

            internalPLCfromMeterTest("L1", 15, "CIR0000000001");
            internalPLCfromMeterTest("L2", 1, 15, 30, true, "CIR0000000002");
            internalPLCfromMeterTest("L3", 1, 15, 30, true, "CIR0000000003");
        }

        private void internalPLCfromMeterTest(string fase, byte numSBT, byte retries, int timeDetectionSeg, bool noDetection, string METER_ID)
        {
            SamplerWithCancel((p) =>
            {
                cdc.DeleteSnifferFile();
                return true;
            }, "Error al limpiar el fichero sniffer.csv por SSH", 2, 2000, 0, false, false);

            Delay(1000, "");
            var timeCurrentStart = DateTime.Now;
            var typePLC = numSBT == 1 ? "" : "_S";

            SamplerWithCancel((p) =>
            {
                var diffCurrentTime = DateTime.Now.Subtract(timeCurrentStart).TotalSeconds;
                logger.InfoFormat("Tiempo en (s) transcurrido: {0}", diffCurrentTime);

                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);
                var meter = device.Where((r) => r.IsSBT == false && r.MeterID == METER_ID).FirstOrDefault();

                if (meter != null)
                {
                    Resultado.Set(METER_ID, "DETECTED", ParamUnidad.SinUnidad);
                    return true;
                }

                return false;

            }, "", retries, 5000, 5000, false, false);
        }

        private void internalPLCfromMeterTest(string fase, byte retries, string METER_ID, bool PLC_S = false)
        {
            SamplerWithCancel((p) =>
            {
                cdc.DeleteSnifferFile();
                return true;
            }, "Error al limpiar el fichero sniffer.csv por SSH", 2, 2000, 0, false, false);

            SamplerWithCancel((p) =>
            {
                logger.InfoFormat("Deteccion del contador en la fase {0} reintento {1}", fase, retries);

                var device = cdc.Soap.ReadDevice(CompactDctSoapService.ComStatus.Conectado);
                var meter = device.Where((r) => r.IsSBT == false && r.MeterID == METER_ID).FirstOrDefault();
                if (meter == null)
                    throw new Exception(string.Format("Error. no se ha detectado un contador conectado al concentrador en la fase {0}", fase));

                Resultado.Set(METER_ID, "DETECTED", ParamUnidad.SinUnidad);
                return true;

            }, "", retries, 5000, 5000, false);
        }

        public override void DeleteNodesRestart()
        {
            base.DeleteNodesRestart();

            tower.IO.DO.On(OFF_ALIMENTACION_CONTADORES);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            Delay(4000, "Espera de equipo se apaque del todo");

            tower.PowerSourceIII.ApplyAndWaitStabilisation();

            if (ZonaHoraria == "NO")
                dateTimeLocalRestart = DateTime.Now;
            else
            {
                var timeZoneInfo = this.TimeZoneToTimeZoneInfo(ZonaHoraria);
                dateTimeLocalRestart = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, timeZoneInfo.Id);
            }
        }

        public override void TestConfigVerification()
        {
            base.TestConfigVerification();

            SetVariable("ConfigFile", cdc.Soap.ReadConfigurationSaveFile());
        }

        public override void TestFinish()
        {
            if (tower != null)
            {
                tower.ShutdownSources();
                if (tower.IO != null)
                {
                    if (string.IsNullOrEmpty(TestInfo.NumSerie))
                        tower.IO.DO.On(PILOTO_ERROR);

                    Delay(1500, "Espera de equipo malo");

                    tower.IO.DO.Off(PISTON_GENERAL);
                }
            }
            base.TestFinish();
        }
    }
}
