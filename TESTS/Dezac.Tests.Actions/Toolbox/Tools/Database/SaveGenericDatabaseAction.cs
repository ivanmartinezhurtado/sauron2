﻿using Dezac.Data;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(SaveGenericDatabaseActionDescription))]
    public class SaveGenericDatabaseAction : ActionBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("SaveGenericDatabaseAction");

        public enum DatabaseTypeEnum
        {
            SQLServer,
            MySql,
            SqLite,
            Oracle,
            Oledb,
            Csv
        }

        [Category("3. Configuration")]
        [DefaultValue("Test")]
        [Description("Nombre de la tabla del test")]
        public string TestTableName { get; set; }
        
        [Category("3. Configuration")]
        [Description("Campos del test")]
        public List<FieldMap> TestFields { get; set; }

        [Category("4. Results")]
        [DefaultValue("TestResults")]
        [Description("Resultados de la tabla del test (o el fichero si es CSV)")]
        public string TestResultsTableName { get; set; }
        
        [Category("4. Results")]
        [Description("Resultados de los campos del test a guardar")]
        public List<FieldMap> TestResultsFields { get; set; }
        
        [DefaultValue(true)]
        [Category("4. Results")]
        [Description("Guarda los resultados del test en la base de datos")]
        public bool SaveResults { get; set; }

        [Category("3.1 Configuration Data")]
        [DefaultValue("TestVariables")]
        [Description("Variables de la tabla del test (o nombre del fichero si es CSV)")]
        public string TestVariablesTableName { get; set; }
        
        [Category("3.1 Configuration Data")]
        [Description("Campos de las variables del test que han de ser guardadas")]
        public List<FieldMap> TestVariablesFields { get; set; }
        
        [DefaultValue(true)]
        [Category("3.1 Configuration Data")]
        [Description("Guarda las variables del test en la base de datos")]
        public bool SaveVariables { get; set; }

        [DefaultValue("TestSteps")]
        [Category("3.2 Configuration Steps")]
        [Description("Pasos de la tabla del test (o nombre del fichero si es CSV)")]
        public string TestStepsTableName { get; set; }
        
        [Category("3.2 Configuration Steps")]
        [Description("Pasos del test que han de ser guardados")]
        public List<FieldMap> TestStepsFields { get; set; }
        
        [DefaultValue(true)]
        [Category("3.2 Configuration Steps")]
        [Description("Guarda los pasos del test en la base de datos")]
        public bool SaveSteps { get; set; }

        [Category("3.3 Database Provider")]
        [DefaultValue(DatabaseTypeEnum.SQLServer)]
        [Description("Tipo de base de datos")]
        public DatabaseTypeEnum DatabaseType { get; set; }
        
        [Category("3.3 Database Provider")]
        [Description("Camino para guardar los ficheros si se usa CSV o cadena de connexión completa de la base de datos")]
        public string ConnectionString { get; set; }

        public SaveGenericDatabaseAction()
            : this(true)
        {
        }

        [JsonConstructor]
        public SaveGenericDatabaseAction(bool initDefaultFields)
        {
            if (initDefaultFields)
                InitDefaultFields();
        }

        private void InitDefaultFields()
        {
            TestFields = new List<FieldMap>
            {
                new FieldMap { Name = "Id", DataType = DbType.Int32, Identity = true },
                new FieldMap { Name = "Guid" },
                new FieldMap { Name = "StartDate", DataType = DbType.DateTime },
                new FieldMap { Name = "EndDate", DataType = DbType.DateTime },
                new FieldMap { Name = "Result" },
            };

            TestResultsFields = new List<FieldMap>
            {
                new FieldMap { Name = "Id", DataType = DbType.Int32, Identity = true },
                new FieldMap { Name = "TestId" },
                new FieldMap { Name = "Name" },
                new FieldMap { Name = "Value" },
                new FieldMap { Name = "Min" },
                new FieldMap { Name = "Max" },
                new FieldMap { Name = "Unit" },
                new FieldMap { Name = "StepName" }
            };

            TestStepsFields = new List<FieldMap>
            {
                new FieldMap { Name = "Id", DataType = DbType.Int32, Identity = true },
                new FieldMap { Name = "TestId" },
                new FieldMap { Name = "Name" },
                new FieldMap { Name = "StartDate", DataType = DbType.DateTime },
                new FieldMap { Name = "EndDate", DataType = DbType.DateTime },
                new FieldMap { Name = "Result" },
                new FieldMap { Name = "Error" },
            };

            TestVariablesFields = new List<FieldMap>
            {
                new FieldMap { Name = "Id", DataType = DbType.Int32, Identity = true },
                new FieldMap { Name = "Name" },
                new FieldMap { Name = "Value" },
                new FieldMap { Name = "Min" },
                new FieldMap { Name = "Max" },
                new FieldMap { Name = "Unit" },
                new FieldMap { Name = "StepName" }
            };
        }

        public override void Execute()
        {
            if (DatabaseType != DatabaseTypeEnum.Csv)
                ExecuteDb();
            else
                ExecuteCsv();
        }

        private void ExecuteDb()
        {
            var context = SequenceContext.Current;

            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"]?.ConnectionString ?? ConnectionString;

            DbProviderFactory db = DbProviderFactories.GetFactory(GetProviderName());

            using (var cn = db.CreateConnection())
            {
                cn.ConnectionString = connectionString;
                cn.Open();

                object identity = null;

                using (var cmd = cn.CreateCommand())
                {
                    ConfigureCommand(cmd, TestTableName, TestFields, InternalResolveValue);
                    cmd.ExecuteNonQuery();

                    if (TestFields.Any(p => p.Identity))
                    {
                        if (DatabaseType == DatabaseTypeEnum.SqLite)
                            cmd.CommandText = "SELECT last_insert_rowid();";
                        else
                            cmd.CommandText = "SELECT @@IDENTITY";

                        identity = cmd.ExecuteScalar();
                    }
                }

                logger.InfoFormat("Saved Test Id: {0}", identity ?? context.Guid);

                if (SaveResults)
                    using (var cmd = cn.CreateCommand())
                    {
                        ConfigureCommand(cmd, TestResultsTableName, TestResultsFields, (f) => DBNull.Value);

                        context.ResultList
                            .Where(p => p.Name != null)
                            .ToList()
                            .ForEach(r =>
                            {
                                var p = r.Value as ParamValue;

                                foreach (var f in TestResultsFields)
                                {
                                    if (f.Identity)
                                        continue;

                                    object value = DBNull.Value;

                                    var name = string.IsNullOrEmpty(f.Value) ? f.Name : f.Value;

                                    switch (name.ToLower())
                                    {
                                        case "testid":
                                            value = identity;
                                            break;
                                        case "guid":
                                            value = context.Guid;
                                            break;
                                        case "name":
                                            value = r.Name;
                                            break;
                                        case "value":
                                            value = p != null ? p.Valor : r.Value != null ? r.Value.ToString() : null;
                                            break;
                                        case "stepname":
                                            value = p?.StepName;
                                            break;
                                        case "min":
                                            value = p?.Min;
                                            break;
                                        case "max":
                                            value = p?.Max;
                                            break;
                                        case "unit":
                                            value = p?.Unidad;
                                            break;
                                        case "reportgroup":
                                            value = r.Step.ReportGroupName;
                                            break;

                                    }

                                    cmd.Parameters["@" + f.Name].Value = value ?? DBNull.Value;
                                }

                                cmd.ExecuteNonQuery();
                            });
                    }

                if (SaveSteps)
                    using (var cmd = cn.CreateCommand())
                    {
                        ConfigureCommand(cmd, TestStepsTableName, TestStepsFields, (f) => DBNull.Value);

                        context.GetSteps(p => p.Step.Steps.Count == 0 || p.Step.Action is NewTestFaseContextAction, p => !(p.Step.Action is NewTestFaseContextAction))
                            .ToList()
                            .ForEach(p =>
                            {
                                foreach (var f in TestStepsFields)
                                {
                                    if (f.Identity)
                                        continue;

                                    object value = DBNull.Value;

                                    var name = string.IsNullOrEmpty(f.Value) ? f.Name : f.Value;

                                    switch (name.ToLower())
                                    {
                                        case "testid":
                                            value = identity;
                                            break;
                                        case "guid":
                                            value = context.Guid;
                                            break;
                                        case "name":
                                            value = p.Name;
                                            break;
                                        case "startdate":
                                            value = p.StartTime;
                                            break;
                                        case "enddate":
                                            value = p.EndTime == DateTime.MinValue ? DateTime.Now : p.EndTime;
                                            break;
                                        case "result":
                                            value = p.ExecutionResult.ToString("G");
                                            break;
                                        case "error":
                                            if (p.Exception != null)
                                                value = p.GetErrorMessage().Left(1000);
                                            break;
                                    }

                                    cmd.Parameters["@" + f.Name].Value = value ?? DBNull.Value;
                                }

                                cmd.ExecuteNonQuery();
                            });
                    }
            }
        }

        private void ExecuteCsv()
        {
            var identity = DateTime.Now.ToString("yyMMdd_HHmmss");
            var context = SequenceContext.Current;

            var header = new StringBuilder();
            var values = new StringBuilder();

            foreach (var f in TestFields)
            {
                if (f.Identity)
                    continue;

                header.AppendFormat("\"{0}\";", f.Name ?? f.Value);
                values.AppendFormat("{0};", InternalResolveValue(f));
            }

            SaveFile(TestTableName, identity, header, values);

            if (SaveResults)
            {
                header.Clear();
                values.Clear();

                TestResultsFields.Where(p => !p.Identity).ToList().ForEach(f => header.AppendFormat("\"{0}\";", f.Name ?? f.Value));

                context.ResultList
                    .Where(p => p.Name != null)
                    .ToList()
                    .ForEach(r =>
                    {
                        var p = r.Value as ParamValue;

                        foreach (var f in TestResultsFields)
                        {
                            if (f.Identity)
                                continue;

                            object value = null;

                            var name = string.IsNullOrEmpty(f.Value) ? f.Name : f.Value;

                            switch (name.ToLower())
                            {
                                case "testid":
                                    value = identity;
                                    break;
                                case "guid":
                                    value = context.Guid;
                                    break;
                                case "name":
                                    value = r.Name;
                                    break;
                                case "value":
                                    value = p != null ? p.Valor : r.Value != null ? r.Value.ToString() : null;
                                    break;
                                case "stepname":
                                    value = p?.StepName;
                                    break;
                                case "min":
                                    value = p?.Min;
                                    break;
                                case "max":
                                    value = p?.Max;
                                    break;
                                case "unit":
                                    value = p?.Unidad;
                                    break;
                            }

                            values.AppendFormat("{0};", value);
                        }

                        values.AppendLine();
                    });

                SaveFile(TestResultsTableName, identity, header, values);
            }

            if (SaveVariables)
            {
                header.Clear();
                values.Clear();

                TestVariablesFields.Where(p => !p.Identity).ToList().ForEach(f => header.AppendFormat("\"{0}\";", f.Name ?? f.Value));

                context.Variables.VariableList
                    .Where(p => p.Key != null)
                    .ToList()
                    .ForEach(r =>
                    {
                        var p = r.Value as ParamValue;

                        foreach (var f in TestVariablesFields)
                        {
                            if (f.Identity)
                                continue;

                            object value = null;

                            var name = string.IsNullOrEmpty(f.Value) ? f.Name : f.Value;

                            switch (name.ToLower())
                            {
                                case "testid":
                                    value = identity;
                                    break;
                                case "guid":
                                    value = context.Guid;
                                    break;
                                case "name":
                                    value = r.Key;
                                    break;
                                case "value":
                                    value = p != null ? p.Valor : r.Value != null ? r.Value.ToString() : null;
                                    break;
                                case "stepname":
                                    value = p?.StepName;
                                    break;
                                case "min":
                                    value = p?.Min;
                                    break;
                                case "max":
                                    value = p?.Max;
                                    break;
                                case "unit":
                                    value = p?.Unidad;
                                    break;
                            }

                            values.AppendFormat("{0};", value);
                        }

                        values.AppendLine();
                    });

                SaveFile(TestVariablesTableName, identity, header, values);
            }

            if (SaveSteps)
            {
                header.Clear();
                values.Clear();

                TestStepsFields.Where(p => !p.Identity).ToList().ForEach(f => header.AppendFormat("\"{0}\";", f.Name ?? f.Value));

                context.GetSteps(p => p.Step.Steps.Count == 0 || p.Step.Action is NewTestFaseContextAction, p => !(p.Step.Action is NewTestFaseContextAction))
                    .ToList()
                    .ForEach(p =>
                    {
                        foreach (var f in TestStepsFields)
                        {
                            if (f.Identity)
                                continue;

                            object value = null;

                            var name = string.IsNullOrEmpty(f.Value) ? f.Name : f.Value;

                            switch (name.ToLower())
                            {
                                case "testid":
                                    value = identity;
                                    break;
                                case "guid":
                                    value = context.Guid;
                                    break;
                                case "name":
                                    value = p.Name;
                                    break;
                                case "startdate":
                                    value = p.StartTime;
                                    break;
                                case "enddate":
                                    value = p.EndTime;
                                    break;
                                case "result":
                                    value = p.ExecutionResult.ToString("G");
                                    break;
                                case "error":
                                    if (p.Exception != null)
                                        value = p.GetErrorMessage().Left(1000);
                                    break;
                            }

                            values.AppendFormat("{0};", value);
                        }

                        values.AppendLine();
                    });

                SaveFile(TestStepsTableName, identity, header, values);
            }
        }

        private string GetProviderName()
        {
            string providerName;

            switch (DatabaseType)
            {
                case DatabaseTypeEnum.Oracle:
                    providerName = "System.Data.OracleClient";
                    break;
                case DatabaseTypeEnum.MySql:
                    providerName = "System.Data.MySql";
                    break;
                case DatabaseTypeEnum.SqLite:
                    providerName = "System.Data.SqLite";
                    break;
                case DatabaseTypeEnum.Oledb:
                    providerName = "System.Data.OleDb";
                    break;
                default:
                    providerName = "System.Data.SqlClient";
                    break;
            }

            return providerName;
        }

        public string ConfigureCommand(DbCommand cmd, string tableName, List<FieldMap> tableFields, Func<FieldMap, object> ResolveValue)
        {
            var fields = string.Empty;

            foreach (var f in tableFields)
            {
                if (f.Identity)
                    continue;

                fields += $", [{f.Name ?? f.Value}]";

                cmd.AddParameter($"@{f.Name}", f.DataType ?? DbType.String, ResolveValue(f));
            }

            fields = fields.Substring(1);

            cmd.CommandText = $"INSERT INTO [{tableName}] ( {fields} ) VALUES ( {fields.Replace("[", "@").Replace("]", "")} )";

            return fields;
        }

        public object InternalResolveValue(FieldMap field)
        {
            var name = string.IsNullOrEmpty(field.Value) ? field.Name : field.Value;

            switch (name.ToLower())
            {
                case "guid":
                    return SequenceContext.Current.Guid.ToString();
                case "startdate":
                    return SequenceContext.Current.TestResult.StartTime;
                case "enddate":
                    return DateTime.Now;
                case "result":
                    return SequenceContext.Current.ExecutionResult == TestExecutionResult.Completed ? "OK" : "KO";
            }

            return VariablesTools.ResolveValue(name, null, null, true, false) ?? DBNull.Value;
        }

        private void SaveFile(string name, string sufix, StringBuilder headers, StringBuilder values)
        {
            var fileName = Path.Combine(ConnectionString, $"{name}_{sufix}.csv");

            File.WriteAllText(fileName, headers.ToString() + Environment.NewLine + values.ToString());
        }
    }

    public class FieldMap
    {
        public string Name { get; set; }
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Value { get; set; }
        public DbType? DataType { get; set; }
        public bool Identity { get; set; }
    }
    
    public class SaveGenericDatabaseActionDescription : ActionDescription
    {
        public override string Name { get { return "SaveGenericDatabase"; } }
        public override string Category { get { return "Tools/Database"; } }
        public override string Description { get { return "Action que sirve para guardar genéricamente en la base de datos"; } }
        public override Image Icon { get { return Properties.Resources.database_save_icon; } }
    }
}