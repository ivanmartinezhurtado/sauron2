﻿using Gurux.Common;
using Gurux.DLMS.Secure;
using System.Collections.Generic;
using System.Diagnostics;

namespace Dezac.DLMS
{
    public class DLMSSettings
    {
        public IGXMedia media { get; set; }
        public GXDLMSSecureClient client { get; set; }
        public TraceLevel trace = TraceLevel.Verbose;
        public bool iec = false;
        //Objects to read.
        public List<KeyValuePair<string, int>> readObjects = new List<KeyValuePair<string, int>>();
    }
}
