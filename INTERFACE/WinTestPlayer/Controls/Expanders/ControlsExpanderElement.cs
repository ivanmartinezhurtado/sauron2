﻿using System;
using Telerik.WinControls.UI;
using Telerik.WinControls.Layouts;


namespace WinTestPlayer.Controls.Expanders
{
    public class ControlsExpanderElement : ExpanderElement
    {
        RadListElement list;
        DockLayoutPanel staticContentHolder;

        public RadListElement ControlsList
        {
            get
            {
                return list;
            }
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            list = new RadListElement();
            list.ItemHeight = 40;
            list.EnableKineticScrolling = true;
            list.Scroller.ScrollState = ScrollState.AlwaysHide;
            list.CreatingVisualItem += list_CreatingVisualItem;

            slidingColumn.Children.Add(list);

            staticContentHolder = new DockLayoutPanel();

            RadButtonElement upButton = new RadRepeatButtonElement();
            upButton.Click += new EventHandler(UpButton_Click);
            staticContentHolder.Children.Add(upButton);
            upButton.ThemeRole = "UpButton";

            RadButtonElement downButton = new RadRepeatButtonElement();
            downButton.Click += new EventHandler(DownButton_Click);
            staticContentHolder.Children.Add(downButton);
            downButton.ThemeRole = "DownButton";

            var scrollButtonsHolder = new StackLayoutPanel();
            scrollButtonsHolder.Orientation = System.Windows.Forms.Orientation.Vertical;
            scrollButtonsHolder.Children.Add(upButton);
            scrollButtonsHolder.Children.Add(downButton);
            staticContentHolder.Children.Add(scrollButtonsHolder);

            var label = new LightVisualElement();
            label.Text = "MENÚ";
            label.ThemeRole = "StaticColumnTitle";
            staticContentHolder.Children.Add(label);

            chevronAndContentHolder.Children.Add(staticContentHolder);
            DockLayoutPanel.SetDock(scrollButtonsHolder, Dock.Bottom);
        }

        void list_CreatingVisualItem(object sender, CreatingVisualListItemEventArgs args)
        {
            args.VisualItem = new RadListVisualItem(); // DemoAppVisualListElement();
        }

        public void DoScrollList(bool scrollUp)
        {
            if (scrollUp)
            {
                if (list.VScrollBar.Value >= list.ItemHeight)
                    list.VScrollBar.Value -= list.ItemHeight;
                else
                    list.VScrollBar.Value = 0;
            }
            else
            {
                if (list.VScrollBar.Value < list.VScrollBar.Maximum - list.VScrollBar.LargeChange)
                {
                    int p = list.VScrollBar.Value + list.ItemHeight;
                    p = Math.Min(p, list.VScrollBar.Maximum - list.VScrollBar.LargeChange);
                    list.VScrollBar.Value = p;
                }
            }
        }

        void UpButton_Click(object sender, EventArgs e)
        {
            DoScrollList(true);
        }

        void DownButton_Click(object sender, EventArgs e)
        {
            DoScrollList(false);
        }
    }
}
