namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductoFase")]
    public partial class ProductoFase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("Producto")]
        [Column(Order = 0)]
        public int ProductoId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Secuencia { get; set; }

        [Required]
        [StringLength(10)]
        [ForeignKey("Fase")]
        public string FaseId { get; set; }

        public decimal? TiempoTeorico { get; set; }
        public decimal? TiempoStandard { get; set; }
        public decimal? TiempoPreparacion { get; set; }
        public string Notas { get; set; }

        public bool FaseTest { get; set; }

        public Fase Fase { get; set; }
        public Producto Producto { get; set; }
    }
}
