﻿using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(SearchStringInVariableDescription))]
    public class SearchStringInVariable : ActionBase
    {
        [Description("Nombre de la variable a buscar")]
        [Category("3. Configuration")]
        public ActionVariable DataInput { get; set; } = new ActionVariable();

        [Description("Texto a encontrar cuando empeza por ...")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToSearchStartWith { get; set; }

        [Description("Texto a encontrar caundo acaba por ...")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToSearchEndWith { get; set; }

        [Description("Cantidad caracteres a obtener después de encontrar el texto buscado")]
        [Category("3. Configuration")]
        [DefaultValue(10)]
        public int CharactersQuantity { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if(DataOutput.IsKeyNullOrEmpty)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataOutput").Throw();

            if (DataInput.IsKeyNullOrEmpty)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DataInput").Throw();

            var value = DataInput.Value.ToString();

            var indexStart = 0;
            var indexEnd = 0;
            var lenghtEnd = CharactersQuantity;

            if (!string.IsNullOrEmpty(value))
            {
                if (!String.IsNullOrEmpty(TextToSearchStartWith))
                    indexStart = value.IndexOf(TextToSearchStartWith);

                if (!String.IsNullOrEmpty(TextToSearchEndWith))
                {
                    indexEnd = value.IndexOf(TextToSearchEndWith);
                    lenghtEnd = indexEnd - indexStart;
                }
           
                var result = value.Substring(indexStart, lenghtEnd);

                result = result.Replace(TextToSearchStartWith, "").Replace(TextToSearchEndWith, "").Trim();

                Logger.InfoFormat("texto encontrado: {0}", result);

                DataOutput.Value = result;
            }
        }

    }


    public class SearchStringInVariableDescription : ActionDescription
    {
        public override string Description { get { return "Action para buscar el texto de una variable que contiene un string";  } }
        public override string Name { get { return "SearchStringInVariable"; }}
        public override string Category { get { return "Actions/Variables/Query"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
