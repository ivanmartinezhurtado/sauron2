﻿using Dezac.Device;
using Dezac.Tests.Model;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ReadFloatRegisterDescription))]
    public class ReadFloatRegister : ActionBase
    {
        [Description("Dreiccion Modbus en decimal")]
        [Category("Configuracion")]
        public ushort Address { get; set; }

        [Description("Tipo registro Modbus HoldingRegister (03) InputRegister (04)")]
        [Category("Configuracion")]
        [DefaultValue(TipoRegisterModbus.HoldingRegister)]
        public TipoRegisterModbus TipoRegistro { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("Instancia")]
        public string DeviceName { get; set; }

        [Description("Nombre de la variable donde guardar el valor")]
        [Category("Resultado")]
        public string OutputVarName { get; set; }

        [Category("Resultados")]
        [Description("Unidades con las que trabaja el valor a obtener")]
        public ParamUnidad OutputUnidades { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDevice;

            var result = TipoRegistro == TipoRegisterModbus.InputRegister ? uut.ReadFloat32(Address) : uut.ReadFloat32HoldingRegister(Address);

            if (string.IsNullOrEmpty(OutputVarName))
                OutputVarName = Address.ToString();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (string.IsNullOrEmpty(OutputVarName))
                OutputVarName = Address.ToString();

            if (testBase != null)
                testBase.Resultado.Set(OutputVarName, result.ToString(), OutputUnidades);
            else
                Context.ResultList.Add(string.Format("{0}={1}", OutputVarName, result));

            Context.Variables.AddOrUpdate(OutputVarName, result);
        }
    }


    public class ReadFloatRegisterDescription : ActionDescription
    {
        public override string Name { get { return "Read Float Register"; } }
        public override string Description { get { return "Lee un registros de modbus en formato Float"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitModbusDeviceAction;DisposedModbusDeviceAction:2"; } }
    }


}
