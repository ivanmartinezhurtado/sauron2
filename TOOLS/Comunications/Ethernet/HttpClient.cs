﻿using log4net;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Comunications.Ethernet
{
    public static class ComunicationHttpClient
    {
        public static readonly ILog _logger = LogManager.GetLogger("ComunicationHttpClient");

        public static string HttpPutContent(string host, string inUrl, string stringContent = "")
        {
            var url = string.Format("http://{0}/{1}", host, inUrl);

            bool done = false;
            var client = new HttpClient();
            var cts = new CancellationTokenSource();
            Exception expectedException = null;
            var respValue = "";

            _logger.InfoFormat("HttpPutContent ->{0}", url);
            var backgroundRequest = new TaskFactory().StartNew(async () =>
            {
                try
                {
                    client.Timeout = new TimeSpan(0, 0, 10);
                    _logger.InfoFormat("HttpClient -> TimeOut 10 seg");
                   // client.DefaultRequestHeaders.ExpectContinue = false;
                    _logger.InfoFormat("HttpClient -> client.PutAsync");
                    var resp = await client.PutAsync(url, new StringContent(stringContent), cts.Token);
                    _logger.InfoFormat("HttpClient -> resp -> {0}", resp);
                    _logger.InfoFormat("HttpClient -> resp.EnsureSuccessStatusCode");
                    resp.EnsureSuccessStatusCode();
                    _logger.InfoFormat("HttpClient -> resp.resp.Content.ReadAsStringAsync()");
                    respValue = await resp.Content.ReadAsStringAsync();

                    _logger.InfoFormat("HttpClient -> resp Xml = {0}", respValue);
                    done = true;
                }
                catch (TaskCanceledException ex)
                {
                    _logger.WarnFormat("HttpPutContent -> Exception {0}", ex.Message);
                    expectedException = ex;
                }
            }, cts.Token);

            // Wait for it to finish
            _logger.InfoFormat("HttpPutContent -> Wait Sleep 10 seg.");
            Thread.Sleep(10000);

            if (!done)
            {
                _logger.WarnFormat("HttpPutContent -> done = false cts.Cancel");
                _logger.WarnFormat("HttpPutContent -> Error");
                cts.Cancel();
            }else
                _logger.InfoFormat("HttpPutContent -> Ok");

            return respValue;
        }

        public static bool HttpPutContentWithoutResponse(string host, string inUrl, string stringContent = "", int timeToCancel=5000)
        {
            var url = string.Format("http://{0}/{1}", host, inUrl);

            Exception expectedException = null;

            bool done = false;

            var client = new HttpClient();
            var cts = new CancellationTokenSource();

            _logger.InfoFormat("HttpPutContent ->{0}", url);

            var backgroundRequest = new TaskFactory().StartNew(async () =>
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Put, url);
                    client.DefaultRequestHeaders.ExpectContinue = false;
                    request.Content = new StringContent(stringContent);

                    var resp = await client.SendAsync(request,
                        HttpCompletionOption.ResponseHeadersRead, cts.Token);

                    _logger.InfoFormat("HttpPutContent -> {0}", stringContent);
                    done = true;
                }
                catch (TaskCanceledException ex)
                {
                    _logger.WarnFormat("HttpPutContent -> done = false cts.Cancel");
                    _logger.WarnFormat("HttpPutContent -> Error");
                    expectedException = ex;
                }
            }, cts.Token);

            // Wait for it to finish
            Thread.Sleep(timeToCancel);

            if (!done)
            {
                _logger.WarnFormat("HttpPutContent -> done = false cts.Cancel");
                _logger.WarnFormat("HttpPutContent -> Error");
                cts.Cancel();
            }
            else
                _logger.InfoFormat("HttpPutContent -> Ok");

            return done;
        }

        public static string HttpGetContent(string host, string inUrl)
        {
            var url = string.Format("http://{0}/{1}", host, inUrl);

            bool done = false;
            var client = new HttpClient();
            var cts = new CancellationTokenSource();
            Exception expectedException = null;
            var respValue = "";

            _logger.InfoFormat("HttpGet ->{0}", url);
            var backgroundRequest = new TaskFactory().StartNew(async () =>
            {
                try
                {
                    client.Timeout = new TimeSpan(0, 0, 10);
                    _logger.InfoFormat("HttpClient -> TimeOut 10 seg");
                    // client.DefaultRequestHeaders.ExpectContinue = false;
                    _logger.InfoFormat("HttpClient -> client.GetAsync");
                    var resp = await client.GetAsync(url, cts.Token);
                    _logger.InfoFormat("HttpClient -> resp -> {0}", resp);
                    _logger.InfoFormat("HttpClient -> resp.EnsureSuccessStatusCode");
                    resp.EnsureSuccessStatusCode();
                    _logger.InfoFormat("HttpClient -> resp.resp.Content.ReadAsStringAsync()");
                    respValue = await resp.Content.ReadAsStringAsync();

                    _logger.InfoFormat("HttpClient -> resp Xml = {0}", respValue);
                    done = true;
                }
                catch (TaskCanceledException ex)
                {
                    _logger.WarnFormat("HttpGet -> Exception {0}", ex.Message);
                    expectedException = ex;
                }
            }, cts.Token);

            // Wait for it to finish
            _logger.InfoFormat("HttpGet -> Wait Sleep 10 seg.");
            Thread.Sleep(10000);

            if (!done)
            {
                _logger.WarnFormat("HttpGet -> done = false cts.Cancel");
                _logger.WarnFormat("HttpGet -> Error");
                cts.Cancel();
            }
            else
                _logger.InfoFormat("HttpGet -> Ok");

            return respValue;
        }

        public static string HttpPostContent(string host, string inUrl, string stringContent = "")
        {
            var url = string.Format("http://{0}/{1}", host, inUrl);

            bool done = false;
            var client = new HttpClient();
            var cts = new CancellationTokenSource();
            Exception expectedException = null;
            var respValue = "";

            _logger.InfoFormat("HttpPost ->{0}", url);
            var backgroundRequest = new TaskFactory().StartNew(async () =>
            {
                try
                {
                    client.Timeout = new TimeSpan(0, 0, 10);
                    _logger.InfoFormat("HttpClient -> TimeOut 10 seg");
                    _logger.InfoFormat("HttpClient -> client.GetAsync");
                    var resp = await client.PostAsync(url, new StringContent(stringContent), cts.Token);
                    _logger.InfoFormat("HttpClient -> resp -> {0}", resp);
                    _logger.InfoFormat("HttpClient -> resp.EnsureSuccessStatusCode");
                    resp.EnsureSuccessStatusCode();
                    _logger.InfoFormat("HttpClient -> resp.resp.Content.ReadAsStringAsync()");
                    respValue = await resp.Content.ReadAsStringAsync();

                    _logger.InfoFormat("HttpClient -> resp Xml = {0}", respValue);
                    done = true;
                }
                catch (TaskCanceledException ex)
                {
                    _logger.WarnFormat("HttpPost -> Exception {0}", ex.Message);
                    expectedException = ex;
                }
            }, cts.Token);

            // Wait for it to finish
            _logger.InfoFormat("HttpPost -> Wait Sleep 10 seg.");
            Thread.Sleep(10000);

            if (!done)
            {
                _logger.WarnFormat("HttpPost -> done = false cts.Cancel");
                _logger.WarnFormat("HttpPost -> Error");
                cts.Cancel();
            }
            else
                _logger.InfoFormat("HttpGet -> Ok");

            return respValue;
        }

    }
}
