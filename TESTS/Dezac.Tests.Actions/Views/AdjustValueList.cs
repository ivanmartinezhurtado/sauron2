﻿using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;

namespace Dezac.Tests.Actions.Views
{
    public class AdjustValueList : CollectionEditor
    {
        private static Type[] types;

        public AdjustValueList(Type type) 
            : base(type)
        {
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (value == null)
                value = new List<AdjustValueDefWrapper>();

            var result = base.EditValue(context, provider, value);

            return result;
        }
        protected override Type[] CreateNewItemTypes()
        {
            
            types = new Type[] { typeof(AdjustValueDefMaxMin), typeof(AdjustValueDefAvgTol) };
            return types;
        }

        protected override CollectionForm CreateCollectionForm()
        {
            var form = base.CreateCollectionForm();

            form.Text = "Adjust Value Type List";

            return form;
        }
    }
}
