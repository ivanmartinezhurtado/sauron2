﻿using Dezac.Device.Measure;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.05)]
    public class SEM_LORATest : TestBase
    {
        private TowerBoard tower;
        private SEM_LORA sem_lora;
        private SEM_LORA sem_lora_Gate;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.ShutdownSources();
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
        }

        public void TestCommunications()
        {
            sem_lora = new SEM_LORA(Comunicaciones.SerialPort);
            logger.InfoFormat("Test comunicaction with {0}", Comunicaciones.SerialPort);

            Logger.Debug("Cambiamos el baudRate a 19200");
            sem_lora.sp.BaudRate = 19200;

            SamplerWithCancel((p) =>
            {
                if (p >0)
                {
                    Logger.Debug("Cambiamos el baudRate a 9600");
                    sem_lora.sp.BaudRate = 9600;
                }

                sem_lora.TestComunication();
                return true;

            }, "", 3, 500, 500, false, false);

            sem_lora_Gate = new SEM_LORA(Comunicaciones.SerialPort2);
            sem_lora_Gate.sp.BaudRate = 19200;
            logger.InfoFormat("Test comunicaction Device Gate with {0}", Comunicaciones.SerialPort2);

            SamplerWithCancel((p) =>
            {
                if (p > 0)
                {
                    Logger.Debug("Cambiamos el baudRate a 9600");
                    sem_lora_Gate.sp.BaudRate = 9600;
                }

                sem_lora_Gate.TestComunication();
                return true;
            }, "", 3, 500, 500, false, false);
       }

        public void TestDefaultParameters()
        {
            SamplerWithCancel((p) =>
            {
                sem_lora.SetupDefault();
                Delay(500, "");
                sem_lora.TestComunication();
                return true;
            }, "", 5, 500, 500, false, false);

            var firmwareVersion = sem_lora.ReadFirmwareVersion();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Versión de firmware incorrecta"), ParamUnidad.SinUnidad);

            sem_lora.TestStatusVoltge();

            var frameNumber = (uint)TestInfo.NumBastidor.Value;

            sem_lora.WriteBastidor(frameNumber.ToString());

            sem_lora.WriteHardwareVersion(Identificacion.VERSION_HARDWARE);

            sem_lora.WriteMAC(frameNumber.ToString());

            sem_lora_Gate.WriteMAC(frameNumber.ToString());

            sem_lora_Gate.WriteSerialNumber(frameNumber.ToString());
        }

        public void TestTransmision()
        {
            var param = new AdjustValueDef() { Name = Params.POWER_SIGNAL.Null.TestPoint("TRANSMISION").Name, Min = Params.POWER_SIGNAL.Null.TestPoint("TRANSMISION").Min(), Max = Params.POWER_SIGNAL.Null.TestPoint("TRANSMISION").Max(), Unidad = ParamUnidad.dBm };
            var param2 = new AdjustValueDef() { Name = Params.SNR.Null.TestPoint("TRANSMISION").Name, Min = Params.SNR.Null.TestPoint("TRANSMISION").Min(), Max = Params.SNR.Null.TestPoint("TRANSMISION").Max(), Unidad = ParamUnidad.dBm };
            var ajustParam = new List<AdjustValueDef>() { param, param2 };

            var resultsAdjustValue = TestMeasureBase(ajustParam,
            (step) =>
            {
                sem_lora.WriteModeTransmited(SEM_LORA.TypeMode.Transmisor);
                sem_lora_Gate.WriteModeTransmited(SEM_LORA.TypeMode.Receptor);

                this.InParellel(() =>
                {
                    sem_lora.WriteSendTestPowerEmision("helloworld");
                }, () =>
                {
                    var portMTX = "MetrixPort";

                    using (var tester = new MTX3293())
                    {
                        tester.PortCom = Convert.ToByte(GetVariable<string>(portMTX, portMTX).Replace("COM", ""));
                        tester.WaitTimeMeasure = 500;

                        var defs = new AdjustValueDef(Params.I.Null.TestPoint("VTransmision").Name, 0, Params.I.Null.TestPoint("VTransmision").Min(), Params.I.Null.TestPoint("VTransmision").Max(), 0, 0, ParamUnidad.mA);
                        TestMeasureBase(defs, (stp) =>
                        {
                            return tester.ReadCurrent(MTX3293.measureTypes.DC);
                        }, 1, 5, 500);
                    }
                    // this.TestConsumo(tower, configuration, true); //Para usar con la fuebte M9111A
                });

                var status = sem_lora.ReadStatusTransmisionEvent();
                Assert.AreEqual("TRANSMISION_FINISH", status, Configuracion.GetString("TRANSMISION_FINISH", "0002", ParamUnidad.SinUnidad), Error().SOFTWARE.ERROR_TRANSFERENCIA.DETECCION("Error. estado deteccion final transmision incorrecto"), ParamUnidad.SinUnidad);

                var statusGate = sem_lora_Gate.ReadStatusTransmisionEvent();
                Assert.AreEqual("TRANSMISION_GATE_FINISH", statusGate, Configuracion.GetString("TRANSMISION_GATE_FINISH", "0001", ParamUnidad.SinUnidad), Error().SOFTWARE.ERROR_TRANSFERENCIA.DETECCION("Error. estado deteccion final transmision en el gateway incorrecto"), ParamUnidad.SinUnidad);

                var response = sem_lora_Gate.ReadTramaRecived();
                if (!response.ToUpper().Contains("HELLO") && !response.ToUpper().Contains("WORLD"))
                    throw new Exception("Error. estado deteccion trama recibida por el Gateway incorrecto");

                var powerRecived = sem_lora_Gate.ReadMaxPowerRecived();
                var result = new List<double>() { powerRecived.powerSignal, powerRecived.SNR };

                return result.ToArray();
            }, 0, 2, 500, null, true);

            Resultado.Set(Params.POWER_SIGNAL.Null.TestPoint("TRANSMISION_MAX").Name, resultsAdjustValue[0].Max, ParamUnidad.dBm);
            Resultado.Set(Params.POWER_SIGNAL.Null.TestPoint("TRANSMISION_MIN").Name, resultsAdjustValue[0].Min, ParamUnidad.dBm);

            Resultado.Set(Params.SNR.Null.TestPoint("TRANSMISION_MAX").Name, resultsAdjustValue[1].Max, ParamUnidad.dBm);
            Resultado.Set(Params.SNR.Null.TestPoint("TRANSMISION_MIN").Name, resultsAdjustValue[1].Min, ParamUnidad.dBm);
        }

        public void TestReception()
        {
            var param = new AdjustValueDef() { Name = Params.POWER_SIGNAL.Null.TestPoint("RECEPTION").Name, Min = Params.POWER_SIGNAL.Null.TestPoint("RECEPTION").Min(), Max = Params.POWER_SIGNAL.Null.TestPoint("RECEPTION").Max(), Unidad = ParamUnidad.dBm };
            var param2 = new AdjustValueDef() { Name = Params.SNR.Null.TestPoint("RECEPTION").Name, Min = Params.SNR.Null.TestPoint("RECEPTION").Min(), Max = Params.SNR.Null.TestPoint("RECEPTION").Max(), Unidad = ParamUnidad.dBm };
            var ajustParam = new List<AdjustValueDef>() { param, param2 };

            var paramVoltageInOn = new AdjustValueDef() { Name = Params.V.Other("RECEPTION").TestPoint("ENABLED").Name, Min = Params.V.Other("RECEPTION").TestPoint("ENABLED").Min(), Max = Params.V.Other("RECEPTION").TestPoint("ENABLED").Max(), Unidad = ParamUnidad.dBm };

            var paramVoltageInOff = new AdjustValueDef() { Name = Params.V.Other("RECEPTION").TestPoint("DISABLED").Name, Min = Params.V.Other("DISABLED").TestPoint("RECEPTION").Min(), Max = Params.V.Other("RECEPTION").TestPoint("DISABLED").Max(), Unidad = ParamUnidad.dBm };

            var magnitud = new MagnitudToMeasure();
            magnitud.Frequency = freqEnum._50Hz;
            magnitud.Rango = 15;
            magnitud.Magnitud = MagnitudsMultimeter.VoltDC;
            magnitud.NumMuestras = 3;
            magnitud.NPLC = 8;
            magnitud.DigitosPrecision = 4;

            var resultsAdjustValue = TestMeasureBase(ajustParam,
            (step) =>
            {
                sem_lora.WriteModeTransmited(SEM_LORA.TypeMode.Receptor);
                sem_lora_Gate.WriteModeTransmited(SEM_LORA.TypeMode.Transmisor);
                sem_lora_Gate.WriteSendTestPowerEmision("helloworld");
                var status = sem_lora_Gate.ReadStatusTransmisionEvent();
                Assert.AreEqual("RECEPTION_FINISH", Convert.ToInt32(status), (int)Configuracion.GetDouble("RECEPTION_FINISH", 2, ParamUnidad.SinUnidad), Error().SOFTWARE.ERROR_TRANSFERENCIA.DETECCION("Error. estado deteccion final transmision incorrecto"), ParamUnidad.SinUnidad);

                if (step == 1)
                    TestMeasureBase(paramVoltageInOn,
                        (p) =>
                        {
                            var measureRecpetionOn = tower.MeasureMultimeter(InputMuxEnum.IN2, magnitud);
                            return measureRecpetionOn;
                        }, 0, 2, 500);

                var statusGate = sem_lora.ReadStatusTransmisionEvent();
                Assert.AreEqual("RECEPTION_GATE_FINISH", Convert.ToInt32(statusGate), (int)Configuracion.GetDouble("RECEPTION_GATE_FINISH", 2, ParamUnidad.SinUnidad), Error().SOFTWARE.ERROR_TRANSFERENCIA.DETECCION("Error. estado deteccion final transmision en el gateway incorrecto"), ParamUnidad.SinUnidad);

                if (step == 1)
                    TestMeasureBase(paramVoltageInOff,
                        (p) =>
                        {
                            var measureRecpetionOff = tower.MeasureMultimeter(InputMuxEnum.IN2, magnitud);
                            return measureRecpetionOff;
                        }, 0, 2, 500);

                var response = sem_lora.ReadTramaRecived();
                if (!response.ToUpper().Contains("HELLO") && !response.ToUpper().Contains("WORLD"))
                    throw new Exception("Error. estado deteccion trama recibida por el equipo incorrecta");

                var powerRecived = sem_lora.ReadMaxPowerRecived();
                var result = new List<double>() { powerRecived.powerSignal, powerRecived.SNR };

                return result.ToArray();
            }, 0, 5, 500, null, true);

            var comSetup = Comunicaciones.BaudRate.ToString();
            var bps = comSetup.Contains("9600") ? SEM_LORA.bps._9600 : SEM_LORA.bps._19200;
            sem_lora.WriteBaudRate(bps);
        }

        public void TestFinish()
        {
            if (sem_lora != null)
                sem_lora.Dispose();

            if (sem_lora_Gate != null)
                sem_lora_Gate.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.Dispose();
            }
        }
    }
}
