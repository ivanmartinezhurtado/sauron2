﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.08)]
    public class ISOMETERTest : TestBase
    {

        #region OUTS
        private const ushort EV_GENERAL = 4;
        private const ushort CONTACTS_LEFT = 5;
        private const ushort CONTACTS_COMS = 6;
        private const ushort CONTACTS_RIGHT = 7;
        private const ushort KEY_R = 9;
        private const ushort KET_T = 10;
        private const ushort KEY_PROG = 11;
        private const ushort OUT_IN_TR = 21;
        private const ushort OUT_IN_PE = 20;
        private const ushort ACTIVATE_LIGHT = 13;
        private const ushort DISABLED_DATAMAN = 22;

        private enum TypeConections
        {
            OL = 0,
            CC_L_GND = 17,
            _100K = 19,
            _1M = 28,
            _10M = 29,
        }
        #endregion

        #region INPUTS
        private const ushort DEVICE_PRESENCE = 9;
        private const ushort BLOCK_SECURITY = 23;
        private const ushort CHECK_CONTACTS_RIGHT = 10;
        private const ushort CHECK_CONTACTS_LEFT = 11;
        private const ushort CHECK_CONTACTS_COMS = 12;
        private const ushort CHECK_NC_AL1 = 15;
        private const ushort CHECK_NO_AL1 = 16;
        private const ushort CHECK_NC_AL2 = 17;
        private const ushort CHECK_NO_AL2 = 18;
        #endregion

        Tower1 tower;
        ISOMETER isometer;
        string CAMERA_IDS;

        public void TestInitialization()
        {            
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower1>("TOWER", () => { return new Tower1(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
            isometer = new ISOMETER(5);
            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);    

            tower.PTE.ApplyOff();
            Delay(150, "Reset de la fuente");

            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveElectroValvule();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DEVICE_PRESENCE];
            }, "Error No se ha detectado el equipo en el útil o el equipo no lleva el ancla puesta", 40, 500, 2500, false, false);


            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[BLOCK_SECURITY];
            }, "No se ha detectado el bloque de seguridad", 5, 1000);          

            tower.IO.DO.On(EV_GENERAL, CONTACTS_LEFT, CONTACTS_COMS, CONTACTS_RIGHT);
            Delay(1000, "Activando Pistones");

            if (!tower.IO.DI[CHECK_CONTACTS_LEFT])
                throw new Exception("No se han detectado los bornes izquierdos");
            if (!tower.IO.DI[CHECK_CONTACTS_COMS])
                throw new Exception("No se han detectado los pines de comunicacion TTL");
            if (!tower.IO.DI[CHECK_CONTACTS_RIGHT])
                throw new Exception("No se han detectado los bornes derechos");
        }

        public void TestComunications()
        {
            tower.IO.DO.On(DISABLED_DATAMAN); //Grabación dataman desactivada
            
            tower.PTE.ApplyPresetsAndWaitStabilisation(230, 0, 0);

            tower.IO.DO.On(OUT_IN_PE); //PE logica inversa -> tiene que estar la salida activa para que PE este desactivada (todo desconectado al iniciar test)

            SamplerWithCancel((p) =>
            {
                isometer.FlagTest();
                return true;
            }, "Error de comunicaciones al enviar FlagTest", 5, 500, 200, false, false);
  
           var firmwareVersion = isometer.ReadFirmwareVersion();

           Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString().Trim() , Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error version de Firmware incorrecta"), ParamUnidad.SinUnidad);

            var firmwareVersionDisplay = isometer.ReadFirmwareVersionDisplay();
            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_2, firmwareVersionDisplay.ToString().Trim() , Identificacion.VERSION_FIRMWARE_2.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error version Firmware del Display incorrecta"), ParamUnidad.SinUnidad);
        }

        public void TestVoltage(string testPoint)
        {
            var defs = new AdjustValueDef(Params.V.Null.EnVacio.TestPoint(testPoint + "_VH").Name, 0, Params.V.Null.EnVacio.TestPoint(testPoint + "_VH").Min(), Params.V.Null.EnVacio.TestPoint(testPoint + "_VH").Max(), 0, 0, ParamUnidad.V);

            var defs2 = new AdjustValueDef(Params.V.Null.EnVacio.TestPoint(testPoint + "_VL").Name, 0, Params.V.Null.EnVacio.TestPoint(testPoint + "_VL").Min(), Params.V.Null.EnVacio.TestPoint(testPoint + "_VL").Max(), 0, 0, ParamUnidad.V);

            var defs3 = new AdjustValueDef(Params.V.Null.EnVacio.TestPoint(testPoint + "_VCPU").Name, 0, Params.V.Null.EnVacio.TestPoint(testPoint + "_VCPU").Min(), Params.V.Null.EnVacio.TestPoint(testPoint + "_VCPU").Max(), 0, 0, ParamUnidad.V);

            var lisdefs = new List<AdjustValueDef>() { defs, defs2, defs3 };
   
            TestMeasureBase(lisdefs, (step) =>
            {
                var voltageVH = isometer.ReadTensionsVH();
                var voltageVL = isometer.ReadTensionsVL();
                var voltageVCPU = isometer.ReadTensionsVCPU();

                var result = new List<double>() { voltageVH, voltageVL, voltageVCPU };
                return result.ToArray();

            }, 0, 5, 500);
        } 

        public void TestDisplay()
        {
            var timeWait = 1200; //AJUSTARLO!! Implementacion de mayor tiempo por requisito de vision (imagen demasiado oscura) 
            isometer.WriteDigitalOutput(ISOMETER.DigitalOuputs.ACTIVATION_DISPLAY);

            isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.ALL_SEGMENTS_OFF);
            Delay(timeWait, "Capturando Imagen");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "ISOMETER_OFF", "OFF", "ISOMETER");

            isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.ODD_SEGMENTS);
            Delay(timeWait, "Capturando Imagen");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "ISOMETER_ODDS", "ODD", "ISOMETER");
         
            isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.ODD_SEGMENTS_AND_COMMON_PAIRS);
            Delay(timeWait, "Capturando Imagen");
            this.TestHalconMatchingProcedure(CAMERA_IDS, "ISOMETER_ODDS_PAIRS", "ODDS_PAIRS", "ISOMETER");

            //isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.ODD_SEGMENTS);
            //Delay(timeWait, "Configurando BackLight verde");

            //isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.ALL_SEGMENTS_ON);
            //Delay(timeWait, "Capturando Imagen");
            //this.TestHalconMatchingProcedure(CAMERA_IDS, "ISOMETER_ALL", "ALL", "ISOMETER");
        }

        public void TestLeds()
        {
            isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.ODD_SEGMENTS);
            Delay(1000, "Capturando Imagen");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "ISOMETER_GREEN", "GREEN", "ISOMETER");

            isometer.WriteDisplaySegments(ISOMETER.ValuesDisplay.PAIRS_SEGMENTS);
            Delay(1000, "Capturando Imagen");
            this.TestHalconFindLedsProcedure(CAMERA_IDS, "ISOMETER_RED", "RED", "ISOMETER");
        }

        public void TestDigitalInputs()
        {
            var InputsAndTowerOutputsDictionary = new Dictionary<ISOMETER.DigitalInputs, ushort>()
            {   
                {ISOMETER.DigitalInputs.INPUT_T_R, OUT_IN_TR},
                {ISOMETER.DigitalInputs.INPUT_PE, OUT_IN_PE},                                                         
            };

            //isometer.FlagTest();

            SamplerWithCancel((p) =>
            {
                return isometer.ReadDigitalInputs() == ISOMETER.DigitalInputs.ALL_INPUTS_OFF;
            }, string.Format("Se ha detectado alguna entrada activada cuando no debería detectarse ninguna"), 3, 500);

            foreach (KeyValuePair<ISOMETER.DigitalInputs, ushort> currentInput in InputsAndTowerOutputsDictionary)
                SamplerWithCancel((p) =>
                {
                    bool valueINPUT_T_R = currentInput.Key == ISOMETER.DigitalInputs.INPUT_T_R;

                    tower.IO.DO[currentInput.Value] = valueINPUT_T_R;  
              
                    Delay(500, "Activando salida");

                    var result = isometer.ReadDigitalInputs();

                    tower.IO.DO[currentInput.Value] = !valueINPUT_T_R;

                    return currentInput.Key == result;

                }, string.Format("No se ha detectado la Entrada Digital {0}", currentInput.Key.GetDescription().Replace("_", " ")), 3, 500);

            var keyboardTowerOutputsDictionary = new Dictionary<ISOMETER.DigitalInputs, ushort>()
            {               
                {ISOMETER.DigitalInputs.KEY_R, KEY_R},
                {ISOMETER.DigitalInputs.KEY_T, KET_T},
                {ISOMETER.DigitalInputs.KEY_PROG, KEY_PROG},      
            };

            foreach (KeyValuePair<ISOMETER.DigitalInputs, ushort> currentKey in keyboardTowerOutputsDictionary)
                SamplerWithCancel((p) =>                      
                {
                    tower.IO.DO.PulseOn(300,currentKey.Value);

                    Delay(250, "Espera detección tecla");
                   
                    return isometer.ReadDigitalInputs() == currentKey.Key;

                }, string.Format("No se ha detectado la tecla {0}", currentKey.Key.GetDescription().Replace("_"," ")), 3, 500);

            isometer.WriteClearRegisterDigitalInput();

        }

        public void TestDigitalOutputs()
        {
            var OutputsList = new List<ISOMETER.DigitalOuputs>()
            {               
                {ISOMETER.DigitalOuputs.ALL_OUTPUTS_OFF},
                {ISOMETER.DigitalOuputs.ACTIVATION_RELAY_A1},
                {ISOMETER.DigitalOuputs.ACTIVATION_RELAY_A2}
            };

            //isometer.FlagTest();

            foreach (ISOMETER.DigitalOuputs currentOut in OutputsList)
                SamplerWithCancel((p) =>
                {
                    isometer.WriteDigitalOutput(currentOut);
                    Delay(500, "Activando salidas");

                    var result = false;
                    switch(currentOut)
                    {
                        case ISOMETER.DigitalOuputs.ALL_OUTPUTS_OFF:
                            result =  tower.IO.DI[CHECK_NC_AL1] && tower.IO.DI[CHECK_NC_AL2];
                            break;
                        case ISOMETER.DigitalOuputs.ACTIVATION_RELAY_A1:
                            result = !tower.IO.DI[CHECK_NC_AL1] && tower.IO.DI[CHECK_NO_AL1];
                            break;
                        case ISOMETER.DigitalOuputs.ACTIVATION_RELAY_A2:
                            result = !tower.IO.DI[CHECK_NC_AL2] && tower.IO.DI[CHECK_NO_AL2];
                            break;
                        default:
                            break;
                    }

                    return result;       
             
                }, string.Format("Se ha detectado un error al activar la salida {0}", currentOut.ToString().Replace("_"," ")));
        }

        public void TestCalibration()
        {
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(24, 0.5); //Activamos Rele L circuito abierto
                                               
            //isometer.FlagTest();

            //****************************************************************************
            //****************** CONSTANE K ********************************************

            var adjustValue = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("K"), ParamUnidad.Puntos);

            var resadjustResult = TestMeasureBase(adjustValue, (s) =>
            {            
                isometer.WriteOrderReadCalibration(ISOMETER.OrderReadCalibrationValues.K);

                Delay(250, "Esperando a escribir la orden de calibracion de K");

                var lecturaCtealibracion = isometer.ReadValueCteCalibration();

                return lecturaCtealibracion;

            }, 0, 5, 500);

            isometer.WriteCteK((int)resadjustResult.Value);

            Delay(1000, "Grabando calibracion K");

            var readCteKRead = isometer.ReadCteK();

            Assert.AreEqual((int)readCteKRead, (int)resadjustResult.Value, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error. No se ha grabado correctamente la constante de K"));

            isometer.WriteOperatingMode(ISOMETER.OperatingModeValues.NORMAL);

            tower.LAMBDA.ApplyOffAndWaitStabilisation(); //Desactivamos Rele L circuito abierto

            //****************************************************************************
            //****************** CONSTANE Ri ********************************************          

            tower.IO.DO.On((byte)TypeConections._1M);

            Delay(150, "Esperando activación rele");

            isometer.FlagTest();

            adjustValue = new AdjustValueDef(Params.POINTS.Null.Ajuste.TestPoint("Ri"), ParamUnidad.Puntos);

            resadjustResult = TestMeasureBase(adjustValue, (s) =>
            {
                isometer.WriteOrderReadCalibration(ISOMETER.OrderReadCalibrationValues.Ri);

                Delay(250, "Esperando escribir la orden de calibración de Ri");

                var lecturaCtealibracion = isometer.ReadValueCteCalibration();

                return lecturaCtealibracion;

            }, 0, 5, 500);

            isometer.WriteCteRi((int)resadjustResult.Value);

            Delay(1000, "Grabacion calibracion Ri");

            var readCteRiRead = isometer.ReadCteRi();

            Assert.AreEqual((int)readCteRiRead, (int)resadjustResult.Value, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error. No se ha grabado correctamente la constante de Ri"));

            tower.IO.DO.OffWait(500, (byte)TypeConections._1M);
        }

        public void TestFuncionalRF()
        {
            //isometer.WriteOperatingMode(ISOMETER.OperatingModeValues.NORMAL);

            foreach (TypeConections conection in Enum.GetValues(typeof(TypeConections))) 
            {
                if (conection != TypeConections.OL)
                    tower.IO.DO.On((ushort)conection);

                Delay(1000, "Esperando que el isometer tome la medida");

                var defs = new AdjustValueDef(Params.RES.Null.TestPoint(conection.GetDescription()).Name, 0, Params.RES.Null.TestPoint(conection.GetDescription()).Min(), Params.RES.Null.TestPoint(conection.GetDescription()).Max(), 0, 0, ParamUnidad.KOhms);

                TestMeasureBase(defs, (step) =>
                {
                    var reading = isometer.ReadExactMeasureRF();
                    return reading;
                }, 0, 5, 500);

                if (conection != TypeConections.OL)
                    tower.IO.DO.Off((ushort)conection);
            }
        }

        public void AutoTestRF()
        {
            isometer.FlagTest();
            
            isometer.WriteAutoTest(ISOMETER.NoYesValues.YES);

            Delay(500, "Esperando configuración del isometer");

            var defs = new AdjustValueDef(Params.RES.Null.TestPoint("AUTOTEST").Name, 0, Params.RES.Null.TestPoint("AUTOTEST").Min(), Params.RES.Null.TestPoint("AUTOTEST").Max(), 0, 0, ParamUnidad.KOhms);
            TestMeasureBase(defs, (step) =>
            {
                return isometer.ReadExactMeasureRF();

            }, 0, 5, 500);

            isometer.WriteAutoTest(ISOMETER.NoYesValues.NO);
        }

        public void TestCustomization()
        {
            isometer.FlagTest();

            var result = isometer.ReadError();
            if (result != ISOMETER.ErrorMessages.NO_ERROR)
                throw new Exception("Error el equipo se ha detetado con un estado de error al finalizar el test");

            var model = isometer.ReadModel();
            var subModel = isometer.ReadSubModel();
            Assert.IsTrue(Convert.ToUInt16(Identificacion.MODELO) == model, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error. el número de modelo grabado en el equipo no es correcto"));
            Assert.IsTrue(Identificacion.SUBMODELO == subModel, Error().PROCESO.PARAMETROS_ERROR.VALOR_INCORRECTO("Error. el submodelo grabado en el equipo no es correcto"));

            isometer.WriteNumBastidor((int)TestInfo.NumBastidor.Value);
            isometer.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));

 /*           tower.PTE.Reset();
            Delay(500, "Reseteando PTE");
            tower.PTE.ApplyAndWaitStabilisation(Consignas.GetDouble(Params.V_AC.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V), 0, 0);
            */
            isometer.WriteOperatingMode(ISOMETER.OperatingModeValues.NORMAL);
            uint SerialNumber = 0;
            int NumBastidor = 0;

            SamplerWithCancel((p) =>
            {
                SerialNumber = isometer.ReadSerialNumber();
                NumBastidor = isometer.ReadNumBastidor();
                return true;
            }, "Error en leer el Numero de serie o de Bastidor despues de reiniciar equipo", 3, 1500, 1000);

            Assert.IsTrue(SerialNumber.ToString().Trim() == TestInfo.NumSerie.Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error en la grabacion del numero de serie ya que no coincide con el original"));

            Assert.AreEqual(NumBastidor, (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error en la grabacion del numero de bastidor ya que no coincide con el original"));
        }

        public void TestCustomerParameterization()
        {
            isometer.FlagTest();

            isometer.ClearParameterization(ISOMETER.FlashPages.PARAMETERIZATION);
            
            isometer.WriteModel(Convert.ToUInt16(Identificacion.MODELO));

            isometer.WriteSubModel(Identificacion.SUBMODELO);

            isometer.WriteResistanceAlarmAL1TableParameter(Convert.ToUInt16(Parametrizacion.GetString(Params.RES.Null.TestPoint("ALARM1").Name, "0", ParamUnidad.SinUnidad))); // Posicion vendra de BBDD

            var lectura = isometer.ReadResistanceAlarmAL1TableParameter();

            isometer.WriteResistanceAlarmAL2TableParameter(Convert.ToUInt16(Parametrizacion.GetString(Params.RES.Null.TestPoint("ALARM2").Name, "26", ParamUnidad.SinUnidad))); // Posicion vendra de BBDD

            lectura = isometer.ReadResistanceAlarmAL2TableParameter();

            isometer.WriteStartDelayTime(Convert.ToUInt16(Parametrizacion.GetString(Params.TIME.Null.TestPoint("DELAY_START").Name, "0", ParamUnidad.SinUnidad)));

            isometer.WriteDelayResponse(Convert.ToUInt16(Parametrizacion.GetString(Params.TIME.Null.TestPoint("DELAY_RESPONSE").Name, "0", ParamUnidad.SinUnidad)));

            isometer.WriteRegisterMemoryErrors(ISOMETER.NoYesValues.YES);

            isometer.WritePolarityRelayAL1(ISOMETER.PolarityRelayValues.STANDARD);

            isometer.WritePolarityRelayAL2(ISOMETER.PolarityRelayValues.STANDARD);

            isometer.WritePasswordLockActivation(ISOMETER.NoYesValues.NO);

            isometer.WritePasswordNumber(Convert.ToUInt16(Parametrizacion.GetString("PASSWORD", "0", ParamUnidad.SinUnidad)));

            var CRCvalue = Parametrizacion.GetString("CRC", "47887", ParamUnidad.SinUnidad);

            isometer.WriteCRC(Convert.ToUInt16(CRCvalue));

            isometer.ClearParameterization(ISOMETER.FlashPages.SETUP);

        }

        public void TestFinalVerification()
        {
            isometer.WriteOperatingMode(ISOMETER.OperatingModeValues.NORMAL);

            var result = isometer.ReadError();
            if (result != ISOMETER.ErrorMessages.NO_ERROR)
                throw new Exception(string.Format("Error el equipo se ha detetado con el error {0} al finalizar el test", result.ToString()));
        }

        public void TestFinish()
        {
            if (isometer != null)
                isometer.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                foreach (TypeConections conection in Enum.GetValues(typeof(TypeConections)))
                    if (conection != 0)
                        tower.IO.DO.Off((ushort)conection);

                tower.IO.DO.Off(CONTACTS_LEFT, CONTACTS_COMS, CONTACTS_RIGHT);
                tower.IO.DO.Off(ACTIVATE_LIGHT, OUT_IN_PE, OUT_IN_TR, 22, 14);
                Delay(1000,"");

                tower.IO.DO.OnWait(500, 12);
                tower.IO.DO.OffWait(500, 12);

                tower.IO.DO.Off(EV_GENERAL);
                tower.Dispose();
            }
        }
   
        

    }
}