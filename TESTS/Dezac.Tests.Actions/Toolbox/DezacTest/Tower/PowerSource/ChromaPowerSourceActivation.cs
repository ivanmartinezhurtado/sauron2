﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(ChromaPowerSourceActivationActionDescription))]
    [Browsable(false)]

    public class ChromaPowerSourceActivation : ActionBase
    {
        [DefaultValue("230")]
        [Description("La tensión de consigna para la fuente CHROMA")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Voltage { get; set; }

        [DefaultValue("0")]
        [Description("La frecuencia de la señal a generar")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Frequency { get; set; }

        [DefaultValue("10")]
        [Description("La limitación de la corriente de la señal a generar")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Current { get; set; }

        [DefaultValue(Chroma.VoltageRangeEnum.HIGH)]
        [Description("Rango de voltage del CHROMA")]
        public Chroma.VoltageRangeEnum VoltageRange { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();


            tower.Chroma.SetVoltageRange(VoltageRange);
            tower.Chroma.ApplyPresetsAndWaitStabilisation(ResolveVariable(Voltage), ResolveVariable(Current), ResolveVariable(Frequency));
        }

        private double ResolveVariable(string variableName)
        {
            return Convert.ToDouble(VariablesTools.ResolveValue(variableName).ToString().Replace(".", ","));
        }

    }

    public class ChromaPowerSourceActivationActionDescription : ActionDescription
    {
        public override string Name { get { return "ActivateChroma"; } }
        public override string Description { get { return "Enciende la fuente Chroma de la torre con las consignas especificadas"; } }
        public override string Category { get { return "Hardware/Tower/PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;PowerSourcesShutdown:2;TowerDispose:2"; } }
    }
}
