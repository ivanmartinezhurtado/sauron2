﻿namespace Dezac.Device
{
    public interface IModbusUUT<T> where T : ModbusDevice
    {
        T Modbus { get; }
    }
}
