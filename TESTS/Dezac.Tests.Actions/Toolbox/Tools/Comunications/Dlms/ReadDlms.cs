﻿using Dezac.DLMS;
using Dezac.Tests.Model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ReadDlmsDescription))]
    public class ReadDlms : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número Obis")]
        public string Obis { get; set; }

        [Category("3. Configuration")]
        [Description("Número de atributo")]
        [DefaultValue(2)]
        public int Attribute { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de registro con el que se trabaja")]
        [DefaultValue(TypeRegister.Data)]
        public TypeRegister Type { get; set; }

        [Category("4. Results")]
        [Description("Nombre de la variable donde guardar el valor")]
        public string DataOutput { get; set; }

        [Category("4. Results")]
        [Description("Unidades con las que trabaja el valor a obtener")]
        [DefaultValue(ParamUnidad.SinUnidad)]
        public ParamUnidad DataOutputUnits { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre de la instancia del objecto creado")]
        [DefaultValue("DlmsDevice")]
        public string DeviceName { get; set; }

        [Category("3.1 GenericProfileUse")]
        [Description("Tiempo de inicio")]
        public DateTime? Start { get; set; }

        [Category("3.2 GenericProfileUse")]
        [Description("Tiempo de finalización")]
        public DateTime? End { get; set; }

        public enum TypeRegister
        {
            Data,
            Register,
            Clock,
            GenericProfile,
            
        }

        public override void Execute()
        {
            string name = DeviceName ?? "DlmsDevice";
            var uut = GetVariable(name) as DLMSDeviceSerialPort;
            var result = "";

            switch (Type)
            {
                case TypeRegister.Data:
                    result = uut.ReadData<string>(Obis, Attribute);
                    break;
                case TypeRegister.Register:
                    result = uut.ReadRegister<string>(Obis, Attribute);
                    break;
                case TypeRegister.Clock:
                    result = uut.ReadClock<string>(Obis, Attribute);
                    break;
                case TypeRegister.GenericProfile:
                    var table = uut.ReadGenericProfile(Obis, Attribute, Start, End);
                    StringWriter sw = new StringWriter();
                    table.WriteXml(sw);
                    result = sw.ToString();
                    break;
                

            }

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (string.IsNullOrEmpty(DataOutput))
                DataOutput = Obis;

            if (testBase != null)
                testBase.Resultado.Set(DataOutput, result, DataOutputUnits);
            else
                Context.ResultList.Add(string.Format("{0}={1}", DataOutput, result));

            Context.Variables.AddOrUpdate(DataOutput, result);
        }
    }

    public class ReadDlmsDescription : ActionDescription
    {
        public override string Name { get { return "ReadDLMS"; } }
        public override string Description { get { return "Leer DLMS"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
    }
}
