﻿using Comunications.Message;
using log4net;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(WriteAndReadHexSerialPortDescription))]
    public class WriteAndReadHexSerialPort : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Texto que se debe enviar")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToSend { get; set; }

        [Category("3. Configuration")]
        [Description("Texto que se debe leer")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TextToRead { get; set; }

        [Category("3. Configuration")]
        [DefaultValue(200)]
        [Description("Tiempo de espera a recibir la trama antes de dar error")]
        public int TimeOut { get; set; }

        [Category("3. Configuration")]
        [DefaultValue(3)]
        [Description("Reintentos de comunicaciones")]
        public int Retries { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM_HEX")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para saber si se espera respuesta")]
        [DefaultValue(true)]
        public bool HasResponse { get; set; }

        [Category("3. Configuration")]
        [DefaultValue(0)]
        [Description("Tiempo de espera antes de realizar la comunicación")]
        public int InitialDelay { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "COM_HEX";
            var transport = GetVariable(name) as MessageTransport;
            if (transport == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("DeviceName").Throw();

            transport.ReadTimeout = TimeOut;
            transport.Retries = Retries;
            transport.LogRawMessage = true;

            string[] stringSeparators = new string[] { "\r\n" };

            string[] linesTx = TextToSend.Split(stringSeparators, StringSplitOptions.None);

            string[] linesRx = HasResponse ? TextToRead.Split(stringSeparators, StringSplitOptions.None) : null;

            HexStringMessage message, response;

            CultureInfo enUS = new CultureInfo("en-US");

            for (int i = 0; i < linesTx.Length; i++)
            {
                Thread.Sleep(InitialDelay);

                message = new HexStringMessage();
                message.Text = VariablesTools.ResolveText(linesTx[i]);
                message.HasResponse = HasResponse;
                if (HasResponse)
                    message.Validator = (m) => { return m.Text == linesRx[i]  ; };
                response = transport.SendMessage<HexStringMessage>(message);
            }
        }
    }

    public class WriteAndReadHexSerialPortDescription : ActionDescription
    {
        public override string Name { get { return "Write&ReadHEXSerialPort"; } }
        public override string Description { get { return "Envía y espera a recibir la cadena de de texto especificada"; } }
        public override string Category { get { return "Tools/Comunications/HEX"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
        public override string Dependencies { get { return "InitHexSerialPort;DisposeHexSerialPort:2"; } }
    }
}
