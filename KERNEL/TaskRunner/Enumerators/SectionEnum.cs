﻿namespace TaskRunner.Enumerators
{
    public enum SectionEnum
    {
        Init = 0,
        Main = 1,
        End = 2,
    }
}
