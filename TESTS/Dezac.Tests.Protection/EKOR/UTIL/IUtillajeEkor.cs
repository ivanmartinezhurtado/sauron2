﻿using System.Collections.Generic;

namespace Dezac.Tests.Protection
{
    public interface IUtillajeEkor
    {
        void TestDeviceConnection();
        void TestDeviceDisconnection();
        void ActivatePistonRS232();
        void DesactivatePistonRS232();
        List<KeyValuePair<byte, byte>> GetKeyValuePairInputs();
        List<KeyValuePair<byte, byte>> GetKeyValuePairOutputs();
        List<KeyValuePair<byte, byte>> GetKeyValuePairKeyBoard();
        byte GetOutActivateVL();
        byte GetOutActivateVF();
        void SupplyUSB(bool state = true);
        int GetInputDetectionTrigger();
        void ActivateMeasurePasatapasL1();
        void ActivateMeasurePasatapasL2();
        void ActivateMeasurePasatapasL3();
        void DesactivateMeasurePasatapasL1();
        void DesactivateMeasurePasatapasL2();
        void DesactivateMeasurePasatapasL3();
        int GetOutCLK();
        void ResetUSB();
        void ActivatePistonTestKeyboard();
        void DesactivatePistonTestKeyboard();
    }
}
