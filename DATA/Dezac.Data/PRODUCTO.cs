namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PRODUCTO")]
    public partial class PRODUCTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCTO()
        {
            PRODUCTOFASE = new HashSet<PRODUCTOFASE>();
            PRODUCTOBINARIO = new HashSet<PRODUCTOBINARIO>();
            PRODUCTOCOMPONENTE = new HashSet<PRODUCTOCOMPONENTE>();
            REPROCESOFAB = new HashSet<REPROCESOFAB>();
            T_TEST = new HashSet<T_TEST>();
            T_TESTREPORTVALOR = new HashSet<T_TESTREPORTVALOR>();
            T_TESTSEQUENCEFILE = new HashSet<T_TESTSEQUENCEFILE>();
            T_VALORPARAMETROPRODUCTO = new HashSet<T_VALORPARAMETROPRODUCTO>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPRODUCTO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VERSION { get; set; }

        public DateTime? FECHAVERSION { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACION { get; set; }

        [StringLength(1)]
        public string ACTIVO { get; set; }

        [StringLength(10)]
        public string IDALMACENPRODUCCION { get; set; }

        [StringLength(1600)]
        public string NOTASVERSION { get; set; }

        public int? MINIMOFABRICACION { get; set; }

        public int? NUMSOLDADURAS { get; set; }

        [StringLength(1600)]
        public string NOTAS { get; set; }

        [Required]
        [StringLength(10)]
        public string IDGFH { get; set; }

        public decimal? TIEMPOFABRICACION { get; set; }

        [Required]
        [StringLength(1)]
        public string PRESERIE { get; set; }

        public DateTime? FECULTREVPRECIO { get; set; }

        [Required]
        [StringLength(1)]
        public string AVISOMARCAJEFASE { get; set; }

        [Required]
        [StringLength(1)]
        public string AUTOPARTE { get; set; }

        [Required]
        [StringLength(1)]
        public string PRODUCTODECOMPRA { get; set; }

        public int? UDSCUBETA { get; set; }

        [StringLength(20)]
        public string ENTRAMADO { get; set; }

        [Required]
        [StringLength(1)]
        public string TRAZABILIDAD { get; set; }

        public int? LOTEOPTIMO { get; set; }

        [Required]
        [StringLength(3)]
        public string IDFAMILIA { get; set; }

        public int? NUMFAMILIA { get; set; }

        [StringLength(10)]
        public string LISTADISTRIBAVISOMARC { get; set; }

        [Required]
        [StringLength(1)]
        public string AVISOPREPROD { get; set; }

        [Required]
        [StringLength(1)]
        public string MATRICULASHIJAS { get; set; }

        [StringLength(10)]
        public string EMPLEADOVALIDAPRODUC { get; set; }

        [StringLength(3000)]
        public string NOTASVALIDAPRODUC { get; set; }

        [StringLength(10)]
        public string EMPLEADORESPONSABLE { get; set; }

        [StringLength(10)]
        public string EMPLEADOVALIDAID { get; set; }

        [StringLength(1000)]
        public string NOTASVALIDAID { get; set; }

        [StringLength(3)]
        public string IDTEST { get; set; }

        public int? NUMDIRECCION { get; set; }

        [Required]
        [StringLength(1)]
        public string ESPEJOSMD { get; set; }

        public int? NUMDOCVALIDA { get; set; }

        [StringLength(1)]
        public string CARASSINO { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTOFASE> PRODUCTOFASE { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTOBINARIO> PRODUCTOBINARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCTOCOMPONENTE> PRODUCTOCOMPONENTE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPROCESOFAB> REPROCESOFAB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TEST> T_TEST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTREPORTVALOR> T_TESTREPORTVALOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTSEQUENCEFILE> T_TESTSEQUENCEFILE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_VALORPARAMETROPRODUCTO> T_VALORPARAMETROPRODUCTO { get; set; }
    }
}
