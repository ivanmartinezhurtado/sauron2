﻿using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Laser;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(LaserActionPreparationnDescription))]
    public class LaserActionPreparation : ActionBase
    {
        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null)
                Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_ES_NULA("Falta iniciar una función de carga de datos de contexto del test (InitProduct)").Throw();

            if (data.NumOrden != null && data.NumProducto == 0)
                Error().PROCESO.PARAMETROS_ERROR.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test").Throw();

            List<LaserItemViewModel> laserItem;

            using (DezacService svc = new DezacService())
            {
                laserItem = svc.GetDatosLaser(data.NumProducto, data.Version, data.IdFase);
            }

            var laser = new LaserPreparation();
            laser.InformationShow(laserItem);

        }
    }

    public class LaserActionPreparationnDescription : ActionDescription
    {
        public override string Name { get { return "LaserInformation"; } }
        public override string Category { get { return "DezacTest/Laser"; } }
        public override string Description { get { return "Información sobre si el láser se ha inicializado correctamente o no"; } }
        public override Image Icon { get { return Properties.Resources.Industry_Laser_Beam_icon; } }
    }
}
