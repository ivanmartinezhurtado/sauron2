﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Telerik.WinControls.UI;

namespace WinTestPlayer.Controls.Expanders
{
    public class DemoAppVisualListElement : RadListVisualItem
    {
        LightVisualElement betaCtpNew;
        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            betaCtpNew = new LightVisualElement();
            betaCtpNew.StretchHorizontally = false;
            betaCtpNew.StretchVertically = false;
            betaCtpNew.Alignment = ContentAlignment.MiddleRight;
            betaCtpNew.Margin = new System.Windows.Forms.Padding(0, 0, 13, 0);

            this.Children.Add(betaCtpNew);
        }

        public override void Synchronize()
        {
            base.Synchronize();

            RadListDataItem customItem = this.Data as RadListDataItem;

            //if (customItem.CtpControl)
            //{
            //    betaCtpNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            //}
            //else if (customItem.BetaControl)
            //{
            //    betaCtpNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            //}
            //else if (customItem.NewExample)
            //{
            //    betaCtpNew.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            //}
            //else
            //{
                betaCtpNew.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            //}
        }

        protected override Type ThemeEffectiveType
        {
            get
            {
                return typeof(RadListVisualItem);
            }
        }
    }
}
