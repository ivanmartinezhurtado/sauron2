﻿using System.Collections.Generic;

namespace Dezac.Tests.Services
{
    public interface IEnvironmentService
    {
        Dictionary<string, object> Environment { get; }
    }
}
