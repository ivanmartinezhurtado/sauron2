﻿using Comunications.Message;
using Comunications.Protocolo;
using Comunications.ST_DFU;
using Comunications.Utility;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Threading;

namespace Comunications.ST7590
{
    public class ST7590_Upgrade: IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger("ST7590_Upgrade");

        private const byte ERR_NOERROR = 0x00;
        private const byte ERR_WCMD = 0x01;
        private const byte ERR_SER = 0x10;
        private const byte ERR_SET_BAUD = 0x11;
        private const byte ERR_FLASH_NOT_PRESENT = 0x30;
        private const byte ERR_FLASH_BUSY = 0x31;
        private const byte ERR_FLASH_RW = 0x33;

        private int TimeOut = 2000;
        private int Reintentos = 3;

        private MessageTransport transport;
        private SerialPort sp;
        private BinaryReader binary;
        private Thread thread;

        public event FirmwareUpdateProgressEventHandler OnFirmwareUpdateProgress;

        public ST7590_Upgrade(int port)
        {
            SetPort(port);
        }

        protected MessageTransport Transport
        {
            get
            {
                if (transport == null)
                {
                    var protocol = new ASCIIProtocol
                    {
                        CaracterFinRx = string.Empty,
                        CaracterFinTx = string.Empty,
                    };

                    transport = new MessageTransport(protocol, new SerialPortAdapter(sp), logger);
                    transport.ReadTimeout = TimeOut;
                    transport.Retries = Reintentos;
                    transport.LogRawMessage = false;
                }

                return transport;
            }
        }

        protected BinaryReader Binary
        {
            get
            {
                if(binary == null)
                    binary = new BinaryReader();

                return binary;
            }
        }

        public void SetPort(int port)
        {
            if (sp == null)
                sp = new SerialPort("COM" + port, 57600, Parity.None, 8, StopBits.One);
            else
                sp.PortName = "COM" + port;
        }

        public int LoadBinaryFile(string filePath)
        {
            UpdateProgress(1, "Load file *.COD");
            Binary.Load(filePath);
            return Binary.MaxRecords;
        }

        public void UpgradeThread()
        {
            thread = new Thread(Upgrade);
            thread.Name = "Firmware update thread";
            thread.Start();
        }

        public void Upgrade()
        {
            UpdateProgress(2, "Erase memory sector");
            EraseSectorSerialMem();

            var i = 0;
            foreach (BinaryRecord record in binary.Records)
            {
                UpdateProgress(i++, String.Format("Write record address: {0}   Record {1} / {2} with Size {3}", record.RecordAddress, i, binary.MaxRecords, record.RecordLength ));
                WriteBinaryRecord(record);
            }

            var val = new byte[] { 0xF8 };
            WriteBinaryRecord(0, 0x00000003, 0x01, val);
        }

        private void WriteBinaryRecord(BinaryRecord record)
        {
            WriteBinaryRecord(0, record.RecordAddress, record.RecordLength, record.Data);
        }

        private void WriteBinaryRecord(byte sector, int startAddres, int numBytes, byte[] txDataBuffer)
        {
            try
            {
                var Frame = new List<byte>();
                Frame.Insert(0, 0x16);
                Frame.Insert(1, 0x16);
                Frame.Insert(2, 0x31);

                //U16ToBuf(11 + numBytes, &Frame[3], false);
                Frame.AddRange(IPAddress.NetworkToHostOrder(Convert.ToInt16(11 + numBytes)).ToBytes());

                Frame.Insert(5, 0x00);

                //U24ToBuf(startAddres, &Frame[6], false);
                Frame.AddRange(IPAddress.NetworkToHostOrder(Convert.ToInt32(startAddres)).ToBytes());
                Frame.RemoveAt(9);
                //U16ToBuf(numBytes, &Frame[9], false);
                Frame.AddRange(IPAddress.NetworkToHostOrder(Convert.ToInt16(numBytes)).ToBytes());

                //memcpy(&Frame[11], txDataBuffer, numBytes);
                Frame.AddRange(txDataBuffer);

                //U32ToBuf(crc, &Frame[11 + numBytes], false);
                Frame.AddRange(IPAddress.NetworkToHostOrder((Int32)(CalculateCrc32(txDataBuffer))).ToBytes());

                var res = SendMessage(Frame.ToArray());
                if (res != "00")
                {
                    logger.InfoFormat("ERROR UPGRADE: Adress:{0} bytes blocks transferred", startAddres);
                    TestException.Create().UUT.COMUNICACIONES.UPGRADE(string.Format("Adress: {0}", startAddres)).Throw();
                }
            }
            catch (Exception ex)
            {
                logger.InfoFormat("UPGRADE: Error transferring data to serial memory. Error number:{0}", ex.Message);
                TestException.Create().UUT.COMUNICACIONES.BORRADO(ex.Message).Throw();
            }
        }

        private void EraseSectorSerialMem()
        {
            try
            {
                var Frame = new byte[5];
                Frame[0] = 0x16;
                Frame[1] = 0x16;
                Frame[2] = 0x32;
                Frame[3] = 0x02;
                Frame[4] = 0;
                var res = SendMessage(Frame);
                if (res == "00")
                    logger.Info("UPGRADE: Sector erase OK.");
                else
                {
                    logger.InfoFormat("UPGRADE: Error erasing sector. Error number: {0}", res);
                    TestException.Create().UUT.COMUNICACIONES.BORRADO("sector 0").Throw();
                }
            }
            catch (Exception ex)
            {
                logger.InfoFormat("UPGRADE: Error erasing sector. Exception by: {0}", ex.Message);
                TestException.Create().UUT.COMUNICACIONES.BORRADO(ex.Message).Throw();
            }
        }

        private string SendMessage(byte[] data, bool HasResponse = true)
        {
            var message = new HexStringMessage();
            message.Text = data.BytesToHexString();
            message.HasResponse = HasResponse;
            if (HasResponse)
            {
                message.Validator = (m) => { return m.Text.StartsWith("00"); };
                var resp = Transport.SendMessage<HexStringMessage>(message);
                return resp.Text;
            }
            else
            {
                Transport.SendMessage<HexStringMessage>(message);
                return "0";
            }
        }

        protected static uint CalculateCrc32(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            uint crc = uint.MaxValue;

            foreach (byte b in data)
            {
                byte tableIndex = (byte)((crc ^ b) & 0xFF);
                crc >>= 8;
                crc ^= CRC32Tab[tableIndex];
            }

            return (crc ^ 0xFFFFFFFF);
        }


        protected static uint[] CRC32Tab = new uint[]
     {
            0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
            0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988, 0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
            0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
            0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
            0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
            0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
            0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
            0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924, 0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
            0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
            0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
            0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
            0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
            0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
            0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
            0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
            0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
            0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
            0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
            0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
            0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
            0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
            0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
            0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236, 0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
            0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
            0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
            0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38, 0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
            0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
            0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
            0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
            0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
            0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
            0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
     };

        protected void UpdateProgress(float progress, string message, bool failed = false)
        {
            if (failed)
                logger.WarnFormat("FAILED UPGRADE:{0} Progress:{1}", message, progress);
            else
                logger.InfoFormat("UPGRADE:{0} Progress:{1}", message, progress);

            if (OnFirmwareUpdateProgress != null)
                OnFirmwareUpdateProgress(this, new FirmwareUpdateProgressEventArgs(progress, message, failed));
        }


        protected class BinaryRecord
        {
            public int RecordAddress { get; set; }
            public int RecordLength { get; set; }
            public byte[] Data { get; set; }
        }

        protected class BinaryReader 
        {
            const int BLOCK_SIZE_WRITE = 256;

            private List<BinaryRecord> records;

            public List<BinaryRecord> Records { get { return records; } }

            public byte[] ReadRecord(int recordNumber)
            {
                return records[recordNumber].Data;
            }

            public int RecordsCount
            {
                get { return records.Count; }
            }

            public string FileName { get; protected set; }

            public int MaxRecords { get; protected set; }

            public void Load(string filePath)
            {
                FileName = filePath;

                if (!File.Exists(FileName))
                    throw new Exception(string.Format("Binary file {0} not found!", FileName));

                try
                {
                    ReadRecords(File.ReadAllBytes(FileName));
                    logger.Info("UPGRADE: File open OK.");
                }
                catch (Exception ex)
                {
                    logger.InfoFormat("UPGRADE: Error opening file {0} by {1}", filePath, ex.Message);
                    TestException.Create().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE(filePath).Throw();
                }
            }

            protected void ReadRecords(byte[] data)
            {
                MaxRecords = (data.Length / BLOCK_SIZE_WRITE);

                var lastRecord = 0;
                if (Math.IEEERemainder(data.Length, BLOCK_SIZE_WRITE) != 0)
                    lastRecord = 1;

                records = new List<BinaryRecord>(MaxRecords + lastRecord);

                using (var ms = new MemoryStream(data))
                {
                    while (records.Count < MaxRecords)
                    {
                        var record = new BinaryRecord();
                        record.RecordAddress = records.Count * BLOCK_SIZE_WRITE;
                        record.RecordLength = BLOCK_SIZE_WRITE;
                        record.Data = new byte[record.RecordLength];
                        var buffer = ms.Read(record.Data, 0, record.RecordLength);
                        records.Add(record);
                    }

                    if (lastRecord == 1)
                    {
                        var recordLast = new BinaryRecord();
                        recordLast.RecordAddress = records.Count * BLOCK_SIZE_WRITE;
                        recordLast.RecordLength = data.Length - (records.Count * BLOCK_SIZE_WRITE);
                        recordLast.Data = new byte[recordLast.RecordLength];
                        var bufferLast = ms.Read(recordLast.Data, 0, recordLast.RecordLength);
                        records.Add(recordLast);
                    }
                }

            }
        }

        public void Dispose()
        {
            if (Transport != null)
                Transport.Dispose();

            if (sp != null)
            {
                sp.Close();
                sp.Dispose();
            }
        }


    }
}
