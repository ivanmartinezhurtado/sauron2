﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.06)]
    [DesignerAction(Type = typeof(InitProcessActionActionDescription))]
    public class InitLaserAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Entrada que se quiere utilizar en la fase de Laseado")]
        [DefaultValue(2)]
        public int InputStart { get; set; }
        
        private string PathFilesLaser
        {
            get { return ConfigurationManager.AppSettings["PathFilesLaser"]; }
        }

        public override void Execute()
        {
            
            string numberOfProcess;
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("Tower").Throw();

            numberOfProcess = cacheSvc.Get<string>("NUMBERS_OF_PROCESS");
            if (numberOfProcess == null)
                numberOfProcess = iShell.ShowDialog<string>("INTRODUCE EL NUMERO DE PIEZAS DEL PROCESO", () =>
                    {
                        return new InputKeyBoard(string.Format("INTRODUCE EL NUMERO DE PIEZAS DEL PALET"));
                    }
                    , MessageBoxButtons.OKCancel, (c, d) =>
                    {

                        if (d == DialogResult.OK)
                            return ((InputKeyBoard)c).TextValue;

                        return null;
                    });

            tower.UsbIO.LogEnabled = false;

            var imageView = new ImageView("Pulsa el boton START para empezar a lasear", Properties.Resources.pushButton);

            SetVariable("LASER_FINISHED", false);
            SetVariable("LASER_PRINTING", false);
            var btn = cacheSvc.Get<bool>("PULSED_BUTTON");

            var frm = testBase.ShowForm(() => imageView, MessageBoxButtons.OK);
            try
            {
                do
                {
                    testBase.CheckCancellationRequested();
                    Thread.Sleep(50);
                } while (!tower.UsbIO.DI[InputStart] && !btn);
                
            }
            finally
            {
                testBase.CloseForm(frm);
            }

            cacheSvc.AddOrSet("NUMBERS_OF_PROCESS", numberOfProcess);
            cacheSvc.AddOrSet("PULSED_BUTTON", false);

            if (numberOfProcess == null)
                Error().PROCESO.OPERARIO.VALOR_INCORRECTO("No se ha especificado el número de piezas").Throw();           
        }

    }

    public class InitProcessActionActionDescription : ActionDescription
    {
        public override string Name { get { return "InitLaser"; } }
        public override string Category { get { return "DezacTest/Laser"; } }
        public override string Description { get { return "Acción para inicializar la fase de Láser en los equipos"; } }
        public override Image Icon { get { return Properties.Resources.Industry_Laser_Beam_icon; } }
    }
}
