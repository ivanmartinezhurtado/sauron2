﻿using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System;
using System.ComponentModel;
using System.Drawing;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ValidateDateTimeMaxMinDescription))]
    public class ValidateDateTimeMaxMin : ActionBase
    {
        //TODO: UNIFICAR en un action generico para fechas
        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInput { get; set; }

        //TODO: Propieada no necesaria, no creo que haga falta porque siempre son segungos. 
        [Category("4. Results")]
        [Description("Descripción de las unidades de la variable a obtener siempre estará en milisegundos o segundos")]
        public ParamUnidad Unit { get; set; }

        [Category("3.1 Margins")]
        [Description("Margen tiempo máximo del test")]
        public ActionVariable Maximum { get; set; } = new ActionVariable();

        [Category("3.1 Margins")]
        [Description("Margen tiempo mínimo del test")]
        public ActionVariable Minimum { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            TimeSpan timeDiff;
            TimeSpan maxDeviationTime;
            TimeSpan minDeviationTime;

            if (Unit != 0)
                timeDiff = (TimeSpan)VariablesTools.ResolveValue(DataInput, null, Unit);
            else
                timeDiff = (TimeSpan)VariablesTools.ResolveValue(DataInput);

            var name = DataInput.Trim().Replace("$", "");

            if (Maximum == null || Maximum.IsKeyNullOrEmpty)
            {
                if (Unit == 0)
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("FALTA UNIDAD PARA CREAR PARAMETRO EN BBDD").Throw();

                var value = testBase.Margenes.GetDouble(string.Format("{0}_MAX", name), 9999999, Unit);
                Maximum = string.Format("$Margenes.{0}_MAX", name);
            }
            if (Minimum == null || Minimum.IsKeyNullOrEmpty)
            {
                if (Unit == 0)
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("FALTA UNIDAD PARA CREAR PARAMETRO EN BBDD").Throw();

                var value = testBase.Margenes.GetDouble(string.Format("{0}_MIN", name), -9999999, Unit);
                Minimum = string.Format("$Margenes.{0}_MIN", name);
            }

            if (Unit == ParamUnidad.s)
            {
                maxDeviationTime = new TimeSpan(0, 0, Convert.ToInt32(Maximum.Value));
                minDeviationTime = new TimeSpan(0, 0, Convert.ToInt32(Minimum.Value));
            }
            else
            {
                maxDeviationTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(Maximum.Value));
                minDeviationTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(Minimum.Value));
            }

            Logger.InfoFormat("Variable:{0}  MIN:{1}s  Desviation:{2}s  MAX:{3}s", name, minDeviationTime, timeDiff, maxDeviationTime);

            if (Unit == ParamUnidad.s)
            {
                testBase.Resultado.Set(name, timeDiff.TotalSeconds, Unit, minDeviationTime.TotalSeconds, maxDeviationTime.TotalSeconds);

                if (timeDiff.TotalSeconds > maxDeviationTime.TotalSeconds)
                    Error().UUT.MEDIDA.MARGENES(string.Format("{0} MAYOR", name)).Throw();

                if (timeDiff.TotalSeconds < minDeviationTime.TotalSeconds)
                    Error().UUT.MEDIDA.MARGENES(string.Format("{0} MENOR", name)).Throw();
            }
            else
            {
                testBase.Resultado.Set(name, timeDiff.TotalMilliseconds, Unit, minDeviationTime.TotalMilliseconds, maxDeviationTime.TotalMilliseconds);

                if (timeDiff.TotalMilliseconds > maxDeviationTime.TotalMilliseconds)
                    Error().UUT.MEDIDA.MARGENES(string.Format("{0} MAYOR", name)).Throw();

                if (timeDiff.TotalMilliseconds < minDeviationTime.TotalMilliseconds)
                    Error().UUT.MEDIDA.MARGENES(string.Format("{0} MENOR", name)).Throw();
            }
        }
    }

    public class ValidateDateTimeMaxMinDescription : ActionDescription
    {
        public override string Name { get { return "ValidateDateTimeMaxMin"; } }
        public override string Description { get { return "Acción genérica para comparar variables o resultados de DateTime con los márgenes establecidos como Máximos y Mínimos de tiempo"; } }
        public override string Category { get { return "Actions/Variables/Validate"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
    }
}
