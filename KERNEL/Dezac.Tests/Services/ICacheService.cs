﻿using System;
using System.Linq.Expressions;

namespace Dezac.Tests.Services
{
    public interface ICacheService
    {
        T Add<T>(string key, T objectToCache);
        T Add<T>(string key, Func<T> loadFunc, TimeSpan cacheFor);
        T Get<T>(string key);
        bool HasValidKey(string key);
        void Remove(string key);
        void Clear();
        object Get(string key);
        bool TryGet<T>(string key, out T cachedObject);
        bool TryAdd<T>(string key, Expression<Func<T>> cacheItemProvider, out T cachedObject);
        T AddOrSet<T>(string key, T objectToCache);
        T AddOrSet<T>(string key, Func<T> loadFunc, TimeSpan cacheFor);
    }
}
