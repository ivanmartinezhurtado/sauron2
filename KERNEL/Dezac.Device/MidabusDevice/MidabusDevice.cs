﻿using Comunications.Message;
using System;
using System.Globalization;
using System.Reflection;
using System.Text;
using Extensions = Comunications.Utility.Extensions;

namespace Dezac.Device
{
    public class MidabusDevice : IDisposable
    {
        protected MidabusTransport transport;
        private byte[] perifericNumber;

        public MidabusDevice()
        {
            Cultura = new CultureInfo("en-US");
        }

        public MidabusDevice(MidabusTransport Transport)
        {
            transport = Transport;
            Cultura = new CultureInfo("en-US");
        }

        public MidabusTransport Transport
        {
            get { return transport; }
            set { transport = value; }
        }

        public CultureInfo Cultura { get; set; }

        public byte PerifericNumber { set { perifericNumber = ASCIIEncoding.ASCII.GetBytes(value.ToString("00")); } get { return Convert.ToByte(ASCIIEncoding.ASCII.GetString(perifericNumber)); } }

        public int TimeOut { set { transport.ReadTimeout = value; } get { return transport.ReadTimeout; } }

        public int Retries { set { transport.Retries = value; } get { return transport.Retries; } }

        public int WaitToRetryMilliseconds { set { transport.WaitToRetryMilliseconds = value; } get { return transport.WaitToRetryMilliseconds; } }

        public T ReadStrings<T>(string message, string response) where T : struct
        {
            byte[] data = transport.Read(perifericNumber, message, response);
            var trama = ASCIIEncoding.ASCII.GetString(data).Trim().Remove(0, response.Length);
            var charTrama = trama.ToCharArray();
            var result = "";
            for (var i = 0; i < charTrama.Length; i+=2)
            {
                var p = i;
                var pos = string.Format("{0}{1}", charTrama[p].ToString(), charTrama[p++].ToString());
                var posByte = byte.Parse(pos, NumberStyles.HexNumber);
                result += string.Format("{0} ", posByte);
            }
            return Extensions.AsignedTramaToStructure<T>(result);
        }

        public T ReadStruct<T>(string message, string response) where T : struct
        {
            byte[] data = transport.Read(perifericNumber, message, response);
            var trama = ASCIIEncoding.ASCII.GetString(data).Trim().Remove(0, response.Length);
            var charTrama = trama.ToCharArray();
            var result = "";
            var p = 0;

            T toStruc = new T();
            object obj = (T)toStruc;
            FieldInfo[] fi = typeof(T).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo info in fi)
            {
                var posString = "";

                if (info.FieldType.Name == "Int32" || info.FieldType.Name == "UInt32")
                {
                    var pos = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString());                   
                    var posint = Int32.Parse(pos, NumberStyles.HexNumber);
                    posString = posint.ToString();
                }

                if (info.FieldType.Name == "Int16" || info.FieldType.Name == "UInt6")
                {
                    var pos = string.Format("{0}{1}{2}{3}", charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString(), charTrama[p++].ToString());
                    var posshort = Int16.Parse(pos, NumberStyles.HexNumber);
                    posString = posshort.ToString();
                }

                if (info.FieldType.Name == "Byte" || info.FieldType.Name == "String")
                {
                    var pos = string.Format("{0}{1}", charTrama[p++].ToString(), charTrama[p++].ToString());
                    var posbyte = Byte.Parse(pos, NumberStyles.HexNumber);
                    posString = posbyte.ToString();
                }

                result += string.Format("{0} ", posString);
            }

            return Extensions.AsignedTramaToStructure<T>(result);
        }

        public string Read(string message, string response)
        {
            byte[] data = transport.Read(perifericNumber, message, response);
           return ASCIIEncoding.ASCII.GetString(data);
        }

        public void WriteWhitoutResponse(string message)
        {
            transport.WriteWithoutResponse(perifericNumber, message);
        }

        public void Write(string message, string response, params object[] param)
        {
            var msg = string.Format(Cultura, message, param);
            transport.Write(perifericNumber, msg, response);
        }

        public void Write(string message)
        {
            transport.Write(perifericNumber, message);
        }

        public virtual void Dispose()
        {
            Transport.Dispose();
        }

        public virtual void ClosePort()
        {

        }
    }
}
