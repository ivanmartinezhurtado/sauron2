﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class COMPACT_DISPENSER : DeviceBase
    {
        public ModbusDevice Modbus { get; internal set; }

        public COMPACT_DISPENSER()
        {
        }

        public COMPACT_DISPENSER(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            SerialPort sp = new SerialPort("COM" + port, baudRate, System.IO.Ports.Parity.None, 8, StopBits.One);

            sp.DtrEnable = true;

            Modbus = new ModbusDeviceSerialPort(sp, _logger);
            Modbus.PerifericNumber = periferic;
            
        }
        
        #region Modbus reading operations

        public ushort ReadModbusAddress()
        {
            return Modbus.ReadHoldingRegister((ushort)Registers.MODBUS_ADDRESS);
        }

        public int ReadSerialNumberReadOnly()
        {
            return Modbus.ReadHoldingRegister((ushort)Registers.SERIAL_NUMBER_READ_ONLY);
        }

        public int ReadSerialNumber()
        {
            return Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public int ReadModel()
        {
            return Modbus.ReadInt32((ushort)Registers.MODEL);
        }

        public int ReadFabricationNumber()
        {
            return Modbus.ReadInt32((ushort)Registers.PRODUCTION_NUMBER);
        }

        public int ReadProductionStatus()
        {
            return Modbus.ReadRegister((ushort)Registers.PRODUCTION_STATUS);
        }

        public ushort ReadVoltageGain()
        {
            return Modbus.Read<ushort>((ushort)Registers.VOLTAGE_GAIN_FACTOR);
        }

        public ushort ReadActivePower50HzGain()
        {
            return Modbus.ReadHoldingRegister((ushort)Registers.ACTIVE_POWER_50HZ_GAIN_FACTOR);
        }

        public ushort ReadReactivePower50HzGain()
        {
            return Modbus.Read<ushort>((ushort)Registers.REACTIVE_POWER_50HZ_GAIN_FACTOR);
        }

        public ushort ReadActivePower50HzOffset()
        {
            return Modbus.Read<ushort>((ushort)Registers.ACTIVE_POWER_50HZ_OFFSET);
        }

        public ushort ReadReactivePower50HzOffset()
        {
            return Modbus.Read<ushort>((ushort)Registers.REACTIVE_POWER_50HZ_OFFSET);
        }

        public short ReadPhaseGap50Hz()
        {
            return Modbus.Read<short>((ushort)Registers.PHASE_GAP_50HZ);
        }

        public ushort ReadActivePower60HzGain()
        {
            return Modbus.Read<ushort>((ushort)Registers.ACTIVE_POWER_60HZ_GAIN_FACTOR);
        }

        public ushort ReadReactivePower60HzGain()
        {
            return Modbus.Read<ushort>((ushort)Registers.REACTIVE_POWER_50HZ_GAIN_FACTOR);
        }

        public short ReadPhaseGap60Hz()
        {
            return Modbus.Read<short>((ushort)Registers.PHASE_GAP_60HZ);
        }

        public ushort ReadInput()
        {
            return Modbus.Read<ushort>((ushort)Registers.INPUT_STATUS);
        }

        public ElectricalVariables ReadElectricalVariables()
        {
            var electricalVariables = new ElectricalVariables();

            electricalVariables.ActiveEnergyExported = Modbus.ReadInt32((ushort)Registers.ACTIVE_ENERGY_EXPORTED);
            electricalVariables.ActiveEnergyImported = Modbus.ReadInt32((ushort)Registers.ACTIVE_ENERGY_IMPORTED);
            electricalVariables.ActivePower = Modbus.ReadInt32((ushort)Registers.ACTIVE_POWER);
            electricalVariables.ApparentPower= Modbus.ReadInt32((ushort)Registers.APPARENT_POWER);
            electricalVariables.Current = Modbus.ReadInt32((ushort)Registers.CURRENT);
            electricalVariables.ReactiveEnergyQ1= Modbus.ReadInt32((ushort)Registers.REACTIVE_ENERGY_Q1);
            electricalVariables.ReactiveEnergyQ2= Modbus.ReadInt32((ushort)Registers.REACTIVE_ENERGY_Q2);
            electricalVariables.ReactiveEnergyQ3= Modbus.ReadInt32((ushort)Registers.REACTIVE_ENERGY_Q3);
            electricalVariables.ReactiveEnergyQ4= Modbus.ReadInt32((ushort)Registers.REACTIVE_ENERGY_Q4);
            electricalVariables.ReactivePower= Modbus.ReadInt32((ushort)Registers.REACTIVE_POWER);
            electricalVariables.Voltage= Modbus.ReadInt32((ushort)Registers.VOLTAGE);

            return electricalVariables;
        }

        public ConverterPoints ReadConverterPoints()
        {
            var converterPoints = new ConverterPoints();

            converterPoints.Voltage = Modbus.ReadInt32((ushort)Registers.CONVERTER_POINTS_VOLTAGE);
            converterPoints.Current = Modbus.ReadInt32((ushort)Registers.CONVERTER_POINTS_CURRENT);
            converterPoints.ActivePower = Modbus.ReadInt32((ushort)Registers.CONVERTER_POINTS_ACTIVE_POWER);
            converterPoints.ReactivePower = Modbus.ReadInt32((ushort)Registers.CONVERTER_POINTS_REACTIVE_POWER);

            return converterPoints;
        }

        public string ReadFirmwareVersion()
        {
            var version = Modbus.ReadMultipleRegister((ushort)Registers.FIRMWARE_VERSION, 3);

            return string.Format("{0}.{1}.{2}", version[0], version[1], version[2]);
        }

        #endregion
        
        #region Modbus writting operations

        public void WriteModbusAddress(ushort address)
        {
            Modbus.Write((ushort)Registers.MODBUS_ADDRESS, address);
        }

        public void WriteSerialNumber(int serialNumber)
        {
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, serialNumber);
        }

        public void WriteModel(string model)
        {
            if (model.Length > 12)
                throw new Exception("el modelo introducido excede el máximo de caracteres permitido");
            Modbus.WriteString((ushort)Registers.MODEL, model);
        }

        public void WriteDisplay(LCDConfigurations configuration)
        {
            Modbus.Write<LCDSegments>(LCDSegmentsConfiguration[configuration]);
        }

        public void WriteVoltageGains(ushort gains)
        {
            Modbus.Write<ushort>((ushort)Registers.VOLTAGE_GAIN_FACTOR, gains);
        }

        public void WriteActivePower50HzGains(ushort gains)
        {
            Modbus.Write<ushort>((ushort)Registers.ACTIVE_POWER_50HZ_GAIN_FACTOR, gains);
        }

        public void WriteReactivePower50HzGains(ushort gains)
        {
            Modbus.Write<ushort>((ushort)Registers.REACTIVE_POWER_50HZ_GAIN_FACTOR, gains);
        }

        public void WriteActivePower50HzOffset(ushort gains)
        {
            Modbus.Write<int>((ushort)Registers.ACTIVE_POWER_50HZ_OFFSET, gains);
        }

        public void WriteReactivePower50HzOffset(ushort gains)
        {
            Modbus.Write<int>((ushort)Registers.REACTIVE_POWER_50HZ_OFFSET, gains);
        }

        public void WritePhaseGap50Hz(short gains)
        { 
            Modbus.Write<short>((ushort)Registers.PHASE_GAP_50HZ, gains);
        }

        public void WriteActivePower60HzGains(ushort gains)
        {
            Modbus.Write<ushort>((ushort)Registers.ACTIVE_POWER_60HZ_GAIN_FACTOR, gains);
        }

        public void WriteReactivePower60HzGains(ushort gains)
        {
            Modbus.Write<ushort>((ushort)Registers.REACTIVE_POWER_60HZ_GAIN_FACTOR, gains);
        }

        public void WritePhaseGap60Hz(short gains)
        {
            Modbus.Write<short>((ushort)Registers.PHASE_GAP_60HZ, gains);
        }

        public void WriteImpulseLedWeight(State state)
        {
            Modbus.Write((ushort)Registers.IMPULSE_LED, (ushort)state);
        }

        public void WriteImpulseLedTrain()
        {
            Modbus.Write((ushort)Registers.IMPULSE_LED, 8);
        }

        public void WriteInformationReset()
        {
            Modbus.WriteSingleCoil((ushort)Registers.SERIAL_NUMBER, true);
        }

        public void WriteModbusKey(int key)
        {
            Modbus.WriteInt32((ushort)Registers.MODBUS_KEY, key);
        }

        public void WriteCodeKey(int key)
        {
            Modbus.WriteInt32((ushort)Registers.CODE_KEY, key);
        }

        public void WriteDailyEnergyBase(ushort dailyEnergy)
        {
            Modbus.Write((ushort)Registers.DAILY_ENERGY_BASE, dailyEnergy);
        }

        public void WritePowerBase(ushort power)
        {
            Modbus.Write((ushort)Registers.POWER_BASE, power);
        }

        public void WriteLastCodeDate(int date)
        {
            Modbus.WriteInt32((ushort)Registers.LAST_CODE_DATE, date);
        }
         
        public void WriteContractedDays(ushort days)
        {
            Modbus.Write((ushort)Registers.CONTRACTED_DAYS, days);
        }

        public void WriteBaseEnergyTime(ushort time)
        {
            Modbus.Write((ushort)Registers.BASE_ENERGY_TIMES, time);
        }

        public void WriteBasePowerTime(ushort time)
        {
            Modbus.Write((ushort)Registers.BASE_POWER_TIMES, time);
        }

        public void WriteSwitchReconTime(ushort time)
        {
            Modbus.Write((ushort)Registers.SWITCH_RECON_TIME, time);
        }

        public void WriteWorkingSeconds(int seconds)
        {
            Modbus.Write((ushort)Registers.DAY_WORKING_SECONDS, seconds);
        }

        public void WriteProductionNumber(int productionNumber)
        {
            Modbus.Write((ushort)Registers.PRODUCTION_NUMBER, productionNumber);
        }

        public void WriteProductionStatus(ushort status)
        {
            Modbus.Write((ushort)Registers.PRODUCTION_STATUS, status);
        }

        public void WriteOutputToActivate(OutputActivations output)
        {
            Modbus.Write((ushort)Registers.OUTPUT_TEST, (ushort)output);
        }

        #endregion

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Dictionarys

        public Dictionary<LCDConfigurations, LCDSegments> LCDSegmentsConfiguration = new Dictionary<LCDConfigurations, LCDSegments>()
        {
            {LCDConfigurations.PAIR_PINS, new LCDSegments(0xA050, 0xA050, 0xA050, 0xA050, 0xA050, 0xA050, 0xA050, 0xA050, 0xA050, 0xA050)},
            {LCDConfigurations.IMPAIR_PINS, new LCDSegments(0x0A05, 0x0A05, 0x0A05, 0x0A05, 0x0A05, 0x0A05, 0x0A05, 0x0A05, 0x0A05, 0x0A05)},
            {LCDConfigurations.ALL, new LCDSegments(0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF)},
            {LCDConfigurations.NONE, new LCDSegments(0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000)},
        };

        public Dictionary<LCDConfigurations, string> LCDConfigurationsTranslation = new Dictionary<LCDConfigurations, string>()
        {
            {LCDConfigurations.PAIR_PINS, "pines pares del LCD"},
            {LCDConfigurations.IMPAIR_PINS, "pines impares del LCD"},
            {LCDConfigurations.ALL, "todos los pines del lcd"},
            {LCDConfigurations.NONE, "ningun pin del lcd"},
        };

        #endregion

        #region Structs

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ACTIVE_ENERGY_IMPORTED)]
        public struct ElectricalVariables
        {
            public int ActiveEnergyImported;
            public int ActiveEnergyExported;
            public int ReactiveEnergyQ1;
            public int ReactiveEnergyQ2;
            public int ReactiveEnergyQ3;
            public int ReactiveEnergyQ4;
            public int Voltage;
            public int Current;
            public int ActivePower;
            public int ReactivePower;
            public int ApparentPower;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Power
        {
            public TriLineValue Phases;
            public int Total;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConverterPoints
        {
            public Int32 Voltage;
            public Int32 ActivePower;
            public Int32 ReactivePower;
            public Int32 Current;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.DISPLAY)]
        public struct LCDSegments
        {
            public LCDSegments(ushort group1, ushort group2, ushort group3, ushort group4, ushort group5, ushort group6, ushort group7, ushort group8, ushort group9, ushort group10)
            {
                Group1 = group1;
                Group2 = group2;
                Group3 = group3;
                Group4 = group4;
                Group5 = group5;
                Group6 = group6;
                Group7 = group7;
                Group8 = group8;
                Group9 = group9;
                Group10 = group10;
            }

            public ushort Group1;
            public ushort Group2;
            public ushort Group3;
            public ushort Group4;
            public ushort Group5;
            public ushort Group6;
            public ushort Group7;
            public ushort Group8;
            public ushort Group9;
            public ushort Group10;
        }

        #endregion
        
        #region Enumerators

        public enum State
        {
            OFF,
            ON
        }

        public enum LCDConfigurations
        {
            PAIR_PINS,
            IMPAIR_PINS,
            ALL,
            NONE
        }

        public enum OutputActivations
        {
            NONE = 0x0000,
            OUTPUT_LED = 0x0001,
            OUTPUT_BICOLOR_RED_LED = 0x0002,
            OUTPUT_BICOLOR_GREEN_LED = 0x0004,
            OUTPUT_RELAY_CLOSE = 0x0008,
            OUTPUT_RELAY_OPEN = 0x0010,
        }

        public enum Registers
        {
            MODBUS_ADDRESS = 0x0008,
            INPUT_STATUS = 0x0020,
            CODE_KEY = 0x0200, // 2 REGISTROS
            DAILY_ENERGY_BASE = 0x0202,
            POWER_BASE = 0x0203,
            LAST_CODE_DATE = 0x0204, // 2 REGISTROS
            CONTRACTED_DAYS = 0x0206,
            BASE_ENERGY_TIMES = 0x0207,
            BASE_POWER_TIMES = 0x0208,
            SWITCH_RECON_TIME = 0x0209,
            DAY_WORKING_SECONDS = 0x020A, // 2 REGISTROS
            VOLTAGE_GAIN_FACTOR = 0xF411,
            ACTIVE_POWER_50HZ_GAIN_FACTOR = 0xF421,
            REACTIVE_POWER_50HZ_GAIN_FACTOR = 0xF431,
            ACTIVE_POWER_50HZ_OFFSET = 0xF441, // 2 REGISTROS
            REACTIVE_POWER_50HZ_OFFSET = 0xF451, // 2 REGISTROS
            PHASE_GAP_50HZ = 0xF461,
            ACTIVE_POWER_60HZ_GAIN_FACTOR = 0xF471,
            REACTIVE_POWER_60HZ_GAIN_FACTOR = 0xF481,
            PHASE_GAP_60HZ = 0xF491,
            REMAINING_DAILY_ENERGY = 0x0110, // 2 REGISTROS
            REMAINING_TIME_NEXTLOAD = 0x0112, // 2 REGISTROS
            REMAINING_DAYS = 0x0114, // 2 REGISTROS
            ELAPSED_TIME_FIRST_WAKEUP = 0x0116, // 2 REGISTROS
            REMAINING_CONSUMPTION_TIME = 0x0118, // 2 REGISTROS
            ACTIVE_ENERGY_IMPORTED = 0x0708,
            ACTIVE_ENERGY_EXPORTED = 0x070A,
            REACTIVE_ENERGY_Q1 = 0x070C,
            REACTIVE_ENERGY_Q2 = 0x070E,
            REACTIVE_ENERGY_Q3 = 0x0710,
            REACTIVE_ENERGY_Q4 = 0x0712,
            VOLTAGE = 0x0732,
            CURRENT = 0x0738,
            ACTIVE_POWER = 0x0746,
            TOTAL_ACTIVE_POWER = 0x074C,
            REACTIVE_POWER = 0x074E,
            TOTAL_REACTIVE_POWER = 0x0754,
            APPARENT_POWER = 0x0756,
            TOTAL_ACTIVE_APPARENT = 0x075C,
            SERIAL_NUMBER = 0xF000, // 2 REGISTROS
            PRODUCTION_STATUS = 0xF060, // 2 REGISTROS
            PRODUCTION_NUMBER = 0xF040,
            IMPULSE_LED = 0xF110,
            DISPLAY = 0xF090,
            MODEL = 0xF010, // 5 REGISTROS
            OUTPUT_TEST = 0x0030,
            ELAPSED_SECONDS = 0x0000, // 2 REGISTROS
            SERIAL_NUMBER_READ_ONLY = 0x0060, // 2 REGISTROS
            FIRMWARE_VERSION = 0x0050, // 3 REGISTROS
            VALIDATION_CODE = 0xF800, // 3 REGISTROS
            FIRMWARE_CRC = 0xF120, // 2 REGISTROS
            CONVERTER_POINTS_VOLTAGE = 0xF311,
            CONVERTER_POINTS_ACTIVE_POWER = 0xF321,
            CONVERTER_POINTS_REACTIVE_POWER = 0xF331,
            CONVERTER_POINTS_CURRENT = 0xF341,
            MODBUS_KEY = 0x0102, // 2 REGISTROS
            GENERAL_RESET = 0xF000,
            SETUP_RESET = 0xF001,
            ENERGY_RESET = 0xF002,
            DISPENSER_RESET = 0xF003,
            UPDATE_MODE = 0xF123,
        }

        #endregion
    }
}
