﻿using System.Drawing;
using Dezac.Tests.AOI;
using System;
using System.ComponentModel;

namespace Dezac.Tests.Actions.AOI
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(AOIInspectionDescription))]
    [Browsable(false)]
    public class AOIinspection : ActionBase
    {
        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            WaitAllTestsAndRunOnlyOnce("_AOIINSPECTION_", () =>
            {
                SharedVariables.AddOrUpdate("AOIINSPECTION_ERROR", null);
                try
                {
                    SharedVariables.AddOrUpdate("AOIINSPECTION_RESULTS", HalconAOIExtension.AOIWeldingInspection(testBase));
                }
                catch (Exception e)
                {
                    SharedVariables.AddOrUpdate("AOIINSPECTION_ERROR", e);
                }
            });

            if (SharedVariables.Get("AOIINSPECTION_ERROR") != null)
                throw SharedVariables.Get("AOIINSPECTION_ERROR") as Exception;

            if (SharedVariables.Get("AOIINSPECTION_RESULTS") != null)
                HalconAOIExtension.AOISetTestResults(testBase);
            else
                testBase.Logger.Warn("El producto no tiene ROIs, no se guardaran resultados");
        }
    }

    public class AOIInspectionDescription : ActionDescription
    {
        public override string Name { get { return "AOIInspection"; } }
        public override string Category { get { return "DezacTest/Vision/AOI"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
    }
}
