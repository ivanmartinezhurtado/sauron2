﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace AssemblyResolve
{
    public class IsolatedInvoker
    {
        // main Invoke method
        public List<ClassVersion> Invoke(string assemblyFile)
        {
            List<ClassVersion> result;

            // get base path
            var appBasePath = Path.GetDirectoryName(assemblyFile);
            Debug.Assert(appBasePath != null);

            // change current directory
            var oldDirectory = Environment.CurrentDirectory;
            Environment.CurrentDirectory = appBasePath;
            try
            {
                // create new app domain
                var domain = AppDomain.CreateDomain(Guid.NewGuid().ToString(),null, appBasePath, null, true);
                try
                {
                    // create instance
                    var invoker = (InvokerHelper)domain.CreateInstanceFromAndUnwrap(Assembly.GetExecutingAssembly().Location, typeof(InvokerHelper).FullName);
                    if (invoker != null)
                    {
                        // invoke method
                        result = invoker.InvokeHelper(assemblyFile);

                        // process result
                        Debug.WriteLine(result);
                    }
                    else
                        result = null;
                }
                finally
                {
                    // unload app domain
                    AppDomain.Unload(domain);
                }
            }
            finally
            {
                // revert current directory
                Environment.CurrentDirectory = oldDirectory;
            }
            return result;
        }

        // This helper class is instantiated in an isolated app domain
        private class InvokerHelper : MarshalByRefObject
        {
            // This helper function is executed in an isolated app domain
            public List<ClassVersion> InvokeHelper(string assemblyFile)
            {
                try
                {
                    var typeName = new  ReflectionClassVersion().GetTypeAssemblyFirstClass(assemblyFile);
                    if (typeName == null)
                        return null;

                    // create an instance of the target object
                    var handle = Activator.CreateInstanceFrom(assemblyFile, typeName.FullName);

                    // get the instance of the target object
                    var instance = handle.Unwrap();

                    // get the type of the target object
                    var type = instance.GetType();

                    // invoke the method
                    var result = type.Assembly; // (methodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance, null, instance, parameters);

                    var clases = new ReflectionClassVersion().GetClasses(result);

                    var asmSource = AssemblyName.GetAssemblyName(assemblyFile);
                    var classVersionSource = new ClassVersion() { NameClass = asmSource.Name, Version = asmSource.Version.ToString() };
                    clases.Add(classVersionSource);

                    // success
                    return clases;
                }catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
