﻿using System;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class EKOR_TMP : DeviceBase
    {
        public ModbusDeviceSerialPort Modbus { get; set; }

        public EKOR_TMP()
        {
        }

        public EKOR_TMP(string port)
        {
            SetPort(port);
        }      

        public void SetPort(string port, byte periferic = 1, int bauRate = 9600)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(Convert.ToInt32(port.Replace("COM", "")), bauRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = Convert.ToInt32(port.Replace("COM", ""));
            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }
        
        public double ReadFirmwareVersion()
        {
            LogMethod();

            var high = Modbus.ReadHoldingRegister((ushort)Registers.FIRMWARE_VERSION_HIGH);
            var low = Modbus.ReadHoldingRegister((ushort)Registers.FIRMWARE_VERSION_LOW);

            return Convert.ToInt32(high + low) / 100d;
        }

        public double ReadTemperature()
        {
            LogMethod();
            return Modbus.ReadHoldingRegister((ushort)Registers.TEMPERATURE) / 100d;
        }

        public ushort ReadPerifericNumber()
        {
            LogMethod();
            return Modbus.ReadHoldingRegister((ushort)Registers.PERIFERIC_NUMBER);
        }

        public void WritePerifericNumber(ushort periferic)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PERIFERIC_NUMBER, periferic);
        }

        public void WriteCommands(CommandsEnum commands)
        {
            LogMethod();
            if (commands == CommandsEnum.RESET)
            {
                int modbusRetries = Modbus.Retries;
                Modbus.Retries = 0;
                try
                {
                    Modbus.Write((ushort)Registers.COMMANDS, (ushort)CommandsEnum.RESET);
                }
                catch
                {
                }
                Modbus.Retries = modbusRetries;
            }
            else
               Modbus.Write((ushort)Registers.COMMANDS, (ushort)commands);
        }

        public enum Registers
        {
            FIRMWARE_VERSION_HIGH = 0x0700,
            FIRMWARE_VERSION_LOW = 0x0701,
            TEMPERATURE = 0x0715,
            PERIFERIC_NUMBER = 0x0000,
            COMMANDS = 0x0100,
        }

        public enum CommandsEnum
        {
            GRABAR_AJUSTES = 1,
            MODO_MONITOR = 2,
            RESET = 3,
        }

        public void Dispose()
        {
            if (Modbus != null)
                Modbus.Dispose();

        }
    }
}
