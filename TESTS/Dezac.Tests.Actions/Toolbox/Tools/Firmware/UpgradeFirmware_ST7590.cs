﻿using Comunications.ST7590;
using Dezac.Data.ViewModels;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(UpgradeFirmware_ST7590Description))]
    public class UpgradeFirmware_ST7590 : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Búsqueda del manual del binario")]
        [DefaultValue(false)]
        public bool SearchManualBinary { get; set; }
        
        [Category("3. Configuration")]
        [Description("Fichero de extensión del binario")]
        [DefaultValue("COD")]
        public string ExtensionFileBinary { get; set; }

        [Description("Puerto serie a utilizar")]
        [Category("3. Configuration")]
        public ActionVariable PortName { get; set; } = new ActionVariable();


        [Description("Fichero de binario a grabar")]
        [Category("3. Configuration")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PathBinario { get; set; }

        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();

            string fileName = null;

            if (string.IsNullOrEmpty(PathBinario))
            {
                if (SearchManualBinary && SequenceContext.Current.RunMode == RunMode.Debug)
                    iShell.RunSync(() =>
                    {
                        var dlgOpenFile = new OpenFileDialog { Title = "Seleccionar Firmware" };
                        if (dlgOpenFile.ShowDialog() != DialogResult.OK)
                            Error().PROCESO.ACTION_EXECUTE.VERSION_FIRMWARE("Upgrade firmware process cancelled!").Throw();

                        fileName = dlgOpenFile.FileName;
                    });
                else
                {
                    var data = SequenceContext.Current.Services.Get<ITestContext>();

                    if (data == null)
                        Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("Falta iniciar una función de carga de datos de contexto del test (InitProduct)").Throw();

                    if (data.NumOrden != null && data.NumProducto == 0)
                        Error().PROCESO.PARAMETROS_ERROR.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test").Throw();

                    List<BinaryItemViewModel> binaryItem = new List<BinaryItemViewModel>();

                    if (SequenceContext.Current.Variables.Get("BinaryItemViewModel") is List<BinaryItemViewModel>)
                        binaryItem = (List<BinaryItemViewModel>)SequenceContext.Current.Variables.Get("BinaryItemViewModel");
                    else
                        binaryItem.Add(SequenceContext.Current.Variables.Get("BinaryItemViewModel") as BinaryItemViewModel);

                    if (binaryItem == null)
                        Error().SOFTWARE.BBDD.VARIABLE_NO_ENCONTRADA("No se ha podido obtener de las variables de contexto ningun BinaryItemViewModel").Throw();

                    var binary = binaryItem.Where(p => p.NombreFichero.ToUpper().Contains(ExtensionFileBinary)).FirstOrDefault();
                    if (binary == null)
                        Error().SOFTWARE.BBDD.VARIABLE_NO_ENCONTRADA("No se ha encontrado en la colección ningun binario con la extensión deseada").Throw();

                    fileName = binary.PathFicheroTest;
                }
            }
            else
                fileName = PathBinario;

            string errMsg = null;

            var port = Convert.ToInt32(PortName.Value.ToString());

            try
            {
                using (var firmwareUpdate = new ST7590_Upgrade(port))
                {
                    var maxrecords = firmwareUpdate.LoadBinaryFile(fileName);

                    var progress = iShell.ShowDialog("Upgrade Firmware in ST7590",
                    () =>
                    {
                        var c = new ProgressBarView();
                        c.MaxValue = maxrecords;

                        firmwareUpdate.OnFirmwareUpdateProgress += (sender, evf) =>
                        {
                            iShell.Run(() =>
                            {
                                c.SetValue((int)evf.Percentage, evf.Message);
                                if (evf.Percentage == c.MaxValue)
                                {
                                    errMsg = evf.Message;
                                    c.CloseView(evf.Failed ? DialogResult.Abort : DialogResult.OK);
                                }
                            });
                        };

                        c.Load += (s, ev) =>
                        {
                            try
                            {
                                firmwareUpdate.UpgradeThread();
                            }
                            catch (Exception ex)
                            {
                                c.CloseView(DialogResult.Abort);
                            }
                        };

                        return c;
                    },
                    MessageBoxButtons.RetryCancel);

                    if (progress != DialogResult.OK)
                        throw new Exception(string.Format("Error upgrading firmware in ST7590: {0}", errMsg));

                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error upgrading firmware in ST7590: {0}", ex.Message));
            }
        }
       
        public class UpgradeFirmware_ST7590Description : ActionDescription
        {
            public override string Name { get { return "UpgradeFirmware_ST7590"; } }
            public override string Category { get { return "Tools/Firmware"; } }
            public override string Description { get { return "Action para actualizar el firmware de un equipo"; } }
            public override Image Icon { get { return Properties.Resources.stmicroelectronics; } }
        }
    }
}
