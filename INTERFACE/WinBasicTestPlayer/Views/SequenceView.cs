﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner.Model;
using System.Threading;
using WinTestPlayer.Services;
using Telerik.WinControls.UI;
using Dezac.Tests;
using Dezac.Tests.Actions;
using TaskRunner.Model;

namespace WinTestPlayer.Views
{
    public partial class SequenceView : UserControl
    {
        private Dictionary<Step, RadTreeNode> stepNodes;
        private Dictionary<RadTreeNode, StepResult> stepErrors;

        protected AppService app;

        private SynchronizationContext sc;

        public SequenceView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            this.sc = SynchronizationContext.Current;

            stepErrors = new Dictionary<RadTreeNode, StepResult>();

            app = DI.Resolve<AppService>();
            //app.ModelChanged += app_ModelChanged;
            app.SequenceLoaded += (s, ee) => {
                UpdateUI();
            };
            app.Runner.AllTestStarting += (s, ee) =>
            {
                treeView.Focus();
                stepErrors.Clear();
                stepNodes.Values.ToList().ForEach(n =>
                {
                    n.BackColor = Color.Empty;
                    n.Text = n.ToolTipText;
                });
            };

            var runner = app.Runner;
            
            runner.StepStart += (s, ev) => { sc.Post(_ => { OnStepStart(ev); }, null); };
            runner.StepEnd += (s, ev) => { sc.Post(_ => { OnStepEnd(ev); }, null); };

            UpdateUI();
        }

        void app_ModelChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            treeView.Nodes.Clear();

            if (app.Sequence == null)
                return;

            stepNodes = new Dictionary<Step, RadTreeNode>();

            AddChildNodes(treeView.Nodes, app.Sequence.Root);

            treeView.ExpandAll();

            ValidateSequence();
        }

        private void AddNodes(RadTreeNode node)
        {
            Step step = node.Tag as Step;
            if (step == null)
                return;

            node.Checked = step.Enabled;

            stepNodes.Add(step, node);

            AddChildNodes(node.Nodes, step);
        }

        private void AddChildNodes(RadTreeNodeCollection node, Step step)
        {
            foreach (var child in step.Steps)
            {
                string nodeName= child.Enabled==true? child.Name: child.Name + " (Deshabilitada)";
                Color color = child.Enabled == true ? Color.Black : Color.Silver;

                var n = new RadTreeNode(nodeName);
                n.ToolTipText = nodeName;
                n.Tag = child;
                n.ForeColor = color;

                node.Add(n);
                AddNodes(n);
            }
        }

        void OnStepStart(StepResult e)
        {
            RadTreeNode node = null;

            if (stepNodes.TryGetValue(e.Step, out node))
            {
                node.BackColor = e.Step.Enabled == true ? Color.Green : Color.Gray;
                node.EnsureVisible();
                Application.DoEvents();
            }
        }

        void OnStepEnd(StepResult e)
        {
            RadTreeNode node = null;

            if (stepNodes.TryGetValue(e.Step, out node))
            {
                node.BackColor = e.ExecutionResult == TaskRunner.Enumerators.StepExecutionResult.Passed ? Color.Empty : Color.Red;
                double duration = e.Duration.TotalSeconds;
                if (duration > 0.1)
                    node.Text = string.Format("{0}  ({1:N2}s)", node.Text, duration);

                if (e.ExecutionResult != TaskRunner.Enumerators.StepExecutionResult.Passed && !stepErrors.ContainsKey(node))
                    stepErrors.Add(node, e);
            }
        }

        private void treeView_NodeMouseDoubleClick(object sender, RadTreeViewEventArgs e)
        {
            StepResult result = null;

            if (stepErrors.TryGetValue(e.Node, out result))
                using (LogItemDetails form = new LogItemDetails())
                {
                    form.LogText = result.GetErrorMessage();
                    form.ShowDialog();
                }
        }

        private void ValidateSequence()
        {
            try
            {
                var actions = stepNodes.Keys
                    .Where(p => p.Action is RunMethodAction)
                    .Select(p => new { Step = p, Action = (RunMethodAction)p.Action });

                var types = actions
                    .Select(p => p.Action.GetMethodType())
                    .Distinct();

                var msg = string.Empty;

                foreach (var type in types)
                {
                    var methods = type
                        .GetMethods()
                        .Where(m => m.GetCustomAttributes(typeof(TestPointAttribute), false).Length > 0)
                        .ToList();

                    foreach (var m in methods)
                    {
                        var attr = m.GetCustomAttributes(typeof(TestPointAttribute), false)[0] as TestPointAttribute;

                        if (attr.Required)
                        {
                            var name = string.Format("{0}/{1}", m.ReflectedType.FullName, m.Name);
                            var step = actions.Where(p => string.Format("{0}/{1}", p.Action.TypeName, p.Action.Method) == name)
                                .FirstOrDefault();

                            if (step == null || !step.Step.Enabled)
                                msg += string.Format("La secuencia no está verificando el test point {0} requerido!{1}",
                                    m.Name, Environment.NewLine);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(msg))
                    DI.Resolve<ShellService>().MsgBox(msg, "ATENCIÓN");
            }
            catch { }
        }
    }
}
