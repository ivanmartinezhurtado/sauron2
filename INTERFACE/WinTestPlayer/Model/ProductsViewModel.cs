﻿using System;

namespace WinTestPlayer.Model
{
    public class ProductsViewModel
    {
        public int NUMPRODUCTO { get; set; }
        public int VERSION { get; set; }
        public Nullable<System.DateTime> FECHAVERSION { get; set; }
        public string ACTIVO { get; set; }

        public int NUMFAMILIA { get; set; }
        public string FAMILIA { get; set; }
        public string DESCRIPCION { get; set; }
    }

    public class ProductFaseViewModel
    {
        public string IDFASE { get; set; }
        public string FASE { get; set; }
        public decimal? TIEMPOPREPARACION { get; set; }
        public decimal? TIEMPOSTANDARD { get; set; }
        public decimal? TIEMPOTEORICO { get; set; }
    }

    public class FasesDecription
    {
        public string IDFASE { get; set; }
        public string FASE { get; set; }
    }

    public class AsistenciaTecnicaViewModel
    {
        public int NUMASISTENCIA { get; set; }
        public int NUMLIN { get; set; }
        public int NUMPRODUCTO { get; set; }
        public string IDOPERARIO { get; set; }
        public Nullable<int> NUMPEDIDO { get; set; }
        //public Nullable<int> NUMLINPEDIDO { get; set; }
        public string NUMEROSERIE { get; set; }
        //public System.DateTime FECHAALTA { get; set; }
        public Nullable<System.DateTime> FECHAFINPREV { get; set; }
        //public string AVERIACOMUNICADA { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
        public Nullable<int> NUMTEST { get; set; }
        public string NROREPARACION { get; set; }
        public string UBICACION { get; set; }
        public string ESTADO { get; set; }

        public string DESCRIPCION { get; set; }
    }
}
