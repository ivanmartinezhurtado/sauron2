using Dezac.Tests.Model;
using Dezac.Tests.Visio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Dezac.Core.Utility;
using Dezac.Tests.Services;
using System.Drawing;
using Instruments.Cameras.DTO;
using Instruments.Cameras;
using Dezac.Tests.Visio.DTO;
using Dezac.Services;
using Comunications.Ethernet;
using System.Text.RegularExpressions;

namespace Dezac.Tests.Extensions
{
    public static class TestHalconExtension
    {
        public static void TestHalconFindLedsProcedure(this TestBase testBase, string serialNumber, string testName, 
            string imageSaveName, string deviceName, byte retry = 1, Action<int> retryFunc = null)
        {
            string errorDetected = "";

            TestHalconCaptureAsync(testBase, TypeProcedure.FindLeds, serialNumber, testName, imageSaveName, deviceName, retry, retryFunc);
            var testInputs = testBase.GetVariable<TestHalconExtension.ProcedureInputs>(String.Format("HALCON_PROCEDURE_{0}", testName));
            var halconResultTupla = TestHalconProcedureAsync(testBase, testInputs);

            testBase.RemoveVariable(String.Format("HALCON_PROCEDURE_{0}", testName));

            var halconResult = halconResultTupla.Item1;
            errorDetected = halconResultTupla.Item2.ToString();

            if (halconResult != ResultImageState.OK)
                throw new Exception(string.Format("Error no se detecta el LED {0} en la imagen {1}", errorDetected, imageSaveName));
        }

        private static Tuple<ResultImageState, string> TestFindLeds(this TestBase testBase, ImageStruct imageStruct,
            ProcedureInputs procedureInputs, bool returnLEDs)
        {
            var result = internalCallHalconProcedure(procedureInputs, imageStruct);
            if(result == null)
                return Tuple.Create<ResultImageState, string>( ResultImageState.HALCON_FAILED, "Error al acceder al servicio de Halcon");

            var timeInspection = result.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEINSPECTION");
            if (timeInspection.Count() == 1)
                testBase.Logger.Debug(String.Format("Inspección vision artificial de imagen {0} en {1} s", procedureInputs.HalconTestName, timeInspection.FirstOrDefault().Value));

            var timeRead = result.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEREAD");
            if (timeRead.Count() == 1)
                testBase.Logger.Debug(String.Format("Tiempo de lectura de imagen {0} en {1} s", procedureInputs.HalconTestName, timeRead.FirstOrDefault().Value));

            var resultJson = JsonConvert.SerializeObject(result.TupleResults);

            var resultFinsLedsTest = JsonConvert.DeserializeObject<ResultFinsLedsTest>(resultJson);

            var halconResult = ResultImageState.OK;

            if (!result.Success)
            {
                halconResult = ResultImageState.HALCON_FAILED;
                testBase.Resultado.Set(string.Format("HALCON_{0}", procedureInputs.TestName), halconResult == ResultImageState.OK ? "OK" : "FAIL", ParamUnidad.SinUnidad);
                testBase.Logger.WarnFormat("Warning: HALCON_FAILED -> {0} ",result.ErrorMessage);
                return Tuple.Create<ResultImageState, string>(halconResult, string.Format("Error por {0}", result.ErrorMessage));
            }

            if (returnLEDs)
            {
                testBase.Resultado.Set(string.Format("HALCON_{0}", procedureInputs.TestName), halconResult == ResultImageState.OK ? "OK" : "FAIL", ParamUnidad.SinUnidad);
                return Tuple.Create(ResultImageState.OK, string.Join(",", resultFinsLedsTest.ColorsList.ToArray()));
            }

            string colorString = "";
            resultFinsLedsTest.Leds.ForEach((s) => colorString = colorString + s.color.ToUpper() + ";");

            testBase.Logger.Debug(colorString);

            ledsPositionAlias ErrorLeds = ledsPositionAlias.Create();

            var colorledsBBDD = testBase.Vision.GetString($"{procedureInputs.TestName}_COLORS", "NO", ParamUnidad.SinUnidad);
            var aliasledsBBDD = testBase.Vision.GetString($"{procedureInputs.TestName}_ALIAS", "NO", false);
            var aliasBBDD = aliasledsBBDD.Split(';');
            var ledsBBDD = colorledsBBDD.Split(';');

            if (colorledsBBDD.Trim() == "NO")
            {
                testBase.Logger.WarnFormat("Atención, no esta configurado el parametro para la detección de LEDS en la imagen {0}", procedureInputs.TestName);
                halconResult = ResultImageState.HALCON_FAILED;
            }
            else if (ledsBBDD.Length != resultFinsLedsTest.Leds.Count)
            {
                testBase.Logger.WarnFormat($"Atención, el numero de LEDs parametrizado en la BBDD es {ledsBBDD.Length} y el devuelto por HALCON es {resultFinsLedsTest.Leds.Count} en la imagen {procedureInputs.TestName}");
                halconResult = ResultImageState.HALCON_FAILED;
            }
            else
                for (byte position = 0; position < ledsBBDD.Count(); position++)
                    if ((ledsBBDD[position].ToUpper().Trim() != "ND") && (resultFinsLedsTest.Leds[position].color.ToUpper().Trim() != ledsBBDD[position].ToUpper().Trim()))
                    {
                        halconResult = ResultImageState.HALCON_FAILED;
                        ErrorLeds.Position.Add(position.ToString());
                        if (aliasledsBBDD != "NO")
                            ErrorLeds.Alias.Add(aliasBBDD[position]);
                    }

            testBase.Resultado.Set(string.Format("HALCON_{0}", procedureInputs.TestName), halconResult == ResultImageState.OK ? "OK" : "FAIL", ParamUnidad.SinUnidad);

            string errorMsg = "";

            if (halconResult != ResultImageState.OK)
                try
                {

                    if (aliasledsBBDD == "NO")
                        errorMsg = string.Format("Error en el LED {0}", ErrorLeds.Position.Aggregate((actual, next) => actual + ", " + next));
                    else
                        errorMsg = string.Format("Error en el LED {0}", ErrorLeds.Alias.Aggregate((actual, next) => actual + ", " + next));
                }
                catch (InvalidOperationException)
                {
                    errorMsg = "";
                }

            return Tuple.Create<ResultImageState, string>(halconResult, errorMsg);
        }

        public static void TestHalconMatchingProcedure(this TestBase testBase, string serialNumber, string testName, string imageSaveName, 
            string deviceName, byte retry = 1, Action<int> retryFunc = null)
        {
            string errorDetected = "";

            TestHalconCaptureAsync(testBase, TypeProcedure.PaternMatching, serialNumber, testName, imageSaveName, deviceName, retry, retryFunc);
            var testInputs = testBase.GetVariable<TestHalconExtension.ProcedureInputs>(String.Format("HALCON_PROCEDURE_{0}", testName));
            var halconResultTupla = TestHalconProcedureAsync(testBase, testInputs);

            testBase.RemoveVariable(String.Format("HALCON_PROCEDURE_{0}", testName));

            var halconResult = halconResultTupla.Item1;
            errorDetected = halconResultTupla.Item2.ToString();

            if (halconResult != ResultImageState.OK)
                throw new Exception(string.Format("{0} en la imagen {1}", errorDetected, imageSaveName));
        }

        private static Tuple<ResultImageState, string> TestPaternMatching(this TestBase testBase, ImageStruct imageStruct,
            ProcedureInputs procedureInputs)
        {
            var listFilesName = new List<ImageStruct>();
            listFilesName.Add(imageStruct);
            return TestPaternMatching(testBase, listFilesName, procedureInputs);
        }

        private static Tuple<ResultImageState, string> TestPaternMatching(this TestBase testBase, List<ImageStruct> listImages, 
            ProcedureInputs procedureInputs)
        {
            var halconProcedureResult = internalCallHalconProcedure(procedureInputs, listImages);

            var timeInspection = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEINSPECTION");
            if (timeInspection.Count() == 1)
                testBase.Logger.Debug(String.Format("Inspección vision artificial de imagen {0} en {1} s", procedureInputs.TestName, timeInspection.FirstOrDefault().Value));

            var timeRead = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEREAD");
            if (timeRead.Count() == 1)
                testBase.Logger.Debug(String.Format("Tiempo de lectura de imagen {0} en {1} s", procedureInputs.TestName, timeRead.FirstOrDefault().Value));

            ResultImageState halconResult = ResultImageState.HALCON_FAILED;

            string resultPatternMatching = "";
            
            if (halconProcedureResult.TupleResults.Count() >= 3)
            {
                var halconProcedureResultList = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "RESULT");

                if (halconProcedureResultList.Count() == 1)
                {
                    resultPatternMatching = halconProcedureResultList.FirstOrDefault().Value.ToString().Trim('[', ']');
                    halconResult = resultPatternMatching == "1" ? ResultImageState.OK : ResultImageState.HALCON_FAILED;

                    if (resultPatternMatching == "-33")
                    {
                        testBase.AddResult("Error controlado prueba Raster");
                        halconResult = ResultImageState.OK;
                    }

                    resultPatternMatching = MatchingResultsDictionary.ContainsKey(resultPatternMatching) == true ? MatchingResultsDictionary[resultPatternMatching] : MatchingResultsDictionary["Default"];
                }
            }
            else
            {
                resultPatternMatching = halconProcedureResult.ErrorMessage;
                testBase.Logger.ErrorFormat(resultPatternMatching);
            }

            testBase.Resultado.Set(string.Format("HALCON_{0}", procedureInputs.TestName), halconResult == ResultImageState.OK ? "OK" : "FAIL", ParamUnidad.SinUnidad);

            if (halconProcedureResult.IconicResults.Where(result => result.Key.ToUpper() == "IMAGEOUTPUT").Any())
                listImages.ToArray()[0].ImageError = halconProcedureResult.IconicResults.Where(result => result.Key.ToUpper() == "IMAGEOUTPUT").First().Value;

            return Tuple.Create<ResultImageState, string>(halconResult, resultPatternMatching);
        }

        public static string TestHalconReadBarCodeProcedure(this TestBase testBase, string serialNumber, TypeBarcode barCodeType, 
            string imageSaveName, string deviceName, byte retries, Action<int> retryFunc = null, int row1 = 0, int column1 = 0, 
            int row2 = 0, int column2 = 0, double resizeFactor = 1)
        {
            TestHalconCaptureAsync(testBase, TypeProcedure.ReadBarCode, serialNumber, barCodeType.ToString("G"), imageSaveName, deviceName, retries, retryFunc, "", row1, column1, row2, column2, resizeFactor);
            var testInputs = testBase.GetVariable<TestHalconExtension.ProcedureInputs>(String.Format("HALCON_PROCEDURE_{0}", barCodeType.ToString("G")));
            var halconResultTupla = TestHalconProcedureAsync(testBase, testInputs);

            testBase.RemoveVariable(String.Format("HALCON_PROCEDURE_{0}", barCodeType.ToString("G")));

            var halconResult = halconResultTupla.Item1;

            if (halconResult != ResultImageState.OK)
                throw new Exception(string.Format("Error: No se ha podido leer el codigo de barras", imageSaveName));

            return halconResultTupla.Item2;
        }

        private static Tuple<ResultImageState, string> ReadBarCode(this TestBase testBase, ImageStruct imageStruct, ProcedureInputs procedureInputs)
        {
            var halconProcedureResult = internalCallHalconProcedure(procedureInputs, imageStruct);
            if (halconProcedureResult == null)
                return Tuple.Create<ResultImageState, string>(ResultImageState.HALCON_FAILED, "Error al acceder al servicio de Halcon");

            var timeInspection = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEINSPECTION");
            if (timeInspection.Count() == 1)
                testBase.Logger.Debug(String.Format("Inspección vision artificial de imagen {0} en {1} s", imageStruct.ImageName, timeInspection.FirstOrDefault().Value));

            var timeRead = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEREAD");
            if (timeRead.Count() == 1)
                testBase.Logger.Debug(String.Format("Tiempo de lectura de imagen {0} en {1} s", imageStruct.ImageName, timeRead.FirstOrDefault().Value));

            string resultReadBarCode = "-1";
            if (halconProcedureResult.Success)
                resultReadBarCode = halconProcedureResult.TupleResults["CodeNumber"].ToString().Replace("\"", "");

            var halconResult = ResultImageState.HALCON_FAILED;

            if (!String.Equals(resultReadBarCode, "-1"))
                halconResult = ResultImageState.OK;
            else
                halconResult = ResultImageState.HALCON_FAILED;

            testBase.Resultado.Set(string.Format("HALCON_{0}", imageStruct.ImageName), halconResult == ResultImageState.OK ? "OK" : "FAIL", ParamUnidad.SinUnidad);

            return Tuple.Create<ResultImageState, string>(halconResult, resultReadBarCode);
        }

        public static string TestHalconOCRProcedure(this TestBase testBase, string serialNumber, string testName, string imageSaveName, 
            string deviceName, byte retry, int row1, int column1, int row2, int column2, double resizeFactor, int charWidth, int charHeight, 
            int charStroke, int minFragmentSize, PolaritySelectionEnum polarity, int numberOfLines, double orientationOffset, 
            double orientationTolerance, string regularExpresion, OCRClassificatorEnum classificator)
        {
            TestHalconCaptureAsync(testBase, TypeProcedure.OCR, serialNumber, testName, imageSaveName, deviceName, retry, null, "", row1, column1, row2, column2, resizeFactor);

            var testInputs = testBase.GetVariable<TestHalconExtension.ProcedureInputs>(String.Format("HALCON_PROCEDURE_{0}", testName));
            testInputs.CharWidth = charWidth;
            testInputs.CharHeight = charHeight;
            testInputs.CharStroke = charStroke;
            testInputs.MinFragmentSize = minFragmentSize;
            testInputs.Polarity = polarity;
            testInputs.NumberOfLines = numberOfLines;
            testInputs.OrientationOffset = orientationOffset;
            testInputs.OrientationTolerance = orientationTolerance;
            testInputs.Classificator = classificator;
            testInputs.RegularExpresion = regularExpresion;

            var halconResultTupla = TestHalconProcedureAsync(testBase, testInputs);

            testBase.RemoveVariable(String.Format("HALCON_PROCEDURE_{0}", testName));

            var halconResult = halconResultTupla.Item1;     
            if (halconResult != ResultImageState.OK)
                throw new Exception(string.Format("Error: No se ha podido realizar la lectura por OCR en la imagen {0}. Halcon Error: {1}", imageSaveName, halconResultTupla.Item2));

            return halconResultTupla.Item2;
        }

        private static Tuple<ResultImageState, string> TestOCR(this TestBase testBase, ImageStruct imageStruct, ProcedureInputs procedureInputs)
        {
            var halconProcedureResult = internalCallHalconProcedure(procedureInputs, imageStruct);
            if (halconProcedureResult == null)
                return Tuple.Create<ResultImageState, string>(ResultImageState.HALCON_FAILED, "Error al acceder al servicio de Halcon");

            var timeInspection = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEINSPECTION");
            if (timeInspection.Count() == 1)
                testBase.Logger.Debug(String.Format("Inspección vision artificial de imagen {0} en {1} s", imageStruct, timeInspection.FirstOrDefault().Value));

            var timeRead = halconProcedureResult.TupleResults.Where((listItem) => listItem.Key.ToUpper() == "TIMEREAD");
            if (timeRead.Count() == 1)
                testBase.Logger.Debug(String.Format("Tiempo de lectura de imagen {0} en {1} s", imageStruct, timeRead.FirstOrDefault().Value));

            ResultImageState halconResult = ResultImageState.HALCON_FAILED;

            string resultOCR;
            if (halconProcedureResult.TupleResults.ContainsKey("Characters"))
            {
                halconResult = ResultImageState.OK;
                resultOCR = halconProcedureResult.TupleResults["Characters"].Replace("\"", "").Replace("[", "").Replace("]", "").Replace(", ","");
                if (string.IsNullOrWhiteSpace(resultOCR))
                {
                    halconResult = ResultImageState.HALCON_FAILED;
                    resultOCR = "No Se ha leido ningun caracter";
                }
            }
            else
            {
                halconResult = ResultImageState.HALCON_FAILED;
                resultOCR = halconProcedureResult.ErrorMessage;
                testBase.Logger.Error(resultOCR);
            }

            testBase.Resultado.Set(string.Format("HALCON_{0}", Path.GetFileNameWithoutExtension(imageStruct.ImageName)), halconResult == ResultImageState.OK ? "OK" : "FAIL", ParamUnidad.SinUnidad);
            testBase.Resultado.Set(string.Format("HALCON_{0}_SCORE", Path.GetFileNameWithoutExtension(imageStruct.ImageName)), halconResult == ResultImageState.OK ? halconProcedureResult.TupleResults["Confidences"] : "0", ParamUnidad.SinUnidad);
            testBase.Resultado.Set(string.Format("HALCON_{0}_CHARACTERS", Path.GetFileNameWithoutExtension(imageStruct.ImageName)), halconResult == ResultImageState.OK ? resultOCR : "CARACTERES NO LEIDOS" , ParamUnidad.SinUnidad);

            return Tuple.Create<ResultImageState, string>(halconResult, resultOCR as string);
        }

        public static void TestHalconCaptureAsync(this TestBase testBase, TypeProcedure TypeProcedure, string serialNumber, 
            string procedureToCall, string imageSaveName, string deviceName, byte totalCaptures = 1, Action<int> captureFunc = null, 
            string parameterName = "", int row1 = 0, int column1 = 0, int row2 = 0, int column2 = 0, double resizeFactor = 1)
        {
            var images = new List<ImageStruct>();

            for (var actualCapture = 0; actualCapture < totalCaptures; actualCapture++)
            {
                string imageName = $"{imageSaveName}_{actualCapture}";
                if (captureFunc != null)
                    captureFunc(actualCapture);

                Bitmap image = testBase.SaveImageCameraIDS(serialNumber, row1, column1, row2, column2, resizeFactor);
                images.Add(new ImageStruct(image, imageName));
            };

            var testInputs = new ProcedureInputs { Images = images, HalconTestName = procedureToCall, DeviceName = deviceName,
                TypeProcedure = TypeProcedure, ParameterName = parameterName, MultiImageTest = false};
            testBase.SetVariable(string.Format("HALCON_PROCEDURE_{0}", procedureToCall), testInputs);
        }

        public static void TestHalconCaptureAsync(this TestBase testBase, TypeProcedure TypeProcedure, string serialNumber,  
            string procedureToCall, Dictionary<string, Action> dictImageNameFunction, string deviceName, 
            int row1 = 0, int column1 = 0, int row2 = 0, int column2 = 0, double resizeFactor = 1)
        {
            var images = new List<ImageStruct>();

            foreach (KeyValuePair<string, Action> action in dictImageNameFunction)
            {
                if (action.Value != null)
                    action.Value();

                Bitmap image = testBase.SaveImageCameraIDS(serialNumber, row1, column1, row2, column2, resizeFactor);
                images.Add(new ImageStruct(image, action.Key));
            }

            var testInputs = new ProcedureInputs { Images = images, HalconTestName = procedureToCall, DeviceName = deviceName,
                TypeProcedure = TypeProcedure, ParameterName = null, MultiImageTest = true};
            testBase.SetVariable(String.Format("HALCON_PROCEDURE_{0}", procedureToCall), testInputs);
        }

        public static void TestHalconCaptureLucid(this TestBase testBase, TypeProcedure procedureType, 
            string cameraName, string device, string test, int totalCaptures, Action<int> preCaptureFunction)
        {
            var cachesvc = testBase.GetService<ICacheService>();
            CameraLucid camera = cachesvc.Get<CameraLucid>($"{cameraName}_CameraLucid");

            List<Bitmap> bitmaps = camera.GetImage(totalCaptures, preCaptureFunction);
            List<ImageStruct> images = bitmaps.Select((bitmap,index) => new ImageStruct(bitmap, $"{test}_{index}")).ToList();

            var testInputs = new ProcedureInputs
            {
                Images = images,
                HalconTestName = $"{device}_{test}",
                DeviceName = device,
                TypeProcedure = procedureType,
                ParameterName = "",
                MultiImageTest = false
            };

            testBase.SetVariable(string.Format("HALCON_PROCEDURE_{0}", test), testInputs);
        }

        public static Tuple<ResultImageState, string> TestHalconProcedureAsync(this TestBase testBase, ProcedureInputs procedureInputs)
        {
            var halconResultTupla = new Tuple<ResultImageState, string>(ResultImageState.HALCON_FAILED, "");
       
            if (!procedureInputs.MultiImageTest)
            {
                var resultTotal = ResultImageState.FAILED;
                var errorDetected = "";

                // Control de reintentos
                foreach (ImageStruct image in procedureInputs.Images)
                {
                    switch (procedureInputs.TypeProcedure)
                    {
                        case TypeProcedure.PaternMatching:
                            halconResultTupla = TestPaternMatching(testBase, image, procedureInputs);
                            break;
                        case TypeProcedure.FindLeds:
                            halconResultTupla = TestFindLeds(testBase, image, procedureInputs, false);
                            break;
                        case TypeProcedure.FindLedsReturnColors:
                            halconResultTupla = TestFindLeds(testBase, image, procedureInputs, true);
                            break;
                        case TypeProcedure.ReadBarCode:
                            halconResultTupla = ReadBarCode(testBase, image, procedureInputs);
                            break;
                        case TypeProcedure.OCR:
                            halconResultTupla = TestOCR(testBase, image, procedureInputs);
                            break;
                    }

                    var halconResult = halconResultTupla.Item1;
                    errorDetected = halconResultTupla.Item2.ToString();
                    resultTotal = halconResult;
                    var imageResult = halconResult;

                    if (halconResult != ResultImageState.OK)
                        testBase.Logger.WarnFormat("Halcon Error: {0}", errorDetected);

                    var automatico = "SI";
                    if (!(procedureInputs.TypeProcedure == TypeProcedure.ReadBarCode))
                        automatico = testBase.Vision.GetString(TypeProcedureDictionary[procedureInputs.TypeProcedure], "NO", ParamUnidad.SinUnidad).Trim().ToUpper();

                    if (((procedureInputs.TypeProcedure == TypeProcedure.PaternMatching) || 
                        (procedureInputs.TypeProcedure == TypeProcedure.FindLeds) || 
                        (procedureInputs.TypeProcedure == TypeProcedure.FindLedsReturnColors) ||
                        (procedureInputs.TypeProcedure == TypeProcedure.OCR)) && 
                        ((automatico == "NO") ||
                        (automatico.Contains("SEMI") && halconResult != ResultImageState.OK) ||
                        (automatico.Contains("TEST") && halconResult == ResultImageState.OK) ||
                        (testBase.Model.modoPlayer == Services.ModoTest.PostVenta && halconResult != ResultImageState.OK)))
                   {
                        var userResult = ResultImageState.USER_FAILED;

                        lock (_syncObject)
                        {
                            string formLabel = procedureInputs.TypeProcedure == TypeProcedure.OCR ? "¿El texto de la imagen es " + errorDetected + "?" : image.ImageName;
                            userResult = testBase.CompareImageFromPattern(image, formLabel) == DialogResult.Yes ? ResultImageState.OK : ResultImageState.USER_FAILED;
                        }

                        imageResult = (ResultImageState)((byte)halconResult + (byte)userResult);
                        resultTotal = userResult;
                    }

                    SaveImageByResult(testBase, image, procedureInputs.DeviceName, imageResult);

                    if (automatico == "TRAINING")
                        resultTotal = ResultImageState.OK;

                    if (resultTotal == ResultImageState.OK)
                        break;
                }

                testBase.SetVariable("HALCON_END", true);
                procedureInputs.Dispose();

                return Tuple.Create(resultTotal, errorDetected);
            }
            else
            {
                if (procedureInputs.TypeProcedure != TypeProcedure.PaternMatching)
                    throw new Exception("El modo multi imagen solo esta habilitado para test de LCD");

                halconResultTupla = TestPaternMatching(testBase, procedureInputs.Images, procedureInputs);

                var halconResult = halconResultTupla.Item1;
                var errorDetected = halconResultTupla.Item2.ToString();
                var resultTotal = halconResult;

                if (halconResult != ResultImageState.OK)
                    testBase.Logger.WarnFormat("Halcon Error: {0}", errorDetected);

                var automatico = "SI";
                if (!(procedureInputs.TypeProcedure == TypeProcedure.OCR || procedureInputs.TypeProcedure == TypeProcedure.ReadBarCode))
                    automatico = testBase.Vision.GetString(TypeProcedureDictionary[procedureInputs.TypeProcedure], "NO", ParamUnidad.SinUnidad).Trim().ToUpper();

                if ((automatico == "NO") || 
                    (automatico.Contains("SEMI") && halconResult != ResultImageState.OK) ||
                    (automatico.Contains("TEST") && halconResult == ResultImageState.OK) ||
                    (testBase.Model.modoPlayer == Services.ModoTest.PostVenta && halconResult != ResultImageState.OK))
                {
                    var userResult = ResultImageState.USER_FAILED;
                    foreach (ImageStruct image in procedureInputs.Images)
                    {
                        lock (_syncObject)
                        {
                            userResult = testBase.CompareImageFromPattern(image, "") == DialogResult.Yes ? ResultImageState.OK : ResultImageState.USER_FAILED;
                        }
                        if (userResult != ResultImageState.OK)
                            break;
                    }

                    halconResult = (ResultImageState)((byte)halconResult + (byte)userResult);
                    resultTotal = userResult;
                }

                SaveImageByResult(testBase, procedureInputs.Images, procedureInputs.DeviceName, halconResult, procedureInputs.HalconTestName);

                if (automatico == "TRAINING")
                    resultTotal = ResultImageState.OK;

                procedureInputs.Dispose();
                return Tuple.Create(resultTotal, errorDetected);
            }
        }

        private static HalconCallResult internalCallHalconProcedure(ProcedureInputs procedureInputs, ImageStruct image)
        {
            var listFileName = new List<ImageStruct>();
            listFileName.Add(image);
            return internalCallHalconProcedure(procedureInputs, listFileName);
        }

        private static HalconCallResult internalCallHalconProcedure(ProcedureInputs procedureInputs, List<ImageStruct> listImages)
        {
            var dictionaryParameters = new Dictionary<string, object>();

            switch(procedureInputs.TypeProcedure)
            {
                case TypeProcedure.ReadBarCode:
                    dictionaryParameters.Add("CodeType", procedureInputs.HalconTestName);
                    dictionaryParameters.Add("Xcalibration", 0);
                    dictionaryParameters.Add("Ycalibration", 0);
                    dictionaryParameters.Add("Row1", 0);
                    dictionaryParameters.Add("Column1", 0);
                    dictionaryParameters.Add("Row2", 0);
                    dictionaryParameters.Add("Column2", 0);
                    break;
                case TypeProcedure.OCR:
                    dictionaryParameters.Add("TestName", procedureInputs.HalconTestName);
                    dictionaryParameters.Add("Xcalibration", 0);
                    dictionaryParameters.Add("Ycalibration", 0);
                    dictionaryParameters.Add("CharWidth", procedureInputs.CharWidth);
                    dictionaryParameters.Add("CharHeight", procedureInputs.CharHeight);
                    dictionaryParameters.Add("CharStroke", procedureInputs.CharStroke);
                    dictionaryParameters.Add("MinFragmentSize", procedureInputs.MinFragmentSize);
                    dictionaryParameters.Add("Polarity", OCRPolaritySelectioDictionary[procedureInputs.Polarity]);
                    dictionaryParameters.Add("NumberOfLines", procedureInputs.NumberOfLines);
                    dictionaryParameters.Add("OrientationOffset", procedureInputs.OrientationOffset);
                    dictionaryParameters.Add("OrientationTolerance", procedureInputs.OrientationTolerance);
                    dictionaryParameters.Add("RegularExpresion", procedureInputs.RegularExpresion);
                    dictionaryParameters.Add("Classificator", OCRClassificatorDictionary[procedureInputs.Classificator]);
                    break;
                default:
                    dictionaryParameters.Add("TestName", procedureInputs.HalconTestName);
                    dictionaryParameters.Add("Xcalibration", 0);
                    dictionaryParameters.Add("Ycalibration", 0);
                    break;
            }

            var result = Halcon.CallProcedure(procedureInputs.TypeProcedure.GetDescription(), listImages, dictionaryParameters);
            return result;
        }

        private static void SaveImageByResult(this TestBase testBase, ImageStruct imageStruct, string deviceName, 
            ResultImageState result)
        {
            string imageFileName =
                $"{testBase.TestInfo.NumBastidor}_" +
                $"{testBase.Model.NumUtilBien}_" +
                $"{imageStruct.ImageName}_" +
                $"{DateTime.Now.ToShortDateString().Replace("/", "")}_" +
                $"{DateTime.Now.ToLongTimeString().Replace(":", "")}.png";

            string imageNameWithoutNumber = Regex.Replace(imageStruct.ImageName, @"_[0-9]$", "");
            string pathImageSave = $"\\\\{deviceName}\\{result.ToString("G")}\\{imageNameWithoutNumber}\\{imageFileName}";

            //FTP
            try
            {
	            using (FTPClient client = new FTPClient(ConfigurationManager.AppSettings["VAFTPDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
	            using (MemoryStream memoryStream = new MemoryStream())
	            {
	                var stopwatch = new Stopwatch();
	                stopwatch.Start();
	                testBase.Logger.Debug("Making a copy image in the selected path...");
	                client.CreateDirectory(Path.GetDirectoryName(pathImageSave));
	                imageStruct.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
	                client.UploadOverwrite(memoryStream, pathImageSave, 3);
	                stopwatch.Stop();
	                testBase.Logger.Debug($"Saving image by result: File Saved in {stopwatch.ElapsedMilliseconds / 1000.0} seconds");

	                //Registro en la tabla TraceTime
	                using (DezacService svc = new DezacService())
	                {
	                    svc.AddTraceTime("TERMINAL_ADDTESTVAFILE", (int)stopwatch.ElapsedMilliseconds, memoryStream.Length);
	                }
	            }
	        }
	        // FTPCabina Backup
            catch (Exception ex)
            {
            	SendMail.Send("Fallo en grabacion de imagen en cabina VA", $"Error en el guardado de la imagen {pathImageSave} {ex.Message}{ex.StackTrace}", false, false, true);

            	using (FTPClient client = new FTPClient(ConfigurationManager.AppSettings["VAFTPBackUpDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
	            using (MemoryStream memoryStream = new MemoryStream())
	            {
	                var stopwatch = new Stopwatch();
	                stopwatch.Start();
	                testBase.Logger.Debug("Making a copy image in the selected path...");
	                client.CreateDirectory(Path.GetDirectoryName($"VAFiles{pathImageSave}"));
	                imageStruct.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
	                client.UploadOverwrite(memoryStream, $"VAFiles{pathImageSave}", 3);
	                stopwatch.Stop();
	                testBase.Logger.Debug($"Saving image by result: File Saved in {stopwatch.ElapsedMilliseconds / 1000.0} seconds");

	                //Registro en la tabla TraceTime
	                using (DezacService svc = new DezacService())
	                {
	                    svc.AddTraceTime("TERMINAL_ADDTESTVAFILE_BACKUP", (int)stopwatch.ElapsedMilliseconds, memoryStream.Length);
	                }
	            }
            }
        
            // BBDD
            var stream = ImageCompressor.CompressImage(imageStruct.Image);
            var files = testBase.GetVariable("IMAGES_FILES", new Dictionary<string, byte[]>());
            files[imageStruct.ImageName] = stream;
            if (imageStruct.ImageError != null)
                files[imageStruct.ImageName + "_HALCON"] = imageStruct.ImageError;
            testBase.SetVariable("IMAGES_FILES", files);

            imageStruct.Image.Dispose();
        }

        private static void SaveImageByResult(this TestBase testBase, List<ImageStruct> listImages, string deviceName,
            ResultImageState result, string folderName)
        {
            var stopwatch = new Stopwatch();

            string saveImageResultDirectory = $"{deviceName}\\{result.ToString("G")}\\{folderName}";
            string testFolder = $"{saveImageResultDirectory}\\" +
                $"{testBase.TestInfo.NumBastidor}_" +
                $"{testBase.Model.NumUtilBien}_" +
                $"{folderName}_" +
                $"{DateTime.Now.ToShortDateString().Replace("/", "")}_" +
                $"{DateTime.Now.ToLongTimeString().Replace(":", "")}";

            //FTP
            try
            {
	            using (FTPClient client = new FTPClient(ConfigurationManager.AppSettings["VAFTPDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
	            {
	                foreach (ImageStruct image in listImages)
	                {
	                    string imageCompletePath = testFolder + "\\" + image.ImageName;
	                    using (MemoryStream memoryStream = new MemoryStream())
	                    {
	                        testBase.Logger.Debug("Saving image by result: Making a copy image in the selected path...");
	                        stopwatch.Restart();
	                        client.CreateDirectory(Path.GetDirectoryName(imageCompletePath));
	                        image.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
	                        client.UploadOverwrite(memoryStream, imageCompletePath, 3);
	                        testBase.Logger.Debug($"Saving test image by result: Files Saved in {stopwatch.ElapsedMilliseconds / 1000.0} seconds");
	                        //Registro en la tabla TraceTime
	                        using (DezacService svc = new DezacService())
	                        {
	                            svc.AddTraceTime("TERMINAL_ADDTESTVAFILE", (int)stopwatch.ElapsedMilliseconds, memoryStream.Length);
	                        }
	                    }

	                    //BBDD
	                    var stream = ImageCompressor.CompressImage(image.Image);
	                    var files = testBase.GetVariable("IMAGES_FILES", new Dictionary<string, byte[]>());
	                    var imageName = folderName + "_" + image.ImageName;
	                    files[imageName] = stream;
	                    testBase.SetVariable("IMAGES_FILES", files);

	                    image.Image.Dispose();
	                }
	            }
	        }
	        // FTPCabina Backup
            catch (Exception ex)
            {
                SendMail.Send("Fallo en grabacion de imagen en cabina VA", $"Error en el guardado de la imagen {saveImageResultDirectory} {ex.Message}{ex.StackTrace}", false, false, true);

	            using (FTPClient client = new FTPClient(ConfigurationManager.AppSettings["VAFTPBackUpDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
	            {
	                foreach (ImageStruct image in listImages)
	                {
	                    string imageCompletePath = testFolder + "\\" + image.ImageName;
	                    using (MemoryStream memoryStream = new MemoryStream())
	                    {
	                        testBase.Logger.Debug("Saving image by result: Making a copy image in the selected path...");
	                        stopwatch.Restart();
	                        client.CreateDirectory(Path.GetDirectoryName($"VAFiles{imageCompletePath}"));
	                        image.Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
	                        client.UploadOverwrite(memoryStream, $"VAFiles{imageCompletePath}", 3);
	                        testBase.Logger.Debug($"Saving test image by result: Files Saved in {stopwatch.ElapsedMilliseconds / 1000.0} seconds");
	                        //Registro en la tabla TraceTime
	                        using (DezacService svc = new DezacService())
	                        {
	                            svc.AddTraceTime("TERMINAL_ADDTESTVAFILE", (int)stopwatch.ElapsedMilliseconds, memoryStream.Length);
	                        }
	                    }

	                    //BBDD
	                    var stream = ImageCompressor.CompressImage(image.Image);
	                    var files = testBase.GetVariable("IMAGES_FILES", new Dictionary<string, byte[]>());
	                    var imageName = folderName + "_" + image.ImageName;
	                    files[imageName] = stream;
	                    testBase.SetVariable("IMAGES_FILES", files);

	                    image.Image.Dispose();
	                }
	            }
            }
        }

        private static readonly object _syncObject = new object();

        internal struct ledsPositionAlias
        {
            public List<string> Position; 
            public List<string> Alias;

            public static ledsPositionAlias Create()
            {
                ledsPositionAlias value = new ledsPositionAlias
                {
                    Position = new List<string>(),
                    Alias = new List<string>(),
                };

                return value;
            }
        }

        public enum ResultImageState
        {
            OK = 0,
            USER_FAILED = 1,
            HALCON_FAILED = 2,
            FAILED = 3,
        }

        public enum TypeProcedure
        {
            [Description("FIND_LEDS")]
            FindLeds,
            [Description("FIND_LEDS")]
            FindLedsReturnColors,
            [Description("FIND_SEGMENTS")]
            PaternMatching,
            [Description("BARCODE")]
            ReadBarCode,
            [Description("OCR")]
            OCR,
        }

        public enum TypeBarcode
        {
            None,
            Code_128,
            Code_93,
            _25_Interleaved,
            _25_Interleaved_Dotprint
        }

        private static readonly Dictionary<TypeBarcode, string> TypeBarcodeDictionary = new Dictionary<TypeBarcode, string>
        {
            {TypeBarcode.None, ""},
            {TypeBarcode.Code_128, "Code 128"},
            {TypeBarcode.Code_93, "Code 93"},
            {TypeBarcode._25_Interleaved, "2/5 Interleaved"},
            {TypeBarcode._25_Interleaved_Dotprint, "BARCODE_DOTPRINT" }
        };

        private static readonly Dictionary<TypeProcedure, string> TypeProcedureDictionary = new Dictionary<TypeProcedure, string>
        {
            {TypeProcedure.FindLeds, "HALCON_FINDLEDS_AUTOMATICO"},
            {TypeProcedure.FindLedsReturnColors, "HALCON_FINDLEDS_AUTOMATICO"},
            {TypeProcedure.PaternMatching, "HALCON_PATERNMATCHING_AUTOMATICO"},
            {TypeProcedure.ReadBarCode, ""},
            {TypeProcedure.OCR, "HALCON_OCR_AUTOMATICO"},
        };

        public enum PolaritySelectionEnum
        {
            Light_On_Dark,
            Dark_On_Light,
        }

        private static readonly Dictionary<PolaritySelectionEnum, string> OCRPolaritySelectioDictionary = new Dictionary<PolaritySelectionEnum, string>
        {
            {PolaritySelectionEnum.Dark_On_Light, "dark_on_light" },
            {PolaritySelectionEnum.Light_On_Dark, "light_on_dark" },
        };

        public enum OCRClassificatorEnum
        {
            Universal_AllCharacters,
            Universal_09,
            Universal_09_Symbols,
            Universal_09_AZ,
            Universal_AZ_Symbols,
            Universal_09_AZ_Symbols,
            Universal_AllCharacters_WithRejection,
            Universal_09_WithRejection,
            Universal_09_Symbols_WithRejection,
            Universal_09_AZ_WithRejection,
            Universal_AZ_Symbols_WithRejection,
            Universal_09_AZ_Symbols_WithRejection
        }

        private static readonly Dictionary<OCRClassificatorEnum, string> OCRClassificatorDictionary = new Dictionary<OCRClassificatorEnum, string>
        {
            {OCRClassificatorEnum.Universal_AllCharacters , "Universal_NoRej.occ" },
            {OCRClassificatorEnum.Universal_09 , "Universal_0-9_NoRej.occ" },
            {OCRClassificatorEnum.Universal_09_Symbols , "Universal_0-9+_NoRej.occ" },
            {OCRClassificatorEnum.Universal_09_AZ , "Universal_0-9A-Z_NoRej.occ" },
            {OCRClassificatorEnum.Universal_AZ_Symbols , "Universal_A-Z+_NoRej.occ" },
            {OCRClassificatorEnum.Universal_09_AZ_Symbols , "Universal_0-9A-Z+_NoRej.occ" },
            {OCRClassificatorEnum.Universal_AllCharacters_WithRejection , "Universal_Rej.occ" },
            {OCRClassificatorEnum.Universal_09_WithRejection , "Universal_0-9_Rej.occ" },
            {OCRClassificatorEnum.Universal_09_Symbols_WithRejection , "Universal_0-9+_Rej.occ" },
            {OCRClassificatorEnum.Universal_09_AZ_WithRejection , "Universal_0-9A-Z+_Rej.occ" },
            {OCRClassificatorEnum.Universal_AZ_Symbols_WithRejection , "Universal_A-Z+_Rej.occ" },
            {OCRClassificatorEnum.Universal_09_AZ_Symbols_WithRejection , "Universal_0-9A-Z+_Rej.occ" },
        };

        private static readonly Dictionary<string, string> MatchingResultsDictionary = new Dictionary<string, string>
        {
            {"0", "Error indeterminado"},
            {"-1", "Error: No se enciende ningun segmento"},
            {"-2", "Error: Faltan segmentos por encender o son tenues"},
            {"-3", "Error: Segmentos encendidos que no deberian estarlo"},
            {"-4", "Error: Suciedad, Mancha en el display"},
            {"-5", "Error: Suciedad, Pequeñas manchas en el display"},
            {"-6", "Error: No se ha retirado el plastico protector"},
            {"-7", "Error: Display apagado o con poca intensidad"},
            {"-8", "Error: Display desplazado o rotado"},
            {"-10", "Error: Camara mal parametrizada: Tiempo de exp. bajo"},
            {"-11", "Error: Numero de imagenes de test incorreto"},
            {"-20", "Error: en Detecion indeterminado"},
            {"-21", "Error: Caratula incorrecta, dañada o sucia"},
            {"-22", "Error: No se detecta el ancla"},
            {"-23", "Error: No se detecta la tapa"},
            {"-24", "Error: Selector en posicion incorrecta"},
            {"-30", "Error: LCD No se enciende"},
            {"-31", "Error: LCD Sucio"},
            {"-32", "Error: LCD Pixel Brillante"},
            {"-33", "Error: LCD en la prueba de rasters"},
            {"-34", "Error: LCD en la prueba de MURA"},

            {"Default", "Error con codigo de error imprevisto"},
        };

        private class ResultFinsLedsTest
        {
            public List<string> ColorsList;

            public string Colors
            {
                set
                {
                    ColorsList = value.Replace("\"", "").Trim('[', ']').Split(',').ToList();
                }
            }

            public List<LedFind> Leds
            {
                get
                {
                    var leds = new List<LedFind>();

                    for (int i = 0; i < ColorsList.Count(); i++)
                    {
                        var led = new LedFind()
                        {
                            color = ColorsList[i].Trim(),
                        };
                        leds.Add(led);
                    }

                    return leds;
                }
            }
        }

        private class LedFind
        {
            public string color { get; set; }
        }

        public struct ProcedureInputs
        {
            public List<ImageStruct> Images;
            public string HalconTestName;
            public string DeviceName;
            public TypeProcedure TypeProcedure;
            public string ParameterName;
            public bool MultiImageTest;
            public int CharWidth;
            public int CharHeight;
            public int CharStroke;
            public int MinFragmentSize;
            public PolaritySelectionEnum Polarity;
            public int NumberOfLines;
            public double OrientationOffset;
            public double OrientationTolerance;
            public string RegularExpresion;
            public OCRClassificatorEnum Classificator;
            public string TestName
            {
                get
                {
                    string a = HalconTestName.Replace(DeviceName + "_", "");
                    return a;

                }
            }
                
            public void Dispose()
            {
                foreach (var image in Images)
                    image.Image.Dispose();
            }
        }
    }

}
