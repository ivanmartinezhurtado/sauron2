﻿namespace TaskRunner.Enumerators
{
    public enum RunningState
    {
        Pending,
        Running,
        Paused,
        Completed
    }
}
