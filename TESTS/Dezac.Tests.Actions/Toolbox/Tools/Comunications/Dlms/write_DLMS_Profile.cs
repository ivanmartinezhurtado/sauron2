﻿using Dezac.DLMS;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    
    
        [ActionVersion(1.00)]
        [DesignerAction(Type = typeof(write_DLMS_Profile_Description))]

        public class write_DLMS_Profile : ActionBase
        {

          
            [Category("3. Configuration")]
            [Description("Período")]
            [DefaultValue(0)]
            public int Period { get; set; }



            [Category("3. Configuration")]
            [Description("Número de atributo")]
            [DefaultValue(0)]
            public int Attribute { get; set; }

            [Category("3. Configuration")]
            [Description("Número Obis")]
            [DefaultValue("0")]
            public string Obis { get; set; }




        public override void Execute()
            {

                var T = GetVariable("DlmsDevice") as DLMSDevice;
                T.Read_and_write_profile(Period,Attribute,Obis);






            }
        }

        public class write_DLMS_Profile_Description : ActionDescription
        {
            public override string Name { get { return "WriteDLMSProfile"; } }
            public override string Description { get { return ""; } }
            public override string Category { get { return "Tools/Comunications/DLMS"; } }
            public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
            public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
        }





    }


