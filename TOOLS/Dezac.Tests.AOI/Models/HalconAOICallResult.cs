﻿using System;
using System.Collections.Generic;
using HalconDotNet;

namespace Dezac.Tests.AOI.Models
{
    public class HalconAOICallResult
    {
        public static HalconAOICallResult OK = new HalconAOICallResult { Success = true };

        public bool Success { get; set; }
        public Exception Exception { get; set; }
        public string ErrorMessage { get; set; }
        public Dictionary<string, HTuple> TupleResults { get; set; }
        public Dictionary<string, HTupleVector> VectorResults { get; set; }
        public Dictionary<string, HObject> IconicResults { get; set; }
        public long ExecutionTime { get; set; }

        public HalconAOICallResult()
        {
            TupleResults = new Dictionary<string, HTuple>();
            IconicResults = new Dictionary<string, HObject>();
            VectorResults = new Dictionary<string, HTupleVector>();
        }
    }
}
