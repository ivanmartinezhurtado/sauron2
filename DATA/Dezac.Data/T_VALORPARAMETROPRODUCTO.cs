namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_VALORPARAMETROPRODUCTO")]
    public partial class T_VALORPARAMETROPRODUCTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_VALORPARAMETROPRODUCTO()
        {
            //T_VALORPARAMETROTOLPROD1 = new HashSet<T_VALORPARAMETROTOLPROD>();
            //T_VALORPARAMETROTOLPROD2 = new HashSet<T_VALORPARAMETROTOLPROD>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMGRUPO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPARAM { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPRODUCTO { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VERSION { get; set; }

        [Required]
        [StringLength(200)]
        public string VALOR { get; set; }

        [StringLength(200)]
        public string VALORINICIO { get; set; }

        public DateTime? FECHAALTA { get; set; }

        [StringLength(10)]
        public string IDCATEGORIA { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }

        public virtual T_VALORPARAMETRO T_VALORPARAMETRO { get; set; }

        //public virtual T_VALORPARAMETROTOLPROD T_VALORPARAMETROTOLPROD { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<T_VALORPARAMETROTOLPROD> T_VALORPARAMETROTOLPROD1 { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<T_VALORPARAMETROTOLPROD> T_VALORPARAMETROTOLPROD2 { get; set; }
    }
}
