using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dezac.Labels
{
    public class BarTenderReportService : IDisposable
    {
        private static readonly ILog logger = LogManager.GetLogger("BarTenderReportService");

        public string FileName { get; set; }
        public string PrinterName { get; set; }

        public const string PARAM_PREDEFINIDO = "20";
        public const string PARAM_CONSTANTE = "21";
        public const string PARAM_RESULTADO = "22";
        public const string PARAM_PARAMETRIZACION = "23";
        public const string PARAM_IDENTIFICACION = "24";
        public const string PARAM_CONSIGNA = "25";
        public const string PARAM_CONFIGURATIONFILES = "";

        private BarTenderService bt;

        // Cada Plantilla tiene su grupo de parametrización
        // En este diccionario se establece el mapeo
        private static Dictionary<string, int> mapTipoGrupos = new Dictionary<string, int> {
            { "01", 19 },
            { "02", 21 },
            { "03", 20 },
            { "04", 22 },
            { "05", 23 },
            { "06", 25 },
        };

        public BarTenderReportService(string fileName)
        {
            FileName = fileName;
            bt = new BarTenderService();
        }

        public Dictionary<string, string> Print(ITestContext testContext, string tipoGrupoEtiqueta, EnumPrinters.Printers printer = EnumPrinters.Printers.NINGUNA, ParamValueCollection labelValues = null)
        {
            return Print(testContext, mapTipoGrupos[tipoGrupoEtiqueta], printer);
        }

        public Dictionary<string, string> Print(ITestContext testContext, int tipoGrupoEtiqueta, EnumPrinters.Printers printer = EnumPrinters.Printers.NINGUNA, ParamValueCollection labelValues = null)
        {
            ParamValueCollection values;
            if (labelValues == null)
                values = testContext.GetGroupList(tipoGrupoEtiqueta);
            else
                values = labelValues;

            var dictionary = new Dictionary<string, string>();

            try
            {
                logger.InfoFormat("Abriendo fichero de etiqueta : {0}", FileName);
                bt.Open(FileName);
            }
            catch(System.Runtime.InteropServices.COMException)
            {
                SendMail.Send("FALLO BARTENDER", string.Format("Fallo abriendo fichero de etiqueta. Bastidor : {0}, Producto : {1}", testContext.TestInfo.NumBastidor, testContext.NumProducto), "cjordan@dezac.com");
                logger.InfoFormat("Fallo abriendo el fichero de etiqueta");
                bt.Dispose();
                logger.InfoFormat("Servicio BarTender liberado");
                bt = new BarTenderService();
                logger.InfoFormat("Nuevo servicio BarTender instanciado");
                logger.InfoFormat("Reintento para abrir el fichero de etiqueta : {0}", FileName);
                bt.Open(FileName);              
            }
            logger.InfoFormat("Fichero {0} abierto con éxito", FileName);

            if (printer == EnumPrinters.Printers.NINGUNA)
                PrinterName = SelectPrint(testContext, tipoGrupoEtiqueta);
            else
                PrinterName = PrinterTools.SelectPrinterByPatternOrDefault(EnumPrinters.RelationPrinterAndPrinterName[printer]);

            if (string.IsNullOrEmpty(PrinterName) || PrinterName == "NINGUNA")
                MessageBox.Show(string.Format("No se ha seleccionado la impresora para este tipo de plantilla --> PLANTILLA TIPO : {0}", ((LabelTamplete.LabelTemplate)tipoGrupoEtiqueta).ToString()));

            bt.PrinterName = PrinterName;
            try
            {
                bt.FillFields((item) =>
                {
                    // Buscamos en la parametrización de la etiqueta el campo que nos viene de BarTender
                    var value = values.Where(p => p.Name == item.Name).FirstOrDefault();
                    if (value == null)
                    {
                        MessageBox.Show(string.Format("Campo de la etiqueta {0} sin parametrización definida!\nSe imprimirá lo que tenga definida la etiqueta de BarTender", item.Name));
                        return true;
                    }

                    if (value.IdCategoria == PARAM_PREDEFINIDO)
                        item.Value = GetPredefinedValue(testContext, item.Name);
                    else if (value.IdCategoria == PARAM_RESULTADO)
                        item.Value = testContext.Resultados.Where(p => p.Name == value.Valor).Select(p => p.Valor).FirstOrDefault();
                    else if (value.IdCategoria == PARAM_CONSIGNA)
                        item.Value = testContext.Consignas.Where(p => p.Name == value.Valor).Select(p => p.Valor).FirstOrDefault();
                    else if (value.IdCategoria == PARAM_IDENTIFICACION)
                        item.Value = testContext.Identificacion.Where(p => p.Name == value.Valor).Select(p => p.Valor).FirstOrDefault();
                    else if (value.IdCategoria == PARAM_PARAMETRIZACION)
                        item.Value = testContext.Parametrizacion.Where(p => p.Name == value.Valor).Select(p => p.Valor).FirstOrDefault();
                    //else if (value.IdCategoria == PARAM_CONFIGURATIONFILES)
                    //    item.Value = GetConfigurationFile(testContext, item.Name);
                    else //if (value.IdCategoria == PARAM_CONSTANTE)
                    {
                        if (item.Name == "IMG") // Provisional hasta que se habilite el nuevo IdCategoria para configurationfiles
                            item.Value = GetConfigurationFile(testContext, value.Valor);
                        else
                            item.Value = value.Valor;
                    }

                    if (item.Value == null)
                    {
                        logger.Warn(string.Format("Valor del campo {0} sin definir o mal definido!\nSe imprimirá el campo en blanco, INFORMAR AL MODEL LEADER!", item.Name));             
                        return true;
                    }

                    logger.InfoFormat("FillFields Key {0} -> Value {1}", item.Name, item.Value);
                    dictionary.Add(item.Name, item.Value);
                    return true;
                });

                int numCopies = 0;
                var parseSucces = Int32.TryParse(values.Where(p => p.Name == "LABEL_NUMBER_COPIES").FirstOrDefault().Valor, out numCopies);
                if (!parseSucces)
                    throw new Exception("Parámetro LABEL_NUMBER_COPIES mal definido, debe ser un valor númerico");

                logger.InfoFormat("Print BarTender");

                bt.Print(numCopies);
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Error en BarTender --> {0}", e.Message));
                logger.Error(string.Format("Error imprimiendo en Bartender --> {0}", e.Message));
            }

            return dictionary;
        }

        public string SelectPrint(ITestContext context, int tipoGrupoEtiqueta)
        {
            var template = mapTipoGrupos.Select(p => p).Where(p => p.Value == tipoGrupoEtiqueta).Select(p => p.Key).FirstOrDefault();

            var printer = template == "01" ? context.TestInfo.PrinterCharacteristics
                                  : template == "02" ? context.TestInfo.PrinterIndividualPackage
                                  : template == "03" ? context.TestInfo.PrinterPackingPackage
                                  : template == "04" ? context.TestInfo.PrinterLabel
                                  : template == "05" ? context.TestInfo.PrinterLabel
                                  : template == "06" ? context.TestInfo.PrinterLabel
                                  : context.TestInfo.PrinterLabel;

            return printer;
        }

        public void Print(ITestContext context, string tipoGrupoEtiqueta, Dictionary<string, string> labelData, int numCopies = 1)
        {
            var values = context.GetGroupList(mapTipoGrupos[tipoGrupoEtiqueta]);

            using (var bt = new BarTenderService(FileName))
            {
                PrinterName = SelectPrint(context, mapTipoGrupos[tipoGrupoEtiqueta]);
                bt.PrinterName = PrinterName;
                if (string.IsNullOrEmpty(bt.PrinterName) || bt.PrinterName == "NINGUNA")
                    throw new Exception(string.Format("No se ha seleccionado una impresora para imprimir este tipo de etiquetas"));

                bt.FillFields((item) =>
                {
                    var valueConstantParam = values.Where(p => p.Name == item.Name && p.IdCategoria == PARAM_CONSTANTE).FirstOrDefault();
                    if (valueConstantParam == null)
                    {
                        var valueRunTime = VariablesFillInRunTime.Where(p => p == item.Name).Any();
                        if (valueRunTime)
                            item.Value = GetPredefinedValue(context, item.Name);
                        else
                        {
                            var value = labelData.Where(p => p.Key == item.Name).FirstOrDefault();
                            item.Value = value.Value;
                        }
                    }
                    else
                         if (item.Name == "IMG") // Provisional hasta que se habilite el nuevo IdCategoria para configurationfiles
                            item.Value = GetConfigurationFile(context, values.Where(p => p.Name == item.Name).FirstOrDefault().Valor);
                        else
                            item.Value = valueConstantParam.Valor;

                    return true;
                });

                bt.Print(numCopies);
            }
        }

        public void PrintData(Dictionary<string, string> dataLabel)
        {
            using (var bt = new BarTenderService(FileName))
            {
                bt.PrinterName = PrinterName;

                if (string.IsNullOrEmpty(bt.PrinterName) || bt.PrinterName == "NINGUNA")
                    throw new Exception(string.Format("No se ha seleccionado una impresora para imprimir este tipo de etiquetas"));

                bt.FillFields((item) =>
                {                
                    item.Value = dataLabel.Where(p => p.Key == item.Name).FirstOrDefault().Value;
                    return true;
                });

                bt.Print();
            }
        }

        private string GetPredefinedValue(ITestContext testContext, string name)
        {
            var testInfo = testContext.TestInfo;

            switch(name.ToUpper())
            {
                case "PN":
                    return testInfo.Producto;
                case "PV":
                    return testInfo.ProductoVersion;
                case "CODCIR":
                    return testInfo.CodCircutor;
                case "REFCLI_1":
                    return testInfo.CodCircutor;
                case "REFCLI_2":
                    return testInfo.CostumerCode2;
                case "COD":
                    return testInfo.DeviceID;
                case "MAC_3G":
                    return testInfo.NumMAC3G == null ? string.Empty : testInfo.NumMAC3G.ToUpper();
                case "MAC_WIFI":
                    return testInfo.NumMACWifi == null ? string.Empty : testInfo.NumMACWifi.ToUpper();
                case "SSID":
                    return testInfo.SSID;
                case "MAC":
                    return testInfo.NumMAC == null ? testInfo.NumMAC_PLC == null ? string.Empty : testInfo.NumMAC_PLC.ToUpper() : testInfo.NumMAC.ToUpper();
                case "MAC_PLC":
                    return testInfo.NumMAC_PLC == null ? string.Empty : testInfo.NumMAC_PLC.ToUpper();
                case "PSW":
                    return testInfo.PSW;
                case "NB":
                    return testInfo.NumBastidor.HasValue ? testInfo.NumBastidor.Value.ToString() : string.Empty;
                case "NS":
                    return testInfo.NumSerie;
                case "IMEI":
                    return testInfo.IMEI;
                case "ID":
                    return testInfo.NumSerieInterno == null ? testInfo.NumSerieFamilia : testInfo.NumSerieInterno;
                case "HW":
                    return string.IsNullOrEmpty(testInfo.HardwareVersion) ? testInfo.ProductoVersion : testInfo.HardwareVersion;
                case "FW":
                    return testInfo.FirmwareVersion;
                case "FECHAFAB":
                    return testInfo.FechaFabricacion;
                case "IPF":
                    return testInfo.FactoryIP;
                case "DIR1":
                    return testContext.Direcciones.FirstOrDefault();
                case "DIR2":
                    return testContext.Direcciones.Count > 1 ? testContext.Direcciones[1] : null;
                case "DIR3":
                    return testContext.Direcciones.Count > 2 ? testContext.Direcciones[2] : null;
                case "DIR4":
                    return testContext.Direcciones.Count > 3 ? testContext.Direcciones[3] : null;
                case "ORDEN":
                    return testContext.NumOrden.ToString();
                case "QTY":
                    return testContext.Cantidad.ToString();
                case "UDS_CAJA":
                    return testContext.NumEquiposCaja.ToString();
                case "LOTE":
                    return testContext.Lote ?? null;
                case "DESC":
                    return testContext.DescripcionLenguagesCliente.ContainsKey("ES") ? testContext.DescripcionLenguagesCliente["ES"]?.Split(';')[0].Trim() : null;
                case "DES_ES":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("ES"))
                        return null;                      
                    return testContext.DescripcionLenguagesCliente["ES"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["ES"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "DES_IN":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("EN"))
                        return null;
                    return testContext.DescripcionLenguagesCliente["EN"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["EN"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "DES_AL":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("DE"))
                        return null;
                    return testContext.DescripcionLenguagesCliente["DE"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["DE"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "DES_FR":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("FR"))
                        return null;
                    return testContext.DescripcionLenguagesCliente["FR"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["FR"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "DES_IT":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("IT"))
                        return null;
                    return testContext.DescripcionLenguagesCliente["IT"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["IT"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "DES_AR":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("AR"))
                        return null;
                    return testContext.DescripcionLenguagesCliente["AR"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["AR"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "DES_NO":
                    if (!testContext.DescripcionLenguagesCliente.ContainsKey("NO"))
                        return null;
                    return testContext.DescripcionLenguagesCliente["NO"]?.LongCount((c) => c.ToString() == ";") > 0 ? testContext.DescripcionLenguagesCliente["NO"].Split(new char[] { ';' }, 2)[1].Trim() : null;
                case "ATR1":
                    return testContext.Atributos.Count > 0 ? testContext.Atributos[0] : null;
                case "ATR2":
                    return testContext.Atributos.Count > 1 ? testContext.Atributos[1] : null;
                case "ATR3":
                    return testContext.Atributos.Count > 2 ? testContext.Atributos[2] : null;
                case "ATR4":
                    return testContext.Atributos.Count > 3 ? testContext.Atributos[3] : null;
                case "ATR5":
                    return testContext.Atributos.Count > 4 ? testContext.Atributos[4] : null;
                case "EAN":
                    return testContext.EAN;
                case "EAN_MARCA":
                    return testContext.EAN_MARCA;
                case "POSITION":
                    return testContext.ReferenciaLineaCliente;
                case "REFORIGEN":
                    return testContext.ReferenciaOrigen;
                case "PED_CLIENT":
                    return testContext.ReferenciaOrigen;
                case "NS_FROM":
                    return testContext.MinSerialNumberBox;
                case "NS_TO":
                    return testContext.MaxSerialNumberBox;
                case "NS_BOX":
                    return ToQRFromList(testContext.SerialNumberBoxRange);
                case "ORDER_INFO":
                    return BuildOrderInfo(testContext);
                case "GUID":
                    return testInfo.GUID;
                case "DESCOM":
                    return testInfo.DescripcionComercial;
                case "REFVENTA":
                    return testInfo.RefVenta;
                case "CODBCN":
                    return testInfo.CodigoBcn;
            }

            if (name.StartsWith("NS"))
            {
                int i = 2;
                foreach (KeyValuePair<string, string> ns2 in testInfo.NumSerieSubconjunto)
                {
                    if (name == "NS" + i)
                        return ns2.Value;
                    i++;
                }
            }

            //if (testInfo.TypeLabel == TestInfo.labels.Numero_Serie_Subconjunto && !string.IsNullOrEmpty(testInfo.NumSerieInterno))
            //    ns["NS"] = testInfo.NumSerieInterno;

            return null;
        }

        private string GetConfigurationFile(ITestContext testContext, string value)
        {
            var file = testContext.ConfigurationFiles.Get(value);
            string filepath = $"{ConfigurationManager.AppSettings["PathFileLabelTemplate"]}{file.FileName}";
            using (var fs = new FileStream(filepath, FileMode.Create, FileAccess.Write))
            {
                fs.Write(file.File, 0, file.File.Length);
            }
            return filepath;     
        }

        private string CreateLote(ITestContext testContext)
        {
            var lote = string.Format("2{0:yy}{1:00}{2}",
                        DateTime.Now, GetIso8601WeekOfYear(DateTime.Now), Right(testContext.NumOrden.ToString(), 5));

            testContext.Lote = lote;
            return lote;
        }

        private int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
                time = time.AddDays(3);

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        private string BuildOrderInfo(ITestContext testContext)
        {
            var testInfo = testContext.TestInfo;
            var batch = testContext.Lote;

            if (testContext.Lote == null)
                throw new Exception("Campo LOTE vacío en la BBDD, avisar al responsable de la BBDD.");

            if (testContext.EAN == null)
                throw new Exception("Campo código EAN vacío en la BBDD, avisar a VENTAS.");

           return string.Format("]2d02{0}10{1}#400{2}#91{3:00000}#37{4}#92{5}#", testContext.EAN.PadLeft(14, '0'), batch, testContext.ReferenciaOrigen, testContext.ReferenciaLineaCliente, testContext.NumEquiposCaja, " ");
        }

        private string Right(string text, int length)
        {
            if (text == null || text.Length < length)
                return text;

            return text.Substring(text.Length - 5);
        }

        private string ToQRFromList(List<string> list)
        {
            var content = new StringBuilder();

            foreach (var element in list)
                content.Append(element + ";");

            return content.ToString();
        }

        private List<string> VariablesFillInRunTime = new List<string>()
        {
            "DIR1", "DIR2", "DIR3", "DIR4", "DESC", "DES_ES", "DES_IN", "DES_AL", "DES_FR", "DES_IT", "DES_AR", "DES_NO", "ATR1","ATR2","ATR3","CODCIR","REFCLI_1","REFCLI_2",
            "ATR4", "ATR5", "EAN", "EAN_MARCA", "POSITION", "REFORIGEN", "PED_CLIENT", "DESCOM", "REFVENTA", "ORDER_INFO", "UDS_CAJA", "NS_BOX"
        };

        public void Dispose()
        {
        }
    }
}
