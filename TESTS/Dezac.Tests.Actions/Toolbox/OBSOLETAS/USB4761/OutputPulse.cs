﻿using Instruments.IO;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(OutputPulseDescription))]
    
    public class OutputPulse : ActionBase
    {
        [Description("valor de la salida o con $ variable de la salida que des/activamos,")]
        public string Output { get; set; }

        [DefaultValue(500)]
        [Description("El tiempo del pulso, en milisegundos")]
        public int PulseTime { get; set; }

        [Description("El tipo de pulso, positivo o negativo")]
        [DefaultValue(pulseType.Positive)]
        public pulseType PulseType { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase");

            var IO = GetVariable<USB4761>("USB4761", () => { return new USB4761(); });

            if (string.IsNullOrEmpty(Output))
                if (testBase.Shell.MsgBox("Error, no se ha indicado ninguna Output para activar", "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.OK, MessageBoxIcon.Error) != DialogResult.Yes)
                    Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("ninguna Output para activar");

            var output = GetPropertyVar<byte>(Output);

            if (PulseType == pulseType.Negative)
                IO.DO.PulseOff(PulseTime, output);
            else
                IO.DO.PulseOn(PulseTime, output);
        }

        public enum pulseType
        {
            Positive,
            Negative
        }
    }

    public class OutputPulseDescription : ActionDescription
    {
        public override string Name { get { return "Output Pulse"; } }
        public override string Description { get { return "Acción genérica para crear un pulso a una salida de la USB4761 en un tiempo indicado"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.output; } }
    }
}
