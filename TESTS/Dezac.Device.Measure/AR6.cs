﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class AR6 : DeviceBase
    {
        public AR6()
        {
        }

        public AR6(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 38400, int perifericNumber = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)perifericNumber;           
        }

        public AR6(SerialPort serialPort)
        {
            SetSerialPort(serialPort);

        }

        public void SetSerialPort(SerialPort serialPort)
        {
            serialPort.BaudRate = 19200;
            Modbus = new ModbusDeviceSerialPort(serialPort, _logger);
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public Tuple<bool, Gains> Ajuste(int delFirst, int initCount, int numMuestras, int intervalo, Escalas escala, double tensionRef, double currentRef, double phaseGap, Func<Gains, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            var GDefectoV = 10800;
            var GDefectoI = 19600;
            var GDefectoILeak = 14500;
            var GDefectoKW = 15600;

            var powerRef = TriLinePower.Create(tensionRef, currentRef, phaseGap); 

            var list = new StatisticalList(15);
            var result = new Gains();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var vars = ReadAllMeasures();
                    var ILeak = ReadILeak();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(vars.V1, vars.V2, vars.V3, vars.VN, vars.I1, vars.I2, vars.I3, vars.IN,
                        ILeak, vars.VL12, vars.VL23, vars.VL31, vars.KW1, vars.KW2, vars.KW3);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {
                        result.V1 = (ushort)(Math.Pow(tensionRef / list.Average(0), 2) * GDefectoV);
                        result.V2 = (ushort)(Math.Pow(tensionRef / list.Average(1), 2) * GDefectoV);
                        result.V3 = (ushort)(Math.Pow(tensionRef / list.Average(2), 2) * GDefectoV);
                        result.VN = (ushort)(Math.Pow(tensionRef / list.Average(3), 2) * GDefectoV);

                        result.I1 = (ushort)(Math.Pow(currentRef / list.Average(4), 2) * GDefectoI); //Gdefecto de I
                        result.I2 = (ushort)(Math.Pow(currentRef / list.Average(5), 2) * GDefectoI);
                        result.I3 = (ushort)(Math.Pow(currentRef / list.Average(6), 2) * GDefectoI);
                        result.IN = (ushort)(Math.Pow(currentRef / list.Average(7), 2) * GDefectoI);
                        result.ILEAK = (ushort)(Math.Pow(currentRef / list.Average(8), 2) * GDefectoILeak);

                        result.VL12= (ushort)(Math.Pow(tensionRef * Math.Sqrt(3) / list.Average(9), 2) * GDefectoV);
                        result.VL23 = (ushort)(Math.Pow(tensionRef * Math.Sqrt(3) / list.Average(10), 2) * GDefectoV);
                        result.VL31 = (ushort)(Math.Pow(tensionRef * Math.Sqrt(3) / list.Average(11), 2) * GDefectoV);
                      
                        result.KW1 = (ushort)(Math.Pow(powerRef.KW.L1 / list.Average(12), 2) * GDefectoKW);
                        result.KW2 = (ushort)(Math.Pow(powerRef.KW.L2 / list.Average(13), 2) * GDefectoKW);
                        result.KW3 = (ushort)(Math.Pow(powerRef.KW.L3 / list.Average(14), 2) * GDefectoKW);


                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGains(escala, result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        public Tuple<bool, Gains> AjusteDesfase(int delFirst, int initCount, int numMuestras, int intervalo, double tensionRef, double currentRef, double phaseGap, Func<Gains, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            //var GDefectoV = 10800;
            //var GDefectoI = 19600;
            //var GDefectoILeak = 14500;
            //var GDefectoKW = 15600;

            var powerRef = TriLinePower.Create(tensionRef, currentRef, phaseGap);

            var list = new StatisticalList(15);
            var result = new Gains();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var vars = ReadAllMeasures();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(vars.KW1, vars.KW2, vars.KW3);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {


                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGains(AR6.Escalas.AA, result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        public Tuple<bool, ushort> AjusteMedidaCorriente(int delFirst, int initCount, int numMuestras, int intervalo, double currentPattern, Func<ushort, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            var list = new StatisticalList(1);
            ushort result = 0;
            var defaultGain = ReadGainsCurrentMeasure();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var currentMeasure = ReadBatteryCurrent();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(currentMeasure);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {
                        result = (ushort)((currentPattern / currentMeasure) * defaultGain);

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGainsCurrentMeasure(result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }

        public Tuple<bool, ushort> AjusteVoltageBateria(int delFirst, int initCount, int numMuestras, int intervalo, double voltagePattern, Func<ushort, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            var list = new StatisticalList(1);
            ushort result = 0;
            var defaultGain = ReadGainsVoltageBattery();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var voltageMeasure = ReadBatteryVoltage();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(voltageMeasure);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {
                        result = (ushort)((voltagePattern / voltageMeasure) * defaultGain);

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            {
                FlagTest();
                WriteGainsBatteryVoltage(result);
            }

            return Tuple.Create(sampler.Canceled, result);
        }



        public Tuple<bool, Gains> AjusteTension(int delFirst, int initCount, int numMuestras, int intervalo, Escalas escala, double tensionRef, double currentRef, double phaseGap, Func<Gains, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            var GDefectoV = 10800;
            //var GDefectoI = 19600;
            //var GDefectoILeak = 14500;
            //var GDefectoKW = 15600;

            var powerRef = TriLinePower.Create(tensionRef, currentRef, phaseGap);

            var list = new StatisticalList(15);
            var result = new Gains();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var vars = ReadAllMeasures();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(vars.V1, vars.V2, vars.V3, vars.VN, vars.VL12, vars.VL23, vars.VL31);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {
                        result.V1 = (ushort)(Math.Pow(tensionRef / list.Average(0), 2) * GDefectoV);
                        result.V2 = (ushort)(Math.Pow(tensionRef / list.Average(1), 2) * GDefectoV);
                        result.V3 = (ushort)(Math.Pow(tensionRef / list.Average(2), 2) * GDefectoV);
                        result.VN = (ushort)(Math.Pow(tensionRef / list.Average(3), 2) * GDefectoV);                      

                        result.VL12 = (ushort)(Math.Pow(tensionRef * Math.Sqrt(3) / list.Average(4), 2) * GDefectoV);
                        result.VL23 = (ushort)(Math.Pow(tensionRef * Math.Sqrt(3) / list.Average(5), 2) * GDefectoV);
                        result.VL31 = (ushort)(Math.Pow(tensionRef * Math.Sqrt(3) / list.Average(6), 2) * GDefectoV);

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            FlagTest();
            //WriteGains(escala, result);
            //PROBAR SI HAY QUE ESCRIBIR ESTRUCTURA CON TODAS LAS GANANCIAS O SE PUEDEN ESCRIBIR POR SEPARADO.

            return Tuple.Create(sampler.Canceled, result);
        }
        public Tuple<bool, Gains> AjusteCorriente(int delFirst, int initCount, int numMuestras, int intervalo, Escalas escala, double tensionRef, double currentRef, double phaseGap, Func<Gains, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            var GDefectoI = 19600;
            var GDefectoILeak = 14500;

            var powerRef = TriLinePower.Create(tensionRef, currentRef, phaseGap);

            var list = new StatisticalList(15);
            var result = new Gains();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var vars = ReadAllMeasures();
                    var ILeak = ReadILeak();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(vars.I1, vars.I2, vars.I3, vars.IN, ILeak);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {                     
                        result.I1 = (ushort)(Math.Pow(currentRef / list.Average(0), 2) * GDefectoI); //Gdefecto de I
                        result.I2 = (ushort)(Math.Pow(currentRef / list.Average(1), 2) * GDefectoI);
                        result.I3 = (ushort)(Math.Pow(currentRef / list.Average(2), 2) * GDefectoI);
                        result.IN = (ushort)(Math.Pow(currentRef / list.Average(3), 2) * GDefectoI);
                        result.ILEAK = (ushort)(Math.Pow(currentRef / list.Average(4), 2) * GDefectoILeak);                       

                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            FlagTest();
            //WriteGains(escala, result);

            return Tuple.Create(sampler.Canceled, result);
        }
        public Tuple<bool, Gains> AjustePotencia(int delFirst, int initCount, int numMuestras, int intervalo, Escalas escala, double tensionRef, double currentRef, double phaseGap, Func<Gains, bool> validacionAjuste, Func<Gains, bool> validacionMuestra = null)
        {
            FlagTest();

            var GDefectoKW = 15600;

            var powerRef = TriLinePower.Create(tensionRef, currentRef, phaseGap);

            var list = new StatisticalList(15);
            var result = new Gains();

            var sampler = Sampler.Run(
                () => new SamplerConfig { Interval = intervalo, NumIterations = numMuestras },
                (step) =>
                {
                    var vars = ReadAllMeasures();

                    if (step.Step < delFirst)
                        return;

                    if (validacionMuestra != null)
                        return;

                    list.Add(vars.KW1, vars.KW2, vars.KW3);

                    var range = list.MaxRange();
                    if (range > 2)
                    {
                        _logger.InfoFormat("Descartando muestras, rango calculado = {0}, permitido = {1}", range, 2);
                        list.Clear();
                    }

                    if (list.Count(0) >= initCount)
                    {                       
                        result.KW1 = (ushort)(Math.Pow(powerRef.KW.L1 / list.Average(12), 2) * GDefectoKW);
                        result.KW2 = (ushort)(Math.Pow(powerRef.KW.L2 / list.Average(13), 2) * GDefectoKW);
                        result.KW3 = (ushort)(Math.Pow(powerRef.KW.L3 / list.Average(14), 2) * GDefectoKW);


                        var valid = validacionAjuste(result);

                        if (!valid)
                            list.Clear();

                        step.Cancel = valid;
                    }
                });

            if (sampler.Canceled)
            FlagTest();
            //WriteGains(escala, result);

            return Tuple.Create(sampler.Canceled, result);
        }


        #region Write Functions

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        public void DeleteMax(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_MAX, state);
        }

        public void DeleteMin(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_MIN, state);
        }

        public void DeleteEnergies(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DELETE_ENERGIES, state);
        }

        public void DeleteMaxMinAndEnergies()
        {
            LogMethod();
            DeleteMax();
            DeleteMin();
            DeleteEnergies();
        }

        public void WriteGroundConnection(GroundConection value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.GROUND_CONECTION, (ushort)value);
        }

        public void WriteDischargeBattery()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.BATTERY_LEVEL, 0x0000);
        }

        public void WriteSerialNumber(string SerialNumber)
        {
            LogMethod();
            Modbus.WriteString((ushort)Registers.SERIAL_NUMBER, SerialNumber);
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.BASTIDOR, bastidor);
        }

        public void WriteProductoVersion(string articuloVersion)
        {
            LogMethod();
            Modbus.WriteString((ushort)Registers.BASTIDOR, articuloVersion);
        }

        public void WriteSimulationPinzas()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SIMULATION_PINZAS, (ushort)0);
        }

        public void WriteLowScale(bool state = true)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.SCALE, state);
        }

        public void WriteExpandRangeVoltage(int value)//??
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CAMBIO_RANGO_TENSION, value);
        }

        public void WriteRelationTransformation(int value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TRANSFORMATION_RELATION, value);
        }////?¿¿?¿?

        public void WriteStartRegister(bool value = true)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.START_REGISTER, value);
        }

        public void WriteStopRegister(bool value = true)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.START_REGISTER, value);
        }

        public void Reset()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }

        public void ShutDown()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.SHUTDOWN, true);
        }

        public void InitMaxDemand()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.MAX_DEMAND, true);
        }

        public void ActiveFilterILeak(bool value = true)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.FILTER_IF, value);
        }

        public void StartFifoWrite()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.START_FIFO_WRITE, true);
        }

        public void StopFifoWrite()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.STOP_FIFO_WRITE, true);
        }

        public void WriteRunTransient()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.RUN_TRANSIENT, true);
        }


        public void WriteStopTransient()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.STOP_TRANSIENT, true);
        }

        public void WriteFastRAMTest()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.FAST_RAM_TEST, true);
        }

        public void WriteBatteryRAMTest()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.BATTERY_RAM_TEST, true);
        }

        public void WriteScalePinzas(int scale)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SCALE_PINZAS, scale);
        }

        public void ResetEnergies()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.RESET_ENERGY, 0x0001);
        }

        public void WriteGainsCurrentMeasure(int value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.GAINS_BATTERY_CURRENT, (ushort)value);
        }

        public void WriteGainsBatteryVoltage(int value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.GAINS_BATTERY_VOLTAGE, (ushort)value);
        }

        public void WriteGains(Escalas scale, Gains gains)
        {
            LogMethod();

            var addres = 0x0000;

            switch (scale)
            {
                case Escalas.AA:
                    addres = (ushort)Registers.GAINS_ESC_AA;
                    break;
                case Escalas.AB:
                    addres = (ushort)Registers.GAINS_ESC_AB;
                    break;
                case Escalas.BA:
                    addres = (ushort)Registers.GAINS_ESC_BA;
                    break;
                case Escalas.BB:
                    addres = (ushort)Registers.GAINS_ESC_BB;
                    break;
            }

            Modbus.Write<Gains>((ushort)addres, gains);
        }

        public void WriteDefaultGains(Gains gainsDefault)
        {
            LogMethod();              

            foreach(Escalas currentScale in Enum.GetValues(typeof(Escalas)))
                if (currentScale == Escalas.AA)
                    WriteGains(currentScale, gainsDefault);
        }

        public void WriteBatteryLevel(BatteryLevel state)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.BATTERY_LEVEL, (ushort)state);
        }

        public void WriteModeLoadBattery(LoadModeBattery value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.MODE_LOAD_BATTERY, (ushort)value);
        }

        public void WriteBattery150mA()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.UNLOAD_BATTERY, 0x0020);
        }

        public void ResetOffsetsCurrent()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.OFFSETS_CURRRENT, 0x0000);
        }

        public void WriteOffsetsCurrent(int value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.OFFSETS_CURRRENT, (ushort)value);
        }

        #endregion

        #region Read Functions
        public double ReadTemperatureBattery()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.BATTERY_TEMPERATURE) / (double)100;
       }

        public double ReadBatteryVoltage()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.BATTERY_VOLTAGE);
        }

        public double ReadBatteryCurrent()
        {
           LogMethod();
           return Modbus.ReadRegister((ushort)Registers.BATTERY_CURRENT);
        }

        public ushort ReadOffsetsCurrent()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.OFFSETS_CURRRENT);
        }

        public ushort ReadOffsetsCurrentPoints()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.OFFSETS_CURRENT_POINTS);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.DSP, 0x000A).Replace(" ", "");
        }

        public string ReadATMELVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.ATMEL, 0x000A).Replace(" ", "");
       }

        public string ReadAPLIVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.APLI, 3);
        }

        public ushort ReadErrorCode()
        {
             LogMethod();
             return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }

        public DateTime ReadDateTime()
        {
            LogMethod();
            return Modbus.Read<FechaHora>((ushort)Registers.CLOCK).ToDateTime();
        }

        public string ReadSerialNumber()
        {
             LogMethod();
             return Modbus.ReadString((ushort)Registers.SERIAL_NUMBER, 5);
        }

        public int ReadBastidor()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public StatePinzas ReadStatePinza(AddresPinzas pinza)
        {
            LogMethod();
            var reading = Modbus.ReadMultipleRegister((ushort)pinza, 3);

            return (StatePinzas) reading[2];
        }

        public double ReadILeak()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.VAR_IF) / (double)10000;
        }

        public double ReadFrequency()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.VAR_FREC) / (double)100;
        }

        public AllVariables ReadAllVariables()
        {
            LogMethod();

            return Modbus.Read<AllVariables>();
        }

        public VarsMeter ReadAllMeasures()
        {
            LogMethod();

            return Modbus.Read<VarsMeter>((ushort)Registers.VARS_MEASURES);
        }

        public Offsets ReadOffsets()
        {
            LogMethod();

            return Modbus.Read<Offsets>();
        }

        public THD ReadTHD()
        {
            LogMethod();

            return Modbus.Read<THD>((ushort)Registers.VARS_THD);
        }

        public Gains ReadGains(Escalas scale)
        {
            LogMethod();

            var addres = 0x0000;

            switch (scale)
            {
                case Escalas.AA:
                    addres = (ushort)Registers.GAINS_ESC_AA;
                    break;
                case Escalas.AB:
                    addres = (ushort)Registers.GAINS_ESC_AB;
                    break;
                 case Escalas.BA:
                    addres = (ushort)Registers.GAINS_ESC_BA;
                     break;
                case Escalas.BB:
                    addres = (ushort)Registers.GAINS_ESC_BB;
                    break;
            }
            return Modbus.Read<Gains>((ushort)addres);
        }

        public ushort ReadGainsCurrentMeasure()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.GAINS_BATTERY_CURRENT);
        }

        public ushort ReadGainsVoltageBattery()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.GAINS_BATTERY_VOLTAGE);
        }

        #endregion

        #region Enums
        public enum Registers
        {
            FLAG_TEST = 0X2AF8,
            BATTERY_TEMPERATURE = 0X4E22,
            BATTERY_VOLTAGE = 0X4E20,
            BATTERY_CURRENT = 0X4E21,
            SETUP_BATTERY_CONTROL = 0X4E84,
            UNLOAD_BATTERY = 0X4E3E,
            OFFSETS_CURRRENT = 0X4EC1,
            OFFSETS_CURRENT_POINTS = 0X4E36,
            DSP = 0X2AF8,
            ATMEL = 0X2B70,
            APLI = 0X2B8E,
            BATTERY_LEVEL = 0X4E28,
            MODE_LOAD_BATTERY = 0X5208,
            ERROR_CODE = 0XA410,
            GAINS_BATTERY_VOLTAGE = 0X4EB6,
            GAINS_BATTERY_CURRENT = 0X4EB7,
            VARS_MEASURES = 0X001E,
            VAR_IF = 0X00D6,
            VAR_FREC = 0X0052,
            VARS_GAP = 0X0022,
            VARS_THD = 0X006D,
            GAINS_ESC_AA = 0X9C40,
            GAINS_ESC_AB = 0X9C90,
            GAINS_ESC_BA = 0XA474,
            GAINS_ESC_BB = 0XA4D8,
            ENERGIES = 0X7558,
            DELETE_ENERGIES = 0X0834,
            DELETE_MAX = 0X0837,
            DELETE_MIN = 0X0838,
            DELETE_QUALITY_MAX = 0X0839,
            DELETE_QUALITY_MIN = 0X083A,
            MAX_DEMAND = 0X083E,
            DELETE_MAX_DEMAND = 0X083F,
            DELETE_ALL = 0X0848,
            START_REGISTER = 0X1388,
            STOP_REGISTER = 0X1389,
            START_FIFO_WRITE = 0X138A,
            STOP_FIFO_WRITE = 0X138B,
            RUN_TRANSIENT = 0X138C,
            STOP_TRANSIENT = 0X138D,           
            CLOCK = 0X274C,
            GROUND_CONECTION = 0X7532,
            SERIAL_NUMBER = 0X2B5C,
            BASTIDOR = 0X2B66,
            SIMULATION_PINZAS = 0XEEAC,
            OFFSETS = 0X9CA4,
            SCALE = 0X3A98,
            CAMBIO_RANGO_TENSION = 0X7536,
            RESET = 0X07D0,
            SHUTDOWN = 0X07DA,
            HARDWARE_VECTOR = 0X2710,
            UPDATE_FW = 0X2EE0,
            UPDATE_FW_CARD1 = 0X2EE1,
            UPDATE_FW_CARD2 = 0X2EE2,
            UPDATE_FW_CARD3 = 0X2EE3,
            FAST_RAM_TEST = 0X2F44,
            BATTERY_RAM_TEST = 0X2F45,
            SCALE_PINZAS = 0X0898,
            TRANSFORMATION_RELATION = 0X2710,
            COMS_COM1_CONFIG = 0X2710,
            COMS_COM2_CONFIG = 0X2720,
            MAX_DEMAND_CONFIG = 0X272E,
            THD_CONFIG = 0X2738,
            EVQ_CONFIG = 0X2756,
            FLICKER_CONFIG = 0X2760,
            TRANSITORIOS_CONFIG = 0X276A,
            FW = 0X2AF8,
            PRODUCT_NUMBER = 0X2B66,
            POWER_SUPPLY_VERSION = 0X2B70,
            CIRCUIT_TYPE = 0X7530,
            CALCULO_ESCALADO_INDIVIDUAL = 0X7534,
            CALCULO_ONDAS_AMPLIADAS = 0X7536,
            CALCULO_ESCALADO = 0X7538,
            RESET_ENERGY = 0X753A,
            FILTER_IF = 0X753C,
            PINZA_RFLEX = 0X753E,
            CODE_ERROR_EPROM = 0X9FC4,
            CHECKSUM_FW = 0X9FCE,
            CODE_PRODUCTION = 0X9FD8,
            CODE_PAHES_ERROR_ADJUST = 0X9FE2,
            GAINS_ANALOG_OUTPUTS = 0XA028,
            VALUE_ANALOG_OUTPUTS = 0XA08C,
            GAINS_ANALOG_INPUTS = 0XA0F0,
            VALUE_ANALOG_INPUTS = 0XA154    
        }

        [Flags]
        public enum Escalas
        {
            AA,
            AB,
            BA,
            BB
        }

        [Flags]
        public enum BatteryLevel
        {
            CARGADA = 0X01406F40,
            DESCARDA = 0X00000000
        }

        [Flags]
        public enum LoadModeBattery
        {
            WITHOUT_LOAD = 0X0003,
            SLOW_LOAD = 0X0001,
            FAST_LOAD = 0X0000
        }

        [Flags]
        public enum StatePinzas
        {
            NO_DETECTION = 0X0000,
            PINZA_I1 = 0X0100,
            PINZA_I2 = 0X0200,
            PINZA_I3 = 0X0300,
            PINZA_IN = 0X0400,
            PINZA_IK = 0X0500
        }

        [Flags]
        public enum AddresPinzas
        {
            PINZA_IL1 = 0X07D0,
            PINZA_IL2 = 0X07E0,
            PINZA_IL3 = 0X07F0,
            PINZA_IN = 0X0800,
            PINZA_IK = 0X0810
        }

        public enum GroundConection
        {
            DISCONECT = 0X0000,
            CONECT = 0X0001
        }
#endregion

#region Structs

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AllVariables
    {
        public VarsPhase Phases;
        public VarsCalibration Calibration;
        public VarsMeter Measures;
    }              

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VarsPhase
     {
        public double KWL1;
        public double KWL2;
        public double KWL3;
        public double GapL1;
        public double GapL2;
        public double GapL3;
        public double GapLN;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VarsCalibration
    {
        public double V1;
        public double V2;
        public double V3;
        public double VN;
        public double I1;
        public double I2;
        public double I3;
        public double IN;
        public long ILEAK;
        public double VL12;
        public double VL23;
        public double VL31;
        public double KWL1;
        public double KWL2;
        public double KWL3;
        public double Freq;
        public double[] THDV;
        public double[] THDI;
  }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VarsMeter
        {
            private Int32 v1;
            private Int32 i1;
            private Int32 kw1;
            private Int32 xx;
            private Int32 xxx;
            private Int32 xxxx;
            private Int32 xxxxx;
            private Int32 xxxxxx;
            private Int32 v2;
            private Int32 i2;
            private Int32 kw2;
            private Int32 yy;
            private Int32 yyy;
            private Int32 yyyy;
            private Int32 yyyyy;
            private Int32 yyyyyy;
            private Int32 v3;
            private Int32 i3;
            private Int32 kw3;
            private Int32 a;
            private Int32 aa;
            private Int32 aaa;
            private Int32 aaaa;
            private Int32 aaaaa;
            private Int32 vN;
            private Int32 iN;
            private Int32 s;
            public Int32 vl12;
            public Int32 vl23;
            public Int32 vl31;

            public double V1 { get { return v1 / (double)100; } }
            public double I1 { get { return i1 / (double)10000; } }
            public double KW1 { get { return kw1 / (double)10; } }
            public double V2 { get { return v2 / (double)100; } }
            public double I2 { get { return i2 / (double)10000; } }
            public double KW2 { get { return kw2 / (double)10; } }
            public double V3 { get { return v3 / (double)100; } }
            public double I3 { get { return i3 / (double)10000; } }
            public double KW3 { get { return kw3 / (double)10; } }
            public double VN { get { return vN / (double)100; } }
            public double IN { get { return iN / (double)10000; } }
            public double VL12 { get { return vl12 / (double)100; } }
            public double VL23 { get { return vl23 / (double)100; } }
            public double VL31 { get { return vl31 / (double)100; } }

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct THD
        {
            private UInt16 x;
            private UInt16 v1;
            private UInt16 xx;
            private UInt16 v2;
            private UInt16 xxx;
            private UInt16 v3;
            private UInt16 xxxx;

            private UInt16 vN;
            private UInt16 b;

            private UInt16 i1;
            private UInt16 y;
            private UInt16 i2;
            private UInt16 yy;
            private UInt16 i3;
            private UInt16 yyy;
            private UInt16 iN;

            public UInt16 V1 { get { return v1; } }
            public UInt16 V2 { get { return v2; } }
            public UInt16 V3 { get { return v3; } }
            public UInt16 VN { get { return vN; } }

            public UInt16 I1 { get { return v2; } }
            public UInt16 I2 { get { return v3; } }
            public UInt16 I3 { get { return vN; } }
            public UInt16 IN { get { return iN; } }


        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Gains
        {
            public ushort V1;
            public ushort V2;
            public ushort V3;
            public ushort VN;
            public ushort I1;
            public ushort I2;
            public ushort I3;
            public ushort IN;
            public ushort ILEAK;
            public ushort VL12;
            public ushort VL23;
            public ushort VL31;
            public ushort KW1;
            public ushort KW2;
            public ushort KW3;
        }   

                [StructLayout(LayoutKind.Sequential, Pack = 1)]
                [ModbusLayout(Address = (ushort)Registers.OFFSETS)]
                public struct Offsets
                {
                    public UInt16 V1;
                    public UInt16 I1;
                    public UInt16 V2;
                    public UInt16 I2;
                    public UInt16 V3;
                    public UInt16 I3;
                    public UInt16 nothing;
                    public UInt16 IN;
                    public UInt16 V1_M;
                    public UInt16 VN_M;
                    public UInt16 V2_M;
                    public UInt16 VN;
                    public UInt16 V3_M;
                    public UInt16 IF;
                    public UInt16 nothing1;
                    public UInt16 nothing2;
                }

                [StructLayout(LayoutKind.Sequential, Pack = 1)]
                [ModbusLayout(Address = (ushort)Registers.ENERGIES)]
                public struct Energies
                {
                    public UInt16 ConsumidaActiva;
                    public UInt16 ConsumidaReactiva;
                    public UInt16 ConsumidaCapacitiva;
                    public UInt16 ConsumnidaAparente;
                    public UInt16 GeneradaActiva;
                    public UInt16 GeneradaReactiva;
                    public UInt16 GeneradaCapacitiva;
                    public UInt16 GeneradaAparente;


                }

                [StructLayout(LayoutKind.Sequential, Pack = 1)]
                public struct InfoPinzas
                {
                    public Int32 pinzaI1;
                    public Int32 pinzaI2;
                    public Int32 pinzaI3;
                    public Int32 pinzaIN;
                    public Int32 pinzaIK;
                }

                [StructLayout(LayoutKind.Sequential, Pack = 1)]
                public struct FechaHora
                {
                    public UInt16 Year;
                    public UInt16 Month;
                    public UInt16 Day;
                    public UInt16 Hour;
                    public UInt16 Minutes;
                    public UInt16 Seconds;

                    public static FechaHora Create(DateTime? date = null)
                    {
                        DateTime fecha = date.GetValueOrDefault(DateTime.Now);

                        FechaHora value = new FechaHora
                        {
                            Year = (ushort)fecha.Year,
                            Month = (ushort)fecha.Month,
                            Day = (ushort)fecha.Day,
                            Hour = (ushort)fecha.Hour,
                            Minutes = (ushort)fecha.Minute,
                            Seconds = (ushort)fecha.Second,
                        };

                        return value;
                    }

                    public DateTime ToDateTime()
                    {
                        return new DateTime(Year, Month, Day, Hour, Minutes, Seconds);
                    }
                }

                [StructLayout(LayoutKind.Sequential, Pack = 1)]
                public struct HardwareVector
                {
                    public ushort unknow;
                    public ushort unknow1;
                    public ushort unknow2;
                    public ushort unknow3;
                    public ushort unknow4;
                    public ushort unknow5;
                }


        #endregion
    }
}
