﻿using log4net;
using Renci.SshNet;
using System.IO;

namespace Comunications.Ethernet
{
    public class SCPClient
    {
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(SCPClient));

        private ScpClient _scp;

        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public PrivateKeyFile PrivateKeyFile { get; set; }

        public SCPClient(string host, string username, string password, string privateKeyFile = null)
        {
            Host = host;
            UserName = username;
            Password = password;

            if (!string.IsNullOrEmpty(privateKeyFile))
                PrivateKeyFile = new PrivateKeyFile(privateKeyFile);

            if (PrivateKeyFile == null)
            {
                _logger.InfoFormat("Creando objeto SCPClient con username y password");
                _scp = new ScpClient(Host, UserName, Password);
            }
            else
            {
                _logger.InfoFormat("Creando objeto SCPClient con username y privateKeyFile");
                _scp = new ScpClient(Host, UserName, PrivateKeyFile);
            }
        }

        public SCPClient(string host, int port, string username, string password, string privateKeyFile = null)
        {
            Host = host;
            Port = port;
            UserName = username;
            Password = password;

            if (!string.IsNullOrEmpty(privateKeyFile))
                PrivateKeyFile = new PrivateKeyFile(privateKeyFile);

            if (PrivateKeyFile == null)
            {
                _logger.InfoFormat("Creando objeto SCPClient con username, puerto y password");
                _scp = new ScpClient(Host, Port, UserName, Password);
            }
            else
            {
                _logger.InfoFormat("Creando objeto SCPClient con username, puerto y privateKeyFile");
                _scp = new ScpClient(Host, Port, UserName, PrivateKeyFile);
            }

            
        }
        
        protected ScpClient scp
        {
            get
            {
                if (!_scp.IsConnected)
                    _scp.Connect();

                return _scp;
            }
        }

        public void DownLoad(string fileName, string destinationName)
        {           
            using (var stream = File.OpenWrite(destinationName))
            {
                _logger.InfoFormat("Descargando fichero --> {0}", fileName);
                scp.Download(fileName, stream);
                _logger.InfoFormat("Fichero descargado --> {0}", destinationName);
            }
        }

        public void UpLoad(string fileName, string destinationName)
        {
            using (var stream = File.OpenRead(fileName))
            {
                _logger.InfoFormat("Subiendo fichero --> {0}", fileName);
                scp.Upload(stream, destinationName);
                _logger.InfoFormat("Fichero subido --> {0}", destinationName);
            }
        }

        public void Disconnect()
        {
            scp.Dispose();
        }
    }
}
