﻿using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.27)]
    public class CITILUXTest : TestBase
    {
        #region Constants
        private const byte REDLIGHT = 20;
        private const byte ANALOGICS_BATTERY_NEGATIVE = 21;
        private const byte ANALOGICS_BATTERY_POSITIVE = 17;
        private const byte DC20_DC200_SELECT = 18;
        private const byte KEY_1_ACTUATOR = 12;
        private const byte KEY_2_ACTUATOR = 11;
        private const byte KEY_3_ACTUATOR = 10;
        private const byte KEY_4_ACTUATOR = 9;
        private const byte CPU_LED = 15;
        private const byte COM_LED = 16;

        private const byte FREE1_LED = 17;
        private const byte FREE2_LED = 18;
        private const byte EMB_LED = 19;
        private const byte LIN_LED = 20;
        private const byte ACT_LED = 21;

        private const byte RS_232_485422_SELECT = 22;
        private const byte RS_485_422_SELECT = 19;
        private const byte DEVICE_PRESENCE = 9;

        private const byte IN_RELE_1 = 10;
        private const byte IN_RELE_2 = 11;
        private const byte IN_RELE_3 = 12;
        private const byte IN_RELE_4_NO = 14;
        private const byte IN_RELE_4_NC = 13;

        #endregion

        private Tower3 tower;
        private CITILUX clux;
        private double transformationRelation;
        private double current;
        private bool CDP0 = false;

        public void TestInitialization(int comPort = 0)
        {     
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (comPort == 0)
                comPort = Comunicaciones.SerialPort;

            clux = AddInstanceVar(new CITILUX(comPort), "UUT");
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", clux.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", clux.GetType().Name), ParamUnidad.SinUnidad);

            clux.Modbus.PerifericNumber = 255;
            clux.Modbus.Retries = 5;
            clux.Modbus.WaitToRetryMilliseconds = 300;

            CDP0 = Identificacion.MODELO.Contains("CDP0");

            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.ActiveElectroValvule();
            tower.IO.DO.Off(REDLIGHT);

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[DEVICE_PRESENCE])
                {
                    if (Shell.MsgBox("NO SE HA DETECTADO EL EQUIPO EN EL UTIL O NO SE DETECTA EL ANCLA COLOCADO EN EL EQUIPO . ¿QUIERES VOLVER A VERIFICARLO?", "NO SE DETECTA EQUIPO",
                        System.Windows.Forms.MessageBoxButtons.YesNo) == DialogResult.Yes)
                        return false;
                    throw new Exception("Error. El usuario ha rechazado comprobar la presencia de equipo o la detección del ancla");
                }
                return true;
            }, "Error. Equipo no detectado o no se detecta la presencia del ancla del equipo", 5, 500, 1000);

            tower.IO.DO.On(5, 6);
        }

        public void TestBatteryComsumption(int waitTimeStabilisation = 2000)
        {
            tower.IO.DO.Off(ANALOGICS_BATTERY_NEGATIVE, ANALOGICS_BATTERY_POSITIVE);

            tower.Chroma.ApplyOff();

            tower.LAMBDA.Initialize();
            tower.LAMBDA.ApplyOff();
            tower.LAMBDA.ApplyPresetsAndWaitStabilisation(12, 1);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            var defs = new AdjustValueDef(Params.I_DC.Null.Bateria.Name, 0, Params.I_DC.Null.Bateria.Min(), Params.I_DC.Null.Bateria.Max(), 0, 0, ParamUnidad.mA);
            TestMeasureBase(defs, (step) =>
            {
                var value = tower.LAMBDA.ReadCurrent();
                return value;
            }, 0, 2, 2000);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void TestSupplyConsumption(int waitTimeStabilisation = 2000)
        {
            tower.Chroma.ApplyOffAndWaitStabilisation();

            tower.Chroma.SetVoltageRange(Chroma.VoltageRangeEnum.HIGH);

            tower.Chroma.ApplyPresetsAndWaitStabilisation(85, 0, 50);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.Null.EnVacio.Vmin.Name, Params.KW.Null.EnVacio.Vmin.Min(), Params.KW.Null.EnVacio.Vmin.Max(),
                () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vmin fuera de margenes");

            TestConsumo(Params.KVAR.Null.EnVacio.Vmin.Name, Params.KVAR.Null.EnVacio.Vmin.Min(), Params.KVAR.Null.EnVacio.Vmin.Max(),
                () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vmin fuera de margenes");

            tower.Chroma.ApplyPresetsAndWaitStabilisation(265, 0, 50);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.Null.EnVacio.Vmax.Name, Params.KW.Null.EnVacio.Vmax.Min(), Params.KW.Null.EnVacio.Vmax.Max(),
                () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vmax fuera de margenes");

            TestConsumo(Params.KVAR.Null.EnVacio.Vmax.Name, Params.KVAR.Null.EnVacio.Vmax.Min(), Params.KVAR.Null.EnVacio.Vmax.Max(),
                () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vmax fuera de margenes");

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            Delay(waitTimeStabilisation, "Espera a la estabilización del arranque del equipo para medir su consumo");

            TestConsumo(Params.KW.Null.EnVacio.Vnom.Name, Params.KW.Null.EnVacio.Vnom.Min(), Params.KW.Null.EnVacio.Vnom.Max(),
                () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa en vacio a Vnom fuera de margenes");

            TestConsumo(Params.KVAR.Null.EnVacio.Vnom.Name, Params.KVAR.Null.EnVacio.Vnom.Min(), Params.KVAR.Null.EnVacio.Vnom.Max(),
                () => tower.CvmminiChroma.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva en vacio a Vnom fuera de margenes");
        }

        public void TestCommunications()
        {
            tower.IO.DO.Off(RS_232_485422_SELECT);
            tower.IO.DO.Off(RS_485_422_SELECT);

            SamplerWithCancel((p) => 
            {
                clux.FlagTest();
                return true; 
            }, "Error. Error en las comunicaciones en el falg de test", 5, 1500, 1500, false, false);

            var firmwareVersion = clux.ReadFirmwareVersion().CToNetString();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de software incorrecta"), ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                clux.FlagTest();

                clux.DisabledEmbeddedComunication();

                clux.WriteDateTime(DateTime.Now);

                Delay(200, "espera");

                clux.WriteHardwareVector(Convert.ToUInt16(Identificacion.VECTOR_HARDWARE));

                Delay(200, "espera");

                clux.WriteInitialMessage(Identificacion.MESSAGE_DISPLAY);

                return true;

            }, "", 15, 2500, 2500, false, false);
        }

        public void TestDisplay()
        {

            SamplerWithCancel((p) =>
            {
                clux.FlagTest();
                clux.LCDBacklight(true);
                var result = Shell.MsgBox("Se enciende la retroiluminación del LCD?", "Comprobación retroiluminación LCD", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                Assert.IsTrue(result == System.Windows.Forms.DialogResult.Yes, Error().PROCESO.PRODUCTO.ERROR_SUBPROCESO("Error. En el encendido retroiluminación del LCD"));
                return true;
            }, "Error. En el encendido retroiluminación del LCD", cancelOnException: false, throwException: false);

            clux.FlagTest();
            clux.ClearDisplay();

            SamplerWithCancel((p) =>
            {
                clux.FlagTest();
                clux.DisplayTest();
                var result = Shell.MsgBox("Se encienden correctamente los segmentos del LCD??", "Comprobación segmentos LCD", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                Assert.IsTrue(result == System.Windows.Forms.DialogResult.Yes, Error().PROCESO.PRODUCTO.ERROR_SUBPROCESO("Error. Fallo en la comprobación de los segmentos verticales del LCD."));
                return true;
            }, "Error. Fallo en la comprobación de los segmentos verticales del LCD.", cancelOnException: false, throwException: false);

            clux.FlagTest();
            clux.ClearDisplay();

            SamplerWithCancel((p) =>
            {
                clux.FlagTest();
                clux.LCDBacklight(false);
                var result = Shell.MsgBox("Se apaga la retroiluminación del LCD?", "Comprobación retroiluminación LCD", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                Assert.IsTrue(result == System.Windows.Forms.DialogResult.Yes, Error().PROCESO.PRODUCTO.ERROR_SUBPROCESO("Error. En el apagado retroiluminación del LCD"));
                return true;
            }, "Error. En el apagado retroiluminación del LCD", cancelOnException: false, throwException: false);

        }

        public void TestBridgeAndKeyboardAndLeds(byte portComRs232 = 9, byte portComRs485 = 7)
        {
            var baudMicro = (int)Configuracion.GetDouble("BAUDRATE_MICRO", 9600, ParamUnidad.SinUnidad);
            var baudEmbedded = (int)Configuracion.GetDouble("BAUDRATE_EMBEDDED", 38400, ParamUnidad.SinUnidad);

            tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(5000, "Espera Inicio");

            tower.IO.DO.On(KEY_1_ACTUATOR, KEY_4_ACTUATOR);

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            clux.Modbus.PortCom = portComRs485;
            clux.Modbus.PerifericNumber = 255;
            clux.Modbus.BaudRate = baudMicro;

            var TimeoutException1 = (int)Configuracion.GetDouble("TIME_WAIT_FLAG_TEST", 25000, ParamUnidad.SinUnidad); ;

            Delay(TimeoutException1, "Esperamos para que el embedded entre en modo pasarela");
           
            SamplerWithCancel((p) => { clux.FlagTest(); return true; }, "Error. no comunicamos con el equipo despues de apagar y encender para entrar en modo pasarela", 3, 500);

            TestLeds();

            //TEST RS485 // RS422
            //********************
            clux.Modbus.PortCom = portComRs485;
            clux.Modbus.PerifericNumber = 255;
            clux.WriteRSMode(CITILUX.rsModeEnum.Rs485);

            if (CDP0)
                tower.IO.DO.On(RS_232_485422_SELECT, RS_485_422_SELECT); //Selección de 422
            else
            {
                //Selección de 485
                tower.IO.DO.On(RS_232_485422_SELECT);
                tower.IO.DO.Off(RS_485_422_SELECT);
            }

            clux.Modbus.PerifericNumber = 16;
            clux.Modbus.BaudRate = baudEmbedded;
            clux.Modbus.TimeOut = 500;

            var TimeoutException2 = (int)Configuracion.GetDouble("TIME_WAIT_TEST_PASARELA", 20000, ParamUnidad.SinUnidad); ;

            Delay(TimeoutException2, "Esperamos para que el embedded entre en modo pasarela");
            //Delay(17000, "Espera de 17 segundos");

            SamplerWithCancel((p) => { clux.ActivateRTSmodeRS485(); return true; }, "Error de comunicaciones activa RTS modo RS485", 20, 1000, 2000);
            SamplerWithCancel((p) => { clux.DeactivateTestModeTimer(); return true; }, "Error en las comunicaciones con el Embedded", 6, 1500);

            //TEST RS485 / 422 PASARELA
            //*************************
            clux.Modbus.PerifericNumber = 255;
            SamplerWithCancel((p) => { clux.ReadFirmwareVersion(); return true; }, "Error de comunicaciones de la pasarela en modo RS485", 2, 500);

            tower.IO.DO.Off(RS_232_485422_SELECT, RS_485_422_SELECT);

            //TEST RS232 
            //****************
            clux.Modbus.BaudRate = baudMicro;
            clux.WriteRSMode(CITILUX.rsModeEnum.Rs232);
            clux.Modbus.PortCom = portComRs232;
            clux.Modbus.PerifericNumber = 16;
            clux.Modbus.BaudRate = baudEmbedded;

            clux.Modbus.TimeOut = 500;
            SamplerWithCancel((p) => { clux.DeactivateTestModeTimer(); return true; }, "Error en las comunicaciones con el Embedded por RS232", 6, 1500);

            var macIPEmbedded = clux.ReadMACIPEmbedded();
            Resultado.Set("MAC", macIPEmbedded.MAC, ParamUnidad.SinUnidad);
            Resultado.Set("VersionARM", macIPEmbedded.VersionEmbedded, ParamUnidad.SinUnidad);

            if (Convert.ToInt64(macIPEmbedded.MAC.Replace(":", ""),16) == 0)
                Error().UUT.FIRMWARE.PARAMETROS_NO_GRABADOS("MAC_EMBEDDED == 0").Throw();

            //TEST RS232 PASARELA
            //********************
            clux.Modbus.PerifericNumber = 255;
            SamplerWithCancel((p) => { clux.ReadFirmwareVersion(); return true; }, "Error de comunicaciones de la pasarla en modo RS232", 2, 500);

            //Grabar mac y version al micro STM
            //**********************************
            clux.Modbus.BaudRate = baudMicro;
            clux.Modbus.PortCom = portComRs485;
            clux.FlagTest();
            clux.WriteMAC(macIPEmbedded.MAC);
            clux.WriteARMSoftwareVersion(macIPEmbedded.VersionEmbedded);
            TestInfo.NumMAC = macIPEmbedded.MAC;

            tower.IO.DO.Off(RS_232_485422_SELECT);
            tower.IO.DO.Off(RS_485_422_SELECT);

            tower.IO.DO.Off(KEY_1_ACTUATOR);
            tower.IO.DO.Off(KEY_4_ACTUATOR);

            tower.Chroma.ApplyOffAndWaitStabilisation();
            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            SamplerWithCancel((p) => { clux.FlagTest(); return true; }, "Error en las comunicaciones con el equipo despues de apagar y endender en la pasarela", 3, 500);

            clux.DisabledEmbeddedComunication();

            TestTeclado();
        }

        public void TestDigitalInputs()
        {
            tower.IO.DO.OffRange(0, 3);
            tower.IO.DO.OffRange(40, 43);
            tower.IO.DO.OffRange(8, 12);

            var inputsValue = new List<KeyValuePair<byte, CITILUX.InputValues>>();
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(0, CITILUX.InputValues.INPUT_1));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(1, CITILUX.InputValues.INPUT_2));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(2, CITILUX.InputValues.INPUT_3));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(3, CITILUX.InputValues.INPUT_4));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(40, CITILUX.InputValues.INPUT_5));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(41, CITILUX.InputValues.INPUT_6));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(42, CITILUX.InputValues.INPUT_7));
            inputsValue.Add(new KeyValuePair<byte, CITILUX.InputValues>(43, CITILUX.InputValues.INPUT_8));

            SamplerWithCancel((p) =>
            {
                return clux.ReadDigitalInputs() == CITILUX.InputValues.NO_INPUTS;
            }, "Error. alguna entrada activada cuando no deberia", 5, 200);

            foreach( KeyValuePair<byte, CITILUX.InputValues> input in inputsValue)
            {
                tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    return clux.ReadDigitalInputs() == input.Value;
                }, string.Format("Error. no se detecta la entrada {0} activada", input.Value.ToString()), 5, 200);

                tower.IO.DO.Off(input.Key);
            }

            SamplerWithCancel((p) =>
            {
                return clux.ReadDigitalInputs() == CITILUX.InputValues.NO_INPUTS;
            }, "Error. alguna entrada activada después de desactivar todos los reles", 5, 200);
     
        }

        public void TestDigitalOutputs()
        {
            var ouputsValue = new List<KeyValuePair<byte, CITILUX.Reles>>();
            ouputsValue.Add(new KeyValuePair<byte, CITILUX.Reles>(IN_RELE_1, CITILUX.Reles.RELE_1));
            ouputsValue.Add(new KeyValuePair<byte, CITILUX.Reles>(IN_RELE_2, CITILUX.Reles.RELE_2));
            ouputsValue.Add(new KeyValuePair<byte, CITILUX.Reles>(IN_RELE_3, CITILUX.Reles.RELE_3));
            ouputsValue.Add(new KeyValuePair<byte, CITILUX.Reles>(IN_RELE_4_NO, CITILUX.Reles.RELE_4));

            foreach (KeyValuePair<byte, CITILUX.Reles> output in ouputsValue)
            {
                clux.WriteOutputs(output.Value, true);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 10, 200);

                clux.WriteOutputs(output.Value, false);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} activada despues de desactivarla", output.Value.ToString()), 10, 200);
            }

            clux.WriteOutputs(CITILUX.Reles.RELE_4, true);

            SamplerWithCancel((p) =>
            {
                return !tower.IO.DI[IN_RELE_4_NC];
            }, string.Format("Error. no se detectado el la salida {0} activada", IN_RELE_4_NC.ToString()), 10, 200);

            clux.WriteOutputs(CITILUX.Reles.RELE_4, false);

            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[IN_RELE_4_NC];
            }, string.Format("Error. se ha detectado la salida {0} activada despues de desactivarla", IN_RELE_4_NC.ToString()), 10, 200);

        }

        public void TestSetupDefault()
        {

            var embeddedVersion = clux.ReadEmbeddedVersion().CToNetString();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_EMBEDDED, embeddedVersion, Identificacion.VERSION_EMBEDDED, Error().SOFTWARE.SECUENCIA_TEST.VERSION_INCORRECTA("Error. Version del embedded incorrecta"), ParamUnidad.SinUnidad);   

            clux.FlagTest();

            clux.WriteTransformRatio();

            current = Configuracion.GetDouble("SECONDARY_CURRENT",0.2, ParamUnidad.A);

            transformationRelation = (double)clux.ReadTransformationRatio().CurrentPrimary / current;

            var voltageGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_V + "_DEFECTO", 7300, ParamUnidad.Puntos);
            var currentGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_I + "_DEFECTO", CDP0 ? 550 : 850, ParamUnidad.Puntos);
            var activepowerGain = Configuracion.GetDouble(ConstantsParameters.FactorGains.GAIN_KW + "_DEFECTO", 2500, ParamUnidad.Puntos);
            ushort DC20 = 9100;
            ushort DC200 = 9100;

            clux.WriteMeasureGains((ushort) voltageGain,(ushort)voltageGain, (ushort)currentGain, (ushort)activepowerGain, DC20, DC200);
            
            clux.WritePhaseOffsetGains();

            clux.ClearEnergy();
        }

        public void TestDC20Adjust(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            clux.Modbus.Retries = 8;

            tower.IO.DO.On(ANALOGICS_BATTERY_POSITIVE, ANALOGICS_BATTERY_NEGATIVE);

            tower.IO.DO.Off(DC20_DC200_SELECT);

            tower.LAMBDA.ApplyPresets(8, 0.02); //Parametros hardcodeados a partir de la base de datos

            Delay(1000, "Esperamos estabilizacion del LAMBDA");

            var defs = new AdjustValueDef(Params.GAIN_I.Other("DC").Ajuste.esc_20mA.Name, 0, Params.GAIN_I.Other("DC").Ajuste.esc_20mA.Min(), Params.GAIN_I.Other("DC").Ajuste.esc_20mA.Max(), 0, 0, ParamUnidad.Puntos);

            var result = clux.AdjustDC20(delFirts , initCount , samples, timeInterval,
                (value) =>
                {
                    defs.Value = value;
                    return defs.IsValid();
                },
                (continous) => (continous[0] > 0));

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, Error().UUT.AJUSTE.MARGENES("Error valores de ajuste de ganacia de la entrada DC20 fuera de margenes"));

            defs = new AdjustValueDef(Params.I_DC.Null.Verificacion.esc_20mA.Name, 0, Params.I_DC.Null.Verificacion.esc_20mA.Min(), Params.I_DC.Null.Verificacion.esc_20mA.Max(), 0, 0, ParamUnidad.mA);

            TestMeasureBase(defs, (step) =>
            {
                var value = clux.ReadContinousVariables().DC20;
                return value;
            }, 1, 2, 500);

            tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void TestDC200Adjust(int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            tower.LAMBDA.ApplyOffAndWaitStabilisation();

            tower.IO.DO.On(ANALOGICS_BATTERY_POSITIVE, ANALOGICS_BATTERY_NEGATIVE);

            tower.IO.DO.On(DC20_DC200_SELECT);

            tower.LAMBDA.ApplyPresets(8, 0.2); 

            Delay(1000, "Esperamos estabilizacion del LAMBDA");

            var defs = new AdjustValueDef(Params.GAIN_I.Other("DC").Ajuste.esc_200mA.Name, 0, Params.GAIN_I.Other("DC").Ajuste.esc_200mA.Min(), Params.GAIN_I.Other("DC").Ajuste.esc_200mA.Max(), 0, 0, ParamUnidad.Puntos);

            var result = clux.AdjustDC200(delFirts, initCount, samples, timeInterval,
                (value) =>
                {
                    defs.Value = value;
                    return defs.IsValid();
                },
                (continous) => (continous[0] > 0));

            defs.AddToResults(Resultado);

            Assert.IsTrue(result.Item1, Error().UUT.AJUSTE.MARGENES("Error valores de ajuste de ganacia de la entrada DC200 fuera de margenes"));

            defs = new AdjustValueDef(Params.I_DC.Null.Verificacion.esc_200mA.Name, 0, Params.I_DC.Null.Verificacion.esc_200mA.Min(), Params.I_DC.Null.Verificacion.esc_200mA.Max(), 0, 0, ParamUnidad.mA);

            TestMeasureBase(defs, (step) =>
            {
                var value = clux.ReadContinousVariables().DC200;
                return value;
            }, 1, 2, 500);
            tower.LAMBDA.ApplyOffAndWaitStabilisation();
        }

        public void TestShortCircuit(int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            var offsetsV = new TriAdjustValueDef(Params.V.L1.Offset.Name, Params.V.Null.Offset.Min(), Params.V.Null.Offset.Max(), 0, 0, ParamUnidad.mV);
            var offsetsI = new TriAdjustValueDef(Params.I.L1.Offset.Name, Params.I.Null.Offset.Min(), Params.I.Null.Offset.Max(), 0, 0, ParamUnidad.mA);

            var OffsetsList = new List<TriAdjustValueDef>();
            OffsetsList.Add(offsetsV);
            OffsetsList.Add(offsetsI);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            clux.WriteTransformRatio();

            TestMeasureBase(OffsetsList,
                (step) =>
                {
                    var vars = clux.ReadAllVariables();
                    return new double[] { vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage, 
                        vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current };

                }, delFirts, samples, timeInterval);

            var ConsignasV = new TriLineValue { L1 = 151, L2 = 200, L3 = 250 };
            var ConsignasI = new TriLineValue { L1 = 0.08, L2 = 0.12, L3 = 0.16 };

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defsFreq = new AdjustValueDef { Name = Params.FREQ.Null.Ajuste.Name,  Min = Params.FREQ.Null.Ajuste.Min(),  Max = Params.FREQ.Null.Ajuste.Max(), Unidad = ParamUnidad.Hz };

            clux.FlagTest();

            int Sincronismo = 0;
            int i = 0;

            TestMeasureBase(defsFreq,
              (step) =>
              {
                  i++;
                  clux.WriteSynchronism(10000);
                  Delay(1500, "Esperando grabación factor de defecto de auste de sincronismo...");
                  var freq = clux.ReadAllVariables().ThreePhase.Frequency;
                  Sincronismo = (int)(50000000 / (freq * 100)) + i;
                  clux.WriteSynchronism(Sincronismo);
                  Delay(1500, "Esperando grabación factor calculado de sincronismo...");
                  return clux.ReadAllVariables().ThreePhase.Frequency;

              }, 0, 5, timeInterval);
        
            Resultado.Set(Params.SYNC.Null.Ajuste.Name, Sincronismo, ParamUnidad.Puntos); 

            Delay(2000, "Esperando grabación factor calculado de auste de sincronismo...");

            var crucePistasV = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, 0, 0, 0, Params.V.Null.CrucePistas.Tol(), ParamUnidad.V);
            crucePistasV.L1.Average = ConsignasV.L1;
            crucePistasV.L2.Average = ConsignasV.L2;
            crucePistasV.L3.Average = ConsignasV.L3;

            var crucePistasI = new TriAdjustValueDef(Params.I.L1.CrucePistas.Name, 0, 0, 0, Params.I.Null.CrucePistas.Tol(), ParamUnidad.A); ;
            crucePistasI.L1.Average = ConsignasI.L1 * transformationRelation;
            crucePistasI.L2.Average = ConsignasI.L2 * transformationRelation;
            crucePistasI.L3.Average = ConsignasI.L3 * transformationRelation;

            var ShortCircuitList = new List<TriAdjustValueDef>();
            ShortCircuitList.Add(crucePistasV);
            ShortCircuitList.Add(crucePistasI);

            clux.WriteTransformRatio();

            TestMeasureBase(ShortCircuitList,
                (step) =>
                {
                    var vars = clux.ReadAllVariables();
                    return new double[] { vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage, 
                        vars.Phases.L1.Current, vars.Phases.L2.Current, vars.Phases.L3.Current };

                }, delFirts, samples, timeInterval);
        }

        public void TestAdjust(double PF = 0, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Ajuste.Name, CDP0 ? 0.25 : 0.2, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, PF, ParamUnidad.Grados);

            tower.PowerSourceIII.Tolerance = Consignas.GetDouble("TOLERANCIA_ESTABILIZACION_MTE", 0.4, ParamUnidad.PorCentage);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, angleGap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            clux.WriteTransformRatio();

            var defs = new AdjustValueDef() { Name = Params.TEMP.Null.Verificacion.Name, Max = Params.TEMP.Null.Verificacion.Max(), Min = Params.TEMP.Null.Verificacion.Min(), Unidad = ParamUnidad.Grados };

            TestMeasureBase(defs, (step) =>
            {
                var temp = clux.ReadTemperature();
                return temp;
            }, 1, 3, 500);

            var triGains = new List<TriAdjustValueDef>();
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_KW.L1.Ajuste.Name, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            triGains.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.L23.Ajuste.Name, Params.GAIN_V.L31.Ajuste.Name, 0, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var measureGains = clux.CalculateAdjustMeasureFactors(delFirts, initCount, samples, timeInterval, adjustVoltage, current, transformationRelation,
                (value) =>
                {

                    triGains[0].L1.Value = value.Voltage.L1;
                    triGains[0].L2.Value = value.Voltage.L2;
                    triGains[0].L3.Value = value.Voltage.L3;

                    triGains[1].L1.Value = value.Current.L1;
                    triGains[1].L2.Value = value.Current.L2;
                    triGains[1].L3.Value = value.Current.L3;

                    triGains[2].L1.Value = value.Power.L1;
                    triGains[2].L2.Value = value.Power.L2;
                    triGains[2].L3.Value = value.Power.L3;

                    triGains[3].L1.Value = value.Compound.L12;
                    triGains[3].L2.Value = value.Compound.L23;
                    triGains[3].L3.Value = value.Compound.L31;

                    return !HasError(triGains, false).Any();

                });

            foreach (var res in triGains)
                res.AddToResults(Resultado);

            if (HasError(triGains, false).Any())
                throw new Exception(string.Format("Error en el ajuste de {0} fuera de margenes", HasError(triGains, false).FirstOrDefault().Name));

            clux.WriteMeasureGains(measureGains.Item2.Voltage,measureGains.Item2.Compound,measureGains.Item2.Current,measureGains.Item2.Power);

            Delay(2000, "Esperando grabación factores calculados...");

        }

        public void TestGapAdjust(double PF = 60, int delFirts = 2, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.AjusteDesfase.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.AjusteDesfase.Name, CDP0 ? 0.25 : 0.2, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.AjusteDesfase.Name, PF, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, angleGap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            clux.WriteTransformRatio();

            var triDefs = new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name,Params.GAIN_DESFASE.L1.Ajuste.Min(), Params.GAIN_DESFASE.L1.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var powerFactor = Math.Cos((PF.ToRadians()));
            var powerReference = adjustVoltage * adjustCurrent * powerFactor;

            var offsetGains = clux.CalculateGapAdjustFactors(delFirts, initCount, samples, timeInterval, powerReference, powerFactor, transformationRelation,
                (value) =>
                {
                    triDefs.L1.Value = value.L1;
                    triDefs.L2.Value = value.L2;
                    triDefs.L3.Value = value.L3;

                    return !triDefs.HasMinMaxError();
                },
                (activePowerOffset) => (activePowerOffset.L1 > 0 && activePowerOffset.L2 > 0 && activePowerOffset.L3 > 0));

            triDefs.AddToResults(Resultado);

            Assert.IsTrue(offsetGains.Item1, Error().UUT.AJUSTE.MARGENES("Error valor de ajuste de ganancia del desfase fuera de márgenes"));

            clux.WritePhaseOffsetGains(offsetGains.Item2);

        }

        public void TestVerification(double PF = 60, int initCount = 3, int samples = 6, int timeInterval = 1100)
        {
            var adjustVoltage = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 300, ParamUnidad.V);
            var adjustCurrent = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 0.2, ParamUnidad.A);
            var angleGap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Verificacion.Name, PF, ParamUnidad.Grados);

            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(adjustVoltage, adjustCurrent, 50, angleGap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            clux.WriteTransformRatio();

            Delay(2000, "Esperando grabación realcion transformacion...");

            var average = adjustVoltage * adjustCurrent * Math.Cos((angleGap.ToRadians()));

            var toleranciaV = Params.V.Null.Verificacion.Tol();
            var toleranciaI = Params.I.Null.Verificacion.Tol();
            var toleranciaKw = Params.KW.Null.Verificacion.Tol();

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.KW.L1.Verificacion.Name, 0, 0, average, toleranciaKw, ParamUnidad.W));
            defs.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.Name, 0, 0, adjustVoltage, toleranciaV, ParamUnidad.V));
            defs.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.Name, 0, 0, adjustCurrent * transformationRelation, toleranciaI, ParamUnidad.A));

            TestCalibracionBase(defs,
                () =>
                {
                    var vars = clux.ReadAllVariables();

                    return new double[] { 
                    vars.Phases.L1.ActivePower/transformationRelation, vars.Phases.L2.ActivePower/transformationRelation, vars.Phases.L3.ActivePower/transformationRelation,  
                    vars.Phases.L1.Voltage, vars.Phases.L2.Voltage, vars.Phases.L3.Voltage, 
                    vars.Phases.L1.Current /transformationRelation , vars.Phases.L2.Current/ transformationRelation, vars.Phases.L3.Current/ transformationRelation};
                },
                () =>
                {
                    var voltageRef = tower.PowerSourceIII.ReadVoltage();
                    var currentRef = tower.PowerSourceIII.ReadCurrent();

                    TriLineValue PowerActive = new TriLineValue()
                    {
                        L1 = voltageRef.L1 * currentRef.L1 * Math.Cos((PF.ToRadians())),
                        L2 = voltageRef.L2 * currentRef.L2 * Math.Cos((PF.ToRadians())),
                        L3 = voltageRef.L3 * currentRef.L3 * Math.Cos((PF.ToRadians())),
                    };

                    return new double[] 
                    { 
                        PowerActive.L1, PowerActive.L2, PowerActive.L3, 
                        voltageRef.L1, voltageRef.L2, voltageRef.L3, 
                        currentRef.L1, currentRef.L2 , currentRef.L3
                    };
                }, initCount, samples, timeInterval);

        }

        public void TestHardwareVerification(double voltage = 300, double PF = 60, int delFirts = 2, int samples = 4, int timeInterval = 500)
        {
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, PF, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.THD_V.L1.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0,0, ParamUnidad.PorCentage));
            defs.Add(new TriAdjustValueDef(Params.THD_I.L1.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0,0, ParamUnidad.PorCentage));

            TestMeasureBase(defs,
                (step) =>
                {
                    var THD = clux.ReadTotalHarmonicDistorsion();
                    return new double[] { THD.VoltageL1, THD.VoltageL2, THD.VoltageL3, THD.CurrentL1, THD.CurrentL2, THD.CurrentL3 };
                }, delFirts, samples, timeInterval);

            var defsFreq = new AdjustValueDef(Params.FREQ.Null.Verificacion.Name, 0, Params.FREQ.Null.Verificacion.Min(), Params.FREQ.Null.Verificacion.Max(), 0, 0, ParamUnidad.Hz);
      
            TestMeasureBase(defsFreq,
              (step) =>
              {
                  return  clux.ReadAllVariables().ThreePhase.Frequency;
              }, delFirts, samples, timeInterval);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            clux.WriteFastRamTest();

            clux.WriteBatteryRamTest();
       
        }

        public void TestCustomization()
        {
            clux.FlagTest();

            clux.WriteSerialNumber(TestInfo.NumSerie.ToString());

          //  clux.WriteBastidor(TestInfo.NumBastidor.Value.ToString());

            clux.WriteProductNumber(Model.NumProducto + "/" + Model.Version); 

            clux.WriteErrorCode(0);

            Delay(600, "Esrcibimos Numero de Serie");

            clux.ClearEnergy();

            tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(4500, "Encendemos el equipo");

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            SamplerWithCancel((p) => { clux.FlagTest(); return true; }, "Error de comunicaciones al volver a enceder el equipo", 20, 1000);

            var numserie = clux.ReadSerialNumber();

            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numserie, TestInfo.NumSerie.ToString(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, valor de número de serie leido diferente al grabado"), ParamUnidad.SinUnidad);

            //var bastidor = clux.ReadBastidor();
            //Assert.AreEqual(ConstantsParameters.TestInfo.NUM_BASTIDOR, bastidor, TestInfo.NumBastidor.Value, "Error, valor de numero de bastidor leido diferente al grabado", ParamUnidad.SinUnidad);
            Delay(1500, "Esperamos par que cargue la hora bien");

            DateTime cluxDateTime = clux.ReadDateTime();

            double timeDiff = (DateTime.Now.Subtract(cluxDateTime)).TotalSeconds;

            int secondsLimit = 30;

            Assert.AreGreater(secondsLimit, timeDiff, Error().PROCESO.PRODUCTO.INSTRUCCIONES("Error. El equipo ha perdido mas de 30 segundos"));

            var vectorHardware = clux.ReadHardwareVector().ToString();

            Assert.AreEqual(ConstantsParameters.Identification.VECTOR_HARDWARE, vectorHardware, Identificacion.VECTOR_HARDWARE, Error().UUT.HARDWARE.VECTOR_HARDWARE("Error, el vector de hardware grabado no corresponde con el leido"), ParamUnidad.SinUnidad);

            var codError = clux.ReadErrorCode();

            Assert.AreEqual(codError, 0, Error().UUT.CONFIGURACION.SETUP("Error, el codigo de error grabado no corresponde con el leido"));

            clux.FlagTest();

            var initMessage = clux.ReadInitialMessage();

            Assert.IsTrue(initMessage.Contains(Identificacion.MESSAGE_DISPLAY), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, el mensaje inicial grabado no corresponde con el leido"));

            var relationTransformer = clux.ReadTransformationRatio();

           // Assert.AreEqual(relationTransformer, 0, "Error, el mensaje inicial grabado no corresponde con el leido");
        }

        public void TestFinish()
        {
            if (clux != null)
                clux.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();

                if (tower.IO != null)
                {
                    tower.IO.DO.OffWait(100, 9);

                    tower.IO.DO.OffWait(100, 10);

                    tower.IO.DO.OffWait(100, 11);

                    tower.IO.DO.OffWait(100, 12);

                    tower.IO.DO.OffWait(100, 7);

                    tower.IO.DO.OffWait(100, 6);

                    tower.IO.DO.OffWait(100, 5);       
                }

                tower.Dispose();
            }
        }

        //----------------------------------------------

        private void TestLeds()
        {
            var ledsValue = new Dictionary<byte, CITILUX.LedsEnum>();
            ledsValue.Add(COM_LED, CITILUX.LedsEnum.Led_COM);
            ledsValue.Add(CPU_LED, CITILUX.LedsEnum.Led_CPU);

            foreach (KeyValuePair<byte, CITILUX.LedsEnum> led in ledsValue)
            {
                clux.WriteLeds(led.Value, true);

                SamplerWithCancel((p) =>
                {
                    return tower.IO.DI[led.Key];
                }, string.Format("Error. no se detectado el LED {0} activado", led.Value.ToString()), 10, 200);

                clux.WriteLeds(led.Value, false);

                SamplerWithCancel((p) =>
                {
                    return !tower.IO.DI[led.Key];
                }, string.Format("Error. se ha detectado el LED {0} activado despues de apagarlo", led.Value.ToString()), 10, 200);
            }
        }

        private void TestTeclado()
        {
            tower.IO.DO.OffRange(8, 12);

            Delay(500, "Esperamos para la el test de leds y teclado por pasarela");

            var keyboardValue = new Dictionary<byte, CITILUX.KeyboardKeys>();
            keyboardValue.Add(KEY_1_ACTUATOR, CITILUX.KeyboardKeys.KEY_1);
            keyboardValue.Add(KEY_2_ACTUATOR, CITILUX.KeyboardKeys.KEY_2);
            keyboardValue.Add(KEY_3_ACTUATOR, CITILUX.KeyboardKeys.KEY_3);
            keyboardValue.Add(KEY_4_ACTUATOR, CITILUX.KeyboardKeys.KEY_4);
 
            SamplerWithCancel((p) =>
            {
                var value = (ushort)clux.ReadKeyboard() & 0x0F;
                return value == (ushort)CITILUX.KeyboardKeys.NO_KEY;
            }, "Error. el equipo detecta tecla pulsada cuando no la hay.", 8, 500, 1000);

            foreach (KeyValuePair<byte, CITILUX.KeyboardKeys> input in keyboardValue)
            {
                tower.IO.DO.On(input.Key);

                Delay(200, "Esperamos activación tecla");

                SamplerWithCancel((p) =>
                {
                    var value = (ushort)clux.ReadKeyboard() & 0x0F;
                    return value == (ushort)input.Value;
                }, string.Format("Error. no se detecta la tecla {0} activada", input.Value.ToString()), 5, 200);

         
                tower.IO.DO.Off(input.Key);
            }
            SamplerWithCancel((p) =>
            {
                var value = (ushort)clux.ReadKeyboard() & 0x0F;
                return value == (ushort)CITILUX.KeyboardKeys.NO_KEY;
            }, "Error. el equipo detecta tecla pulsada cuando no la hay despues de probarlas", 8, 500, 1000);
        }

        //----------------------------------------------

        public void TestRewriteMAC(int comPort = 0)
        {
            if (comPort == 0)
                comPort = Comunicaciones.SerialPort;

            clux = new CITILUX(comPort);
            clux.Modbus.PerifericNumber = 255;
            SamplerWithCancel((p) => { clux.FlagTest(); return true; }, "Error. Error en las comunicaciones.", 5, 500);

            var firmwareVersion = clux.ReadFirmwareVersion().CToNetString();

            Assert.AreEqual(firmwareVersion, "4.1.3.00", Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Version de software incorrecta"));

            var mac = clux.ReadMAC();

            clux.FlagTest();

            clux.WriteMAC(mac);

            Resultado.Set("MAC", mac, ParamUnidad.SinUnidad);

            Delay(1000, "Espera grabación de la MAC");

            Shell.MsgBox(" OK ", "GRABACION MAC", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

        }

        public void TestFinishRewriteMAC()
        {
            if (clux != null)
                clux.Dispose();
        }
    }
}