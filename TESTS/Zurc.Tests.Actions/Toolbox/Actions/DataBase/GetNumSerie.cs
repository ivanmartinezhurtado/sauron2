﻿using Dezac.Core.Exceptions;
using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;
using Zurc.Data;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(GetNumSerieDescription))]
    public class GetNumSerie : ActionBase
    {
        public bool ManualSerialNumber { get; set; }

        public override void Execute()
        {
            if (SequenceContext.Current.RunningInstances > 1)
                RunTestSequentiallyOrdered("_GETNUMSERIE_", InternalExecute);
            else
                InternalExecute();
        }

        private void InternalExecute()
        {
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null || data.TestInfo == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestConext").Throw();

            if (string.IsNullOrEmpty(data.TestInfo.NumSerie))
            {
                string numSerie = null;

                if (ManualSerialNumber)
                    numSerie = GetNumSerieManual();
                else
                    using (var svc = new ZurcService())
                    {
                        numSerie = svc.GetNumSerie(data.NumProducto, data.NumOrden);
                    }

                if (string.IsNullOrEmpty(numSerie))
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("NUMERO SERIE NO EXISTE PARA ESTA ORDEN O PRODUCTO").Throw();

                data.TestInfo.NumSerie = numSerie.Trim();
            }

            Logger.InfoFormat("TestInfo.NumSerie: {0}", data.TestInfo.NumSerie);
        }

        private string GetNumSerieManual()
        {
            var context = SequenceContext.Current;
            var shell = context.Services.Get<IShell>();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            string msg = "ASIGNAR NUMERO DE SERIE MANUAL (SI) - ASIGNAR NUEVO NUMERO DE SERIE (NO)";
            string numSerie = "";

            var result = shell.MsgBox(msg, "ATENCIÓN", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
                numSerie = testBase.GetIntroNumSerieByUser();

            return numSerie;
        }
    }

    public class GetNumSerieDescription : ActionDescription
    {
        public override string Description { get { return "Action to get serial number from Product and Version"; } }
        public override string Name { get { return "GetNumSerie"; } }
        public override string Category { get { return "ZurcTest/Trazabilidad"; } }
        public override Image Icon { get { return Properties.Resources.Barcode_icon; } }
        public override string Dependencies { get { return "InitTestBase;InitProductAction;InitBastidorMultipleAction"; } }
    }
}
