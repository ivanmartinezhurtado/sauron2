﻿using Comunications.Ethernet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Device.Protection
{
    public class Pasarela : DeviceBase
    {
        public string HostName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string PrivateKeyFile { get; set; }

        private SSHClient _ssh;
        protected SSHClient Ssh
        {
            get
            {
                if (_ssh == null)
                {
                    if (string.IsNullOrEmpty(PrivateKeyFile))
                        _ssh = new SSHClient(HostName, UserName, Password, _logger);
                    else
                        _ssh = new SSHClient(HostName, UserName, Password, _logger, PrivateKeyFile);

                }

                return _ssh;
            }
        }
    }
}
