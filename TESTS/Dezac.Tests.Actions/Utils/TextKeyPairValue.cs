﻿using Dezac.Tests.Actions.Views;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    public class TextValuePair
    {
        [Description("Nombre de la variable a crear en el contexto del test")]
        public string Key { get; set; }

        [Description("Valor de la variable creada con el nombre asignado en la porpiedad Key")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Value { get; set; }
    }
}
