﻿using Dezac.Core.Utility;
using Dezac.Tests.Extensions;
using Dezac.Tests.Visio;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(CameraIDSModifyParametersDescription))]
    public class CameraIDSModifyParameters : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre para guardar los parámetros del action")]
        public string DataInput { get; set; }

        [Category("3. Configuration")]
        [Description("Número de serie nuevo")]
        public string SerialNumber { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de exposición")]
        [DefaultValue(100)]
        public double ExposureTime { get; set; }

        [Category("3. Configuration")]
        [Description("Saturación")]
        [DefaultValue(155)]
        public int Saturation { get; set; }

        [Category("3. Configuration")]
        [Description("Cuanto se quiere enfocar en la cámara")]
        [DefaultValue(1D)]
        public double Zoom { get; set; }

        [Category("3. Configuration")]
        [Description("Autoenfoque habilitado")]
        [DefaultValue(false)]
        public bool AutoFocusEnable { get; set; }

        [Category("3. Configuration")]
        [Description("Enfoque manual")]
        [DefaultValue(154)]
        public int FocusManual { get; set; }

        [Category("3. Configuration")]
        [Description("Ganancia")]
        [DefaultValue(0)]
        public int Gain { get; set; }

        [Category("3. Configuration")]
        [Description("AutoGanancia")]
        [DefaultValue(false)]
        public bool AutoGain { get; set; }

        [Category("3. Configuration")]
        [Description("Nivel de oscuridad")]
        [DefaultValue(1)]
        public int BlackLevel { get; set; }

        [Category("3. Configuration")]
        [Description("Offset de color rojo")]
        [DefaultValue(1)]
        public int RedOffset { get; set; }

        [Category("3. Configuration")]
        [Description("Offset de color azul")]
        [DefaultValue(1)]
        public int BlueOffset { get; set; }

        [Category("3. Configuration")]
        [Description("Autobalance de nivel blanco ")]
        [DefaultValue(false)]
        public bool AutoWhiteBalance { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de espera inicial")]
        [DefaultValue(2000)]
        public int WaitTimeInit { get; set; }

        [Category("3. Configuration")]
        [Description("Cuadros por segudo")]
        [DefaultValue(1)]
        public double Framerate { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de píxel")]
        [DefaultValue(15)]
        public int PixelClock { get; set; }

        [Category("3. Configuration")]
        [Description("Formato de la imagen")]
        [DefaultValue(IDSFormats._5M)]
        public IDSFormats ImageFormat { get; set; }

        [Category("3. Configuration")]
        [Description("Nitidez")]
        [DefaultValue(-2)]
        public int? Sharpness { get; set; }

        public override void Execute()
        {
            ImageFormat = ImageFormat == 0 ? IDSFormats._5M : ImageFormat;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha instanciado el método de inicialización del testbase").Throw();


            if (!string.IsNullOrEmpty(DataInput))
                SerialNumber = GetVariable<string>(DataInput);

            if (Sharpness == null)
                Sharpness = -2;

            var settings = new CameraIDSSettings()
            {
                AutoFocusEnable = AutoFocusEnable,
                ExposureTime = ExposureTime,
                FocusManual = (uint)FocusManual,
                SerialNumber = SerialNumber,
                Saturation = Saturation,
                Zoom = Zoom,
                Gain = Gain,
                AutoGain = AutoGain,
                BlackLevel = BlackLevel,
                Framerate = Framerate,
                PixelClock = PixelClock,
                AutoWhiteBalance = AutoWhiteBalance,
                BlueOffset = BlueOffset,
                RedOffset = RedOffset,
                AutoGainShutter = AutoGain,
                Sharpness = Sharpness.Value,
                ImageFormat = (ushort)ImageFormat
            };

            testBase.ModifyCameraIDSParameters(settings, WaitTimeInit);
        }
    }

    public class CameraIDSModifyParametersDescription : ActionDescription
    {
        public override string Name { get { return "CameraIDSModifyParameters"; } }
        public override string Description { get { return "Modifica los parámetros de captura para una cámara IDS en particular"; } }
        public override string Category { get { return "Tools/CamaraIDS"; } }
        public override Image Icon { get { return Properties.Resources.Devices_camera_web_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
