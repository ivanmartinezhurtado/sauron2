﻿using Instruments.Utility;
using System;
using System.ComponentModel;

namespace Instruments.Towers
{
    [InstrumentsVersion(1.03)]
    public class Tower1 :TowerBase<Tower1>, IDisposable, ITower
    {
        private const double VoltageFactorInputMux = 21.4;

        [Browsable(false)]
        public override double MeasureMultimeter(InputMuxEnum input, MagnitudToMeasure magnitud, int waitTimeMeasure = 500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            var result = base.MeasureMultimeter(input, magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);
            switch (input)
            {
                case InputMuxEnum.IN14:
                    result *= VoltageFactorInputMux;
                    break;
            }

            return result;
        }

    }
}
