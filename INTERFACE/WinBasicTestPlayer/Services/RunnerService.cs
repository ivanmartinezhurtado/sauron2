﻿using TaskRunner;
using TaskRunner.Model;

namespace WinTestPlayer.Services
{
    public class RunnerService
    {
        public SequenceRunner Runner { get; private set; }

        public RunnerService()
        {
            Runner = new SequenceRunner();
        }

        public void Play(SequenceContext doc)
        {
            Runner.Play((numInstance, sharedVariables) =>
            {
                doc.NumInstance = numInstance;
                return doc;
            }, 1);
        }
    }
}
