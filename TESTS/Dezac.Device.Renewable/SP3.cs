﻿
using Comunications.Message;
using Comunications.Protocolo;
using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.IO.Ports;
using System.Linq;

namespace Dezac.Device.Renewable
{
    [DeviceVersion(1.00)]
    public class SP3 : DeviceBase
    {
        private string St_Num { get; set; }

        public SP3()
        {
        }

        public SP3(string port)
        {
            SetPort(port);
        }

        public void SetPort(string port)
        {
            if (!port.ToUpper().Contains("COM"))
                port = "COM" + port;

            if (SerailPort == null)
            {
                var sp = new SerialPort(port, 19200, Parity.None, 8, StopBits.One);
                sp.ReadTimeout = 2000;
                sp.WriteTimeout = 2000;
                sp.DtrEnable = false;
                sp.RtsEnable = false;
                sp.Handshake = Handshake.None;
                SerailPort = new SerialPortAdapter(sp);

                Transport = new MessageTransport(new ASCIIProtocol(), SerailPort, _logger);
            }
            else
                SerailPort.PortName = port;
        }

        public MessageTransport Transport { get; internal set; }
        public SerialPortAdapter SerailPort { get; internal set; }
  
        #region METHODS DEVICE

        public string ReadDeviceInformation()
        {
            LogMethod();
            var versionHW = SendCommandHex(Registers.XCPDT_READ_INFO).HexStringToBytes();
            return string.Format("{0}.{1}", versionHW[1] / 10, versionHW[1] % 10);
        }

        public byte ReadPotenciometro()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_POTENCIOMETRO).HexStringToBytes();
            return response[0];
        }

        public string ReadStatusDetection()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_DETECTION).HexStringToBytes();
            return  response[0] == 0 ? "GREEN" : "RED";
        }

        public struct PulseCount
        {
            public byte pulsOK;
            public byte pulsNOK;
            public byte pulsOUT;
            public byte pulsNUM;
        }

        public PulseCount ReadPulsesCount()
        {
            LogMethod();
            var response = SendCommandHex<PulseCount>(Registers.XCPDT_READ_PULSES_COUNT);
            return response;
        }

        public string WriteModoTest()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_MODO_TEST, false);
            return response.Trim();
        }

        public enum typeHeight
        {
            _180_185cm = 0x0a,
            _190_209cm = 0x0b,
            _210_229cm = 0x0c,
            _230_249cm = 0x0d,
            _250_269cm = 0x0e,
            _270_279cm = 0x0f,
            _280_299cm = 0x10,
            _300_319cm = 0x11,
            _320_339cm = 0x12,
            _340_359cm = 0x13,
            _360_379cm = 0x14,
            _380_399cm = 0x15,
            _More_400cm = 0x16
        }

        public string WriteHeight(typeHeight altura)
        {
            LogMethod();
            var hexHight = Convert.ToByte(altura).ToString("X2");
            var response = SendCommandHex(Registers.XCPDT_WRITE_HEIGHT,true, hexHight);
            return response.Trim();
        }

        public byte ReadStatusCommand09()
        {
            LogMethod();
            var response = SendCommandHex(Registers.XCPDT_READ_XXX).HexStringToBytes();
            return response[0];
        }

        #endregion

        #region NUM PERIFERICO

        public void SetStNum(string stNum)
        {
            St_Num = stNum;
        }

        public string WriteResetStNum()
        {
            LogMethod();
            St_Num = "0000";
            var response = SendCommandHex(Registers.XCPDT_DELETE_STNUM, false);
            return response;
        }

        public string WriteStNum(string StNum)
        {
            LogMethod();
            St_Num = StNum;
            var response = SendCommandHex(Registers.XCPDT_WRITE_STNUM, false);
            return response;
        }

        #endregion

        //*******************************************************************************
        //*******************************************************************************

        private string SendCommandHex(Registers instruction, bool hasResponse = true, params object[] args)
        {
            LogMethod();
            var trama = string.Format("{0}{1}", St_Num, ((int)instruction).ToString("X2"));

            if (args != null)
            {
                var tramaArgs = "";
                foreach (object ob in args)
                    tramaArgs += string.Format("{0:00}", ob);
                trama += tramaArgs;
            }

            var cheksum = ComunicationUtility.CheckSum(trama.HexStringToBytes());
            var cheksumHex = cheksum.ToString("X2");

            HexStringMessage message, response;
            message = new HexStringMessage
            {
                AnswerLen = CalcAnswerLen(instruction),
                Text = string.Format("{0}{1}", trama, cheksumHex),
            };

            message.HasResponse = hasResponse;

            response = Transport.SendMessage<HexStringMessage>(message);

            if (hasResponse)
            {
                var responseClear = response.Text.Substring(4); //quitamos Direccion 2 bytes
                _logger.InfoFormat("TX:{0} -> RX: Raw {1}   Bilogy {2}", message.Text, response.Text, responseClear);
                return responseClear;
            }
            else
            {
                _logger.InfoFormat("TX:{0} ", message.Text);
                return "NO HAS RESPONSE";
            }
        }

        private T SendCommandHex<T>(Registers instruction, params object[] args) where T : struct
        {
            LogMethod();
            var trama = string.Format("{0}{1}", St_Num, ((int)instruction).ToString("X2"));

            if (args != null)
            {
                var tramaArgs = "";
                foreach (object ob in args)
                    tramaArgs += string.Format("{0:00}", ob);
                trama += tramaArgs;
            }

            var cheksum = ComunicationUtility.CheckSum(trama.HexStringToBytes());
            var cheksumHex = cheksum.ToString("X2");

            HexStringMessage message, response;
            message = new HexStringMessage
            {
                AnswerLen = CalcAnswerLen(instruction),
                Text = string.Format("{0}{1}", trama, cheksumHex),
            };

            response = Transport.SendMessage<HexStringMessage>(message);

            var responseMessage = response.MessageFrame.Skip(2).ToArray();

            return Comunications.Utility.Extensions.AsignaEstructura<T>(responseMessage);
        }

        private enum Registers
        {
            XCPDT_DELETE_STNUM = 0xDB, // borramos direccion del equipo
            XCPDT_WRITE_STNUM = 0x00, // Programa la direcci�n del equipo
            XCPDT_READ_INFO = 0x05, // Devuelve informaci�n del equipo (tipo y versi�n)
            XCPDT_READ_POTENCIOMETRO = 0x04, // Devuelve valor del potenciometro
            XCPDT_MODO_TEST = 0xDC, // pone en modo test
            XCPDT_READ_DETECTION = 0x10,// Estado de la plaza del parking 
            XCPDT_READ_PULSES_COUNT = 0x85,// lee el contador de impulsos
            XCPDT_WRITE_HEIGHT = 0x11,// envia altura 
            XCPDT_READ_XXX = 0x09,// 
        }

        private int CalcAnswerLen(Registers instruction)
        {
            switch (instruction)
            {
                case Registers.XCPDT_READ_PULSES_COUNT: return 6;
            }
            return 4;
        }

        public override void Dispose()
        {
            if (SerailPort != null)
                SerailPort.Dispose();

            if (Transport != null)
                Transport.Dispose();
        }
    }
}
