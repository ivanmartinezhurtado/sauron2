﻿using Dezac.Device;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.02)]
    public class REC3TestPlacas : TestBase
    {
        private const int BLOQ_SECURITY = 23;
        private const int IN_DETECT_UUT = 11;

        private bool HasReles = false;
        private TowerBoard Tower;

        private REC3 rec3;

        public void TestInitialization(int port = 0)
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out Tower))
            {
                Tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", Tower);
            }

            if (Tower == null)
                throw new Exception("There is not any tower instance to initializate in this test");

            HasReles = Identificacion.MODELO.Trim().ToUpper().Contains("CON_RELE");

            if (port == 0)
                port = Comunicaciones.SerialPort;

            rec3 = new REC3(port);

            rec3.Modbus.PerifericNumber = Comunicaciones.Periferico;

            ((ModbusDeviceSerialPort)rec3.Modbus).RtsEnable = true;

            ((ModbusDeviceSerialPort)rec3.Modbus).DtrEnable = true;

            Tower.Active24VDC();

            SamplerWithCancel((p) =>
            {
                if (!this.Tower.IO.DI[BLOQ_SECURITY])
                    return false;
                
               return true;

            }, "Error no se ha detectado el Bloque de Seguridad Conectado", 40, 500, 1500, false, false);


            SamplerWithCancel((p) =>
            {
                if (!this.Tower.IO.DI[IN_DETECT_UUT])
                    return false;

                return true;

            }, "Error No se ha detectado el equipo o el equipo no lleva el ancla puesta", 5, 500, 1500, false, false);
        }

        public void TestConsumption([Description("Tiempo espera estabilización consumo")]int timeWait = 2000)
        {
            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = "",
                TiempoIntervalo = 1000,
                typePowerSource = TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(Tower, configuration);
            Tower.IO.DO.OnWait(200, 21);
            Delay(2000, "");

            configuration.typeTestPoint = TypeTestPoint.ConCarga;
            this.TestConsumo(Tower, configuration);
        }

        public void TestComunications()
        {
            ushort version = 0;

            SamplerWithCancel((p) =>
            {
                version = rec3.ReadFimrwareVersion();
                return true;
            }, "Error. No se puede establecer comunicación con el equipo.", 5, 500);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. La versión de firmware grabada en el equipo es incorrecta."), ParamUnidad.SinUnidad);

            rec3.WorkMode(REC3.WorkModeValue.Test);

            rec3.WriteErrorCode(255);

            rec3.WriteBastidor((int)TestInfo.NumBastidor);

            Delay(500, "Escritura del nuemro de bastidor al equipo");
        }

        public void TestMeasureVoltage()
        {
            var defs = new List<AdjustValueDef> {
                new AdjustValueDef { Name= Params.V.Other("3").Verificacion.Name, Min=Params.V.Other("3").Verificacion.Min(), Max =  Params.V.Other("3").Verificacion.Max(), Unidad= ParamUnidad.V},
                new AdjustValueDef { Name= Params.V.Other("12").Verificacion.Name, Min=Params.V.Other("12").Verificacion.Min(), Max =  Params.V.Other("12").Verificacion.Max(), Unidad= ParamUnidad.V} };

            TestMeasureBase(defs,
            (step) =>
            {
                var v3 = rec3.ReadValueMeasureV3();
                var v12 = rec3.ReadValueV12();

                return new double[] { v3, v12 };

            }, 1, 5, 600 );

            if (HasReles)
            {
                var measureV = new MagnitudToMeasure()
                {
                    Frequency = freqEnum._50Hz,
                    Magnitud = MagnitudsMultimeter.VoltAC,
                    NumMuestras = 3,
                    Rango = 20,
                    DigitosPrecision = 4,
                    NPLC = 8
                };

                var defs2 = new List<AdjustValueDef> {
                 new AdjustValueDef { Name = Params.V_DC.Other("RELE").Name,  Min = Params.V_DC.Other("RELE").Verificacion.Min(),  Max= Params.V_DC.Other("RELE").Verificacion.Max(), Unidad= ParamUnidad.V },
                 new AdjustValueDef { Name = Params.V_AC.Other("RELE").Name, Min = Params.V_AC.Other("RELE").Verificacion.Min(), Max = Params.V_AC.Other("RELE").Verificacion.Max(), Unidad = ParamUnidad.V } };


                TestMeasureBase(defs2, (Func<int, double[]>)((step) =>
                {
                    measureV.Magnitud = MagnitudsMultimeter.VoltDC;
                    var VDC = this.Tower.MeasureMultimeter(InputMuxEnum.IN14, measureV, 5000);
                    this.Tower.MUX.Reset();

                    measureV.Magnitud = MagnitudsMultimeter.VoltAC;
                    var VAC = this.Tower.MeasureMultimeter(InputMuxEnum.IN14, measureV, 5000);
                    this.Tower.MUX.Reset();

                    return new double[] { VDC, VAC };

                }), 0, 3, 500);
            }
        }

        public void TestMeasureMotor()
        {
            var defsUp = new AdjustValueDef { Name = Params.I.Other("Motor").TestPoint("UP").Name, Min = Params.I.Other("Motor").TestPoint("UP").Min(), Max = Params.I.Other("Motor").TestPoint("UP").Max(), Unidad = ParamUnidad.mA };

            rec3.WriteOutputs(REC3.OuputState.MotorUp);
            Delay(1500, "");

            TestMeasureBase(defsUp,
            (step) =>
            {
                var Imotor = rec3.ReadValueMeasureImotor();
                return Imotor;

            }, 1, 5, 600, true);

            var defsDown = new AdjustValueDef { Name = Params.I.Other("Motor").TestPoint("DOWN").Name, Min = Params.I.Other("Motor").TestPoint("DOWN").Min(), Max = Params.I.Other("Motor").TestPoint("DOWN").Max(), Unidad = ParamUnidad.mA };

            rec3.WriteOutputs(REC3.OuputState.MotorDown);
            Delay(1500, "");

            TestMeasureBase(defsDown,
            (step) =>
            {
                var Imotor = rec3.ReadValueMeasureImotor();
                return Imotor;

            }, 1, 5, 600, true);

        }

        public void TestFinish()
        {
            if (rec3 != null)
                rec3.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();
                Tower.Dispose();
            }
        }

        //**********************************
    }
}
