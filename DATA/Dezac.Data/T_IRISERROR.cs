namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_IRISERROR")]
    public partial class T_IRISERROR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_IRISERROR()
        {
            T_IRISERRORPRODUCTOFASE = new HashSet<T_IRISERRORPRODUCTOFASE>();
            T_IRISERRORFAMILIAFASE = new HashSet<T_IRISERRORFAMILIAFASE>();
            T_TESTERROR = new HashSet<T_TESTERROR>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(50)]
        public string ERRORCODE { get; set; }

        [StringLength(200)]
        public string DESCRIPCION { get; set; }

        [StringLength(200)]
        public string DESCRIPCIONML { get; set; }

        [StringLength(200)]
        public string DIAGNOSTICOML { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_IRISERRORPRODUCTOFASE> T_IRISERRORPRODUCTOFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_IRISERRORFAMILIAFASE> T_IRISERRORFAMILIAFASE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TESTERROR> T_TESTERROR { get; set; }
    }
}
