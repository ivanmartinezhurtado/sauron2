namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TESTFASES
    {
        public int NUMTEST { get; set; }
        public string IDFASE { get; set; }
        public int SECUENCIA { get; set; }
    
        public virtual FASE FASE { get; set; }
        public virtual T_TEST T_TEST { get; set; }
    }
}
