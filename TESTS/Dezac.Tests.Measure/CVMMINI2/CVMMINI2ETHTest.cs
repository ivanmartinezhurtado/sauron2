﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Instruments.Router;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.10)]
    public class CVMMINI2ETHTest : CVMMINI2Test
    {
        string ipEthernet = string.Empty;
        private bool WifiOk = false;

        public void TestInitialization()
        {
            CAMERA_IDS = GetVariable<string>("CAMARA_IDS_DISPLAY", CAMERA_IDS);

            if (VectorHardware.GetString("ALIMENTACION", CVMMINI2.PowerSupply.VAC230.GetDescription(), ParamUnidad.SinUnidad).Trim().ToUpper().Contains("VDC"))     // Averigurar si Vcc es Vdc del Vector de Hardware
            {
                alimentacionDC = true;
                SamplerWithCancel((p) =>
                {
                    Tower.LAMBDA.Initialize();
                    Tower.LAMBDA.ApplyOff();
                    return true;
                }, "Error. No se puede comunicar con la fuente LAMBDA DC", 2, 1500, 0, false, false);
            }

            else
                Tower.Chroma.ApplyOff();

            Tower.PowerSourceIII.ApplyOff();

            Tower.MUX.Reset();
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[BLOQUE_SEGURIDAD])
                {
                    if (Shell.MsgBox("Comprovar que el CONNECTOR DE SEGURIDAD esté conectado y el utillaje CERRADO", "No se detecta el BLOQUE DE SEGURIDAD", System.Windows.Forms.MessageBoxButtons.OKCancel) == DialogResult.OK)
                        return false;
                    Error().HARDWARE.UTILLAJE.BLOQUE_SEGURIDAD().Throw();
                }
                return true;
            }, "Error. BLOQUE DE SEGURIDAD no detectado", 5, 500, 1000);

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[PRESENCIA_EQUIPO])
                {
                    if (Shell.MsgBox("Comprovar que el EQUIPO SE HA INTRODUCIDO EN EL UTIL y el ANCLA ESTA COLOCADA", "No se detecta la PRESENCIA del equipo", System.Windows.Forms.MessageBoxButtons.OKCancel) == DialogResult.OK)
                        return false;
                    Error().HARDWARE.UTILLAJE.DETECCION_EQUIPO().Throw();
                }
                return true;
            }, "Error de la PRESENCIA del equipo. Equipo o ancla no detectados", 5, 500, 1000);

            Tower.IO.DO.OnWait(500, FIJACION_CARRO);
            Tower.IO.DO.OnWait(500, FIJACION_EQUIPO);
            Delay(1500, "Activando pistones de fijacion");

            if (!Tower.IO.DI[IN_FIJACION_EQUIPO])
                throw new Exception("No se detecta la FIJACION DEL EQUIPO activada");
            if (!Tower.IO.DI[PRESENCIA_TAPA])
                throw new Exception("No se detecta la TAPA DEL EQUIPO colocada");

            Tower.IO.DO.OnWait(500, AUX_EV); //Activamos válvula auxiliar
            Tower.IO.DO.OnWait(500, SELEC_BORNAS_ETH); //Aseguramos que la posición es el modelo normal con bornas

            if (!Tower.IO.DI[IN_DETECCION_MODELO_ETHERNET])
                throw new Exception("No se detecta la posición del pistón de selección de modelo en el modelo ETHERNET");

            Tower.IO.DO.On(BORNAS_J2_J3, BORNAS_J5_ETH);

            Delay(1500, "Activando pistones de comunicaciones");

            if (!Tower.IO.DI[IN_BORNAS_1])
                throw new Exception("No se detecta BLOQUE BORNAS J2 y J3 activado");
            if (!Tower.IO.DI[IN_BORNAS_ETH])
                throw new Exception("No se detecta BLOQUE BORNAS J5 y Ethernet activado");
            if (!Tower.IO.DI[PRESENCIA_CUBRE_BORNAS_1])
                throw new Exception("No se detecta el CUBREBORNAS 1 DEL EQUIPO colocado");
        }

        public void TestInitializationTCPWfi(int portTCP = 502, byte periferic = 1, string Ssid = "wifimini2", string Password = "123456789a")
        {
            var router = Router.Default();
            var clients = new List<ClientEntry>();
            WifiOk = false;

            SamplerWithCancel((p) =>
            {
                clients = router.GetConnectedClientsFromWeb(true);
                return clients.Count > 0;
            }, "No se ha encontrado ningun cliente DHCP conectado", 20, 500, 2000, false, false,
            (p) => { Error().HARDWARE.INSTRUMENTOS.ROUTER(p).Throw(); });

            ipEthernet = clients.Select(p => p.IP).FirstOrDefault();

            SamplerWithCancel((x) =>
            {
                logger.DebugFormat("Creando conexion TCP");
                Mini2.SetPortAndHost(portTCP, ipEthernet, periferic);

                logger.DebugFormat("Prueba de comunicaciones");
                Mini2.ReadFirmwareVersion();

                return true;
            }, "No se ha configurado Wifi correctamente", 5, 5000, 10000, false, false,
                (p) => { Error().HARDWARE.INSTRUMENTOS.ROUTER(p).Throw(); });


            var config = new CVMMINI2.WifiConfig { Enabled = 1, SSID = Ssid, Password = Password };

            Mini2.WriteWifiConfig(config);
            logger.DebugFormat("Habilitando Wifi");

            SamplerWithCancel((x) =>
            {
                try
                {
                    TestCommunications();

                    logger.DebugFormat("Leer configuración Wifi");
                    var wifiConfig = Mini2.ReadWifiConfig();
                    if (!wifiConfig.Equals(config))
                        return false;

                    return true;
                }
                catch
                {
                    Mini2.Dispose();
                    Delay(1000, "Hacemos Disposed del Ethernet por un error en el buffer del CMVMINI2");
                    Mini2.SetPortAndHost(502, ipEthernet, 1);
                }

                return false;

            }, "No se ha configurado Wifi correctamente", 5, 5000, 10000, false, false,
                (p) => { Error().HARDWARE.INSTRUMENTOS.ROUTER(p).Throw(); });
        }

        public override void TestCommunications()
        {
            Tower.IO.DO.On(DISCONNECT_DATAMAN);
            Tower.IO.DO.Off(BLOQUE_GRABACION);

            Tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0, 50);

            SamplerWithCancel((x) =>
            {
                try
                {
                    logger.DebugFormat("Leer ");
                    Mini2.FlagTest();

                    var firmwareVersion = Mini2.ReadFirmwareVersion();
                    Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.ToString().Trim(), Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error de versión. La versión de firmware del equipo es incorrecta"), ParamUnidad.SinUnidad);

                    return true;
                }
                catch (Exception ex)
                {
                    Mini2.Dispose();
                    Delay(1000, "Hacemos Disposed del Ethernet por un error en el buffer del CMVMINI2");
                    Mini2.SetPortAndHost(502, ipEthernet, 1);
                }

                return false;

            }, "", 5, 5000, 10000, false, false);


            var voltageAjust = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentAjust = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 4, ParamUnidad.A);
            var powerFactorAjust = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltageAjust, currentAjust, 50, powerFactorAjust, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });  // I+D: Durante todo el test, el equipo le debe circular corriente y voltaje
        }

        public void TestComunicationWifi()
        {
            try
            {
                if (!WifiOk)
                {
                    var ipWifi = "";
                    SamplerWithCancel((x) =>
                    {
                        try
                        {
                            logger.DebugFormat("Leer estado conexión Wifi");
                            var status = Mini2.ReadtWifiStatus();
                            if (string.IsNullOrEmpty(status) || status == "0")
                                return false;

                            logger.DebugFormat("Leer ip Wifi asignada reint:{0}", x);
                            ipWifi = Mini2.ReadtWifiIpAssigned();
                            if (string.IsNullOrEmpty(ipWifi) || ipWifi == "0")
                                return false;

                            return true;
                        }
                        catch (Exception ex)
                        {
                            Mini2.Dispose();
                            Delay(1000, "Hacemos Disposed del Ethernet por un error en el buffer del CMVMINI2");
                            Mini2.SetPortAndHost(502, ipEthernet, 1);
                        }

                        return false;

                    }, "No se ha encontrado la IP del Wifi conectada por DHCP", 40, 2000, 2000, false, false,
                    (p) => { Error().HARDWARE.INSTRUMENTOS.ROUTER(p).Throw(); });

                    WifiOk = true;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Excepción en la prueba de  Wifi por {0}", ex.Message);
                throw ex;
            }
            finally
            {
                if (WifiOk)
                {
                    logger.DebugFormat("DesHabilitando Wifi");
                    var configWifi = new CVMMINI2.WifiConfig { Enabled = 0, SSID = "0", Password = "0" };
                    Mini2.WriteWifiConfig(configWifi);
                }
            }
        }

        public void TestBluetooth()
        {
            var fwBluetooth = Mini2.ReadFirmwareVersionBluetooth();
            Assert.AreEqual("FW_BLUETOOTH", fwBluetooth, Identificacion.VERSION_FIRMWARE_BLUETOOTH, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, versión firmware de bluetooth incorrecta"), ParamUnidad.SinUnidad);

            Mini2.BLEFindDevice(TestInfo.NumSerie);

            if (Mini2.PairAsync())
            {
                var BLInfo = Mini2.BLEGetInfo();
                Assert.AreEqual("BLE_MANUFACTURER_NAME", BLInfo.ManufacturerName, Configuracion.GetString("BLE_MANUFACTURER_NAME", "Circutor", ParamUnidad.SinUnidad), Error().UUT.COMUNICACIONES.BLUETOOTH("Error, campo BLE Manufacturer Name incorrecto"), ParamUnidad.SinUnidad);
                Assert.AreEqual("BLE_MODEL_NUMBER", BLInfo.ModelNumber, Configuracion.GetString("BLE_MODEL_NUMBER", "ITF-WiEth", ParamUnidad.SinUnidad), Error().UUT.COMUNICACIONES.BLUETOOTH("Error, campo BLE Model Number incorrecto"), ParamUnidad.SinUnidad);
                Assert.AreEqual(BLInfo.SerialNumber, TestInfo.NumSerie, Error().UUT.COMUNICACIONES.BLUETOOTH("Error, el numero de serie en el registro BLE no coincide con el del equipo"));
                Assert.AreEqual("BLE_HW_REVISION", BLInfo.HardwareRevision, Configuracion.GetString("BLE_HARDWARE_REVISION", "1", ParamUnidad.SinUnidad), Error().UUT.COMUNICACIONES.BLUETOOTH("Error, campo BLE Hardware Revision incorrecto"), ParamUnidad.SinUnidad);
                Assert.AreEqual("BLE_FW_REVISION", BLInfo.FirmwareRevision, Configuracion.GetString("BLE_FIRMWARE_REVISION", "1.17", ParamUnidad.SinUnidad), Error().UUT.COMUNICACIONES.BLUETOOTH("Error, campo BLE Firmware Revision incorrecto"), ParamUnidad.SinUnidad);

                Mini2.BLEWritedWifiEnabled(1);
                var stateWifi = Mini2.BLEReadWifiEnabled();
                Assert.AreEqual("WIFI_ENABLED_BT", stateWifi, 1, Error().SOFTWARE.SECUENCIA_TEST.WIFI("Error. El wifi está desactivado cuando debería estar activado"), ParamUnidad.SinUnidad);

                Mini2.BLEWritedWifiEnabled(0);
                Delay(1500, "Realizando escritura de desactivación del wifi por BLE");

                stateWifi = Mini2.BLEReadWifiEnabled();
                Assert.AreEqual("WIFI_DISABLED_BT", stateWifi, 0, Error().SOFTWARE.SECUENCIA_TEST.WIFI("Error. El wifi está activado cuando debería estar desactivado"), ParamUnidad.SinUnidad);
            }
            else
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("Pair Bluetooth").Throw();

            if (!Mini2.UnpairAsync())
                TestException.Create().UUT.COMUNICACIONES.NO_COMUNICA("UnPair Bluetooth").Throw();

            Resultado.Set("BLUETOOTH", "OK", ParamUnidad.SinUnidad);
        }

        public override void TestCustomization()
        {
            Mini2.FlagTest();
            Mini2.WriteSerialNumber(TestInfo.NumSerie);
            Delay(250, "Escribiendo numero de Producción");

            modelo = Mini2.ReadModelName();
            if (modelo != "CIRCUTOR")
                Mini2.WriteBacnetConfiguration();

            var alarm1 = new CVMMINI2.AlarmConfiguration32();
            alarm1.Hi = (uint)Configuracion.GetDouble("VALOR_HIGH_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm1.Lo = (uint)Configuracion.GetDouble("VALOR_LOW_ALARMA", 0, ParamUnidad.SinUnidad);
            Mini2.WriteAlarm32bits(alarm1);
            Delay(500, "Escribiendo configuración alarma 1");

            var alarm2 = new CVMMINI2.AlarmConfiguration16();
            alarm2.code = (ushort)Configuracion.GetDouble("CODIGO_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.delay = (ushort)Configuracion.GetDouble("DELAY_ON_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.hysteresis = (ushort)Configuracion.GetDouble("HISTERESIS_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.latch = (ushort)Configuracion.GetDouble("LATCH_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.delayOff = (ushort)Configuracion.GetDouble("DELAY_OFF_ALARMA", 0, ParamUnidad.SinUnidad);
            alarm2.State = (ushort)Configuracion.GetDouble("ESTADO_CONTACTO_ALARMA", 0, ParamUnidad.SinUnidad);
            Mini2.WriteAlarm16bits(alarm2);
            Delay(500, "Escribiendo configuración alarma 2");

            TestInfo.NumMAC = Mini2.ReadMacEthernet();
            Resultado.Set("MAC_ETHERNET", TestInfo.NumMAC, ParamUnidad.SinUnidad);
            TestInfo.NumMACWifi = Mini2.ReadMacWifi();
            Resultado.Set("MAC_WIFI", TestInfo.NumMACWifi, ParamUnidad.SinUnidad);

            Mini2.CleanAllParameters(CVMMINI2.CleanParametersRegisters.CLEAN_ALL);
            Delay(10000, "Realizando borrado de registros");

            Mini2.Dispose();

            SamplerWithCancel((t) =>
            {
                var router = Router.Default();
                var config = router.ReadConfig();
                Router.Default().Disposed();

                Tower.Chroma.ApplyOffAndWaitStabilisation();
                Delay(4000, "Realizando reset de HW");

                router = Router.Default();
                router.ApplyConfig(config);
                var clients = new List<ClientEntry>();
                Tower.Chroma.ApplyAndWaitStabilisation();

                SamplerWithCancel((p) =>
                {
                    clients = router.GetConnectedClientsFromWeb(true);
                    return clients.Count > 0;
                }, "No se ha encontrado ningun cliente DHCP conectado", 10, 2000, 5000, false, false,
                (p) => { Error().HARDWARE.INSTRUMENTOS.ROUTER(p).Throw(); });

                ipEthernet = clients.Select(p => p.IP).FirstOrDefault();
                return true;

            }, "No se ha encontrado ningun cliente DHCP conectado", 2, 4000, 2000, false, false,
            (t) => { Error().HARDWARE.INSTRUMENTOS.ROUTER(t).Throw(); });
                

            SamplerWithCancel((x) =>
            {
                Mini2.SetPortAndHost(502, ipEthernet, 1);
                return true;
            }, "No se ha podido conectar con el equipo despues del Reset Hardware", 3, 500, 2000, false, false,
            (p) => { Error().UUT.COMUNICACIONES.ETHERNET(p).Throw(); });

            var modelReading = Mini2.ReadModelName();
            modelReading = string.IsNullOrEmpty(modelReading) ? "NEUTRO" : modelReading;
            Assert.AreEqual("MODELO_FINAL", modelReading, Configuracion.GetString("MODELO_EQUIPO", "NA", ParamUnidad.SinUnidad), Error().UUT.CONFIGURACION.NO_COINCIDE("Error, modelo de cliente mal grabado"), ParamUnidad.SinUnidad);

            var numSerieReaded = Mini2.ReadSerialNumber();
            Assert.AreEqual(ConstantsParameters.TestInfo.NUM_SERIE, numSerieReaded, TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El numero de serie leido no coincide con el grabado"), ParamUnidad.SinUnidad);
        }

        public override void TestFinish()
        {
            if (Mini2 != null)
            {
                try
                {
                    if (!WifiOk)
                    {
                        logger.DebugFormat("DesHabilitando Wifi");
                        var configWifi = new CVMMINI2.WifiConfig { Enabled = 0, SSID = "0", Password = "0" };
                        Mini2.WriteWifiConfig(configWifi);
                    }
                }
                catch (Exception ex)
                {
                    logger.DebugFormat("No se ha podido deshabilitar el wifi por Excepción por {0}", ex.Message);
                }

                Delay(2000, "");

                Mini2.Dispose();
            }
            
            if (Tower != null)
            {
                Tower.ShutdownSources();

                if (Tower.IO != null)
                {
                    Tower.IO.DO.Off(PISTON_LEFT_KEY, PISTON_CENTRAL_KEY, PISTON_RIGHT_KEY, OUT_DIGITAL_IN, DISCONNECT_DATAMAN);
                    Tower.IO.DO.OffWait(700, BLOQUE_GRABACION);
                    Tower.IO.DO.OffWait(300, BORNAS_J2_J3);
                    Tower.IO.DO.OffWait(500, BORNAS_J5_ETH);
                    Tower.IO.DO.OffWait(700, FIJACION_EQUIPO);
                    Tower.IO.DO.OffWait(1500, SELEC_BORNAS_ETH);
                    Tower.IO.DO.OffWait(300, AUX_EV);
                    Tower.IO.DO.OffWait(700, FIJACION_CARRO);
                }

                Tower.Dispose();
            }
        }
    }
}
