﻿using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskRunner.Model;

namespace Dezac.Tests.Model
{
    public class ParamValueResultsCollection : ParamValueCollection
    {
        public ParamValueResultsCollection(string name) : base(name)
        {
        }

        public override void Set(string key, string value, ParamUnidad unidad, string expectedValue = null)
        {
            if (SequenceContext.Current != null && SequenceContext.Current.Services.Contains<ITestContext>() && unidad == ParamUnidad.SinDefinir)
                throw new Exception($"En el resultado {key} falta informar la unidad");
            base.Set(key, value, unidad, expectedValue);
        }

        public override void Set(string key, double value, ParamUnidad unidad, double? min = null, double? max = null)
        {
            if (SequenceContext.Current != null && SequenceContext.Current.Services.Contains<ITestContext>() && unidad == ParamUnidad.SinDefinir)
                throw new Exception($"En el resultado {key} falta informar la unidad");
            base.Set(key, value, unidad, min, max);
        }

        public override void Set(string key, DateTime value, ParamUnidad unidad)
        {
            if (SequenceContext.Current != null && SequenceContext.Current.Services.Contains<ITestContext>() && unidad == ParamUnidad.SinDefinir)
                throw new Exception($"En el resultado {key} falta informar la unidad");
            base.Set(key, value, unidad);
        }
    }
}
