﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Services
{
    public static class SaveResults
    {
        public static void SaveResultsToDDBB(StepResult stepResult = null)
        {
            string logName = stepResult != null ? $"SaveResults_{stepResult.Name}" : "SaveResults";
            ILog logger = LogManager.GetLogger(logName);

            var sw = Stopwatch.StartNew();
            LogicalThreadContext.Properties["StepPath"] = "SaveResults";

            var context = SequenceContext.Current;
            var iShell = context.Services.Get<IShell>();
            var data = context.Services.Get<ITestContext>();

            logger.Debug("Guardando Resultados en la BBDD");

            if (data == null)
                throw new Exception("No se pueden guardar datos de un test sin datos de contexto");
            if (!data.TestInfo.NumBastidor.HasValue)
                throw new Exception("TEST SIN BASTIDOR");
            if (context.ExecutionResult == TestExecutionResult.Aborted)
                return;

            try
            {
                int numBastidor = (int)data.TestInfo.NumBastidor.Value;
                string resultado = stepResult == null ?
                    context.ExecutionResult == TestExecutionResult.Completed ? "O" : "E" :
                    stepResult.ExecutionResult == StepExecutionResult.Passed ? "O" : "E";

                if (!data.NumFamilia.HasValue || string.IsNullOrEmpty(data.IdFase))
                    return;

                using (DezacService svc = new DezacService())
                {
                    SaveParameters(svc, data, data.Comunicaciones, "Com", 6, 1, logger);
                    SaveParameters(svc, data, data.Consignas, "Consig", 7, 18, logger);
                    SaveParameters(svc, data, data.Configuracion, "Config", 1, 6, logger);
                    SaveParameters(svc, data, data.Identificacion, "Ident", 5, 7, logger);
                    SaveParameters(svc, data, data.Margenes, "Marg", 2, 4, logger);
                    SaveParameters(svc, data, data.Resultados, "Result", 8, 19, logger);
                    SaveParameters(svc, data, data.Parametrizacion, "Param", 16, 23, logger);
                    SaveParameters(svc, data, data.VectorHardware, "VectorHardw", 17, 25, logger);
                    SaveParameters(svc, data, data.Vision, "Vision", 18, 26, logger);
                }
                data.TestInfo.SaveBBDD = true;

                using (DezacService svc = new DezacService())
                {
                    logger.Debug($"Buscando T_TEST para el bastidor {numBastidor}");
                    T_TEST test = svc.GetTest(data.TestInfo.NumFabricacion, numBastidor);

                    if (test == null || data.modoPlayer == ModoTest.SAT)
                    {
                        logger.Debug($"Creando nuevo T_TEST para el bastidor {numBastidor}");
                        test = new T_TEST
                        {
                            NUMFABRICACION = data.TestInfo.NumFabricacion,
                            NUMMATRICULA = numBastidor,
                            NUMMATRICULATEST = numBastidor,
                            FECHA = data.TestInfo.StartTime,
                            NUMPRODUCTOTEST = data.NumProducto,
                            VERSIONPRODUCTOTEST = data.Version,
                            TIEMPO = Convert.ToInt32(data.TestInfo.Elapsed.TotalSeconds),
                            RESULTADO = resultado,
                            NUMFAMILIA = data.NumFamilia,
                            VERSIONREPORT = data.TestInfo.ReportVersion
                        };
                    }

                    if (resultado == "O")
                    {
                        test.RESULTADO = "O";
                        if (data.TestInfo.NumSerie != null)
                            test.NROSERIE = data.TestInfo.NumSerie;
                        if (data.TestInfo.NumSerieInterno != null)
                            test.NROSERIEORIGINAL = data.TestInfo.NumSerieInterno;
                        logger.Debug($"Test OK -> Rellenado NROSERIE {test.NROSERIE} y NROSERIEORIGINAL {test.NROSERIEORIGINAL}");
                        test.NUMMATRICULA = numBastidor;
                        logger.Debug($"Test OK -> Rellenado NUMMATRICULA {test.NUMMATRICULA}");
                    }
                    else
                    {
                        test.RESULTADO = "E";
                        if (data.modoPlayer != ModoTest.TIC && data.TestInfo.NumSerieAssigned && !data.MantenerNumSerie)
                        {
                            test.NROSERIE = null;
                            test.NROSERIEORIGINAL = null;
                            logger.Debug("Test KO: Borrado NROSERIE y NROSERIEORIGINAL");
                        }
                        test.NUMMATRICULA = numBastidor;
                        logger.Debug($"Test KO: Rellenado NUMMATRICULA {test.NUMMATRICULA}");
                    }

                    logger.Debug("Creando nuevo T_TESTFASE");
                    T_TESTFASE fase = new T_TESTFASE
                    {
                        T_TEST = test,
                        IDFASE = data.IdFase,
                        IDOPERARIO = data.IdOperario,
                        FECHA = data.TestInfo.StartTime,
                        TIEMPO = Convert.ToInt32(data.TestInfo.Elapsed.TotalSeconds),
                        RESULTADO = resultado,
                        NUMCAJA = data.NumCajaActual,
                        NOMBREPC = data.PC
                    };

                    var steps = stepResult == null ?
                        context.GetSteps(step => step.Step.Action == null || step.Step.Action?.Name != "NewTestFaseContext", step => step.Step.Action == null || step.Step.Action?.Name != "NewTestFaseContext") :
                        context.GetSteps(stepResult.Name, null);

                    var faultedSteps = context.GetSteps(StepExecutionResult.Failed);

                    foreach (var faultedStep in faultedSteps)
                    {
                        if (faultedStep.Exception == null)
                            continue;

                        List<Exception> innerExceptions;

                        if (faultedStep.Exception is AggregateException)
                            innerExceptions = (faultedStep.Exception as AggregateException).InnerExceptions.ToList();
                        else
                            innerExceptions = new List<Exception>() { faultedStep.Exception };

                        foreach (Exception e in innerExceptions)
                        {
                            var sauronExc = e as SauronException;
                            var errMsg = e.Message.Left(200);
                            logger.WarnFormat($"Creando T_TESTERROR, PUNTOERROR {faultedStep.Step.Name}, mensaje {errMsg}");
                            fase.T_TESTERROR.Add(new T_TESTERROR
                            {
                                PUNTOERROR = faultedStep.Step.Name.Left(50),
                                EXTENSION = errMsg,
                                NUMERROR = 160,
                                ERRORCODE = sauronExc != null ? sauronExc.CodeError : null
                            });
                        }
                    }

                    logger.Debug("Creando nuevos T_TESTVALOR para cada resultado");
                    data.Resultados.ForEach(p =>
                    {
                        if (p.Modified != null)
                            fase.T_TESTVALOR.Add(new T_TESTVALOR { NUMPARAM = p.IdParam, VALOR = p.Valor, VALMAX = p.Max.ToDecimal(), VALMIN = p.Min.ToDecimal(), EXPECTEDVALUE = p.ValorEsperado, STEPNAME = p.StepName, NUMUNIDAD = (int)p.IdUnidad });
                    });

                    logger.Debug("Creando nuevos T_TESTSTEP para cada step");
                    var stepsGroupedByName = steps
                        .GroupBy(s => s.Name)
                        .Select(group => group
                        .OrderBy(stepGroup => stepGroup.StartTime.Date)
                        .ThenBy(stepGroup => stepGroup.StartTime.TimeOfDay)
                        .Last())
                        .ToList();
                    foreach (var step in stepsGroupedByName)
                        fase.T_TESTSTEP.Add(new T_TESTSTEP
                        {
                            STEPNAME = step.Name,
                            TIMESTART = step.StartTime,
                            TIMEEND = step.EndTime,
                            RESULT = step.ExecutionResult != StepExecutionResult.Failed ? "O" : "E",
                            EXCEPTION = step.Exception == null ? null : step.GetErrorMessage().Left(500),
                            GROUPNAME = step.Step.ReportGroupName,
                            //PATH = step.Path,
                        });

                    test.T_TESTFASE.Add(fase);

                    if (data.NumAsistencia.HasValue && data.NumLineaAsistencia.HasValue)
                    {
                        var asistencia = svc.GetAsistenciaTecnica(data.NumAsistencia.Value, data.NumLineaAsistencia.Value);
                        if (asistencia != null && asistencia.NUMEROSERIE != test.NROSERIE)
                            asistencia.NUEVONROSERIE = test.NROSERIE;
                    }

                    logger.Debug("Llamada a DezacService.GrabarDatos");
                    svc.GrabarDatos(test);
                    logger.Info($"SaveResult OK => Nº TEST: {test.NUMTEST}, Nº FASE: {fase.NUMTESTFASE}, " +
                        $"Nª FABRICACIÓN: {test.NUMFABRICACION}, Nº RESULTS: {fase.T_TESTVALOR.Count}");

                    data.TestInfo.NumTest = test.NUMTEST;
                    data.TestInfo.NumTestFase = fase.NUMTESTFASE;

                    if (!svc.ExistFasesTest(test.NUMTEST) && (data.IdFase != "038" && data.IdFase != "039"))
                        iShell.MsgBox("ATENCIÓN SE ESTA FABRICANDO SOBRE UNA ORDEN SIN FASES DE TEST CREADAS, POR FAVOR COMENTASELO A TU ENCARGADO", "ATENCIÓN",
                            MessageBoxButtons.OK, MessageBoxIcon.Question);
                }

                sw.Stop();
                LogicalThreadContext.Properties["StepPath"] = "SaveResults";
                LogicalThreadContext.Properties["Result"] = StepExecutionResult.Passed;
                LogicalThreadContext.Properties["ExecutionTime"] = sw.Elapsed;
                logger.Info($"SaveResults Passed ({sw.ElapsedMilliseconds} ms)");
                LogicalThreadContext.Properties["Result"] = null;
                LogicalThreadContext.Properties["ExecutionTime"] = null;
            }
            catch (Exception ex)
            {
                sw.Stop();
                LogicalThreadContext.Properties["StepPath"] = "SaveResults";
                LogicalThreadContext.Properties["Result"] = StepExecutionResult.Failed;
                LogicalThreadContext.Properties["ExecutionTime"] = sw.Elapsed;
                logger.Info($"SaveResults Failed ({sw.ElapsedMilliseconds} ms)");
                LogicalThreadContext.Properties["Result"] = null;
                LogicalThreadContext.Properties["ExecutionTime"] = null;

                logger.Warn($"Excepción en SaveResult: {String.Join(" -> ", ex.GetAllInnerMessages())}");
                if (data != null)
                {
                    var body = new StringBuilder();
                    body.AppendLine("SaveResultsAction Failed");
                    body.AppendLine("----------------------------");
                    body.AppendLine(" ");
                    body.AppendLine($"TEST FAMILIA: {data.DescFamilia}");
                    body.AppendLine(" ");
                    body.AppendLine($"PC: {data.PC} TORRE:{data.LocationTest}");
                    body.AppendLine(" ");
                    body.AppendLine($"ORDEN:{data.NumOrden} PRODUCTO:{data.NumProducto} VERSION:{data.Version}");
                    body.AppendLine(" ");
                    body.AppendLine($"EXCEPCION: {ex.Message}");
                    body.AppendLine(" ");
                    body.AppendLine($"STACKTRACE: {ex.StackTrace}");
                    body.AppendLine(" ");
                    logger.Warn("Creando y guardando log en carpeta temporal");
                    LogService.ShowLog(FormatLogViewer.LogsTypesEnum.DebugLog, true, null, data, false, Path.Combine(Path.GetTempPath(), string.Format("Log{0}.html", context.NumInstance)));
                    logger.Warn($"Enviando correo con Body {body}");
                    SendMail.Send("Error --->  SaveResultsAction", body.ToString(), false, false, true, false, Path.Combine(Path.GetTempPath(), string.Format("Log{0}.html", context.NumInstance)));
                }
                logger.Warn("Lanzando la excepcion despues de notificar por correo el fallo en el SaveResults");
                throw ex;
            }
        }

        private static void SaveParameters(DezacService svc, ITestContext data, ParamValueCollection list, string alias, int numTipoGrupo, int numTipoParam, ILog logger)
        {
            if (list == null)
                return;

            logger.DebugFormat("Entramos en SaveParameters tipoGrupo: {0}", numTipoGrupo);

            list.ForEach(p =>
            {
                if (p.IdParam <= 0)
                {
                    p.IdParam = svc.CrearParametro(data.NumFamilia.Value, data.IdFase, numTipoGrupo, p.Name, (int)p.IdUnidad, p.GetTipoValorFromUnidad(), numTipoParam, alias, p.Valor);
                    data.ReloadParametersOnNextTest = true;
                }
            });
        }

    }
}
