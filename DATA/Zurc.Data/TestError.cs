namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TestError")]
    public partial class TestError
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TestFase")]
        public int TestFaseId { get; set; }
        public int NumError { get; set; }

        [Required]
        public string PuntoError { get; set; }
        public string Extension { get; set; }
        public string ErrorCode { get; set; }

        public virtual TestFase TestFase { get; set; }
    }
}
