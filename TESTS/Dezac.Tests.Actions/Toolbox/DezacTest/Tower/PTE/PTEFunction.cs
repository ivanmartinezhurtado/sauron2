﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions.Tower.PowerSource
{
    [ActionVersion(1.16)]
    [DesignerAction(Type = typeof(PTEFunctionDescription))]
    public class PTEFunction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Incremento")]
        public int Increment { get; set; }

        [Category("3. Configuration")]
        [Description("Intervalo")]
        public int Interval { get; set; }

        [Category("3. Configuration")]
        [Description("Nivel de inicio")]
        public int StartLevel { get; set; }

        [Category("3. Configuration")]
        [Description("Nivel de parada")]
        public int StopLevel { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de función")]
        public PTE.Function.FunctionType Function { get; set; }

        [Category("3. Configuration")]
        [Description("Magnitud")]
        public PTE.Function.Magnitud Magnitud { get; set; }

        [Category("3. Configuration")]
        [Description("Factor de multiplicación")]
        public double MultiplyingFactor { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del Test")]
        public string TestName { get; set; }

        [Category("3. Configuration")]
        [Description("Relé de entrada")]
        [DefaultValue(null)]
        public int? RelayInput { get; set; }

        [Category("3. Configuration")]
        [Description("Relé lógico")]
        [DefaultValue(RelayLogicEnum.NO)]
        public RelayLogicEnum RelayLogic { get; set; }

        private double multiplyingFactor;

        public enum RelayLogicEnum
        {
            NO,
            NC
        }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();


            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();

            if (!string.IsNullOrEmpty(TestName))
                TestName = "_" + TestName.ToUpper().Trim();

            var configFunction = new PTE.Function();
            configFunction.Increment = Increment != 0 ? Increment : testBase.Consignas.GetDouble(String.Format("PTE_INCREMENT{0}", TestName), 0.001, ParamUnidad.A);
            configFunction.Interval = Interval != 0 ? Interval : testBase.Consignas.GetDouble(String.Format("PTE_INTERVAL{0}", TestName), 1000, ParamUnidad.ms); 
            configFunction.StartLevel = StartLevel != 0 ? StartLevel : testBase.Consignas.GetDouble(String.Format("PTE_STARTLEVEL{0}", TestName), 0, ParamUnidad.A);
            configFunction.StopLevel = StopLevel != 0 ? StopLevel: testBase.Consignas.GetDouble(String.Format("PTE_STOP_LEVEL{0}", TestName), 0, ParamUnidad.A);
            multiplyingFactor = MultiplyingFactor != 0 ? MultiplyingFactor : testBase.Configuracion.GetDouble(String.Format("PTE_MULTIPLYING{0}", TestName), 1, ParamUnidad.Numero);

            var unidad = Magnitud == PTE.Function.Magnitud.VOLTAGE ? ParamUnidad.V : ParamUnidad.A;
            var min = testBase.Margenes.GetDouble(string.Format("TRIGGER_LEVEL{0}_MIN",TestName), configFunction.StartLevel, unidad);
            var max = testBase.Margenes.GetDouble(string.Format("TRIGGER_LEVEL{0}_MAX", TestName), configFunction.StopLevel, unidad);

            var def = new AdjustValueDef(string.Format("TRIGGER_LEVEL{0}", TestName), 0, min, max, 0, 0, unidad);

            tower.PTE.ConfigFunction(Magnitud, Function, configFunction);
            tower.PTE.ApplyConfigMonitor(3, PTE.Monitor.EventType.TRIP, PTE.Monitor.EventTrigger.AlActivarse,true, false, 20);

            testBase.Delay(500,"Estabilización de preset");

            var logic = RelayLogic == RelayLogicEnum.NO ? false : true;
            if ((RelayInput != null) && tower.IO.DI[(int)RelayInput] != logic)
                Error().UUT.DISPARO.YA_DISPARADO().Throw();

            try
            {
                testBase.TestMeasureBase(def, (step) =>
                {
                    var result = tower.PTE.StartFunctionAndWaitReturnLevel((int) ((((configFunction.StopLevel - configFunction.StartLevel) / configFunction.Increment) * configFunction.Interval) + 3000));

                    if (result.Item2 == PTE.FunctionResultEnum.STEPLEVEL && ((RelayInput != null) && tower.IO.DI[(int)RelayInput] == logic))
                        testBase.Error().UUT.DISPARO.NO_DISPARA().Throw();
                    if (result.Item2 == PTE.FunctionResultEnum.STEPLEVEL && ((RelayInput != null) && tower.IO.DI[(int)RelayInput] != logic))
                        testBase.Error().HARDWARE.INSTRUMENTOS.FUNCION_PTE("La PTE no ha detectado el disparo pero el equipo ha disparado").Throw();
                    if (result.Item2 == PTE.FunctionResultEnum.TRIP && ((RelayInput != null) && tower.IO.DI[(int)RelayInput] == logic))
                        testBase.Error().HARDWARE.INSTRUMENTOS.FUNCION_PTE("La PTE ha detectado el disparo pero el equipo no ha disparado").Throw();

                    return result.Item1 * multiplyingFactor;
                }, 0,1, 100);
            }
            finally
            {
                tower.PTE.ApplyOff();
            }

        }
    }

    public class PTEFunctionDescription : ActionDescription
    {
        public override string Name { get { return "FunctionPTE"; } }
        public override string Category { get { return "DezacTest/Tower/PowerSource"; } }
        public override string Description { get { return "Acción genérica para medir con la fuente PTE"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
