﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Data.ViewModels;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.06)]
    [DesignerAction(Type = typeof(FtpClientUploadFileDescription))]
    public class FtpClientUploadFile : ActionBase
    {
        [Description("Dirección IP del cliente (Desactivado si se usa el action GetLastDeviceIP)")]
        [Category("Configuracion")]
        public ActionVariable ipClient { get; set; } = new ActionVariable();

        [Description("Nombre de usuario")]
        [Category("Configuracion")]
        public ActionVariable User { get; set; } = new ActionVariable();

        [Description("Contraseña")]
        [Category("Configuracion")]
        public ActionVariable Password { get; set; } = new ActionVariable();

        [Description("")]
        [Category("Configuracion")]
        public ActionVariable UploadFile { get; set; } = new ActionVariable();

        [Description("")]
        [Category("Configuracion")]
        public ActionVariable ClientPath { get; set; } = new ActionVariable("/");

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            FTPClient ftpClient = null;

            if (ClientPath == null || ClientPath.IsKeyNullOrEmpty)
                ClientPath = "/";

            if (ipClient.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("ipClient").Throw();

            testBase.SamplerWithCancel((p) =>
            {
                ftpClient = new FTPClient(ipClient.Value.ToString(), User.Value.ToString(), Password.Value.ToString());            
                return true;
            }, string.Format("Error, no se ha podido realizar la conexión por FTP a la direccion {0}", ipClient.Value.ToString()), 5, 10000, 0, false, false);


            if (!File.Exists(UploadFile.Value.ToString()))
                throw new Exception(string.Format("No exite el fichero {0} para enviar por FTP", UploadFile.Value.ToString()));

            bool succes = ftpClient.UploadOverwrite(UploadFile.Value.ToString(), ClientPath.Value.ToString() + Path.GetFileName(UploadFile.Value.ToString()), 6);

            ftpClient.Disconnect();

            if (!succes)
                throw new Exception(string.Format("No se ha podido subir el fichero {0} por FTP", UploadFile.Value));
        }                

        public class FtpClientUploadFileDescription : ActionDescription
        {
            public override string Name { get { return "FTPClientUploadFile"; } }
            public override string Category { get { return "Tools/Ethernet/FTP"; } }
            public override string Description { get { return "Action que sirve para subir ficheros con el cliente FTP"; } }
            public override Image Icon { get { return Properties.Resources.ftp_icon; } }
        }
    }
}
