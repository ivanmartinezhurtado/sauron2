﻿using Dezac.Core.Exceptions;
using Dezac.Core.Model;
using Dezac.Data;
using Dezac.Data.ViewModels;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;

namespace Dezac.Services
{
    public interface IDezacService : IDisposable
    {
        VW_EMPLEADO GetOperarioByIdEmpleado(string empleadoId);
        ORDEN GetOrden(int idOrden);
        DIARIOMARCAJEFASE GetLastDiarioMarcaje(string operarioId);
        PRODUCTO GetProducto(int NumProducto, int version);
        BIEN GetTorreByPC(string devicePC);
        PRODUCTO GetProductoByIOrdenFab(int NumProducto, int version);
        List<T_GRUPOPARAMETRIZACION> GetGruposParametrizacion(int numProducto, int version, string idfase, int? numTipoGrupo = null);
        List<T_GRUPOPARAMETRIZACION> GetGruposParametrizacion(PRODUCTO producto, string idfase, int? numTipoGrupo = null);
        List<DeviceDataView> GetTestHistory(int numOrden, string idFase);
        PRODUCTOFASE GetProductoFase(PRODUCTO producto, string idFase);
        List<PRODUCTOFASE> GetProductoFases(PRODUCTO producto);
        List<ORDENPRODUCTO> GetOrdenProductoByOrden(int Orden, int cajaActual);
        string GetCodigoCircutor(int numProducto);
        string GetCostumerCode2(int numProducto);
        string GetCodigoBcn(int numProducto);
        TestIndicators GetTestIndicators(int orden, string idFase);
        List<TestOrdenProductoResult> GetTestResults(int orden, string idFase, bool ok);
        void SetParametroTestReportProducto(PRODUCTO producto, string idFase, int numParam, bool selFamilia, bool? selProducto);
        void AddTestReport(int numTest, byte[] pdf);
        bool GetValidateProductByIDP(int numArticulo, int version);
        bool GetValidateLineaHost(string host);
        int RetirarMatriculaOrdenProducto(int numMatricula, string username);
        List<LaserItemViewModel> GetDatosLaser(int producto, int version, string idfase);
        VW_EMPLEADO Login(string userName, string password);
        string GetUserParam(string application, string userName, string paramName);
        T_TESTFASE GetLastTestFase(int numBastidor, string idFase, int numOrden = 0, bool? OK = true, bool includeParams = false);
        int? GetLastNumTestFase(int numBastidor, int numOrden, string idFase);
        List<T_TESTFASE> GetLastTestFaseOKByOrden(int numOrden, string idFase);
        T_TESTFASE GetLastTestFaseOKByOrden(int numOrden);
        Tuple<int, byte[]> GetSequenceFileData(PRODUCTO producto, string idFase);
        Tuple<int, byte[]> GetSequenceFileData(PRODUCTO producto, string idFase, ref int tipoSecuencia, ref string nombreFichero);
        ASISTENCIATECNICA GetAsistenciaTecnica(int numAsistencia, int numLinea);
        int AddTestConfigFile(int numTCF, string userName, byte[] data, string description);
        byte[] GetTestConfigFile(PRODUCTO producto, string description);
        byte[] GetTestConfigFile(int numTCFV);
        int? NuevaOrdenAsistencia(int numeroAssistencia, int version = 0);
        PRODUCTO GetProductoAutoTest(string devicePC);
        Tuple<DateTime?, string, string> GetLastAutoTest(string devicePC);
        void AddTestFaseFile(int numTestFase, string name, byte[] data, string tipoDocId);
        bool IsSerialNumberValidInOF(string serialNumber, int orden);
        ORDENPRODUCTO GetOrdenProductoByBastidor(int bastidor);
        ORDENPRODUCTO GetOrdenProductoByFabricacion(int bastidor, int nummatricula);
        List<BIEN> GetBienByProductoFase(int numProducto, int version, string idfase);
        BIEN GetBienParamsAssitencia(int numBien);
        List<LabelItemViewModel> GetLabels(int numProducto, int version, string idFase, string tipo = null);
        byte[] GetBinario(int numArticulo, string tipoDoc);
        List<string> GetDireccionesByNumProducto(int numProducto, int version);
        List<string> GetDirecciones(int numDireccion);
        int GetIDCajaActual(int orden);
        int? GetIDCajaByBastidor(int bastidor);
        string GetLoteCaja(int numCaja);
        void SetLoteCaja(int numCaja, string lote);
        Tuple<int,int> GetCajaActual(int orden);
        int GetNumEquiposCajaActual(int cajaActual);
        string GetCurrentSerialNumber();
        string GetMinNumSerieCaja(int numCaja);
        string GetMaxNumSerieCaja(int numCaja);
        List<string> GetAllSerialNumbersBox(int numCaja);
        bool ResetLineaCajas();
        int? GenerarOrdenEnsayo(int numProducto, int version, int cantidad, string notas);
        string GetCodigoGrupo(string idFase);
        T_FAMILIA AddFamiliaToProducto(PRODUCTO producto, int familia);
        ErrorsIrisDescription GetIrisErrorsDescriptions(string errorcode, int numfamilia, int producto, string fase);
        Dictionary<string, string> GetDescripcionLenguagesCliente(int numProducto, string codCircutor);
        List<string> GetAtributosCliente(int numProducto, string codCircutor);
        int? GetVersionTestReport(int numFamilia);
        int GetLaseredPieces(int orden);
        bool GetBoxHasFinished(int numCaja, string numSerie);
        bool GetBoxHasClosed(int numCaja);
        string GetRefClientLine(int numOrden);
        string GetReferenciaOrigen(int numOrden);
        string GetEANCode(int numProducto, string codCircutor);
        string GetEANMarcaCode(int numProducto, string codCircutor);
        string GetReferenciaVenta(int numProducto);
        string GetDescripcionComercial(int numProducto);
        List<T_TESTFASEFILE> GetTestFaseFiles(int bastidor, string tipoDocId);
        List<T_TESTFASEFILE> GetTestFaseFiles(string name, string tipoDocId);
        string[] GetNewMatricula(int numMatriculas);
        List<DeviceDataView> GetAllDeviceTestOK(int numOrden, string idFase);
        List<DeviceDataView> GetDeviceBox(int numOrden);
        List<BoxesView> GetBoxes(int numOrden);
    }

    public class DezacService : IDezacService
    {
        protected static readonly ILog logger = LogManager.GetLogger("DezacService");

        private DezacContext uow = new DezacContext();

        public DezacContext Db { get { return uow; } }

        public List<T_PARAMETRO> GetParametros()
        {
            return uow.T_PARAMETRO
                .Include("T_TIPOPARAMETRO")
                .Include("T_TIPOVALORPARAMETRO")
                .Include("T_UNIDAD")
                .Where((p)=> p.NUMPARAM > 14300 & !string.IsNullOrEmpty(p.PARAM)).ToList(); //14385 Primer parametro creado con SAURON
        }

        public List<T_UNIDAD> GetUnidades()
        {
            return uow.T_UNIDAD.ToList();
        }

        public List<T_TIPOVALORPARAMETRO> GetTipoValorParametro()
        {
            return uow.T_TIPOVALORPARAMETRO.ToList();
        }

        public List<T_TIPOPARAMETRO> GetTipoParametros()
        {
            return uow.T_TIPOPARAMETRO.ToList();
        }

        public List<T_TIPOGRUPOPARAMETRIZACION> GetTipoGrupoParametrizacion()
        {
            return uow.T_TIPOGRUPOPARAMETRIZACION.ToList();
        }

        public VW_EMPLEADO GetOperarioByIdEmpleado(string empleadoId)
        {
            return uow.VW_EMPLEADO.Where(p => p.IDEMPLEADO == empleadoId).FirstOrDefault();
        }

        public DIARIOMARCAJEFASE GetLastDiarioMarcaje(string operarioId)
        {
            var dmf = uow.DIARIOMARCAJEFASE.Where(p => p.IDOPERARIO == operarioId)
                .Include(p => p.HOJAMARCAJEFASE)
                .OrderByDescending(p => p.FECHA)
                .FirstOrDefault();

            return dmf;
        }

        public ORDEN GetOrden(int idOrden)
        {
            return uow.ORDEN.Where(p => p.NUMORDEN == idOrden).FirstOrDefault(); // .Find(idOrden);
        }

        public List<T_FAMILIA> GetFamilia()
        {
            return uow.T_FAMILIA.Where(p => p.PLATAFORMA == "TS2").OrderBy(p => p.NUMFAMILIA).ToList(); 
        }

        public List<ProductsViewModel> GetProductoByFamilia(int? idfamilia)
        {
            var results = from t in uow.PRODUCTO
                          where t.NUMFAMILIA == idfamilia
                          orderby t.NUMPRODUCTO
                          select new ProductsViewModel
                          {
                              DESCRIPCION = t.ARTICULO.DESCRIPCION,
                              NUMFAMILIA = t.NUMFAMILIA.Value,
                              NUMPRODUCTO = t.NUMPRODUCTO,
                              FAMILIA = t.T_FAMILIA.FAMILIA,
                              VERSION = t.VERSION,
                              ACTIVO = t.ACTIVO,
                          };

            return results.ToList();
        }

        public List<ProductFaseViewModel> GetProductoFaseByNumProductoVersion(int numproducto, int version)
        {
            var results = from t in uow.PRODUCTOFASE
                          where t.NUMPRODUCTO == numproducto && t.VERSION == version && t.FASETEST =="S"
                          orderby t.IDFASE
                          select new ProductFaseViewModel
                          {
                              FASE = t.FASE.DESCRIPCION,
                              IDFASE = t.IDFASE,
                              TIEMPOPREPARACION = t.TIEMPOPREPARACION,
                              TIEMPOSTANDARD = t.TIEMPOSTANDARD,
                              TIEMPOTEORICO = t.TIEMPOTEORICO
                          };

            return results.ToList();
        }

        public int GetNumFabricacion(int idOrden, int numMatricula)
        {
            var numFabricacion = GetNumFabricacionInternal(idOrden, numMatricula);

            if (numFabricacion == 0)
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("No hay números de fabricación disponibles para esta OF").Throw();
                //throw new Exception("No hay números de fabricación disponibles para esta OF");

            if (numFabricacion == -1)
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("La matrícula está registrada en más de un número de fabricación").Throw();
                // throw new Exception("La matrícula está registrada en más de un número de fabricación");

            if (numFabricacion == -2)
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("La matrícula está registrada en una parte de un número de fabricación").Throw();
              //  throw new Exception("La matrícula está registrada en una parte de un número de fabricación");

            return numFabricacion;
        }

        public PRODUCTO GetProducto(int numProducto, int version)
        {
            var producto = uow.PRODUCTO
               .Include("ARTICULO")
               .Where(p => p.NUMPRODUCTO == numProducto && p.VERSION == version)
               .FirstOrDefault();

            return producto;
        }

        public PRODUCTO GetProductoByIOrdenFab(int numProducto, int version)
        {
            var producto = uow.PRODUCTO
               .Where(p => p.NUMPRODUCTO == numProducto && p.VERSION == version)
               .Include(p => p.T_FAMILIA)
               .Include(p => p.T_FAMILIA.T_GRUPOPARAMETRIZACION)
               .Include(p => p.ARTICULO)
               .FirstOrDefault();

            return producto;
        }

        public int? GetVersionTestReport(int numFamilia)
        {
            var familia = uow.T_TESTREPORTVALOR
             .Where(p => p.NUMFAMILIA == numFamilia).Max((p) => p.VERSIONREPORT);

            return familia;
        }

        public PRODUCTOFASE GetProductoFase(PRODUCTO producto, string idFase)
        {
            return uow.PRODUCTOFASE.Include("FASE").Where(p => p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == producto.VERSION && p.IDFASE == idFase).FirstOrDefault();
        }

        public List<PRODUCTOFASE> GetProductoFases(PRODUCTO producto)
        {
            return uow.PRODUCTOFASE.Include("FASE").Where(p => p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == producto.VERSION).ToList();
        }

        public List<T_GRUPOPARAMETRIZACION> GetGruposParametrizacion(int numProducto, int version, string idFase, int? numTipoGrupo = null)
        {
            var producto = GetProductoByIOrdenFab(numProducto, version);

            return GetGruposParametrizacion(producto, idFase, numTipoGrupo);
        }

        public List<T_GRUPOPARAMETRIZACION> GetGruposParametrizacion(PRODUCTO producto, string idfase, int? numTipoGrupo = null)
        {
            var grupos = producto.T_FAMILIA.T_GRUPOPARAMETRIZACION.Where(g => g.IDFASE == idfase).AsQueryable().AsNoTracking();

            if (numTipoGrupo.HasValue)
                grupos = grupos.Where(p => p.NUMTIPOGRUPO == numTipoGrupo.Value);

            foreach (var grupo in grupos)
            {
                uow.T_VALORPARAMETRO
                    .AsNoTracking()
                    .Where(p => p.NUMGRUPO == grupo.NUMGRUPO)
                    .Include(p => p.T_PARAMETRO)
                    .Include(p => p.T_PARAMETRO.T_UNIDAD)
                    .ToList()
                    .ForEach(a => grupo.T_VALORPARAMETRO.Add(a));

                uow.T_VALORPARAMETROPRODUCTO
                    .AsNoTracking()
                    .Where(p => p.NUMGRUPO == grupo.NUMGRUPO && p.NUMPRODUCTO == producto.NUMPRODUCTO && producto.VERSION == p.VERSION)
                    .ToList()
                    .ForEach(vp => grupo.T_VALORPARAMETRO.Where(p => p.NUMPARAM == vp.NUMPARAM && p.NUMGRUPO == vp.NUMGRUPO).FirstOrDefault().ValorProducto = vp.VALOR);
            }

            return grupos.ToList();
        }

        public T_TEST GetTest(int? numFabricacion, int matricula)
        {
            var test = uow.T_TEST.Where(p => p.NUMFABRICACION == numFabricacion).OrderByDescending(p => p.NUMTEST).FirstOrDefault();

            if (test != null && test.NUMMATRICULATEST == matricula)
                return test;
            
            return null;
        }

        public T_TEST GetTestByBeforeTest(int matricula)
        {
            //var test = uow.T_TEST.Where(p => p.NUMMATRICULATEST == matricula && p.RESULTADO=="O" && p.NROSERIE != null).OrderByDescending(p => p.NUMTEST).FirstOrDefault();
            var test = uow.T_TEST.Where(p => p.NUMMATRICULATEST == matricula && p.NROSERIE != null).OrderByDescending(p => p.NUMTEST).FirstOrDefault();
            if (test == null)
                return null;

            return test;
        }

        public string GetNumSerieByBeforeTest(int matricula)
        {
            var test = uow.T_TEST.Where(p => p.NUMMATRICULATEST == matricula && p.RESULTADO == "O" && p.NROSERIE != null).OrderByDescending(p => p.NUMTEST).FirstOrDefault();
            if (test == null || test.NROSERIE==null)
                return null;

            return test.NROSERIE;
        }

        public void TestProductSameCkeck(int matricula, int producto)
        {
            var test = uow.T_TEST.Where(p => p.NUMMATRICULATEST == matricula).OrderByDescending(p => p.NUMTEST).FirstOrDefault();

            if (test == null)
                return;

            //if (test.NUMPRODUCTOTEST.Value != producto)
            //    TestException.Create().PROCESO.TRAZABILIDAD.PRODUCTO_INCORRECTO("Equipo en T_TEST registrado como un producto diferente al asignado").Throw();
            //    ///throw new Exception(string.Format("Equipo {0} en T_TEST registrado como Producto: {1} y no como el que se ha asignado {2}", matricula, test.NUMPRODUCTOTEST, producto));
        }

        public bool GetValidateLineaHost(string host)
        {
            var result = uow.Database.SqlQuery<int>(string.Format("SELECT count(*) from app.LINEA where UPPER(testhost) = '{0}'", host.ToUpper())).ToList().FirstOrDefault();      
            return result > 0;
        }

        public bool GrabarDatos(T_TEST test)
        {
            logger.Debug("Entramos en la funcion de grabar Datos");
            var fase = test.T_TESTFASE.FirstOrDefault();

            if (test.NUMTEST == 0)
            {
                test.NUMTEST = uow.Database.SqlQuery<int>("SELECT app.t_testnumtest.nextval from dual").FirstOrDefault();
                logger.DebugFormat("Obtenemos NUMTEST: {0}", test.NUMTEST);
                uow.T_TEST.Add(test);
            } else
                logger.DebugFormat("NUMTEST: {0}", test.NUMTEST);


            fase.NUMTESTFASE = uow.Database.SqlQuery<int>("SELECT app.t_TestFaseNumTestFase.nextval from dual").FirstOrDefault();
            logger.DebugFormat("Obtenemos NUMTESTFASE: {0}", fase.NUMTESTFASE);
            fase.NUMTEST = test.NUMTEST;
            fase.NUMREINTENTOS = uow.T_TESTFASE
                .Where(p => p.NUMTEST == fase.NUMTEST && p.T_TEST.NUMMATRICULATEST == test.NUMMATRICULATEST && p.IDFASE == fase.IDFASE)
                .Count();

            foreach (var error in fase.T_TESTERROR)
            {
                error.NROERROR = uow.Database.SqlQuery<int>("SELECT app.t_testerrornroerror.nextval from dual").FirstOrDefault();
                logger.DebugFormat("Obtenemos NROERROR: {0}", error.NROERROR);
                if (!string.IsNullOrEmpty(error.ERRORCODE))
                {
                    var irisError = uow.T_IRISERROR.Find(error.ERRORCODE);
                    if (irisError == null)
                        uow.T_IRISERROR.Add(new T_IRISERROR { ERRORCODE = error.ERRORCODE, DESCRIPCION = error.EXTENSION });

                    //var irisErrorFam = uow.T_IRISERRORFAMILIAFASE.Where(p => p.NUMFAMILIA == test.NUMFAMILIA.Value && p.IDFASE == fase.IDFASE).FirstOrDefault();
                    //if (irisErrorFam == null)
                    //    uow.T_IRISERRORFAMILIAFASE.Add(new T_IRISERRORFAMILIAFASE { ERRORCODE = error.ERRORCODE, NUMFAMILIA = test.NUMFAMILIA.Value, IDFASE = fase.IDFASE });

                    //var irisErrorArt = uow.T_IRISERRORPRODUCTOFASE.Where(p => p.IDFASE == fase.IDFASE && p.NUMARTICULO == test.NUMPRODUCTOTEST).FirstOrDefault();
                    //if (irisErrorArt == null)
                    //    uow.T_IRISERRORPRODUCTOFASE.Add(new T_IRISERRORPRODUCTOFASE { ERRORCODE = error.ERRORCODE, IDFASE = fase.IDFASE, NUMARTICULO = test.NUMPRODUCTOTEST.Value });

                    logger.DebugFormat("Añadimos Codificacion T_IRISERROR: {0}", error.ERRORCODE);
                }
            }

            logger.DebugFormat("Ejecutamos metodo SaveChanges");
            uow.SaveChanges();

            logger.DebugFormat("SaveChanges OK");
            return true;
        }

        public bool GrabarDatos(ORDENPROCESOFASE process)
        {
            logger.Debug("Entramos en la funcion de grabar ORDENPROCESOFASE");
            uow.ORDENPROCESOFASE.Add(process);

            uow.SaveChanges();
            logger.Debug("Grabacion ORDENPROCESOFASE OK");
            return true;
        }

        public string GetCodigoGrupo(string idFase)
        {
            return uow.FASE.Where(p => p.IDFASE == idFase).Select(p => p.CODIGOGRUPO).FirstOrDefault();
        }

        public MATRICULA GetMatricula(int idMatricula)
        {
            return uow.MATRICULA.Find(idMatricula);
        }

        public bool ExistFasesTest(int numTest)
        {
            return (from ft in uow.T_TESTFASES
                    where ft.NUMTEST == numTest
                    select ft).ToList().Any();
        }

        public string GetNumSerieReservaByBox(int numProducto, int version, int numOrden, int equipsByCaja)
        {
            string result = null;

            using (var cmd = uow.CreateCommand("app.cj_NextSerialCode"))
            {
                var pSerie = cmd.AddParameter("NSERIE", DbType.String, null, ParameterDirection.ReturnValue, 40);

                cmd.AddParameter("P_PRODUCTO", DbType.Int32, numProducto);
                cmd.AddParameter("P_VERSION", DbType.Int32, version);
                cmd.AddParameter("P_ORDEN", DbType.Int32, numOrden);
                cmd.AddParameter("P_UDSCAJA", DbType.Int32, equipsByCaja);

                cmd.ExecuteCommand();

                if (pSerie.Value == null)
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("Error no se ha obtenido numero de serie del la BBDD de Oracle").Throw();

                logger.DebugFormat("DezacService -> GetNumSerieReservaByBox ->  Numro Serie Original {0}", pSerie.Value);

                result = pSerie.Value.ToString();
            }                                  

            return result;
        }

        public string SetReservaBoxByNumSerie(int numProducto, int version, int numOrden, int equipsByCaja, string numserie)
        {
            string result = null;

            using (var cmd = uow.CreateCommand("app.cj_NextSerialCode"))
            {
                var pSerie = cmd.AddParameter("NSERIE", DbType.String, null, ParameterDirection.ReturnValue, 40);

                cmd.AddParameter("P_PRODUCTO", DbType.Int32, numProducto);
                cmd.AddParameter("P_VERSION", DbType.Int32, version);
                cmd.AddParameter("P_ORDEN", DbType.Int32, numOrden);
                cmd.AddParameter("P_UDSCAJA", DbType.Int32, equipsByCaja);
                cmd.AddParameter("P_NROSERIE", DbType.String, numserie);

                cmd.ExecuteCommand();

                if (pSerie.Value == null)
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("Error no se ha obtenido numero de serie del la BBDD de Oracle").Throw();

                logger.DebugFormat("DezacService -> GetNumSerieReservaByBox ->  Numro Serie Original {0}", pSerie.Value);

                result = pSerie.Value.ToString();
            }

            return result;
        }

        public string GetNumSerie(int numProducto, int version, int numOrden, string familia ="")
        {
            string result = null;

            using (var cmd = uow.CreateCommand("app.NextSerialCode"))
            {
                var pSerie = cmd.AddParameter("NSERIE", DbType.String, null, ParameterDirection.ReturnValue, 40);

                cmd.AddParameter("P_PRODUCTO", DbType.Int32, numProducto);
                cmd.AddParameter("P_VERSION", DbType.Int32, version);
                cmd.AddParameter("P_ORDEN", DbType.Int32, numOrden);

                if (!string.IsNullOrEmpty(familia))
                    cmd.AddParameter("P_FAMILIA", DbType.String, familia);

                cmd.ExecuteCommand();

                if (pSerie.Value == null)
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("Error no se ha obtenido numero de serie del la BBDD de Oracle").Throw();

                logger.DebugFormat("DezacService -> GetNumSerie ->  Numro Serie Original {0}", pSerie.Value);

                result = pSerie.Value.ToString();
            }

            return result;
        }

        public int GetNumSP3()
        {
            string sql = string.Format("SELECT app.NUMSP3.nextval FROM dual");

            var number = uow.Database.SqlQuery<int>(sql).FirstOrDefault();

            return number;
        }

        public int GetNumBilogy()
        {
            string sql = string.Format("SELECT app.NUMBILOGY.nextval FROM dual");

            var number = uow.Database.SqlQuery<int>(sql).FirstOrDefault();

            return number;
        }

        public string SearchNumSerieTestFaseBefore(int nummatricula, int numproducto, int version)
        {
            var ordenCaja = uow.ORDENPRODUCTO
                            .Include("ORDEN")
                            .Where(p => p.NUMMATRICULA.Value == nummatricula && p.NROSERIE != null);

            var ordenSearch = ordenCaja.ToList().FirstOrDefault();
            if (ordenSearch == null)
                return "";

            if (numproducto == ordenSearch.ORDEN.NUMPRODUCTO)
                return ordenSearch.NROSERIE;
            
            if (ExistComponentsEstructureInProduct(numproducto, version, ordenSearch.ORDEN.NUMPRODUCTO))
                if (ordenSearch != null)
                    return ordenSearch.NROSERIE;

            return "";
        }

        public int GetBacNetID(string counterName = "BACNET")
        {
            return uow.Database.SqlQuery<int>(string.Format("select app.{0}.nextval from dual", counterName)).FirstOrDefault();
        }

        private int GetNumFabricacionInternal(int numOrden, int numMatricula)
        {
            int result = -1;

            using (var cmd = uow.CreateCommand("app.NextFab_vb"))
            {
                cmd.AddParameter("P_NUMORDEN", DbType.Int32, numOrden);
                cmd.AddParameter("P_NUMMATRICULA", DbType.Int32, numMatricula);
                var pValue = cmd.AddParameter("P_NUMFABRICACION", DbType.Int32, 0, ParameterDirection.Output);

                cmd.ExecuteCommand();

                if (pValue.Value != null)
                    int.TryParse(pValue.Value.ToString(), out result);
            }

            return result;
        }

        public int RetirarMatriculaOrdenProducto(int numMatricula, string username)
        {
            int result = -1;

            using (var cmd = uow.CreateCommand("app.RetirarMat_vb"))
            {
                cmd.AddParameter("p_numMatricula", DbType.Decimal, numMatricula);
                cmd.AddParameter("p_userName", DbType.String, username);
                var pValue = cmd.AddParameter("resultado", DbType.Decimal, 0, ParameterDirection.Output);

                cmd.ExecuteCommand();

                if (pValue.Value != null)
                    int.TryParse(pValue.Value.ToString(), out result);
            }

            return result;
        }

        public List<ORDENPRODUCTO> GetOrdenProductoByOrden(int Orden, int cajaActual)
        {
            var ordenCaja = uow.ORDENPRODUCTO
               .Where(p => p.NUMORDEN == Orden && p.NUMCAJA == cajaActual);

            return ordenCaja.ToList();
        }

        public int CrearParametro(int idFamilia, string idFase, int numTipoGrupo, string name, int unidad, int tipoValor, int numTipoParam, string alias)
        {
            var p = uow.T_PARAMETRO.Where(g => g.PARAM == name).FirstOrDefault();
            if (p == null)
            {
                p = new T_PARAMETRO();
                p.NUMPARAM = uow.Database.SqlQuery<int>("select app.t_ParametroNumParam.nextval from dual").FirstOrDefault();
                p.PARAM = name;
                p.NUMTIPOPARAM = numTipoParam;
                p.NUMUNIDAD = unidad;
                p.NUMTIPOVALOR = tipoValor;
                p.NIVELSEGURIDAD = 1;
                p.METROLOGIASINO = "N";

                uow.T_PARAMETRO.Add(p);
            }

            var grupo = uow.T_GRUPOPARAMETRIZACION.Where(g => g.NUMFAMILIA == idFamilia && g.IDFASE == idFase && g.NUMTIPOGRUPO == numTipoGrupo).FirstOrDefault();
            if (grupo == null)
            {
                grupo = new T_GRUPOPARAMETRIZACION { NUMFAMILIA = idFamilia, IDFASE = idFase, NUMTIPOGRUPO = numTipoGrupo, ALIAS = alias, INOUT = numTipoGrupo == 8 ? "O" : "I" };
                grupo.NUMGRUPO = uow.Database.SqlQuery<int>("select app.T_GRUPOPARAMNUMGRUPO.nextval from dual").FirstOrDefault();
                uow.T_GRUPOPARAMETRIZACION.Add(grupo);
            }

            uow.SaveChanges();

            return p.NUMPARAM;
        }

        public int CrearParametro(int idFamilia, string idFase, int numTipoGrupo, string name, int unidad, int tipoValor, int numTipoParam, string alias, string value)
        {
            var p = uow.T_PARAMETRO.Where(g => g.PARAM == name).FirstOrDefault();
            if (p == null)
            {
                p = new T_PARAMETRO();
                p.NUMPARAM = uow.Database.SqlQuery<int>("select app.t_ParametroNumParam.nextval from dual").FirstOrDefault();
                p.PARAM = name;
                p.NUMTIPOPARAM = numTipoParam;
                p.NUMUNIDAD = unidad;
                p.NUMTIPOVALOR = tipoValor;
                p.NIVELSEGURIDAD = 1;
                p.METROLOGIASINO = "N";

                uow.T_PARAMETRO.Add(p);
            }

            var grupo = uow.T_GRUPOPARAMETRIZACION.Where(g => g.NUMFAMILIA == idFamilia && g.IDFASE == idFase && g.NUMTIPOGRUPO == numTipoGrupo).FirstOrDefault();
            if (grupo == null)
            {
                grupo = new T_GRUPOPARAMETRIZACION { NUMFAMILIA = idFamilia, IDFASE = idFase, NUMTIPOGRUPO = numTipoGrupo, ALIAS = alias, INOUT = numTipoGrupo == 8 ? "O" : "I" };
                grupo.NUMGRUPO = uow.Database.SqlQuery<int>("select app.T_GRUPOPARAMNUMGRUPO.nextval from dual").FirstOrDefault();
                uow.T_GRUPOPARAMETRIZACION.Add(grupo);
            }

            var valor = new T_VALORPARAMETRO();
            valor.NUMGRUPO = grupo.NUMGRUPO;
            valor.NUMPARAM = p.NUMPARAM;
            valor.VALOR = value;
            valor.VALORINICIO = value;

            grupo.T_VALORPARAMETRO.Add(valor);

            uow.SaveChanges();

            return p.NUMPARAM;
        }

        public ORDENPRODUCTO GetOrdenProductoByFabricacion(int numFabricacion, int bastidor)
        {
            var of = (from f in uow.ORDENPRODUCTO
                      where f.NUMMATRICULA == bastidor & f.NUMFABRICACION == numFabricacion
                      select f);

            var fechaMax = of.Max((j) => j.FECHAMAT);
            return of.Where((p) => p.FECHAMAT == fechaMax).FirstOrDefault();
        }

        public ORDENPRODUCTO GetOrdenProductoByBastidor(int bastidor)
        {
            var of = (from f in uow.ORDENPRODUCTO
                      where f.NUMMATRICULA == bastidor
                      select f);

            var fechaMax = of.Max((j) => j.FECHAMAT);
            return of.Where((p) => p.FECHAMAT == fechaMax).FirstOrDefault();
        }

        public ORDENPRODUCTO GetOrdenProductoByBastidorAndProducto(int bastidor, int numProducto)
        {
            var of = (from f in uow.ORDENPRODUCTO
                      join o in uow.ORDEN
                      on f.NUMORDEN equals o.NUMORDEN
                      where f.NUMMATRICULA == bastidor && o.NUMPRODUCTO == numProducto
                      select f);

            var fechaMax = of.Max((j) => j.FECHAMAT);
            return of.Where((p) => p.FECHAMAT == fechaMax).FirstOrDefault();
        }

        public TestRepeatStatus GetStatusRepeat(int bastidor, string fase)
        {
            var resultadotest = uow.Database.SqlQuery<int>(string.Format("select app.StatusRepeticion ({0},'{1}') from dual", bastidor, fase)).ToList().FirstOrDefault();

            //1 Matricula; sin fabricaciones
            //2 Matricula/fabricación; sin ningún test
            //3 Matricula/fabricación; sin ningun test fase
            //10 Matricula/fabricación/test; ultimo testfase 'O'; dos o más tests de la fase
            //13 Matricula/fabricación/test; ultimo testfase 'O'; un test de la fase

            //6 Matrícula/fabricación/test; ultimo testfase 'E'; dos o más tests de la fase; numero de testfase par; último test de la fase sin ningún reproceso cerrado.
            //7 Matricula/fabricación/test; ultimo testfase 'E'; dos o más tests de la fase; numero de testfase par; último test de la fase algún reproceso cerrado.
            //8 Matrícula/fabricación/test; ultimo testfase 'E'; dos o más tests de la fase; numero de testfase impar; último test de la fase sin ningún reproceso cerrado.
            //9 Matricula/fabricación/test; ultimo testfase 'E'; dos o más tests de la fase; numero de testfase impar; último test de la fase algún reproceso cerrado.

            //12 Matricula/fabricación/test; ultimo testfase 'E'; un test de la fase;  último test de la fase algún reproceso cerrado.

            switch (resultadotest)
            {
                case 1:
                case 2:
                case 3:
                case 7:
                case 9:
                case 13:
                case 10:
                    return TestRepeatStatus.PASS;
                case 12:
                    return TestRepeatStatus.REPEAT_1;
                case 8:
                    return TestRepeatStatus.REPEAT_2;
                case 11:
                    return TestRepeatStatus.NOT_PASS_1;
                case 6:
                default:
                    return TestRepeatStatus.NOT_PASS_2;
            }
        }

        public bool ExisteMatriculaSubconjunto(int orden, int bastidor)
        {
            try
            {
                if (orden == 0) return false;

                var resultadotest = uow.Database.SqlQuery<int>(string.Format("select app.TestMatriculaOrden ({0},{1}) from dual", orden, bastidor)).ToList().FirstOrDefault();

                //null no existe la orden
                //0 La matricula no corresponde a un subconjunto de un producto de una orden
                //1 La matricula corresponde a un subconjunto de un producto de la orden

                return resultadotest == 1 ? true : false;

            }
            catch (Exception ex)
            {
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(string.Format("Error.Comprobar si existe matricula Hija por {0}", ex.Message)).Throw();
                return false;
               // throw new Exception(string.Format("Error. Comprobar si existe matricula Hija por {0}", ex.Message));
            }
        }

        public void GetResultadoNumTestFasesIsOK(int bastidor, int? numOrden, string fase, bool isReprocess)
        {
            var resultadotest = uow.Database.SqlQuery<string>(string.Format("select app.ValidaFaseMatricula({0},{1},'{2}') from dual", numOrden.Value, bastidor, fase)).ToList().FirstOrDefault();

            switch (resultadotest)
            {
                case "0":
                    return;
                case "1":
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("numero de orden incorrecta").Throw();
                    break;
                case "2":
                    if (isReprocess) break;
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("la fase especificada no es una fase de test del producto.").Throw();
                    break;
                case "3":
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("la fase ha de ser la primera y no lo es").Throw();
                    break;
                case "4":
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("la fase no es una de las fases del test").Throw();
                    break;
                case "?":
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI("Error. Error desconocido devuelto por validafasetest").Throw();
                    break;
                case "5":
                    //throw new Exception("Error. Matricula con fabricación/tests; el test tiene completas correctamente todas sus fases: no tiene sentido una siguiente fase");
                case "6":
                    //throw new Exception("Error. Matricula con fabricación/tests; el test tiene una fase anterior, no esta pasada como OK");
                case "7":
                    // throw new Exception("Error. Matricula con fabricación/tests; el test tiene una fase siguiente, pero no es la indicada");
                    break;
            }

            GetOrderByNumTestFaseSecuencia(bastidor, fase);
        }

        public void GetOrderByNumTestFaseSecuencia(int bastidor, string fase)
        {
            var sql = @"SELECT t.SECUENCIA, t.NUMTEST, t.IDFASE, tf.NUMTEST AS MAXTF, tf.FECHA as FECHAMAXTESTFASE, tf.RESULTADO as RESULTADOMAXTESTFASE, tf2.FECHA AS FECHAMAXTESTCONORDEN, tf2.RESULTADO as RESULTADOMAXTESTCONORDEN,
                CASE WHEN tf.RESULTADO = 'O' THEN tf2.RESULTADO ELSE 'E' END AS RESULTADO
                FROM
                (
                    SELECT tfs.*,
                        (SELECT MAX(NUMTESTFASE) 
                        FROM app.T_TESTFASE tf
                        INNER JOIN app.T_TEST t ON t.NUMTEST = tf.NUMTEST
                        WHERE nummatriculatest = {0} AND tf.IDFASE = tfs.IDFASE
                        ) as NUMTESTFASE,
                        (SELECT MAX(NUMTESTFASE) 
                        FROM app.T_TESTFASE tf
                        INNER JOIN app.T_TEST t ON t.NUMTEST = tf.NUMTEST
                        WHERE nummatriculatest = {0} AND tf.IDFASE = tfs.IDFASE  and NUMFABRICACION IS NOT NULL
                        ) as NUMTESTFASEORDEN        
                    FROM app.t_testfases tfs
                    WHERE tfs.numtest = (SELECT max(numtest) FROM app.T_TEST where nummatriculatest = {0} and NUMFABRICACION IS NOT NULL)
                ) T
                LEFT JOIN app.T_TESTFASE tf ON tf.NUMTESTFASE = T.NUMTESTFASE
                LEFT JOIN app.T_TESTFASE tf2 ON tf2.NUMTESTFASE = T.NUMTESTFASEORDEN
                ORDER BY SECUENCIA";

            var list = uow.Database.SqlQuery<ResultadoFaseBySecuencias>(string.Format(sql, bastidor)).ToList();

            list.ForEach(p => 
            {
                // Si existe una secuencia anterior con una fecha superior inválidamos el test
                // Permite evitar ciclos del tipo: DIELECTRICO OK - AJUSTAR OK - VERIFICAR OK - DIELETRICO OK
                // En este caso se invalida AJUSTAR y VERIFICAR
                if (list.Any(v => v.SECUENCIA < p.SECUENCIA && v.FECHAMAXTESTFASE > p.FECHAMAXTESTFASE))
                    p.RESULTADO = "E";
            });

            foreach (var res in list)
            {
                if (fase == res.IDFASE)
                    return;

                if (res.RESULTADO != "O")
                {
                    var sFase = string.Format("SECUENCIA:{0} FASE:{1} RESULTADO:{2}", res.SECUENCIA, res.IDFASE, res.RESULTADO);
                    TestException.Create().PROCESO.TRAZABILIDAD.FASES_TEST_INCORRECTAS(sFase).Throw();
                }
            }
        }

        public void GetResultadoNumTestIsOK(int bastidor)
        {
            var of = (from f in uow.ORDENPRODUCTO
                      join tt in uow.T_TEST on f.NUMFABRICACION equals tt.NUMFABRICACION
                      where tt.NUMMATRICULA == bastidor
                      select f);
            var fechaMax = of.Max((j) => j.FECHAMAT);

            if(!fechaMax.HasValue)
                TestException.Create().PROCESO.TRAZABILIDAD.FASES_TEST_INCORRECTAS("No se ha encontrado ningún test para este producto").Throw();

            var numfabricacion = of.Where((p) => p.FECHAMAT == fechaMax).FirstOrDefault().NUMFABRICACION;

            var numtest = (from tt in uow.T_TEST
                           join op in uow.ORDENPRODUCTO on tt.NUMFABRICACION equals op.NUMFABRICACION
                           where op.NUMFABRICACION == numfabricacion
                           select tt.NUMTEST).Max();

            if (numtest == 0)
                return;

            var resultadotest = uow.Database.SqlQuery<string>(string.Format("select app.ResultadoTest({0}) from dual", numtest)).ToList().FirstOrDefault();

            if (resultadotest != "O")
                TestException.Create().PROCESO.TRAZABILIDAD.FASES_TEST_INCORRECTAS("Error. bastidor placa no ha superado el test de placa").Throw();
            //throw new Exception("Error. bastidor placa no ha superado el test de placa");
        }

        public void GetResultadoNumTestFaseIsOK(int bastidor, string idFase)
        {
            var numtest = (from tt in uow.T_TEST
                           join op in uow.ORDENPRODUCTO on tt.NUMFABRICACION equals op.NUMFABRICACION
                           join tf in uow.T_TESTFASE on tt.NUMTEST equals tf.NUMTEST
                           where op.NUMMATRICULA == bastidor && tf.IDFASE == idFase
                           select tt.NUMTEST).Max();

            if (numtest == 0)
                return;

            var resultadotest = uow.Database.SqlQuery<string>(string.Format("select app.ResultadoTest({0}) from dual", numtest)).ToList().FirstOrDefault();

            if (resultadotest != "O")
                TestException.Create().PROCESO.TRAZABILIDAD.FASES_TEST_INCORRECTAS("Error. bastidor placa no ha superado el test de placa").Throw();
        }

        public int GetNumEstructura(int bastidor, string operario)
        {
            var Estructura = uow.ESTRUCTURAMATRICULA.Where(g => g.NUMMATRICULA == bastidor).FirstOrDefault();
            int numEstructura = 0;

            if (Estructura == null)
            {
                numEstructura = uow.Database.SqlQuery<int>("select app.EstructMatriculaNumEstruct.nextval from dual").ToList().FirstOrDefault();
                Estructura = new ESTRUCTURAMATRICULA { NUMESTRUCTURA = numEstructura, NUMMATRICULA = bastidor, IDOPERARIO = operario };
                uow.ESTRUCTURAMATRICULA.Add(Estructura);
                uow.SaveChanges();
            }
            else
                numEstructura = Estructura.NUMESTRUCTURA;

            return numEstructura;
        }

        public void GrabarEstructuraMatricula(int numEstructura, int matriculaHija)
        {
            var numBastidor = uow.Database.SqlQuery<int>(string.Format("select count(*) from APP.MatriculaMatricula where NumEstructura = {0} and  NumMatricula={1}", numEstructura, matriculaHija)).ToList().FirstOrDefault();

            if (numBastidor == 0)
                uow.Database.ExecuteSqlCommand(string.Format("INSERT INTO APP.MatriculaMatricula (NumEstructura, NumMatricula) VALUES ({0},{1})", numEstructura, matriculaHija));
        }

        public TestIndicators GetTestIndicators(int orden, string idFase)
        {
            var indicators = new TestIndicators();
            var faseDescription = uow.Database.SqlQuery<string>(string.Format(@"SELECT descripcion FROM app.fase WHERE idfase LIKE '{0}'", idFase)).FirstOrDefault();

            if (faseDescription.Contains("LASEAR"))
            {
                var kpi = uow.Database.SqlQuery<ORDENPROCESOFASE>(string.Format(@"SELECT * FROM app.ordenprocesofase WHERE numorden = {0} AND idfase = {1}", orden, idFase)).ToList();
                if (kpi.Count > 0)
                {
                    indicators.NumOK = kpi.Where((p) => p.RESULTADO == "O").Count();
                    indicators.NumError = kpi.Where((p) => p.RESULTADO == "E").Count();
                    indicators.NumTiempoMedio = kpi.Average((p) => p.TIEMPO);
                }
            }
            else
            {
                var kpi = uow.Database.SqlQuery<IndicatorsDescriptions>(string.Format(@"select i.Descripcion, i.Valor from table(app.indtestOrden({0}, '{1}')) i order by i.NumInd", orden, idFase)).ToList();

                if (kpi.Count > 0)
                {
                    indicators.NumOK = kpi.Where((p) => p.Descripcion == "Fabricaciones Ok").FirstOrDefault().Valor;
                    indicators.NumError = kpi.Where((p) => p.Descripcion == "Fabricaciones Ko").FirstOrDefault().Valor;
                    indicators.NumFPY = kpi.Where((p) => p.Descripcion == "Porcentaje de fabricaciones Ok a la primera sobre fabricaciones Ok/Ko").FirstOrDefault().Valor;
                    indicators.NumTiempoMedio = kpi.Where((p) => p.Descripcion == "Tiempo medio de test/fase Ok").FirstOrDefault().Valor;
                }
            }

            return indicators;
        }

        public List<TestOrdenProductoResult> GetTestResults(int orden, string idFase, bool ok)
        {
            if (ok)
                return (from tf in uow.T_TESTFASE
                        join t in uow.T_TEST on tf.NUMTEST equals t.NUMTEST
                        join f in uow.ORDENPRODUCTO on t.NUMFABRICACION equals f.NUMFABRICACION
                        where f.NUMORDEN == orden && t.NUMMATRICULA == f.NUMMATRICULA && tf.RESULTADO == "O" && tf.NUMTESTFASE == (uow.T_TESTFASE.Where(p => p.IDFASE == idFase && p.NUMTEST == t.NUMTEST).Max(p => p.NUMTESTFASE))
                        select f)
                                .Distinct()
                                .Select(s => new TestOrdenProductoResult { Fecha = s.FECHAMAT, IdBastidor = s.NUMMATRICULA, NumSerie = s.NROSERIE, NumCaja = s.NUMCAJA })
                                .ToList();

            return (from tf in uow.T_TESTFASE
                    join t in uow.T_TEST on tf.NUMTEST equals t.NUMTEST
                    join f in uow.ORDENPRODUCTO on t.NUMFABRICACION equals f.NUMFABRICACION
                    where f.NUMORDEN == orden && t.NUMMATRICULA == f.NUMMATRICULA & tf.RESULTADO != "O" && tf.NUMTESTFASE == (uow.T_TESTFASE.Where(p => p.IDFASE == idFase && p.NUMTEST == t.NUMTEST).Max(p => p.NUMTESTFASE))
                    select new { t.NUMMATRICULATEST, tf.FECHA, error = tf.T_TESTERROR, f.NUMCAJA })
                    .ToList()
                    .Distinct()
                    .Select(s => new TestOrdenProductoResult
                    {
                        Fecha = s.FECHA,
                        IdBastidor = s.NUMMATRICULATEST,
                        PuntoError = s.error.Count > 0 ? s.error.First().PUNTOERROR : null,
                        MsgError = s.error.Count > 0 ? s.error.First().EXTENSION : null,
                        NumCaja = s.NUMCAJA
                    })
                    .ToList();
        }

        public List<ComponentsEstructureInProduct> GetComponentsEstructureInProductByTrazabilidad(int numProducto, int version)
        {
            var results = (from pc in uow.PRODUCTOCOMPONENTE
                           from p in uow.PRODUCTO
                           where p.NUMPRODUCTO == pc.NUMCOMPONENTE && p.VERSION == pc.VERSIONCOMPONENTE && pc.NUMPRODUCTO == numProducto && pc.VERSIONPRODUCTO == version && p.TRAZABILIDAD == "S"
                           select new { p.NUMPRODUCTO, p.VERSION, p.ARTICULO.DESCRIPCION, pc.CANTIDAD })
                          .Select(s => new ComponentsEstructureInProduct
                          {
                              descripcion = s.DESCRIPCION,
                              numproducto = s.NUMPRODUCTO,
                              version = s.VERSION,
                              cantidad = (int)s.CANTIDAD
                          }).ToList();

            return results;
        }

        public List<ComponentsEstructureInProduct> GetComponentsEstructureInProduct(int numProducto, int version, bool trazabilidad, bool trazabilidadPrimerNivel)
        {
            string sql = string.Format(@"
                SELECT AC.DESCRIPCION, PC.NUMCOMPONENTE as NUMPRODUCTO, PC.VERSIONCOMPONENTE as VERSION, LEVEL AS NIVEL,
                    LTRIM(SYS_CONNECT_BY_PATH (PC.NUMCOMPONENTE,','),',') as PATH, P2.TRAZABILIDAD
                    ,(SELECT COUNT(*) FROM APP.PRODUCTOCOMPONENTE PCS WHERE PCS.NUMPRODUCTO = PC.NUMCOMPONENTE AND PCS.VERSIONPRODUCTO = PC.VERSIONCOMPONENTE) AS CHILDS
                FROM APP.PRODUCTOCOMPONENTE PC
                INNER JOIN APP.PRODUCTO P2 ON P2.NUMPRODUCTO = PC.NUMCOMPONENTE AND P2.VERSION = PC.VERSIONCOMPONENTE
                INNER JOIN APP.ARTICULO AC ON AC.NUMARTICULO = PC.NUMCOMPONENTE
                START WITH PC.NUMPRODUCTO = {0} AND PC.VERSIONPRODUCTO = {1}
                CONNECT BY PC.NUMPRODUCTO = PRIOR PC.NUMCOMPONENTE AND PC.VERSIONPRODUCTO = PRIOR PC.VERSIONCOMPONENTE
                ORDER SIBLINGS BY  PC.NUMCOMPONENTE", numProducto, version);

            var list = uow.Database.SqlQuery<ComponentsEstructureInProduct>(sql).OrderBy(p => p.path).ToList();
            if (!list.Any())
                TestException.Create().PROCESO.MATERIAL.ESTRUCTURA_COMPONENTES("no existen componentes en la estructura del producto version").Throw();

            if (trazabilidad)
            {
                list = list.Where(p => p.trazabilidad == "S" && p.childs > 0).ToList();

                if (trazabilidadPrimerNivel)
                {
                    var result = new List<ComponentsEstructureInProduct>();

                    foreach(var item in list)
                        if (!result.Any(p => item.path.StartsWith(p.path)))
                            result.Add(item);

                    return result;
                }
            }

            return list;
        }

        public int GetNumRegistroOrdenProcesoFase()
        {
            string sql = string.Format("SELECT app.OrdenProcesoFaseNumRegistro.nextval FROM dual");

            var id = uow.Database.SqlQuery<int>(sql).FirstOrDefault();

            return id;
        }

        public bool ExistComponentsEstructureInProduct(int numProducto, int version, int numComponent)
        {
            string sql = string.Format(@"
                     SELECT COUNT(*)
                    FROM
                    (
                        SELECT PC.*
                        FROM APP.PRODUCTOCOMPONENTE PC
                        START WITH PC.NUMPRODUCTO = {0} AND PC.VERSIONPRODUCTO = {1}
                        CONNECT BY PC.NUMPRODUCTO = PRIOR PC.NUMCOMPONENTE AND PC.VERSIONPRODUCTO = PRIOR PC.VERSIONCOMPONENTE
                        ORDER SIBLINGS BY  PC.NUMCOMPONENTE
                    ) T
                    WHERE T.NUMCOMPONENTE = {2}", numProducto, version, numComponent);

            var exist = uow.Database.SqlQuery<int>(sql).ToList();
            if (exist == null)
                TestException.Create().PROCESO.MATERIAL.ESTRUCTURA_COMPONENTES("no existe el componente en la estructura del producto version").Throw();

            if (exist.FirstOrDefault() == 0)
                TestException.Create().PROCESO.MATERIAL.ESTRUCTURA_COMPONENTES("no existe el componente en la estructura del producto version").Throw();

            return true;
        }

        public void SetParametroTestReportProducto(PRODUCTO producto, string idFase, int numParam, bool selFamilia, bool? selProducto)
        {
            var testGroup = uow.T_GRUPOPARAMETRIZACION.Include("T_VALORPARAMETRO").Where(g => g.NUMFAMILIA == producto.NUMFAMILIA && g.IDFASE == idFase && g.NUMTIPOGRUPO == 13).FirstOrDefault();

            if (testGroup == null)
            {
                // Si no marcamos ni familia ni producto y no existe grupo test report no hacemos nada
                if (!selFamilia && selProducto == null)
                    return;

                testGroup = new T_GRUPOPARAMETRIZACION { NUMFAMILIA = producto.NUMFAMILIA.Value, IDFASE = idFase, NUMTIPOGRUPO = 13, ALIAS = "Report", INOUT = "O" };
                testGroup.NUMGRUPO = uow.Database.SqlQuery<int>("select app.T_GRUPOPARAMNUMGRUPO.nextval from dual").FirstOrDefault();
                uow.T_GRUPOPARAMETRIZACION.Add(testGroup);
            }

            var fValor = testGroup.T_VALORPARAMETRO.Where(p => p.NUMPARAM == numParam).FirstOrDefault();
            if (fValor == null)
            {
                fValor = new T_VALORPARAMETRO { NUMGRUPO = testGroup.NUMGRUPO, NUMPARAM = numParam, FECHAALTA = DateTime.Now };
                testGroup.T_VALORPARAMETRO.Add(fValor);
            }

            fValor.VALOR = selFamilia ? "1" : "0";

            var pValor = uow.T_VALORPARAMETROPRODUCTO.Where(p => p.NUMGRUPO == testGroup.NUMGRUPO && p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == producto.VERSION && p.NUMPARAM == numParam).FirstOrDefault();
            if (pValor == null && selProducto.HasValue && selProducto.Value != selFamilia)
            {
                pValor = new T_VALORPARAMETROPRODUCTO { NUMGRUPO = testGroup.NUMGRUPO, NUMPRODUCTO = producto.NUMPRODUCTO, VERSION = producto.VERSION, NUMPARAM = numParam, FECHAALTA = DateTime.Now };
                uow.T_VALORPARAMETROPRODUCTO.Add(pValor);
            }

            if (pValor != null)
            {
                if (selProducto == null)
                    uow.T_VALORPARAMETROPRODUCTO.Remove(pValor);
                else
                    pValor.VALOR = selProducto.Value ? "1" : "0";
            }

            uow.SaveChanges();
        }

        public void AddTestReport(int numTest, byte[] pdf)
        {
            if (pdf == null)
                return;

            var existPDF = uow.T_TESTREPORT.Where(g => g.NUMTEST == numTest).Any();
            if (!existPDF)
                uow.T_TESTREPORT.Add(new T_TESTREPORT { NUMTEST = numTest, IMAGEDOC = pdf });
            else
            {
                var report = uow.T_TESTREPORT.Where(g => g.NUMTEST == numTest).FirstOrDefault();
                report.IMAGEDOC = pdf;
            }

            uow.SaveChanges();
        }

        public bool GetValidateProductByIDP(int numProducto, int version)
        {
            return uow.PRODUCTO.Where(t => t.NUMPRODUCTO == numProducto && t.VERSION == version && t.EMPLEADOVALIDAPRODUC != null).Any();
        }

        public List<LaserItemViewModel> GetDatosLaser(int producto, int version, string idfase)
        {
            string sql = string.Format(@"
                    SELECT articulo.descripcion as descripcionarticulo, articulo.codigobcn, it.descripcion as descripcionit, it.fechapdf, it.DocPdf, pfbien.numbien, b.codigo, b.imagen, b.descripcion
                    FROM app.productofase pf
                    INNER JOIN app.fase on fase.idfase = pf.idfase
                    INNER JOIN app.ProductoFaseBinario pfb ON pfb.numproducto = pf.numproducto AND pfb.version = pf.version AND pfb.secuencia = pf.secuencia AND pfb.Activo = 'S'
                    INNER JOIN app.articulo ON pfb.numarticulo = articulo.numarticulo
                    INNER JOIN app.productofaseinstec pfi ON pfi.numproducto = pf.numproducto AND pfi.version = pf.version AND pfi.secuencia = pf.secuencia
                    INNER JOIN app.instrucciontecnica it ON pfi.refinstruccion = it.refinstruccion
                    INNER JOIN app.productofasebien pfbien ON pfbien.numproducto = pf.numproducto AND pfbien.version = pf.version AND pfbien.secuencia = pf.secuencia
                    INNER JOIN app.bien b ON b.numbien = pfbien.numbien
                    WHERE pfb.NumProducto={0} AND pfb.Version={1} AND pf.idfase = {2}
                    ORDER BY pf.secuencia",
                producto, version, idfase);

            var Lineas = uow.Database.SqlQuery<LaserItemViewModel>(sql).ToList();
            if (Lineas == null)
                TestException.Create().PROCESO.MATERIAL.PLANTILLAS_LASER("No existe o no creadas para este producto").Throw();
               //throw new Exception("No existe secuencia de fase para este producto ");

            if (Lineas.Count == 0)
                TestException.Create().PROCESO.MATERIAL.PLANTILLAS_LASER("No existe o no creadas para este producto").Throw();

            return Lineas;
        }

        public List<BinaryItemViewModel> GetDatosBinario(int producto, int version, string idfase)
        {
            string sql = string.Format(@"
                    SELECT articulo.descripcion as descripcionarticulo, articulo.codigobcn, articulo.codclaprov as versionbinario, otart.idtipodoc as tipofichero, otart.nombrepc as nombrefichero, aotdpdf.docpdf as fichero
                    FROM app.productofase pf
                    INNER JOIN app.ProductoFaseBinario pfb ON pfb.numproducto = pf.numproducto AND pfb.version = pf.version AND pfb.secuencia = pf.secuencia
                    INNER JOIN app.articulo ON pfb.numarticulo = articulo.numarticulo
                    INNER JOIN app.aot_articulodoc otart ON pfb.numarticulo = otart.numarticulo
                    INNER JOIN app.aot_articulodocpdf otnumpdf ON pfb.numarticulo = otnumpdf.numarticulo
                    INNER JOIN app.aot_docpdf aotdpdf ON otnumpdf.numdoc = aotdpdf.numdoc
                    WHERE pfb.NumProducto={0} AND pfb.Version={1} AND pf.idfase = {2}
                    ORDER BY pf.secuencia",
                producto, version, idfase);

            var Lineas = uow.Database.SqlQuery<BinaryItemViewModel>(sql).ToList();
            if (Lineas == null)
                TestException.Create().PROCESO.MATERIAL.BINARIOS("No existe o no creadas para este producto").Throw();

            if (Lineas.Count == 0)
                TestException.Create().PROCESO.MATERIAL.BINARIOS("No existe o no creadas para este producto").Throw();

            return Lineas;
        }

        public string GetBloqueoBinario(int producto, int version, string idFase, string tipoDoc)
        {
            string sql = string.Format(@"
                        SELECT ab.MOTIVO
	                    FROM app.ARTICULOBLOQUEO ab
                        WHERE ab.NUMARTICULO = (SELECT articulo.numarticulo
							                    FROM app.productofase pf
							                    INNER JOIN app.ProductoFaseBinario pfb ON pfb.numproducto = pf.numproducto AND pfb.version = pf.version AND pfb.secuencia = pf.secuencia AND pfb.Activo = 'S'
							                    INNER JOIN app.articulo ON pfb.numarticulo = articulo.numarticulo
                                                INNER JOIN app.aot_articulodoc otart ON pfb.numarticulo = otart.numarticulo and otart.idtipodoc = '{3}'
							                    WHERE pfb.NumProducto= {0} AND pfb.Version= {1} AND pf.idfase = '{2}')", producto, version, idFase, tipoDoc);

            return uow.Database.SqlQuery<string>(sql).FirstOrDefault();
        }

        public BinaryItemViewModel GetBinario(int producto, int version, string idfase, string tipoDoc)
        {
            var binaryLocked = GetBloqueoBinario(producto, version, idfase, tipoDoc);
            if (!string.IsNullOrEmpty(binaryLocked))
                throw new Exception(string.Format("Binario Bloqueado, Motivo --> {0}", binaryLocked));

            string sql = string.Format(@"
                    SELECT articulo.descripcion as descripcionarticulo, articulo.codigobcn, articulo.codclaprov as versionbinario, otart.idtipodoc as tipofichero, NVL(otart.nombrepc, ARTICULO.CODIGOBCN||'.'||aottd.fileextension) as nombrefichero, aotdpdf.docpdf as fichero
                    FROM app.productofase pf
                    INNER JOIN app.ProductoFaseBinario pfb ON pfb.numproducto = pf.numproducto AND pfb.version = pf.version AND pfb.secuencia = pf.secuencia AND pfb.Activo = 'S'
                    INNER JOIN app.articulo ON pfb.numarticulo = articulo.numarticulo
                    INNER JOIN app.aot_articulodoc otart ON pfb.numarticulo = otart.numarticulo and otart.idtipodoc = '{3}'
                    INNER JOIN app.aot_articulodocpdf otnumpdf ON pfb.numarticulo = otnumpdf.numarticulo and otnumpdf.idtipodoc = otart.idtipodoc
                    INNER JOIN app.aot_docpdf aotdpdf ON otnumpdf.numdoc = aotdpdf.numdoc
                    INNER JOIN app.aot_tipodoc aottd ON otart.idtipodoc = aottd.idtipodoc
                    WHERE pfb.NumProducto={0} AND pfb.Version={1} AND pf.idfase ='{2}'
                    ORDER BY pf.secuencia", producto, version, idfase, tipoDoc);        

            return uow.Database.SqlQuery<BinaryItemViewModel>(sql).FirstOrDefault();
        }

        public List<LabelItemViewModel> GetLabels(int numProducto, int version, string idFase, string tipo = null)
        {
            var sql = string.Format(@"SELECT pf.IDFASE, a.NUMARTICULO, a.DESCRIPCION, SUBSTR(a.CODIGOBCN, 6, 2) as TIPO, a.CODIGOBCN, PC.NUMCOMPONENTE, PC.VERSIONCOMPONENTE, am.DESCRIPCION AS PAPEL
                    FROM app.productofase pf
                    INNER JOIN app.ProductoFaseBinario pfb ON pfb.numproducto = pf.numproducto AND pfb.version = pf.version AND pfb.secuencia = pf.secuencia AND pfb.ACTIVO = 'S'
                    INNER JOIN app.ARTICULO a on a.NUMARTICULO = pfb.NUMARTICULO
                    INNER JOIN APP.PRODUCTOCOMPONENTE PC ON PC.NUMPRODUCTO = pfb.NUMARTICULO 
                    INNER JOIN app.ARTICULO am on am.NUMARTICULO = PC.NUMCOMPONENTE
                    WHERE pf.NumProducto = {0} AND pf.Version = {1} AND a.CODIGOBCN  LIKE '00119{3}%' {2}
                    ORDER BY pf.secuencia",
                numProducto,
                version,
                string.IsNullOrEmpty(idFase) ? string.Empty : " AND pf.IDFASE = '" + idFase + "'",
                string.IsNullOrEmpty(tipo) ? null : tipo.PadLeft(2, '0'));

            return uow.Database.SqlQuery<LabelItemViewModel>(sql).ToList();
        }
       
        public byte[] GetBinario(int numArticulo, string tipoDoc)
        {
            var sql = string.Format(@"SELECT d.DOCPDF
                FROM APP.AOT_ARTICULODOC ad
                INNER JOIN APP.AOT_ARTICULODOCPDF ADP ON ADP.NUMARTICULO = AD.NUMARTICULO AND ADP.IDTIPODOC = AD.IDTIPODOC AND ADP.NUMREVISION = AD.NUMREVISION
                INNER JOIN APP.AOT_DOCPDF d ON d.NUMDOC = adp.NUMDOC
                WHERE ad.NUMARTICULO = {0} AND ad.IDTIPODOC = '{1}'
                    AND ad.NUMREVISION = (SELECT MAX(NUMREVISION) FROM APP.AOT_ARTICULODOC ad2 WHERE ad2.NUMARTICULO = ad.NUMARTICULO AND ad2.IDTIPODOC = ad.IDTIPODOC)",
                numArticulo, tipoDoc);

            var x = uow.Database.SqlQuery<byte[]>(sql).FirstOrDefault();
            return x;
        }

        public VW_EMPLEADO Login(string userName, string password)
        {
            string sql = string.Format("select app.wu_testuser('{0}','{1}') from dual", userName, password);
            var result = uow.Database.SqlQuery<int>(sql).FirstOrDefault();

            if (result == 0)
                return null;

            sql = string.Format("select app.wu_getuserid('{0}') from dual", userName);
            var empleadoId = uow.Database.SqlQuery<string>(sql).FirstOrDefault();

            return uow.VW_EMPLEADO.Where(p => p.IDEMPLEADO == empleadoId).FirstOrDefault();
        }

        public string GetUserParam(string application, string userName, string paramName)
        {
            return uow.WEBUSERPARAM.Where(p => p.IDUSER == userName && p.APPLICATION == application && p.PARAM == paramName).Select(p => p.VALUE).FirstOrDefault();
        }

        public T_TESTFASE GetLastTestFase(int numBastidor, string idFase, int numOrden = 0, bool? OK = true, bool includeParams = false)
        {
            var query = uow.T_TESTFASE
                    .Include("T_TESTVALOR")
                    .Include("T_TEST")
                    .AsQueryable();

            if (includeParams)
                query = query.Include("T_TESTVALOR.T_PARAMETRO");

            if (OK.HasValue)
            {
                var result = OK.Value ? "O" : "E";
                query = query.Where(p => p.RESULTADO == result);
            }

            if (numOrden != 0)
                query = query.Where(p => p.T_TEST.ORDENPRODUCTO.NUMMATRICULA == numBastidor && p.T_TEST.ORDENPRODUCTO.NUMORDEN == numOrden);

            if (idFase == "0")
                query = query.Where(p => p.T_TEST.NUMMATRICULA == numBastidor);
            else
                query = query.Where(p => p.T_TEST.NUMMATRICULA == numBastidor && p.IDFASE == idFase);

            if (numOrden == 0 && idFase == "0")
                query = query.Where(p => p.T_TEST.NUMMATRICULA == numBastidor);

            var testFase = query.OrderByDescending(p => p.NUMTESTFASE).FirstOrDefault();

            return testFase;
        }

        public int? GetLastNumTestFase(int numBastidor, int numOrden, string idFase)
        {
            var testFase = uow.T_TESTFASE
                .Where(p =>
                    p.T_TEST.ORDENPRODUCTO.NUMMATRICULA == numBastidor &&
                    p.T_TEST.ORDENPRODUCTO.NUMORDEN == numOrden &&
                    p.IDFASE == idFase)
                .OrderByDescending(p => p.NUMTESTFASE)
                .FirstOrDefault();

            if (testFase != null)
                return testFase.NUMTESTFASE;

            return null;
        }

        public List<T_TESTFASE> GetLastTestFaseOKByOrden(int numOrden, string idFase)
        {
            var testFase = uow.T_TESTFASE
                .Include("T_TESTVALOR")
                .Include("T_TEST")
                .Where(p =>
                    p.T_TEST.ORDENPRODUCTO.NUMORDEN == numOrden &&
                    p.IDFASE == idFase &&
                    p.RESULTADO == "O")
                .OrderByDescending(p => p.NUMTESTFASE).ToList();

            return testFase;
        }

        public T_TESTFASE GetLastTestFaseOKByOrden(int numOrden)
        {
            var testFase = uow.T_TESTFASE
                .Include("T_TESTVALOR")
                .Include("T_TEST")
                .Where(p =>
                    p.T_TEST.ORDENPRODUCTO.NUMORDEN == numOrden &&
                    p.RESULTADO == "O")
                .OrderByDescending(p => p.NUMTESTFASE).FirstOrDefault();

            return testFase;
        }

        public T_TESTFASE GetLastTestFaseOKBySerialNumber(int numOrden, string nroserie)
        {
            var testFase = uow.T_TESTFASE
                .Include("T_TESTVALOR")
                .Include("T_TEST")
                .Where(p =>
                    p.T_TEST.ORDENPRODUCTO.NUMORDEN == numOrden &&
                    p.T_TEST.ORDENPRODUCTO.NROSERIE == nroserie &&
                    p.RESULTADO == "O")
                .OrderByDescending(p => p.NUMTESTFASE).FirstOrDefault();

            return testFase;
        }

        public Tuple<int, byte[]> GetSequenceFileData(PRODUCTO producto, string idFase)
        {
            int tipoSecuencia = 0;
            string nombreFichero = "";

            return GetSequenceFileData(producto, idFase, ref tipoSecuencia, ref nombreFichero);
        }

        public Tuple<int, byte[]> GetSequenceFileData(PRODUCTO producto, string idFase, ref int tipoSecuencia, ref string nombreFichero)
        {
            var data = GetNewSequenceFileData(producto, idFase, ref tipoSecuencia, ref nombreFichero);
            if (data == null)
                  TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("NO SE HA ENCONTRADO SECUENCIA CREADA PARA ESTE PRODUCTO").Throw();
          
            return data;
        }

        private Tuple<int, byte[]> GetNewSequenceFileData(PRODUCTO producto, string idFase, ref int tipoSecuencia, ref string nombreFichero)
        {
            T_TESTSEQUENCEFILEPRODUCTO t_prod = null;
            T_TESTSEQUENCEFILEVERSION t_ver = null;

            t_prod = uow.T_TESTSEQUENCEFILEPRODUCTO
                .Include("T_TESTSEQUENCEFILE")
                .Where(p =>
                    p.T_TESTSEQUENCEFILE.NUMFAMILIA == producto.NUMFAMILIA &&
                    p.T_TESTSEQUENCEFILE.IDFASE == idFase &&
                    p.T_TESTSEQUENCEFILE.FICHERO == null &&
                    p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == producto.VERSION &&
                    p.FECHABAJA == null)
                .OrderByDescending(p => p.FECHAALTA)
                .FirstOrDefault();

            if (t_prod == null)
            {
                t_prod = uow.T_TESTSEQUENCEFILEPRODUCTO
                    .Include("T_TESTSEQUENCEFILE")
                    .Where(p =>
                        p.T_TESTSEQUENCEFILE.NUMFAMILIA == producto.NUMFAMILIA &&
                        p.T_TESTSEQUENCEFILE.IDFASE == idFase &&
                        p.T_TESTSEQUENCEFILE.FICHERO == null &&
                        p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == null &&
                        p.FECHABAJA == null)
                    .OrderByDescending(p => p.FECHAALTA)
                    .FirstOrDefault();

                if (t_prod == null)
                    return null;
                else
                    tipoSecuencia = 2;
            }
            else
                tipoSecuencia = 3;

            if (t_prod != null)
                t_ver = uow.T_TESTSEQUENCEFILEVERSION
                    .Where(p => p.NUMTSF == t_prod.NUMTSF && p.FECHAVALIDACION != null)
                    .OrderByDescending(p => p.FECHAVALIDACION)
                    .FirstOrDefault();

            if (t_ver == null)
                return null;

            nombreFichero = t_prod.T_TESTSEQUENCEFILE.NOMBREFICHERO ?? string.Empty;

            return Tuple.Create(t_ver.NUMTSF, t_ver.FICHERO);
        }

        public List<T_TESTSEQUENCEFILE> GetFamilySequenceFiles(int numFamilia)
        {
            var listSequenceFiles = uow.T_TESTSEQUENCEFILE
                .Where(t =>
                t.NUMFAMILIA == numFamilia &&
                t.FICHERO == null)
                .ToList();

            return listSequenceFiles;
        }

        public List<T_TESTSEQUENCEFILEPRODUCTO> GetSequenceFileProducto(T_TESTSEQUENCEFILE sequence)
        {
            var listSequenceProducto = uow.T_TESTSEQUENCEFILEPRODUCTO
                .Where(t =>
                t.NUMTSF == sequence.NUMTSF)
                .OrderBy(t => t.NUMPRODUCTO)
                .ToList();

            return listSequenceProducto;
        }

        public List<T_TESTSEQUENCEFILEVERSION> GetSequenceFileVersion(T_TESTSEQUENCEFILE sequence)
        {
            var listSequenceVersion = uow.T_TESTSEQUENCEFILEVERSION
                .Where(t =>
                t.NUMTSF == sequence.NUMTSF)
                .OrderByDescending(t => t.FECHAALTA)
                .ToList();

            return listSequenceVersion;
        }

        public List<FASE> GetFamilyTestFases (int numFamilia)
        {
            var listProducts = GetProductoByFamilia(numFamilia).Select(product => product.NUMPRODUCTO).ToList();

            var listTestFases = uow.PRODUCTOFASE
                .Where(t =>
                listProducts.Contains(t.NUMPRODUCTO) &&
                t.FASETEST == "S")
                .Select(productoFase => productoFase.FASE)
                .Distinct()
                .ToList();

            return listTestFases;
        }

        public ASISTENCIATECNICA GetAsistenciaTecnica(int numAsistencia, int numLinea)
        {
            return uow.ASISTENCIATECNICA.Where(p => p.NUMASISTENCIA == numAsistencia && p.NUMLIN == numLinea).FirstOrDefault();
        }

        public PRODUCTO GetProductoAutoTest(string devicePC)
        {
            string sql = string.Format("SELECT NUMARTICULO FROM APP.ARTICULOBIEN INNER JOIN APP.BIEN ON BIEN.NUMBIEN = ARTICULOBIEN.NUMBIEN WHERE BIEN.REFPROVEEDOR = '{0}'", devicePC);

            var numArticulo = uow.Database.SqlQuery<int?>(sql).FirstOrDefault();
            if (!numArticulo.HasValue)
                return null;

            return uow.PRODUCTO.Include("ARTICULO").Where(p => p.NUMPRODUCTO == numArticulo).OrderByDescending(p => p.VERSION).FirstOrDefault();
        }

        public bool IsSerialNumberValidInOF(string serialNumber, int orden)
        {
            string sql = string.Format("SELECT COUNT(*) FROM APP.RESERVANROSERIELOG rnsl WHERE rnsl.NumOrden = '{0}' AND replace(rnsl.NroSerieDesde,'-','')<='{1}' AND '{1}' <=replace (rnsl.NroSerieHasta,'-','')", orden, serialNumber);

            var count = uow.Database.SqlQuery<int?>(sql).ToList().FirstOrDefault();
            if (!count.HasValue)
                return false;

            return count.Value == 1;
        }

        public BIEN GetTorreByPC(string devicePC)
        {
            string sql = string.Format("SELECT * FROM APP.BIEN WHERE BIEN.REFPROVEEDOR = '{0}'", devicePC);
            var bien = uow.Database.SqlQuery<BIEN>(sql).FirstOrDefault();
            if (bien == null)
                return null;
            return bien;
        }

        public Tuple<DateTime?, string, string> GetLastAutoTest(string devicePC)
        {
            string sql = string.Format("SELECT * FROM APP.BIEN WHERE BIEN.REFPROVEEDOR = '{0}'", devicePC);
            var bien = uow.Database.SqlQuery<BIEN>(sql).FirstOrDefault();
            if (bien == null)
                return null;

            var producto = GetProductoAutoTest(devicePC);
            if (producto == null)
                return null;

            if (producto.ARTICULO.DESCRIPCION.Contains("PLACAS"))
                return null;

            var dateTest = uow.T_TESTFASE
                .Where(p =>
                    p.T_TEST.NUMMATRICULATEST == bien.NUMBIEN &&
                    p.T_TEST.NUMPRODUCTOTEST == producto.NUMPRODUCTO &&
                    p.RESULTADO == "O"
                    )
               .OrderByDescending(p => p.FECHA)
               .Select(p => p.FECHA)
               .FirstOrDefault();

            return Tuple.Create<DateTime?, string, string>(dateTest, bien.DESCRIPCION, bien.UBICACION);
        }

        public void AddTestFaseFile(int numTestFase, string name, byte[] data, string tipoDocId)
        {
            var testFile = new T_TESTFASEFILE { NUMTESTFASE = numTestFase, NOMBRE = name, IDTIPODOC = tipoDocId};

            testFile.NUMTFF = uow.Database.SqlQuery<int>("SELECT app.T_TESTFASEFILENUMTFF.nextval from dual").FirstOrDefault();

            uow.T_TESTFASEFILE.Add(testFile);

            uow.SaveChanges();

            SetTestFile(testFile.NUMTFF, data);
        }

        private void SetTestFile(int NumTestFile, byte[] blob)
        {
            using (var trans = uow.Database.BeginTransaction())
            {
                try
                {
                    using (var cmd = uow.CreateCommand("app.SetTestFile"))
                    {
                        cmd.AddParameter("p_NumTestFile", DbType.Decimal, NumTestFile);
                        cmd.AddParameter("p_Blob", DbType.Binary, blob);
                        cmd.ExecuteCommand();
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(ex.Message).Throw();
                    throw;
                }
            }
        }

        private byte[] GetTestFile(int NumTestFile)
        {
            var result = uow.Database.SqlQuery<byte[]>(string.Format("select app.GetTestFile({0}) from dual", NumTestFile));
            return result.FirstOrDefault();
        }

        public List<T_TESTFASEFILE> GetTestFaseFiles(int bastidor, string tipoDocId)
        {
            var listNumTestFases = uow.T_TESTFASEFILE
                .Where(t =>
                t.NUMTESTFASE == t.T_TESTFASE.NUMTESTFASE
                && t.T_TESTFASE.NUMTEST == t.T_TESTFASE.T_TEST.NUMTEST)
                .Where(t =>
                t.T_TESTFASE.T_TEST.NUMMATRICULA == bastidor
                && t.T_TESTFASE.RESULTADO == "O")
                .ToList();

            if (listNumTestFases.Count == 0)
                return null;

            var maxNumTestFase = listNumTestFases.Max(t => t.NUMTESTFASE);

            var testFaseFiles = listNumTestFases
                .Where(t =>
                t.NUMTESTFASE == maxNumTestFase
                && t.IDTIPODOC == tipoDocId)             
                .Select(t => t)
                .ToList();

            foreach (var tff in testFaseFiles)
                if (tff.FICHERO == null)
                    tff.FICHERO = GetTestFile(tff.NUMTFF);

            return testFaseFiles;
        }

        public List<T_TESTFASEFILE> GetTestFaseFiles(string name, string tipoDocId)
        {
            var listNumTestFases = uow.T_TESTFASEFILE               
                .Where(t =>
                t.NOMBRE == name
                && t.T_TESTFASE.RESULTADO == "O")
                .ToList();

            if (listNumTestFases.Count == 0)
                return null;

            var maxNumTestFase = listNumTestFases.Max(t => t.NUMTESTFASE);

            var testFaseFiles = listNumTestFases.Where(p => p.NUMTESTFASE == maxNumTestFase).Where(p => p.IDTIPODOC == tipoDocId).ToList();

            foreach (var tff in testFaseFiles)
                if (tff.FICHERO == null)
                    tff.FICHERO = GetTestFile(tff.NUMTFF);

            return testFaseFiles;
        }

        public void AddProcessFaseFile(int numRegister, byte[] data, string tipoDocId)
        {
            var processFile = new ORDENPROCESOFASEFILE { NUMREGISTRO = numRegister, FICHERO = data, IDTIPODOC = tipoDocId };

            processFile.NUMOPFF = uow.Database.SqlQuery<int>("SELECT app.ORDENPROCESOFASEFILENUMOPFF.nextval from dual").FirstOrDefault();

            uow.ORDENPROCESOFASEFILE.Add(processFile);

            uow.SaveChanges();
        }

        public int AddTestFaseVAFile(int numTestFase, string name, byte[] data, string tipoDocId)
        {
            int numVATFF = -1;
            using (var trans = uow.Database.BeginTransaction())
            {
                try
                {
                    using (var cmd = uow.CreateCommand("app.AddTestVAFile"))
                    {
                        cmd.AddParameter("p_NumTestFase", DbType.Int32, numTestFase);
                        cmd.AddParameter("p_IdTipoDoc", DbType.String, tipoDocId);
                        cmd.AddParameter("p_nombre", DbType.String, name);
                        cmd.AddParameter("p_blob", DbType.Binary, data);
                        var paramNumVATFF = cmd.AddParameter("p_NumVATFF", DbType.Int32, null, ParameterDirection.Output);
                        cmd.ExecuteCommand();
                        numVATFF = (int)paramNumVATFF.Value;
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(ex.Message).Throw();
                }
            }
            return numVATFF;
        }

        public void AddTraceTime(string description, int time, long size)
        {
            string sql = $"INSERT INTO app.TraceTime (Fecha, Descripcion,Tiempo,Longitud, hostname) " +
                $"VALUES (sysdate, '{description}', {time}, {size}, '{Dns.GetHostName()}')";

            uow.Database.ExecuteSqlCommand(sql);
        }

        public int AddTestConfigFile(int numTCF, string userName, byte[] data, string description)
        {
            int numTCFV = -1;
            using (var trans = uow.Database.BeginTransaction())
            {
                try
                {
                    using (var cmd = uow.CreateCommand("app.AddTestCfgFileVer"))
                    {
                        cmd.AddParameter("p_NumTCF", DbType.Int32, numTCF);
                        cmd.AddParameter("p_Description", DbType.String, description);
                        cmd.AddParameter("p_Usuario", DbType.String, userName);
                        cmd.AddParameter("p_Blob", DbType.Binary, data);
                        var paramNumTCFV = cmd.AddParameter("p_NumTCFV", DbType.Int32, null, ParameterDirection.Output);
                        cmd.ExecuteCommand();
                        numTCFV = (int)paramNumTCFV.Value;
                    }
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(ex.Message).Throw();
                }
            }

            return numTCFV;
        }

        public List<T_TESTCONFIGFILEVERSION> GetAllProductTestConfigFiles(int numProducto, int version)
        {
            return GetAllProductTestConfigFiles(GetProducto(numProducto, version));
        }

        public List<T_TESTCONFIGFILEVERSION> GetAllProductTestConfigFiles(PRODUCTO producto)
        {
            var productTableList = uow.T_TESTCONFIGFILEPRODUCTO
                .Where(p => p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == producto.VERSION)
                .ToList();

            productTableList.AddRange(uow.T_TESTCONFIGFILEPRODUCTO
                .Where(p => p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == null)
                .ToList());

            var versionTableList = new List<T_TESTCONFIGFILEVERSION>();

            if (productTableList == null)
                return versionTableList;

            foreach (var productTable in productTableList)
            {
                var versionTable = uow.T_TESTCONFIGFILEVERSION
                    .Include("T_TESTCONFIGFILE")
                    .Where(t => t.NUMTCF == productTable.NUMTCF)
                    .OrderByDescending(t => t.FECHAALTA)
                    .FirstOrDefault();

                if (versionTable != null)
                {
                    versionTableList.Add(versionTable);
                    logger.Info($"Obtenido fichero de configuracion \"{versionTable.T_TESTCONFIGFILE.DESCRIPCION}\"");
                }  
            }

            return versionTableList;
        }

        public byte[] GetTestConfigFile(PRODUCTO producto, string description)
        {
            var listconfigFiles = uow.T_TESTCONFIGFILE
                    .Where(t =>
                    t.NUMFAMILIA == producto.NUMFAMILIA
                    && t.DESCRIPCION == description)
                    .ToList();

            var configFile = listconfigFiles
                .Where(c => 
                c.T_TESTCONFIGFILEPRODUCTO
                    .Where(p => 
                    p.NUMPRODUCTO == producto.NUMPRODUCTO 
                    && p.VERSION == producto.VERSION)
                    .Any())
                .FirstOrDefault();

            if (configFile == null)
                configFile = listconfigFiles
                    .Where(c =>
                        c.T_TESTCONFIGFILEPRODUCTO
                            .Where(p =>
                                p.NUMPRODUCTO == producto.NUMPRODUCTO
                                && p.VERSION == null)
                            .Any())
                    .FirstOrDefault();

            if (configFile == null)
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI($"No existe ningun archivo con descripcion: \"{description}\" para el producto {producto.NUMPRODUCTO}").Throw();

            var configFileVersion = configFile.T_TESTCONFIGFILEVERSION.OrderByDescending(t => t.FECHAALTA).FirstOrDefault();

            if (configFileVersion == null)
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI($"No existe ninguna version de archivo con descripcion: \"{description}\" para el producto {producto.NUMPRODUCTO}").Throw();

            byte[] data = GetTestConfigFile(configFileVersion.NUMTCFV);

            return data;
        }

        public byte[] GetTestConfigFile(int numTCFV)
        {
            string sql = $"SELECT APP.GetTestCfgFileVer({numTCFV}) FROM DUAL";
            var configFile = uow.Database.SqlQuery<byte[]>(sql).FirstOrDefault();
            return configFile;
        }

        public List<BIEN> GetBienByProductoFase(int numProducto, int version, string idfase)
        {
            var sql = string.Format(@"select * from app.BIEN
             INNER JOIN
             app.PRODUCTOFASEBIEN ON app.BIEN.NUMBIEN = app.PRODUCTOFASEBIEN.NUMBIEN
             INNER JOIN
             app.PRODUCTOFASE ON app.PRODUCTOFASE.NUMPRODUCTO = app.PRODUCTOFASEBIEN.NUMPRODUCTO
             AND app.PRODUCTOFASE.VERSION = app.PRODUCTOFASEBIEN.VERSION
             AND app.PRODUCTOFASE.SECUENCIA = app.PRODUCTOFASEBIEN.SECUENCIA
             where app.PRODUCTOFASE.NUMPRODUCTO = {0} AND app.PRODUCTOFASE.VERSION = {1} AND app.PRODUCTOFASE.IDFASE = '{2}'
             AND app.BIEN.SITUACION = 'A' AND app.BIEN.CONTROLBIEN = 'S'", numProducto, version, idfase);

            var list = uow.Database.SqlQuery<BIEN>(sql).ToList();
            return list;
        }

        public List<BIEN> GetBienHijos(int numBien)
        {
            var bien = uow.BIEN
                .Where(p => p.NUMBIENPADRE == numBien).ToList();

            return bien;
        }

        public BIEN GetBienParamsAssitencia(int numBien)
        {
            var bien = uow.BIEN
                .Where(p => p.NUMBIEN == numBien)
                .Include("BIEN1")
                .Include("BIENPARAM")
                .Include("BIEN1.BIENPARAM")
                .Include("BIENASISTENCIA")
                .Include("BIENPROGRAMACIONASISTENCIA")
                .FirstOrDefault();

            return bien;
        }

        public string GetParamByDescription(int numProduct, string idFase, string description)
        {
            var sql = string.Format(@"SELECT VALOR
                FROM APP.T_TEST t
                INNER JOIN APP.T_TESTFASE tf ON TF.NUMTEST = t.NUMTEST
                INNER JOIN APP.T_TESTVALOR tv ON tv.NUMTESTFASE = tf.NUMTESTFASE
                INNER JOIN APP.T_PARAMETRO p ON p.NUMPARAM = tv.NUMPARAM
                WHERE t.NUMPRODUCTOTEST = {0} AND tf.IDFASE = '{1}' AND p.PARAM = '{2}' AND tf.RESULTADO = 'O'
                ORDER BY tf.FECHA DESC
                FETCH FIRST 1 ROWS ONLY",
                numProduct, idFase, description);

            var valor = uow.Database.SqlQuery<string>(sql).FirstOrDefault();

            return valor;
        }

        public int? GetIdentifierNumberKSP()
        {
            string sql = string.Format("SELECT app.numidkca.nextval FROM dual");

            var id = uow.Database.SqlQuery<int?>(sql).FirstOrDefault();
            if (!id.HasValue)
                return null;
            return id;
        }

        public int? GetIdentifierNumberKSPZity()
        {
            string sql = string.Format("SELECT app.KSPCITYID.nextval FROM dual");

            var id = uow.Database.SqlQuery<int?>(sql).FirstOrDefault();
            if (!id.HasValue)
                return null;
            return id;
        }

        public int? GetMasterNumberKSPZity()
        {
            string sql = string.Format("SELECT app.KSPCITYMASTER.nextval FROM dual");

            var id = uow.Database.SqlQuery<int?>(sql).FirstOrDefault();
            if (!id.HasValue)
                return null;
            return id;
        }

        public int GetMAC_OPA()
        {
            string sql = string.Format("SELECT app.opa01.nextval FROM dual");
            return uow.Database.SqlQuery<int>(sql).FirstOrDefault();
        }

        public string GetCodigoCircutor(int numProducto)
        {
            return uow.Database.SqlQuery<string>(string.Format("SELECT RefCliente FROM app.ArticuloCliente WHERE NumArticulo={0} AND INACTIVO ='N'", numProducto)).ToList().FirstOrDefault();
        }

        public string GetCostumerCode2(int numProducto)
        {
            var costumerCode2 = uow.ARTICULOCLIENTE.Where(p => p.NUMARTICULO == numProducto).Select(p => p.REFCLIENTE2).FirstOrDefault();

            return costumerCode2;
        }

        public string GetCodigoBcn(int numProducto)
        {
            var codeBcn = uow.ARTICULO.Where(p => p.NUMARTICULO == numProducto).Select(p => p.CODIGOBCN).FirstOrDefault();

            return codeBcn;
        }

        public List<string> GetDireccionesByNumProducto(int numProducto, int version)
        {
            var numDireccion = uow.PRODUCTO.Where(p => p.NUMPRODUCTO == numProducto && p.VERSION == version).Select(p => p.NUMDIRECCION).FirstOrDefault();

            return GetDirecciones(numDireccion.GetValueOrDefault(-1));
        }

        public List<string> GetDirecciones(int numDireccion)
        {
            var result = new List<string>();
            var list = uow.Database.SqlQuery<DireccionesViewModel>("SELECT * FROM APP.MARCADIRECCION WHERE NUMDIRECCION = " + numDireccion).FirstOrDefault();

            if (list != null)
            {
                result.Add(list.DIRECCION1);
                result.Add(list.DIRECCION2);
                result.Add(list.DIRECCION3);
                result.Add(list.DIRECCION4);
            }

            return result;
        }

        public List<string> GetAtributosCliente(int numProducto, string codCircutor)
        {
            var result = new List<string>();
            var atrib = uow.ARTICULOCLIENTE.Where(p => p.NUMARTICULO == numProducto && p.REFCLIENTE == codCircutor)
                .Select(s => new AtributosViewModel
                {
                     ATRIB1 = s.ATTRIB1,
                     ATRIB2 = s.ATTRIB2,
                     ATRIB3 = s.ATTRIB3,
                     ATRIB4 = s.ATTRIB4,
                     ATRIB5 = s.ATTRIB5,
                }).FirstOrDefault();

            if (atrib != null)
            {
                result.Add(atrib.ATRIB1);
                result.Add(atrib.ATRIB2);
                result.Add(atrib.ATRIB3);
                result.Add(atrib.ATRIB4);
                result.Add(atrib.ATRIB5);
            }

            return result;
        }

        public string GetLoteCaja(int numCaja)
        {
            var sql = string.Format(@"SELECT lote				 
	                                FROM app.CAJA WHERE numcaja = {0}", numCaja);

            return uow.Database.SqlQuery<string>(sql).FirstOrDefault();
        }

        public void SetLoteCaja(int numCaja, string lote)
        {
            var sql = string.Format(@"UPDATE app.CAJA SET lote = {0} WHERE numcaja = {1}", lote, numCaja);
            uow.Database.ExecuteSqlCommand(sql);
        }

        public string GetEANCode(int numProducto, string codCircutor)
        {           
            var ean = uow.ARTICULOCLIENTE.Where(p => p.NUMARTICULO == numProducto && p.REFCLIENTE == codCircutor)
                .Select(s => s.EAN13).FirstOrDefault();
            
            return ean;
        }

        public string GetEANMarcaCode(int numProducto, string codCircutor)
        {
            var ean = uow.ARTICULOCLIENTE.Where(p => p.NUMARTICULO == numProducto && p.REFCLIENTE == codCircutor)
                .Select(s => s.EAN13MARCA).FirstOrDefault();

            return ean;
        }


        public string GetRefClientLine(int numOrden)
        {
            var sql = string.Format(@"SELECT pva.NumLinRef				 
	                                FROM app.PedidoventaArticulo pva, app.PedidoVenta pv
		                            WHERE pva.numPedido = (SELECT pvao.NumPedido
								                            FROM app.pva_orden pvao
									                        WHERE pvao.NumOrden={0} AND
											                pvao.Informativo='N'
                                                            AND rownum = 1) 
		                            AND pva.numLin = (SELECT pvao.numLin
								                        FROM app.pva_orden pvao
									                    WHERE pvao.NumOrden={0} AND
											            pvao.Informativo='N'
                                                        AND rownum = 1) 
		                            AND pv.NumPedido=pva.numPedido", numOrden);

            var refClientLine =  uow.Database.SqlQuery<int?>(sql).FirstOrDefault();

            return string.Format("{0:00000}", refClientLine);
        }

        public string GetReferenciaOrigen(int numOrden)
        {
            var sql = string.Format(@"SELECT referenciaOrigen
	                                FROM app.pedidoventa pv 
		                            WHERE pv.numpedido = (SELECT NUMPEDIDO FROM app.pva_orden WHERE numorden = {0} AND rownum = 1)", numOrden);

            return uow.Database.SqlQuery<string>(sql).FirstOrDefault();
        }

        public Dictionary<string,string> GetDescripcionLenguagesCliente(int numProducto, string codCircutor)
        {
            var result = new Dictionary<string, string>();
            var atrib = uow.ARTICULOCLIENTEDESCRIPCION.Where(p => p.NUMARTICULO == numProducto && p.REFCLIENTE == codCircutor)
                .Select(s => new DescripcionLenguages
                {
                   IDIDIOMA = s.IDIDIOMA,
                   DESCRIPCION = s.DESCRIPCION
                }).Distinct().ToList();

            if (atrib != null)
                foreach (DescripcionLenguages leng in atrib)
                    result.Add(leng.IDIDIOMA, leng.DESCRIPCION);

            return result;
        }

        public int GetIDCajaActual(int orden)
        {
            try
            {
                var result = uow.Database.SqlQuery<int?>(string.Format("select app.cj_getLastNumCaja({0}) from dual", orden)).ToList().FirstOrDefault();
                if (!result.HasValue)
                    return 0;
                else
                    return result.Value;
            }catch(Exception ex)
            {
                TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(ex.Message).Throw();
                throw;
            }
        }

        public int? GetIDCajaByBastidor(int bastidor)
        {
            var query = string.Format(@"SELECT NROSERIE FROM app.T_TEST t JOIN app.T_TESTFASE tf ON t.numtest = tf.numtest WHERE t.NUMMATRICULA = {0} AND tf.RESULTADO = 'O'
                        AND tf.FECHA = (SELECT MAX(tf.FECHA) FROM app.t_test t JOIN app.t_testfase tf ON t.numtest = tf.numtest WHERE t.nummatricula = {0} AND tf.RESULTADO = 'O')", bastidor);

            var ns = uow.Database.SqlQuery<string>(query).ToList().FirstOrDefault();

            var result = uow.Database.SqlQuery<int?>(string.Format("select app.CJ_CAJAFROMNROSERIE('{0}') from dual", ns)).ToList().FirstOrDefault();

            return result ?? 0;
        }

        public string GetReferenciaVenta(int numProducto)
        {
            var refVenta = uow.Database.SqlQuery<string>(string.Format("select REFVENTA from app.ARTICULO where numarticulo = {0}", numProducto)).ToList().FirstOrDefault();
            return refVenta;
        }

        public string GetDescripcionComercial(int numProducto)
        {
            var desCom = uow.Database.SqlQuery<string>(string.Format("select DESCRIPCIONCOMERCIAL from app.ARTICULO where numarticulo = {0}", numProducto)).ToList().FirstOrDefault();
            return desCom;
        }

        public int GetLaseredPieces(int orden)
        {
            var result = uow.Database.SqlQuery<int?>(string.Format("SELECT MIN(COUNT(*)) FROM app.ORDENPROCESOFASE WHERE NUMORDEN = {0} GROUP BY IDFASE", orden)).FirstOrDefault();

            if (result.HasValue)
                return result.Value;

            return 0;
            
        }

        public Tuple<int, int> GetCajaActual(int orden)
        {
            var result = uow.Database.SqlQuery<string>(string.Format("select app.cj_CAJAORDEN({0}) from dual", orden)).ToList().FirstOrDefault();
            if (string.IsNullOrEmpty(result))
                return Tuple.Create<int, int>(0, 0);
            else
            {
                var cajaTotalActual = result.Split('/');
                return Tuple.Create<int, int>(Convert.ToInt32(cajaTotalActual[0].Trim()), Convert.ToInt32(cajaTotalActual[1].Trim()));
            }
        }

        public int GetNumEquiposCajaActual(int cajaActual)
        {
            var result = uow.Database.SqlQuery<int?>(string.Format("select app.cj_getNumEquiposAsignados({0}) from dual", cajaActual)).ToList().FirstOrDefault();

            if (!result.HasValue)
                return 0;
            else
                return result.Value;
        }

        public bool GetBoxHasFinished(int numCaja, string numSerie)
        {
            var sql = string.Format(@"SELECT COUNT(*)
                                        FROM app.CAJAEQUIPO ce
                                            WHERE ce.Numcaja = {0} AND
                                            ce.NroSerie = '{1}' AND
                                            ce.NumEquipo = (SELECT COUNT(*)
                                                                FROM app.cajaEquipo ce2
                                                                    WHERE ce2.NumCaja = {0})", numCaja, numSerie);

            var valor = uow.Database.SqlQuery<int>(sql).FirstOrDefault();

            return valor == 1;
        }

        public bool GetBoxHasClosed(int numCaja)
        {
            var sql = string.Format(@"SELECT cerrada
                                        FROM app.CAJA
                                            WHERE Numcaja = {0}", numCaja);

            var valor = uow.Database.SqlQuery<string>(sql).FirstOrDefault();

            return valor == "S";
        }

        public string GetMinNumSerieCaja(int numCaja)
        {
            var sql = string.Format(@"SELECT ce.NroSerie 
                                    FROM app.cajaEquipo ce
                                    WHERE ce.NumCaja = {0} AND
                                    ce.NumEquipo = (SELECT MIN(ce.NumEquipo)
                                                    FROM app.CajaEquipo ce
                                                    WHERE ce.NumCaja = {0} AND
                                                    ce.Situacion='A')", numCaja);

            return uow.Database.SqlQuery<string>(sql).FirstOrDefault();
        }

        public string GetMaxNumSerieCaja(int numCaja)
        {

            var sql = string.Format(@"SELECT ce.NroSerie 
                                    FROM app.cajaEquipo ce
                                    WHERE ce.NumCaja = {0} AND
                                    ce.NumEquipo = (SELECT MAX(ce.NumEquipo)
                                                    FROM app.CajaEquipo ce
                                                    WHERE ce.NumCaja = {0} AND
                                                    ce.Situacion='A')", numCaja);

            return uow.Database.SqlQuery<string>(sql).FirstOrDefault();
        }

        public List<string> GetAllSerialNumbersBox(int numCaja)
        {
            return uow.CAJAEQUIPO.Where(c => c.NUMCAJA == numCaja).Select(c => c.NROSERIE).ToList();
        }

        public string GetCurrentSerialNumber()
        {
            return uow.Database.SqlQuery<string>("select app.cj_getCurrentSerialCode from dual").ToList().FirstOrDefault();
        }

        public bool ResetLineaCajas()
        {
            var resp = uow.Database.SqlQuery<string>("select app.cj_resetLineaCaja from dual").ToList().FirstOrDefault();
            return resp.Trim().ToUpper().Contains("OK");
        }

        public int? GenerarOrdenEnsayo(int numProducto, int version, int unidades, string notas)
        {
            using (var trans = uow.Database.BeginTransaction())
            {
                try
                {
                    int? numOrden = null;

                    using (var cmd = uow.CreateCommand("app.NuevaOrdenEnsayo"))
                    {
                        cmd.AddParameter("result", DbType.Int32, null, ParameterDirection.ReturnValue);

                        cmd.AddParameter("producto", DbType.Int32, Convert.ToInt32(numProducto));
                        cmd.AddParameter("version", DbType.Int32, Convert.ToInt32(version));
                        cmd.AddParameter("cantidad", DbType.Int64, Convert.ToInt64(unidades));
                        cmd.AddParameter("notas", DbType.String, notas, ParameterDirection.Input, 100);

                        cmd.ExecuteCommand();

                        numOrden = Convert.ToInt32(cmd.Parameters["result"].Value);
                    }

                    trans.Commit();

                    return numOrden;

                } catch (Exception ex)
                {
                    trans.Rollback();
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(ex.Message).Throw();
                    throw;
                }
            }
        }

        public int? NuevaOrdenAsistencia(int numeroAssistencia, int version = 0)
        {
            using (var trans = uow.Database.BeginTransaction())
            {
                try
                {
                    int? numOrden = null;

                    using (var cmd = uow.CreateCommand("app.NuevaOrdenAsistencia"))
                    {
                        cmd.AddParameter("result", DbType.Int32, null, ParameterDirection.ReturnValue);
                        cmd.AddParameter("numAsistencia", DbType.Int32, Convert.ToInt32(numeroAssistencia));
                        cmd.AddParameter("version", DbType.Int32, Convert.ToInt32(version));

                        cmd.ExecuteCommand();

                        numOrden = Convert.ToInt32(cmd.Parameters["result"].Value);
                    }

                    trans.Commit();

                    switch (numOrden)
                    {
                        case 2002:
                            throw new Exception("Error. numero de orden incorrecta");
                        case 2003:
                            throw new Exception("Error. numero de orden incorrecta");
                        case 2004:
                            throw new Exception("Error. numero de orden incorrecta");
                    }

                    return numOrden;

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    TestException.Create().SOFTWARE.BBDD.CONSULTA_PGI(ex.Message).Throw();
                    throw;
                }
            }
        }

        public T_FAMILIA AddFamiliaToProducto(PRODUCTO producto, int familia)
        {
            var product = uow.PRODUCTO
             .Where(p => p.NUMPRODUCTO == producto.NUMPRODUCTO && p.VERSION == producto.VERSION)
             .FirstOrDefault();

            var t_familia = uow.T_FAMILIA
            .Where(p => p.NUMFAMILIA == familia)
            .FirstOrDefault();

            product.NUMFAMILIA = familia;

            uow.SaveChanges();

            return t_familia;
        }

        public ErrorsIrisDescription GetIrisErrorsDescriptions(string errorcode, int numfamilia, int producto, string fase)
        {
            var sql = string.Format(@"SELECT T.DESCRIPCION, T.DIAGNOSTICO 
                                    FROM
                                    (
                                        SELECT DESCRIPCIONML as DESCRIPCION, DIAGNOSTICOML as DIAGNOSTICO, 1 as ORDEN
                                        FROM app.T_IRISERRORPRODUCTOFASE
                                        WHERE ERRORCODE = '{0}' AND NUMARTICULO = {1} AND IDFASE = '{3}' AND DESCRIPCIONML IS NOT NULL
                                        UNION ALL
                                        SELECT DESCRIPCIONML, DIAGNOSTICOML, 2
                                        FROM app.T_IRISERRORFAMILIAFASE
                                        WHERE ERRORCODE = '{0}' AND NUMFAMILIA = {2} AND IDFASE = '{3}' AND DESCRIPCIONML IS NOT NULL
                                        UNION ALL
                                        SELECT NVL(DESCRIPCIONML, DESCRIPCION), DIAGNOSTICOML, 3
                                        FROM app.T_IRISERROR
                                        WHERE ERRORCODE = '{0}'
                                    ) T
                                    ORDER BY ORDEN
                                    FETCH FIRST 1 ROWS ONLY", errorcode, producto, numfamilia, fase);

            var result = uow.Database.SqlQuery<ErrorsIrisDescription>(sql).ToList().FirstOrDefault();

            return result;
        }

        public string[] GetNewMatricula(int numMatriculas = 1)
        {
            string[] result = new string[numMatriculas];
            for (int i = 0; i < numMatriculas; i++)
            {
                string sql = "SELECT app.MatriculaNumMatricula.NextVal FROM dual";

                result[i] = uow.Database.SqlQuery<int>(sql).FirstOrDefault().ToString();

                string sql2 = string.Format("INSERT INTO app.Matricula (NumMatricula, Usuario, Situacion) VALUES ({0},'DezacWebUser', 'I')", result[i]);

                uow.Database.ExecuteSqlCommand(sql2);
            }

            return result;
        }

        public List<DeviceDataView> GetAllDeviceTestOK(int numOrden, string idfase)
        {

            var HasNumSerie = (from f in uow.ORDENPRODUCTO
                               where f.NUMORDEN == numOrden && f.NROSERIE != null
                               select f).ToList();

            var result = new List<DeviceDataView>();

            if (HasNumSerie == null)
                result = GetTestHistory(numOrden, idfase);
            else
                result = GetDeviceBox(numOrden);

            return result;
        }

        public List<DeviceDataView> GetTestHistory(int numOrden, string idFase)
        {
            var test = uow.T_TEST
                .Include("ORDENPRODUCTO")
                .Include("ORDENPRODUCTO.ORDEN")
                .Include("PRODUCTO")
                .Include("PRODUCTO.ARTICULO")
                .AsQueryable();

            test = test.Where(p => p.ORDENPRODUCTO != null && p.ORDENPRODUCTO.NUMORDEN == numOrden);

            var results = from t in test
                          join f in uow.T_TESTFASE
                                .Include("FASE")
                          on t.NUMTEST equals f.NUMTEST
                          where f.RESULTADO == "O" && f.NUMTESTFASE == uow.T_TESTFASE.Where(p => p.NUMTEST == t.NUMTEST && p.IDFASE == idFase).Select(p => p.NUMTESTFASE).Max()
                          orderby f.NUMTESTFASE
                          select new DeviceDataView
                          {
                              NUMORDEN = t.ORDENPRODUCTO != null ? t.ORDENPRODUCTO.NUMORDEN : 0,
                              DESCRIPCION = t.PRODUCTO.ARTICULO.DESCRIPCION,
                              NUMMATRICULA = t.NUMMATRICULATEST.Value,
                              NUMPRODUCTO = t.NUMPRODUCTOTEST.Value,
                              VERSION = t.VERSIONPRODUCTOTEST.Value,
                              NROSERIE = t.NROSERIE,
                          };

            var x = results.ToList();
            return x;
        }

        public List<DeviceDataView> GetDeviceBox(int numOrden)
        {
            List<DeviceDataView> devicesBox;

            var FirstNumSerie = (from f in uow.ORDENPRODUCTO
                                 where f.NUMORDEN == numOrden && f.NROSERIE != null
                                 select f.NROSERIE).ToList().FirstOrDefault();

            var HasBoxes = uow.Database.SqlQuery<int?>(string.Format("SELECT app.cj_CajaFromNroSerie('{0}') as idcaja FROM dual", FirstNumSerie)).FirstOrDefault();

            if (HasBoxes != null)
                devicesBox = uow.Database.SqlQuery<DeviceDataView>(string.Format(
                    @"SELECT op.NumOrden, op.NumFabricacion, op.NumMatricula, op.NroSerie, op.FechaMat,
                        Cast(substr(app.cj_CajaOrdenFromNroSerie(op.NroSerie), 1, instr(app.cj_CajaOrdenFromNroSerie (op.NroSerie), '/') - 1) as Integer)  as NumCaja,
                        app.cj_CajaFromNroSerie (op.NroSerie) as idcaja,
                        o.NumProducto,
                        o.Version,
                        a.Descripcion
                FROM app.OrdenProducto op,
                        app.Orden o,
                        app.Articulo a
                WHERE op.NumOrden={0} and
                      o.NumOrden=op.NumOrden and
                      a.NumArticulo=o.NumProducto and
                      op.NroSerie IS NOT NULL"
                    , numOrden)).ToList();
            else
                devicesBox = uow.Database.SqlQuery<DeviceDataView>(string.Format(
                    @"SELECT op.NumOrden, op.NumFabricacion, op.NumMatricula, op.NroSerie, op.FechaMat,
                        1 as NumCaja,
                        1 as idcaja,
                        o.NumProducto,
                        o.Version,
                        a.Descripcion
                FROM app.OrdenProducto op,
                        app.Orden o,
                        app.Articulo a
                WHERE op.NumOrden={0} and
                      o.NumOrden=op.NumOrden and
                      a.NumArticulo=o.NumProducto and
                      op.NroSerie IS NOT NULL"
                    , numOrden)).ToList();

            return devicesBox;
        }

        public List<BoxesView> GetBoxes(int numOrden)
        {
            var boxes = uow.Database.SqlQuery<BoxesView>(string.Format(
            @"select Cast(substr(c.NumCaja, 0, instr(c.NumCaja, '/') -1) as Integer) as NumCaja,c.desdeNroSerie,c.hastanroSerie,c.NumeroCaja,app.cj_testCaja(c.NumeroCaja) as TestCaja,c.Equipos,
            c2.UdsCaja
            from (select c.NumCaja,c.desdeNroSerie,c.hastaNroSerie,app.cj_CajaFromNroSerie (c.desdeNroSerie) as NumeroCaja,
            c.Equipos
                from (select c.NumCaja,
                       min(c.NroSerie) as desdeNroSerie,
                       max(c.NroSerie) as hastaNroSerie,
                       count(*) as Equipos
                        from (select app.cj_CajaOrdenFromNroSerie (op.NroSerie) as NumCaja, op.NroSerie as NroSerie
                                from app.OrdenProducto op
                                where op.NumOrden={0} and
                                op.NroSerie is not null
                              ) c
                 group by c.NumCaja
               ) c
         ) c,
        app.Caja c2
        where c2.NumCaja=c.NumeroCaja"
                , numOrden)).ToList();

            return boxes;
        }

        public void Dispose()
        {
            if (uow != null)
                uow.Dispose();
        }

    }
}
