﻿using Instruments.Router;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(ChangeRouterWifiDescription))]
    
    public class ChangeRouterWifi : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del usuario en el router")]
        [DefaultValue("root")]
        public string UserName { get; set; }

        [Category("3. Configuration")]
        [Description("Contraseña")]
        [DefaultValue("dezac2309")]
        public string Password { get; set; }

        [Category("3. Configuration")]
        [Description("Habilita el Wifi")]
        [DefaultValue(false)]
        public bool EnabledWifi { get; set; }

        [Category("3. Configuration")]
        [Description("Identificador del router para redes inalámbricas")]
        [DefaultValue("")]
        public string SSID { get; set; }

        [Category("3. Configuration")]
        [Description("Contraseña del Wifi")]
        [DefaultValue("")]
        public string passwordWIFI { get; set; }

        public override void Execute()
        {

            Router router = Router.Default();

            if (router == null)
                throw new System.Exception("Error, no se ha encontrado router DDWRT o router DLink810 conectado a este PC");

            if (!string.IsNullOrEmpty(UserName))
            {
                router.UserName = UserName;
                router.Password = Password;
            }

            var config = new RouterConfigWifi
            {
                Enabled = EnabledWifi,
                SSID = SSID,
                Password = passwordWIFI
            };

            var newConfig = router.ApplyConfigWifi(config);

            Assert.AreEqual(newConfig.Enabled, EnabledWifi, Error().SOFTWARE.ERROR_TRANSFERENCIA.ERROR_GRABACION("Error no se ha cambiando la configuracion WIFI del router"));

            Assert.AreEqual(newConfig.SSID, SSID, Error().SOFTWARE.ERROR_TRANSFERENCIA.ERROR_GRABACION("Error no se ha cambiando la configuracion WIFI del router"));
        }
    }

    public class ChangeRouterWifiDescription : ActionDescription
    {
        public override string Name { get { return "ChangeRouterWIFI"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override string Description { get { return "Action para cambiar de router"; } }
        public override Image Icon { get { return Properties.Resources.router_icon; } }
    }
}
