﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Services;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.UserControls;
using RTF;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using TaskRunner.Model;
using TaskRunner.Enumerators;
using WinTestPlayer.Properties;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class TestResultView : DialogViewBase
    {
        public TestResultView(RunnerTestsInfo e)
        {
            InitializeComponent();

            pboxResult.Image = Resources.Ok_icon;
            lblResult.Text = "TEST OK";

            BuildMessage(e);
        }

        private void BuildMessage(RunnerTestsInfo e)
        {
            var sb = new RTFBuilder();

            sb.FontSize(35).FontStyle(FontStyle.Regular).Font(RTFFont.Impact).Alignment(StringAlignment.Center);
            sb.AppendLine();

            foreach (var test in e.Results.OrderBy(p => p.NumInstance))
            {
                bool HasError = false;

                if (test.ExecutionResult == TestExecutionResult.Completed)
                {
                    foreach (StepResult step in test.StepResults)
                    {
                        if (!string.IsNullOrEmpty(step.GetErrorMessage()))
                        {
                            ResultError(sb, test, false);
                            HasError = true;
                        }
                    }

                    if (!HasError)
                        ResultOk(sb, test);
                }
                else
                    ResultError(sb, test, test.ExecutionResult == TestExecutionResult.Aborted);
            }

            lblResultDescription.Rtf = sb.ToString();
        }

        private void ResultOk(RTFBuilder sb, TestResult test)
        {
            sb.FontSize(35).FontStyle(FontStyle.Regular).Font(RTFFont.Impact).Alignment(StringAlignment.Center);

            sb.AppendLine();
            sb.AppendFormat("--- TEST {0} finalizado satisfactoriamente ---", test.NumInstance);
            sb.AppendLine();
            sb.AppendLine();
        }

        private void ResultError(RTFBuilder sb, TestResult test, bool aborted)
        {
            lblResult.Text = aborted ? "TEST CANCELADO" : "TEST ERROR";
            pboxResult.Image = Resources.ErrorIcon;

            sb.FontSize(35).FontStyle(FontStyle.Regular).Font(RTFFont.Impact).Alignment(StringAlignment.Center);

            if (aborted)
                sb.AppendLineFormat("--- TEST {0} CANCELADO POR EL USUARIO ---", test.NumInstance);
            else
                sb.AppendLineFormat("--- TEST {0} CON ERRORES!! ---", test.NumInstance);

            sb.AppendLine();

            var data = test.Context.Services.Get<ITestContext>();
            var stepWithExceptions = test.StepResults.Where(p => p.Exception != null);
            foreach (StepResult sr in stepWithExceptions)
            {
                var error = sr.Exception as SauronException;

                sb.FontSize(20).FontStyle(FontStyle.Bold).Font(RTFFont.ArialBlack).Append("Función: ").Append(sr.Name);
                sb.AppendLine();
                sb.AppendLine();

                sb.FontSize(20).FontStyle(FontStyle.Bold).Font(RTFFont.ArialBlack).Append("Error: ").Append($"{sr.GetErrorMessage()}");
                sb.AppendLine();
            }
        }

        private void btloggerNew_Click(object sender, EventArgs e)
        {
            LogService.ShowLog(FormatLogViewer.XSLFilesEnum.DebugLog, false, null, false);
        }

        private void btChekSteps_Click(object sender, EventArgs e)
        {
            LogService.ShowLog(FormatLogViewer.XSLFilesEnum.CheckReport, false, null, false);
        }
    }
}
