﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Entities.ViewModels
{
    public class TestView
    {
        public int NUMTEST { get; set; }
        public int NUMTESTFASE { get; set; }
        //public Nullable<int> NUMFABRICACION { get; set; }
        public int NUMORDEN { get; set; }
        public System.DateTime FECHA { get; set; }
        public Nullable<int> TIEMPO { get; set; }
        public string RESULTADO { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
        public Nullable<int> NUMFAMILIA { get; set; }
        public Nullable<int> NUMPRODUCTO { get; set; }
        public Nullable<int> VERSION { get; set; }
        public string DESCRIPCION { get; set; }

        public string NROSERIE { get; set; }
        public int? NUMCAJA { get; set; }

        public string IDFASE { get; set; }
        public string FASE { get; set; }
        public string IDOPERARIO { get; set; }

        public IEnumerable<T_TESTERROR> Errors { get; set; }
        public IEnumerable<TestValorView> Results { get; set; }

        public class TestValorView
        {
            public string Parametro { get; set; }
            public string Valor { get; set; }
            public string Funcion { get; set; }
            public Nullable<decimal> ValorMax { get; set; }
            public Nullable<decimal> ValorMin { get; set; }
            public string ValorEsperado { get; set; }
        }
    }

    public class TestIndicators
    {
        public double NumOK { get; set; }
        public double NumError { get; set; }
        public double NumFPY { get; set; }
        public double NumTiempoMedio { get; set; }
    }

    public class IndicatorsDescriptions
    {
        public string Descripcion { get; set; }
        public double Valor { get; set; }
    }

    public class TestOrdenProductoResult
    {
        public DateTime? Fecha { get; set; }
        public int? IdBastidor { get; set; }
        public string NumSerie { get; set; }
        public string PuntoError { get; set; }
        public string MsgError { get; set; }
        public int? NumCaja { get; set; }
    }

    public class LaserItemViewModel
    {
        public string DescripcionArticulo { get; set; }
        public string CodigoBcn { get; set; }
        public string DescripcionIT { get; set; }
        public DateTime? FechaPdf { get; set; }
        public byte[] DocPdf { get; set; }
        public Int32 NumBien { get; set; }
        public string Codigo { get; set; }
        public string descripcion { get; set; }
        public byte[] Imagen { get; set; }
    }

    public class BinaryItemViewModel
    {
        public string DescripcionArticulo { get; set; }
        public string CodigoBcn { get; set; }
        public string versionBinario { get; set; }
        public string Tipofichero { get; set; }
        public string NombreFichero { get; set; }
        public byte[] Fichero { get; set; }
        public string PathFicheroTest { get; set; }
    }

    public class ComponentsEstructureInProduct
    {
        public int numproducto { get; set; }
        public int version { get; set; }
        public string descripcion { get; set; }
        public int matricula { get; set; }
        public int cantidad { get; set; }
    }

    public enum TestRepeatStatus
    {
        PASS,
        REPEAT,
        NOT_PASS
    }
}
