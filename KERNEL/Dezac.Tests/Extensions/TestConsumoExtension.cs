﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Towers;
using System;

namespace Dezac.Tests.Extensions
{
    public struct TestPointConfiguration
    {
        public TypePowerSource typePowerSource;
        public TypeTestExecute typeTestExecute;
        public TypeTestVoltage typeTestVoltage;
        public TypeTestPoint typeTestPoint;
        public string ResultName;
        public double Voltage;
        public double Current;
        public double Frecuency;
        public int WaitTimeReadMeasure;
        public int TiempoIntervalo;
        public int Iteraciones;
        public int DeleteFirst;
        public MeasureMagnitud MagnitudToMeasure;
        public int offset;
        public int puntosMuestreo;
        public double rangoCorriente;
        public double limiteSuperiorCorriente;
        public double limiteInferiorCorriente;
    }

    public enum TypeTestExecute
    {
        ActiveAndReactive,
        OnlyActive,
        OnlyReactive,
        Aparente,
        ActiveAndAparent
    }

    public enum TypeTestVoltage
    {
        VoltageMaximo,
        VoltageMinimo,
        VoltageNominal,
    }

    public enum TypeTestPoint
    {
        ConCarga,
        EnVacio,
    }

    public enum TypePowerSource
    {
        CHROMA,
        PTE,
        POWER_SOURCE_III,
        LAMBDA
    }

    public enum MeasureMagnitud
    {
        Current,
        Voltage
    }

    public static class TestConsumoExtension
    {  
        public static void TestConsumo(this TestBase testBase, ITower tower, TestPointConfiguration testPointConfiguration, Action preCondition=null)
         {
            var testPoinrParameters = TestPointConsumConsignas(testBase, testPointConfiguration.typeTestVoltage, testPointConfiguration.typeTestPoint, testPointConfiguration.ResultName);
            var Voltage = testBase.Consignas.GetDouble(testPoinrParameters.VoltageConsigna, testPointConfiguration.Voltage, ParamUnidad.V);
            var Current = testBase.Consignas.GetDouble(testPoinrParameters.CurrentConsigna, testPointConfiguration.Current, ParamUnidad.A);
            var Frecuency = testBase.Consignas.GetDouble(testPoinrParameters.FrecuencyConsigna, testPointConfiguration.Frecuency, ParamUnidad.Hz);

            CVMMINITower cvm = null;

            switch (testPointConfiguration.typePowerSource)
            {
                case TypePowerSource.CHROMA:
                    if (Voltage > 300)
                    {
                        Voltage /= 2;
                        tower.IO.DO.On(13);
                    }
                    tower.Chroma.ApplyPresetsAndWaitStabilisation(Voltage, Current, Frecuency);        
                    cvm = tower.CvmminiChroma;
                    break;
                case TypePowerSource.LAMBDA:
                    tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Voltage, Current);
                    break;
                case TypePowerSource.PTE:
                    tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;
                    tower.PTE.ApplyPresets(Voltage, 0,0);
                    cvm = tower.CvmminiChroma;
                    break;
                case TypePowerSource.POWER_SOURCE_III:
                    tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(new TriLineValue { L1 = Voltage, L2 = 0, L3 = 0 }, 0, Frecuency, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, true);
                    cvm = tower.CvmminiMTE;
                    break;
            }

            testBase.Delay(testPointConfiguration.WaitTimeReadMeasure, "Espera antes de hacer la lectura del consumo");

            if (testPointConfiguration.typePowerSource == TypePowerSource.LAMBDA)
            {
                var testPoinrParametersMargen = TestPointConsumMargenes("I_DC", ParamUnidad.A, testBase, testPointConfiguration.typeTestVoltage, testPointConfiguration.typeTestPoint, testPointConfiguration.ResultName);
                testBase.TestConsumo(testPoinrParametersMargen.NameTestPoint, testPoinrParametersMargen.MinTestPoint, testPoinrParametersMargen.MaxTestPoint,
                    () => tower.LAMBDA.ReadCurrent(), "Error consumo de corriente DC fuera de margenes", testPointConfiguration.TiempoIntervalo, testPointConfiguration.Iteraciones, ParamUnidad.A);
            }
            else
            {
                if (testPointConfiguration.typeTestExecute == TypeTestExecute.Aparente || testPointConfiguration.typeTestExecute == TypeTestExecute.ActiveAndAparent)
                {
                    var testPoinrParametersMargen = TestPointConsumMargenes("VA", ParamUnidad.VA, testBase, testPointConfiguration.typeTestVoltage, testPointConfiguration.typeTestPoint, testPointConfiguration.ResultName);
                    testBase.TestConsumo(testPoinrParametersMargen.NameTestPoint, testPoinrParametersMargen.MinTestPoint, testPoinrParametersMargen.MaxTestPoint,
                        () => cvm.ReadAllVariables().Phases.L1.PotenciaAparente, "Error Consumo Pot. Aparente fuera de margenes", testPointConfiguration.TiempoIntervalo, testPointConfiguration.Iteraciones, ParamUnidad.VA);

                    if (testPointConfiguration.typeTestExecute == TypeTestExecute.ActiveAndAparent)
                    {
                        testPoinrParametersMargen = TestPointConsumMargenes("KW", ParamUnidad.W, testBase, testPointConfiguration.typeTestVoltage, testPointConfiguration.typeTestPoint, testPointConfiguration.ResultName);
                        testBase.TestConsumo(testPoinrParametersMargen.NameTestPoint, testPoinrParametersMargen.MinTestPoint, testPoinrParametersMargen.MaxTestPoint,
                            () => cvm.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa fuera de margenes", testPointConfiguration.TiempoIntervalo, testPointConfiguration.Iteraciones, ParamUnidad.W);
                    }
                }
                
                else
                {
                    if (testPointConfiguration.typeTestExecute == TypeTestExecute.ActiveAndReactive || testPointConfiguration.typeTestExecute == TypeTestExecute.OnlyActive)
                    {
                        var testPoinrParametersMargen = TestPointConsumMargenes("KW", ParamUnidad.W, testBase, testPointConfiguration.typeTestVoltage, testPointConfiguration.typeTestPoint, testPointConfiguration.ResultName);
                        testBase.TestConsumo(testPoinrParametersMargen.NameTestPoint, testPoinrParametersMargen.MinTestPoint, testPoinrParametersMargen.MaxTestPoint,
                            () => cvm.ReadAllVariables().Phases.L1.PotenciaActiva, "Error Consumo Pot. Activa fuera de margenes", testPointConfiguration.TiempoIntervalo, testPointConfiguration.Iteraciones, ParamUnidad.W);
                    }

                    if (testPointConfiguration.typeTestExecute == TypeTestExecute.ActiveAndReactive || testPointConfiguration.typeTestExecute == TypeTestExecute.OnlyReactive)
                    {
                        var testPoinrParametersMargen = TestPointConsumMargenes("KVAR", ParamUnidad.Var, testBase, testPointConfiguration.typeTestVoltage, testPointConfiguration.typeTestPoint, testPointConfiguration.ResultName);
                        testBase.TestConsumo(testPoinrParametersMargen.NameTestPoint, testPoinrParametersMargen.MinTestPoint, testPoinrParametersMargen.MaxTestPoint,
                            () => cvm.ReadAllVariables().Phases.L1.PotenciaReactiva, "Error Consumo Pot. Reactiva fuera de margenes", testPointConfiguration.TiempoIntervalo, testPointConfiguration.Iteraciones, ParamUnidad.Var);
                    }
                }
            }
        }

        //public static void TestConsumo(this TestBase testBase, ITower tower, TestPointConfiguration testPointConfiguration, bool OnlyReadMeasure)
        //{
        //    string name = "";
        //    double min = 0;
        //    double max = 0;
        //    ParamUnidad unidad = ParamUnidad.SinUnidad;

        //    switch (testPointConfiguration.MagnitudToMeasure)
        //    {
        //        case MeasureMagnitud.Current:
        //            name = testBase.Params.I_DC.Null.TestPoint(testPointConfiguration.ResultName).Name;
        //            min = testBase.Params.I_DC.Null.TestPoint(testPointConfiguration.ResultName).Min();
        //            max = testBase.Params.I_DC.Null.TestPoint(testPointConfiguration.ResultName).Max();
        //            unidad = ParamUnidad.mA;
        //            break;

        //        case MeasureMagnitud.Voltage:
        //            name = testBase.Params.V.Null.TestPoint(testPointConfiguration.ResultName).Name;
        //            min = testBase.Params.V_DC.Null.TestPoint(testPointConfiguration.ResultName).Min();
        //            max = testBase.Params.V_DC.Null.TestPoint(testPointConfiguration.ResultName).Max();
        //            unidad = ParamUnidad.V;
        //            break;
        //    }

        //    var adjustValueTestPoint = new AdjustValueDef(name, 0, min, max, 0, 0, unidad);

        //    if (!tower.PowerSourceM9111A.IsConnected)
        //        tower.PowerSourceM9111A.Connect();

        //    if (!OnlyReadMeasure)
        //        tower.PowerSourceM9111A.ApplyVoltage(testPointConfiguration.Voltage, testPointConfiguration.limiteSuperiorCorriente, testPointConfiguration.limiteInferiorCorriente);

        //    testBase.TestMeasureBase(adjustValueTestPoint, 
        //    (step) =>
        //    {
        //        var result = tower.PowerSourceM9111A.ReadMeasure(testPointConfiguration.MagnitudToMeasure.ToString(), testPointConfiguration.offset, testPointConfiguration.puntosMuestreo, testPointConfiguration.rangoCorriente);
        //        return result;

        //    }, testPointConfiguration.DeleteFirst, testPointConfiguration.Iteraciones, testPointConfiguration.WaitTimeReadMeasure);      
        //}

        private struct ParametersConsumConsignas
         {
             public string VoltageConsigna;
             public string CurrentConsigna;
             public string FrecuencyConsigna;
         }

        private static ParametersConsumConsignas TestPointConsumConsignas(TestBase testBase, TypeTestVoltage typeTestConsumo, TypeTestPoint typeTestPoint, string resultName = "")
         {
             var ParametersConsum = new ParametersConsumConsignas();
             var testPoint = internalSelectionTesPoint(typeTestConsumo, typeTestPoint);

             ParametersConsum.CurrentConsigna = testBase.Params.I.Other(resultName).TestPoint(testPoint.Item1).TestPoint(testPoint.Item2).Name;
             ParametersConsum.VoltageConsigna = testBase.Params.V.Other(resultName).TestPoint(testPoint.Item1).TestPoint(testPoint.Item2).Name;
             ParametersConsum.FrecuencyConsigna = testBase.Params.FREQ.Other(resultName).TestPoint(testPoint.Item1).TestPoint(testPoint.Item2).Name;
             return ParametersConsum;
         }

        private struct ParametersConsumMargenes
         {
             public string NameTestPoint;
             public double MaxTestPoint;
             public double MinTestPoint;
         }

        private static ParametersConsumMargenes TestPointConsumMargenes(string parameter, ParamUnidad unidad, TestBase testBase, TypeTestVoltage typeTestConsumo, TypeTestPoint typeTestPoint, string resultName = "")
         {
             var ParametersConsum = new ParametersConsumMargenes();
             var testPoint = internalSelectionTesPoint(typeTestConsumo, typeTestPoint);

             ParametersConsum.NameTestPoint = testBase.Params.PARAMETER(parameter, unidad).Other(resultName).TestPoint(testPoint.Item1).TestPoint(testPoint.Item2).Name;
             ParametersConsum.MinTestPoint = testBase.Params.PARAMETER(parameter, unidad).Other(resultName).TestPoint(testPoint.Item1).TestPoint(testPoint.Item2).Min();
             ParametersConsum.MaxTestPoint = testBase.Params.PARAMETER(parameter, unidad).Other(resultName).TestPoint(testPoint.Item1).TestPoint(testPoint.Item2).Max();
             return ParametersConsum;
         }

        private static Tuple<string, string> internalSelectionTesPoint(TypeTestVoltage typeTestConsumo, TypeTestPoint typeTestPoint)
         {
             string testVoltage = "";
             string testPoint = "";

             switch (typeTestConsumo)
             {
                 case TypeTestVoltage.VoltageMinimo:
                     testVoltage = "VMIN";
                     break;
                 case TypeTestVoltage.VoltageMaximo:
                     testVoltage = "VMAX";
                     break;
                 case TypeTestVoltage.VoltageNominal:
                     testVoltage = "VNOM";
                     break;
             }

             switch (typeTestPoint)
             {
                 case TypeTestPoint.ConCarga:
                     testPoint = "CARGA";
                     break;
                 case TypeTestPoint.EnVacio:
                     testPoint = "VACIO";
                     break;
             }

             return Tuple.Create<string, string>(testPoint, testVoltage);
         }
    }
}
