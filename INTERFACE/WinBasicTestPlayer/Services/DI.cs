﻿using Autofac;
using Dezac.Tests.Services;
using System.Configuration;
using TaskRunner.Model;
using WinTestPlayer.Data;
using WinTestPlayer.Views;

namespace WinTestPlayer.Services
{
    public class DI
    {
        public static IContainer Container { get; private set; }

        public static IContainer Build()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<AssemblyResolverService>().SingleInstance();
            builder.RegisterType<SequenceContext>();
            builder.RegisterType<RunnerService>().SingleInstance();
            builder.RegisterType<ShellService>().SingleInstance();
            builder.RegisterType<AppService>().SingleInstance();
            builder.RegisterType<Header>().SingleInstance();
            builder.RegisterType<TestView>().SingleInstance();
            builder.RegisterType<MainForm>().SingleInstance();
            builder.RegisterType<CacheService>().As<ICacheService>().SingleInstance();

            var offline = ConfigurationManager.AppSettings["HasDatabase"];
            if (offline == "SI")
                builder.RegisterType<TestModelSQLServer>().As<ITestModelDataBaseService>();
            //else
            //    builder.RegisterType<NoDbDezacService>().As<IDezacService>();

            Container = builder.Build();

            return Container;
        }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
