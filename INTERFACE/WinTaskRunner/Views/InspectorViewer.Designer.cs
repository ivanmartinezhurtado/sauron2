﻿namespace WinTaskRunner.Views
{
    partial class InspectorViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn2 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridMethods = new Telerik.WinControls.UI.RadGridView();
            this.bsList = new System.Windows.Forms.BindingSource(this.components);
            this.gridResults = new Telerik.WinControls.UI.RadGridView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.radButtonClose = new Telerik.WinControls.UI.RadButton();
            this.radButtonLoad = new Telerik.WinControls.UI.RadButton();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDevice = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxDivision = new System.Windows.Forms.ComboBox();
            this.btStart = new Telerik.WinControls.UI.RadButton();
            this.chkRepetir = new System.Windows.Forms.CheckBox();
            this.nInterval = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMethods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMethods.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 843);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridMethods);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridResults);
            this.splitContainer1.Size = new System.Drawing.Size(1397, 772);
            this.splitContainer1.SplitterDistance = 815;
            this.splitContainer1.TabIndex = 7;
            // 
            // gridMethods
            // 
            this.gridMethods.BackColor = System.Drawing.SystemColors.Control;
            this.gridMethods.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridMethods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMethods.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.gridMethods.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridMethods.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.gridMethods.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridMethods.MasterTemplate.AllowAddNewRow = false;
            this.gridMethods.MasterTemplate.AllowSearchRow = true;
            this.gridMethods.MasterTemplate.AutoGenerateColumns = false;
            this.gridMethods.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewCheckBoxColumn1.EnableExpressionEditor = false;
            gridViewCheckBoxColumn1.FieldName = "Selected";
            gridViewCheckBoxColumn1.HeaderText = "Selected";
            gridViewCheckBoxColumn1.IsAutoGenerated = true;
            gridViewCheckBoxColumn1.MaxWidth = 70;
            gridViewCheckBoxColumn1.MinWidth = 20;
            gridViewCheckBoxColumn1.Name = "SSelected";
            gridViewCheckBoxColumn1.Width = 60;
            gridViewDecimalColumn1.DataType = typeof(System.Nullable<int>);
            gridViewDecimalColumn1.EnableExpressionEditor = false;
            gridViewDecimalColumn1.FieldName = "Orden";
            gridViewDecimalColumn1.HeaderText = "Orden";
            gridViewDecimalColumn1.IsAutoGenerated = true;
            gridViewDecimalColumn1.MaxWidth = 70;
            gridViewDecimalColumn1.Name = "SOrden";
            gridViewDecimalColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewDecimalColumn1.Width = 60;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "Name";
            gridViewTextBoxColumn1.HeaderText = "Name";
            gridViewTextBoxColumn1.Name = "SName";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 232;
            gridViewCommandColumn1.EnableExpressionEditor = false;
            gridViewCommandColumn1.FieldName = "Args";
            gridViewCommandColumn1.HeaderText = "Args";
            gridViewCommandColumn1.Name = "SArgs";
            gridViewCommandColumn1.Width = 178;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "Value";
            gridViewTextBoxColumn2.HeaderText = "Value";
            gridViewTextBoxColumn2.Name = "SValue";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 220;
            gridViewCommandColumn2.HeaderText = "Send";
            gridViewCommandColumn2.Image = global::WinTaskRunner.Properties.Resources.goto16;
            gridViewCommandColumn2.Name = "Send";
            this.gridMethods.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewDecimalColumn1,
            gridViewTextBoxColumn1,
            gridViewCommandColumn1,
            gridViewTextBoxColumn2,
            gridViewCommandColumn2});
            this.gridMethods.MasterTemplate.DataSource = this.bsList;
            this.gridMethods.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.PropertyName = "SOrden";
            this.gridMethods.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.gridMethods.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridMethods.Name = "gridMethods";
            this.gridMethods.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.gridMethods.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 240, 150);
            this.gridMethods.Size = new System.Drawing.Size(815, 772);
            this.gridMethods.TabIndex = 2;
            this.gridMethods.Text = "radGridView1";
            this.gridMethods.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.gridMethods_RowFormatting);
            this.gridMethods.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.gridMethods_CommandCellClick);
            // 
            // bsList
            // 
            this.bsList.DataSource = typeof(WinTaskRunner.Views.InspectorItem);
            this.bsList.Sort = "";
            this.bsList.CurrentChanged += new System.EventHandler(this.bsList_CurrentChanged);
            // 
            // gridResults
            // 
            this.gridResults.BackColor = System.Drawing.SystemColors.Control;
            this.gridResults.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResults.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.gridResults.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridResults.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.gridResults.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridResults.MasterTemplate.AllowAddNewRow = false;
            this.gridResults.MasterTemplate.AllowColumnChooser = false;
            this.gridResults.MasterTemplate.AllowColumnReorder = false;
            this.gridResults.MasterTemplate.AllowColumnResize = false;
            this.gridResults.MasterTemplate.AllowDeleteRow = false;
            this.gridResults.MasterTemplate.AllowDragToGroup = false;
            this.gridResults.MasterTemplate.AllowEditRow = false;
            this.gridResults.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.gridResults.MasterTemplate.EnableGrouping = false;
            this.gridResults.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridResults.Name = "gridResults";
            this.gridResults.ReadOnly = true;
            this.gridResults.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.gridResults.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 240, 150);
            this.gridResults.Size = new System.Drawing.Size(578, 772);
            this.gridResults.TabIndex = 2;
            this.gridResults.Text = "radGridView1";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.radButtonClose);
            this.splitContainer2.Panel1.Controls.Add(this.radButtonLoad);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.comboBoxDevice);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.comboBoxDivision);
            this.splitContainer2.Panel1.Controls.Add(this.btStart);
            this.splitContainer2.Panel1.Controls.Add(this.chkRepetir);
            this.splitContainer2.Panel1.Controls.Add(this.nInterval);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer2.Size = new System.Drawing.Size(1397, 843);
            this.splitContainer2.SplitterDistance = 67;
            this.splitContainer2.TabIndex = 10;
            // 
            // radButtonClose
            // 
            this.radButtonClose.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButtonClose.Image = global::WinTaskRunner.Properties.Resources.Action_exit_icon;
            this.radButtonClose.Location = new System.Drawing.Point(477, 12);
            this.radButtonClose.Name = "radButtonClose";
            // 
            // 
            // 
            this.radButtonClose.RootElement.ControlBounds = new System.Drawing.Rectangle(477, 12, 110, 24);
            this.radButtonClose.Size = new System.Drawing.Size(89, 40);
            this.radButtonClose.TabIndex = 22;
            this.radButtonClose.Text = "    Close";
            this.radButtonClose.Visible = false;
            this.radButtonClose.Click += new System.EventHandler(this.RadButtonClose_Click);
            // 
            // radButtonLoad
            // 
            this.radButtonLoad.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButtonLoad.Image = global::WinTaskRunner.Properties.Resources.printpreview32;
            this.radButtonLoad.Location = new System.Drawing.Point(382, 12);
            this.radButtonLoad.Name = "radButtonLoad";
            // 
            // 
            // 
            this.radButtonLoad.RootElement.ControlBounds = new System.Drawing.Rectangle(382, 12, 110, 24);
            this.radButtonLoad.Size = new System.Drawing.Size(89, 40);
            this.radButtonLoad.TabIndex = 17;
            this.radButtonLoad.Text = "    Load";
            this.radButtonLoad.Visible = false;
            this.radButtonLoad.Click += new System.EventHandler(this.RadButtonLoad_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(206, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Equipo:";
            this.label3.Visible = false;
            // 
            // comboBoxDevice
            // 
            this.comboBoxDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDevice.FormattingEnabled = true;
            this.comboBoxDevice.Location = new System.Drawing.Point(206, 31);
            this.comboBoxDevice.Name = "comboBoxDevice";
            this.comboBoxDevice.Size = new System.Drawing.Size(148, 21);
            this.comboBoxDevice.TabIndex = 20;
            this.comboBoxDevice.Visible = false;
            this.comboBoxDevice.SelectedValueChanged += new System.EventHandler(this.ComboBoxDevice_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Division:";
            this.label2.Visible = false;
            // 
            // comboBoxDivision
            // 
            this.comboBoxDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDivision.FormattingEnabled = true;
            this.comboBoxDivision.Items.AddRange(new object[] {
            "MEDIDA",
            "PROTECCION",
            "METERING",
            "REACTIVA",
            "RENOVABLES",
            "MADEL",
            "DEVICE GENERATED",
            "EXAMINAR ..."});
            this.comboBoxDivision.Location = new System.Drawing.Point(15, 31);
            this.comboBoxDivision.Name = "comboBoxDivision";
            this.comboBoxDivision.Size = new System.Drawing.Size(148, 21);
            this.comboBoxDivision.TabIndex = 18;
            this.comboBoxDivision.Visible = false;
            this.comboBoxDivision.SelectedValueChanged += new System.EventHandler(this.ComboBoxDivision_SelectedValueChanged);
            // 
            // btStart
            // 
            this.btStart.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btStart.Image = global::WinTaskRunner.Properties.Resources.Play_icon__1_;
            this.btStart.Location = new System.Drawing.Point(726, 12);
            this.btStart.Name = "btStart";
            // 
            // 
            // 
            this.btStart.RootElement.ControlBounds = new System.Drawing.Rectangle(726, 12, 110, 24);
            this.btStart.Size = new System.Drawing.Size(89, 40);
            this.btStart.TabIndex = 16;
            this.btStart.Text = "    Start";
            this.btStart.Click += new System.EventHandler(this.BtStart_Click_1);
            // 
            // chkRepetir
            // 
            this.chkRepetir.AutoSize = true;
            this.chkRepetir.Location = new System.Drawing.Point(849, 35);
            this.chkRepetir.Name = "chkRepetir";
            this.chkRepetir.Size = new System.Drawing.Size(60, 17);
            this.chkRepetir.TabIndex = 17;
            this.chkRepetir.Text = "Repetir";
            this.chkRepetir.UseVisualStyleBackColor = true;
            // 
            // nInterval
            // 
            this.nInterval.Location = new System.Drawing.Point(915, 32);
            this.nInterval.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nInterval.Name = "nInterval";
            this.nInterval.Size = new System.Drawing.Size(77, 20);
            this.nInterval.TabIndex = 14;
            this.nInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nInterval.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(919, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Intervalo (ms):";
            // 
            // InspectorViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.splitter1);
            this.Name = "InspectorViewer";
            this.Size = new System.Drawing.Size(1400, 843);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMethods.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMethods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButtonClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInterval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource bsList;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Telerik.WinControls.UI.RadGridView gridMethods;
        private Telerik.WinControls.UI.RadGridView gridResults;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Telerik.WinControls.UI.RadButton btStart;
        private System.Windows.Forms.CheckBox chkRepetir;
        private System.Windows.Forms.NumericUpDown nInterval;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton radButtonLoad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDevice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxDivision;
        private Telerik.WinControls.UI.RadButton radButtonClose;
    }
}
