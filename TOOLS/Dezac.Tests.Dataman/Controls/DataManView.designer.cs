﻿namespace Dezac.Tests.Dataman.Controls
{
    partial class DataManView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gLog = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.flow = new System.Windows.Forms.FlowLayoutPanel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.gLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // gLog
            // 
            this.gLog.BackColor = System.Drawing.SystemColors.Control;
            this.gLog.Controls.Add(this.txtLog);
            this.gLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gLog.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gLog.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gLog.Location = new System.Drawing.Point(0, 431);
            this.gLog.Name = "gLog";
            this.gLog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gLog.Size = new System.Drawing.Size(653, 143);
            this.gLog.TabIndex = 49;
            this.gLog.TabStop = false;
            this.gLog.Text = "Log window";
            // 
            // txtLog
            // 
            this.txtLog.AcceptsReturn = true;
            this.txtLog.BackColor = System.Drawing.SystemColors.Window;
            this.txtLog.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtLog.Location = new System.Drawing.Point(3, 16);
            this.txtLog.MaxLength = 0;
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(647, 124);
            this.txtLog.TabIndex = 49;
            this.txtLog.WordWrap = false;
            // 
            // flow
            // 
            this.flow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flow.Location = new System.Drawing.Point(0, 0);
            this.flow.Name = "flow";
            this.flow.Size = new System.Drawing.Size(653, 431);
            this.flow.TabIndex = 75;
            // 
            // timer
            // 
            this.timer.Interval = 750;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // DataManView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 574);
            this.ControlBox = false;
            this.Controls.Add(this.flow);
            this.Controls.Add(this.gLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DataManView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Programador Dataman";
            this.gLog.ResumeLayout(false);
            this.gLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.GroupBox gLog;
        public System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.FlowLayoutPanel flow;
        private System.Windows.Forms.Timer timer;
    }
}
