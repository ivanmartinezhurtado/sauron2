﻿using System.Globalization;

namespace Comunications.Message
{
    public class SCPIMessage : StringMessage
    {
        private static CultureInfo enUS = new CultureInfo("en-US");
        private readonly char COMMAND_SEPARATOR = ';';

        public static SCPIMessage Create(string format, params object[] args)
        {
            SCPIMessage message = new SCPIMessage { Format = format };
            message.SetArgs(enUS, args);

            return message;
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                HasResponse = value != null && value.IndexOf("?") >= 0;
            }
        }

        public int NumCommands
        {
            get { return this.Text.Split(COMMAND_SEPARATOR).Length; }
        }

        public string[] Commands
        {
            get
            {
                return this.Text.Split(COMMAND_SEPARATOR);
            }
        }
    }
}
