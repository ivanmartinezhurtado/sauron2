﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TaskRunner.Model;

namespace Dezac.Tests
{
    public class TestBase : TestClass
    {
        public TestBase()
           : this("TEST_UUT")
        {
        }

        public TestBase(string testName)
        {
            if (SequenceContext.Current != null)
                if (!string.IsNullOrEmpty(testName))
                    AddInstanceVar(this, testName);
        }

        public void CheckCancellationRequested()
        {
            if (SequenceContext.Current.IsCancellationRequested)
                TestException.Create().SOFTWARE.SAURON.PARALELISMO_CANCELADO().Throw();
        }

        public TestException Error()
        {
            return TestException.Create();
        }

        //-------------------------------------------------------------------------

        public void Delay(int milliseconds, string message)
        {
            logger.DebugFormat("Espera de {0} ms: \"{1}\" ", milliseconds, message);
            Thread.Sleep(milliseconds);
        }

        //-------------------------------------------------------------------------

        public void SamplerWithCancelWhitOutCancelToken(Func<bool> valueFunc, string errMessage, byte numIterations = 3, int interval = 500, int initialDelay = 0, bool cancelOnException = true, bool throwException = true, Action<string> exception = null)
        {
            SauronException exceptionCach = null;

            var sampler = Sampler.Run(
               () => { return new SamplerConfig { Interval = interval, InitialDelay = initialDelay, NumIterations = numIterations, CancelOnException = cancelOnException, ThrowException = throwException }; },
               (step) =>
               {
                   try
                   {
                       step.Cancel = valueFunc();
                   }
                   catch (TimeoutException ex)
                   {
                       errMessage = ex.Message;
                   }
               });

            if (sampler.Canceled)
                return;

            if (exception != null)
                exception(errMessage);

            if (sampler.HasExceptions)
            {
                exceptionCach = sampler.Exceptions.InnerExceptions.FirstOrDefault() as SauronException;
                if (exceptionCach == null)
                    throw sampler.Exceptions.InnerExceptions.FirstOrDefault();
                else
                    exceptionCach.Throw();
            }

            if (!string.IsNullOrEmpty(errMessage))
                throw new Exception(errMessage);
            else
                Error().SOFTWARE.SECUENCIA_TEST.SAMPLER_WITH_CANCEL_MAL_IMPLEMENTADO().Throw();
        }

        public void SamplerWithCancel(Func<int, bool> valueFunc, string errMessage, byte numIterations = 3, int interval = 500, int initialDelay = 0, bool cancelOnException = true, bool throwException = true, Action<string> exception = null)
        {
            SauronException exceptionCach = null;

            var sampler = Sampler.Run(
                () => { return new SamplerConfig { Interval = interval, InitialDelay = initialDelay, NumIterations = numIterations, CancelOnException = cancelOnException, ThrowException = throwException, CancellationToken = SequenceContext.Current.CancellationToken }; },
                (step) =>
                {
                    step.Cancel = valueFunc(step.Step);
                });

            if (sampler.Canceled)
                return;

            if (exception != null)
                exception(errMessage);

            if (sampler.HasExceptions)
            {
                exceptionCach = sampler.Exceptions.InnerExceptions.FirstOrDefault() as SauronException;
                if (exceptionCach == null)
                    throw sampler.Exceptions.InnerExceptions.FirstOrDefault();
                else
                    exceptionCach.Throw();
            }

            if (!string.IsNullOrEmpty(errMessage))
                throw new Exception(errMessage);
            else
                Error().SOFTWARE.SECUENCIA_TEST.SAMPLER_WITH_CANCEL_MAL_IMPLEMENTADO().Throw();
        }

        //-------------------------------------------------------------------------

        public bool HasError(AdjustValueDef value, bool compare = true)
        {
            if ((compare && !value.IsValid()) || (!compare && !value.IsValidAverageError()))
                return true;

            return false;
        }

        public List<AdjustValueDef> HasError(IEnumerable<AdjustValueDef> values, bool compare = true)
        {
            var result = new List<AdjustValueDef>();

            foreach (var value in values)
                if ((compare && !value.IsValid()) || (!compare && !value.IsValidAverageError()))
                    result.Add(value);

            return result;
        }

        public List<AdjustValueDef> HasError(IEnumerable<TriAdjustValueDef> values, bool compare = true)
        {
            var result = new List<AdjustValueDef>();

            foreach (var value in values)
            {
                if ((compare && !value.L1.IsValid()) || (!compare && !value.L1.IsValidAverageError()))
                    result.Add(value.L1);

                if ((compare && !value.L2.IsValid()) || (!compare && !value.L2.IsValidAverageError()))
                    result.Add(value.L2);

                if ((compare && !value.L3.IsValid()) || (!compare && !value.L3.IsValidAverageError()))
                    result.Add(value.L3);

                if (value.Neutro != null)
                    if ((compare && !value.Neutro.IsValid()) || (!compare && !value.Neutro.IsValidAverageError()))
                        result.Add(value.Neutro);

                if (value.Fugas != null)
                    if ((compare && !value.Fugas.IsValid()) || (!compare && !value.Fugas.IsValidAverageError()))
                        result.Add(value.Fugas);

                if (value.NeutroCalc != null)
                    if ((compare && !value.NeutroCalc.IsValid()) || (!compare && !value.NeutroCalc.IsValidAverageError()))
                        result.Add(value.NeutroCalc);
            }

            return result;
        }

        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------

        public void TestConsumo(string resultname, double min, double max, Func<double> valueFunc, string errorMsg = null, int Interval = 1000, int Numiteraciones = 4, ParamUnidad unidad = ParamUnidad.W)
        {
            bool activa = resultname.Contains(ConstantsParameters.Electrics.KW);
            bool reactiva = resultname.Contains(ConstantsParameters.Electrics.KVAR);

            if (activa)
                unidad = ParamUnidad.W;

            if (reactiva)
                unidad = ParamUnidad.Var;

            AdjustValueDef valueDef = new AdjustValueDef { Max = max, Min = min, Unidad = unidad };

            var sampler = Sampler.Run(
               () => { return new SamplerConfig { Interval = Interval, NumIterations = Numiteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken }; },
               (step) =>
               {
                   valueDef.Value = valueFunc();

                   step.Cancel = valueDef.IsValid();
               });

            Resultado.Set(resultname, valueDef.Value, unidad, min, max);

            if (errorMsg == null)
                errorMsg = string.Format("Error {0} fuera de márgenes", resultname);

            Assert.IsTrue(sampler.Canceled, Error().UUT.CONFIGURACION.MARGENES(errorMsg));
        }

        //-------------------------------------------------------------------------

        public AdjustValueDef TestMeasureBase(AdjustValueDef defs, Func<int, double> funcVariables, int DeleteFirst, int numIteraciones, int intervalo, bool withAverage = false)
        {
            var list = new StatisticalList(defs.Name) { Name = "Measure" };

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var varsVariables = funcVariables(step.Step);

                   if (step.Step < DeleteFirst)
                       return;

                   list.Add(varsVariables);

                   defs.Value = list.Average(0);

                   if (withAverage)
                   {
                       if (list.Count(0) >= numIteraciones - DeleteFirst)
                           step.Cancel = defs.IsValid();                      
                   }
                   else
                   {
                       step.Cancel =  defs.IsValid();                  
                       if (!step.Cancel)
                           list.Clear();
                   }
                 
                   defs.AddToResults(Resultado);

               });

            if (!defs.IsValid())
                Error().UUT.MEDIDA.MARGENES(defs.Name).Throw();   

            return defs;
        }

        public List<AdjustValueDef> TestMeasureBase(List<AdjustValueDef> defs, Func<int, double[]> funcVariables, int DeleteFirst, int numIteraciones, int intervalo, Func<List<AdjustValueDef>, bool> validFunc = null, bool withAverage = false)
        {
            var names = defs.Select((p) => p.Name).ToArray();
            var list = new StatisticalList(names) { Name = "Measure" };

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var varsVariables = funcVariables(step.Step);

                   if (step.Step < DeleteFirst)
                       return;

                   list.Add(varsVariables);

                   for (int i = 0; i < defs.Count; i++)
                       defs[i].Value = list.Average(i);


                   if (withAverage)
                   {
                       if (list.Count(0) >= numIteraciones - DeleteFirst)
                           step.Cancel = validFunc != null ? validFunc(defs) : !HasError(defs, false).Any();
                   }
                   else
                   {
                       step.Cancel = validFunc != null ? validFunc(defs) : !HasError(defs, false).Any();

                       if (!step.Cancel)
                           list.Clear();
                   }

                   defs.AddToResults(Resultado);

               });

            if (HasError(defs, false).Any())
                TestException.Create().UUT.MEDIDA.MARGENES(HasError(defs, false).FirstOrDefault().Name).Throw();

            return defs;
        }

        public TriAdjustValueDef TestMeasureBase(TriAdjustValueDef defs, Func<int, double[]> funcVariables, int DeleteFirst, int numIteraciones, int intervalo, Func<TriAdjustValueDef, bool> validFunc = null, bool withAverage = false)
        {
            var list = new StatisticalList(defs.NamesToList().ToArray()) { Name = "Measure" }; ;

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var varsVariables = funcVariables(step.Step);

                   if (step.Step < DeleteFirst)
                       return;

                   list.Add(varsVariables);

                   var index = 0;
       
                       defs.L1.Value = list.Average(index);
                       index += 1;
                       defs.L2.Value = list.Average(index);
                       index += 1;
                       defs.L3.Value = list.Average(index);
                       if (defs.Neutro != null)
                       {
                           index += 1;
                           defs.Neutro.Value = list.Average(index);
                       }
                       if (defs.NeutroCalc != null)
                       {
                           index += 1;
                           defs.NeutroCalc.Value = list.Average(index);
                       }
                       if (defs.Fugas != null)
                       {
                           index += 1;
                           defs.Fugas.Value = list.Average(index);
                       }
                       index += 1;
                   
                   if (withAverage)
                   {
                       if (list.Count(0) >= numIteraciones - DeleteFirst)
                           step.Cancel = validFunc != null ? validFunc(defs) : !defs.HasMinMaxError();
                   }
                   else
                   {
                       step.Cancel = validFunc != null ? validFunc(defs) : !defs.HasMinMaxError();

                       if (!step.Cancel)
                           list.Clear();
                   }

                   Resultado.Set(defs.L1.Name, defs.L1.Value, defs.L1.Unidad, defs.L1.Min, defs.L1.Max);
                   Resultado.Set(defs.L2.Name, defs.L2.Value, defs.L2.Unidad, defs.L2.Min, defs.L2.Max);
                   Resultado.Set(defs.L3.Name, defs.L3.Value, defs.L3.Unidad, defs.L3.Min, defs.L3.Max);
                   if (defs.Neutro != null)
                       Resultado.Set(defs.Neutro.Name, defs.Neutro.Value, defs.Neutro.Unidad, defs.Neutro.Min, defs.Neutro.Max);
                   if (defs.NeutroCalc != null)
                       Resultado.Set(defs.NeutroCalc.Name, defs.NeutroCalc.Value, defs.NeutroCalc.Unidad, defs.NeutroCalc.Min, defs.NeutroCalc.Max);
                   if (defs.Fugas != null)
                       Resultado.Set(defs.Fugas.Name, defs.Fugas.Value, defs.Fugas.Unidad, defs.Fugas.Min, defs.Fugas.Max);
               });

            if (defs.HasMinMaxError())
                TestException.Create().UUT.MEDIDA.MARGENES(defs.GetMinMaxError().FirstOrDefault().Name).Throw();

            return defs;
        }

        public List<TriAdjustValueDef> TestMeasureBase(List<TriAdjustValueDef> defs, Func<int, double[]> funcVariables, int DeleteFirst, int numIteraciones, int intervalo, Func<List<TriAdjustValueDef>, bool> validFunc = null, bool withAverage = false)
        {
            var names = new List<string>();
            defs.ForEach((p) => { names.Add(p.NamesToList().ToArray()); });
            var list = new StatisticalList(names.ToArray()) { Name = "Measure" }; 

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var varsVariables = funcVariables(step.Step);

                   if (step.Step < DeleteFirst)
                       return;

                   list.Add(varsVariables);

                   var index = 0;

                   for (int i = 0; i < defs.Count; i++)
                   {
                       defs[i].L1.Value = list.Average(index);
                       index += 1;
                       defs[i].L2.Value = list.Average(index);
                       index += 1;
                       defs[i].L3.Value = list.Average(index);
                       if (defs[i].Neutro != null)
                       {
                           index += 1;
                           defs[i].Neutro.Value = list.Average(index);
                       }
                       if (defs[i].NeutroCalc != null)
                       {
                           index += 1;
                           defs[i].NeutroCalc.Value = list.Average(index);
                       }
                       if (defs[i].Fugas != null)
                       {
                           index += 1;
                           defs[i].Fugas.Value = list.Average(index);
                       }
                       index += 1;
                   }

                   if (withAverage)
                   {
                       if (list.Count(0) >= numIteraciones - DeleteFirst)
                           step.Cancel = validFunc != null ? validFunc(defs) : !HasError(defs, false).Any();
                   }
                   else
                   {
                       step.Cancel = validFunc != null ? validFunc(defs) : !HasError(defs, false).Any();

                       if (!step.Cancel)
                           list.Clear();
                   }

                   foreach (var res in defs)
                   {
                       Resultado.Set(res.L1.Name, res.L1.Value, res.L1.Unidad, res.L1.Min, res.L1.Max);
                       Resultado.Set(res.L2.Name, res.L2.Value, res.L2.Unidad, res.L2.Min, res.L2.Max);
                       Resultado.Set(res.L3.Name, res.L3.Value, res.L3.Unidad, res.L3.Min, res.L3.Max);
                       if (res.Neutro != null)
                           Resultado.Set(res.Neutro.Name, res.Neutro.Value, res.Neutro.Unidad, res.Neutro.Min, res.Neutro.Max);
                       if (res.NeutroCalc != null)
                           Resultado.Set(res.NeutroCalc.Name, res.NeutroCalc.Value, res.NeutroCalc.Unidad, res.NeutroCalc.Min, res.NeutroCalc.Max);
                       if (res.Fugas != null)
                           Resultado.Set(res.Fugas.Name, res.Fugas.Value, res.Fugas.Unidad, res.Fugas.Min, res.Fugas.Max);
                   }
               });

            if (HasError(defs, false).Any())
                TestException.Create().UUT.MEDIDA.MARGENES(HasError(defs, false).FirstOrDefault().Name).Throw();

            return defs;
        }

        //-------------------------------------------------------------------------

        public void TestCalibracionBase(AdjustValueDef defs, Func<double> funcVariables, Func<double> funcPatron, int numMuestrasToAverage = 1, int numIteraciones = 5, int intervalo = 500, string errMessage = "")
        {
            var list = new StatisticalList(defs.Name) { Name = "Calibration" }; 
            var listPatron = new StatisticalList(defs.Name + "_PATRON") { Name = "Calibration" }; 
            bool valueValid = false;

            if (numMuestrasToAverage == 0)
                throw new Exception("Error, no se permite hacer una calibración con un numMuestrastoAverage igual a 0 del equipo y el patron (Avisar a idp)");

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var varsVariables = funcVariables();
                   var varsPatron = funcPatron();

                   list.Add(varsVariables);
                   listPatron.Add(varsPatron);

                   defs.Value = list.Average(0);
                   defs.Average = listPatron.Average(0);

                   logger.InfoFormat("TestCalibracionBase reintento {0} de {1} lista de {2} series", step.Step, numIteraciones, list.Count(0));

                   if (!valueValid)
                   {
                       if (!defs.IsValidAverageError())
                           list.Clear();
                       else
                       {
                           valueValid = true;
                           step.Sampler.Config.NumIterations += numMuestrasToAverage;
                       }
                   }
                   else
                   {
                       if (list.Count(0) >= numMuestrasToAverage)
                           step.Cancel = defs.IsValidAverageError();
                   }

                   Resultado.Set(defs.Name, defs.Value, defs.Unidad, defs.Min, defs.Max);
                   Resultado.Set(defs.Name + "_PATRON", defs.Average, defs.Unidad);
                   Resultado.Set(defs.Name + "_ERROR", defs.Error, ParamUnidad.PorCentage, defs.Tolerance * -1, defs.Tolerance);
               });

            Assert.IsTrue(sampler.Canceled, Error().UUT.CONFIGURACION.MARGENES(errMessage));        
        }

        public void TestCalibracionBase(List<AdjustValueDef> defs, Func<double[]> funcVariables, Func<double[]> funcPatron, int numMuestrasToAverage = 1, int numIteraciones = 5, int intervalo = 500)
        {
            var names = defs.Select((p) => p.Name).ToArray();
            var namesPatron = defs.Select((p) => string.Format("{0}_´{1}", p.Name, "PATRON")).ToArray();

            var list = new StatisticalList(names) { Name = "Calibration" }; 
            var listPatron = new StatisticalList(namesPatron) { Name = "Calibration" }; 
            bool valueValid = false;

            if (numMuestrasToAverage == 0)
                throw new Exception("Error, no se permite hacer una calibración con un numMuestrastoAverage igual a 0 del equipo y el patron (Avisar a idp)");

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                  // var varsVariables = Task.Factory.StartNew<double[]>(funcVariables);
                   //var varsPatron = Task.Factory.StartNew<double[]>(funcPatron);

                   var varsVariables = funcVariables();
                   var varsPatron = funcPatron();

                  // Task.WaitAll(varsVariables, varsPatron);

                   list.Add(varsVariables);
                   listPatron.Add(varsPatron);

                   for (int i = 0; i < defs.Count; i++)
                   {
                       defs[i].Value = list.Average(i);
                       defs[i].Average = listPatron.Average(i);
                   }

                   if (!valueValid)
                   {
                       if (HasError(defs, false).Any())
                           list.Clear();
                       else
                       {
                           valueValid = true;
                           step.Sampler.Config.NumIterations += numMuestrasToAverage;
                       }
                   }
                   else
                   {
                       if (list.Count(0) >= numMuestrasToAverage)
                           step.Cancel = !HasError(defs, false).Any();
                   }

                   foreach (var res in defs)
                   {
                       Resultado.Set(res.Name, res.Value, res.Unidad, res.Min, res.Max);
                       Resultado.Set(res.Name + "_PATRON", res.Average, res.Unidad);
                       Resultado.Set(res.Name + "_ERROR", res.Error, ParamUnidad.PorCentage, res.Tolerance * -1, res.Tolerance);
                   }
               });
          
            if (HasError(defs, false).Any())
                TestException.Create().UUT.CALIBRACION.MARGENES(HasError(defs, false).FirstOrDefault().Name).Throw();
        }

        public void TestCalibracionBase(TriAdjustValueDef defs, Func<double[]> funcVariables, Func<double[]> funcPatron, int numMuestrasToAverage = 1, int numIteraciones = 5, int intervalo = 500, string errMessage = "")
        {
            var list = new StatisticalList(defs.NamesToList().ToArray()) { Name = "Calibration" }; 
            var namesPatron = defs.NamesToList().Select((p) => string.Format("{0}_{1}", p, "PATRON"));
            var listPatron = new StatisticalList(namesPatron.ToArray()) { Name = "Calibration" }; 
            bool valueValid = false;

            if (numMuestrasToAverage == 0)
                throw new Exception("Error, no se permite hacer una calibración con un numMuestrastoAverage igual a 0 del equipo y el patron (Avisar a idp)");

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   
                   var varsVariables = funcVariables();
                   var varsPatron = funcPatron();

                   list.Add(varsVariables);
                   listPatron.Add(varsPatron);

                   var index = 0;
                   defs.L1.Value = list.Average(index);
                   defs.L1.Average = listPatron.Average(index);
                   index += 1;
                   defs.L2.Value = list.Average(index);
                   defs.L2.Average = listPatron.Average(index);
                   index += 1;
                   defs.L3.Value = list.Average(index);
                   defs.L3.Average = listPatron.Average(index);
                   if (defs.Neutro != null)
                   {
                       index += 1;
                       defs.Neutro.Value = list.Average(index);
                       defs.Neutro.Average = listPatron.Average(index);
                   }
                   if (defs.NeutroCalc != null)
                   {
                       index += 1;
                       defs.NeutroCalc.Value = list.Average(index);
                       defs.NeutroCalc.Average = listPatron.Average(index);
                   }
                   if (defs.Fugas != null)
                   {
                       index += 1;
                       defs.Fugas.Value = list.Average(index);
                       defs.Fugas.Average = listPatron.Average(index);
                   }
                   if (!valueValid)
                   {
                       if (defs.HasAverageError())
                           list.Clear();
                       else
                       {
                           valueValid = true;
                           step.Sampler.Config.NumIterations += numMuestrasToAverage;
                       }
                   }
                   else
                   {
                       if (list.Count(0) >= numMuestrasToAverage)
                           step.Cancel = !defs.HasAverageError();
                   }

                   Resultado.Set(defs.L1.Name, defs.L1.Value, defs.L1.Unidad, defs.L1.Min, defs.L1.Max);
                   Resultado.Set(defs.L1.Name + "_PATRON", defs.L1.Average, defs.L1.Unidad);
                   Resultado.Set(defs.L1.Name + "_ERROR", defs.L1.Error, ParamUnidad.PorCentage, defs.L1.Tolerance * -1, defs.L1.Tolerance);

                   Resultado.Set(defs.L2.Name, defs.L2.Value, defs.L2.Unidad, defs.L2.Min, defs.L2.Max);
                   Resultado.Set(defs.L2.Name + "_PATRON", defs.L2.Average, defs.L2.Unidad);
                   Resultado.Set(defs.L2.Name + "_ERROR", defs.L2.Error, ParamUnidad.PorCentage, defs.L2.Tolerance * -1, defs.L2.Tolerance);

                   Resultado.Set(defs.L3.Name, defs.L3.Value, defs.L3.Unidad, defs.L3.Min, defs.L3.Max);
                   Resultado.Set(defs.L3.Name + "_PATRON", defs.L3.Average, defs.L3.Unidad);
                   Resultado.Set(defs.L3.Name + "_ERROR", defs.L3.Error, ParamUnidad.PorCentage, defs.L3.Tolerance * -1, defs.L3.Tolerance);

                   if (defs.Neutro != null)
                   {
                       Resultado.Set(defs.Neutro.Name, defs.Neutro.Value, defs.Neutro.Unidad, defs.Neutro.Min, defs.Neutro.Max);
                       Resultado.Set(defs.Neutro.Name + "_PATRON", defs.Neutro.Average, defs.Neutro.Unidad);
                       Resultado.Set(defs.Neutro.Name + "_ERROR", defs.Neutro.Error, ParamUnidad.PorCentage, defs.Neutro.Tolerance * -1, defs.Neutro.Tolerance);
                   }

                   if (defs.NeutroCalc != null)
                   {
                       Resultado.Set(defs.NeutroCalc.Name, defs.NeutroCalc.Value, defs.NeutroCalc.Unidad, defs.NeutroCalc.Min, defs.NeutroCalc.Max);
                       Resultado.Set(defs.NeutroCalc.Name + "_PATRON", defs.NeutroCalc.Average, defs.NeutroCalc.Unidad);
                       Resultado.Set(defs.NeutroCalc.Name + "_ERROR", defs.NeutroCalc.Error, ParamUnidad.PorCentage, defs.NeutroCalc.Tolerance * -1, defs.NeutroCalc.Tolerance);
                   }

                   if (defs.Fugas != null)
                   {
                       Resultado.Set(defs.Fugas.Name, defs.Fugas.Value, defs.Fugas.Unidad, defs.Fugas.Min, defs.Fugas.Max);
                       Resultado.Set(defs.Fugas.Name + "_PATRON", defs.Fugas.Average, defs.Fugas.Unidad);
                       Resultado.Set(defs.Fugas.Name + "_ERROR", defs.Fugas.Error, ParamUnidad.PorCentage, defs.Fugas.Tolerance * -1, defs.Fugas.Tolerance);
                   }
               });

            if (defs.HasMinMaxError())
                TestException.Create().UUT.CALIBRACION.MARGENES(defs.GetAverageError().FirstOrDefault().Name).Throw();
        }

        public void TestCalibracionBase(List<TriAdjustValueDef> defs, Func<double[]> funcVariables, Func<double[]> funcPatron, int numMuestrasToAverage = 1, int numIteraciones = 5, int intervalo = 500)
        {
            var names = new List<string>();
            defs.ForEach((p) => { names.Add(p.NamesToList().ToArray()); });
            var list = new StatisticalList(names.ToArray()) { Name = "Calibration" };

            var namesPatron = new List<string>();
            defs.ForEach((p) => { namesPatron.Add(p.NamesToList().Select((s) => string.Format("{0}_{1}", s, "PATRON")).ToArray()); });
            var listPatron = new StatisticalList(namesPatron.ToArray()) { Name = "Calibration" }; 

            bool valueValid = false;

            if (numMuestrasToAverage == 0)
                throw new Exception("Error, no se permite hacer una calibración con un numMuestrastoAverage igual a 0 del equipo y el patron (Avisar a idp)");

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = intervalo, NumIterations = numIteraciones, CancelOnException = true, ThrowException = true, CancellationToken = SequenceContext.Current.CancellationToken },
               (step) =>
               {
                   var varsVariables = funcVariables();
                   var varsPatron = funcPatron();

                   list.Add(varsVariables);
                   listPatron.Add(varsPatron);

                   var index = 0;
                   for (int i = 0; i < defs.Count; i++)
                   {
                       defs[i].L1.Value = list.Average(index);
                       defs[i].L1.Average = listPatron.Average(index);
                       index += 1;
                       defs[i].L2.Value = list.Average(index);
                       defs[i].L2.Average = listPatron.Average(index);
                       index += 1;
                       defs[i].L3.Value = list.Average(index);
                       defs[i].L3.Average = listPatron.Average(index);
                       if (defs[i].Neutro != null)
                       {
                           index += 1;
                           defs[i].Neutro.Value = list.Average(index);
                           defs[i].Neutro.Average = listPatron.Average(index);
                       }
                       if (defs[i].NeutroCalc != null)
                       {
                           index += 1;
                           defs[i].NeutroCalc.Value = list.Average(index);
                           defs[i].NeutroCalc.Average = listPatron.Average(index);
                       }
                       if (defs[i].Fugas != null)
                       {
                           index += 1;
                           defs[i].Fugas.Value = list.Average(index);
                           defs[i].Fugas.Average = listPatron.Average(index);
                       }
                       index += 1;
                   }

                   if (!valueValid)
                   {
                       if (HasError(defs, false).Any())
                           list.Clear();
                       else
                       {
                           valueValid = true;
                           step.Sampler.Config.NumIterations += numMuestrasToAverage;
                       }
                   }
                   else
                   {
                       if (list.Count(0) >= numMuestrasToAverage)
                           step.Cancel = !HasError(defs, false).Any();
                   }

                   foreach (var res in defs)
                   {
                       Resultado.Set(res.L1.Name, res.L1.Value, res.L1.Unidad, res.L1.Min, res.L1.Max);
                       Resultado.Set(res.L1.Name + "_PATRON", res.L1.Average, res.L1.Unidad);
                       Resultado.Set(res.L1.Name + "_ERROR", res.L1.Error, ParamUnidad.PorCentage, res.L1.Tolerance * -1, res.L1.Tolerance);

                       Resultado.Set(res.L2.Name, res.L2.Value, res.L2.Unidad, res.L2.Min, res.L2.Max);
                       Resultado.Set(res.L2.Name + "_PATRON", res.L2.Average, res.L2.Unidad);
                       Resultado.Set(res.L2.Name + "_ERROR", res.L2.Error, ParamUnidad.PorCentage, res.L2.Tolerance * -1, res.L2.Tolerance);

                       Resultado.Set(res.L3.Name, res.L3.Value, res.L3.Unidad, res.L3.Min, res.L3.Max);
                       Resultado.Set(res.L3.Name + "_PATRON", res.L3.Average, res.L3.Unidad);
                       Resultado.Set(res.L3.Name + "_ERROR", res.L3.Error, ParamUnidad.PorCentage, res.L3.Tolerance * -1, res.L3.Tolerance);

                       if (res.Neutro != null)
                       {
                           Resultado.Set(res.Neutro.Name, res.Neutro.Value, res.Neutro.Unidad, res.Neutro.Min, res.Neutro.Max);
                           Resultado.Set(res.Neutro.Name + "_PATRON", res.Neutro.Average, res.Neutro.Unidad);
                           Resultado.Set(res.Neutro.Name + "_ERROR", res.Neutro.Error, ParamUnidad.PorCentage, res.Neutro.Tolerance * -1, res.Neutro.Tolerance);
                       }

                       if (res.NeutroCalc != null)
                       {
                           Resultado.Set(res.NeutroCalc.Name, res.NeutroCalc.Value, res.NeutroCalc.Unidad, res.NeutroCalc.Min, res.NeutroCalc.Max);
                           Resultado.Set(res.NeutroCalc.Name + "_PATRON", res.NeutroCalc.Average, res.NeutroCalc.Unidad);
                           Resultado.Set(res.NeutroCalc.Name + "_ERROR", res.NeutroCalc.Error, ParamUnidad.PorCentage, res.NeutroCalc.Tolerance * -1, res.NeutroCalc.Tolerance);
                       }

                       if (res.Fugas != null)
                       {
                           Resultado.Set(res.Fugas.Name, res.Fugas.Value, res.Fugas.Unidad, res.Fugas.Min, res.Fugas.Max);
                           Resultado.Set(res.Fugas.Name + "_PATRON", res.Fugas.Average, res.Fugas.Unidad);
                           Resultado.Set(res.Fugas.Name + "_ERROR", res.Fugas.Error, ParamUnidad.PorCentage, res.Fugas.Tolerance * -1, res.Fugas.Tolerance);
                       }
                   }
               });

            if (HasError(defs, false).Any())
                Error().UUT.CALIBRACION.MARGENES(HasError(defs, false).FirstOrDefault().Name).Throw();
        }
     
        //-------------------------------------------------------------------------

    }
}
