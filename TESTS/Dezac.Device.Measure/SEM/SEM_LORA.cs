﻿using Comunications.Message;
using Comunications.Protocolo;
using System;
using System.Globalization;
using System.IO.Ports;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.01)]
    public class SEM_LORA : DeviceBase
    {
        public MessageTransport mt;
        public SerialPortAdapter sp;
        public int portNumber;

        public SEM_LORA()
        {
        }

        public SEM_LORA(int SerialPort)
        {
            SetSerialPort(SerialPort);
        }

        public void SetSerialPort(int serialPort)
        {
            LogMethod();
            portNumber = serialPort;
            _logger.InfoFormat("SerialPort PortName:{0}  BaudRate{1}", SP.PortName, SP.BaudRate);
        }

        public SerialPortAdapter SP
        {
            get
            {
                if (sp == null)
                {
                    var serialPort = new SerialPort("COM" + portNumber, 9600, Parity.None, 8, StopBits.One);
                    sp = new SerialPortAdapter(serialPort);
                }
                return sp;
            }
        }


        public MessageTransport MT
        {
            get
            {
                if (mt == null)
                {
                    var protocol = new ASCIIProtocol() { CaracterFinTx = "\r", CaracterFinRx = "\r" };
                    mt = new MessageTransport(protocol, SP, _logger);
                    mt.ReadTimeout = 2000;
                    mt.WriteTimeout = 2000;
                    mt.Retries = 1;
                }
                return mt;
            }
        }

        public StringMessage response { get; set; }

        public enum TypeMode
        {
            Transmisor=0,
            Receptor =2
        }

        public void TestComunication()
        {
            LogMethod();
            var result = WriteAndReadSerialPort("AT.S.Ping?", "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK de test comunicaciones");
        }
                
        public void SetupDefault()
        {
            LogMethod();
            var result = WriteAndReadSerialPort("AT.S.Default=YA", "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK de test comunicaciones");

           sp.BaudRate = 19200;
           mt.StreamResource = sp;
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            return WriteAndReadSerialPort("AT.S.FirmVersion?");
        }

        public void TestStatusVoltge()
        {
            LogMethod();
            var result = WriteAndReadSerialPort("AT.S.Vbat?", "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK de estado interno de tension");
        }

        public void WriteMAC(string MAC)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("AT.M.DeviceId={0}",MAC.PadLeft(8,'0')), "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK al grabar MAC al equipo");
        }

        public void WriteHardwareVersion(string hardwareVersion)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("AT.S.HardVersion={0}", hardwareVersion.PadLeft(8,'0')), "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK al grabar Hardware Version al equipo");
        }

        public void WriteSerialNumber(string SerialNumber)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("AT.S.SerialNumber={0}", SerialNumber.PadLeft(8, '0')), "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK al grabar Serial Number al equipo");
        }

        public void WriteBastidor(string Bastidor)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("AT.S.Bastidor={0}", Bastidor.PadLeft(8, '0')), "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK al grabar Bastidor al equipo");
        }

        public void WriteModeTransmited(TypeMode mode)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("AT.M.NodeMode={0}", (byte)mode), "OK");
            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK al enviar el tipo de modo transmisio al equipo");
        }

        public void WriteSendTestPowerEmision(string tramaSend)
        {
            LogMethod();
            var result = WriteAndReadSerialPort(string.Format("AT.M.Send={0}", tramaSend), "OK");

            if (!result.Contains("OK"))
                throw new Exception("Error no hemos recibido el OK al enviar el trama test potencia emision al equipo");
        }

        public enum bps
        {
            _9600 = 0,
            _19200 = 1,
        }
        public void WriteBaudRate(bps bps)
        {
            LogMethod();
            WriteSerialPort(string.Format("AT.S.ComSetup={0}", ((byte)bps).ToString()));
        }

        public string ReadStatusTransmisionEvent()
        {
            LogMethod();
            return WriteAndReadSerialPort("AT.M.GetInt?");
        }

        public string ReadTramaRecived()
        {
            LogMethod();
            return WriteAndReadSerialPort("AT.M.Receive?");
        }

        public struct PowerMaxSignal
        {
            public double powerSignal;
            public double SNR;
        }

        public PowerMaxSignal ReadMaxPowerRecived()
        {
            LogMethod();
            var signal = WriteAndReadSerialPort("AT.R.GetSignal?");
            var signalArray = signal.Split(',');

            return new PowerMaxSignal() { powerSignal = Convert.ToDouble(signalArray[0]), SNR = Convert.ToDouble(signalArray[1]) };
        }

        public override void Dispose()
        {
            if (mt != null)
            {
                mt.Dispose();
                mt = null;
            }
        }

        public string WriteAndReadSerialPort(string command, string validation = "", int numRetries = 3)
        {
            bool error = false;
            var enUS = new CultureInfo("en-US");
            StringMessage message;
            do
            {
                try
                {
                    _logger.InfoFormat("Command {0}", command);
                    error = false;
                    message = StringMessage.Create(enUS, command);

                   if(!string.IsNullOrEmpty(validation))
                    message.Validator = (m) => { return m.Text == validation; };

                    response = MT.SendMessage<StringMessage>(message);
                }
                catch (Exception ex)
                {
                    error = true;
                    numRetries--;

                    _logger.ErrorFormat("{0}, retry: {1} -> {2}", ex.GetType().Name, numRetries, ex.Message);

                    if (numRetries <= 0 || ex is NotSupportedException)
                        throw;
                }
            } while (error);

            return response.Text;
        }

        public void WriteSerialPort(string command)
        {
            bool error = false;
            var enUS = new CultureInfo("en-US");
            StringMessage message;
            do
            {
                try
                {
                    _logger.InfoFormat("Command {0}", command);
                    error = false;
                    message = StringMessage.Create(enUS, command);
                    message.HasResponse = false;
                    MT.SendMessage<StringMessage>(message);
                }
                catch (Exception ex)
                {
                    error = true;
                    _logger.ErrorFormat("{0}, Exception: {1}", ex.GetType().Name, ex.Message);
                    throw;
                }
            } while (error);
        }

    }
}
