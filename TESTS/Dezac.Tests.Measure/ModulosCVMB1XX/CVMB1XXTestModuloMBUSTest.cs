﻿using Dezac.Device.Measure;
using Dezac.Tests.Model;
using System;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.02)]
    public class CVMB1XXTestModuloMBUSTest : CVMB1XXTestModulosGenerico
    {
        private CVMB1XXModuloMBUS mod;

        public void TestInitialization(int portCVM = 0, int portResi = 0)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            if (portResi == 0)
                portResi = Comunicaciones.SerialPortModulo;

            CVMDummy = AddInstanceVar(new CVMB1XX(portCVM), "UUT");

            mod = new CVMB1XXModuloMBUS(portResi);
            mod.Modbus.PerifericNumber = Comunicaciones.PerifericoModulo;
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", mod.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", mod.GetType().Name), ParamUnidad.SinUnidad);

            Shell.MsgBox("Conecte el modulo M-BUS en el CVM-B100 (DUMMY), enciendalo y después pulse aceptar ", "INICIO TEST", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Question);
        }

        public void TestMBUS()
        {
            Shell.MsgBox("Apague el CVM-B100 (DUMMY), espere 5 segundos y enciendalo. Después pulse aceptar ", "TEST", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Question);
            Delay(30000, "Esperando al b100");

            SamplerWithCancel((p) => { return mod.ReadSatus() == 3; }, "Error el modulo M-BUS estado incorrecto", 50, 1000);

            var numserie = mod.ReadNumSerie();

            var shortNumeroSerie = Convert.ToInt32(TestInfo.NumSerie.ToString().Substring(TestInfo.NumSerie.ToString().Length - 8));

            Assert.AreEqual(shortNumeroSerie, numserie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error numero de serie leido del M-BUS no corresponde con el grabado en la placa M-BUS"));
        }

        public void TestFinish()
        {
            if (CVMDummy != null)
                TestDispose();

            if (mod != null)
                mod.Dispose();
        }
    }
}
