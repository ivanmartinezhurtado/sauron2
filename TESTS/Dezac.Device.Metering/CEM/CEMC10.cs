﻿using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class CEMC10 : CEMBase
    {
        public CEMC10()
            :base()
        {
        }
        public CEMC10(int port)
            :base(port)
        {
          
        }

        public CEMC10(string hostname, int port)
            :base(hostname, port)
        {
           
        }

        public void WriteDisplay(DisplayConfigurations value)
        {
            Modbus.Write<Display>(displayConfigurations[value]);
        }

        public ushort ReadGainGap()
        {
            return base.ReadGainGap<ushort>();
        }

        public void WriteGainsGap(ushort value)
        {
            base.WriteGainsGap(value);
        }

        public ushort ReadGainVoltage()
        {
            return base.ReadGainVoltage<ushort>();
        }

        public void WriteGainsVoltage(ushort value)
        {
            base.WriteGainsVoltage<ushort>(value);
        }

        public ushort ReadGainActive()
        {
            return base.ReadGainActive<ushort>();
        }

        public ushort ReadGainReactive()
        {
            return base.ReadGainReactive<ushort>();
        }

        public void WriteGainsActive(ushort value)
        {
            base.WriteGainsActive(value);
        }

        public void WriteGainsReactive(ushort value)
        {
            base.WriteGainsReactive(value);
        }

        public int ReadVoltage()
        {
            return base.ReadVoltage<int>();
        }

        public int ReadCurrent()
        {
            return base.ReadCurrent<int>();
        }

        public double ReadPowerActiveHiResolution()
        {
            return ReadPointsActive() / ReadGainActive();
        }

        public double ReadPowerReactiveHiResolution()
        {
            return ReadPointsReactive() / ReadGainReactive();
        }

        public double ReadPointsVoltage()
        {
            return base.ReadPointsVoltage<int>();
        }

        public double ReadPointsActive()
        {
            return base.ReadPointsActive<int>();
        }

        public double ReadPointsReactive()
        {
            return base.ReadPointsReactive<int>();
        }

        public void WriteGainOffsetActive(int value)
        {
            base.WriteGainOffsetActive(value);
        }

        public void WriteGainOffsetReactive(int value)
        {
            base.WriteGainOffsetReactive(value);
        } 

        public Tuple<bool, ushort> CalculateGapAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, double anguloDesfase, Func<ushort, bool> adjustValidation, Func<ushort, bool> sampleValidation = null)
        {
            var resultadoAjuste = ushort.MinValue;

            WriteGainsGap(0);

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var lecturas = new List<double>();
                    lecturas.Add(ReadPointsActive());
                    lecturas.Add(ReadPointsReactive());
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    var anguloCalculado = Math.Atan(listValues.Average(1) / listValues.Average(0)) * 360 / (2 * Math.PI);
                    double errorCalculado = anguloDesfase - anguloCalculado;

                    resultadoAjuste = Convert.ToUInt16(Math.Round(1048576 * errorCalculado / (360 * 50)));
                    return adjustValidation(resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, resultadoAjuste);
        }

        public Tuple<bool, ushort> CalculatePowerMeasureFactors(int delFirst, int initCount, int samples, int interval, double powerActive, Func<ushort, bool> adjustValidation, Func<ushort, bool> sampleValidation = null)
        {
            var resultadoAjuste = 0;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var lecturas = new List<double>();
                    lecturas.Add(ReadPointsActive());
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste = Convert.ToUInt16(listValues.Average(0) / powerActive);
                    return adjustValidation((ushort)resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, (ushort)resultadoAjuste);
        }

        public Tuple<bool, ushort> CalculateVoltageAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, double voltage,  Func<ushort, bool> adjustValidation, Func<ushort, bool> sampleValidation = null)
        {
            var resultadoAjuste = 0;
            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 0,
                () =>
                {
                    var lecturas = new List<double>();
                    lecturas.Add(ReadPointsVoltage());
                    return lecturas.ToArray();
                },
                (listValues) =>
                {
                    resultadoAjuste = Convert.ToUInt16((listValues.Average(0) * 100) / voltage);
                    return adjustValidation((ushort)resultadoAjuste);
                });

            return Tuple.Create(adjustResult.Item1, (ushort)resultadoAjuste);
        }

        public Dictionary<DisplayConfigurations, Display> displayConfigurations = new Dictionary<DisplayConfigurations, Display> 
        { 
            { DisplayConfigurations.TODOS_SEGMENTOS_DESACTIVADOS, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x0000, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.TODOS_SEGMENTOS_ACTIVADOS, new Display{ Parte1 = 0xFFFF, Parte2 = 0xFFFF, Parte3 = 0xFFFF, Parte4 = 0xFFFF, Parte5 = 0xFFFF, Parte6 = 0xFFFF, Parte7 = 0xFFFF, Parte8 = 0xFFFF } },
            { DisplayConfigurations.SEGMENTOS_NUMEROS_IMPARES, new Display{ Parte1 = 0x2525, Parte2 = 0x2525, Parte3 = 0x2525, Parte4 = 0x2500, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.SEGMENTOS_NUMEROS_PARES, new Display{ Parte1 = 0x5A5A, Parte2 = 0x5A5A, Parte3 = 0x5A5A, Parte4 = 0x5A00, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.SEGMENTOS_NUMEROS, new Display{ Parte1 = 0xFFFF, Parte2 = 0xFFFF, Parte3 = 0xFFFF, Parte4 = 0xFF00, Parte5 = 0x0000, Parte6 = 0x0000, Parte7 = 0x0000, Parte8 = 0x0000 } },
            { DisplayConfigurations.SEGMENTOS_SIMBOLOS_IMPARES, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x0025, Parte5 = 0x2525, Parte6 = 0x2525, Parte7 = 0x2525, Parte8 = 0x2525 } },
            { DisplayConfigurations.SEGMENTOS_SIMBOLOS_PARES, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x005A, Parte5 = 0x5A5A, Parte6 = 0x5A5A, Parte7 = 0x5A5A, Parte8 = 0x5A5A } },
            { DisplayConfigurations.SEGMENTOS_SIMBOLOS, new Display{ Parte1 = 0x0000, Parte2 = 0x0000, Parte3 = 0x0000, Parte4 = 0x00FF, Parte5 = 0xFFFF, Parte6 = 0xFFFF, Parte7 = 0xFFFF, Parte8 = 0xFFFF } },
            { DisplayConfigurations.SEGMENTOS_PARES, new Display{ Parte1 = 0x5A5A, Parte2 = 0x5A5A, Parte3 = 0x5A5A, Parte4 = 0x5A5A, Parte5 = 0x5A5A, Parte6 = 0x5A5A, Parte7 = 0x5A5A, Parte8 = 0x5A5A } },
            { DisplayConfigurations.SEGMENTOS_IMPARES, new Display{ Parte1 = 0x2525, Parte2 = 0x2525, Parte3 = 0x2525, Parte4 = 0x2525, Parte5 = 0x2525, Parte6 = 0x2525, Parte7 = 0x2525, Parte8 = 0x2525 } }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.DISPLAY)]
        public struct Display
        {
            public UInt16 Parte1;
            public UInt16 Parte2;
            public UInt16 Parte3;
            public UInt16 Parte4;
            public UInt16 Parte5;
            public UInt16 Parte6;
            public UInt16 Parte7;
            public UInt16 Parte8;
        };
    }
}
