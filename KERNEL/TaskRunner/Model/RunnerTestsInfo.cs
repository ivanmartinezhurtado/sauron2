﻿using System.Collections.Generic;

namespace TaskRunner.Model
{
    public class RunnerTestsInfo
    {
        public int NumInstances { get { return Contexts.Count; } }
        public SharedVariablesList Variables { get; set; }
        public List<SequenceContext> Contexts { get; internal set; }
        public RunnerTestsInfo()
        {
            Contexts = new List<SequenceContext>();
        }
    }
}
