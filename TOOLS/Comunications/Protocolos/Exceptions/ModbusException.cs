﻿using Comunications.Message;
using System;

namespace Comunications.Utility
{
    public class ModbusException : Exception
    {
        public ModbusException()
        {
        }

        internal ModbusException(SlaveExceptionResponse ser)
            : base(string.Format("Exepción de modbus, código: {0}", ser.ExceptionCode))
        {
            this.ExceptionCode = Convert.ToInt32(ser.ExceptionCode.GetValueOrDefault());
        }

        public int ExceptionCode { get; set; }
    }
}
