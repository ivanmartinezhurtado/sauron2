﻿using Dezac.Data;
using Dezac.Tests.Actions;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using WinTaskRunner.Models;
using WinTaskRunner.Services;

namespace WinTaskRunner.Views
{
    public partial class Toolbox : UserControl
    {
        public List<ToolboxItem> items;

        protected readonly AppService app;
        protected readonly Font obsoleteActionFont;

        public Toolbox()
        {
            InitializeComponent();

            obsoleteActionFont = new Font(treeView.Font.FontFamily, treeView.Font.Size, FontStyle.Italic);

            treeView.NodeMouseDown += TreeView_NodeMouseDown;
            treeView.MouseUp += TreeView_MouseUp;
            treeView.MouseMove += TreeView_MouseMove;
        }
        public Toolbox(AppService appService)
            : this()
        {
            app = appService;
            //this.assemblyResolver = assemblyResolver;

            treeView.TreeViewElement.ShowNodeToolTips = true;

            Reload();
        }

        private static IShell NewMethod()
        {
            return DI.Resolve<IShell>();
        }

        public void Reload()
        {
            if (items == null)
                items = new List<ToolboxItem>();

            AddNodeIconActions();

            items.Clear();
            treeView.Nodes.Clear();

            try
            {
                List<Assembly> assemblies = LoadAssemblies();

                var actions =
                        from a in assemblies
                        from t in a.GetTypes()
                        let attributes = t.GetCustomAttributes(typeof(DesignerActionAttribute), true)
                        where attributes != null && attributes.Length > 0
                        select new { Type = t, Design = attributes.Cast<DesignerActionAttribute>().FirstOrDefault() };


                foreach (var action in actions)
                {
                    var tb = new ToolboxItem { Name = action.Type.Name, Type = action.Type };

                    var instance = Activator.CreateInstance(action.Design.Type) as ActionDescription;
                    if (instance != null)
                    {
                        tb.Name = instance.Name ?? tb.Name;
                        tb.Description = instance.Description;
                        tb.Category = instance.Category;
                        tb.IconAction = instance.Icon;
                        tb.Obsolete = action.Type.GetCustomAttribute<ObsoleteAttribute>() != null;
                        tb.NoVisible = action.Type.GetCustomAttribute<BrowsableAttribute>() != null;
                    }

                    if (tb.NoVisible)
                        continue;

                    string name = !tb.Obsolete ? tb.Name : tb.Name + " (OBSOLETA)";
                    var actionNode = new RadTreeNode(name);
                    actionNode.Image = tb.IconAction;
                    actionNode.Tag = tb;
                    actionNode.ToolTipText = tb.Description;
                    if (tb.Obsolete)
                        actionNode.Font = obsoleteActionFont;

                    RadTreeNode categoryNode = GetParentNode(tb);

                    categoryNode.Nodes.Add(actionNode);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private RadTreeNode GetParentNode(ToolboxItem tb)
        {
            string[] path = tb.Category.Split('/');
            RadTreeNode categoryNode = null;
            RadTreeNode prevNode = null;
            string fullPath = string.Empty;

            foreach (string name in path)
            {
                fullPath += name + "/";

                categoryNode = treeView.Find(p => p.Name == fullPath);
                if (categoryNode == null)
                {
                    categoryNode = new RadTreeNode(name);
                    int index = imageList.Images.IndexOfKey(name);
                    if (index == -1)
                        index = imageList.Images.IndexOfKey("Folder");

                    categoryNode.ImageIndex = index;
                    categoryNode.Name = fullPath;

                    if (prevNode == null)
                        treeView.Nodes.Add(categoryNode);
                    else
                        prevNode.Nodes.Add(categoryNode);
                }

                prevNode = categoryNode;
            }

            return categoryNode;
        }
        private List<Assembly> LoadAssemblies()
        {
            var list = AppDomain.CurrentDomain.GetAssemblies().ToList();

            foreach (string name in app.Config.Toolbox.LoadAssemblies)
            {
                var assembly = Assembly.LoadFrom(name);
                if (assembly != null)
                    if (!list.Contains(assembly))
                        list.Add(assembly);
            }
            return list;
        }
        private void AddNodeIconActions()
        {
            foreach (Utils.AppConfig.IconTreeNode icons in app.Config.ImageActions)
            {
                string imagePath = string.Format(@"{0}\Images\{1}.png", Application.StartupPath, icons.Icon);
                if (!File.Exists(imagePath))
                    imagePath = string.Format(@"{0}\Images\Action.png", Application.StartupPath);

                var imageIcon = Image.FromFile(imagePath);
                imageList.Images.Add(icons.Key, imageIcon);
            }
        }

        #region Treeview
        private void TreeView_NodeMouseDown(object sender, RadTreeViewMouseEventArgs e)
        {
            if (e.OriginalEventArgs.Button == MouseButtons.Right)
            {
                mnuNodo.Tag = e.Node;
                mnuNodo.Show(treeView, new Point(e.OriginalEventArgs.X, e.OriginalEventArgs.Y));
                return;
            }

            if (treeView.Tag != null && treeView.Tag == e.Node)
                return;

            RadTreeNode node = e.Node;
            if (node == null || node.Tag == null)
                return;

            var m = node.Tag as ToolboxItem;
            if (m == null)
                return;

            treeView.Tag = e.Node;
            treeView.DoDragDrop(m, DragDropEffects.Copy);
        }
        private void TreeView_MouseUp(object sender, MouseEventArgs e)
        {
            treeView.Tag = null;
        }
        private void TreeView_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.None)
                treeView.Tag = null;
        }
        private void treeView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
        private void treeView_DragStarting(object sender, RadTreeViewDragCancelEventArgs e)
        {
            e.Cancel = e.Node == null || e.Node.Nodes.Count != 0;
        }
        private void treeView_DragEnding(object sender, RadTreeViewDragCancelEventArgs e)
        {
            e.Cancel = true;
        }
        private void mnuVerProductos_Click(object sender, EventArgs e)
        {
            var item = treeView.SelectedNode.Tag as ToolboxItem;
            if (item == null)
                return;

            string sql = $@"SELECT T_TESTSEQUENCEFILE.NUMTSF, T_TESTSEQUENCEFILE.DESCRIPCION, T_TESTSEQUENCEFILE.NUMFAMILIA, T_FAMILIA.FAMILIA, T_TESTSEQUENCEFILE.IDFASE
                FROM APP.T_TESTSEQUENCEFILE 
                INNER JOIN APP.T_TESTSEQUENCEFILEPRODUCTO ON T_TESTSEQUENCEFILEPRODUCTO.NUMTSF = T_TESTSEQUENCEFILE.NUMTSF
                INNER JOIN APP.ARTICULO ON ARTICULO.NUMARTICULO = T_TESTSEQUENCEFILEPRODUCTO.NUMPRODUCTO
                INNER JOIN APP.T_FAMILIA ON T_FAMILIA.NUMFAMILIA = T_TESTSEQUENCEFILE.NUMFAMILIA
                INNER JOIN APP.T_TESTSEQUENCEFILEVERSION ON T_TESTSEQUENCEFILEVERSION.NUMTSF = T_TESTSEQUENCEFILE.NUMTSF
                WHERE T_TESTSEQUENCEFILE.NUMTSF IN 
                (
                    SELECT NUMTSF
                    FROM APP.T_TESTSEQUENCEFILEVERSION
                    WHERE dbms_lob.instr(FICHERO, utl_raw.CAST_TO_RAW('.{item.Name}'), 1, 1) > 0
                ) AND T_TESTSEQUENCEFILEVERSION.FECHAVALIDACION = (SELECT MAX(t.FECHAVALIDACION) FROM APP.T_TESTSEQUENCEFILEVERSION t WHERE t.NUMTSF = APP.T_TESTSEQUENCEFILE.NUMTSF)
                GROUP BY T_TESTSEQUENCEFILE.NUMTSF, T_TESTSEQUENCEFILE.DESCRIPCION, T_TESTSEQUENCEFILE.NUMFAMILIA, T_FAMILIA.FAMILIA, T_TESTSEQUENCEFILE.IDFASE";

            object data = null;

            using (var db = new DezacContext())
            {
                data = db.DataQuery(sql).Tables[0];
            }

            IShell shell = DI.Resolve<IShell>();
            shell.ShowTableDialog($"PRODUCTOS DONDE SE USA LA ACTION {item.Name}", data);
        }
        private void verInformacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var item = treeView.SelectedNode.Tag as ToolboxItem;
            if (item == null)
                return;

            IShell shell = NewMethod();

            string msg = string.Format("{1}", item.Category, item.Description);
            DialogResult resp = shell.ShowDialog($"INFORMACION ACTION {item.Name}", new Label() { Text = msg, Width = 400, Height = 200, Padding = new Padding(10), Font = new Font("Arial", 16, FontStyle.Regular, GraphicsUnit.Point) }, MessageBoxButtons.OK);
        }
        #endregion

        #region Searchbox
        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            treeView.Filter = searchBox.Text;
            treeView.TreeViewElement.FilterPredicate = FilterNode;

            if (!string.IsNullOrEmpty(searchBox.Text))
                treeView.ExpandAll();
            else
                treeView.CollapseAll();
        }
        private bool FilterNode(RadTreeNode node)
        {
            string text = searchBox.Text;
            if (string.IsNullOrEmpty(text) || node.Text == null)
                return true;

            if (NodeContainsText(node, text))
                return true;

            if (ParentsNodeContainsText(node, text))
                return true;

            return false;
        }
        private bool NodeContainsText(RadTreeNode node, string text)
        {
            if (node.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            foreach (RadTreeNode child in node.Nodes)
                if (NodeContainsText(child, text))
                    return true;

            return false;
        }
        private bool ParentsNodeContainsText(RadTreeNode node, string text)
        {
            if (node.Parent?.Text.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                return true;

            if (node.Parent != null && ParentsNodeContainsText(node.Parent, text))
                return true;

            return false;
        }
        #endregion
    }
}
