﻿using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.07)]
    public class DataloggerCirResearchTest : TestBase
    {
        private Tower1 tower;
        private DataloggerCirResearch datalogger;
        private double aParameterAdjust;
        private double bParameterAdjust;
        private const string IMAGE_PATH = @"\\SFSERVER01\Idp\Idp público\IMAGESOURCE\DATALOGGER_CIRRESEARCH\";

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower1>("TOWER", () => { return new Tower1(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.ShutdownSources();

            tower.Active24VDC();
            tower.PTE.Reset();

            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
        }

        public void TestInitPortUSBTester(string nameFlukePortUSB)
        {
            byte portFluke;

            try
            {
                portFluke = Convert.ToByte(GetVariable<string>(nameFlukePortUSB, nameFlukePortUSB).Replace("COM", ""));

                tower.Fluke183.PortCom = portFluke;
                tower.Fluke183.ConfigCurrent(FLUKE183.TypeMeasure.DC, FLUKE183.MeasureData.RangeCurrent._500uA);
            }
            catch (Exception e)
            {
                if (e is TimeoutException || e is IOException)
                {
                    var usercontrolFluke = new InputKeyBoard("Introduzca el puerto del Fluke al que esta conectado", "99");
                    Shell.ShowDialog("INICIO TEST", () => usercontrolFluke, MessageBoxButtons.OKCancel);
                    portFluke = Convert.ToByte(usercontrolFluke.TextValue);
                }
                else
                    throw new Exception(e.Message);
            }

            tower.Fluke183.PortCom = (byte)portFluke;
            tower.Fluke183.ConfigCurrent(FLUKE183.TypeMeasure.DC, FLUKE183.MeasureData.RangeCurrent._500uA);
        }

        public void TestConsumption()
        {
            tower.Fluke183.ConfigCurrent(FLUKE183.TypeMeasure.DC, FLUKE183.MeasureData.RangeCurrent._500uA);

            Shell.ShowDialog("Test de consumo", () => new ImageView("Conecte el USB al equipo", IMAGE_PATH + "CONEXION_USB.jpg"), MessageBoxButtons.OK, string.Empty, 500);

            Thread.Sleep(1000);

            var consumoConFuenteAlimentacion = new AdjustValueDef(Params.I.Null.TestPoint("BATTERY_PS_ON").Name, 0, Params.I.Null.TestPoint("BATTERY_PS_ON").Min(), Params.I.Null.TestPoint("BATTERY_PS_ON").Max(), 0, 0, ParamUnidad.uA);

            TestMeasureBase(consumoConFuenteAlimentacion, (step) =>
            {
                return tower.Fluke183.ReadMeasure();
            }, 1, 4, 500);

            Shell.ShowDialog("Test de consumo", () => new ImageView("Desconecte el USB al equipo", IMAGE_PATH + "DESCONEXION_USB.jpg"), MessageBoxButtons.OK, string.Empty, 500);

            Thread.Sleep(1000);

            var consumoSinFuenteAlimentacion = new AdjustValueDef(Params.I.Null.TestPoint("BATTERY_PS_OFF").Name, 0, Params.I.Null.TestPoint("BATTERY_PS_OFF").Min(), Params.I.Null.TestPoint("BATTERY_PS_OFF").Max(), 0, 0, ParamUnidad.uA);

            TestMeasureBase(consumoSinFuenteAlimentacion, (step) =>
            {
                return tower.Fluke183.ReadMeasure();
            }, 1, 4, 500);
        }

        public void TestCommunications(string comPortDatalogger)
        {
            var port = GetVariable<string>("STME_PORT", comPortDatalogger);
            datalogger = new DataloggerCirResearch(Convert.ToInt32(port.Replace("COM","")));
            logger.InfoFormat("Test comunicaction with {0}", port);     
        }

        public void TestClearAll()
        {
            datalogger.WriteClearParameters();
        }

        public void TestDefaultParameters()
        {
            var firmwareVersion = datalogger.ReadFirmwareVersion();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, Convert.ToDouble(firmwareVersion, new CultureInfo("en-US")).ToString(), Convert.ToDouble(Identificacion.VERSION_FIRMWARE, new CultureInfo("en-US")).ToString(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error. Versión de firmware incorrecta"), ParamUnidad.SinUnidad);

            var crcReading = datalogger.ReadCRC();

            Assert.AreEqual(ConstantsParameters.Identification.CRC_FIRMWARE, crcReading.ToString().Trim(), Identificacion.CRC_FIRMWARE.ToString().Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error. El CRC del equipo es incorrecto."), ParamUnidad.SinUnidad);

            var manufacturingMode = datalogger.WriteManufacturingMode(DataloggerCirResearch.State.ON);

            var batteryReading = datalogger.ReadBatteryVoltage();

            Assert.AreBetween(Params.V_DC.Null.Bateria.Name, batteryReading, Params.V_DC.Null.Bateria.Min(), Params.V_DC.Null.Bateria.Max(), Error().UUT.MEDIDA.MARGENES("Error. Tensión de bateria con precinto fuera de márgenes"), ParamUnidad.SinUnidad);
        }

        public void TestLedsVerificacion()
        {
            CheckLed(DataloggerCirResearch.Leds.LED1, false);

            CheckLed(DataloggerCirResearch.Leds.LED2, false);
        }

        public void TestLedsAjuste()
        {
            CheckLed(DataloggerCirResearch.Leds.LED1, true);

            CheckLed(DataloggerCirResearch.Leds.LED2, true);
        }

        public void TestKeyboardVerificacion()
        {
            CheckPushButton(false);
        }

        public void TestKeyboardAjuste()
        {
            CheckPushButton(true);
        }

        public void TestAdjust()
        {
            var minimumVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Vmin.Name, 85, ParamUnidad.V);

            var maximumVoltage = Consignas.GetDouble(Params.V.Null.Ajuste.Vmax.Name, 255, ParamUnidad.V);

            tower.PTE.ApplyPresets(minimumVoltage, 0, 0);

            Delay(3000, "Espera estabilizacion fuente PTE");

            minimumVoltage = tower.MeasureMultimeter(InputMuxEnum.IN14, MagnitudsMultimeter.VoltAC);

            var lowLevelAdjustResult = datalogger.ReadCalibrationResults();

            Assert.AreBetween(Params.FREQ.Null.EnVacio.Name, lowLevelAdjustResult.averageFrequency, Params.FREQ.Null.EnVacio.Min(), Params.FREQ.Null.EnVacio.Max(), Error().UUT.MEDIDA.MARGENES("Error, la frecuencia medida por el equipo en tensión mínima se encuentra fuera de márgenes"), ParamUnidad.SinUnidad);

            Resultado.Set(Params.V.Null.TestPoint("MIN_CALIBRATION_RESULT").Name, lowLevelAdjustResult.calibrationValue, ParamUnidad.SinUnidad);

            tower.PTE.ApplyPresets(maximumVoltage, 0, 0);

            maximumVoltage = tower.MeasureMultimeter(InputMuxEnum.IN14, MagnitudsMultimeter.VoltAC);

            var highLevelAdjustResult = datalogger.ReadCalibrationResults();

            Resultado.Set(Params.V.Null.TestPoint("MAX_CALIBRATION_RESULT").Name, highLevelAdjustResult.calibrationValue, ParamUnidad.SinUnidad);

            Assert.AreBetween(Params.FREQ.Null.EnVacio.Name, highLevelAdjustResult.averageFrequency, Params.FREQ.Null.EnVacio.Min(), Params.FREQ.Null.EnVacio.Max(), Error().UUT.MEDIDA.MARGENES("Error, la frecuencia medida por el equipo en tensión máxima se encuentra fuera de márgenes"), ParamUnidad.SinUnidad);

            aParameterAdjust = (maximumVoltage - minimumVoltage) / (highLevelAdjustResult.calibrationValue - lowLevelAdjustResult.calibrationValue);

            Resultado.Set("A_ADJUST_PARAMETER", aParameterAdjust, ParamUnidad.SinUnidad);

            bParameterAdjust = minimumVoltage - (lowLevelAdjustResult.calibrationValue * (maximumVoltage - minimumVoltage)) / (highLevelAdjustResult.calibrationValue - lowLevelAdjustResult.calibrationValue);

            Resultado.Set("B_ADJUST_PARAMETER", bParameterAdjust, ParamUnidad.SinUnidad);

            tower.PTE.ApplyOffAllAndWaitStabilisation();
        }

        public void TestCustomization()
        {
            datalogger.WriteTestMode();

            var frameNumber = (uint)TestInfo.NumBastidor.Value;

            datalogger.WriteFrameNumber(frameNumber);

            datalogger.WriteHardwareVersion(Identificacion.VERSION_HARDWARE);

            datalogger.WriteProductVersion(Identificacion.VERSION_PRODUCTO);

            if(!string.IsNullOrEmpty(TestInfo.NumSerieFamilia))
            {
                datalogger.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerieFamilia));
                Resultado.Set("SERIAL_NUMBER_INTERN", Convert.ToUInt32(TestInfo.NumSerieFamilia).ToString(), ParamUnidad.SinUnidad);
            }else
                datalogger.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerieInterno));

            datalogger.WriteCalibrationFactorA(aParameterAdjust);

            datalogger.WriteCalibrationFactorB(bParameterAdjust);

            if (datalogger.Modbus.IsOpen)
                datalogger.Modbus.ClosePort();

            if (datalogger.SP.IsOpen)
                datalogger.SP.Close();

            datalogger.Dispose();

            Shell.ShowDialog("RESET HARDWARE", () => new ImageView("Desconecte el USB al equipo", IMAGE_PATH + "DESCONEXION_USB.jpg"), MessageBoxButtons.OK, string.Empty, 500);

            Thread.Sleep(2000);

            Shell.ShowDialog("RESET HARDWARE", () => new ImageView("Conecte el USB al equipo", IMAGE_PATH + "CONEXION_USB.jpg"), MessageBoxButtons.OK, string.Empty, 500);

            Thread.Sleep(2000);

            SamplerWithCancel((p) =>
            {
                datalogger.WriteTestMode();
                return true;
            }, "Error. No se ha podido tomar control del puerto serie", 5, 500, 1000, false, false);

            var reading = datalogger.ReadHardwareVersion();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_HARDWARE, reading, Identificacion.VERSION_HARDWARE, Error().UUT.HARDWARE.NO_COINCIDE("Error, no se ha grabado correctamente la version de hardware"), ParamUnidad.SinUnidad);

            var firmwareReading = datalogger.ReadFirmwareVersion();

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareReading, Identificacion.VERSION_FIRMWARE, Error().UUT.HARDWARE.NO_COINCIDE("Error, no se ha grabado correctamente la version de hardware"), ParamUnidad.SinUnidad);
        }

        public void TestVerification()
        {
            SamplerWithCancel((p) =>
            {
                datalogger.WriteManufacturingMode(DataloggerCirResearch.State.ON);
                return true;
            }, "Error. No se ha podido tomar control del puerto serie", 5, 500, 0, false, false);

            tower.PTE.ApplyPresets(230, 0, 0);

            Delay(3000, "Espera estabilizacion fuente PTE");

            var reading = datalogger.ReadLineVoltage();

            Assert.AreBetween(Params.V_AC.Null.EnVacio.Name, reading, Params.V_AC.Null.EnVacio.Min(), Params.V_AC.Null.EnVacio.Max(), Error().UUT.MEDIDA.MARGENES("Error tension de linea en verificación fuera de márgenes"), ParamUnidad.V);

        }

        public void TestFinish()
        {
            if (datalogger != null)
                datalogger.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.Dispose();
            }
        }

        private void CheckLed(DataloggerCirResearch.Leds led, bool faseAjuste)
        {
            var color = led== DataloggerCirResearch.Leds.LED1 ? "VERDE" : "ROJO";
            
            datalogger.WriteLedState(led, DataloggerCirResearch.State.ON);

            if (Shell.ShowDialog("Test de leds", () => new ImageView(string.Format("Se enciende el led {0} destacado en NEGRO y el LED destacado en AMARILLO como en la imagen ?", color), IMAGE_PATH + led.ToString() + (faseAjuste == true ? "_AJUSTE" : string.Empty) + ".jpg"), MessageBoxButtons.YesNo, string.Empty, 500) != DialogResult.Yes)
                throw new Exception("Error. no se detecta el " + led.ToString() + " encendido");

            datalogger.WriteLedState(led, DataloggerCirResearch.State.OFF);

            if (Shell.ShowDialog("Test de leds", () => new ImageView(string.Format("Se ha apagado el led {0} destacato en NEGRO y sigue encendido el LED destacado en AMARILLO como en la imagen?", color), IMAGE_PATH + "LEDS_APAGADOS" + (faseAjuste == true ? "_AJUSTE" : string.Empty) + ".jpg"), MessageBoxButtons.YesNo, string.Empty, 500) != DialogResult.Yes)
                throw new Exception("Error. no se detecta el " + led.ToString() + " apagado");

            Resultado.Set(led.ToString(), "OK", ParamUnidad.SinUnidad);
        }

        private void CheckPushButton(bool faseAjuste)
        {
            Shell.ShowDialog("Test de pulsador", () => new ImageView("Accionar el pulsador destacado en la imagen durante 1 segundo y pulsar aceptar", IMAGE_PATH + "ACCIONAR_PULSADOR" + (faseAjuste == true ? "_AJUSTE" : string.Empty) + ".jpg"), MessageBoxButtons.OK, string.Empty, 500);

            SamplerWithCancel((p) =>
                {
                    var result= datalogger.ReadPushButton();
                    return result.Contains("OK");
      
                }, "Error. El equipo no ha detectado el pulsador activado", 3, 1000, 100, false, false);

            Resultado.Set("PUSH_BUTTON", "OK", ParamUnidad.SinUnidad);
        }
    }
}
