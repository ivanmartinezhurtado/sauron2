﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using Tftp.Net;

namespace Comunications.Ethernet
{
    public class TFTPClient
    {
        private AutoResetEvent TransferFinishedEvent = new AutoResetEvent(false);
        private TftpClient client = null;

        public bool TransferSucces { get; set; }
        public bool TransferError { get; set; }
        public TftpTransferError TftpTransferError { get; set; }
        public int TransferredBytes { get; set; }
        public int TotalBytes { get; set; }

        public TFTPClient()
        {
            //Setup a TftpClient instance
            client = new TftpClient(new IPEndPoint(IPAddress.Loopback, 69)); 
        }

        public TFTPClient(IPAddress ipDeviecClient)
        {
            //Setup a TftpClient instance
            client = new TftpClient(new IPEndPoint(ipDeviecClient, 69));
        }

        public void StartDownload(string clientPath, string DownloadFile)
        {
            TransferSucces = false;
            TransferError = false;

            //Prepare a simple transfer (GET test.dat)
            var transfer = client.Download(DownloadFile);

            //Capture the events that may happen during the transfer
            transfer.OnProgress += new TftpProgressHandler(transfer_OnProgress);
            transfer.OnFinished += new TftpEventHandler(transfer_OnFinshed);
            transfer.OnError += new TftpErrorHandler(transfer_OnError);

            String file = Path.Combine(clientPath, DownloadFile);

            //Start the transfer and write the data that we're downloading into a memory stream
            transfer.Start(new FileStream(file, FileMode.CreateNew));

            //Wait for the transfer to finish
            TransferFinishedEvent.WaitOne();
        }

        public void StartUpload(string clientPath, string UploadFile)
        {
            TransferSucces = false;
            TransferError = false;

            //Prepare a simple transfer (GET test.dat)
            var transfer = client.Upload(UploadFile);

            //Capture the events that may happen during the transfer
            transfer.OnProgress += new TftpProgressHandler(transfer_OnProgress);
            transfer.OnFinished += new TftpEventHandler(transfer_OnFinshed);
            transfer.OnError += new TftpErrorHandler(transfer_OnError);

            String path = Path.Combine(clientPath, UploadFile);
            FileInfo file = new FileInfo(path);

            if (!file.Exists)
            {
                Console.WriteLine("[" + UploadFile + "] Denying request because the file does not exist.");
                transfer.Cancel(TftpErrorPacket.FileNotFound);
                return;
            }

            //Ok, start the transfer
            Stream str = new FileStream(file.FullName, FileMode.Open);
            //Start the transfer and write the data that we're downloading into a memory stream
            transfer.Start(str);

            //Wait for the transfer to finish
            TransferFinishedEvent.WaitOne();
        }

        public void transfer_OnProgress(ITftpTransfer transfer, TftpTransferProgress progress)
        {
            TransferredBytes = progress.TransferredBytes;
            TotalBytes = progress.TotalBytes;
        }

        public void transfer_OnError(ITftpTransfer transfer, TftpTransferError error)
        {
            TransferError = true;
            TftpTransferError = error;
            TransferFinishedEvent.Set();
        }

        public void transfer_OnFinshed(ITftpTransfer transfer)
        {
            TransferSucces = true;

            TransferFinishedEvent.Set();
        }
    }
}
