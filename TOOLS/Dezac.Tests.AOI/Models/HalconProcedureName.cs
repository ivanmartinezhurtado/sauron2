﻿
namespace Dezac.Tests.AOI.Models
{
    public static class HalconProcedureName
    {
        public static string ExtractWeldign { get { return "AOI_EXTRACT_WELDING"; } }
        public static string ExtractWeldignV2 { get { return "AOI_EXTRACT_WELDING_V2"; } }
        public static string ExtractWeldignV3 { get { return "AOI_EXTRACT_WELDING_V3"; } }
        public static string ExtractBasicFeatures { get { return "AOI_EXTRACT_BASIC_FEATURES"; } }
        public static string ExtractGeometricFeatures { get { return "AOI_EXTRACT_GEOMETRIC_FEATURES"; } }
        public static string ExtractSpecularityFeatures { get { return "AOI_EXTRACT_SPEC_FEATURES"; } }
        public static string ExtractSpecularity2Features { get { return "AOI_EXTRACT_SPEC_FULL_FEATURES"; } }
        public static string AddSamplesToSVM { get { return "AOI_ADD_SAMPLES_SVM"; } }
        public static string Classification {  get { return "AOI_CLASSIFICATION"; } }
        public static string ReadBarcodes { get { return "AOI_READ_BARCODE"; } }
        public static string ResultViewer { get { return "AOI_RESULT_VIEWER"; } }
    }

}
