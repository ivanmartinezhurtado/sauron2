﻿
using Comunications.Utility;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

/// Autogenerated - 09/09/2019 15:11:01
namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class WRU_10 : DeviceBase
    {

        public WRU_10()
        {

        }

        public void SetPort(string port, int baudRate = 19200, byte periferic = 1)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;

            if (!byte.TryParse(portTmp, out result))
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");

            if (modbus == null)
                modbus = new ModbusDeviceSerialPort(result, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                modbus.PortCom = result;

            modbus.UseFunction06 = true;
            modbus.PerifericNumber = periferic;
        }

        private ModbusDeviceSerialPort modbus;

        public ModbusDeviceSerialPort Modbus
        {
            get
            {
                if (modbus == null)
                    modbus = new ModbusDeviceSerialPort(1, 19200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);

                return modbus;
            }
        }

        #region All Register Methods

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadPERIFERIC_NUMBER()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.PERIFERIC_NUMBER);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WritePERIFERIC_NUMBER(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PERIFERIC_NUMBER, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadSPEED_COMUNICATION()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.SPEED_COMUNICATION);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteSPEED_COMUNICATION(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SPEED_COMUNICATION, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadTYPE_OF_PARITY()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.TYPE_OF_PARITY);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTYPE_OF_PARITY(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TYPE_OF_PARITY, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadWORKING_FRECUENCY()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.WORKING_FRECUENCY);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteWORKING_FRECUENCY(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.WORKING_FRECUENCY, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadTRIP_CURRENT_PROGRAM()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.TRIP_CURRENT_PROGRAM);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTRIP_CURRENT_PROGRAM(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIP_CURRENT_PROGRAM, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadTIME_DELAY_CURRENT_TRIP()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.TIME_DELAY_CURRENT_TRIP);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTIME_DELAY_CURRENT_TRIP(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TIME_DELAY_CURRENT_TRIP, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadRELAY_SAFETY_TRIP()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.RELAY_SAFETY_TRIP);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteRELAY_SAFETY_TRIP(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RELAY_SAFETY_TRIP, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadDEVICE_STATUS()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.DEVICE_STATUS);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadTOTAL_NUMBER_RECONNECTION()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.TOTAL_NUMBER_RECONNECTION);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTOTAL_NUMBER_RECONNECTION(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TOTAL_NUMBER_RECONNECTION, value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadSAFETY_RELAY_LOCK()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.SAFETY_RELAY_LOCK);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteSAFETY_RELAY_LOCK(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SAFETY_RELAY_LOCK, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadRECONNECTION_ENABLED()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.RECONNECTION_ENABLED);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadSERIAL_NUMBER()
        {
            LogMethod(); 
            return (uint)((Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER) << 16) + Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER + 1));
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteSERIAL_NUMBER(UInt32 value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, (int)value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadVERSION_SOFT()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.VERSION_SOFT);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public ushort ReadMODEL_DEVICE()
        {
            LogMethod();
            return (ushort)Modbus.ReadRegister((ushort)Registers.MODEL_DEVICE);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteMODEL_DEVICE(ushort model)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MODEL_DEVICE, model);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadOPTIONS_LOCK()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.OPTIONS_LOCK);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA

        public void WriteOPTIONS_LOCK(OptionsLock registersLock)
        {
            LogMethod();
            WriteOPTIONS_LOCK(registersLock.OptionsLockUshort());
        }

        private void WriteOPTIONS_LOCK(ushort registersToLock)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OPTIONS_LOCK, registersToLock);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadDIFERENTIAL_RECONNECTION_SEQUENCE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.DIFERENTIAL_RECONNECTION_SEQUENCE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteDIFERENTIAL_RECONNECTION_SEQUENCE(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DIFERENTIAL_RECONNECTION_SEQUENCE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadMAGNETO_RECONNECTION_SEQUENCE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.MAGNETO_RECONNECTION_SEQUENCE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteMAGNETO_RECONNECTION_SEQUENCE(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MAGNETO_RECONNECTION_SEQUENCE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadNUMBER_RECONNECTION_DIFERENTIAL()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.NUMBER_RECONNECTION_DIFERENTIAL);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadNUMBER_RECONNECTION_MAGNETO()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.NUMBER_RECONNECTION_MAGNETO);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadSTATUS_OUTPUTS_RELAYS()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.STATUS_OUTPUTS_RELAYS);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCURRENT_LEAK_VALUE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CURRENT_LEAK_VALUE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCURRENT_TRIP_VALUE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CURRENT_TRIP_VALUE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadRECONNECTION_NUMBER_DIFERENTIAL_TRIP()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.RECONNECTION_NUMBER_DIFERENTIAL_TRIP);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteRECONNECTION_NUMBER_DIFERENTIAL_TRIP(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RECONNECTION_NUMBER_DIFERENTIAL_TRIP, value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadRECONNECTION_NUMBER_MAGNETO_TRIP()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.RECONNECTION_NUMBER_MAGNETO_TRIP);
        }

        public void WriteRECONNECTION_NUMBER_MAGNETO_TRIP(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RECONNECTION_NUMBER_MAGNETO_TRIP, value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTRIP_EXTERNAL_RESET(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIP_EXTERNAL_RESET, value);
        }


        //// Description: 
        //// Unit: NA - Scale: NA - Max.: NA - Min.: NA
        //public void WriteENABLE_RECORDING_VALUES(UInt16 value)
        //{
        //    LogMethod();
        //    Modbus.Write((ushort)Registers.ENABLE_RECORDING_VALUES, value);
        //}

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteENABLE_RECORDING_VALUES(EnableRecordModes value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ENABLE_RECORDING_VALUES, (ushort)value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCURRENT_PREALARM_ACTIVATION()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CURRENT_PREALARM_ACTIVATION);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCURRENT_PREALARM_ACTIVATION(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CURRENT_PREALARM_ACTIVATION, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadDELAY_ACTIVATION_PREALARM()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.DELAY_ACTIVATION_PREALARM);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteDELAY_ACTIVATION_PREALARM(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DELAY_ACTIVATION_PREALARM, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadSECURITY_RELAY_PREALARM()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.SECURITY_RELAY_PREALARM);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteSECURITY_RELAY_PREALARM(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SECURITY_RELAY_PREALARM, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadRESET_PREALARM()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.RESET_PREALARM);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteRESET_PREALARM(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RESET_PREALARM, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadLIMIT_SCALES_TIMEANDCURRENT()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.LIMIT_SCALES_TIMEANDCURRENT);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteLIMIT_SCALES_TIMEANDCURRENT(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.LIMIT_SCALES_TIMEANDCURRENT, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadWORK_MODE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.WORK_MODE);
        }


        //// Description: 
        //// Unit: NA - Scale: NA - Max.: NA - Min.: NA
        //public void WriteWORK_MODE(UInt16 value)
        //{
        //    LogMethod();
        //    Modbus.Write((ushort)Registers.WORK_MODE, value);
        //}

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteWORK_MODE(WorkModes value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.WORK_MODE, (ushort)value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteDELETE_MANUFACTURING_VALUES(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DELETE_MANUFACTURING_VALUES, value);
        }

        public void WriteCONSTANT_CALIBRATION_SCALE_1(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONSTANT_CALIBRATION_SCALE_1, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCONSTANT_CALIBRATION_SCALE_2(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONSTANT_CALIBRATION_SCALE_2, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCONSTANT_CALIBRATION_SCALE_3(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONSTANT_CALIBRATION_SCALE_3, value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCURRENT_TABLE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CURRENT_TABLE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCURRENT_TABLE(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CURRENT_TABLE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadTIME_TABLE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.TIME_TABLE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteTIME_TABLE(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TIME_TABLE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public Inputs ReadINPUTS_TEST()
        {
            LogMethod();
            return new Inputs((UInt16)Modbus.ReadRegister((ushort)Registers.INPUTS_TEST));
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteOUTPUTS_TEST(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS_TEST, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCONSTANT_CALIBRATION_SCALE_1()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CONSTANT_CALIBRATION_SCALE_1);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCONSTANT_CALIBRATION_SCALE_2()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CONSTANT_CALIBRATION_SCALE_2);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCONSTANT_CALIBRATION_SCALE_3()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.CONSTANT_CALIBRATION_SCALE_3);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadPARAMETERIZATION_CODE()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.PARAMETERIZATION_CODE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WritePARAMETERIZATION_CODE(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PARAMETERIZATION_CODE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadSUMMATION_CALIBRATION()
        {
            LogMethod();
            return (uint)((Modbus.ReadRegister((ushort)Registers.SUMMATION_CALIBRATION_HIGH) << 16) + Modbus.ReadRegister((ushort)Registers.SUMMATION_CALIBRATION_LOW));
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadERROR_CODE_IDP()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.ERROR_CODE_IDP);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteERROR_CODE_IDP(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ERROR_CODE_IDP, value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCUSTOM_DIFERENTIAL()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.SRD_RECONNECTION_TABLE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCUSTOM_DIFERENTIAL(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SRD_RECONNECTION_TABLE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadCUSTOM_MAGNET()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.SRM_RECONNECTION_TABLE);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCUSTOM_MAGNET(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SRM_RECONNECTION_TABLE, value);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt16 ReadLOWER_LIMIT_TIMEANDCURRENT()
        {
            LogMethod();
            return (UInt16)Modbus.ReadRegister((ushort)Registers.SCALE_LIMIT_SELECTION);
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteLOWER_LIMIT_TIMEANDCURRENT(UInt16 value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SCALE_LIMIT_SELECTION, value);
        }

        public void WriteTRIGGER_TYPE_MAG(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIGGER_TYPE_MAG, value);
        }

        public ushort ReadTRIGGER_TYPE_MAG()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TRIGGER_TYPE_MAG);
        }

        public void WriteScaleLimit(byte currentLimit, byte timeLimit)
        {
            LogMethod();

            ushort value = (ushort)(0x0010 * timeLimit + currentLimit);

            Modbus.Write((ushort)Registers.SCALE_LIMIT_SELECTION, value);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public string ReadCARACTERS_START_MESSAGE()
        {
            LogMethod();

            var low = Modbus.ReadString((ushort)Registers.CARACTERS_START_MESSAGE_LOW, 1);
            var high = Modbus.ReadString((ushort)Registers.CARACTERS_START_MESSAGE_HIGH, 1);

            return low + high;
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteCARACTERS_START_MESSAGE(string subModel)
        {
            LogMethod();

            var lowLevel = subModel.Substring(0, 2);
            var highLevel = subModel.Substring(2, 2);

            Modbus.WriteString((ushort)Registers.CARACTERS_START_MESSAGE_LOW, lowLevel);
            Modbus.WriteString((ushort)Registers.CARACTERS_START_MESSAGE_HIGH, highLevel);
        }

        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public UInt32 ReadBASTIDOR_NUMBER()
        {
            LogMethod();
            return (uint)((Modbus.ReadRegister((ushort)Registers.BASTIDOR_NUMBER) << 16) + Modbus.ReadRegister((ushort)Registers.BASTIDOR_NUMBER + 1));
        }


        // Description: 
        // Unit: NA - Scale: NA - Max.: NA - Min.: NA
        public void WriteBASTIDOR_NUMBER(UInt32 value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.BASTIDOR_NUMBER, (int)value);
        }

        public ushort ReadCRC_CALCULTED()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.CRC_CALCULATED);
        }

        //**************************************************************

        public void WriteScaleTable(string currentScales, TableKind typeTable)
        {
            LogMethod();
            WriteScaleTable(new ScaleTableConfiguration(currentScales, typeTable.ToString()), typeTable);

        }

        private void WriteScaleTable(ScaleTableConfiguration currentScales, TableKind typeTable)
        {
            LogMethod();

            byte position = 0;
            int positionValue = 0;

            foreach (var elemnt in currentScales.ListElemnts)
            {
                positionValue = position << 11;
                var value = positionValue + ((elemnt.X100 ? 1 : 0) << 10) + elemnt.ElementValue;

                if (typeTable == TableKind.CURRENT)
                    WriteCurrentTableElement((ushort)value);
                else if (typeTable == TableKind.TIME)
                    WriteTimeTableElement((ushort)value);
                else if (typeTable == TableKind.SRD)
                    WriteSRDCTableElement((ushort)value);
                else if (typeTable == TableKind.SRM)
                    WriteSRMCTableElement((ushort)value);

                position++;
            }

            Thread.Sleep(200);
        }

        private void WriteCurrentTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CURRENT_TABLE, value);
            Thread.Sleep(200);
        }

        private void WriteTimeTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TIME_TABLE, (ushort)value);
            Thread.Sleep(200);
        }

        private void WriteSRDCTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRD_RECONNECTION_TABLE, value);
            Thread.Sleep(200);
        }

        private void WriteSRMCTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SRM_RECONNECTION_TABLE, value);
            Thread.Sleep(200);
        }

        public string ReadScaleTable(TableKind typeTable)
        {
            LogMethod();

            StringBuilder table = new StringBuilder();

            for (byte pos = 0; pos < 255; pos++)
            {
                var valor = typeTable == TableKind.CURRENT ? ReadCurrentTableElement(pos) : typeTable == TableKind.TIME ? ReadTimeTableElement(pos)
                    : typeTable == TableKind.SRD ? ReadSRDCTableElement(pos) : ReadSRMCTableElement(pos);

                if (valor == 0xFFFF)
                    break;

                table.Append(valor + ";");
            }

            return table.ToString();
        }

        private ushort ReadCurrentTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.CURRENT_TABLE, (ushort)value);

            Thread.Sleep(500);

            var result = Modbus.ReadRegister((ushort)Registers.CURRENT_TABLE);

            Thread.Sleep(500);

            return result;
        }

        private ushort ReadTimeTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TIME_TABLE, (ushort)value);

            Thread.Sleep(500);

            var result = Modbus.ReadRegister((ushort)Registers.TIME_TABLE);

            Thread.Sleep(500);

            return result;
        }

        private ushort ReadSRDCTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.SRD_RECONNECTION_TABLE, (ushort)value);

            Thread.Sleep(500);

            var result = Modbus.ReadRegister((ushort)Registers.SRD_RECONNECTION_TABLE);

            Thread.Sleep(500);

            return result;
        }

        private ushort ReadSRMCTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.SRM_RECONNECTION_TABLE, (ushort)value);

            Thread.Sleep(500);

            var result = Modbus.ReadRegister((ushort)Registers.SRM_RECONNECTION_TABLE);

            Thread.Sleep(500);

            return result;
        }

        //***************************************************************

        #endregion

        public struct OptionsLock
        {
            public bool LockTeclaTretardo { get; set; }
            public bool LockTeclaCorriente { get; set; }
            public bool LockTeclaPolaridad { get; set; }
            public bool LockTeclaRec { get; set; }
            public bool LockMenuFrec { get; set; }
            public bool LockMenuLimits { get; set; }
            public bool LockMenuReset { get; set; }
            public bool LockMenuAlarm { get; set; }
            public bool SRFija { get; set; }
            public bool LockMenuTrip { get; set; }
            public bool LockActivationExternInput { get; set; }

            public ushort OptionsLockUshort()
            {
                var locking = new bool[11] 
                {
                  LockTeclaTretardo,
                  LockTeclaCorriente,
                  LockTeclaPolaridad,
                  LockTeclaRec,
                  LockMenuFrec,
                  LockMenuLimits,
                  LockMenuReset,
                  LockMenuAlarm,
                  SRFija,
                  LockMenuTrip,
                  LockActivationExternInput,        
                };

                return locking.ArrayBoolToRegister();
            }
        }

        public struct ScaleTableConfiguration
        {
            private string ScaleTableName;

            private List<TableElement> listElements;

            public bool HasElements
            {
                get
                {
                    if (listElements == null)
                        return false;

                    return listElements.Any();
                }
            }

            public ScaleTableConfiguration(string scaleElementsString, string scaleTableName)
            {
                ScaleTableName = scaleTableName;

                List<string> scaleElements = scaleElementsString.Split(';').ToList();
                if (scaleElements.Count() > 32)
                    throw new Exception("Error en la configuración de la tabla de reconexión, el numero de elementos está limitado a 32");

                listElements = new List<TableElement>();

                foreach (string scaleElement in scaleElements)
                {
                    int temporalValue = 0;

                    if (!int.TryParse(scaleElement, out temporalValue))
                        throw new Exception(string.Format("Error al configurar la tabla de escalas de {0}, el valor introducido en la base de datos no es un formato válido", scaleTableName));

                    listElements.Add(new TableElement(temporalValue, (byte)listElements.Count()));
                }
            }

            public List<TableElement> ListElemnts { get { return listElements; } }

            public string listElementsString
            {
                get
                {
                    if(ListElemnts ==  null)
                            return string.Empty;

                    StringBuilder list = new StringBuilder();

                    foreach (TableElement element in ListElemnts)
                        list.Append(element.GetElementValue.ToString() + ";");
                    return list.ToString();
                }

            }
        }

        public struct TableElement
        {
            private bool x100;
            private ushort elementValue;
            private byte position;

            public TableElement(int value, byte positionValue)
            {
                if (value > 1024)
                {
                    x100 = true;
                    elementValue = (ushort)(value / 100);
                }
                else
                {
                    x100 = false;
                    elementValue = (ushort)value;
                }
                position = positionValue;
            }

            public bool X100
            {
                get
                {
                    return x100;
                }
            }

            public ushort GetElementValue
            {
                get
                {
                    return (ushort)(elementValue * (x100 ? 100 : 1));
                }
            }

            public ushort ElementValue
            {
                get
                {
                    return (ushort)elementValue;
                }
            }

            public byte Position
            {
                get
                {
                    return position;
                }
            }
        }


        public struct Inputs
        {
            public bool KeyTd { get; set; }
            public bool KeyIN { get; set; }
            public bool KeyStd { get; set; }
            public bool KeyRec { get; set; }
            public bool KeyProg { get; set; }
            public bool KeyTest { get; set; }
            public bool KeyReset { get; set; }
            public bool Magneto { get; set; }
            public bool ExternInput { get; set; }
            public bool Toroidal { get; set; }

            public Inputs(ushort value)
            {
                KeyIN = (value & 0x01) == 0x01 ? true : false;
                KeyStd = (value & 0x02) == 0x02 ? true : false;
                KeyTd = (value & 0x04) == 0x04 ? true : false;
                KeyTest = (value & 0x08) == 0x08 ? true : false;
                KeyProg = (value & 0x10) == 0x10 ? true : false;
                KeyReset = (value & 0x20) == 0x20 ? true : false;
                KeyRec = (value & 0x40) == 0x40 ? true : false;

                Magneto = (value & 0x80) == 0x80 ? true : false;
                ExternInput = (value & 0x200) == 0x200 ? true : false;
                Toroidal = (value & 0x1000) == 0x1000 ? true : false;
            }
        }

        public enum WorkModes
        {
            NORMAL = 0x00,
            CALIB1 = 0x104,
            CALIB2 = 0x204,
            CALIB3 = 0x304,
            FAB = 0X05,
            RESET = 0x0B
        }

        public enum EnableRecordModes
        {
            ENABLE_PARAMS_CONFIG = 0x1111,
            RECOR_PARAMS_CONFIG = 0x2222,
            ENABLE_PARAMS_IDP = 0x3333,
            RECORD_PARAMS_IDP = 0x4444,
            ENABLE_PARAMS_PARAMETRIZATION = 0x5555,
            RECORD_PARAMS_PARAMETRIZATION = 0x6666
        }

        public enum Registers
        {
            PERIFERIC_NUMBER = 0x0000,
            SPEED_COMUNICATION = 0x0001,
            TYPE_OF_PARITY = 0x0002,
            WORKING_FRECUENCY = 0x0003,
            TRIP_CURRENT_PROGRAM = 0x0004,
            TIME_DELAY_CURRENT_TRIP = 0x0005,
            RELAY_SAFETY_TRIP = 0x0006,
            DEVICE_STATUS = 0x0007,
            TOTAL_NUMBER_RECONNECTION = 0x0008,
            SAFETY_RELAY_LOCK = 0x0009,
            RECONNECTION_ENABLED = 0x000A,
            SERIAL_NUMBER = 0x000B,
            VERSION_SOFT = 0x000D,
            MODEL_DEVICE = 0x000E,
            OPTIONS_LOCK = 0x000F,
            DIFERENTIAL_RECONNECTION_SEQUENCE = 0x0010,
            MAGNETO_RECONNECTION_SEQUENCE = 0x0011,
            NUMBER_RECONNECTION_DIFERENTIAL = 0x0012,
            NUMBER_RECONNECTION_MAGNETO = 0x0013,
            STATUS_OUTPUTS_RELAYS = 0x0014,
            CURRENT_LEAK_VALUE = 0x0015,
            CURRENT_TRIP_VALUE = 0x0016,
            RECONNECTION_NUMBER_DIFERENTIAL_TRIP = 0x0017,
            RECONNECTION_NUMBER_MAGNETO_TRIP = 0x0018,
            TRIP_EXTERNAL_RESET = 0x0019,
            ENABLE_RECORDING_VALUES = 0x001A,
            CURRENT_PREALARM_ACTIVATION = 0x001B,
            DELAY_ACTIVATION_PREALARM = 0x001C,
            SECURITY_RELAY_PREALARM = 0x001D,
            RESET_PREALARM = 0x001E,
            LIMIT_SCALES_TIMEANDCURRENT = 0x001F,
            WORK_MODE = 0x0020,
            DELETE_MANUFACTURING_VALUES = 0x0021,
            CURRENT_TABLE = 0x0022,
            TIME_TABLE = 0x0023,
            INPUTS_TEST = 0x0024,
            OUTPUTS_TEST = 0x0025,
            CONSTANT_CALIBRATION_SCALE_1 = 0x0026,
            CONSTANT_CALIBRATION_SCALE_2 = 0x0027,
            CONSTANT_CALIBRATION_SCALE_3 = 0x0028,
            PARAMETERIZATION_CODE = 0x0029,
            SUMMATION_CALIBRATION_HIGH = 0x002A,
            SUMMATION_CALIBRATION_LOW = 0x002B,
            ERROR_CODE_IDP = 0x002C,
            SRD_RECONNECTION_TABLE = 0x002D,
            SRM_RECONNECTION_TABLE = 0x002E,
            SCALE_LIMIT_SELECTION = 0x002F,
            CARACTERS_START_MESSAGE_LOW = 0x0030,
            CARACTERS_START_MESSAGE_HIGH = 0x0031,
            BASTIDOR_NUMBER = 0x0032,
            TRIGGER_TYPE_MAG = 0x0034,
            CRC_CALCULATED = 0x1770
        }

        public enum TableKind
        {
            TIME,
            CURRENT,
            SRD,
            SRM
        }

        public enum Limits
        {
            HIGHT,
            LOW
        }

        public override void Dispose()
        {
            if (modbus != null)
                modbus.Dispose();
        }
    }
}
