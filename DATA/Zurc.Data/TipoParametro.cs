namespace Zurc.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoParametro")]
    public partial class TipoParametro
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Tipo { get; set; }

        [StringLength(100)]
        public string Descripcion { get; set; }

        public virtual List<Parametro> Parametros { get; set; }
    }
}
