﻿using Dezac.Data;
using Dezac.Tests.Model;
using System.Collections.Generic;

namespace Dezac.Tests.Services
{
    public interface ITestContext
    {
        string IdOperario { get; set; }
        string IdEmpleado { get; set; }
        string IdFamilia { get; set; }
        int? NumFamilia { get; set; }
        string DescFamilia { get; set; }
        int NumProducto { get; set; }
        int Version { get; set; }
        string IdFase { get; set; }
        int? NumOrden { get; set; }
        int Nivelcontroltest { get; set; }
        int Cantidad { get; set; }
        int CantidadPendiente { get; set; }

        int IdCajaActual { get; set; }
        int NumCajaActual { get; set; }
        int NumEquiposCaja { get; set; }
        int UdsCajaActual { get; set; }
        string MinSerialNumberBox { get; set; }
        string MaxSerialNumberBox { get; set; }
        List<string> SerialNumberBoxRange { get; set; }
        string Lote { get; set; }
        string ReferenciaLineaCliente { get; set; }
        string ReferenciaOrigen { get; set; }
        string PC { get; set; }
        string LocationTest { get; set; }
        int? NumAsistencia { get; set; }
        int? NumLineaAsistencia { get; set; }
        string NumSerieAsistencia { get; set; }
        bool IsAOITrace { get; set; }
        string EAN { get; set; }
        string EAN_MARCA { get; set; }
        int NumUtilBien { get; set; }
        bool HayUtilBien { get; set; }
        BIEN UtilBien { get; set; }
        int NumMangueraBien { get; set; }
        bool HayMangueraBien { get; set; }
        BIEN MangueraBien { get; set; }
        List<string> Direcciones { get; set; }
        List<string> Atributos { get; set; }
        Dictionary<string, string> DescripcionLenguagesCliente { get; set; }
        string SequenceVersion { get; set; }
        string SequenceName { get; set; }
        string SauronVersion { get; set; }
        string SauronInterface { get; set; }
        bool IsReproceso { get; set; }
        bool MantenerNumSerie { get; set; }
        string Codigo { get; set; }

        ModoTest modoPlayer { get; set; }

        TestInfo TestInfo { get; set; }

        ParamValueCollection Comunicaciones { get; }
        ParamValueCollection Consignas { get; }
        ParamValueCollection Configuracion { get; }
        ParamValueCollection Identificacion { get; }
        ParamValueCollection Margenes { get; }
        ParamValueResultsCollection Resultados { get; }
        ParamValueCollection Parametrizacion { get; }
        ParamValueCollection VectorHardware { get; }
        ParamValueCollection Vision { get; }

        ParamValueCollection EtiquetasEquipo { get; }
        ParamValueCollection EtiquetasNumSerie { get; }
        ParamValueCollection EtiquetasEmbalajeIndividual { get; }
        ParamValueCollection EtiquetasEmbalajeConjunto { get; }
        ParamValueCollection EtiquetasPromocion { get; }

        ConfigurationFilesCollection ConfigurationFiles { get;}

        bool ReloadParametersOnNextTest { get; set; }

        ParamValueCollection GetGroupList(int tipoGrupo);
        ITestContext Clone(bool copyValues);
    }

    public enum ModoTest
    {
        SAT,
        PostVenta,
        AutoTest,
        TIC,
        PrintLabel,
        SinPermisos,
        Normal,
    }
}
