﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WinTestPlayer.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class IndicatorsView : UserControl
    {
        protected AppService app;
        protected ShellService shell;

        private DateTime? testStart;
        private double? tiempoPrevisto;

        public IndicatorsView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            shell = DI.Resolve<ShellService>();
            app = DI.Resolve<AppService>();
            //app.ModelChanged += (s, ee) => { UpdateUI(); };

            app.Runner.AllTestStarting += (s, ee) => { testStart = DateTime.Now; UpdateTimes(); UpdateUI();  };
            app.Runner.AllTestEnded += (s, ee) => { UpdateUI(); };

            UpdateUI();

            timer.Enabled = true;

            bdBoxReport.DataSource = app.Model;
        }

        private void UpdateUI()
        {
            radLabelFecha.Text = DateTime.Now.ToShortDateString();

            TestModel model = app.Model;

            double total = model.Cantidad;

            lblTotal.Text = total.ToString();

            rgOK.RangeEnd = total;
            radialGaugeArc1.RangeEnd = total;
            radialGaugeArc2.RangeEnd = total;

            rgKO.RangeEnd = total;
            radialGaugeArc5.RangeEnd = total;
            radialGaugeArc6.RangeEnd = total;

            rgOK.Value = model.NumOK;
            rgKO.Value = model.NumNG;
        }

        private void UpdateTimes()
        {
            var now = DateTime.Now;

            lblHour.Text = now.ToString("HH:mm:ss");

            if (testStart.HasValue)
            {
                if (app.IsRunning)
                    lblTiempoTest.Text = (now - testStart.Value).ToString(@"mm\:ss");

                var span = (now - testStart.Value);
                
                lblTiempoCiclo.Text = span.ToString(@"mm\:ss");
                lblTiempoTest.ForeColor = tiempoPrevisto.HasValue && span.TotalSeconds > tiempoPrevisto.Value ? Color.Red : Color.Black;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            UpdateTimes();
        }
    }
}
