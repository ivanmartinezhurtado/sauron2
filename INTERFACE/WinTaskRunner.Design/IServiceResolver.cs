﻿namespace WinTaskRunner.Design
{
    public interface IServiceResolver
    {
        T Resolve<T>();
    }
}
