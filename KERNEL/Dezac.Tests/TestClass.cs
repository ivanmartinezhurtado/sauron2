﻿using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using log4net;
using System.Configuration;
using System.Globalization;
using System.Linq;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace Dezac.Tests
{
    public class TestClass : ThreadBase
    {
        protected static readonly ILog logger = LogManager.GetLogger("TEST_UUT");

        public ILog Logger { get { return logger; } }

        public T GetService<T>()
        {
            var context = SequenceContext.Current;

            if (context == null)
                return default(T);

            return context.Services.Get<T>();
        }

        public ITestContext Model
        {
            get
            {
                var model = GetService<ITestContext>();

                if (model != null && model.TestInfo != null && string.IsNullOrEmpty(model.TestInfo.TestVersion))
                {
                    var version = GetTestVersion();
                    if (model.TestInfo != null)
                    {
                        model.TestInfo.TestVersion = version;
                        Resultado.Set("CLASSE_TEST", string.Format("{0}", this.GetType().Name), ParamUnidad.SinUnidad);
                        Resultado.Set("VERSION_TEST", string.Format("{0}", model.TestInfo.TestVersion), ParamUnidad.SinUnidad);
                    }
                }
                return model;
            }
        }

        public IShell Shell { get { return GetService<IShell>(); } }

        public TestInfo TestInfo
        {
            get
            {
                if (Model != null)
                    return Model.TestInfo;

                return null;
            }
        }

        public string PathCameraImages
        {
            get { return ConfigurationManager.AppSettings["PathCameraImages"]; }
        }

        public string PathImageSave
        {
            get { return ConfigurationManager.AppSettings["PathImageTestSave"]; }
        }

        //-------------------------------------------------------------------------

        public ParamValueCollection Margenes { get { return Model.Margenes; } }
        public ParamValueCollection Consignas { get { return Model.Consignas; } }
        public ParamValueResultsCollection Resultado { get { return Model.Resultados; } }
        public ParamValueCollection Configuracion { get { return Model.Configuracion; } }
        public ParamValueCollection Parametrizacion { get { return Model.Parametrizacion; } }
        public ParamValueCollection VectorHardware { get { return Model.VectorHardware; } }
        public ParamValueCollection Vision { get { return Model.Vision; } }

        public ConfigurationFilesCollection ConfigurationFiles { get { return Model.ConfigurationFiles; } }

        private ComunicationParameters comunicaciones;
        public ComunicationParameters Comunicaciones
        {
            get
            {
                if (comunicaciones == null)
                    comunicaciones = new ComunicationParameters(Model.Comunicaciones);

                return comunicaciones;
            }
        }

        private IdentificationParameters identificacion;
        public IdentificationParameters Identificacion
        {
            get
            {
                if (identificacion == null)
                    identificacion = new IdentificationParameters(Model.Identificacion);

                return identificacion;
            }
        }

        private TestParameters parametros;
        public TestParameters Params
        {
            get
            {
                if (parametros == null)
                    parametros = new TestParameters(Model.Margenes);

                return parametros;
            }
        }

        public int NumInstance { get { return SequenceContext.Current.NumInstance; } }

        public void AddResult(string text)
        {
            SequenceContext.Current.ResultList.Add(text);
        }

        public void AddResult(string format, params object[] args)
        {
            AddResult(string.Format(format, args));
        }

        public void AddResultValue(string name, object value)
        {
            AddResult("{0} = {1}", name, value);
        }

        public T GetEnvironmentVariable<T>(string key, T def)
        {
            var iEnvironment = SequenceContext.Current.Services.Get<IEnvironmentService>();
            if (iEnvironment == null)
                return def;

            if (iEnvironment.Environment.ContainsKey(key))
                return RunnerHelper.ChangeType<T>(iEnvironment.Environment[key]);

            return def;
        }

        public string GetTestVersion()
        {
            var version = "SIN VERSIONAR";

            var attr = this.GetType().GetCustomAttributes(typeof(TestVersionAttribute), true).FirstOrDefault() as TestVersionAttribute;
            if (attr != null)
                version = string.Format(new CultureInfo("en-US"), "{0:0.00}", attr.Version);

            return version; 
        }         
    }
}
