﻿using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using System.Windows.Forms;

namespace Dezac.Tests.Forms
{
    public partial class BienInputlView : DialogViewBase
    {
        public BienInputlView(TypeBien tipo, BienClass bien)
        {
            InitializeComponent();

            lblTipoBien.Text = tipo.ToString();
            lblDescripcion.Text = bien.Descripcion;
            pbUtil.Image = bien.imagen;
            txtBien.Focus();
        }

        public int? NumBien { get; set; }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Enter))
            {
                if (string.IsNullOrEmpty(txtBien.Text))
                {
                    MessageBox.Show("NO SE PUEDE DEJAR EN BLANCO ESTE BIEN, PORQUE EXISTE UNO PARA ESTE PRODUCTO FASE", "AVISO BIEN NO PUEDE DEJARSE EN BLANCO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                ValidateBien();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private bool ValidateBien()
        {
            int numBien =0;
            if (!int.TryParse(txtBien.Text, out numBien))
                return false;

            NumBien = numBien;
            return true;
        }
 
        public override bool ValidateView(DialogResult result)
        {
            return NumBien != null;
        }
    }
}
