﻿using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.ComponentModel;

namespace WinTestPlayer.Controls.Expanders
{
    class ChevronButton : LightVisualElement
    {
        static ChevronButton()
        {
            ItemStateManagerFactoryRegistry.AddStateManagerFactory(new ChevronButtonStateManager(), typeof(ChevronButton));
        }

        public static RadProperty IsExpandedProperty = RadProperty.Register(
         "Expanded",
         typeof(bool),
         typeof(ChevronButton),
         new RadElementPropertyMetadata(true));

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool Expanded
        {
            get
            {
                return (bool)GetValue(IsExpandedProperty);
            }
            set
            {
                SetValue(IsExpandedProperty, value);
            }
        }
    }
}
