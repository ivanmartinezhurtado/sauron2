﻿using Circutor.Tests.MyCircutor;
using Dezac.Tests;
using Dezac.Tests.Actions;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Services;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(AFQEvoMyCircutorCheckRegisterActionDescription))]
    public class AFQEvoMyCircutorCheckRegister : ActionBase
    {
        [Description("Valor o variable del UserName para la conexión a MyCircutor")]
        [Category("Login")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string UserName { get; set; }

        [Description("Valor o variable del password para la conexión a MyCircutor")]
        [Category("Login")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Password { get; set; }

        [Description("Valor o variable del Numero de serie del producto para registrarlo en MyCircutor")]
        [Category("Configuracion")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SerialNumber { get; set; }

        public bool DeveloperApi { get; set; }

        public override void Execute()
        {
            var testContext = Context.Services.Get<ITestContext>();
            if (testContext == null)
                throw new Exception("No se ha inicializado el producto!");

            var afqTest = new AFQTest();

            // Url de test
            if (DeveloperApi)
                afqTest.BaseUrl = "https://mycapi-dev.mycircutor.com:8085/mycapi/v1.0.0/";

            afqTest.Login(VariablesTools.ResolveValue(UserName).ToString(), VariablesTools.ResolveValue(Password).ToString());

            var serialNumber = VariablesTools.ResolveValue(SerialNumber).ToString() ?? testContext.TestInfo.NumSerie;

            var checkResult = afqTest.GetCheck(serialNumber);

            Assert.AreEqual(checkResult, "Ok", Error().UUT.COMUNICACIONES.NO_COMUNICA("Verificación de registro de comunicación del equipo con myCircutor incorrecta!"));
        }
    }

    public class AFQEvoMyCircutorCheckRegisterActionDescription : ActionDescription
    {
        public override string Name { get { return "AFQEvoMyCircutorCheckRegister"; } }
        public override string Description { get { return "Action para comprobar que en MyCircutor un producto AFQEvo por su numero de serie se ha registrado correctamente"; } }
        public override string Category { get { return "ZurcTest/Tools"; } }
        public override Image Icon { get { return Properties.Resources.Tools_icon; } }
    }
}
