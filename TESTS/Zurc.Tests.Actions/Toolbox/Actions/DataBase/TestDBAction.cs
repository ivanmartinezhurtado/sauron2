﻿using Dezac.Tests;
using Dezac.Tests.Actions;
using System.Drawing;
using Zurc.Data;

namespace Zurc.Tests.Actions
{
    [ActionVersion(1.13)]
    [DesignerAction(Type = typeof(TestDBActionDescription))]
    public class TestDBAction : ActionBase
    {
        public override void Execute()
        {
            using (var db = new ZurcService())
            {
                db.TestDB();
            }
        }
    }

    public class TestDBActionDescription : ActionDescription
    {
        public override string Name { get { return "TestDB"; } }
        public override string Category { get { return "ZurcTest/Database"; } }
        public override Image Icon { get { return Properties.Resources.DatabaseParameters; } }
    }
}