﻿using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TaskRunner.Enumerators;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.02)]
    [DesignerAction(Type = typeof(NewTestFaseContextActionDescription))]
    public class NewTestFaseContextAction : ActionBase
    {
        public enum RunTestFaseEnum
        {
            Always = 0,
            LastTestIsNotOKToday = 1,
            LastTestIsNotOK = 2,
            PromptIfLastTestIsOK = 3
        }

        [Category("3. Configuration")]
        [Description("Sirve para decir en que fase del test quieres que estén los actions que estarán dentro de este action")]
        public TestInfo.FasesEnum Fase { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve para decir cuando se ha de hacer la secuencia dentro de este test")]
        public RunTestFaseEnum RunActionCondition { get; set; }

        private ITestContext parentContext;

        private bool isOmitted;

        private string fase;

        [Browsable(false)]
        public override bool IsGroupAction => true;
        public override void Execute()
        {
            parentContext = SequenceContext.Current.Services.Get<ITestContext>();
            if (parentContext == null)
                return;

            fase = ((int)Fase).ToString("000");
            isOmitted = CheckStepOmitted();
            if (isOmitted)
                throw new Exception("Fase de test omitida");

            var newContext = parentContext.Clone(false);
            if (!string.IsNullOrEmpty(fase))
                newContext.IdFase = fase;

            List<T_GRUPOPARAMETRIZACION> parametros;
            using (DezacService svc = new DezacService())
            {
                parametros = svc.GetGruposParametrizacion(newContext.NumProducto, newContext.Version, fase);
            }

            parametros.ForEach(p =>
            {
                var list = newContext.GetGroupList(p.NUMTIPOGRUPO);
                if (list != null)
                    foreach (var vp in p.T_VALORPARAMETRO)
                    {
                        ParamValue pv = new ParamValue
                        {
                            IdGrupo = vp.NUMGRUPO,
                            IdTipoGrupo = (TipoGrupo)p.NUMTIPOGRUPO,
                            Grupo = list.Name,
                            IdParam = vp.NUMPARAM,
                            ValorInicio = vp.VALORINICIO,
                            Valor = vp.VALOR,
                            IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                            IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                            Unidad = vp.T_PARAMETRO.T_UNIDAD.UNIDAD,
                            Name = vp.T_PARAMETRO.PARAM
                        };

                        list.Add(pv);
                    }
            });

            SetContext(newContext);
        }
        public override void PostExecute(StepResult stepResult)
        {
            if (isOmitted)
            {
                stepResult.ExecutionResult = StepExecutionResult.Omitted;
                Logger.Info("Fase de test omitida");
            }
            else
            {
                SaveResults.SaveResultsToDDBB(stepResult);
                if (stepResult.ExecutionResult == StepExecutionResult.Failed)
                    stepResult.Exception = new Exception(string.Format("Error en la fase {0}", this.Fase));
                Logger.Info("Resutado del NewTestContext guardados en la BBDD ");
            }
            SetContext(parentContext);
        }
        private void SetContext(ITestContext context)
        {
            if (SequenceContext.Current.Services.Contains<ITestContext>())
                SequenceContext.Current.Services.Remove<ITestContext>();

            if (context != null)
                SequenceContext.Current.Services.Add<ITestContext>(context);
        }
        private bool CheckStepOmitted()
        {
            var run = RunActionCondition == RunTestFaseEnum.Always;
            if (!run && parentContext.TestInfo.NumBastidor.HasValue)
            {
                T_TESTFASE test = null;
                using (DezacService svc = new DezacService())
                {
                    test = svc.GetLastTestFase((int)parentContext.TestInfo.NumBastidor.Value, fase, parentContext.NumOrden.GetValueOrDefault(), null);
                }
                run = test == null;
                if (!run && RunActionCondition == RunTestFaseEnum.LastTestIsNotOK)
                    run = test.RESULTADO != "O";
                else if (!run && RunActionCondition == RunTestFaseEnum.LastTestIsNotOKToday)
                    run = (test.RESULTADO != "O") || (test.FECHA.Date != DateTime.Now.Date);
                else if (!run && RunActionCondition == RunTestFaseEnum.PromptIfLastTestIsOK)
                {
                    run = test.RESULTADO != "O";
                    if (!run)
                    {
                        var iShell = SequenceContext.Current.Services.Get<IShell>();
                        run = iShell.MsgBox(
                            string.Format("El último test de la fase {0} para el equipo con matrícula {1} se ejecutó correctamente.\n¿Desea volver a ejecutarlo?",
                                fase, parentContext.TestInfo.NumBastidor),
                            "ATENCIÓN",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes;
                    }
                }
            }
            return !run;
        }
    }

    public class NewTestFaseContextActionDescription : ActionDescription
    {
        public override string Name { get { return "NewTestFaseContext"; }}
        public override string Category { get { return "DezacTest/GroupActions"; } }
        public override Image Icon { get { return Properties.Resources.GroupAction; } }
        public override string Description { get { return "Action para agrupar otros actions en una nivel inferior (hijos)"; } }
    }
}
