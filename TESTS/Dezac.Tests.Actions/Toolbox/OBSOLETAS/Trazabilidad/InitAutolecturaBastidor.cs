﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(2.00)]
    [DesignerAction(Type = typeof(InitAutolecturaBastidorActionDescription))]
    [Obsolete("Usar la nueva action InitBastidorMultipleAction")]
    
    public class InitAutolecturaBastidor : ActionBase
    {
        [DefaultValue(TestHalconExtension.TypeBarcode.Code_128)]
        public TestHalconExtension.TypeBarcode barCodeType { get; set; }

        [Category("1.- Test Configuration")]
        [DefaultValue((byte)3)]
        public byte retry { get; set; }

        [Category("1.- Test Configuration")]
        [DefaultValue("")]
        public string ledOutput { get; set; }

        [Category("2.- Camera Selection")]
        [Description("Camera Name if has been defined as variable with AddVariable Action")]
        [DefaultValue("")]
        public string VariableName { get; set; }

        [Category("2.- Camera Selection")]
        [Description("Camera Serial Number (not required if has Variable Name)")]
        [DefaultValue("")]
        public string serialNumber { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Row1 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Row1 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Column1 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Column1 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Row2 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Row2 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Column2 for specific area searching (0 means search across all image)")]
        [DefaultValue(0)]
        public int Column2 { get; set; }

        [Category("3.- Crop and Resize image")]
        [Description("Resize factor used to reduce image resolution (1 means no resolution reduction)")]
        [DefaultValue(1)]
        public double ResizeFactor { get; set; }

        public override void Execute()
        {
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null)
                throw new Exception("Falta iniciar una función de carga de datos de contexto del test (InitProduct)");

            if (data.NumOrden != null && data.NumProducto == 0)
                throw new Exception("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test");

            UInt64 bastidor = 0;

            var iShell = SequenceContext.Current.Services.Get<IShell>();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del TestBase");

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                throw new Exception("No se ha llamado al metodo de inicialización de la torre");

            if (!string.IsNullOrEmpty(VariableName))
                serialNumber = GetVariable<string>(VariableName);

            if (!string.IsNullOrEmpty(ledOutput))
            {
                var output = GetPropertyVar<byte>(ledOutput);
                tower.IO.DO.On(output);

                try
                {
                    bastidor = UInt64.Parse(testBase.TestHalconReadBarCodeProcedure(serialNumber, barCodeType, "BARCODE", "BARCODE", 
                        (byte)retry, null, Row1, Column1, Row2, Column2, ResizeFactor));
                }
                finally
                {
                    tower.IO.DO.Off(output);
                }
            }
            else
                bastidor = UInt64.Parse(testBase.TestHalconReadBarCodeProcedure(serialNumber, barCodeType, "BARCODE", "BARCODE", 
                    (byte)retry, null, Row1, Column1, Row2, Column2, ResizeFactor));

            testBase.Resultado.Set("BASTIDOR_AUTOLECTURA", bastidor.ToString(), ParamUnidad.SinUnidad);

            data.TestInfo.NumBastidor = bastidor;
        }
    }

    public class InitAutolecturaBastidorActionDescription : ActionDescription
    {
        public override string Name { get { return "InitAutolecturaBastidor"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.Ecommerce_Barcode_Scanner_icon; } }
    }
}
