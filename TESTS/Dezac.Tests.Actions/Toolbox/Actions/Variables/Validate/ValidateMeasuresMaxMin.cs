﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(ValidateMeasuresMaxMinDescription))]
    public class ValidateMeasuresMaxMin : ActionBase
    {
        [Description("Descripción del nombre de la variable a validar")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInput { get; set; }

        [Category("4. Results")]
        [Description("Descripción de las unidades de la variable a obtener")]
        public ParamUnidad Unit { get; set; }

        [Category("3.1 Margins")]
        [Description("Margen de la medida máxima del test")]
        public ActionVariable Maximum { get; set; } = new ActionVariable();

        [Category("3.1 Margins")]
        [Description("Margen de la medida mínima del test")]
        public ActionVariable Minimum { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            double variable = 0;

            if (Unit != 0)
                variable = Convert.ToDouble(VariablesTools.ResolveValue(DataInput, null, Unit));
            else
                variable = Convert.ToDouble(VariablesTools.ResolveValue(DataInput));

            var name = DataInput.Trim().Replace("$", "");

            if (Maximum == null || Maximum.IsKeyNullOrEmpty)
            {
                if (Unit == 0)
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("FALTA UNIDAD PARA CREAR PARAMETRO EN BBDD").Throw();
                
                var value = testBase.Margenes.GetDouble(string.Format("{0}_MAX", name), 9999999, Unit);

                Maximum = string.Format("$Margenes.{0}_MAX", name);
            }
            if (Minimum == null || Minimum.IsKeyNullOrEmpty)
            {
                if (Unit == 0)
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("FALTA UNIDAD PARA CREAR PARAMETRO EN BBDD").Throw();
               
                var value = testBase.Margenes.GetDouble(string.Format("{0}_MIN", name), -9999999, Unit);

                Minimum = string.Format("$Margenes.{0}_MIN", name);
            }

            var max = Convert.ToDouble(Maximum.Value);
            var min = Convert.ToDouble(Minimum.Value);

            var adjustValueTestPoint = new AdjustValueDef(name, min, max, Unit);
            adjustValueTestPoint.Value = variable;

            testBase.Resultado.Set(adjustValueTestPoint, false, false);

            if (!adjustValueTestPoint.IsCorrect())           
                Error().UUT.MEDIDA.MARGENES(adjustValueTestPoint.Name).Throw();
        }
    }

    public class ValidateMeasuresMaxMinDescription : ActionDescription
    {
        public override string Name { get { return "ValidateMeasuresMaxMin"; } }
        public override string Description { get { return "Acción genérica para comparar variables o resultados de medidas con los márgenes establecidos como Máximos y Mínimos de tiempo"; } }
        public override string Category { get { return "Actions/Variables/Validate"; } }
        public override Image Icon { get { return Properties.Resources.Action; } }
    }
}
