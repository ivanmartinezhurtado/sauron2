namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TIPOEVENTO
    {
        public T_TIPOEVENTO()
        {
            this.T_LOG = new HashSet<T_LOG>();
        }
    
        public string IDTIPO { get; set; }
        public string DESCRIPCION { get; set; }
    
        public virtual ICollection<T_LOG> T_LOG { get; set; }
    }
}
