﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Instruments.Utility;
using Dezac.Core.Utility;
using Instruments.PowerSource;
using System.Threading;
using Instruments.Towers;
using Dezac.Instruments.Utils;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(FlukeDescription))]
    public partial class UC_FLUKE : UserControl, IDisposable
    {
        private TriLineValue Voltage;
        private TriLineValue Current;
        private TriLineValue Desfase;
        private TriLineValue PF;
        private double freq;

        private SynchronizationContext sc;

        private FLUKE6003A fluke;

        public Type InstrumentType
        {
            get
            {
                return typeof(FLUKE6003A);
            }
        }


        public FLUKE6003A Instrument
        {
            get
            {
                if (fluke == null)
                    fluke = new FLUKE6003A();

                return fluke;
            }
            set
            {
                if (fluke != null)
                {
                    fluke.Dispose();
                    fluke = null;
                }
                fluke = value;
            }
        }

        public UC_FLUKE()
        {
            InitializeComponent();      
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string[] Addres = new string[] { "ADDR1", "ADDR2", "ADDR3", "ADDR6" };
            
            sc = SynchronizationContext.Current;

            cmb_Port.Items.AddRange(Addres);
            cmb_Port.SelectedIndex = cmb_Port.FindStringExact("ADDR6");            
        }        

        private void btnRST_Click(object sender, EventArgs e)
        {
            ToggleButtonsState((Button)sender);
            try
            {
                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.Reset(); }));
                LifeMethod("Reseteando", task);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            try
            {               
                if (cmb_Port.SelectedItem == null)
                    errorProvider1.SetError(cmb_Port, "No se ha seleccionado una dirección GPIB");
                else
                {
                    Instrument.PortAddress = Convert.ToByte(cmb_Port.Text.Replace("ADDR", string.Empty));
                    if (ValidatePresets() == true)
                    {
                        ToggleButtonsState((Button)sender);

                        try
                        {
                            var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyPresetsAndWaitStabilisation(Voltage, Current, freq, PF, Desfase); }));
                            LifeMethod("Encendiendo", task);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("Error al encender el FLUKE6003A", ex);
                        }
                        finally
                        {
                            ToggleButtonsState((Button)sender);
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No se ha encontrado la Fuente FLUKE", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            try
            {
                ToggleButtonsState((Button)sender);
                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyOff(); }));
                LifeMethod("Apagando", task);
            }
            catch (Exception ex)
            {
                errorProvider1.SetError(cmb_Port, "Error al apagar la FLUKE");
                Logger.Error("Error al apagar el FLUKE6003A", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }               

        private bool ValidatePresets()
        {
            try
            {
                if (!ValidateVoltageFreq()) return false;
                if (!ValidateCurrent()) return false;
                if (!ValidateDesfase()) return false;
            }
            catch (Exception)
            {
                
            }
            return true;
        }

        private bool ValidateVoltageFreq()
        {
            Voltage = new TriLineValue { L1 = Convert.ToDouble(txtV1.Text), L2 = Convert.ToDouble(txtV2.Text), L3 = Convert.ToDouble(txtV3.Text) };
            freq = Convert.ToDouble(txtFrec.Text == "LINE" ? "0" : txtFrec.Text);      
            return true;
        }

        private bool ValidateCurrent()
        {
            Current = new TriLineValue { L1 = Convert.ToDouble(txtI1.Text), L2 = Convert.ToDouble(txtI2.Text), L3 = Convert.ToDouble(txtI3.Text) };
            return true;
        }

        private bool ValidateDesfase()
        {
            Desfase = new TriLineValue { L1 = Convert.ToDouble(txtP1.Text), L2 = Convert.ToDouble(txtP2.Text), L3 = Convert.ToDouble(txtP3.Text) };
            return true;
        }             

        private void rbI_CheckedChanged(object sender, EventArgs e)
        {
            var rb = (RadioButton)sender;
            var mode = OutputCurrentModes.CURRENT;
            mode = rb.Checked ? OutputCurrentModes.CURRENT : OutputCurrentModes.VOLTAGE_FROM_CURRENT;
            lblCurrent1.Text = lblCurrent2.Text = lblCurrent3.Text = rb.Checked ? "Current" : "Voltage";

            try
            {
                var task = Task.Factory.StartNew((Action)(() => { this.Instrument.SetOutputCurrentMode(mode); }));
                LifeMethod("Enviando modo canal de corriente", task);
            }
            catch
            {
                errorProvider1.SetError(cmb_Port, "Error enviando el modo de salida para el canal de corriente");
            }
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }

    public class FlukeDescription : ControlDescription
    {
        public override string Name { get { return "FLUKE"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.PowerSource; } }
        public override Image IconBig { get { return Properties.Resources.FLUKE_PowerReference; } }
        public override Type SubcomponentType { get { return typeof(FLUKE6003A); } }
    }
}
