using Comunications.Ethernet;
using Dezac.Core.Utility;
using Dezac.Data.ViewModels;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(TftpClientDownloadFileDescription))]
    public class TftpClientDownloadFile : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Fichero que se ha de descargar del servidor")]
        public string DownloadFile { get; set; }

        [Category("3. Configuration")]
        [Description("Camino del cliente")]
        public string ClientPath { get; set; }
       
        [Category("3. Configuration")]
        [Description("Direcci�n IP del servidor")]
        public string IpServer { get; set; }

        public override void Execute()
        {
            IPAddress ipClient;
            TFTPClient tftpClient;

            if(String.IsNullOrEmpty(ClientPath))
                ClientPath = ConfigurationManager.AppSettings["PathTFTPFileDowload"];

            Directory.CreateDirectory(ClientPath);

            if (Context.Variables.ContainsKey("IPClienTFTPtDevice"))
            {
                var ip = Context.Variables.Get("IPClienTFTPtDevice").ToString().Split('.').Select((s) => Convert.ToByte(s)).ToArray();
                ipClient = new IPAddress(ip);
            }
            else
            {
                var ip = IpServer.Split('.').Select((s) => Convert.ToByte(s)).ToArray();
                ipClient = new IPAddress(ip);
            }

            tftpClient = new TFTPClient(ipClient);

            if (String.IsNullOrEmpty(DownloadFile))
                if (Context.Variables.ContainsKey("BinaryItemViewModel"))
                {
                    BinaryItemViewModel fileVersion = Context.Variables.Get("BinaryItemViewModel") as BinaryItemViewModel;
                    DownloadFile = fileVersion.NombreFichero;
                }

            if (File.Exists(Path.Combine(ClientPath, DownloadFile)))
                File.Delete(Path.Combine(ClientPath, DownloadFile));

            tftpClient.StartDownload(ClientPath, DownloadFile);

            while (!tftpClient.TransferSucces && !tftpClient.TransferError)
                Thread.Sleep(100);

            if(tftpClient.TransferError)
                Error().UUT.COMUNICACIONES.ERROR_DOWNLOAD(tftpClient.TftpTransferError.ToString()).Throw();

        }

        public class TftpClientDownloadFileDescription : ActionDescription
        {
            public override string Name { get { return "TFTPClientDownloadFile"; } }
            public override string Category { get { return "Tools/Ethernet/TFTP"; } }
            public override string Description { get { return "Action que sirve para que el cliente descargue un fichero en el servidor TFTP"; } }
            public override Image Icon { get { return Properties.Resources.ftp_icon; } }
        }
    }

}
