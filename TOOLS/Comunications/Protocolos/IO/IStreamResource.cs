using System;

namespace Comunications.Message
{
    public interface IStreamResource : IDisposable
	{
        string PortName { get; set; }

		int InfiniteTimeout { get; }
		
		int ReadTimeout { get; set; }

		int WriteTimeout { get; set; }

        int ReadBufferSize { get; set; }

        void DiscardInBuffer();

		int Read(byte[] buffer, int offset, int count);

        string ReadLineString();

        void WriteLineString(string tx);

		void Write(byte[] buffer, int offset, int count);

        void WriteLineStringAsBytes(string tx, int byteInterval = 50, bool writeCR = true, bool writeLF = false);

        void HandleMessageException(Exception ex);
	}
}
