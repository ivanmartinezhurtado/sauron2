﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Tests;
using System;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.00)]
    public class T_SABT : DeviceBase
    {
        private DeviceDlmsTrifasic meter;

        internal DeviceDlmsTrifasic Meter
        {
            get
            {
                if (meter == null)
                meter = new DeviceDlmsTrifasic();
                //meterModel = new MeterType("310QT1A");

                return meter;
            }
        }

        public void SetPort(int portDevice)
        {
            Meter.SetPort(portDevice, 115200);
            _logger.InfoFormat("New T-SABT quality by  COM PORT:{0}", portDevice);
            Meter.Retries = 3;
        }

        public void StartSession()
        {
            Meter.StartSession();
        }

        public void CloseSession()
        {
            Meter.CloseSession();
        }

        #region Read Parameters

        public string ReadFirmwareVersion()
        {
            return Meter.ReadFirmwareVersion();
        }

        public uint ReadFabricationNumber()
        {
            return Meter.ReadFabricationNumber();
        }

        public uint ReadFabricationStatus()
        {
            return Meter.ReadFabricationStatus();
        }

        public string ReadManufactureCode()
        {
            return Meter.ReadManufactureCode();
        }

        public string ReadPhysycalAdress()
        {
            return Meter.ReadPhysycalAdress();
        }

        public DateTime ReadDateTime()
        {
            return Meter.ReadDateTime();
        }

        public string ReadModel()
        {
            return Meter.ReadModel();
        }

        public string ReadSerialNumber()
        {
            return Meter.ReadSerialNumber();
        }

        public uint ReadCrcBinary()
        {
            return Meter.ReadCrcBinary();
        }

        #endregion

        #region Write Parameters

        public void WriteFabricationNumber(uint bastidor)
        {
            Meter.WriteFabricationNumber(bastidor);
        }

        public void WriteFabricationStatus(ushort coderror)
        {
            Meter.WriteFabricationStatus(coderror);
        }

        public void WriteManufactureCode(string codeManufacture)
        {
            Meter.WriteManufactureCode(codeManufacture);
        }

        public void WriteDateTimeCurrent()
        {
            Meter.WriteDateTimeCurrent();
        }

        public void WriteModel(string model)
        {
            Meter.WriteModel(model);
        }

        public void WriteSerialNumber(string serialNumber)
        {
            Meter.WriteSerialNumber(serialNumber);
        }

        public void WriteMDateTimeManufacture(DateTime date)
        {
            Meter.WriteMDateTimeManufacture(date);
        }

        public void WriteDateTimeVerification(DateTime date)
        {
            Meter.WriteDateTimeVerification(date);
        }

        public void WritePartNumber(string productoVersion)
        {
            Meter.WritePartNumber(productoVersion + "            ");
        }

        #endregion

        public void WriteLeds(ushort leds)
        {
            Meter.WriteLeds(leds);
        }

        public void WritePPM(short ppmValue)
        {
            Meter.WritePPM(ppmValue);
        }

        #region Write Adjust Default

        public void WriteAdjustFactorsDefualt()
        {
            Meter.WriteOffset<int>(TipoPotencia.Activa, 0);

            //Meter.WriteOffset<int>(TipoPotencia.Reactiva, 0);

            Meter.WriteVoltageGain<ushort>(3018);

            Meter.WriteGapGain<short>(TypeMeterConection.frequency._50Hz, 0);

            Meter.WritePowerGain<ushort>(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa, Convert.ToUInt16(35594));

            //Meter.WritePowerGain<ushort>(TypeMeterConection.frequency._50Hz, TipoPotencia.Reactiva, Convert.ToUInt16(35594));
        }

        #endregion

        #region Adjust Calibration

        public TriLineValue AdjustVoltage(double voltageRef = 300)
        {
            var defs = new TriLineValue();

            var result = Meter.AdjustVoltage(voltageRef, (value) =>
            {
                defs.L1 = value.L1;
                defs.L2 = value.L2;
                defs.L3 = value.L3;
                return true;
            });

            return defs;
        }

        public void WriteVoltageGain(ushort gainL1, ushort gainL2, ushort gainL3)
        {
            var resultadoAjuste = new TriLineGenericValue<ushort>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };      
            this.Meter.WriteVoltageGain(resultadoAjuste);
        }

        public TriLineValue AdjustGap(double defase = 60, double desfaseFactor = -22.2)
        {
            var defs = new TriLineValue();

            var result = Meter.AdjustGap(TypeMeterConection.frequency._50Hz, defase, desfaseFactor, (value) =>
            {
                defs.L1= value.L1;
                defs.L2 = value.L2;
                defs.L3 = value.L3;
                return true;
            });

            return defs;
        }

        public void WriteGapGain(short gainL1, short gainL2, short gainL3)
        {
            var resultadoAjuste = new TriLineGenericValue<short>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };

            Meter.WriteGapGain(TypeMeterConection.frequency._50Hz, resultadoAjuste);
        }

        public TriLineValue AdjustPowerActive(double voltage = 230, double current = 1, double defase = 0)
        {
            var powerReference = voltage * current * Math.Cos(defase.ToRadians());

            var defs = new TriLineValue();

            var result = Meter.AdjustPowerActive(TypeMeterConection.frequency._50Hz, powerReference, (value) =>
            {
                defs.L1 = value.L1;
                defs.L2 = value.L2;
                defs.L3 = value.L3;
                return true;
            });

            return defs;
        }

        public void WritePowerGain(ushort gainL1, ushort gainL2, ushort gainL3)
        {
            var resultadoAjuste = new TriLineGenericValue<ushort>()
            {
                L1 = gainL1,
                L2 = gainL2,
                L3 = gainL3,
            };

            Meter.WritePowerGain(TypeMeterConection.frequency._50Hz, TipoPotencia.Activa, resultadoAjuste);
        }

        public TriLineValue ReadPowerActive(TypeMeterConection.frequency freq)
        {
            var powerActive = this.Meter.ReadPower(freq, TipoPotencia.Activa);
            return powerActive;
        }

        public TriLineValue ReadPowerReactive(TypeMeterConection.frequency freq)
        {
            var powerReactive = this.Meter.ReadPower(freq, TipoPotencia.React);
            return powerReactive;
        }

        #endregion

        public override void Dispose()
        {
            if (meter != null)
                meter.Dispose();                        
         }
    }
}
