namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class OPERARIO
    {
        public OPERARIO()
        {
            this.ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            this.DIARIOMARCAJE = new HashSet<DIARIOMARCAJE>();
            this.DIARIOMARCAJEFASE = new HashSet<DIARIOMARCAJEFASE>();
            this.ESTRUCTURAMATRICULA = new HashSet<ESTRUCTURAMATRICULA>();
            this.ORDENPRODUCTOERROR = new HashSet<ORDENPRODUCTOERROR>();
            this.T_LOG = new HashSet<T_LOG>();
            this.T_TESTFASE = new HashSet<T_TESTFASE>();
            this.DIARIOMARCAJE1 = new HashSet<DIARIOMARCAJE>();
        }
    
        public string IDOPERARIO { get; set; }
        public string IDEMPLEADO { get; set; }
        public string IDCATEGORIA { get; set; }
        public Nullable<decimal> PRECIO { get; set; }
        public string VARIASORDENES { get; set; }
        public decimal PRODUCTIVIDAD { get; set; }
    
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }
        public virtual ICollection<DIARIOMARCAJE> DIARIOMARCAJE { get; set; }
        public virtual ICollection<DIARIOMARCAJEFASE> DIARIOMARCAJEFASE { get; set; }
        public virtual ICollection<ESTRUCTURAMATRICULA> ESTRUCTURAMATRICULA { get; set; }
        public virtual ICollection<ORDENPRODUCTOERROR> ORDENPRODUCTOERROR { get; set; }
        public virtual ICollection<T_LOG> T_LOG { get; set; }
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }
        public virtual ICollection<DIARIOMARCAJE> DIARIOMARCAJE1 { get; set; }
    }
}
