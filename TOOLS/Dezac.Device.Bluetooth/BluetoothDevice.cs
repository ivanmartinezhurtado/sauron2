﻿using Dezac.Device.Bluetooth.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.Storage.Streams;

namespace Dezac.Device.Bluetooth
{
    public class BluetoothDevice : IDisposable
    {
        protected static readonly ILog _logger = LogManager.GetLogger("BluetoothDevice");

        protected BluetoothLEDevice transport;

        public event EventHandler<BluetoothValueChanged> OnNotifyValueChanged;

        public int TimeoutTx = 20000;

        public int TimeoutRx = 20000;

        public string PIN = "";

        private bool IsPair = false;

        protected void LogMethod()
        {
            var stack = new System.Diagnostics.StackTrace();

            var frame = stack.GetFrame(1);

            if (frame != null)
               _logger.InfoFormat(frame.GetMethod().Name);
        }

        public BluetoothDevice()
        {
        }

        public BluetoothDevice(ulong address)
            : this()
        {
            BluetoothAddress = address;
        }

        public BluetoothDevice(string name)
            : this()
        {
            Name = name;
        }

        public ulong BluetoothAddress
        {
            get { return transport.BluetoothAddress; }
            set
            {
                transport = BluetoothLEDevice.FromBluetoothAddressAsync(value).AsTask().Result;
                //svcCache.Clear();
            }
        }

        public string Name
        {
            get { return transport.Name; }
            set
            {
                Close();

                var device = BluetoothHelper.Scan(value);
                if (device != null)
                    BluetoothAddress = device.BluetoothAddress;
            }
        }

        private List<GattDeviceService> cachedSvc = new List<GattDeviceService>();
        private List<GattCharacteristic> cachedChr = new List<GattCharacteristic>();

        public string GetDeviceNameAsync()
        {
            return ReadString(GattServiceUuids.GenericAccess, GattCharacteristicUuids.GapDeviceName);
        }

        public string GetManufacturerNameAsync()
        {
            return ReadString(GattServiceUuids.DeviceInformation, GattCharacteristicUuids.ManufacturerNameString);
        }

        public string GetModelNumberAsync()
        {
            return ReadString(GattServiceUuids.DeviceInformation, GattCharacteristicUuids.ModelNumberString);
        }

        public string GetSerialNumberAsync()
        {
            return ReadString(GattServiceUuids.DeviceInformation, GattCharacteristicUuids.SerialNumberString);
        }

        public string GetHardwareRevisionAsync()
        {
            return ReadString(GattServiceUuids.DeviceInformation, GattCharacteristicUuids.HardwareRevisionString);
        }

        public string GetFirmwareRevisionAsync()
        {
            return ReadString(GattServiceUuids.DeviceInformation, GattCharacteristicUuids.FirmwareRevisionString);
        }

        public byte GetBatteryLevelAsync()
        {
            return ReadByte(GattServiceUuids.Battery, GattCharacteristicUuids.BatteryLevel);
        }

        public List<BluetoothService> GetServicesAsync()
        {
            LogMethod();
            var result = transport.GetGattServicesAsync(BluetoothCacheMode.Uncached).AsTask().Result;
            _logger.InfoFormat("GetGattServicesAsync result {0}", result.Status);
            if (result.Status != GattCommunicationStatus.Success)
                return null;

            var services = new List<BluetoothService>();

            foreach (var p in result.Services)
            {
                _logger.InfoFormat("GattDeviceService atribute:{0} uid:{1}", p.AttributeHandle, p.Uuid);

                var svc = new BluetoothService { AttributeHandle = p.AttributeHandle, Uuid = p.Uuid };

                var characteristics = p.GetCharacteristicsAsync(BluetoothCacheMode.Uncached).AsTask().Result;

                foreach (var character in characteristics.Characteristics)
                {
                    _logger.InfoFormat("GattCharacteristics userDescription:{0} uid:{1}", character.UserDescription, character.Uuid);

                    svc.Characteristics.Add(new BluetoothCharacteristic { Uuid = character.Uuid });
                }

                services.Add(svc);
            }

            return services;
        }

        private GattDeviceService GetService(Guid uuid)
        {
            LogMethod();

            var svc = cachedSvc.Find(p => p.Uuid == uuid);
            if (svc != null)
            {
                _logger.InfoFormat("Service UUID: {0}, Recovered from cache", svc.Uuid);
                return svc;
            }

            Thread.Sleep(1000);
            var result = transport.GetGattServicesAsync(BluetoothCacheMode.Uncached).AsTask().Result;
            if (result.Status == GattCommunicationStatus.Success)
            {
                svc = result.Services.Where(p => p.Uuid == uuid).FirstOrDefault();
                _logger.InfoFormat("Service UUID: {0}, Status: {1}", svc.Uuid, result.Status);
                _logger.DebugFormat("Obtained with GetGattServicesAsync");
            }

            if (svc == null)
            {
                Thread.Sleep(1000);
                result = transport.GetGattServicesForUuidAsync(uuid).AsTask().Result;
                if (result.Status == GattCommunicationStatus.Success)
                {
                    svc = result.Services.Where(p => p.Uuid == uuid).FirstOrDefault();
                    _logger.InfoFormat("Service UUID: {0}, Status: {1}", svc.Uuid, result.Status);
                    _logger.DebugFormat("Obtained with GetGattServicesForUuidAsync");
                }
                else
                    _logger.WarnFormat("Service UUID: {0}, Status: {1}", uuid, result.Status);
            }

            if (svc != null)
            {
                cachedSvc.Add(svc);
                _logger.DebugFormat("Service UUID:{0}, Has been cached", svc.Uuid);
                return svc;
            }

            throw new Exception("Service not found or access denied!");        
        }

        private GattCharacteristic GetCharacteristic(Guid serviceUuid, Guid characteristicUuid)
        {
            LogMethod();

            var characteristic = cachedChr.Find(p => p.Uuid == characteristicUuid);
            if (characteristic != null)
            {
                _logger.InfoFormat("Characteristic {0} - UUID: {1}, Recovered from cache", characteristic.UserDescription, characteristic.Uuid);
                return characteristic;
            }

            var svc = GetService(serviceUuid);

            Thread.Sleep(1000);
            var result = svc.GetCharacteristicsAsync(BluetoothCacheMode.Uncached).AsTask().Result;
            if (result.Status == GattCommunicationStatus.Success)
            {
                characteristic = result.Characteristics.Where(p => p.Uuid == characteristicUuid).FirstOrDefault();
                _logger.InfoFormat("Characteristic {0} - UUID: {1}, Status: {2}", characteristic.UserDescription, characteristic.Uuid, result.Status);
                _logger.DebugFormat("Obtained with GetCharacteristicsAsync");
            }

            if (characteristic == null)
            {
                Thread.Sleep(1000);
                result = svc.GetCharacteristicsForUuidAsync(characteristicUuid, BluetoothCacheMode.Uncached).AsTask().Result;

                if (result.Status == GattCommunicationStatus.Success)
                {
                    characteristic = result.Characteristics.FirstOrDefault();
                    _logger.InfoFormat("Characteristic {0} - UUID: {1}, Status: {2}", characteristic.UserDescription, characteristic.Uuid, result.Status);
                    _logger.DebugFormat("Obtained with GetCharacteristicsForUuidAsync");
                }
                else
                    _logger.WarnFormat("Characteristic UUID: {0}, Status: {1}", characteristicUuid, result.Status);
            }

            if (characteristic != null)
            {
                cachedChr.Add(characteristic);
                _logger.DebugFormat("Characteristic {0} - UUID: {1}, Has been cached", characteristic.UserDescription, characteristic.Uuid);
                return characteristic;
            }

            throw new Exception("Characteristic not found or access denied!");
        }

        public DataReader InternalReadTimeoutController(Guid serviceUuid, Guid characteristicUuid, bool paring)
        {
            //TODO añadir timeout, control de riententos aqui

            var readTask = InternalRead(serviceUuid, characteristicUuid, paring);
            return readTask;
        }

        public DataReader InternalRead(Guid serviceUuid, Guid characteristicUuid, bool paring)
        {
            GattReadResult value;

            LogMethod();
            _logger.InfoFormat("BT Read: Servicio: {0}, Caracteristica: {1}", serviceUuid.ToString(), characteristicUuid.ToString());
            if (paring)
                value = InternalReadWithPairing(serviceUuid, characteristicUuid);
            else
            {
                var characteristic = GetCharacteristic(serviceUuid, characteristicUuid);

                Thread.Sleep(1000);
                value = characteristic.ReadValueAsync(BluetoothCacheMode.Uncached).AsTask().Result;
                if (value.Status != GattCommunicationStatus.Success)
                    throw new Exception(String.Format("Error: BT Read, no se ha podido leer el valor en Servicio {0}, Caracteristica: {1}, Status: {2}", serviceUuid, characteristicUuid, value.Status));
            }

            DataReader result = DataReader.FromBuffer(value.Value);
            result.ByteOrder = ByteOrder.LittleEndian;

            return result;
        }

        public GattReadResult InternalReadWithPairing(Guid serviceUuid, Guid characteristicUuid, byte retry = 2)
        {
            GattCharacteristic characteristic;

            characteristic = GetCharacteristic(serviceUuid, characteristicUuid);

            Thread.Sleep(1000);
            var presult = PairAsync(PIN);

            GattReadResult value = null;
            try
            {
                value = characteristic.ReadValueAsync(BluetoothCacheMode.Uncached).AsTask().Result;
                if (value.Status != GattCommunicationStatus.Success)
                    throw new Exception(String.Format("Error: No se ha podido leer el valor - Status {0}", value.Status.ToString()));          
            }
            catch (Exception ex)
            {
                if (retry > 0)
                {
                    _logger.WarnFormat("Error: BT Read, servicio {0}, caracteristica {1}, reintentos restantes {2}", serviceUuid, characteristicUuid, retry--);
                    Thread.Sleep(1000);
                    presult = UnpairAsync();
                    Thread.Sleep(1000);
                    return InternalReadWithPairing(serviceUuid, characteristicUuid, retry--);
                }
                _logger.WarnFormat("Error: BT Read, no se ha podido leer el servicio {0}, caracteristica {1}", serviceUuid, characteristicUuid);
                throw ex;  
            }

            presult = UnpairAsync();

            return value;
        }

        public bool InternalWriteTimeoutController(Guid serviceUuid, Guid characteristicUuid, DataWriter writer)
        {
            //TODO añadir timeout, control de riententos aqui
            var writeTask = InternalWrite(serviceUuid, characteristicUuid, writer);
            return writeTask;
        }

        public bool InternalWrite(Guid serviceUuid, Guid characteristicUuid, DataWriter writer, byte retry = 2)
        {
            GattCharacteristic characteristic;
            GattCommunicationStatus result;

            LogMethod();
            _logger.InfoFormat("BT Write: Servicio: {0}, Caracteristica: {1}", serviceUuid.ToString(), characteristicUuid.ToString());

            var tmpPair = IsPair;
            if (!IsPair)
                PairAsync(PIN);

            characteristic = GetCharacteristic(serviceUuid, characteristicUuid);

            try
            {
                result = characteristic.WriteValueAsync(writer.DetachBuffer()).AsTask().Result;
            }
            catch (Exception ex)
            {
                if (retry > 0)
                {
                    _logger.WarnFormat("Error: BT Write, servicio {0}, caracteristica {1}, reintentos restantes {2}", serviceUuid, characteristicUuid, retry--);
                    Thread.Sleep(1000);
                    var presult = UnpairAsync();
                    Thread.Sleep(1000);
                    return InternalWrite(serviceUuid, characteristicUuid, writer, retry);
                }
                _logger.WarnFormat("Error: BT Write, no se ha podido escribir el servicio {0}, caracteristica {1}", serviceUuid, characteristicUuid);
                throw ex;
            }

            if (!tmpPair && IsPair)
                UnpairAsync();

            return result == GattCommunicationStatus.Success;
        }

        public byte[] ReadBytes(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = InternalReadTimeoutController(serviceUuid, characteristicUuid, pairing);
            if (readResult == null)
                return null;

            var data = new byte[readResult.UnconsumedBufferLength];
            readResult.ReadBytes(data);

            return data;
        }

        public byte ReadByte(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = InternalReadTimeoutController(serviceUuid, characteristicUuid, pairing);
            var result = readResult.ReadByte();
            _logger.InfoFormat("BT - RX: {0}", result);
            return result; 
        }

        public UInt16 ReadUInt16(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = InternalReadTimeoutController(serviceUuid, characteristicUuid, pairing);
            var result = readResult.ReadUInt16();
            _logger.InfoFormat("BT - RX: {0}", result);
            return result;
        }

        public Int16 ReadInt16(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = InternalReadTimeoutController(serviceUuid, characteristicUuid, pairing);
            var result = readResult.ReadInt16();
            _logger.InfoFormat("BT - RX: {0}", result);
            return result;
        }

        public Int32 ReadInt32(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = InternalReadTimeoutController(serviceUuid, characteristicUuid, pairing);
            var result = readResult.ReadInt32();
            _logger.InfoFormat("BT - RX: {0}", result);
            return result;
        }

        public UInt32 ReadUInt32(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = InternalReadTimeoutController(serviceUuid, characteristicUuid, pairing);
            var result = readResult.ReadUInt32();
            _logger.InfoFormat("BT - RX: {0}", result);
            return result;
        }

        public string ReadString(Guid serviceUuid, Guid characteristicUuid, bool pairing = false)
        {
            var readResult = ReadBytes(serviceUuid, characteristicUuid, pairing);
            if (readResult == null)
                return null;

            var result = Encoding.UTF8.GetString(readResult);
            _logger.InfoFormat("BT - RX: {0}", result);
            return result;
        }

        public bool Write(Guid serviceUuid, Guid characteristicUuid, byte[] data)
        {
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;

            writer.WriteBytes(data);

            var result = InternalWriteTimeoutController(serviceUuid, characteristicUuid, writer);
            return result;
        }

        public bool Write(Guid serviceUuid, Guid characteristicUuid, byte value)
        {
            _logger.InfoFormat("BT - TX: {0}", value);

            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;
            writer.WriteByte(value);

            var result = InternalWriteTimeoutController(serviceUuid, characteristicUuid, writer);
            return result;
        }

        public bool Write(Guid serviceUuid, Guid characteristicUuid, ushort value)
        {
            _logger.InfoFormat("BT - TX: {0}", value);

            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;

            writer.WriteUInt16(value);

            var result = InternalWriteTimeoutController(serviceUuid, characteristicUuid, writer);
            return result;
        }

        public bool Write(Guid serviceUuid, Guid characteristicUuid, int value)
        {
            _logger.InfoFormat("BT - TX: {0}", value);

            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;

            writer.WriteInt32(value);

            var result = InternalWriteTimeoutController(serviceUuid, characteristicUuid, writer);
            return result;
        }

        public bool Write(Guid serviceUuid, Guid characteristicUuid, UInt32 value)
        {
            _logger.InfoFormat("BT - TX: {0}", value);

            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;

            writer.WriteUInt32(value);

            var result = InternalWriteTimeoutController(serviceUuid, characteristicUuid, writer);
            return result;
        }
  
        public bool Write(Guid serviceUuid, Guid characteristicUuid, string value)
        {
            _logger.InfoFormat("BT - TX: {0}", value);

            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.LittleEndian;

            writer.WriteString(value);

            var result = InternalWriteTimeoutController(serviceUuid, characteristicUuid, writer);
            return result;
        }

        public virtual void EnableNotifications(Guid serviceUuid, Guid characteristicUuid)
        {
            LogMethod();
            var characteristic = GetCharacteristic(serviceUuid, characteristicUuid);

            characteristic.ValueChanged += NotifyValueChanged;
            GattCommunicationStatus status = characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue.Notify).AsTask().Result;
        }

        public virtual void DisableNotifications(Guid serviceUuid, Guid characteristicUuid)
        {
            LogMethod();
            var characteristic = GetCharacteristic(serviceUuid, characteristicUuid);

            characteristic.ValueChanged -= NotifyValueChanged;
            GattCommunicationStatus status = characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue.None).AsTask().Result;
        }

        private void NotifyValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            LogMethod();
            var data = new byte[args.CharacteristicValue.Length];
            DataReader.FromBuffer(args.CharacteristicValue).ReadBytes(data);

            var e = new BluetoothValueChanged { Characteristic = sender.Uuid, Data = data };

            if (OnNotifyValueChanged != null)
                OnNotifyValueChanged(this, e);
        }

        public bool PairAsync(string pin)
        {
            if (string.IsNullOrEmpty(pin))
                throw new Exception("BT Error: No se ha configurado el PIN para hacer un emparejamiento");

            LogMethod();
            ClearCache();

            var holder = new PairingRequest(pin);

            var customPairing = transport.DeviceInformation.Pairing.Custom;

            customPairing.PairingRequested += holder.PairingRequested;

            var result = customPairing.PairAsync(DevicePairingKinds.ProvidePin).AsTask().Result;

            customPairing.PairingRequested -= holder.PairingRequested;

            _logger.InfoFormat("Paring status: {0}", result.Status);

            IsPair = result.Status == DevicePairingResultStatus.Paired || result.Status == DevicePairingResultStatus.AlreadyPaired;

            return IsPair;
        }

        public bool UnpairAsync()
        {
            LogMethod();
            ClearCache();

            try
            {
                var result = transport.DeviceInformation.Pairing.UnpairAsync().AsTask().Result;
                _logger.InfoFormat("Unparing status: {0}", result.Status);
                var unpaired = result.Status == DeviceUnpairingResultStatus.Unpaired;
                IsPair = false;
                return unpaired;
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.Message);

                if (ex.InnerException != null)
                //Debug.WriteLine(ex.InnerException);
                    _logger.InfoFormat("Unparing failed, Exception:{0}. InnerException:{1}", ex.Message, ex.InnerException.Message);
                else
                    _logger.InfoFormat("Unparing failed, Exception:{0}. InnerException:{1}", ex.Message, ex.InnerException.Message);

                return false;
            }
        }

        public void Close()
        {
            LogMethod();
            
            if (transport != null)
            {
                transport.Dispose();
                transport = null;
            }
        }

        private void ClearCache()
        {
            _logger.DebugFormat("Clearing Bluetooth cache");
            cachedSvc.Clear();
            cachedChr.Clear();
        }

        public void Dispose()
        {
            Close();
        }
    }

    public class BluetoothService
    {
        public System.UInt16 AttributeHandle { get; internal set; }
        public Guid Uuid { get; internal set; }
        public List<BluetoothCharacteristic> Characteristics { get; internal set; }

        public BluetoothService()
        {
            Characteristics = new List<BluetoothCharacteristic>();
        }
    }

    public class BluetoothCharacteristic
    {
        public Guid Uuid { get; internal set; }
    }

    internal class PairingRequest
    {
        public string Pin { get; set; }

        public PairingRequest(string pin)
        {
            this.Pin = pin;
        }
        
        public  void PairingRequested(DeviceInformationCustomPairing sender, DevicePairingRequestedEventArgs args)
        {
            args.Accept(Pin);
        }
    }

    public class BluetoothValueChanged
    {
        public Guid Characteristic { get; set; }
        public byte[] Data { get; set; }
    }
}
