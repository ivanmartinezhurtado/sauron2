﻿using Comunications.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Dezac.Device.ThermalStations
{
    [DeviceVersion(1.00)]
    public class KSP : DeviceBase
    {
        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public KSP()
        {
        }

        public KSP(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = false;
            Modbus.PerifericNumber = 1;
        }

        public KSP(SerialPort SerialPort)
        {
            SetSerialPort(SerialPort);
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public void SetSerialPort(SerialPort SerialPort)
        {
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }
        

        #region Write Functions

        public void FlagTest()
        {
            LogMethod();

            Modbus.WriteMultipleCoil((ushort)Registers.FLAG_TEST, new bool[]{true});
        }

        public void ActivateRelay(Relays relay)
        {
            LogMethod();
            foreach (KeyValuePair<Relays, bool[]> currentRelays in RelaysAndValues)
                if (currentRelays.Key == relay)
                    Modbus.WriteMultipleCoil((ushort)Registers.RELAYS, currentRelays.Value);
        }

        public void DesactivateRelays()
        {
            LogMethod();
            ActivateRelay(Relays.ALL_OFF);
        }

        public void WriteMotorMode(MotorModes mode)
        {
            LogMethod();
            foreach (KeyValuePair<MotorModes, bool[]> currentMotorMode in MotorModesAndValues)
                if (currentMotorMode.Key == mode)
                    Modbus.WriteMultipleCoil((ushort)Registers.MOTOR_MODE, currentMotorMode.Value);
        }

        public void InitGrids()
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)AddresGrids.ADDR_GRID1, new bool[] { false, false, false, false, false, false });
        }

        public void InitLeds()
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.CONTROL_LEDS, new bool[] { false, false });
            Thread.Sleep(200);
            Modbus.WriteMultipleCoil((ushort)Registers.LEDS, new bool[] { true, true, true, true, true, true, true, true });
        }

        public void ActivateGrid(AddresGrids grid)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)grid, new bool[]{ true });
        }

        public void DesactivateGrid(AddresGrids grid)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)grid, new bool[] { false });
        }

        public void WriteLedsMode(LedsGridsMode mode)
        {
            LogMethod();
            foreach (KeyValuePair<LedsGridsMode, bool[]> currentLedMode in ControlLedsAndValues)
                if (currentLedMode.Key == mode)
                    Modbus.WriteMultipleCoil((ushort)Registers.CONTROL_LEDS, currentLedMode.Value);
        }

        public void DesactivateControlLedsGrids()
        {
            Modbus.WriteMultipleCoil((ushort)Registers.CONTROL_LEDS, new bool[] { false, true });
        }

        public void ActivateLed(Leds led)
        {
            LogMethod();
            foreach (KeyValuePair<Leds, bool[]> currentLed in LedsAndValues)
                if (currentLed.Key == led)
                    Modbus.WriteMultipleCoil((ushort)Registers.LEDS, currentLed.Value);
        }

        public void WriteTemperaturePatterns(TemperaturePatterns temp)
        {
            LogMethod();

            Modbus.Write<TemperaturePatterns>(temp);
        }
       
        public void WriteTemporizators(Temporizators temp)
        {
            LogMethod();
            Modbus.Write<Temporizators>(temp);
        }

        public void WriteIdentificationNumber(int value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ID_NUMBER, (ushort)value);
        }

        public void WriteSerialNumber(int SN)
        {
            LogMethod();

            var custom = new Customize();
            custom.CodeError = 0;
            custom.NumSerie = SN;
            custom.unknow = 0;

            Modbus.Write<Customize>(custom);
        }

        public Tuple<bool, double> Calibration(int valueRntc, int delFirst, int initCount, int samples, int interval, Func<double, bool> adjustValidation)
        {
            FlagTest();
            var result = new double();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 30,
                () =>
                {
                    var temperatureReading = ReadTemperature();
                    return new double[] { temperatureReading };
                },
                (listValues) =>
                {
                    result = (valueRntc * (1024 - listValues.Average(0))) / listValues.Average(0);
                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }

        public void WriteFrecuencyWorker(RadioFrequency RF)
        {
            LogMethod();

            bool state = RF == RadioFrequency._434MHz;
            Modbus.WriteSingleCoil((ushort)Registers.FREQUENCY_WORK, state);      
        }

        public void ResetCounters()
        {
            LogMethod();
            var counters = new ContadoresZonas(0);

            Modbus.Write<ContadoresZonas>(counters);
        }

        public void Reset()
        {
            LogMethod();
            try 
            { 
                Modbus.WriteSingleCoil((ushort)Registers.RESET, true); 
            }
            catch (TimeoutException) { }
        }
 
        #endregion

        #region Read Functions

        public bool ReadFlagTest()
        {
            LogMethod();
            var flagTest = Modbus.ReadMultipleCoil((ushort)Registers.FLAG_TEST, 1);
            return flagTest[0] == 1;
        }

        public ushort ReadIdentificationNumber()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ID_NUMBER);
        }

        public string ReadSoftwareVersion()
        {
            LogMethod();
            var reading = Modbus.ReadMultipleCoil((ushort)Registers.SOFTWARE_VERSION, 64);

            char[] charArray = new char[Encoding.ASCII.GetCharCount(reading, 0, reading.Length)];
            Encoding.ASCII.GetChars(reading, 0, reading.Length, charArray, 0);

            return new String(charArray).Trim();
        }

        public ushort ReadPositionSwitchRotative()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Registers.ROTATIVE_SWITCH, 6)[0];
        }

        public ushort ReadPositionSwitch()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Registers.SWITCH, 4)[0];
        }

        public ushort ReadTemperature()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TEMPERATURE);
        }

        public ushort[] ReadCounters()
        {
            LogMethod();
            return Modbus.ReadMultipleRegister((ushort)Registers.COUNTERS_AREAS, 6);
        }

        public Alarms ReadAlarm()
        {
            LogMethod();
            return (Alarms)Modbus.ReadRegister((ushort)Registers.ALARMS);
        }

        public TemperaturePatterns ReadTemperaturePatterns()
        {
            LogMethod();

            return Modbus.Read<TemperaturePatterns>();
        }

        public Temporizators ReadTemporizators()
        {
            LogMethod();

            return Modbus.Read<Temporizators>();
        }

        public string ReadSerialNumber()
        {
            LogMethod();

            var custom = new Customize();
            custom = Modbus.Read<Customize>();

            return custom.NumSerie.ToString();
        }

        #endregion

        #region Dictionarys
        private Dictionary<Relays, bool[]> RelaysAndValues = new Dictionary<Relays, bool[]>()
        {
            {Relays.RELAY_Y, new bool[] { true, false} },
            {Relays.RELAY_G, new bool[] { false , true} },
            {Relays.ALL_OFF, new bool[] { false, false} },
            {Relays.ALL_ON, new bool[] { true , true} },
        };

        private Dictionary<MotorModes, bool[]> MotorModesAndValues = new Dictionary<MotorModes, bool[]>()
        {
            {MotorModes.REPOSE, new bool[] { false, false} },
            {MotorModes.OPENING, new bool[] { true , false} },
            {MotorModes.CLOSING, new bool[] { false , true} }
        };

        private Dictionary<LedsGridsMode, bool[]> ControlLedsAndValues = new Dictionary<LedsGridsMode, bool[]>()
        {
            {LedsGridsMode.LEDS_CIERRE, new bool[] { true, false} },
            {LedsGridsMode.LEDS_APERTURA, new bool[] { false , true} }
        };

        private Dictionary<Leds, bool[]> LedsAndValues = new Dictionary<Leds, bool[]>()
        {
            {Leds.LED_GRID1, new bool[]{ true, true, true, true, true, false, true, true}},
            {Leds.LED_GRID2, new bool[]{ true, true, true, true, false, true, true, true}},
            {Leds.LED_GRID3, new bool[]{ true, true, true, false, true, true, true, true}},
            {Leds.LED_GRID4, new bool[]{ true, true, false, true, true, true, true, true}},
            {Leds.LED_GRID5,  new bool[]{ true, false, true, true, true, true, true, true}},
            {Leds.LED_GRID6, new bool[]{ false, true, true, true, true, true, true, true}},
            {Leds.LED_RELAY_Y, new bool[]{ true, true, true, true, true, true, true, true}},
            {Leds.LED_TX,  new bool[]{ true, true, true, true, true, true, true, false}},
            {Leds.LED_RELAY_G, new bool[]{ true, true, true, true, true, true, false, true}},
            {Leds.ANY_LED, new bool[]{ true, true, true, true, true, true, true, true}}
        };
        #endregion

        #region Enumerators and Structs



        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TEMPERATURE_PATTERNS)]
        public struct TemperaturePatterns
        {
            public ushort Pattern_4C;
            public ushort Pattern60C;
            public ushort Pattern0C;
            public ushort Pattern55C;
            public ushort Pattern27C;
            public ushort Pattern27C2;
            public ushort Pattern26C;
            public ushort Pattern26C2;
            public ushort Pattern33C;
            public ushort Pattern20C;
            public ushort Pattern7C;
        }      

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TEMPORIZATORS)]
        public struct Temporizators
        {
            public ushort TEMP0;
            public ushort TEMP1;
            public ushort TEMP2;
            public ushort TEMP3;
            public ushort TEMP4;
            public ushort TEMP5;
            public ushort TEMP6;
            public ushort TEMP7;
            public ushort TEMP8;
            public ushort TEMP9;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COUNTERS_AREAS)]
        public struct ContadoresZonas
        {
            public ushort Zona1;
            public ushort Zona2;
            public ushort Zona3;
            public ushort Zona4;
            public ushort Zona5;
            public ushort Zona6;

            public ContadoresZonas(ushort valueZones)
            {
                Zona1 = Zona2 = Zona3 = Zona4 = Zona5 = Zona6 = valueZones;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CODE_ERROR)]
        public struct Customize
        {
            public Int32 CodeError;
            public Int32 NumSerie;
            public Int32 unknow;
        }

        [Flags]
        public enum RadioFrequency
        {
            _433MHz,
            _434MHz
        }

        [Flags]
        public enum Relays
        {
            [Description("RELÉ Y")]
            RELAY_Y,
            [Description("RELÉ G")]
            RELAY_G,
            ALL_OFF,
            ALL_ON
        }

        [Flags]
        public enum Alarms
        {
            SIN_ALARMA = 0,
            ALARMA_CRUCE = 1,
            ALARMA_EXTERIOR = 2,
            ALARMA_POSITIVA = 4,
            ALARMA_NEGATIVA = 8
        }

        [Flags]
        public enum Switches
        {
            ALL_SWITCHES_ON = 0X0000,
            ALL_SWITCHES_OFF = 0X00FF
        }

        [Flags]
        public enum MotorModes
        {
            REPOSE,
            [Description("APERTURA")]
            OPENING,
            [Description("CIERRE")]
            CLOSING
        }

        [Flags]
        public enum AddresGrids
        {
            [Description("REJA1")]
            ADDR_GRID1 = 0X0456,
            [Description("REJA2")]
            ADDR_GRID2 = 0X0457,
            [Description("REJA3")]
            ADDR_GRID3 = 0X0458,
            [Description("REJA4")]
            ADDR_GRID4 = 0X0459,
            [Description("REJA5")]
            ADDR_GRID5 = 0X045A,
            [Description("REJA6")]
            ADDR_GRID6 = 0X045B
        }

        [Flags]
        public enum LedsGridsMode
        {
            LEDS_APERTURA,
            LEDS_CIERRE
        }

        [Flags]
        public enum SwitchPositions
        {
            [Description("ON")]
            SWITCH_ALL_ON = 0X00,
            [Description("OFF")]
            SWITCH_ALL_OFF = 0X0F
            //SWITCH_1_OFF = 0X01,
            //SWITCH_2_0FF = 0X02,
            //SWITCH_3_OFF = 0X04,
            //SWITCH_4_OFF = 0X08,            
        }

        [Flags]
        public enum SwitchRotationPositions
        {
            POSITION1 = 0X3E,
            POSITION2 = 0X3D,
            POSITION3 = 0X3B,
            POSITION4 = 0X37,
            POSITION5 = 0X2F,
            POSITION6 = 0X1F
        }

        public enum Leds
        {
            LED_GRID1,
            LED_GRID2,
            LED_GRID3,
            LED_GRID4,
            LED_GRID5,
            LED_GRID6,
            LED_RELAY_Y,
            LED_TX,
            LED_RELAY_G,
            //LEDS_PAIRS,
            //LEDS_ODDS,
            ANY_LED
        }

        [Flags]
        public enum PatternsValues
        {
            _60C = 2463,
            _55C = 2954,
            _33C = 7057,
            _27C = 9150,
            _26C = 9564,
            _25C = 10000,
            _20C = 12555,
            _7C = 23562,
            _0C = 33900,
            __4C = 42090
        }

        [Flags]
        public enum Registers
        {
            FLAG_TEST = 0X0441,
            RELAYS = 0X046A,
            MOTOR_MODE = 0X0470,
            ROTATIVE_SWITCH = 0X044C,
            SWITCH = 0X0442,
            TEMPORIZATORS = 0X0410,
            ID_NUMBER = 0X03E8,
            BASTIDOR = 0X042E,
            SOFTWARE_VERSION = 0X03E8,
            CONTROL_LEDS = 0X0488,
            LEDS = 0X047E,
            TEMPERATURE = 0X03EC,
            TEMPERATURE_PATTERNS = 0X03F2,
            COUNTERS_AREAS = 0X044C,
            FREQUENCY_WORK = 0X07D4,
            ALARMS = 0X0190,
            RESET = 0X07D0,
            CODE_ERROR = 0X042E,
        }
        #endregion
    }
}
