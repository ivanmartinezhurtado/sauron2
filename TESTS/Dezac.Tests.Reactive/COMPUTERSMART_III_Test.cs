﻿using Comunications.Message;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Device.Reactive;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;

namespace Dezac.Tests.Reactive
{
    [TestVersion(1.22)]
    public class COMPUTERSMART_III_Test : TestBase
    {
        private Tower3 tower;
        private COMPUTERSMARTIII compSmartIII;
        private string CAMARA;
        private const int OUT_1 = 17;
        private const int OUT_2 = 18;

        private const int IN_1 = 9;
        private const int IN_2 = 10;

        private const int KEY_RIGHT = 8;
        private const int KEY_DOWN = 9;
        private const int KEY_CENTRAL = 10;
        private const int KEY_UP = 11;
        private const int KEY_LEFT = 12;

        private const int OUT_230VAux = 15;
        private const int OUT_14RELES = 43;

        private const int RELE_ALARMA_NO = 12;
        private const int RELE_ALARMA_NC = 13;
        private const int RELE_VENTILADOR = 11;

        private const int CONEXION_ILK = 19;

        public byte NumeroMuestras { get; set; }
        public byte NumeroReles { get; set; }
        public double TensionConsigna { get; set; }
        public double CorrienteConsigna { get; set; }
        public double DesfaseConsigna { get; set; }


        public void TestInitialization()
        {
            compSmartIII = AddInstanceVar(new COMPUTERSMARTIII(Comunicaciones.SerialPort), "UUT");

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
               TestException.Create().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            tower.IO.DO.Off(43); // apagamos Neon equipo malo

            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveElectroValvule();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            SamplerWithCancel((p) =>
            {
                tower.WaitBloqSecurity(100);
                return true;
            }, "", 10, 100, 1000, false, false);

            CAMARA = GetVariable<string>("CAMARA", CAMARA);

            NumeroMuestras = (byte)Configuracion.GetDouble("NUMERO_MUESTRAS", 5, ParamUnidad.SinUnidad);

            tower.IO.DO.On(CONEXION_ILK);
        }

        public void TestCommunications()
        {
            SamplerWithCancel((p) =>
            {
                compSmartIII.WriteFlagTest();
                return true;

            }, "", 5, 200, 2500, false, false);

            var firmwareVersion = compSmartIII.ReadFirmwareVersion();
            var firmwareVersionBBDD = Identificacion.VERSION_FIRMWARE;
            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, ParamUnidad.SinUnidad, firmwareVersionBBDD);

            Assert.IsTrue(firmwareVersion == firmwareVersionBBDD, () => TestException.Create().PROCESO.PARAMETROS_ERROR.VERSION_FIRMWARE().Throw());
              
            compSmartIII.WriteBastidor((int)TestInfo.NumBastidor.Value);
        }

        public void TestConsumption()
        {
            internalTestConsumption(TypeTestPoint.ConCarga, 230, TypeTestVoltage.VoltageNominal);

            internalTestConsumption(TypeTestPoint.EnVacio, 230, TypeTestVoltage.VoltageNominal);
        }

        public void SetupDefault()
        {
            compSmartIII.WriteFlagTest();

            compSmartIII.WriteComunicationCom1();

            compSmartIII.WriteComunicationCom2();

            compSmartIII.WriteRelatcionTrafosCorriente();

            var primariVoltage = Configuracion.GetDouble("PRIMARIO_TENSION", 400, ParamUnidad.SinUnidad);
            var secundariVoltage = Configuracion.GetDouble("SECUNDARIO_TENSION", 400, ParamUnidad.SinUnidad);
            compSmartIII.WriteRelatcionTrafosTension(new COMPUTERSMARTIII.RelacionTrafosTension() { Primario = (int)primariVoltage, Secundario = (int)secundariVoltage });

            compSmartIII.WriteConfiguracionTipoConexion();

            if (VectorHardware.GetString("MODELO", "SMART_NORMAL", ParamUnidad.SinUnidad).ToUpper().Contains("FAST"))
                compSmartIII.WriteConfiguracionOnOffAutoFast();
            else
                compSmartIII.WriteConfiguracionOnOffAuto();

            var THDVInf = Configuracion.GetDouble("ALARM1_THDV_LIMITE_INF", 5, ParamUnidad.SinUnidad);
            var Alarm1_THDV_Limite_Sup = Configuracion.GetDouble("ALARM1_THDV_LIMITE_SUP", 8, ParamUnidad.SinUnidad);
            var Alarm1_THDI_Limite_Inf = Configuracion.GetDouble("ALARM1_THDI_LIMITE_INF", 4, ParamUnidad.SinUnidad);
            var Alarm1_THDI_Limite_Sup = Configuracion.GetDouble("ALARM1_THDI_LIMITE_SUP", 5, ParamUnidad.SinUnidad);
            var Alarm1_Temp_Limite_Inf = Configuracion.GetDouble("ALARM1_TEMP_LIMITE_INF", 65, ParamUnidad.SinUnidad);
            var Alarm1_Temp_Limite_Sup = Configuracion.GetDouble("ALARM1_TEMP_LIMITE_SUP", 70, ParamUnidad.SinUnidad);

            var ILEAKOnOff = Configuracion.GetDouble("ALARM1_ILEAK_OnOff", 0, ParamUnidad.SinUnidad);
            var ILEAKLimit = Configuracion.GetDouble("ALARM1_ILEAK_Limit", 300, ParamUnidad.SinUnidad); //0x12C,
            var ILEAKReset = Configuracion.GetDouble("ALARM1_ILEAK_Reset", 0, ParamUnidad.SinUnidad);
            var COSCoseno = Configuracion.GetDouble("ALARM1_COS_Coseno", 95, ParamUnidad.SinUnidad); //0x005F,
            var COSLimit = Configuracion.GetDouble("ALARM1_COS_Limit", 20, ParamUnidad.SinUnidad);  // 0x0014,
            var COSETipo = Configuracion.GetDouble("ALARM1_COS_Tipo", 1, ParamUnidad.SinUnidad); // 0x0001
            var FANLimitTemp = Configuracion.GetDouble("ALARM1_FAN_Limit_Temp", 30, ParamUnidad.SinUnidad); // 0x0023,
            var FANEnabled = Configuracion.GetDouble("ALARM1_FAN_Enabled", 0, ParamUnidad.SinUnidad);

            var configAlarm1 = new COMPUTERSMARTIII.Alarm1Configuration()
            {
                THDVInf = (short)THDVInf, //0x0005,
                THDVsup = (short)Alarm1_THDV_Limite_Sup, //0x000A,
                THDIinf = (short)Alarm1_THDI_Limite_Inf, //0x0004,
                THDIsup = (short)Alarm1_THDI_Limite_Sup, //0x0005,
                TEMPinf = (short)Alarm1_Temp_Limite_Inf, //0x0037, -> 55
                TEMPsup = (short)Alarm1_Temp_Limite_Sup, //0x0046, -> 70
                ILEAKOnOff = (short)ILEAKOnOff,
                ILEAKLimit = (short)ILEAKLimit, //0x12C,
                ILEAKReset = (short)ILEAKReset, //0,
                COSCoseno = (short)COSCoseno, //0x005F,
                COSETipo = (short)COSETipo, //0x0001,
                COSLimit = (short)COSLimit, //0x0014,
                FANEnabled = (short)FANEnabled, //0,
                FANLimitTemp = (short)FANLimitTemp, //0x0023,
            };

            compSmartIII.WriteAlarm1Configuration(configAlarm1);

            compSmartIII.WriteAlarm2Configuration();

            var Alarm_Enabled = Configuracion.GetString("ALARM_ENABLED", "1;2;3;4;8;10;11;12", ParamUnidad.SinUnidad).Trim();
            var enabledAlamrs = Alarm_Enabled.Split(';').ToList();
            var resp = new COMPUTERSMARTIII.AlarmConfiguration()
            {
                S1 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "1")),
                S2 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "2")),
                S3 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "3")),
                S4 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "4")),
                S5 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "5")),
                S6 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "6")),
                S7 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "7")),
                S8 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "8")),
                S9 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "9")),
                S10 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "10")),
                S11 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "11")),
                S12 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "12")),
                S13 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "13")),
                S14 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "14")),
                S15 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "15")),
                S16 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "16")),
                S17 = Convert.ToInt16(enabledAlamrs.Exists(p => p == "17")),
            };
            compSmartIII.WriteAlarmEnabled(resp);

            var PorCentCorteSubTension = Configuracion.GetDouble("PORCENTAJE_CORTE_SUBTENSION", 80, ParamUnidad.SinUnidad);
            if (PorCentCorteSubTension != -1)
                compSmartIII.WritePorCentCorteSubTension((ushort)PorCentCorteSubTension);

            var NumeroDeslastresCorteSubTension = Configuracion.GetDouble("NUMERO_DESLASTRES_CORTE_SUBTENSION", 0, ParamUnidad.SinUnidad);
            if (NumeroDeslastresCorteSubTension != -1)
                compSmartIII.WriteNumeroDeslastresCorteSubTension((ushort)NumeroDeslastresCorteSubTension);

            compSmartIII.WriteAlarmOutputConfiguration();

            var IdiomaByte = Parametrizacion.GetString("IDIOMA", "ESP", ParamUnidad.SinUnidad) == "ESP" ? 0 : 1;
            compSmartIII.WriteConfiguracionDisplay((short)IdiomaByte);

            NumeroReles = (byte)VectorHardware.GetDouble("NUMERO_RELES", 6, ParamUnidad.SinUnidad);
            compSmartIII.WriteConfiguraciManiobra(NumeroReles);

            var hardwareVector = new COMPUTERSMARTIII.HardwareVector();
            hardwareVector.ComCPC = VectorHardware.GetString("COMUNICACIONES_CPC", "SI", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Shunt = VectorHardware.GetString("SHUNT", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.red3G = VectorHardware.GetString("3G", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.Ethernet = VectorHardware.GetString("ETHERNET", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.tempExt = VectorHardware.GetString("TEMP_EXT_SONDA", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            hardwareVector.NumRelesString = VectorHardware.GetString("NUMERO_RELES", "6", ParamUnidad.SinUnidad);
            hardwareVector.TipoSalida = VectorHardware.GetString("TIPO_SALIDAS", "RELES", ParamUnidad.SinUnidad).ToUpper() == "RELES" ? false : true;
            hardwareVector.PowerSupplyString = VectorHardware.GetString("ALIMENTACION", "Vac_110_480V", ParamUnidad.SinUnidad);
            hardwareVector.InputCurrentString = VectorHardware.GetString("ENTRADA_CORRIENTE", "IN_5", ParamUnidad.SinUnidad);
            hardwareVector.InputVoltageString = VectorHardware.GetString("ENTRADA_TENSION", "V_100_480", ParamUnidad.SinUnidad);
            hardwareVector.ModeloString = VectorHardware.GetString("MODELO", "SMART_NORMAL", ParamUnidad.SinUnidad);
            hardwareVector.MarcaString = VectorHardware.GetString("MARCA", "CIRCUTOR", ParamUnidad.SinUnidad);
            hardwareVector.ModeloP2String = VectorHardware.GetString("MODELO_P2", "SOLO_SALIDAS", ParamUnidad.SinUnidad);

            compSmartIII.WriteVectorHardware(hardwareVector);
            Resultado.Set("VECTOR_HARDWARE_TRAMA", hardwareVector.VectorHardwareHexString, ParamUnidad.SinUnidad);

            compSmartIII.WriteFactoresCalibracion();

            Delay(1000, "Escribiendo factores");

            ResetHardware();

            if (hardwareVector.ComCPC)
                TestCom_CPC_NET();
        }
  
        public void TestDisplay()
        {
            compSmartIII.WriteFlagTest();

            compSmartIII.WriteBackLight(true);

            compSmartIII.WriteDisplayAllSegments(true);

            Delay(750, "");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMARA, "SMART_III_ALL_SEGEMENTS", "ALL_SEGEMENTS", "SMART_III");
        }

        public void TestLeds()
        {
            compSmartIII.WriteFlagTest();

            compSmartIII.WriteBackLight(false);

            compSmartIII.WriteLeds(true, COMPUTERSMARTIII.Leds.LED_CPU);
            compSmartIII.WriteLeds(true, COMPUTERSMARTIII.Leds.LED_ALARM);

            Delay(500, "");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA, "SMART_III_LEDS_CPU_ALARM", "LEDS_CPU_ALARM", "SMART_III");

            compSmartIII.WriteLeds(false);
            Delay(200, "Apagando Leds");

            compSmartIII.WriteLeds(true, COMPUTERSMARTIII.Leds.LED_FAN);
            compSmartIII.WriteLeds(true, COMPUTERSMARTIII.Leds.LED_KEY);

            Delay(500, "");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMARA, "SMART_III_LEDS_FAN_KEY", "LEDS_FAN_KEY", "SMART_III");

            compSmartIII.WriteLeds(false);
        }

        public void TestInputsDigital()
        {
            var inputsValue = new Dictionary<COMPUTERSMARTIII.InputsDigitals, byte>();
            inputsValue.Add(COMPUTERSMARTIII.InputsDigitals.IN_D1, OUT_1);
            inputsValue.Add(COMPUTERSMARTIII.InputsDigitals.IN_D2, OUT_2);

            compSmartIII.WriteFlagTest();

            foreach (KeyValuePair<COMPUTERSMARTIII.InputsDigitals, byte> output in inputsValue)
            {
                tower.IO.DO.On(output.Value);

                SamplerWithCancel((p) =>
                {
                    return compSmartIII.ReadInputsDigital() == output.Key;

                }, string.Format("Erro no se ha detectado la entrada digital {0} activada", output.Key.ToString()), 20, 100, 200);

                tower.IO.DO.Off(output.Value);

                SamplerWithCancel((p) =>
                {
                    return compSmartIII.ReadInputsDigital() == 0;

                }, string.Format("Erro no se ha detectado la entrada digital {0} desactivada", output.Key.ToString()), 20, 100, 100);

                Resultado.Set(output.Key.ToString(), "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestKeyboard()
        {
            compSmartIII.WriteFlagTest();

            var KeyboardValue = new Dictionary<COMPUTERSMARTIII.KeyboardKeys, byte>();
            KeyboardValue.Add(COMPUTERSMARTIII.KeyboardKeys.RIGHT, KEY_RIGHT);
            KeyboardValue.Add(COMPUTERSMARTIII.KeyboardKeys.CENTER, KEY_CENTRAL);
            KeyboardValue.Add(COMPUTERSMARTIII.KeyboardKeys.DOWN, KEY_DOWN);
            KeyboardValue.Add(COMPUTERSMARTIII.KeyboardKeys.UP, KEY_UP);
            KeyboardValue.Add(COMPUTERSMARTIII.KeyboardKeys.LEFT, KEY_LEFT);

            foreach (KeyValuePair<COMPUTERSMARTIII.KeyboardKeys, byte> keys in KeyboardValue)
            {

                SamplerWithCancel((s) =>
                {
                    tower.IO.DO.On(keys.Value);

                    var teclaCorrecta = false;

                    for(var i =0; i<4; i++)             
                    {
                        Delay(100, "");

                        var lastKeyPressed = compSmartIII.ReadKeyBoard();

                        teclaCorrecta = lastKeyPressed == keys.Key;

                        Resultado.Set(string.Format("TECLA_{0}", keys.Key.ToString()), teclaCorrecta ? "OK" : "KO", ParamUnidad.SinUnidad);

                        if (teclaCorrecta)
                            break;
                    };                 

                    tower.IO.DO.Off(keys.Value);

                    return teclaCorrecta;

                }, string.Format("Error en la comprobación de tecla {0}, NO se detecta la tecla pulsada", keys.Key.ToString()), 10, 500, 100);


                SamplerWithCancel((s) =>
                {
                    var lastKeyPressed = compSmartIII.ReadKeyBoard();

                    var teclaCorrecta = lastKeyPressed == COMPUTERSMARTIII.KeyboardKeys.NONE;

                    Resultado.Set(string.Format("TECLA_{0}", keys.Key.ToString()), teclaCorrecta ? "OK" : "KO", ParamUnidad.SinUnidad);

                    return teclaCorrecta;

                }, string.Format("Error en la comprobación de tecla {0}, se detecta la tecla pulsada sin estarlo", keys.Key.ToString()), 10, 500, 100);

            }

            Resultado.Set("TECLADO", "OK", ParamUnidad.SinUnidad);
        }

        public void TestOutputsDigital()
        {
            compSmartIII.WriteFlagTest();

            compSmartIII.WriteDigitalOutput1(true);

            SamplerWithCancel((p) =>
            {
                var DO1 = tower.IO.DI[IN_1];
                if (!DO1)
                    throw new Exception("Erro no se ha detectado la salida digital DO1 activada");
                return true;

            },"", 20, 100, 100);

            compSmartIII.WriteDigitalOutput1(false);

            SamplerWithCancel((p) =>
            {
                var DO1 = tower.IO.DI[IN_1];
                if (DO1)
                    throw new Exception("Erro no se ha detectado la salida digital DO1 desactivada");
                return true;

            }, "", 20, 100, 100);

            Resultado.Set("DIGITAL_OUTPUT_1", "OK", ParamUnidad.SinUnidad);

            compSmartIII.WriteDigitalOutput2(true);

            SamplerWithCancel((p) =>
            {
                var DO2 = tower.IO.DI[IN_2];
                if (!DO2)
                    throw new Exception("Erro no se ha detectado la salida digital DO2 activada");
                return true;

            }, "", 20, 100, 100);

            compSmartIII.WriteDigitalOutput2(false);

            SamplerWithCancel((p) =>
            {
                var DO2 = tower.IO.DI[IN_2];
                if (DO2)
                    throw new Exception("Erro no se ha detectado la salida digital DO2 desactivada");
                return true;

            }, "", 20, 100, 100);

            Resultado.Set("DIGITAL_OUTPUT_2", "OK", ParamUnidad.SinUnidad);
        }

        public void TestReles()
        {
            compSmartIII.WriteFlagTest();

            var reles = new Dictionary<String, byte>();
            reles.Add("RELE1", 0);
            reles.Add("RELE2", 1);
            reles.Add("RELE3", 2);
            reles.Add("RELE4", 3);
            reles.Add("RELE5", 14);
            reles.Add("RELE6", 15);

            if (NumeroReles > 6)
            {
                reles.Add("RELE7", 16);
                reles.Add("RELE8", 17);
                reles.Add("RELE9", 18);
                reles.Add("RELE10", 19);
                reles.Add("RELE11", 20);
                reles.Add("RELE12", 21);
            }

            if (NumeroReles > 12)
            {
                reles.Add("RELE13", 7);
                reles.Add("RELE14", 8);
                tower.IO.DO.OnWait(100,OUT_14RELES);
                tower.IO.DO.OnWait(100,OUT_230VAux);
            }

            foreach (KeyValuePair<string, byte> rele in reles)
            {
                var releNum = Convert.ToInt16(rele.Key.Replace("RELE", ""));

                compSmartIII.WriteReles(true, releNum);

                SamplerWithCancel((p) =>
                {
                    var DO1 = tower.IO.DI[rele.Value];
                    if (!DO1)
                        throw new Exception(string.Format("Erro no se ha detectado el {0} activada", rele.Key));

                    return true;

                }, "", 20, 100, 100);

                compSmartIII.WriteReles(false, releNum);

                SamplerWithCancel((p) =>
                {
                    var DO1 = tower.IO.DI[rele.Value];
                    if (DO1)
                        throw new Exception(string.Format("Erro no se ha detectado el {0} desactivada", rele.Key));

                    return true;

                }, "", 20, 100, 100);

                Resultado.Set(string.Format("TEST_{0}", rele.Key), "OK", ParamUnidad.SinUnidad);
            }

            tower.IO.DO.OnWait(100, OUT_230VAux);
            tower.IO.DO.OnWait(100, OUT_14RELES);
        }

        public void TestRelesAlarmaVentilador()
        {
            compSmartIII.WriteFlagTest();

            compSmartIII.WriteReleAlarma(true);

            SamplerWithCancel((p) =>
            {
                var releAlarmaNO = tower.IO.DI[RELE_ALARMA_NO];
                if (!releAlarmaNO)
                    throw new Exception(string.Format("Error, no se ha detectado el RELE_ALARMA N.O. activado"));

                var releAlarmaNC = tower.IO.DI[RELE_ALARMA_NC];
                if (releAlarmaNC)
                    throw new Exception(string.Format("Error, no se ha detectado el RELE_ALARMA N.C. desactivado"));

                return true;

            }, "", 20, 100, 100);

            compSmartIII.WriteReleAlarma(false);

            SamplerWithCancel((p) =>
            {
                var releAlarmaNO = tower.IO.DI[RELE_ALARMA_NO];
                if (releAlarmaNO)
                    throw new Exception(string.Format("Error, no se ha detectado el RELE_ALARMA N.O. desactivado"));

                var releAlarmaNC = tower.IO.DI[RELE_ALARMA_NC];
                if (!releAlarmaNC)
                    throw new Exception(string.Format("Error, no se ha detectado el RELE_ALARMA N.C. activado"));

                return true;

            }, "", 20, 100, 100);

            Resultado.Set("TEST_RELE_ALARMA", "OK", ParamUnidad.SinUnidad);

            compSmartIII.WriteReleVentilador(true);

            SamplerWithCancel((p) =>
            {
                var releAlarmaNO = tower.IO.DI[RELE_VENTILADOR];
                if (!releAlarmaNO)
                    throw new Exception(string.Format("Erro no se ha detectado el RELE_VENTILADOR  activado"));

                return true;

            }, "", 20, 100, 100);

            compSmartIII.WriteReleVentilador(false);

            SamplerWithCancel((p) =>
            {
                var releAlarmaNO = tower.IO.DI[RELE_VENTILADOR];
                if (releAlarmaNO)
                    throw new Exception(string.Format("Erro no se ha detectado el RELE_VENTILADOR desactivado"));

                return true;

            }, "", 20, 100, 100);

            Resultado.Set("TEST_RELE_VENTILADOR", "OK", ParamUnidad.SinUnidad);
        }

        //**********************************************************

        public void TestAdjustVoltageCurrent()
        {

            var VoltageAndCurrentDesfase = ApplyAndWaitStabilisation();
            TensionConsigna = VoltageAndCurrentDesfase.Item1;
            CorrienteConsigna = VoltageAndCurrentDesfase.Item2;
            DesfaseConsigna = VoltageAndCurrentDesfase.Item3;

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.SUM_V.L1.Ajuste.Name, Params.SUM_V.Null.Ajuste.Min(), Params.SUM_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new TriAdjustValueDef(Params.SUM_I.L1.Ajuste.Name, Params.SUM_I.Null.Ajuste.Min(), Params.SUM_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new TriAdjustValueDef(Params.SUM_V.L12.Ajuste.Name, Params.SUM_V.L12.Ajuste.Min(), Params.SUM_V.L12.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new TriAdjustValueDef(Params.OFFSET_V.L1.Ajuste.Name, Params.OFFSET_V.Null.Ajuste.Min(), Params.OFFSET_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new TriAdjustValueDef(Params.OFFSET_I.L1.Ajuste.Name, Params.OFFSET_I.L1.Ajuste.Min(), Params.OFFSET_I.L1.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var defsCalibration = new List<TriAdjustValueDef>();
            defsCalibration.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.Name, Params.GAIN_V.Null.Ajuste.Min(), Params.GAIN_V.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.Name, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos, true));
            defsCalibration.Add(new TriAdjustValueDef(Params.GAIN_V.L12.Ajuste.Name, Params.GAIN_V.L12.Ajuste.Min(), Params.GAIN_V.L12.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new TriAdjustValueDef(Params.GAIN_DESFASE.L1.Ajuste.Name, Params.GAIN_DESFASE.Null.Ajuste.Min(), Params.GAIN_DESFASE.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            compSmartIII.WriteFlagTest();

            var result = compSmartIII.CalculateAdjustFactorsVoltageCurrentDesfase(1, NumeroMuestras, NumeroMuestras + 3, 1200, TensionConsigna, CorrienteConsigna, DesfaseConsigna,
            () =>
            {
                var sumatorios = compSmartIII.ReadSumatoriosCiclo();
                var offsets = compSmartIII.ReadOffsets();

                return new double[]
                {
                    sumatorios.tensionL1,   sumatorios.tensionL2,   sumatorios.tensionL3,
                    sumatorios.corrienteL1, sumatorios.corrienteL2,  sumatorios.corrienteL3, 
                    sumatorios.tensionCompuestaL12,   sumatorios.tensionCompuestaL23,   sumatorios.tensionCompuestaL31,
                    sumatorios.potenciaActivaL1,  sumatorios.potenciaActivaL2, sumatorios.potenciaActivaL3,
                    sumatorios.potenciaReactivaL1,   sumatorios.potenciaReactivaL2,   sumatorios.potenciaReactivaL3,
                    offsets.offsetTensionL1, offsets.offsetTensionL2,   offsets.offsetTensionL3,
                    offsets.offsetCorrienteL1, offsets.offsetCorrienteL2,   offsets.offsetCorrienteL3,
                };
            },
            (adjustValues) =>
            {
                defsCalibration[0].L1.Value = adjustValues.CalibracionVL1;  defsCalibration[0].L2.Value = adjustValues.CalibracionVL2;  defsCalibration[0].L3.Value = adjustValues.CalibracionVL3;
                defsCalibration[1].L1.Value = adjustValues.CalibracionIL1; defsCalibration[1].L2.Value = adjustValues.CalibracionIL2; defsCalibration[1].L3.Value = adjustValues.CalibracionIL3;
                defsCalibration[1].Neutro.Value = adjustValues.CalibracionCorrienteNeutro;
                defsCalibration[2].L1.Value = adjustValues.CalibracionVL12; defsCalibration[2].L2.Value = adjustValues.CalibracionVL23; defsCalibration[2].L3.Value = adjustValues.CalibracionVL31;
                defsCalibration[3].L1.Value = adjustValues.CalibracionDesfaseL1; defsCalibration[3].L2.Value = adjustValues.CalibracionDesfaseL2; defsCalibration[3].L3.Value = adjustValues.CalibracionDesfaseL3;

                foreach (var res in defsCalibration)
                    res.AddToResults(Resultado);

                return HasError(defsCalibration, false);
            },
            (samples) =>
            {
                defs[0].L1.Value = samples.sumatorios.tensionL1;  defs[0].L2.Value = samples.sumatorios.tensionL2;   defs[0].L3.Value = samples.sumatorios.tensionL3;
                defs[1].L1.Value = samples.sumatorios.corrienteL1;   defs[1].L2.Value = samples.sumatorios.corrienteL2;   defs[1].L3.Value = samples.sumatorios.corrienteL3;
                defs[2].L1.Value = samples.sumatorios.tensionCompuestaL12;  defs[2].L2.Value = samples.sumatorios.tensionCompuestaL23;   defs[2].L3.Value = samples.sumatorios.tensionCompuestaL31;
                defs[3].L1.Value = samples.offsets.offsetTensionL1; defs[3].L2.Value = samples.offsets.offsetTensionL2;  defs[3].L3.Value = samples.offsets.offsetTensionL3;
                defs[4].L1.Value = samples.offsets.offsetCorrienteL1;   defs[4].L2.Value = samples.offsets.offsetCorrienteL2;   defs[4].L3.Value = samples.offsets.offsetCorrienteL3;

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                return HasError(defs, false);
            });

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES(string.Format("VOLTAGE Y CORRIENTE POR {0}", result.Item3)).Throw();
     //       throw new Exception(string.Format("Error en Ajuste de volatge y corriente por {0} ", result.Item3));

            compSmartIII.WriteFactoresCalibracion(result.Item2);

            Delay(1000, "Escribiendo factores");

            ResetHardware();           
        }

        public void TestAdjustPowerActiveReactive()
        {
            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.SUM_KW.L1.Ajuste.Name, Params.SUM_KW.Null.Ajuste.Min(), Params.SUM_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defs.Add(new TriAdjustValueDef(Params.SUM_KVAR.L1.Ajuste.Name, Params.SUM_KVAR.Null.Ajuste.Min(), Params.SUM_KVAR.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            var defsCalibration = new List<TriAdjustValueDef>();
            defsCalibration.Add(new TriAdjustValueDef(Params.GAIN_KW.L1.Ajuste.Name, Params.GAIN_KW.Null.Ajuste.Min(), Params.GAIN_KW.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));
            defsCalibration.Add(new TriAdjustValueDef(Params.GAIN_KVAR.L1.Ajuste.Name, Params.GAIN_KVAR.Null.Ajuste.Min(), Params.GAIN_KVAR.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos));

            compSmartIII.WriteFlagTest();

            var result = compSmartIII.CalculateAdjustFactorsPoweraActiveReactive(1, NumeroMuestras, NumeroMuestras + 3, 1200, TensionConsigna, CorrienteConsigna, DesfaseConsigna,
            () =>
            {
                var sumatorios = compSmartIII.ReadSumatoriosCiclo();
                return new double[]
                {
                    sumatorios.potenciaActivaL1,  sumatorios.potenciaActivaL2,   sumatorios.potenciaActivaL3,
                    sumatorios.potenciaReactivaL1,  sumatorios.potenciaReactivaL2,   sumatorios.potenciaReactivaL3,
                 };
            },
            (adjustValues) =>
            {
                defsCalibration[0].L1.Value = adjustValues.CalibracionPActivaL1;
                defsCalibration[0].L2.Value = adjustValues.CalibracionPActivaL2;
                defsCalibration[0].L3.Value = adjustValues.CalibracionPActivaL2;
                defsCalibration[1].L1.Value = adjustValues.CalibracionPReactivaL1;
                defsCalibration[1].L2.Value = adjustValues.CalibracionPReactivaL2;
                defsCalibration[1].L3.Value = adjustValues.CalibracionPReactivaL3;

                foreach (var res in defsCalibration)
                    res.AddToResults(Resultado);

                return HasError(defsCalibration, false);
            },
            (samples) =>
            {
                defs[0].L1.Value = samples.sumatorios.potenciaActivaL1;
                defs[0].L2.Value = samples.sumatorios.potenciaActivaL2;
                defs[0].L3.Value = samples.sumatorios.potenciaActivaL3;
                defs[1].L1.Value = samples.sumatorios.potenciaReactivaL1;
                defs[1].L2.Value = samples.sumatorios.potenciaReactivaL2;
                defs[1].L3.Value = samples.sumatorios.potenciaReactivaL3;

                foreach (var res in defs)
                    res.AddToResults(Resultado);

                return HasError(defs, false);
            });

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES(string.Format("POTENCIA POR {0}",result.Item3)).Throw();
           // throw new Exception(string.Format("Error en Ajuste de potencias por {0} ", result.Item3));

            compSmartIII.WriteFactoresCalibracion(result.Item2);

            Delay(1000, "Escribiendo factores");

            ResetHardware();          
        }

        public void TestAdjustCurrentFugas()
        {
            tower.IO.DO.On(CONEXION_ILK);

            var VoltageAndCurrentDesfase = ApplyAndWaitStabilisation(true);

            var defs = new AdjustValueDef(Params.SUM_ILK.Null.Ajuste.Name,0, Params.SUM_ILK.Null.Ajuste.Min(), Params.SUM_ILK.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            var defsCalibration = new AdjustValueDef(Params.GAIN_I.LK.Ajuste.Name,0, Params.GAIN_I.LK.Ajuste.Min(), Params.GAIN_I.LK.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            compSmartIII.WriteFlagTest();

            Delay(8000, "");

            var result = compSmartIII.CalculateAdjustFactorsCurrentLeak(1, NumeroMuestras, NumeroMuestras + 3, 1500, VoltageAndCurrentDesfase.Item2,
            () =>
            {
                var sumatorios = compSmartIII.ReadSumatoriosCiclo();
                return new double[]
                {
                    sumatorios.corrienteFugas
                };
            },
            (adjustValues) =>
            {
                defsCalibration.Value = adjustValues.CalibracionCorrienteFugas;
                defsCalibration.AddToResults(Resultado);
                return !HasError(defsCalibration, false);
            },
            (samples) =>
            {
                defs.Value = samples.sumatorios.corrienteFugas;
                defs.AddToResults(Resultado);
                return true; // HasError(defs, false);
            });

            if (!result.Item1)
                Error().UUT.AJUSTE.MARGENES("CORRIENTE DE FUGAS").Throw();
               // throw new Exception("Error en el Ajuste de corriente de fugas");

            compSmartIII.WriteFactoresCalibracion(result.Item2);

            Delay(1000, "Escribiendo factores");

            ResetHardware();

            Delay(6000, "Estabilizando  medida");       

            var defsLeak = new AdjustValueDef(Params.I_LK.Null.Verificacion.Name, 0, 0, 0, 0, Params.I_LK.Null.Verificacion.Tol(), ParamUnidad.s);
            TestCalibracionBase(defsLeak,
            () =>
            {
                return compSmartIII.ReadVariablesTrifasicas().CorrienteFugas;          
            },
            () =>
            {
                return tower.MTEPS.ReadCurrent().L3;

            }, 3, 5, 500, "Error en la verificación de la corriente de disparo");

            tower.IO.DO.Off(CONEXION_ILK);
        }

        public void TestVerification()
        {
            var VoltageAndCurrentDesfase = ApplyAndWaitStabilisation();
            TensionConsigna = VoltageAndCurrentDesfase.Item1;
            CorrienteConsigna = VoltageAndCurrentDesfase.Item2;
            DesfaseConsigna = VoltageAndCurrentDesfase.Item3;

            var voltageCoumpound = TensionConsigna * Math.Sqrt(3);
            var power = TriLinePower.Create(TensionConsigna, CorrienteConsigna, DesfaseConsigna);
            var powerFactor = Math.Cos(DesfaseConsigna.ToRadians());

            var defsCalibration = new List<TriAdjustValueDef>();
            defsCalibration.Add(Params.V.L1.Verificacion.Name, 0, 0, TensionConsigna, Params.V.Null.Verificacion.Tol(), ParamUnidad.V);
            defsCalibration.Add(Params.I.L1.Verificacion.Name, 0, 0, CorrienteConsigna, Params.I.Null.Verificacion.Tol(), ParamUnidad.A);
            defsCalibration.Add(Params.KW.L1.Verificacion.Name, 0, 0, power.KW.L1, Params.KW.Null.Verificacion.Tol(), ParamUnidad.Kw);
            defsCalibration.Add(Params.KVAR.L1.Verificacion.Name, 0, 0, power.KVAR.L1, Params.KVAR.Null.Verificacion.Tol(), ParamUnidad.Kvar);
            defsCalibration.Add(Params.KVA.L1.Verificacion.Name, 0, 0, power.KVA.L1, Params.KVA.Null.Verificacion.Tol(), ParamUnidad.kVA);
            defsCalibration.Add(Params.COS_PHI.L1.Verificacion.Name, 0, 0, powerFactor, Params.COS_PHI.Null.Verificacion.Tol(), ParamUnidad.SinUnidad);
            defsCalibration.Add(Params.V.L12.Verificacion.Name, 0, 0, voltageCoumpound, Params.V.L12.Verificacion.Tol(), ParamUnidad.V);

            TestCalibracionBase(defsCalibration,
            () =>
            {
                var variablesL1 = compSmartIII.ReadVariablesInstantaneasL1();
                var variablesL2 = compSmartIII.ReadVariablesInstantaneasL2();
                var variablesL3 = compSmartIII.ReadVariablesInstantaneasL3();
                var variablesTri = compSmartIII.ReadVariablesTrifasicas();

                return new double[] { variablesL1.Tension, variablesL2.Tension, variablesL3.Tension,
                                      variablesL1.Corriente, variablesL2.Corriente, variablesL3.Corriente,
                                      variablesL1.PotenciaActiva, variablesL2.PotenciaActiva, variablesL3.PotenciaActiva,
                                      variablesL1.PotenciaReactiva, variablesL2.PotenciaReactiva, variablesL3.PotenciaReactiva,
                                      variablesL1.PotenciaAparente, variablesL2.PotenciaAparente, variablesL3.PotenciaAparente,
                                      variablesL1.CosenoPhi, variablesL2.CosenoPhi, variablesL3.CosenoPhi,
                                      variablesTri.TensionLineaL1L2, variablesTri.TensionLineaL2L3, variablesTri.TensionLineaL3L1 };
            },
            () =>
            {
                var average = new List<double>();

                foreach (TriAdjustValueDef defs in defsCalibration)
                    average.AddRange(defs.AverageToArray());

                return average.ToArray();

            }, 5, 10, 1000);

            var defsCalibration2 = new List<AdjustValueDef>();
            defsCalibration2.Add(Params.FREQ.Null.Verificacion.Name,  0, 0, 50, Params.FREQ.Null.Verificacion.Tol(), ParamUnidad.Hz);
            defsCalibration2.Add(Params.V.III.Verificacion.Name, 0, 0, TensionConsigna, Params.V.III.Verificacion.Tol(), ParamUnidad.V);
            defsCalibration2.Add(Params.I.III.Verificacion.Name,  0, 0, CorrienteConsigna, Params.I.III.Verificacion.Tol(), ParamUnidad.A);
            defsCalibration2.Add(Params.KW.III.Verificacion.Name,  0, 0, power.KW.L1 * 3, Params.KW.III.Verificacion.Tol(), ParamUnidad.Kw);
            defsCalibration2.Add(Params.KVAR.III.Verificacion.Name,  0, 0, power.KVAR.L1 * 3, Params.KVAR.III.Verificacion.Tol(), ParamUnidad.Kvar);
            defsCalibration2.Add(Params.COS_PHI.III.Verificacion.Name,  0, 0, powerFactor, Params.COS_PHI.III.Verificacion.Tol(), ParamUnidad.SinUnidad);

            TestCalibracionBase(defsCalibration2,
            () =>
            {
                var variablesTri = compSmartIII.ReadVariablesTrifasicas();

                return new double[] {
                    variablesTri.Frecuencia, variablesTri.TensionFaseIII, variablesTri.CorrienteIII, variablesTri.PotenciaActivaIII,
                    variablesTri.PotenciaReactivaIII, variablesTri.CosenoPhiIII  };
            },
            () =>
            {
                var average = new List<double>();

                foreach (AdjustValueDef defs in defsCalibration2)
                    average.Add(defs.Average);

                return average.ToArray();

            }, 5, 10, 1000);


            var defsCalibration3 = new List<AdjustValueDef>();
            defsCalibration3.Add(Params.THD_V.L1.Verificacion.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.SinUnidad);
            defsCalibration3.Add(Params.THD_V.L2.Verificacion.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.SinUnidad);
            defsCalibration3.Add(Params.THD_V.L3.Verificacion.Name, Params.THD_V.Null.Verificacion.Min(), Params.THD_V.Null.Verificacion.Max(), 0, 0, ParamUnidad.SinUnidad);
            defsCalibration3.Add(Params.THD_I.L1.Verificacion.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.SinUnidad);
            defsCalibration3.Add(Params.THD_I.L2.Verificacion.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.SinUnidad);
            defsCalibration3.Add(Params.THD_I.L3.Verificacion.Name, Params.THD_I.Null.Verificacion.Min(), Params.THD_I.Null.Verificacion.Max(), 0, 0, ParamUnidad.SinUnidad);
            defsCalibration3.Add(Params.I.LN.Verificacion.Name, Params.I.LN.Verificacion.Min(), Params.I.LN.Verificacion.Max(), 0, 0, ParamUnidad.A);
            defsCalibration3.Add(Params.I.LK.Verificacion.Name, Params.I.LK.Verificacion.Min(), Params.I.LK.Verificacion.Max(), 0, 0, ParamUnidad.A);
            defsCalibration3.Add(Params.TEMP.Null.Verificacion.Name, Params.TEMP.Null.Verificacion.Min(), Params.TEMP.Null.Verificacion.Max(), 0, 0, ParamUnidad.Grados);

            TestMeasureBase(defsCalibration3,
            (p) => {

                var variablesTHD = compSmartIII.ReadTHD();
                var variablesTri = compSmartIII.ReadVariablesTrifasicas();

                return new double[] {
                                     variablesTHD.THDtensionL1, variablesTHD.THDtensionL2,  variablesTHD.THDtensionL2,
                                     variablesTHD.THDcorrienteL1, variablesTHD.THDcorrienteL2,  variablesTHD.THDcorrienteL3,
                                     variablesTri.CorrienteNeutro, variablesTri.CorrienteFugas, variablesTri.Temperatura };
         
            }, 0, 3, 500);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        //**********************************************************

        public void TestCostumization()
        {
            compSmartIII.WriteSerialNumber(Convert.ToInt64(TestInfo.NumSerie));

            compSmartIII.WriteCodeError(0);

            Delay(2000, "");

            tower.Chroma.ApplyOffAndWaitStabilisation();

            Delay(4000, "");

            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0.8, 50);

            SamplerWithCancel((p) =>
            {
                compSmartIII.WriteFlagTest();
                return true;

            }, "", 5, 200, 1000, false, false);

            var numSerie = compSmartIII.ReadSerialNumber();
            Assert.IsTrue(numSerie.ToString() == TestInfo.NumSerie, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, Numero de serie leido diferente al grabado en el equipo"));

            var bastodorRead = compSmartIII.ReadBastidor();
            Assert.IsTrue(bastodorRead == (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, bastidor leido diferente al grabado en el equipo"));

            var coderrorRead = compSmartIII.ReadCodeError();
            Assert.IsTrue(coderrorRead == 0, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, codError leido diferente al grabado en el equipo"));

            compSmartIII.WriteBorradoEnergias(true);

            compSmartIII.WriteBorradoMaximosMinimos(true);

            compSmartIII.WriteBorradoNumConexiones(true);

            Delay(4000, "");

        }

        public void TestFinish()
        {
            if (compSmartIII != null)
                compSmartIII.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                if (IsTestError)
                    tower.PowerSourceIII.Reset();

                tower.IO.DO.Off(6); 
                tower.IO.DO.Off(7); 
                tower.IO.DO.Off(5); 
                tower.Dispose();

                if (IsTestError)
                    tower.IO.DO.On(48);
            }
        }

        /////////////////////////////////////////

        private void TestCom_CPC_NET()
        {
           // tower.IO.DO.On(20, 21, 22);
          //  compSmartIII.Modbus.ClosePort();

            var serialPort = new SerialPort("COM" + Comunicaciones.SerialPort2, 38400, Parity.None, 8, StopBits.One);
            //serialPort.DtrEnable = false;
            //serialPort.RtsEnable = false;
            //serialPort.Handshake = Handshake.XOnXOff;
            var spDebug = new SerialPortAdapter(serialPort, "\n");
            spDebug.ReadTimeout = 5000;
            spDebug.WriteTimeout = 5000;

            var result = this.ReadBytesToText((SerialPortAdapter)spDebug, 1500, 0, 20, 5, true, "000F");
            if (!result.Contains("000F006400"))
                throw new Exception("Error, de comunicaciones del COM_CPC");

            spDebug.Dispose();

            Delay(500, "");

           // tower.IO.DO.Off(20, 21, 22);

            Delay(1000, "");
        }

        private void internalTestConsumption(TypeTestPoint typeTestPoint, double voltage, TypeTestVoltage typTestvoltage)
        {
            var isConCarga = typeTestPoint == TypeTestPoint.ConCarga ? COMPUTERSMARTIII.TypeCarga.CARGA_MAXIMA : COMPUTERSMARTIII.TypeCarga.CARGA_MINIMA;
            compSmartIII.WriteCargaMinimaMaxima(isConCarga);

            var configuration = new TestPointConfiguration()
            {
                Voltage = voltage,
                Current = 0.1,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = voltage.ToString(),
                TiempoIntervalo = 1000,
                typePowerSource = TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = typeTestPoint,
                typeTestVoltage = typTestvoltage,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(tower, configuration);
        }

        public Tuple<double,double, double> ApplyAndWaitStabilisation(bool Lk = false)
        {
            var voltageDatabase = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230D, ParamUnidad.V);
            var currentDatabase = Consignas.GetDouble(!Lk ? Params.I.Null.Ajuste.Name : Params.I.LK.Ajuste.Name, 5, ParamUnidad.A);
            var anguloDesfaseDatabase = Consignas.GetDouble(Params.PHASE.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            TriLineValue voltage = new TriLineValue
            {
                L1 = voltageDatabase,
                L2 = voltageDatabase,
                L3 = voltageDatabase
            };

            TriLineValue current = new TriLineValue
            {
                L1 = !Lk ? currentDatabase : 0,
                L2 = !Lk ? currentDatabase : 0,
                L3 = currentDatabase
            };

            tower.PowerSourceIII.Tolerance = Consignas.GetDouble("TOLERANCIA_ESTABILIZACION_MTE", 0.01, ParamUnidad.PorCentage);
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, 50, anguloDesfaseDatabase, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });
            var voltageReference = !Lk ? tower.PowerSourceIII.ReadVoltage().L1 : tower.PowerSourceIII.ReadVoltage().L3;
            Resultado.Set(Params.V.Null.Ajuste.TestPoint("PATRON").Name, voltageReference, ParamUnidad.V);
            var currentReference = !Lk ? tower.PowerSourceIII.ReadCurrent().L1: tower.PowerSourceIII.ReadCurrent().L3;
            Resultado.Set(Params.I.Null.Ajuste.TestPoint("PATRON").Name, currentReference, ParamUnidad.A);
            return Tuple.Create(voltageReference, currentReference, anguloDesfaseDatabase);
        }

        public void ResetHardware()
        {
            tower.IO.DO.OnWait(1000, 23);

            tower.IO.DO.OffWait(4000, 23);
        }
    }
}
