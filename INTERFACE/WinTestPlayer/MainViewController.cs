﻿using Dezac.Entities;
using Dezac.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using TaskRunner.Model;

namespace WinTestPlayer
{
    public class MainViewController
    {
        public OPERARIO Operario { get; private set; }
        public ORDEN OF { get; private set; }
        public PRODUCTO Producto { get; private set; }
        public List<T_GRUPOPARAMETRIZACION> Parametros { get; private set; }

        public SequenceModel Sequence { get; private set; }

        public event EventHandler InfoChanged;

        public MainViewController()
        {
        }

        public void Logout()
        {
            Operario = null;
            OnInfoChanged();
        }

        public void Login(string idEmpleado)
        {
            if (string.IsNullOrEmpty(idEmpleado))
                throw new Exception("Falta informar el código de empleado!");

            using (DezacService svc = new DezacService())
            {
                Operario = svc.GetOperarioByIdEmpleado(idEmpleado);
                if (Operario == null)
                    throw new Exception("Operario inexistente!");
            }
        }

        public bool IsLogged { get { return Operario != null;  } }

        public void LoadOF()
        {
            if (!IsLogged)
                throw new Exception("Falta identificar el operario!");

            using (DezacService svc = new DezacService())
            {
                var dmf = svc.GetLastDiarioMarcaje(Operario.IDEMPLEADO);
                if (dmf == null)
                    throw new Exception("No existe marcajes para este operario");

                if (dmf.HOJAMARCAJEFASE == null)
                    throw new Exception("No existe O.F. marcada para este operario");

                OF = svc.GetOrden(dmf.HOJAMARCAJEFASE.NUMORDEN);
                if (OF == null)
                    throw new Exception("Orden inexistente");

                Producto = svc.GetProductoByIOrdenFab(OF.NUMPRODUCTO, OF.VERSION);
                if (Producto == null)
                    throw new Exception("Producto inexistente");

                Parametros = svc.GetGruposParametrizacion(Producto, dmf.IDFASE /*"120" */);
                if (Parametros == null)
                    throw new Exception("Grupos de parametrización inexistentes");
            }

            //LoadSequence();
        }

        public void GrabaTest(T_TEST test)
        {
            using (DezacService svc = new DezacService())
            {
                if (svc.GrabarDatos(test) == false)
                {
                    //TODO 
                }
            }
        }

        private void OnInfoChanged()
        {
            if (InfoChanged != null)
                InfoChanged(this, EventArgs.Empty);
        }

        public string SequenceFileName
        {
            get
            {
                if (Producto == null)
                    return null;

                string name = string.Format("{0}_{1}.seq", Producto.NUMPRODUCTO, Producto.VERSION);

                return Path.Combine(ConfigurationManager.AppSettings["PathSequences"], name);
            }
        }

        public void LoadSequence()
        {
            if (!File.Exists(SequenceFileName))
                return;

            SequenceModel model = SequenceModel.Load(SequenceFileName);

            Sequence = model;
        }
    }
}
