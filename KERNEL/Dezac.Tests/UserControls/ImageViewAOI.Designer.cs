﻿namespace Dezac.Tests.UserControls
{
    partial class ImageViewAOI
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.PictureImage = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.PictureImage)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureImage
            // 
            this.PictureImage.BackColor = System.Drawing.Color.AliceBlue;
            this.PictureImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureImage.Location = new System.Drawing.Point(3, 10);
            this.PictureImage.Name = "PictureImage";
            this.PictureImage.Size = new System.Drawing.Size(581, 473);
            this.PictureImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureImage.TabIndex = 0;
            this.PictureImage.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.PictureImage, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.646091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 98.35391F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(587, 486);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // ImageviewAOI
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ImageviewAOI";
            this.Size = new System.Drawing.Size(587, 486);
            ((System.ComponentModel.ISupportInitialize)(this.PictureImage)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureImage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

    }
}
