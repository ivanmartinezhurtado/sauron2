﻿using System;
using HalconDotNet;
using System.Configuration;
using System.IO;

using Dezac.Tests.AOI.Models;
using System.Diagnostics;

namespace Dezac.Tests.AOI
{
    public static class HalconEngine
    {
        private static HDevEngine engine;

        public static void Init()
        {
            if (engine != null)
                return;

            string programsPath = ConfigurationManager.AppSettings["PathAOIPrograms"];

            engine = new HDevEngine();

            engine.SetProcedurePath(programsPath);
        }

        public static HDevProgram GetProgram(string fileName)
        {
            if (!Path.IsPathRooted(fileName))
            {
                string programsPath = ConfigurationManager.AppSettings["PathAOIPrograms"];

                fileName = Path.Combine(programsPath, fileName);
            }

            return new HDevProgram(fileName);
        }

        public static HDevProcedure GetProcedure(string fileName)
        {
            return new HDevProcedure(fileName);
        }

        public static void CallProgram(string fileName)
        {
            using (var program = GetProgram(fileName))
            {
                using (var call = new HDevProgramCall(program))
                {
                    call.Execute();
                }
            }
        }

        public static HalconAOICallResult CallProcedure(string fileName, object args)
        {
            return CallProcedure(fileName, new object[] { args });
        }

        public static HalconAOICallResult CallProcedure(string fileName, object[] args = null)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var result = new HalconAOICallResult();

            try
            {
                using (var procedure = GetProcedure(fileName))
                {
                    using (var call = new HDevProcedureCall(procedure))
                    {
                        if (args != null)
                        {
                            int NumberI = 1;
                            int NumberT = 1;
                            foreach (var arg in args)
                                if (arg is HImage || arg is HRegion || arg is HObject)
                                {
                                    call.SetInputIconicParamObject(NumberI, (HObject)arg);
                                    NumberI++;
                                }
                                else if (arg is HTupleVector)
                                {
                                    call.SetInputCtrlParamVector(NumberT, (HTupleVector)arg);
                                    NumberT++;
                                }
                                else
                                {
                                    call.SetInputCtrlParamTuple(NumberT, (HTuple)arg);
                                    NumberT++;
                                }
                        }

                        call.Execute();

                        for (int i = 1; i <= procedure.GetOutputCtrlParamCount(); i++)
                        {
                            var name = procedure.GetOutputCtrlParamName(i);

                            var dim = procedure.GetOutputCtrlParamDimension(i);
                            if (dim > 0)
                            {
                                var value = call.GetOutputCtrlParamVector(i);
                                result.VectorResults.Add(name, value);
                            }
                            else
                            {
                                var value = call.GetOutputCtrlParamTuple(i);
                                result.TupleResults.Add(name, value);
                            }
                        }

                        for (int i = 1; i <= procedure.GetOutputIconicParamCount(); i++)
                        {
                            var name = procedure.GetOutputIconicParamName(i);
                            var value = call.GetOutputIconicParamObject(i);

                            result.IconicResults.Add(name, value);
                        }
                        result.Success = true;
                    }
                }
            }
            catch (HDevEngineException dex)
            {
                engine.UnloadProcedure(fileName);
                result.Exception = dex;
                result.ErrorMessage = $"Message: {dex.Message}, Procedure: {dex.ProcedureName}, Line: {dex.LineText}, " +
                    $"Line Number: {dex.LineNumber}, HALCON errror Code: {dex.HalconError}, Categoría: { dex.Category}";
            }
            catch (Exception ex)
            {
                engine.UnloadProcedure(fileName);
                result.Exception = ex;
                result.ErrorMessage = ex.Message;
            }

            engine.UnloadProcedure(fileName);

            result.ExecutionTime = stopwatch.ElapsedMilliseconds;
            return result;
        }

        public static void UnloadProcedures()
        {
            if (engine != null)
                engine.UnloadAllProcedures();
        }
    }
}
