﻿using Dezac.Tests.Actions.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(AddVariablesFromCSVDescription))]
    public class AddVariablesFromCSV : ActionBase
    {
        [Description("Instancia/ hilo (Equipo en paralelo) de las variables a asignar")]
        [Category("3. Configuration")]
        public ActionVariable Instance { get; set; } = new ActionVariable();

        [Description("Como se crearan las nuevas variables. Solo para el test que ejecuta el action o para todos los test en paralelo.")]
        [Category("3. Configuration")]
        [DefaultValue(tipoVariableEnum.Normal)]
        public tipoVariableEnum DataType { get; set; }

        [Category("3.1 DataMapFile")]
        [Description("Path del fichero *.CSV de las variables a mapear en el test. Nota: Solo funciona si el DataMapFile esta vacio.")]
        public ActionVariable PathDataMapFile { get; set; } = new ActionVariable();

        [Category("3.1 DataMapFile")]
        [Description("con \"$\" se obtiene el Fichero *.CSV de las variables a mapear que se cargan desde BBDD.")]
        public ActionVariable DataMapFile { get; set; } = new ActionVariable();

        public enum tipoVariableEnum
        {
            Normal,
            Shared
        }

        public override void Execute()
        {
            var NumInstance = SequenceContext.Current.NumInstance;
            if (Instance == null || Instance.IsKeyNullOrEmpty)
                Instance = 1;

            if (NumInstance == Instance.GetValue<int>())
            {
                try
                {
                    if (DataMapFile.IsKeyNullOrEmpty && PathDataMapFile.IsKeyNullOrEmpty)
                        Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("La variable VariablesMapFile o la PathVariablesMapFile no pueden ser nula").Throw();

                    var content = "";

                    if (!DataMapFile.IsKeyNullOrEmpty)
                    {
                        var stream = DataMapFile.Value;
                        if (stream?.GetType() == typeof(byte[]))
                            content = ASCIIEncoding.ASCII.GetString(stream as byte[]);
                        else
                        {
                            if (File.Exists(DataMapFile.Value.ToString()))
                                content = File.ReadAllText(DataMapFile.Value.ToString());
                            else
                                Error().SOFTWARE.SECUENCIA_TEST.FICHERO_NO_ENCONTRADO("La variable VariablesMapFile tiene un Path de un fichero que no existe").Throw();
                        }
                    }
                    else
                    {
                        if (!File.Exists(PathDataMapFile.Value.ToString()))
                            Error().SOFTWARE.SECUENCIA_TEST.FICHERO_NO_ENCONTRADO("PathVariablesMapFile").Throw();

                        if (Path.GetExtension(PathDataMapFile.Value.ToString()) != ".csv")
                            Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("El fichero en PathVariablesMapFile no es un fichero CSV").Throw();
                        try
                        {
                            content = File.ReadAllText(PathDataMapFile.Value.ToString());
                        }
                        catch (Exception ex)
                        {
                            Error().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA(ex.Message).Throw();
                        }
                    }

                    var Variables = CSVFill(content);

                    if (DataType == tipoVariableEnum.Normal)
                    {
                        foreach (var item in Variables)
                        {
                            var value = VariablesTools.ResolveValue(item.Value);
                            Logger.InfoFormat("Instance {0} Add Variable {1} = {2}", NumInstance, item.Key, value);
                            Context.ResultList.Add(string.Format("Instance {0} Add Variable {1} = {2}", NumInstance, item.Key, value));
                            SetVariable(item.Key, value);
                        }
                    }
                    else
                    {
                        foreach (var item in Variables)
                        {
                            var value = VariablesTools.ResolveValue(item.Value);
                            Logger.InfoFormat("Instance {0} Add SharedVariable {1} = {2}", NumInstance, item.Key, value);
                            Context.ResultList.Add(string.Format("Instance {0} Add SharedVariable {1} = {2}", NumInstance, item.Key, value));
                            SetSharedVariable(item.Key, value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Error().SOFTWARE.SAURON.ACTION_MAL_CONFIGURADO(ex.Message).Throw();
                }
            }
        }

        private List<TextValuePair> CSVFill(string content)
        {
            var sr = new StringReader(content);
            string line = null;
            char delimiter = ';';

            List<TextValuePair> items = new List<TextValuePair>();

            while ((line = sr.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line))
                    continue;

                var separate = line.Split(delimiter).Count() >= 2;
                if (!separate)
                    continue;

                var tokens = line.Split(delimiter);

                if (string.IsNullOrEmpty(tokens[0]))
                    continue;

                if (string.IsNullOrEmpty(tokens[1]))
                    continue;

                if (tokens[0].ToUpper().Contains("KEY") || tokens[1].ToUpper().Contains("VALUE"))
                    continue;

                var value = new TextValuePair();
                value.Key = tokens[0];
                value.Value = tokens[1];
                items.Add(value);
            }

            return items;

        }
    }


    public class AddVariablesFromCSVDescription : ActionDescription
    {
        public override string Description { get { return "Este Action sirve para crear las variables del valor que vienen mapeadas en un fichero CSV";  } }
        public override string Name { get { return "AddVariablesFromCSV"; }}
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
