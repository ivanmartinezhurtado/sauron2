﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.04)]
    public class MTEPRS : MeasureBase<MTEPRS>, IDisposable
    {
        public class MeasureData
        {
            public MeasureData(TriLineValue t)
            {
                Voltage = t;
                Current = t;
                Desfase = t;
                PowerActive = t;
                PowerReactive = t;           
                PowerAparent = t;
                Voltage_Between_Phases = t;
                PowerFactor = t;
                PhasesAngleVoltage = t;
                PhasesAngleCurrent = t;
                Freq = 0;
                SumPowerActive = 0;
                SumPowerReactive = 0;
                SumPowerAparent = 0;
            }   
            
            public TriLineValue Voltage { get; set; }
            public TriLineValue Current { get; set; }
            public TriLineValue Desfase { get; set; }
            public TriLineValue PowerActive { get; set; }
            public TriLineValue PowerReactive { get; set; }
            public TriLineValue PowerAparent { get; set; }
            public TriLineValue PowerFactor { get; set; }
            public TriLineValue Voltage_Between_Phases { get; set; }
            public TriLineValue PhasesAngleVoltage { get; set; }
            public TriLineValue PhasesAngleCurrent { get; set; }
            public double Freq { get; set; }
            public double SumPowerActive { get; set; }
            public double SumPowerReactive { get; set; }
            public double SumPowerAparent { get; set; }
        }

        internal MeasureData DeviceData { get; set; }

        public double Tolerance { get; set; }

        private const string RESET_COMMAND = "R";
        private const string SET_ACTIVE_RELE_120A_COMMAND = "DB{0}";
        private const string MODEFORMAT_COMMAND = "MODE{0}";
        private const string READ_IDN_COMMAND = "VER{0}";

        public enum PrivateCommands
        {
            SET_ACTIVE_RELE_120A = Commands.USER_DEFINED_COMMANDS + 1,
            SET_CONNECTION_TYPE= Commands.USER_DEFINED_COMMANDS + 2,
            SET_MODEFORMAT = Commands.USER_DEFINED_COMMANDS + 3,
        };

        public enum Variables
        {
            CURRENT,
            VOLTAGE,
            ACTIVE_POWER,
            REACTIVE_POWER,
            APARENT_POWER,
            ACTIVE_SUM_POWER,
            REACTIVE_SUM_POWER,
            APARENT_SUM_POWER,
            POWER_FACTOR,
            VOLTAGE_BETWEEN_PHASES,
            FREQUENCY,
            PHASE_ANGLES_VOLTAGE,
            PHASE_ANGLES_CURRENT,
        };

        private readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Variables.CURRENT, "?1" },
            { (int)Variables.VOLTAGE, "?2" },
            { (int)Variables.ACTIVE_POWER, "?3" },
            { (int)Variables.REACTIVE_POWER, "?4" },
            { (int)Variables.APARENT_POWER, "?5" },        
            { (int)Variables.ACTIVE_SUM_POWER, "?6" },
            { (int)Variables.REACTIVE_SUM_POWER, "?7" },
            { (int)Variables.APARENT_SUM_POWER, "?8" },
            { (int)Variables.POWER_FACTOR, "?9" },
            { (int)Variables.VOLTAGE_BETWEEN_PHASES, "?12" },     
            { (int)Variables.FREQUENCY, "?13" },   
            { (int)Variables.PHASE_ANGLES_VOLTAGE, "?21" }, 
            { (int)Variables.PHASE_ANGLES_CURRENT, "?22" }, 
        };

        public enum ConectionTypeEnum
        {
            ActivePower4wire,
            ActivePower3wire,
            ReactivePower4wire,
            ReactivePower3wire,
            ReactivePowerCross4wire,
            ReactivePowerCross3wire,
        }

        private readonly Dictionary<int, string> messagesConnect = new Dictionary<int, string> 
        {
            { (int)ConectionTypeEnum.ActivePower4wire, "P4" },
            { (int)ConectionTypeEnum.ActivePower3wire, "P3" },
            { (int)ConectionTypeEnum.ReactivePower4wire, "N4" },
            { (int)ConectionTypeEnum.ReactivePower3wire, "N3" },
            { (int)ConectionTypeEnum.ReactivePowerCross4wire, "K4" },
            { (int)ConectionTypeEnum.ReactivePowerCross3wire, "K3" },
        };

        private bool _output120A = false;
        private bool _ModeFormat1 = false;
        private ConectionTypeEnum _TypeConnect= ConectionTypeEnum.ActivePower4wire;
        private string idn = "";
        private SerialPort sp;
        private byte _port = 2;
        private CultureInfo enUS = new CultureInfo("en-US");
        private TcpClientAdapter adapter;

        public MTEPRS()
        {
            if (sp == null)
                sp = new SerialPort();

            ByEthernet = false;
            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.BaudRate = 19200;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
            DeviceData = new MeasureData(TriLineValue.Zero);
            protocol = new MTEProtocol();
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
            transport.Retries = 1;
        }

        public MTEPRS(string hostName, int port)
        {
            ByEthernet = true;
            DeviceData = new MeasureData(TriLineValue.Zero);
            protocol = new MTEProtocol();
            IP = hostName;
            var tcp = new TcpClient(hostName, port);
            tcp.ReceiveTimeout = 5000;
            adapter = new TcpClientAdapter(tcp);
            transport = new MessageTransport(protocol, adapter, _logger);
            transport.Retries = 1;
        }

        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public string IP { get; set; }

        public bool ByEthernet { get; set; }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public bool Output120A
        {
            get { return _output120A; }
            set
            {
                _output120A = value;
                SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_ACTIVE_RELE_120A, true, _output120A ? 0 : 1)); //En el wattimetro ir por los 120A es = 0
            }
        }

        public ConectionTypeEnum ConnectionType
        {
            get { return _TypeConnect; }
            set
            {
                _TypeConnect = value;
                SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CONNECTION_TYPE, true, messagesConnect));
            }
        }

        public bool ModeFormat1
        {
            get { return _ModeFormat1; }
            set
            {
                SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MODEFORMAT, true, _ModeFormat1 ? 0 : 1));
            }
        }
     
        public Result<TriLineValue> ReadVoltage(bool? throwException = null)
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.VOLTAGE));
            return DeviceData.Voltage;
        }

        public Result<TriLineValue> ReadActivePower(bool? throwException = null)
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.ACTIVE_POWER));
            return DeviceData.PowerActive;
        }

        public Result<TriLineValue> ReadReactivePower(bool? throwException = null)
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.REACTIVE_POWER));
            return DeviceData.PowerReactive;
        }

        public Result<TriLineValue> ReadPowerFactor(bool? throwException = null)
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.POWER_FACTOR));
            return DeviceData.PowerFactor;
        }

        public MeasureData ReadAll(bool? throwException = null)
        {
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.VOLTAGE));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.CURRENT));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.FREQUENCY));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.ACTIVE_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.APARENT_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.POWER_FACTOR));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.REACTIVE_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.VOLTAGE_BETWEEN_PHASES));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.PHASE_ANGLES_CURRENT));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.PHASE_ANGLES_VOLTAGE));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.REACTIVE_SUM_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.APARENT_SUM_POWER));
            SendCommand(new Command<Commands>(Commands.READ_VARIABLE, true, Variables.ACTIVE_SUM_POWER));
            return DeviceData;
        }

        public Task<MeasureData> ReadAllAsync(bool? throwException = null)
        {
            return Task.Factory.StartNew<MeasureData>(() => { return ReadAll(throwException); }, TaskCreationOptions.AttachedToParent);
        }

        public Task<Result<TriLineValue>> ReadActivePowerAsync(bool? throwException = null)
        {
            return Task.Factory.StartNew<Result<TriLineValue>>(() => { return ReadActivePower(throwException); }, TaskCreationOptions.AttachedToParent);
        }

        public Task<Result<TriLineValue>> ReadReactivePowerAsync(bool? throwException = null)
        {
            return Task.Factory.StartNew<Result<TriLineValue>>(() => { return ReadReactivePower(throwException); }, TaskCreationOptions.AttachedToParent);
        }

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));
            return idn;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            MTEMessage message, response;

            if (sp != null)
                if (!sp.IsOpen)
                    sp.Open();

            switch (command.Commands)
            {       
                case (int)Commands.READ_VARIABLE:

                    int variable = (int)command.Args[0];
                   
                    message = new MTEMessage { Text = messages[variable] };
                    response = transport.SendMessage<MTEMessage>(message);

                    switch (variable)
                    {
                        case (int)Variables.VOLTAGE:
                            DeviceData.Voltage = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.VOLTAGE_BETWEEN_PHASES:
                            DeviceData.Voltage_Between_Phases = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.CURRENT:
                            DeviceData.Current = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.ACTIVE_POWER:
                            DeviceData.PowerActive = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.REACTIVE_POWER:
                            DeviceData.PowerReactive = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.APARENT_POWER:
                            DeviceData.PowerAparent = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.FREQUENCY:
                             DeviceData.Freq = Convert.ToDouble(response.GetFormattedText(), enUS);
                            break;
                        case (int)Variables.ACTIVE_SUM_POWER:
                            DeviceData.SumPowerActive = Convert.ToDouble(response.GetFormattedText(), enUS);
                            break;
                        case (int)Variables.REACTIVE_SUM_POWER:
                            DeviceData.SumPowerReactive = Convert.ToDouble(response.GetFormattedText(), enUS);
                            break;
                        case (int)Variables.APARENT_SUM_POWER:
                            DeviceData.SumPowerAparent = Convert.ToDouble(response.GetFormattedText(), enUS);
                            break;
                        case (int)Variables.POWER_FACTOR:
                            var pf = TriLineValue.CreateFromString(response.GetFormattedText());
                            if (pf.L1 > 350)
                                pf.L1 -= 360;

                            if (pf.L2 > 350)
                                pf.L2 -= 360;

                            if (pf.L3 > 350)
                                pf.L3 -= 360;

                            DeviceData.PowerFactor = pf;
                            break;
                        case (int)Variables.PHASE_ANGLES_VOLTAGE:
                            DeviceData.PhasesAngleVoltage = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        case (int)Variables.PHASE_ANGLES_CURRENT:
                            DeviceData.PhasesAngleCurrent = TriLineValue.CreateFromString(response.GetFormattedText());
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                    break;
 
                case (int)Commands.READ_STATUS:
                    message = new MTEMessage { Text = "SP" };
                    message.Validator = (m) => { return m.Text.StartsWith("SP="); };
                    response = transport.SendMessage<MTEMessage>(message);

                    string text = response.Text.Substring(3);
                    
                    int p = text.IndexOf('S');
                    int pe = text.IndexOf('E');

                    Status.IsBusy = p >= 0 && text[p + 3] == '1';
                    Status.HasError = pe >= 0 && text[pe + 1] == '1';
                    Status.HasWarning = pe >= 0 && text[pe + 2] == '1';              
                    break;

                case (int)Commands.RESET:
                    message = MTEMessage.Create(RESET_COMMAND);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_ACTIVE_RELE_120A:
                    message = MTEMessage.Create(SET_ACTIVE_RELE_120A_COMMAND, command.Args[0]);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                case(int)PrivateCommands.SET_CONNECTION_TYPE:
                    int typeconnect = (int)command.Args[0];
                    message = new MTEMessage { Text = messages[typeconnect] };
                    response = transport.SendMessage<MTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_MODEFORMAT:
                    message = MTEMessage.Create(MODEFORMAT_COMMAND, command.Args[0]);
                    transport.SendMessage<MTEMessage>(message);
                    break;

                case (int)Commands.READ_IDN:
                    message = MTEMessage.Create(READ_IDN_COMMAND, "1");
                    message.Validator = (m) => { return m.Text.StartsWith("VER"); };
                    response = transport.SendMessage<MTEMessage>(message);
                    var versionTmp = response.Text.Replace("VER1", "").Split('#');
                    idn = string.Format("{0}, SN{1}", versionTmp[0], versionTmp[1]);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();

            if (adapter != null)
                adapter.Dispose();
        }
    }
}
