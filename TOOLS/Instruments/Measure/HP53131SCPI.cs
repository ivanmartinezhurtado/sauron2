﻿using Instruments.SCPI;
using Instruments.Utility;

namespace Instruments.Measure
{
    public class HP53131SCPI : SCPIDevice
    {
        internal HP53131SCPI(HP53131A device)
            : base(device)
        {
        }

        public SCPICommand INITIATE { get { return new SCPICommand(context, ":INITIATE"); } }
        public SCPIQueryCommand<double> FETCh { get { return new SCPIQueryCommand<double>(context, ":FETCh"); } }
        public SCPIQueryCommand<double> AVERAGEMIN { get { return new SCPIQueryCommand<double>(context, ":CALC3:AVERAGE:TYPE MIN;:CALC3:DATA"); } }
        public SCPIQueryCommand<double> AVERAGEMAX { get { return new SCPIQueryCommand<double>(context, ":CALC3:AVERAGE:TYPE MAX;:CALC3:DATA"); } }
        public SCPIQueryCommand<double> AVERAGEMEAN { get { return new SCPIQueryCommand<double>(context, ":CALC3:AVERAGE:TYPE MEAN;:CALC3:DATA"); } }
        public SCPIQueryCommand<double> AVERAGESDEV { get { return new SCPIQueryCommand<double>(context, ":CALC3:AVERAGE:TYPE SDEV;:CALC3:DATA"); } }
    }
}
