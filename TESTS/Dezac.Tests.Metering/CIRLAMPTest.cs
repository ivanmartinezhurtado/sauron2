﻿using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.PowerSource;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;


namespace Dezac.Tests.Metering
{
    [TestVersion(1.04)]
    public class CIRLAMPTest : TestBase
    {
        private CIRLAMP cirlamp;

        private Tower1 tower;

        public void TestInitialization()
        {
            tower = new Tower1();

            tower.Active24VDC();
           
            Shell.MsgBox("COLOQUE EL EQUIPO EN EL UTILLAJE", "TEST INIT", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }

        public void TestCommunications()
        {
            cirlamp = new CIRLAMP(Comunicaciones.SerialPort); 

            cirlamp.CirPLCModbus.PerifericNumber = Comunicaciones.Periferico;

            cirlamp.CirPLCModbus.RtsEnable = true;

            cirlamp.CirPLCModbus.DtrEnable = true;

            tower.IO.DO.On(21, 22); //Aislamos la fuente PTE

            tower.PTE.InitConfig();

            tower.PTE.SelecctionSource = PTE.SourceType.PTE_100V;

            tower.PTE.Presets.Voltage = 230;

            tower.PTE.ApplyAndWaitStabilisation();

            cirlamp.CirPLCModbus.SerialNumberHost = 1000000;

            Delay(5000, "Esperamos a la conexión de los Nodos al concentrador");

            string[] versionBBDD = Identificacion.VERSION_FIRMWARE.Split('.');

            SamplerWithCancel((p) =>
            {
                var version = cirlamp.ReadSoftwareVersion();

                Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, string.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Revision), ParamUnidad.SinUnidad);

                if (version.Major != Convert.ToUInt16(versionBBDD[0]) || version.Minor != Convert.ToUInt16(versionBBDD[1]) || version.Revision != Convert.ToUInt16(versionBBDD[2]))
                    throw new Exception("Error, Versión de Firmware del equipo diferenete a la de la BBDD");

                return true;
            }, "Error. No se puede establecer comunicación con el equipo", 5, 500);

            var modelo = Identificacion.MODELO;

            CIRLAMP.ModelCirLamp modeloEnum = CIRLAMP.ModelCirLamp._1_10;

            if (modelo.Trim().ToUpper() == "DALI")
                modeloEnum = CIRLAMP.ModelCirLamp.DALI;

            if (modelo.Trim().ToUpper() == "DN")
                modeloEnum = CIRLAMP.ModelCirLamp.DN;

            Resultado.Set("MODELO", modelo, ParamUnidad.SinUnidad);

            cirlamp.WriteModel(modeloEnum);

            ushort potencia = Convert.ToUInt16(Identificacion.SUBMODELO);

            cirlamp.WritePowerFactor(potencia);
        }

        public void TestGapAdjust(int numMuestras = 2, int intervaloMuestras = 1100)
        {
            var voltageRef = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentRef = Consignas.GetDouble(Params.I.Null.AjusteDesfase.Name, 0.65, ParamUnidad.A);
            var angleRef = Consignas.GetDouble(Params.PF.Null.AjusteDesfase.Name, 60, ParamUnidad.Grados);

            tower.PTE.ModoExtConfig();
            tower.PTE.Presets.Voltage = voltageRef;
            tower.PTE.Presets.Current = currentRef;
            tower.PTE.Presets.PF = angleRef;
            tower.PTE.ApplyAndWaitStabilisation();

            Delay(5000, "Estabilización de la fuente PTE");

            SamplerWithCancel((p) =>
            {
                var powerActive = cirlamp.ReadPowerActive();

                if (powerActive < 50)
                    return false;

                return true;

            }, "Error la fuente PTE no se ha activado su salida de corriente", 5, 500);

            var adjusGapPhase = new AdjustValueDef() { Name = Params.GAIN_DESFASE.Null.Ajuste.Name, Min = Params.GAIN_DESFASE.Null.Ajuste.Min(), Max = Params.GAIN_DESFASE.Null.Ajuste.Max(), Unidad = ParamUnidad.Puntos };

            TestMeasureBase(adjusGapPhase,
              (step) =>
              {
                  return cirlamp.AdjustDesfase(angleRef, numMuestras, intervaloMuestras);
              }, 1, 5, 500);

        }

        public void TestAdjust(int numMuestras = 2, int intervaloMuestras = 1100)
        {
            var voltageRef = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 230, ParamUnidad.V);
            var currentRef = Consignas.GetDouble(Params.I.Null.Ajuste.Name, 0.65, ParamUnidad.A);
            var angleRef = Consignas.GetDouble(Params.PF.Null.Ajuste.Name, 0, ParamUnidad.Grados);

            tower.PTE.Presets.Voltage = voltageRef;
            tower.PTE.Presets.Current = currentRef;
            tower.PTE.Presets.PF = angleRef;
            tower.PTE.ApplyAndWaitStabilisation();

            Delay(5000, "Estabilización de la fuente PTE");

            var adjustVolatge = new AdjustValueDef() { Name = Params.GAIN_V.Null.Ajuste.Name, Min = Params.GAIN_V.Null.Ajuste.Min(), Max = Params.GAIN_V.Null.Ajuste.Max(), Unidad = ParamUnidad.Puntos};

            TestMeasureBase(adjustVolatge,
              (step) =>
              {
                  return cirlamp.AdjustVoltage(voltageRef, numMuestras, intervaloMuestras);
              }, 1, 5, 500);


            var adjustPower = new AdjustValueDef() { Name = Params.GAIN_KW.Null.Ajuste.Name, Min = Params.GAIN_KW.Null.Ajuste.Min(), Max = Params.GAIN_KW.Null.Ajuste.Max(), Unidad = ParamUnidad.Puntos };

            TestMeasureBase(adjustPower,
              (step) =>
              {
                  return cirlamp.AdjustPower((voltageRef * currentRef), numMuestras, intervaloMuestras);
              }, 1, 5, 500);

        }

        public void TestVerification(int numMuestras = 2, int intervaloMuestras = 1100)
        {
            var voltageRef = Consignas.GetDouble(Params.V.Null.Verificacion.Name, 230, ParamUnidad.V);
            var currentRef = Consignas.GetDouble(Params.I.Null.Verificacion.Name, 0.5, ParamUnidad.A);
            var angleRef = Consignas.GetDouble(Params.PF.Null.Verificacion.Name, 30, ParamUnidad.Grados);

            tower.PTE.Presets.Voltage = voltageRef;
            tower.PTE.Presets.Current = currentRef;
            tower.PTE.Presets.PF = angleRef;
            tower.PTE.ApplyAndWaitStabilisation();

            Delay(5000, "Estabilización de la fuente PTE");
         
            var defsMeaure = new List<AdjustValueDef>();
            defsMeaure.Add(new AdjustValueDef() { Name = Params.V.Null.Verificacion.Name, Min = Params.V.Null.Verificacion.Min(), Max = Params.V.Null.Verificacion.Max(), Unidad = ParamUnidad.V });
            defsMeaure.Add(new AdjustValueDef() { Name = Params.I.Null.Verificacion.Name, Min = Params.I.Null.Verificacion.Min(), Max = Params.I.Null.Verificacion.Max(), Unidad = ParamUnidad.A });
            defsMeaure.Add(new AdjustValueDef() { Name = Params.KW.Null.Verificacion.Name, Min = Params.KW.Null.Verificacion.Min(), Max = Params.KW.Null.Verificacion.Max(), Unidad = ParamUnidad.W });

            TestMeasureBase(defsMeaure,
              (step) =>
              {
                  double voltage = cirlamp.ReadVoltage();
                  double current = cirlamp.ReadCurrent();
                  double activePower = cirlamp.ReadPowerActive();
                  var varsList = new List<double>() { voltage, current, activePower };
                  return varsList.ToArray();
              }, 1, 5, 500);

        }

        public void TestDALI()
        {
            tower.PTE.ApplyPresets(0,0,0);

            tower.PTE.ApplyOffAllAndWaitStabilisation();
        
            Delay(2000, "Esperamos a que la fuente se apague");

            tower.IO.DO.Off(21,22); //Aislamos la fuente PTE

            Delay(2000, "Esperamos a que la fuente se apague");

            tower.IO.DO.On(17, 18);

            Delay(1000, "Esperamos conexión de la carga ne la linea 230V");

            tower.IO.DO.On(19);

            Delay(1000, "Esperamos estabilización de los 230V de linea");

            tower.IO.DO.On(15);// Alimentacion linea 230V

            Delay(1000, "Esperamos secuencia de conexión alimentación linea");

            tower.IO.DO.On(20);

            Delay(4000, "Esperamos arranque de los equipos ...");

            cirlamp.CirPLCModbus.SerialNumberHost = 130000002; //Dummy

            Delay(1000, "Esperamos a la conexión de los Nodos al concentrador");

            var value = cirlamp.ReadTestDaly();

            Assert.IsFalse(Convert.ToBoolean(value), Error().UUT.CONFIGURACION.SETUP("Error Test Daly Dummy Conectado la carga"));

            cirlamp.CirPLCModbus.SerialNumberHost = 1000000; //UUT

            CIRLAMP.TemporalInterval tempInterval = new CIRLAMP.TemporalInterval
            {
                Power = 10,
                Time = 60
            };

            cirlamp.WriteTemporalInterval(tempInterval);

            cirlamp.CirPLCModbus.SerialNumberHost = 130000002; //Dummy

            Delay(4000, "Esperamos a la conexión de los Nodos al concentrador");

            value= cirlamp.ReadTestDaly();

            Assert.IsTrue(Convert.ToBoolean(value), Error().UUT.CONFIGURACION.SETUP("Error Test Daly Dummy no Conectado la carga"));

            cirlamp.CirPLCModbus.SerialNumberHost = 1000000; //UUT

            tempInterval.Power = 0;
            tempInterval.Time = 60;

            cirlamp.WriteTemporalInterval(tempInterval);

            Delay(4000, "Esperamos a la conexión de los Nodos al concentrador");

            var powerActive = cirlamp.ReadPowerActive();

            Resultado.Set("Kw_OpenRelay", powerActive, ParamUnidad.Kw, 0, 50);

            Assert.AreBetween(powerActive, 0, 50, Error().UUT.CONFIGURACION.SETUP("Error Test Rele no se ha abierto"));

            cirlamp.ResetTotalTimeLight();
        }

        public void Test1_10()
        {
            tower.PTE.ApplyPresets(0,0,0);

            tower.PTE.ApplyOffAllAndWaitStabilisation();

            Delay(2000, "Esperamos a que la fuente se apague");

            tower.IO.DO.Off(21, 22); //Aislamos la fuente PTE

            Delay(2000, "Esperamos a que la fuente se apague");

            tower.IO.DO.On(17, 18);

            Delay(1000, "Esperamos conexión de la carga ne la linea 230V");

            tower.IO.DO.On(19);

            Delay(1000, "Esperamos estabilización de los 230V de linea");

            tower.IO.DO.On(15);// Alimentacion linea 230V

            Delay(1000, "Esperamos secuencia de conexión alimentación linea");

            tower.IO.DO.On(20);

            Delay(5000, "Esperamos a la conexión de los Nodos al concentrador");

            cirlamp.CirPLCModbus.SerialNumberHost = 1000000; //UUT

            CIRLAMP.TemporalInterval tempInterval = new CIRLAMP.TemporalInterval   {  Power = 100,  Time = 60   };

            cirlamp.WriteTemporalInterval(tempInterval);

            var defsVoltageMaxCarga = new AdjustValueDef() { Name = Params.V.Other("J6").ConCarga.modo("100%").Name, Min = Params.V.Other("J6").ConCarga.modo("100%").Min(), Max = Params.V.Other("J6").ConCarga.modo("100%").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageMaxCarga,
              (step) =>
              {
                return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
              }, 0, 5, 500);


            tempInterval.Power = 50;
            tempInterval.Time = 60;

            cirlamp.WriteTemporalInterval(tempInterval);

            var defsVoltageMediaCarga = new AdjustValueDef() { Name = Params.V.Other("J6").ConCarga.modo("50%").Name, Min = Params.V.Other("J6").ConCarga.modo("50%").Min(), Max = Params.V.Other("J6").ConCarga.modo("50%").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageMediaCarga,
              (step) =>
              {
                  return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
              }, 0, 5, 500);


            tempInterval.Power = 30;
            tempInterval.Time = 60;

            cirlamp.WriteTemporalInterval(tempInterval);

            var defsVoltageMinCarga = new AdjustValueDef() { Name = Params.V.Other("J6").ConCarga.modo("30%").Name, Min = Params.V.Other("J6").ConCarga.modo("30%").Min(), Max = Params.V.Other("J6").ConCarga.modo("30%").Max(), Unidad = ParamUnidad.V };

            TestMeasureBase(defsVoltageMinCarga,
              (step) =>
              {
                  return tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
              }, 0, 5, 500);


            cirlamp.ResetTotalTimeLight();
        }

        public void TestDN()
        {
            tower.PTE.ApplyPresets(0,0,0);

            tower.PTE.ApplyOffAllAndWaitStabilisation();

            Delay(2000, "Esperamos a que la fuente se apague");

            tower.IO.DO.Off(21, 22); //Aislamos la fuente PTE

            Delay(2000, "Esperamos a que la fuente se apague");

            tower.IO.DO.On(17, 18);

            Delay(1000, "Esperamos conexión de la carga ne la linea 230V"); //CARGA DE 0,1 A PARA MODELO DN

            tower.IO.DO.On(19);

            Delay(1000, "Esperamos estabilización de los 230V de linea");

            tower.IO.DO.On(15);// Alimentacion linea 230V

            Delay(1000, "Esperamos secuencia de conexión alimentación linea");

            //**********************************************************************************************

            tower.IO.DO.Off(29, 20); //Desactivamos Carga de 1A 1uA

            tower.IO.DO.On(28); //Activamos Carga de 0,1A

            Delay(5000, "Esperamos a la conexión de los Nodos al concentrador");

            cirlamp.CirPLCModbus.SerialNumberHost = 1000000; //UUT

            var defsMeaure = new AdjustValueDef(Params.KW.Null.ConCarga.modo("01A_100%").Name, 0, Params.KW.Null.ConCarga.modo("01A_100%").Min(), Params.KW.Null.ConCarga.modo("01A_100%").Max(), 0, 0, ParamUnidad.W);
            TestMeasureBase(defsMeaure,
              (step) =>
              {
                  return cirlamp.ReadPowerActive(); ;
              }, 0, 5, 500);

            CIRLAMP.TemporalInterval tempInterval = new CIRLAMP.TemporalInterval { Power = 50, Time = 60 };

            cirlamp.WriteTemporalInterval(tempInterval);

            Delay(3000, "Espera para la configuración del equipo");

            var defsVoltageMediaCarga = new AdjustValueDef(Params.KW.Null.ConCarga.modo("01A_50%").Name, 0, Params.KW.Null.ConCarga.modo("01A_50%").Min(), Params.KW.Null.ConCarga.modo("01A_50%").Max(), 0, 0, ParamUnidad.W);

            TestMeasureBase(defsVoltageMediaCarga,
              (step) =>
              {
                  return cirlamp.ReadPowerActive();
              }, 0, 5, 500);

            //**********************************************************************************************

            tower.IO.DO.Off(28); //Desactivamos Carga de 0,1A

            tower.IO.DO.On(29,20); //Activamos Carga de 1A 1uA

            tempInterval.Power = 100;
            tempInterval.Time = 60;

            cirlamp.WriteTemporalInterval(tempInterval);

            Delay(3000, "Espera para la configuración del equipo");

            var defsVoltageMaxCarga = new AdjustValueDef(Params.KW.Null.ConCarga.modo("1A_100%").Name, 0, Params.KW.Null.ConCarga.modo("1A_100%").Min(), Params.KW.Null.ConCarga.modo("1A_100%").Max(), 0, 0, ParamUnidad.W);

            TestMeasureBase(defsVoltageMaxCarga,
              (step) =>
              {
                  return cirlamp.ReadPowerActive();      
              }, 0, 5, 500);

            tempInterval.Power = 0;
            tempInterval.Time = 60;

            cirlamp.WriteTemporalInterval(tempInterval);

            Delay(3000, "Espera para la configuración del equipo");
            
            var defsVoltageMinCarga = new AdjustValueDef() { Name = Params.KW.Null.ConCarga.modo("1A_0%").Name, Min = Params.KW.Null.ConCarga.modo("1A_0%").Min(), Max = Params.KW.Null.ConCarga.modo("1A_0%").Max(), Unidad = ParamUnidad.W };

            TestMeasureBase(defsVoltageMinCarga,
              (step) =>
              {
                  return cirlamp.ReadPowerActive();
              }, 0, 5, 500);

            tower.IO.DO.Off(29, 20); //Desactivamos Carga de 1A 1uA

            cirlamp.ResetTotalTimeLight();
        }

        public void TestWriteSerialNumber()
        {
            bool respOk = false;
            do
            {
                respOk = WriteSerialNumber();
                if (!respOk)
                    try
                    {
                        SamplerWithCancel((p) => { cirlamp.ReadNumSerie(); return true; }, "", 3, 2000, 2000, false, false);
                    }
                    catch (Exception)
                    {
                        respOk = true;
                    }
            } while (!respOk);

            Delay(1000, "Esperamos grabacion correcta del numero de serie ...");

            tower.IO.DO.Off(15);// Alimentacion linea 230V

            Delay(1000, "Esperamos desconexión de la carga");

            tower.IO.DO.Off(19);// Carga del UUT

            tower.IO.DO.Off(20);// Alimentacion linea 230V

            Delay(1000, "Esperamos desconexión de la carga");

            tower.IO.DO.Off(17, 18);

            Delay(1000, "Esperamos finalización del test ...");
        }

        public void TestFinish()
        {
            if (cirlamp != null)
                cirlamp.Dispose();

            if (tower != null)
            {
                if(tower.PTE != null)
                    tower.PTE.ApplyOffAllAndWaitStabilisation();

                if(tower.IO != null)
                {
                    tower.IO.DO.Off(15);// Alimentacion linea 230V

                    tower.IO.DO.Off(19, 17, 18, 20);
                }
                tower.Dispose();
            }
        }

        private bool WriteSerialNumber()
        {
            try
            {
                SamplerWithCancel((p) => { cirlamp.WriteNumSerie(Convert.ToUInt32(TestInfo.NumSerie)); return true; }, "", 3, 2000, 2000, false, false);
            }
            catch (Exception)
            {
               return false;
            }

           return true;
        }
    }
}
