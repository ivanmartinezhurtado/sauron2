﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Forms;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using WinTestPlayer.Model;
using WinTestPlayer.Utils;
using WinTestPlayer.Views;

namespace WinTestPlayer.Services
{
    public class AppService : IEnvironmentService
    {
        private bool StartRunning = false;

        private readonly ShellService shell;
        private readonly ReportsService reportsService;
        private readonly ICacheService cacheSvc;
        public SequenceRunner Runner { get; private set; }
        public SequenceModel Sequence { get; private set; }
        public TestModel Model { get; private set; }
        public Dictionary<int, TestContextModel> TestContexts { get; set; }
        public Dictionary<string, object> Environment { get; set; }
        public SynchronizationContext SC { get; set; }
        public bool IsUserLogged { get { return !string.IsNullOrEmpty(Model.IdUsuario); } }
        public bool IsRunning
        {
            get { return Runner.RunningState == RunningState.Running || StartRunning; }
        }

        public event EventHandler SequenceLoaded;
        public event EventHandler ModelChanged;
        public event EventHandler TestInfoChanged;
        public event EventHandler<string> OnEventFired;
        public event EventHandler CloseForm;

        public AppService(RunnerService runnerService
            , ShellService shell
            , ReportsService reportsService
            , ICacheService cacheService)
        {
            try
            {
                this.shell = shell;
                this.reportsService = reportsService;
                cacheSvc = cacheService;

                Environment = new Dictionary<string, object>();
                TestContexts = new Dictionary<int, TestContextModel>();

                Model = new TestModel();
                Model.PropertyChanged += (s, e) => { OnModelChanged(); };

                Runner = runnerService.Runner;
                Runner.AllTestEnded += (s, e) =>
                {
                    StartRunning = false;
                    foreach (KeyValuePair<int, TestContextModel> testContext in TestContexts)
                        testContext.Value.TestInfo.PropertyChanged -= OnTestInfoPropertyChanged;
                };
            }
            catch (Exception ex)
            {
                Model.WhitOutLine = true;
                MessageBox.Show(string.Format("Error en el constructor del AppService por Excepcion del sistema: {0}", ex.Message), "ERROR AL ACCEDER AL CONSTRUCTOR APPSERVICE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Metodos Publicos
        public void Play(List<string> bastidores = null)
        {
            var lastTestContexts = TestContexts.ToList().ToDictionary(x => x.Key, x => x.Value);
            if (lastTestContexts.Any(p => p.Value.ReloadParametersOnNextTest))
                using (IDezacService svc = DI.Resolve<IDezacService>())
                {
                    Model.Parametros = svc.GetGruposParametrizacion(Model.Producto, Model.IdFase);
                }

            TestContexts.Clear();
            StartRunning = true;

            Runner.Play((_numInstance, _sharedVariables) =>
            {
                Model.FaseNumSerieAssigned = Sequence.ExistStepName("GetNumSerie");

                TestContextModel testContext = CreateTestContext(_numInstance);
                if (Model.ModoPlayer == ModoPlayer.TIC)
                    testContext.TestInfo.NumBastidor = Model.BastidorTIC;

                var context = new SequenceContext(Sequence, _numInstance);
                context.RunMode = RunMode.Release;

                if (lastTestContexts.TryGetValue(_numInstance, out TestContextModel lastContextModel))
                {
                    if (bastidores != null && bastidores.Any())
                    {
                        testContext.TestInfo.IsRetry = bastidores[_numInstance - 1] == lastContextModel.TestInfo.NumBastidor.GetValueOrDefault().ToString();
                        if (_numInstance == 1)
                            _sharedVariables.AddOrUpdate(ConstantsParameters.TestInfo.BASTIDORES, bastidores);
                    }
                    if (testContext.TestInfo.IsRetry)
                    {
                        testContext.TestInfo.NumBastidor = lastContextModel.TestInfo.NumBastidor;
                        testContext.TestInfo.ListComponents = lastContextModel.TestInfo.ListComponents;
                    }
                }

                InitUpdateBoxCurrent(testContext);

                TestContexts.Add(_numInstance, testContext);

                context.Services.Add<ITestContext>(testContext);
                context.Services.Add<IShell>(shell);
                context.Services.Add<IEnvironmentService>(this);
                context.Services.Add<IInstanceResolveService>(InstanceResolveService.Default);
                context.Services.Add<ICacheService>(cacheSvc);

                return context;

            }, Model.RunNumInstances.GetValueOrDefault(1));
        }
        public string NotifyAutoTest()
        {
            string result = "*** No se ha realizado ningún Autotest de la torre ***";

            try
            {
                using (IDezacService svc = DI.Resolve<IDezacService>())
                {
                    Tuple<DateTime?, string, string> lastTest = svc.GetLastAutoTest(Dns.GetHostName());
                    if (lastTest != null)
                    {
                        Model.FechaUltimoAutoTest = lastTest.Item1;
                        Model.TestLocationDescription = lastTest.Item2;
                        Model.Ubicacion = lastTest.Item3;
                    }
                }
                if (Model.FechaUltimoAutoTest.HasValue)
                {
                    result = string.Format("Último Autotest de la torre realizado el {0:dd/MM/yyyy HH:mm:ss}", Model.FechaUltimoAutoTest);

                    TimeSpan diffTime = DateTime.Now.Subtract(Model.FechaUltimoAutoTest.Value);
                    if (diffTime.TotalDays > 30)
                    //var message = string.Format("AUTOTEST NO REALIZADO EN: {0} {1}  CON EL: {2}  --> ULTIMO AUTOTEST= {3}  HACE= {3} dias", Model.TestLocationDescription, Model.Ubicacion, Model.DevicePC, Model.FechaUltimoAutoTest, diffTime.TotalDays);
                    //SendMail.EnviarCorreo("AVISO AUTOTEST", message + "\r\n", false, true, false);
                        MessageBox.Show("No se ha realizado un autotest desde hace mas de 30 dias", "ATENCIÓN NO SE HA REALIZADO AUTOTEST", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // Model.WhitOutLine = true;
                }
            }
            catch (Exception ex)
            {
                Model.WhitOutLine = true;
                MessageBox.Show(string.Format("Error al acceder a la BBDD por falta de conexion a la red o por la Excepcion del sistema: {0}", ex.Message), "ERROR AL ACCEDER A LA BBDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return result;
        }
        public void Login(string idEmpleado)
        {
            if (string.IsNullOrEmpty(idEmpleado))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("código de empleado").Throw();

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                VW_EMPLEADO operario = svc.GetOperarioByIdEmpleado(idEmpleado);
                if (operario == null)
                    TestException.Create().PROCESO.OPERARIO.PARAMETRO_INCOHERENTE("Operario inexistente").Throw();

                Model.IdEmpleado = operario.IDEMPLEADO;
                Model.IdOperario = operario.IDOPERARIO;
                Model.Operario = operario.NOMBRE;

                OnModelChanged();
            }
        }
        public void LoadOrden()
        {
            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                LoadOrdenByDataBase(svc);

                Model.IsProcess = svc.GetCodigoGrupo(Model.IdFase) != null;
                if (Model.Producto.T_FAMILIA == null)
                {
                    if (Model.IsAOI)
                        Model.Producto.T_FAMILIA = svc.AddFamiliaToProducto(Model.Producto, 285);
                    else if (!Model.IsProcess)
                        TestException.Create().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Producto sin famila de test asignada o marcado en una fase que no es de test").Throw();
                }

                ValidaProductoFase(svc);
                LoadParametersByProductoFase(svc);
                LoadVersionTestReport(svc);
                LoadLineaNumCajasByOrden(svc);
                OnModelChanged();

                if (Model.DescripcionFase.Contains("LASEAR") || Model.DescripcionFase.Contains("AOI"))
                    LoadSequenceFile(Model.DescripcionFase);
                else
                    LoadSequence(svc);

                if (SequenceLoaded != null)
                    SequenceLoaded(this, EventArgs.Empty);

                ControlPrinterDefaultSelection();
                ControlLaser(svc, Model.Orden, Model.Producto);

            }
            LoadRunNumInstances();
            ControlBienByProducto();
            DeleteTempDirectories();
        }
        public void LoadTest(int? numProducto, int? version)
        {
            if (!IsUserLogged)
                return;

            List<ProductFaseViewModel> fases;
            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                fases = db.PRODUCTOFASE
                    .Include("FASE")
                    .Where(p => p.NUMPRODUCTO == numProducto && p.VERSION == version && p.FASETEST == "S")
                    .Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                    .ToList();

                ProductFaseViewModel fase = shell.ShowTableDialog<ProductFaseViewModel>("FASE", fases);
                if (fase == null)
                    LoadTest(numProducto, version);

                Model.IdFase = fase.IDFASE;
            }
            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                LoadProductoVersion(svc, numProducto, version, Model.IdFase);
                ValidaProductoFase(svc);
                LoadParametersByProductoFase(svc);
                if (MessageBox.Show("¿Desea seleccionar un archivo de secuencia?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    LoadSequenceFileTest(svc);
                else
                    LoadSequence(svc);
            }
            LoadRunNumInstances()
                ;
            ControlBienByProducto();
            OnModelChanged();

            if (SequenceLoaded != null)
                SequenceLoaded(this, EventArgs.Empty);

            DeleteTempDirectories();
        }
        public void LoadLaser(int? numProducto, int? version)
        {
            if (!IsUserLogged)
                return;

            List<ProductFaseViewModel> fases;

            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                fases = db.PRODUCTOFASE
                 .Include("FASE")
                 .Where(p => p.NUMPRODUCTO == numProducto && p.VERSION == version && p.FASETEST == "N" && p.FASE.CODIGOGRUPO == "LAS")
                 .Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                 .ToList();

                ProductFaseViewModel fase = shell.ShowTableDialog<ProductFaseViewModel>("FASE", fases);
                if (fase == null)
                    return;

                Model.IdFase = fase.IDFASE;
            }

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                LoadProductoVersion(svc, numProducto, version, Model.IdFase);

                ValidaProductoFase(svc);

                LoadParametersByProductoFase(svc);

                OnModelChanged();

                LoadSequence(svc);

                if (SequenceLoaded != null)
                    SequenceLoaded(this, EventArgs.Empty);
            }

            ControlBienByProducto();

            DeleteTempDirectories();
        }
        public void LoadPostVenta(int? numProducto, int? version)
        {
            if (!IsUserLogged)
                return;

            List<ProductFaseViewModel> fases;

            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                fases = db.PRODUCTOFASE
                    .Include("FASE")
                    .Where(p => p.NUMPRODUCTO == numProducto && p.VERSION == version && p.FASETEST == "S")
                    .Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                    .ToList();


                ProductFaseViewModel fase = shell.ShowTableDialog<ProductFaseViewModel>("FASE", fases);
                if (fase == null)
                    return;

                Model.IdFase = fase.IDFASE;
            }

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                LoadPostVenta(svc, numProducto, version, Model.IdFase);

                ValidaProductoFase(svc);

                LoadParametersByProductoFase(svc);

                OnModelChanged();

                LoadSequence(svc);

                if (SequenceLoaded != null)
                    SequenceLoaded(this, EventArgs.Empty);
            }

            ControlBienByProducto();

            DeleteTempDirectories();
        }
        public void LoadAutoTest()
        {
            PRODUCTO producto = null;

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                producto = svc.GetProductoAutoTest(Dns.GetHostName());
                if (producto == null)
                    throw new Exception(string.Format("Información de test del pc {0} no encontrada!", Dns.GetHostName()));

                LoadProductoVersion(svc, producto.NUMPRODUCTO, producto.VERSION, "120");

                ValidaProductoFase(svc);

                LoadParametersByProductoFase(svc);

                OnModelChanged();

                LoadSequence(svc);

                if (SequenceLoaded != null)
                    SequenceLoaded(this, EventArgs.Empty);
            }

            ControlBienByProducto();

            DeleteTempDirectories();
        }
        public void LoadTIC(int bastidor)
        {
            List<FasesDecription> fases;
            ORDEN ordenSubconjunto;

            using (IDezacService db = DI.Resolve<IDezacService>())
            {
                ORDENPRODUCTO orden = db.GetOrdenProductoByBastidor(bastidor);
                if (orden == null)
                    return;

                ordenSubconjunto = db.GetOrden(orden.NUMORDEN);
                if (ordenSubconjunto == null)
                    return;
            }

            Model.Orden = ordenSubconjunto;
            Model.BastidorTIC = Convert.ToUInt64(bastidor);

            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                fases = db.PRODUCTOFASE
                        .Include("FASE")
                        .Where(p => p.NUMPRODUCTO == ordenSubconjunto.NUMPRODUCTO)
                        .GroupBy(g => new { g.IDFASE, g.FASE.DESCRIPCION })
                        .OrderBy(t => t.FirstOrDefault().IDFASE)
                        .Select(s => new FasesDecription { IDFASE = s.FirstOrDefault().IDFASE, FASE = s.FirstOrDefault().FASE.DESCRIPCION })
                        .ToList();
            }

            FasesDecription fase = null;

            if (fases.Count > 1)
                fase = shell.ShowTableDialog<FasesDecription>("FASE", fases);
            else
                fase = fases.FirstOrDefault();

            if (fase == null)
                return;

            using (IDezacService svc = DI.Resolve<IDezacService>())
            {
                LoadProductoVersion(svc, ordenSubconjunto.NUMPRODUCTO, ordenSubconjunto.VERSION, fase.IDFASE);

                Model.IsReproceso = Model.Orden == null ? false : Model.Orden.REPROCESO == "S";
                Model.MantenerNumSerie = Model.Orden == null ? false : Model.Orden.MANTENERNROSERIE == "S";

                ValidaProductoFase(svc);

                LoadParametersByProductoFase(svc);

                LoadVersionTestReport(svc);

                LoadLineaNumCajasByOrden(svc);

                OnModelChanged();

                LoadSequence(svc);

                if (SequenceLoaded != null)
                    SequenceLoaded(this, EventArgs.Empty);

                ControlPrinterDefaultSelection();

                ControlLaser(svc, Model.Orden, Model.Producto);
            }

            ControlBienByProducto();

            DeleteTempDirectories();
        }
        #endregion

        #region Metodos Privados
        private TestContextModel CreateTestContext(int numInstance)
        {

            var data = new TestContextModel
            {
                IdEmpleado = Model.IdEmpleado,
                IdOperario = Model.IdOperario,
                IdFamilia = Model.Producto.IDFAMILIA,
                DescFamilia = Model.Producto.ARTICULO.DESCRIPCION,
                NumFamilia = Model.Producto.NUMFAMILIA,
                NumProducto = Model.Producto.NUMPRODUCTO,
                ModelLeader = Model.Producto.T_FAMILIA.IDEMPLEADO,
                Version = Model.Producto.VERSION,
                IdFase = Model.IdFase,
                Cantidad = Model.Orden != null ? (int)Model.Orden.CANTIDAD : 0,
                CantidadPendiente = Model.Orden != null ? Model.Orden.PENDIENTE.HasValue ? (int)Model.Orden.PENDIENTE.Value : 0 : 0,
                NumCajaActual = Model.Orden != null ? Model.CajaActual : 0,
                NumEquiposCaja = Model.Orden != null ? Model.EquiposEntradosCajaActual : 0,
                NumOrden = Model.Orden != null ? Model.Orden.NUMORDEN : 0,
                Nivelcontroltest = Model.Orden != null ? Model.Orden.NIVELCONTROLTEST != null ? Convert.ToInt32(Model.Orden.NIVELCONTROLTEST) : 0 : 0,
                PC = Dns.GetHostName(),
                SauronInterface = Application.ProductName,
                SauronVersion = Application.ProductVersion,
                NumAsistencia = Model.NumAsistencia,
                NumLineaAsistencia = Model.NumLineaAsistencia,
                NumSerieAsistencia = Model.NumSerieAsistencia,
                modoPlayer = (ModoTest)Model.ModoPlayer,
                UtilBien = Model.UtilBien,
                MangueraBien = Model.MangueraBien,
                NumUtilBien = Model.NumUtilBien,
                UdsCajaActual = Model.EquiposPorCaja,
                Direcciones = Model.Direcciones,
                LocationTest = Model.TestLocationDescription,
                Atributos = Model.Atributos,
                DescripcionLenguagesCliente = Model.DescripcionLenguagesCliente,
                ReferenciaLineaCliente = Model.ReferenciaLineaCliente,
                ReferenciaOrigen = Model.ReferenciaOrigen,
                EAN = Model.EANcode,
                EAN_MARCA = Model.EANMarcacode,
                MinSerialNumberBox = Model.MinNumSerieCaja,
                MaxSerialNumberBox = Model.MaxNumSerieCaja,
                SerialNumberBoxRange = Model.SerialNumberBoxRange,
                Lote = Model.Lote,
                SequenceVersion = Sequence.Version,
                SequenceName = Sequence.Name,
                IsReproceso = Model.IsReproceso,
                MantenerNumSerie = Model.MantenerNumSerie,
                IsAOITrace = Model.IsAOITrace
            };

            data.TestInfo.SC = SC;
            data.TestInfo.PropertyChanged += OnTestInfoPropertyChanged;
            data.TestInfo.NumInstance = numInstance;
            data.TestInfo.CodCircutor = Model.CodigoCircutor;
            data.TestInfo.CostumerCode2 = Model.CostumerCode2;
            data.TestInfo.ReportVersion = Model.ReportVersion;
            data.TestInfo.DescripcionComercial = Model.DesCom;
            data.TestInfo.RefVenta = Model.RefVenta;
            data.TestInfo.CodigoBcn = Model.CodigoBcn;
            data.TestInfo.NumSerieAssigned = Model.FaseNumSerieAssigned;

            Model.Parametros.ForEach(p =>
            {
                ParamValueCollection list = data.GetGroupList(p.NUMTIPOGRUPO);
                if (list != null)
                    foreach (T_VALORPARAMETRO vp in p.T_VALORPARAMETRO)
                    {
                        var pv = new ParamValue
                        {
                            IdGrupo = vp.NUMGRUPO,
                            IdTipoGrupo = (TipoGrupo)p.NUMTIPOGRUPO,
                            Grupo = list.Name,
                            IdParam = vp.NUMPARAM,
                            ValorInicio = vp.VALORINICIO,
                            Valor = vp.VALOR,
                            IdTipoValor = vp.T_PARAMETRO.NUMTIPOVALOR,
                            IdUnidad = (ParamUnidad)vp.T_PARAMETRO.NUMUNIDAD,
                            Unidad = vp.T_PARAMETRO.T_UNIDAD.UNIDAD,
                            Name = vp.T_PARAMETRO.PARAM,
                            IdCategoria = vp.IDCATEGORIA
                        };

                        list.Add(pv);
                    }
            });

            data.ConfigurationFiles.LoadAllFilesFromBBDD(Model.Producto);

            foreach (KeyValuePair<string, string> keyPair in Model.ResultadosComunes)
                data.Resultados.Set(keyPair.Key, keyPair.Value, ParamUnidad.SinUnidad);

            if (!string.IsNullOrEmpty(Model.PrinterLabel))
                data.TestInfo.PrinterLabel = Model.PrinterLabel;

            if (!string.IsNullOrEmpty(Model.PrinterCharacteristics))
                data.TestInfo.PrinterCharacteristics = Model.PrinterCharacteristics;

            if (!string.IsNullOrEmpty(Model.PrinterIndividualPackage))
                data.TestInfo.PrinterIndividualPackage = Model.PrinterIndividualPackage;

            if (!string.IsNullOrEmpty(Model.PrinterPackingPackage))
                data.TestInfo.PrinterPackingPackage = Model.PrinterPackingPackage;

            if (!string.IsNullOrEmpty(Model.PrinterReport))
                data.TestInfo.PrinterReport = Model.PrinterReport;

            if (Model.UtilBien != null)
                data.UtilBien = Model.UtilBien;

            return data;
        }

        private void LoadOrdenByDataBase(IDezacService svc)
        {
            ORDEN orden = null;
            PRODUCTO producto = null;
            string idFase = "";

            if (string.IsNullOrEmpty(Model.IdEmpleado))
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("No está identificado").Throw();

            DIARIOMARCAJEFASE dmf = svc.GetLastDiarioMarcaje(Model.IdEmpleado);
            if (dmf == null)
                TestException.Create().PROCESO.OPERARIO.PARAMETRO_INCOHERENTE("No existe marcajes para este operario").Throw();

            if (dmf.HOJAMARCAJEFASE == null)
                TestException.Create().PROCESO.OPERARIO.PARAMETRO_INCOHERENTE("No existe marcajes para este operario").Throw();

            orden = svc.GetOrden(dmf.HOJAMARCAJEFASE.NUMORDEN);
            if (orden == null)
                TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("orden inexistente").Throw();

            producto = svc.GetProductoByIOrdenFab(orden.NUMPRODUCTO, orden.VERSION);

            idFase = FaseTestSelection(orden, dmf.IDFASE);
            if (idFase == null)
                TestException.Create().PROCESO.OPERARIO.PARAMETRO_INCOHERENTE("No esta macada en la linea correcta para esta O.F. y Fase").Throw();

            if (producto == null)
                TestException.Create().PROCESO.PARAMETROS_ERROR.PRODUCTO_INCORRECTO("producto inexistente").Throw();

            Model.Producto = producto;
            Model.IdFase = idFase;
            Model.Orden = orden;
            Model.ProductoFase = svc.GetProductoFase(Model.Producto, Model.IdFase);
            Model.DescripcionFase = Model.ProductoFase == null ? string.Format("Fase_{0}", Model.IdFase) : Model.ProductoFase.FASE.DESCRIPCION;
            Model.IsReproceso = Model.Orden == null ? false : Model.Orden.REPROCESO == "S";
            Model.MantenerNumSerie = Model.Orden == null ? false : Model.Orden.MANTENERNROSERIE == "S";
        }
        private string FaseTestSelection(ORDEN orden, string idFase)
        {
            string IdFase = idFase;
            bool IsReproceso = orden.REPROCESO == "S";

            var fases = new List<ProductFaseViewModel>();
            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                var producFase = db.PRODUCTOFASE.Where(p => p.NUMPRODUCTO == orden.NUMPRODUCTO && p.VERSION == orden.VERSION && p.IDFASE == idFase).ToList();
                if (producFase.Count() == 0 && !IsReproceso)
                    return null;

                int secuencia;

                if (IsReproceso)
                {
                    bool fasesTestByOrden = db.ORDENFASETEST.Where(p => p.NUMORDEN == orden.NUMORDEN && p.IDFASE == idFase).ToList().Any();
                    if (fasesTestByOrden)
                        secuencia = db.ORDENFASETEST.Where(p => p.NUMORDEN == orden.NUMORDEN && p.IDFASE == idFase).FirstOrDefault().SECUENCIA;
                    else
                        secuencia = db.PRODUCTOFASE.Where(p => p.NUMPRODUCTO == orden.NUMPRODUCTO && p.VERSION == orden.VERSION && p.IDFASE == idFase).FirstOrDefault().SECUENCIA;
                }
                else
                    secuencia = db.PRODUCTOFASE.Where(p => p.NUMPRODUCTO == orden.NUMPRODUCTO && p.VERSION == orden.VERSION && p.IDFASE == idFase).FirstOrDefault().SECUENCIA;

                fases.AddRange(db.PRODUCTOFASE
                    .Include("FASE")
                    .Where(p => p.NUMPRODUCTO == orden.NUMPRODUCTO && p.VERSION == orden.VERSION && p.SECUENCIAPADRE == secuencia && (p.IDFASE == "038" || p.IDFASE == "039"))
                    .Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                    .ToList());

                fases.AddRange(db.PRODUCTOFASE
                    .Include("FASE")
                    .Where(p => p.NUMPRODUCTO == orden.NUMPRODUCTO && p.VERSION == orden.VERSION && p.SECUENCIAPADRE == secuencia && p.FASETEST == "S")
                    .Select(s => new ProductFaseViewModel { IDFASE = s.IDFASE, FASE = s.FASE.DESCRIPCION, TIEMPOPREPARACION = s.TIEMPOPREPARACION, TIEMPOSTANDARD = s.TIEMPOSTANDARD, TIEMPOTEORICO = s.TIEMPOTEORICO })
                    .ToList());

                if (fases.Count > 0)
                {
                    if (fases.Count == 1)
                        IdFase = fases.FirstOrDefault().IDFASE;
                    else
                    {
                        ProductFaseViewModel fase = shell.ShowTableDialog<ProductFaseViewModel>("FASE", fases);
                        if (fase != null)
                            IdFase = fase.IDFASE;
                    }
                    if (IdFase == "038" || IdFase == "039")
                    {
                        Model.IsAOI = true;
                        if (db.PRODUCTOFASE.Where(p => p.NUMPRODUCTO == orden.NUMPRODUCTO && p.VERSION == orden.VERSION && p.SECUENCIAPADRE == secuencia && p.IDFASE == IdFase).First().FASETEST == "S")
                            Model.IsAOITrace = true;
                    }
                }
            }
            return IdFase;
        }
        private void ValidaProductoFase(IDezacService svc)
        {
            if (Model.Producto.T_FAMILIA == null)
                TestException.Create().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("Producto sin famila de test asignada o marcado en una fase que no es de test").Throw();

            if (Model.Producto.T_FAMILIA.PLATAFORMA != "TS2" && !Model.DescripcionFase.Contains("LASEAR"))
            {
                MessageBox.Show("ESTE PRODUCTO NO TIENE LA PLATAFORMA DE TEST SAURON SYSTEM SELECIONADA, SE EJECUTARA EL TSD2013", "AVISO PLATAFORMA DE TEST", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                OnCloseForm();
            }

            if (Model.Producto.T_FAMILIA.BLOQUEOTEST.Trim().ToUpper() == "S")
                TestException.Create().PROCESO.FAMILIA.BLOQUEADO().Throw();

            if (!svc.GetValidateProductByIDP(Model.Producto.NUMPRODUCTO, Model.Producto.VERSION))
                TestException.Create().PROCESO.PRODUCTO.NO_VALIDADO_IDP().Throw();

            string codigoGrupo = svc.GetCodigoGrupo(Model.IdFase);
            if (Model.ProductoFase.FASETEST.Trim() == "N" && codigoGrupo == null)
                TestException.Create().PROCESO.OPERARIO.FASES_TEST_INCORRECTAS("no esta asignada como fase de test").Throw();
        }
        private void LoadParametersByProductoFase(IDezacService svc)
        {
            Model.ProductoFase = svc.GetProductoFase(Model.Producto, Model.IdFase);
            string descripcionFase = Model.ProductoFase == null ? string.Format("Fase_{0}", Model.IdFase) : Model.ProductoFase.FASE.DESCRIPCION;

            List<T_GRUPOPARAMETRIZACION> parametros = svc.GetGruposParametrizacion(Model.Producto, Model.IdFase);
            if (parametros == null)
                TestException.Create().PROCESO.PRODUCTO.PARAMETRO_INCOHERENTE("Grupos de parametrización inexistentes").Throw();

            Model.Parametros = parametros;
            Model.Direcciones = svc.GetDirecciones(Model.Producto.NUMDIRECCION.GetValueOrDefault(-1));
            Model.CodigoCircutor = svc.GetCodigoCircutor(Model.Producto.NUMPRODUCTO);
            Model.CodigoBcn = svc.GetCodigoBcn(Model.Producto.NUMPRODUCTO);
            Model.CostumerCode2 = svc.GetCostumerCode2(Model.Producto.NUMPRODUCTO);
            Model.Atributos = svc.GetAtributosCliente(Model.Producto.NUMPRODUCTO, Model.CodigoCircutor);
            Model.DescripcionLenguagesCliente = svc.GetDescripcionLenguagesCliente(Model.Producto.NUMPRODUCTO, Model.CodigoCircutor);
            Model.EANcode = svc.GetEANCode(Model.Producto.NUMPRODUCTO, Model.CodigoCircutor);
            Model.EANMarcacode = svc.GetEANMarcaCode(Model.Producto.NUMPRODUCTO, Model.CodigoCircutor);
            Model.ReferenciaLineaCliente = svc.GetRefClientLine(Model.NumOrden);
            Model.ReferenciaOrigen = svc.GetReferenciaOrigen(Model.NumOrden);
            Model.RefVenta = svc.GetReferenciaVenta(Model.Producto.NUMPRODUCTO);
            Model.DesCom = svc.GetDescripcionComercial(Model.Producto.NUMPRODUCTO);
            Model.RunNumInstances = null;
            Model.NumAsistencia = null;
            Model.NumLineaAsistencia = null;
            Model.NumSerieAsistencia = null;
            Model.CajaActual = 0;
            Model.EquiposPorCaja = 1;
        }
        private void LoadRunNumInstances()
        {
            int num = 1;
            if (Model.Parametros != null)
            {
                T_GRUPOPARAMETRIZACION grupo = Model.Parametros.Where(p => p.NUMTIPOGRUPO == 1).FirstOrDefault() ?? null;
                if (grupo == null)
                    num = 1;

                string valor = Model.Parametros.Where(p => p.NUMTIPOGRUPO == 1)
                    .FirstOrDefault()
                    .T_VALORPARAMETRO
                    .Where(p => p.NUMPARAM == 16837) //NUMPARAM_MAX_PARALLEL_INSTANCES;
                    .Select(v => v.VALOR)
                    .FirstOrDefault() ?? "1";

                if (!int.TryParse(valor, out num))
                    num = 1;
            }

            Model.RunNumInstances = num;
        }
        private void LoadVersionTestReport(IDezacService svc)
        {

            int? versionReport = svc.GetVersionTestReport(Model.Producto.T_FAMILIA.NUMFAMILIA);
            if (versionReport.HasValue)
                Model.ReportVersion = versionReport.Value;

            T_GRUPOPARAMETRIZACION configuracion = Model.Parametros.Where(p => p.NUMTIPOGRUPO == 5).FirstOrDefault();
            if (configuracion != null)
            {
                T_VALORPARAMETRO versionReportProduct = configuracion.T_VALORPARAMETRO.Where(p => p.T_PARAMETRO.PARAM == "TEST_REPORT_VERSION").FirstOrDefault();
                if (versionReportProduct != null)
                    Model.ReportVersion = Convert.ToInt32(versionReportProduct.VALOR);
            }
        }
        private void LoadLineaNumCajasByOrden(IDezacService svc)
        {               
            if (!svc.GetValidateLineaHost(Dns.GetHostName()))
                TestException.Create().PROCESO.LINEA.VALOR_INCORRECTO("PC NO ENCONTRADO").Throw();

            if (Model.Producto.ARTICULO.UDSEMBALAJE.HasValue)
                Model.EquiposPorCaja = Model.Producto.ARTICULO.UDSEMBALAJE.Value;
            else
            {
                shell.ShowDialog("Error FALTA UDS. EMBALAJE", new Label() { Text = "ERROR, FALTAN LAS UDS. EMBALAJE DE ESTE PRODUCTO ENTRADA EN EL PGI, CONTACTE CON OPERACIONES", Width = 400, Height = 80, Padding = new Padding(10) }, MessageBoxButtons.OK);
                TestException.Create().PROCESO.MATERIAL.FALTA_VALOR_PARAMETRO("NUMERO DE UDS. EMBALAJE DE ESTE PRODUCTO").Throw();
            }
            if (Model.EquiposPorCaja != 0)
            {
                Model.CajaActual = Model.CajaActual == 0 ? 1 : Model.CajaActual;
                InitUpdateBoxCurrent(null);
            }
        }
        private void LoadSequenceFileTest(IDezacService svc)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.InitialDirectory = ConfigurationManager.AppSettings["PathSequences"];
                if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName != null)
                {
                    Sequence = SequenceModel.LoadFile(ofd.FileName);
                    return;
                }
                else
                {
                    if (MessageBox.Show("¿Desea seleccionar un archivo de secuencia?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        LoadSequenceFileTest(svc);
                    else
                        LoadSequence(svc);
                }
            }
        }
        private void LoadSequence(IDezacService svc)
        {
            Tuple<int, byte[]> data;

            data = svc.GetSequenceFileData(Model.Producto, Model.IdFase);

            if (data == null)
            {
                LoadSequenceFile();
                return;
            }

            string content = UTF8Encoding.UTF8.GetString(data.Item2);
            Sequence = SequenceModel.Load(content);
        }
        private void LoadSequenceFile(string fase = null)
        {
            string sequenceFileName = string.Empty;
            if (fase == null)
            {
                string descripcion = Model.Producto.ARTICULO == null ? Model.Producto.T_FAMILIA.DESCRIPCION : Model.Producto.ARTICULO.DESCRIPCION;
                string message = string.Format("AVISO PRODUCTO: {0} DESCRIPCION: {1} SIN SECUENCIA EN LA BBDD", Model.Producto.NUMPRODUCTO, descripcion);
                SendMail.Send("AVISO SECUENCIA DE TEST NO SUBIDA A  LA BBDD", message + "\r\n", true, false, true);

                T_GRUPOPARAMETRIZACION identificacion = Model.Parametros.Where(p => p.NUMTIPOGRUPO == 5).FirstOrDefault();
                if (identificacion == null)
                    TestException.Create().PROCESO.PRODUCTO.PARAMETRO_INCOHERENTE("Grupos de parametrización inexistentes").Throw();

                T_VALORPARAMETRO sequenceFile = identificacion.T_VALORPARAMETRO.Where(p => p.T_PARAMETRO.PARAM == "SEQUENCE_FILE").FirstOrDefault();
                if (sequenceFile == null || string.IsNullOrEmpty(sequenceFile.VALOR))
                    TestException.Create().PROCESO.PRODUCTO.PARAMETRO_INCOHERENTE("Falta especificar el parámetro del fichero de secuencia").Throw();

                string sequenceFileParam = sequenceFile.VALOR;
                sequenceFileName = Path.Combine(ConfigurationManager.AppSettings["PathSequences"], sequenceFileParam);
                if (!File.Exists(sequenceFileName))
                    TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("No se ha encontrado el archivo de secuencias de test").Throw();
            }
            else
            {
                if (fase.Contains("LASEAR"))
                    sequenceFileName = Path.Combine(ConfigurationManager.AppSettings["PathInnerSequences"], "LASEAR.SEQ");
                else if (fase.Contains("AOI"))
                    sequenceFileName = Path.Combine(ConfigurationManager.AppSettings["PathInnerSequences"], "AOI.SEQ");
                else
                    TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA("no tiene fichero de secuencia de test cargada en la solución").Throw();
            }
            try
            {
                Sequence = SequenceModel.LoadFile(sequenceFileName);
            }
            catch (Exception ex)
            {
                TestException.Create().SOFTWARE.SECUENCIA_TEST.CONFIGURACION_INCORRECTA(string.Format("Fichero incorrecto: \r { 0}.SEQ  \r Causa: \r { 1}", sequenceFileName, ex.Message)).Throw();
            }
        }
        private void ControlPrinterDefaultSelection()
        {
            Model.PrinterLabel = "NINGUNA";
            Model.PrinterCharacteristics = "NINGUNA";
            Model.PrinterIndividualPackage = "NINGUNA";
            Model.PrinterPackingPackage = "NINGUNA";

            if (PrinterSettings.InstalledPrinters.Count == 0)
                return;

            var printer = new PrinterSettings();

            foreach (object item in PrinterSettings.InstalledPrinters)
            {
                printer.PrinterName = item.ToString();

                if (printer.IsValid)
                {

                    if (Model.PrinterLabel.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("LABEL") || printer.PrinterName.ToUpper().Contains("#1"))
                            Model.PrinterLabel = printer.PrinterName;

                    if (Model.PrinterCharacteristics.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("LABEL") || printer.PrinterName.ToUpper().Contains("#1"))
                            Model.PrinterCharacteristics = printer.PrinterName;

                    if (Model.PrinterIndividualPackage.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("INDIVIDUAL") || printer.PrinterName.ToUpper().Contains("#2"))
                            Model.PrinterIndividualPackage = printer.PrinterName;


                    if (Model.PrinterPackingPackage.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("CONJUNTO") || printer.PrinterName.ToUpper().Contains("#3"))
                            Model.PrinterPackingPackage = printer.PrinterName;
                }
            }
        }
        private void ControlLaser(IDezacService svc, ORDEN orden, PRODUCTO product)
        {
            List<PRODUCTOFASE> productoFase = svc.GetProductoFases(product);
            IEnumerable<PRODUCTOFASE> faseLasear = productoFase.Where(p => p.FASE.CODIGOGRUPO == "LAS");

            if (faseLasear.Count() == 0 || orden == null || Model.ModoPlayer != ModoPlayer.Normal)
                return;

            int piecesLasered = svc.GetLaseredPieces(orden.NUMORDEN);

            if (piecesLasered < (orden.CANTIDAD - (orden.CANTIDAD * 1.1m)))
                MessageBox.Show("SE HA DETECTADO QUE NO ESTAN LASEADAS TODAS LAS PIEZAS DE ESTA ORDEN", "LASER CONTROLLER", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        }
        private void DeleteTempDirectories()
        {
            string url = ConfigurationManager.AppSettings["PathFileLabelTemplate"];

            if (Directory.Exists(url))
                Directory.Delete(url, true);

            Thread.Sleep(1000);

            if (!Directory.Exists(url))
                Directory.CreateDirectory(url);
        }
        private void LoadProductoVersion(IDezacService svc, int? numProducto, int? version, string idFase)
        {
            PRODUCTO producto = null;
            producto = svc.GetProductoByIOrdenFab(numProducto.Value, version.Value);

            if (producto == null)
                TestException.Create().PROCESO.PARAMETROS_ERROR.PRODUCTO_INCORRECTO("producto inexistente").Throw();

            Model.Producto = producto;
            Model.IdFase = idFase;
            Model.ProductoFase = svc.GetProductoFase(Model.Producto, Model.IdFase);
            Model.DescripcionFase = Model.ProductoFase == null ? string.Format("Fase_{0}", Model.IdFase) : Model.ProductoFase.FASE.DESCRIPCION;
        }
        private void LoadPostVenta(IDezacService svc, int? numProducto, int? version, string idFase)
        {
            ORDEN orden = null;
            PRODUCTO producto = null;

            AsistenciaTecnicaViewModel asistencia = shell.ShowDialog<AsistenciaTecnicaViewModel>("ASISTENCIA TECNICA",
             () =>
             {
                 return new AsistenciaTecnicaView(this, shell, numProducto.Value);
             }
            , MessageBoxButtons.YesNoCancel, (c, d) =>
            {
                if (d == DialogResult.Yes)
                    return ((AsistenciaTecnicaView)c).Current;

                if (d == DialogResult.Cancel)
                    return null;

                return new AsistenciaTecnicaViewModel()
                {
                    NUMASISTENCIA = 0,
                };
            });

            if (asistencia == null)
                return;

            if (asistencia.NUMASISTENCIA != 0)
            {
                Model.NumAsistencia = asistencia.NUMASISTENCIA;
                Model.NumLineaAsistencia = asistencia.NUMLIN;
                Model.NumSerieAsistencia = asistencia.NUMEROSERIE;

                int? numorden = svc.NuevaOrdenAsistencia(asistencia.NUMASISTENCIA, version.Value);
                if (!numorden.HasValue)
                    TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("orden inexistente").Throw();

                orden = svc.GetOrden(numorden.Value);
                if (orden == null)
                    TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("orden inexistente").Throw();

                producto = svc.GetProductoByIOrdenFab(orden.NUMPRODUCTO, orden.VERSION);
            }
            else
                producto = svc.GetProductoByIOrdenFab(numProducto.Value, version.Value);

            if (producto == null)
                TestException.Create().PROCESO.PARAMETROS_ERROR.PRODUCTO_INCORRECTO("producto inexistente").Throw();

            Model.Producto = producto;
            Model.IdFase = idFase;
            Model.Orden = orden;
            Model.ProductoFase = svc.GetProductoFase(Model.Producto, Model.IdFase);
        }

        #endregion

        #region control Bienes
        private void ControlBienByProducto()
        {
            var bienService = new BienService(Model.Producto.NUMPRODUCTO, Model.Producto.VERSION, Model.IdFase);
            foreach (BienListType Listbien in bienService.ListaBienes)
            foreach (BienClass bien in Listbien.ListBien)
            {
                if (Model.ResultadosComunes.ContainsKey(Listbien.tipoBien.ToString()))
                    continue;
                    
                if (Listbien.tipoBien != TypeBien.TORRE)
                {
                    int numBien = 0;

                    var userControl = new BienInputlView(Listbien.tipoBien, bien);
                    if (shell.ShowDialog("CONTROL DE TRAZABILIDAD DE LOS UTILLAJES", userControl, null) != DialogResult.OK)
                        TestException.Create().PROCESO.OPERARIO.FALTA_VALOR_PARAMETRO("NO SE HA INTRODUCIDO CORRECTAMENTE EL BIEN PARA EL CONTROL DE TRAZABILIDAD DE LOS UTILLAJES").Throw();

                    numBien = userControl.NumBien.Value;

                    if (bien.HasHjios)
                    {
                        BienClass hijo = bien.bienHijos.Where(p => p.Bien.NUMBIEN == numBien).FirstOrDefault();
                        if (hijo == null)
                            ControlBienSendWarningMail(Model, bien.bienHijos, Listbien.tipoBien, numBien);
                        else
                        {
                            BienTestException(hijo);
                            numBien = hijo.Bien.NUMBIEN;
                        }
                    }
                    else
                    {
                        if (numBien != bien.Bien.NUMBIEN)
                            ControlBienSendWarningMail(Model, bien, Listbien.tipoBien, numBien);

                        BienTestException(bien);
                    }

                    if (Listbien.tipoBien == TypeBien.UTIL_TIPO_MAQUINA || Listbien.tipoBien == TypeBien.UTIL_TIPO_NO_MAQUINA)
                        Model.NumUtilBien = numBien;

                    if (Listbien.tipoBien == TypeBien.MANGUERA)
                        Model.NumMangueraBien = numBien;

                    Model.ResultadosComunes.Add(Listbien.tipoBien.ToString(), numBien.ToString());
                }
                //else
                //{
                //    using (IDezacService svc = DI.Resolve<IDezacService>())
                //    {
                //        //var torre = svc.GetTorreByPC(Model.DevicePC);
                //        // if(torre == null)
                //        //     TestException.Create().PROCESO.LINEA.VALOR_INCORRECTO("NO SE HA ENCONTRADO UNA TORRE REFERENCIADA AL PC").Throw();

                //        // Model.ResultadosComunes.Add(Listbien.tipoBien.ToString(), torre.NUMBIEN.ToString());

                //        //if(torre.NUMBIEN)
                //    }
                //}
            }
        }
        private void ControlBienSendWarningMail(TestModel Model, BienClass bien, TypeBien tipoBien, int bienInput)
        {
            var message = new StringBuilder();
            message.AppendFormat("PRODUCTO {0} / {1}    DESCRIPCION: {2}    FAMILIA:{3}    FASE:{4}     MODEL LEADER: {5}", Model.Producto.NUMPRODUCTO, Model.Producto.VERSION, Model.Producto.ARTICULO.DESCRIPCION, Model.Producto.IDFAMILIA, Model.IdFase, Model.Producto.T_FAMILIA.IDEMPLEADO);
            message.AppendLine();
            message.AppendFormat("BIEN:{0} DESCRPCION {1} COMO TIPO {2} NECESARIOS PARA REALIZAR EL TEST", bien.Bien.NUMBIEN, bien.Descripcion, tipoBien.ToString());
            message.AppendFormat("Y SE HA INTRODUICIDO EL BIEN:{0} CUANDO NO EXISTE O NO ES DEL TIPO ESPECIFICADO", bienInput);
            message.AppendLine();

            SendMail.Send("¡¡¡ ATENCION !!! BIEN INCORRECTO EN LA ESTRUCTURA DEL PRODUCTO FASE", message + "\r\n", false, false, false, true);
        }
        private void ControlBienSendWarningMail(TestModel Model, List<BienClass> bien, TypeBien tipoBien, int bienInput)
        {
            var message = new StringBuilder();
            message.AppendFormat("PRODUCTO {0} / {1}    DESCRIPCION: {2}    FAMILIA:{3}    FASE:{4}     MODEL LEADER: {5}", Model.Producto.NUMPRODUCTO, Model.Producto.VERSION, Model.Producto.ARTICULO.DESCRIPCION, Model.Producto.IDFAMILIA, Model.IdFase, Model.Producto.T_FAMILIA.IDEMPLEADO);

            foreach (BienClass bienChild in bien)
            {
                message.AppendLine();
                message.AppendFormat("BIEN:{0} DESCRPCION {1} COMO TIPO {2} NECESARIOS PARA REALIZAR EL TEST", bienChild.Bien.NUMBIEN, bienChild.Descripcion, tipoBien.ToString());
                message.AppendFormat("Y SE HA INTRODUICIDO EL BIEN:{0} CUANDO NO EXISTE O NO ES DEL TIPO ESPECIFICADO", bienInput);
                message.AppendLine();
            }

            SendMail.Send("¡¡¡ ATENCION !!! BIEN INCORRECTO EN LA ESTRUCTURA DEL PRODUCTO FASE", message + "\r\n", false, false, false, true);
        }
        private void BienTestException(BienClass bien)
        {
            if (bien.ErrorCalibraion)
                MessageBox.Show("BIEN CON UNA CALIBRACION CADUCADA", "AVISO BIEN FUERA TIEMPO DE LA CALIBRACION", MessageBoxButtons.OK, MessageBoxIcon.Error);
            // TestException.Create().HARDWARE.INSTRUMENTOS.CALIBRACION_CADUCADA(string.Format("{0}_{1}", bien.Bien.NUMBIEN, bien.Descripcion)).Throw();

            if (bien.HasAsistenciaCalibracion)
                MessageBox.Show("BIEN CON UNA ASISTENCIA CADUCADA", "AVISO BIEN FUERA TIEMPO DE LA ASISTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            // TestException.Create().HARDWARE.INSTRUMENTOS.ASISTENCIA_CADUCADA(string.Format("{0}_{1}", bien.Bien.NUMBIEN, bien.Descripcion)).Throw();

            if (bien.WarningCalibracion)
                MessageBox.Show("BIEN CON UNA PROGRAMACION DE CALIBRACION APUNTO DE CADUCARSE", "AVISO BIEN FUERA TIEMPO DE CALIBRACIÓN EN DOS SEMANAS", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (bien.WarningMantenimeinto)
                MessageBox.Show("BIEN CON UNA PROGRAMACION DE MANTENIMIENTO APUNTO DE CADUCARSE", "AVISO BIEN FUERA TIEMPO DE MANTENIMIENTO EN DOS SEMANAS", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (bien.HasAsistenciaAjuste)
                MessageBox.Show("BIEN CON UNA PROGRAMACION DE AJUSTE NO REALIZADA", "BIEN FUERA DE TIEMPO DE AJUSTE", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (bien.HasAsistenciaMantenimiento)
                MessageBox.Show("BIEN CON UNA PROGRAMACION DE MANTENIMIENTO NO REALIZADA", "BIEN FUERA DE TIEMPO DE MANTENIMIENTO", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (bien.HasAsistenciaReparacion)
                MessageBox.Show("BIEN CON UNA PROGRAMACION DE REPARACION NO REALIZADA", "BIEN FUERA DE TIEMPO DE REPARACION", MessageBoxButtons.OK, MessageBoxIcon.Error);

            if (bien.ErrorMantenimeinto)
                MessageBox.Show("BIEN CON UNA PROGRAMACION DE MANTENIMIENTO NO REALIZADA", "BIEN FUERA DE TIEMPO DE MANTENIMIENTO", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        #region Control Cajas
        public void InitUpdateBoxCurrent(TestContextModel testContext)
        {
            if (Model.Orden == null || Model.EquiposPorCaja == 0)
                return;

            if ((Model.ModoPlayer == ModoPlayer.Normal || Model.ModoPlayer == ModoPlayer.TIC) && !Model.IsProcess)
            {
                using (IDezacService svc = DI.Resolve<IDezacService>())
                {
                    Model.CajaActualID = svc.GetIDCajaActual(Model.Orden.NUMORDEN);
                    Tuple<int, int> resultTupla = svc.GetCajaActual(Model.CajaActualID);
                    Model.CajaActual = resultTupla.Item1;
                    Model.CajasTotales = resultTupla.Item2;
                    Model.EquiposEntradosCajaActual = svc.GetNumEquiposCajaActual(Model.CajaActualID);
                    Model.EquiposFaltanCajaActual = Model.EquiposPorCaja - Model.EquiposEntradosCajaActual;
                    Model.Lote = svc.GetLoteCaja(Model.CajaActualID);
                }

                if (testContext != null)
                {
                    testContext.NumCajaActual = Model.CajaActual;
                    testContext.Lote = Model.Lote;
                }
            }
        }
        public string FinishUpdateBoxCurrent(RunnerTestsInfo e)
        {

            if (Model.Orden == null || Model.EquiposPorCaja == 0)
                return null;

            if ((Model.ModoPlayer == ModoPlayer.Normal || Model.ModoPlayer == ModoPlayer.TIC) && !Model.IsProcess)
                using (IDezacService svc = DI.Resolve<IDezacService>())
                {
                    Model.CajaActualID = svc.GetIDCajaActual(Model.Orden.NUMORDEN);
                    Tuple<int, int> resultTupla = svc.GetCajaActual(Model.CajaActualID);
                    Model.CajaActual = resultTupla.Item1;
                    Model.CajasTotales = resultTupla.Item2;
                    Model.EquiposEntradosCajaActual = svc.GetNumEquiposCajaActual(Model.CajaActualID);
                    Model.EquiposFaltanCajaActual = Model.EquiposPorCaja - Model.EquiposEntradosCajaActual;
                    Model.MinNumSerieCaja = svc.GetMinNumSerieCaja(Model.CajaActualID);
                    Model.MaxNumSerieCaja = svc.GetMaxNumSerieCaja(Model.CajaActualID);
                    Model.SerialNumberBoxRange = svc.GetAllSerialNumbersBox(Model.CajaActualID);
                }

            var sb = new StringBuilder();
            bool multiple = e.Contexts.Count > 1;

            if (Model.CajaActual >= 1)
                foreach (SequenceContext test in e.Contexts.Where(p => p.ExecutionResult == TestExecutionResult.Completed))
                {
                    ITestContext testContext = test.Services.Get<ITestContext>();
                    if (multiple)
                        sb.AppendFormat("     El equipo {0} se ha registrado en la caja {1}",
                            test.NumInstance, testContext != null ? testContext.NumCajaActual : Model.CajaActual);
                    else
                        sb.AppendFormat("     Este equipo se ha registrado en la caja {0}", Model.CajaActual);

                    sb.AppendLine();

                    using (IDezacService svc = DI.Resolve<IDezacService>())
                    {
                        Model.BoxIsFinished = svc.GetBoxHasFinished(Model.CajaActualID, testContext.TestInfo.NumSerie);
                    }

                    if (Model.BoxIsFinished)
                    {
                        sb.AppendFormat("     Caja {0} completada", Model.CajaActual);
                        sb.AppendLine();

                        testContext.MinSerialNumberBox = Model.MinNumSerieCaja;
                        testContext.MaxSerialNumberBox = Model.MaxNumSerieCaja;
                        testContext.SerialNumberBoxRange = Model.SerialNumberBoxRange;
                        testContext.NumEquiposCaja = Model.EquiposEntradosCajaActual;

                        Dictionary<string, string> value = reportsService.PrintBoxLabel(testContext);
                        if (value != null)
                        {
                            string json = AppProfile.Serialize(value);
                            DataUtils.SaveFileNumTestFase(testContext.TestInfo.NumTestFase.Value, string.Format("PRINT_LABEL_EmbalajeConjunto_{0}", Model.CajaActualID), "501", UTF8Encoding.UTF8.GetBytes(json));
                        }
                    }
                }

            return sb.ToString();
        }
        public int CurrentBox(int currentBox)
        {
            int uds = shell.ShowInputKeyboard<int>("ENTRADA DE CAJA ACTUAL", "Introduce el nº de caja actual", currentBox, null, currentBox.ToString());
            if (uds < 0)
                uds = 0;

            return uds;
        }
        #endregion

        #region Eventos
        private void OnTestInfoPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SC.Post(_ =>
            {
                if (TestInfoChanged != null)
                    TestInfoChanged(sender, EventArgs.Empty);
            }, null);
        }
        protected void OnModelChanged()
        {
            if (ModelChanged != null)
                SC.Post(_ => { ModelChanged(this, EventArgs.Empty); }, null);
        }
        protected void OnCloseForm()
        {
            if (ModelChanged != null)
                SC.Post(_ => { CloseForm(this, EventArgs.Empty); }, null);
        }
        public void FireEvent(object sender, string name)
        {
            if (OnEventFired != null)
                OnEventFired(sender, name);
        }
        #endregion

    }
}
