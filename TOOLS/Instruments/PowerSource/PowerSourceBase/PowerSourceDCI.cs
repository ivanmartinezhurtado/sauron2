﻿using Dezac.Core.Exceptions;
using Instruments.Utility;
using log4net;

namespace Instruments.PowerSource
{
    public abstract class PowerSourceDCI : PowerSourceDCBase<double>
    {
        public PowerSourceDCI(ILog logger) : base(0D, logger)
        {
        }

        public double ReadVoltage()
        {
            var cmd = new Command<Commands>(Commands.READ_VOLTAGE, true);

            if (SendCommand(cmd))
                return DeviceData.Voltage;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        public double ReadCurrent()
        {
            var cmd = new Command<Commands>(Commands.READ_CURRENT, true);

            if (SendCommand(cmd))
                return DeviceData.Current;

            TestException.Create().HARDWARE.INSTRUMENTOS.LECTURA_FUENTE(cmd.Exception.Message).Throw();
            return 0;
        }

        protected override void ResetPresets()
        {
            this.DevicePreset.Voltage = 0;
            this.DevicePreset.Current = 0;
        }
    }
}
