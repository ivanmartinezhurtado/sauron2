﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Services;
using Instruments.Router;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(GetDeviceIPDescription))]
    public class GetLastDeviceIP : ActionBase
    {
        [Description("Nombre del usuario en el router")]
        [Category("3. Configuration")]
        [DefaultValue("root")]
        public string UserName { get; set; }

        [Description("Contraseña")]
        [Category("3. Configuration")]
        [DefaultValue("dezac2309")]
        public string Password { get; set; }
        
        [DefaultValue("")]
        [Category("3. Configuration")]
        [Description("Dirección IP que devolverá si existe el Host en caso de no encontrar ningún cliente en el router")]
        public string DefaultIP { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            string variableName = string.Empty;

            Router router = Router.Default();

            if (!string.IsNullOrEmpty(UserName))
            {
                router.UserName = UserName;
                router.Password = Password;
            }

            var data = SequenceContext.Current.Services.Get<ITestContext>();

            string myMAC = string.Empty;
            string deviceIP = string.Empty;

            if (DataOutput.IsKeyNullOrEmpty)
                variableName = "LastDevice";
            else
                variableName = DataOutput.Value.ToString();

            var sampler = Sampler.Run(
                () => { return new SamplerConfig { NumIterations = 5, Interval = 3500, CancelOnException = true }; },
                (step) =>
                {
                    try
                    {
                        var clients = router.GetConnectedClientsFromWeb(true);
                        if (clients.Count == 0)
                        {
                            if (!string.IsNullOrEmpty(DefaultIP))
                                if (router.PingIP(DefaultIP))
                                {
                                    if (SequenceContext.Current.Variables.ContainsKey(variableName + "_IP"))
                                        SequenceContext.Current.Variables.AddOrUpdate(variableName + "_IP", DefaultIP);
                                    else
                                        SequenceContext.Current.Variables.AddOrUpdate(variableName + "_IP", DefaultIP);

                                    SequenceContext.Current.ResultList.Add(variableName + "_IP" + DefaultIP);

                                    step.Cancel = true;
                                }

                            return;
                        }
                        myMAC = clients[0].MAC;
                        deviceIP = clients[0].IP;

                        if (SequenceContext.Current.Variables.ContainsKey(variableName + "_MAC"))
                            SequenceContext.Current.Variables.AddOrUpdate(variableName + "_MAC", myMAC);
                        else
                            SequenceContext.Current.Variables.AddOrUpdate(variableName + "_MAC", myMAC);

                        SequenceContext.Current.ResultList.Add(variableName + "_MAC" +  myMAC);


                        if (SequenceContext.Current.Variables.ContainsKey(variableName + "_IP"))
                            SequenceContext.Current.Variables.AddOrUpdate(variableName + "_IP", deviceIP);
                        else
                            SequenceContext.Current.Variables.AddOrUpdate(variableName + "_IP", deviceIP);

                        SequenceContext.Current.ResultList.Add(variableName + "_IP" + deviceIP);


                        if (myMAC != "00:01:02:03:04:05")
                            if (data != null)
                                data.TestInfo.NumMAC = myMAC;

                        step.Cancel = true;
                    }
                    catch (TimeoutException)
                    {
                        Thread.Sleep(500);
                    }
                });

            sampler.ThrowExceptionIfExists();

            
            Assert.IsTrue(sampler.Canceled, Error().UUT.CONFIGURACION.NO_COMUNICA("No se ha detectado ningún equipo conectado al router"));   
        }
    }

    public class GetDeviceIPDescription : ActionDescription
    {
        public override string Name { get { return "GetLastDeviceIPAndMAC"; } }
        public override string Category { get { return "Tools/Ethernet/Tools"; } }
        public override string Description { get { return "Action que sirve para coger y guardar la última IP y MAC del equipo"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
