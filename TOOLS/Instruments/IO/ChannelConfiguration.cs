﻿using System;
using System.Collections.Generic;

namespace Instruments.IO
{
    public enum IOType
    {
        Input = 1,
        Output = 2,
    }

    public interface IIOBaseConfiguration
    {
        int NumDI { get; set; }
        int NumDO { get; set; }
        void Apply();
    }

    public class IOBaseConfiguration : IIOBaseConfiguration
    {
        protected IOBase ioBase;

        public int NumDI { get; set; }
        public int NumDO { get; set; }

        public IOBaseConfiguration(IOBase ioBase)
        {
            this.ioBase = ioBase;
        }

        public virtual void Apply()
        {

        }
    }

    public class ChannelConfiguration : IOBaseConfiguration
    {
        public List<IOChannel> Channels { get; protected set; }

        public ChannelConfiguration(IOBase ioBase) 
            : base(ioBase)
        {
        }

        public ChannelConfiguration SetChannels(int numChannels, int numPortsPerChannel)
        {
            return SetChannels(numChannels, numPortsPerChannel, null);
        }

        public ChannelConfiguration SetChannels(int numChannels, int numPortsPerChannel, Action<IOChannel> action = null)
        {
            this.Channels = new List<IOChannel>(numChannels);

            for (int i = 0; i < numChannels; i++)
            {
                var channel = new IOChannel(i, numPortsPerChannel);

                this.Channels.Add(channel);

                if (action != null)
                    action(channel);
            }

            return this;
        }

        public ChannelConfiguration SetChannelAsInput(int channel)
        {
            Channels[channel].IOType = IOType.Input;

            return this;
        }

        public ChannelConfiguration SetChannelAsOutput(int channel)
        {
            Channels[channel].IOType = IOType.Output;

            return this;
        }

        public ChannelConfiguration SetPortAsInput(int channel, int port)
        {
            Channels[channel].Ports[port].IOType = IOType.Input;

            return this;
        }

        public ChannelConfiguration SetPortAsOutput(int channel, int port)
        {
            Channels[channel].Ports[port].IOType = IOType.Output;

            return this;
        }

        public override void Apply()
        {
            NumDI = 0;
            NumDO = 0;

            foreach (var channel in Channels)
                foreach (var port in channel.Ports)
                    if (port.IOType == IOType.Input)
                    {
                        port.MinBit = NumDI;
                        NumDI = port.MaxBit + 1;
                    }
                    else
                    {
                        port.MinBit = NumDO;
                        NumDO = port.MaxBit + 1;
                    }

            ioBase.ApplyConfig(this);
        }
    }
}
