﻿using log4net;

namespace Dezac.Tests.Services.Logger
{
    public static class LogExtention
    {
        public static void Verbose(this ILog log, string message)
        {
            log.Logger.Log(typeof(LogExtention),log4net.Core.Level.Verbose, message, null);
        }

        public static void VerboseFormat(this ILog log, string format, params object[] args)
        {
            var message = string.Format(format, args);

            log.Logger.Log(typeof(LogExtention), log4net.Core.Level.Verbose, message, null);
        }

        public static bool IsVerboseEnabled(this ILog log)
        {
            return log.Logger.IsEnabledFor(log4net.Core.Level.Verbose);
        }

        public static void Trace(this ILog log, string message)
        {
            log.Logger.Log(typeof(LogExtention), log4net.Core.Level.Trace, message, null);
        }

        public static void TraceFormat(this ILog log, string format, params object[] args)
        { 
            var message = string.Format(format, args);

            log.Logger.Log(typeof(LogExtention), log4net.Core.Level.Trace, message, null);
        }

        public static bool IsTraceEnabled(this ILog log)
        {
            return log.Logger.IsEnabledFor(log4net.Core.Level.Trace);
        }
    }
}
