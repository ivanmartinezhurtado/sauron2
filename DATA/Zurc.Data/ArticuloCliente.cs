namespace Zurc.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("VArticuloCliente")]
    public partial class ArticuloCliente
    {
        [Key]
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int ArticuloId { get; set; }
        public string CodigoSegunCliente { get; set; }
        public string DescripcionSegunCliente { get; set; }
        public string Codigo { get; set; }
    }
}
