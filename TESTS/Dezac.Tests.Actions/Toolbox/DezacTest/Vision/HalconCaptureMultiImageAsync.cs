﻿using Dezac.Tests.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(HalconCaptureMultiImageAsyncDescription))]
    public class HalconCaptureMultiImageAsync : ActionBase
    {
        [Description("Nombre del test de visión artificial")]
        [Category("3. Configuration")]
        [DefaultValue("LCD")]
        public string TestName { get; set; }

        [Description("Nombre del equipo")]
        [Category("3. Configuration")]
        [DefaultValue("Device")]
        public string DeviceName { get; set; }

        [Description("Nombre de la primera imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image1Name { get; set; }

        [Description("Sirve para hacer una action después de la Image1 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image1Action { get; set; }

        [Description("Nombre de la segunda imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image2Name { get; set; }

        [Description("Sirve para hacer una action después de la Image2 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image2Action { get; set; }

        [Description("Nombre de la tercera imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image3Name { get; set; }

        [Description("Sirve para hacer una action después de la Image3 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image3Action { get; set; }

        [Description("Nombre de la cuarta imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image4Name { get; set; }

        [Description("Sirve para hacer una action después de la Image4 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image4Action { get; set; }

        [Description("Nombre de la quinta imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image5Name { get; set; }

        [Description("Sirve para hacer una action después de la Image5 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image5Action { get; set; }

        [Description("Nombre de la sexta imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image6Name { get; set; }

        [Description("Sirve para hacer una action después de la Image6 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image6Action { get; set; }
       
        [Description("Nombre de la séptima imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image7Name { get; set; }

        [Description("Sirve para hacer una action después de la Image7 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image7Action { get; set; }

        [Description("Nombre de la octava imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image8Name { get; set; }

        [Description("Sirve para hacer una action después de la Image8 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image8Action { get; set; }

        [Description("Nombre de la novena imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image9Name { get; set; }

        [Description("Sirve para hacer una action después de la Image9 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image9Action { get; set; }

        [Description("Nombre de la décima imagen")]
        [Category("3. Configuration")]
        [DefaultValue("")]
        public string Image10Name { get; set; }

        [Description("Sirve para hacer una action después de la Image10 como por ejemplo activar un LED")]
        [Category("3. Configuration")]
        public RunMethodAction Image10Action { get; set; }

        
        [Category("3. Configuration")]
        [Description("Tipo de procedimiento que el action debe transformar")]
        [DefaultValue(TestHalconExtension.TypeProcedure.ReadBarCode)]
        public TestHalconExtension.TypeProcedure Type { get; set; }

        [Category("3.1 Camera Selection")]
        [Description("Nombre de la cámara si se ha definido como una variable con el action AddVariable")]
        [DefaultValue("")]
        public string DataInput { get; set; }

        [Category("3.1 Camera Selection")]
        [Description("Número de serie de la cámara (no necesario si tiene DataInput)")]
        [DefaultValue("")]
        public string SerialNumber { get; set; }

        [Category("3.2 Crop and Resize image")]
        [Description("Fila 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Row1 { get; set; }

        [Category("3.2 Crop and Resize image")]
        [Description("Columna 1 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Column1 { get; set; }

        [Category("3.2 Crop and Resize image")]
        [Description("Fila 2 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Row2 { get; set; }

        [Category("3.2 Crop and Resize image")]
        [Description("Columna 2 para un área específica de búsqueda (0 significa búsqueda por toda la imagen)")]
        [DefaultValue(0)]
        public int Column2 { get; set; }

        [Category("3.2 Crop and Resize image")]
        [Description("Factor de redimensión para una imagen con menos resolución (1 significa sin reducción de resolución)")]
        [DefaultValue(1)]
        public double ResizeFactor { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            if (!string.IsNullOrEmpty(DataInput))
                SerialNumber = GetVariable<string>(DataInput);

            var dictImageNameFunction = new Dictionary<string, Action>();
            if (!string.IsNullOrEmpty(Image1Name))
                dictImageNameFunction.Add(Image1Name, () => Image1Action?.Execute());
            if (!string.IsNullOrEmpty(Image2Name))
                dictImageNameFunction.Add(Image2Name, () => Image2Action?.Execute());
            if (!string.IsNullOrEmpty(Image3Name))
                dictImageNameFunction.Add(Image3Name, () => Image3Action?.Execute());
            if (!string.IsNullOrEmpty(Image4Name))
                dictImageNameFunction.Add(Image4Name, () => Image4Action?.Execute());
            if (!string.IsNullOrEmpty(Image5Name))
                dictImageNameFunction.Add(Image5Name, () => Image5Action?.Execute());
            if (!string.IsNullOrEmpty(Image6Name))
                dictImageNameFunction.Add(Image6Name, () => Image6Action?.Execute());
            if (!string.IsNullOrEmpty(Image7Name))
                dictImageNameFunction.Add(Image7Name, () => Image7Action?.Execute());
            if (!string.IsNullOrEmpty(Image8Name))
                dictImageNameFunction.Add(Image8Name, () => Image8Action?.Execute());
            if (!string.IsNullOrEmpty(Image9Name))
                dictImageNameFunction.Add(Image9Name, () => Image9Action?.Execute());
            if (!string.IsNullOrEmpty(Image10Name))
                dictImageNameFunction.Add(Image10Name, () => Image10Action?.Execute());

            testBase.TestHalconCaptureAsync(Type, SerialNumber, TestName, dictImageNameFunction, DeviceName, Row1, Column1, Row1, Column1, ResizeFactor);
        }
    }

    public class HalconCaptureMultiImageAsyncDescription : ActionDescription
    {
        public override string Name { get { return "HalconCaptureMultiImageAsync"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override string Description { get { return "Este action sirve para hacer diferentes imágenes de un mismo equipo desde la misma secuencia con solo un action asíncronamente, eso quiere decir que se hace aparte del test"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
