﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using WinTestPlayer.Services;
using Telerik.WinControls.UI;
using Dezac.Tests;
using Dezac.Tests.Actions;
using System.Text.RegularExpressions;
using TaskRunner.Model;
using TaskRunner.Enumerators;

namespace WinTestPlayer.Views
{
    public partial class SequenceView : UserControl
    {
        private Dictionary<Step, CustomTreeNode> stepNodes;
        private Dictionary<RadTreeNode, StepResult> stepErrors;

        protected AppService app;
        private static int numInstances;
        private SynchronizationContext sc;
        private bool multipleInstances;

        public SequenceView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            sc = SynchronizationContext.Current;

            stepErrors = new Dictionary<RadTreeNode, StepResult>();

            app = DI.Resolve<AppService>();
            app.SequenceLoaded += (s, ee) => { sc.Post(_ => { OnSequenceLoaded(); }, null); };
            app.Runner.AllTestStarting += (s, runnerTestsInfo) => { sc.Post(_ => { OnAllTestStarting(runnerTestsInfo); }, null); };
         
            if (app.Model.ModoPlayer == Model.ModoPlayer.SAT || app.Model.ModoPlayer == Model.ModoPlayer.PostVenta)
                treeView.CheckBoxes = true;
            else
                treeView.CheckBoxes = false;

            TaskRunner.SequenceRunner runner = app.Runner;
            runner.StepStart += (s, stepResult) => { sc.Post(_ => { OnStepStart(stepResult); }, null); };
            runner.StepEnd += (s, stepResult) => { sc.Post(_ => { OnStepEnd(stepResult); }, null); };

            UpdateUI();
        }

        private void OnSequenceLoaded() => UpdateUI();
        private void OnAllTestStarting(RunnerTestsInfo runnerTestsInfo)
        {
            multipleInstances = runnerTestsInfo.NumInstances > 1;
            numInstances = runnerTestsInfo.NumInstances;
            treeView.Focus();
            stepErrors.Clear();
            ClearTime();
        }
        private void OnStepStart(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                node.EnsureVisible();
                node.BackColor = Color.LightSteelBlue;

                if (stepResult.ExecutionResult == StepExecutionResult.Pending)
                {
                    node.AddImage(stepResult.NumInstance, Properties.Resources._712);
                    WriteNodeInformation(stepResult);
                }
            }
        }
        private void OnStepEnd(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                var step = node.Tag as Step;
                switch (stepResult.ExecutionResult)
                {
                    case StepExecutionResult.Failed:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.ko_16);
                        break;
                    case StepExecutionResult.Aborted:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.aborted_16);
                        break;
                    case StepExecutionResult.Continued:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.next_16);
                        break;
                    case StepExecutionResult.Repeated:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.retry_16);
                        break;
                    case StepExecutionResult.Omitted:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.omitted_16);
                        break;
                    case StepExecutionResult.Disabled:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.omitted_16);
                        break;
                    case StepExecutionResult.Passed:
                        node.AddImage(stepResult.NumInstance, Properties.Resources.ok_16);
                        break;
                }
                node.BackColor = Color.Empty;
                WriteNodeInformation(stepResult);
                if (stepResult.Exception != null && !stepErrors.ContainsKey(node))
                    stepErrors.Add(node, stepResult);
            }
        }
        private void UpdateUI()
        {
            treeView.Nodes.Clear();

            if (app.Sequence == null)
                return;

            stepNodes = new Dictionary<Step, CustomTreeNode>();

            AddChildNodes(treeView.Nodes, app.Sequence.Root);

            treeView.ExpandAll();

            ValidateSequence();

            stepNodes.Values.ToList().ForEach(node =>
            {
                if (!node.Checked)
                    node.Collapse(true);
            });
        }
        private void ClearTime()
        {
            stepNodes.Values.ToList().ForEach(node =>
            {
                var step = node.Tag as Step;
                node.BackColor = Color.Empty;
                node.ClearImages();

                if (node.Parent != null && node.Text != step.Name)
                    if (node.TreeView != null && !node.TreeView.IsDisposed)
                        node.Text = ((Step)node.Tag).Name;
            });
        }
        private void AddNodes(CustomTreeNode node)
        {
            var step = node.Tag as Step;
            if (step == null)
                return;

            node.Checked = step.Enabled;

            stepNodes.Add(step, node);

            AddChildNodes(node.Nodes, step);
        }
        private void AddChildNodes(RadTreeNodeCollection node, Step step)
        {
            foreach (Step child in step.Steps)
            {
                string nodeName = child.Enabled == true ? child.Name : child.Name + " (Deshabilitada)";
                Color color = child.Enabled == true ? Color.Black : Color.Silver;

                var n = new CustomTreeNode(nodeName);
                n.ToolTipText = nodeName;
                n.Tag = child;
                n.ForeColor = color;

                node.Add(n);
                AddNodes(n);
            }
        }

        private void WriteNodeInformation(StepResult stepResult)
        {
            if (stepNodes.TryGetValue(stepResult.Step, out CustomTreeNode node))
            {
                var step = node.Tag as Step;
                double duration = stepResult.Duration.TotalSeconds;
                string durationText = "";
                if (duration >= 0)
                    durationText = $"({duration:N2}s)";

                if (multipleInstances)
                {
                    if (Regex.IsMatch(node.Text, $"#{stepResult.NumInstance}"))
                    {
                        if (Int32.TryParse(stepResult.Step.LoopMaxIterations, out int maxInterations) && maxInterations > 1)
                            node.Text = Regex.Replace(node.Text, $@"#{stepResult.NumInstance}(.*?)\|", $"#{stepResult.NumInstance} ({stepResult.LoopIndex + 1} de {stepResult.Step.LoopMaxIterations}) {durationText} |");
                        else if (stepResult.LoopIndex > 0)
                            node.Text = Regex.Replace(node.Text, $@"#{stepResult.NumInstance}(.*?)\|", $"#{stepResult.NumInstance} (Retry: {stepResult.LoopIndex + 1}) {durationText} |");
                        else
                            node.Text = Regex.Replace(node.Text, $@"#{stepResult.NumInstance}(.*?)\|", $"#{stepResult.NumInstance} {durationText} |");
                    }
                    else
                    {
                        if (Int32.TryParse(stepResult.Step.LoopMaxIterations, out int maxInterations) && maxInterations > 1)
                            node.Text += $" #{stepResult.NumInstance} ({stepResult.LoopIndex + 1} de {stepResult.Step.LoopMaxIterations}) {durationText} |";
                        else if (stepResult.LoopIndex > 0)
                            node.Text += $" #{stepResult.NumInstance} (Retry: {stepResult.LoopIndex + 1}) {durationText} |";
                        else
                            node.Text += $" #{stepResult.NumInstance} {durationText} |";
                    }
                }
                else
                {
                    if (Int32.TryParse(stepResult.Step.LoopMaxIterations, out int maxInterations) && maxInterations > 1)
                        node.Text = $"{stepResult.Step.Name} ({stepResult.LoopIndex + 1} de {stepResult.Step.LoopMaxIterations}) {durationText}";
                    else if (stepResult.LoopIndex > 1)
                        node.Text = $"{stepResult.Step.Name} (Retry: {stepResult.LoopIndex + 1}) {durationText}";
                    else
                        node.Text = $"{stepResult.Step.Name} {durationText}";
                }
            }
        }
        private void treeView_NodeMouseDoubleClick(object sender, RadTreeViewEventArgs e)
        {

            if (stepErrors.TryGetValue(e.Node, out StepResult result))
                using (var form = new LogItemDetails())
                {
                    form.LogText = result.GetErrorMessage();
                    form.ShowDialog();
                }
        }
        private void ValidateSequence()
        {
            try
            {
                var actions = stepNodes.Keys
                    .Where(p => p.Action is RunMethodAction)
                    .Select(p => new { Step = p, Action = (RunMethodAction)p.Action });

                IEnumerable<Type> types = actions
                    .Select(p => p.Action.GetMethodType())
                    .Distinct();

                string msg = string.Empty;

                foreach (Type type in types)
                {
                    var methods = type
                        .GetMethods()
                        .Where(m => m.GetCustomAttributes(typeof(TestPointAttribute), false).Length > 0)
                        .ToList();

                    foreach (System.Reflection.MethodInfo m in methods)
                    {
                        var attr = m.GetCustomAttributes(typeof(TestPointAttribute), false)[0] as TestPointAttribute;

                        if (attr.Required)
                        {
                            string name = string.Format("{0}/{1}", m.ReflectedType.FullName, m.Name);
                            var step = actions.Where(p => string.Format("{0}/{1}", p.Action.TypeName, p.Action.Method) == name)
                                .FirstOrDefault();

                            if (step == null || !step.Step.Enabled)
                                msg += string.Format("La secuencia no está verificando el test point {0} requerido!{1}",
                                    m.Name, Environment.NewLine);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(msg))
                    DI.Resolve<ShellService>().MsgBox(msg, "ATENCIÓN");
            }
            catch { }
        }

        public class CustomTreeNode : RadTreeNode
        {
            public CustomTreeNode()
            {
            }
            public CustomTreeNode(string name)
                : base(name)
            {
            }

            public override Image Image
            {
                get
                {
                    if (images.Count == 0)
                        return base.Image;
                    return UnionImage;
                }
                set
                {
                    images.Clear();
                    base.Image = value;
                }
            }
            private Image UnionImage;
            private Dictionary<int, Image> images = new Dictionary<int, Image>();
            public void AddImage(int instance, Image image)
            {
                images[instance] = image;
                UnionImage = GetImageForList(images);
            }
            public void ClearImages()
            {
                images.Clear();
            }
            private Image GetImageForList(Dictionary<int, Image> images)
            {
                if (images.Count == 0 || numInstances == 0)
                    return null;
                if (numInstances == 1)
                    return images.FirstOrDefault().Value;

                var bitmap = new Bitmap(16 * numInstances, 16);
                var g = Graphics.FromImage(bitmap);
                for (int i = 0; i <= numInstances; i++)
                    if (images.ContainsKey(i + 1))
                        g.DrawImage(images[i + 1], 16 * i, 0, 16, 16);
                    else
                        g.DrawImage(Properties.Resources.pending_16, 16 * i, 0, 16, 16);
                return bitmap;
            }
        }

        private void treeView_NodeCheckedChanged(object sender, TreeNodeCheckedEventArgs e)
        {
            var step = e.Node.Tag as Step;
            if (step != null)
            {
                step.Enabled = e.Node.Checked;
                e.Node.ForeColor = e.Node.Checked ? Color.Black : Color.Gray;
            }
        }
    }
}
