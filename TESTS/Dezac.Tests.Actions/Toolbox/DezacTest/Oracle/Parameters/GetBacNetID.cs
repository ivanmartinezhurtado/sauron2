﻿using Dezac.Services;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using System.ComponentModel;
using System.Drawing;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(GetBacNetIDDescription))]

    public class GetBacNetID : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número de BacNetID que se quiere poner al equipo")]
        public ActionVariable CounterName { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if (CounterName == null || CounterName.IsKeyNullOrEmpty)
                CounterName = "BACNET";

            var context = SequenceContext.Current;
            var data = context.Services.Get<ITestContext>();

            using (DezacService svc = new DezacService())
            {
                var bacNetID = svc.GetBacNetID(CounterName.ToString());

                Assert.IsNotNull(bacNetID, Error().SOFTWARE.BBDD.CARGA_SERVICIO("No se han obtenido ID Bacnet de la BBDD"));

                SequenceContext.Current.Variables.AddOrUpdate(ConstantsParameters.TestInfo.BACNET_ID, bacNetID);

                if (data != null && data.TestInfo != null)
                    data.TestInfo.BacNetID = bacNetID;
            }
        }
    }

    public class GetBacNetIDDescription : ActionDescription
    {
        public override string Name { get { return "GetBacNetID"; } }
        public override string Category { get { return "DezacTest/Oracle/Parameters"; } }
        public override string Description { get { return "Action que sirve para poner un BacNetID al equipo al que se está realizando la secuencia"; } }
        public override Image Icon { get { return Properties.Resources.database_process_icon; } }
    }
}
