﻿using Dezac.Tests.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace Dezac.Tests.Utils
{
    public static class Extensions
    {
        public static string Left(this string text, int length)
        {
            if (string.IsNullOrEmpty(text) || text.Length < length)
                return text;

            return text.Substring(0, length);
        }

        public static string GetInnerMessage(this Exception ex)
        {
            if (ex == null)
                return null;

            while (ex.InnerException != null)
                ex = ex.InnerException;

            return ex.Message;
        }

        public static List<string> GetAllInnerMessages(this Exception ex)
        {
            if (ex == null)
                return null;

            List<string> messages = new List<string>();
            messages.Add(ex.Message);

            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                messages.Add(ex.Message);
            }

            return messages;
        }

        public static string GetInnerMessage(this AggregateException AgregatEx)
        {
            Exception ex = new Exception();
            if (AgregatEx == null)
                return null;

            ex = AgregatEx.InnerExceptions.LastOrDefault();

            while (ex.InnerException != null)
                ex = ex.InnerException;

            return ex.Message;
        }

        public static void AddToResults(this TriAdjustValueDef defs, ParamValueCollection list)
        {
            list.Set(defs.L1.Name, defs.L1.Value, defs.L1.Unidad, defs.L1.Min, defs.L1.Max);
            list.Set(defs.L2.Name, defs.L2.Value, defs.L2.Unidad, defs.L2.Min, defs.L2.Max);
            list.Set(defs.L3.Name, defs.L3.Value, defs.L3.Unidad, defs.L3.Min, defs.L3.Max);

            if (defs.Neutro != null)
                list.Set(defs.Neutro.Name, defs.Neutro.Value, defs.Neutro.Unidad, defs.Neutro.Min, defs.Neutro.Max);
        }

        public static void AddToResults(this List<AdjustValueDef> defs, ParamValueCollection list)
        {
            defs.ForEach((p) => p.AddToResults(list));
        }

        public static void AddToResults(this AdjustValueDef defs, ParamValueCollection list)
        {
            list.Set(defs.Name, defs.Value, defs.Unidad, defs.Min, defs.Max);
        }

        public static List<T> Add<T>(this List<T> list, params T[] values)
        {
            list.AddRange(values);
            return list;
        }

        public static int GetWeekNumber(this TestBase testBase)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        public static List<TriAdjustValueDef> Add(this List<TriAdjustValueDef> list, string nameL1, double min, double max, double average, double tolerance, ParamUnidad unidad = ParamUnidad.Puntos, bool hasNeutro = false, bool hasNeutroCalc = false, bool hasFugas = false)
        {
            list.Add(new TriAdjustValueDef(nameL1, min, max, average, tolerance, unidad, hasNeutro, hasNeutroCalc, hasFugas));

            return list;
        }

        public static List<AdjustValueDef> Add(this List<AdjustValueDef> list, string nameL1, double min, double max, double average, double tolerance, ParamUnidad unidad = ParamUnidad.Puntos)
        {
            list.Add(new AdjustValueDef(nameL1, 0,  min, max, average, tolerance, unidad));

            return list;
        }
    }
}
