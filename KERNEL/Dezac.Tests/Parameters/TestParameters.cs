﻿using Dezac.Tests.Model;

namespace Dezac.Tests.Parameters
{
    public class TestParameters
    {
        private FasesParameters tpp;

        public FasesParameters PARAMETER(string value,  ParamUnidad unidad)
        {
            tpp.Variable = value;
            tpp.Unidad = unidad;
            return tpp;
        }

        public FasesParameters KW { get { tpp.Variable = ConstantsParameters.Electrics.KW; tpp.Unidad= ParamUnidad.Kw; return tpp; } }

        public FasesParameters KVAR { get { tpp.Variable = ConstantsParameters.Electrics.KVAR; tpp.Unidad = ParamUnidad.Kvar; return tpp; } }

        public FasesParameters KVA { get { tpp.Variable = ConstantsParameters.Electrics.KVA; tpp.Unidad = ParamUnidad.kVA; return tpp; } }

        public FasesParameters VA { get { tpp.Variable = ConstantsParameters.Electrics.VA; tpp.Unidad = ParamUnidad.VA; return tpp; } }

        public FasesParameters W { get { tpp.Variable = ConstantsParameters.Electrics.W; tpp.Unidad = ParamUnidad.W; return tpp; } }

        public FasesParameters VAR { get { tpp.Variable = ConstantsParameters.Electrics.VAR; tpp.Unidad = ParamUnidad.Var; return tpp; } }

        public FasesParameters V { get { tpp.Variable = ConstantsParameters.Electrics.V; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters VRMS { get { tpp.Variable = ConstantsParameters.Electrics.VRMS; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters VF { get { tpp.Variable = ConstantsParameters.Electrics.VF; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters V_DC { get { tpp.Variable = ConstantsParameters.Electrics.V_DC; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters V_AC { get { tpp.Variable = ConstantsParameters.Electrics.V_AC; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters V_LINE { get { tpp.Variable = ConstantsParameters.Electrics.V_LINE; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters I { get { tpp.Variable = ConstantsParameters.Electrics.I; tpp.Unidad = ParamUnidad.A; return tpp; } }

        public FasesParameters I_AC { get { tpp.Variable = ConstantsParameters.Electrics.I_AC; tpp.Unidad = ParamUnidad.A; return tpp; } }

        public FasesParameters I_DC { get { tpp.Variable = ConstantsParameters.Electrics.I_DC; tpp.Unidad = ParamUnidad.A; return tpp; } }

        public FasesParameters I_N { get { tpp.Variable = ConstantsParameters.Electrics.I_N; tpp.Unidad = ParamUnidad.A; return tpp; } }

        public FasesParameters I_LK { get { tpp.Variable = ConstantsParameters.Electrics.I_LK; tpp.Unidad = ParamUnidad.A; return tpp; } }

        public FasesParameters FREQ { get { tpp.Variable = ConstantsParameters.Electrics.FREQ; tpp.Unidad = ParamUnidad.Hz; return tpp; } }

        public FasesParameters TEMP { get { tpp.Variable = ConstantsParameters.Electrics.TEMP; tpp.Unidad = ParamUnidad.Grados; return tpp; } }

        public FasesParameters TIME { get { tpp.Variable = ConstantsParameters.Electrics.TIME; tpp.Unidad = ParamUnidad.s;  return tpp; } }

        public FasesParameters THD_V { get { tpp.Variable = ConstantsParameters.Electrics.THD_V; tpp.Unidad = ParamUnidad.s; return tpp; } }

        public FasesParameters THD_I { get { tpp.Variable = ConstantsParameters.Electrics.THD_I; tpp.Unidad = ParamUnidad.s; return tpp; } }

        public FasesParameters CUADRATICPOINTS { get { tpp.Variable = ConstantsParameters.Electrics.CUADRATICPOINTS; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters POINTS { get { tpp.Variable = ConstantsParameters.Electrics.POINTS; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters RES { get { tpp.Variable = ConstantsParameters.Electrics.RES; tpp.Unidad = ParamUnidad.Ohms; return tpp; } }

        public FasesParameters RES_FUGA { get { tpp.Variable = ConstantsParameters.Electrics.RES_FUGA; tpp.Unidad = ParamUnidad.Ohms; return tpp; } }

        public FasesParameters RES_RCC { get { tpp.Variable = ConstantsParameters.Electrics.RES_RCC; tpp.Unidad = ParamUnidad.Ohms; return tpp; } }

        public FasesParameters RES_RDO { get { tpp.Variable = ConstantsParameters.Electrics.RES_RDO; tpp.Unidad = ParamUnidad.Ohms; return tpp; } }

        public FasesParameters GAIN_DESFASE { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_DESFASE; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_FACTOR { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_FACTOR; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_V { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_V; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_I { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_I; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_I_N { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_I_N; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_I_LK { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_I_LK; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_FREC { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_FREC; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_KW { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_KW; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_KVAR { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_KVAR; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_OFFSET { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_OFFSET; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_OFFSET_RDO { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_OFFSET_RDO; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_FACTOR_RDO { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_FACTOR_RDO; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_OFFSET_RCC { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_OFFSET_RCC; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters GAIN_FACTOR_RCC { get { tpp.Variable = ConstantsParameters.FactorGains.GAIN_FACTOR_RCC; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters SYNC { get { tpp.Variable = ConstantsParameters.Electrics.SYNC; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters PF { get { tpp.Variable = ConstantsParameters.Electrics.PF; tpp.Unidad = ParamUnidad.SinUnidad; return tpp; } }

        public FasesParameters COS_PHI { get { tpp.Variable = ConstantsParameters.Electrics.COS_PHI; tpp.Unidad = ParamUnidad.SinUnidad; return tpp; } }

        public FasesParameters ANGLE_PF { get { tpp.Variable = ConstantsParameters.Electrics.ANGLE_PF; tpp.Unidad = ParamUnidad.Grados; return tpp; } }

        public FasesParameters ANGLE_GAP { get { tpp.Variable = ConstantsParameters.Electrics.ANGLE_GAP; tpp.Unidad = ParamUnidad.Grados; return tpp; } }

        public FasesParameters MEMORY_RAM { get { tpp.Variable = ConstantsParameters.DeviceParameters.MEMORY_RAM; tpp.Unidad = ParamUnidad.MByte; return tpp; } }

        public FasesParameters MEMORY_INTERNAL { get { tpp.Variable = ConstantsParameters.DeviceParameters.MEMORY_INTERNAL; tpp.Unidad = ParamUnidad.MByte; return tpp; } }

        public FasesParameters NWID { get { tpp.Variable = ConstantsParameters.Electrics.NWID; tpp.Unidad = ParamUnidad.s; return tpp; } }

        public FasesParameters PWID { get { tpp.Variable = ConstantsParameters.Electrics.PWID; tpp.Unidad = ParamUnidad.s; return tpp; } }

        public FasesParameters PER { get { tpp.Variable = ConstantsParameters.Electrics.PER; tpp.Unidad = ParamUnidad.s; return tpp; } }

        public FasesParameters VPEAK_MAX { get { tpp.Variable = ConstantsParameters.Electrics.VPEAK_MAX; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters VPEAK_MIN { get { tpp.Variable = ConstantsParameters.Electrics.VPEAK_MIN; tpp.Unidad = ParamUnidad.V; return tpp; } }

        public FasesParameters TINT { get { tpp.Variable = ConstantsParameters.Electrics.TINT; tpp.Unidad = ParamUnidad.s; return tpp; } }

        public FasesParameters PHASE { get { tpp.Variable = ConstantsParameters.Electrics.PHASE; tpp.Unidad = ParamUnidad.Grados; return tpp; } }

        public FasesParameters DCYCLE { get { tpp.Variable = ConstantsParameters.Electrics.DCYCLE; tpp.Unidad = ParamUnidad.PorCentage; return tpp; } }

        public FasesParameters PPM { get { tpp.Variable = ConstantsParameters.Electrics.PPM; tpp.Unidad = ParamUnidad.PorCentage; return tpp; } }

        public FasesParameters LUX { get { tpp.Variable = ConstantsParameters.Electrics.LUX; tpp.Unidad = ParamUnidad.LUX; return tpp; } }

        public FasesParameters POWER_SIGNAL { get { tpp.Variable = ConstantsParameters.Electrics.POWER_SIGNAL; tpp.Unidad = ParamUnidad.dBm; return tpp; } }

        public FasesParameters SNR { get { tpp.Variable = ConstantsParameters.Electrics.SNR; tpp.Unidad = ParamUnidad.dBm; return tpp; } }

        public FasesParameters WEIGHT_LED { get { tpp.Variable = ConstantsParameters.Electrics.WEIGHT_LED; tpp.Unidad = ParamUnidad.Whimp; return tpp; } }

        public FasesParameters SUM_V { get { tpp.Variable = ConstantsParameters.FactorGains.SUM_V; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters SUM_I { get { tpp.Variable = ConstantsParameters.FactorGains.SUM_I; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters SUM_KW { get { tpp.Variable = ConstantsParameters.FactorGains.SUM_KW; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters SUM_KVAR { get { tpp.Variable = ConstantsParameters.FactorGains.SUM_KVAR; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters SUM_IN { get { tpp.Variable = ConstantsParameters.FactorGains.SUM_IN; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters SUM_ILK { get { tpp.Variable = ConstantsParameters.FactorGains.SUM_ILK; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters OFFSET_V { get { tpp.Variable = ConstantsParameters.Electrics.OFFSET_V; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters OFFSET_I { get { tpp.Variable = ConstantsParameters.Electrics.OFFSET_I; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters NUM_MUESTRAS { get { tpp.Variable = ConstantsParameters.Electrics.NUM_MUESTRAS; tpp.Unidad = ParamUnidad.Numero; return tpp; } }

        public FasesParameters CURRENT_FACTOR { get { tpp.Variable = ConstantsParameters.Electrics.CURRENT_FACTOR; tpp.Unidad = ParamUnidad.Puntos; return tpp; } }

        public FasesParameters VALOR { get { tpp.Variable = ConstantsParameters.DeviceParameters.VALOR; tpp.Unidad = ParamUnidad.SinUnidad; return tpp; } }

        public TestParameters(ParamValueCollection collection)
        {
            tpp = new FasesParameters(collection);
        }
    }
}
