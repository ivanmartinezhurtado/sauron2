﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.02)]
    public class CITILUX : DeviceBase
    {
        #region Instanciación del equipo

        public CITILUX()
        {
        }

        public CITILUX(int port)
        {
            SetPort(port, 255, 38400, System.IO.Ports.StopBits.One);
        }
 
        public void SetPort(int port, byte periferico = 16, int baudRate = 19200, System.IO.Ports.StopBits bitStop = System.IO.Ports.StopBits.Two)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, bitStop, _logger);
            else
            {
                Modbus.PortCom = port;
                Modbus.BaudRate = baudRate;
            }

            Modbus.PerifericNumber = periferico;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        #endregion

        #region Register Operations

        public void Reset()
        {
            LogMethod();
            Modbus.WithTimeOut((m) => { m.WriteSingleCoil((ushort)Registers.Setup.RESET, true); });
        }

        public void FlagTest(bool test = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.Setup.FLAG_TEST, test);
            Thread.Sleep(1000);
        }

        public void EnabledEmbeddedComunication()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.EMBEDDED_COM, false);
            Thread.Sleep(1000);
        }

        public void DisabledEmbeddedComunication()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.EMBEDDED_COM, true);
            Thread.Sleep(1000);
        }

        public AllVariables ReadAllVariables()
        {
            LogMethod();

            return Modbus.Read<AllVariables>();
        }

        public PhaseVariables ReadPhaseVariables()
        {
            LogMethod();

            return Modbus.Read<PhaseVariables>();
        }
    
        public ThreePhaseVariables ReadThreePhaseVariables()
        {
            LogMethod();

            return Modbus.Read<ThreePhaseVariables>();
        }

        public TotalHarmonicDistorsion ReadTotalHarmonicDistorsion()
        {
            LogMethod();

            return Modbus.Read<TotalHarmonicDistorsion>();
        }
 
        public ContinousVariables ReadContinousVariables()
        {
            LogMethod();

            return Modbus.Read<ContinousVariables>();
        }

        public MeasureGains ReadMeasureGains()
        {
            LogMethod();

            return Modbus.Read<MeasureGains>();
        }

        public TriUShort ReadVoltageGains()
        {
            LogMethod();

            return Modbus.Read<TriUShort>((ushort)Registers.Gains.VOLTAGE);
        }

        public TriUShort ReadCurrentGains()
        {
            LogMethod();

            return Modbus.Read<TriUShort>((ushort)Registers.Gains.CURRENT);
        }

        public CompoundGains ReadCompountGains()
        {
            LogMethod();

            return Modbus.Read<CompoundGains>((ushort)Registers.Gains.COMPOUND);
        }

        public TriUShort ReadPowerGains()
        {
            LogMethod();

            return Modbus.Read<TriUShort>((ushort)Registers.Gains.KW);
        }

        public ushort ReadDC20Gains()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.Gains.DC_20);
        }

        public ushort ReadDC200Gains()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.Gains.DC_200);
        }

        public TriUShort ReadOffSetGains()
        {
            LogMethod();

            return Modbus.Read<TriUShort>((ushort)Registers.Gains.PHASE_OFFSET);
        }

        public string ReadMAC()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.Setup.MAC, 9);
        }

        public double ReadTemperature()
        {
            LogMethod();

            return (double)(Modbus.ReadRegister((ushort)Registers.Inputs.TEMPERATURE) / 10);
        }

        public MACIPEmbedded ReadMACIPEmbedded()
        {
            LogMethod();

            var deviceInfo2 = Modbus.ReadBytes((ushort)Registers.Setup.MAC_IP_EMBEDDED,8);
            MACIPEmbedded output = new MACIPEmbedded();

            for (byte i = 0; i < 6; i++)
                output.MAC += deviceInfo2[i].ToString("x2") + ":";

            for (byte i = 6; i < 10; i++)
                output.IP += deviceInfo2[i].ToString() + ".";

            output.VersionEmbedded = ASCIIEncoding.ASCII.GetString(deviceInfo2).Substring(10,4);
                       
            output.MAC = output.MAC.Substring(0, output.MAC.Length - 1).ToUpper();
            output.IP = output.IP.Substring(0, output.IP.Length - 1);
            return output;
        }

        public string ReadEmbeddedVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.Setup.EMBEDDED_VERSION, 2);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.Setup.SOFTWARE_VERSION, 4);
        }

        public int ReadBastidor()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.Setup.BASTIDOR);
        }

        public string ReadSerialNumber()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.Setup.SERIAL_NUMBER, 5);
        }

        public string ReadProductNumber()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.Setup.PRODUCT_NUMBER, 6);
        }

        public InputValues ReadDigitalInputs()
        {
            LogMethod();

            return (InputValues)Modbus.ReadRegister((ushort)Registers.Inputs.DIGITAL_INPUTS);
        }

        public KeyboardKeys ReadKeyboard()
        {
            LogMethod();

            return (KeyboardKeys)Modbus.ReadRegister((ushort)Registers.Inputs.KEYBOARD);
        }

        public DateTime ReadDateTime()
        {
            LogMethod();

            var deviceDateTime = Modbus.Read<CLUXDateTime>();

            DateTime dt = new DateTime(deviceDateTime.Year, deviceDateTime.Month, deviceDateTime.Day,
                deviceDateTime.Hour, deviceDateTime.Minutes, deviceDateTime.Seconds);

            return dt;
        }

        public ushort ReadHardwareVector()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.Setup.HARDWARE_VECTOR);
        }

        public string ReadInitialMessage()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.Setup.INITIAL_MESSAGE, 10);
        }

        public TransformRatio ReadTransformationRatio()
        {
            LogMethod();

            return Modbus.Read<TransformRatio>();
        }

        public int ReadErrorCode()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.Setup.ERROR_CODE);
        }

        public void WriteLeds(LedsEnum led, bool state)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)led, state);
        }

        public enum LedsEnum
        {
           Led_COM = CITILUX.Registers.Leds.LED_COM,
           Led_CPU = CITILUX.Registers.Leds.LED_CPU,
           Led_1 = CITILUX.Registers.Leds.LED_1,
           Led_2 = CITILUX.Registers.Leds.LED_2,
           Led_3 = CITILUX.Registers.Leds.LED_3,
        }
   
        public void WriteMAC(String addressMAC)
        {
            LogMethod();

            if (addressMAC.Substring(0,1) == "\0")
                addressMAC = addressMAC.Substring(1, addressMAC.Length);
            
            addressMAC = addressMAC.PadRight(18, '\0');
            Modbus.WriteString((ushort)Registers.Setup.MAC, addressMAC);
        }

        public void WriteARMSoftwareVersion(string ARMSoftwareVersion)
        {
            LogMethod();

            Modbus.WriteString((ushort)Registers.Setup.ARM_SOFTWARE_VERSION, ARMSoftwareVersion);
        }

        public void WriteSynchronism(int Sincronismo)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.Gains.FREQUENCY, (ushort)Sincronismo);
        }

        public void WriteMeasureGains(ushort voltage, ushort compound, ushort current, ushort power, ushort DC20, ushort DC200)
        {
            LogMethod();

            var Voltage = new CITILUX.TriUShort() { L1 = voltage, L2 = voltage, L3 = voltage };
            var Compound = new CITILUX.CompoundGains() { L12 = compound, L23 = compound, L31 = compound };
            var Current = new CITILUX.TriUShort() { L1 = current, L2 = current, L3 = current };
            var Power = new CITILUX.TriUShort() { L1 = power, L2 = power, L3 = power };

            WriteMeasureGains(Voltage, Compound, Current, Power, DC20, DC200);
        }

        public void WriteMeasureGains(CITILUX.TriUShort voltage, CITILUX.CompoundGains compuond, CITILUX.TriUShort current, CITILUX.TriUShort power)
        {
            LogMethod();

            CITILUX.MeasureGains gains = new CITILUX.MeasureGains()
            {

                Voltage = voltage,
                Compound = compuond,
                Current = current,
                Power = power,
                DC20 = ReadDC20Gains(),
                DC200 = ReadDC200Gains(),
            };

            WriteMeasureGains(gains);
        }

        public void WriteMeasureGains(CITILUX.TriUShort voltage, CITILUX.CompoundGains compuond, CITILUX.TriUShort current, CITILUX.TriUShort power, ushort dc20, ushort dc200)
        {
            LogMethod();

            CITILUX.MeasureGains gains = new CITILUX.MeasureGains()
            {

                Voltage = voltage,
                Compound = compuond,
                Current = current,
                Power = power,
                DC20 = dc20,
                DC200 = dc200,
            };

            WriteMeasureGains(gains);
        }

        public void WriteMeasureGains(MeasureGains gains)
        {
            LogMethod();

            Modbus.Write<MeasureGains>(gains);
        }

        public void WriteCurrentGains(TriUShort gains)
        {
            LogMethod();

            Modbus.Write<TriUShort>((ushort)Registers.Gains.CURRENT, gains);
        }
        public void WritePhaseOffsetGains()
        {
            LogMethod();

            CITILUX.TriUShort offsetGains = new CITILUX.TriUShort()
            {
                L1 = 1000,
                L2 = 1000,
                L3 = 1000
            };
            WritePhaseOffsetGains(offsetGains);
        }

        public void WritePhaseOffsetGains(ushort L1, ushort L2, ushort L3)
        {
            LogMethod();

            CITILUX.TriUShort offsetGains = new CITILUX.TriUShort()
            {
                L1 = L1,
                L2 = L2,
                L3 = L3
            };

            WritePhaseOffsetGains(offsetGains);
        }

        public void WritePhaseOffsetGains(TriUShort phaseOffsetGain)
        {
            LogMethod();

            Modbus.Write<TriUShort>((ushort)Registers.Gains.PHASE_OFFSET, phaseOffsetGain);
        }

        public void WriteDC20Gain(ushort dc20Gain)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.Gains.DC_20, dc20Gain);
        }

        public void WriteDC200Gain(ushort dc200Gain)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.Gains.DC_200, dc200Gain);
        }

        public void WriteSerialNumber(string serialNumber)
        {
            LogMethod();

            Modbus.WriteString((ushort)Registers.Setup.SERIAL_NUMBER, serialNumber);
        }

        public void WriteBastidor(string bastidor)
        {
            LogMethod();

            var value = bastidor.PadRight(6, '\0');
            Modbus.WriteString((ushort)Registers.Setup.BASTIDOR, bastidor);
        }

        public void WriteProductNumber(string productNumber)
        {
            LogMethod();

            Modbus.WriteString((ushort)Registers.Setup.PRODUCT_NUMBER, productNumber);
        }

        public void WriteErrorCode(int errorCode)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.Setup.ERROR_CODE, errorCode);
        }

        public void WriteDateTime(DateTime dateTime)
        {
            LogMethod();

            Modbus.Write<CLUXDateTime>(new CLUXDateTime(dateTime));
        }

        public void WriteRSMode(rsModeEnum rsMode)
        {
            LogMethod();

            bool value = rsMode == rsModeEnum.Rs232 ? true : false;
            Modbus.WriteSingleCoil((ushort)Registers.Setup.RS232_RS485, value);
        }

        public enum rsModeEnum
        {
            Rs232,
            Rs485
        }

        public void WriteFastRamTest()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.TEST_RAM_FAST, true);
        }

        public void WriteBatteryRamTest()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.TEST_RAM_BATTERY, true);
        }

        public void WriteInitialMessage(String message)
        {
            LogMethod();

            message = message.PadRight(20,' ');
            Modbus.WriteString((ushort)Registers.Setup.INITIAL_MESSAGE, message);
        }

        public void WriteHardwareVector(ushort hardwareVector)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.Setup.HARDWARE_VECTOR, hardwareVector);
        }

        public void DeactivateTestModeTimer()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.TIMER_TEST_EMBEDDED, false);
        }

        public void ActivateRTSmodeRS485()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.ACTIVATE_RTS_RS485, true);
        }

        public void ClearEnergy()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.Setup.CLEAR_CURRENT_ENERGY, true);
        }

        public void WriteTransformRatio()
        {
            LogMethod();

            CITILUX.TransformRatio transformRatio = new CITILUX.TransformRatio { CurrentPrimary = 1, VoltagePrimary = 1, VoltageSecondary = 1 };
            Modbus.Write<TransformRatio>(transformRatio);
        }

        public void WriteTransformRatio(TransformRatio transformRatio)
        {
            LogMethod();

            Modbus.Write<TransformRatio>(transformRatio);
        }

        public void WriteOutputs(Reles output, bool state)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)output, state);
        }

        public void LCDBacklight(bool state)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.LCD.BACKLIGHT, state);
        }

        public void DisplayTest()
        {
            LogMethod();

            var trama = "FF".PadRight(40, 'F');
            Modbus.WriteString((ushort)Registers.LCD.MAP, trama);
        }

        public void ClearDisplay()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.LCD.CLEAR, true);
        }

        #endregion

        #region Auxiliar functions

        /// <summary>
        /// Rewrites the string in the MAC format
        /// </summary>
        /// <param name="stringtoFormat">The string to give MAC format</param>
        /// <returns>The string formatted</returns>
        private String StringTOMACFormat(String stringtoFormat)
        {
            if (stringtoFormat.Length <= 12)
            {
                string sendMAC = stringtoFormat.Substring(0, 2) + ":"
                    + stringtoFormat.Substring(2, 2) + ":"
                    + stringtoFormat.Substring(4, 2) + ":"
                    + stringtoFormat.Substring(6, 2) + ":"
                    + stringtoFormat.Substring(8, 2) + ":"
                    + stringtoFormat.Substring(10, 2);

                return sendMAC;
            }
            else
                throw new FormatException("Error. La longitud de la dirección MAC es mayor de la esperada");
        }

        /// <summary>
        /// Efectua un swap por parejas de caracteres para una cadena de caracteres
        /// </summary>
        /// <param name="StringToSwap">La cadena de caracteres sobre la que efectuar el swap</param>
        /// <returns>La cadena de caracteres con el swap efectuado</returns>
        private string StringSwap(string stringToSwap)
        {
            if (stringToSwap.Length < 8)
                for (int i = stringToSwap.Length; i < 8; i++)
                    stringToSwap += " ";

            char[] array = stringToSwap.ToCharArray();

            for (int i = 0; i < array.Length; i += 2)
            {
                char temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
            }
            return new string(array);
        }


        #endregion

        #region Adjust functions

        public Tuple<bool, ushort> AdjustDC20(int delFirst, int initCount, int samples, int interval, Func<ushort, bool> adjustValidation, Func<double[], bool> sampleValidation = null)
        {
            FlagTest();
            var dc20Gain = ReadDC20Gains();
            var result = new ushort();
            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 30,
                () =>
                {
                    var DC20 = ReadContinousVariables().DC20;
                    return new double[] { DC20 };
                },
                (listValues) =>
                {
                    result = (ushort)((20 / listValues.Average(0)) * dc20Gain);
                    return adjustValidation(result);
                },
                (SamplesToValidate) =>
                {
                    return sampleValidation(SamplesToValidate);
                });


            if (adjustResult.Item1)
            {
                FlagTest();
                WriteDC20Gain(result);
            }

            return Tuple.Create(adjustResult.Item1, result);
        }

        public Tuple<bool, ushort> AdjustDC200(int delFirst, int initCount, int samples, int interval, Func<ushort, bool> adjustValidation, Func<double[], bool> sampleValidation = null)
        {
            FlagTest();
            var dc200Gain = ReadDC200Gains();
            var result = new ushort();
            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 30,
                () =>
                {
                    var DC200 = ReadContinousVariables().DC200;
                    return new double[] { DC200 };
                },
                (listValues) =>
                {
                    result = (ushort)((200 / listValues.Average(0)) * dc200Gain);
                    return adjustValidation(result);
                },
                (SamplesToValidate) =>
                {
                    return sampleValidation(SamplesToValidate);
                });

            if (adjustResult.Item1)
            {
                FlagTest();
                WriteDC200Gain(result);
            }
            return Tuple.Create(adjustResult.Item1, result);
        }

        public Tuple<bool, MeasureGains> CalculateAdjustMeasureFactors(int delFirst, int initCount, int samples, int interval, double voltage, double current, double transfomationRelation, Func<MeasureGains, bool> adjustValidation, Func<TriDouble, bool> sampleValidation = null)
        {
            FlagTest();

            var gains = ReadMeasureGains();

            var result = new MeasureGains();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 100,
                () =>
                {
                    var allVariables = ReadAllVariables();
                    return new double[] { allVariables.Phases.L1.Voltage, allVariables.Phases.L2.Voltage, allVariables.Phases.L3.Voltage,
                                          allVariables.Phases.L1.Current / transfomationRelation, allVariables.Phases.L2.Current / transfomationRelation, allVariables.Phases.L3.Current / transfomationRelation,
                                          allVariables.Phases.L1.ActivePower / transfomationRelation , allVariables.Phases.L2.ActivePower / transfomationRelation, allVariables.Phases.L3.ActivePower / transfomationRelation,
                                          allVariables.ThreePhase.L12Voltage, allVariables.ThreePhase.L23Voltage, allVariables.ThreePhase.L31Voltage};
                },
                (listValues) =>
                {
                    result.Voltage.L1 = Convert.ToUInt16(gains.Voltage.L1 * Math.Pow(voltage / listValues.Average(0), 2));
                    result.Voltage.L2 = Convert.ToUInt16(gains.Voltage.L2 * Math.Pow(voltage / listValues.Average(1), 2));
                    result.Voltage.L3 = Convert.ToUInt16(gains.Voltage.L3 * Math.Pow(voltage / listValues.Average(2), 2));

                    result.Current.L1 = Convert.ToUInt16(gains.Current.L1 * Math.Pow(current / listValues.Average(3), 2));
                    result.Current.L2 = Convert.ToUInt16(gains.Current.L2 * Math.Pow(current / listValues.Average(4), 2));
                    result.Current.L3 = Convert.ToUInt16(gains.Current.L3 * Math.Pow(current / listValues.Average(5), 2));

                    result.Power.L1 = Convert.ToUInt16(gains.Power.L1 * (voltage * current / listValues.Average(6)));
                    result.Power.L2 = Convert.ToUInt16(gains.Power.L2 * (voltage * current / listValues.Average(7)));
                    result.Power.L3 = Convert.ToUInt16(gains.Power.L3 * (voltage * current / listValues.Average(8)));

                    result.Compound.L12 = Convert.ToUInt16(gains.Compound.L12 * Math.Pow((voltage * Math.Sqrt(3)) / listValues.Average(9), 2));
                    result.Compound.L23 = Convert.ToUInt16(gains.Compound.L23 * Math.Pow((voltage * Math.Sqrt(3)) / listValues.Average(10), 2));
                    result.Compound.L31 = Convert.ToUInt16(gains.Compound.L31 * Math.Pow((voltage * Math.Sqrt(3)) / listValues.Average(11), 2));

                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }

        public Tuple<bool, TriUShort> CalculateGapAdjustFactors(int delFirst, int initCount, int samples, int interval, double powerReference, double powerFactor, double transfomationRelation, Func<TriUShort, bool> adjustValidation, Func<TriDouble, bool> sampleValidation = null)
        {
            FlagTest();

            var offsetGains = ReadOffSetGains();

            var result = new TriUShort();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 30,
                () =>
                {
                    var allVariables = ReadAllVariables();
                    return new double[] { allVariables.Phases.L1.ActivePower / transfomationRelation, allVariables.Phases.L2.ActivePower / transfomationRelation, allVariables.Phases.L3.ActivePower / transfomationRelation };
                },
                (listValues) =>
                {
                    var angleL1 =  (1 - ((listValues.Average(0) - powerReference) / powerReference)) * powerFactor;
                    angleL1 = Math.Atan(Math.Sqrt(1 - Math.Pow(angleL1, 2)) / angleL1).ToDegree() - 60;
                    result.L1 = Convert.ToUInt16(1000 - ((angleL1 * 128000) / 360));

                    var angleL2 = (1 - ((listValues.Average(1) - powerReference) / powerReference)) * powerFactor;
                    angleL2 = Math.Atan(Math.Sqrt(1 - Math.Pow(angleL2, 2)) / angleL2).ToDegree() - 60;
                    result.L2 = Convert.ToUInt16(1000 - ((angleL2 * 128000) / 360));

                    var angleL3 = (1 - ((listValues.Average(2) - powerReference) / powerReference)) * powerFactor;
                    angleL3 = Math.Atan(Math.Sqrt(1 - Math.Pow(angleL3, 2)) / angleL3).ToDegree() - 60;
                    result.L3 = Convert.ToUInt16(1000 - ((angleL3 * 128000) / 360));

                    return adjustValidation(result);
                },
                (SamplesToValidate) =>
                {
                    TriDouble toValidate = new TriDouble { L1 = SamplesToValidate[0], L2 = SamplesToValidate[1], L3 = SamplesToValidate[2] };
                    return sampleValidation(toValidate);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }

        #endregion

        #region Structures

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ElectricalVariables.PHASES)]
        public struct AllVariables
        {
            public PhaseVariables Phases;
            public ContinousVariables Continous;
            public ThreePhaseVariables ThreePhase;
            public TotalHarmonicDistorsion THD;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ElectricalVariables.PHASES)]
        public struct PhaseVariables
        {
            public SinglePhaseVariables L1;
            public SinglePhaseVariables L2;
            public SinglePhaseVariables L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ElectricalVariables.PHASES)]
        public struct SinglePhaseVariables
        {
            private Int32 voltage;
            private Int32 current;
            private Int32 activePower;
            private Int32 inductiveReactivePower;
            private Int32 capacitiveReactivePower;
            private Int32 apparentPower;
            private Int32 powerFactor;
            private Int32 phiCosine;

            public double Voltage { get { return Convert.ToDouble(voltage) / 100; } }
            public double Current { get { return Convert.ToDouble(current) / 10000; } } // en Amperios Escala Internacional
            public double ActivePower { get { return Convert.ToDouble(activePower) / 10; } } // en Watios
            public double InductiveReactivePower { get { return Convert.ToDouble(inductiveReactivePower) / 10; } }
            public double CapacitiveReactivePower { get { return Convert.ToDouble(capacitiveReactivePower) / 10; } }
            public double ApparentPower { get { return Convert.ToDouble(apparentPower) / 10; } }
            public double PowerFactor { get { return Convert.ToDouble(powerFactor) / 1000; } }
            public double PhiCosine { get { return Convert.ToDouble(phiCosine) / 1000; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ElectricalVariables.CONTINUOUS)]
        public struct ContinousVariables
        {
            private Int32 dc200;
            private Int32 dc20;

            public double DC200 { get { return Convert.ToDouble(dc200) / 100; } }
            public double DC20 { get { return Convert.ToDouble(dc20) / 1000; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ElectricalVariables.THREE_PHASE)]
        public struct ThreePhaseVariables
        {
            private Int32 frequency;
            private Int32 l12Voltage;
            private Int32 l23Voltage;
            private Int32 l31Voltage;
            private Int32 averagePhaseVoltage;
            private Int32 averageLinevoltage;
            private Int32 averageCurrent;
            private Int32 activePower;
            private Int32 inductiveReactivePower;
            private Int32 capacitiveReactivePower;
            private Int32 apparentPower;
            private Int32 powerFactor;
            private Int32 phiCosine;

            public double Frequency { get { return Convert.ToDouble(frequency) / 100; } }
            public double L12Voltage { get { return Convert.ToDouble(l12Voltage) / 100; } }
            public double L23Voltage { get { return Convert.ToDouble(l23Voltage) / 100; } }
            public double L31Voltage { get { return Convert.ToDouble(l31Voltage) / 100; } }
            public double AveragePhaseVoltage { get { return Convert.ToDouble(averagePhaseVoltage) / 100; } }
            public double AverageLineVoltage { get { return Convert.ToDouble(averageLinevoltage) / 100; } }
            public double AverageCurrent { get { return Convert.ToDouble(averageCurrent) / 10; } }
            public double ActivePower { get { return Convert.ToDouble(activePower) / 10; } }
            public double InductiveReactivePower { get { return Convert.ToDouble(inductiveReactivePower) / 10; } }
            public double CapacitiveReactivePower { get { return Convert.ToDouble(capacitiveReactivePower) / 10; } }
            public double ApparentPower { get { return Convert.ToDouble(apparentPower); } }
            public double PowerFactor { get { return Convert.ToDouble(powerFactor) / 1000; } }
            public double PhiCosine { get { return Convert.ToDouble(phiCosine) / 1000; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ElectricalVariables.THD)]
        public struct TotalHarmonicDistorsion
        {
            private Int32 voltageL1;
            private Int32 voltageL2;
            private Int32 voltageL3;
            private Int32 currentL1;
            private Int32 currentL2;
            private Int32 currentL3;

            public double VoltageL1 { get { return Convert.ToDouble(voltageL1) / 10; } }
            public double VoltageL2 { get { return Convert.ToDouble(voltageL2) / 10; } }
            public double VoltageL3 { get { return Convert.ToDouble(voltageL3) / 10; } }
            public double CurrentL1 { get { return Convert.ToDouble(currentL1) / 10; } }
            public double CurrentL2 { get { return Convert.ToDouble(currentL2) / 10; } }
            public double CurrentL3 { get { return Convert.ToDouble(currentL3) / 10; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.Gains.VOLTAGE)]
        public struct MeasureGains
        {
            public TriUShort Voltage;
            public TriUShort Current;
            public CompoundGains Compound;
            public TriUShort Power;
            public ushort DC20;
            public ushort DC200;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CompoundGains
        {
            public ushort L12;
            public ushort L23;
            public ushort L31;
        }

        public struct TriInt32
        {
            public Int32 L1;
            public Int32 L2;
            public Int32 L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriUShort
        {
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        public struct TriDouble
        {
            public double L1;
            public double L2;
            public double L3;
        }

        public struct TriDoubleCompound
        {
            public double L12;
            public double L23;
            public double L31;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.Setup.TRANSFORM_RATIO)]
        public struct TransformRatio
        {
            public uint VoltagePrimary;
            public ushort VoltageSecondary;
            public ushort CurrentPrimary;
            //No falta el secundario de intensidad?
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.Setup.DATETIME)]
        public struct CLUXDateTime
        {
            public ushort Day;
            public ushort Month;
            public ushort Year;
            public ushort Hour;
            public ushort Minutes;
            public ushort Seconds;

            public CLUXDateTime(DateTime date)
            {
                Day = (ushort)date.Day;
                Month = (ushort)date.Month;
                Year = (ushort)date.Year;
                Seconds = (ushort)date.Second;
                Minutes = (ushort)date.Minute;
                Hour = (ushort)date.Hour;
            }
        }

        public struct MACIPEmbedded
        {
            public string MAC { get; set; }
            public string IP { get; set; }
            public string VersionEmbedded { get; set; }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Fases
        {
            private Int32 l1;
            private Int32 l2;
            private Int32 l3;

            public double L1 { get { return Convert.ToDouble(l1); } set { l1 = Convert.ToInt32(value); } }
            public double L2 { get { return Convert.ToDouble(l2); } set { l2 = Convert.ToInt32(value); } }
            public double L3 { get { return Convert.ToDouble(l3); } set { l3 = Convert.ToInt32(value); } }
        }

        #endregion

        #region Registers and values

        public enum KeyboardKeys
        {
            NO_KEY = 0x0000,
            KEY_1 = 0X0001,
            KEY_2 = 0X0002,
            KEY_3 = 0X0004,
            KEY_4 = 0X0008,
            BUTTON = 0X0010,
        }

        /// <summary>
        /// El valor que devuelve el dispositivo en funcion de la entrada activa (se comprueban de una en una) al pedir el registro de las entradas
        /// </summary>
        public enum InputValues
        {
            NO_INPUTS = 0x00,
            INPUT_1 = 0x01,
            INPUT_2 = 0x02,
            INPUT_3 = 0x04,
            INPUT_4 = 0x08,
            INPUT_5 = 0x10,
            INPUT_6 = 0x20,
            INPUT_7 = 0x40,
            INPUT_8 = 0x80,
        }

        public enum Reles
        {
            RELE_1 = 0x1770,
            RELE_2 = 0x1771,
            RELE_3 = 0x1772,
            RELE_4 = 0x1773,
        }

        public struct Registers
        {
            public enum ElectricalVariables
            {
                PHASES = 0x0000,
                CONTINUOUS = 0x0030,
                THREE_PHASE = 0x0034,
                THD = 0x004E,
            }

            public enum Gains
            {
                VOLTAGE = 0x9C40,
                CURRENT = 0x9C43,
                COMPOUND = 0x9C46,
                KW = 0x9C49,
                DC_20 = 0x9C4C,
                DC_200 = 0x9C4D,
                PHASE_OFFSET = 0x9CA4,
                FREQUENCY= 0x9CAE,
            }

            public enum Leds
            {
                LED_1 = 0x0101,
                LED_2 = 0x0102,
                LED_3 = 0x0103,
                LED_COM = 0x17D7,
                LED_CPU = 0x17D8,
            }

            public enum LCD
            {
                BACKLIGHT = 0X138B,
                CLEAR = 0x1388,
                MAP = 0x4650,
                TEST = 0x3E8A,
            }

            public enum Setup
            {
                FLAG_TEST = 0x2AF8,
                EMBEDDED_COM = 0x2FA8,
                RESET = 0x07D0,

                TRANSFORM_RATIO = 0x2710,
                DATETIME = 0x274C,
                MAC = 0x2BC0,
                DEFAULT = 0x0BBC,
                INITIAL_MESSAGE = 0x2b7a,
              
                CLEAR_CURRENT_ENERGY = 0x0834,
                ERROR_CODE = 0x9FC4,

                BASTIDOR=0x0456,
                SERIAL_NUMBER = 0x2B5C,
                PRODUCT_NUMBER = 0x2B66,
                HARDWARE_VECTOR = 0xB3B0,

                RS232_RS485 = 0x0FA0,
                RS485 = 0x0FA1,

                TEST_RAM_FAST = 0x2F44,
                TEST_RAM_BATTERY = 0x2F45,
                DSP = 0x2724,

                EMBEDDED_VERSION = 0x2C88,
                SOFTWARE_VERSION = 0x2AF8,
                ARM_SOFTWARE_VERSION = 0x2C24,

                MAC_IP_EMBEDDED = 0x0000,
                TIMER_TEST_EMBEDDED = 0x0001,
                ACTIVATE_RTS_RS485 = 0x0002,
            }

            public enum Inputs
            {
                DIGITAL_INPUTS = 0x4E20,
                KEYBOARD = 0x4E22,
                TEMPERATURE = 0x3A98,
            }
        }

        #endregion

        public  override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}