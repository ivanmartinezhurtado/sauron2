﻿using Dezac.Device;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(InitMidabusDeviceActionDescription))]
    public class InitMidabusDeviceAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nº de Periférico")]
        public byte? Periferico { get; set; }

        [Category("3. Configuration")]
        [Description("Puerto serie")]
        public string Port { get; set; }

        [Category("3. Configuration")]
        [Description("Velocidad de comunicación")]
        public BpsEnum Bps { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            MidabusDevice device;

                var portDevice = Convert.ToByte(VariablesTools.ResolveValue(Port));
                device = new MidabusDeviceSerialPort(portDevice, (int)Bps, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);  

            if (Periferico.HasValue)
                device.PerifericNumber = Periferico.Value;
          
            Context.Variables.AddOrUpdate(DeviceName ?? "MidabusDevice", device);
        }
    }

    public class InitMidabusDeviceActionDescription : ActionDescription
    {
        public override string Name { get { return "InitMidabusDevice"; } }
        public override string Description { get { return "Crea una instancia de un dispositivo midabus"; } }
        public override string Category { get { return "Tools/Comunications/Midabus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "DisposedMidabusDeviceAction:2"; } }
    }
}
