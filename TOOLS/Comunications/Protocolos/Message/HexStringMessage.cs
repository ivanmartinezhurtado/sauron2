﻿using Comunications.Utility;
using System;
using System.Text;

namespace Comunications.Message
{
    public class HexStringMessage : BasicMessage, IValidateMessage
    {
        public HexStringMessage()
        {
            Encoding = ASCIIEncoding.ASCII;
        }

        public HexStringMessage(string text)
            : this()
        {
            Text = text;
        }

        public static HexStringMessage Create(IFormatProvider formatProvider, string format, params object[] args)
        {
            HexStringMessage message = new HexStringMessage { Format = format };

            message.SetArgs(formatProvider, args);

            return message;
        }

        public Func<HexStringMessage, bool> Validator { get; set; }

        public Encoding Encoding { get; set; }

        public string Format { get; set; }

        public virtual string Text
        {
            get
            {
                if (frame == null)
                    return null;

                return Extensions.BytesToHexString(frame);
            }
            set
            {

                byte[] frames = Extensions.HexStringToBytes(value); 
                Initialize(frames);
            }
        }

        public void SetArgs(params object[] args)
        {
            SetArgs((IFormatProvider)null, args);
        }

        public void SetArgs(IFormatProvider formatProvider, params object[] args)
        {
            if (string.IsNullOrEmpty(Format))
            {
                if (args.Length > 1)
                    throw new Exception("Invalid arguments for given format");

                Text = args[0].ToString();
            }
            else
                if (formatProvider != null)
                    Text = string.Format(formatProvider, Format, args);
                else
                    Text = string.Format(Format, args);
        }

        public virtual void ValidateResponse(ITransportMessage response)
        {
            if (Validator != null && !Validator((HexStringMessage)response))
                throw new FormatException("Error, formato de la trama de respuesta del Hex Messsage incorrecta");
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
