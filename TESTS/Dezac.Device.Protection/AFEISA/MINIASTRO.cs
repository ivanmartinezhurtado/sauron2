﻿using System;
using System.Collections.Generic;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.02)]
    public class MINIASTRO : DeviceBase
    {
        public MINIASTRO()
        {
        }

        public MINIASTRO(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 38400, byte periferic = 1)
        {
            if (Midabus == null)
                Midabus = new MidabusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, 8, _logger);
            else
                Midabus.PortCom = port;

            Midabus.PerifericNumber = periferic;
        }

        public MidabusDeviceSerialPort Midabus { get; internal set; }


        private const string FORMAT_EEPROM = "91DE2ACFAB";
        private const string FORMAT_EEPROM_VERIFICATION = "12000004";
        private const string READ_VERSION = "10";
        private const string READ_SERIAL_NUMBER = "19";
        private const string READ_KEYBOARD = "16002806";
        private const string READ_TEMPERATURE = "31";

        private const string WRITE_SERIAL_NUMBER = "99DE2ACFAB{0}";
        private const string WRITE_OUTPUTS_OFF = "9E01FF020003FF";
        private const string WRITE_OUTPUTS_ON = "9E01{0:00}02{1:00}03{2:00}";
        private const string WRITE_ACTIVE_PULSE_FREQ = "3010";
        private const string WRITE_DESACTIVE_PULSE_FREQ = "31FF";
        private const string WRITE_TEST_CPU = "12000004";
        private const string WRITE_RESET_CPU = "1100";

        private const string WRITE_DISPLAY_SEGMENTS_HORIZONTAL = "9829292929122812281228122812280000";
        private const string WRITE_DISPLAY_SEGMENTS_VERTICAL = "98565656D6055005500550055005500000";
        private const string WRITE_DISPLAY_ALL_SEGMENTS = "987F7F7FFFF77FF77FF77FF77FF77F0000";
        private const string WRITE_DISPLAY_ALL_SIMBOLS = "988080800008800880088008800880FFFF";

        private const string WRITE_TABLA_EVENTOS_1 = "92008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000800000008000000000";
        private const string WRITE_TABLA_EVENTOS_2 = "9200C000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        private const string WRITE_TABLA_EVENTOS_3 = "92010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        private const string WRITE_TABLA_EVENTOS_4 = "92014000000000000000000000000000000000800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        private const string WRITE_TABLA_EVENTOS_VALIDACION = "92007E600D";

        private const string WRITE_INICIALIZACION_CONTADORES= "A67FFF";
        private const string WRITE_DATETIME = "9D{0}";
        private const string WRITE_FEQUENCY_CLOCK = "92002C{0}";
        private const string WRITE_TEST_GPS = "17009801";
        private const string WRITE_TABLA_CAMBIO_HORARIO_1 = "922F30007800B432045F3C5E3B5D3A5C395A3E593D5F3C5E3B5C395B3F5A3E593D5E3B5D3A5C395B3F593D5F3C5E3B5D3A5B3F5A3E593D5F3C5D3A2D";
        private const string WRITE_TABLA_CAMBIO_HORARIO_2 = "922F685C395B3F5A3E5F3C5E3B5D3A5C395A3E593D5F3C5E3B5C395B3F5A3E593D5E3B5D3A5C395B3F593D5F3C5E3B5D3A5B3F5A3E02";
        private const string WRITE_TABLA_PROG_TECLADO = "923EF5FFFC00FC0080006000FFFF";

        private const string WRITE_ACTIVACION_FABRICA = "13F1";
        private const string WRITE_GRABACION_FABRICA = "93FA";
        private const string WRITE_ACTIVACION_USUARIO_1 = "A001";
        private const string WRITE_GRABACION_USUARIO_1 = "9301";
        private const string WRITE_ACTIVACION_USUARIO_2 = "A002";
        private const string WRITE_GRABACION_USUARIO_2 = "9302";

        public string ReadVersion()
        {
            LogMethod();
            var result= Midabus.Read(READ_VERSION, "90");
            var version = result.Substring(2, result.Length - 4);
            return version;
        }
        public int ReadSerialNumber()
        {
            LogMethod();
            var trama = Midabus.Read(READ_SERIAL_NUMBER, "99");
            var ns = Convert.ToInt32(trama.Substring(2,8), 16);
            return ns;
        }

        #region Temperatura
        public double  ReadTemperature()
        {
            LogMethod();
            var trama = Midabus.Read(READ_TEMPERATURE, "B1");
            var puntosHex = trama.Substring(2, 4);
            var puntos = Convert.ToInt32(puntosHex, 16);
            return TemperatureCponvertPunt[puntos];
        }

        private readonly Dictionary<int, double> TemperatureCponvertPunt = new Dictionary<int, double>
        {
                { 418, 17.31134004 },
                { 419, 17.39318762 },
                { 420, 17.47501403 },
                { 421, 17.55681994 },
                { 422, 17.63860604 },
                { 423, 17.72037298 },
                { 424, 17.80212146 },
                { 425, 17.88385212 },
                { 426, 17.96556566 },
                { 427, 18.04726272 },
                { 428, 18.12894398 },
                { 429, 18.21061011 },
                { 430, 18.29226175 },
                { 431, 18.37389959 },
                { 432, 18.45552428 },
                { 433, 18.53713647 },
                { 434, 18.61873683 },
                { 435, 18.70032601 },
                { 436, 18.78190466 },
                { 437, 18.86347345 },
                { 438, 18.94503303 },
                { 439, 19.02658405 },
                { 440, 19.10812715 },
                { 441, 19.189663 },
                { 442, 19.27119223 },
                { 443, 19.35271551 },
                { 444, 19.43423347 },
                { 445, 19.51574676 },
                { 446, 19.59725603 },
                { 447, 19.67876192 },
                { 448, 19.76026509 },
                { 449, 19.84176616 },
                { 450, 19.92326579 },
                { 451, 20.00476462 },
                { 452, 20.08626329 },
                { 453, 20.16776243 },
                { 454, 20.2492627 },
                { 455, 20.33076472 },
                { 456, 20.41226915 },
                { 457, 20.49377661 },
                { 458, 20.57528775 },
                { 459, 20.6568032 },
                { 460, 20.73832361 },
                { 461, 20.8198496 },
                { 462, 20.90138182 },
                { 463, 20.98292091 },
                { 464, 21.06446749 },
                { 465, 21.1460222 },
                { 466, 21.22758569 },
                { 467, 21.30915859 },
                { 468, 21.39074152 },
                { 469, 21.47233513 },
                { 470, 21.55394006 },
                { 471, 21.63555693 },
                { 472, 21.71718638 },
                { 473, 21.79882905 },
                { 474, 21.88048557 },
                { 475, 21.96215658 },
                { 476, 22.04384272 },
                { 477, 22.12554461 },
                { 478, 22.2072629 },
                { 479, 22.28899821 },
                { 480, 22.37075119 },
                { 481, 22.45252247 },
                { 482, 22.53431269 },
                { 483, 22.61612249 },
                { 484, 22.69795249 },
                { 485, 22.77980334 },
                { 486, 22.86167568 },
                { 487, 22.94357014 },
                { 488, 23.02548737 },
                { 489, 23.107428 },
                { 490, 23.18939266 },
                { 491, 23.27138201 },
                { 492, 23.35339668 },
                { 493, 23.43543731 },
                { 494, 23.51750455 },
                { 495, 23.59959903 },
                { 496, 23.6817214 },
                { 497, 23.76387231 },
                { 498, 23.84605239 },
                { 499, 23.92826229 },
                { 500, 24.01050266 },
                { 501, 24.09277415 },
                { 502, 24.1750774 },
		        { 503, 24.25741306 },
		        { 504, 24.33978179 },
		        { 505, 24.42218422 },
		        { 506, 24.50462101 },
		        { 507, 24.58709282 },
		        { 508, 24.6696003 },
		        { 509, 24.7521441 },
		        { 510, 24.83472488 },
		        { 511, 24.91734329 },
 		        { 512, 25 },
                { 513, 25.08269566 },
                { 514, 25.16543092 },
                { 515, 25.24820646 },
                { 516, 25.33102294 },
                { 517, 25.41388101 },
                { 518, 25.49678135 },
                { 519, 25.49678135 },
                { 520, 25.66271149 },
                { 521, 25.74574263 },
                { 522, 25.82881871 },
                { 523, 25.9119404 },
                { 524, 25.99510838 },
                { 525, 26.07832332 },
                { 526, 26.1615859 },
                { 527, 26.24489679 },
                { 528, 26.32825669 },
                { 529, 26.41166626 },
                { 530, 26.4951262 },
                { 531, 26.57863719 },
                { 532, 26.66219992 },
                { 533, 26.74581507 },
                { 534, 26.82948334 },
                { 535, 26.91320543 },
                { 536, 26.99698201 },
                { 537, 27.08081381 },
                { 538, 27.1647015 },
                { 539, 27.24864579 },
                { 540, 27.3326474 },
                { 541, 27.41670701 },
                { 542, 27.50082534 },
                { 543, 27.5850031 },
                { 544, 27.66924099 },
                { 545, 27.75353974 },
                { 546, 27.83790006 },
                { 547, 27.92232267 },
                { 548, 28.00680829 },
                { 549, 28.09135763 },
                { 550, 28.17597144 },
                { 551, 28.26065043 },
                { 552, 28.34539534 },
                { 553, 28.4302069 },
                { 554, 28.51508584 },
                { 555, 28.60003291 },
                { 556, 28.68504884 },
                { 557, 28.77013438 },
                { 558, 28.85529028 },
                { 559, 28.94051728 },
                { 560, 29.02581614 },
                { 561, 29.11118761 },
                { 562, 29.19663245 },
                { 563, 29.28215142 },
                { 564, 29.36774529 },
                { 565, 29.45341482 },
                { 566, 29.53916077 },
                { 567, 29.62498394 },
                { 568, 29.71088508 },
                { 569, 29.79686499 },
                { 570, 29.88292443 },
                { 571, 29.96906421 },
                { 572, 30.0552851 },
                { 573, 30.14158791 },
                { 574, 30.22797342 },
                { 575, 30.31444243 },
                { 576, 30.40099576 },
                { 577, 30.4876342 },
                { 578, 30.57435857 },
                { 579, 30.66116968 },
                { 580, 30.74806835 },
                { 581, 30.83505539 },
                { 582, 30.92213164 },
                { 583, 31.00929793 },
                { 584, 31.09655507 },
                { 585, 31.18390393 },
                { 586, 31.27134532 },
                { 587, 31.3588801 },
                { 588, 31.44650912 },
                { 589, 31.53423323 },
                { 590, 31.62205329 },
                { 591, 31.70997016 },
                { 592, 31.7979847 },
                { 593, 31.88609779 },
                { 594, 31.9743103 },
                { 595, 32.0626231 },
                { 596, 32.15103709 },
                { 597, 32.23955315 },
                { 598, 32.32817218 },
                { 599, 32.41689506 },
                { 600, 32.50572271 },
                { 601, 32.59465603 },
                { 602, 32.68369593 },
                { 603, 32.77284333 },
                { 604, 32.86209916 },
                { 605, 32.95146433 },
                { 606, 33.04093978 },
                { 607, 33.13052645 },
                { 608, 33.22022528 },
                { 609, 33.31003722 },
                { 610, 33.39996321 },
                { 611, 33.49000423 },
                { 612, 33.58016123 },
                { 613, 33.67043519 },
                { 614, 33.76082708 },
                { 615, 33.85133787 },
                { 616, 33.94196856 },
                { 617, 34.03272014 },
                { 618, 34.12359361 },
                { 619, 34.21458997 },
                { 620, 34.30571023 },
                { 621, 34.39695541 },
                { 622, 34.48832653 },
                { 623, 34.57982463 },
                { 624, 34.67145073 },
                { 625, 34.76320588 },
                { 626, 34.85509112 },
                { 627, 34.94710752 },
                { 628, 35.03925613 },
                { 629, 35.13153803 },
                { 630, 35.22395428 },
                { 631, 35.31650597 },
                { 632, 35.40919418 },
                { 633, 35.50202002 },
                { 634, 35.59498459 },
                { 635, 35.688089 },
                { 636, 35.78133436 },
                { 637, 35.87472181 },
                { 638, 35.96825246 },
                { 639, 36.06192748 },
                { 640, 36.15574799 },
                { 641, 36.24971516 },
                { 642, 36.34383015 },
                { 643, 36.43809414 },
                { 644, 36.5325083 },
                { 645, 36.62707382 },
                { 646, 36.72179189 },
                { 647, 36.81666373 },
                { 648, 36.91169054 },
                { 649, 37.00687355 },
                { 650, 37.10221398 },
        };

        #endregion

        public enum KeyBoardEnum: long
        {
            TECLA_UP = 280375465082880,
            TECLA_LEFT = 1095216660480,
            TECLA_DOWN = 16711680,
            TECLA_RIGHT = 4278190080,
            TECLA_EXIT = 65280,
            TECLA_OK = 255,
        }

        public KeyBoardEnum ReadKeyBorad()
        {
            LogMethod();
            var result = Midabus.Read(READ_KEYBOARD, "9600");
            var keyboard = result.Substring(6, result.Length-8);
            return (KeyBoardEnum)Convert.ToInt64(keyboard, 16);
        }

        public void WriteFormatEEPROM()
        {
            LogMethod();
            Midabus.Write(FORMAT_EEPROM, "00");
        }
        public void WriteFormatEEPROMVerification()
        {
            LogMethod();
            Midabus.Write(FORMAT_EEPROM_VERIFICATION, "920000FFFFFF");
        }

        public void WriteSerialNumber(int serialNumber)
        {
            LogMethod();
            Midabus.Write(WRITE_SERIAL_NUMBER, "00", serialNumber.ToString("X8"));
        }

        public void WriteOuputsOFF()
        {
            LogMethod();
            Midabus.Write(WRITE_OUTPUTS_OFF , "EE");
        }

        public enum Reles
        {
            C1,
            C2,
            C3
        }

        public void WriteOuput(Reles rele, bool state)
        {
            LogMethod();
            var off= "FF";
            var on = "00";
            var c1 = off;
            var c2 = off;
            var c3 = off;

            if (rele == Reles.C1)
                c1 = state ? on : off;

            if (rele == Reles.C2)
                c2 = state ? off : on;

            if (rele == Reles.C3)
                c3 = state ? on : off;

            var tramaTx = string.Format(WRITE_OUTPUTS_ON, c1, c2, c3);
            Midabus.Write(tramaTx, "EE");
        }

        public enum DisplayEnum
        {
            SEGMENTOS_HORIZONTALES = 0,
            SEGMENTOS_VERTICALES = 1,
            TODOS_SEGEMENTOS = 2,
            TODOS_SIMBOLOS =3
        }

        public void WriteDisplay(DisplayEnum typeDisplayTest)
        {
            LogMethod();

            string tramaTx = WRITE_DISPLAY_ALL_SEGMENTS;

            if (typeDisplayTest == DisplayEnum.SEGMENTOS_HORIZONTALES)
                tramaTx = WRITE_DISPLAY_SEGMENTS_HORIZONTAL;
            else if (typeDisplayTest == DisplayEnum.SEGMENTOS_VERTICALES)
                tramaTx = WRITE_DISPLAY_SEGMENTS_VERTICAL;
            else if (typeDisplayTest == DisplayEnum.TODOS_SIMBOLOS)
                tramaTx = WRITE_DISPLAY_ALL_SIMBOLS;

            Midabus.Write(tramaTx, "00");
        }

        public void WriteActivePulseFreq()
        {
            LogMethod();
            Midabus.Write(WRITE_ACTIVE_PULSE_FREQ);  
        }

        public void WriteDesActivePulseFreq()
        {
            LogMethod();
            Midabus.Write(WRITE_DESACTIVE_PULSE_FREQ);
        }

        public void WriteResetCPU()
        {
            LogMethod();
            Midabus.Write(WRITE_RESET_CPU);
        }

        public void WriteTestCPU()
        {
            LogMethod();
            Midabus.Write(WRITE_TEST_CPU, "920000FFFFFFF");
        }

        public int WriteTestGPS()
        {
            LogMethod();
            var trama = Midabus.Read(WRITE_TEST_GPS, "97");
            var ns = Convert.ToInt32(trama.Substring(6, 4), 16);
            return ns;
        }

        public void WriteInitializationTablaEventos()
        {
            LogMethod();       

            Midabus.Write(WRITE_TABLA_EVENTOS_1, "00");
  
            Midabus.Write(WRITE_TABLA_EVENTOS_2, "00");

            Midabus.Write(WRITE_TABLA_EVENTOS_3, "00");

            Midabus.Write(WRITE_TABLA_EVENTOS_4, "00");
        }

        public void WriteValidacionTablasEventos()
        {
            LogMethod();
            Midabus.Write(WRITE_TABLA_EVENTOS_VALIDACION, "00");
        }

        public void WriteInitializationContadores()
        {
            LogMethod();
            Midabus.Write(WRITE_INICIALIZACION_CONTADORES, "00");
        }

        public void WriteDateTime()
        {
            LogMethod();
            var tramaDateTime = string.Format("{0:00}{1:00}{2:00}{3:00}{4:00}{5:00}00", DateTime.Now.Second, DateTime.Now.Minute, DateTime.Now.Hour, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year.ToString().Substring(2,2));

            Midabus.Write(WRITE_DATETIME, "00", tramaDateTime);
        }

        public void WriteFrequenceClock(double frequenceClock)
        {
            LogMethod();
            var valor = Convert.ToUInt32(frequenceClock);
            var trama = valor.ToString("X8");
            Midabus.Write("92002C{0}", "00", trama);
        }

        public void WriteTablaCambioHorario()
        {
            LogMethod();

            Midabus.Write(WRITE_TABLA_CAMBIO_HORARIO_1, "00");
            Midabus.Write(WRITE_TABLA_CAMBIO_HORARIO_2, "00");
            Midabus.Write("A42F30", "00");
        }


        public void WriteTablaProgTeclado()
        {
            LogMethod();

            Midabus.Write(WRITE_TABLA_PROG_TECLADO, "00");
            Midabus.Write("92000D3EF83EF5", "00");
        }


        public void WriteConfiguracionFabrica()
        {
            LogMethod();

            Midabus.Write(WRITE_ACTIVACION_FABRICA, "00");
            Midabus.Write(WRITE_GRABACION_FABRICA, "00");
        }

        public void WriteConfiguracionUsuario()
        {
            LogMethod();

            Midabus.Write(WRITE_GRABACION_USUARIO_1, "00");
            Midabus.Write(WRITE_GRABACION_USUARIO_2, "00");
            Midabus.Write(WRITE_ACTIVACION_USUARIO_2, "00");
        }

        public override void Dispose()
        {
            Midabus.Dispose();
        }
    }
}
