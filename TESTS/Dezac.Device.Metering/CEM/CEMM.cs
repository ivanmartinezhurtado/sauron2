﻿using Dezac.Core.Utility;
using System;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.01)]
    public class CEMM : DeviceBase
    {
        public CEMM()
        {
        }

        public CEMM(int port)
        {
            SetPort(port);
        }

        public CEMM(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;

            if (!byte.TryParse(portTmp, out result))
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");

            SetPort(result);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Modbus.PerifericNumber = periferic;
        }


        public CEMM(string hostname, int port, ModbusTCPProtocol modbusTCPProtocol)
        {
            SetHostPortAndModbus(hostname, port, modbusTCPProtocol);
        }

        public void SetHostPortAndModbus(string hostname, int port, ModbusTCPProtocol modbusTCPProtocol)
        {
            Modbus = new ModbusDeviceTCP(hostname, port, modbusTCPProtocol, _logger); 
        }
   
        public void WriteFlagTest(bool value)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, value);
        }

        public void WriteMAC(string value)
        {
            if (value == null || value.Length != 12)
                throw new Exception(string.Format("La dirección MAC {0} no es correcta!", value));

            var data = value.HexStringToBytes();

            Modbus.WriteBytes((ushort)Registers.MAC, data);
        }

        public string ReadMAC()
        {
            var data = Modbus.ReadBytes((ushort)Registers.MAC, 3);

            return data.BytesToHexString();
        }

        public int ReadNumSerie()
        {
            var ns = Modbus.ReadString((ushort)Registers.NUM_SERIE, 5);
            return Convert.ToInt32(ns);
        }

        public int ReadNumSerie485()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.NUM_SERIE_485);
        }

        public void WriteNumSerie(string value)
        {
            string text = value.ToString().PadLeft(10, '0');

            Modbus.WriteString((ushort)Registers.NUM_SERIE, text);
        }

        public void WriteNumSerie485(string value)
        {
            LogMethod();

            int sn = Convert.ToInt32(value.ToString().PadLeft(10, '0'));

            Modbus.WriteInt32((ushort)Registers.NUM_SERIE_485, sn);
        }

        public int ReadBastidor()
        {
            var bastidor = Modbus.ReadString((ushort)Registers.BASTIDOR, 5);
            return Convert.ToInt32(bastidor);
        }

        public int ReadBastidor485()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR_485);
        }

        public void WriteBastidor(int value)
        {
            string text = value.ToString().PadLeft(10, '0');

            Modbus.WriteString((ushort)Registers.BASTIDOR, text);
        }

        public void WriteBastidor485(int value)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.BASTIDOR_485, value);
        }

        public string ReadSoftwareVersion()
        {
            return Modbus.ReadString((ushort)Registers.VERSION_SOFT, 4);
        }

        public string ReadSoftwareVersion485()
        {
            ushort[] fw = Modbus.ReadMultipleRegister((ushort)Registers.VERSION_SOFT_485, 3);

            return string.Format("{0}.{1}.{2}", fw[0].ToString(), fw[1].ToString(), fw[2].ToString());
        }
  
        public void WriteLedRed(bool value)
        {
            Modbus.WriteSingleCoil((ushort)Registers.LED_RED, value);
        }

        public void WriteLedGreen(bool value)
        {
            Modbus.WriteSingleCoil((ushort)Registers.LED_GREEN, value);
        }


        public void WriteBastidorCEMC10(int value)
        {
            Modbus.WriteInt32((ushort)Registers.BASTIDOR_CEMC10, value);
        }


        public void WriteErrorCodeCEMC10(ushort value)
        {
            Modbus.Write((ushort)Registers.ERROR_CODE_CEMC10, value);
        }

        public int ReadBastidorCEMC10()
        {
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR_CEMC10);
        }


        public int ReadEnergyCEMC10()
        {
            return Modbus.ReadInt32((ushort)Registers.ENERGY_CEMC10);
        }

        public ModbusDevice Modbus { get; internal set; }


        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public enum Registers
        {
            FLAG_TEST= 0x2AF8,
            LED_RED=0x1000,
            LED_GREEN = 0x1001,
            NUM_SERIE = 0x2756,
            BASTIDOR = 0x2792,
            VERSION_SOFT = 0x2AF8,

            
            MAC=0xF030,
            BASTIDOR_CEMC10 = 61504,
            ERROR_CODE_CEMC10 = 61536,
            ENERGY_CEMC10=0,

            VERSION_SOFT_485 = 1400,
            NUM_SERIE_485 = 61440,
            BASTIDOR_485 = 61504
        };
    }
}
