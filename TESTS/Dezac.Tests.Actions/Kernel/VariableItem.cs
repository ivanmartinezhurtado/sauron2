﻿using Dezac.Tests.Actions.Views;
using System.ComponentModel;

namespace Dezac.Tests.Actions.Kernel
{
    public class VariableItem
    {
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string VariableName { get; set; }
    }
}
