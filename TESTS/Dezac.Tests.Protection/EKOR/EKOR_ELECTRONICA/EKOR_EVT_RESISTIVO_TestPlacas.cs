﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.PowerSource;
using System;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.05)]
    public class EKOR_EVT_RESISTIVO_TestPlacas : TestBase
    {
        private const int DEVICE_PRESENCE = 0;
        private USB4761 usb;
        private MTX3293 tester;
        private MTEPPS mte;
        private double measurePrevious = 0;

        private enum OutputsManagementShortCircuit
        {
            OUT1 = 1,
            OUT2 = 2,
            OUT3 = 3,
            OUT4 = 4,
            OUT5 = 5,
            OUT6 = 6,
        }

        public void TestInitialization(string portFLUKE)
        {           
            usb = new USB4761();

            tester = new MTX3293();
            tester.WaitTimeMeasure = 1100;
            tester.PortCom = Convert.ToByte(GetVariable<string>(portFLUKE, portFLUKE).Replace("COM", ""));

            usb.DO.OffRange(0, 7);

            if (!usb.DI[DEVICE_PRESENCE])
                throw new Exception("No se ha detectado la presencia del equipo");

            mte = new MTEPPS();
        }

        public void MeasureVoltageNoSwitch()
        {
            usb.DO.OffRange(0, 7);
            Delay(200, "Esperando commutacion Adv");

            ApplyVoltage();

            var defs = new AdjustValueDef(Params.V_AC.Null.TestPoint("NO_SWITCH").Name, 0, Params.V_AC.Null.TestPoint("NO_SWITCH").Min(), Params.V_AC.Null.TestPoint("NO_SWITCH").Max(), 0, 0, ParamUnidad.mV);
            TestMeasureBase(defs, (step) =>
            {
                var value = tester.ReadVoltage(MTX3293.measureTypes.AC) * 1000;
                measurePrevious = value;
                return value;
            }, 0, 5, 500);

            mte.ApplyOffAndWaitStabilisation();
        }

        public void MeasureTotalResistance()
        { 
            var defs = new AdjustValueDef("TOTALRESISTANCE", 0, Params.RES.Null.TestPoint("TOTAL").Min(), Params.RES.Null.TestPoint("TOTAL").Max(), 0, 0, ParamUnidad.Ohms);
            TestMeasureBase(defs, (step) =>
            {
                Delay(tester.WaitTimeMeasure, "Estabilizacion del tester");
                //var value = tester.ReadResistance(FLUKE183.MeasureData.RangeResistence._50Kohm).Value;
                var value = tester.ReadResistance();
                measurePrevious = value;
                return value;
            }, 1, 5, 500);
        }

        public void ActivatedOutputsAndMeasureResistances()
        {
            usb.DO.On(0);

             var defs = new AdjustValueDef(Params.RES.Null.TestPoint("CC_TP01_TP02").Name, 0, Params.RES.Null.TestPoint("CC_TP01_TP02").Min(), Params.RES.Null.TestPoint("CC_TP01_TP02").Max(), 0, 0, ParamUnidad.Ohms);

            var tempPrevious = measurePrevious;

            var resultAfter = TestMeasureBase(defs, (step) =>
            {
                var value = tester.ReadResistance();
                measurePrevious = value;
                return tempPrevious - value;
            }, 1, 5, 1000);


            foreach (int output in Enum.GetValues(typeof(OutputsManagementShortCircuit)))
            {
                usb.DO.On(output);

                string testPointName = string.Format("CC_TP{0:00}_TP{1:00}", output + 1, output + 2);

                tempPrevious = measurePrevious;

                var defs2 = new AdjustValueDef(Params.RES.Null.TestPoint(testPointName).Name, 0, Params.RES.Null.TestPoint(testPointName).Min(), Params.RES.Null.TestPoint(testPointName).Max(), 0, 0, ParamUnidad.Ohms);
                TestMeasureBase(defs2, (step) =>
                {
                    var value = tester.ReadResistance();
                    measurePrevious = value;
                    return tempPrevious - value;
                },1 ,6, 500);
    
            }
        }

        public void ActivatedOutput7AndMeasureCapacitance()
        {
            usb.DO.On(7);

            var defs = new AdjustValueDef("CAPACITANCE", 0, Params.RES.Null.TestPoint("CAPACITANCE").Min(), Params.RES.Null.TestPoint("CAPACITANCE").Max(), 0, 0, ParamUnidad.nF);
            TestMeasureBase(defs, (step) =>
            {
                var value = tester.ReadCapacitance() * 1000000000;
                return value;
            }, 1, 5, 500);
        }

        public void TestFinish()
        {
            if (mte != null)
            {
                mte.ApplyOffAndWaitStabilisation();
                mte.Dispose();
            }
            if (tester != null)
                tester.Dispose();
            if (usb != null)
            {
                usb.DO.OffRange(0, 7);
                usb.Dispose();
            }
        }


        private void ApplyVoltage()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 300, ParamUnidad.V);
            var ConsignasV = new TriLineValue { L1 = voltage, L2 = 0, L3 = 0 };
            var ConsignasI = new TriLineValue { L1 = 0, L2 = 0, L3 = 0 };
            var freq = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 60, ParamUnidad.Hz);
            var desfase = new TriLineValue { L1 = 0, L2 = 0, L3 = 0 };
            mte.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, freq, 0, desfase, true);
        }
    }
}

