﻿using Comunications.Utility;
using Dezac.Core.Utility;
using Dezac.Device.Bluetooth;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.17)]
    public class CVMMINI2 : DeviceBase
    {
        public CVMMINI2()
        {
        }

        public CVMMINI2(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, int periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 19200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                (Modbus as ModbusDeviceSerialPort).PortCom = port;

            Modbus.PerifericNumber = (byte)periferic;
        }
        public void SetPortAndHost(int port, string host, byte periferico)
        {
            Modbus = new ModbusDeviceTCP(host, port, ModbusTCPProtocol.TCP, _logger);
            Modbus.PerifericNumber = periferico;
            Modbus.Retries = 1;
            Modbus.TimeOut = 2000;
            _logger.DebugFormat("IP Communications = {0}", ((ModbusDeviceTCP)Modbus).HostName);
        }

        public ModbusDevice Modbus { get; internal set; }

        #region Bluetooth

        private CVM_E3Device cvmBLE;

        public void BLEFindDevice(string serialNumber)
        {
            _logger.Info("Scanning CVM_E3 bluetooth LE Device...");

            cvmBLE = null;

            try
            {
                cvmBLE = CVM_E3Device.Find(serialNumber, 5000);

                if (cvmBLE != null)
                    _logger.Info($"CVM-E3 Device Found at Address {cvmBLE.BluetoothAddress}");
                else
                {
                    _logger.Info("No device found!");
                    throw new Exception("Error comunications Bluetooth, no device found");
                }
            }
            catch (Exception ex)
            {
                _logger.Info(ex.Message);
                if (ex.InnerException != null)
                    _logger.Info(ex.InnerException.Message);

                if (cvmBLE != null)
                    cvmBLE.Dispose();

                throw new Exception(ex.Message);
            }
        }

        public struct BLEDeviceInfo
        {
            public string ManufacturerName;
            public string ModelNumber;
            public string SerialNumber;
            public string HardwareRevision;
            public string FirmwareRevision;
        }

        public BLEDeviceInfo BLEGetInfo()
        {
            var value = new BLEDeviceInfo();

            value.ModelNumber = cvmBLE.GetModelNumberAsync();
            _logger.Info($"Device Model: {value.ModelNumber}");

            value.ManufacturerName = cvmBLE.GetManufacturerNameAsync();
            _logger.Info($"Manufacturer Name: { value.ManufacturerName}");

            value.SerialNumber = cvmBLE.GetSerialNumberAsync();
            _logger.Info($"Serial Number: {value.SerialNumber}");

            value.HardwareRevision = cvmBLE.GetHardwareRevisionAsync();
            _logger.Info($"HW Revision: {value.HardwareRevision}");

            value.FirmwareRevision = cvmBLE.GetFirmwareRevisionAsync();
            _logger.Info($"Firmware Revision: {value.FirmwareRevision}");

            return value;
        }

        public bool PairAsync()
        {
            _logger.Info("Starting pairing...");
            var result = cvmBLE.PairAsync(CVM_E3Device.DEFAULT_PIN);    
           _logger.InfoFormat("Pairing {0}", result);
           return result;              
        }

        public bool UnpairAsync()
        {
            _logger.Info("Starting UnpairAsync...");
            var result = cvmBLE.UnpairAsync();
           _logger.InfoFormat("Unpairing {0}", result);
            return result;
        }

        public void BLEWritedWifiEnabled(byte value)
        {
            _logger.Info($"WriteRtdSlected : {value}");
            cvmBLE.WriteWifiEnabled(value);
        }

        public ushort BLEReadWifiEnabled()
        {
            var value = cvmBLE.ReadWifiEnabled();
            _logger.Info($"ReadRtdSlected : {value}");
            return value;
        }



        #endregion

        #region Rutinas de control del equipo

        public void Reset()
        {
            LogMethod();
            try
            {
                Modbus.Retries = 0;
                Modbus.WithTimeOut((m) => { m.WriteSingleCoil((ushort)Registers.RESET, true); });
                Modbus.Retries = 3;
            }
            catch (Exception) { }
        }

        public void FlagTest(bool state = true)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }

        #endregion

        #region Operaciones de Lectura

        public ushort ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.VERSION_FIRMWARE);
        }

        public string ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.NUMERO_SERIE, 7);
        }       

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();
            return Modbus.Read<HardwareVector>();
        }

        public PropertyVector ReadPropertyVector()
        {
            LogMethod();
            return new PropertyVector(Modbus.ReadRegister((ushort)Registers.PROPIEDADES_MODELO));
        }

        public AllVariablesMeasure ReadAllVariables()
        {
            LogMethod();
            return Modbus.Read<AllVariablesMeasure>();
        }

        public TolerancesParametersCalibration ReadTolerancesError()
        {
            LogMethod();
            return Modbus.Read<TolerancesParametersCalibration>();
        }

        public Keys ReadKeyboard()
        {
            LogMethod();
            return (Keys)Modbus.ReadRegister((ushort)Registers.KEYBOARD);
        }

        public InputsState ReadDigitalInput()
        {
            LogMethod();
            return (InputsState)Modbus.ReadRegister((ushort)Registers.ESTADO_DIGITAL_INPUT);
        }

        public ushort ReadDigitalOutput() // No funciona, comentar-li al Marcos
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ESTADO_DIGITAL_OUTPUT);
        }

        public string ReadModelName()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.NOMBRE_EQUIPO, (ushort)10).Replace("\0", "").Replace(" ", "").ToUpper().Trim(' ');
        }

        public CalibrationState ReadCalibrationState()
        {
            LogMethod();
            return (CalibrationState)Modbus.ReadRegister((ushort)Registers.ESTADO_AUTOCALIBRACION);
        }

        public int ReadErrorVariables(ErrorVariablesCalibration errorAddress)
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)errorAddress);
        }

        public ErrorParametersCalibration ReadParametersErrors(ErrorVariablesCalibration errorAdress)
        {
            LogMethod();
            return (ErrorParametersCalibration)Modbus.ReadInt32((ushort)errorAdress);            
        }

        public GainFactorsVI ReadGainsVI()
        {
            LogMethod();
            return Modbus.Read<GainFactorsVI>();
        }

        public TriUShortSimple ReadCurrentGainsE2()
        {
            LogMethod();
            return Modbus.Read<TriUShortSimple>((ushort)GainFactors.CURRENT_E2);
        }

        public TriUShortSimple ReadPowerGainsE2()
        {
            LogMethod();
            return Modbus.Read<TriUShortSimple>((ushort)GainFactors.POWER_E2);
        }

        public TriUShortSimple ReadPowerGains()
        {
            LogMethod();
            return Modbus.Read<TriUShortSimple>((ushort)GainFactors.POWER);
        }

        public TriUShortSimple ReadPhaseAjustGains()
        {
            LogMethod();
            return Modbus.Read<TriUShortSimple>((ushort)GainFactors.PHASE);
        }

        public TolerancesParametersCalibration ReadMeasureError()
        {
            LogMethod();
            return Modbus.Read<TolerancesParametersCalibration>();
        }

        public Sumatories ReadSumatories(SumatoriesType sumType)
        {
            LogMethod();
            return Modbus.Read<Sumatories>((ushort)sumType);
        }

        public ushort ReadSynchFrequencyGain()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)GainFactors.SYNC_FREQUENCY);
        }

        public int ReadBacnetID()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.BACNET_ID);
        }

        public AlarmConfiguration32 ReadAlarm32bits()
        {
            LogMethod();
            return Modbus.Read<AlarmConfiguration32>();
        }

        public AlarmConfiguration16 ReadAlarm16bits()
        {
            LogMethod();
            return Modbus.Read<AlarmConfiguration16>();
        }

        public WifiConfig ReadWifiConfig()
        {
            LogMethod();
            return Modbus.ReadHolding<WifiConfig>();
        }

        public string ReadMacEthernet()
        {
            LogMethod();
            var bytesMac = Modbus.ReadMultipleRegister((ushort)Registers.MAC_ETHERNET, 3);
            var macHigh = bytesMac[0].ToBytes();
            var macMedium = bytesMac[1].ToBytes();
            var macLow = bytesMac[2].ToBytes();
            return string.Format("{0}:{1}:{2}:{3}:{4}:{5}", macHigh[0].ToString("X2"), macHigh[1].ToString("X2"), macMedium[0].ToString("X2"), macMedium[1].ToString("X2"), macLow[0].ToString("X2"), macLow[1].ToString("X2"));
        }

        public string ReadMacWifi()
        {
            LogMethod();
            var bytesMac = Modbus.ReadMultipleRegister((ushort)Registers.MAC_WIFI, 3);
            var macHigh = bytesMac[0].ToBytes();
            var macMedium = bytesMac[1].ToBytes();
            var macLow = bytesMac[2].ToBytes();
            return string.Format("{0:00}:{1:00}:{2:00}:{3:00}:{4:00}:{5:00}", macHigh[0].ToString("X2"), macHigh[1].ToString("X2"), macMedium[0].ToString("X2"), macMedium[1].ToString("X2"), macLow[0].ToString("X2"), macLow[1].ToString("X2"));
        }

        public string ReadFirmwareVersionBluetooth()
        {
            LogMethod();
            var resp = Modbus.ReadHexString((ushort)Registers.FW_BLUETOOTH, 2);
            resp = resp.Substring(0, resp.Length - 2);
            var version = "";
            for (var i = 0; i < resp.Length; i += 2)
            {
                var numVersion = Convert.ToInt16(resp.Substring(i, 2), 16);
                var versionPoint = string.Format("{0}.", numVersion);
                if (i == resp.Length -2)
                    versionPoint = versionPoint.Replace(".", "");

                version += versionPoint;
            }
            return version;
        }

       #endregion

        #region Operaciones de Escritura

        public void WriteHardwareVector(HardwareVector VectorHardware)
        {
            LogMethod();
            Modbus.Write<HardwareVector>(VectorHardware);
        }
        
        public void WritePropertyVector(PropertyVector vectorPropiedades)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PROPIEDADES_MODELO, vectorPropiedades.ToInt());
        }
        
        public void WriteSerialNumber(int Numserie)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.NUMERO_SERIE, Numserie);
        }

        public void WriteSerialNumber(string Numserie)
        {
            LogMethod();
            Modbus.WriteString((ushort)Registers.NUMERO_SERIE, Numserie);
        }        


        public void WriteSetupDefault()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.SETUP_DEFECTO, true);
        }

        public void WriteGainsDefault()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.GAINS_DEFECTO, true);
        }

        public void WriteGainsNonDefault(ushort voltageGain, ushort currentGain, ushort powerGain, ushort gapGain, ushort neutralCurrentGain, ushort synchronismFrequencyGain)
        {
            LogMethod();

            TriUShortSimple VoltageGain = new TriUShortSimple { L1 = voltageGain, L2 = voltageGain, L3 = voltageGain };
            TriUShortSimple CurrentGain = new TriUShortSimple { L1 = currentGain, L2 = currentGain, L3 = currentGain };
            TriUShortCompound VoltageCompoundGain = new TriUShortCompound { L12 = voltageGain, L23 = voltageGain, L31 = voltageGain };
            
            var MeasureGains = new GainFactorsVI { Tension = VoltageGain, Current = CurrentGain, CompoundTension = VoltageCompoundGain };
            WriteGains(MeasureGains);

            TriUShortSimple ActivePowerGain = new TriUShortSimple { L1 = powerGain, L2 = powerGain, L3 = powerGain };
            WritePowerGains(ActivePowerGain);

            TriUShortSimple GapGains = new TriUShortSimple { L1 = gapGain, L2 = gapGain, L3 = gapGain };
            WriteGapGains(GapGains);

            WriteNeutralCurrentGains(neutralCurrentGain);
            WriteSynchFrequencyGain(synchronismFrequencyGain);
        }

        public void WriteGains(GainFactorsVI GainFactor)
        {
            LogMethod();

            Modbus.Write((ushort)GainFactors.TENSION, GainFactor.Tension);
            Modbus.Write((ushort)GainFactors.CURRENT, GainFactor.Current);
            Modbus.Write((ushort)GainFactors.COMPOUND_TENSION, GainFactor.CompoundTension);
        }

        public void WritePowerGains(TriUShortSimple PowerGains)
        {
            LogMethod();
            Modbus.Write<TriUShortSimple>((ushort)GainFactors.POWER, PowerGains);
        }

        public void WriteGapGains(TriUShortSimple GapGains)
        {
            LogMethod();
            Modbus.Write<TriUShortSimple>((ushort)GainFactors.PHASE, GapGains);
        }

        public void WriteNeutralCurrentGains(ushort NeutralCurrentGain)
        {
            LogMethod();
            Modbus.Write((ushort)GainFactors.NEUTRAL, NeutralCurrentGain);
        }

        public void WriteSynchFrequencyGain(ushort FrequencyGain)
        {
            LogMethod();
            Modbus.Write<ushort>((ushort)GainFactors.SYNC_FREQUENCY, FrequencyGain);
        }

        public void WriteDigitalOutput(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DIGITAL_OUTPUT, state);
        }

        public void WriteLoad(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CARGA_COMUNICACIONES, state);
        }

        public void CleanAllParameters(CleanParametersRegisters Clean)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Clean, true);
        }

        public void WriteOrderCalibration()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.AUTOCALIBRACION, true);
        }

        public void WriteDisplayLCD(RegistersDisplay DisplayType, bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)DisplayType, state);
        }

        public void WriteLEDS(LEDsRegisters LEDType, bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)LEDType, state);
        }

        public void WriteModelName(string mensaje)
        {
            LogMethod();
            Modbus.WriteString((ushort)Registers.NOMBRE_EQUIPO, mensaje);
        }

        public void WriteCommunicationsSCI(CommunicationsSCI Comunicaciones)
        {
            LogMethod();
            Modbus.Write<CommunicationsSCI>((ushort)Registers.COMUNICACIONES_SCI, Comunicaciones);
        }

        public void WriteCommunicationsSCIDefault()
        {
            LogMethod();
            
            var communications_default = new CommunicationsSCI { protocol = 0, baudRate = 1, parity = 0, bitsDates = 0, bitsStop = 0, periferic = 1 };
            WriteCommunicationsSCI(communications_default);
        }

        public void WritePassword(ushort Password)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONFIG_CONTRASEÑA, Password);
        }

        public void WriteLockPassword(bool Block)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CONFIG_BLOQUEO_CONTRASEÑA, Convert.ToInt16(Block));
        }

        public void WriteTransformationRatio(TransformationRatio relacionTransformacion) 
        {
            LogMethod();
            Modbus.Write<TransformationRatio>(relacionTransformacion);
        }

        public TransformationRatio ReadTransformationRatio()
        {
            LogMethod();
            return Modbus.Read<TransformationRatio>();
        }

        public void WriteMDConfiguration(byte Minutes = 15, bool TipoVentana = false) // Assegurar valors per defecte!!
        {
            LogMethod();
            
            MaximumDemandConfiguration MD = new MaximumDemandConfiguration();
               
            MD.WindowPeriod = Minutes;
            MD.WindowType = Convert.ToUInt16(TipoVentana);

            Modbus.Write<MaximumDemandConfiguration>(MD);
        }
        
        public void WriteAlarm32bits(AlarmConfiguration32 Alarma)
        {
            LogMethod();
            Modbus.Write<AlarmConfiguration32>(Alarma);
        }
        
        public void WriteAlarm16bits(AlarmConfiguration16 Alarm)
        {
            LogMethod();
            Modbus.Write<AlarmConfiguration16>(Alarm);
        }

        public void WriteAutoCalibration(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.AUTOCALIBRACION, state);
        }

        public void WriteConfigurationChannels(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CANALES, value);
        }

        public void WriteAutocalibrationInputsIdeal(InputsAutocalibrationIDEAL Registro, int value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registro, value);
        }

        public void WriteBacnetRegisters(int BacnetID)
        {
            LogMethod();
            Modbus.Retries = 0;
            Modbus.WithTimeOut((m) => { m.WriteInt32((ushort)Registers.BACNET_ID, BacnetID); });
            Modbus.Retries = 3;
        }

        //Si equipo no es Circutor

        public void WriteBacnetConfiguration()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.BACNET_DEVICE, true);
        }

        public void WriteWifiConfig(WifiConfig config)
        {
            LogMethod();
            Modbus.Write<WifiConfig>(config);
        }

        public void WriteDefaultWifiConfig()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.WIFI_DEFAULT, true);
        }

        public string ReadtWifiIpAssigned()
        {
            LogMethod();
            var ip = Modbus.ReadHexString((ushort)Registers.WIFI_IP,2);
            if (ip == "00000000")
                return "0";

            var ips = string.Format("{0}.{1}.{2}.{3}", Convert.ToByte(ip.Substring(0, 2), 16), Convert.ToByte(ip.Substring(2,2), 16), Convert.ToByte(ip.Substring(4, 2), 16), Convert.ToByte(ip.Substring(6, 2), 16));
            return ips;
        }

        public string ReadtWifiStatus()
        {
            LogMethod();
            var status = Modbus.ReadRegister((ushort)Registers.WIFI_STATUS);
            return status.ToString();
        }

        #endregion

        #region Clases

        //public class PropertyVector
        //{
        //    public bool OutReles { get; set; }
        //    public bool OutTransistor { get; set; }
        //    public bool Inputs { get; set; }
        //    public bool Harmonics { get; set; }
        //    public bool THD { get; set; }
        //    public bool PD { get; set; }

        //    public PropertyVector()
        //    {
        //    }

        //    public PropertyVector(ushort value)
        //    {
        //        OutReles = (value & 0x01) == 0x01;
        //        OutTransistor = (value & 0x02)  == 0x02;
        //        Inputs = (value & 0x04) == 0x04;
        //        Harmonics = (value & 0x08)== 0x08;
        //        THD = (value & 0x10) == 0x10;
        //        PD = (value & 0x20) == 0x20;
        //    }

        //    public Int16 ToInt()
        //    {
        //        int valor = 0;

        //        if (OutReles && OutTransistor && Inputs && Harmonics && THD && PD)
        //            valor = (byte)VectorProperty.POR_DEFECTO;

        //        else
        //        {
        //            if (OutReles) valor += (byte)VectorProperty.OUT_RELES;
        //            if (OutTransistor) valor += (byte)VectorProperty.OUT_TRANSISTOR;
        //            if (Inputs) valor += (byte)VectorProperty.INPUTS;
        //            if (Harmonics) valor += (byte)VectorProperty.HARMONICS;
        //            if (THD) valor += (byte)VectorProperty.THD;
        //            if (PD) valor += (byte)VectorProperty.PD;
        //        }

        //        return (Int16)valor;
        //    }
        //}

        #endregion

        #region Estructuras

        public struct PropertyVector
        {
            public bool OutReles { get; set; }
            public bool OutTransistor { get; set; }
            public bool Inputs { get; set; }
            public bool Harmonics { get; set; }
            public bool THD { get; set; }
            public bool PD { get; set; }

            public PropertyVector(ushort value)
            {
                OutReles = (value & 0x01) == 0x01;
                OutTransistor = (value & 0x02) == 0x02;
                Inputs = (value & 0x04) == 0x04;
                Harmonics = (value & 0x08) == 0x08;
                THD = (value & 0x10) == 0x10;
                PD = (value & 0x20) == 0x20;
            }

            public Int16 ToInt()
            {
                int valor = 0;

                if (OutReles && OutTransistor && Inputs && Harmonics && THD && PD)
                    valor = (byte)VectorProperty.POR_DEFECTO;

                else
                {
                    if (OutReles) valor += (byte)VectorProperty.OUT_RELES;
                    if (OutTransistor) valor += (byte)VectorProperty.OUT_TRANSISTOR;
                    if (Inputs) valor += (byte)VectorProperty.INPUTS;
                    if (Harmonics) valor += (byte)VectorProperty.HARMONICS;
                    if (THD) valor += (byte)VectorProperty.THD;
                    if (PD) valor += (byte)VectorProperty.PD;
                }

                return (Int16)valor;
            }

            public string ToHexString()
            {
                return Convert.ToString(ToInt(), 16).ToUpper();                
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.VECTOR_HARDWARE)]
        public struct HardwareVector
        {
            public ushort PowerSupply;
            public ushort InputsNumber;
            public ushort OutputsNumber;
            public ushort Communications;
            public ushort CurrentMeasure;
            public ushort VoltageMeasure;
            public ushort ModelTipo;
            public ushort NoNeeded;
            public int na;

            public PowerSupply Source
            {
                set
                {
                    var Value_Byte = (byte)value;
                    PowerSupply = (ushort)(Value_Byte);
                }

                get
                {
                    return (PowerSupply)((PowerSupply));
                }
            }

            public InputsNumber Inputs
            {
                set
                {
                    var Value_Byte = (byte)value;
                    InputsNumber = (ushort)(Value_Byte);
                }

                get
                {
                    return (InputsNumber)(InputsNumber);
                }
            }

            public OutputsNumber Outputs
            {
                set
                {
                    var Value_Byte = (byte)value;
                    OutputsNumber = (ushort)(Value_Byte);
                }

                get
                {
                    return (OutputsNumber)(OutputsNumber);
                }
            }

            public Communications Comms
            {
                set
                {
                    var Value_Byte = (byte)value;
                    Communications = (ushort)(Value_Byte);
                }

                get
                {
                    return (Communications)(Communications);
                }
            }

            public MeasureI Measure_I
            {
                set
                {
                    var Value_Byte = (byte)value;
                    CurrentMeasure = (ushort)(Value_Byte);
                }

                get
                {
                    return (MeasureI)(CurrentMeasure);
                }
            }

            public MeasureV Measure_V
            {
                set
                {
                    var Value_Byte = (byte)value;
                    VoltageMeasure = (ushort)(Value_Byte);
                }

                get
                {
                    return (MeasureV)(VoltageMeasure);
                }
            }

            public ModelType Model
            {
                set
                {
                    var Value_Byte = (byte)value;
                    ModelTipo = (ushort)(Value_Byte);
                }

                get
                {
                    return (ModelType)(ModelTipo);
                }
            }

            public ushort noNeed
            {
                set
                {                   
                    NoNeeded = value;
                }

                get
                {
                    return NoNeeded;
                }
            }

            public int NotApply
            {
                set
                {
                    na = value;
                }
                get
                {
                    return na;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]                                       //Ens servirà per instanciar totes les variables de MESURA simples i trifàsiques
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.L1)]
        public struct AllVariablesMeasure
        {
            public PhasesVariables Phases;
            public ThreePhaseParameters ThreePhase;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]                                       //Definim les 3 fases simples
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.L1)]
        public struct PhasesVariables
        {
            public PhaseParameters L1;
            public PhaseParameters L2;
            public PhaseParameters L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]                                       //Definim els paràmetres de cada fase simple
        public struct PhaseParameters
        {
            public Int32 voltage;
            public Int32 current;
            public Int32 activepower;
            public Int32 inductivereactivepower;
            public Int32 capacityreactivepower;
            public Int32 apparentpower;
            public Int32 powerfactor;
            public Int32 phicoseno;

            public double Voltage { get { return Convert.ToDouble(voltage) / 10; } }
            public double Current { get { return Convert.ToDouble(current) / 1000; } }
            public double activePower { get { return Convert.ToDouble(activepower); } }
            public double inductiveReactivePower { get { return Convert.ToDouble(inductivereactivepower); } }
            public double capacityReactivePower { get { return Convert.ToDouble(capacityreactivepower); } }
            public double apparentPower { get { return Convert.ToDouble(apparentpower); } }
            public double powerFactor { get { return Convert.ToDouble(powerfactor) / 100; } }
            public double phiCoseno { get { return Convert.ToDouble(phicoseno) / 100;} }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]                                       //Definim els paràmetres trifàsics 
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.TRIFASICA)]
        public struct ThreePhaseParameters
        {
            public Int32 threephaseactivepower;
            public Int32 threephaseinductivereactivepower;
            public Int32 threephasecapacityreactivepower;
            public Int32 threephaseapparentpower;
            public Int32 threephasepowerfactor;
            public Int32 threephasephicoseno;
            public Int32 frequency;
            public Int32 voltageL12;
            public Int32 voltageL23;
            public Int32 voltageL31;
            public Int32 neutralcurrent;

            public double ThreePhaseActivePower { get { return Convert.ToDouble(threephaseactivepower); } }
            public double ThreePhaseInductiveReactivePower { get { return Convert.ToDouble(threephaseinductivereactivepower); } }
            public double ThreePhaseCapacityReactivePower { get { return Convert.ToDouble(threephasecapacityreactivepower); } }
            public double ThreePhaseApparentPower { get { return Convert.ToDouble(threephaseapparentpower); } }
            public double ThreePhasePowerFactor { get { return Convert.ToDouble(threephasepowerfactor) / 100; } }
            public double ThreePhasePhiCoseno { get { return Convert.ToDouble(threephasephicoseno) / 100; } }
            public double Frequency { get { return Convert.ToDouble(frequency) / 100; } }
            public double VoltageL12 { get { return Convert.ToDouble(voltageL12) / 10; } }
            public double VoltageL23 { get { return Convert.ToDouble(voltageL23) / 10; } }
            public double VoltageL31 { get { return Convert.ToDouble(voltageL31) / 10; } }
            public double NeutralCurrent { get { return Convert.ToDouble(neutralcurrent) / 1000; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]                                       //Definim els paràmetres THD Harmonics
        [ModbusLayout(Address = (ushort)Registers.DISTORSION_HARMONICA_TOTAL)]
        public struct TotalHarmonicDistorsion
        {
            public Int32 THDvoltageL1;
            public Int32 THDvoltageL2;
            public Int32 THDvoltageL3;
            public Int32 THDcurrentL1;
            public Int32 THDcurrentL2;
            public Int32 THDcurrentL3;

            public double THDVoltageL1 { get { return Convert.ToDouble(THDvoltageL1) / 10; } }
            public double THDVoltageL2 { get { return Convert.ToDouble(THDvoltageL2) / 10; } }
            public double THDVoltageL3 { get { return Convert.ToDouble(THDvoltageL3) / 10; } }
            public double THDCurrentL1 { get { return Convert.ToDouble(THDcurrentL1) / 10; } }
            public double THDCurrentL2 { get { return Convert.ToDouble(THDcurrentL2) / 10; } }
            public double THDCurrentL3 { get { return Convert.ToDouble(THDcurrentL3) / 10; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.ENERGIA_TRIFASICA_CONSUMIDA)]
        public struct ConsumedTrifasicEnergy
        {
            public Int32 activeEnergy_Kwh;
            public Int32 activeEnergy_wh;
            public Int32 inductiveReactiveEnergy_KvarLh;
            public Int32 inductiveReactiveEnergy_varLh;
            public Int32 capacityReactiveEnergy_KvarCh;
            public Int32 capacityReactiveEnergy_varCh;
            public Int32 apparentEnergy_KVAh;
            public Int32 apparentEnergy_VAh;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.ENERGIA_TRIFASICA_GENERADA)]
        public struct GeneratedTrifasicEnergy
        {
            public Int32 activeEnergy_Kwh;
            public Int32 activeEnergy_wh;
            public Int32 inductiveReactiveEnergy_KvarLh;
            public Int32 inductiveReactiveEnergy_varLh;
            public Int32 capacityReactiveEnergy_KvarCh;
            public Int32 capacityReactiveEnergy_varCh;
            public Int32 apparentEnergy_KVAh;
            public Int32 apparentEnergy_VAh;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.RELACIONES_TRANSFORMACION)]
        public struct TransformationRatio
        {
            public UInt32 primaryVoltage;
            public UInt16 secundaryVoltage;
            public UInt16 primaryCurrent;
            public UInt16 secundaryCurrent;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COMUNICACIONES_SCI)]
        public struct CommunicationsSCI
        {
            public ushort protocol;
            public ushort periferic;
            public ushort baudRate;
            public ushort parity;
            public ushort bitsDates;
            public ushort bitsStop;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIG_ALARMA_32)]
        public struct AlarmConfiguration32
        {
            public uint Hi;
            public uint Lo;           
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIG_ALARMA_16)]
        public struct AlarmConfiguration16
        {
            public ushort code;
            public ushort delay;
            public ushort hysteresis;
            public ushort latch;
            public ushort delayOff;
            public ushort State;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ERROR_VARIABLE_MEDIDA_CALIBRACION)]
        public struct TolerancesParametersCalibration
        {
            public Int32 errorV1;
            public Int32 errorV2;
            public Int32 errorV3;
            public Int32 errorI1;
            public Int32 errorI2;
            public Int32 errorI3;
            public Int32 errorV12;
            public Int32 errorV23;
            public Int32 errorV31;
            public Int32 errorFreq;
            public Int32 errorkW1;
            public Int32 errorkW2;
            public Int32 errorkW3;
            public Int32 errorkvar1;
            public Int32 errorkvar2;
            public Int32 errorkvar3;
            public Int32 errorDesfase1;
            public Int32 errorDesfase2;
            public Int32 errorDesfase3;

            public double errorPorcentajeV1 { get { return Convert.ToDouble(errorV1) / 100; } }
            public double errorPorcentajeV2 { get { return Convert.ToDouble(errorV2) / 100; } }
            public double errorPorcentajeV3 { get { return Convert.ToDouble(errorV3) / 100; } }
            public double errorPorcentajeI1 { get { return Convert.ToDouble(errorI1) / 100; } }
            public double errorPorcentajeI2 { get { return Convert.ToDouble(errorI2) / 100; } }
            public double errorPorcentajeI3 { get { return Convert.ToDouble(errorI3) / 100; } }
            public double errorPorcentajeV12 { get { return Convert.ToDouble(errorV12) / 100; } }
            public double errorPorcentajeV23 { get { return Convert.ToDouble(errorV23) / 100; } }
            public double errorPorcentajeV31 { get { return Convert.ToDouble(errorV31) / 100; } }
            public double errorPorcentajeFreq { get { return Convert.ToDouble(errorFreq) / 100; } }
            public double errorPorcentajekW1 { get { return Convert.ToDouble(errorkW1) / 100; } }
            public double errorPorcentajekW2 { get { return Convert.ToDouble(errorkW2) / 100; } }
            public double errorPorcentajekW3 { get { return Convert.ToDouble(errorkW3) / 100; } }
            public double errorPorcentajekvar1 { get { return Convert.ToDouble(errorkvar1) / 100; } }
            public double errorPorcentajekvar2 { get { return Convert.ToDouble(errorkvar2) / 100; } }
            public double errorPorcentajekvar3 { get { return Convert.ToDouble(errorkvar3) / 100; } }
            public double errorPorcentajeDesfase1 { get { return Convert.ToDouble(errorDesfase1) / 100; } }
            public double errorPorcentajeDesfase2 { get { return Convert.ToDouble(errorDesfase2) / 100; } }
            public double errorPorcentajeDesfase3 { get { return Convert.ToDouble(errorDesfase3) / 100; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)GainFactors.TENSION)]
        public struct GainFactorsVI
        {
            public TriUShortSimple Tension;
            public TriUShortSimple Current;
            public TriUShortCompound CompoundTension;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)GainFactors.POWER)]
        public struct GainFactorsPower
        {
            public TriUShortSimple ActivePower;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)GainFactors.PHASE)]
        public struct GainFactorsPhase
        {
            public TriUShortSimple PhaseAjust;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriUShortSimple
        {
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriUShortCompound
        {
            public ushort L12;
            public ushort L23;
            public ushort L31;
        }


        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIG_MD)]
        public struct MaximumDemandConfiguration
        {
            public ushort WindowPeriod;
            public ushort WindowType;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Sumatories
        {
            public Int32 V1;
            public Int32 V2;
            public Int32 V3;
            public Int32 I1;
            public Int32 I2;
            public Int32 I3;
            public Int32 V12;
            public Int32 V23;
            public Int32 V31;
            public Int32 Freq;
            public Int32 KW1;
            public Int32 KW2;
            public Int32 KW3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.WIFI_CONFIG)]
        public struct WifiConfig
        {
            private ushort enabled;
            private int ssid1;
            private int ssid2;
            private int ssid3;
            private int ssid4;
            private int ssid5;
            private int ssid6;
            private int ssid7;
            private int ssid8;
            private int password1;
            private int password2;
            private int password3;
            private int password4;
            private int password5;
            private int password6;
            private int password7;
            private int password8;

            public ushort Enabled { get { return enabled; } set { enabled = value; } }
            public string SSID
            {
                get
                {
                    byte[] bytes = new byte[32];
                    bytes[0] = ssid1.ToBytes()[0];
                    bytes[1] = ssid1.ToBytes()[1];
                    bytes[2] = ssid1.ToBytes()[2];
                    bytes[3] = ssid1.ToBytes()[3];
                    bytes[4] = ssid2.ToBytes()[0];
                    bytes[5] = ssid2.ToBytes()[1];
                    bytes[6] = ssid2.ToBytes()[2];
                    bytes[7] = ssid2.ToBytes()[3];
                    bytes[8] = ssid3.ToBytes()[0];
                    bytes[9] = ssid3.ToBytes()[1];
                    bytes[10] = ssid3.ToBytes()[2];
                    bytes[11] = ssid3.ToBytes()[3];
                    bytes[12] = ssid4.ToBytes()[0];
                    bytes[13] = ssid4.ToBytes()[1];
                    bytes[14] = ssid4.ToBytes()[2];
                    bytes[15] = ssid4.ToBytes()[3];
                    bytes[16] = ssid5.ToBytes()[0];
                    bytes[17] = ssid5.ToBytes()[1];
                    bytes[18] = ssid5.ToBytes()[2];
                    bytes[19] = ssid5.ToBytes()[3];
                    bytes[20] = ssid6.ToBytes()[0];
                    bytes[21] = ssid6.ToBytes()[1];
                    bytes[22] = ssid6.ToBytes()[2];
                    bytes[23] = ssid6.ToBytes()[3];
                    bytes[24] = ssid7.ToBytes()[0];
                    bytes[25] = ssid7.ToBytes()[1];
                    bytes[26] = ssid7.ToBytes()[2];
                    bytes[27] = ssid7.ToBytes()[3];
                    bytes[28] = ssid8.ToBytes()[0];
                    bytes[29] = ssid8.ToBytes()[1];
                    bytes[30] = ssid8.ToBytes()[2];
                    bytes[31] = ssid8.ToBytes()[3];

                    return ASCIIEncoding.ASCII.GetString(bytes);
                }
                set
                {
                    byte[] data = new byte[32];
                    var valueBytes = ASCIIEncoding.ASCII.GetBytes(value.ToCharArray());

                    for(byte b = 0; b < valueBytes.Length; b++)
                        data[b] = valueBytes[b];

                    ssid1 = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
                    ssid2 = data[4] << 24 | data[5] << 16 | data[6] << 8 | data[7];
                    ssid3 = data[8] << 24 | data[9] << 16 | data[10] << 8 | data[11];
                    ssid4 = data[12] << 24 | data[13] << 16 | data[14] << 8 | data[15];
                    ssid5 = data[16] << 24 | data[17] << 16 | data[18] << 8 | data[19];
                    ssid6 = data[20] << 24 | data[21] << 16 | data[22] << 8 | data[23];
                    ssid7 = data[24] << 24 | data[25] << 16 | data[26] << 8 | data[27];
                    ssid8 = data[28] << 24 | data[29] << 16 | data[30] << 8 | data[31];
                }
            }
            public string Password
            {
                get
                {
                    byte[] bytes = new byte[32];
                    bytes[0] = password1.ToBytes()[0];
                    bytes[1] = password1.ToBytes()[1];
                    bytes[2] = password1.ToBytes()[2];
                    bytes[3] = password1.ToBytes()[3];
                    bytes[4] = password2.ToBytes()[0];
                    bytes[5] = password2.ToBytes()[1];
                    bytes[6] = password2.ToBytes()[2];
                    bytes[7] = password2.ToBytes()[3];
                    bytes[8] = password3.ToBytes()[0];
                    bytes[9] = password3.ToBytes()[1];
                    bytes[10] = password3.ToBytes()[2];
                    bytes[11] = password3.ToBytes()[3];
                    bytes[12] = password4.ToBytes()[0];
                    bytes[13] = password4.ToBytes()[1];
                    bytes[14] = password4.ToBytes()[2];
                    bytes[15] = password4.ToBytes()[3];
                    bytes[16] = password5.ToBytes()[0];
                    bytes[17] = password5.ToBytes()[1];
                    bytes[18] = password5.ToBytes()[2];
                    bytes[19] = password5.ToBytes()[3];
                    bytes[20] = password6.ToBytes()[0];
                    bytes[21] = password6.ToBytes()[1];
                    bytes[22] = password6.ToBytes()[2];
                    bytes[23] = password6.ToBytes()[3];
                    bytes[24] = password7.ToBytes()[0];
                    bytes[25] = password7.ToBytes()[1];
                    bytes[26] = password7.ToBytes()[2];
                    bytes[27] = password7.ToBytes()[3];
                    bytes[28] = password8.ToBytes()[0];
                    bytes[29] = password8.ToBytes()[1];
                    bytes[30] = password8.ToBytes()[2];
                    bytes[31] = password8.ToBytes()[3];

                    return ASCIIEncoding.ASCII.GetString(bytes);
                }
                set
                {
                    byte[] data = new byte[32];
                    var valueBytes = ASCIIEncoding.ASCII.GetBytes(value.ToCharArray());

                    for (byte b = 0; b < valueBytes.Length; b++)
                        data[b] = valueBytes[b];

                    password1 = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
                    password2 = data[4] << 24 | data[5] << 16 | data[6] << 8 | data[7];
                    password3 = data[8] << 24 | data[9] << 16 | data[10] << 8 | data[11];
                    password4 = data[12] << 24 | data[13] << 16 | data[14] << 8 | data[15];
                    password5 = data[16] << 24 | data[17] << 16 | data[18] << 8 | data[19];
                    password6 = data[20] << 24 | data[21] << 16 | data[22] << 8 | data[23];
                    password7 = data[24] << 24 | data[25] << 16 | data[26] << 8 | data[27];
                    password8 = data[28] << 24 | data[29] << 16 | data[30] << 8 | data[31];
                }

            }
        }
        #endregion

        #region Registros

        public enum RegistersDisplay
        {
            ALL_SEGMENTS = 0x0028,
            ICONS_SEGMENTS = 0x0029,
            DIGITS_SEGMENTS = 0x002A,
            UNITS_SEGMENTS = 0x002B,
            EVEN_SEGMENTS = 0x002C,
            ODD_SEGMENTS = 0x002D,
            BACKLIGHT = 0x001E,
        }

        public enum LEDsRegisters
        {
            LED_CPU = 0x000A,
            LED_ALARM = 0x000B,
        }

        public enum Keys
        {
            NO_KEY = 0x0000,
            TECLA_IZQUIERDA = 0x0001,
            TECLA_CENTRAL = 0x0002,
            TECLA_DERECHA = 0x0004,
        }

        public enum InputsState
        {
            OFF = 0X0000,
            ON = 0X0001
        }

        public enum CleanParametersRegisters
        {
            CLEAN_ALL_ENERGY = 0x0834,
            CLEAN_MAXSMINS = 0x0848,
            CLEAN_MD = 0x0853,
            CLEAN_ALL = 0x0898,
        }

        public enum ElectricalVariablesRegisters
        {
            L1 = 0x1000,
            L2 = 0x1010,
            L3 = 0x1020,
            TRIFASICA = 0x1030,
            INTENSIDAD_NEUTRO = 0x1044,
            ENERGIA_TRIFASICA_CONSUMIDA = 0x105E,
            ENERGIA_TRIFASICA_GENERADA = 0x1072,
        }

        public enum GainFactors
        {
            TENSION = 0x9C40,
            CURRENT = 0x9C43,
            COMPOUND_TENSION = 0x9C46,
            POWER = 0x9C49,
            NEUTRAL = 0x9C54,
            PHASE = 0x9CA4,
            SYNC_FREQUENCY = 0x9CAE,
            CURRENT_E2 = 0x9CB8,
            POWER_E2 = 0x9CBB
        }

        public enum InputsAutocalibrationIDEAL
        {
            SUMATORIO_V = 0xA028,
            SUMATORIO_V_COMPUESTA = 0xA02A,
            SUMATORIO_I = 0xA02C,
            SUMATORIO_FREQ = 0xA02E,
            SUMATORIO_POT = 0xA030,

            TOLERANCIA_SUMATORIO_V_PROMEDIO = 0xA032, 
            TOLERANCIA_SUMATORIO_V_MAXMIN = 0xA034,
            TOLERANCIA_SUMATORIO_I_PROMEDIO = 0xA036,
            TOLERANCIA_SUMATORIO_I_MAXMIN = 0xA038,
            TOLERANCIA_SUMATORIO_FREQ_PROMEDIO = 0xA03A,
            TOLERANCIA_SUMATORIO_FREQ_MAXMIN = 0xA03C,
            TOLERANCIA_SUMATORIO_POT_PROMEDIO = 0xA03E,
            TOLERANCIA_SUMATORIO_POT_MAXMIN = 0xA040,

            TOLERANCIA_VALOR_V = 0xA042,
            TOLERANCIA_VALOR_I = 0xA044,
            TOLERANCIA_VALOR_FREQ = 0xA046,
            TOLERANCIA_VALOR_POT = 0xA048,
        }

        [Flags]
        public enum CalibrationState
        {
            INICIO_CALIBRACION = 1,
            FASE_CALIBRACION_1 = 10,
            FASE_CALIBRACION_2 = 20,
            FASE_CALIBRACION_3 = 30,
            CAMBIO_ESCALA_CALIBRACION = 50,
            FINALIZACION_CORRECTA_CALIBRACION = 100,
            ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR = 131,
            ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR = 132,
  
        }

        [Flags]
        public enum ErrorSumatorio
        {
            ERROR_SUMATORIOS_MIN = 1,
            ERROR_SUMATORIOS_PROMEDIO_INFERIOR = 2,
            ERROR_SUMATORIOS_PROMEDIO_SUPERIOR = 4,
            ERROR_SUMATORIOS_MAX = 8
        }

        [Flags]
        public enum ErrorSumatorioType30
        {
            ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR = 1,
            ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR = 2,
        }

        [Flags]
        public enum ErrorVariablesCalibration
        {
            /*ESCALA1 Y ESCALA2*/
            [Description("Variable d'error sumatorios MAXIMOS")]
            VARIABLE_ERROR_SUMATORIO_MAX = 0xA08C,
            [Description("Variable d'error sumatorios MINIMOS")]
            VARIABLE_ERROR_SUMATORIO_MIN = 0xA0F0,
            [Description("Variable d'error sumatorios PROMEDIO SUPERIOR")]
            VARIABLE_ERROR_SUMATORIO_PROMEDIO_SUPERIOR = 0xA154,
            [Description("Variable d'error sumatorios PROMEDIO INFERIOR")]
            VARIABLE_ERROR_SUMATORIO_PROMEDIO_INFERIOR = 0xA156,

            /*MEDIDA*/
            [Description("Variable d'error valor MEDIDO PROMEDIO SUPERIOR")]
            VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_SUPERIOR = 0xA1B8,
            [Description("Variable d'error valor MEDIDO PROMEDIO INFERIOR")]
            VARIABLE_ERROR_VALOR_MEDIDO_PROMEDIO_INFERIOR = 0xA1BA,   
        }

        public enum SumatoriesType
        {
            MAX = 0xA08E,
            MIN = 0xA0F2,
            AVG = 0xA158
        }

        [Flags]
        public enum ErrorParametersCalibration
        {
            [Description("Tension V1")]
            TENSION_V1 = 0x0001,
            [Description("Tension V2")]
            TENSION_V2 = 0x0002,
            [Description("Tension V3")]
            TENSION_V3 = 0x0004,
            [Description("Intensidad I1")]
            CORRIENTE_I1 = 0x0008,
            [Description("Intensidad I2")]
            CORRIENTE_I2 = 0x0010,
            [Description("Intensidad I3")]
            CORRIENTE_I3 = 0x0020,
            [Description("Tension V12")]
            TENSION_V12 = 0x0040,
            [Description("Tension V23")]
            TENSION_V23 = 0x0080,
            [Description("Tension V31")]
            TENSION_V31 = 0x0100,
            [Description("Frecuencia")]
            FREQUENCIA = 0x0200,
            [Description("Potencia activa Kw1")]
            POTENCIA_ACTIVA_KW1 = 0x0400,
            [Description("Potencia activa Kw2")]
            POTENCIA_ACTIVA_KW2 = 0x0800,
            [Description("Potencia activa Kw3")]
            POTENCIA_ACTIVA_KW3 = 0x1000,
            [Description("Potencia reactiva KVAr1")]
            POTENCIA_REACTIVA_KVAr1 = 0x2000,
            [Description("Potencia reactiva KVAr2")]
            POTENCIA_REACTIVA_KVAr2 = 0x4000,
            [Description("Potencia reactiva KVAr3")]
            POTENCIA_REACTIVA_KVAr3 = 0x8000,
            [Description("Desfase linia 1")]
            DESFASE_1 = 0x10000,
            [Description("Desfase linia 2")]
            DESFASE_2 = 0x20000,
            [Description("Desfase linia 3")]
            DESFASE_3 = 0x40000,
            [Description("THD V1")]
            THD_V1 = 0x80000,
            [Description("THD V2")]
            THD_V2 = 0x100000,
            [Description("THD V3")]
            THD_V3 = 0x200000,
            [Description("THD I1")]
            THD_I1 = 0x400000,
            [Description("THD I2")]
            THD_I2 = 0x800000,
            [Description("THD I3")]
            THD_I3 = 0x1000000,
        }

        [Flags]
        public enum VectorProperty
        {
            OUT_RELES = 0x01,
            OUT_TRANSISTOR = 0x02,
            INPUTS = 0x04,
            HARMONICS = 0x08,
            THD = 0x10,
            PD = 0x20,

            POR_DEFECTO = 0xFF,
        }

        //HardwareVector ENUMERADOS        
        public enum PowerSupply
        {
            VAC230 = 0x00,
            VAC400 = 0x01,
            VAC480 = 0x02,
            VCC24_120 = 0x03,     
            VAC85_265 = 0x04,
            VAC127_60Hz = 0x05,

            NULL = 0xFF,
        }

        public enum InputsNumber
        {
            NO_INPUTS = 0x00,
            INPUT1 = 0x01,
            INPUT2 = 0x02,
            INPUT3 = 0x03,

            NULL = 0xFF,
        }

        public enum OutputsNumber
        {
            NO_OUTPUTS = 0x00,
            OUTPUT1 = 0x01,
            OUTPUT2 = 0x02,
            OUTPUT3 = 0x03,

            NULL = 0xFF,
        }

        public enum Communications
        {
            NOCOMMS = 0x00,
            RS232 = 0x01,
            RS485MODBUS = 0x02,
            RS485MODBUSBACNET = 0x03,
            ETHERNET = 0x04,
            NULL = 0xFF,
        }

        public enum MeasureI         
        {
            NONEUTRAL_PRIMARY1A = 0x00,
            NONEUTRAL_PRIMARY5A = 0x01,
            NONEUTRAL_PRIMARY1A5A = 0x02,
            NONEUTRAL_SHUNT5A = 0x03,
            NONEUTRAL_PRIMARI250MA = 0x04,
            NONEUTRAL_333 = 0x05,
            NONEUTRAL_ROGOWSKY = 0x06,

            CALCULATEDNEUTRAL_PRIMARY1A = 0x10,
            CALCULATEDNEUTRAL_PRIMARY5A = 0x11,
            CALCULATEDNEUTRAL_PRIMARY1A5A = 0x12,
            CALCULATEDNEUTRAL_SHUNT5A = 0x13,
            CALCULATEDNEUTRAL_PRIMARI250MA = 0x14,
            CALCULATEDNEUTRAL_333 = 0x15,
            CALCULATEDNEUTRAL_ROGOWSKY = 0x16,

            MEASUREDNEUTRAL_PRIMARY1A = 0x20,
            MEASUREDNEUTRAL_PRIMARY5A = 0x21,
            MEASUREDNEUTRAL_PRIMARY1A5A = 0x22,
            MEASUREDNEUTRAL_SHUNT5A = 0x23,
            MEASUREDNEUTRAL_PRIMARI250MA = 0x24,
            MEASUREDNEUTRAL_333 = 0x25,
            MEASUREDNEUTRAL_ROGOWSKY = 0x26,

            NULL = 0xFF,
        }

        public enum MeasureV
        {
            VAC300 = 0x00,
            VAC110 = 0x01,
            VAC500 = 0x02,

            NULL = 0xFF,
        }

        public enum ModelType
        {
            CVM_MINI2 = 0x01,

            NULL = 0xFF,
        }

        public enum Scale
        {
            E1,
            E2
        }

        public enum Registers
        {
            NOMBRE_EQUIPO = 0x0514,
            NUMERO_SERIE = 0x05AA,
            BACNET_DEVICE = 0x0BE0,
            VERSION_FIRMWARE = 0x05DC,
            VECTOR_HARDWARE = 0x0640,
            PROPIEDADES_MODELO = 0x06A4,
            ESTADO_POTENCIA = 0x07D1,
            CANALES = 0x0BC2,
            DIGITAL_OUTPUT = 0x0014,            
            ESTADO_DIGITAL_INPUT = 0x4E20,
            ESTADO_DIGITAL_OUTPUT = 0xAE21,
            KEYBOARD = 0x4E22,
            RELACIONES_TRANSFORMACION = 0x2710,
            //RELACIONES_TRANSFORMACION_I = 0x2714,
            COEFICIENTES_CO2_CONSUMIDA = 0x2724,
            COEFICIENTES_CO2_GENERADA = 0x2728,
            COEFICIENTES_COIN_CONSUMIDA = 0x272C,
            COEFICIENTES_COIN_GENERADA = 0x2730,
            COMUNICACIONES_SCI = 0x2742,
            CONFIG_MD = 0x274C,
            THD_TIPO = 0x2774,
            FLAG_TEST = 0x2AF8,
            CONFIG_ALARMA_32 = 0x2AF8,
            CONFIG_ALARMA_16 = 0x2AFC,
            RESET = 0x07D0,
            TIPO_SISTEMA = 0x2B5C,
            TIEMPO_DISPLAY = 0x2B5E,
            PERFIL = 0x2B60,
            HARMONICOS = 0x2B62,
            NUMERO_CUADRANTES = 0x2B64,
            CONFIG_INPUT = 0x2B66,
            CONFIG_CONTRASEÑA = 0x2B70,
            CONFIG_BLOQUEO_CONTRASEÑA = 0x2B71,
            CONVENIO_CUADRANTES = 0x2B86,
            BACNET_ID = 0x2EE0,
            BACNET_MAC = 0x2EE2,
            DISTORSION_HARMONICA_TOTAL = 0x1046,
            CONFIGURACION_DEFECTO = 0x0BCC,
            SETUP_DEFECTO = 0x0BB8,
            GAINS_DEFECTO = 0x0BC2,
            CARGA_COMUNICACIONES = 0x0032,
            AUTOCALIBRACION = 0x0FA0,
            ESTADO_AUTOCALIBRACION = 0xA05A,
            ERROR_VARIABLE_MEDIDA_CALIBRACION = 0xA280,
            WIFI_CONFIG = 0x37B1,
            WIFI_DEFAULT = 0x0C1D,
            WIFI_IP = 0x36EA,
            WIFI_STATUS = 0x36E9,
            MAC_ETHERNET = 0x36F0,
            MAC_WIFI = 0x36F9,
            FW_BLUETOOTH = 0x36B9
        };

        #endregion

        public override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}
