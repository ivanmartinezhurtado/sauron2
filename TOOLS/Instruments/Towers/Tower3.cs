﻿using Dezac.Core.Utility;
using Instruments.Utility;
using System;
using System.ComponentModel;
using Instruments.Measure;
using Instruments.IO;
using Newtonsoft.Json;

namespace Instruments.Towers
{
    [InstrumentsVersion(1.06)]
    public class Tower3 : TowerBase<Tower3>, IDisposable, ITower
    {
        private const double VoltageFactorInputMux = 21.4;

        private static Tower3 shared;

        public Tower3()
        {
            if (shared == null)
                shared = this;
        }

        [JsonIgnore]
        public static Tower3 Default
        {
            get
            {
                if (shared == null)
                    shared = new Tower3();

                return shared;
            }
        }

        [Browsable(false)]
        public override double MeasureMultimeter(InputMuxEnum input, MagnitudToMeasure magnitud, int waitTimeMeasure = 500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            var result = base.MeasureMultimeter(input, magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);
            switch (input)
            {
                case InputMuxEnum.IN14:
                case InputMuxEnum.IN13:
                case InputMuxEnum.IN12:
                    result *= VoltageFactorInputMux;
                    break;
            }
            return result;
        }

        public double MeasureWithMultimeter(InputMuxEnum input, MagnitudsMultimeter magnitud, int waitTimeMeasure = 500)
        {
            var result = base.InternalMeasureMultimeter34410A(input, magnitud, waitTimeMeasure);
            switch (input)
            {
                case InputMuxEnum.IN14:
                case InputMuxEnum.IN13:
                case InputMuxEnum.IN12:
                    result *= VoltageFactorInputMux;
                    break;
            }
            return result;
        }

        public void PowerSourceIIIApplyPresetsAndWaitStabilisation(TriLineValue voltage, TriLineValue current, double frequency, TriLineValue pfAngle, TriLineValue gap, double tolerance, int timeOut, bool forceApply = false)
        {
            PowerSourceIII.Tolerance = tolerance;
            PowerSourceIII.TimeOutStabilitation = timeOut;
            PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, frequency, pfAngle, gap, forceApply);
        }

        public void PowerSourceIIIApplyPresets(TriLineValue voltage, TriLineValue current, double frequency, TriLineValue pfAngle, TriLineValue gap, bool forceApply = false)
        {
            LogMethod();
            PowerSourceIII.ApplyPresets(voltage, current, frequency, pfAngle, gap, forceApply);
        }

        public void PowerSourceIIIApplyOff()
        {
            LogMethod();
            PowerSourceIII.ApplyOff();
        }

        public void PowerSourceIIIApplyOffAndWaitStabilisation()
        {
            LogMethod();
            PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        public TriLineValue ReadVoltage()
        {
            LogMethod();
            var result =  PowerSourceIII.ReadVoltage();
            _logger.InfoFormat("PowerSourceIII Type {0}  ReadVoltage -> result = {1}", PowerSourceIII.GetType().Name, result);
            return result;
        }

        public TriLineValue ReadCurrent()
        {
            LogMethod();
            var result = PowerSourceIII.ReadCurrent();
            _logger.InfoFormat("PowerSourceIII Type {0}  ReadCurrent -> result = {1}", PowerSourceIII.GetType().Name, result);
            return result;

        }

        public TriLineValue ReadPowerFactor()
        {
            LogMethod();
            var result= PowerSourceIII.ReadPowerFactor();
            _logger.InfoFormat("PowerSourceIII Type {0}  ReadPowerFactor -> result = {1}", PowerSourceIII.GetType().Name, result);
            return result;

        }

        public TriLineValue ReadDesfase()
        {
            LogMethod();
            var result = PowerSourceIII.ReadDesfase();
            _logger.InfoFormat("PowerSourceIII Type {0}  ReadDesfase -> result = {1}", PowerSourceIII.GetType().Name, result);
            return result;
        }


        public AllVariablesPowerSourceIII ReadAllVariablesPowerSourceIII()
        {
            LogMethod();

            var voltage = PowerSourceIII.ReadVoltage();
            var current = PowerSourceIII.ReadCurrent();
            var pf = PowerSourceIII.ReadPowerFactor();
            var desfase = PowerSourceIII.ReadDesfase();

            var allVar = new AllVariablesPowerSourceIII()
            {
                Voltage = voltage,
                Current = current,
                PowerFactor = pf,
                Desfase = desfase,
            };

            return allVar;
        }

        public struct AllVariablesPowerSourceIII
        {
            public TriLineValue Voltage;
            public TriLineValue Current;
            public TriLineValue PowerFactor;
            public TriLineValue Desfase;
        }

        public CVMB100Tower.VariablesInstantaneas ReadVarsCVMA1000Hi()
        {
            return CvmB100Hi.ReadVariablesInstantaneas();
        }

        public CVMB100Tower.VariablesInstantaneas ReadVarsCVMA1000Lo()
        {
            return CvmB100Low.ReadVariablesInstantaneas();
        }

        public CVMMINITower.AllVariables ReadAllVarsCVMMiniChroma()
        {
            return CvmminiChroma.ReadAllVariables();
        }

        public CVMMINITower.AllVariables ReadAllVarsCVMMiniMTE()
        {
            return CvmminiMTE.ReadAllVariables();
        }

        [JsonIgnore]
        public IOMux IOMUX
        {
            get
            {
                if (mux == null)
                {
                    _logger.InfoFormat("No se ha encontrdo la MUX M9120A de Keysight");
                    _logger.InfoFormat("Inicializmos MUX de I/O con la PCI7296");
                    mux = new IOMux(IO.DO);

                }
                return (IOMux)mux;
            }
            set
            {
                mux = value;
            }
        }
    }
}
