namespace Dezac.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("APP.BIENPROGRAMACIONASISTENCIA")]
    public partial class BIENPROGRAMACIONASISTENCIA
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPROGRAMACION { get; set; }

        public int? NUMBIEN { get; set; }

        [StringLength(50)]
        public string DESCRIPCIONPROGRAMACION { get; set; }

        [Required]
        [StringLength(1)]
        public string ACTIVA { get; set; }

        public DateTime? FECHAULTIMAEJECUCION { get; set; }

        public int? PERIODICIDAD { get; set; }

        public virtual BIEN BIEN { get; set; }
    }
}
