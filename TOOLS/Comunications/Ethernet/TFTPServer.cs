﻿using log4net;
using System;
using System.IO;
using System.Net;
using Tftp.Net;

namespace Comunications.Ethernet
{
    public class TFTPServer: IDisposable
    {
        protected static readonly ILog _logger = LogManager.GetLogger("TFTPServer");

        private string ServerDirectory = Environment.CurrentDirectory;
        private TftpServer server = null;
        public TftpTransferProgress TftpTransferProgress { get; set; }
        public TftpTransferError TftpTransferError { get; set; }
        public string TransferReadFileName { get; set; }
        public string TransferWriteFileName { get; set; }
        public bool IsFinished { get; internal set; }
        public int ServerPort { get; set; }

        public TFTPServer(string serverDirectory, IPAddress serverIP, int serverPort= 69)
        {
            ServerDirectory = serverDirectory;
            ServerPort = serverPort;
            server = new TftpServer(new IPEndPoint(serverIP, serverPort));
            server.OnReadRequest += new TftpServerEventHandler(server_OnReadRequest);
            server.OnWriteRequest += new TftpServerEventHandler(server_OnWriteRequest);
            server.Start();
        }

        public void server_OnWriteRequest(ITftpTransfer transfer, EndPoint client)
        {
            IsFinished = false;

            String file = Path.Combine(ServerDirectory, transfer.Filename);

            if (File.Exists(file))
            //_logger.InfoFormat("[" + transfer.Filename + "] Rejecting Write request from " + client + " for " + transfer.Filename + ": File already exists");
                transfer.Cancel(TftpErrorPacket.FileAlreadyExists);
            else
            //_logger.InfoFormat("[" + transfer.Filename + "] Write request from " + client + " for " + transfer.Filename);
                transfer.Start(new FileStream(file, FileMode.CreateNew));
        }

        public void server_OnReadRequest(ITftpTransfer transfer, EndPoint client)
        {
            IsFinished = false;

            _logger.InfoFormat("[" + transfer.Filename + "] Read request from " + client + " for " + transfer.Filename);

            String path = Path.Combine(ServerDirectory, transfer.Filename);
            FileInfo file = new FileInfo(path);

            //Is the file within the server directory?
            if (!file.FullName.ToLower().StartsWith(ServerDirectory.ToLower()))
            {
                //_logger.InfoFormat("[" + transfer.Filename + "] Denying request because the file is outside the server directory.");
                transfer.Cancel(TftpErrorPacket.AccessViolation);
                return;
            }

            //Does the file exist at all?
            if (!file.Exists)
            {
                //_logger.InfoFormat("[" + transfer.Filename + "] Denying request because the file does not exist.");
                transfer.Cancel(TftpErrorPacket.FileNotFound);
                return;
            }

            //Ok, start the transfer
            Stream str = new FileStream(file.FullName, FileMode.Open);
            _logger.InfoFormat("[" + transfer.Filename + "] Accepting request");
            transfer.OnProgress += new TftpProgressHandler(transfer_OnProgress);
            transfer.OnError += new TftpErrorHandler(transfer_OnError);
            transfer.OnFinished += new TftpEventHandler(transfer_OnFinished);
            transfer.Start(str);
        }

        public void transfer_OnError(ITftpTransfer transfer, TftpTransferError error)
        {
            //_logger.InfoFormat("[" + transfer.Filename + "] Error: " + error);
            TftpTransferError = error;
        }

        public void transfer_OnFinished(ITftpTransfer transfer)
        {
            //_logger.InfoFormat("[" + transfer.Filename + "] Finished.");
            IsFinished = true;
        }

        public void transfer_OnProgress(ITftpTransfer transfer, TftpTransferProgress progress)
        {
            //_logger.InfoFormat("[" + transfer.Filename + "] Progress: " + progress);
            TftpTransferProgress = progress;
        }

        public void Dispose()
        {
            if (server != null)
                server.Dispose();
        }
    }
}
