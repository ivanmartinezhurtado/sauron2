﻿using Comunications.Utility;
using Dezac.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.03)]
    public class REC4 : DeviceBase
    {
        public REC4()
        {
        }

        public REC4(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            //Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        #region HOLDING REGISTERS

        public CAL_REARTH ReadCalREarth()
        {
            LogMethod();
            return Modbus.ReadHolding<CAL_REARTH>();
        }

        public void WriteCalREarth(CAL_REARTH calREarth)
        {
            LogMethod();
            Modbus.Write<CAL_REARTH>(calREarth);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0xC350)]
        public struct CAL_REARTH
        {
            public UInt32 CAL_REARTH_FACTOR_N;
            public Int32 CAL_REARTH_OFFSET_N;
            public UInt32 CAL_REARTH_FACTOR_L1;
            public Int32 CAL_REARTH_OFFSET_L1;
        }

        public void CalibrationOffset(int delFirst, int numMuestras, int numMuestrasMax, int inerval)
        {
            var carREarth = ReadCalREarth();
            _logger.InfoFormat("Factor CAL_REARTH_OFFSET_L1={0}", carREarth.CAL_REARTH_OFFSET_L1);
            _logger.InfoFormat("Factor CAL_REARTH_OFFSET_N={0}", carREarth.CAL_REARTH_OFFSET_N);

            var result = AdjustBase(delFirst, numMuestras, numMuestrasMax, inerval, 0,
            () =>
            {
                WriteREarthAssessment();
                Thread.Sleep(2000);

                var measure = ReadREarth();
                _logger.InfoFormat("READ REARTH_OFFSET N={0}", measure.REARTH_N);
                _logger.InfoFormat("READ REARTH_OFFSET L1={0}", measure.REARTH_L1);

                var REARTH_N = (double)measure.REARTH_N;
                var REARTH_L1 = (double)measure.REARTH_L1;

                if (measure.REARTH_N > 60000)
                    REARTH_N = measure.REARTH_N - 65536;

                if (measure.REARTH_L1 > 60000)
                    REARTH_L1 = measure.REARTH_L1 - 65536;

                var measureList = new List<double>()
                {
                    REARTH_N,
                    REARTH_L1,
                };

                _logger.InfoFormat("CALC REARTH_OFFSET N={0}", measureList[0]);
                _logger.InfoFormat("CALC REARTH_OFFSET L1={0}", measureList[1]);
                return measureList.ToArray();
            },
            (listValues) =>
            {
                _logger.InfoFormat("StadisticList REARTH_OFFSET N={0}", listValues.series[0].ToString());
                _logger.InfoFormat("StadisticList REARTH_OFFSET L1={0}", listValues.series[1].ToString());

                carREarth.CAL_REARTH_OFFSET_N = (int)(carREarth.CAL_REARTH_OFFSET_N - listValues.Average(0));
                carREarth.CAL_REARTH_OFFSET_L1 = (int)(carREarth.CAL_REARTH_OFFSET_L1 - listValues.Average(1));

                _logger.InfoFormat("Factor CAL_REARTH_OFFSET_L1={0}", carREarth.CAL_REARTH_OFFSET_L1);
                _logger.InfoFormat("Factor CAL_REARTH_OFFSET_N={0}", carREarth.CAL_REARTH_OFFSET_N);
                return true;
            });

            WriteCalREarth(carREarth);
        }

        public void CalibrationFactor(int delFirst, int numMuestras, int numMuestrasMax, int inerval, double resistenciaUtil)
        {
            var carREarth = ReadCalREarth();
            _logger.InfoFormat("Factor CAL_REARTH_FACTOR_L1={0}", carREarth.CAL_REARTH_FACTOR_L1);
            _logger.InfoFormat("Factor CAL_REARTH_FACTOR_N={0}", carREarth.CAL_REARTH_FACTOR_N);

            var result = AdjustBase(delFirst, numMuestras, numMuestrasMax, inerval, 0,
            () =>
            {
                WriteREarthAssessment();
                Thread.Sleep(2000);

                var measure = ReadREarth();
                _logger.InfoFormat("READ REARTH_FACTOR N={0}", measure.REARTH_N);
                _logger.InfoFormat("READ REARTH_FACTOR L1={0}", measure.REARTH_L1);

                var REARTH_N = (double)measure.REARTH_N;
                var REARTH_L1 = (double)measure.REARTH_L1;

                if (measure.REARTH_N > 60000)
                    REARTH_N = measure.REARTH_N - 65536;

                if (measure.REARTH_L1 > 60000)
                    REARTH_L1 = measure.REARTH_L1 - 65536;

                var measureList = new List<double>()
                {
                    REARTH_N,
                    REARTH_L1,
                };

                _logger.InfoFormat("CALC REARTH_FACTOR N={0}", measureList[0]);
                _logger.InfoFormat("CALC REARTH_FACTOR L1={0}", measureList[1]);

                return measureList.ToArray();
            },
            (listValues) =>
            {
                _logger.InfoFormat("StadisticList REARTH_FACTOR N={0}", listValues.series[0].ToString());
                _logger.InfoFormat("StadisticList REARTH_FACTOR L1={0}", listValues.series[1].ToString());

                carREarth.CAL_REARTH_FACTOR_N = (uint)(carREarth.CAL_REARTH_FACTOR_N * (resistenciaUtil / listValues.Average(0)));
                carREarth.CAL_REARTH_FACTOR_L1 = (uint)(carREarth.CAL_REARTH_FACTOR_L1 * (resistenciaUtil / listValues.Average(1)));

                _logger.InfoFormat("CAL FACTOR REARTH N={0}", carREarth.CAL_REARTH_FACTOR_N);
                _logger.InfoFormat("CAL FACTOR REARTH L1={0}", carREarth.CAL_REARTH_FACTOR_L1);
                return true;
            });

            WriteCalREarth(carREarth);
        }

        public void WriteMotorLever(MOTOR_LEVEL motorLever)
        {
            LogMethod();
            Modbus.Write<MOTOR_LEVEL>(motorLever);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0xC36E)]
        public struct MOTOR_LEVEL
        {
            public UInt16 MOTOR_LEVER_UP;
            public UInt16 MOTOR_LEVER_DOWN;
        }

        public PARAM ReadParams()
        {
            LogMethod();
            return Modbus.ReadHolding<PARAM>();
        }

        public void WriteParams(PARAM param)
        {
            LogMethod();
            Modbus.Write<PARAM>(param);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address=(ushort)HoldingRegisters.PARAM_COSTUMICE‬)]
        public struct PARAM
        { 
            public UInt16 PARAM_SEC_NUM_ARD;
            public UInt16 PARAM_SCALE_REARTH;
            public UInt16 PARAM_RDO_30MA;
            public UInt16 PARAM_RD_30MA;
            public UInt16 PARAM_RDO_300MA;
            public UInt16 PARAM_RD_300MA;
            public UInt16 PARAM_RDO_CUSTOM;
            public UInt16 PARAM_RD_CUSTOM;
        }

        public int ReadParamCRCFact()
        {
            LogMethod();
            return Modbus.ReadInt32HoldingRegisters((ushort)HoldingRegisters.PARAM_CRC_FACT);
        }

        public void WriteParamCRCFact(int crc)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)HoldingRegisters.PARAM_CRC_FACT, crc);
        }

        public string ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadStringHoldingRegister((ushort)HoldingRegisters.SERIAL_NUMBER, 7);
        }

        public void WriteSerialNumber(string serialNumber)
        {
            LogMethod();
            Modbus.WriteString((ushort)HoldingRegisters.SERIAL_NUMBER, serialNumber);
        }

        public int ReadSerialID()
        {
            LogMethod();
            return Modbus.ReadInt32HoldingRegisters((ushort)HoldingRegisters.SERIAL_ID);
        }

        public void WriteSerialID(int serialID)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)HoldingRegisters.SERIAL_ID, serialID);
        }

        public int ReadFrameNumber()
        {
            LogMethod();
            return Modbus.ReadInt32HoldingRegisters((ushort)HoldingRegisters.FRAME_NUMBER);
        }

        public void WriteFrameNumber(int frameNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)HoldingRegisters.FRAME_NUMBER, frameNumber);
        }

        public UInt16 ReadProductionStatus()
        {
            LogMethod();
            return Modbus.ReadHoldingRegister((ushort)HoldingRegisters.PRODUCTION_STATUS);
        }

        public void WriteProductionStatus(int productionStatus)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)HoldingRegisters.PRODUCTION_STATUS, productionStatus);
        }


        public void WritePARAM_MODEL(UInt16 model)
        {
            LogMethod();
            Modbus.Write((ushort)HoldingRegisters.MODEL, model);
        }

        public UInt16 ReadPARAM_MODEL()
        {
            LogMethod();
            return Modbus.ReadHoldingRegister((ushort)HoldingRegisters.MODEL);
        }

        public void WritePARAM_SUBMODEL(string submodel)
        {
            LogMethod();
            Modbus.WriteString((ushort)HoldingRegisters.SUBMODEL, submodel);
        }

        public string ReadPARAM_SUBMODEL()
        {
            LogMethod();
            return Modbus.ReadStringHoldingRegister((ushort)HoldingRegisters.SUBMODEL, 2);
        }

        public enum HoldingRegisters
        {
            MODEL = 0xD6D9,// 550001, //‭0xD6D9‬,
            SUBMODEL = 0xD6DA,
            PARAM_COSTUMICE = 0xD6DC,
            PARAM_CRC_FACT = 0xEA5E,
            SERIAL_NUMBER = 0xF000,
            SERIAL_ID = 0xF00A,
            FRAME_NUMBER = 0xF040,
            PRODUCTION_STATUS = 0xF060,
        };

        public CAPTURE ReadCapture()
        {
            LogMethod();
            return Modbus.ReadHolding<CAPTURE>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0xFDE8)]
        public struct CAPTURE
        {
            public UInt16 CAPTURE_ID;
            public UInt16 CAPTURE_SIZE;
            public UInt32 CAPTURE_INDEX;
            public UInt16 CAPTURE_VALUE;
        }

        #endregion

        #region INPUT REGISTERS

        public MEASURE ReadMeasure()
        {
            LogMethod();
            return Modbus.Read<MEASURE>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x9C40)]
        public struct MEASURE
        {
            private UInt16 VCPU;
            private UInt16 VMOTOR;
            private UInt16 IMOTOR;
            private UInt16 VMAIN;

            public double V_CPU { get { return VCPU / 1000D; } }
            public double V_MOTOR { get { return (VMOTOR * (VCPU / Math.Pow(2, 12)) / 0.175) / 1000D; } }
            public double I_MOTOR { get { return IMOTOR / 1000D; } }
            public double V_MAIN { get { return VMAIN; } }
        }

        public REARTH ReadREarth()
        {
            LogMethod();
            var measure = Modbus.Read<REARTH>();

            return measure;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x9CA4)]
        public struct REARTH
        {
            public UInt16 PHASE_LOWEST_REARTH;
            public UInt16 REARTH_N;
            public UInt16 REARTH_L1;
        }

        public NUM_REC ReadNum_Rec()
        {
            LogMethod();
            return Modbus.Read<NUM_REC>();
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x9CAE)]
        public struct NUM_REC
        {
            public UInt16 PARTIAL_NUM_REC_RCCB;
            public UInt16 TOTAL_NUM_REC_RCCB;
        }

        public string ReadVersionFirmware()
        {
            LogMethod();
            var version = Modbus.Read<VERSION>();

            return string.Format("{0}.{1}.{2}", version.VER_MAJOR, version.VER_MINOR, version.VER_REVISION);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0xC2EC)]
        public struct VERSION
        {
            public UInt16 VER_MAJOR;
            public UInt16 VER_MINOR;
            public UInt16 VER_REVISION;
        }

        public UInt16 ReadStatDevice()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)InputRegisters.STATE_DEVICE);
        }

        public int ReadIntegritiCRC()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)InputRegisters.INTEGRITY_CRC);
        }

        public enum InputRegisters
        {
            INTEGRITY_CRC = 0xC30A,
            STATE_DEVICE = 0xC2E2,
        }

        #endregion

        #region DISCRETE INPUTS

        public byte ReadMode()
        {
            LogMethod();
            return Modbus.ReadSinlgeInputs((ushort)DigitalInputs.MODE);
        }

        public void ReadAlarms()
        {
            LogMethod();
            var resp = Modbus.ReadMultipleInputs((ushort)DigitalInputs.ALARMS, 7);
            var alarm = new ALARM()
            {
                ALARM_ERROR_PARAM = (resp[0] & 0x01) == 0x01,
                ALARM_UV_VMOTOR_STDY = (resp[0] & 0x02) == 0x02,
                ALARM_UV_VMOTOR_REC = (resp[0] & 0x04) == 0x04,
                ALARM_OC_IMOTOR = (resp[0] & 0x08) == 0x08,
                ALARM_TIMEOUT_RECLOSE = (resp[0] & 0x10) == 0x10,
                ALARM_UV_VMAIN = (resp[0] & 0x20) == 0x20,
                ALARM_UV_VCPU = (resp[0] & 0x40) == 0x40,
            };

            if (alarm.ALARM_ERROR_PARAM)
                TestException.Create().UUT.FIRMWARE.ALARMA("ERROR_PARAM").Throw();

            if (alarm.ALARM_UV_VMOTOR_STDY)
                TestException.Create().UUT.FIRMWARE.ALARMA("ALARM_UV_VMOTOR_STDY").Throw();

            if (alarm.ALARM_UV_VMOTOR_REC)
                TestException.Create().UUT.FIRMWARE.ALARMA("ALARM_UV_VMOTOR_REC").Throw();

            if (alarm.ALARM_OC_IMOTOR)
                TestException.Create().UUT.FIRMWARE.ALARMA("ALARM_OC_IMOTOR").Throw();

            if (alarm.ALARM_TIMEOUT_RECLOSE)
                TestException.Create().UUT.FIRMWARE.ALARMA("ALARM_TIMEOUT_RECLOSE").Throw();

            if (alarm.ALARM_UV_VMAIN)
                TestException.Create().UUT.FIRMWARE.ALARMA("ALARM_UV_VMAIN").Throw();

            if (alarm.ALARM_UV_VCPU)
                TestException.Create().UUT.FIRMWARE.ALARMA("ALARM_UV_VCPU").Throw();
        }

        public struct ALARM
        {
            public bool ALARM_ERROR_PARAM;
            public bool ALARM_UV_VMOTOR_STDY;
            public bool ALARM_UV_VMOTOR_REC;
            public bool ALARM_OC_IMOTOR;
            public bool ALARM_TIMEOUT_RECLOSE;
            public bool ALARM_UV_VMAIN;
            public bool ALARM_UV_VCPU;
        }

        public MOTOR_LEVER ReadMotorInputsStatus()
        {
            LogMethod();
            var resp = Modbus.ReadMultipleInputs((ushort)DigitalInputs.MOTOR_LEVER_ON, 2);
            var motor = new MOTOR_LEVER()
            {
                MOTOR_LEVER_ON = (resp[0] & 0x01) == 0x01,
                MOTOR_LEVER_OFF =(resp[0] & 0x02) == 0x02,
            };

            return motor;
        }

        public struct MOTOR_LEVER
        {
            public bool MOTOR_LEVER_ON;
            public bool MOTOR_LEVER_OFF;
        }

        public byte ReadRccbTripped()
        {
            LogMethod();
            return Modbus.ReadSinlgeInputs((ushort)DigitalInputs.RCCB_TRIPPED);
        }

        public enum DigitalInputs
        {
            ALARMS = 0x927B,
            MODE = 0x8CA0,
            MOTOR_LEVER_ON = 0x8CA1,
            MOTOR_LEVER_OFF = 0x8CA2,
            RCCB_TRIPPED = 0x8CA3
        }

        #endregion

        #region COIL

        public byte[] ReadLeds()
        {
            LogMethod();
            return Modbus.ReadMultipleCoil((ushort)Coil.LED_ARD_ENABLED, 2);
        }    

        public enum Coil
        {
            LED_ARD_ENABLED = 0x9470,
            LED_ARD_STATUS = 0x9471,
            RECLOSE_RCCB = 0x9472,
            REARTH_ASSESSMENT= 0x9473,
            CALIB_DEFAULT = 0x9C3B,
            SETUP_DEFAULT = 0x9C3C,
            DEVICE_RESET = 0x9C3D,
            //UPDATE_MODE = 0x9C3E,
            FACTORY_MODE = 0x9C3A,
        }

        public void WriteLedEnabled(bool status)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.LED_ARD_ENABLED, status);
        }

        public void WriteLedStatus(bool status)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.LED_ARD_STATUS, status);
        }

        public void WriterReCloseRCCB()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.RECLOSE_RCCB, true);
        }

        public void WriteREarthAssessment()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.REARTH_ASSESSMENT, true);
        }

        public void WriteCalibDefault()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.CALIB_DEFAULT, true);
        }

        public void WriteSetupDefault()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.SETUP_DEFAULT, true);
        }

        public void WriteDeviceReset()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.DEVICE_RESET, true);
        }

        public void WriteFactoryMode()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Coil.FACTORY_MODE, true);
        }

        #endregion

        public override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}
