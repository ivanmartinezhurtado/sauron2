namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_FAMILIAPARAM
    {
        public int NUMFAMILIA { get; set; }
        public string IDPARAM { get; set; }
        public string PARAM { get; set; }
        public string IDFASE { get; set; }
    
        public virtual FASE FASE { get; set; }
        public virtual T_FAMILIA T_FAMILIA { get; set; }
    }
}
