﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace AssemblyResolve
{
    public class ReflectionClassVersion
    {
        public Type GetTypeAssemblyFirstClass(string assemblyPath)
        {
            try
            {
                var assembly = Assembly.LoadFrom(assemblyPath);

                var types = from t in assembly.GetTypes()
                            where t.IsClass && !t.ContainsGenericParameters
                            select t;

                return types.FirstOrDefault();
            }catch(Exception ex)
            {
                MessageBox.Show(string.Format("Error en el GetTypeAssemblyFirstClass la clase {0} excepcion por {1}", assemblyPath, ex.Message), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public List<ClassVersion> GetClasses(string assemblyPath)
        {
            var assembly = Assembly.LoadFrom(assemblyPath);
            return GetClasses(assembly);
        }

        public List<ClassVersion> GetClasses(Assembly assembly)
        {
            string typeClass = "";
            try
            {
                var Assemblyversion = new List<ClassVersion>();
                if (assembly != null)
                {
                    var types = from t in assembly.GetTypes()
                                where t.IsClass && !t.ContainsGenericParameters && !t.IsAbstract && t.IsPublic
                                select t;

                    foreach (var type in types)
                    {
                        var methods = type
                            .GetMethods()
                            .Where(m => m.Name == "GetDeviceVersion" || m.Name == "GetTestVersion" || m.Name == "GetInstrumentVersion" || m.Name == "GetActionVersion")
                            .OrderBy(p => p.Name);

                        typeClass = type.Name;
                        Debug.WriteLine(string.Format("Clase: {0} Methodo:{1}", typeClass, methods.FirstOrDefault()));

                        if (methods != null && methods.Count() == 1)
                        {
                            var target = Activator.CreateInstance(type);
                            var version = methods.FirstOrDefault().Invoke(target, null).ToString();
                            var classVwresion = new ClassVersion() { NameClass = type.Name, Version = version };
                            Assemblyversion.Add(classVwresion);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(string.Format("Error en el AssemblyResolve la clase {0} assembly {1} es null", typeClass, assembly), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
                return Assemblyversion;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error en el AssemblyResolve la clase {0} assembly {1} por {2}", typeClass, assembly, ex.Message), "EXCEPCION", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
