﻿using Comunications.Ethernet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ScpClientFinishDescription))]
    public class ScpClientFinish : ActionBase
    {
        /// <summary>
        /// Instance of object SCPClient.
        /// </summary>
        [DefaultValue("SCPClient")]
        [Category("3. Instance")]
        [Description("Nombre de la instancia del objeto SCPClient")]
        public string DeviceName { get; set; }
        public override void Execute()
        {
            var scp = GetVariable(DeviceName) as SCPClient;

            if (scp != null)
                scp.Disconnect();
        }
    }
    class ScpClientFinishDescription : ActionDescription
    {
        public override string Name { get { return "SCPClientFinish"; } }
        public override string Category { get { return "Tools/Ethernet/SCP"; } }
        public override string Description { get { return "Desconecta y libera la instancia del cliente SCP"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
    }
}
