﻿using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.IO
{
    public class IOException : InstrumentException
    {
        public enum Codes
        {
            BUSY,
            INTERNAL_ERROR,
            CANNOT_CONFIG,
            INVALID_PORT,
            READ_ON_WRITE_PORT,
            WRITE_ON_READ_PORT,
            DEVICE_NOT_INSTALLED,
        };

        private static readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Codes.BUSY, "El equipo está ocupado!" },
            { (int)Codes.INTERNAL_ERROR, "Error interno del dispositivo!" },
            { (int)Codes.CANNOT_CONFIG, "Imposible incializar la interfaz!" },
            { (int)Codes.INVALID_PORT, "Puerto inválido!" },
            { (int)Codes.READ_ON_WRITE_PORT, "Intento de leer un puerto de escriptura!" },
            { (int)Codes.WRITE_ON_READ_PORT, "Intento de escribir un puerto de lectura!" },
            { (int)Codes.DEVICE_NOT_INSTALLED, "El dispositivo no esta instalado en esta maquina!" }
        };

        public IOException(Codes code)
            : base((int)code)
        {
        }

        public IOException(Codes code, string message)
            : base((int)code, message)
        {
        }

        public string DecodedMessage
        {
            get
            {
                string message = null;

                messages.TryGetValue(this.Code, out message);

                return message ?? "Error desconocido!";
            }
        }
    }
}
