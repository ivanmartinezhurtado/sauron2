﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Linq;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.09)]
    public class RECMAX : DeviceBase
    {
        public RECMAX()
        {
        }

        public RECMAX(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;
            Modbus.PerifericNumber = periferic;
            Modbus.UseFunction06 = true;
        }

        public override void Dispose()
        {
            Modbus.Dispose();
            base.Dispose();
        }
        public ModbusDeviceSerialPort Modbus { get; internal set; }

        #region Writting operations

        public void WriteSerialNumber(int serialNumber)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, serialNumber);
        }

        public void WriteErrorCode(ushort errorCode)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ERROR_CODE_IDP, errorCode);
        }

        public void WriteFactorsCalibrationConstant(FactorsCalibration factors)
        {
            LogMethod();           
            Modbus.WriteInt32((ushort)Registers.FACTORS_CALIBRATION_CONSTANT_ESC0, factors.pointsScale0);
            Thread.Sleep(500);
            Modbus.WriteInt32((ushort)Registers.FACTORS_CALIBRATION_CONSTANT_ESC1, factors.pointsScale1);
        }

        public struct FactorsCalibration
        {
            public Int32 pointsScale0;
            public Int32 pointsScale1;
        }        

        public void WriteOperatingFrequency(OperatingFrequency operatingFrequency)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OPERATING_FREQUENCY, (ushort)operatingFrequency);
        }
       
        //public void WriteTriggerCurrent(TriggerCurrentSelected triggerCurrent)
        //{
        //    LogMethod();
        //    Modbus.Write((ushort)Registers.TRIGGER_CURRENT_SELECTION, (ushort)triggerCurrent);
        //}

        public void WriteTriggerCurrent(ushort positionTriggerCurrent)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIGGER_CURRENT_SELECTION, positionTriggerCurrent);
        }

        //public void WriteTriggerTime(TriggerTimeSelected positionTriggerTime)
        //{
        //    LogMethod();
        //    Modbus.Write((ushort)Registers.TRIGGER_TIME_DELAY_SELECTION, positionTriggerTime);
        //}

        public void WriteTriggerTime(ushort positionTriggerTime)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIGGER_TIME_DELAY_SELECTION, positionTriggerTime);
        }

        public void WritePrealarmCurrentPercentage(TriggerCurrentAlarmPercentage prealarmSelection)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.PREALARM_CURRENT_PERCENTAGE_SELECTION, (ushort)prealarmSelection);
        }

        public void WriteSecurityTripRelay(RelayLogic logic)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIP_RELAY_SECURITY, (ushort)logic);
        }

        public void WriteSecurityAuxRelay(RelayLogic logic)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.STATUS_RELAY_SECURITY, (ushort)logic);
        }

        public void WriteDifferentialReconnectionSequence(ushort sequence)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DIFERENTIAL_RECONNECTION_SEQUENCE, sequence);
        }

        public void WriteMagnetothermicReconnectionSequence(ushort sequence)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MAGNETOTHERMIC_RECONNECTION_SEQUENCE, sequence);
        }

        public void WriteDeviceModel(DeviceModel model)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DEVICE_MODEL, (ushort)model);
        }

        public void WriteDeviceSubModel(string subModel)
        {
            LogMethod();

            var lowLevel = subModel.Substring(0,2);
            var highLevel = subModel.Substring(2,2);

            Modbus.WriteString((ushort)Registers.SUBMODEL_CHARACTERS_LOW, lowLevel);
            Modbus.WriteString((ushort)Registers.SUBMODEL_CHARACTERS_HIGH, highLevel);
       }

        public void WriteMenuOptionsLocking(params MenuOptions[] menuOptions)
        {
            LogMethod();

            ushort lockResults = 0;

            foreach (MenuOptions option in menuOptions)
                lockResults |= (ushort)option;

            Modbus.Write((ushort)Registers.SETUP_MENU_OPTIONS_LOCKING, lockResults);
        }

        public void WriteKeyboardLocking(params KeyboardLocking[] keyboardOptions)
        {
            LogMethod();

            ushort lockResults = 0;

            foreach (KeyboardLocking option in keyboardOptions)
                lockResults |= (ushort)option;

            Modbus.Write((ushort)Registers.KEYBOARD_LOCKING, lockResults);
        }

        public void WriteSettings(params Settings[] setting)
        {
            LogMethod();

            ushort settingsConfig = 0;

            foreach (Settings option in setting)
                settingsConfig |= (ushort)option;

            Modbus.Write((ushort)Registers.DEVICE_CONFIGURATION, settingsConfig);
        }

        public void WriteCodeParametrization(ushort code)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PARAMETRIZATION_CODE, code);
        }

        public void WriteFirmwareVersion(ushort version)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FIRMWARE_VERSION, version);
        }

        public void WriteExternalControlConfiguration(ExternalControlConfiguration externalControl)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.EXTERNAL_DEVICE_CONTROL, (ushort)externalControl);
        }

        public void FlagTest()
        {
            LogMethod();
            WriteOperatingMode(OperatingMode.TEST);
        }
        public void WriteOperatingMode(OperatingMode mode)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OPERATING_MODE, (ushort)mode);
            //Thread.Sleep(1000);
        }

        //public void WriteCurrentTableParameter(ushort value)
        //{
        //    LogMethod();
        //    Modbus.Write((ushort)Registers.TRIGGER_CURRENT_TABLE, value);
        //}

        public void WriteScaleTable(ScaleTableConfiguration currentScales, TableKind typeTable)
        {
            LogMethod();

            byte position = 0;
            int positionValue = 0;

            foreach (var elemnt in currentScales.ListElemnts)
            {
                positionValue = position << 11;
                var value = positionValue + ((elemnt.X100 ? 1 : 0) << 10) + elemnt.ElementValue;

                if (typeTable == TableKind.CURRENT)
                    WriteCurrentTableElement((ushort)value);
                else if(typeTable == TableKind.TIME)
                    WriteTimeTableElement((ushort)value);
                else if(typeTable == TableKind.SRDC)
                    WriteCustomDiferentialTableElement((ushort)value);
                else if(typeTable == TableKind.SRMC)
                    WriteCustomMagnetoTableElement((ushort)value);

                position++;
            }
        }

        public void WriteCustomScaleTable(ScaleTableConfiguration currentScales, TableKind typeTable)
        {
            LogMethod();

            byte position = 0;
            int positionValue = 0;

            foreach (var elemnt in currentScales.ListElemnts)
            {
                positionValue = position << 11;
                var value = positionValue + ((elemnt.X100 ? 1 : 0) << 10) + elemnt.ElementValue;

                if (typeTable == TableKind.CURRENT)
                    WriteCurrentTableElement((ushort)value);
                else
                    WriteTimeTableElement((ushort)value);

                position++;
            }
        }

        public void WriteCurrentTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TRIGGER_CURRENT_TABLE, value);
        }

        public void WriteCustomDiferentialTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TRIGGER_SRDC_TABLE, value);
        }

        public void WriteTimeTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TRIGGER_TIME_TABLE, value);
        }

        public void WriteCustomMagnetoTableElement(ushort value)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TRIGGER_SRMC_TABLE, value);
        }

        public void WriteTimeTableParameter(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIGGER_TIME_TABLE, value);
        }

        public void WriteDiferentialSequenceTableParameter(byte position, byte value)
        {
            LogMethod();

            var data = (byte)(((position * 8) << 4 ) + value);
            Modbus.Write((ushort)Registers.TRIGGER_SRDC_TABLE, (ushort)data);
        }

        public void WriteMagnetothermicSequenceTableParameter(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.TRIGGER_SRMC_TABLE, value);
        }

        public void WriteOutputs(params Outputs[] output)
        {
            LogMethod();
            ushort outputResult = 0;
            foreach (Outputs item in output)
                outputResult |= (ushort)item;
            Modbus.Write((ushort)Registers.OUTPUT_ACTIVATION, outputResult);
        }

        public void ClearAcknowledge()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.RECEIVED_ACKNOWLEDGES, (ushort)0);
        }

        public void WriteValueV3Reading()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_3_TESTPOINT_ACCESS, (ushort)0x1111);
        }

        public void WriteValueV12Reading()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_12_TESTPOINT_ACCESS, (ushort)0x1111);
        }

        public void WriteValueIEngineReading()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.I_ENGINE_TESTPOINT_ACCESS, (ushort)0x1111);
        }

        public void WritePageErasing(PagesErasing page)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.PAGE_CLEAR, (ushort)page);
        }

        public void WriteCalculateCuadraticPointsOrder(CuadraticPointsCalculOrder cuadraticPoints)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.CALCULATE_CUADRATIC_POINTS, (ushort)cuadraticPoints);
        }

        public void WriteDisplayFirmwareVersion()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DISPLAY_FIRMWARE_VERSION, (ushort)0x1111);
        }

        public void WriteDisplayAndLeds(DisplayLedsStates states)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.DISPLAY_BACKLIGHT, (ushort)states);
        }

        public void WriteBastidorPCB(int bastidor)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.BASTIDOR_NUMBER_PCB, bastidor);
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.BASTIDOR_NUMBER, bastidor);
        }

        public void WriteDefaultCustomization()
        {
            LogMethod();
            WriteExternalControlConfiguration(ExternalControlConfiguration.DEFAULT_CUSTOM);
        }

        #endregion

        #region Reading operations

        public int ReadSerialNumber()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public ushort ReadAcknowledges()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.RECEIVED_ACKNOWLEDGES);
        }

        public int ReadBastidorPCB()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.BASTIDOR_NUMBER_PCB);
        }

        public int ReadBastidor()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.BASTIDOR_NUMBER);
        }

        public DeviceModel ReadDeviceModel()
        {
            LogMethod();
            return (DeviceModel)Modbus.ReadRegister((ushort)Registers.DEVICE_MODEL);
        }

        public string ReadDeviceSubModel()
        {
            LogMethod();

            var low = Modbus.ReadString((ushort)Registers.SUBMODEL_CHARACTERS_LOW, 1);
            var high = Modbus.ReadString((ushort)Registers.SUBMODEL_CHARACTERS_HIGH, 1);

            return low + high;
        }

        public RelayLogic ReadSecurityAuxRelay()
        {
            LogMethod();
            return (RelayLogic)Modbus.ReadRegister((ushort)Registers.STATUS_RELAY_SECURITY);
        }

        public RelayLogic ReadSecurityTripRelay()
        {
            LogMethod();
            return (RelayLogic)Modbus.ReadRegister((ushort)Registers.TRIP_RELAY_SECURITY);
        }

        public TriggerCurrentAlarmPercentage ReadPrealarmCurrentPercentage()
        {
            LogMethod();

            return (TriggerCurrentAlarmPercentage)Modbus.ReadRegister((ushort)Registers.PREALARM_CURRENT_PERCENTAGE_SELECTION);
        }

        public Settings ReadSettings()
        {
            LogMethod();

            return (Settings)Modbus.ReadRegister((ushort)Registers.DEVICE_CONFIGURATION);
        }

        public MenuOptions ReadMenuOptionsLocking()
        {
            LogMethod();

            return (MenuOptions)Modbus.ReadRegister((ushort)Registers.SETUP_MENU_OPTIONS_LOCKING);
        }

        public ushort ReadDifferentialReconnectionSequence()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.DIFERENTIAL_RECONNECTION_SEQUENCE);
        }

        public ushort ReadMagnetothermicReconnectionSequence()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.MAGNETOTHERMIC_RECONNECTION_SEQUENCE);
        }

        public OperatingFrequency ReadOperatingFrequency()
        {
            LogMethod();
            return (OperatingFrequency)Modbus.ReadRegister((ushort)Registers.OPERATING_FREQUENCY);
        }

        public ErrorCodes ReadErrorCode()
        {
            LogMethod();
            return (ErrorCodes) Modbus.ReadRegister((ushort)Registers.ERROR_MESSAGE);
        }

        public Tuple<bool, string> ReadErrorMessage()
        {
            LogMethod();

            var errorTemp = Modbus.ReadRegister((ushort)Registers.ERROR_MESSAGE);

            string dictionaryEntry = string.Empty;
            var error = ErrorMessages.TryGetValue(errorTemp, out dictionaryEntry);
            return new Tuple<bool, string>(error, error ? dictionaryEntry : "Error no especificado");
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();

            var version = Convert.ToInt32(Convert.ToString(Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION), 16)) / 100D;
            
            return version.ToString("N2").Replace(",",".");
        }

        public FactorsCalibration ReadFactorsCalibrationConstants()
        {
            LogMethod();
            return Modbus.Read<FactorsCalibration>(((ushort)Registers.FACTORS_CALIBRATION_CONSTANT_ESC0));            
        }  

        public ushort ReadDeviceStatus()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.DEVICE_STATUS);
        }

        public KeyboardLocking ReadKeyboardLocking()
        {
            LogMethod();       

            return (KeyboardLocking)Modbus.ReadRegister((ushort)Registers.KEYBOARD_LOCKING);
        }

        public Inputs ReadInputStatus()
        {
            LogMethod();
            return (Inputs)Modbus.ReadRegister((ushort)Registers.INPUT_STATUS);
        }

        public RelayState ReadRelayStatus()
        {
            LogMethod();
            return (RelayState)Modbus.ReadRegister((ushort)Registers.RELAY_OUTPUTS_STATUS);
        }     

        public ushort ReadReconnectionStatus()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.RECONNECTION_ENABLING);
        }

        public ushort ReadTotalReconnections()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TOTAL_RECONNECTIONS);
        }

        public ushort ReadPartialDifferentialTriggerReconnections()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.PARTIAL_DIFERENTIAL_TRIGGER_RECONNECTIONS);
        }

        public ushort ReadPartialMagnetothermicTriggerReconnections()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.PARTIAL_MAGNETOTHERMIC_TRIGGER_RECONNECTIONS);
        }

        public ushort ReadTotalDifferentialTriggerReconnections()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TOTAL_DIFERENTIAL_TRIGGER_RECONNECTIONS);
        }

        public ushort ReadParametrizationCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.PARAMETRIZATION_CODE);
        }

        public ushort ReadtotalMagnetothermicTriggerReconnections()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.TOTAL_MAGNETOTHERMIC_TRIGGER_RECONNECTIONS);
        }

        public double ReadLeakCurrent()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.I_LEAK_RMS_VALUE) / 1000D;
        }

        public ushort ReadTriggerCurrent()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.I_TRIGGER_RMS_VALUE);
        }

        public double ReadVoltageV3TestPointValue(int timeWait = 1500) 
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_3_TESTPOINT_ACCESS, (ushort)0x1111);

            Thread.Sleep(timeWait);

            return Modbus.ReadRegister((ushort)Registers.V_3_TESTPOINT_ACCESS) * 0.01D;
        }

        public double ReadVoltageV12TestPointValue()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.V_12_TESTPOINT_ACCESS, (ushort)0x1111);

            Thread.Sleep(1500);

            return Modbus.ReadRegister((ushort)Registers.V_12_TESTPOINT_ACCESS);
        }

        public double ReadCurrentEngineTestPointValue()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.I_ENGINE_TESTPOINT_ACCESS, (ushort)0x1111);

            Thread.Sleep(1500);

            return Modbus.ReadRegister((ushort)Registers.I_ENGINE_TESTPOINT_ACCESS);
        }

        public int ReadCuadraticPoints()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.CUADRATIC_POINTS);
        }

        public ushort ReadVoltage()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.V_NET);
        }

        public ushort ReadDisplayFirmwareVersion()
        {
            LogMethod();

            WriteDisplayFirmwareVersion();

            Thread.Sleep(1000);

            return Modbus.ReadRegister((ushort)Registers.DISPLAY_FIRMWARE_VERSION);
        }

        public int ReadPowerNetCuadraticPoints()
        {
            LogMethod();
            WriteCalculateCuadraticPointsOrder(RECMAX.CuadraticPointsCalculOrder.V_NET);

            Thread.Sleep(3000);

            return ReadCuadraticPoints();
        }

        public string ReadScaleTable(TableKind typeTable)
        {
            LogMethod();

            StringBuilder table = new StringBuilder();

            for (byte pos = 0; pos < 255; pos++)
            {
                var valor = typeTable == TableKind.CURRENT ? ReadCurrentTableElement(pos) : typeTable == TableKind.TIME ? ReadTimeTableElement(pos)
                    : typeTable == TableKind.SRDC ? ReadSRDCTableElement(pos) : typeTable == TableKind.SRMC ? ReadSRMCTableElement(pos) : ReadCurrentTableElement(pos);

                if (valor == 0xFFFF)
                    break;

                table.Append(valor + ";");
            }

            return table.ToString();
        }

        public ushort ReadCurrentTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TRIGGER_CURRENT_TABLE, (ushort)value);

            Thread.Sleep(200);

            var result = Modbus.ReadRegister((ushort)Registers.TRIGGER_CURRENT_TABLE);

            return result;
        }

        public ushort ReadTimeTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TRIGGER_TIME_TABLE, (ushort)value);

            Thread.Sleep(200);

            var result = Modbus.ReadRegister((ushort)Registers.TRIGGER_TIME_TABLE);

            return result;
        }

        public ushort ReadSRDCTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TRIGGER_SRDC_TABLE, (ushort)value);

            Thread.Sleep(200);

            var result = Modbus.ReadRegister((ushort)Registers.TRIGGER_SRDC_TABLE);

            return result;
        }

        public ushort ReadSRMCTableElement(byte scale)
        {
            LogMethod();

            var positionValue = scale << 11;
            var value = positionValue + 0x7FF;

            Modbus.Write((ushort)Registers.TRIGGER_SRMC_TABLE, (ushort)value);

            Thread.Sleep(200);

            var result = Modbus.ReadRegister((ushort)Registers.TRIGGER_SRMC_TABLE);

            return result;
        }

        public int ReadScale1LeakCurrentCuadraticPoints()
        {
            LogMethod();

            WriteCalculateCuadraticPointsOrder(RECMAX.CuadraticPointsCalculOrder.I_LEAK_SCALE_0);

            Thread.Sleep(3000);

            return ReadCuadraticPoints();
        }

        public int ReadScale2LeakCurrentCuadraticPoints()
        {
            LogMethod();

            WriteCalculateCuadraticPointsOrder(RECMAX.CuadraticPointsCalculOrder.I_LEAK_SCALE_1);

            Thread.Sleep(3000);

            return ReadCuadraticPoints();
        }

        #endregion

        #region Enums, structures and dictionarys

        public struct ScaleTableConfiguration
        {
            private string ScaleTableName;

            private List<TableElement> listElements;

            public bool HasElements
            {
                get
                {
                    return listElements.Any();
                }
            }

            public ScaleTableConfiguration(List<string> scaleElements, string scaleTableName)
            {
                ScaleTableName = scaleTableName;

                if (scaleElements.Count() > 32)
                    throw new Exception("Error en la configuración de la tabla de reconexión, el numero de elementos está limitado a 32");

                listElements = new List<TableElement>();

                foreach (string scaleElement in scaleElements)
                {
                    int temporalValue = 0;

                    if (!int.TryParse(scaleElement, out temporalValue))
                        throw new Exception(string.Format("Error al configurar la tabla de escalas de {0}, el valor introducido en la base de datos no es un formato válido", scaleTableName));

                    listElements.Add(new TableElement(temporalValue, (byte)listElements.Count()));
                }
            }

            public List<TableElement> ListElemnts { get { return listElements; } }

            public string listElementsString
            {
                get
                {
                    StringBuilder list = new StringBuilder();

                    foreach (TableElement element in ListElemnts)
                        list.Append(element.GetElementValue.ToString() + ";");
                    return list.ToString();
                }

            }
        }

        public struct TableElement
        {
            private bool x100;
            private ushort elementValue;
            private byte position;

            public TableElement(int value, byte positionValue)
            {
                if (value > 1024)
                {
                    x100 = true;
                    elementValue = (ushort)(value / 100);
                }
                else
                {
                    x100 = false;
                    elementValue = (ushort)value;
                }
                position = positionValue;
            }

            public bool X100
            {
                get
                {
                    return x100;
                }
            }

            public ushort GetElementValue
            {
                get
                {
                    return (ushort)(elementValue * (x100 ? 100 : 1));
                }
            }

            public ushort ElementValue
            {
                get
                {
                    return (ushort)elementValue;
                }
            }

            public byte Position
            {
                get
                {
                    return position;
                }
            }
        }

        public static Dictionary<ushort, string> ErrorMessages = new Dictionary<ushort, string>
        {
            {0x0000, "No hay errores"},
            {0x0001, "El Magnetotermico no ha disparado correctamente"},
            {0x0002, "El valor en el condensador de disparo no alcanza el valor mínimo"},
            {0x0004, "Existe un error en la pagina flash de IDP"},
            {0x0008, "Equipo con parametrizacion incorrecta"},
            {0x0010, "Existe un error en la pagina flash de SETUP"},
            {0x0020, "Medida Idif una vez disparado el equipo"},
            {0x0040, "Toroidal no conectado"}
        };

        public enum TableKind
        {
            TIME,
            CURRENT,
            SRDC,
            SRMC
        }

        public enum OperatingFrequency
        {
            _50Hz = 50,
            _60Hz = 60
        }
                

        public enum TriggerCurrentAlarmPercentage
        {
            ERROR,
            STAT,
            _50,
            _60,
            _70,
            _80,
            MAIN
   }

        public enum RelayLogic
        {
            STANDARD,
            POSITIVE
        }

        public enum DeviceModel
        {
            MP = 0,
            P = 1,
            L = 2,
            LP = 3,
            LPD = 100,
            CVM_2P = 200,
            CVM_4P = 201
        }

        public enum MenuOptions
        {
            NO = 0x0000,
            SRD = 0x0001,
            SRM = 0x0002,
            RST = 0x0004,
            ALAR = 0x0008,
            POL_T = 0x0010,
            POL_S = 0x0020,
            FREQ = 0x0040,
            FACT = 0x0080,
        }

        public enum KeyboardLocking
        {
            NO = 0,
            PROG_ESCALAS_I_T = 1,
            PROG_SETUP = 2
        }

        public enum Settings
        {
            NO_SETTINGS = 0x0000,
            AMOUNT_TERMINALS = 0x0001,
            EXTERNAL_TRIGGER = 0x0002,
            UNDERVOLTAGE_MONITORING = 0x0004,
            OVERVOLTAGE_MONITORING = 0x0008,
            RECONNECTION_SEQUENCE_FIXED = 0x0010
        }

        public enum ExternalControlConfiguration
        {
            TRIGGER = 1,
            REARM = 2,
            RESET = 3,
            DEFAULT_CUSTOM = 4
        }

        public enum OperatingMode
        {
            NORMAL = 1,
            TEST = 2
        }

        public enum ActivationTypeExternalInput
        {
            _3TERMINALS,
            _2TERMINALS
        }

        public enum ActivationModeExternalInput
        {
            LEVEL,
            PULSE
        }

        public enum ValuesYesNo
        {
            NO,
            SI
        }

        public enum Outputs
        {
            [Description("todo apagado")]
            ALL_OFF,
            [Description("rele de disparo")]
            TRIP_RELAY = 0x0001,
            [Description("rele de bloqueo")]
            AUX_RELAY = 0x0002,
            ALL_RELAYS = 0x0003,
            [Description("led verde encendido, led rojo apagado")]
            GREEN_LED = 0x0004,
            [Description("led verde apagado, led rojo apagado")]
            RED_LED = 0x0008,
            ENGINE_UP = 0x0010,
            ENGINE_DOWN = 0x0020,
            TRIGGER_INDUCTOR = 0x0040,
            DISPLAY_ACTIVATION = 0x0080,
            TOROIDAL_TEST = 0x0100,
            REARM = 0X0200,
            [Description("led verde encendido, led rojo encendido")]
            BOTH_LEDS = GREEN_LED | RED_LED
        }

        public enum PagesErasing
        {
            PARAMETRIZATIONS = 0x1111,
            SETUP = 0x2222,
            IDP = 0x3333,
        }

        public enum CuadraticPointsCalculOrder
        {
            V_NET = 0x1111,
            I_LEAK_SCALE_0 = 0x2222,
            I_LEAK_SCALE_1 = 0x3333,
        }

        [Flags]
        public enum Inputs
        {
            ALL_OFF = 0x0000,
            TEST_RESET_KEY = 0x0001,
            MAGNETOTHERMIC = 0x0002,
            EXTERNAL_TRIGGER = 0x0004,
            EXTERNAL_REARM = 0x0008,
            TOROIDAL_NOT_CONNECTED = 0x0010,
            [Description("TECLA RESET")]
            RESET_KEY = 0x0020,
            [Description("TECLA TEST")]
            TEST_KEY = 0x0040,
            [Description("TECLA PROG")]
            PROG_KEY = 0x0080,
            ARD_MANUAL = 0x0100,
            ARD_REMOTE = 0x0200
        }       

        [Flags]
        public enum RelayState
        {
            OFF,
            TRIP = 0x0001,
            LOCK = 0x0010,
        }

        public enum MemoriesMaps
        {
            C10,
            MINI
        }

        public enum DisplayLedsStates
        {
            [Description("BL_GREEN")]
            DISPLAY_ODD_BACKLIGHT_GREEN = 0x0000,
            [Description("BL_RED")]
            DISPLAY_PAIR_BACKLIGHT_RED = 0x0001,
            [Description("ODDS_GREEN")]
            SEGMENTS_PAIRS_COMMONS_ODDS_GREEN = 0X0002,
            [Description("PAIRS_RED")]
            SEGMENTS_ODDS_COMMONS_PAIRS_RED = 0X0003,
            [Description("ANY")]
            NO_SEGMENTS = 0x0004,
            [Description("ALL")]
            ALL_SEGMENTS = 0x0005
        }

        public enum Registers
        {
            SERIAL_NUMBER = 0x0000,
            BASTIDOR_NUMBER = 0x0002,
            BASTIDOR_NUMBER_PCB = 0x0004,
            ERROR_CODE_IDP = 0x0006,
            FACTORS_CALIBRATION_CONSTANT_ESC0 = 0x0007,
            FACTORS_CALIBRATION_CONSTANT_ESC1 = 0x0009,
            OPERATING_FREQUENCY = 0x0010,
            TRIGGER_CURRENT_SELECTION = 0x0011,
            TRIGGER_TIME_DELAY_SELECTION = 0x0012,
            PREALARM_CURRENT_PERCENTAGE_SELECTION = 0x0013,
            TRIP_RELAY_SECURITY = 0x0016,
            STATUS_RELAY_SECURITY = 0x0017,
            DIFERENTIAL_RECONNECTION_SEQUENCE = 0x0018,
            MAGNETOTHERMIC_RECONNECTION_SEQUENCE = 0x0019,
            DEVICE_MODEL = 0x0024,
            SUBMODEL_CHARACTERS_LOW = 0x0025,
            SUBMODEL_CHARACTERS_HIGH = 0x0026,
            SETUP_MENU_OPTIONS_LOCKING = 0x0027,
            KEYBOARD_LOCKING = 0x0028,
            DEVICE_CONFIGURATION = 0x0029,
            PARAMETRIZATION_CODE = 0x002A,
            FIRMWARE_VERSION = 0x003C,
            ERROR_MESSAGE = 0x003D,
            DEVICE_STATUS = 0x003E,
            INPUT_STATUS = 0x003F,
            RELAY_OUTPUTS_STATUS = 0x0040,
            RECONNECTION_ENABLING = 0x0041,
            TOTAL_RECONNECTIONS = 0x0042,
            PARTIAL_DIFERENTIAL_TRIGGER_RECONNECTIONS = 0x0043,
            PARTIAL_MAGNETOTHERMIC_TRIGGER_RECONNECTIONS = 0x0044,
            TOTAL_DIFERENTIAL_TRIGGER_RECONNECTIONS = 0x0045,
            TOTAL_MAGNETOTHERMIC_TRIGGER_RECONNECTIONS = 0x0046,
            I_LEAK_RMS_VALUE = 0x0047,
            I_TRIGGER_RMS_VALUE = 0x0048,
            V_NET = 0x0049,
            EXTERNAL_DEVICE_CONTROL = 0x005C,
            OPERATING_MODE = 0x005D,
            TRIGGER_CURRENT_TABLE = 0x005E,
            TRIGGER_TIME_TABLE = 0x005F,
            TRIGGER_SRDC_TABLE = 0x0060,
            TRIGGER_SRMC_TABLE = 0x0061,
            OUTPUT_ACTIVATION = 0x006A,
            V_3_TESTPOINT_ACCESS = 0x006B,
            V_12_TESTPOINT_ACCESS = 0x006C,
            I_ENGINE_TESTPOINT_ACCESS = 0x006D,
            PAGE_CLEAR = 0x006E,
            CALCULATE_CUADRATIC_POINTS = 0x006F,
            CUADRATIC_POINTS = 0x0070,
            RECEIVED_ACKNOWLEDGES = 0x0072,
            DISPLAY_BACKLIGHT = 0x0073,
            DISPLAY_FIRMWARE_VERSION = 0x0074,            
        } 

        public enum ErrorCodes
        {
            NO_ERROR = 0x0000,
            IDIF_SENSED_CUTTED_CURRENT = 0x0001,
            ERROR_FLASH_SETUP_PAGE= 0x0002,
            INCORRECT_PARAMETRIZATION= 0x0004,
            ERROR_FLASH_IDP_PAGE = 0x0008,
            TRIGGER_CAPACITOR_LOW_LEVEL = 0x0010,
            MAGNETOTHERMIC_INCORRECT_TRIGGERING = 0x0020,
            TOROIDAL_ERROR = 0x0040,
            UNDERVOLTAGE_ALARM = 0x0080,
            OVERVOLTAGE_ALARM = 0x0100,
            DISPLAY_NOT_WORKING_PROPERLY = 0x0200,           
        }
        #endregion
    }
}
