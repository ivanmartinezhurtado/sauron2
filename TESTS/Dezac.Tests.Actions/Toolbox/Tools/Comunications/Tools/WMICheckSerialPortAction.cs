﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Management;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.06)]
    [DesignerAction(Type = typeof(WMICheckSerialPortActionDescription))]
    public class WMICheckSerialPortAction : ActionBase
    {
        [Category("3. Configuration")]
        [Description("La descripción del dispositivo PnP buscado")]
        public string SerialPortToCheck { get; set; }

        public override void Execute()
        {
        
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                testBase.Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TEST_BASE").Throw();

            var sp = string.Empty;
            sp = GetVariable<string>(SerialPortToCheck);

            if (sp == null)
                testBase.Error().SOFTWARE.SECUENCIA_TEST.VARIABLE_NO_ENCONTRADA(SerialPortToCheck).Throw();

            string comportInfo = string.Empty;

            var comList = new List<string>();

            testBase.SamplerWithCancel((p) =>
            {
                var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity");
                foreach (ManagementObject queryObj in searcher.Get())
                    if (queryObj["Caption"] != null)
                    {
                        var comPortFind = queryObj["Caption"].ToString().Replace(SerialPortToCheck, "");
                        var posInit = (comPortFind.IndexOf("(COM"));
                        if (posInit != -1)
                        {
                            var stringSearch = comPortFind.Substring(posInit);
                            comportInfo = stringSearch.Replace(")", "").Replace("(", "");
                            testBase.Logger.InfoFormat("{0} serial port: {1}", SerialPortToCheck, comportInfo);
                            comList.Add(comportInfo);
                        }
                    }

                var instancesNumber = comList.Where(t => t.Equals(sp)).Count();
                if (instancesNumber > 1)
                    testBase.Error().PROCESO.ACTION_EXECUTE.DISPOSITIVO_REPETIDO(string.Format("{0} SERIAL PORT {1}",SerialPortToCheck, sp)).Throw();

                return true;

            }, "", 3, 3000, 1000, false, false);
        }
    }

    public class WMICheckSerialPortActionDescription : ActionDescription
    {
        public override string Name { get { return "WMICheckPort"; } }
        public override string Category { get { return "Tools/Comunications/Tools"; } }
        public override string Description { get{return "Este action hace una comprobación del puerto de serie"; } }
        public override Image Icon { get { return Properties.Resources.Tools_icon; } }
    }
}
