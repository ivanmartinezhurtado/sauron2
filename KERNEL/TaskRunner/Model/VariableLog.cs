﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TaskRunner.Tools;

namespace TaskRunner.Model
{
    public class VariableLog
    {
        public  EventHandler<VariableLogEntry> EntryAdded;

        private Dictionary<string, List<VariableLogEntry>> variablesEntries = new Dictionary<string, List<VariableLogEntry>>();    
        public ReadOnlyDictionary<string, List<VariableLogEntry>> VariablesEntries
        {
            get
            {
                return new ReadOnlyDictionary<string, List<VariableLogEntry>>(variablesEntries);
            }
        }
        public void AddEntry(int instancia, string variable, object value)
        {
            var flattenVariablesEntries = FlattenVariablesEntries(variable, value);
            foreach (var item in flattenVariablesEntries)
            {
                var entry = new VariableLogEntry(instancia, item.Key, item.Value);
                if (!variablesEntries.ContainsKey(item.Key))
                {
                    var items = new List<VariableLogEntry>();
                    items.Add(entry);
                    variablesEntries.Add(item.Key, items);
                }else
                {
                    var list = variablesEntries[item.Key];
                    list.Add(entry);
                    variablesEntries[item.Key] = list;
                }

                if (EntryAdded != null)
                    EntryAdded(this, entry);
            }         
        }

        public void DeleteEntry(int instancia, string variable)
        {
          //  variablesEntries.Remove();
        }

        public ConcurrentDictionary<string, object> FlattenVariablesEntries(string key, object variable)
        {
            ConcurrentDictionary<string, object> flattenVariables = new ConcurrentDictionary<string, object>();
            try
            {
                JsonHelper.DeserializeObjectAndFlattenList(variable).ForEach(property =>
                    {
                        if (string.IsNullOrEmpty(property.Name))
                            flattenVariables.AddOrUpdate($"{key}", property.Value, (k, curValue) => property.Value);
                        else
                            flattenVariables.AddOrUpdate($"{key}.{property.Name}", property.Value, (k, curValue) => property.Value);
                    });
            }
            catch
            {
                flattenVariables.AddOrUpdate(key, variable, (k, curValue) => variable);
            }
            return flattenVariables;
        }
    }

    public class VariableLogEntry
    {
        public int Instancia { get; set; }
        public string Variable { get; set; }
        public string TimeSpan { get; set; }
        public object Value { get; set; }
        public VariableLogEntry(int instancia, string variable, object value)
        {
            Instancia = instancia;
            Variable = variable;
            Value = value;
            TimeSpan = DateTime.Now.ToString("hh:mm:ss.fff tt");
        }
    }

}
