﻿using Instruments.Towers;

namespace Dezac.Tests.Actions.Utility
{
    public class TestScriptBase : TestBase
    {
        private Tower3 tower;
        private Tower1 tower1;

        private bool ownTower3;
        private bool ownTower1;

        public TestScriptBase()
            : base(null)
        {
        }

        protected Tower3 Tower
        {
            get
            {
                if (tower == null)
                {
                    tower = GetInstanceVar<Tower3>();
                    if (tower == null)
                    {
                        ownTower3 = true;
                        tower = new Tower3();
                    }
                }

                return tower;
            }
        }

        protected Tower1 TowerMono
        {
            get
            {
                if (tower1 == null)
                {
                    tower1 = GetInstanceVar<Tower1>();
                    if (tower == null)
                    {
                        ownTower1 = true;
                        tower1 = new Tower1();
                    }
                }

                return tower1;
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            if (ownTower3 && tower != null)
                tower.Dispose();

            if (ownTower1 && tower1 != null)
                tower1.Dispose();
        }
    }
}
