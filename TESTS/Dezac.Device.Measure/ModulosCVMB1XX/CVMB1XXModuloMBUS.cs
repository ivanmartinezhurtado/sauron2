﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.00)]
    public class CVMB1XXModuloMBUS :DeviceBase
    {
        public CVMB1XXModuloMBUS()
        {
        }

        public CVMB1XXModuloMBUS(int port)
        {
            SetPort(port);          
        }

        public void SetPort(int port, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public int ReadNumSerie()
        {
            var response = Modbus.ReadBytes((ushort)Registers.NUM_SERIE,4);

            List<byte> bytes = new List<byte>(4);
            bytes.Add(response[2]);
            bytes.Add(response[3]);
            bytes.Add(response[0]);
            bytes.Add(response[1]);

            int value = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes.ToArray(), 0));

            return value;
        }

        public ushort ReadSatus()
        {
            return Modbus.ReadRegister((ushort)Registers.STATUS);
        }

        public void ReadDateTime()
        {
            var data= Modbus.ReadMultipleHoldingRegister((ushort)Registers.DATE_TIME, 4);
        }

        public enum Registers
        {
            NUM_SERIE = 2001,
            STATUS = 2000,
            DATE_TIME = 0x0000,
        }
    }
}
