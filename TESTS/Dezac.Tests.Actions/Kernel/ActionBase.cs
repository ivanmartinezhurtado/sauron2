﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Services.Logger;
using Dezac.Tests.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace Dezac.Tests.Actions
{
    /// <summary>
    /// Base class for all actions
    /// </summary>
    public abstract class ActionBase : ThreadBase, IActionBase
    {
        private static readonly ILog logger = LogManager.GetLogger("ActionBase");

        protected ILog Logger { get { return logger; } }

        /// <summary>
        /// Step description to comment sequence functionality
        /// </summary>
        [Category("2. Comments")]
        [Description("Sirve para describir la funcinalidad de la secuencia del action")]
        public string Description { get;  set; } //ToDo Miki: Cambiar a Descripción

        /// <summary>
        /// Editable user notes to remark important step features or considerations
        /// </summary>
        [Category("2. Comments")]
        [Description("Notas del usuario editables para remarcar cualquier consideración o función del action")]
        public string Notes { get; set; } //ToDo Miki: Cambiar a notas

        /// <summary>
        /// Action class name 
        /// </summary>
        [Category("1. Action")]
        [Description("Nombre del Action que se está utilizando")]
        public string Name { get { return this.GetType().Name.ToString(); } }

        [Browsable(false)]
        public virtual bool IsGroupAction => false;

        public virtual void PreExecute()
        {
            if (!String.IsNullOrWhiteSpace(Description))
                Logger.Verbose(Description);
            if (!String.IsNullOrWhiteSpace(Notes))
                Logger.Trace(Notes);
        }

        public abstract void Execute();

        public virtual void PostExecute(StepResult stepResult)
        {
        }

        protected SequenceContext Context
        {
            get
            {
                return SequenceContext.Current;
            }
        }

        public string GetActionVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(ActionVersionAttribute), true).FirstOrDefault() as ActionVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }

        public T GetPropertyVar<T>(string name)
        {
            return name.StartsWith("$") ?
                Context.Variables.Get<T>(name.Substring(1)) :
                (T)Convert.ChangeType(name, typeof(T));
        }

        public TestException Error()
        {
            return TestException.Create();
        }

        public ListAdjustValueDef ResolveProperties(List<AdjustValueDefWrapper> ListadjustValueDefWrapper, string TestPoint = "")
        {
            ListAdjustValueDef list = string.IsNullOrEmpty(TestPoint) ? new ListAdjustValueDef() : new ListAdjustValueDef(TestPoint);

            foreach (var adjustValueDefString in ListadjustValueDefWrapper)
                if (adjustValueDefString.GetType() == typeof(AdjustValueDefAvgTol))
                    list.Add(new AdjustValueDef(adjustValueDefString.Name, Convert.ToDouble(VariablesTools.ResolveValue(adjustValueDefString.Average).ToString().Replace(".", ",")), Convert.ToDouble(VariablesTools.ResolveValue(adjustValueDefString.Tolerance).ToString().Replace(".", ",")), Convert.ToDouble(VariablesTools.ResolveValue(adjustValueDefString.MaxVarianza).ToString().Replace(".", ",")), adjustValueDefString.Unidad));
                else
                    list.Add(new AdjustValueDef(adjustValueDefString.Name, Convert.ToDouble(VariablesTools.ResolveValue(adjustValueDefString.Min).ToString().Replace(".", ",")), Convert.ToDouble(VariablesTools.ResolveValue(adjustValueDefString.Max).ToString().Replace(".", ",")), adjustValueDefString.Unidad));

            return list;
        }
    }
}
