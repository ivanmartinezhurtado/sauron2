namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.T_GRUPOPARAMETRIZACION")]
    public partial class T_GRUPOPARAMETRIZACION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_GRUPOPARAMETRIZACION()
        {
            T_VALORPARAMETRO = new HashSet<T_VALORPARAMETRO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMGRUPO { get; set; }

        public int NUMTIPOGRUPO { get; set; }

        [Required]
        [StringLength(20)]
        public string ALIAS { get; set; }

        [Required]
        [StringLength(10)]
        public string IDFASE { get; set; }

        [Required]
        [StringLength(1)]
        public string INOUT { get; set; }

        public int NUMFAMILIA { get; set; }

        public virtual FASE FASE { get; set; }

        public virtual T_FAMILIA T_FAMILIA { get; set; }

        public virtual T_TIPOGRUPOPARAMETRIZACION T_TIPOGRUPOPARAMETRIZACION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_VALORPARAMETRO> T_VALORPARAMETRO { get; set; }
    }
}
