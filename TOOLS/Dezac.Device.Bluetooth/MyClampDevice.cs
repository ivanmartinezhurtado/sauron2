﻿using Dezac.Device.Bluetooth.Utils;
using System;

namespace Dezac.Device.Bluetooth
{
    public class MyClampDevice : BluetoothDevice
    {
        public static Guid SERVICE1_UUID = new Guid("4349522020456C656374726963697479");

        public static Guid CHARACTERISTIC_RMS_CURRENT_UUID = new Guid("4349522020524D532043757272656E74");

        public static Guid SERVICE2_UUID = new Guid("434952204D616F796661637475726572");

        public static Guid CHARACTERISTIC_INPUT_STATUS_UUID = new Guid("43495220496E70757453746174737573");
        public static Guid CHARACTERISTIC_TEST_OUTPUT_UUID = new Guid("434952202054657374204F7574707574");
        public static Guid CHARACTERISTIC_SERIAL_NUMBER_UUID = new Guid("4349522053657269616C4E756D626572");
        public static Guid CHARACTERISTIC_PRODUCT_NUMBER_UUID = new Guid("4349522050726F6475634E756D626572");
        public static Guid CHARACTERISTIC_PRODUCT_STATUS_UUID = new Guid("4349522050726F647563537461747573");
        public static Guid CHARACTERISTIC_INTEGRITY_CRC_UUID = new Guid("43495220496E74656772697479435243");
        public static Guid CHARACTERISTIC_POINTS_CURRENT_UUID = new Guid("43495220506F696E747343757272656E");
        public static Guid CHARACTERISTIC_GAIN_CURRENT_UUID = new Guid("43495220204761696E43757272656E74");
        public static Guid CHARACTERISTIC_OFFSET_CURRENT_UUID = new Guid("434952204F666673657443757272656E");

        public static string DEFAULT_PIN = "159842";

        public static MyClampDevice Find(int timeOut = 10000)
        {
            var result = BluetoothHelper.Scan("MyClamp", timeOut);
            if (result != null)
                return new MyClampDevice { BluetoothAddress = result.BluetoothAddress, PIN = DEFAULT_PIN };

            return null;
        }

        public MyClampDevice()
        {
        }

        public MyClampDevice(string name)
            : base(name)
        {
        }

        public uint ReadRMSCurrentAsync()
        {
            return ReadUInt32(SERVICE1_UUID, CHARACTERISTIC_RMS_CURRENT_UUID);
        }

        public UInt16 ReadInputStatusAsync()
        {
            return ReadUInt16(SERVICE2_UUID, CHARACTERISTIC_INPUT_STATUS_UUID);
        }

        public bool WriteTestOutputAsync(ushort value)
        {
            return Write(SERVICE2_UUID, CHARACTERISTIC_TEST_OUTPUT_UUID, value);
        }

        //public uint ReadSerialNumberAsync()
        //{
        //    return ReadUInt32(SERVICE2_UUID, CHARACTERISTIC_SERIAL_NUMBER_UUID,true);
        //}

        public string ReadSerialNumberAsync()
        {
            return ReadString(SERVICE2_UUID, CHARACTERISTIC_SERIAL_NUMBER_UUID, true);
        }

        public bool WriteSerialNumberAsync(string value)
        {
            return Write(SERVICE2_UUID, CHARACTERISTIC_SERIAL_NUMBER_UUID, value);
        }

        public uint ReadProductNumberAsync()
        {
            return ReadUInt32(SERVICE2_UUID, CHARACTERISTIC_PRODUCT_NUMBER_UUID);
        }

        public bool WriteProductNumberAsync(uint value)
        {
            return Write(SERVICE2_UUID, CHARACTERISTIC_PRODUCT_NUMBER_UUID, value);
        }

        public UInt16 ReadProductStatusAsync()
        {
            return ReadUInt16(SERVICE2_UUID, CHARACTERISTIC_PRODUCT_STATUS_UUID);
        }

        public bool WriteProductStatusAsync(UInt16 value)
        {
            return Write(SERVICE2_UUID, CHARACTERISTIC_PRODUCT_STATUS_UUID, value);
        }

        public uint ReadIntegrityCRCAsync()
        {
            return ReadUInt32(SERVICE2_UUID, CHARACTERISTIC_INTEGRITY_CRC_UUID);
        }

        public uint ReadPointsCurrentAsync()
        {
            return ReadUInt32(SERVICE2_UUID, CHARACTERISTIC_POINTS_CURRENT_UUID);
        }

        public uint ReadGainCurrentAsync()
        {
            return ReadUInt32(SERVICE2_UUID, CHARACTERISTIC_GAIN_CURRENT_UUID);
        }

        public bool WriteGainCurrentAsync(uint value)
        {
            return Write(SERVICE2_UUID, CHARACTERISTIC_GAIN_CURRENT_UUID, value);
        }

        public uint ReadOffsetCurrentAsync()
        {
            return ReadUInt32(SERVICE2_UUID, CHARACTERISTIC_OFFSET_CURRENT_UUID);
        }

        public bool WriteOffsetCurrentAsync(uint value)
        {
            return Write(SERVICE2_UUID, CHARACTERISTIC_OFFSET_CURRENT_UUID, value);
        }
    }
}
