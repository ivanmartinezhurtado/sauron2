﻿using Dezac.Core.Utility;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Visio;
using Dezac.Tests.Visio.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;

namespace Dezac.Tests.Extensions
{
    public static class TestCameraIDSExtension
    {
        public static void StartCameraIDS(this TestBase testBase, CameraIDSSettings settings, int waitTimeInit)
        {
            Stopwatch stopwatch = new Stopwatch();
            var cacheSvc = testBase.GetService<ICacheService>();
            var cameraKey = string.Format("IDS_{0}", settings.SerialNumber);

            CameraIDS camera = null;

            stopwatch.Start();
            if (cacheSvc == null || !cacheSvc.TryGet(cameraKey, out camera))
            {
                camera = new CameraIDS(settings);

                if (settings.StartCapture)
                    camera.Start();

                testBase.Delay(waitTimeInit, "Espera para iniciar la camara");

                if (cacheSvc != null)
                    cacheSvc.Add(cameraKey, camera);
            }
            testBase.SetSharedVariable(cameraKey, camera);
            stopwatch.Stop();
            testBase.Logger.Debug(string.Format("Camara nro.{0} instanciada en {1}s", settings.SerialNumber, stopwatch.ElapsedMilliseconds / 1000.0));

            ModifyCameraIDSParameters(testBase, settings);
        }

        public static void RestartCameraIDS(this TestBase testBase, string serialNumber)
        {
            Stopwatch stopwatch = new Stopwatch();
            CameraIDS camera = null;
            string cameraKey = string.Format("IDS_{0}", serialNumber);
            camera = testBase.GetSharedVariable<CameraIDS>(cameraKey, camera);

            stopwatch.Start();
            testBase.Logger.Info("Realizando un Restart de la camara");
            testBase.RemoveSharedVar(cameraKey);

            var cacheSvc = testBase.GetService<ICacheService>();

            cacheSvc.Remove(cameraKey);

            StartCameraIDS(testBase, camera.CameraSettings, 2000);

            stopwatch.Stop();
            testBase.Logger.Debug(string.Format("Restart de la camara IDS realizado en {0}s.", stopwatch.ElapsedMilliseconds / 1000.0));
        }

        public static void ModifyCameraIDSParameters(this TestBase testBase, CameraIDSSettings settings, int waitTimeInit = 2000)
        {
            Stopwatch stopwatch = new Stopwatch();
            CameraIDS camara = null;

            stopwatch.Start();
            camara = testBase.GetSharedVariable<CameraIDS>(string.Format("IDS_{0}", settings.SerialNumber), camara);

            if (camara == null)
                throw new Exception("No se ha instanciado la camara con el numero de serie especificado");

            camara.CameraSettings = settings;

            var settingsRead = camara.SetSettings();
            LoggerSettings(testBase, settingsRead, settings);

            testBase.SetVariable(string.Format("IDS_{0}", settings.SerialNumber), camara);

            testBase.Delay(waitTimeInit, "Espera para iniciar la camara");

            stopwatch.Stop();
            testBase.Logger.Debug(string.Format("Modificacion de los parametros de la camara realizada en {0}s.", stopwatch.ElapsedMilliseconds / 1000.0));
        }

        public static void LoggerSettings(this TestBase testBase, CameraIDSSettings cameraSettingsRead, CameraIDSSettings cameraSettingsWrite)
        {
            testBase.Logger.Debug("Camera Modify Parameters");

            testBase.Logger.DebugFormat("PixelClock {0} -> {1}", cameraSettingsRead.PixelClock, cameraSettingsWrite.PixelClock);
            testBase.Logger.DebugFormat("Framerate {0} -> {1}", cameraSettingsRead.Framerate, cameraSettingsWrite.Framerate);
            testBase.Logger.DebugFormat("AutoGainShutter {0} -> {1}", cameraSettingsRead.AutoGainShutter, cameraSettingsWrite.AutoGainShutter);
            testBase.Logger.DebugFormat("AutoGain {0} -> {1}", cameraSettingsRead.AutoGain, cameraSettingsWrite.AutoGain);
            testBase.Logger.DebugFormat("Gain {0} -> {1}", cameraSettingsRead.Gain, cameraSettingsWrite.Gain);
            testBase.Logger.DebugFormat("ExposureTime {0} -> {1}", cameraSettingsRead.ExposureTime, cameraSettingsWrite.ExposureTime);
            testBase.Logger.DebugFormat("Saturation {0} -> {1}", cameraSettingsRead.Saturation, cameraSettingsWrite.Saturation);
            testBase.Logger.DebugFormat("Zoom {0} -> {1}", cameraSettingsRead.Zoom, cameraSettingsWrite.Zoom);
            testBase.Logger.DebugFormat("AutoFocusEnable {0} -> {1}", cameraSettingsRead.AutoFocusEnable, cameraSettingsWrite.AutoFocusEnable);
            testBase.Logger.DebugFormat("FocusManual {0} -> {1}", cameraSettingsRead.FocusManual, cameraSettingsWrite.FocusManual);
            testBase.Logger.DebugFormat("BlackLevel {0} -> {1}", cameraSettingsRead.BlackLevel, cameraSettingsWrite.BlackLevel);
            testBase.Logger.DebugFormat("AutoWhiteBalance {0} -> {1}", cameraSettingsRead.AutoWhiteBalance, cameraSettingsWrite.AutoWhiteBalance);
            testBase.Logger.DebugFormat("RedOffset {0} -> {1}", cameraSettingsRead.RedOffset, cameraSettingsWrite.RedOffset);
            testBase.Logger.DebugFormat("BlueOffset {0} -> {1}", cameraSettingsRead.BlueOffset, cameraSettingsWrite.BlueOffset);
            testBase.Logger.DebugFormat("Sharpness {0} -> {1}", cameraSettingsRead.Sharpness, cameraSettingsWrite.Sharpness);
        }

        public static Bitmap SaveImageCameraIDS(this TestBase testBase, string serialNumber, 
            int row1, int column1, int row2, int column2, double resizeFactor)
        {
            CameraIDS camara = null;
            double timeGet = 0.0;
            double timeSave = 0.0;
            Stopwatch stopwatch = new Stopwatch();
            camara = testBase.GetSharedVariable<CameraIDS>(string.Format("IDS_{0}", serialNumber), camara);

            if (camara != null)
            {
                Bitmap image = null;

                testBase.SamplerWithCancel((p) =>
                {
                    try
                    {
                        stopwatch.Start();
                        camara = testBase.GetSharedVariable<CameraIDS>(string.Format("IDS_{0}", serialNumber), camara);

                        testBase.SamplerWithCancel((t) =>
                        {
                            image = camara.GetImage(row1, column1, row2, column2, resizeFactor);
                            timeGet = stopwatch.ElapsedMilliseconds / 1000.0;

                            if (image != null)
                            {
                                //image.Save(fileName, ImageFormat.Png);
                                timeSave = (stopwatch.ElapsedMilliseconds - timeGet) / 1000.0;
                                return true;
                            }
                            else
                                throw new Exception("No se ha podido obtener una imagen de la camara IDS al realizar un GetImage");

                        }, "", 2, 500, 100, false, false);

                        testBase.Logger.Debug(string.Format("Imagen capturada con exito. Tiempo captura {0}s, Tiempo Guardado {1}s", timeGet, timeSave));
                        return true;
                    }
                    catch (Exception e)
                    {
                        testBase.Logger.Warn($"{e.Message} {e.StackTrace}");

                        RestartCameraIDS(testBase, serialNumber);                
                        return false;
                    }                  
                }, "No se ha podido recuperar la camara", 2, 0, 0);

                var imageCloned = ExtensionsTools.Clone(image);

                image.Dispose();
                image = null;

                return imageCloned;
            }
            else
                throw new Exception(string.Format("No se ha instanciado una camara IDS para realizar un SaveImage"));
        }

        public static List<string> SaveSequenceCameraIDS(this TestBase testBase, string serialNumber, string saveName, int numberFrames, int timeBetweenFrames)
        {
            CameraIDS camara = null;
            camara = testBase.GetSharedVariable<CameraIDS>(string.Format("IDS_{0}", serialNumber), camara);
            if (camara != null)
            {
                Directory.CreateDirectory(testBase.PathImageSave);

                List<string> fileNames = new List<string>();

                for (int i = 0; i < numberFrames; i++)
                {
                    string fileName = Path.Combine(testBase.PathImageSave, string.Format("{0}_{1}.png", saveName, i.ToString()));
                    if (File.Exists(fileName))
                        File.Delete(fileName);
                    fileNames.Add(fileName);
                }

                Stopwatch imageStopwatch = new Stopwatch();

                Dictionary<string, System.Drawing.Image> listImage = new Dictionary<string, System.Drawing.Image>();

                foreach (var name in fileNames)
                {
                    testBase.SamplerWithCancel((p) =>
                    {                 
                        imageStopwatch.Start();

                        var image = camara.GetImage();

                        if (image != null)
                        {
                            listImage.Add(name, image);
                            return true;
                        }
                        else
                            throw new Exception("No se ha podido obtener una imagen de la camara IDS al realizar un GetImage");
                    }, "", 2, 500, 100, false, false);

                    imageStopwatch.Stop();

                    testBase.Logger.DebugFormat(String.Format("SaveSequence: Tiempo de captura de imagen {0} : {1} ms", name, imageStopwatch.ElapsedMilliseconds.ToString()));

                    if ((timeBetweenFrames - (int)imageStopwatch.ElapsedMilliseconds) > 0)
                        Thread.Sleep(timeBetweenFrames - (int)imageStopwatch.ElapsedMilliseconds);
                    else
                        testBase.Logger.WarnFormat(string.Format("SaveSequence: Atención, la captura no ha podido comenzar en el tiempo especificado. Tiempo entre frames especificado: {0} ms. Retraso: {1} ms", 
                            timeBetweenFrames.ToString(), ((int)imageStopwatch.ElapsedMilliseconds - timeBetweenFrames).ToString()));

                    imageStopwatch.Restart();
                }

                imageStopwatch.Restart();
                imageStopwatch.Start();

                foreach (var selImage in listImage)
                    selImage.Value.Save(selImage.Key, ImageFormat.Png);

                imageStopwatch.Stop();

                testBase.Logger.DebugFormat(String.Format("SaveSequence: Tiempo de Guardado de la secuencia en local: {0} ms", imageStopwatch.ElapsedMilliseconds.ToString()));

                return fileNames;
            }
            else
                throw new Exception(string.Format("No se ha instanciado una camara IDS para realizar un SaveImage"));
        } 

        public static DialogResult ViewCameraIDSForm(this TestBase testBase, string serialNumber, string FolderDeviceName, string ImageName, bool activeSave = false)
        {
            CameraIDS camara = null;
            camara = testBase.GetSharedVariable<CameraIDS>(string.Format("IDS_{0}", serialNumber), camara);
            if (camara != null)
            {
                var camaraResult = testBase.Shell.ShowDialog("Test de camera", () =>
                {
                    var camaraView = new CameraIDSView(camara, "EQUIPO", "PATRON", string.Format("{0}\\{1}\\{2}.png", testBase.PathCameraImages.TrimEnd('\\'), FolderDeviceName, ImageName));
                    camaraView.AllowSaveCameraImage = activeSave;
                    camaraView.SaveImagePath = testBase.PathImageSave;
                    camaraView.SaveName = string.Format("{0}_{1}_{2}.png", ImageName, DateTime.Now.ToShortDateString().Replace("/", ""), DateTime.Now.ToLongTimeString().Replace(":", ""));
                    return camaraView;
                }, MessageBoxButtons.YesNo, "Comprobar la imagen de la Camara como en la imagen patrón", 2000);

                testBase.Resultado.Set(string.Format("{0}_{1}", ImageName, "USER"), camaraResult == DialogResult.Yes ? "OK" : "FAIL", ParamUnidad.SinUnidad);

                return camaraResult;
            }
            else
                throw new Exception(string.Format("No se ha instanciado una camara IDS para realizar un ViewCameraIDSForm"));
        }
    }
}
