﻿using Comunications.Message;
using Comunications.Protocolo;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;

namespace Instruments.Auxiliars
{
    [InstrumentsVersion(1.00)]
    public class DielectricStrengthTester : InstrumentsAuxiliarBase, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(DielectricStrengthTester));

        public enum PrivateCommands
        {
            SET_INIT = Commands.USER_DEFINED_COMMANDS + 1,
            READ_ERROR,
            LOCK_KEYBOARD,
            UNLOCK_KEYBOARD,
            STOP_TEST,
            START_TEST,
            REMOTE_MODE,
            LOCAL_MODE,
            READ_TEST_RESULT,
            DELETE_STEP,
            READ_STEP_PROPERTIES,
            SET_AC_TEST_VOLTAGE,
            SET_AC_TEST_CURRENT_HIGH,
            SET_AC_TEST_CURRENT_LOW,
            SET_AC_TEST_TIME,
            SET_AC_OUTPUT_CHANNEL_HIGH,
            SET_AC_OUTPUT_CHANNEL_LOW,
            SET_AC_LIMIT_LOW,
            SET_AC_LIMIT_ARC,
            SET_AC_LIMIT_REAL,
            SET_AC_TIME_RAMP,
            SET_AC_TIME_FALL,
            SET_DC_TEST_VOLTAGE,
            SET_DC_TEST_CURRENT_HIGH,
            SET_DC_TEST_CURRENT_LOW,
            SET_DC_TEST_TIME,
            SET_DC_OUTPUT_CHANNEL_HIGH,
            SET_DC_OUTPUT_CHANNEL_LOW,
            SET_DC_LIMIT_LOW,
            SET_DC_LIMIT_ARC,
            SET_DC_TIME_RAMP,
            SET_DC_TIME_FALL,
            SET_IR_TEST_RESISTANCE_LOW,
            SET_IR_TEST_RESISTANCE_HIGH,
            SET_IR_TEST_VOLTAGE,
            SET_IR_TEST_TIME,
            SET_IR_TIME_RAMP,
            SET_IR_TIME_FALL,
            SET_IR_OUTPUT_CHANNEL_HIGH,
            SET_IR_OUTPUT_CHANNEL_LOW,
            READ_TEST_STATE,
            READ_TEST_VOLTAGE_RESULT,
            READ_TEST_VOLTAGE_RESULT_STEP,
            READ_TEST_CURRENT_RESULT,
            READ_TEST_CURRENT_RESULT_STEP,
            READ_TEST_TIME_RESULT,
            READ_TEST_STEPS,
            READ_TEST_STEP_MODE,
            RESET,

        };

        public enum TestResult
        {
            PASS,
            FAIL,
            FAIL_STEP_1,
            FAIL_STEP_2,
            FAIL_STEP_3,
            FAIL_STEP_4,
            FAIL_STEP_5,
            FAIL_STEP_6,
            FAIL_STEP_7,
            FAIL_STEP_8
        }

        public enum TestStatus
        {
            RUNNING,
            STOPPED
        }

        public Dictionary<PrivateCommands, string> commandStrings = new Dictionary<PrivateCommands, string>(){
            {PrivateCommands.DELETE_STEP, "SOUR:SAFE:STEP{0}:DEL"},
            {PrivateCommands.LOCAL_MODE, "SYST:LOCK:REL"},
            {PrivateCommands.LOCK_KEYBOARD, "SYST:KLOC ON"},
            {PrivateCommands.READ_ERROR, "SYST:ERR ?"},
            {PrivateCommands.READ_STEP_PROPERTIES, "SOUR:SAFE:STEP{0}:SET ?"},
            {PrivateCommands.READ_TEST_RESULT, "SOUR:SAFE:RES:ALL:JUDG ?"},
            {PrivateCommands.READ_TEST_STATE, "SOUR:SAFE:STAT ?"},
            {PrivateCommands.READ_TEST_STEP_MODE, "SOUR:SAFE:STEP{0}:MODE ?"},
            {PrivateCommands.REMOTE_MODE, "SYST:LOCK:REQuest ?"},
            {PrivateCommands.SET_AC_TEST_CURRENT_HIGH, "SOUR:SAFE:STEP{0}:AC:LIM:HIGH {1}"},
            {PrivateCommands.SET_AC_TEST_CURRENT_LOW, "SOUR:SAFE:STEP{0}:AC:LIM:LOW {1}"},
            {PrivateCommands.SET_AC_TEST_TIME, "SOUR:SAFE:STEP{0}:AC:TIME:TEST {1}"},
            {PrivateCommands.SET_AC_TEST_VOLTAGE, "SOUR:SAFE:STEP{0}:AC:LEV {1}"},
            {PrivateCommands.SET_DC_TEST_CURRENT_HIGH, "SOUR:SAFE:STEP{0}:DC:LIM:HIGH {1}"},
            {PrivateCommands.SET_DC_TEST_CURRENT_LOW, "SOUR:SAFE:STEP{0}:DC:LIM:LOW {1}"},
            {PrivateCommands.SET_DC_TEST_TIME, "SOUR:SAFE:STEP {0}:DC:TIME:TEST {1}"},
            {PrivateCommands.SET_DC_TEST_VOLTAGE, "SOUR:SAFE:STEP{0}:DC:LEV {1}"},
            {PrivateCommands.UNLOCK_KEYBOARD, "SYST:KLOC OFF"},
            {PrivateCommands.RESET, "*RST"},
            {PrivateCommands.READ_TEST_VOLTAGE_RESULT, "SOUR:SAFE:RES:ALL:OMET ?"},
            {PrivateCommands.READ_TEST_VOLTAGE_RESULT_STEP, "SOUR:SAFE:RES:STEP{0}:OMET ?"},
            {PrivateCommands.READ_TEST_CURRENT_RESULT, "SOUR:SAFE:RES:ALL:MMET ?"},
            {PrivateCommands.READ_TEST_CURRENT_RESULT_STEP, "SOUR:SAFE:RES:STEP{0}:MMET ?"},
            {PrivateCommands.READ_TEST_TIME_RESULT, "SOUR:SAFE:RES:ALL:TIME:TEST ?"},
            {PrivateCommands.READ_TEST_STEPS, "SOUR:SAFE:SNUM ?"},
            {PrivateCommands.STOP_TEST, "SOUR:SAFE:STOP"},
            {PrivateCommands.START_TEST, "SOUR:SAFE:STAR"},
            {PrivateCommands.SET_AC_OUTPUT_CHANNEL_HIGH, "SOUR:SAFE:STEP{0}:AC:CHAN(@({1}))"},
            {PrivateCommands.SET_DC_OUTPUT_CHANNEL_HIGH, "SOUR:SAFE:STEP{0}:DC:CHAN(@({1}))"},
            {PrivateCommands.SET_AC_OUTPUT_CHANNEL_LOW, "SOUR:SAFE:STEP{0}:AC:CHAN:LOW(@({1}))"},
            {PrivateCommands.SET_DC_OUTPUT_CHANNEL_LOW, "SOUR:SAFE:STEP{0}:DC:CHAN:LOW(@({1}))"},
            {PrivateCommands.SET_AC_LIMIT_LOW, "SOUR:SAFE:STEP{0}:AC:LIM:LOW {1}"},
            {PrivateCommands.SET_AC_LIMIT_ARC, "SOUR:SAFE:STEP{0}:AC:LIM:ARC:LEV {1}"},
            {PrivateCommands.SET_AC_LIMIT_REAL, "SOUR:SAFE:STEP{0}:AC:LIM:REAL:HIGH {1}"},
            {PrivateCommands.SET_AC_TIME_RAMP, "SOUR:SAFE:STEP{0}:AC:TIME:RAMP {1}"},
            {PrivateCommands.SET_AC_TIME_FALL, "SOUR:SAFE:STEP{0}:AC:TIME:FALL {1}"},
            {PrivateCommands.SET_DC_LIMIT_LOW, "SOUR:SAFE:STEP{0}:DC:LIM:LOW {1}"},
            {PrivateCommands.SET_DC_LIMIT_ARC, "SOUR:SAFE:STEP{0}:DC:LIM:ARC:LEV {1}"},
            {PrivateCommands.SET_DC_TIME_RAMP, "SOUR:SAFE:STEP{0}:DC:LIM:TIME:RAMP {1}"},
            {PrivateCommands.SET_DC_TIME_FALL, "SOUR:SAFE:STEP{0}:DC:LIM:TIME:FALL {1}"},
            {PrivateCommands.SET_IR_TEST_RESISTANCE_LOW, "SOUR:SAFE:STEP{0}:IR:LIM:LOW {1}"},
            {PrivateCommands.SET_IR_TEST_RESISTANCE_HIGH, "SOUR:SAFE:STEP{0}:IR:LIM:HIGH {1}"},
            {PrivateCommands.SET_IR_TEST_VOLTAGE, "SOUR:SAFE:STEP{0}:IR:LEV {1}"},
            {PrivateCommands.SET_IR_TEST_TIME, "SOUR:SAFE:STEP{0}:IR:TIME:TEST {1}"},
            {PrivateCommands.SET_IR_TIME_RAMP, "SOUR:SAFE:STEP{0}:IR:TIME:RAMP {1}"},
            {PrivateCommands.SET_IR_TIME_FALL, "SOUR:SAFE:STEP{0}:IR:TIME:FALL {1}"},
            {PrivateCommands.SET_IR_OUTPUT_CHANNEL_HIGH, "SOUR:SAFE:STEP{0}:IR:CHAN(@({1}))"},
            {PrivateCommands.SET_IR_OUTPUT_CHANNEL_LOW, "SOUR:SAFE:STEP{0}:IR:CHAN:LOW(@({1}))"},
        };

        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private SerialPort sp;
        private byte _port = 10;

        private const string APPLY_CONFIG_COMMAND = "OUT 1";
        private const string APPLY_OFF_COMMAND = "OUT 0";
        private const string RESET_COMMAND = "*RST";
        private const string READ_STATUS_COMMAND = "STT?";
        private const string SET_INIT_COMMAND = "ADR {0:00}";

        public DielectricStrengthTester()
           : base(logger)
        { }
            
        public DielectricStrengthTester(byte port)
            : base(logger)
        {
            InitializeComponent();
            PortCom = port;

            protocol = new ASCIIProtocol() { CaracterFinTx = "\r\n", CaracterFinRx = "\r\n" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), logger);
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();            
            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.RtsEnable = true;
            sp.DtrEnable = true;
            sp.PortName = "COM" + _port.ToString();
        }

        public TestStatus ReadTestState()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_STATE, true,  string.Empty);

            var result = SendLocalCommand(command);

            return result.ToUpper() == "RUNNING" ? TestStatus.RUNNING : TestStatus.STOPPED;
        }

        public TestResult ReadTestResult()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_RESULT, true, string.Empty);

            var result = SendLocalCommand(command);

            var resultBySteps = result.Split(',');

            var FinalResult = "116";

            for (int i = 0; i < resultBySteps.Length; i++)
                if (resultBySteps[i] != "116" && FinalResult == "116")  
                    FinalResult = i.ToString();

            switch(FinalResult)
            {
                case "116":
                    return TestResult.PASS;
                case "0":
                    return TestResult.FAIL_STEP_1;
                case "1":
                     return TestResult.FAIL_STEP_2;
                case "2":
                    return TestResult.FAIL_STEP_3;
                case "3":
                    return TestResult.FAIL_STEP_4;
                case "4":
                    return TestResult.FAIL_STEP_5;
                case "5":
                    return TestResult.FAIL_STEP_6;
                case "6":
                    return TestResult.FAIL_STEP_7;
                case "7":
                    return TestResult.FAIL_STEP_8;
                default:
                    return TestResult.FAIL;
            }
        }

        public short ReadError()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_ERROR, true, string.Empty);

            var errorReading = InternalSendCommand<PrivateCommands>(command);

            return Convert.ToInt16(errorReading.Split(',')[0].Replace("-", string.Empty));
        }


        public double ReadTestVoltageResult()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_VOLTAGE_RESULT, true, string.Empty);

            return double.Parse(SendLocalCommand(command), System.Globalization.NumberStyles.Float, enUS);
        }

        public double ReadTestVoltageResultOfStep(int step)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_VOLTAGE_RESULT_STEP, true, step);

            return double.Parse(SendLocalCommand(command), System.Globalization.NumberStyles.Float, enUS);
        }

        public double ReadTestCurrentResult()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_CURRENT_RESULT, true, string.Empty);

            return double.Parse(SendLocalCommand(command), System.Globalization.NumberStyles.Float, enUS);
        }

        public string ReadtestStepMode(int stepNumber)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_STEP_MODE, true, stepNumber);

            return SendLocalCommand(command);
        }

        public double ReadTestCurrentResultOfStep(int step)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_CURRENT_RESULT_STEP, true, step.ToString());

            return double.Parse(SendLocalCommand(command), System.Globalization.NumberStyles.Float, enUS);
        }

        public double  ReadTestTimeResult()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_TIME_RESULT, true, string.Empty);

            return double.Parse(SendLocalCommand(command), System.Globalization.NumberStyles.Float, enUS);
        }

        public void SetLocalMode()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.UNLOCK_KEYBOARD, true, string.Empty);
            
            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.LOCAL_MODE, true, string.Empty);

            SendLocalCommand(command);
        }
        
        public void SetRemoteMode()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.LOCK_KEYBOARD, true, string.Empty);

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.REMOTE_MODE, true, string.Empty);

            SendLocalCommand(command);
        }

        public void DeleteTestParameters(int stepNumber)
        {
            var steps = ReadAmountOfSteps();

            if (steps > 0)
            {
                string message = commandStrings[PrivateCommands.DELETE_STEP];
                var command = new Command<PrivateCommands>(PrivateCommands.DELETE_STEP, true, stepNumber);

                SendLocalCommand(command);
            }
        }

        public void SetACTestParameters(int stepNumber, double voltage, double maximumCurrent, double time)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_TEST_CURRENT_HIGH, true, (int)stepNumber, maximumCurrent.ToString(enUS));

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.SET_AC_TEST_VOLTAGE, true, (int)stepNumber, voltage.ToString(enUS));

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.SET_AC_TEST_TIME, true, (int)stepNumber, time.ToString(enUS));

            SendLocalCommand(command);
        }

        public void SetACOutputChannelHigh(int stepNumber, string channels)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_OUTPUT_CHANNEL_HIGH, true, stepNumber, channels);
            SendLocalCommand(command);
        }

        public void SetDCOutputChannelHigh(int stepNumber, string channels)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_OUTPUT_CHANNEL_HIGH, true, stepNumber, channels);
            SendLocalCommand(command);
        }

        public void SetIROutputChannelHigh(int stepNumber, string channels)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_IR_OUTPUT_CHANNEL_HIGH, true, stepNumber, channels);
            SendLocalCommand(command);
        }

        public void SetACOutputChannelLow(int stepNumber, string channels)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_OUTPUT_CHANNEL_LOW, true, stepNumber, channels);
            SendLocalCommand(command);
        }

        public void SetDCOutputChannelLow(int stepNumber, string channels)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_OUTPUT_CHANNEL_LOW, true, stepNumber, channels);
            SendLocalCommand(command);
        }

        public void SetIROutputChannelLow(int stepNumber, string channels)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_IR_OUTPUT_CHANNEL_LOW, true, stepNumber, channels);
            SendLocalCommand(command);
        }

        public void SetDCTestParameters(int stepNumber, double voltage, double maximumCurrent, double time)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_TEST_CURRENT_HIGH, true, (int)stepNumber, maximumCurrent.ToString(enUS));

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.SET_DC_TEST_VOLTAGE, true, (int)stepNumber, voltage.ToString(enUS));

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.SET_DC_TEST_TIME, true, (int)stepNumber, time.ToString(enUS));

            SendLocalCommand(command);

        }

        public void SetAcLimitLow(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_LIMIT_LOW, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetDcLimitLow(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_LIMIT_LOW, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetAcLimitArc(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_LIMIT_ARC, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetDcLimitArc(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_LIMIT_ARC, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetAcLimitReal(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_LIMIT_REAL, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetAcTimeRamp(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_TIME_RAMP, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetAcTimeFall(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_AC_TIME_FALL, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetDcTimeRamp(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_TIME_RAMP, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetDcTimeFall(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_DC_TIME_FALL, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetIRTestParameters(int stepNumber, double voltage, double minimumResistance, double time)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_IR_TEST_RESISTANCE_LOW, true, (int)stepNumber, minimumResistance.ToString(enUS));

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.SET_IR_TEST_VOLTAGE, true, (int)stepNumber, voltage.ToString(enUS));

            SendLocalCommand(command);

            command = new Command<PrivateCommands>(PrivateCommands.SET_IR_TEST_TIME, true, (int)stepNumber, time.ToString(enUS));

            SendLocalCommand(command);

        }

        public void SetIRMaximumResistance(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_IR_TEST_RESISTANCE_HIGH, true, (int)stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetIRTimeRamp(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_IR_TIME_RAMP, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public void SetIRTimeFall(int stepNumber, double value)
        {
            var command = new Command<PrivateCommands>(PrivateCommands.SET_IR_TIME_FALL, true, stepNumber, value);
            SendLocalCommand(command);
        }

        public int ReadAmountOfSteps()
        {
            var command = new Command<PrivateCommands>(PrivateCommands.READ_TEST_STEPS, true, string.Empty);

            var steps = SendLocalCommand(command);

            int outSteps;
            
            int.TryParse(steps, out outSteps);

            return outSteps;
        }

        public void StartTest()
        {
            SendLocalCommand(new Command<PrivateCommands>(PrivateCommands.START_TEST, true, string.Empty));
        }

        public void StopTest()
        {
            SendLocalCommand(new Command<PrivateCommands>(PrivateCommands.STOP_TEST, true, string.Empty));
        }

        private string SendLocalCommand(Command<PrivateCommands> command)
        {
            var reading = InternalSendCommand<PrivateCommands>(command);

            var errorReading = ReadError();
            if (errorReading != 0)
                throw new DielectricStrengthTesterException((DielectricStrengthTesterException.Codes)errorReading);

            return reading;
        }

        protected override string InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message;
            
            //message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands] + " {0}", command.Args[0]);

                message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands], command.Args);

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)PrivateCommands.READ_ERROR:
                case (int)PrivateCommands.REMOTE_MODE:
                case (int)PrivateCommands.READ_TEST_RESULT:
                case (int)PrivateCommands.READ_STEP_PROPERTIES:
                case (int)PrivateCommands.READ_TEST_STATE:
                case (int)PrivateCommands.READ_TEST_TIME_RESULT:
                case (int)PrivateCommands.READ_TEST_VOLTAGE_RESULT:
                case (int)PrivateCommands.READ_TEST_CURRENT_RESULT:
                case (int)PrivateCommands.READ_TEST_STEPS:
                case (int)PrivateCommands.READ_TEST_CURRENT_RESULT_STEP:
                case (int)PrivateCommands.READ_TEST_VOLTAGE_RESULT_STEP:
                case (int)PrivateCommands.READ_TEST_STEP_MODE:
                    message.HasResponse = true;
                    break;
                case (int)PrivateCommands.LOCK_KEYBOARD:
                case (int)PrivateCommands.UNLOCK_KEYBOARD:
                case (int)PrivateCommands.LOCAL_MODE:
                case (int)PrivateCommands.DELETE_STEP:
                case (int)PrivateCommands.SET_AC_TEST_VOLTAGE:
                case (int)PrivateCommands.SET_AC_TEST_CURRENT_HIGH:
                case (int)PrivateCommands.SET_AC_TEST_CURRENT_LOW:
                case (int)PrivateCommands.SET_AC_TEST_TIME:
                case (int)PrivateCommands.SET_DC_TEST_VOLTAGE:
                case (int)PrivateCommands.SET_DC_TEST_CURRENT_HIGH:
                case (int)PrivateCommands.SET_DC_TEST_CURRENT_LOW:
                case (int)PrivateCommands.SET_DC_TEST_TIME:
                case (int)PrivateCommands.RESET:
                case (int)PrivateCommands.START_TEST:
                case (int)PrivateCommands.STOP_TEST:
                case (int)PrivateCommands.SET_AC_OUTPUT_CHANNEL_HIGH:
                case (int)PrivateCommands.SET_AC_OUTPUT_CHANNEL_LOW:
                case (int)PrivateCommands.SET_DC_OUTPUT_CHANNEL_HIGH:
                case (int)PrivateCommands.SET_DC_OUTPUT_CHANNEL_LOW:
                case (int)PrivateCommands.SET_AC_LIMIT_LOW:
                case (int)PrivateCommands.SET_DC_LIMIT_LOW:
                case (int)PrivateCommands.SET_AC_LIMIT_ARC:
                case (int)PrivateCommands.SET_DC_LIMIT_ARC:
                case (int)PrivateCommands.SET_AC_LIMIT_REAL:
                case (int)PrivateCommands.SET_AC_TIME_RAMP:
                case (int)PrivateCommands.SET_AC_TIME_FALL:
                case (int)PrivateCommands.SET_DC_TIME_RAMP:
                case (int)PrivateCommands.SET_DC_TIME_FALL:
                case (int)PrivateCommands.SET_IR_TEST_RESISTANCE_LOW:
                case (int)PrivateCommands.SET_IR_TEST_RESISTANCE_HIGH:
                case (int)PrivateCommands.SET_IR_TEST_VOLTAGE:
                case (int)PrivateCommands.SET_IR_TEST_TIME:
                case (int)PrivateCommands.SET_IR_TIME_RAMP:
                case (int)PrivateCommands.SET_IR_TIME_FALL: 
                case (int)PrivateCommands.SET_IR_OUTPUT_CHANNEL_HIGH:
                case (int)PrivateCommands.SET_IR_OUTPUT_CHANNEL_LOW:
                    message.HasResponse = false;
                    break;
                default:
                    throw new NotSupportedException();
            }

            var reading = string.Empty;

            if (message.HasResponse)
                reading = transport.SendMessage<StringMessage>(message).Text;
            else
                transport.SendMessage<StringMessage>(message);
            

            return reading;
        }

        public override void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }

        public string GetInstrumentVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(InstrumentsVersionAttribute), true).FirstOrDefault() as InstrumentsVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }
    }
}
