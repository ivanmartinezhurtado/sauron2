﻿namespace Dezac.Tests
{
    public class TestPoint
    {
        private MeterType meter;
        private TipoPotencia potencia;
        private string description;
        private double gap;
        private double current;
        private double voltage;
        private double frequency;
        private double tolerance;

        public MeterType Meter { get { return meter; } }
        public TipoPotencia TipoPotencia { get { return potencia; } }
        public string Name { get { return description; } }
        public double Desfase { get { return gap; } }
        public double Current { get { return current; } }
        public double Voltage { get { return voltage; } }
        public double Frequency { get { return frequency; } }
        public double Tolerance { get { return tolerance; } }
        public TestPointsName testPointName { get; set; }

        public TestPoint(MeterType meterType, TestPointsName testPoint, TipoPotencia TipoPotencia, bool isMultiRangoVoltage = false)
        {
            testPointName = testPoint;
            var typeVoltage = isMultiRangoVoltage ? "_VMIN" : "";
            meter = meterType;
            description = string.Format("{0}_{1}_{2}Hz", testPoint.ToString() + typeVoltage, TipoPotencia.ToString(), meter.FrequencyProperty.Frequency.ToString());
            potencia = TipoPotencia;
            frequency = meter.FrequencyProperty.Frequency;
            voltage = isMultiRangoVoltage ? meter.VoltageProperty.VoltageMinimo : meter.VoltageProperty.VoltageNominal;
            tolerance = potencia == TipoPotencia.Activa ? meter.PrecisionProperty.PrecisionActiva : meter.PrecisionProperty.PrecisionReacctiva;
            gap = potencia == TipoPotencia.Activa ? 0 : 88;
            switch (testPoint)
            {
                case TestPointsName.TP_IMIN:
                    current = meter.CurrentProperty.CorrienteMinima;
                    tolerance = potencia == TipoPotencia.Activa ? meter.PrecisionProperty.PrecisionIminActiva : meter.PrecisionProperty.PrecisionIminReactiva;
                    break;
                case TestPointsName.TP_IMAX:
                    current = meter.CurrentProperty.CorrienteMaxima;
                    break;
                case TestPointsName.TP_IbCOS05:
                case TestPointsName.TP_IbSEN05:
                    current = meter.CurrentProperty.CorrienteBase;
                    gap = potencia == TipoPotencia.Activa ? 60 : 30;
                    break;
                case TestPointsName.TP_ARRANQUE:
                    current = meter.CurrentProperty.CorrienteBase * 0.04;
                    break;
                case TestPointsName.TP_MARCHA_VACIO:
                    voltage = meter.VoltageProperty.VoltageNominal * 1.15;
                    current = 0;
                    break;
            }
        }
    }
}
