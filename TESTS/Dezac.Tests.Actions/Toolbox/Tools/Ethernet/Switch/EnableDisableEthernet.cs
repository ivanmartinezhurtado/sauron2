﻿using Dezac.Tests.Services;
using Instruments.Switch;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(EnableDisableEthernetDescription))]
    [Browsable(false)]

    public class EnableDisableEthernet : ActionBase
    {
        [Description("Contraseña")]
        [Category("3. Configuration")]
        [DefaultValue("F3rran.18")]
        public string Password { get; set; }
        
        [Description("Número de la IP")]
        [Category("3. Configuration")]
        [DefaultValue("100.0.0.8")]
        public string IP { get; set; }

        [Description("Puerto Ethernet en el que esta conectado")]
        [Category("3. Configuration")]
        [DefaultValue(NetgearGS108T.Ethernet.ETH1)]
        public NetgearGS108T.Ethernet Ethernet { get; set; }

        [Description("Estado, activado o desactivado")]
        [Category("3. Configuration")]
        public NetgearGS108T.State State { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var sw = cacheSvc.Get<NetgearGS108T>(string.Format("NetgearSwitch_{0}", IP));
            if (sw == null)
            {
                sw = new NetgearGS108T();
                cacheSvc.Add(string.Format("NetgearSwitch_{0}", IP), sw);
            }

            sw.ChangeStateEthernet(Ethernet, State);
        }
    }

    public class EnableDisableEthernetDescription : ActionDescription
    {
        public override string Name { get { return "EnabledDisabledEthernet"; } }
        public override string Category { get { return "Tools/Ethernet/Switch"; } }
        public override string Description { get { return "Action para activar o desactivar el Ethernet"; } }
        public override Image Icon { get { return Properties.Resources.router_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
