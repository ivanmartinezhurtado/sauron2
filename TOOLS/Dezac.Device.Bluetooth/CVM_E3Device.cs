﻿using Dezac.Device.Bluetooth.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Device.Bluetooth
{
    public class CVM_E3Device : BluetoothDevice
    {
        public static Guid UNKNOW_SERVICE_WIFI_CONFIGURATION_UUID = new Guid("1AEAFFAC-EC30-11E9-81B4-2A2AE2DBCCE4");
        public static Guid CHARACTERISTIC_ENABLED_WIFI = new Guid("8AC9BD5E-EC0D-11E9-81B4-2A2AE2DBCCE4");

        public static string DEFAULT_PIN = "000000";
        public static CVM_E3Device Find(int timeOut = 10000)
        {
            var result = BluetoothHelper.Scan("E3-Mini", timeOut);
            if (result != null)
                return new CVM_E3Device { BluetoothAddress = result.BluetoothAddress, PIN = DEFAULT_PIN };

            return null;
        }

        public CVM_E3Device()
        {
        }

        public CVM_E3Device(string name)
            : base(name)
        {
        }

        public static CVM_E3Device Find(string serialNumber, int timeOut = 10000)
        {
            DEFAULT_PIN = serialNumber.Substring(serialNumber.Length - 6);
            var result = BluetoothHelper.Scan(string.Format("E3-Mini-{0}", serialNumber.Substring(serialNumber.Length - 4)), timeOut);
            if (result != null)
                return new CVM_E3Device { BluetoothAddress = result.BluetoothAddress, PIN = DEFAULT_PIN };

            return null;
        }

        public byte ReadWifiEnabled()
        {
            return ReadByte(UNKNOW_SERVICE_WIFI_CONFIGURATION_UUID, CHARACTERISTIC_ENABLED_WIFI);
        }

        public bool WriteWifiEnabled(byte value)
        {
            return Write(UNKNOW_SERVICE_WIFI_CONFIGURATION_UUID, CHARACTERISTIC_ENABLED_WIFI, value);
        }
    }
}
