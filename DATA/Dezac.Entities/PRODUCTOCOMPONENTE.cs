namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class PRODUCTOCOMPONENTE
    {
        public int NUMPRODUCTO { get; set; }
        public int VERSIONPRODUCTO { get; set; }
        public int NUMCOMPONENTE { get; set; }
        public int VERSIONCOMPONENTE { get; set; }
        public decimal CANTIDAD { get; set; }
        public string ORDENMONTAJE { get; set; }
    
        public virtual PRODUCTO PRODUCTO { get; set; }
        public virtual ARTICULO ARTICULO { get; set; }
    }
}
