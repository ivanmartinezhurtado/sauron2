﻿using Dezac.Data.ViewModels;
using Dezac.Services;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Services;
using NUnrar.Archive;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using TaskRunner.Model;


namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(GetBinaryDataBaseDescription))]
    public class GetBinaryDataBase : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Tipo de documento que queremos coger para hacer la secuencia")]
        public ActionVariable DocumentType { get; set; } = new ActionVariable();

        [Category("4. Results")]
        [Description("Nombre del dato de salida")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var iShell = SequenceContext.Current.Services.Get<IShell>();
            var data = SequenceContext.Current.Services.Get<ITestContext>();

            if (data == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("Falta iniciar una función de carga de datos de contexto del test (InitProduct)").Throw();

            if (data.NumOrden != null && data.NumProducto == 0)
                Error().PROCESO.FICHERO_NO_ENCONTRADO.NO_ORDEN_FABRICACION_NO_PRODUCTO("No se ha entrado ni una orden de fabricación o ni un producto para poder iniciar un test").Throw();

            BorrarBinaryTemp();

            BinaryItemViewModel binaryItem;

            using (DezacService svc = new DezacService())
            {               
                binaryItem = svc.GetBinario(data.NumProducto, data.Version, data.IdFase, DocumentType.Key);
            }

            if (binaryItem == null)
                Error().PROCESO.PRODUCTO.BINARIOS("NO SE ENCUENTRA FICHERO").Throw();

            var filePathBinary = PathFileBinary;

            binaryItem.PathFicheroTest = CrearArchivoBinario(binaryItem);
            try
            {
                if (binaryItem.Tipofichero == "911")
                {
                    if (RarArchive.IsRarFile(binaryItem.PathFicheroTest))
                        RarArchive.WriteToDirectory(binaryItem.PathFicheroTest, PathFileBinary);
                    else
                        ZipFile.ExtractToDirectory(binaryItem.PathFicheroTest, PathFileBinary);

                    if (Directory.Exists(Path.Combine(PathFileBinary, binaryItem.CodigoBcn)))
                        filePathBinary = Path.Combine(PathFileBinary, binaryItem.CodigoBcn);
                    else
                        filePathBinary = Path.Combine(PathFileBinary);
                }

                Context.Variables.AddOrUpdate("DirectoryGetBinaryPGI", filePathBinary);
            }
            catch (Exception ex)
            {
                //throw new Exception(string.Format("Error al descomprimir el binario (ZIP) por {0}, o el fichero {1} de BBB no esta en formato ZIP", ex.Message, binaryItem.NombreFichero));
                Error().PROCESO.ACTION_EXECUTE.BINARIOS(string.Format("Error al descomprimir el binario (ZIP) por {0}, o el fichero {1} de BBB no esta en formato ZIP", ex.Message, binaryItem.NombreFichero)).Throw();
            }
            if(DataOutput !=null && !DataOutput.IsKeyNullOrEmpty)
                DataOutput.Value = binaryItem;
            SequenceContext.Current.Variables.AddOrUpdate("BinaryItemViewModel", binaryItem);
            SequenceContext.Current.Variables.AddOrUpdate("BinaryPath", binaryItem.PathFicheroTest);
        }

        public void BorrarBinaryTemp()
        {
            string url = PathFileBinary;

            if (Directory.Exists(url))
                Directory.Delete(url, true);

            System.Threading.Thread.Sleep(1000);

            if (!Directory.Exists(url))
                Directory.CreateDirectory(url);
        }

        private string PathFileBinary
        {
            get { return ConfigurationManager.AppSettings["PathFileBinary"]; }
        }

        private string CrearArchivoBinario(BinaryItemViewModel item)
        {
            if (!Directory.Exists(PathFileBinary))
                Directory.CreateDirectory(PathFileBinary);

            string name = string.Format("{0}", item.NombreFichero);

            string fileName = Path.Combine(PathFileBinary, name);

            if (!File.Exists(fileName))
                File.WriteAllBytes(fileName, item.Fichero);

            return fileName;
        }
    }

    public class GetBinaryDataBaseDescription : ActionDescription
    {
        public override string Name { get { return "GetBinaryPGI"; } }
        public override string Category { get { return "DezacTest/Oracle/FilesPGI"; } }
        public override string Description { get { return "Action que sirve para cargar la base de datos de los documentos de binarios que existen en el PGI a la secuencia"; } }
        public override Image Icon { get { return Properties.Resources.DatabaseFiles; } }
    }
}
