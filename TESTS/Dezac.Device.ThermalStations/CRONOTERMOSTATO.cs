﻿using System.Threading;

namespace Dezac.Device.ThermalStations
{
    [DeviceVersion(1.00)]
    public class CRONOTERMOSTATO : DeviceBase
    {
        #region Inicializacion y destrucción de la clase del dispositivo

        public CRONOTERMOSTATO()
        {
        }

        public CRONOTERMOSTATO(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }
        
        #endregion

        #region Funciones de escritura
        
        public void WriteStartTemperatureSensing()
        {
            Modbus.WriteSingleCoil((ushort)Registers.START_TEMPERATURE_SENSING, true);
        }

        #endregion

        #region Funciones de lectura
        
        public int ReadTemperature()
        {
            return Modbus.ReadRegister((ushort)Registers.TEMPERATURE_READING);
        }

        #endregion

        #region Metodos Privados

        public double ReadTemperatureAndConvertToReal()
        {                
            return ReadTemperature() / 100D;
        }

        #endregion

        #region Registros

        public enum Registers
        {
            START_TEMPERATURE_SENSING = 0x4004,
            TEMPERATURE_READING = 0x0001
        }
        
        #endregion
    }
}
