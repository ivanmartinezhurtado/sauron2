﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Dezac.Tests.Visio
{
    public static class CameraUtils
    {
        public static List<uEye.Types.CameraInformation> GetCamerasIDS()
        {         
            uEye.Types.CameraInformation[] cameraList;
            uEye.Info.Camera.GetCameraList(out cameraList);

            return cameraList.ToList();
        }

        public static uEye.Types.CameraInformation GetCameraInfoIDSBySerialNumber(string serialNumber)
        {
            var cameras = GetCamerasIDS();

            return cameras.Where((p) => p.SerialNumber == serialNumber).FirstOrDefault();
        }
    }
}
