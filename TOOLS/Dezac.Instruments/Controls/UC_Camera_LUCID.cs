﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.Auxiliars;
using Instruments.Cameras;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(Camera_LUCID_Description))]
    public partial class UC_Camera_LUCID : UserControl, IDisposable
    {
        private static readonly string TAG = "Camera LUCID";
        private CameraLucid camera;
        public Type InstrumentType { get { return typeof(CameraLucid); } }

        public CameraLucid Instrument
        {
            get
            {
                if (camera == null)
                    camera = new CameraLucid();
                return camera;
            }
            set
            {
                if (camera != null)
                {
                    camera.Dispose();
                    camera = null;
                }
                camera = value;
            }
        }

        public UC_Camera_LUCID() 
        {
            InitializeComponent();         
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))       
                components.Dispose();
            
            if(Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }

    public class Camera_LUCID_Description : ControlDescription
    {
        public override string Name { get { return "Camera LUCID"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Devices_camera_web_icon; } }
        public override Type SubcomponentType { get { return typeof(CameraLucid); } }
        public override bool Visible => false;
    }
}
