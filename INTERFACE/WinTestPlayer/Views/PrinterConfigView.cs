﻿using System;
using System.Windows.Forms;
using Dezac.Tests.UserControls;
using System.Drawing.Printing;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class PrinterConfigView : DialogViewBase
    {
        protected AppService app;

        private string PrinterLabels { get; set; }

        private string PrinterCharacteristics { get; set; }

        private string PrinterIndividualPackaging { get; set; }

        private string PrinterPackingPackage { get; set; }

        public PrinterConfigView()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (DesignMode)
                return;

            if (PrinterSettings.InstalledPrinters.Count == 0)
                return;

            var printer = new PrinterSettings();

            app = DI.Resolve<AppService>();

            if (app.Model == null)
                return;

            PrinterLabels = app.Model.PrinterLabel;
            PrinterCharacteristics = app.Model.PrinterCharacteristics;
            PrinterIndividualPackaging = app.Model.PrinterIndividualPackage;
            PrinterPackingPackage = app.Model.PrinterPackingPackage;

            ddlPrintersLabel.Items.Add("NINGUNA");
            ddlPrintersCaracteristicas.Items.Add("NINGUNA");
            ddlPrintersIndividual.Items.Add("NINGUNA");
            ddlPrintersConjunto.Items.Add("NINGUNA");


            if (string.IsNullOrEmpty(PrinterLabels) || (PrinterLabels.Contains("NO")))
                PrinterLabels = "NINGUNA";

            if (string.IsNullOrEmpty(PrinterCharacteristics) || (PrinterCharacteristics.Contains("NO")))
                PrinterCharacteristics = "NINGUNA";

            if (string.IsNullOrEmpty(PrinterIndividualPackaging) || (PrinterIndividualPackaging.Contains("NO")))
                PrinterIndividualPackaging = "NINGUNA";

            if (string.IsNullOrEmpty(PrinterPackingPackage) || (PrinterPackingPackage.Contains("NO")))
                PrinterPackingPackage = "NINGUNA";

            foreach (object item in PrinterSettings.InstalledPrinters)
            {
                printer.PrinterName = item.ToString();

                if (printer.IsValid)
                {
                    ddlPrintersLabel.Items.Add(printer.PrinterName);

                    if (PrinterLabels.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("LABEL") || printer.PrinterName.ToUpper().Contains("#1"))
                            PrinterLabels = printer.PrinterName;

                    ddlPrintersCaracteristicas.Items.Add(printer.PrinterName);

                    if (PrinterCharacteristics.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("LABEL") || printer.PrinterName.ToUpper().Contains("#1"))
                            PrinterCharacteristics = printer.PrinterName;

                    ddlPrintersIndividual.Items.Add(printer.PrinterName);

                    if (PrinterIndividualPackaging.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("INDIVIDUAL") || printer.PrinterName.ToUpper().Contains("#2"))
                            PrinterIndividualPackaging = printer.PrinterName;

                    ddlPrintersConjunto.Items.Add(printer.PrinterName);

                    if (PrinterPackingPackage.Contains("NINGUNA"))
                        if (printer.PrinterName.ToUpper().Contains("CONJUNTO") || printer.PrinterName.ToUpper().Contains("#3"))
                            PrinterPackingPackage = printer.PrinterName;
                }
            }

            ddlPrintersLabel.SelectedItem = ddlPrintersLabel.FindItemExact(PrinterLabels, true);
            ddlPrintersCaracteristicas.SelectedItem = ddlPrintersCaracteristicas.FindItemExact(PrinterCharacteristics, true);
            ddlPrintersIndividual.SelectedItem = ddlPrintersIndividual.FindItemExact(PrinterIndividualPackaging, true);
            ddlPrintersConjunto.SelectedItem = ddlPrintersConjunto.FindItemExact(PrinterPackingPackage, true);

        }

        public override bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.Cancel)
                return true;

            if (ddlPrintersLabel.SelectedItem == null || ddlPrintersCaracteristicas == null || ddlPrintersIndividual.SelectedItem == null || ddlPrintersConjunto.SelectedItem == null)
                return false;

            if (string.IsNullOrEmpty(ddlPrintersLabel.SelectedItem.Text) || string.IsNullOrEmpty(ddlPrintersCaracteristicas.SelectedItem.Text) || string.IsNullOrEmpty(ddlPrintersIndividual.SelectedItem.Text) || string.IsNullOrEmpty(ddlPrintersConjunto.SelectedItem.Text))
                return false;

            PrinterLabels = ddlPrintersLabel.SelectedItem.Text;

            PrinterCharacteristics = ddlPrintersCaracteristicas.SelectedItem.Text;

            PrinterIndividualPackaging = ddlPrintersIndividual.SelectedItem.Text;

            PrinterPackingPackage = ddlPrintersConjunto.SelectedItem.Text;

            if ((PrinterLabels.ToUpper().Trim() != "NINGUNA") && (PrinterCharacteristics.ToUpper().Trim() != "NINGUNA") && (PrinterIndividualPackaging.ToUpper().Trim() != "NINGUNA") && (PrinterPackingPackage.ToUpper().Trim() != "NINGUNA"))
                if (PrinterLabels == PrinterIndividualPackaging || PrinterLabels == PrinterPackingPackage)
                    return false;

            app.Model.PrinterLabel = PrinterLabels;
            app.Model.PrinterCharacteristics = PrinterCharacteristics;
            app.Model.PrinterIndividualPackage = PrinterIndividualPackaging;
            app.Model.PrinterPackingPackage = PrinterPackingPackage;

            //if (PrinterIndividualPackaging.ToUpper().Trim() != "NINGUNA")
            //    Dezac.Tests.Reports.Utils.CustomPrintForm.AddCustomPaperSize(PrinterIndividualPackaging, "Report", 105, 250);

            return true;
        }

    }

}
