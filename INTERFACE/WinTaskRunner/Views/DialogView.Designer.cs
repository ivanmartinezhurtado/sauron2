﻿namespace WinTaskRunner.Views
{
    partial class DialogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogView));
            this.content = new System.Windows.Forms.Panel();
            this.footer = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdCancel = new Telerik.WinControls.UI.RadButton();
            this.cmdNo = new Telerik.WinControls.UI.RadButton();
            this.cmdYes = new Telerik.WinControls.UI.RadButton();
            this.cmdOk = new Telerik.WinControls.UI.RadButton();
            this.lblDecripction = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.footer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.content.Location = new System.Drawing.Point(0, 0);
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(811, 410);
            this.content.TabIndex = 1;
            // 
            // footer
            // 
            this.footer.Controls.Add(this.cmdCancel);
            this.footer.Controls.Add(this.cmdNo);
            this.footer.Controls.Add(this.cmdYes);
            this.footer.Controls.Add(this.cmdOk);
            this.footer.Controls.Add(this.lblDecripction);
            this.footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footer.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.footer.Location = new System.Drawing.Point(0, 410);
            this.footer.Name = "footer";
            this.footer.Padding = new System.Windows.Forms.Padding(10);
            this.footer.Size = new System.Drawing.Size(811, 90);
            this.footer.TabIndex = 0;
            this.footer.TabStop = true;
            // 
            // cmdCancel
            // 
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdCancel.Location = new System.Drawing.Point(694, 13);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(94, 70);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancelar";
            this.cmdCancel.Click += new System.EventHandler(this.OnButtonClicked);
            // 
            // cmdNo
            // 
            this.cmdNo.DialogResult = System.Windows.Forms.DialogResult.No;
            this.cmdNo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdNo.Location = new System.Drawing.Point(594, 13);
            this.cmdNo.Name = "cmdNo";
            this.cmdNo.Size = new System.Drawing.Size(94, 70);
            this.cmdNo.TabIndex = 2;
            this.cmdNo.Text = "No";
            this.cmdNo.Click += new System.EventHandler(this.OnButtonClicked);
            // 
            // cmdYes
            // 
            this.cmdYes.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.cmdYes.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdYes.Location = new System.Drawing.Point(494, 13);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(94, 70);
            this.cmdYes.TabIndex = 1;
            this.cmdYes.Text = "Sí";
            this.cmdYes.Click += new System.EventHandler(this.OnButtonClicked);
            // 
            // cmdOk
            // 
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdOk.Location = new System.Drawing.Point(394, 13);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(94, 70);
            this.cmdOk.TabIndex = 0;
            this.cmdOk.Text = "OK";
            this.cmdOk.Click += new System.EventHandler(this.OnButtonClicked);
            // 
            // lblDecripction
            // 
            this.lblDecripction.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblDecripction.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDecripction.Location = new System.Drawing.Point(12, 10);
            this.lblDecripction.Name = "lblDecripction";
            this.lblDecripction.Size = new System.Drawing.Size(376, 76);
            this.lblDecripction.TabIndex = 4;
            this.lblDecripction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DialogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 500);
            this.ControlBox = false;
            this.Controls.Add(this.content);
            this.Controls.Add(this.footer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DialogView";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MsgBox";
            this.ThemeName = "TelerikMetroBlue";
            this.footer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel content;
        private System.Windows.Forms.FlowLayoutPanel footer;
        private Telerik.WinControls.UI.RadButton cmdYes;
        private Telerik.WinControls.UI.RadButton cmdOk;
        private Telerik.WinControls.UI.RadButton cmdNo;
        private Telerik.WinControls.UI.RadButton cmdCancel;
        private System.Windows.Forms.Label lblDecripction;
        private System.Windows.Forms.Timer timer1;
    }
}