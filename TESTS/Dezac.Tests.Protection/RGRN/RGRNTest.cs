﻿using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.Utils;
using Instruments.IO;
using System;
using Dezac.Tests.Services;
using System.Collections.Generic;
using Dezac.Core.Utility;
using Instruments.PowerSource;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.42)]
    public class RGRNTest : RGRNTestBase
    {
        private ushort gain = 0;

        new RG_RN Rgrn;

        public override void TestInitialization()
        {
            Rgrn = new RG_RN(Comunicaciones.SerialPort);

            base.TestInitialization();
        }

        public override void TestInitLAC25Macros(bool rewrite = false)
        {
            bool Macros;
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("LAC25_MACROS", out Macros) || rewrite)
            {
                Macros = AddOrGetSharedVar<bool>("LAC25_MACROS", () => { return true; });

                Tower.Actuator_LAC25.InitLAC25();

                var pushTorque = (int)Configuracion.GetDouble("FUERZA_LINEAL", 12000, ParamUnidad.Numero);
                var rotationTorque = (int)Configuracion.GetDouble("FUERZA_ROTACION", 12000, ParamUnidad.Numero);
                var minimumTorqueDrop = (int)Configuracion.GetDouble("CAIDA_TORQUE_MINIMA", 1200, ParamUnidad.Numero);

                Tower.Actuator_LAC25.WriteAndRunFindSelector(false, true, 30, 231072, 5243, 32767, 9243, 5243, pushTorque, pushTorque, 60000, 4500, rotationTorque);

                Tower.Actuator_LAC25.WriteTorqueStep(60000, 30000, rotationTorque, 40, 3000, minimumTorqueDrop);

                if (cacheSvc != null)
                    cacheSvc.Add("LAC25_MACROS", Macros);
            }
        }

        public void TestInitializationWithoutCom()
        {
            base.TestInitialization();
        }

        public override void TestCommunications()
        {
            string firmwareVersion = "0";

            SamplerWithCancel((p) => { firmwareVersion = Rgrn.ReadFirmwareVersion(); return true; }, 
                "Error de comunicaciones, el equipo no arranca o no comunica", 1, 500, 500, true, true,
                 (s) => { Error().UUT.COMUNICACIONES.INICIALIZACION(s).Throw(); } );

            TestInfo.FirmwareVersion = firmwareVersion;

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, Identificacion.VERSION_FIRMWARE.ToString(), Error().PROCESO.PARAMETROS_ERROR.VERSION_FIRMWARE(), ParamUnidad.SinUnidad);
        }

        public void TestReleKeyboardLeds()
        {
            CAMERA_IDS = GetVariable<string>("CAMARA_LEDS", "");

            SamplerWithCancel((p) =>
            {
                return (!Tower.IO.DI.Read(IN_RELAY_NO).Value & Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error: El equipo esta disparado antes del test de rele", 2, 100, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.RELE(s).Throw();
            });

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "RG_RN_VERDE", "VERDE", "RG_RN", 1);

            SamplerWithCancel((p) =>
            {
                Tower.IO.DO.PulseOnWait(new IOBase.Time(500), new IOBase.Time(200), OUT_EV_TECLA_TEST);
                return (Tower.IO.DI.Read(IN_RELAY_NO).Value & !Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error Rele de disparo o fallo en la tecla Test", 3, 500, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.RELE(s).Throw();
            });

            Resultado.Set("TECLA_TEST", "OK", ParamUnidad.SinUnidad);

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "RG_RN_ROJO", "ROJO", "RG_RN", 1);

            SamplerWithCancel((p) =>
            {
                Tower.IO.DO.PulseOnWait(new IOBase.Time(500), new IOBase.Time(200), OUT_EV_TECLA_RESET);
                return (!Tower.IO.DI.Read(IN_RELAY_NO).Value & Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error Rele de disparo o fallo en la tecla Reset", 3, 500, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.RELE(s).Throw();
            });

            Resultado.Set("TECLA_RESET", "OK", ParamUnidad.SinUnidad);        
            Resultado.Set("RELE_DISPARO", "OK", ParamUnidad.SinUnidad);
        }

        public override void TestToroidal()
        {
            Tower.IO.DO.PulseOn(500, OUT_EV_TECLA_RESET);

            SamplerWithCancel((p) =>
            {
                return (!Tower.IO.DI.Read(IN_RELAY_NO).Value & Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error: El equipo esta disparado antes del test de toroidal", 2, 100, 0, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.TOROIDAL(s).Throw();
            });

            Tower.IO.DO.On(OUT_ABRE_TOROIDAL);

            SamplerWithCancel((p) =>
            {
                return (Tower.IO.DI.Read(IN_RELAY_NO).Value & !Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error: El equipo no detecta la desconexion del toroidal", 10, 200, 500, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.TOROIDAL(s).Throw();
            });

            Resultado.Set("TEST_TOROIDAL_DESCONECTADO", "OK", ParamUnidad.SinUnidad);

            Tower.IO.DO.OffWait(200, OUT_ABRE_TOROIDAL);
            Tower.IO.DO.PulseOnWait(new IOBase.Time(500), new IOBase.Time(200), OUT_EV_TECLA_RESET);

            SamplerWithCancel((p) =>
            {
                return (!Tower.IO.DI.Read(IN_RELAY_NO).Value & Tower.IO.DI.Read(IN_RELAY_NC).Value);
            }, "Error: El equipo no detecta el toroidal conectado", 10, 200, 500, true, true,
            (s) =>
            {
                Error().UUT.HARDWARE.TOROIDAL(s).Throw();
            });

            Resultado.Set("TEST_TOROIDAL_CONECTADO", "OK", ParamUnidad.SinUnidad);
        }

        public override void TestAdjust()
        {
            InitAdjust(true);

            var def = new AdjustValueDef(Params.I.Null.Ajuste.TestPoint("PUNTOS").Name, 0, Params.I.Null.Ajuste.TestPoint("PUNTOS").Min(), Params.I.Null.Ajuste.TestPoint("PUNTOS").Max(), 0, 0, ParamUnidad.Puntos);

            Rgrn.WriteNormalMode();
            Delay(2000, "Modo Normal");

            TestMeasureBase(def, (step) =>
            {
                var currentRead = Rgrn.ReadCurrent();

                return currentRead;
            }, 0, 3, 500);

            Rgrn.WriteFlagTest();
            Delay(1500, "Modo test");

            AdjustValueDef adjustValueGain = new AdjustValueDef(Params.GAIN_I.Null.Ajuste.Name, 0, Params.GAIN_I.Null.Ajuste.Min(), Params.GAIN_I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);
            var escalaCurrent = Consignas.GetDouble(Params.I.Null.TestPoint("ESCALA_DE_AJUSTE").Name, 0, ParamUnidad.A);
            var gainFactor = Consignas.GetDouble(Params.GAIN_FACTOR.Null.Ajuste.Name, 1, ParamUnidad.Numero);

            gain = 545;

            TestMeasureBase(adjustValueGain, (step) =>
            {
                var readCurrent = Rgrn.WriteComputeGain();
                gain = Rgrn.CumputeCurrentGain(readCurrent, escalaCurrent, gainFactor);
                return gain;
            }, 0, 1, 500);

            Rgrn.WriteFlagTest();
            Rgrn.WriteCurrentGain(gain);
            Delay(2000, "Espera guardado de la cte.");

            var readGain = Rgrn.ReadCurrentGain();
            Assert.AreEqual(readGain, gain, Error().UUT.MEDIDA.MARGENES(String.Format("Error: La ganancia de corriente no se ha almacenado correctamente. Escrita: {0}, Leida: {1}.", gain, readGain)));

            Rgrn.WriteNormalMode();

            Delay(2000, "Modo Normal");

            AdjustValueDef adjustValueCurrent = new AdjustValueDef(Params.I.Null.Ajuste.Name, 0, Params.I.Null.Ajuste.Min(), Params.I.Null.Ajuste.Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(adjustValueCurrent, (step) =>
            {
                var currentRead = Rgrn.ReadCurrent(escalaCurrent, readGain);
                return currentRead;
            }, 0, 3, 500);

            //Tower.Actuator_LAC25.RunGoPosition(0);
            Tower.Actuator_LAC25.GoInitialPosition();
        }   

        public void TestCurrentSelector()
        {
            Rgrn.WriteNormalMode();

            //base.TestCurrentSelector(() => 
            //{
            //    return Rgrn.ReadCurrent();
            //}, 3000, true);

            Dictionary<RG_RN_LC.CurrentSelectorPosition, AdjustValueDef> dictionaryPositionEscale = new Dictionary<RG_RN_LC.CurrentSelectorPosition, AdjustValueDef>()
            {
                { RG_RN_LC.CurrentSelectorPosition.ESC_2, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_2").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_2").Min(), Params.I.Null.Verificacion.TestPoint("ESC_2").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_3, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_3").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_3").Min(), Params.I.Null.Verificacion.TestPoint("ESC_3").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_4, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_4").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_4").Min(), Params.I.Null.Verificacion.TestPoint("ESC_4").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_5, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_5").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_5").Min(), Params.I.Null.Verificacion.TestPoint("ESC_5").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_6, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_6").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_6").Min(), Params.I.Null.Verificacion.TestPoint("ESC_6").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_7, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_7").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_7").Min(), Params.I.Null.Verificacion.TestPoint("ESC_7").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.CurrentSelectorPosition.ESC_1, new AdjustValueDef(Params.I.Null.Verificacion.TestPoint("ESC_1").Name, 0, Params.I.Null.Verificacion.TestPoint("ESC_1").Min(), Params.I.Null.Verificacion.TestPoint("ESC_1").Max(), 0, 0, ParamUnidad.Puntos)},
            };

            foreach (KeyValuePair<RG_RN_LC.CurrentSelectorPosition, AdjustValueDef> escala in dictionaryPositionEscale)
                SamplerWithCancel((s) =>
                {
                    try
                    {
                        if (escala.Key != RG_RN_LC.CurrentSelectorPosition.ESC_1)
                            Tower.Actuator_LAC25.GoNextTorqueStep();
                        else
                            Tower.Actuator_LAC25.GoInitialPosition();
                        
                        Delay(3000, "Estabilizacion medida");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message + " en " + escala.Key.GetDescription());
                    }
                    finally
                    {
                        uint currentRead = 0;
                        TestMeasureBase(escala.Value,
                            (step) =>
                            {
                                currentRead = Rgrn.ReadCurrent();
                                return currentRead;
                            }, 0, 3, 500);
                        Resultado.Set(String.Format("CORRIENTE_{0}", escala.Key.ToString()), "OK", ParamUnidad.SinUnidad);
                    }
                    return true;
                }, "", 2, 300, 0, false, false, null);

            this.InParellel(() =>
            {
                Tower.PTE.SetSource(PTE.SourceType.PTE_50_CE);
                Tower.PTE.ApplyOff();
            }, () =>
            {
                Tower.Actuator_LAC25.Home(false, true);
            });
        }

        public void TestTimeSelector()
        {
            Rgrn.WriteNormalMode();

            //base.TestTimeSelector(() =>
            //{
            //    return Rgrn.ReadTime();
            //}, 1000, true);

            Tower.IO.DO.Off(OUT_EV_ACTUADOR);
            SamplerWithCancel((p) => { return (!Tower.IO.DI[IN_POSICION_ACTUADOR_CORRIENTE] && Tower.IO.DI[IN_POSICION_ACTUADOR_TIEMPO]); }, "Error: No se ha detectado el sensor del actuador en posicion del selector de tiempo", 20, 100);

            Tower.Actuator_LAC25.WriteAndRunFindSelector();

            Delay(1000, "Estabilizacion medida");

            var defRef = new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_REF").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_1").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_1").Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(defRef, (step) =>
            {
                var read = Rgrn.ReadTime();
                return read;
            }, 0, 4, 500);

            Dictionary<RG_RN_LC.TimeSelectorPosition, AdjustValueDef> dictionaryPositionEscale = new Dictionary<RG_RN_LC.TimeSelectorPosition, AdjustValueDef>()
            {
                { RG_RN_LC.TimeSelectorPosition.ESC_2, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_2").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_2").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_2").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_3, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_3").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_3").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_3").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_4, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_4").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_4").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_4").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_5, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_5").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_5").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_5").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_6, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_6").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_6").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_6").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_7, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_7").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_7").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_7").Max(), 0, 0, ParamUnidad.Puntos)},
                { RG_RN_LC.TimeSelectorPosition.ESC_1, new AdjustValueDef(Params.TIME.Null.Verificacion.TestPoint("ESC_1").Name, 0, Params.TIME.Null.Verificacion.TestPoint("ESC_1").Min(), Params.TIME.Null.Verificacion.TestPoint("ESC_1").Max(), 0, 0, ParamUnidad.Puntos)},
            };

            foreach (KeyValuePair<RG_RN_LC.TimeSelectorPosition, AdjustValueDef> escala in dictionaryPositionEscale)
                SamplerWithCancel((s) =>
                {
                    try
                    {
                        if (escala.Key != RG_RN_LC.TimeSelectorPosition.ESC_1)
                            Tower.Actuator_LAC25.GoNextTorqueStep();
                        else
                            Tower.Actuator_LAC25.GoInitialPosition();
                        
                        Delay(1000, "Estabilizacion medida");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message + " en " + escala.Key.GetDescription());
                    }
                    finally
                    {
                        ushort timeRead = 0;
                        TestMeasureBase(escala.Value, (step) =>
                        {
                            timeRead = Rgrn.ReadTime();
                            return timeRead;
                        }, 0, 4, 500);
                        Resultado.Set(String.Format("TIEMPO_{0}", escala.Key.ToString()), "OK", ParamUnidad.SinUnidad);
                    }
                    return true;
                }, "", 2, 300, 0, false, false, null);

            Tower.Actuator_LAC25.Home(false, true);
        }

        public void TestVerification()
        {
            Rgrn.WriteNormalMode();

            Tower.IO.DO.PulseOn(500,OUT_EV_TECLA_RESET);
        
            var escalaCurrent = Consignas.GetDouble(Params.I.Null.TestPoint("ESCALA_DE_VERIFICACION").Name, 0, ParamUnidad.A);

            base.TestVerification(() =>
            {
                return Rgrn.ReadCurrent(escalaCurrent, gain);
            });
        }

        public override void TestCostumization()
        {
            throw new NotImplementedException();
        }

        public override void TestFinish()
        {
            base.TestFinish();

            if (Rgrn != null)
                Rgrn.Dispose();
        }
    }
}