using System;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms.DataVisualization.Charting;
using WinTaskRunner.Services;
using Telerik.WinControls.UI;
using TaskRunner.Model;

namespace WinTaskRunner.Views
{
    public partial class PlotForm : Form
    {
        private const int REFRESHING_PERIOD = 1000;

        private SynchronizationContext sc;
        private System.Windows.Forms.Timer timer1;
        private bool refreshingRequested = false;
        private SharedVariablesList variables;

        public PlotForm()
        {
            InitializeComponent();
            Font = SystemInformation.MenuFont;
            timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = REFRESHING_PERIOD;
            timer1.Tick += (s, e) => { timer1_Tick(s, e); };
            timer1.Enabled = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            sc = SynchronizationContext.Current;

            //if (SequenceContext.Current != null)
            //    SequenceContext.Current.Variables.VariableChangued += OnVariablesChanged;

            AppService app = DI.Resolve<AppService>();
            app.Runner.TestStart += (_, ee) => OnTestStart();
            app.Runner.TestEnd += (_, ee) => OnTestEnd();
        }

        void OnVariablesChanged(object sender, string e)
        {
            variables = sender as SharedVariablesList;
            refreshingRequested = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (refreshingRequested && variables != null)
            {
                //IEnumerable<IGrouping<string, VariableLog.VariableLogEntry>> grouping = variables.VariableLog.VariablesEntries.GroupBy(entry => entry.Variable);
                //foreach (IGrouping<string, VariableLog.VariableLogEntry> gruop in grouping)
                //{
                //    if (!treeView.Nodes.Contains(gruop.Key))
                //        treeView.Nodes.Add(gruop.Key);
                //}
                //chart.Series?.ToList().ForEach(serie =>
                //{
                //    serie.Points.Clear();
                //    variables.VariableLog.VariablesEntries.Where(entry => entry.Variable == serie.Name).ToList().ForEach(point => serie.Points.AddXY(point.TimeSpan, point.Value));
                //});
            }
            refreshingRequested = false;
        }

        private void OnTestStart()
        {
            //SequenceContext.Current.Variables.VariableChangued += OnVariablesChanged;
            variables = null;
        }

        private void OnTestEnd()
        {
           // SequenceContext.Current.Variables.VariableChangued -= OnVariablesChanged;
            variables = null;
        }

        private void treeView_NodeCheckedChanged(object sender, TreeNodeCheckedEventArgs e)
        {
            string key = e.Node.Name as string;
            if (key == null)
                return;
            
            if (e.Node.Checked)
                sc.Post(_ => AddSerie(key), null);
            else
                sc.Post(_ => RemoveSerie(key), null);
        }
        public void AddSerie(string key)
        {
            if (chart.Series.IndexOf(key) < 0)
            {
                var serie = new Series();
                serie.ChartArea = "ChartArea1";
                serie.ChartType = SeriesChartType.FastLine; // Spline
                serie.XValueType = ChartValueType.Time;
                serie.Name = key;
                serie.LegendText = key;
               // variables.VariableLog.VariablesEntries.Where(entry => entry.Variable == key).ToList().ForEach(point => serie.Points.AddXY(point.TimeSpan, point.Value));
                chart.Series.Add(serie);
            }
        }
        public void RemoveSerie(string key)
        {
            if (chart.Series.IndexOf(key) >= 0)
                chart.Series.Remove(chart.Series[key]);      
        }

        #region Buttons 
        private void cmdViewTable_Click(object sender, EventArgs e)
        {
            panelGrid.Collapsed = !panelGrid.Collapsed;
        }
        private void cmdClear_Click(object sender, EventArgs e)
        {
        }
        private void cmdLegend_Click(object sender, EventArgs e)
        {
            chart.Legends[0].Enabled = !chart.Legends[0].Enabled;
        }
        private void cmdTree_Click(object sender, EventArgs e)
        {
            panelTree.Collapsed = !panelTree.Collapsed;
        }
        #endregion buttons

        protected override void Dispose(bool disposing)
        {
            //if (SequenceContext.Current != null)
            //    SequenceContext.Current.Variables.VariableChangued -= OnVariablesChanged;

            AppService app = DI.Resolve<AppService>();
            app.Runner.TestStart -= (_, ee) => OnTestStart();
            app.Runner.TestEnd -= (_, ee) => OnTestEnd();

            timer1.Enabled = false;
            timer1.Dispose();

            if (disposing && (components != null))
                components.Dispose();

            base.Dispose(disposing);
        }

        private void PlotForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Dispose(true);
        }
    }
}
