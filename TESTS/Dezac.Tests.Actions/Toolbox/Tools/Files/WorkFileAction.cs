﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(FileCopyActionDescription))]
    public class FileCopyAction : ActionBase
    {
        [Category("3.1 Source")]
        [Description("Ruta donde está la carpeta a copiar")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SourceFilePath { get; set; }

        [Category("3.1 Source")]
        [Description("Ruta donde está el fichero a copiar")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SourceFolderPath { get; set; }

        [Category("3.1 Target")]
        [Description("Ruta donde se copiará el fichero")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string TargetFolderPath { get; set; }

        [Category("3.1 Target")]
        [Description("Nombre con el que crear el fichero copiado")]
        public string TargetFileName { get; set; }

        [Category("3. Configuration")]
        [Description("Copiar todo el directorio o el fichero")]
        [DefaultValue(typeCopy.File)]
        public typeCopy CopyType { get; set; }

        public enum typeCopy
        {
            Folder,
            File
        }

        public override void Execute()
        {
            try
            {
                if (CopyType == typeCopy.Folder)
                {
                    if (!Directory.Exists(SourceFolderPath))
                        Error().PROCESO.ACTION_EXECUTE.VALOR_INCORRECTO("Carpeta en la propiedad SourcePath no existe").Throw();

                    if (!Directory.Exists(TargetFolderPath))
                        Directory.CreateDirectory(TargetFolderPath);

                    foreach (string dirPath in Directory.GetDirectories(SourceFolderPath, "*", SearchOption.AllDirectories))
                    {
                        var newDirectory = dirPath.Replace(SourceFolderPath, TargetFolderPath);
                        Context.ResultList.Add(string.Format("Create Directory {0} to {1}", SourceFilePath, newDirectory));
                        Logger.InfoFormat("Copy File {0} to {1}", SourceFilePath, newDirectory);
                        Directory.CreateDirectory(newDirectory);
                    }

                    //Copy all the files & Replaces any files with the same name
                    foreach (string newPath in Directory.GetFiles(SourceFolderPath, "*.*", SearchOption.AllDirectories))
                    {
                        var newFile = newPath.Replace(SourceFolderPath, TargetFolderPath);

                        Context.ResultList.Add(string.Format("Copy File {0} to {1}", SourceFilePath, newFile));
                        Logger.InfoFormat("Copy File {0} to {1}", SourceFilePath, newFile);
                        File.Copy(newPath, newFile, true);
                    }
                }
                else
                {
                    var sourceFilePath = VariablesTools.ResolveValue(SourceFilePath).ToString();
                    var targetFileName = VariablesTools.ResolveValue(TargetFileName).ToString();

                    if (!File.Exists(sourceFilePath))
                        Error().PROCESO.ACTION_EXECUTE.VALOR_INCORRECTO("fichero en la propiedad SourcePath no existe").Throw();

                    if (string.IsNullOrEmpty(targetFileName))
                        Error().PROCESO.ACTION_EXECUTE.VALOR_INCORRECTO("Nombre del fichero en la propiedad TargetFileName no puede ser nulo").Throw();

                    Context.ResultList.Add(string.Format("Copy File {0} to {1}", sourceFilePath, targetFileName));
                    Logger.InfoFormat("Copy File {0} to {1}", sourceFilePath, targetFileName);
                    File.Copy(sourceFilePath, targetFileName, true);
                }
            }
            catch (Exception e)
            {
                Error().SOFTWARE.SAURON.FICHERO_NO_ENCONTRADO(e.Message).Throw();
            }
        }

        class FileCopyActionDescription : ActionDescription
        {
            public override string Name { get { return "FileFolderCopy"; } }
            public override string Description { get { return "Action que s irve para copiar un fichero o una carpeta"; } }
            public override string Category { get { return "Tools/Files"; } }
            public override Image Icon { get { return Properties.Resources.report; } }
        }
    }
}
