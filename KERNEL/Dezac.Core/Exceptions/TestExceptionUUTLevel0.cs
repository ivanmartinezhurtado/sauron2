﻿namespace Dezac.Core.Exceptions
{
    public class TestExceptionUUTLevel0
    {
        private TestExceptionUUTLevel1 psl;

        public string[] Category { get { return psl.Category; } set { psl.Category = value; } }

        public string ClassName { get { return psl.ClassName; } set { psl.ClassName = value; } }

        public TestExceptionUUTLevel1 CONFIGURACION
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.CONFIGURACION; return psl; }
        }

        public TestExceptionUUTLevel1 CONSUMO
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.CONSUMO; return psl; }
        }

        public TestExceptionUUTLevel1 COMUNICACIONES
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.COMUNICACIONES; return psl; }
        }

        public TestExceptionUUTLevel1 FIRMWARE
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.FIRMWARE; return psl; }
        }

        public TestExceptionUUTLevel1 HARDWARE
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.HARDWARE; return psl; }
        }


        public TestExceptionUUTLevel1 EEPROM
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.EEPROM; return psl; }
        }

        public TestExceptionUUTLevel1 MEDIDA
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.MEDIDA; return psl; }
        }

        public TestExceptionUUTLevel1 CALIBRACION
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.CALIBRACION; return psl; }
        }

        public TestExceptionUUTLevel1 AJUSTE
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.AJUSTE; return psl; }
        }

        public TestExceptionUUTLevel1 DISPARO
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.DISPARO; return psl; }
        }
        public TestExceptionUUTLevel1 NUMERO_DE_SERIE
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.NUMERO_DE_SERIE; return psl; }
        }
        public TestExceptionUUTLevel1 NUMERO_DE_BASTIDOR
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.NUMERO_DE_BASTIDOR; return psl; }
        }
        public TestExceptionUUTLevel1 MEMORIA
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.MEMORIA; return psl; }
        }
        public TestExceptionUUTLevel1 SIM
        {
            get { psl.SubCategory = ConstantsExceptions.UUTLevel0.SIM; return psl; }
        }

        public TestExceptionUUTLevel0()
        {
            psl = new TestExceptionUUTLevel1();
        }
    }
}
