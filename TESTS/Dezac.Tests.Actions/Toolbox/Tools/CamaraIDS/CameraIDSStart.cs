﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Visio;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using TaskRunner.Model;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(CameraIDSStartDescription))]
    public class CameraIDSStart : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número de serie nuevo")]
        public string SerialNumber { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de exposición")]
        public ActionVariable ExposureTime { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Saturación")]
        [DefaultValue("0")]
        public string Saturation { get; set; }

        [Category("3. Configuration")]
        [Description("Cuanto se quiere enfocar en la cámara")]
        [DefaultValue("1")]
        public string Zoom { get; set; }

        [Category("3. Configuration")]
        [Description("Autoenfoque habilitado")]
        [DefaultValue(false)]
        public bool AutoFocusEnable { get; set; }

        [Category("3. Configuration")]
        [Description("Enfoque manual")]
        [DefaultValue("112")]
        public string FocusManual { get; set; }

        [Category("3. Configuration")]
        [Description("Ganancia")]
        [DefaultValue("0")]
        public string Gain { get; set; }

        [Category("3. Configuration")]
        [Description("AutoGanancia")]
        [DefaultValue(false)]
        public bool AutoGain { get; set; }

        [Category("3. Configuration")]
        [Description("Nivel de oscuridad")]
        [DefaultValue("0")]
        public string BlackLevel { get; set; }

        [Category("3. Configuration")]
        [Description("Offset de color rojo")]
        [DefaultValue("60")]
        public string RedOffset { get; set; }

        [Category("3. Configuration")]
        [Description("Offset de color azul")]
        [DefaultValue("60")]
        public string BlueOffset { get; set; }

        [Category("3. Configuration")]
        [Description("Autobalance de nivel blanco ")]
        [DefaultValue(false)]
        public bool AutoWhiteBalance { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de espera inicial")]
        [DefaultValue("2000")]
        public string WaitTimeInit { get; set; }

        [Category("3. Configuration")]
        [Description("Cuadros por segudo")]
        [DefaultValue("1")]
        public string Framerate { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de píxel")]
        [DefaultValue("18")]
        public string PixelClock { get; set; }

        [Category("3. Configuration")]
        [Description("Sirve por si se han de hacer capturas")]
        public bool StartCapture { get; set; }

        [Category("3. Configuration")]
        [Description("Formato de la imagen")]
        [DefaultValue(IDSFormats._5M)]
        public IDSFormats ImageFormat { get; set; }

        [Category("3. Configuration")]
        [Description("Nitidez")]
        [DefaultValue("0")]
        public string Sharpness { get; set; }

        public override void Execute()
        {
            ImageFormat = ImageFormat == 0 ? IDSFormats._5M : ImageFormat;

            if (SequenceContext.Current.RunningInstances > 1)
                WaitAllTestsAndRunOnlyOnce("_CAMARASTART_", InternalExecute);
            else
                InternalExecute();
        }

        private void InternalExecute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha instanciado el método de inicialización del testbase").Throw();

            if (Sharpness == null)
                Sharpness = "-2";

            var settings = new CameraIDSSettings();

            settings.AutoFocusEnable = AutoFocusEnable;
            settings.ExposureTime = Convert.ToDouble(ExposureTime.Value);
            settings.FocusManual = Convert.ToUInt32(VariablesTools.ResolveValue(FocusManual));
            settings.SerialNumber = VariablesTools.ResolveValue(SerialNumber).ToString() ;
            settings.Saturation = Convert.ToInt32(VariablesTools.ResolveValue(Saturation));
            settings.Zoom = Convert.ToDouble(VariablesTools.ResolveValue(Zoom));
            settings.Gain = Convert.ToInt32(VariablesTools.ResolveValue(Gain));
            settings.AutoGain = AutoGain;
            settings.BlackLevel = Convert.ToInt32(VariablesTools.ResolveValue(BlackLevel));
            settings.Framerate = Convert.ToDouble(VariablesTools.ResolveValue(Framerate));
            settings.PixelClock = Convert.ToInt32(VariablesTools.ResolveValue(PixelClock));
            settings.AutoWhiteBalance = AutoWhiteBalance;
            settings.BlueOffset = Convert.ToInt32(VariablesTools.ResolveValue(BlueOffset));
            settings.RedOffset = Convert.ToInt32(VariablesTools.ResolveValue(RedOffset));
            settings.ImageFormat = (ushort)ImageFormat;
            settings.AutoGainShutter = AutoGain;
            settings.Sharpness = Convert.ToInt32(VariablesTools.ResolveValue(Sharpness)); 
            settings.StartCapture = StartCapture;
            settings.ColorMode = IDSColor.RGB;
            
            testBase.StartCameraIDS(settings, Convert.ToInt32(VariablesTools.ResolveValue(WaitTimeInit)));
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            if (Double.TryParse(ExposureTime.Key, out double aux))
                ExposureTime = ExposureTime.Key.Replace(".", ",");
            if (Double.TryParse(FocusManual, out aux))
                FocusManual = FocusManual.Replace(".", ",");
            if (Double.TryParse(Saturation, out  aux))
                Saturation = Saturation.Replace(".", ",");
            if (Double.TryParse(Zoom, out  aux))
                Zoom = Zoom.Replace(".", ",");
            if (Double.TryParse(Gain, out  aux))
                Gain = Gain.Replace(".", ",");
            if (Double.TryParse(BlackLevel, out  aux))
                BlackLevel = BlackLevel.Replace(".", ",");
            if (Double.TryParse(PixelClock, out aux))
                PixelClock = PixelClock.Replace(".", ",");
            if (Double.TryParse(Framerate, out aux))
                Framerate = Framerate.Replace(".", ",");
            if (Double.TryParse(BlueOffset, out  aux))
                BlueOffset = BlueOffset.Replace(".", ",");
            if (Double.TryParse(RedOffset, out  aux))
                RedOffset = RedOffset.Replace(".", ",");
            if (Double.TryParse(Sharpness, out aux))
                Sharpness = Sharpness.Replace(".", ",");
            if (Double.TryParse(WaitTimeInit, out aux))
                WaitTimeInit = WaitTimeInit.Replace(".", ",");
            if (Double.TryParse(Framerate, out aux))
                Framerate = Framerate.Replace(".", ",");
            if (Double.TryParse(PixelClock, out aux))
                PixelClock = PixelClock.Replace(".", ",");
        }
    }

    public class CameraIDSStartDescription : ActionDescription
    {
        public override string Name { get { return "CameraIDSStart"; } }
        public override string Description { get { return "Inicializa la camara IDS"; } }
        public override string Category { get { return "Tools/CamaraIDS"; } }
        public override Image Icon { get { return Properties.Resources.Devices_camera_web_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
