﻿using Dezac.DLMS;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(CreateDlmsSerialPortDescription))]
    public class CreateDlmsSerialPort : ActionBase
    {
        [Description("Puerto (socket) por ")]
        [Category("3.1 Ethernet Configuracion")]
        public string Port { get; set; }

        [Description("BaudRate velocidad transmision")]
        [Category("3.1 Serie Configuration")]
        [DefaultValue(BpsEnum.Bps115200)]
        public BpsEnum Bps { get; set; }

        [Description("Dirección del cliente")]
        [Category("3.2 Adress Configuration")]
        [DefaultValue(73)]              
        public int ClientAddress { get; set; }

        [Description("Dirección del servidor")]
        [Category("3.2 Adress Configuration")]
        [DefaultValue(1)]
        public int ServerAddress { get; set; }

        [Description("Dirección Física")]
        [Category("3.2 Adress Configuration")]
        [DefaultValue(16)]
        public int PhysicalAddress { get; set; }

        [Description("Contraseña")]
        [Category("3. Configuration")]
        [DefaultValue("qxzbravo")]
        public string Password { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("DlmsDevice")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            var portDevice = Convert.ToByte(GetVariable<string>(Port, Port).Replace("COM", ""));

            var dlms = new DLMSDeviceSerialPort(portDevice, (int)Bps);
            SetVariable(DeviceName ?? "DlmsDevice", dlms);

            dlms.ClientAddress = ClientAddress;
            dlms.password = Password;
            dlms.ServerAddres(ServerAddress, PhysicalAddress);
            dlms.InitializeConnection();
        }
    }

    public class CreateDlmsSerialPortDescription : ActionDescription
    {
        public override string Name { get { return "CreateDlmsSerialPort"; } }
        public override string Description { get { return "Crea una instancia de un dispositivo DLMS serie"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "DisposeDlmsSerialPort:2"; } }
    }

}
