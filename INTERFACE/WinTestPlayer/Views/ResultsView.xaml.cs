﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using TaskRunner.Model;

namespace WinTestPlayer.Views
{
    public partial class ResultsView : Window
    {
        public bool Result { get; private set; }
        public List<ulong?> Bastidores { get; private set; }

        private ResultsView()
        {
            InitializeComponent();
        }

        public ResultsView(RunnerTestsInfo testInfo, bool bastidorEntry)
            : this()
        {
            var rows = testInfo.Contexts.FirstOrDefault().SharedVariables.Get<int>("ROWS");
            var columns = testInfo.Contexts.FirstOrDefault().SharedVariables.Get<int>("COLUMNS");

            if (columns == 0 || rows == 0)
            {
                panelResults.Width = 400;
                panelResults.Height = 300;
            }
            else
            {
                panelResults.Width = 400 * columns;
                panelResults.Height = 300 * rows;
            }

            for (int i = 1; i < testInfo.NumInstances + 1; i++)
            {
                var control = new ResultView(testInfo.Contexts.Where(result => result.TestResult.NumInstance == i).FirstOrDefault(), bastidorEntry);
                panelResults.Children.Add(control);
            }
            Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
            {
                panelResults.Children[0].Focus();
                (panelResults.Children[0] as ResultView).SetFocus();
            }));
        }

        private List<ulong?> GetBastidores()
        {
            var bastidores = new List<ulong?>();
            foreach (object control in panelResults.Children)
                bastidores.Add((control as ResultView).Bastidor);

            return bastidores;
        }

        public bool ValidateView(bool showMessage)
        {
            var sb = new StringBuilder();
            List<ulong?> bastidores = GetBastidores();
            // Si un bastidor falta se entiende que esta deshabilitado
            //for (int i = 0; i < bastidores.Count; i++)
            //{          
            //if (bastidores[i] == null)
            //{
            //    sb.AppendFormat("Falta informar el bastidor nº {0}!\n", (i + 1));
            //    (panelResults.Children[i] as ResultView).BastidorValidated = false;
            //}
            //else
            //    (panelResults.Children[i] as ResultView).BastidorValidated = true;
            //}

            var duplicates = bastidores
              .Select((t, i) => new { Index = i, Text = t })
              .Where(s => s.Text != null)
              .GroupBy(g => g.Text)
              .Where(g => g.Count() > 1);
            if (duplicates.Any())
            {
                sb.Append($"Hay bastidores repetidos!");
                duplicates.ToList().ForEach((group) => group.ToList().ForEach((element) => (panelResults.Children[element.Index] as ResultView).BastidorValidated = false));
            }

            if (sb.Length > 0)
            {
                if (showMessage)
                    MessageBox.Show(sb.ToString(), "Atención", MessageBoxButton.OK, MessageBoxImage.Warning);

                return false;
            }
            Bastidores = bastidores.Select(bastidor => bastidor).ToList();
            return true;
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateView(true))
            {
                Result = true;
                Close();
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = false;
            Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ValidateView(false);
                int currentControlIndex;
                for (currentControlIndex = 0; currentControlIndex < panelResults.Children.Count; currentControlIndex++)
                    if (FocusManager.GetFocusedElement(this).Equals((panelResults.Children[currentControlIndex] as ResultView).textBastidor))
                        break;
                if (currentControlIndex < panelResults.Children.Count - 1)
                    Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
                    {
                        panelResults.Children[currentControlIndex + 1].Focus();
                        (panelResults.Children[currentControlIndex + 1] as ResultView).SetFocus();
                    }));
                else
                {
                    if (ValidateView(true))
                    {
                        Result = true;
                        Close();
                    }
                }
            }
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ValidateView(false);
        }

        private void panelResults_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ValidateView(false);
        }
    }
}
