﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using log4net;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(SSHInitializationActionDescription))]
    public class SSHInitialization : ActionBase
    {
        [DefaultValue("SSH_Device")]
        [Category("3.2 Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }

        [DefaultValue("root")]
        [Category("3.1 Authentication")]
        [Description("Nombre de usuario")]
        public string UserName { get; set; }

        [Category("3.1 Authentication")]
        [DefaultValue("")]
        [Description("Contraseña")]
        public string Password { get; set; }

        [Category("3.1 Authentication")]
        [DefaultValue("100.0.0.1")]
        [Description("Direccion IP del host")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string HostName { get; set; }

        [DefaultValue("")]
        [Category("3.1 Authentication")]
        [Description("fichero de certificado")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string keyFile { get; set; }

        [DefaultValue(false)]
        [Category("3. Configuration")]
        [Description("Si se quiere realizar una conexión")]
        public bool HasConnect { get; set; }

        public override void Execute()
        {
            try
            {
                string host = VariablesTools.ResolveValue(HostName) as string;
                var _ssh = new SSHClient(host, UserName, Password, Logger, keyFile);
                SetVariable(DeviceName.Replace("-","_") ?? "SSH_Device", _ssh);

                if (HasConnect)
                    _ssh.Connect();
            }
            catch (Exception ex)
            {
                Error().PROCESO.ACTION_EXECUTE.EXCEPTION_NOT_CONTROLLED(ex.Message).Throw();
            }
        }
    }

    public class SSHInitializationActionDescription : ActionDescription
    {
        public override string Name { get { return "SSHInit"; } }
        public override string Description { get { return "Inicializa una instancia de las comunicaciones SSH"; } }
        public override string Category { get { return "Tools/Ethernet/SSH"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "SSHFinish:2"; } }
    }
}
