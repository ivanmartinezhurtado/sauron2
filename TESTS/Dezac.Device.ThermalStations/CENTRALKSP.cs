﻿using System.IO.Ports;

namespace Dezac.Device.ThermalStations
{
    [DeviceVersion(1.00)]
    public class CENTRALKSP : DeviceBase
    {
        public CENTRALKSP()
        {
        }

        public CENTRALKSP(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }

        public CENTRALKSP(SerialPort SerialPort)
        {
            SetSerialPort(SerialPort);
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);
        }

        public void SetSerialPort(SerialPort SerialPort)
        {
            Modbus = new ModbusDeviceSerialPort(SerialPort, _logger);        
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Modbus read operations
        
        public ushort ReadMessageFramesReceived()
        {
            return Modbus.ReadRegister((ushort)Registers.FRAME_COUNTER);
        }
        #endregion

        #region Modbus write operations

        public void WriteMessageFrameClear()
        {
            Modbus.WriteMultipleInt32((ushort)Registers.FRAME_COUNTER, 0, 0, 0);
        }

        public void WriteFrequencySelection(RadioFrequency frequencySelected)
        {
            Modbus.WriteSingleCoil((ushort)Registers.FREQUENCY_SELECTION, frequencySelected == RadioFrequency._433MHz ? false : true);
        }

        #endregion

        #region Enumerators
        
            public enum RadioFrequency
            {
                _433MHz,
                _434MHz
            }

        public enum Registers
        {
            FRAME_COUNTER = 0x044C,
            FREQUENCY_SELECTION = 0x07D4
        }
        #endregion
    }
}