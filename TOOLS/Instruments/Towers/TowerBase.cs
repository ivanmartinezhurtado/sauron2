﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Instruments.Actuators;
using Instruments.Auxiliars;
using Instruments.IO;
using Instruments.Measure;
using Instruments.PowerSource;
using Instruments.Router;
using Instruments.Utility;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Instruments.Towers
{
    public class TowerBase<T> : IDisposable, ITower
    {
        internal static readonly ILog _logger = LogManager.GetLogger(typeof(T));
        private int PortMtePowerSource = 1;
        private int PortPte = 1;       
        private int PortMteWattmeter = 2;
        private int PortCvmk2CvmMini = 8;
        private int PortLambdaPowerSource = 12;
        private int PerifericNumberCvmMiniChroma = 1;
        private int PerifericNumberCvmB100Voltage = 2;
        private int PerifericNumberCvmB100Current = 5;
        private int PerifericNumberCvmMiniMTE = 6;
        private int PerifericNumberCvmK2 = 2;
        private int Portfluke183 = 2;
        private int PortMetrix3293 = 2;
        private byte PortLAC25 = 9;         
        private const int Adresshp34401A = 3;
        private const int Adresshp53131A = 2;
        private const int AddressSignalGenerator = 4;
        private const int AdressFluke = 6;
        private const int POWER_24VDC = 14;
        private const int POWER_EV = 4;
        private const int BLOQ_SECURITY = 23;
        private const int VOLTAGE_CIRCUIT_L1 = 24;
        private const int VOLTAGE_CIRCUIT_L2 = 25;
        private const int VOLTAGE_CIRCUIT_L3 = 26;
        private const int VOLTAGE_CIRCUIT_N = 27;
        private const int CURRENT_CIRCUIT_L1 = 28;
        private const int CURRENT_CIRCUIT_L2 = 29;
        private const int CURRENT_CIRCUIT_L3 = 30;
        private const int MEASURE_CURRENT_MUX = 39;
        private const int MEASURE_VOLTAGE_MUX = 44;
        private const int CLOSE_CURRENTS_UNION_MTE = 45;
        private const int IN_CURRENT_MUX_M9120A = 32;

        private ZERA zeraPowerSource;
        private MTEPPS mtePowerSource;
        private MTEPRS mteWatmeter;
        private FLUKE6003A flukePowerSource;
        private PCI7296 io;
        private Chroma chroma;
        private CVMMINITower cvmminiChroma;
        private CVMMINITower cvmminiMTE;
        private CVMB100Tower cvmB100Hi;
        private CVMB100Tower cvmB100Low;
        private CVMK2Tower cvmK2;
        private HP34401A hp34401A;
        private HP53131A hp53131A;
        private Lambda Lambda;
        private PTE Pte;
        private FLUKE183 fluke183;
        private MTX3293 metrix3293;
        protected IMux mux;
        private ISignalGenerator signalGenerator;
        private IPowerSourceIII powerSourceIII;
        private M9183A multimeterM9183A;
        private SerialPort sp;
        private SMAC_LAC25 actuator_LAC25;
        private USB4761 usbIO;
        private string TypePowerSourceTrifasica;

        public bool IsZERAInitialized { get; set; }
        public bool IsMTEPSInitialized { get; set; }
        public bool IsMTEWATInitialized { get; set; }
        public bool IsFLUKEInitialized { get; set; }
        public bool IsChromaInitialized { get; set; }
        public bool IsLambdaInitialized { get; set; }
        public bool IsHP34401AInitialized { get; set; }
        public bool IsHP53131AInitialized { get; set; }
        public bool IsSignalGeneratorInitialized { get; set; }
        public bool IsPTEInitialized { get; set; }
        public bool IspowerSourceM9111AInitialized { get; set; }
        public bool Isactuator_LAC25Initialized { get; set; }

        public int? PortZera { get; set; }
        public int? PortMte { get; set; }
        public int? PortLambda { get; set; }
        public int? PortMteWattimetro { get; set; }

        //*********************************************************
        [JsonIgnore]
        public IPowerSourceIII PowerSourceIII
        {
            get
            {
                if (powerSourceIII == null)
                {
                    if (!string.IsNullOrEmpty(TypePowerSourceTrifasica))
                        switch (TypePowerSourceTrifasica)
                        {
                            case "MTE":
                                powerSourceIII = MTEPS;
                                break;
                            case "FLUKE":
                                powerSourceIII = FLUKE;
                                break;
                            case "ZERA":
                                powerSourceIII = ZERA;
                                break;
                        }
                    else
                    {
                        _logger.InfoFormat("Inicializmos powerSourceIII");

                         powerSourceIII = MTEPS;
                        if (powerSourceIII != null)
                            TypePowerSourceTrifasica = "MTE";
                        else
                        {
                            powerSourceIII = FLUKE;
                            TypePowerSourceTrifasica = "FLUKE";
                        }               
                    }
                }
                return powerSourceIII;
            }
            set
            {
                powerSourceIII = value;
            }
        }
        [JsonIgnore]
        public ZERA ZERA
        {
            get
            {
                if (zeraPowerSource == null)
                {
                    if (PortZera != null)
                        PortMtePowerSource = PortZera.Value;

                    _logger.InfoFormat("Inicializmos ZERA en el puerto: {0}", PortMtePowerSource);
                    zeraPowerSource = new ZERA();
                    zeraPowerSource.PortCom = (byte)PortMtePowerSource;
                    IsZERAInitialized = true;
                }

                return zeraPowerSource;
            }
        }
        [JsonIgnore]
        public MTEPPS MTEPS
        {
            get
            {
                if (mtePowerSource == null)
                {
                    if (PortMte != null)
                        PortMtePowerSource = PortMte.Value;
                    try
                    {
                        _logger.InfoFormat("Inicializmos MTEPS en el puerto: {0}", PortMtePowerSource);
                        mtePowerSource = new MTEPPS();
                        mtePowerSource.PortCom = (byte)PortMtePowerSource;
                        mtePowerSource.GetIdentification();
                        IsMTEPSInitialized = true;
                    }
                    catch
                    {
                        mtePowerSource.Dispose();
                        return null;
                    }
                }

                return mtePowerSource;
            }
        }
        [JsonIgnore]
        public MTEPRS MTEWAT
        {
            get
            {
                if (mteWatmeter == null)
                {
                    try
                    {
                        if (PortMteWattimetro != null)
                            PortMteWattmeter = PortMteWattimetro.Value;

                        _logger.InfoFormat("Inicializmos MTEWAT en el puerto: {0}", PortMteWattmeter);
                        mteWatmeter = new MTEPRS();
                        mteWatmeter.PortCom = (byte)PortMteWattmeter;
                        mteWatmeter.ModeFormat1 = true;
                        IsMTEWATInitialized = true;
                    }
                    catch (Exception ex)
                    {
                        mteWatmeter.Dispose();
                        mteWatmeter = null;
                    }
                    if (mteWatmeter == null)
                    {
                        var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Caption LIKE '%Dispositivo serie USB%'");
                        foreach (ManagementObject queryObj in searcher.Get())
                            if (queryObj["Caption"] != null)
                            {
                                _logger.InfoFormat("Inicializmos MTE PRS600.3");
                                var comPortFind = queryObj["Caption"].ToString().Replace("Dispositivo serie USB", "");
                                var posInit = (comPortFind.IndexOf("(COM"));
                                if (posInit != -1)
                                {
                                    var stringSearch = comPortFind.Substring(posInit);
                                    var comportInfo = stringSearch.Replace(")", "").Replace("(", "").Replace("COM","");
                                    mteWatmeter = new MTEPRS();
                                    mteWatmeter.PortCom = Convert.ToByte(comportInfo);
                                    mteWatmeter.ModeFormat1 = true;
                                    IsMTEWATInitialized = true;
                                    break;
                                }else
                                    TestException.Create().HARDWARE.INSTRUMENTOS.DETECCION_INSTRUMENTO("No se ha encontrado COM para el watimetro MTE PRS600.3").Throw();
                            }
                    }
                    if (mteWatmeter == null)
                        TestException.Create().HARDWARE.INSTRUMENTOS.DETECCION_INSTRUMENTO("No se ha encontrado el watimetro MTE PRS, compruebe su conexión").Throw();
                }

                return mteWatmeter;
            }
        }

        [JsonIgnore]
        public FLUKE6003A FLUKE
        {
            get
            {
                if (flukePowerSource == null)
                {
                    _logger.InfoFormat("Inicializmos FLUKE6003A en la adress: {0}", AdressFluke);
                    flukePowerSource = new FLUKE6003A();
                    flukePowerSource.PortAddress = (byte)AdressFluke;
                    flukePowerSource.SetOutputCurrentMode(OutputCurrentModes.CURRENT);
                    flukePowerSource.SetOuputSynchronism(FLUKE6003A.OutputFrequencySynchronizationModes.LINE);
                    IsFLUKEInitialized = true;
                }

                return flukePowerSource;
            }
        }
        [JsonIgnore]
        public Chroma Chroma
        {
            get
            {
                if (chroma == null)
                {
                    _logger.InfoFormat("Inicializmos CHROMA");
                    chroma = new Chroma();
                    chroma.GetIdentification();
                    IsChromaInitialized = true;
                }

                return chroma;    
            }
        }
        [JsonIgnore]
        public Lambda LAMBDA
        {
            get
            {
                if (PortLambda.HasValue)
                    PortLambdaPowerSource = PortLambda.Value;

                if (Lambda == null)
                   Lambda = new Lambda();
                
                if (!IsLambdaInitialized)
                {
                   Lambda.PortCom = (byte)PortLambdaPowerSource;         
                   _logger.InfoFormat("Inicializamos Lambda en el puerto: {0}", Lambda.PortCom);
                    Lambda.Initialize();
                    IsLambdaInitialized = true;
                }
                return Lambda;
            }
        }
        [JsonIgnore]
        public PTE PTE
        {
            get
            {
                if (Pte == null)
                {
                    _logger.InfoFormat("Inicializmos PTE en el puerto: {0}", PortPte);
                    Pte = new PTE();
                    Pte.PortCom = (byte)PortPte;
                    IsPTEInitialized = true;
                }

                return Pte;
            }
        }
        [JsonIgnore]
        public CVMMINITower CvmminiChroma
        {
            get
            {
                if (cvmminiChroma == null)
                {
                    _logger.InfoFormat("Inicializmos CVMMINITowerCHROMA en el puerto: {0} periderico: {1}", ModbusSerialPort, PerifericNumberCvmMiniChroma);
                    cvmminiChroma = new CVMMINITower(ModbusSerialPort);
                    cvmminiChroma.Modbus.PerifericNumber = (byte)PerifericNumberCvmMiniChroma;
                    cvmminiChroma.SetupDefault(1, 1, 5, CVMMINITower.VectorHardwareVoltageInputs.Primary300V, CVMMINITower.VectorHardwareCurrentInputs.Primary5A);
                }
                return cvmminiChroma;
            }
        }
        [JsonIgnore]
        public CVMMINITower CvmminiMTE
        {
            get
            {
                if (cvmminiMTE == null)
                {
                    _logger.InfoFormat("Inicializmos CVMMINITowerMTE en el puerto: {0} periderico: {1}", ModbusSerialPort, PerifericNumberCvmMiniMTE);
                    cvmminiMTE = new CVMMINITower(ModbusSerialPort);
                    cvmminiMTE.Modbus.PerifericNumber = (byte)PerifericNumberCvmMiniMTE;
                    cvmminiMTE.SetupDefault(1, 1, 5, CVMMINITower.VectorHardwareVoltageInputs.Primary300V, CVMMINITower.VectorHardwareCurrentInputs.Primary5A);
                }
                return cvmminiMTE;
            }
        }
        [JsonIgnore]
        public CVMB100Tower CvmB100Hi
        {
            get
            {
                if (cvmB100Hi == null)
                {
                    _logger.InfoFormat("Inicializmos CvmB100Hi en el puerto: {0} periderico: {1}", ModbusSerialPort, PerifericNumberCvmB100Voltage);
                    cvmB100Hi = new CVMB100Tower(ModbusSerialPort);
                    cvmB100Hi.Modbus.PerifericNumber = (byte)PerifericNumberCvmB100Voltage;
                }
                return cvmB100Hi;
            }
        }
        [JsonIgnore]
        public CVMB100Tower CvmB100Low
        {
            get
            {
                if (cvmB100Low == null)
                {
                    _logger.InfoFormat("Inicializmos CvmB100Low en el puerto: {0} periderico: {1}", ModbusSerialPort, PerifericNumberCvmB100Current);
                    cvmB100Low = new CVMB100Tower(ModbusSerialPort);
                    cvmB100Low.Modbus.PerifericNumber = (byte)PerifericNumberCvmB100Current;
                }
                return cvmB100Low;
            }
        }
        [JsonIgnore]
        public CVMK2Tower CvmK2
        {
            get
            {
                if (cvmK2 == null)
                {
                    _logger.InfoFormat("Inicializmos CvmK2 en el puerto: {0} periderico: {1}", ModbusSerialPort, PerifericNumberCvmK2);
                    cvmK2 = new CVMK2Tower(ModbusSerialPort);
                    cvmK2.Modbus.PerifericNumber = (byte)PerifericNumberCvmK2;
                }
                return cvmK2;
            }
        }
        [JsonIgnore]
        public SerialPort ModbusSerialPort
        {
            get
            {
                if (sp == null)
                {
                    _logger.InfoFormat("Inicializmos ModbusSerialPort en el puerto: {0}", PortCvmk2CvmMini);
                    sp = new SerialPort("COM" + PortCvmk2CvmMini, 9600, Parity.None, 8, StopBits.One);
                }

                return sp;
            }
        }
        [JsonIgnore]
        public HP34401A HP34401A
        {
            get
            {
                if (hp34401A == null)
                {
                    _logger.InfoFormat("Inicializmos Multimetro HP34401A en la address: {0} ", Adresshp34401A);
                    hp34401A = new HP34401A();
                    hp34401A.PortAdress = (byte)Adresshp34401A;
                    IsHP34401AInitialized = true;
                }
                return hp34401A;
            }
        }
        [JsonIgnore]
        public HP53131A HP53131A
        {
            get
            {
                if (hp53131A == null)
                {
                    _logger.InfoFormat("Inicializmos Frecuecnimetro HP53131A en la address: {0} ", Adresshp53131A);
                    hp53131A = new HP53131A();
                    hp53131A.PortAdress = (byte)Adresshp53131A;
                    IsHP53131AInitialized = true;
                }
                return hp53131A;
            }
        }
        [JsonIgnore]
        public FLUKE183 Fluke183
        {
            get
            {
                if (fluke183 == null)
                {
                    _logger.InfoFormat("Inicializmos Multimetro FLUKE183 en la puerto: {0} ", Portfluke183);
                    fluke183 = new FLUKE183();
                    fluke183.PortCom = (byte)Portfluke183;
                }

                return fluke183;
            }
        }
        [JsonIgnore]
        public MTX3293 Metrix3293
        {
            get
            {
                if (metrix3293 == null)
                {
                    _logger.InfoFormat("Inicializmos Multimetro MTX3293 en la puerto: {0} ", PortMetrix3293);
                    metrix3293 = new MTX3293();
                    metrix3293.PortCom = (byte)PortMetrix3293;
                }

                return metrix3293;
            }
        }
        [JsonIgnore]
        public PCI7296 IO
        {
            get
            {
                if (io == null)
                {
                    _logger.InfoFormat("Inicializmos trageta I/O PCI7296");
                    io = new PCI7296();
                }

                return io;
            }
        }
        [JsonIgnore]
        public M9183A MultimeterM9183A
        {
            get
            {
                if (multimeterM9183A == null)
                {
                    _logger.InfoFormat("Inicializmos multimetro M9183A de Keysight");
                    multimeterM9183A = new M9183A();
                    multimeterM9183A.connect();
                }

                return multimeterM9183A;
            }
        }
        [JsonIgnore]
        public USB4761 UsbIO
        {
            get
            {
                if (usbIO == null)
                {
                    _logger.InfoFormat("Inicializmos trageta I/O USB4761");
                    usbIO = new USB4761();
                }

                return usbIO;
            }
        }
        [JsonIgnore]
        public SMAC_LAC25 Actuator_LAC25
        {
            get
            {
                if (actuator_LAC25 == null)
                {
                    actuator_LAC25 = new SMAC_LAC25(PortLAC25);
                    Isactuator_LAC25Initialized = true;
                }

                return actuator_LAC25;
            }
        }
        [JsonIgnore]
        public ISignalGenerator SignalGenerator
        {
            get
            {
                if (signalGenerator == null)
                {
                    _logger.InfoFormat("Inicializmos generador Funciones  KeySight33500");
                    signalGenerator = new KeySight33500();
                    signalGenerator.portAddress = (byte)AddressSignalGenerator;
                    try
                    {
                        if (!signalGenerator.GetIdentification().Contains("33522B"))
                        {
                            signalGenerator.Dispose();
                            _logger.InfoFormat("No se ha encontrado el generador de funciones Keysight");
                            _logger.InfoFormat("Inicializmos generador Funciones  Yokogawa");
                            signalGenerator = new Yokogawa();
                        }
                    }
                    catch
                    {
                        signalGenerator.Dispose();
                        _logger.InfoFormat("No se ha encontrado el generador de funciones Keysight");
                        _logger.InfoFormat("Inicializmos generador Funciones  Yokogawa");
                        signalGenerator = new Yokogawa();
                        if (!signalGenerator.GetIdentification().Contains("Yokowaga"))
                            Error().HARDWARE.TORRE.DETECCION_INSTRUMENTO("Yokowaga").Throw();
                       // throw new Exception("Error no se ha encontrado un Generador de funciones a instanciar");
                    }
                }

                IsSignalGeneratorInitialized = true;
                return signalGenerator;
            }
        }
        [JsonIgnore]
        public virtual IMux MUX
        {
            get
            {
                if (mux == null)
                    try
                    {
                        var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Caption LIKE '%M9120A%'");
                        foreach (ManagementObject queryObj in searcher.Get())
                            if (queryObj["Caption"] != null)
                            {
                                _logger.InfoFormat("Inicializmos MUX M9120A de Keysight");
                                mux = new M9120A();
                                return mux;
                            }

                        if (mux == null)
                        {
                            _logger.InfoFormat("No se ha encontrdo la MUX M9120A de Keysight");
                            _logger.InfoFormat("Inicializmos MUX de I/O con la PCI7296");
                            mux = new IOMux(IO.DO);
                        }
                    }
                    catch (Exception ex)
                    {
                        TestException.Create().HARDWARE.PC.DETECCION_INSTRUMENTO("MUX M9120A o PCI7296").Throw();
                    }

                return mux;
            }
            set
            {
                mux = value;
            }

        } 

        //*******************************************************

        public void Active24VDC()
        {
            _logger.Info("Activando 24VDC...");
            IO.DO[POWER_24VDC] = true;
            ActiveCurrentHighLowUnionMTE(false);
        }
        public void ActiveVoltageCircuit(bool l1 = true, bool l2 = true, bool l3 = true, bool neutro = true)
        {
            _logger.InfoFormat("Configurando circuito de voltage: L1={0}, L2={0}, L3={0}, N={0}", l1, l2, l3, neutro);

            IO.DO[VOLTAGE_CIRCUIT_L1] = l1;
            IO.DO[VOLTAGE_CIRCUIT_L2] = l2;
            IO.DO[VOLTAGE_CIRCUIT_L3] = l3;
            IO.DO[VOLTAGE_CIRCUIT_N] = neutro;
        }
        public void ActiveCurrentCircuit(bool l1, bool l2, bool l3)
        {
            _logger.InfoFormat("Configurando circuito de corriente: L1={0}, L2={0}, L3={0}", l1, l2, l3);

            IO.DO[CURRENT_CIRCUIT_L1] = l1;
            IO.DO[CURRENT_CIRCUIT_L2] = l2;
            IO.DO[CURRENT_CIRCUIT_L3] = l3;
        }
        public void ActiveCurrentHighLowUnionMTE(bool value = true)
        {
            if (!value)
            {
                _logger.Info(string.Format("Abrimos rele union corrientes MTE (OUT {0})", CLOSE_CURRENTS_UNION_MTE));
                IO.DO.Off(CLOSE_CURRENTS_UNION_MTE);
            }else
            {
                _logger.Info(string.Format("Cerramos rele union corrientes MTE (OUT {0})", CLOSE_CURRENTS_UNION_MTE));
                IO.DO.On(CLOSE_CURRENTS_UNION_MTE);
            }
        }
        public void ActiveElectroValvule()
        {
            IO.DO.On(POWER_EV);
        }
        public void WaitBloqSecurity(int TimeOut)
        {
            if (!IO.DI.WaitOn(BLOQ_SECURITY, TimeOut))
                Error().HARDWARE.UTILLAJE.BLOQUE_SEGURIDAD().Throw();
        }
        [Browsable(false)]
        public void OnOutAndWaitInput(int Out, int TimeOut, params int[] In)
        {
            IO.DO.On(Out);

            if (!IO.DI.WaitAllOn(TimeOut, In))
            {
                string inputMsg = "";
                foreach (int inputsArray in In)
                    inputMsg += inputsArray.ToString() + "  ";
                Error().HARDWARE.TORRE.ENTRADAS_DIGITALES(inputMsg).Throw();
            }
        }
        [Browsable(false)]
        public void OnOutAndWaitInput(int Out, int TimeOut, int In, string message = null)
        {
            IO.DO.On(Out);

            if (!IO.DI.WaitOn(In, TimeOut))
                Error().HARDWARE.TORRE.ENTRADAS_DIGITALES(In.ToString()).Throw();
        }
        [Browsable(false)]
        public void OffOutAndWaitInput(int Out, int TimeOut, params int[] In)
        {
            IO.DO.Off(Out);

            if (!IO.DI.WaitAllOff(TimeOut, In))
            {
                string inputMsg = "";
                foreach (int inputsArray in In)
                    inputMsg += inputsArray.ToString() + "  ";
                Error().HARDWARE.TORRE.ENTRADAS_DIGITALES(inputMsg).Throw();
            }
        }
        [Browsable(false)]
        public void OffOutAndWaitInput(int Out, int TimeOut, int In)
        {
            IO.DO.Off(Out);

            if (!IO.DI.WaitAllOff(TimeOut, In))
                Error().HARDWARE.TORRE.ENTRADAS_DIGITALES(In.ToString()).Throw();
        }
        [Browsable(false)]
        public double MeasureMultimeter(MagnitudsMultimeter magnitud, int waitTimeMeasure=500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            return MeasureMultimeter(InputMuxEnum.NOTHING, magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);
        }
        [Browsable(false)]
        public double MeasureMultimeter(InputMuxEnum input, MagnitudsMultimeter magnitud, int waitTimeMeasure=500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            MagnitudToMeasure param = new MagnitudToMeasure(magnitud);
            return MeasureMultimeter(input, param, waitTimeMeasure, preCondition, postCondition, whitOutIO);
        }
        [Browsable(false)]
        public virtual double MeasureMultimeter(InputMuxEnum input, MagnitudToMeasure magnitud, int waitTimeMeasure =500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            if(whitOutIO)
               return InternalMeasureMultimeter34410A(input, magnitud.Magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);

            if (MUX.GetType() != typeof(M9120A))
                return InternalMeasureMultimeter34410A(input, magnitud.Magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);
            else
                return InternalMeasureMultimeterM9183A(input, magnitud, waitTimeMeasure, preCondition, postCondition, whitOutIO);
        }
        internal double InternalMeasureMultimeterM9183A(InputMuxEnum input, MagnitudToMeasure magnitud, int waitTimeMeasure, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            if (!whitOutIO)
                IO.DO.Off(MEASURE_CURRENT_MUX, MEASURE_VOLTAGE_MUX);

            if (magnitud.Magnitud == MagnitudsMultimeter.AmpAC || magnitud.Magnitud == MagnitudsMultimeter.AmpDC)
            {
                MUX.Connect(IN_CURRENT_MUX_M9120A, Outputs.Aux);
                if (!whitOutIO)
                {
                    IO.DO.On(MEASURE_CURRENT_MUX);
                    Thread.Sleep(100);
                }
            }
            else
            {
                if (input != InputMuxEnum.NOTHING)
                    MUX.Connect((byte)input, Outputs.Multimetro);
            }

            if (preCondition != null)
                preCondition();

            MultimeterM9183A.ConfigurePowerLineFrequency(magnitud.Frequency);
            MultimeterM9183A.ConfigureFunction(magnitud.Magnitud.ToString(), Math.Abs(magnitud.Rango), (double)magnitud.DigitosPrecision);
            MultimeterM9183A.ConfigureNPLC(magnitud.Magnitud.ToString(), magnitud.NPLC);
            var result = MultimeterM9183A.ReadMeasure(waitTimeMeasure, magnitud.NumMuestras);

            if (!whitOutIO)
                IO.DO.Off(MEASURE_CURRENT_MUX);

            if (postCondition != null)
                postCondition();


            if (magnitud.Magnitud == MagnitudsMultimeter.AmpAC || magnitud.Magnitud == MagnitudsMultimeter.AmpDC)
                MUX.Disconnect(IN_CURRENT_MUX_M9120A, Outputs.Aux);
            else
            {
                if (input != InputMuxEnum.NOTHING)
                    MUX.Disconnect((byte)input, Outputs.Multimetro);
            }

            return result;
        }
        internal double InternalMeasureMultimeter34410A(InputMuxEnum input, MagnitudsMultimeter magnitud, int waitTimeMeasure = 500, Action preCondition = null, Action postCondition = null, bool whitOutIO = false)
        {
            if (input != InputMuxEnum.NOTHING)
            {
                MUX.Reset();
                MUX.Connect((byte)input, Outputs.Multimetro);
            }

            Result<double> dblMedida = 0;

            HP34401A.SCPI.Reset.Command();
            HP34401A.SCPI.Clear.Command();

            if (HP34401A.WriteCommand("ROUT:TERM?").Value != "REAR")
                Error().HARDWARE.TORRE.CONFIGURACION_INSTRUMENTO("HP34401A_BOTON_REAR").Throw();

            if (!whitOutIO)
            {
                IO.DO[MEASURE_VOLTAGE_MUX] = false;
                IO.DO[MEASURE_CURRENT_MUX] = false;
            }

            if (preCondition != null)
                preCondition();

            switch (magnitud)
            {
                case MagnitudsMultimeter.AmpAC:

                    if (!whitOutIO)
                        IO.DO[MEASURE_CURRENT_MUX] = true;

                    Thread.Sleep(waitTimeMeasure);
                    dblMedida = HP34401A.SCPI.MEASure.CURRent.AC().QueryNumber();
                    break;
                case MagnitudsMultimeter.AmpDC:

                    if (!whitOutIO)
                        IO.DO[MEASURE_CURRENT_MUX] = true;

                    Thread.Sleep(waitTimeMeasure);
                    dblMedida = HP34401A.SCPI.MEASure.CURRent.DC().QueryNumber();
                    break;
                case MagnitudsMultimeter.Resistencia:

                    if (!whitOutIO)
                        IO.DO[MEASURE_VOLTAGE_MUX] = true;

                    Thread.Sleep(waitTimeMeasure);
                    dblMedida = HP34401A.SCPI.MEASure.RESistance().QueryNumber();
                    break;
                case MagnitudsMultimeter.VoltAC:

                    if (!whitOutIO)
                        IO.DO[MEASURE_VOLTAGE_MUX] = true;

                    Thread.Sleep(waitTimeMeasure);
                    dblMedida = HP34401A.SCPI.MEASure.VOLTage.AC().QueryNumber();
                    break;
                case MagnitudsMultimeter.VoltDC:

                    if (!whitOutIO)
                        IO.DO[MEASURE_VOLTAGE_MUX] = true;

                    Thread.Sleep(waitTimeMeasure);
                    dblMedida = HP34401A.SCPI.MEASure.VOLTage.DC().QueryNumber();
                    break;
            }

            if (postCondition != null)
                postCondition();

            if (!whitOutIO)
            {
                IO.DO[MEASURE_VOLTAGE_MUX] = false;
                IO.DO[MEASURE_CURRENT_MUX] = false;
            }

            if (dblMedida.Error)
                Error().HARDWARE.TORRE.MEDIDA_INSTRUMENTO(string.Format("HP34401A_{0}", magnitud.ToString())).Throw();

            return dblMedida.Value;
        }
        [Browsable(false)]
        public double MeasureWithFrecuency(int TimeOut = 10000, HP53131A.Variables MagnitudToMeasure = HP53131A.Variables.FREQ, HP53131A.Channels channel = HP53131A.Channels.CH1, Action preCondition = null, Action postCondition = null)
        {
            return MeasureWithFrecuency(InputMuxEnum.NOTHING, TimeOut, MagnitudToMeasure, channel, preCondition, postCondition);
        }
        public double MeasureWithFrecuency(InputMuxEnum input, int TimeOut = 10000, HP53131A.Variables MagnitudToMeasure = HP53131A.Variables.FREQ, HP53131A.Channels channel = HP53131A.Channels.CH1, Action preCondition = null, Action postCondition = null)
        {
            if (input != InputMuxEnum.NOTHING)
            {
                MUX.Reset();
                MUX.Connect((byte)input, channel == HP53131A.Channels.CH1 ? Outputs.FreCH1 : Outputs.FreCH2);
            }

            if (preCondition != null)
                preCondition();

            var result = HP53131A.RunMeasure(MagnitudToMeasure, TimeOut, channel);

            if (postCondition != null)
                postCondition();

            if (input != InputMuxEnum.NOTHING)
                MUX.Disconnect((byte)input, channel == HP53131A.Channels.CH1 ? Outputs.FreCH1 : Outputs.FreCH2);

            if (result.Error)
                Error().HARDWARE.TORRE.MEDIDA_INSTRUMENTO(string.Format("HP53131A_{0}", MagnitudToMeasure.ToString())).Throw();

            return result.Value;
        }
        public void ShutdownSources()
        {
            _logger.Info("Apagando las fuentes instanciadas de la torre");

            try
            {
                var tasks = new List<Task>();

                if (flukePowerSource != null)
                    if (flukePowerSource.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => flukePowerSource.ApplyOffAndWaitStabilisation()));

                if (mtePowerSource != null)
                    if (mtePowerSource.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => mtePowerSource.ApplyOffAndWaitStabilisation()));

                if (zeraPowerSource != null)
                    if (zeraPowerSource.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => zeraPowerSource.ApplyOffAndWaitStabilisation()));

                if (chroma != null)
                    if (Chroma.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => Chroma.ApplyOffAndWaitStabilisation()));

                if (Lambda != null)
                    if (Lambda.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => Lambda.ApplyOffAndWaitStabilisation()));

                if (Pte != null)
                    tasks.Add(Task.Factory.StartNew(() => 
                    {
                        Pte.ApplyOff(); //No podemos saber la estbilizacion de la PTE y no tenemos trama RX asi que reintentamos por si no se apagara
                        Pte.ApplyOff();
                    }));


                //if (powerSourceM9111A != null)
                //{
                //    tasks.Add(Task.Factory.StartNew(() =>
                //    {
                //        powerSourceM9111A.ApplyOFF();
                //    }));
                //}

                if (tasks.Any())
                    Task.WaitAll(tasks.ToArray());

            } catch (AggregateException agr)
            {
                var exgr = agr.Flatten();

                _logger.WarnFormat("Excepción en el ShutdownSources by : {0}", exgr.Message);
            }
            catch (Exception ex)
            {
                _logger.WarnFormat("Excepción en el ShutdownSources by : {0}", ex.Message);
            }
        }
        public void ShutdownSourcesQuick()
        {
            _logger.Info("Apagando las fuentes instanciadas de la torre");

            try
            {
                var tasks = new List<Task>();

                if (flukePowerSource != null)
                    if (flukePowerSource.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => flukePowerSource.ApplyOff()));

                if (mtePowerSource != null)
                    if (mtePowerSource.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => mtePowerSource.ApplyOff()));

                if (zeraPowerSource != null)
                    if (zeraPowerSource.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => zeraPowerSource.ApplyOff()));

                if (chroma != null)
                    if (Chroma.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => Chroma.ApplyOff()));

                if (Lambda != null)
                    if (Lambda.IsOn)
                        tasks.Add(Task.Factory.StartNew(() => Lambda.ApplyOff()));

                if (Pte != null)
                    tasks.Add(Task.Factory.StartNew(() =>
                    {
                        Pte.ApplyOff(); //No podemos saber la estbilizacion de la PTE y no tenemos trama RX asi que reintentamos por si no se apagara
                        Pte.ApplyOff();
                    }));

                if (tasks.Any())
                    Task.WaitAll(tasks.ToArray());

            }
            catch (AggregateException agr)
            {
                var exgr = agr.Flatten();

                _logger.WarnFormat("Excepción en el ShutdownSources by : {0}", exgr.Message);
            }
            catch (Exception ex)
            {
                _logger.WarnFormat("Excepción en el ShutdownSources by : {0}", ex.Message);
            }
        }
        [Browsable(false)]
        public void FreeResources(bool whitOutIO)
        {
            _logger.Info("liberando recursos de la torre...");

            if (powerSourceIII != null)
            {
                _logger.InfoFormat("Liberamos powerSourceIII ...");
                powerSourceIII = null;
            }

            if (zeraPowerSource != null)
            {
                _logger.InfoFormat("Liberamos ZERAPowerSource ... en el puerto: {0} ", zeraPowerSource.PortCom);
                zeraPowerSource.Dispose();
                zeraPowerSource = null;
                IsZERAInitialized = false;
            }

            if (mtePowerSource != null)
            {
                _logger.InfoFormat("Liberamos MTEPowerSource ... en el puerto: {0} ", mtePowerSource.PortCom);
                mtePowerSource.Dispose();
                mtePowerSource = null;
                IsMTEPSInitialized = false;
            }

            if (flukePowerSource != null)
            {
                _logger.InfoFormat("Liberamos FlukePowerSource ... en la direccion GPIB: {0} ", flukePowerSource.PortAddress);
                flukePowerSource.Dispose();
                flukePowerSource = null;
                IsFLUKEInitialized = false;
            }

            if (mteWatmeter != null)
            {
                if (!mteWatmeter.ByEthernet)
                    _logger.InfoFormat("Liberamos MTEWatimetro ... en el puerto: {0} ", mteWatmeter.PortCom);
                else
                    _logger.InfoFormat("Liberamos MTEWatimetro ... en la IP: {0} ", mteWatmeter.IP);

                mteWatmeter.Dispose();
                mteWatmeter = null;
                IsMTEWATInitialized = false;
            }

            if (Lambda != null)
            {
                _logger.InfoFormat("Liberamos LAMBDA ... en el puerto: {0} ", Lambda.PortCom);
                Lambda.Dispose();
                Lambda = null;
                IsLambdaInitialized = false;
            }

            if (chroma != null)
            {
                _logger.InfoFormat("Liberamos CHROMA");
                chroma.Dispose();
                chroma = null;
                IsChromaInitialized = false;
            }

            if (Pte != null)
            {
                _logger.InfoFormat("Liberamos PTE ... en el puerto: {0} ", Pte.PortCom);
                Pte.Dispose();
                Pte = null;
                IsPTEInitialized = false;
            }

            if (signalGenerator != null)
            {
                _logger.InfoFormat("Liberamos SIGNALGENERATOR ... en la direccion GPIB: {0} ", signalGenerator.portAddress);
                signalGenerator.Dispose();
                signalGenerator = null;
                IsSignalGeneratorInitialized = false;
            }

            if (hp34401A != null)
            {
                _logger.InfoFormat("Liberamos HP34401A ... en la direccion GPIB: {0} ", hp34401A.PortAdress);
                hp34401A.Dispose();
                hp34401A = null;
                IsHP34401AInitialized = false;
            }

            if (hp53131A != null)
            {
                _logger.InfoFormat("Liberamos HP53131A ... en la direccion GPIB: {0} ", hp53131A.PortAdress);
                hp53131A.Dispose();
                hp53131A = null;
                IsHP53131AInitialized = false;
            }

            if (cvmminiChroma != null)
            {
                _logger.InfoFormat("Liberamos CVMMINICHROMA ... en el puerto: {0} ", cvmminiChroma.Modbus.PortCom);
                cvmminiChroma.Dispose();
                cvmminiChroma = null;
            }

            if (cvmminiMTE != null)
            {
                _logger.InfoFormat("Liberamos CVMMINIMTE ... en el puerto: {0} ", cvmminiMTE.Modbus.PortCom);
                cvmminiMTE.Dispose();
                cvmminiMTE = null;
            }

            if (cvmB100Hi != null)
            {
                _logger.InfoFormat("Liberamos CVMB100HI ... en el puerto: {0} ", cvmB100Hi.Modbus.PortCom);
                cvmB100Hi.Dispose();
                cvmB100Hi = null;
            }

            if (cvmB100Low != null)
            {
                _logger.InfoFormat("Liberamos CVMB100LOW ... en el puerto: {0} ", cvmB100Low.Modbus.PortCom);
                cvmB100Low.Dispose();
                cvmB100Low = null;
            }

            if (cvmK2 != null)
            {
                _logger.InfoFormat("Liberamos CVMK2 ... en el puerto: {0} ", cvmK2.Modbus.PortCom);
                cvmK2.Dispose();
                cvmK2 = null;
            }

            if (fluke183 != null)
            {
                _logger.InfoFormat("Liberamos fluke183 ... en el puerto: {0} ", fluke183.PortCom);
                fluke183.Dispose();
                fluke183 = null;
            }

            if (metrix3293 != null)
            {
                _logger.InfoFormat("Liberamos METRIX3293 ... en el puerto: {0} ", metrix3293.PortCom);
                metrix3293.Dispose();
                metrix3293 = null;
            }

            if (sp != null)
            {
                _logger.InfoFormat("Liberamos SP ... en el puerto: {0} ", sp.PortName);
                sp.Dispose();
                sp = null;
            }

            //if (powerSourceM9111A != null)
            //{
            //    _logger.InfoFormat("Liberamos POWERSOURCE_M9111A Keysight ");
            //    powerSourceM9111A.Dispose();
            //    powerSourceM9111A = null;
            //}

            if (actuator_LAC25 != null)
            {
                _logger.InfoFormat("Liberamos ACTUATOR_LAC25 ... en el puerto: {0} ", actuator_LAC25.PortCom);
                actuator_LAC25.Dispose();
            }

            if (multimeterM9183A != null)
            _logger.InfoFormat("No Liberamos multimeterM9183A de Keysight");
            //multimeterM9183A.Dispose();
            //multimeterM9183A = null;

            if (!whitOutIO)
            {
                if (mux != null)
                {
                    if (mux.GetType() != typeof(M9120A))
                    {
                        mux.Reset();
                        _logger.InfoFormat("Liberamos MUX ");
                        mux = null;
                    }
                    else
                    _logger.InfoFormat(" No liberamos la MUX M9120A de Keysight");
                    //mux.Dispose();
                    //mux = null;
               
                }

                if (io != null)
                {
                    io.DO.OffRange(0, POWER_24VDC - 1);
                    io.DO.OffRange(POWER_24VDC + 1, io.DO.Count -1);
                    _logger.InfoFormat("Desactivamos todas las OUTPUTS, pero no se libera la targeta IO");
                }

                if (usbIO != null)
                {
                    usbIO.DO.OffRange(0, 7);
                    _logger.InfoFormat("Desactivamos todas las OUTPUTS, pero no se libera la targeta USB4761");
                }
            }
        }
        public void Dispose()
        {
            FreeResources(false);
        }

        [Browsable(false)]
        public void Dispose(bool whitOutIO)
        {
            FreeResources(whitOutIO);
        }
        public string GetInstrumentVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(InstrumentsVersionAttribute), true).FirstOrDefault() as InstrumentsVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }
        protected void LogMethod([CallerMemberName] string name = "")
        {
            _logger.InfoFormat(name);
        }

        protected TestException Error()
        {
            return TestException.Create();
        }
    }
}
