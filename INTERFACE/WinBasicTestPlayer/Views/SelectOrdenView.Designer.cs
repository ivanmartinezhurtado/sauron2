﻿using System.Windows.Forms;

namespace WinTestPlayer.Views
{
    partial class SelectOrdenView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picLock = new Telerik.WinControls.UI.RadPanel();
            this.txtUserName = new Telerik.WinControls.UI.RadTextBox();
            this.LblOrden = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).BeginInit();
            this.picLock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).BeginInit();
            this.SuspendLayout();
            // 
            // picLock
            // 
            this.picLock.BackColor = System.Drawing.Color.AliceBlue;
            this.picLock.Controls.Add(this.txtUserName);
            this.picLock.Controls.Add(this.LblOrden);
            this.picLock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLock.Location = new System.Drawing.Point(0, 0);
            this.picLock.Name = "picLock";
            this.picLock.Size = new System.Drawing.Size(418, 234);
            this.picLock.TabIndex = 6;
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(65, 136);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(300, 27);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUserName_KeyDown);
            // 
            // LblOrden
            // 
            this.LblOrden.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblOrden.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.LblOrden.Location = new System.Drawing.Point(65, 80);
            this.LblOrden.Name = "LblOrden";
            this.LblOrden.Size = new System.Drawing.Size(300, 26);
            this.LblOrden.TabIndex = 7;
            this.LblOrden.Text = "Orden de fabricacion";
            this.LblOrden.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SelectOrdenView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picLock);
            this.Name = "SelectOrdenView";
            this.Size = new System.Drawing.Size(418, 234);
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).EndInit();
            this.picLock.ResumeLayout(false);
            this.picLock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel picLock;
        private Label LblOrden;
        private Telerik.WinControls.UI.RadTextBox txtUserName;

    }
}
