﻿using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using TaskRunner.Model;

namespace Dezac.Tests.Actions.Kernel
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(AddToFileVariableLogDescription))]
    public class AddToFileVariableLog : ActionBase
    {
        [Category("Configuracion")]
        [Description("Path to save the files if using CSV")]
        public string ConnectionString { get; set; }

        [Category("Configuracion")]
        public VariableLogClass[] VariablesLogToResult { get; set; }

        public override void Execute()
        {
            var identity = DateTime.Now.ToString("yyMMdd_HHmmss");
            var header = new StringBuilder();
            var values = new StringBuilder();

            var variablesLog = SequenceContext.Current.Variables.VariableLogger;

            if (VariablesLogToResult == null)
                Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("VariablesLogToResult").Throw();

            foreach (VariableLogClass result in VariablesLogToResult)
            {
                var variableFind = result.Variable.Replace("$", "");
                var variable = variablesLog.VariablesEntries[variableFind];

                header.AppendFormat("Time\";\"{0}\";", variableFind);

                foreach (var value in variable)
                {
                    Logger.InfoFormat("Add Result Time:{0}  Key:{1}  --> Value {2}", value.TimeSpan, value.Variable, value.Value);
                    values.AppendFormat("{0};{1}", value.TimeSpan, value.Value);
                    values.AppendLine();
                }

                SaveFile(variableFind, identity, header, values);
            }
        }

        private void SaveFile(string name, string sufix, StringBuilder headers, StringBuilder values)
        {
            var fileName = Path.Combine(ConnectionString, $"{name}_{sufix}.csv");
            File.WriteAllText(fileName, headers.ToString() + Environment.NewLine + values.ToString());
        }
    }

    public class VariableLogClass
    {
        [Description("Nombre de la variable que se añadira su log a los resultados")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Variable { get; set; }

        public ParamUnidad Unit { get; set; }
    }

    public class AddToFileVariableLogDescription : ActionDescription
    {
        public override string Name { get { return "AddToFileVariableLog"; } }
        public override string Category { get { return "Actions/Variables/Add"; } }
        public override Image Icon { get { return Properties.Resources.AddVariable; } }
    }
}
