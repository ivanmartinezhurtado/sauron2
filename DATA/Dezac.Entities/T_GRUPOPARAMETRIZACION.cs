namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_GRUPOPARAMETRIZACION
    {
        public T_GRUPOPARAMETRIZACION()
        {
            this.T_VALORPARAMETRO = new HashSet<T_VALORPARAMETRO>();
        }
    
        public int NUMGRUPO { get; set; }
        public int NUMTIPOGRUPO { get; set; }
        public string ALIAS { get; set; }
        public string IDFASE { get; set; }
        public string INOUT { get; set; }
        public int NUMFAMILIA { get; set; }
    
        public virtual FASE FASE { get; set; }
        public virtual T_FAMILIA T_FAMILIA { get; set; }
        public virtual T_TIPOGRUPOPARAMETRIZACION T_TIPOGRUPOPARAMETRIZACION { get; set; }
        public virtual ICollection<T_VALORPARAMETRO> T_VALORPARAMETRO { get; set; }
    }
}
