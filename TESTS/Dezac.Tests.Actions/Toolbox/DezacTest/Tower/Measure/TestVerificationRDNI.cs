﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(TestVerificationRDNIDescription))]
    public class TestVerificationRDNI : ActionBase
    {
        private static readonly object syncObject = new object();
        private const string RDNI_VAR = "TestVerificationRDNI_Var";

        public override void Execute()
        {

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                TestException.Create().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();;

            var cacheSvc = testBase.GetService<ICacheService>();

            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw(); ;

            var maxim = testBase.Identificacion.RDNI_MAX;
            var minim = testBase.Identificacion.RDNI_MIN;
            var adjustValueTestPoint = new AdjustValueDef("R_DNI", 0, minim, maxim, 0, 0, ParamUnidad.Ohms);

            double rdn = 0;

            lock (syncObject)
            {
                if (!cacheSvc.TryGet(RDNI_VAR, out rdn))
                    try
                    {
                        var measure = new MagnitudToMeasure()
                        {
                            Frequency = freqEnum._50Hz,
                            Magnitud = MagnitudsMultimeter.Resistencia,
                            NumMuestras = 3,
                            Rango = maxim,
                            DigitosPrecision = 6,
                            NPLC = 16
                        };
                        testBase.TestMeasureBase(adjustValueTestPoint, (step) =>
                        {
                            rdn = tower.MeasureMultimeter(InputMuxEnum.IN7, measure, 1000, null, null, false);
                            return rdn;
                        }, 0, 5, 1000);

                        if (cacheSvc != null)
                            cacheSvc.Add(RDNI_VAR, rdn);
                    }
                    finally
                    {
                        tower.IO.DO.Off(44);
                    }
            }                      
        }
    }

    public class TestVerificationRDNIDescription : ActionDescription
    {
        public override string Name { get { return "TestVerificationRDNI"; } }
        public override string Description { get { return "Acción genérica para detectar la manguera conectada a la torre"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }


}
