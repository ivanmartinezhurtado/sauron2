﻿using Dezac.Tests.Model;
using Instruments.IO;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(InputDetectionDescription))]
    

    public class InputDetection : ActionBase
    {
        [Description("Nombre de la acción de la entrada que queremos detectar")]
        public string DescriptionAction { get; set; }

        [Description("Tiempo de espera antes de leer el estado de la entrada si esta activada")]
        [DefaultValue((int)500)]
        public int WaitTimetToDetectionDevice { get; set; }

        [Description("Entrada que queremos detectar activada  o con $ variable que queremos detectar")]
        public string InputDetectionDevice { get; set; }

        [DefaultValue(true)]
        [Description("Estado que esperamos de la entrada a detectar")]
        public bool StateInputDetection { get; set; }

        [Description("valor de la salida o con $ variable de la salida que des/activamos,")]
        public string OuputActivatedPiston { get; set; }

        [Description("Numero de reintentos para detectar la entrada")]
        [DefaultValue((byte)3)]
        public byte NumRetries { get; set; }

        [Description("Tiempo de espera entre los reintentos")]
        [DefaultValue((int)3)]
        public int WaitTimeBetweenRetries { get; set; }

        [Description("Si queremos mostrar un mensaje de aviso antes de dar como error la acción")]
        [DefaultValue(false)]
        public bool ShowMessageWarning { get; set; }

        [DefaultValue(false)]
        public bool saveResult { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el metodo de inicialización del testbase");

            var IO = GetVariable<USB4761>("USB4761", () => { return new USB4761(); });

            byte? ouputActivatedPiston = null;

            if (!string.IsNullOrEmpty(OuputActivatedPiston))
                ouputActivatedPiston = GetPropertyVar<byte>(OuputActivatedPiston);

            if (string.IsNullOrEmpty(InputDetectionDevice))
                if (testBase.Shell.MsgBox(string.Format("Error, no se ha indicado ninguna Input para detectar en {0}", DescriptionAction), "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.OK, MessageBoxIcon.Error) != DialogResult.Yes)
                    throw new Exception(string.Format("Error, no se ha indicado ninguna Input para detectar en {0}", DescriptionAction));

            byte inputDetectionDevice = GetPropertyVar<byte>(InputDetectionDevice);

            testBase.SamplerWithCancel((p) =>
            {
                if (ouputActivatedPiston.HasValue)
                    IO.DO.On(ouputActivatedPiston.Value);

                testBase.Delay(WaitTimetToDetectionDevice, "Espera a la detección de la entrada");

                if (IO.DI[inputDetectionDevice] == StateInputDetection)
                {
                    if (saveResult)
                        testBase.Resultado.Set(string.Format("IN_{0}", InputDetectionDevice), "OK", ParamUnidad.SinUnidad);

                    return true;
                }

                if (ShowMessageWarning)
                    if (testBase.Shell.MsgBox(string.Format("Entrada digital {0}  {1}  no activada, ¿Quieres volver a leerla?", InputDetectionDevice, DescriptionAction), "AVISO, EN LA DETECCION DE ENTRADA DE LA TORRE", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    {
                        if (ouputActivatedPiston.HasValue)
                            IO.DO.Off(ouputActivatedPiston.Value);

                        throw new Exception(string.Format("Error en la detección de la entrada digital {0}, el usuario ha indicado que no quiere volver a comprobarla", InputDetectionDevice));
                    }

                if (p >= NumRetries)
                    if (ouputActivatedPiston.HasValue)
                        IO.DO.Off(ouputActivatedPiston.Value);

                if (saveResult)
                    testBase.Resultado.Set(string.Format("IN_{0}", InputDetectionDevice), "ERROR", ParamUnidad.SinUnidad);

                return false;

            }, string.Format("Error. Entrada {0}  {1} no detectada en estado {2}", InputDetectionDevice, DescriptionAction, StateInputDetection), NumRetries, WaitTimeBetweenRetries);
        }
    }

    public class InputDetectionDescription : ActionDescription
    {
        public override string Name { get { return "Input Detection"; } }
        public override string Description { get { return "Acción genérica para detectar las entradas activadas del USB4761"; } }
        public override string Category { get { return "Obsoletas/Mike"; } }
        public override Image Icon { get { return Properties.Resources.input; } }
    }
}
