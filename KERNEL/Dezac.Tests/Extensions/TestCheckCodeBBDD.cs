﻿using Dezac.Tests.Model;
using Dezac.Tests.UserControls;
using System;
using System.Windows.Forms;

namespace Dezac.Tests.Extensions
{
    public static class TestCheckCodeBBDD
    {
        public static void PackageCheck(this TestBase testBase, string parameterName, bool cancelButton = true)
        {
            MessageBoxButtons buttons = cancelButton ? MessageBoxButtons.OKCancel : MessageBoxButtons.OK;

            var labelBox = testBase.Parametrizacion.GetString(parameterName, "NO", ParamUnidad.SinUnidad);
            if (labelBox != "NO")
            {
                string barcode = "";
                testBase.SamplerWithCancel((p) =>
                {
                    var barcodeInput = new InputKeyBoard("Leer código de barras de la etiqueta del embalaje");

                    testBase.Shell.ShowDialog("VERIFICACION DE EMBALAJE", () => barcodeInput, buttons);

                    if (barcodeInput.TextValue.ToUpper().Trim() != labelBox.ToUpper().Trim())
                    {
                        testBase.Shell.MsgBox("Error. código de barras del embalaje leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DE EMBALAJE INCORRECTO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }

                    barcode = barcodeInput.TextValue;
                    return true;

                }, "Error no se ha leido el código de barras del embalaje", 2);

                if (barcode.ToUpper().Trim() != labelBox.ToUpper().Trim())
                    testBase.Error().PROCESO.MATERIAL.EMBALAJE_INDIVIDUAL_INCORRECTO().Throw();
            }
        }

        public static void DifferentialCodeCheck()
        {
            //TODO aqui iria la comprobacion del diferencial del  REC3 y REC max
            throw new NotImplementedException();
        }


    }
}
