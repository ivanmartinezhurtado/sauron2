namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class WEBUSERPARAM
    {
        public string IDUSER { get; set; }
        public string APPLICATION { get; set; }
        public string PARAM { get; set; }
        public string VALUE { get; set; }
    }
}
