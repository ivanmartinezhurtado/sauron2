﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dezac.Tests.UserControls;

namespace Circutor.Tests.MyCircutor
{
    public partial class ItemsInputView : DialogViewBase
    {
        private static string itemCode;
        private static int stages;
        private static string regulatorItemCode;

        public ItemsInputView()
        {
            InitializeComponent();

            if (!string.IsNullOrEmpty(itemCode))
            {
                txtLVCBItemCode.Text = itemCode;
                nudStages.Value = stages;
                txtRegulatorItemCode.Text = regulatorItemCode;
            }
        }

        public string ItemCode
        {
            get { return txtLVCBItemCode.Text; }
        }

        public int Stages
        {
            get { return Convert.ToInt32(nudStages.Value); }
        }

        public string RegulatorItemCode
        {
            get { return txtRegulatorItemCode.Text; }
        }

        public override bool ValidateView(DialogResult result)
        {
            if (string.IsNullOrEmpty(ItemCode) || string.IsNullOrEmpty(RegulatorItemCode) || (Stages <= 0 || Stages > 10))
                return false;

            itemCode = ItemCode;
            stages = Stages;
            regulatorItemCode = RegulatorItemCode;

            return base.ValidateView(result);
        }
    }
}
