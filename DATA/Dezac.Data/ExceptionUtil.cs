﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.Validation;

namespace Dezac.Data
{
    internal class ExceptionUtil
    {

        public static string BuildExceptionMessage(Exception x)
        {
            Exception logException = x;

            while (logException.InnerException != null)
                logException = logException.InnerException;

            string strErrorMsg = "";

            strErrorMsg += Environment.NewLine + "Message :" + logException.Message;
            strErrorMsg += Environment.NewLine + "Source :" + logException.Source;
            strErrorMsg += Environment.NewLine + "Stack Trace :" + logException.StackTrace;
            strErrorMsg += Environment.NewLine + "TargetSite :" + logException.TargetSite;

            if (x is DbEntityValidationException)
                strErrorMsg += BuildExceptionMessage((DbEntityValidationException)x);
            else if (x != logException)
            {
                strErrorMsg += Environment.NewLine + "Outer Message:" + x.Message;
                strErrorMsg += Environment.NewLine + "Outer Stack Trace:" + x.StackTrace;
            }

            return strErrorMsg;
        }

        public static string GetInnerExceptionMessage(Exception x)
        {
            Exception logException = x;

            while (logException.InnerException != null)
                logException = logException.InnerException;

            return logException.Message;
        }

        public static string BuildExceptionMessage(DbEntityValidationException x)
        {
            string strErrorMsg = "";

            foreach (DbEntityValidationResult r in x.EntityValidationErrors)
            {
                strErrorMsg += Environment.NewLine + "Validation errors:";

                foreach (DbValidationError e in r.ValidationErrors)
                    strErrorMsg += Environment.NewLine + e.PropertyName + ": " + e.ErrorMessage;
            }

            return strErrorMsg;
        }
    }
}
