﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using log4net;
using System;
using System.Drawing;
using Dezac.Core.Utility;
using System.ComponentModel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(TelnetComunicationDescription))]
    public class TelnetComunication : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Texto que se ha de enviar")]
        public string TextToSend { get; set; }

        [Category("3. Configuration")]
        [Description("Texto que se ha de leer")]
        public string TextToRead { get; set; }

        [Category("3.1 Instance")]
        [Description("Nombre del equipo")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return new TestBase(); });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();;

            var sender = GetVariable(DeviceName) as TelnetConnection;

            if (sender == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha llamado al metodo de inicialización del Telnet").Throw();

            testBase.SamplerWithCancel((p) =>
            {
                var send = ReplaceContextVariable(TextToSend);
                Logger.InfoFormat("Enviamos por Telnet {0}", send);
                sender.WriteLineString(send);

                if (!string.IsNullOrEmpty(TextToRead))
                {
                    var response = sender.Read();
                    Logger.InfoFormat("Recibimos por Telnet {0}", response);
                    Context.ResultList.Add(response);

                    return response.Contains(TextToRead);
                }
                else
                    return true;

            }, "Error en las comunicaciones Telnet", 5, 500, 500, false, false);           
        }

        private string ReplaceContextVariable(string trama)
        {
            string tramaSend = trama;
            if (trama.Contains("{{") && trama.Contains("}}"))
            {
                var indexStart = trama.IndexOf("{{");
                var indexEnd = trama.LastIndexOf("}}") + 2;
                var lenght = indexEnd - indexStart;
                var variable = trama.Substring(indexStart, lenght);
                var varReplace = Context.Variables.Get(variable.Replace("{{", "").Replace("}}", "")).ToString();
                tramaSend = trama.Replace(variable, varReplace);
            }
            return tramaSend;
        }
    }

    public class TelnetComunicationDescription : ActionDescription
    {
        public override string Name { get { return "TelnetComunication"; } }
        public override string Description { get { return "Envía y espera a recibir la trama por Telnet"; } }
        public override string Category { get { return "Tools/Ethernet/Telnet"; } }
        public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
        public override string Dependencies { get { return "TelnetInitialization;TelnetFinalized:2"; } }
    }
}
