﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DetectionDriveDescription))]
    public class DetectionDrive : ActionBase
    {
        [Category("3.1 Search")]
        [Description("Tipo de unidad a buscar")]
        [DefaultValue(DriveType.Removable)]
        public DriveType DriveType { get; set; }

        [Category("3.1 Search")]
        [Description("Nombre de la unidad a buscar")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DriveSearchName { get; set; }

        [Category("3. Configuration")]
        [Description("Lanzar excepción sino la encuentra")]
        [DefaultValue(true)]
        public bool ThrowException { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            try
            {
                var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
                if (testBase == null)
                    Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

                if (string.IsNullOrEmpty(DriveSearchName))
                    Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DriveSearchName).Throw();

                var driveSearchName = VariablesTools.ResolveValue(DriveSearchName).ToString();
                if (string.IsNullOrEmpty(driveSearchName))
                    Error().PROCESO.ACTION_EXECUTE.PARAMETRO_INCOHERENTE(driveSearchName).Throw();


                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType))
                {
                    Logger.InfoFormat("DRIVE NAME:{0} LABEL:{1} TYPE:{2}  FREE SPACE:{3}", drive.Name, drive.VolumeLabel, drive.DriveType, drive.TotalFreeSpace);
                    Context.ResultList.Add(string.Format("DRIVE NAME:{0} LABEL:{1} TYPE:{2}  FREE SPACE:{3}", drive.Name, drive.VolumeLabel, drive.DriveType, drive.TotalFreeSpace));
                    if (!string.IsNullOrEmpty(driveSearchName) && (drive.VolumeLabel.ToUpper() == driveSearchName.Trim().ToUpper()))
                    {
                        var result = String.Format("DRIVE NAME:{0} LABEL:{1} TYPE:{2}  FREE SPACE:{3}", drive.Name, drive.VolumeLabel, drive.DriveType, drive.TotalFreeSpace);
                        Variables.AddOrUpdate(string.Format("{0}_FREE_SPACE", DataOutput), drive.TotalFreeSpace);
                        DataOutput.Value = result;
                        break;
                    }
                }      

                if (ThrowException)
                {
                   if (!Variables.ContainsKey(DataOutput.Key))
                        Error().PROCESO.ACTION_EXECUTE.DISPOSITIVO_NO_ENCONTRADO(DataOutput.Key).Throw();
                }
            }
            catch (Exception e)
            {
                Error().SOFTWARE.SAURON.FICHERO_NO_ENCONTRADO(e.Message).Throw();
            }
        }

        class DetectionDriveDescription : ActionDescription
        {
            public override string Name { get { return "DetectionDrive"; } }
            public override string Description { get { return "Crea un fichero con el contenido de la propiedad 'Content'"; } }
            public override string Category { get { return "Tools/Drive"; } }
            public override Image Icon { get { return Properties.Resources.removable_usb; } }
            public override string Dependencies { get { return "InitTestBase"; } }
        }
    }
}
