﻿using Dezac.DLMS;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(SendDlmsDescription))]
    public class SendDlms : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Número Obis")]
        public string Obis { get; set; }

        [Category("3. Configuration")]
        [Description("Número de atributo")]
        [DefaultValue(2)]
        public int Attribute { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de registro con el que se trabaja")]
        [DefaultValue(TypeRegister.Data)]
        public TypeRegister Type { get; set; }

        [Category("3. Configuration")]
        [Description("Valor")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Value { get; set; }

        [Category("3. Configuration")]
        [Description("Nombre de la instancia del objecto creado")]
        [DefaultValue("DlmsDevice")]
        public string DeviceName { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo de valor")]
        [DefaultValue(TypeValueEnum.String)]
        public TypeValueEnum TypeValue { get; set; }

        public enum TypeRegister
        {
            Data,
            Register,
            Clock,
        }

        public enum TypeValueEnum
        {
            UByte,
            Byte,
            String,
            Ushort,
            UInt,
            Short,
            Int,
            ArrayByte,
        }

        public SendDlms()
        {
        }

        public override void Execute()
        {
            string name = DeviceName ?? "DlmsDevice";
            var uut = GetVariable(name) as DLMSDeviceSerialPort;

           var valueIntern = VariablesTools.ResolveValue(Value).ToString();
                
            switch (Type)
            {
                case TypeRegister.Data:
                    switch (TypeValue)
                    {
                        case TypeValueEnum.Byte:
                            uut.WriteData<byte>(Obis, Attribute, Convert.ToByte(valueIntern));
                            break;
                        case TypeValueEnum.UByte:
                            uut.WriteData<SByte>(Obis, Attribute, Convert.ToSByte(valueIntern));
                            break;
                        case TypeValueEnum.Ushort:
                            uut.WriteData<ushort>(Obis, Attribute, Convert.ToUInt16(valueIntern));
                            break;
                        case TypeValueEnum.UInt:
                            uut.WriteData<uint>(Obis, Attribute, Convert.ToUInt32(valueIntern));
                            break;
                        case TypeValueEnum.Short:
                            uut.WriteData<short>(Obis, Attribute, Convert.ToInt16(valueIntern));
                            break;
                        case TypeValueEnum.Int:
                            uut.WriteData<int>(Obis, Attribute, Convert.ToInt32(valueIntern));
                            break;
                        case TypeValueEnum.String:
                            uut.WriteData<string>(Obis, Attribute, valueIntern);
                            break;
                        case TypeValueEnum.ArrayByte:
                            var value = Encoding.ASCII.GetBytes(valueIntern);
                            uut.WriteData<byte[]>(Obis, Attribute, value);
                            break;
                    }
                    break;
                case TypeRegister.Register:
                    switch (TypeValue)
                    {
                        case TypeValueEnum.Byte:
                            uut.WriteRegister<byte>(Obis, Attribute, Convert.ToByte(valueIntern));
                            break;
                        case TypeValueEnum.UByte:
                            uut.WriteRegister<SByte>(Obis, Attribute, Convert.ToSByte(valueIntern));
                            break;
                        case TypeValueEnum.Ushort:
                            uut.WriteRegister<ushort>(Obis, Attribute, Convert.ToUInt16(valueIntern));
                            break;
                        case TypeValueEnum.UInt:
                            uut.WriteRegister<uint>(Obis, Attribute, Convert.ToUInt32(valueIntern));
                            break;
                        case TypeValueEnum.Short:
                            uut.WriteRegister<short>(Obis, Attribute, Convert.ToInt16(valueIntern));
                            break;
                        case TypeValueEnum.Int:
                            uut.WriteRegister<int>(Obis, Attribute, Convert.ToInt32(valueIntern));
                            break;
                        case TypeValueEnum.String:
                            uut.WriteRegister<string>(Obis, Attribute, valueIntern);
                            break;
                        case TypeValueEnum.ArrayByte:
                            var value = Encoding.ASCII.GetBytes(valueIntern);
                            uut.WriteRegister<byte[]>(Obis, Attribute, value);
                            break;
                    }
                    break;
                case TypeRegister.Clock:
                    var time = Convert.ToDateTime(valueIntern);
                    uut.WriteClock(Obis, Attribute, time);
                    break;
            }

           Context.ResultList.Add(string.Format("{0}={1}", Obis, valueIntern));
        }
    }

    public class SendDlmsDescription : ActionDescription
    {
        public override string Name { get { return "SendDlms"; } }
        public override string Description { get { return "Enviar y Recibir por protocolo DLMS"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort;DisposeDlmsSerialPort:2"; } }
    }
}
