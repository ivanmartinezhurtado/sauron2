﻿using Comunications.Utility;
using Dezac.Core.Utility;
using Dezac.Core.Exceptions;
using Dezac.Tests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Dezac.Device.Reactive
{
    [DeviceVersion(1.06)]
    public class COMPUTERSMARTIII : DeviceBase
    {
        public ModbusDeviceSerialPort Modbus { get; set; }

        public COMPUTERSMARTIII()
        {
            SetPort(7, 1, 19200);
        }

        public COMPUTERSMARTIII(byte port = 7)
        {
            SetPort(port, 1, 19200);
        }

        public void SetPort(byte port, byte periferico, int baudrate)
        {
            Modbus = new ModbusDeviceSerialPort(port, baudrate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            Modbus.PerifericNumber = periferico;
        }

        public override void Dispose()
        {
            Modbus.Dispose();
            base.Dispose();
        }

        public int ReadBastidor()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.BASTIDOR);
        }

        public ushort ReadCodeError()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.CODERROR);
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadInt32((ushort)Registers.FIRMWARE_VERSION).ToString();
        }

        public long ReadSerialNumber()
        {
            LogMethod();
            return (long)Modbus.ReadInt64((ushort)Registers.SERIAL_NUMBER);
        }

        public VariablesFase ReadVariablesInstantaneasL1()
        {
            LogMethod();
            return Modbus.Read<VariablesFase>((ushort)0x0000);
        }

        public VariablesFase ReadVariablesInstantaneasL2()
        {
            LogMethod();
            return Modbus.Read<VariablesFase>((ushort)0x001A);
        }

        public VariablesFase ReadVariablesInstantaneasL3()
        {
            LogMethod();
            return Modbus.Read<VariablesFase>((ushort)0x0034);
        }

        public VariablesTrifasicas ReadVariablesTrifasicas()
        {
            LogMethod();
            return Modbus.Read<VariablesTrifasicas>();
        }

        public SumatoriosOffsets ReadSumatoriosOffsetsCiclo()
        {
            var sumatorios = ReadSumatoriosCiclo();
            var offsets = ReadOffsets();

            var sumOff = new SumatoriosOffsets()
            {
                sumatorios = sumatorios,
                offsets = offsets
            };

            return sumOff;
        }

        public THD ReadTHD()
        {
            LogMethod();
            return Modbus.Read<THD>();
        }

        public Sumatorios ReadSumatoriosCiclo()
        {
            LogMethod();
            return Modbus.Read<Sumatorios>();
        }

        public Offsets ReadOffsets()
        {
            LogMethod();
            return Modbus.Read<Offsets>();
        }

        /**********************************************************************/

        public void Upgrade(string binaryName)
        {        
            LogMethod();
            Modbus.UpgradeHexFile(13002, 0x2EE0, binaryName);
        }

        /**********************************************************************/

        public enum InputsDigitals
        {
            IN_D1 = 0x01,
            IN_D2 = 0x02
        }

        public InputsDigitals ReadInputsDigital()
        {
            LogMethod();
            return (InputsDigitals)Modbus.ReadRegister((ushort)Registers.DIGITAL_INPUTS);
        }

        public enum KeyboardKeys
        {
            UP = 0x0200,
            LEFT = 0x0100,
            CENTER = 0x0400,
            DOWN = 0x0800,
            RIGHT = 0x1000,
            NONE = 0x00
        }

        public KeyboardKeys ReadKeyBoard()
        {
            LogMethod();
            return (KeyboardKeys)Modbus.ReadRegister((ushort)Registers.TECLADO);
        }

        #region CALIBRATION

        public Tuple<bool, FactoresCalibracion, string> CalculateAdjustFactorsVoltageCurrentDesfase(int delFirst, int initCount, int samples, int interval, double adjustVoltage, double adjustCurrent, double Angulo, Func<double[]> measureVariables, Func<FactoresCalibracion, List<AdjustValueDef>> adjustValidation, Func<SumatoriosOffsets, List<AdjustValueDef>> sampleValidation = null)
        {
            var factors = ReadFactoresCalibracion();

            var errorString = "";

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    factors.CalibracionVL1 = Convert.ToUInt16((Math.Pow(adjustVoltage * 100, 2) * 10) / listValues.Average(0));
                    factors.CalibracionVL2 = Convert.ToUInt16((Math.Pow(adjustVoltage * 100, 2) * 10) / listValues.Average(1));
                    factors.CalibracionVL3 = Convert.ToUInt16((Math.Pow(adjustVoltage * 100, 2) * 10) / listValues.Average(2));

                    factors.CalibracionIL1 = Convert.ToUInt16((Math.Pow(adjustCurrent * 1000, 2) * 100) / listValues.Average(3));
                    factors.CalibracionIL2 = Convert.ToUInt16((Math.Pow(adjustCurrent * 1000, 2) * 100) / listValues.Average(4));
                    factors.CalibracionIL3 = Convert.ToUInt16((Math.Pow(adjustCurrent * 1000, 2) * 100) / listValues.Average(5));

                    factors.CalibracionCorrienteNeutro = Convert.ToUInt16((factors.CalibracionIL1 + factors.CalibracionIL2 + factors.CalibracionIL3) / 3);
                    factors.factorCorriente = Convert.ToUInt16((adjustCurrent * 10000) / Math.Sqrt(factors.CalibracionCorrienteNeutro));

                    factors.CalibracionVL12 = Convert.ToUInt16((Math.Pow(adjustVoltage * 100, 2) * 3 * 10) / listValues.Average(6));
                    factors.CalibracionVL23 = Convert.ToUInt16((Math.Pow(adjustVoltage * 100, 2) * 3 * 10) / listValues.Average(7));
                    factors.CalibracionVL31 = Convert.ToUInt16((Math.Pow(adjustVoltage * 100, 2) * 3 * 10) / listValues.Average(8));

                    var radians_Desfase = (Math.Atan(listValues.Average(12) / listValues.Average(9))).ToDegree(); //* (180 / Math.PI);
                    factors.CalibracionDesfaseL1 = Convert.ToUInt16(((radians_Desfase - Angulo) * 160) + 1000);

                    radians_Desfase = (Math.Atan(listValues.Average(13) / listValues.Average(10))).ToDegree(); //* (180 / Math.PI);
                    factors.CalibracionDesfaseL2 = Convert.ToUInt16(((radians_Desfase - Angulo) * 160) + 1000);

                    radians_Desfase = (Math.Atan(listValues.Average(14) / listValues.Average(11))).ToDegree(); //* (180 / Math.PI);
                    factors.CalibracionDesfaseL3 = Convert.ToUInt16(((radians_Desfase - Angulo) * 160) + 1000);

                    var errorList = adjustValidation(factors);

                    if (errorList.Count != 0)
                        errorString = string.Format("Error, fuera de margenes de {0}", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                },
                (samplesToValidate) =>
                {
                    var sumatorios = new SumatoriosOffsets();

                    sumatorios.sumatorios.tensionL1 = (int)samplesToValidate[0];
                    sumatorios.sumatorios.tensionL2 = (int)samplesToValidate[1];
                    sumatorios.sumatorios.tensionL3 = (int)samplesToValidate[2];

                    sumatorios.sumatorios.corrienteL1 = (int)samplesToValidate[3];
                    sumatorios.sumatorios.corrienteL2 = (int)samplesToValidate[4];
                    sumatorios.sumatorios.corrienteL3 = (int)samplesToValidate[5];

                    sumatorios.sumatorios.tensionCompuestaL12 = (int)samplesToValidate[6];
                    sumatorios.sumatorios.tensionCompuestaL23 = (int)samplesToValidate[7];
                    sumatorios.sumatorios.tensionCompuestaL31 = (int)samplesToValidate[8];

                    sumatorios.offsets.offsetTensionL1 = (int)samplesToValidate[15];
                    sumatorios.offsets.offsetTensionL2 = (int)samplesToValidate[16];
                    sumatorios.offsets.offsetTensionL3 = (int)samplesToValidate[17];

                    sumatorios.offsets.offsetCorrienteL1 = (int)samplesToValidate[18];
                    sumatorios.offsets.offsetCorrienteL2 = (int)samplesToValidate[19];
                    sumatorios.offsets.offsetCorrienteL3 = (int)samplesToValidate[20];

                    var errorList = sampleValidation(sumatorios);

                    if (errorList.Count != 0)
                        errorString = string.Format("{0} fuera de margenes", errorList.FirstOrDefault().Name);

                    return true; // errorList.Count == 0;
                });

            if (!string.IsNullOrEmpty(adjustResult.Item3))
                errorString = adjustResult.Item3;

            return Tuple.Create(adjustResult.Item1, factors, errorString);
        }

        public Tuple<bool, FactoresCalibracion, string> CalculateAdjustFactorsPoweraActiveReactive(int delFirst, int initCount, int samples, int interval, double adjustVoltage, double adjustCurrent, double Angulo, Func<double[]> measureVariables, Func<FactoresCalibracion, List<AdjustValueDef>> adjustValidation, Func<SumatoriosOffsets, List<AdjustValueDef>> sampleValidation = null)
        {
            var factors = ReadFactoresCalibracion();

            var errorString = "";

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    factors.CalibracionPActivaL1 = Convert.ToUInt16((adjustVoltage * adjustCurrent * 1000 * Math.Cos(Angulo.ToRadians()) * 10000) / listValues.Average(0));
                    factors.CalibracionPActivaL2 = Convert.ToUInt16((adjustVoltage * adjustCurrent * 1000 * Math.Cos(Angulo.ToRadians()) * 10000) / listValues.Average(1));
                    factors.CalibracionPActivaL3 = Convert.ToUInt16((adjustVoltage * adjustCurrent * 1000 * Math.Cos(Angulo.ToRadians()) * 10000) / listValues.Average(2));

                    factors.CalibracionPReactivaL1 = Convert.ToUInt16((adjustVoltage * adjustCurrent *1000 * Math.Sin(Angulo.ToRadians()) * 10000) / listValues.Average(3));
                    factors.CalibracionPReactivaL2 = Convert.ToUInt16((adjustVoltage * adjustCurrent *1000 * Math.Sin(Angulo.ToRadians()) * 10000) / listValues.Average(4));
                    factors.CalibracionPReactivaL3 = Convert.ToUInt16((adjustVoltage * adjustCurrent *1000 * Math.Sin(Angulo.ToRadians()) * 10000) / listValues.Average(5));

                    var errorList = adjustValidation(factors);

                    if (errorList.Count != 0)
                        errorString = string.Format("{0} fuera de margenes", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                },
                (samplesToValidate) =>
                {
                    var sumatorios = new SumatoriosOffsets();

                    sumatorios.sumatorios.potenciaActivaL1 = (int)samplesToValidate[0];
                    sumatorios.sumatorios.potenciaActivaL2 = (int)samplesToValidate[1];
                    sumatorios.sumatorios.potenciaActivaL3 = (int)samplesToValidate[2];

                    sumatorios.sumatorios.potenciaReactivaL1 = (int)samplesToValidate[3];
                    sumatorios.sumatorios.potenciaReactivaL2 = (int)samplesToValidate[4];
                    sumatorios.sumatorios.potenciaReactivaL3 = (int)samplesToValidate[5];

                    var errorList = sampleValidation(sumatorios);

                    if (errorList.Count != 0)
                        errorString = string.Format("{0} fuera de margenes", errorList.FirstOrDefault().Name);

                    return errorList.Count == 0;
                });


            if (!string.IsNullOrEmpty(adjustResult.Item3))
                errorString = adjustResult.Item3;

            return Tuple.Create(adjustResult.Item1, factors, errorString);
        }

        public Tuple<bool, FactoresCalibracion> CalculateAdjustFactorsCurrentLeak(int delFirst, int initCount, int samples, int interval, double adjustCurrent, Func<double[]> measureVariables, Func<FactoresCalibracion, bool> adjustValidation, Func<SumatoriosOffsets, bool> sampleValidation = null)
        {
            var factors = ReadFactoresCalibracion();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, int.MaxValue,
                () =>
                {
                    return measureVariables();
                },
                (listValues) =>
                {
                    factors.CalibracionCorrienteFugas = Convert.ToUInt16((Math.Pow(adjustCurrent * 1000, 2) * 1000) / listValues.Average(0));
                    return adjustValidation(factors);
                },
                (samplesToValidate) =>
                {
                    var sumatorios = new SumatoriosOffsets();
                    sumatorios.sumatorios.corrienteFugas = (int)samplesToValidate[0];
                    return sampleValidation(sumatorios);
                });

            return Tuple.Create(adjustResult.Item1, factors);
        }
        #endregion

        #region Modbus Writting operations

        public void WriteFlagTest()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, true);
        }

        public void WriteSerialNumber(long value)
        {
            LogMethod();
            Modbus.WriteInt64((ushort)Registers.SERIAL_NUMBER, value);
        }

        public void WriteBastidor(int value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.BASTIDOR, value);
        }

        public void WriteCodeError(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.CODERROR, value);
        }

        public enum TypeCarga
        {
            CARGA_MINIMA = 0,
            CARGA_MAXIMA = 1,
        }

        public void WriteCargaMinimaMaxima(TypeCarga state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.CARGA_MINIMA_MAXIMA, state == TypeCarga.CARGA_MAXIMA ? true : false);
        }

        public void WriteReset()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RESET, true);
        }
   
        public void WriteDisplayAllSegments(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.ALL_DISPLAY, state);
        }

        public void WriteLeds(bool state, Leds led = Leds.ALL)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)((ushort)Registers.LEDS + (ushort)led), state);
        }

        public void WriteReles(bool state, int Rele)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)(Registers.RELAYS + Rele), state);
        }

        public void WriteReleAlarma(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RELE_ALARMA, state);
        }

        public void WriteReleVentilador(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.RELE_VENTILADOR, state);
        }

        public void WriteDigitalOutput1(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DIGITAL_OUTPUTS_1, state);
        }

        public void WriteDigitalOutput2(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.DIGITAL_OUTPUTS_2, state);
        }

        public void WriteBackLight(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.BACKLIGHT, state);
        }

        public void WriteBorradoEnergias(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.BORRADO_ENERGIAS, state);
        }

        public void WriteBorradoMaximosMinimos(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.BORRADO_MAXIMIN, state);
        }

        public void WriteBorradoNumConexiones(bool state)
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.BORRADO_NUMCONN, state);
        }

        public void WriteVectorHardware(HardwareVector value)
        {
            LogMethod();
            var hardware = value.ToStructure();
            Modbus.Write<VectorHardwareStruct>(hardware);
        }

        public void WriteConfigurationDisplay(int value)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.CONFIGURATION_DISPLAY, value);
        }

        public void WriteComunicationCom1(ConfigurationCom value)
        {
            LogMethod();
            Modbus.Write<ConfigurationCom>((ushort)Registers.CONFIGURATION_COM1, value);
        }

        public void WriteComunicationCom1()
        {
            var config = new ConfigurationCom()
            {
                baudrate = 1, // 19200,
                bitsstop = 0, // 1bit stop
                databits = 0, //  8 bits datos
                paridad = 0,  //  None
                periferico = 1,
                protocolo = 0, //MODBUS
            };

            WriteComunicationCom1(config);
        }

        public void WriteComunicationCom2(ConfigurationCom value)
        {
            LogMethod();
            Modbus.Write<ConfigurationCom>((ushort)Registers.CONFIGURATION_COM2, value);
        }

        public void WriteComunicationCom2()
        {
            var config = new ConfigurationCom()
            {
                baudrate = 2, // 38400,
                bitsstop = 0, // 1bit stop
                databits = 0, //  8 bits datos
                paridad = 0,  //  None
                periferico = 1, 
                protocolo = 0, //MODBUS
            };

            WriteComunicationCom2(config);
        }

        public void WriteRelatcionTrafosCorriente(RelacionTrafosCorriente value)
        {
            LogMethod();
            Modbus.Write<RelacionTrafosCorriente>((ushort)Registers.RELATION_TRAFO_CURRENT, value);
        }

        public void WriteRelatcionTrafosCorriente()
        {
            var relacion = new RelacionTrafosCorriente()
            {
                Primario = 5,
                Secundario = 1,
            };
           
            WriteRelatcionTrafosCorriente(relacion);
        }

        public RelacionTrafosCorriente ReadRelatcionTrafosCorriente()
        {
            LogMethod();
            return Modbus.Read<RelacionTrafosCorriente>((ushort)Registers.RELATION_TRAFO_CURRENT);
        }

        public void WriteRelatcionTrafosTension(RelacionTrafosTension value)
        {
            LogMethod();
            Modbus.Write<RelacionTrafosTension>((ushort)Registers.RELATION_TRAFO_VOLTAGE, value);
        }

        public RelacionTrafosTension ReadRelatcionTrafosTension()
        {
            LogMethod();
            return Modbus.Read<RelacionTrafosTension>((ushort)Registers.RELATION_TRAFO_VOLTAGE);
        }

        public void WriteConfiguraciManiobra(ConfiguraciManiobra value)
        {
            LogMethod();
            Modbus.Write<ConfiguraciManiobra>(value);
        }

        public void WriteConfiguraciManiobra(byte numReles = 12)
        {
            short numRelesHex = numReles;
            if (numReles == 12)
                numRelesHex = 0x000C;
            if(numReles == 14)
                numRelesHex = 0x000E;

            LogMethod();
            var maniobra = new ConfiguraciManiobra()
            {
                C1 = 0x0064,
                C2 = 0x0064,
                C3 = 0x0064,
                C4 = 0x0064,
                T1 = 0x0001,
                T2 = 0x0001,
                T3 = 0x0001,
                T4 = 0x0001,
                CK = 0x0064,
                PR = 0x0457,
                TM = 0,
                ST = numRelesHex,
                TO = 0x000A,
                TR = 0x0032, //Segun Ingeniero esta mal en el documento
            };

            WriteConfiguraciManiobra(maniobra);
        }

        public void WriteConfiguracionTipoConexion(ConfiguracionTipoConexion value)
        {
            LogMethod();
            Modbus.Write<ConfiguracionTipoConexion>(value);
        }

        public void WriteConfiguracionTipoConexion()
        {
            var maniobra = new ConfiguracionTipoConexion()
            {
                 C1 = 1,
                 C2  =2,
                 C3 = 3, 
                 PH = 1,
                 TC = 0,
            };

            WriteConfiguracionTipoConexion(maniobra);
        }

        public void WriteConfiguracionOnOffAuto(OnOffAuto value)
        {
            LogMethod();
            Modbus.Write<OnOffAuto>(value);
        }

        public void WriteConfiguracionOnOffAuto()
        {
            var onOff = new OnOffAuto()
            {
                O1 = 0,
                O2 = 0,
                O3 = 0,
                O4 = 0,
                O5 = 0,
                O6 = 0,
                O7 = 0,
                O8 = 0,
                O9 = 0,
                O10 = 0,
                O11 = 0,
                O12 = 0,
                O13 = 0,
                O14 = 0,
            };
            WriteConfiguracionOnOffAuto(onOff);
        }


        public void WriteConfiguracionOnOffAutoFast(OnOffAutoFast value)
        {
            LogMethod();
            Modbus.Write<OnOffAutoFast>(value);
        }

        public void WriteConfiguracionOnOffAutoFast()
        {
            var onOff = new OnOffAutoFast()
            {
                O1 = 0,
                O2 = 0,
                O3 = 0,
                O4 = 0,
                O5 = 0,
                O6 = 0,
                O7 = 0,
                O8 = 0,
                O9 = 0,
                O10 = 0,
                O11 = 0,
                O12 = 0,
            };
            WriteConfiguracionOnOffAutoFast(onOff);
        }

        public void WriteConfiguracionDisplay(DisplayConfiguration value)
        {
            LogMethod();
            Modbus.Write<DisplayConfiguration>(value);
        }

        public void WriteConfiguracionDisplay(short IdiomaByte)
        {
            var configurationDisplay = new DisplayConfiguration()
            {
                BackLight = 0x0007,
                Idioma = IdiomaByte,
                BarraDisplay = 0x0003,
                OnOffAuto = 0x0000,
                SetupExpert = 0x0000
            };
            WriteConfiguracionDisplay(configurationDisplay);
        }

        public void WriteAlarmEnabled(AlarmConfiguration value)
        {
            LogMethod();
            Modbus.Write<AlarmConfiguration>((ushort)Registers.HABILITACION_ALARMAS, value);
        }

        public AlarmConfiguration ReadAlarmEnabled()
        {
            LogMethod();
            return Modbus.Read<AlarmConfiguration>((ushort)Registers.HABILITACION_ALARMAS);
        }

        public void WriteAlarmEnabled()
        {
            var resp = new AlarmConfiguration()
            {
                S1 = 1,
                S2 = 1,
                S3 = 1,
                S4 = 1,
                S5 = 0,
                S6 = 0,
                S7 = 0,
                S8 = 0,
                S9 = 0,
                S10 = 0,
                S11 = 0,
                S12 = 0,
                S13 = 0,
                S14 = 0,
                S15 = 0,
                S16 = 0,
                S17 = 0
            };
            WriteAlarmEnabled(resp);
        }

        public void WriteAlarmOutputConfiguration(AlarmConfiguration value)
        {
            LogMethod();
            Modbus.Write<AlarmConfiguration>((ushort)Registers.CONFIGURATION_SALIDAS_ALARMAS, value);
        }

        public void WriteAlarmOutputConfiguration()
        {
            var resp = new AlarmConfiguration()
            {
                S1 = 0,
                S2 = 0,
                S3 = 0,
                S4 = 0,
                S5 = 0,
                S6 = 0,
                S7 = 0,
                S8 = 0,
                S9 = 0,
                S10 = 0,
                S11 = 0,
                S12 = 0,
                S13 = 0,
                S14 = 0,
                S15 = 0,
                S16 = 0,
                S17 = 0
            };
            WriteAlarmOutputConfiguration(resp);
        }

        public void WriteAlarm1Configuration(Alarm1Configuration value)
        {
            LogMethod();
            Modbus.Write<Alarm1Configuration>(value);
        }

        public Alarm1Configuration ReadAlarm1Configuration()
        {
            LogMethod();
            return Modbus.Read<Alarm1Configuration>();
        }

        public void WriteAlarm1Configuration()
        {
            var configAlarm1 = new Alarm1Configuration()
            {
                THDVInf = 0x0005,
                THDVsup = 0x000A,
                THDIinf = 0x0004,
                THDIsup = 0x0005,
                TEMPinf = 0x0037,
                TEMPsup = 0x0046,
                ILEAKOnOff = 0,
                ILEAKLimit = 0x12C,
                ILEAKReset = 0,
                COSCoseno = 0x005F,
                COSETipo = 0x0001,
                COSLimit = 0x0014,
                FANEnabled = 0,
                FANLimitTemp = 0x0023,
            };

            WriteAlarm1Configuration(configAlarm1);
        }

        public void WriteAlarm2Configuration(Alarm2Configuration value)
        {
            LogMethod();
            Modbus.Write<Alarm2Configuration>(value);
        }

        public void WriteAlarm2Configuration()
        {
            var alarm = new Alarm2Configuration()
            {
                LimSupV = 0x000001B8, 
                LimInfV = 0x00000168,
                LimNumManiobras = 0x00001388,
            };
            WriteAlarm2Configuration(alarm);
        }

        public void WriteFactoresCalibracion(FactoresCalibracion value)
        {
            LogMethod();
            Modbus.Write<FactoresCalibracion>(value);
        }

        public void WriteFactoresCalibracion()
        {
            var gananciasDefecto = new FactoresCalibracion()
            {
                CalibracionVL1 = 5290,
                CalibracionVL2 = 5290,
                CalibracionVL3 = 5290,
                CalibracionIL1 = 1856,
                CalibracionIL2 = 1856,
                CalibracionIL3 = 1856,
                CalibracionVL12 = 5290,
                CalibracionVL23 = 5290,
                CalibracionVL31 = 5290,
                CalibracionPActivaL1 = 9909,
                CalibracionPActivaL2 = 9909,
                CalibracionPActivaL3 = 9909,
                CalibracionPReactivaL1 = 9909,
                CalibracionPReactivaL2 = 9909,
                CalibracionPReactivaL3 = 9909,
                CalibracionCorrienteNeutro = 1856,
                CalibracionCorrienteFugas = 1414,
                MinimoCorriente = 135,
                factorCorriente = 1160,
                CalibracionDesfaseL1 = 1000,
                CalibracionDesfaseL2 = 1000,
                CalibracionDesfaseL3 = 1000,
            };

            WriteFactoresCalibracion(gananciasDefecto);
         }

        public FactoresCalibracion ReadFactoresCalibracion()
        {
            LogMethod();
            return Modbus.Read<FactoresCalibracion>();
        }

        public void WritePorCentCorteSubTension(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.POR_CENT_CORTE_SUBTENSION, value);
        }

        public ushort ReadPorCentCorteSubTension()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.POR_CENT_CORTE_SUBTENSION);
        }

        public void WriteNumeroDeslastresCorteSubTension(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.NUMERO_DESLASTRES_CORTE_SUBTENSION, value);
        }

        public ushort ReadNumeroDeslastresCorteSubTension()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.NUMERO_DESLASTRES_CORTE_SUBTENSION);
        }

        #endregion

        #region Structures

        //***********************************************************
        //**********************************************************

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct VariablesFase
        {
            private Int32 tension;
            private Int32 corriente;
            private Int32 potenciaActiva;
            private Int32 potenciaInductiva;
            private Int32 potenciaCapicitiva;
            private Int32 potenciaReactiva;
            private Int32 potenciaAparente;
            private Int32 potenciaReactivaConsumida;
            private Int32 potenciaReactivaGenerada;
            private Int32 factorPotencia;
            private Int32 cosenoPhi;
            private Int32 PLsign;
            private Int32 QLsign;

            public double Tension { get { return Convert.ToDouble(tension) / 100; } }
            public double Corriente { get { return Convert.ToDouble(corriente) / 1000; } }
            public double PotenciaActiva { get { return Convert.ToDouble(potenciaActiva); } }
            public double PotenciaReactiva { get { return Convert.ToDouble(potenciaReactiva); } }
            public double PotenciaAparente { get { return Convert.ToDouble(potenciaAparente); } }
            public double CosenoPhi { get { return Convert.ToDouble(cosenoPhi) / 1000; } }

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x004E)]
        public struct VariablesTrifasicas
        {
            private Int32 tensionFaseIII;
            private Int32 corrienteIII;
            private Int32 potenciaActivaIII;
            private Int32 potenciaInductivaIII;
            private Int32 potenciaCapacitivaIII;
            private Int32 potenciaReactivaIII;
            private Int32 potenciaAparenteIII;
            private Int32 potenciaReactivaConsumidaIII;
            private Int32 potenciaReactivaGeneradaIII;
            private Int32 facttorPotenciaIII;
            private Int32 cosenoPhiIII;
            private Int32 PIIIsign;
            private Int32 QIIIsign;
            private Int32 frecuencia;
            private Int32 tensionLineaL1L2;
            private Int32 tensionLineaL2L3;
            private Int32 tensionLineaL3L1;
            private Int32 corrienteNeutro;
            private Int32 corrienteFugas;
            private Int32 temperaturaInt;
            private Int32 temperaturaExt;
            private Int32 tst_OP;
            private Int32 Ad_aux;

            public double TensionFaseIII { get { return tensionFaseIII / 100D; } }
            public double CorrienteIII { get { return corrienteIII / 1000D; } }
            public double PotenciaActivaIII { get { return Convert.ToDouble(potenciaActivaIII); } }
            public double PotenciaReactivaIII { get { return Convert.ToDouble(potenciaReactivaIII); } }
            public double PotenciaAparenteIII { get { return Convert.ToDouble(potenciaAparenteIII); } }
            public double CosenoPhiIII { get { return cosenoPhiIII / 1000D; } }
            public double TensionLineaL1L2 { get { return tensionLineaL1L2 / 100D; } }
            public double TensionLineaL2L3 { get { return tensionLineaL2L3 / 100D; } }
            public double TensionLineaL3L1 { get { return tensionLineaL3L1 / 100D; } }
            public double CorrienteFugas { get { return corrienteFugas / 1000D; } }
            public double CorrienteNeutro { get { return corrienteNeutro / 1000D; } }
            public double Frecuencia { get { return frecuencia / 10D; } }
            public double Temperatura { get { return temperaturaInt / 10D;  } } 
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x007C)]
        public struct THD
        {
            public Int32 THDtensionL1;
            public Int32 THDtensionL2;
            public Int32 THDtensionL3;
            public Int32 THDcorrienteL1;
            public Int32 THDcorrienteL2;
            public Int32 THDcorrienteL3;
        };

        public struct SumatoriosOffsets
        {
            public Sumatorios sumatorios;
            public Offsets offsets;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x4000)]
        public struct Sumatorios
        {
            public Int32 tensionL1;
            public Int32 corrienteL1;
            public Int32 tensionL2;
            public Int32 corrienteL2;
            public Int32 tensionL3;
            public Int32 corrienteL3;
            public Int32 potenciaActivaL1;
            public Int32 potenciaActivaL2;
            public Int32 potenciaActivaL3;
            public Int32 potenciaReactivaL1;
            public Int32 potenciaReactivaL2;
            public Int32 potenciaReactivaL3;
            public Int32 tensionCompuestaL12;
            public Int32 tensionCompuestaL23;
            public Int32 tensionCompuestaL31;
            public Int32 corrienteFugas;
            public Int32 corrienteNeutro;
            public Int32 Tst_op;
            public Int32 Temp_in;
            public Int32 Temp_ext;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x40E0)]
        public struct Offsets
        {
            public Int32 offsetTensionL1;
            public Int32 offsetTensionL2;
            public Int32 offsetTensionL3;
            public Int32 offsetCorrienteL1;
            public Int32 offsetCorrienteL2;
            public Int32 offsetCorrienteL3;
            public Int32 offsetCorrienteNeutro;
            public Int32 offsetCorrienteFugas;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address =(ushort)Registers.FACTORES_CALIBRACION)]
        public struct FactoresCalibracion
        {
            public UInt16 CalibracionVL1;
            public UInt16 CalibracionVL2;
            public UInt16 CalibracionVL3;
            public UInt16 CalibracionIL1;
            public UInt16 CalibracionIL2;
            public UInt16 CalibracionIL3;
            public UInt16 CalibracionVL12;
            public UInt16 CalibracionVL23;
            public UInt16 CalibracionVL31;
            public UInt16 CalibracionPActivaL1;
            public UInt16 CalibracionPActivaL2;
            public UInt16 CalibracionPActivaL3;
            public UInt16 CalibracionPReactivaL1;
            public UInt16 CalibracionPReactivaL2;
            public UInt16 CalibracionPReactivaL3;
            public UInt16 CalibracionCorrienteNeutro;
            public UInt16 CalibracionCorrienteFugas;
            public UInt16 MinimoCorriente;
            public UInt16 factorCorriente;
            public UInt16 CalibracionDesfaseL1;
            public UInt16 CalibracionDesfaseL2;
            public UInt16 CalibracionDesfaseL3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConfigurationCom
        {
            public Int16 protocolo;
            public Int16 periferico;
            public Int16 baudrate;
            public Int16 paridad;
            public Int16 databits;
            public Int16 bitsstop;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RelacionTrafosCorriente
        {
            public Int16 Primario;
            public Int16 Secundario;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RelacionTrafosTension
        {
            public Int32 Primario;
            public Int32 Secundario;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_MANIOBRA)]
        public struct ConfiguraciManiobra
        {
            public Int16 C1;
            public Int16 C2;
            public Int16 C3;
            public Int16 C4;
            public Int16 T1;
            public Int16 T2;
            public Int16 T3;
            public Int16 T4;
            public Int16 CK;
            public Int16 PR;
            public Int16 TM;
            public Int16 ST;
            public Int16 TO;
            public Int16 TR;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_TIPO_CONEXION)]
        public struct ConfiguracionTipoConexion
        {
            public Int16 TC;
            public Int16 PH;
            public Int16 C1;
            public Int16 C2;
            public Int16 C3;
        };    

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct AlarmConfiguration
        {
            public Int16 S1;
            public Int16 S2;
            public Int16 S3;
            public Int16 S4;
            public Int16 S5;
            public Int16 S6;
            public Int16 S7;
            public Int16 S8;
            public Int16 S9;
            public Int16 S10;
            public Int16 S11;
            public Int16 S12;
            public Int16 S13;
            public Int16 S14;
            public Int16 S15;
            public Int16 S16;
            public Int16 S17;

            public override string ToString()
            {
                return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15} {16} {17}", S1, S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,S16,S17);
            }
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_ONOFF_AUTO)]
        public struct OnOffAuto
        {
            public Int16 O1;
            public Int16 O2;
            public Int16 O3;
            public Int16 O4;
            public Int16 O5;
            public Int16 O6;
            public Int16 O7;
            public Int16 O8;
            public Int16 O9;
            public Int16 O10;
            public Int16 O11;
            public Int16 O12;
            public Int16 O13;
            public Int16 O14;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_ONOFF_AUTO)]
        public struct OnOffAutoFast
        {
            public Int16 O1;
            public Int16 O2;
            public Int16 O3;
            public Int16 O4;
            public Int16 O5;
            public Int16 O6;
            public Int16 O7;
            public Int16 O8;
            public Int16 O9;
            public Int16 O10;
            public Int16 O11;
            public Int16 O12;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_DISPLAY)]
        public struct DisplayConfiguration
        {
            public Int16 OnOffAuto;
            public Int16 BackLight;
            public Int16 Idioma;
            public Int16 SetupExpert;
            public Int16 BarraDisplay;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_ALARMAS1)]
        public struct Alarm1Configuration
        {
            public Int16 THDVInf;
            public Int16 THDVsup;
            public Int16 THDIinf;
            public Int16 THDIsup;
            public Int16 TEMPinf;
            public Int16 TEMPsup;
            public Int16 ILEAKOnOff;
            public Int16 ILEAKLimit;
            public Int16 ILEAKReset;
            public Int16 COSCoseno;
            public Int16 COSLimit;
            public Int16 COSETipo;
            public Int16 FANLimitTemp;
            public Int16 FANEnabled;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONFIGURATION_ALARMAS2)]
        public struct Alarm2Configuration
        {
            public Int32 LimSupV;
            public Int32 LimInfV;
            public Int32 LimNumManiobras;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.REGISTRO_HARDWARE)]
        public struct VectorHardwareStruct
        {
            public UInt16 Registro1;
            public UInt16 Registro2;
            public UInt16 Registro3;
            public UInt16 Registro4;
        };

        public struct DisplaySegemnts
        {
            public bool LimSupV;
        }

        #endregion

        #region Enumerators


        public enum Registers
        {
            FLAG_TEST = 0x2AF8,
            BACKLIGHT = 0x0050, //80,
            ALL_DISPLAY = 0x0071,//112,
            RELAYS = 0x0020,
            RELE_ALARMA = 0x0043, 
            RELE_VENTILADOR = 0x0042,
            LEDS = 0x0011, 

            DIGITAL_OUTPUTS_1 = 0x0032, //48,
            DIGITAL_OUTPUTS_2 = 0x0033, //48,

            DIGITAL_INPUTS = 0x5000, //48,
            TECLADO = 0x5000,

            FIRMWARE_VERSION = 0x01020,
            CODERROR = 0x01040, // 4160,
            SERIAL_NUMBER = 0x1000, //4096,
            BASTIDOR = 0x1010, //4112,
            REGISTRO_HARDWARE = 0x1030,
            CONFIGURATION_COM1 = 0x1070, //4208,
            CONFIGURATION_COM2 = 0x1080, //4224,
            RELATION_TRAFO_CURRENT = 0x1090, //4240,
            RELATION_TRAFO_VOLTAGE = 0x1095, //4245,
            CONFIGURATION_MANIOBRA = 0x1130, //
            CONFIGURATION_TIPO_CONEXION = 0x1100, //
            CONFIGURATION_ONOFF_AUTO = 0x1110, //
            CONFIGURATION_DISPLAY = 0x1125, // 4389,
            CONFIGURATION_ALARMAS1 = 0x1140, // ,
            CONFIGURATION_ALARMAS2 = 0x114E, // ,
            HABILITACION_ALARMAS = 0x1155, // ,
            CONFIGURATION_SALIDAS_ALARMAS = 0x1170, // ,
            FACTORES_CALIBRACION = 0x4500,

            POR_CENT_CORTE_SUBTENSION = 0x1190,
            NUMERO_DESLASTRES_CORTE_SUBTENSION = 0x1191,

            RESET = 0x2000,
            SETUP_DEFAULT = 0x0300,

            CARGA_MINIMA_MAXIMA = 0x0060, //96,
            BORRADO_ENERGIAS = 0x0230, //560,
            BORRADO_MAXIMIN = 0x0220, //544,
            BORRADO_NUMCONN = 0x0250, //592,

            JUMP_BOOT = 0x32C8,
            LOAD_FIRMWARE = 0x32CA,
        }

        public enum Leds
        {
            ALL,
            LED_CPU,
            LED_FAN,
            LED_ALARM,
            LED_KEY
        }

        #endregion

        #region Vector de Wardware

        public class HardwareVector
        {
            private string modeloString;
            private string modeloP2String;

            public bool TipoSalida { get; set; }
            public bool ComCPC { get; set; }
            public bool Ethernet { get; set; }
            public bool red3G { get; set; }
            public bool tempExt { get; set; }
            public bool Shunt { get; set; }

            private string inputCurrentString;
            private string inputVoltageString;
            private string powerSupplyString;
            private string marcaString;
            private string numRelesString;

            public string TipoSalidaString { get { return TipoSalida == true ? "RELES" : "OPTOS"; } }
            public string ComCPCString { get { return ComCPC == true ? "SI" : "NO"; } }
            public string EthernetString { get { return Ethernet == true ? "SI" : "NO"; } }
            public string red3GString { get { return red3G == true ? "SI" : "NO"; } }
            public string tempExtString { get { return tempExt == true ? "SI" : "NO"; } }
            public string ShuntString { get { return Shunt == true ? "SI" : "NO"; } }

            public NumRelesEnum NumReles { get; set; }
            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }
            public VectorHardwareModeloP2 ModeloP2 { get; set; }
            public VectorHardwareMarca Marca { get; set; }

            public string VectorHardwareHexString { get; internal set; }

            public string NumRelesString
            {
                get
                {
                    return numRelesString;
                }
                set
                {
                    numRelesString =string.Format("RELES_{0}", value);
                    NumRelesEnum reles;
                    if (!NumRelesEnum.TryParse<NumRelesEnum>(numRelesString, out reles))
                        throw new Exception("Error valor de modelo no permitido en la codificación del Numero de Reles");

                    NumReles = reles;
                }
            }


            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    VectorHardwareSupply powerSupply;
                    powerSupplyString = value;

                    if (!VectorHardwareSupply.TryParse<VectorHardwareSupply>(powerSupplyString, out powerSupply))
                        throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");

                    PowerSupply = powerSupply;
                }
            }

            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    VectorHardwareVoltageInputs inputVoltage;
                    inputVoltageString = value;

                    if (!VectorHardwareVoltageInputs.TryParse<VectorHardwareVoltageInputs>(inputVoltageString, out inputVoltage))
                        throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");

                    InputVoltage = inputVoltage;
                }
            }

            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    VectorHardwareCurrentInputs inputCurrent;
                    if (!VectorHardwareCurrentInputs.TryParse<VectorHardwareCurrentInputs>(inputCurrentString, out inputCurrent))
                        throw new Exception("Error valor de entradas de corriente no permitido en la codificación del Vector de Hardware");

                    InputCurrent = inputCurrent;
                }
            }

            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    VectorHardwareModelo modelo;
                    modeloString = value;
                    if (!VectorHardwareModelo.TryParse<VectorHardwareModelo>(modeloString, out modelo))
                        throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");

                    Modelo = modelo;
                }
            }

            public string ModeloP2String
            {
                get
                {
                    return modeloP2String;
                }
                set
                {
                    modeloP2String = value;
                    VectorHardwareModeloP2 modeloP2;
                    if (!VectorHardwareModeloP2.TryParse<VectorHardwareModeloP2>(modeloP2String, out modeloP2))
                        throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");

                    ModeloP2 = modeloP2;
                }
            }

            public string MarcaString
            {
                get
                {
                    return marcaString;
                }
                set
                {
                    marcaString = value;
                    VectorHardwareMarca marca;
                    if (!VectorHardwareMarca.TryParse<VectorHardwareMarca>(marcaString, out marca))
                        throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");

                    Marca = marca;
                }
            }

            public HardwareVector()
            {

            }

            public VectorHardwareStruct ToStructure()
            {
                var Register1 = new bool[16];
                Register1[0] = ((int)Modelo & 0x01) == 0x01;
                Register1[1] = ((int)Modelo & 0x02) == 0x02;
                Register1[2] = ((int)Modelo & 0x04) == 0x04;
                Register1[3] = ((int)Modelo & 0x08) == 0x08;
                Register1[4] = ((int)ModeloP2 & 0x010) == 0x010;
                Register1[5] = ((int)ModeloP2 & 0x020) == 0x020;
                Register1[6] = ((int)ModeloP2 & 0x040) == 0x040;
                Register1[7] = ((int)ModeloP2 & 0x080) == 0x080;

                Register1[8] = TipoSalida;
                Register1[9] = ((int)NumReles & 0x01) == 0x01; 
                Register1[10] = ((int)NumReles & 0x10) == 0x10; 
                Register1[11] = ComCPC;
                Register1[12] = Ethernet;
                Register1[13] = red3G;
                Register1[14] = tempExt;

                var Register1Ushort = Register1.ArrayBoolToRegister();

                var Register2 = new bool[16];
                Register2[0] = ((int)InputCurrent & 0x01) == 0x01;
                Register2[1] = ((int)InputCurrent & 0x02) == 0x02;
                Register2[2] = ((int)InputCurrent & 0x04) == 0x04;
                Register2[3] = ((int)InputCurrent & 0x08) == 0x08;
                Register2[4] = Shunt;
                Register2[5] = ((int)InputVoltage & 0x20) == 0x20;
                Register2[6] = ((int)InputVoltage & 0x40) == 0x40;
                Register2[7] = ((int)InputVoltage & 0x80) == 0x80;
                Register2[8] = ((int)PowerSupply & 0x0100) == 0x0100;
                Register2[9] = ((int)PowerSupply & 0x0200) == 0x0200;
                Register2[10] = ((int)PowerSupply & 0x0400) == 0x0400;
                Register2[11] = false;
                Register2[12] = false;
                Register2[13] = false;
                Register2[14] = false;

                var Register2Ushort = Register2.ArrayBoolToRegister();

                var Register4 = new bool[16];
                Register4[0] = ((int)Marca & 0x01) == 0x01;
                Register4[1] = ((int)Marca & 0x02) == 0x02;
                Register4[2] = ((int)Marca & 0x04) == 0x04;
                Register4[3] = ((int)Marca & 0x08) == 0x08;

                var Register4Ushort = Register4.ArrayBoolToRegister();

                VectorHardwareStruct vectorHardware = new VectorHardwareStruct()
                {
                    Registro1 = Register1Ushort,
                    Registro2 = Register2Ushort,
                    Registro3 = 0,
                    Registro4 = Register4Ushort,
                };

                VectorHardwareHexString = string.Format("{0:X4}{1:X4}{2:X4}{3:X4}", Register1Ushort, Register2Ushort, 0, Register4Ushort);

                return vectorHardware;
            }
        }

        public enum NumRelesEnum
        {
            RELES_6 = 0x01,
            RELES_12 = 0,
            RELES_14 = 0x10
        }

        public enum VectorHardwareVoltageInputs 
        {
            V_100_480 = 0,
            V_100_690 = 1,
        }

        public enum VectorHardwareCurrentInputs
        {
            IN_5 = 0,
            IN_1 = 1,
            IN_025 = 2,
        }

        public enum VectorHardwareSupply
        {
            Vac_110_480V = 0,
        }

        public enum VectorHardwareModelo
        {
            SMART_NORMAL = 0,
            SMART_FAST =1,
            SMART_MT = 2,
            SMART_FAST_12V =3
        }

        public enum VectorHardwareModeloP2
        {
            SOLO_SALIDAS = 0,
            SALIDAS_ETHERNET = 1,
            SALIDAS_3G = 2,
            SALIDAS_TEMP_EXT = 3
        }

        public enum VectorHardwareMarca
        {
            CIRCUTOR = 0,
            LIFASA = 1,
            DNELE = 2,
            APS = 3,
            RITA = 4,
            ZEZ_SILKO = 5
        }




        #endregion

    }
}