﻿using ArenaNET;
using Dezac.Core.Exceptions;
using log4net;

namespace Instruments.Cameras
{
    public static class ArenaExtension
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ArenaExtension));

        public static long SetIntValue(this IInteger node, long value)
        {
            // Ensure increment
            long value2;
            if (value != (value2 = (((value - node.Min) / node.Inc) * node.Inc) + node.Min) )
            {
                logger.Warn($"El parametro {node.Description} solo acepta incrementos de {node.Inc}, el valor ha sido modificado de {value} a {value2}");
                value = value2;
            }
            // Check min/max values
            if (value < node.Min)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO(
                    $"Error Camara LUCID: Se ha asignado al parametro {node.Description} el valor {value}, cuando el mínimo es {node.Min} ").Throw();
            if (value > node.Max)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO(
                    $"Error Camara LUCID: Se ha asignado al parametro {node.Description} el valor {value}, cuando el máximo es {node.Max} ").Throw();
            // set value
            node.Value = value;
            // return value for output
            return value;
        }

        public static double SetFloatValue(this IFloat node, double value)
        {
            // Ensure increment
            double value2;
            if (node.HasInc && value != (value2 = (((value - node.Min) / node.Inc) * node.Inc) + node.Min))
            {
                logger.Warn($"El parametro {node.Description} solo acepta incrementos de {node.Inc}, el valor ha sido modificado de {value} a {value2}");
                value = value2;
            }
            // Check min/max values
            if (value < node.Min)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO(
                    $"Error Camara LUCID: Se ha asignado al parametro {node.Description} el valor {value}, cuando el mínimo es {node.Min} ").Throw();
            if (value > node.Max)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO(
                    $"Error Camara LUCID: Se ha asignado al parametro {node.Description} el valor {value}, cuando el máximo es {node.Max} ").Throw();
            // set value
            node.Value = value;
            // return value for output
            return value;
        }

        public static void LogSetting(this IInteger node)
        {
            logger.Info($"{node.DisplayName} -> {node.Value}");
        }

        public static void LogSetting(this IFloat node)
        {
            logger.Info($"{node.DisplayName} -> {node.Value}");
        }

        public static void LogSetting(this IEnumeration node)
        {
            logger.Info($"{node.DisplayName} -> {node.Symbolic}");
        }

        public static void LogSetting(this IBoolean node)
        {
            logger.Info($"{node.DisplayName} -> {node.Value.ToString()}");
        }
    }
}
