﻿using AssemblyResolve;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WinPublisher
{
    public static class RepositoryGit
    {
        public static string pathGitLab = string.Format("C:\\Users\\{0}\\Documents\\GitLab\\SAURON", Environment.UserName);

        public static List<string> ReadCommits()
        {
            using (var repo = new Repository(pathGitLab))
            {
                var develop = repo.Branches.Where((p) => p.FriendlyName == "DEVELOPMENT").FirstOrDefault();
                var body = string.Format("SYNC & PRAY");
                var commits = develop.Commits.Where((p) => p.MessageShort.Contains(body)).ToList().FirstOrDefault();
                if (commits == null)
                    return null;

                var filter = new CommitFilter();
                filter.ExcludeReachableFrom = commits.Sha;
                var commitsFilter = repo.Commits.QueryBy(filter).ToList();
                return commitsFilter.Select(x => x.Message.ToString()).ToList();
            }
        }

        public static void Commit(FileAssemblyItem fileSourceChangeVersion)
        {
            var body = string.Format("WINPUBLISHER -> Fichero: {0} version: {1}", fileSourceChangeVersion.NameAssembly, fileSourceChangeVersion.NewVersion);

            using (var repo = new Repository(pathGitLab))
            {
                Signature author = new Signature(Environment.UserName, Environment.UserName + "@dezac.com", DateTime.Now);
                Signature committer = author;

                var pathRelative = Path.Combine(fileSourceChangeVersion.PathProjectAssembly, "Properties", "AssemblyInfo.cs");
                var indexPath = pathRelative.IndexOf("SAURON") + 7;
                var relativePath = pathRelative.Substring(indexPath);
                Commands.Stage(repo, relativePath);
                Commit commit = repo.Commit(body, author, committer);
            }
        }

        public static void CommitSync(string pathRelative)
        {
            var body = string.Format("SYNC & PRAY");

            using (var repo = new Repository(pathGitLab))
            {
                Signature author = new Signature(Environment.UserName, Environment.UserName + "@dezac.com", DateTime.Now);
                Signature committer = author;

                var indexPath = pathRelative.IndexOf("SAURON") + 7;
                var relativePath = pathRelative.Substring(indexPath);
                Commands.Stage(repo, relativePath);
                Commit commit = repo.Commit(body, author, committer);
            }
        }

        public static void Push()
        {
            using (var repo = new Repository(pathGitLab))
            {
                Signature author = new Signature("Ferran", "flsanchez@dezac.com", DateTime.Now);
                Signature committer = author;

                Remote remote = repo.Network.Remotes.FirstOrDefault();
                var options = new PushOptions();
                options.CredentialsProvider = (_url, _user, _cred) =>
                    new UsernamePasswordCredentials { Username = "flsanchez", Password = "wallace2019" };

                repo.Network.Push(remote, repo.Branches["DEVELOPMENT"].CanonicalName, options);
            }
        }
    }
}
