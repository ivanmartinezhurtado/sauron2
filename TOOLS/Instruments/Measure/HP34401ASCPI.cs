﻿using Instruments.SCPI;
using System;

namespace Instruments.Measure
{
    public class HP34401ASCPI : SCPIDevice
    {
        internal HP34401ASCPI(HP34401A device)
            : base(device)
        {
        }

        public Measure MEASure { get { return new Measure { Parent = context, Cmd = ":MEAS" }; } }

        public class Measure : SCPIContext
        {
            private const string DEFAULT = "DEF";
            private const string MAXIM = "MAX";

            public Magnitude VOLTage { get { return new Magnitude { Parent = this, Cmd = ":VOLT" }; } }
            public Magnitude CURRent { get { return new Magnitude { Parent = this, Cmd = ":CURR" }; } }

            public SCPIQueryCommand<double> RESistance(string range = DEFAULT, string resolution = DEFAULT)
            {
                return new SCPIQueryCommand<double>(this, ":RES", range, resolution);
            }

            public SCPIQueryCommand<double> FREQuence(string range = DEFAULT, string resolution = DEFAULT)
            {
                return new SCPIQueryCommand<double>(this, ":FREQ", range, resolution);
            }

            public SCPIQueryCommand<double> PERiod(string range = DEFAULT, string resolution = DEFAULT)
            {
                return new SCPIQueryCommand<double>(this, ":PER", range, resolution);
            }

            public SCPIQueryCommand<double> DIODe(string range = DEFAULT, string resolution = DEFAULT)
            {
                return new SCPIQueryCommand<double>(this, ":DIOD", range, resolution);
            }

            public class Magnitude : SCPIContext
            {
                public SCPIQueryCommand<Single> AC(string range = DEFAULT, string resolution = DEFAULT)
                {
                    return new SCPIQueryCommand<Single>(this, ":AC", range, resolution);
                }

                public SCPIQueryCommand<Single> DC(string range = DEFAULT, string resolution = DEFAULT)
                {
                    return new SCPIQueryCommand<Single>(this, ":DC", range, resolution);
                }
            }
        }
    }
}
