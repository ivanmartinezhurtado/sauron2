﻿using System;

using System.Windows.Forms;

namespace Dezac.Tests.AOI
{
    static class Program
    {
        [STAThread]
        static void Main()
        {  
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AOIStatisticsForm());
            //Application.Run(new AOIResultViewerForm("0", "038"));
        }
    }
}
