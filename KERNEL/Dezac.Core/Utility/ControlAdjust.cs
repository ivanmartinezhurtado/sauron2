﻿using System.ComponentModel;

namespace Dezac.Core.Utility
{
    // TODO Cambiar nombre de las properties al ingles
    public class ConfigurationSampler
    {
        [Description("Tiempo de espera para la primera iteración")]
        public int InitDelayTime { get; set; } //ToDo Miki: Cambiado de TiempoEsperaInicial a InitDelayTime
        
        [Description("Número de muestras que se quiere hacer")]
        public int SampleNumber { get; set; } //ToDo Miki: Cambiado de NumeroMuestras a SampleNumber
        
        [Description("Número máximo de muestras que se quiere hacer")]
        public int SampleMaxNumber { get; set; }  //ToDo Miki: Cambiado de NumeroMaximoMuestras a SampleMaxNumber
        
        [Description("Tiempo entre una muestra y la siguiente")]
        public int DelayBetweenSamples { get; set; } //ToDo Miki: Cambiado de TiempoEntreMuestras a DelayBetweenSamples

        public override string ToString()
        {
            return string.Format("InitDelayTime={0}  SampleNumber={1} SampleMaxNumber={2} DelayBetweenSamples={3}", InitDelayTime, SampleNumber, DelayBetweenSamples, SampleMaxNumber);
        }
    }

    // TODO Cambiar nombre de las properties al ingles
    public class ConfigurationSamplerWithCancel
    {
        [Description("Tiempo de espera para la primera iteración")]
        public int InitDelayTime { get; set; } //ToDo Miki: Cambiado de TiempoEsperaInicial a InitDelayTime
        [Description("Maximum number of iterations")]
        public byte IterationNumber { get; set; } //ToDo Miki: Cambiado de NumeroIteraciones a IterationNumber
        [Description("Delay between iterations")]
        public int DelayBetweenIteration { get; set; } //ToDo Miki: Cambiado de TiempoEntreIteraciones a DelayBetweenIteration
        [Description("Cancel on exception")]
        public bool CancelIfError { get; set; } //ToDo Miki: Cambiado de CancelarSiHayError a CancelIfError
        [Description("Throw exception?")]
        public bool ThrowException { get; set; } //ToDo Miki: Cambiado de LanzarExcepción a ThrowException

        public override string ToString()
        {
            return string.Format("InitDelayTime={0}  IterationNumber={1} DelayBetweenIteration={2} CancelIfError={3} ThrowException={4}", InitDelayTime, IterationNumber, DelayBetweenIteration, CancelIfError, ThrowException);
        }
    }
}
