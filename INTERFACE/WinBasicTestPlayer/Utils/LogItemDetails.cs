﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace WinTestPlayer.Views
{
    public partial class LogItemDetails : RadForm
    {
        public LogItemDetails()
        {
            InitializeComponent();
        }

        public string LogText
        {
            get { return txtLog.Text; }
            set { txtLog.Text = value; }
        }

        private void btCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(LogText);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
