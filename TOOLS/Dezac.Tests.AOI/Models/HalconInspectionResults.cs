﻿using Dezac.Tests.Services;
using HalconDotNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Dezac.Tests.AOI.Models
{
    public class HalconInspectionResults
    {
        [DefaultValue(false)]
        public bool Success { get; set; }
        public Exception Exception { get; set; }
        public List<BoardResult> Boards { get; set; }
        [JsonIgnore]
        public ReadOnlyCollection<ResultEnum> WeldingsResult
        {
            get
            {
                List<ResultEnum> weldingsResult = new List<ResultEnum>();
                foreach (BoardResult board in Boards)
                foreach (WeldingResult welding in board.Weldigns)
                    weldingsResult.Add(welding.Classification);
                return weldingsResult.AsReadOnly();
            }
        }
        [JsonIgnore]
        public ReadOnlyCollection<RepairEnum> WeldingsRepair 
        {
            get
            {
                List<RepairEnum> weldingsRepair = new List<RepairEnum>();
                foreach (BoardResult board in Boards)
                foreach (WeldingResult welding in board.Weldigns)
                    weldingsRepair.Add(welding.Repair);
                return weldingsRepair.AsReadOnly();
            }
        }
        [JsonIgnore]
        public ReadOnlyCollection<ResultEnum> BoardsResult
        {
            get
            {
                List<ResultEnum> boardsResult = new List<ResultEnum>();
                foreach (BoardResult board in Boards)
                    boardsResult.Add(board.Classification);
                return boardsResult.AsReadOnly();
            }
        }
        public string[] Barcodes { get; set; }
        public int NumProduct { get; set; }
        public string IdFase { get; set; }
        public int NumOrden { get; set; }
        public string NumOperario { get; set; }
        public int NumFamilia { get; set; }
        public DateTime Date { get; set; }

        public long ExtractWeldingsTime { get; set; }
        public long ReadBarcodesTime { get; set; }
        public long ExtractFeaturesTime { get; set; }
        public long ClassificationTime { get; set; }
        public long GrabImageTime { get; set; }
        public long SaveImageTime { get; set; }

        [JsonIgnore]
        public string ExtractWeldingsTimeSeconds { get { return (ExtractWeldingsTime / 1000D).ToString("0.000") + " s"; } }
        [JsonIgnore]
        public string ReadBarcodesTimeSeconds { get { return (ReadBarcodesTime / 1000D).ToString("0.000") + " s"; } }
        [JsonIgnore]
        public string ExtractFeaturesTimeSeconds { get { return (ExtractFeaturesTime / 1000D).ToString("0.000") + " s"; } }
        [JsonIgnore]
        public string ClassificationTimeSeconds { get { return (ClassificationTime / 1000D).ToString("0.000") + " s"; } }
        [JsonIgnore]
        public string GrabImageTimeSeconds { get { return (GrabImageTime / 1000D).ToString("0.000") + " s"; } }
        [JsonIgnore]
        public string SaveImageTimeSeconds { get { return (SaveImageTime / 1000D).ToString("0.000") + " s"; } }

        public HalconInspectionResults(string[] barcodes, double[] scores, int[] classificator)
        {
            Barcodes = barcodes;   
            Boards = new List<BoardResult>();

            var weldingForBoard = scores.Length / barcodes.Length;

            if (classificator.Length == 0)
                classificator = Enumerable.Repeat(0, weldingForBoard).ToArray();

            var j = 0;
            for (int boardNumber = 0; boardNumber <= barcodes.Length - 1 ; boardNumber++)
            {
                var boardScores = scores.Skip(boardNumber * weldingForBoard).Take(weldingForBoard).ToList();

                var weldings = new List<WeldingResult>();
                for (int weldingNumber = 0; weldingNumber <= boardScores.Count() - 1; weldingNumber++)
                    weldings.Add(new WeldingResult() { Score = boardScores[weldingNumber], IDBoard = weldingNumber, IDPanel = j++, BoardNumber = boardNumber, Classificator = classificator[weldingNumber] });

                Boards.Add(new BoardResult() { Weldigns = weldings, Barcode = barcodes[boardNumber], Position = boardNumber });
            }

            Success = true;
        }

        public void AddRepair(int index, RepairEnum repair)
        {
            int board = index / Boards.First().Weldigns.Count() ;
            int welding = index % Boards.First().Weldigns.Count();

            Boards[board].Weldigns[welding].Repair = repair;
        }

        public void AddError(int index)
        {
            int board = index / Boards.First().Weldigns.Count();
            int welding = index % Boards.First().Weldigns.Count();

            Boards[board].Weldigns[welding].Score = 0;
        }

        public int GetBoardOfWelding(int weldingIndex) { return weldingIndex / Boards[0].Weldigns.Count; }


        [JsonConstructor]
        public HalconInspectionResults()
        {
            Success = false;
        }    
    }

    public class BoardResult
    {
        public int Position { get; set; }
        public string Barcode { get; set; }
        public ResultEnum Classification
        {
            get
            {
                return ((weldingsKO.Length + weldingsND.Length - WeldingsRepaired.Length) > 0) ? ResultEnum.KO : ResultEnum.OK;
            }
        }
        public List<WeldingResult> Weldigns { get; set; }
        [JsonIgnore]
        public int[] WeldingsOK
        {
            get
            {
                var list = new List<int>();
                foreach (WeldingResult welding in Weldigns)
                    if (welding.Classification == ResultEnum.OK)
                        list.Add(welding.IDBoard);
                return list.ToArray();
            }
        }
        [JsonIgnore]
        public int[] weldingsKO
        {
            get
            {
                var list = new List<int>();
                foreach (WeldingResult welding in Weldigns)
                    if (welding.Classification == ResultEnum.KO)
                        list.Add(welding.IDBoard);
                return list.ToArray();
            }
        }
        [JsonIgnore]
        public int[] weldingsND
        {
            get
            {
                var list = new List<int>();
                foreach (WeldingResult welding in Weldigns)
                    if (welding.Classification == ResultEnum.ND)
                        list.Add(welding.IDBoard);
                return list.ToArray();
            }
        }
        [JsonIgnore]
        public int[] WeldingsRepaired
        {
            get
            {
                var list = new List<int>();
                foreach (WeldingResult welding in Weldigns)
                    if (welding.Repair != RepairEnum.SIN_REPARACION)
                        list.Add(welding.IDBoard);
                return list.ToArray();
            }
        }
        [JsonIgnore]
        public RepairEnum[] WeldingsReparations
        {
            get
            {
                var list = new List<RepairEnum>();
                foreach (WeldingResult welding in Weldigns)
                    list.Add(welding.Repair);
                return list.ToArray();
            }
        }

        [JsonConstructor]
        public BoardResult()
        {
        }
    }

    public class WeldingResult
    {
        public double Score { get; set; }
        public ResultEnum Classification
        {
            get
            {
                return Score == 1 ? ResultEnum.OK : ResultEnum.KO; 
            }
            set { }
        }
        public RepairEnum Repair { get; set; }
        public bool MissClassified 
        {
            get 
            {
                if (Classification == ResultEnum.OK &&
                    (Repair != RepairEnum.SIN_REPARACION && Repair != RepairEnum.DESCARTADO && Repair != RepairEnum.FALSO_FALLO))
                    return true;
                return false;
            } 
        }
        public int IDBoard { get; set; }
        public int IDPanel { get; set; }
        public int BoardNumber { get; set; }
        public int Classificator { get; set; }

        [JsonConstructor]
        public WeldingResult()
        {
        }
    }

    public enum ResultEnum
    {
        OK = 1,
        KO = -1,
        ND = 0,
    }

    public enum RepairEnum
    {
        SIN_REPARACION,
        FALSO_FALLO,
        FALTA_ESTAÑO,
        FORMA_DEFECTUOSA,
        CRUCE,
        PATA_LARGA,
        FALTA_COMPONENTE,
        COMPONENTE_LEVANTADO,
        OTRO_FALLO,
        DESCARTADO,
    }
}
