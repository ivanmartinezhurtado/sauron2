﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Tests.Dataman.Controls
{
    public partial class DataManView : Form
    {
        private DatamanManager manager;
        private List<DataManControlSite> sites = new List<DataManControlSite>();

        public DataManView()
        {
            InitializeComponent();
        }

        [Browsable(false)]
        public DatamanManager Manager
        {
            get { return manager; }
            set
            {
                SetManager(value);
            }
        }     

        private void SetManager(DatamanManager manager)
        {
            if (this.manager == manager)
                return;

            if (this.manager != null)
            {
                //this.manager.Log -= OnManagerLog;
                //this.manager.StateChanged -= UpdateUI;
                this.manager.OperationCompleted -= OnManagerOperationCompleted;
                this.manager.OnConnected -= OnManagerConnected;
                this.timer.Enabled = false;
            }

            this.manager = manager;

            if (this.manager != null)
            {
                //this.manager.Log += OnManagerLog;
                //this.manager.StateChanged += UpdateUI;
                this.manager.OperationCompleted += OnManagerOperationCompleted;
                this.manager.OnConnected += OnManagerConnected;
            }
        }

        private void OnManagerConnected(object sender, EventArgs e)
        {
            sites.Clear();

            var width = 0;

            foreach (var site in manager.Sites)
            {
                var ctrl = new DataManControlSite(this.manager, site.Site);
                flow.Controls.Add(ctrl);
                width += ctrl.Width + 50;
                sites.Add(ctrl);
            }

            if (manager.Sites.Count == 1)
                width += width;
            
            this.Width = width;

            this.timer.Enabled = true;
        }

        private void OnManagerOperationCompleted(object sender, EventArgs e)
        {
            if (manager.DefaultSite.OpResult == DatamanManager.DetailedOpResult.NoCreditBox)
                this.DialogResult = DialogResult.Cancel;
            else
            {
                var result = DialogResult.OK;

                foreach (var site in manager.Sites)
                    if (site.OpResult != DatamanManager.DetailedOpResult.Good)
                    {
                        result = site.OpResult== DatamanManager.DetailedOpResult.TimeOut ? DialogResult.Cancel : DialogResult.Abort;
                        break;
                    }

                this.DialogResult = result;
            }
        }

        private void Log(string text)
        {
            this.txtLog.Text = text;
            this.txtLog.SelectionStart = this.txtLog.Text.Length;
            this.txtLog.ScrollToCaret();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Log(manager.Log);
            sites.ForEach(p => p.UpdateUI());
        }

        protected override void Dispose(bool disposing)
        {
            Manager = null;

            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }
    }
}
