﻿using Dezac.Data;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WinTestPlayer.Model;
using WinTestPlayer.Services;

namespace WinTestPlayer.Views
{
    public partial class ProductsView : UserControl
    {
        protected readonly AppService app;
        protected readonly MainForm mainForm;
        protected readonly ShellService shell;

        public ProductsView()
        {
            InitializeComponent();
        }

        public ProductsView(AppService app
            , MainForm mainForm
            , ShellService shell)
            : this()
        {
            this.app = app;
            this.mainForm = mainForm;
            this.shell = shell;

            UpdateUI();
        }

        private void UpdateUI()
        {
            bool tsd2015 = cmdTSD2015.CheckState == CheckState.Checked;

            using (DezacContext db = DI.Resolve<DezacContext>())
            {
                var list = db.PRODUCTO.Include("ARTICULO").Include("T_FAMILIA")
                    .Where(p => !tsd2015 || (p.T_FAMILIA != null && p.T_FAMILIA.PLATAFORMA == "TS2"))
                    .Select(s => new ProductsViewModel
                    {
                        NUMPRODUCTO = s.NUMPRODUCTO,
                        VERSION = s.VERSION,
                        FECHAVERSION = s.FECHAVERSION,
                        ACTIVO = s.ACTIVO,
                        NUMFAMILIA = s.T_FAMILIA != null ? s.T_FAMILIA.NUMFAMILIA : 0,
                        FAMILIA = s.T_FAMILIA != null ? s.T_FAMILIA.FAMILIA : null,
                        DESCRIPCION = s.ARTICULO.DESCRIPCION
                    }).ToList();

                bsList.DataSource = list;
            }

            cmdPlay.Enabled = !app.IsRunning;

            if (bsList.Count > 1)
                grid.MasterTemplate.CollapseAllGroups();
        }

        private ProductsViewModel Current
        {
            get
            {
                return bsList.Current as ProductsViewModel;
            }
        }

        private void cmdPlay_Click(object sender, EventArgs e)
        {
            ProductsViewModel item = Current;
            if (item == null)
                return;

            try
            {
                if (app.Model.ModoPlayer == ModoPlayer.Laser)
                    app.LoadLaser(item.NUMPRODUCTO, item.VERSION);
                else if (app.Model.ModoPlayer == ModoPlayer.PostVenta)
                    app.LoadPostVenta(item.NUMPRODUCTO, item.VERSION);
                else
                    app.LoadTest(item.NUMPRODUCTO, item.VERSION);

                mainForm.DoTest();
            }
            catch (Exception ex)
            {
                shell.MsgBox(ex.Message, "Excepción al cargar los datos del  producto", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        private void cmdParameters_Click(object sender, EventArgs e)
        {
            ProductsViewModel item = Current;
            if (item == null)
                return;

            using (ParamsView view = DI.Resolve<ParamsView>())
            {
                view.SetProducto(item.NUMPRODUCTO, item.VERSION, "120");
                shell.ShowDialog(string.Format("Parametrización {0}", item.DESCRIPCION), view, MessageBoxButtons.OK);
            }
        }

        private void cmdTSD2015_CheckStateChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}
