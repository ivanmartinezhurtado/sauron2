﻿using Comunications.Message;
using log4net;
using System.IO.Ports;

namespace Dezac.Device
{
    public class CirbusDeviceSerialPort : CirbusDevice
    {
        private int portCom;

        private SerialPort sp;

        public CirbusDeviceSerialPort(int portCom, int bps = 9600, Parity parity = Parity.None, StopBits stopBits = StopBits.One, ILog _logger = null)
            : base()
        {
            this.portCom = portCom;

            sp = new SerialPort("COM" + portCom, bps, parity, 8, stopBits);
            Transport = new CirbusTransport(new SerialPortAdapter(sp), _logger);

        }

        public CirbusDeviceSerialPort(SerialPort SerialPort, ILog _logger = null)
            : base()
        {
            sp = SerialPort;
            Transport = new CirbusTransport(new SerialPortAdapter(sp), _logger);
        }

        public int PortCom
        {
            get { return portCom; }
            set
            {
                if (portCom == value)
                    return;

                if (sp.IsOpen)
                    sp.Close();

                portCom = value;
                sp.PortName = "COM" + portCom;
            }
        }

        public int BaudRate
        {
            get
            {
                return sp.BaudRate;
            }
            set
            {
                sp.BaudRate = value;
            }
        }

        public int DataBits
        {
            get
            {
                return sp.DataBits;
            }
            set
            {
                sp.DataBits = value;
            }
        }

        public Parity Parity
        {
            get
            {
                return sp.Parity;
            }
            set
            {
                sp.Parity = value;
            }
        }

        public StopBits StopBits
        {
            get
            {
                return sp.StopBits;
            }
            set
            {
                sp.StopBits = value;
            }
        }

        public bool RtsEnable
        {
            get { return sp.RtsEnable; }
            set { sp.RtsEnable = value; }
        }

        public bool DtrEnable
        {
            get { return sp.DtrEnable; }
            set { sp.DtrEnable = value; }
        }

        public bool IsOpen
        {
            get
            {
                return sp.IsOpen;
            }
        }

        public void OpenPort()
        {
            if (!sp.IsOpen)
                sp.Open();
        }

        public override void ClosePort()
        {
            if (sp.IsOpen)
                sp.Close();
        }
    }
}
