﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;

namespace Dezac.Tests.Protection.PLACAS
{
    [TestVersion(1.12)]
    public class REC_MAX_CPU_TestPlacas : TestBase
    {
        #region MyRegion
        private const byte IN_REC_MAX_LED_GREEN = 9;
        private const byte IN_REC_MAX_LED_RED = 10;
        private const byte IN_DEVICE_PRESENCE = 11;
        private const byte IN_TRIP_RELAY = 22;
        private const byte IN_LOCK_RELAY = 23;

        private const byte OUT_ACTIVATE_SUPPLY_BLOCKING = 17;
        private const byte OUT_ACTIVATE_COIL_END = 18;
        private const byte OUT_OPEN_SECUNDARY_TORO = 19;
        private const byte OUT_RED_LIGHT_ACTIVATION = 20;
        private const byte OUT_DEACTIVATE_ENGINE = 21;
        private const byte OUT_ACTIVATE_ENGINE = 22;
        private const byte OUT_TEST_KEY = 43;

        private const InputMuxEnum MEASURE_V12 = InputMuxEnum.IN13;
        private const InputMuxEnum MEASURE_V3 = InputMuxEnum.IN3;
        private const InputMuxEnum MEASURE_VDISPLAY= InputMuxEnum.IN4;
        private const InputMuxEnum MEASURE_V_TRIGGER_COIL= InputMuxEnum.IN5;
        private const InputMuxEnum MEASURE_V_MOTA_MOTB = InputMuxEnum.IN6;
        private const InputMuxEnum MEASURE_V_ENGINE= InputMuxEnum.IN9;
        private const InputMuxEnum MEASURE_VAN= InputMuxEnum.IN11;
        private const InputMuxEnum MEASURE_VREF = InputMuxEnum.IN12;
        private const InputMuxEnum MEASURE_V_POWER_SUPPLY = InputMuxEnum.IN1;
        private const InputMuxEnum MEASURE_V_CONSUMPTION_SECONDARY = InputMuxEnum.NOTHING;

        private const String TEST_IMAGES_PATH = "RECMAX_CPU";
        #endregion

        private RECMAX recmax;
        private TowerBoard tower;
        private double measureTP_V3 { get; set; }

        public void TestInitialization()
        {
            recmax = new RECMAX(Comunicaciones.SerialPort);
            recmax.Modbus.PerifericNumber = Comunicaciones.Periferico;
           
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }
            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.IO.DO.Off(17);
            tower.Chroma.ApplyOff();

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[IN_DEVICE_PRESENCE])
                    return false;

                return true;

            }, "Error No se ha detectado el equipo o util abierto", 5, 500, 1500, false, false);
        }

        public void TestPowerConsumptions()
        {
            tower.IO.DO.On(15);

            var powerSupply = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name,230, ParamUnidad.V);
            // Se comprueban los consumos antes de encender el equipo
            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 2,
                ResultName = "",
                TiempoIntervalo = 100,
                typePowerSource = TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = powerSupply,
                WaitTimeReadMeasure = 500
            };
            this.TestConsumo(tower, configuration);

            tower.IO.DO.Off(15);

            Delay(1000, "Espera quitar alimentacion");

            tower.IO.DO.On(17);

            tower.MUX.Reset();
            tower.MUX.Connect2(InputMuxEnum.IN2, Outputs.FreCH1, InputMuxEnum.IN13, Outputs.FreCH2);

            tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

            tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.0);


            var adjustValue = new AdjustValueDef(Params.TIME.Null.TestPoint("START").Name, 0, Params.TIME.Null.TestPoint("START").Min(), Params.TIME.Null.TestPoint("START").Max(), 0, 0, ParamUnidad.s);
            var adjustResult = TestMeasureBase(adjustValue, (Func<int, double>)((s) =>
            {
                var timeResult = tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 3000, HP53131A.Channels.CH1, (Action)(() =>
                {
                    Delay(500, "Espera arranque");
                    tower.IO.DO.On(15);
                }));
                return (timeResult.Value);
            }), 0, 1, 1000);


            // Se comprueban los consumos una vez se ha asegurado el encendido del equipo
            configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 2,
                ResultName = "",
                TiempoIntervalo = 100,
                typePowerSource = TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.ActiveAndReactive,
                typeTestPoint = TypeTestPoint.ConCarga,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = powerSupply,
                WaitTimeReadMeasure = 500
            };
            this.TestConsumo(tower, configuration);

            tower.MUX.Disconnect2((int)InputMuxEnum.IN2, Outputs.FreCH1, (int)InputMuxEnum.IN13, Outputs.FreCH2);
        }

        public void TestCommunications()
        {
            recmax.WriteOperatingMode(RECMAX.OperatingMode.TEST);

            var firmwareVersion = recmax.ReadFirmwareVersion();

            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE,firmwareVersion.ToString(),ParamUnidad.SinUnidad, Identificacion.VERSION_FIRMWARE);

            Assert.AreEqual(firmwareVersion.ToString(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, la versión de firmware incorrecta"));

            recmax.WriteErrorCode(0x035A);
        }

        public void TestLeds()
        {
            recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);

            Dictionary<RECMAX.Outputs, bool[]> ledsDevice = new Dictionary<RECMAX.Outputs, bool[]>
            { 
                {RECMAX.Outputs.GREEN_LED,new bool[2]{true, false}},
                {RECMAX.Outputs.RED_LED,new bool[2]{false, true}},
                {RECMAX.Outputs.ALL_OFF,new bool[2]{false, false}},
            };

            foreach (var led in ledsDevice)
                SamplerWithCancel((p) =>
                {
                    recmax.WriteOutputs(led.Key);

                    Delay(150, "Espera encendido leds");

                    if (tower.IO.DI.Read(IN_REC_MAX_LED_GREEN).Value != led.Value[0])
                        throw new Exception(string.Format("Error en el estado esperado del led verde"));

                    if (tower.IO.DI.Read(IN_REC_MAX_LED_RED).Value != led.Value[1])
                        throw new Exception(string.Format("Error en el estado esperado del led rojo"));

                    recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);

                    return true;
                }, string.Empty, 3, 500, 200, false, false);

            Resultado.Set("LEDS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestRelays()
        {
             recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);

            Dictionary<RECMAX.Outputs, byte> deviceRelays = new Dictionary<RECMAX.Outputs, byte>
            { 
                {RECMAX.Outputs.AUX_RELAY, IN_LOCK_RELAY},
                {RECMAX.Outputs.TRIP_RELAY, IN_TRIP_RELAY},
            };

            foreach (var relay in deviceRelays)
                SamplerWithCancel((p) =>
                {
                    recmax.WriteOutputs(relay.Key);

                    var value = relay.Key == RECMAX.Outputs.AUX_RELAY ? false : true;

                    Delay(300, "Espera encendido rele");

                    if(tower.IO.DI[relay.Value]==value)
                        throw new Exception(string.Format("Error estado incorrecto del rele {0}", relay.Key.ToString()));

                    recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);

                    Delay(300, "Espera apagado rele");

                    if(!tower.IO.DI[relay.Value]==value)
                        throw new Exception(string.Format("Error estado incorrecto del rele {0}", relay.Key.ToString()));

                    return true;

                }, string.Empty, 3, 500, 200, false, false);

            Resultado.Set("RELES", "OK", ParamUnidad.SinUnidad);
        }

        public void TestInputs()
        {
            Dictionary<RECMAX.Inputs, byte> inputDevice = new Dictionary<RECMAX.Inputs, byte>
            { 
                {RECMAX.Inputs.ALL_OFF, 0},
                {RECMAX.Inputs.MAGNETOTHERMIC,OUT_ACTIVATE_COIL_END },
                {RECMAX.Inputs.EXTERNAL_REARM, OUT_ACTIVATE_ENGINE },
                {RECMAX.Inputs.EXTERNAL_TRIGGER, OUT_DEACTIVATE_ENGINE},
                {RECMAX.Inputs.TEST_RESET_KEY, OUT_TEST_KEY},
            };

            foreach (var input in inputDevice)
                SamplerWithCancel((p) =>
                {
                    tower.IO.DO.OnWait(250, input.Value);

                    var status = recmax.ReadInputStatus();

                    if (status != input.Key)
                        throw new Exception(string.Format("Error no se detecta la entrada {0} activada", input.Key.ToString()));

                    tower.IO.DO.Off(input.Value);

                    return true;

                }, "", 3, 500, 200, false, false);

            Resultado.Set("INPUTS", "OK", ParamUnidad.SinUnidad);
        }

        public void TestVoltageTestPoint()
        {
            //****************************************************************
            //************** TP_V3 ******************************************

            var measureV3PatternInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_V3_MULTIMETRO").Name, 0, 0, 0, Params.V.Null.TestPoint("TP_V3_MULTIMETRO").Avg(), Params.V.Null.TestPoint("TP_V3_MULTIMETRO").Tol(), ParamUnidad.V);

            TestMeasureBase(measureV3PatternInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_V3, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC,measureV3PatternInfo.Max));
                return measurement;
            }, 0, 3, 500);

            var measureV3Info = new AdjustValueDef(Params.V.Null.TestPoint("TP_V3").Name, 0, 0, 0, Params.V.Null.TestPoint("TP_V3").Avg(), Params.V.Null.TestPoint("TP_V3").Tol(), ParamUnidad.V);
            measureV3Info.calculateMaxMin();

            TestCalibracionBase(measureV3Info,
            () =>
            {
                measureTP_V3 = recmax.ReadVoltageV3TestPointValue();
                return measureTP_V3;
            },
            () =>
            {
                return tower.MeasureMultimeter(MEASURE_V3, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureV3Info.Max));
            }, 2, 4, 500, "Error, la tensión medida por el equipo en el TP_V3 fuera de márgenes");

            //****************************************************************
            //************** TP_VAN ******************************************
            var measureVANInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_VAN").Name, 0, Params.V.Null.TestPoint("TP_VAN").Min(), Params.V.Null.TestPoint("TP_VAN").Max(), 0, 0, ParamUnidad.V);

            TestMeasureBase(measureVANInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_VAN, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVANInfo.Max));
                return measurement;
            }, 0, 3, 500);
            
            //****************************************************************
            //************** TP_VREF ******************************************
            var measureVRefInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_VREF").Name, 0, Params.V.Null.TestPoint("TP_VREF").Min(), Params.V.Null.TestPoint("TP_VREF").Max(), 0, 0, ParamUnidad.V);

            TestMeasureBase(measureVRefInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_VREF, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVRefInfo.Max));
                return measurement;
            }, 0, 3, 500);


            //****************************************************************
            //************** TP_V12 ******************************************
            var measureV12PatternInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_V12_MULTIMETRO").Name, 0, 0, 0, Params.V.Null.TestPoint("TP_V12_MULTIMETRO").Avg(), Params.V.Null.TestPoint("TP_V12_MULTIMETRO").Tol(), ParamUnidad.V);

            TestMeasureBase(measureV12PatternInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_V12, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureV12PatternInfo.Max));
                return measurement;
            }, 0, 3, 500);

            var measureV12Info = new AdjustValueDef(Params.V.Null.TestPoint("TP_V12").Name, 0, 0,0,Params.V.Null.TestPoint("TP_V12").Avg(), Params.V.Null.TestPoint("TP_V12").Tol(), ParamUnidad.V);
            measureV12Info.calculateMaxMin();

            TestCalibracionBase(measureV12Info,
            () =>
            {
                var pointsV12= recmax.ReadVoltageV12TestPointValue();
                var valueV12 = (pointsV12 * (measureTP_V3 / Math.Pow(2, 12))) / 0.175;
                return valueV12;
            },
            () =>
            {
                return tower.MeasureMultimeter(MEASURE_V12, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureV12Info.Max));
            }, 2, 4, 500, "Error, la tensión medida por el equipo en el TP_V12 fuera de márgenes");
        }

        public void TestVoltageDisplay()
        {
            recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);
            //****************************************************************
            //************** DISPLAY_INACTIVE ******************************************

            Delay(500, "Espera desactivación display");

            var measureVDisplayInactiveInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_DISPLAY_OFF").Name, 0, Params.V.Null.TestPoint("TP_DISPLAY_OFF").Min(), Params.V.Null.TestPoint("TP_DISPLAY_OFF").Max(), 0, 0, ParamUnidad.V);
            TestMeasureBase(measureVDisplayInactiveInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_VDISPLAY, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVDisplayInactiveInfo.Max));
                return measurement;
            }, 0, 5, 500);

            //****************************************************************
            //************** DISPLAY_ACTIVE ******************************************
            recmax.WriteOutputs(RECMAX.Outputs.DISPLAY_ACTIVATION);

            Delay(500, "Espera desactivación display");

            var measureVDisplayActiveInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_DISPLAY_ON").Name, 0, Params.V.Null.TestPoint("TP_DISPLAY_ON").Min(), Params.V.Null.TestPoint("TP_DISPLAY_ON").Max(), 0, 0, ParamUnidad.V);

            TestMeasureBase(measureVDisplayActiveInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_VDISPLAY, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVDisplayActiveInfo.Max));
                return measurement;
            }, 0, 5, 500);

            recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);
        }

        public void TestVoltageTriggerCoil()
        {
            //****************************************************************
            //************** MEASURE_V_TRIGGER_COIL ***************************
            tower.MUX.Connect2((ushort)MEASURE_V_TRIGGER_COIL, Outputs.FreCH2, (ushort)MEASURE_V_TRIGGER_COIL, Outputs.FreCH1);

            tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);

            tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
            tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.6);

            var timeTrigger = new AdjustValueDef(Params.TIME.Null.TestPoint("TRIGGER_COIL").Name, 0, Params.TIME.Null.TestPoint("TRIGGER_COIL").Min(), Params.TIME.Null.TestPoint("TRIGGER_COIL").Max(), 0, 0, ParamUnidad.ms);

            TestMeasureBase(timeTrigger, (p) =>
            {
                var timeIntervalResult = tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 5000, HP53131A.Channels.CH1, () =>
                {
                    recmax.WriteOutputs(RECMAX.Outputs.TRIGGER_INDUCTOR);
                });
                return timeIntervalResult.Value;
            }, 0, 2, 500);

            tower.MUX.Disconnect2((int)MEASURE_V_TRIGGER_COIL, Outputs.FreCH2, (int)MEASURE_V_TRIGGER_COIL, Outputs.FreCH1);
        }

        public void TestVoltageEngine()
        {
            //****************************************************************
            //************** TP_V_ENGINE_UP ***************************
            recmax.WriteOutputs(RECMAX.Outputs.ENGINE_UP);

            Delay(500, "Espera activación rele");

            var measureVEngineUpPatternInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_ENGINE_UP_MULTIMETRO").Name, 0, 0, 0, Params.V.Null.TestPoint("TP_ENGINE_UP_MULTIMETRO").Avg(), Params.V.Null.TestPoint("TP_ENGINE_UP_MULTIMETRO").Tol(), ParamUnidad.V);
            measureVEngineUpPatternInfo.calculateMaxMin();

            TestMeasureBase(measureVEngineUpPatternInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_V_MOTA_MOTB, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVEngineUpPatternInfo.Max));
                return measurement;
            }, 0, 3, 500);

            var measureVEngineUpInfo = new AdjustValueDef(Params.I.Null.TestPoint("TP_ENGINE_UP").Name, 0, 0,0,Params.I.Null.TestPoint("TP_ENGINE_UP").Avg(), Params.I.Null.TestPoint("TP_ENGINE_UP").Tol(), ParamUnidad.mA);
            measureVEngineUpInfo.calculateMaxMin();

            TestCalibracionBase(measureVEngineUpInfo,
            () =>
            {
                var valueEngine= recmax.ReadCurrentEngineTestPointValue();
                var vEngineUpError = (valueEngine * (measureTP_V3 / Math.Pow(2, 12))) / 3.3;
                return vEngineUpError;
            },
            () =>
            {
                var measure = tower.MeasureMultimeter(MEASURE_V_MOTA_MOTB, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVEngineUpInfo.Max *180));
                return measure / 180;
            }, 2, 4, 500, "Error, tensión positiva del motor fuera de márgenes");

            //****************************************************************
            //************** TP_V_ENGINE_DOWN ***************************
            recmax.WriteOutputs(RECMAX.Outputs.ENGINE_DOWN);

            Delay(500, "Espera activación rele");

            var measureVEngineDownPatternInfo = new AdjustValueDef(Params.V.Null.TestPoint("TP_ENGINE_DOWN_MULTIMETRO").Name, 0, 0, 0, Params.V.Null.TestPoint("TP_ENGINE_DOWN_MULTIMETRO").Avg(), Params.V.Null.TestPoint("TP_ENGINE_DOWN_MULTIMETRO").Tol(), ParamUnidad.V);
            measureVEngineDownPatternInfo.calculateMaxMin();

            TestMeasureBase(measureVEngineDownPatternInfo, (step) =>
            {
                var measurement = tower.MeasureMultimeter(MEASURE_V_MOTA_MOTB, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVEngineDownPatternInfo.Max));
                return measurement;
            }, 0, 3, 500);

            var measureVEngineDownInfo = new AdjustValueDef(Params.I.Null.TestPoint("TP_ENGINE_DOWN").Name, 0, 0, 0, Params.I.Null.TestPoint("TP_ENGINE_DOWN").Avg(), Params.I.Null.TestPoint("TP_ENGINE_DOWN").Tol(), ParamUnidad.mA);
            measureVEngineDownInfo.calculateMaxMin();

            TestCalibracionBase(measureVEngineDownInfo,
            () =>
            {
                var valueEngine = recmax.ReadCurrentEngineTestPointValue();
                var vEngineUpError = (valueEngine * (measureTP_V3 / Math.Pow(2, 12))) / 3.3;
                return -vEngineUpError;
            },
            () =>
            {
                var measure = tower.MeasureMultimeter(MEASURE_V_MOTA_MOTB, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, measureVEngineDownInfo.Max *180));
                return measure / 180;
            }, 2, 4, 500, "Error, tensión negativa del motor fuera de márgenes");

            recmax.WriteOutputs(RECMAX.Outputs.ALL_OFF);
        }

        public void TestPowerMeasurements()
        {
            //****************************************************************
            //************** V_NET ******************************************
            var powerNetReadingInfo = new AdjustValueDef(Params.POINTS.Null.TestPoint("V_NET").Name, 0, Params.POINTS.Null.TestPoint("V_NET").Min(), Params.POINTS.Null.TestPoint("V_NET").Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(powerNetReadingInfo, (step) =>
            {
                var result=  recmax.ReadPowerNetCuadraticPoints();
                return result;
            }, 0, 5, 500);

            //****************************************************************
            //************** I_LEAK_0A ******************************************
            var leakCurrentReadingInfo = new AdjustValueDef(Params.POINTS.Null.TestPoint("I_LEAK_0A").Name, 0, Params.POINTS.Null.TestPoint("I_LEAK_0A").Min(), Params.POINTS.Null.TestPoint("I_LEAK_0A").Max(), 0, 0, ParamUnidad.Puntos);

            tower.IO.DO.OnWait(2500, OUT_OPEN_SECUNDARY_TORO);

            TestMeasureBase(leakCurrentReadingInfo, (step) =>
            {
                var result= recmax.ReadScale1LeakCurrentCuadraticPoints();
                return result;
            }, 0, 5, 500);

            tower.IO.DO.OffWait(2500, OUT_OPEN_SECUNDARY_TORO);

            //****************************************************************
            //************** CALIBRACION I_LEAK 0,5A *************************
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(0.5D, 0, 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var leakCurrent0A5ReadingInfo = new AdjustValueDef(Params.POINTS.Null.TestPoint("I_05A_LEAK").Name, 0, Params.POINTS.Null.TestPoint("I_05A_LEAK").Min(), Params.POINTS.Null.TestPoint("I_05A_LEAK").Max(), 0, 0, ParamUnidad.V);

            TestMeasureBase(leakCurrent0A5ReadingInfo, (step) =>
            {
                var result= recmax.ReadScale1LeakCurrentCuadraticPoints();
                return result;
            }, 0, 5, 500);

            //****************************************************************
            //************** CALIBRACION I_LEAK 0,5A **************************
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(5D, 0, 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

            var leakCurrent5AReadingInfo = new AdjustValueDef(Params.POINTS.Null.TestPoint("I_5A_LEAK").Name, 0, Params.POINTS.Null.TestPoint("I_5A_LEAK").Min(), Params.POINTS.Null.TestPoint("I_5A_LEAK").Max(), 0, 0, ParamUnidad.V);

            TestMeasureBase(leakCurrent5AReadingInfo, (step) =>
            {
                var result= recmax.ReadScale2LeakCurrentCuadraticPoints();
                return result;
            }, 0, 5, 500);

            tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            var calibrationContants = new RECMAX.FactorsCalibration();
            calibrationContants.pointsScale0 = (int)leakCurrent0A5ReadingInfo.Value;
            calibrationContants.pointsScale1 = (int)leakCurrent5AReadingInfo.Value;

            recmax.WriteFactorsCalibrationConstant(calibrationContants);

        }

        public void TestToroidal()
        {
            var testsToroiddal = new List<RECMAX.Outputs>()
            {
                {RECMAX.Outputs.ALL_OFF},
                {RECMAX.Outputs.TOROIDAL_TEST},
                {RECMAX.Outputs.ALL_OFF}
            };

            foreach (RECMAX.Outputs testToroiddal in testsToroiddal)
            {
                tower.IO.DO.OffWait(2500, OUT_OPEN_SECUNDARY_TORO); 

                if (testToroiddal == RECMAX.Outputs.TOROIDAL_TEST)
                    tower.IO.DO.OnWait(2500, OUT_OPEN_SECUNDARY_TORO);

                recmax.WriteOutputs(RECMAX.Outputs.TOROIDAL_TEST);

                SamplerWithCancel((p) =>
                {
                    var inputReading = recmax.ReadInputStatus();

                    var result = testToroiddal == RECMAX.Outputs.TOROIDAL_TEST ? RECMAX.Inputs.TOROIDAL_NOT_CONNECTED : RECMAX.Inputs.ALL_OFF;

                    return inputReading == result;

                }, "Error en la detección test Toroidal", 20, 1500, 1000);

            }
             
            Resultado.Set("TEST_TOROIDAL", "OK", ParamUnidad.SinUnidad);
        }

        public void TestDisplayComunications()
        {
            recmax.WriteOperatingMode(RECMAX.OperatingMode.TEST);

            var port = Comunicaciones.SerialPort2;
            using (var serialPort1 = new SerialPort("COM" + port, 9600, Parity.None, 8, StopBits.One))
            {
                serialPort1.DtrEnable = true;

                serialPort1.Open();

                string displayPortReading = string.Empty;
                Stopwatch displayPortStopwatch = new Stopwatch();
                displayPortStopwatch.Start();
                while (displayPortStopwatch.Elapsed.TotalSeconds < Configuracion.GetDouble("LIMITE_TIEMPO_PRUEBA_DISPLAY", 5, ParamUnidad.SinUnidad))
                {
                    displayPortReading += serialPort1.ReadByte().ToString("X2");
                    if (displayPortReading.Length / 2 >= Configuracion.GetDouble("TRAMAS_DISPLAY_MINIMAS", 60, ParamUnidad.SinUnidad))
                        break;
                }

                displayPortStopwatch.Stop();

                Resultado.Set("TRAMAS_DISPLAY", displayPortReading.Length / 2, ParamUnidad.SinUnidad, min: 60);

                if (displayPortStopwatch.Elapsed.TotalSeconds > Configuracion.GetDouble("LIMITE_TIEMPO_PRUEBA_DISPLAY", 5, ParamUnidad.SinUnidad))
                    throw new Exception("Error de comunicaciones, no se reciben tramas del display o no se reciben la cantidad minima de  tramas en el tiempo especificado");

                var communicationsOK = displayPortReading.ToUpper().Contains(Configuracion.GetString("TRAMA_PRUEBA_DISPLAY", "025044492000B000000000000000E920", ParamUnidad.SinUnidad).ToUpper());

                Assert.IsTrue(communicationsOK, Error().UUT.COMUNICACIONES.DISPLAY("Error de comunicaciones, la trama enviada por el display no es coherente"));

                recmax.ClearAcknowledge();

                Delay(2000, "Espera reinicio registro de tramas recibidas");

                serialPort1.DtrEnable = false;

                var tramaTx = new byte[4] { 0x01, 0x3F, 0x40, 0x30 };

                SamplerWithCancel((p) =>
                {
                    serialPort1.Write(tramaTx, 0, 4);
                    
                    var acknowledgedBytes = recmax.ReadAcknowledges();
                    return acknowledgedBytes >= 3;
                }, "Error de comubnicaciones, no llegan las tramas enviadas al display", 10, 300);
            }

            Resultado.Set("COMUNICACIONES_DISPLAY", "OK", ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (recmax != null)
                recmax.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(17);
                tower.ShutdownSources();
                tower.Dispose();
            }
        }
    }
}
