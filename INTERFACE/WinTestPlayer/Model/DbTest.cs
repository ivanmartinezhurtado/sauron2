﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskRunner;
using TaskRunner.Model;

namespace WinTestPlayer.Model
{
    public class DbTest
    {
        public string Id { get; set; }
        public string IdEmpleado { get; set; }
        public string IdOperario { get; set; }
        public string Operario { get; set; }
        public int IdOrden { get; set; }
        public int? IdFase { get; set; }
        public int IdProducto { get; set; }
        public int Version { get; set; }

        public DateTime StartTime { get; internal set; }
        public DateTime EndTime { get; internal set; }
        public TimeSpan Duration { get { return EndTime - StartTime; } }
        public ResultsList ResultList { get; internal set; }
        public TestExecutionResult ExecutionResult { get; internal set; }
        public List<StepResult> StepResults { get; internal set; }
    }
}
