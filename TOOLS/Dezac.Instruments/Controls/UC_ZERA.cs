﻿using Dezac.Core.Utility;
using Dezac.Instruments.Utils;
using Instruments.PowerSource;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(ZERADescription))]
    public partial class UC_ZERA : UserControl, IDisposable
    {
        private static readonly string TAG = "ZERA";

        private TriLineValue Voltage;
        private TriLineValue Current;
        private TriLineValue Desfase;
        private TriLineValue PF;
        private double freq;

        public int numGraphs = 0;

        private SynchronizationContext sc;
        private ZERA zera;

        public Type InstrumentType
        {
            get
            {
                return typeof(ZERA);
            }
        }

        public ZERA Instrument
        {
            get
            {
                if (zera == null)
                    zera = new ZERA();

                return zera;
            }
            set
            {
                if (zera != null)
                {
                    zera.Dispose();
                    zera = null;
                }

                zera = value;
            }
        }

        public UC_ZERA()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            sc = SynchronizationContext.Current;

            try
            {
                var ports = new MyComputer().SerialPortNames.ToArray();
                if (ports.Length == 0)
                {
                    errorProvider1.SetError(cmb_Port, "No se ha encontrado puertos en el PC");
                    return;
                }

                cmb_Port.Items.AddRange(ports);
                cmb_Port.SelectedIndex = cmb_Port.FindStringExact("COM1");
                if (cmb_Port.SelectedItem == null)
                    cmb_Port.SelectedIndex = 0;
            }
            catch (Exception)
            {
                errorProvider1.SetError(cmb_Port, "No se ha encontrado la MTE");
            }

        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();

                if (cmb_Port.SelectedItem == null)
                    errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
                else
                {
                    if (ValidatePresets())
                    {
                        ToggleButtonsState((Button)sender);

                        try
                        {
                            var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyPresetsAndWaitStabilisation(Voltage, Current, freq, PF, Desfase); }));

                            LifeMethod("Encendiendo", task);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("Error al encender el MTE PPS400.3", ex);
                        }
                        finally
                        {
                            ToggleButtonsState((Button)sender);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se ha encontrado la Fuente ZERA", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.Error("Error al inicializar la ZERA", ex);
            }
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.Clear();
                if (cmb_Port.SelectedItem == null)
                    errorProvider1.SetError(cmb_Port, "Needs to select a Serial Port");
                else
                {
                    Instrument.PortCom = Convert.ToByte(cmb_Port.SelectedItem.ToString().Replace("COM", ""));
                    ToggleButtonsState((Button)sender);
                    var task = Task.Factory.StartNew((Action)(() => { this.Instrument.ApplyOffAndWaitStabilisation(); }));
                    LifeMethod("Apagando", task);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error al apagar el ZERA ", ex);
            }
            finally
            {
                ToggleButtonsState((Button)sender);
            }
        }

        private bool ValidatePresets()
        {
            try
            {
                if (!ValidateVoltageFreq()) return false;
                if (!ValidateCurrent()) return false;
                if (!ValidateDesfase()) return false;
            }
            catch (Exception ex)
            {
                Logger.Error("Error comunicando con la MTE PPS400.3", ex);
            }
            return true;
        }

        private bool ValidateVoltageFreq()
        {
            Voltage = new TriLineValue { L1 = Convert.ToDouble(txtV1.Text), L2 = Convert.ToDouble(txtV2.Text), L3 = Convert.ToDouble(txtV3.Text) };

            if (Voltage.L1 > 400)
            {
                errorProvider1.SetError(txtV1, "Voltage must lower 400V");
                return false;
            }

            if (Voltage.L2 > 400)
            {
                errorProvider1.SetError(txtV2, "Voltage must lower 400V");
                return false;
            }

            if (Voltage.L3 > 400)
            {
                errorProvider1.SetError(txtV3, "Voltage must lower 400V");
                return false;
            }

            freq = Convert.ToDouble(txtFreq.Text);
            if (freq > 100)
            {
                errorProvider1.SetError(txtFreq, "Frequency must be lower 100Hz");
                return false;
            }

            return true;
        }

        private bool ValidateCurrent()
        {
            Current = new TriLineValue { L1 = Convert.ToDouble(txtI1.Text), L2 = Convert.ToDouble(txtI2.Text), L3 = Convert.ToDouble(txtI3.Text) };
            if (Current.L1 > 120)
            {
                errorProvider1.SetError(txtI1, "Current must lower 120A with Output Current select 120A");
                return false;
            }
            if (Current.L2 > 120)
            {
                errorProvider1.SetError(txtI2, "Current must lower 120A with Output Current select 120A");
                return false;
            }
            if (Current.L3 > 120)
            {
                errorProvider1.SetError(txtI3, "Current must lower 120A with Output Current select 120A");
                return false;
            }
            return true;
        }

        private bool ValidateDesfase()
        {
            Desfase = new TriLineValue { L1 = Convert.ToDouble(txtA1.Text), L2 = Convert.ToDouble(txtA2.Text), L3 = Convert.ToDouble(txtA3.Text) };
            if (Desfase.L1 > 360)
            {
                errorProvider1.SetError(txtA1, "Anlge must lower 360º");
                return false;
            }
            if (Desfase.L2 > 360)
            {
                errorProvider1.SetError(txtA2, "Anlge must lower360º");
                return false;
            }
            if (Desfase.L3 > 360)
            {
                errorProvider1.SetError(txtA3, "Anlge must lower 360º");
                return false;
            }
            PF = new TriLineValue { L1 = Convert.ToDouble(txtPF.Text), L2 = Convert.ToDouble(txtPF.Text), L3 = Convert.ToDouble(txtPF.Text) };
            if (PF.L1 > 360)
            {
                errorProvider1.SetError(txtPF, "Power Factor must lower 360º");
                return false;
            }
            return true;
        }

        private void ToggleButtonsState(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private void StartTask(string status)
        {
            pbStatus.Value = 0;
            pbStatus.Visible = true;
            pbStatus.Maximum = 100;
        }

        private void EndTask()
        {
            pbStatus.Value = 0;
            pbStatus.Visible = false;
        }

        private void LifeMethod(string status, Task task)
        {
            StartTask(status);

            do
            {
                pbStatus.Value += 10;
                if (pbStatus.Value == 100) pbStatus.Value = 0;
                Application.DoEvents();
                Thread.Sleep(10);
            } while (!task.IsCompleted);

            EndTask();
        }

        protected override void Dispose(bool disposing)
        {
            numGraphs = 0;

            if (disposing && (components != null))      
                components.Dispose();       

            if (Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }

    public class ZERADescription : ControlDescription
    {
        public override string Name { get { return "ZERA"; } }
        public override string Category { get { return "PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.PowerSource; } }
        public override Image IconBig { get { return Properties.Resources.ZERA_MT; } }
        public override Type SubcomponentType { get { return typeof(ZERA); } }
    }
}
