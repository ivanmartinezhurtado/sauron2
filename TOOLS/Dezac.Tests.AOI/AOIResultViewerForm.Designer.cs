﻿using HalconDotNet;
using Dezac.Tests.AOI.Models;

namespace Dezac.Tests.AOI
{
    partial class AOIResultViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnFalsoFallo = new Telerik.WinControls.UI.RadButton();
            this.btnNoReparado = new Telerik.WinControls.UI.RadButton();
            this.hWindow1 = new HalconDotNet.HSmartWindowControl();
            this.hWindow2 = new HalconDotNet.HSmartWindowControl();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.bsList = new System.Windows.Forms.BindingSource(this.components);
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.btnPrevious = new Telerik.WinControls.UI.RadButton();
            this.btnNext = new Telerik.WinControls.UI.RadButton();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnComponenteLevantaado = new Telerik.WinControls.UI.RadButton();
            this.btnFinishRepair = new Telerik.WinControls.UI.RadButton();
            this.btnOtroFallo = new Telerik.WinControls.UI.RadButton();
            this.btnCruce = new Telerik.WinControls.UI.RadButton();
            this.btnFaltaEstano = new Telerik.WinControls.UI.RadButton();
            this.btnBolaEstano = new Telerik.WinControls.UI.RadButton();
            this.btnCable = new Telerik.WinControls.UI.RadButton();
            this.btnSinComponente = new Telerik.WinControls.UI.RadButton();
            this.btnDescartarCircuito = new Telerik.WinControls.UI.RadButton();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.lblFalsosFallos = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDPMO = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.lblCruces = new Telerik.WinControls.UI.RadLabel();
            this.lblSoldadurasFriasBolas = new Telerik.WinControls.UI.RadLabel();
            this.lblPatasLargas = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.lblFaltasComponentes = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.lblFaltasEstaño = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.labelNumOrden = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.lblOtrosFallos = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lblLevantado = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.btnFalsoFallo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNoReparado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevious)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnComponenteLevantaado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFinishRepair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOtroFallo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCruce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFaltaEstano)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBolaEstano)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSinComponente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDescartarCircuito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFalsosFallos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDPMO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCruces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoldadurasFriasBolas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPatasLargas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFaltasComponentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFaltasEstaño)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelNumOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtrosFallos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLevantado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnFalsoFallo
            // 
            this.btnFalsoFallo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFalsoFallo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFalsoFallo.Location = new System.Drawing.Point(23, 23);
            this.btnFalsoFallo.Name = "btnFalsoFallo";
            this.btnFalsoFallo.Size = new System.Drawing.Size(201, 50);
            this.btnFalsoFallo.TabIndex = 6;
            this.btnFalsoFallo.Text = "Falso Fallo";
            this.btnFalsoFallo.Click += new System.EventHandler(this.btnFalsoFallo_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnFalsoFallo.GetChildAt(0))).Text = "Falso Fallo";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(248)))), ((int)(((byte)(108)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(245)))), ((int)(((byte)(18)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(114)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5843621F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9218107F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(238)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFalsoFallo.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNoReparado
            // 
            this.btnNoReparado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNoReparado.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoReparado.Location = new System.Drawing.Point(23, 175);
            this.btnNoReparado.Name = "btnNoReparado";
            this.btnNoReparado.Size = new System.Drawing.Size(201, 50);
            this.btnNoReparado.TabIndex = 6;
            this.btnNoReparado.Text = "Eliminar \r\nReparación";
            this.btnNoReparado.Click += new System.EventHandler(this.btnNoReparado_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnNoReparado.GetChildAt(0))).Text = "Eliminar \r\nReparación";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(122)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(149)))), ((int)(((byte)(21)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(74)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.6584362F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9691358F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(204)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnNoReparado.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hWindow1
            // 
            this.hWindow1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.hWindow1.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.hWindow1.HDoubleClickToFitContent = true;
            this.hWindow1.HDrawingObjectsModifier = HalconDotNet.HSmartWindowControl.DrawingObjectsModifier.None;
            this.hWindow1.HImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindow1.HKeepAspectRatio = true;
            this.hWindow1.HMoveContent = true;
            this.hWindow1.HZoomContent = HalconDotNet.HSmartWindowControl.ZoomContent.WheelForwardZoomsIn;
            this.hWindow1.Location = new System.Drawing.Point(14, 47);
            this.hWindow1.Margin = new System.Windows.Forms.Padding(0);
            this.hWindow1.Name = "hWindow1";
            this.hWindow1.Size = new System.Drawing.Size(900, 600);
            this.hWindow1.TabIndex = 8;
            this.hWindow1.WindowSize = new System.Drawing.Size(900, 600);
            this.hWindow1.HMouseUp += new HalconDotNet.HMouseEventHandler(this.hWindow1_HMouseUp);
            // 
            // hWindow2
            // 
            this.hWindow2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.hWindow2.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.hWindow2.HDoubleClickToFitContent = true;
            this.hWindow2.HDrawingObjectsModifier = HalconDotNet.HSmartWindowControl.DrawingObjectsModifier.None;
            this.hWindow2.HImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.hWindow2.HKeepAspectRatio = true;
            this.hWindow2.HMoveContent = true;
            this.hWindow2.HZoomContent = HalconDotNet.HSmartWindowControl.ZoomContent.WheelForwardZoomsIn;
            this.hWindow2.Location = new System.Drawing.Point(17, 47);
            this.hWindow2.Margin = new System.Windows.Forms.Padding(0);
            this.hWindow2.Name = "hWindow2";
            this.hWindow2.Size = new System.Drawing.Size(900, 600);
            this.hWindow2.TabIndex = 8;
            this.hWindow2.WindowSize = new System.Drawing.Size(900, 600);
            this.hWindow2.HMouseUp += new HalconDotNet.HMouseEventHandler(this.hWindow2_HMouseUp);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 979);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(1912, 26);
            this.radStatusStrip1.TabIndex = 10;
            this.radStatusStrip1.Text = "radStatusStrip1";
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(900, 247);
            // 
            // radGridView1
            // 
            this.radGridView1.BackColor = System.Drawing.Color.White;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterTemplate.AllowColumnResize = false;
            this.radGridView1.MasterTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterTemplate.AllowRowResize = false;
            this.radGridView1.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "Barcodes";
            gridViewTextBoxColumn4.HeaderText = "Barcodes";
            gridViewTextBoxColumn4.Name = "Barcodes";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn4.Width = 460;
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "TotalFailures";
            gridViewTextBoxColumn5.HeaderText = "TotalFailures";
            gridViewTextBoxColumn5.Name = "TotalFailures";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 74;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "RemainingFailures";
            gridViewTextBoxColumn6.HeaderText = "RemainingFailures";
            gridViewTextBoxColumn6.Name = "RemainingFailures";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 103;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.radGridView1.MasterTemplate.DataSource = this.bsList;
            this.radGridView1.MasterTemplate.EnableGrouping = false;
            this.radGridView1.MasterTemplate.ShowRowHeaderColumn = false;
            sortDescriptor2.PropertyName = "Barcodes";
            this.radGridView1.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.Size = new System.Drawing.Size(638, 249);
            this.radGridView1.TabIndex = 7;
            this.radGridView1.Text = "radGridView1";
            this.radGridView1.ThemeName = "ControlDefault";
            // 
            // bsList
            // 
            this.bsList.CurrentChanged += new System.EventHandler(this.bsList_CurrentChanged);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radPanel7);
            this.radPanel1.Controls.Add(this.radPanel5);
            this.radPanel1.Location = new System.Drawing.Point(30, 45);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1857, 686);
            this.radPanel1.TabIndex = 12;
            // 
            // radPanel7
            // 
            this.radPanel7.Controls.Add(this.radLabel2);
            this.radPanel7.Controls.Add(this.hWindow2);
            this.radPanel7.Location = new System.Drawing.Point(929, 0);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(928, 686);
            this.radPanel7.TabIndex = 17;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(53, 8);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(169, 33);
            this.radLabel2.TabIndex = 10;
            this.radLabel2.Text = "Vista Soldadura";
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel2.GetChildAt(0))).Text = "Vista Soldadura";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel2.GetChildAt(0).GetChildAt(2).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel2.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // radPanel5
            // 
            this.radPanel5.Controls.Add(this.radLabel1);
            this.radPanel5.Controls.Add(this.hWindow1);
            this.radPanel5.Controls.Add(this.btnPrevious);
            this.radPanel5.Controls.Add(this.btnNext);
            this.radPanel5.Location = new System.Drawing.Point(0, 0);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(930, 686);
            this.radPanel5.TabIndex = 15;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(40, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(121, 33);
            this.radLabel1.TabIndex = 9;
            this.radLabel1.Text = "Vista Panel";
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel1.GetChildAt(0))).Text = "Vista Panel";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel1.GetChildAt(0).GetChildAt(2).GetChildAt(1))).TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radLabel1.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnPrevious
            // 
            this.btnPrevious.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnPrevious.Image = global::Dezac.Tests.AOI.Properties.Resources.flecha_derecha;
            this.btnPrevious.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPrevious.Location = new System.Drawing.Point(278, 655);
            this.btnPrevious.Name = "btnPrevious";
            // 
            // 
            // 
            this.btnPrevious.RootElement.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPrevious.Size = new System.Drawing.Size(110, 24);
            this.btnPrevious.TabIndex = 5;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnPrevious.GetChildAt(0))).UseSmallImageList = true;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnPrevious.GetChildAt(0))).Image = global::Dezac.Tests.AOI.Properties.Resources.flecha_derecha;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnPrevious.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnPrevious.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnPrevious.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnPrevious.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.btnPrevious.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ScaleSize = new System.Drawing.Size(50, 28);
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.btnPrevious.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ImageScaling = Telerik.WinControls.Enumerations.ImageScaling.SizeToFit;
            // 
            // btnNext
            // 
            this.btnNext.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnNext.Image = global::Dezac.Tests.AOI.Properties.Resources.flecha_izquierda;
            this.btnNext.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnNext.Location = new System.Drawing.Point(528, 655);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(110, 24);
            this.btnNext.TabIndex = 5;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnNext.GetChildAt(0))).UseSmallImageList = true;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnNext.GetChildAt(0))).Image = global::Dezac.Tests.AOI.Properties.Resources.flecha_izquierda;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnNext.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnNext.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnNext.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.btnNext.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ScaleSize = new System.Drawing.Size(50, 28);
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.btnNext.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ImageScaling = Telerik.WinControls.Enumerations.ImageScaling.SizeToFit;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radGridView1);
            this.radPanel2.Location = new System.Drawing.Point(30, 730);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(638, 249);
            this.radPanel2.TabIndex = 13;
            // 
            // radPanel4
            // 
            this.radPanel4.Controls.Add(this.tableLayoutPanel1);
            this.radPanel4.Location = new System.Drawing.Point(959, 730);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(928, 249);
            this.radPanel4.TabIndex = 14;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnComponenteLevantaado, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnFinishRepair, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnFalsoFallo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnOtroFallo, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnNoReparado, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnCruce, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnFaltaEstano, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnBolaEstano, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnCable, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnSinComponente, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDescartarCircuito, 5, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(928, 249);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // btnComponenteLevantaado
            // 
            this.btnComponenteLevantaado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnComponenteLevantaado.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComponenteLevantaado.Location = new System.Drawing.Point(477, 99);
            this.btnComponenteLevantaado.Name = "btnComponenteLevantaado";
            this.btnComponenteLevantaado.Size = new System.Drawing.Size(201, 50);
            this.btnComponenteLevantaado.TabIndex = 7;
            this.btnComponenteLevantaado.Text = "Componente\r\nLevantado";
            this.btnComponenteLevantaado.Click += new System.EventHandler(this.BtnComponenteLevantaado_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnComponenteLevantaado.GetChildAt(0))).Text = "Componente\r\nLevantado";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnComponenteLevantaado.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFinishRepair
            // 
            this.btnFinishRepair.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFinishRepair.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinishRepair.Location = new System.Drawing.Point(250, 175);
            this.btnFinishRepair.Name = "btnFinishRepair";
            this.btnFinishRepair.Size = new System.Drawing.Size(201, 50);
            this.btnFinishRepair.TabIndex = 9;
            this.btnFinishRepair.Text = "Finalizar Panel";
            this.btnFinishRepair.Click += new System.EventHandler(this.btnFinishRepair_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnFinishRepair.GetChildAt(0))).Text = "Finalizar Panel";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(122)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(149)))), ((int)(((byte)(21)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(74)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.6584362F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9691358F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(204)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFinishRepair.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOtroFallo
            // 
            this.btnOtroFallo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOtroFallo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOtroFallo.Location = new System.Drawing.Point(704, 99);
            this.btnOtroFallo.Name = "btnOtroFallo";
            this.btnOtroFallo.Size = new System.Drawing.Size(201, 50);
            this.btnOtroFallo.TabIndex = 8;
            this.btnOtroFallo.Text = "Otro Fallo";
            this.btnOtroFallo.Click += new System.EventHandler(this.btnOtroFallo_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnOtroFallo.GetChildAt(0))).Text = "Otro Fallo";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnOtroFallo.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCruce
            // 
            this.btnCruce.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCruce.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCruce.Location = new System.Drawing.Point(250, 99);
            this.btnCruce.Name = "btnCruce";
            this.btnCruce.Size = new System.Drawing.Size(201, 50);
            this.btnCruce.TabIndex = 7;
            this.btnCruce.Text = "Cruce";
            this.btnCruce.Click += new System.EventHandler(this.btnCruce_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnCruce.GetChildAt(0))).Text = "Cruce";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnCruce.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFaltaEstano
            // 
            this.btnFaltaEstano.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFaltaEstano.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFaltaEstano.Location = new System.Drawing.Point(250, 23);
            this.btnFaltaEstano.Name = "btnFaltaEstano";
            this.btnFaltaEstano.Size = new System.Drawing.Size(201, 50);
            this.btnFaltaEstano.TabIndex = 6;
            this.btnFaltaEstano.Text = "Falta Estaño";
            this.btnFaltaEstano.Click += new System.EventHandler(this.btnFaltaEstano_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnFaltaEstano.GetChildAt(0))).Text = "Falta Estaño";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnFaltaEstano.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBolaEstano
            // 
            this.btnBolaEstano.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBolaEstano.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBolaEstano.Location = new System.Drawing.Point(23, 99);
            this.btnBolaEstano.Name = "btnBolaEstano";
            this.btnBolaEstano.Size = new System.Drawing.Size(201, 50);
            this.btnBolaEstano.TabIndex = 6;
            this.btnBolaEstano.Text = "Forma Defectuosa";
            this.btnBolaEstano.Click += new System.EventHandler(this.btnBolaEstano_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnBolaEstano.GetChildAt(0))).Text = "Forma Defectuosa";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnBolaEstano.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCable
            // 
            this.btnCable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCable.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCable.Location = new System.Drawing.Point(704, 23);
            this.btnCable.Name = "btnCable";
            this.btnCable.Size = new System.Drawing.Size(201, 50);
            this.btnCable.TabIndex = 7;
            this.btnCable.Text = "Pata demasiado \r\nLarga\r\n";
            this.btnCable.Click += new System.EventHandler(this.btnCable_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnCable.GetChildAt(0))).Text = "Pata demasiado \r\nLarga\r\n";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnCable.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSinComponente
            // 
            this.btnSinComponente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSinComponente.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSinComponente.Location = new System.Drawing.Point(477, 23);
            this.btnSinComponente.Name = "btnSinComponente";
            this.btnSinComponente.Size = new System.Drawing.Size(201, 50);
            this.btnSinComponente.TabIndex = 6;
            this.btnSinComponente.Text = "Falta\r\nComponente";
            this.btnSinComponente.Click += new System.EventHandler(this.btnSinComponente_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnSinComponente.GetChildAt(0))).Text = "Falta\r\nComponente";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(19)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.5349794F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9176955F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnSinComponente.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDescartarCircuito
            // 
            this.btnDescartarCircuito.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDescartarCircuito.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDescartarCircuito.Location = new System.Drawing.Point(477, 175);
            this.btnDescartarCircuito.Name = "btnDescartarCircuito";
            this.btnDescartarCircuito.Size = new System.Drawing.Size(201, 50);
            this.btnDescartarCircuito.TabIndex = 6;
            this.btnDescartarCircuito.Text = "Descartar Circuito";
            this.btnDescartarCircuito.Click += new System.EventHandler(this.btnDescartarCircuito_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnDescartarCircuito.GetChildAt(0))).Text = "Descartar Circuito";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(189)))), ((int)(((byte)(122)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(149)))), ((int)(((byte)(21)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(74)))), ((int)(((byte)(19)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).GradientAngle = 90F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).GradientPercentage = 0.6584362F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).GradientPercentage2 = 0.9691358F;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(204)))), ((int)(((byte)(156)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.btnDescartarCircuito.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radPanel6
            // 
            this.radPanel6.Location = new System.Drawing.Point(30, 4);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(1857, 43);
            this.radPanel6.TabIndex = 15;
            // 
            // lblFalsosFallos
            // 
            this.lblFalsosFallos.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFalsosFallos.Location = new System.Drawing.Point(209, 32);
            this.lblFalsosFallos.Name = "lblFalsosFallos";
            this.lblFalsosFallos.Size = new System.Drawing.Size(16, 18);
            this.lblFalsosFallos.TabIndex = 0;
            this.lblFalsosFallos.Text = "0";
            this.lblFalsosFallos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.Location = new System.Drawing.Point(3, 32);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(97, 18);
            this.radLabel15.TabIndex = 0;
            this.radLabel15.Text = "Falsos Fallos";
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.tableLayoutPanel2);
            this.radPanel3.Location = new System.Drawing.Point(667, 730);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(293, 249);
            this.radPanel3.TabIndex = 23;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.64846F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.35154F));
            this.tableLayoutPanel2.Controls.Add(this.lblDPMO, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.radLabel10, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.lblCruces, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.lblSoldadurasFriasBolas, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblPatasLargas, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.radLabel12, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.radLabel18, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblFaltasComponentes, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.radLabel6, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblFaltasEstaño, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.radLabel8, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblFalsosFallos, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.radLabel4, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelNumOrden, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel15, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radLabel19, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel16, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.lblOtrosFallos, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.radLabel3, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.lblLevantado, 1, 7);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.76471F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.803922F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(293, 249);
            this.tableLayoutPanel2.TabIndex = 31;
            // 
            // lblDPMO
            // 
            this.lblDPMO.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDPMO.Location = new System.Drawing.Point(209, 224);
            this.lblDPMO.Name = "lblDPMO";
            this.lblDPMO.Size = new System.Drawing.Size(16, 22);
            this.lblDPMO.TabIndex = 0;
            this.lblDPMO.Text = "0";
            this.lblDPMO.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(3, 224);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(161, 22);
            this.radLabel10.TabIndex = 0;
            this.radLabel10.Text = "DPMO";
            this.radLabel10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCruces
            // 
            this.lblCruces.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCruces.Location = new System.Drawing.Point(209, 152);
            this.lblCruces.Name = "lblCruces";
            this.lblCruces.Size = new System.Drawing.Size(16, 18);
            this.lblCruces.TabIndex = 0;
            this.lblCruces.Text = "0";
            this.lblCruces.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSoldadurasFriasBolas
            // 
            this.lblSoldadurasFriasBolas.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoldadurasFriasBolas.Location = new System.Drawing.Point(209, 128);
            this.lblSoldadurasFriasBolas.Name = "lblSoldadurasFriasBolas";
            this.lblSoldadurasFriasBolas.Size = new System.Drawing.Size(16, 18);
            this.lblSoldadurasFriasBolas.TabIndex = 0;
            this.lblSoldadurasFriasBolas.Text = "0";
            this.lblSoldadurasFriasBolas.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPatasLargas
            // 
            this.lblPatasLargas.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatasLargas.Location = new System.Drawing.Point(209, 104);
            this.lblPatasLargas.Name = "lblPatasLargas";
            this.lblPatasLargas.Size = new System.Drawing.Size(16, 18);
            this.lblPatasLargas.TabIndex = 0;
            this.lblPatasLargas.Text = "0";
            this.lblPatasLargas.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(3, 152);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(55, 18);
            this.radLabel12.TabIndex = 0;
            this.radLabel12.Text = "Cruces";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel18
            // 
            this.radLabel18.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel18.Location = new System.Drawing.Point(3, 128);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(139, 18);
            this.radLabel18.TabIndex = 0;
            this.radLabel18.Text = "Forma Defectuosa";
            this.radLabel18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFaltasComponentes
            // 
            this.lblFaltasComponentes.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaltasComponentes.Location = new System.Drawing.Point(209, 80);
            this.lblFaltasComponentes.Name = "lblFaltasComponentes";
            this.lblFaltasComponentes.Size = new System.Drawing.Size(16, 18);
            this.lblFaltasComponentes.TabIndex = 0;
            this.lblFaltasComponentes.Text = "0";
            this.lblFaltasComponentes.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(3, 104);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(163, 18);
            this.radLabel6.TabIndex = 0;
            this.radLabel6.Text = "Patas Largas o Cables";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFaltasEstaño
            // 
            this.lblFaltasEstaño.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaltasEstaño.Location = new System.Drawing.Point(209, 56);
            this.lblFaltasEstaño.Name = "lblFaltasEstaño";
            this.lblFaltasEstaño.Size = new System.Drawing.Size(16, 18);
            this.lblFaltasEstaño.TabIndex = 0;
            this.lblFaltasEstaño.Text = "0";
            this.lblFaltasEstaño.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(3, 80);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(169, 18);
            this.radLabel8.TabIndex = 0;
            this.radLabel8.Text = "Faltas de Componente";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(3, 56);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(124, 18);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Faltas de Estaño";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNumOrden
            // 
            this.labelNumOrden.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumOrden.Location = new System.Drawing.Point(209, 3);
            this.labelNumOrden.Name = "labelNumOrden";
            this.labelNumOrden.Size = new System.Drawing.Size(16, 23);
            this.labelNumOrden.TabIndex = 30;
            this.labelNumOrden.Text = "0";
            this.labelNumOrden.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel19.Location = new System.Drawing.Point(3, 3);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(86, 23);
            this.radLabel19.TabIndex = 1;
            this.radLabel19.Text = "Nº Orden:";
            this.radLabel19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel16.Location = new System.Drawing.Point(3, 200);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(93, 18);
            this.radLabel16.TabIndex = 0;
            this.radLabel16.Text = "Otros Fallos";
            this.radLabel16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOtrosFallos
            // 
            this.lblOtrosFallos.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOtrosFallos.Location = new System.Drawing.Point(209, 200);
            this.lblOtrosFallos.Name = "lblOtrosFallos";
            this.lblOtrosFallos.Size = new System.Drawing.Size(16, 18);
            this.lblOtrosFallos.TabIndex = 0;
            this.lblOtrosFallos.Text = "0";
            this.lblOtrosFallos.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(3, 176);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(181, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Componente Levantado";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLevantado
            // 
            this.lblLevantado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLevantado.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevantado.Location = new System.Drawing.Point(209, 176);
            this.lblLevantado.Name = "lblLevantado";
            this.lblLevantado.Size = new System.Drawing.Size(81, 18);
            this.lblLevantado.TabIndex = 0;
            this.lblLevantado.Text = "0";
            this.lblLevantado.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AOIResultViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1912, 1005);
            this.Controls.Add(this.radPanel3);
            this.Controls.Add(this.radPanel6);
            this.Controls.Add(this.radPanel4);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.radStatusStrip1);
            this.Name = "AOIResultViewerForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SAURON (AOI)";
            this.ThemeName = "TelerikMetroBlue";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnFalsoFallo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNoReparado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            this.radPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            this.radPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevious)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnComponenteLevantaado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFinishRepair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOtroFallo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCruce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFaltaEstano)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBolaEstano)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSinComponente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDescartarCircuito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFalsosFallos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDPMO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCruces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSoldadurasFriasBolas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPatasLargas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFaltasComponentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFaltasEstaño)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelNumOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtrosFallos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLevantado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private System.Windows.Forms.BindingSource bsList;
        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.UI.RadButton btnPrevious;
        private Telerik.WinControls.UI.RadButton btnNext;
        private Telerik.WinControls.UI.RadButton btnFalsoFallo;
        private Telerik.WinControls.UI.RadButton btnNoReparado;
        private HSmartWindowControl hWindow1;
        private HSmartWindowControl hWindow2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadButton btnCable;
        private Telerik.WinControls.UI.RadButton btnSinComponente;
        private Telerik.WinControls.UI.RadButton btnFaltaEstano;
        private Telerik.WinControls.UI.RadButton btnBolaEstano;
        private Telerik.WinControls.UI.RadButton btnCruce;
        private Telerik.WinControls.UI.RadButton btnOtroFallo;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.UI.RadLabel lblFalsosFallos;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel lblDPMO;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel lblPatasLargas;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel lblCruces;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel lblFaltasEstaño;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel lblOtrosFallos;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel lblFaltasComponentes;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel lblSoldadurasFriasBolas;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadButton btnFinishRepair;
        private Telerik.WinControls.UI.RadLabel labelNumOrden;
        private Telerik.WinControls.UI.RadButton btnComponenteLevantaado;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel lblLevantado;
        private Telerik.WinControls.UI.RadButton btnDescartarCircuito;
    }
}
