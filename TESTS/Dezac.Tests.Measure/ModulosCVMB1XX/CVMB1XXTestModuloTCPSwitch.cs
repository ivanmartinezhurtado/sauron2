﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Instruments.Router;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.05)]
    public class CVMB1XXTestModuloTCPSwitch : CVMB1XXTestModulosGenerico
    {
        CVMB1XXModuloModBusTCP moduloTCPSwitch;
        Router router;

        public void TestInitialization()
        {
            CVMDummy = new CVMB1XX(Comunicaciones.SerialPort);
            CVMDummy.Modbus.PerifericNumber = Comunicaciones.Periferico;
            Resultado.Set("VERSION_DEVICE", string.Format("{0}", CVMDummy.GetDeviceVersion()), ParamUnidad.SinUnidad);
            Resultado.Set("CLASSE_DEVICE", string.Format("{0}", CVMDummy.GetType().Name), ParamUnidad.SinUnidad);

            AddInstanceVar(CVMDummy, "UUT");

            router = Router.Default();
        }

        public void ConfigComunications(string namePortCVMDummy = "0")
        {
            if (namePortCVMDummy == "0")
                namePortCVMDummy = Comunicaciones.SerialPort.ToString();

            var portCVMDummy = Convert.ToByte(GetVariable<string>(namePortCVMDummy, namePortCVMDummy).Replace("COM", ""));
            CVMDummy.Modbus.PortCom = portCVMDummy;
        }

        public void TestComunicationsTCP()
        {
            var macReading = CVMDummy.ReadMac();
            string IP = "";
            string macArray = "";

            if (macReading.ToString().ToUpper().Equals(Configuracion.GetString("MAC_DEFECTO", "14:A6:2C:00:00:00", ParamUnidad.SinUnidad)) || macReading.ToString().Equals("00:00:00:00:00:00"))
            {
                var macs = this.GetMacServiceDezac(1);

                Assert.IsNotNullOrEmpty(macs, Error().SOFTWARE.WEB_SERVICE.VARIABLE_ES_NULA("No se han obtenido MAC del Servicio Web"));

                macArray = macs.FirstOrDefault().Replace("-", "");
            }
            else
            {
                CVMDummy.Reset();
                Delay(5000, "espera");
                return;
            }
                      
            var clients = new List<ClientEntry>();

            SamplerWithCancel((p) =>
            {
                clients = router.GetConnectedClientsFromWeb(true);

                return !clients.Count.Equals(0);
            }, "No se han encontrado clientes DHCP conectados al router", 50, 1000);

            Delay(1000, "Espera conexion");

            var ipMaster = clients.Where((p) => { return p.MAC.ToUpper() == macReading.ToString().ToUpper(); }).FirstOrDefault();

            if (ipMaster == null)
            {
                var tcpConfiguration = CVMDummy.ReadTCPConfiguration();

                CVMDummy.WriteDHCPFlag(false);

                tcpConfiguration.Gateway = router.RouterIP;

                var configRouter = router.ReadConfig();
                if(configRouter.LanMaskIP != null)
                    tcpConfiguration.Mask = router.ReadConfig().LanMaskIP;

                var ip = router.ReadConfig().LanIP.Split('.');

                tcpConfiguration.IP = string.Format("{0}.{1}.{2}.{3}", ip[0], ip[1], ip[2], (Convert.ToInt16(ip[3]) + 10).ToString());

                CVMDummy.WriteTCPConfiguration(tcpConfiguration);

                IP = tcpConfiguration.IP;
            }
            else
                IP = ipMaster.IP;

            SamplerWithCancel((p) =>
            {
                moduloTCPSwitch = new CVMB1XXModuloModBusTCP(IP, 502, ModbusTCPProtocol.TCP);

                return true;
            }, "No se ha podido conecar con el Modulo Ethernet", 5, 1500, 0, false, false);

            if (!string.IsNullOrEmpty(macArray))
                moduloTCPSwitch.WriteMACTCP(macArray);

            CVMDummy.WriteDHCPFlag(true);

            router.DeleteAllDHCPEntry(true);

            CVMDummy.Reset();

            Delay(5000, "espera");

            moduloTCPSwitch.Dispose();

            moduloTCPSwitch = null;
        }

        public void TestTCPSwitchToMaster()
        {
            //*************************************************************************
            SamplerWithCancel((p) =>
            {
                CVMDummy.ReadMac();
                return true;
            }, "Error de comunicaciones al  con el Modulo Ethernet", 30, 1500, 1000, false, false);

            var macReading = CVMDummy.ReadMac();

            var clients = new List<ClientEntry>();

            SamplerWithCancel((p) =>
            {
                clients = router.GetConnectedClientsFromWeb(true);

                return !clients.Count.Equals(0);
            }, "No se han encontrado clientes DHCP conectados al router", 50, 1000);

            Delay(1000, "Espera conexion");

            var ipMaster = clients.Where((p) => { return p.MAC.ToUpper() == macReading.ToString().ToUpper(); }).FirstOrDefault();

            if (ipMaster == null)
                throw new Exception(string.Format("No se ha encontrado la MAC del TCP/MASTER en el router, MAC -> {0}", macReading.ToString()));

            SamplerWithCancel((p) =>
            {
                moduloTCPSwitch = new CVMB1XXModuloModBusTCP(ipMaster.IP, 502, ModbusTCPProtocol.TCP);

                return true;
            }, "No se ha podido conecar con el Modulo Ethernet", 5, 1500, 0, false, false);

            moduloTCPSwitch.tcp.PerifericNumber = 1;

            SamplerWithCancel((p) =>
            {
                var macRead = moduloTCPSwitch.ReadMAC();

                Assert.AreEqual("MAC", macRead.ToString().ToUpper(), ipMaster.MAC.ToUpper(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error. La MAC leida no coincide con la grabada"), ParamUnidad.SinUnidad);

                TestInfo.NumMAC = macRead.ToString();

                var numserieMaster = moduloTCPSwitch.ReadNumSerie().CToNetString();

                Assert.IsTrue(numserieMaster.Trim() == Configuracion.GetString("SERIAL_NUMBER_CVMB100_MASTER", numserieMaster, ParamUnidad.SinUnidad).Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. el numero de serie leido del CVM-B100 Master no corresponde"));

                return true;
            }, string.Empty, 5, 1000, 0, false, false);

            Resultado.Set("TEST_COMUNICATIONS_TCP-MASTER", "OK", ParamUnidad.SinUnidad);
        }

        public void TestTCPSwitchToSlave()
        {
            Shell.MsgBox("Encienda el modulo SLAVE con el TCP-SWITCH conectado al MASTER", "Test TCP-MASTER --> TCP-SLAVE", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

            logger.Info("CVM-B1XX SLAVE CON PERIFERICO 2 y MODULO TCP-SWITCH EN DHCP ACTIVADO");

            var macReading = CVMDummy.ReadMac();

            var clients = new List<ClientEntry>();

            SamplerWithCancel((p) =>
            {
                clients = router.GetConnectedClientsFromWeb(true);

                return clients.Count == 2;
            }, "No se han encontrado clientes DHCP conectados al router", 50, 4000);

            Delay(1000, "Espera conexion");

            var ipSlave = clients.Where((p) => { return p.MAC.ToUpper() != macReading.ToString().ToUpper(); }).FirstOrDefault();

            if (ipSlave == null)
                throw new Exception(string.Format("No se ha encontrado la MAC del TCP/SLAVE en el router, MAC -> {0}", macReading.ToString()));

            SamplerWithCancel((p) =>
            {
                moduloTCPSwitch = new CVMB1XXModuloModBusTCP(ipSlave.IP, 502, ModbusTCPProtocol.TCP);

                return true;
            }, "No se ha podido conecar con el Modulo Ethernet", 5, 1500, 0, false, false);

            moduloTCPSwitch.tcp.PerifericNumber = 2;

            SamplerWithCancel((p) =>
            {
                var macRead = moduloTCPSwitch.ReadMAC();

                Assert.AreEqual(macRead.ToString().ToUpper(), ipSlave.MAC.ToUpper(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error. La MAC leida no coincide con la del modulo patron"));

                var numserieslave = moduloTCPSwitch.ReadNumSerie().CToNetString();

                Assert.IsTrue(numserieslave.Trim() == Configuracion.GetString("SERIAL_NUMBER_CVMB100_SLAVE", numserieslave, ParamUnidad.SinUnidad).Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. el numero de serie leido del CVM-B100 Master no corresponde"));

                return true;
            }, string.Empty, 5, 1000, 0, false, false);

            Resultado.Set("TEST_COMUNICATIONS_TCP-SLAVE", "OK", ParamUnidad.SinUnidad);
        }

        public void TestFinish()
        {
            if (moduloTCPSwitch != null)
                moduloTCPSwitch.Dispose();

            base.Dispose();
        }
    }
}
