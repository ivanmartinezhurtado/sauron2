﻿using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.Measure;
using System;
using System.Windows.Forms;


namespace Dezac.Tests.Measure
{
    [TestVersion(1.02)]
    public class CVMB1XXTestModuloDIO : CVMB1XXTestModulosGenerico
    {
        public void TestInitialization(byte portCVM)
        {
            if (portCVM == 0)
                portCVM = Comunicaciones.SerialPort;

            if (Model.IdFase != "120")
            {
                portCVM = Convert.ToByte(GetVariable<string>("portCVM", "portCVM").Replace("COM", ""));
                CVMDummy = AddInstanceVar(new CVMB1XX(portCVM), "UUT");
                CVMDummy.Modbus.PerifericNumber = Comunicaciones.Periferico;
                Resultado.Set("VERSION_DEVICE", string.Format("{0}", CVMDummy.GetDeviceVersion()), ParamUnidad.SinUnidad);
                Resultado.Set("CLASSE_DEVICE", string.Format("{0}", CVMDummy.GetType().Name), ParamUnidad.SinUnidad);
            }

        }

        public void TestConsumptionDIOOpen(string portMTX)
        {
            SamplerWithCancel((p) => { CVMDummy.ReadSoftwareVersion(); return true; }, "Error, no se ha podido comunicar con el B100 de dentro del útil", 10, 2000, 4000, false, false);

            CVMDummy.WriteOutputs(CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF);
            Delay(3000, "");

            using (var tester = new MTX3293())
            {
                //min = 68 //max = 102
                tester.PortCom = Convert.ToByte(GetVariable<string>(portMTX, portMTX).Replace("COM", ""));
                tester.WaitTimeMeasure = 1500;

                var defs = new AdjustValueDef(Params.I.Null.TestPoint("CONSUM_OPEN").Name, 0, Params.I.Null.TestPoint("CONSUM_OPEN").Min(), Params.I.Null.TestPoint("CONSUM_OPEN").Max(), 0, 0, ParamUnidad.mV);

                TestMeasureBase(defs, (step) =>
                {
                    return tester.ReadCurrent(MTX3293.measureTypes.DC) * 1000;
                }, 1, 5, 500);
            }
        }

        public void TestConsumptionDIOClose(string portMTX)
        {    
            CVMDummy.WriteOutputs(CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON, CVMB1XX.OutputState.ON);
            Delay(6000, "Esperando que la placa active las salidas");

            using(var tester = new MTX3293())
            {
                //min = 152 //max = 228
                tester.PortCom = Convert.ToByte(GetVariable<string>(portMTX, portMTX).Replace("COM", ""));
                tester.WaitTimeMeasure = 1500;

                var defs = new AdjustValueDef(Params.I.Null.TestPoint("CONSUM_CLOSE").Name, 0, Params.I.Null.TestPoint("CONSUM_CLOSE").Min(), Params.I.Null.TestPoint("CONSUM_CLOSE").Max(), 0, 0, ParamUnidad.mV);

                TestMeasureBase(defs, (step) =>
                {
                    return tester.ReadCurrent(MTX3293.measureTypes.DC) * 1000;
                }, 1, 5, 500);
            }

            CVMDummy.WriteOutputs(CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF, CVMB1XX.OutputState.OFF);
            Delay(6000, "");
        }

        public void TestTypeModule()
        {         
            var tipoModulo = (CVMB1XX.ModuleType)CVMDummy.ReadDetectionModulo();
            var tipoModuloBBDD = CVMB1XX.ModuleType.UNIDENTIFIED;
            Enum.TryParse<CVMB1XX.ModuleType>(Identificacion.MODELO.Trim(), out tipoModuloBBDD);

            Assert.IsTrue(tipoModulo == tipoModuloBBDD, Error().UUT.CONFIGURACION.NO_COINCIDE("Error, el tipo de modelo no es el esperado"));

            Resultado.Set("MODULE_TYPE", tipoModulo.ToString(), ParamUnidad.SinUnidad);
        }

        public void TestDIO()
        {
            CVMDummy.FlagTestDIO();           

            SamplerWithCancel(
                (p) =>
                {
                    return CVMDummy.ReadInputStatus();
                }, "Error, Se ha detectado un error al leer las entradas digitales", 100, 1000);

            Resultado.Set("STATE_INPUTS", "OK", ParamUnidad.SinUnidad);
        }
       
        public void TestFinish()
        {
            if (CVMDummy != null)
                TestDispose();
        }
    }
}
