﻿namespace Instruments.Utility
{
    public struct MagnitudToMeasure
    {
        public MagnitudsMultimeter Magnitud;
        public freqEnum Frequency;
        public double Rango;
        public double DigitosPrecision;
        public double NPLC;
        public int NumMuestras;

        public MagnitudToMeasure(MagnitudsMultimeter magnitud)
        {
            Magnitud = magnitud;
            Frequency = freqEnum._50Hz;
            Rango = 2;
            DigitosPrecision = 6;
            NPLC = 0;
            NumMuestras = 3;
        }

        public MagnitudToMeasure(MagnitudsMultimeter magnitud, double rango)
        {
            Magnitud = magnitud;
            Frequency = freqEnum._50Hz;
            Rango = rango;
            DigitosPrecision = 6;
            NPLC = 0;
            NumMuestras = 3;
        }

        public MagnitudToMeasure(MagnitudsMultimeter magnitud, double rango, double digitos)
        {
            Magnitud = magnitud;
            Frequency = freqEnum._50Hz;
            Rango = rango;
            DigitosPrecision = digitos;
            NPLC = 0;
            NumMuestras = 3;
        }
    }

        public enum freqEnum
        {
            _50Hz = 50,
            _60Hz = 60
        }

        public enum MagnitudsMultimeter
        {
            Resistencia,
            VoltAC,
            VoltDC,
            AmpAC,
            AmpDC,
        }

    public enum InputMuxEnum
    {
        NOTHING = 0,
        IN1 = 1,
        IN2 = 2,
        IN3 = 3,
        IN4 = 4,
        IN5 = 5,
        IN6 = 6,
        IN7 = 7,
        IN8 = 8,
        IN9 = 9,
        IN10 = 10,
        IN11 = 11,
        IN12 = 12,
        IN13 = 13,
        IN14 = 14,
        IN15 = 15,
        IN16 = 16,
        IN17 = 17,
        IN18 = 18,
        IN19 = 19,
        IN20 = 20,
        IN21 = 21,
        IN22 = 22,
        IN23 = 23,
        IN24 = 24,
        IN25 = 25,
        IN26 = 26,
        IN27 = 27,
        IN28 = 28,
        IN29 = 29,
        IN30 = 30,
        IN31 = 31,
        IN32 = 32,
    }
}
