﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Instruments.Utility
{
    public class Command<T>
    {
        private T commands;

        public Command(T commands, bool? throwException, params object[] args)
        {
            this.commands = commands;
            this.ThrowException = throwException;
            this.Args = args;
        }

        public int Commands { get { return Convert.ToInt32(commands); } }

        public object[] Args { get; set; }
        public bool? ThrowException { get; set; }

        public ushort[] Request { get; set; }

        public Exception Exception { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", commands);
        }
    }
}
