﻿using log4net.Appender;
using log4net.Core;
using System;

namespace Dezac.Tests.Services.Logger
{
    public class LogInterceptor : AppenderSkeleton
    {
        private static Object _syncRoot;

        public static event EventHandler<LoggingEventWrapper> OnLog;

        public LogInterceptor()
        {
            _syncRoot = new object();
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            EventHandler<LoggingEventWrapper> temp = OnLog;
        
            if (temp != null)
                lock (_syncRoot)
                {
                    temp(null, new LoggingEventWrapper(loggingEvent));
                }
        }

        public class OnLog4NetLogEventArgs : EventArgs
        {
            public LoggingEvent LoggingEvent { get; private set; }

            public OnLog4NetLogEventArgs(LoggingEvent loggingEvent)
            {
                LoggingEvent = loggingEvent;
            }
        }
    }

    public class LoggingEventWrapper
    {
        private LoggingEvent ev;

        public LoggingEventWrapper(LoggingEvent l)
        {
            ev = l;
            var obj = l.LookupProperty("StepPath");
            if (obj != null)
                StepPath = obj.ToString();
            obj = l.LookupProperty("NumInstance");
            if (obj != null)
                NumInstance = (int) obj;
            obj = l.LookupProperty("Result");
            if (obj != null)
                Result = obj.ToString();
            obj = l.LookupProperty("ExecutionTime");
            if (obj != null)
                ExecutionTime = (TimeSpan)obj;
        }

        public Level Level { get { return ev.Level; } }
        public string LoggerName { get { return ev.LoggerName; } }
        public string RenderedMessage { get { return ev.RenderedMessage; } }
        public DateTime TimeStamp { get { return ev.TimeStamp; } }
        public string StepPath { get; set; }
        public string ThreadName { get { return ev.ThreadName; } }
        public int NumInstance { get; set; }
        public string Result { get; set; }
        public TimeSpan ExecutionTime { get; set; }
    }
}
