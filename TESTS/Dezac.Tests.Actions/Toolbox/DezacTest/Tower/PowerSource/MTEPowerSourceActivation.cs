﻿using Dezac.Core.Utility;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.05)]
    [DesignerAction(Type = typeof(MTEPowerSourceActivationActionDescription))]
    [Browsable(false)]

    public class MTEPowerSourceActivation : ActionBase
    {
        [DefaultValue(null)]
        [Description("Puerto Serie")]
        public string SerialPort { get; set; }

        [DefaultValue("230")]
        [Description("Tensión L1")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string VoltageL1 { get; set; }

        [DefaultValue("230")]
        [Description("Tensión L2")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string VoltageL2 { get; set; }

        [Description("Tensión L3")]
        [DefaultValue("230")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string VoltageL3 { get; set; }

        [DefaultValue("0")]
        [Description("Corriente L1")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string CurrentL1 { get; set; }

        [DefaultValue("0")]
        [Description("Corriente L2")]
        public string CurrentL2 { get; set; }

        [DefaultValue("0")]
        [Description("Corriente L3")]
        public string CurrentL3 { get; set; }

        [DefaultValue("0")]
        [Description("Factor de potencia L1")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PowerFactorL1 { get; set; }

        [DefaultValue("0")]
        [Description("Factor de potencia L2")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PowerFactorL2 { get; set; }

        [DefaultValue("0")]
        [Description("Factor de potencia L3")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PowerFactorL3 { get; set; }

        [DefaultValue("0")]
        [Description("El desfase L1")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PhaseGapL1 { get; set; }

        [DefaultValue("120")]
        [Description("El desfase L2")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PhaseGapL2 { get; set; }

        [DefaultValue("240")]
        [Description("El desfase L3")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string PhaseGapL3 { get; set; }

        [DefaultValue("50")]
        [Description("Frecuencia")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string Frequency { get; set; }

        [DefaultValue(false)]
        [Description("No Wait stabilization")]
        public bool NoWaitStabilization { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();

            if (!string.IsNullOrEmpty(SerialPort))
            {
                var portDevice = Convert.ToByte(GetVariable<string>(SerialPort, SerialPort).Replace("COM", ""));
                tower.PortMte = portDevice;
                tower.MTEPS.PortCom = portDevice;
            }

            var voltage = new TriLineValue { L1 = ResolveVariable(VoltageL1), L2 = ResolveVariable(VoltageL2), L3 = ResolveVariable(VoltageL3) };
            var current = new TriLineValue { L1 = ResolveVariable(CurrentL1), L2 = ResolveVariable(CurrentL2), L3 = ResolveVariable(CurrentL3) };
            var powerFactor = new TriLineValue { L1 = ResolveVariable(PowerFactorL1), L2 = ResolveVariable(PowerFactorL2), L3 = ResolveVariable(PowerFactorL3) };
            var desfase = new TriLineValue { L1 = ResolveVariable(PhaseGapL1), L2 = ResolveVariable(PhaseGapL2), L3 = ResolveVariable(PhaseGapL3) };
            var frecuency = ResolveVariable(Frequency);

            if (!NoWaitStabilization)
                tower.MTEPS.ApplyPresetsAndWaitStabilisation(voltage, current, frecuency, powerFactor, desfase);
            else
                tower.MTEPS.ApplyPresets(voltage, current, frecuency, powerFactor, desfase);
        }

        private double ResolveVariable(string variableName)
        {
            return Convert.ToDouble(VariablesTools.ResolveValue(variableName).ToString().Replace(".", ","));
        }
    }
    
    public class MTEPowerSourceActivationActionDescription : ActionDescription
    {
        public override string Name { get { return "ActivateMTE"; } }
        public override string Description { get { return "Control de la fuente MTE de la torre"; } }
        public override string Category { get { return "Hardware/Tower/PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;PowerSourcesShutdown:2;TowerDispose:2"; } }
    }
}