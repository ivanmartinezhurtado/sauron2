﻿namespace Dezac.Instruments.Controls
{
    partial class UC_PTE
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.btnOffPTE100V = new System.Windows.Forms.Button();
            this.btnOnPTE100V = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.label7 = new System.Windows.Forms.Label();
            this.txtV1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(362, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "V";
            // 
            // cmb_Port
            // 
            this.cmb_Port.BackColor = System.Drawing.Color.AliceBlue;
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(29, 85);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(81, 24);
            this.cmb_Port.TabIndex = 6;
            // 
            // btnOffPTE100V
            // 
            this.btnOffPTE100V.BackColor = System.Drawing.Color.DimGray;
            this.btnOffPTE100V.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOffPTE100V.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOffPTE100V.ForeColor = System.Drawing.Color.White;
            this.btnOffPTE100V.Location = new System.Drawing.Point(337, 78);
            this.btnOffPTE100V.Name = "btnOffPTE100V";
            this.btnOffPTE100V.Size = new System.Drawing.Size(77, 32);
            this.btnOffPTE100V.TabIndex = 8;
            this.btnOffPTE100V.Text = "OFF";
            this.btnOffPTE100V.UseVisualStyleBackColor = false;
            this.btnOffPTE100V.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // btnOnPTE100V
            // 
            this.btnOnPTE100V.BackColor = System.Drawing.Color.DimGray;
            this.btnOnPTE100V.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOnPTE100V.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOnPTE100V.ForeColor = System.Drawing.Color.White;
            this.btnOnPTE100V.Location = new System.Drawing.Point(189, 78);
            this.btnOnPTE100V.Name = "btnOnPTE100V";
            this.btnOnPTE100V.Size = new System.Drawing.Size(77, 32);
            this.btnOnPTE100V.TabIndex = 7;
            this.btnOnPTE100V.Text = "ON";
            this.btnOnPTE100V.UseVisualStyleBackColor = false;
            this.btnOnPTE100V.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.BackgroundImage = global::Dezac.Instruments.Properties.Resources.Actions_exit_icon;
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(280, 74);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(41, 37);
            this.btnReset.TabIndex = 17;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.AliceBlue;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label11.Location = new System.Drawing.Point(30, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 19);
            this.label11.TabIndex = 30;
            this.label11.Text = "Serial Port";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // pbStatus
            // 
            this.pbStatus.BackColor = System.Drawing.Color.AliceBlue;
            this.pbStatus.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.pbStatus.Location = new System.Drawing.Point(190, 58);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(224, 10);
            this.pbStatus.TabIndex = 40;
            this.pbStatus.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(209, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.TabIndex = 48;
            this.label7.Text = "Voltage";
            // 
            // txtV1
            // 
            this.txtV1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV1.ForeColor = System.Drawing.Color.Blue;
            this.txtV1.Location = new System.Drawing.Point(274, 21);
            this.txtV1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV1.Name = "txtV1";
            this.txtV1.Size = new System.Drawing.Size(84, 26);
            this.txtV1.TabIndex = 55;
            this.txtV1.Text = "230";
            this.txtV1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.PowerSourcePTE;
            this.pictureBox1.Location = new System.Drawing.Point(5, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 64;
            this.pictureBox1.TabStop = false;
            // 
            // UC_PTE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtV1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.btnOffPTE100V);
            this.Controls.Add(this.btnOnPTE100V);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Name = "UC_PTE";
            this.Size = new System.Drawing.Size(469, 119);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.Button btnOffPTE100V;
        private System.Windows.Forms.Button btnOnPTE100V;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtV1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
