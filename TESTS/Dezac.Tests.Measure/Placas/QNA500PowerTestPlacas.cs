﻿using Dezac.Tests.Services;
using Instruments.Towers;

namespace Dezac.Tests.Measure.Placas
{
    public class QNA500PowerTestPlacas : TestBase
    {
        TowerBoard tower;

        public void TestInitialization()
        {
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.Active24VDC();
        }

        public void TestEnd()
        {
            if (tower != null)
            {
                tower.IO.DO.Off(14, 17, 18, 19, 20, 21);

                tower.ShutdownSources();

                tower.Dispose();
            }
        }
    }
}
