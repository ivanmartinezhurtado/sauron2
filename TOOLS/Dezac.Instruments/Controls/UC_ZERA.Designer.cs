﻿namespace Dezac.Instruments.Controls
{
    partial class UC_ZERA
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnOn = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.btnOff = new System.Windows.Forms.Button();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFreq = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPF = new System.Windows.Forms.TextBox();
            this.txtV1 = new System.Windows.Forms.TextBox();
            this.txtI1 = new System.Windows.Forms.TextBox();
            this.txtI2 = new System.Windows.Forms.TextBox();
            this.txtA1 = new System.Windows.Forms.TextBox();
            this.txtA2 = new System.Windows.Forms.TextBox();
            this.txtV2 = new System.Windows.Forms.TextBox();
            this.txtI3 = new System.Windows.Forms.TextBox();
            this.txtA3 = new System.Windows.Forms.TextBox();
            this.txtV3 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(441, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "PF(º)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(426, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Freq (Hz)";
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(9, 110);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(95, 36);
            this.btnOn.TabIndex = 11;
            this.btnOn.Text = "On";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(15, 81);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(78, 21);
            this.cmb_Port.TabIndex = 13;
            // 
            // btnOff
            // 
            this.btnOff.BackColor = System.Drawing.Color.DimGray;
            this.btnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOff.ForeColor = System.Drawing.Color.White;
            this.btnOff.Location = new System.Drawing.Point(9, 147);
            this.btnOff.Name = "btnOff";
            this.btnOff.Size = new System.Drawing.Size(95, 36);
            this.btnOff.TabIndex = 12;
            this.btnOff.Text = "Off";
            this.btnOff.UseVisualStyleBackColor = false;
            this.btnOff.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(117, 172);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(380, 10);
            this.pbStatus.TabIndex = 39;
            this.pbStatus.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(136, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "Angle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(241, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 42;
            this.label2.Text = "Angle";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(344, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 43;
            this.label3.Text = "Angle";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(130, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 44;
            this.label4.Text = "Current:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(129, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.TabIndex = 47;
            this.label7.Text = "Voltage";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(234, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 48;
            this.label5.Text = "Voltage";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(337, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 49;
            this.label6.Text = "Voltage";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(235, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 16);
            this.label8.TabIndex = 50;
            this.label8.Text = "Current:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(338, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 16);
            this.label9.TabIndex = 51;
            this.label9.Text = "Current:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.MediumBlue;
            this.label10.Location = new System.Drawing.Point(144, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 24);
            this.label10.TabIndex = 52;
            this.label10.Text = "L1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.MediumBlue;
            this.label13.Location = new System.Drawing.Point(249, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 24);
            this.label13.TabIndex = 53;
            this.label13.Text = "L2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.MediumBlue;
            this.label14.Location = new System.Drawing.Point(352, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 24);
            this.label14.TabIndex = 54;
            this.label14.Text = "L3";
            // 
            // txtFreq
            // 
            this.txtFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFreq.ForeColor = System.Drawing.Color.Black;
            this.txtFreq.Location = new System.Drawing.Point(425, 46);
            this.txtFreq.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtFreq.Name = "txtFreq";
            this.txtFreq.Size = new System.Drawing.Size(74, 24);
            this.txtFreq.TabIndex = 3;
            this.txtFreq.Text = "50";
            this.txtFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(14, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 16);
            this.label15.TabIndex = 55;
            this.label15.Text = "Serial Port";
            // 
            // txtPF
            // 
            this.txtPF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPF.ForeColor = System.Drawing.Color.Green;
            this.txtPF.Location = new System.Drawing.Point(425, 140);
            this.txtPF.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtPF.Name = "txtPF";
            this.txtPF.Size = new System.Drawing.Size(74, 26);
            this.txtPF.TabIndex = 7;
            this.txtPF.Text = "0";
            this.txtPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtV1
            // 
            this.txtV1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV1.ForeColor = System.Drawing.Color.Blue;
            this.txtV1.Location = new System.Drawing.Point(118, 45);
            this.txtV1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV1.Name = "txtV1";
            this.txtV1.Size = new System.Drawing.Size(84, 26);
            this.txtV1.TabIndex = 0;
            this.txtV1.Text = "230";
            this.txtV1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtI1
            // 
            this.txtI1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtI1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI1.ForeColor = System.Drawing.Color.Red;
            this.txtI1.Location = new System.Drawing.Point(118, 92);
            this.txtI1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtI1.Name = "txtI1";
            this.txtI1.Size = new System.Drawing.Size(84, 26);
            this.txtI1.TabIndex = 4;
            this.txtI1.Text = "0";
            this.txtI1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtI2
            // 
            this.txtI2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtI2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI2.ForeColor = System.Drawing.Color.Red;
            this.txtI2.Location = new System.Drawing.Point(223, 92);
            this.txtI2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtI2.Name = "txtI2";
            this.txtI2.Size = new System.Drawing.Size(84, 26);
            this.txtI2.TabIndex = 5;
            this.txtI2.Text = "0";
            this.txtI2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtA1
            // 
            this.txtA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA1.ForeColor = System.Drawing.Color.Green;
            this.txtA1.Location = new System.Drawing.Point(118, 140);
            this.txtA1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtA1.Name = "txtA1";
            this.txtA1.Size = new System.Drawing.Size(84, 26);
            this.txtA1.TabIndex = 8;
            this.txtA1.Text = "0";
            this.txtA1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtA2
            // 
            this.txtA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtA2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA2.ForeColor = System.Drawing.Color.Green;
            this.txtA2.Location = new System.Drawing.Point(223, 140);
            this.txtA2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtA2.Name = "txtA2";
            this.txtA2.Size = new System.Drawing.Size(84, 26);
            this.txtA2.TabIndex = 9;
            this.txtA2.Text = "120";
            this.txtA2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtV2
            // 
            this.txtV2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV2.ForeColor = System.Drawing.Color.Blue;
            this.txtV2.Location = new System.Drawing.Point(223, 45);
            this.txtV2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV2.Name = "txtV2";
            this.txtV2.Size = new System.Drawing.Size(84, 26);
            this.txtV2.TabIndex = 1;
            this.txtV2.Text = "230";
            this.txtV2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtI3
            // 
            this.txtI3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtI3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtI3.ForeColor = System.Drawing.Color.Red;
            this.txtI3.Location = new System.Drawing.Point(327, 92);
            this.txtI3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtI3.Name = "txtI3";
            this.txtI3.Size = new System.Drawing.Size(83, 26);
            this.txtI3.TabIndex = 6;
            this.txtI3.Text = "0";
            this.txtI3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtA3
            // 
            this.txtA3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA3.ForeColor = System.Drawing.Color.Green;
            this.txtA3.Location = new System.Drawing.Point(327, 140);
            this.txtA3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtA3.Name = "txtA3";
            this.txtA3.Size = new System.Drawing.Size(83, 26);
            this.txtA3.TabIndex = 10;
            this.txtA3.Text = "240";
            this.txtA3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtV3
            // 
            this.txtV3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtV3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtV3.ForeColor = System.Drawing.Color.Blue;
            this.txtV3.Location = new System.Drawing.Point(326, 45);
            this.txtV3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtV3.Name = "txtV3";
            this.txtV3.Size = new System.Drawing.Size(84, 26);
            this.txtV3.TabIndex = 2;
            this.txtV3.Text = "230";
            this.txtV3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.PowerSourceZERA;
            this.pictureBox1.Location = new System.Drawing.Point(5, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // UC_ZERA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPF);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.txtFreq);
            this.Controls.Add(this.txtV1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtI1);
            this.Controls.Add(this.txtI2);
            this.Controls.Add(this.txtA1);
            this.Controls.Add(this.txtA2);
            this.Controls.Add(this.txtV2);
            this.Controls.Add(this.txtI3);
            this.Controls.Add(this.txtA3);
            this.Controls.Add(this.txtV3);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.btnOff);
            this.Controls.Add(this.btnOn);
            this.DoubleBuffered = true;
            this.Name = "UC_ZERA";
            this.Size = new System.Drawing.Size(510, 193);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.Button btnOff;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox txtV3;
        private System.Windows.Forms.TextBox txtV2;
        private System.Windows.Forms.TextBox txtV1;
        private System.Windows.Forms.TextBox txtI3;
        private System.Windows.Forms.TextBox txtI2;
        private System.Windows.Forms.TextBox txtI1;
        private System.Windows.Forms.TextBox txtA3;
        private System.Windows.Forms.TextBox txtA2;
        private System.Windows.Forms.TextBox txtA1;
        private System.Windows.Forms.TextBox txtPF;
        private System.Windows.Forms.TextBox txtFreq;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
