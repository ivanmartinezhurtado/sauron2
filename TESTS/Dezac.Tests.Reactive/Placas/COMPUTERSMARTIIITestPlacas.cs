﻿using Dezac.Device.Reactive;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Towers;
using Instruments.Utility;
using System;

namespace Dezac.Tests.Reactive
{
    [TestVersion(1.02)]
    public class COMPUTERSMARTIIITestPlacas : TestBase
    {
        private TowerBoard tower;
        private COMPUTERSMARTIII compSmartIII;

        public void TestInitialization()
        {
            compSmartIII = AddInstanceVar(new COMPUTERSMARTIII(Comunicaciones.SerialPort), "UUT");

            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            if (tower == null)
                throw new Exception("No se ha instanciado una torre para iniciar este test");

            tower.IO.DO.Off(43); // apagamos Neon equipo malo

            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);

            SamplerWithCancel((p) =>
            {
                tower.WaitBloqSecurity(100);
                return true;
            }, "Error: No se ha cerrado el utillaje.", 10, 100, 1000, false, false);
        }

        public void TestCommunications()
        {
            tower.Chroma.ApplyPresetsAndWaitStabilisation(230, 0.5, 50);
           
            SamplerWithCancel((p) =>
            {
                compSmartIII.WriteFlagTest();
                return true;

            }, "", 15, 500, 10000, false, false);

            var firmwareVersion = compSmartIII.ReadFirmwareVersion();
            var firmwareVersionBBDD = Identificacion.VERSION_FIRMWARE;
            Resultado.Set(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion, ParamUnidad.SinUnidad, firmwareVersionBBDD);
            Assert.IsTrue(firmwareVersion == firmwareVersionBBDD, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware incorrecta"));

            compSmartIII.WriteBastidor((int)TestInfo.NumBastidor.Value);

            compSmartIII.WriteCodeError(0);
        }

        public void TestConsumptionEnVacio()
        {
            internalTestConsumption(TypeTestPoint.EnVacio, 110, TypeTestVoltage.VoltageMinimo);

            internalTestConsumption(TypeTestPoint.EnVacio, 480, TypeTestVoltage.VoltageMaximo);
        }

        public void TestConsumptionConCarga()
        {
            tower.IO.DO.On(19); //Maxima carga

            internalTestConsumption(TypeTestPoint.ConCarga, 110, TypeTestVoltage.VoltageMinimo);

            internalTestConsumption(TypeTestPoint.ConCarga, 480, TypeTestVoltage.VoltageMaximo);

            tower.IO.DO.Off(19); //Maxima carga
        }

        public void TestFinish()
        {
            if (compSmartIII != null)
                compSmartIII.Dispose();

            if (tower != null)
            {
                tower.IO.DO.Off(19); //Maxima carga
                tower.IO.DO.Off(13); // Vx2
                tower.ShutdownSources();
                tower.Dispose();

                if (IsTestError && tower != null)
                    tower.IO.DO.On(43);
            }
        }

        //**********************************************************
        //**********************************************************

        private void internalTestConsumption(TypeTestPoint typeTestPoint, double voltage, TypeTestVoltage typTestvoltage)
        {
            var isConCarga = typeTestPoint == TypeTestPoint.ConCarga ? COMPUTERSMARTIII.TypeCarga.CARGA_MAXIMA : COMPUTERSMARTIII.TypeCarga.CARGA_MINIMA;

            var voltageAppply = voltage;
            if (voltageAppply > 300)
            {
                voltageAppply /= 2;
                tower.IO.DO.On(13);
            }else
                tower.IO.DO.Off(13);

            compSmartIII.WriteCargaMinimaMaxima(isConCarga);

            var configuration = new TestPointConfiguration()
            {
                Voltage = voltageAppply,
                Current = 0.9,
                Frecuency = 50,
                Iteraciones = 10,
                ResultName = voltage.ToString(),
                TiempoIntervalo = 1000,
                typePowerSource = TypePowerSource.CHROMA,
                typeTestExecute = TypeTestExecute.OnlyActive,
                typeTestPoint = typeTestPoint,
                typeTestVoltage = typTestvoltage,
                WaitTimeReadMeasure = 1000
            };

            this.TestConsumo(tower, configuration);

            var measure = new MagnitudToMeasure()
            {
                Frequency = freqEnum._50Hz,
                Magnitud = MagnitudsMultimeter.VoltDC,
                NumMuestras = 3,
                DigitosPrecision = 3,
                NPLC = 0
            };

            internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_28_VIN", 10, InputMuxEnum.IN9);

           // internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_30_V_5", 5, InputMuxEnum.IN10_AuxMedida9);

            internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_31_V_33", 3, InputMuxEnum.IN11);

            internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_32_VREF", 3, InputMuxEnum.IN12);

            internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_33_VREF2", 1, InputMuxEnum.IN13);

            internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_35_VRS_A", 10, InputMuxEnum.IN2);

            internalTestMeasureTestPoint(typeTestPoint, voltage, measure, "TP_34_VRS", 5, InputMuxEnum.IN3);
        }

        private void internalTestMeasureTestPoint(TypeTestPoint typeTestPoint, double voltage, MagnitudToMeasure measure, string testpoint, double rango, InputMuxEnum inMUx)
        {
            var StepTest = string.Format("{0}_{1}", typeTestPoint.ToString(), voltage.ToString());

            var consumption = new AdjustValueDef(Params.V_DC.Other(StepTest).TestPoint(testpoint).Name, 0, Params.V_DC.Other(StepTest).TestPoint(testpoint).Min(), Params.V_DC.Other(StepTest).TestPoint(testpoint).Max(), 0, 0, ParamUnidad.V);
            measure.Rango = rango;

            TestMeasureBase(consumption, (s) =>
            {
                return tower.MeasureMultimeter(inMUx, measure, 1000);
            }, 0, 5, 500);
        }

        //**********************************************************




    }
}
