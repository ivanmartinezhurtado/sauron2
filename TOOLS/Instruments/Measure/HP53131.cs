﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Instruments.Utility;
using Ivi.Visa.Interop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.05)]
    public class HP53131A : MeasureBase<HP53131A>, IDisposable, IConsoleDevice
    {
        public enum PrivateCommands
        {
            SELEC_VARIABLE = Commands.USER_DEFINED_COMMANDS + 1,
            CONFIG_TRIGGER = Commands.USER_DEFINED_COMMANDS + 2,
            CONFIG_IMPEDANCE = Commands.USER_DEFINED_COMMANDS + 3,
            CONFIG_GATE = Commands.USER_DEFINED_COMMANDS + 4,
            CONFIG_STATICS = Commands.USER_DEFINED_COMMANDS + 5,
            COMPATIBILITY_MODE = Commands.USER_DEFINED_COMMANDS + 6,
        };

        public enum Variables
        {
            FREQ,
            PER,
            TINT,
            TOT,
            PHASE,
            DCYCLE,
            VOLT_MIN,
            VOLT_MAX,
            RISE_TIME,
            FALL_TIME,
            PWID,
            NWID
        }

        public enum Channels
        {
            CH1 = 1,
            CH2 = 2,
        }

        private FormattedIO488 _Gpib;
        private ResourceManager mgr;
        private byte _adrr = 2;
        private HP53131SCPI SCPIContext;
        private Gate gate;
        private Trigger triggerCH1;
        private Impedancia impedanciaCH1;
        private Trigger triggerCH2;
        private Impedancia impedanciaCH2;
        private Statistics statistics;
        private string idn = "";
        private bool _IsOpen = false;

        public class Trigger
        {
            private string[] TipoSlopeDesc = { "POS", "NEG" };
            public enum SlopeEnum
            {
                POS = 0,
                NEG = 1,
            };

            public enum SensibilitiEnum
            {
                HI = 0,
                MED = 50,
                LOW = 100,
            };

            public enum LevelEnum
            {
                AUTO_ON = 0,
                NUM_V_OFF = 1,
                NUM_PCT_OFF = 2,
            };


            public LevelEnum Level { get; set; }
            public SensibilitiEnum Hysterisis { get; set; }
            public SlopeEnum Slope { get; set; }
            public int Channel { get; set; }
            public double LevelValue { get; set; }

            public override string ToString()
            {
                 CultureInfo enUS = new CultureInfo("en-US");

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(":EVEN{0}", (int)Channel);

                if (Level == Trigger.LevelEnum.NUM_V_OFF)
                    sb.AppendFormat(":LEV:AUTO OFF;:EVEN{0}:LEV {1}V;", (int)Channel, LevelValue.ToString(enUS));
                else if (Level == Trigger.LevelEnum.NUM_PCT_OFF)
                    sb.AppendFormat(":LEV:AUTO ON;:EVEN{0}:LEV:REL {1};", (int)Channel, LevelValue.ToString(enUS));
                else if (Level == Trigger.LevelEnum.AUTO_ON)
                    sb.AppendFormat(":LEV:AUTO ON;");

                sb.AppendFormat(":EVEN{0}:SLOP {1};", (int)Channel, TipoSlopeDesc[(int)Slope]);
                sb.AppendFormat(":EVEN{0}:HYST:REL {1};", (int)Channel, (int)Hysterisis /* TipoSens ¿?*/);

                return sb.ToString();
            }
        }

        public class Impedancia
        {
            public enum CoupEnum
            {
                AC = 0,
                DC = 1,
            };

            public enum AttenEnum
            {
                X1 = 0,
                X10 = 1,
            };

            public enum FilterEnum
            {
                Filt_ON = 0,
                Filt_OFF = 1,
            };

            public enum ImpedanciaEnum
            {
                OHM_50= 50,
                OHM_1M= 1000000,
            };

            public CoupEnum Acoplo { get; set; }
            public AttenEnum Atenuacion { get; set; }
            public FilterEnum Filtro { get; set; }
            public ImpedanciaEnum LevelImpedancia { get; set; }
            public int Channel { get; set; }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat(":INP{0}:IMP {1};", Channel, (int)LevelImpedancia);
                sb.AppendFormat(":INP{0}:COUP {1};", Channel, (int)Acoplo == 0 ? "AC" : "DC");
                sb.AppendFormat(":INP{0}:ATT {1};", Channel, (int)Atenuacion * 10);
                sb.AppendFormat(":INP{0}:FILT {1};", Channel, (int)Filtro == 0 ? "ON" : "OFF");

                return sb.ToString();
            }
        }

        public class Gate
        {
            private string[] FuncionGateDesc = { "FREQ", "PER", "PHAS", "TOT", "TINT", "NWIDTH", "PWIDTH", "DCYCLE", "PTPEAK" };
            private string[] TipoGateStartDesc = { "IMM", "EXT" };
            private string[] TipoGateStopDesc = { "IMM", "TIM", "DIG", "EXT" };
            private string[] TipoSlopeDesc = { "POS", "NEG" };
           
            public enum FuncionGateEnum
            {
                FREQ = 0,
                PERIOD = 1,
                PHASE = 2,
                TOTALIZE = 3,
                TINT = 4,
                NWIDTH = 5,
                PWIDTH = 6,
                DCYCLE = 7,
                PTPEAK = 8,
            };

            public enum ModoGateStartEnum
            {
                AutoStart = 0,
                ExtStart = 1
            };

            public enum ModoGateStopEnum
            {
                AutoStop = 0,
                G_Time = 1,
                Digits = 2,
                ExtStop = 3
            };

            public enum SlopeEnum
            {
                POS = 0,
                NEG = 1,
            };

            public FuncionGateEnum FuncionGate { get; set; }
            public ModoGateStopEnum ModoGateStop { get; set; }
            public ModoGateStartEnum ModoGateStart { get; set; }
            public SlopeEnum SlopeStart { get; set; }
            public SlopeEnum SlopeStop { get; set; }
            public int NumValue { get; set; }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat(":{0}:ARM:SOUR {1};", FuncionGateDesc[(int)FuncionGate], TipoGateStartDesc[(int)ModoGateStart]);
                if (ModoGateStart == ModoGateStartEnum.ExtStart)
                    sb.AppendFormat(":{0}:ARM:SLOPE {1};", FuncionGateDesc[(int)FuncionGate], TipoSlopeDesc[(int)SlopeStart]);
                ;

                sb.AppendFormat(":{0}:ARM:STOP:SOUR {1};", FuncionGateDesc[(int)FuncionGate], TipoGateStopDesc[(int)ModoGateStop]);
                if (ModoGateStop == ModoGateStopEnum.ExtStop)
                    sb.AppendFormat(":{0}:ARM:STOP:SLOPE {1};", FuncionGateDesc[(int)FuncionGate], TipoSlopeDesc[(int)SlopeStop]);
                else if (ModoGateStop == ModoGateStopEnum.Digits)
                    sb.AppendFormat(":{0}:ARM:STOP:DIG {1};", FuncionGateDesc[(int)FuncionGate], NumValue);
                else if (ModoGateStop == ModoGateStopEnum.G_Time)
                    sb.AppendFormat(":{0}:ARM:STOP:TIM {1};", FuncionGateDesc[(int)FuncionGate], NumValue);
                ;

                return sb.ToString();
            }
        }

        public class Statistics
        {
            private string[] KindMeasDesc = {"SDEV", "MEAN", "MIN", "MAX" };

            public enum KindMeasShowEnum
            {
                Data_SDEV = 0,
                Data_MEAN = 1,
                Data_MIN = 2,
                Data_MAX = 3,
            };

            public KindMeasShowEnum KindMeasDisplayShow { get; set; }
            public int NumSamples { get; set; }
            public bool OnSingle { get; set; }
            public bool UseAllMeas { get; set; }
            
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat(":DISP:TEXT:FEED 'CALC3';");
                sb.AppendFormat(":CALC3:AVER:TYPE {0};", KindMeasDesc[(int)KindMeasDisplayShow]);
                sb.AppendFormat(":CALC3:AVER ON;");
                sb.AppendFormat(":CALC3:AVER:COUNT {0};", NumSamples);

                if (UseAllMeas == true)
                    sb.AppendFormat(":CALC3:LFIL:STAT OFF;");
                else
                    sb.AppendFormat(":CALC3:LFIL:STAT ON;");
                ;
              
                if (OnSingle == true)
                    sb.AppendFormat(":TRIG:COUN:AUTO OFF;");
                else
                    sb.AppendFormat(":TRIG:COUN:AUTO ON;");
                ;

                return sb.ToString();
            }
        }

        private readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Variables.FREQ, "FREQ" },
            { (int)Variables.TOT, "TOT" },   
            { (int)Variables.PHASE, "PHAS" }, 
            { (int)Variables.DCYCLE, "DCYC" }, 
            { (int)Variables.VOLT_MIN, "VOLT:MIN" }, 
            { (int)Variables.VOLT_MAX, "VOLT:MAX" }, 
            { (int)Variables.TINT, "TINT" },  
            { (int)Variables.PER, "PER" },
            { (int)Variables.RISE_TIME, "RISE:TIME" },
            { (int)Variables.FALL_TIME, "FALL:TIME" }, 
            { (int)Variables.PWID, "PWID" },
            { (int)Variables.NWID, "NWID" }
        };

        public HP53131A()
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinRx = "\n", CaracterFinTx = "\n" };
            transport = new MessageTransport(protocol, new GpibPortAdapter(_Gpib), _logger);
            mgr = new ResourceManager();
            SCPIContext = new HP53131SCPI(this);
        }

        public byte PortAdress
        {
            get { return _adrr; }
            set
            {
                Close();
                _adrr = value;
                Open();
            }
        }

        public double AverageMax { get; internal set;}
        public double AverageMin { get; internal set; }
        public double AverageMean { get; internal set; }
        public double AverageSdev { get; internal set; }

        private void InitializeComponent()
        {
            if (_Gpib == null)
                _Gpib = new FormattedIO488();
        }

        private void Close()
        {
            if ((_IsOpen == true) && (_Gpib.IO != null)) 
                _Gpib.IO.Close();
            _IsOpen = false;
        }

        private void Open()
        {
            _logger.Info("Openning HP53131");
            if ((_IsOpen == true) && (_Gpib.IO != null))
                _Gpib.IO.Close();
            _Gpib.IO = (IMessage)mgr.Open("GPIB0::" + _adrr.ToString(), AccessMode.NO_LOCK, 2000, "");
            _Gpib.IO.TerminationCharacterEnabled = true;
            _IsOpen = true;
            _logger.Info("HP53131 Opened");

            var id = GetIdentification();
            if (id.Contains("MODEL=53220A"))
            {
                _logger.Info("Envio modo compatibilidad para 53131A");
                CompatibilityMode("53131A");
            }
        }

        private void ClearBusError()
        {
            //  Send a device clear
            _Gpib.IO.Clear();
            // Clear status
            SCPIContext.Clear.Command();
        }

        public void ConfigVariableToMeas(Variables VariableToMeas, Channels channel= Channels.CH1)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SELEC_VARIABLE, true, VariableToMeas, channel));
        }

        public HP53131A PresetConfigImpendance(Channels channel, Impedancia.CoupEnum acoplo, Impedancia.AttenEnum atenuacion, Impedancia.FilterEnum filtro, Impedancia.ImpedanciaEnum levelImpedancia)
        {
            if (channel == Channels.CH1)
                impedanciaCH1 = new Impedancia
                {
                    Channel = (int)channel,
                    Acoplo = acoplo,
                    Atenuacion = atenuacion,
                    Filtro = filtro,
                    LevelImpedancia = levelImpedancia
                };
            else
                impedanciaCH2 = new Impedancia
                {
                    Channel = (int)channel,
                    Acoplo = acoplo,
                    Atenuacion = atenuacion,
                    Filtro = filtro,
                    LevelImpedancia = levelImpedancia
                };

            return this;
        }

        private void ConfigImpendance(Impedancia impendance)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.CONFIG_IMPEDANCE, true, impendance));
        }

        public HP53131A PresetAndConfigImpendance(Channels channel, Impedancia.CoupEnum acoplo, Impedancia.AttenEnum atenuacion, Impedancia.FilterEnum filtro, Impedancia.ImpedanciaEnum levelImpedancia)
        {
            PresetConfigImpendance(channel, acoplo, atenuacion, filtro, levelImpedancia);
            
            if (channel == Channels.CH1)
                ConfigImpendance(impedanciaCH1);
            else
                ConfigImpendance(impedanciaCH2);

            return this;
        }

        public HP53131A PresetConfigTrigger(Channels channel, Trigger.LevelEnum KindLevel = HP53131A.Trigger.LevelEnum.AUTO_ON, Trigger.SlopeEnum Slope = Trigger.SlopeEnum.POS, Trigger.SensibilitiEnum Hysteresis = Trigger.SensibilitiEnum.MED, double LevelValue = 0)
        {
            if (channel == Channels.CH1)
                triggerCH1 = new Trigger
                {
                    Channel = (int)channel,
                    Level = KindLevel,
                    Slope = Slope,
                    Hysterisis = Hysteresis,
                    LevelValue = LevelValue
                };
            else
                triggerCH2 = new Trigger
                {
                    Channel = (int)channel,
                    Level = KindLevel,
                    Slope = Slope,
                    Hysterisis = Hysteresis,
                    LevelValue = LevelValue
                };

            return this;
        }

        private void ConfigTrigger(Trigger trigger)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.CONFIG_TRIGGER, true, trigger));
        }

        private void CompatibilityMode(string modelToEmulate)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.COMPATIBILITY_MODE, true, modelToEmulate));
        }

        public void PresetAndConfigTrigger(Channels channel, Trigger.LevelEnum KindLevel = HP53131A.Trigger.LevelEnum.AUTO_ON, Trigger.SlopeEnum Slope = Trigger.SlopeEnum.POS, Trigger.SensibilitiEnum Hysteresis = Trigger.SensibilitiEnum.MED, double LevelValue = 0)
        {
            PresetConfigTrigger(channel, KindLevel, Slope, Hysteresis, LevelValue);
            if (channel == Channels.CH1)
                ConfigTrigger(triggerCH1);
            else
                ConfigTrigger(triggerCH2);
        }

        public void PresetConfigGate(Gate.FuncionGateEnum funcionGate, Gate.ModoGateStartEnum ModeGateStart, Gate.SlopeEnum slopeStart, Gate.ModoGateStopEnum ModeGateStop, Gate.SlopeEnum slopeStop, int LevelValue = 0)
        {
            gate = new Gate
            {
                FuncionGate = funcionGate,
                ModoGateStart = ModeGateStart,
                ModoGateStop = ModeGateStop,
                NumValue = LevelValue,
                SlopeStart = slopeStart,
                SlopeStop = slopeStop
            };
        }

        private void ConfigGate(Gate gate)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.CONFIG_GATE, true, gate));
        }

        public void PresetAndConfigGate(Gate.FuncionGateEnum funcionGate, Gate.ModoGateStartEnum ModeGateStart, Gate.SlopeEnum slopeStart, Gate.ModoGateStopEnum ModeGateStop, Gate.SlopeEnum slopeStop, int LevelValue = 0)
        {
            PresetConfigGate(funcionGate,ModeGateStart, slopeStart, ModeGateStop, slopeStop, LevelValue);
            ConfigGate(gate);
        }

        public void PresetConfigStatistics(Statistics.KindMeasShowEnum KindShowMeas, int numSamples, bool useAllMeas, bool onSingle)
        {
            statistics = new Statistics
            {
                KindMeasDisplayShow = KindShowMeas,
                NumSamples = numSamples,
                OnSingle = onSingle,
                UseAllMeas = useAllMeas,
            };
        }

        private void ConfigStatistics(Statistics statics)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.CONFIG_STATICS, true, statics));
        }

        public void PresetAndConfigStatistics(Statistics.KindMeasShowEnum KindShowMeas, int numSamples, bool useAllMeas, bool onSingle)
        {
            PresetConfigStatistics(KindShowMeas, numSamples, useAllMeas, onSingle);
            ConfigStatistics(statistics);
        }

        public Result<double> RunMeasure(Variables VariableToMeas, int TimeOutMeasure = 8000, Channels channel = Channels.CH1)
        {
            return RunMeasure(VariableToMeas, TimeOutMeasure, channel, null);
        }

        [Browsable(false)]
        public Result<double> RunMeasure(Variables VariableToMeas, int TimeOutMeasure, Channels channel, Action PostConfigurationStarted)
        {
            AverageMax = 0;
            AverageMin = 0;
            AverageMean = 0;
            AverageSdev = 0;

            // Inicialización
            SCPIContext.Reset.Command();
            SCPIContext.Clear.Command();
            SCPIContext.ClearSRE.Command();
            SCPIContext.ClearESE.Command();

            //Selec Variable a medir
            ConfigVariableToMeas(VariableToMeas, channel);

            //TODO enviar configuraciones de Gate y Tirgger e Impedancia si las hay
            if (gate != null) 
                ConfigGate(gate);
            
            if (triggerCH1 != null) 
                ConfigTrigger(triggerCH1);
            
            if (impedanciaCH1 != null) 
                ConfigImpendance(impedanciaCH1);
            
            if (triggerCH2 != null) 
                ConfigTrigger(triggerCH2);
            
            if (impedanciaCH2 != null) 
                ConfigImpendance(impedanciaCH2);
            
            if (statistics != null) 
                ConfigStatistics(statistics);

            // Set a TimeOutMeasure minute measurement time. This is in case there is
            // no input applied to the counter; your time may be different.
            int Delay = TimeOutMeasure;   // delay in milliseconds
            transport.ReadTimeout = Delay;

            // Set bit 0 in standard event status register
            SCPIContext.OperationComplete.Command();

            // Start a measurement
            SCPIContext.INITIATE.Command();

            if (PostConfigurationStarted != null)
                PostConfigurationStarted();

            try
            {
                // Read status register
                var result = SCPIContext.OperationCompleteQuery.Query();

            if (result.Error)
                throw result.Exception;

            if (result.Value != "1")
                throw new Exception("NO hay valor pendeinte de leer despues de OPC");
  
            if (statistics != null)
            {
                AverageMax = SCPIContext.AVERAGEMAX.QueryNumber().Value;
                AverageMin = SCPIContext.AVERAGEMIN.QueryNumber().Value;
                AverageMean = SCPIContext.AVERAGEMEAN.QueryNumber().Value;
                AverageSdev = SCPIContext.AVERAGESDEV.QueryNumber().Value;
            }
           
               var measure = SCPIContext.FETCh.QueryNumber();

                if (measure.Error)
                    throw measure.Exception;

                return measure;

            }catch(TimeoutException ex) 
            {
                ClearBusError();
                TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(ex.Message).Throw();
            }
            catch(COMException ex)
            {
                ClearBusError();
                TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(ex.Message).Throw();
            }

            return null;
        }

        public void InitiateMeasure(Variables VariableToMeas = Variables.FREQ, Channels channel = Channels.CH1)
        {
            AverageMax = 0;
            AverageMin = 0;
            AverageMean = 0;
            AverageSdev = 0;

            // Inicialización
            SCPIContext.Reset.Command();
            SCPIContext.Clear.Command();
            SCPIContext.ClearSRE.Command();
            SCPIContext.ClearESE.Command();

            //Selec Variable a medir
            ConfigVariableToMeas(VariableToMeas, channel);
        }

        public void StartMeasure (int TimeOutMeasure)
        {
            // Set a TimeOutMeasure minute measurement time. This is in case there is
            // no input applied to the counter; your time may be different.
            transport.ReadTimeout = TimeOutMeasure;

            // Set bit 0 in standard event status register
            SCPIContext.OperationComplete.Command();

            // Start a measurement
            SCPIContext.INITIATE.Command();
        }

        public double ResultMeasure()
        {
            // Read status register
            var result = SCPIContext.OperationCompleteQuery.Query();

            if (result.Error)
                throw result.Exception;

            if (result.Value != "1")
                throw new Exception("NO hay valor pendeinte de leer despues de OPC");

            var measure = SCPIContext.FETCh.QueryNumber();

            if (measure.Error)
                throw measure.Exception;

            return measure.Value;
        }

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));
            return idn;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            try
            {

                Result<string> response;

                if (!_IsOpen)
                    Open();

                switch (command.Commands)
                {
                    case (int)PrivateCommands.CONFIG_TRIGGER:
                    case (int)PrivateCommands.CONFIG_IMPEDANCE:
                    case (int)PrivateCommands.CONFIG_GATE:
                    case (int)PrivateCommands.CONFIG_STATICS:
                        response = WriteCommand(command.Args[0].ToString());   // Crida al ToString del Trigger que composa els SCPI
                        break;

                    case (int)PrivateCommands.SELEC_VARIABLE:
                        int variable = (int)command.Args[0];
                        int channel = (int)command.Args[1];
                        string trama;
                        if (variable == (int)Variables.TINT)
                            trama = string.Format("FUNC \"{0} 1,2\"", messages[variable]);
                        else
                            trama = string.Format("FUNC \"{0} {1}\"", messages[variable], channel);
                        response = WriteCommand(trama);
                        break;

                    case (int)Commands.READ_IDN:
                        response = WriteCommand("*IDN?");
                        var resp = response.Value.Split(',');
                        idn = string.Format("{0}, MODEL={1}, SN={2}", resp[0], resp[1], resp[3]);
                        break;

                    case (int)PrivateCommands.COMPATIBILITY_MODE:
                        response = WriteCommand($"SYSTem:LANGuage \"{command.Args[0]}\"");
                        break;

                    default:
                        throw new NotSupportedException();
                }
            }
            catch (TimeoutException ex)
            {
                ClearBusError();
                TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(ex.Message).Throw();
            }
            catch (COMException ex)
            {
                ClearBusError();
                TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(ex.Message).Throw();
            }
        }

        public void Dispose()
        {
            Close();
        }

        [Browsable(false)]
        public Result<string> WriteCommand(string text)
        {
            try
            {

                if (!_IsOpen)
                    Open();

                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                response = transport.SendMessage<SCPIMessage>(new SCPIMessage { Text = "*ESR?" });
                int result = Convert.ToInt32(response.Text);
                if (result > 0)
                    return ReadError();

                return string.Empty;
            }
            catch (TimeoutException ex)
            {
                ClearBusError();
                TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(ex.Message).Throw();
                return null;
            }
            catch (COMException ex)
            {
                ClearBusError();
                TestException.Create().HARDWARE.INSTRUMENTOS.MEDIDA_INSTRUMENTO(ex.Message).Throw();
                return null;
            }
        }

        private Result<string> ReadError()
        {
            var message = new StringMessage { Text = "SYSTem:ERRor?" };
            string text = string.Empty;
            int code;

            var response = transport.SendMessage<StringMessage>(message);
            string result = response.Text;
            code = Convert.ToInt32(result.Substring(0, result.IndexOf(',')));
            if (code < 0 || text == string.Empty)
                text += result + Environment.NewLine;

            return new Result<string>(text);
        }

         public HP53131SCPI SCPI { get { return SCPIContext; } }

    }
}
