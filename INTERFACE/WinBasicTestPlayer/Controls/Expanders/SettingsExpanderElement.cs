﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.Layouts;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Drawing;
using System.Windows.Forms;


namespace WinTestPlayer.Controls.Expanders
{
    public class SettingsExpanderElement : ExpanderElement
    {
        StackLayoutPanel settingsButtonsContainer;

        RadToggleButtonElement settingsButton;
        LightVisualElement settingsButtonImage;

        RadToggleButtonElement themesButton;
        LightVisualElement themeButtonImage;

        RadHostItem themesAndSettingsHost;
        RadPanel themesAndSettingsContainer;

        public SettingsExpanderElement()
        {
            settingsButton.BorderElement.Visibility = ElementVisibility.Collapsed;
            settingsButton.ButtonFillElement.Visibility = ElementVisibility.Hidden;
            settingsButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

            themesButton.BorderElement.Visibility = ElementVisibility.Collapsed;
            themesButton.ButtonFillElement.Visibility = ElementVisibility.Hidden;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            this.staticColumn.MinSize = new System.Drawing.Size(40, 0);
            this.staticColumn.MaxSize = new System.Drawing.Size(40, 0);

            this.settingsButtonsContainer = new StackLayoutPanel();
            this.settingsButtonsContainer.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.chevronAndContentHolder.Children.Add(settingsButtonsContainer);

            settingsButton = new RadToggleButtonElement();
            settingsButton.Name = "settingsButton";
            settingsButton.ImagePrimitive.StretchHorizontally = true;
            settingsButton.ImagePrimitive.StretchVertically = true;
            settingsButton.ImagePrimitive.Margin = new System.Windows.Forms.Padding(0, -1, 0, -1);

            settingsButton.ToggleStateChanged += new StateChangedEventHandler(settingsButton_ToggleStateChanged);
            settingsButton.ToggleStateChanging += new StateChangingEventHandler(settingsButton_ToggleStateChanging);

            settingsButtonImage = new LightVisualElement();
            settingsButtonImage.StretchHorizontally = true;
            settingsButtonImage.StretchVertically = true;
            settingsButtonImage.ZIndex = 10;
            settingsButtonImage.ShouldHandleMouseInput = false;

            settingsButton.Children.Add(settingsButtonImage);

            this.settingsButtonsContainer.Children.Add(settingsButton);

            themesButton = new RadToggleButtonElement();
            themesButton.Name = "themesButton";
            themesButton.ImagePrimitive.StretchHorizontally = true;
            themesButton.ImagePrimitive.StretchVertically = true;
            themesButton.ImagePrimitive.Margin = new System.Windows.Forms.Padding(0, -1, 0, -1);

            themesButton.ToggleStateChanged += new StateChangedEventHandler(themesButton_ToggleStateChanged);
            themesButton.ToggleStateChanging += new StateChangingEventHandler(themesButton_ToggleStateChanging);

            themeButtonImage = new LightVisualElement();
            themeButtonImage.StretchHorizontally = true;
            themeButtonImage.StretchVertically = true;
            themeButtonImage.ZIndex = 10;
            themeButtonImage.ShouldHandleMouseInput = false;

            themesButton.Children.Add(themeButtonImage);
           
            this.settingsButtonsContainer.Children.Add(themesButton);

            // Only this way we can avoid the fact that controls can't be clipped
            themesAndSettingsContainer = new RadPanel();
            themesAndSettingsContainer.PanelElement.PanelFill.Visibility = ElementVisibility.Collapsed;
            themesAndSettingsContainer.PanelElement.PanelBorder.Width = 0;
            themesAndSettingsContainer.ControlAdded += new ControlEventHandler(settingPanel_ControlAdded);

            themesAndSettingsHost = new RadHostItem(themesAndSettingsContainer);
            slidingColumn.Children.Add(themesAndSettingsHost);

            DockLayoutPanel parent = (DockLayoutPanel)slidingColumn.Parent;
            parent.Children.Remove(slidingColumn);

            RadControl ctrl = new RadControl();
            ctrl.RootElement.Children.Add(slidingColumn);

            RadHostItem h = new RadHostItem(ctrl);
            parent.Children.Add(h);

            DockLayoutPanel.SetDock(h, Dock.Left);
        }

        void settingsButton_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            if ((sender as RadToggleButtonElement).Name == "settingsButton" && args.NewValue == Telerik.WinControls.Enumerations.ToggleState.Off && themesButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
            {
                args.Cancel = true;
            }
        }

        void themesButton_ToggleStateChanging(object sender, StateChangingEventArgs args)
        {
            if ((sender as RadToggleButtonElement).Name == "themesButton" && args.NewValue == Telerik.WinControls.Enumerations.ToggleState.Off && settingsButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off) 
            {
                args.Cancel = true;
            }
        }

        void settingsButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {

            if (settingsButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
            {
                settingsButton.ImagePrimitive.Image = null;
            }
            if (settingsButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                themesButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                if (settingsPanel != null)
                {
                    settingsPanel.BringToFront();
                }
            }
        }

        void themesButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (themesButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
            {
                themesButton.ImagePrimitive.Image = null;
            }
            if (themesButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                settingsButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                if (themesPanel != null)
                {
                    themesPanel.BringToFront();
                }
            }
        }

        RadPanel settingsPanel;
        RadPanel themesPanel;

        void settingPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            if (e.Control.Name == "settingsPanel")
            {
                settingsPanel = (RadPanel)e.Control;
            }
            else if (e.Control.Name == "themesPanel")
            {
                themesPanel = (RadPanel)e.Control;
            }
        }

        public RadPanel ThemesAndSettingsContainer
        {
            get
            {
                return themesAndSettingsContainer;
            }
        }

        public RadPanel ThemesPanel
        {
            get
            {
                return themesPanel;
            }
        }

        public RadPanel SettingsPanel
        {
            get
            {
                return settingsPanel;
            }
        }

        public RadToggleButtonElement SettingsButton
        {
            get 
            {
                return settingsButton; 
            }
        }
    }
}
