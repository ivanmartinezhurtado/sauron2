﻿using Dezac.Services;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using WinTestPlayer.Services;
using ViewModels = Dezac.Data.ViewModels;

namespace WinTestPlayer.Views
{
    public partial class HistorialView : UserControl
    {
        protected readonly AppService app;
        protected readonly ReportsService reportService;

        private List<ViewModels.DeviceDataView> list;
        private List<ViewModels.BoxesView> boxList;

        public HistorialView()
        {
            InitializeComponent();
        }

        public HistorialView(AppService app, ReportsService reportsService)
            : this()
        {
            this.app = app;
            reportService = reportsService;

            if (app.Model.NumOrden > 0)
            {
                txtOrden.Text = app.Model.NumOrden.ToString();
                UpdateUI();
            }

            DeleteTempFiles();
        }

        private ViewModels.DeviceDataView Current { get; set; }

        private void grid_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            var item = e.RowElement.RowInfo.DataBoundItem as ViewModels.DeviceDataView;
            if (item == null)
                return;

            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local);
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);
        }

        private async void UpdateUI()
        {
            grid.Enabled = false;
            boxGrid.Enabled = false;

            int numOrden;

            try
            {
                if (!string.IsNullOrEmpty(txtOrden.Text))
                {
                    numOrden = Convert.ToInt32(txtOrden.Text);
                    list = await Task.Run<List<ViewModels.DeviceDataView>>(() =>
                        {
                            using (var ds = new DezacService())
                            {
                                return ds.GetDeviceBox(numOrden);
                            }
                        });
                    boxList = await Task.Run<List<ViewModels.BoxesView>>(() =>
                    {
                        using (var ds = new DezacService())
                        {
                            return ds.GetBoxes(numOrden);
                        }
                    });
                }
                bsList.DataSource = list;
                bsBoxList.DataSource = boxList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cargar datos de la Orden. " + ex.Message, "DATOS ORDEN", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                grid.Enabled = true;
                boxGrid.Enabled = true;
            }
        }

        private void PrintTestResport(object sender, EventArgs e)
        {
            //Current = reportService.GetdatosByBastidor();
            //if (Current == null)
            //    return;
            //try
            //{
            //    bsList.DataSource = Current;

            //    reportService.PrintTestReport(app, Current);
            //}
            //catch (Exception ex)
            //{
            //    var view = DI.Resolve<ShellService>();
            //    var bastidor = view.MsgBox("ERROR AL IMPRIMIR EL REPORT", "Error al imprimir el Report por " + ex.Message);
            //    grid.Enabled = true;
            //}
        }

        private void Click_Filtrar(object sender, EventArgs e)
        {
            UpdateUI();
        }

        protected IShell Shell { get { return DI.Resolve<ShellService>(); } }

        private void btLabel_Click(object sender, EventArgs e)
        {
            try
            {
                int orden = Convert.ToInt32(!string.IsNullOrEmpty(txtOrden.Text) ? txtOrden.Text : "0");
                if (orden == 0)
                {
                    Shell.MsgBox("No se ha cargado ninguna orden de fabricacion", "Orden no seleccionada");
                    return;
                }

                List<ViewModels.DeviceDataView> devices = GetDeviceSelecteds();
                if (devices.Count == 0)
                {
                    Shell.MsgBox("No se ha seleccionado ningún equipo para imprimir su etiqueta", "EQUIPO NO SELECCIONADO");
                    return;
                }
                grid.Enabled = false;
                boxGrid.Enabled = false;
                LabelsPrint(devices);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al imprimir la etiqueta. " + ex.Message, "Print Test Label Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ParentForm.Enabled = true;
            }
            finally
            {
                grid.Enabled = true;
                boxGrid.Enabled = true;
            }
        }

        private void btLabelBox_Click(object sender, EventArgs e)
        {
            int orden = Convert.ToInt32(!string.IsNullOrEmpty(txtOrden.Text) ? txtOrden.Text : "0");
            if (orden == 0)
            {
                Shell.MsgBox("No se ha cargado ninguna orden de fabricacion", "Orden no seleccionada");
                return;
            }

            try
            {
                List<ViewModels.BoxesView> box = GetBoxSelecteds();
                if (box.Count == 0)
                {
                    Shell.MsgBox("No se ha seleccionado ninguna caja en la pestañas CAJAS", "EQUIPO NO SELECCIONADO");
                    return;
                }
                grid.Enabled = false;
                boxGrid.Enabled = false;
                LabelBoxPrint(box, orden);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al imprimir la etiqueta. " + ex.Message, "Print Test Label Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                grid.Enabled = true;
                boxGrid.Enabled = true;
            }
        }

        private void BtnPrintBastidor_Click(object sender, EventArgs e)
        {
            try
            {
                grid.Enabled = false;
                boxGrid.Enabled = false;
                PrintByBastidor();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al imprimir la etiqueta. " + ex.Message, "Print Test Label Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                grid.Enabled = true;
                boxGrid.Enabled = true;
            }

        }

        private void btLabelManual_Click(object sender, EventArgs e)
        {
            var control = new LabelTemplateView(app, reportService);
            var container = new ContainerControl();
            var frm = new Form();
            container.Dock = DockStyle.Fill;
            container.Controls.Add(control);
            frm.BackColor = SystemColors.Control;
            frm.Text = "Pint label editor";
            frm.Size = new Size(control.Width + 20, control.Height + 30);
            frm.Controls.Add(container);
            frm.Icon = ParentForm.Icon;
            frm.Show();
        }

        private void PrintByBastidor()
        {
            Int32.TryParse(txtBastidor.Text, out int bastidor);
            if (bastidor == 0)
            {
                MessageBox.Show("Error al imprimir la etiqueta. No se ha especificado ningún bastidor", "Print Test Label Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ReportsService.DatosEquipoToPrint Current = reportService.GetdatosByBastidor(false, bastidor);
            ReportsService.GetDatosBBDDToPrinLabel result = reportService.ConfigPrintLabels(app, Current);

            if (result.Context == null)
                foreach (string label in result.NumCodeLabel)
                    reportService.PrintLabelCurrentNumSerie(result.TestFase, label, result.NumCopiesLabel, app.Model.PrinterLabel);
            else
            {
                string labelDataFileName = result.TipoEtiqueta == "01" ? "PRINT_LABEL_Caracteristicas"
                              : result.TipoEtiqueta == "02" ? "PRINT_LABEL_EmbalajeIndividual"
                              : result.TipoEtiqueta == "03" ? "PRINT_LABEL_EmbalajeConjunto"
                              : result.TipoEtiqueta == "04" ? "PRINT_LABEL_NumSerie"
                              : result.TipoEtiqueta == "05" ? "PRINT_LABEL_EtiquetasEsquema"
                              : result.TipoEtiqueta == "06" ? "PRINT_LABEL_EtiquetaPromocional"
                              : "PRINT_LABEL_NumSerie";

                string labelDataFile = reportService.GetFileLabelData(bastidor, labelDataFileName);
                if (!string.IsNullOrEmpty(labelDataFile))
                    reportService.PrintLabelFromDataFile(result.Context, result.TipoEtiqueta, labelDataFile, result.FileName, result.NumCopiesLabel);
                else
                    reportService.GenerateLabelAndPrint(result.Context, result.TestFase, result.TipoEtiqueta, result.FileName, result.NumCopiesLabel);
            }
        }

        private void LabelsPrint(List<ViewModels.DeviceDataView> devices, string tipoEtiqueta = null)
        {
            foreach (ViewModels.DeviceDataView device in devices)
            {

                ReportsService.DatosEquipoToPrint Current = reportService.GetDatosByDeviceData(device);
                if (Current == null)
                    return;

                ReportsService.GetDatosBBDDToPrinLabel result = reportService.ConfigPrintLabels(app, Current, tipoEtiqueta);
                tipoEtiqueta = result.TipoEtiqueta;
                if (result.Context == null)
                    foreach (string label in result.NumCodeLabel)
                        reportService.PrintLabelCurrentNumSerie(result.TestFase, label, result.NumCopiesLabel, app.Model.PrinterLabel);
                else
                {
                    string labelDataFileName = result.TipoEtiqueta == "01" ? "PRINT_LABEL_Caracteristicas"
                                  : result.TipoEtiqueta == "02" ? "PRINT_LABEL_EmbalajeIndividual"
                                  : result.TipoEtiqueta == "03" ? "PRINT_LABEL_EmbalajeConjunto"
                                  : result.TipoEtiqueta == "04" ? "PRINT_LABEL_NumSerie"
                                  : result.TipoEtiqueta == "05" ? "PRINT_LABEL_EtiquetasEsquema"
                                  : result.TipoEtiqueta == "06" ? "PRINT_LABEL_EtiquetaPromocional"
                                  : "PRINT_LABEL_NumSerie";

                    string labelDataFile = reportService.GetFileLabelData(device.NUMMATRICULA, labelDataFileName);
                    if (!string.IsNullOrEmpty(labelDataFile))
                        reportService.PrintLabelFromDataFile(result.Context, result.TipoEtiqueta, labelDataFile, result.FileName, result.NumCopiesLabel);
                    else
                        reportService.GenerateLabelAndPrint(result.Context, result.TestFase, result.TipoEtiqueta, result.FileName, result.NumCopiesLabel);
                }
            }
        }

        private void LabelBoxPrint(List<ViewModels.BoxesView> boxes, int orden)
        {
            foreach (ViewModels.BoxesView box in boxes)
            {

                ReportsService.DatosEquipoToPrint Current = reportService.GetDatosByBoxData(box, orden);
                if (Current == null)
                    return;

                ReportsService.GetDatosBBDDToPrinLabel result = reportService.ConfigPrintLabels(app, Current, "03");

                string labelDataFile = reportService.GetFileLabelBoxData(box.NUMEROCAJA);
                if (!string.IsNullOrEmpty(labelDataFile))
                    reportService.PrintLabelFromDataFile(result.Context, result.TipoEtiqueta, labelDataFile, result.FileName, result.NumCopiesLabel);
                else
                    reportService.GenerateLabelAndPrint(result.Context, result.TestFase, result.TipoEtiqueta, result.FileName, result.NumCopiesLabel);
            }
        }

        private void DeleteTempFiles()
        {
            string url = ConfigurationManager.AppSettings["PathFileLabelTemplate"];

            if (Directory.Exists(url))
                Directory.Delete(url, true);

            Thread.Sleep(1000);

            if (!Directory.Exists(url))
                Directory.CreateDirectory(url);
        }

        private void txtOrden_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
                UpdateUI();
        }

        private void txtBastidor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
                PrintByBastidor();
        }

        private List<ViewModels.DeviceDataView> GetDeviceSelecteds()
        {
            var selectedRowsList = new List<ViewModels.DeviceDataView>();

            foreach (GridViewRowInfo row in grid.Rows)
            {
                object rowIsChecked = row.Cells["CHECK"].Value ?? false;
                if ((bool)rowIsChecked)
                    selectedRowsList.Add((ViewModels.DeviceDataView)row.DataBoundItem);
            }

            return selectedRowsList;
        }

        private List<ViewModels.BoxesView> GetBoxSelecteds()
        {
            var selectedRowsList = new List<ViewModels.BoxesView>();

            foreach (GridViewRowInfo row in boxGrid.Rows)
            {
                object rowIsChecked = row.Cells["CHECK"].Value ?? false;
                if ((bool)rowIsChecked)
                    selectedRowsList.Add((ViewModels.BoxesView)row.DataBoundItem);
            }

            return selectedRowsList;
        }

        private void BoxGrid_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
                boxGrid.Rows[e.RowIndex].Cells["CHECK"].Value = boxGrid.Rows[e.RowIndex].Cells["CHECK"].Value == null ? true : !(bool)boxGrid.Rows[e.RowIndex].Cells["CHECK"].Value;
        }

        private void Grid_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
                grid.Rows[e.RowIndex].Cells["CHECK"].Value = grid.Rows[e.RowIndex].Cells["CHECK"].Value == null ? true : !(bool)grid.Rows[e.RowIndex].Cells["CHECK"].Value;
        }

        private void RadPageView1_SelectedPageChanged(object sender, EventArgs e)
        {
            var rp = sender as RadPageView;

            if (rp.SelectedPage.Text == "Cajas")
            {
                btnOrdenPrint.Enabled = false;
                btnOrdenPrint.VisibleInStrip = false;
                btnCaja.Enabled = true;
                btnCaja.VisibleInStrip = true;
            }
            else if (rp.SelectedPage.Text == "Equipos")
            {
                btnOrdenPrint.Enabled = true;
                btnOrdenPrint.VisibleInStrip = true;
                btnCaja.Enabled = false;
                btnCaja.VisibleInStrip = false;
            }

        }

        private void Grid_SelectionChanged(object sender, EventArgs e)
        {
            foreach (GridViewRowInfo row in grid.Rows)
                row.Cells["CHECK"].Value = row.IsSelected;
        }

        private void BoxGrid_SelectionChanged(object sender, EventArgs e)
        {
            foreach (GridViewRowInfo row in boxGrid.Rows)
                row.Cells["CHECK"].Value = row.IsSelected;
        }
    }
}
