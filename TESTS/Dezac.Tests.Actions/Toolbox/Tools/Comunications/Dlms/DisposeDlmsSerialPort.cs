﻿using Dezac.Core.Utility;
using Dezac.DLMS;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(DisposeDlmsSerialPortDescription))]
    public class DisposeDlmsSerialPort : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("DlmsDevice")]
        public string DeviceName { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "DlmsDevice";
            var uut = GetVariable(name) as DLMSDeviceSerialPort;

            if (uut != null)
                uut.Disconnect();
        }
    }

    public class DisposeDlmsSerialPortDescription : ActionDescription
    {
        public override string Name { get { return "DisposeDlmsSerialPort"; } }
        public override string Description { get { return "Cierra el puerto Dlms serie del objeto especificado"; } }
        public override string Category { get { return "Tools/Comunications/DLMS"; } }
        public override Image Icon { get { return Properties.Resources.Logo_DLMS; } }
        public override string Dependencies { get { return "CreateDlmsSerialPort"; } }
    }
}
