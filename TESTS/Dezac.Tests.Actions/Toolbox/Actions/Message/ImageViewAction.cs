﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.UserControls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TaskRunner.Model;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.09)]
    [DesignerAction(Type = typeof(ImageViewActionDescription))]
    public class ImageViewAction : ActionBase
    {
        protected ConfigurationSamplerWithCancel configuration = new ConfigurationSamplerWithCancel();

        [Category("3.1. Dialog")]
        [Description("Título que ha de mostrar sobre la imagen")]
        [DefaultValue("PREGUNTA AL USUARIO MOSTRANDO UNA IMAGEN")]
        public string Title { get; set; }

        [Category("3.1. Dialog")]
        [Description("Conjunto de botones del cuadro de diálogo")]
        [DefaultValue(EnumBoxButtons.OKCancel)]
        public EnumBoxButtons? Buttons { get; set; }

        [Category("3.2. Image")]
        [Description("La imagen a mostrar, con \"$\" se puede cargar la imagen desde BBDD o desde una variable de test")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ImagePath{ get; set; }

        [Category("3.2. Image")]
        [Description("Descripción a mostrar bajo la imagen")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ImageDescription { get; set; }

        [Category("3.4. Exception")]
        [Description("El texto de excepción en caso de fallar la parte de Resultado")]
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ExceptionText { get; set; }

        [Category("3.3. Control Logic")]
        [Description("Nombre de la acción para el mensaje de cambio de imagen")]
        public string ActionName { get; set; }

        [Category("3.3. Control Logic")]
        [Description("Diálogo esperado como resultado")]
        [DefaultValue(DialogResult.OK)]
        public DialogResult DialogResult { get; set; } 

        [Category("3.3. Control Logic")]
        [Description("Método que llamaremos para leer el valor del dato a validar")]
        public RunMethodAction ReadMethodAction { get; set; }

        [Category("3.3. Control Logic")]
        [Description("Descripción del nombre del dato a validar para cerrar el formulario")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataInputName { get; set; }

        [Category("3.3. Control Logic")]
        [Description("Valor del dato a validar para cerrar el formulario")]
        public string DataInput { get; set; }

        [Category("3.3. Control Logic")]
        [Description("Valor Máximo del dato a validar para cerrar el formulario")]
        public string DataInputMax { get; set; }

        [Category("3.3. Control Logic")]
        [Description("Valor Mínimo del dato a validar para cerrar el formulario")]
        public string DataInputMin { get; set; }

        [Category("3. Configuration")]
        [Description("Parámetros de configuración de agrupación de la imagen")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ConfigurationSamplerWithCancel Configuration { get { return configuration; } set { configuration = value; } }

        [Description("Guardado como OK o ERROR el diálogo resultado")]
        [Category("4. Results")]
        public bool SaveResults { get; set; }

        [Description("Nombre para el resultado si el SaveResults es correcto")]
        [Category("4. Results")]
        public string TestPoint { get; set; }

        [Description("Opción para guardar el valor y los márgenes de el RunMethodAction ejecutados")]
        [Category("4. Results")]
        public bool AddToResults { get; set; }

        public enum EnumBoxButtons
        {
            OK = 0,
            OKCancel = 1,
            AbortRetryIgnore = 2,
            YesNoCancel = 3,
            YesNo = 4,
            RetryCancel = 5,
            Nothing = 10,
       }
     
        public override void Execute()
        {
            var context = SequenceContext.Current;
            if (context == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("No se ha inicializado el contexto").Throw();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            Image image = null;

            if (string.IsNullOrEmpty(ImagePath))
                image = Properties.Resources.Chat_icon;
            else if (ImagePath.Trim().StartsWith("$"))
            {
                var stream = VariablesTools.ResolveValue(ImagePath);
                if (stream?.GetType() == typeof(byte[]))
                {
                    using (MemoryStream ms = new MemoryStream(stream as byte[]))
                    {
                        image = Image.FromStream(ms);
                    }
                }
                else if (stream?.GetType() == typeof(Image))
                    image = stream as Image;
                else
                    Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("La variable de Image Path no es una imagen o array de bytes");
            }
            else
                image = Image.FromFile(ImagePath);

            if (Configuration.IterationNumber == 0)
            {
                Configuration.IterationNumber = 50;
                Configuration.DelayBetweenIteration = 1000;
                Configuration.CancelIfError = false;
                Configuration.ThrowException = true;
            }

            MessageBoxButtons? buttons = null;

            if (Buttons.HasValue == true && Buttons != EnumBoxButtons.Nothing)
                buttons = (MessageBoxButtons)Buttons;

            var frm = testBase.ShowForm(() => new ImageView(VariablesTools.ResolveText(Title), image), buttons, ImageDescription);
            try
            {
                if (ReadMethodAction != null)
                {
                    testBase.SamplerWithCancelWhitOutCancelToken(() =>
                    {
                        ReadMethodAction?.Execute();

                        if (!string.IsNullOrEmpty(DataInputName) || (!string.IsNullOrEmpty(DataInputMax) && !string.IsNullOrEmpty(DataInputMin)))
                        {
                            var value = VariablesTools.ResolveValue(DataInputName).ToString();

                            if (!string.IsNullOrEmpty(DataInput))
                            {
                                var varValue = VariablesTools.ResolveValue(DataInput).ToString();
                                Logger.InfoFormat("{0} -> {1}  == {2}", DataInputName, value, varValue);

                                if (AddToResults)
                                    testBase.Resultado.Set(ReadMethodAction.VariableName + TestPoint, varValue, ParamUnidad.SinUnidad, varValue);

                                if (value == varValue)
                                {
                                    frm.DialogResult = DialogResult.OK;
                                    return true;
                                }
                            }
                            else
                            {
                                var resultMax = false;
                                var resultMin = false;
                                var valueMax = 0D;
                                var valueMin = 0D;
                                var valuebl = Convert.ToDouble(value);

                                if (!string.IsNullOrEmpty(DataInputMax))
                                {
                                    valueMax = Convert.ToDouble(VariablesTools.ResolveValue(DataInputMax));
                                    Logger.InfoFormat("{0} -> {1}  < {2}", DataInputName, value, valueMax);
                                    if (valuebl < valueMax)
                                        resultMax = true;
                                }
                                else
                                    resultMax = true;

                                if (!string.IsNullOrEmpty(DataInputMin))
                                {
                                    valueMin = Convert.ToDouble(VariablesTools.ResolveValue(DataInputMin));
                                    Logger.InfoFormat("{0} -> {1}  > {2}", DataInputName, value, valueMin);
                                    if (valuebl > valueMin)
                                        resultMin = true;
                                }
                                else
                                    resultMin = true;

                                if (AddToResults)
                                    testBase.Resultado.Set(ReadMethodAction.VariableName + TestPoint, valuebl, ParamUnidad.SinUnidad, valueMin, valueMax);

                                var result = resultMin && resultMax;

                                if (result)
                                    frm.DialogResult = DialogResult.OK;

                                return result;
                            }
                        }
                        else
                        {
                            if (frm.DialogResult == DialogResult)
                                return true;
                        }

                        return false;

                    }, "", configuration.IterationNumber, configuration.DelayBetweenIteration, configuration.InitDelayTime, configuration.CancelIfError, configuration.ThrowException,
                    (p) => testBase.Error().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO(DataInputName).Throw());
                }
                else
                {
                    testBase.SamplerWithCancel((p) =>
                    {
                        if (!string.IsNullOrEmpty(DataInputName) && !string.IsNullOrEmpty(DataInput))
                        {
                            var value = VariablesTools.ResolveValue(DataInputName).ToString();
                            var varValue = VariablesTools.ResolveValue(DataInput).ToString();

                            Logger.InfoFormat("{0} -> {1}  == {2}", DataInputName, value, varValue);

                            if (value == varValue)
                                frm.DialogResult = DialogResult.OK;

                        }
                        else
                        {
                            if (!Buttons.HasValue || Buttons == EnumBoxButtons.Nothing)
                                Error().SOFTWARE.SECUENCIA_TEST.ACTION_MAL_CONFIGURADO("Action mal configurado, la propiedad Buttons ha de ser un EnumBoxButtons").Throw();

                            System.Threading.Thread.Sleep(50);
                        }

                        if (frm.DialogResult != DialogResult.None)
                            return true;

                        return false;

                    }, "", configuration.IterationNumber, configuration.DelayBetweenIteration, configuration.InitDelayTime, configuration.CancelIfError, configuration.ThrowException,
                    (p) => testBase.Error().PROCESO.ACTION_EXECUTE.TEST_CANCELADO(DataInputName).Throw());
                }
            }
            finally
            {
                var resultOK = frm.DialogResult == DialogResult;

                testBase.CloseForm(frm);

                if (!resultOK)
                {
                    if (SaveResults)
                        testBase.Resultado.Set(TestPoint, "ERROR", Model.ParamUnidad.SinUnidad);

                    testBase.Error().PROCESO.ACTION_EXECUTE.TEST_CANCELADO(ExceptionText).Throw();
                }
                if (SaveResults)
                    testBase.Resultado.Set(TestPoint, "OK", Model.ParamUnidad.SinUnidad);

                image?.Dispose();
            }
        }
    }

    public class ImageViewActionDescription :  ActionDescription
    {
        public override string Name { get { return "ShowImage"; } }
        public override string Description { get { return "Permite hacer preguntas al operario mostrando una imagen"; } }
        public override string Category { get { return "Actions/Message"; } }
        public override Image Icon { get { return Properties.Resources.image_balloon_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}