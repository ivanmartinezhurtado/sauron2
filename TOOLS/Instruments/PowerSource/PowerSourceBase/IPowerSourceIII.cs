﻿using Dezac.Core.Utility;
using System;

namespace Instruments.PowerSource
{
    public interface IPowerSourceIII
    {
        TriLineValue ReadVoltage();
        TriLineValue ReadCurrent();
        TriLineValue ReadDesfase();
        double ReadFreq();
        TriLinePower ReadPower();
        TriLineValue ReadPowerFactor();

        double Tolerance { set; get; }
        int TimeOutStabilitation { get; set; }

        void ApplyOff();
        void ApplyOffAndWaitStabilisation();
        void Reset();
        void ApplyPresets(TriLineValue voltage, TriLineValue current, double freq, TriLineValue powerFase, TriLineValue desfases, bool forceApply = false);
        bool ApplyAndWaitStabilisation(bool forceApply = false);
        bool ApplyPresetsAndWaitStabilisation(TriLineValue voltage, TriLineValue current, double freq, TriLineValue powerFase, TriLineValue desfases, bool forceApply = false);
        bool ApplyPresetsAndWaitStabilisationWithValidator(TriLineValue voltage, TriLineValue current, double freq, TriLineValue powerFase, TriLineValue desfases, bool forceApply = false, Func<bool> validator = null);
    }
}
