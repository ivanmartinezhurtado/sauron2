﻿using Dezac.Core.Model;
using Dezac.Services;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Windows.Forms;

namespace WinTaskRunner.Views
{
    public partial class NewOrderView : UserControl, IDialogView
    {
        public int? NumProducto { get; set; }
        public int? Version { get; set; }
        public string Descripcion { get; set; }
        public int Unidades { get; set; }
        public string Notas { get; set; }
        public string IdUser { get; set; }

        public int? NumOrden { get; set; }

        protected readonly IShell shell;

        public NewOrderView()
        {
            InitializeComponent();
        }

        public NewOrderView(IShell shell)
            : this()
        {
            this.shell = shell;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            UpdateUI();
        }

        private void UpdateUI()
        {
            if (NumProducto.HasValue)
                lblProducto.Text = string.Format("{0}/{1}-{2}", NumProducto, Version, Descripcion);
            else
                lblProducto.Text = null;

            lblUsuario.Text = IdUser;
        }

        public event DialgoResultEventHandler CloseDialog;

        public bool ValidateView(DialogResult result)
        {
            if (result == DialogResult.OK)
                return GenerarOrden();

            return true;
        }

        private bool ValidateForm()
        {
            string msg = string.Empty;

            Notas = txtNotas.Text;
            Unidades = Convert.ToInt32(txtUnidades.Value);

            if (NumProducto.GetValueOrDefault() <= 0)
                msg += "Falta el producto\n";

            if (Version.GetValueOrDefault() <= 0)
                msg += "Falta la versión\n";

            if (Unidades <= 0)
                msg += "Falta indicar las unidades\n";

            if (string.IsNullOrEmpty(IdUser))
                msg += "Falta el usuario\n";

            if (string.IsNullOrEmpty(Notas))
                msg += "Falta la descripcion\n";

            if (msg != string.Empty)
                MessageBox.Show(msg, "Atención");

            return msg == string.Empty;
        }

        private bool GenerarOrden()
        {
            if (!ValidateForm())
                return false;

            using (var svc = new DezacService())
            {
                try
                {
                    NumOrden = svc.GenerarOrdenEnsayo(NumProducto.Value, Version.Value, Unidades, Notas);
                }
                catch (Exception ex)
                {
                    shell.MsgBox(ex.Message, "ERROR GENERANDO LA ORDEN");
                }
            }

            return true;
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            ProductsViewModel item = null;

            using (var prodView = new ProductsView())
            {
                prodView.OnlyActive = true;
                prodView.HidePanels();

                if (shell.ShowDialog("Seleccione el producto", prodView, MessageBoxButtons.OKCancel) == DialogResult.OK)
                    item = prodView.Current;
            }

            if (item == null)
                return;

            NumProducto = item.NUMPRODUCTO;
            Version = item.VERSION;
            Descripcion = item.DESCRIPCION;

            UpdateUI();
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            var ctrl = new LoginView();

            if (shell.ShowDialog("IDENTIFICACIÓN", ctrl, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;

            IdUser = ctrl.Empleado.IDEMPLEADO;
            UpdateUI();
        }
    }
}
