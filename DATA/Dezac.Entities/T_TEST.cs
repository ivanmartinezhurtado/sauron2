namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class T_TEST
    {
        public T_TEST()
        {
            this.ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            this.T_TESTFASE = new HashSet<T_TESTFASE>();
            this.T_TESTFASES = new HashSet<T_TESTFASES>();
        }
    
        public int NUMTEST { get; set; }
        public int IDTEST { get; set; }
        public int NUMPROCESO { get; set; }
        public Nullable<int> NUMFABRICACION { get; set; }
        public System.DateTime FECHA { get; set; }
        public Nullable<int> TIEMPO { get; set; }
        public string RESULTADO { get; set; }
        public Nullable<int> NUMMATRICULATEST { get; set; }
        public Nullable<int> NUMPRODUCTOTEST { get; set; }
        public Nullable<int> VERSIONPRODUCTOTEST { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
        public string NROSERIE { get; set; }
        public Nullable<int> VERSIONREPORT { get; set; }
        public Nullable<int> NUMFAMILIA { get; set; }
    
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }
        public virtual MATRICULA MATRICULA { get; set; }
        public virtual ORDENPRODUCTO ORDENPRODUCTO { get; set; }
        public virtual PRODUCTO PRODUCTO { get; set; }
        public virtual ICollection<T_TESTFASE> T_TESTFASE { get; set; }
        public virtual ICollection<T_TESTFASES> T_TESTFASES { get; set; }
        public virtual T_TESTREPORT T_TESTREPORT { get; set; }
        public virtual T_FAMILIA T_FAMILIA { get; set; }
    }
}
