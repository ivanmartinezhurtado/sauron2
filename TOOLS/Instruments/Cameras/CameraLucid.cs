﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ArenaNET;
using Dezac.Core.Exceptions;
using Instruments.Cameras.DTO;
using System.Drawing.Imaging;

namespace Instruments.Cameras
{
    public class CameraLucid
    {
        private const int GET_IMAGE_TIMEOUT = 12000;
        private const int TRANSPORT_TIMEOUT = 200;
        private const int FIND_DEVICE_TIMEOUT = 100;

        protected static readonly ILog logger = LogManager.GetLogger(typeof(CameraLucid));

        private string serialNumber;
        private static ISystem arenaSystem;
        public IDevice camera;

        public CameraLucidSettings CameraSettings { get; internal set; }

        public CameraLucid()
        {
        }
        private void StartCamera()
        {
            if (camera != null)
                return;

            try
            {
                if (arenaSystem == null)
                    arenaSystem = ArenaNET.Arena.OpenSystem();

                int i = 0;
                while (i++ < 10)
                {
                    arenaSystem.UpdateDevices(FIND_DEVICE_TIMEOUT);
                    if (arenaSystem.Devices.Count == 0)
                        continue;

                    var deviceInfo = arenaSystem.Devices.Where((devInfo) => devInfo.SerialNumber == serialNumber).ToList();
                    if (deviceInfo.Count == 0)
                        continue;

                    camera = arenaSystem.CreateDevice(deviceInfo.FirstOrDefault());
                    break;
                }
                if (i >= 10)
                    TestException.Create().HARDWARE.INSTRUMENTOS.DETECCION_INSTRUMENTO($"There is no Camera connected with the Serial Number {serialNumber}").Throw();

                //IInteger transportTimeout = (IInteger)camera.NodeMap.GetNode("GevMCTT");
                //transportTimeout.Value = TRANSPORT_TIMEOUT;
            }
            catch (Exception e)
            {
                Dispose();
                throw e;
            }
        }
        public void StartCamera(CameraLucidSettings settings)
        {
            serialNumber = settings.SerialNumber;
            StartCamera();
            SetSettings(settings);
        }
        public void SetSettings(CameraLucidSettings settings)
        {
            if (settings == CameraSettings)
                return;

            if (CameraSettings == null || settings.Width != CameraSettings.Width)
            {
                IInteger widthNode = (IInteger)camera.NodeMap.GetNode("Width");
                if (settings.Width != 0)
                    widthNode.SetIntValue(settings.Width);
                else
                    widthNode.SetIntValue(widthNode.Max);
                widthNode.LogSetting();
            }
            if (CameraSettings == null || settings.Height != CameraSettings.Height)
            {
                IInteger heightNode = (IInteger)camera.NodeMap.GetNode("Height");
                if (settings.Height != 0)
                    heightNode.SetIntValue(settings.Height);
                else
                    heightNode.SetIntValue(heightNode.Max);
                heightNode.LogSetting();
            }
            if (CameraSettings == null || settings.OffsetX != CameraSettings.OffsetX)
            {
                IInteger offsetXNode = (IInteger)camera.NodeMap.GetNode("OffsetX");
                offsetXNode.SetIntValue(settings.OffsetX);
                offsetXNode.LogSetting();
            }
            if (CameraSettings == null || settings.OffsetY != CameraSettings.OffsetY)
            {
                IInteger offsetYNode = (IInteger)camera.NodeMap.GetNode("OffsetY");
                offsetYNode.SetIntValue(settings.OffsetY);
                offsetYNode.LogSetting();
            }
            if (CameraSettings == null || settings.FrameRate != CameraSettings.FrameRate)
            {
                IBoolean acquisitionFrameRateEnableNode = (IBoolean)camera.NodeMap.GetNode("AcquisitionFrameRateEnable");
                acquisitionFrameRateEnableNode.Value = true;
                acquisitionFrameRateEnableNode.LogSetting();

                IFloat frameRateNode = (IFloat)camera.NodeMap.GetNode("AcquisitionFrameRate");
                frameRateNode.SetFloatValue(settings.FrameRate);
                frameRateNode.LogSetting();
            }
            if (CameraSettings == null || settings.ExposureTime != CameraSettings.ExposureTime)
            {
                IEnumeration exposureAutoNode = (IEnumeration)camera.NodeMap.GetNode("ExposureAuto");
                exposureAutoNode.Symbolic = "Off";
                exposureAutoNode.LogSetting();

                IFloat exposureTimeNode = (IFloat)camera.NodeMap.GetNode("ExposureTime");
                exposureTimeNode.SetFloatValue(settings.ExposureTime * 1000);
                exposureTimeNode.LogSetting();
            }
            if (CameraSettings == null || settings.Gain != CameraSettings.Gain)
            {
                IEnumeration gainAutoNode = (IEnumeration)camera.NodeMap.GetNode("GainAuto");
                gainAutoNode.Symbolic = "Off";
                gainAutoNode.LogSetting();

                IFloat gainNode = (IFloat)camera.NodeMap.GetNode("Gain");
                gainNode.SetFloatValue(settings.Gain);
                gainNode.LogSetting();
            }
            if (CameraSettings == null || settings.RedOffset != CameraSettings.RedOffset)
            {
                IEnumeration balanceWhiteAuto = (IEnumeration)camera.NodeMap.GetNode("BalanceWhiteAuto");
                balanceWhiteAuto.Symbolic = "Off";
                balanceWhiteAuto.LogSetting();

                IEnumeration balanceRatioSelector = (IEnumeration)camera.NodeMap.GetNode("BalanceRatioSelector");
                balanceRatioSelector.Symbolic = "Red";
                balanceRatioSelector.LogSetting();

                IFloat redOffsetNode = (IFloat)camera.NodeMap.GetNode("BalanceRatio");
                redOffsetNode.SetFloatValue(settings.RedOffset);
                redOffsetNode.LogSetting();
            }
            if (CameraSettings == null || settings.GreenOffset != CameraSettings.GreenOffset)
            {
                IEnumeration balanceWhiteAuto = (IEnumeration)camera.NodeMap.GetNode("BalanceWhiteAuto");
                balanceWhiteAuto.Symbolic = "Off";
                balanceWhiteAuto.LogSetting();

                IEnumeration balanceRatioSelector = (IEnumeration)camera.NodeMap.GetNode("BalanceRatioSelector");
                balanceRatioSelector.Symbolic = "Green";
                balanceRatioSelector.LogSetting();

                IFloat redOffsetNode = (IFloat)camera.NodeMap.GetNode("BalanceRatio");
                redOffsetNode.SetFloatValue(settings.GreenOffset);
                redOffsetNode.LogSetting();
            }
            if (CameraSettings == null || settings.BlueOffset != CameraSettings.BlueOffset)
            {
                IEnumeration balanceWhiteAuto = (IEnumeration)camera.NodeMap.GetNode("BalanceWhiteAuto");
                balanceWhiteAuto.Symbolic = "Off";
                balanceWhiteAuto.LogSetting();

                IEnumeration balanceRatioSelector = (IEnumeration)camera.NodeMap.GetNode("BalanceRatioSelector");
                balanceRatioSelector.Symbolic = "Blue";
                balanceRatioSelector.LogSetting();

                IFloat redOffsetNode = (IFloat)camera.NodeMap.GetNode("BalanceRatio");
                redOffsetNode.SetFloatValue(settings.BlueOffset);
                redOffsetNode.LogSetting();
            }
            if (CameraSettings == null || settings.StreamPacketSize != CameraSettings.StreamPacketSize)
            {
                IInteger streamChannelPacketSizeNode = (IInteger)camera.NodeMap.GetNode("GevSCPSPacketSize");
                streamChannelPacketSizeNode.Value = 9000;
                streamChannelPacketSizeNode.LogSetting();
            }

            CameraSettings = settings;
        }
        public void StartStream(uint numberImages)
        {
            try { camera.StopStream(); } catch { }

            try { camera.StartStream(numberImages); } catch { }
        }
        public List<Bitmap> GetImage(int numImages, Action<int> preCaptureFunction)
        {
            List<IImage> images = new List<IImage>();
            List<Bitmap> bitmaps = new List<Bitmap>();

            try
            {
                camera.StartStream((uint)numImages);

                for (int i = 1; i <= numImages; i++)
                {
                    preCaptureFunction?.Invoke(i);
                    IImage image = camera.GetImage((ulong)(GET_IMAGE_TIMEOUT + CameraSettings.ExposureTime));
                    images.Add(image);
                    bitmaps.Add(new Bitmap(image.Bitmap));
                    camera.RequeueBuffer(image);
                }

                camera.StopStream();
            }
            catch (Exception e)
            {
                Dispose();
                throw e;
            }
            return bitmaps;
        }
        public void SaveImage(string fileName)
        {
            var images = GetImage(1, null);
            images.First().Save(fileName, ImageFormat.Png);
            images.ForEach(image => image.Dispose());
        }
        public void Dispose()
        {
            try { camera.StopStream(); } catch { }
            try { arenaSystem.DestroyDevice(camera); } catch { }
            camera = null;
        }
    }
}
