﻿using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.07)]
    public class EKOR_RPA200 : EKOR_SAFE
    {
        public EKOR_RPA200()
        {
        }

        public EKOR_RPA200(int port)
        {
            SetPort(port);
        }

        #region Enumerados
        public enum Scale
        {
            Esc1 = 0,
            Esc2 = 1,
            Esc3 = 2,
            EscCC = 3
        }

        [Flags]
        public enum Channel
        {
            [Browsable(false)]
            ALL = 0x03FF,
            VN = 0x0100,
            VL123S_IL123NS = ALL - VN,
        }

        public enum Lines
        {
            L1 = 1,
            L2 = 2,
            L3 = 3,
            LH = 5,
            LS = 6
        }

        public enum enumInputs
        {
            [Description("Entrada digital 1")]
            IN1 = 0,
            [Description("Entrada digital 2")]
            IN2 = 1,
            [Description("Entrada digital 3")]
            IN3 = 2,
            [Description("Entrada digital 4")]
            IN4 = 3,
            [Description("Entrada digital 5")]
            IN5 = 4,
            [Description("Entrada digital 6")]
            IN6 = 5,
            [Description("Entrada digital 7")]
            IN7 = 6,
            [Description("Entrada digital 8")]
            IN8 = 7,
            [Description("Entrada digital 9")]
            IN9 = 8,
            [Description("Entrada digital 10")]
            IN10 = 9,
        }

        #endregion

        #region Metodos AutoCaliobracion

        public void AutoCalibration_All_Sin_VN(Scale scale, TriLineValue voltage, TriLineValue current, int numberCycles, double relGanaciasTension, double relGanaciasCorriente)
        {
            LogMethod();

            var channel = Channel.VL123S_IL123NS;
            _logger.InfoFormat("AutoCalibration  con Channel = {0}  Scala = {1}",  channel.ToString(), scale.ToString());

            if (relGanaciasCorriente == 0)
                TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("Relacion_Corriente").Throw();

            if (relGanaciasTension == 0)
                TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("relGanaciasTension").Throw();

            _logger.InfoFormat("AutoCalibration Voltage = {0}", voltage.ToString());
            _logger.InfoFormat("AutoCalibration Corriente = {0}", current.ToString());

            current *= relGanaciasCorriente;
            voltage *= relGanaciasTension;

            var valorVoltageL1 = (int)(voltage.L1 * FactorPasaTapas.L1_FactorAjuste);
            var valorVoltageL2 = (int)(voltage.L2 * FactorPasaTapas.L2_FactorAjuste);
            var valorVoltageL3 = (int)(voltage.L3 * FactorPasaTapas.L3_FactorAjuste);
            var valorVoltageL4 = (int)(voltage.L3 * FactorPasaTapas.L4_FactorAjuste);

            _logger.InfoFormat("AutoCalibration Voltage L1 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L1_FactorAjuste, valorVoltageL1.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L2 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L2_FactorAjuste, valorVoltageL2.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L3 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L3_FactorAjuste, valorVoltageL3.ToString());
            _logger.InfoFormat("AutoCalibration Voltage L4 con Relacion = {0}  Factor Pasa Tapas = {1} Voltage = {2}", relGanaciasTension, FactorPasaTapas.L4_FactorAjuste, valorVoltageL4.ToString());

            _logger.InfoFormat("AutoCalibration Corriente L1 con Relacion = {0}  corriente = {1}", relGanaciasCorriente, current.L1);
            _logger.InfoFormat("AutoCalibration Corriente L2 con Relacion = {0}  corriente = {1}", relGanaciasCorriente, current.L2);
            _logger.InfoFormat("AutoCalibration Corriente L3 con Relacion = {0}  corriente = {1}", relGanaciasCorriente, current.L3);

            var param = new List<string>();
            param.Add(((int)current.L1).ToString());
            param.Add(((int)current.L2).ToString());
            param.Add(((int)current.L3).ToString());
            param.Add(((int)current.L2).ToString());
            param.Add(((int)current.L3).ToString());
            param.Add(valorVoltageL1.ToString());
            param.Add(valorVoltageL2.ToString());
            param.Add(valorVoltageL3.ToString());
            param.Add(valorVoltageL4.ToString());

            var parameters = "";
            foreach (var data in param.ToArray())
                parameters += " " + data;

            Cirbus.Write(AUTOCALIBRATION_NEW, listErrorCalibration, ((ushort)channel).ToString("X4"), (ushort)scale, parameters.TrimStart(), numberCycles.ToString());
        }

        public void AutoCalibration_VN(Scale scale, double voltage, int numberCycles, double relGanaciasTension)
        {
            LogMethod();

            var channel = Channel.VN;

            _logger.InfoFormat("AutoCalibration con Channel = {0}  Scala = {1}",  channel.ToString(), scale.ToString());

            voltage *= relGanaciasTension;

            if (relGanaciasTension == 0)
                TestException.Create().PROCESO.PARAMETROS_ERROR.FALTA_VALOR_PARAMETRO("Relacion_Voltage").Throw();

            _logger.InfoFormat("AutoCalibration Voltage con Relacion = {0}  Voltage = {1}", relGanaciasTension, voltage.ToString());

            Cirbus.Write(AUTOCALIBRATION_NEW, listErrorCalibration, ((ushort)channel).ToString("X4"), (ushort)scale, ((int)voltage).ToString(), numberCycles.ToString());
        }

        public StructCalibrationMeasure ReadFactorsCalibrated(Scale scale)
        {
            LogMethod();
            return Cirbus.Read<StructCalibrationMeasure>(READCALIBRATIONSCALE, listErrorCalibration, (byte)scale);
        }

        public void WriteFactorsCalibration(Scale scale, StructCalibrationMeasure factorsScaleCalibrated, double relGanaciasTension, double relGanaciasCorriente)
        {
            LogMethod();

            var resp = factorsScaleCalibrated.resp;
            var factor_IL1 = Convert.ToInt64(factorsScaleCalibrated.factor_IL1 / relGanaciasCorriente);
            var factor_IL2 = Convert.ToInt64(factorsScaleCalibrated.factor_IL2 / relGanaciasCorriente);
            var factor_IL3 = Convert.ToInt64(factorsScaleCalibrated.factor_IL3 / relGanaciasCorriente);
            var factor_IH = Convert.ToInt64(factorsScaleCalibrated.factor_IH / relGanaciasCorriente);
            var factor_IBT = Convert.ToInt64(factorsScaleCalibrated.factor_IBT / relGanaciasCorriente);
            var factor_VL1 = Convert.ToInt64(factorsScaleCalibrated.factor_VL1 / relGanaciasTension);
            var factor_VL2 = Convert.ToInt64(factorsScaleCalibrated.factor_VL2 / relGanaciasTension);
            var factor_VL3 = Convert.ToInt64(factorsScaleCalibrated.factor_VL3 / relGanaciasTension);
            var factor_VN = factorsScaleCalibrated.factor_VN;
            var factor_VSync = Convert.ToInt64(factorsScaleCalibrated.factor_VSync / relGanaciasTension);
            var delimiter = factorsScaleCalibrated.delimiter;
            var offset_IL1 = factorsScaleCalibrated.offset_IL1;
            var offset_IL2 = factorsScaleCalibrated.offset_IL2;
            var offset_IL3 = factorsScaleCalibrated.offset_IL3;
            var offset_IH = factorsScaleCalibrated.offset_IH;
            var offset_IBT = factorsScaleCalibrated.offset_IBT;
            var offset_VL1 = factorsScaleCalibrated.offset_VL1;
            var offset_VL2 = factorsScaleCalibrated.offset_VL2;
            var offset_VL3 = factorsScaleCalibrated.offset_VL3;
            var offset_VN = factorsScaleCalibrated.offset_VN;
            var offset_VSync = factorsScaleCalibrated.offset_VSync;

            _logger.InfoFormat("Factors Corriente Scala = {0}   IL1 ={1}   IL2={2}  IL3={3}  IH = {4}  IBT = {5}" , scale.ToString(), factor_IL1, factor_IL2, factor_IL3, factor_IH, factor_IBT);
            _logger.InfoFormat("Factors Tensiones Scala = {0}   VL1 ={1}   VL2={2}  VL3={3}  VN = {4}  VSYNC = {5}", scale.ToString(), factor_VL1, factor_VL2, factor_VL3, factor_VN, factor_VSync);
            _logger.InfoFormat("Factors Offsets Corrientes  Scala = {0}   offset_IL1 ={1}   offset_IL2={2}  offset_IL3={3}  offset_IH = {4}  offset_IBT = {5}", scale.ToString(), offset_IL1, offset_IL2, offset_IL3, offset_IH, offset_IBT);
            _logger.InfoFormat("Factors Offsets Tension  Scala = {0}   offset_VL1 ={1}   offset_VL2={2}  offset_VL3={3}  offset_VN = {4}  offset_VSync = {5}", scale.ToString(), offset_VL1, offset_VL2, offset_VL3, offset_VN, offset_VSync);

            var trama = string.Format("{0:00000000000} {1:00000000000} {2:00000000000} {3:00000000000} {4:00000000000} {5:00000000000} {6:00000000000} {7:00000000000} {8:00000000000} {9:00000000000} {10:00000} {11:00000} {12:00000} {13:00000} {14:00000} {15:00000} {16:00000} {17:00000} {18:00000} {19:00000}",
    factor_IL1, factor_IL2, factor_IL3, factor_IH, factor_IBT, factor_VL1, factor_VL2, factor_VL3, factor_VN, factor_VSync, offset_IL1, offset_IL2, offset_IL3, offset_IH, offset_IBT, offset_VL1, offset_VL2, offset_VL3, offset_VN, offset_VSync);

            Cirbus.Write(WRITEFACTORSCALIBRATION, listErrorCalibration, (int)scale, trama);
        }

        public struct StructCalibrationMeasure
        {
            public string resp;
            public long factor_IL1;
            public long factor_IL2;
            public long factor_IL3;
            public long factor_IH;
            public long factor_IBT;
            public long factor_VL1;
            public long factor_VL2;
            public long factor_VL3;
            public long factor_VN;
            public long factor_VSync;
            public string delimiter;
            public long offset_IL1;
            public long offset_IL2;
            public long offset_IL3;
            public long offset_IH;
            public long offset_IBT;
            public long offset_VL1;
            public long offset_VL2;
            public long offset_VL3;
            public long offset_VN;
            public long offset_VSync;
        }

        public void WriteScale(Scale scale)
        {
            LogMethod();
            Cirbus.Write(WRITESCALE, listErrorCirbus, (ushort)scale + 1);
        }

        #endregion

        #region Metodos Medida

        public MedidaInstantanea ReadInstantMeasures(Lines canal)
        {
            LogMethod();

            var result = Cirbus.Read<MeasureInstante>(string.Format(READPARAMETERSMEASURE, (byte)canal), false);

            if (Rel_Voltage_Measure == 0)
                Rel_Voltage_Measure = 1;

            var _rel_Voltage_Measure = Rel_Voltage_Measure;

            if (canal == Lines.LH)
            {
                if (Rel_Voltage_Vn_Measure == 0)
                    Rel_Voltage_Vn_Measure = 1;

                _rel_Voltage_Measure = Rel_Voltage_Vn_Measure;
            }
         
            if (Rel_Current_Measure == 0)
                Rel_Current_Measure = 1;

            var Rel_PasaTapas = 1D;

            switch (canal)
            {
                case Lines.L1:
                    Rel_PasaTapas = FactorPasaTapas.L1_FactorCalibracion;
                    break;
                case Lines.L2:
                    Rel_PasaTapas = FactorPasaTapas.L2_FactorCalibracion;
                    break;
                case Lines.L3:
                    Rel_PasaTapas = FactorPasaTapas.L3_FactorCalibracion;
                    break;
                case Lines.LS:
                    Rel_PasaTapas = FactorPasaTapas.L4_FactorCalibracion;
                    break;
                default:
                    Rel_PasaTapas = 1;
                    break;
            }

            _logger.InfoFormat("Medida en la linea {0} con Relación Voltage = {1}  con Relación Corriente = {2} y valor PasaTapas = {3}", canal.ToString(), _rel_Voltage_Measure, Rel_Current_Measure, Rel_PasaTapas);
            var value = new MedidaInstantanea(result, _rel_Voltage_Measure, Rel_Current_Measure, Rel_PasaTapas);
            _logger.InfoFormat("Medida en la linea {0} Valor = {1}", canal.ToString(), value);

            return value;
        }

        public AllMeasureInstante ReadAllInstantMeasures()
        {
            LogMethod();

            var l1_value = ReadInstantMeasures(Lines.L1);
            var l2_value = ReadInstantMeasures(Lines.L2);
            var l3_value = ReadInstantMeasures(Lines.L3);
            var lH_value = ReadInstantMeasures(Lines.LH);
            var lS_value = ReadInstantMeasures(Lines.LS);

            var result = new AllMeasureInstante()
            {
                L1 = l1_value,
                L2 = l2_value,
                L3 = l3_value,
                LH = lH_value,
                LS = lS_value,
            };

            return result;
        }

        public struct AllMeasureInstante
        {
            public MedidaInstantanea L1;
            public MedidaInstantanea L2;
            public MedidaInstantanea L3;
            public MedidaInstantanea LH;
            public MedidaInstantanea LS;
        }

        public struct MedidaInstantanea
        {
            public double Voltage { get; set; }
            public double Current { get; set; }
            public double ActivePower { get; set; }
            public double ReactivePower { get; set; }
            public double AparentPower { get; set; }
            public double PowerFactor { get; set; }
            public double Phi { get; set; }

            public MedidaInstantanea(MeasureInstante value, double rel_Voltage, double rel_Current, double rel_PasaTapas)
            {
                Voltage = (value.Voltage / rel_Voltage) * rel_PasaTapas;
                Current = value.Current / rel_Current;
                ActivePower = value.ActivePower;
                ReactivePower = value.ReactivePower;
                AparentPower = value.AparentPower;
                PowerFactor = value.PowerFactor;
                Phi = value.Phi;
            }

            public override string ToString()
            {
                return string.Format("Voltage={0}  Current={1}  ActivePower={2}  ReactivePower={3}  AparentPower={4}  PowerFactor={5}  Phi={6}", Voltage, Current, ActivePower, ReactivePower, AparentPower, PowerFactor, Phi);
            }
        }

        public struct MeasureInstante
        {
            public string RESP;
            public string V;
            public string I;
            public string P;
            public string Q;
            public string S;
            public string PF;
            public string PHI;

            public double Voltage
            {
                get
                {
                    var resp = V.Replace("V:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double Current
            {
                get
                {
                    var resp = I.Replace("I:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double ActivePower
            {
                get
                {
                    var resp = P.Replace("P:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double ReactivePower
            {
                get
                {
                    var resp = Q.Replace("Q:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double AparentPower
            {
                get
                {
                    var resp = S.Replace("S:", "");
                    return Convert.ToDouble(resp);
                }
            }
            public double PowerFactor
            {
                get
                {
                    var resp = PF.Replace("PF:", "");
                    var valor = 999D;
                    Double.TryParse(resp, out valor);
                    return valor;
                }
            }
            public double Phi
            {
                get
                {
                    var resp = PHI.Replace("Phi:", "");
                    return Convert.ToDouble(resp);
                }
            }
        }

        // Medida Corriente

        public MedidaInstantaneaCurrentPhase ReadCurrentPhase()
        {
            LogMethod();
            var result = Cirbus.Read<MeasureStructCurrenPhase>(READCURRENTFUNDAMENTAL, listErrorFlash, "P1");

            if (Rel_Current_Measure == 0)
                Rel_Current_Measure = 1;

            _logger.InfoFormat("Medida Corriente con Relación Corriente = {0}", Rel_Current_Measure);
            var value = new MedidaInstantaneaCurrentPhase(result, Rel_Current_Measure);
            _logger.InfoFormat("Medida corriente ={0}",  value);

            return value;
        }

        public struct MeasureStructCurrenPhase
        {
            public double I1;
            public double I2;
            public double I3;
            public double INC;
            public double IH;
            public double IBT;
            public string signe;
            public double PH1;
            public double PH2;
            public double PH3;
            public double PHNC;
            public double PHH;
            public double PHIBT;
        }

        public struct MedidaInstantaneaCurrentPhase
        {
            public double I1 { get; set; }
            public double I2 { get; set; }
            public double I3 { get; set; }
            public double INC { get; set; }
            public double IH { get; set; }
            public double IBT { get; set; }
            public double PH1 { get; set; }
            public double PH2 { get; set; }
            public double PH3 { get; set; }
            public double PHNC { get; set; }
            public double PHH { get; set; }
            public double PHIBT { get; set; }

            public MedidaInstantaneaCurrentPhase(MeasureStructCurrenPhase value, double rel_Current)
            {
                I1 = value.I1 / rel_Current;
                I2 = value.I2 / rel_Current;
                I3 = value.I3 / rel_Current;
                INC = value.INC / rel_Current;
                IH = value.IH / rel_Current;
                IBT = value.IBT / rel_Current;

                PH1 = value.PH1 / 100;
                PH2 = value.PH2 / 100;
                PH3 = value.PH3 / 100;
                PHNC = value.PHNC / 100;
                PHH = value.PHH / 100;
                PHIBT = value.PHIBT / 100;
            }

            public override string ToString()
            {
                return string.Format("I1={0}  I2={1}  I3={2}  INC={3}  IH={4}  IBT={5}  PH1={6}  PH2={7}  PH3={8}  PHNC={9}  PHH={10}  PHIBT={11}", 
                    I1, I2, I3, INC, IH, IBT, PH1,PH2,PH3, PHNC, PHH, PHIBT);
            }
        }

        // Medida Voltage

        public MedidaInstantaneaVoltagePhase ReadVoltage()
        {
            LogMethod();
            var result = Cirbus.Read<MeasureStructVoltagePhase>(READVOLTAGEFUNFAMENTAL, listErrorFlash, "P1");

            if (Rel_Voltage_Measure == 0)
                Rel_Voltage_Measure = 1;

            _logger.InfoFormat("Medida Tension con Relación Tension = {0}", Rel_Voltage_Measure);
            var value = new MedidaInstantaneaVoltagePhase(result, Rel_Voltage_Measure);
            _logger.InfoFormat("Medida Tension ={0}", value);

            return value;
        }

        public struct MeasureStructVoltagePhase
        {
            public double V1;
            public double V2;
            public double V3;
            public double VNC;
            public double VN;
            public double VS;
            public string signe;
            public double PH1;
            public double PH2;
            public double PH3;
            public double PHNC;
            public double PHN;
            public double PHS;
        }

        public struct MedidaInstantaneaVoltagePhase
        {
            public double V1 { get; set; }
            public double V2 { get; set; }
            public double V3 { get; set; }
            public double VNC { get; set; }
            public double VN { get; set; }
            public double VS { get; set; }
            public double PH1 { get; set; }
            public double PH2 { get; set; }
            public double PH3 { get; set; }
            public double PHNC { get; set; }
            public double PHN { get; set; }
            public double PHSYNC { get; set; }

            public MedidaInstantaneaVoltagePhase(MeasureStructVoltagePhase value, double rel_Voltage)
            {
                V1 = value.V1 / rel_Voltage;
                V2 = value.V2 / rel_Voltage;
                V3 = value.V3 / rel_Voltage;
                VNC = value.VNC / rel_Voltage;
                VN = value.VN / rel_Voltage;
                VS = value.VS / rel_Voltage;

                PH1 = value.PH1 / 100;
                PH2 = value.PH2 / 100;
                PH3 = value.PH3 / 100;
                PHNC = value.PHNC / 100;
                PHN = value.PHN / 100;
                PHSYNC = value.PHS / 100;
            }

            public override string ToString()
            {
                return string.Format("V1={0}  V2={1}  V3={2}  VNC={3}  VN={4}  VS={5}  PH1={6}  PH2={7}  PH3={8}  PHNC={9}  PHN={10}  PHSYNC={11}",
                    V1, V2, V3, VNC, VN, VS, PH1, PH2, PH3, PHNC, PHN, PHSYNC);
            }
        }

        #endregion
  
    }
}
