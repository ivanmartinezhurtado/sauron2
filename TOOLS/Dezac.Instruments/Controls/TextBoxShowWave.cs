﻿using System;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    public partial class TextBoxShowWave : UserControl
    {
        private GraphForm graph;

        public event EventHandler ShowGraph;
        public event EventHandler HideGraph;

        public TextBoxShowWave()
        {
            InitializeComponent();
        }

        public string TitleGraph { get; set; }

        public string Value { get { return txtVariable.Text; } set { txtVariable.Text = value; } }

        private void buttonGrafh_Click(object sender, EventArgs e)
        {
            if (graph == null)
                OpenGraph();
            else
               CloseGraph();
        }

        private void OpenGraph()
        {
            graph = new GraphForm();
            graph.Text = TitleGraph;
            graph.Variable = TitleGraph;
            graph.MdiParent = this.ParentForm.MdiParent;
            graph.Disposed += (s, e) => { CloseGraph(); };
            graph.Show();

            if (ShowGraph != null)
                ShowGraph(this, EventArgs.Empty);
        }

        public void CloseGraph()
        {
            if (graph != null && !graph.IsDisposed)
                graph.Dispose();

            graph = null;

            if (HideGraph != null)
                HideGraph(this, EventArgs.Empty);
        }

        public void AddPoint(double value, bool updateText = false)
        {
            if (graph != null && !graph.IsDisposed)
                graph.AddValue(value);

            if (updateText)
                Value = value.ToString();
        }

        public bool IsGraphVisible { get { return graph != null; } }

        public double AsDouble
        {
            get
            {
                var value = Value;

                if (string.IsNullOrEmpty(value))
                    return 0;

                return Convert.ToDouble(value.Replace('.', ','));
            }
        }
    }
}
