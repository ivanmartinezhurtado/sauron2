﻿using Dezac.Tests.Actions.Views;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Utils;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(InitSerialPortDeviceActionDescription))]
    public class InitSerialPortDeviceAction : ActionBase
    {

        [Category("3. Configuration")]
        [Description("Nombre del equipo con el que se trabaja")]
        [DefaultValue("COM")]
        public string DeviceName { get; set; }

        [Description("Valor del puerto")]
        [Category("3. Configuration")]
        public ActionVariable Port { get; set; } = new ActionVariable();

        [Description("Velocidad de transmsion del puerto")]
        [Category("3. Configuration")]
        [DefaultValue(BaudRateEnum.Bps9600)]
        public BaudRateEnum Bps { get; set; }

        [DefaultValue(1000)]
        [Description("Timepo de espera de la respuesta")]
        [Category("3. Configuration")]
        public int TimeOut { get; set; }

        [Description("Tipo de final de trama del protoolo de comunicaciones")]
        [Category("3. Configuration")]
        public EndLine EndLineCaracter { get; set; }

        [Description("Activcion hardware del espera de recepcion")]
        [Category("3. Configuration")]
        [DefaultValue(false)]
        public bool DTREnable { get; set; }

        [Description("Activcion hardware del espera de transmision")]
        [Category("3. Configuration")]
        [DefaultValue(false)]
        public bool RTSEnable { get; set; }

        [Description("Activcion hardware del Handsahke")]
        [Category("3. Configuration")]
        [DefaultValue(Handshake.None)]
        public Handshake Handshake { get; set; }

        public enum EndLine
        {
            CRLF,
            CR,
            LF,
            PuntoComa,
            TimeOut
        }

        public override void Execute()
        {
            var port = Port.Value.ToString().Trim();

            if (!port.Contains("COM"))
                port = "COM" + port;

            var device = new SerialPort(port, (int)Bps, Parity.None, 8, StopBits.One);
            device.ReadTimeout = TimeOut;
            device.WriteTimeout = TimeOut;
            device.DtrEnable= DTREnable;
            device.RtsEnable = RTSEnable;
            device.Handshake = Handshake;
            device.NewLine = GetStringEndLine(EndLineCaracter);

            if (!device.IsOpen)
                device.Open();

            if (Context.Variables.ContainsKey(DeviceName))
                Context.Variables.Remove(DeviceName);

            Context.Variables.AddOrUpdate(DeviceName, device);
        }

        private string GetStringEndLine(EndLine endLine)
        {
            switch (endLine)
            {
                case EndLine.CR:
                    return "\r";
                case EndLine.LF:
                    return "\n";
                case EndLine.CRLF:
                    return "\r\n";
                case EndLine.PuntoComa:
                    return ";";
                case EndLine.TimeOut:
                    return "TimeOut";
                default:
                    return "\r\n";
            }
         }
    }


    public class InitSerialPortDeviceActionDescription : ActionDescription
    {
        public override string Name { get { return "InitSerialPort"; } }
        public override string Description { get { return "Crea una instancia de un puerto de comunicacion Serie"; } }
        public override string Category { get { return "Tools/Comunications/ASCII"; } }
        public override Image Icon { get { return Properties.Resources.vspk_icon_256; } }
    }
}
