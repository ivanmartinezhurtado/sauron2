﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using Instruments.SCPI;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.04)]
    public class PM100 : MeasureBase<PM100>, IDisposable, IConsoleDevice
    {
        public PM100SCPI SCPI { get { return new PM100SCPI(this); } }
        private SerialPort sp;
        private byte _port = 2;
        private readonly static CultureInfo enUS = new CultureInfo("en-US");
        public int WaitTimeMeasure { get; set; }

        private string measureReaded;
        public enum PrivateCommands
        {
            SET_AVG_FIX = Commands.USER_DEFINED_COMMANDS + 1,
            SET_AVG_AUTO = Commands.USER_DEFINED_COMMANDS + 2,
            SET_BALLAST_MODE = Commands.USER_DEFINED_COMMANDS + 3,
            SET_CALIBRATION = Commands.USER_DEFINED_COMMANDS + 4,
            SET_CALIBRATION_END = Commands.USER_DEFINED_COMMANDS + 5,
            QUERY_CALIBRATION = Commands.USER_DEFINED_COMMANDS + 6,
            SET_CONFIGURE = Commands.USER_DEFINED_COMMANDS + 7,
            QUERY_CONFIGURE = Commands.USER_DEFINED_COMMANDS + 8,
            SET_DSE = Commands.USER_DEFINED_COMMANDS + 9,
            SET_ESE = Commands.USER_DEFINED_COMMANDS + 10,
            QUERY_FUNCTION = Commands.USER_DEFINED_COMMANDS + 11,
            QUERY_FUNDAMENTAL = Commands.USER_DEFINED_COMMANDS + 12,
            QUERY_FOREGROUND_DATA = Commands.USER_DEFINED_COMMANDS + 13,
            SET_FREQ_SOURCE = Commands.USER_DEFINED_COMMANDS + 14,
            SET_HRM = Commands.USER_DEFINED_COMMANDS + 15,
            SET_INTEGRATOR = Commands.USER_DEFINED_COMMANDS + 16,
            SET_RNG_FIX = Commands.USER_DEFINED_COMMANDS + 17,
            SET_RNG_AUTO = Commands.USER_DEFINED_COMMANDS + 18,
            SET_SCALING = Commands.USER_DEFINED_COMMANDS + 19,
            SET_SELECTION_FUNCTION = Commands.USER_DEFINED_COMMANDS + 20,
            SET_SHUNT = Commands.USER_DEFINED_COMMANDS + 21,
            SET_WIRING_CONFIG = Commands.USER_DEFINED_COMMANDS + 22
        }

        public Dictionary<PrivateCommands, string> commandStrings = new Dictionary<PrivateCommands, string>()
        {
            {PrivateCommands.SET_AVG_FIX, ":AVG:FIX {0}"},
            {PrivateCommands.SET_AVG_AUTO, ":AVG:AUT"},
            {PrivateCommands.SET_BALLAST_MODE, ":BALL:H{0}"},
            {PrivateCommands.SET_CALIBRATION, ":CAL:{0} {1}, {2}"},
            {PrivateCommands.SET_CALIBRATION_END, ":CAL:END {0}"},
            {PrivateCommands.QUERY_CALIBRATION, ":CAL:{0}? {1}"},
            {PrivateCommands.SET_CONFIGURE, ":CFG {0}, {1}"},
            {PrivateCommands.QUERY_CONFIGURE, ":CFG? {0}"},
            {PrivateCommands.SET_DSE, ":DSE {0}"},
            {PrivateCommands.SET_ESE, "*ESE {0}"},
            {PrivateCommands.QUERY_FUNCTION, ":FNC:{0}?"},
            {PrivateCommands.QUERY_FUNDAMENTAL, ":FND:{0}?"},
            {PrivateCommands.QUERY_FOREGROUND_DATA, ":FRD?"},
            {PrivateCommands.SET_FREQ_SOURCE, ":FSR:{0}"},
            {PrivateCommands.SET_HRM, ":HRM {0}"},
            {PrivateCommands.SET_INTEGRATOR, ":INT:{0}"},
            {PrivateCommands.SET_RNG_FIX, ":RNG:{0}:FIX {1}"},
            {PrivateCommands.SET_RNG_AUTO, ":RNG:{0}:AUT"},
            {PrivateCommands.SET_SCALING, ":SCL:{0} {1}"},
            {PrivateCommands.SET_SELECTION_FUNCTION, ":SEL:{0}"},
            {PrivateCommands.SET_SHUNT, ":SHU:{0}"},
            {PrivateCommands.SET_WIRING_CONFIG, ":WRG:{0}"},
        };

        public enum Functions
        {
            VOLT,
            CURR,
            RES,
            FREQ,
            CONT,
            TEMP,
            CAPA,
            DIOD,
            DIODEZ
        }
        public PM100()
        {
            if (sp == null)
                sp = new SerialPort();

            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
            protocol = new StringProtocol();
            //protocol.CaracterFinRx = "\r";
            protocol.CaracterFinTx = "\r";
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp, "\r"), _logger);
            transport.Retries = 1;
        }
        public string Idn { get; internal set; }
        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else
                    sp.PortName = "COM" + _port;
            }
        }
        public void SetPort(string port)
        {
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }
        public void SetAverageFix(byte depth)
        {
            if (depth > 16)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO("El valor de depth mayor de 16").Throw();

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_AVG_FIX, true, depth));
        }
        public void SetAverageAuto()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_AVG_AUTO, true));
        }
        public enum BallastMode
        {
            H50 = 50,
            H60 = 60,
        }
        public void SetBallastMode(BallastMode value)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_BALLAST_MODE, true, value));
        }
        public enum CalibrationMode
        {
            VOLTAGE,
            CURRENT,
            EXTERNAL
        }
        public void SetCalibrationMode(CalibrationMode mode, byte range, double value)
        {
            var modeTrama = mode == CalibrationMode.VOLTAGE ? "VLT" : mode == CalibrationMode.CURRENT ? "AMP" : "EXT";
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CALIBRATION, true, modeTrama, range, value));
        }
        public void SetCalibrationEnd(int password)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CALIBRATION_END, true, password));
        }
        public void SetConfigure(int prog, double data)
        {
            if (prog > 49)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO("El valor de prog mayor de 49").Throw();

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CONFIGURE, true, prog, data));
        }
        public void SetEventStatusEnabled(int data)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_DSE, true, data));
        }
        public void SetDataStatusEnabled(int data)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_ESE, true, data));
        }
        public double ReadCalibration(CalibrationMode mode, byte range)
        {
            double res = 0;

            var modeTrama = mode == CalibrationMode.VOLTAGE ? "VLT" : mode == CalibrationMode.CURRENT ? "AMP" : "EXT";
            SendCommand(new Command<PrivateCommands>(PrivateCommands.QUERY_CALIBRATION, true, modeTrama, range));
            var responseArray = measureReaded.Split(';');
            if (responseArray.Length > 1)
                res = double.Parse(responseArray[1], NumberStyles.Float, new CultureInfo("en-US"));
            else
                throw new InstrumentException(1, "Error no se ha recibido el trozo de trama con el valor de medida del FLUKE");

            return res;
        }
        public double ReadConfiguration(int prog)
        {
            if (prog > 49)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO("El valor de prog mayor de 49").Throw();

            double res = 0;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.QUERY_CONFIGURE, true, prog));
            var responseArray = measureReaded.Split(';');
            if (responseArray.Length > 1)
                res = double.Parse(responseArray[1], NumberStyles.Float, new CultureInfo("en-US"));
            else
                throw new InstrumentException(1, "Error no se ha recibido el trozo de trama con el valor de medida del FLUKE");

            return res;
        }
        public enum FuncOptions
        {
            WAT,
            VAS,
            VAR,
            VLT,
            AMP,
            PWF,
            VPK,
            APK,
            VCF,
            ACF,
            WHR,
            VAH,
            VRH,
            AHR,
            APF,
            VHM,
            AHM,
            VDF,
            ADF,
            FRQ,
            ADC,
            VDC,
            AHA,
            VHA,
            WHM,
        }
        public double ReadFunctionData(FuncOptions magnitud)
        {
            double res = 0;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.QUERY_FUNCTION, true, magnitud.ToString()));
            var responseArray = measureReaded.Split(';');
            if (responseArray.Length > 1)
                res = double.Parse(responseArray[1], NumberStyles.Float, new CultureInfo("en-US"));
            else
                throw new InstrumentException(1, "Error no se ha recibido el trozo de trama con el valor de medida del FLUKE");

            return res;
        }
        public enum FundamentalData
        {
            WAT,
            VAS,
            VAR,
            VLT,
            AMP,
            PWF,
            WHR,
            VAH,
            VRH,
            AHR,
            APF,
        }
        public double ReadFundamentalData(FundamentalData magnitud)
        {
            double res = 0;
            SendCommand(new Command<PrivateCommands>(PrivateCommands.QUERY_FUNDAMENTAL, true, magnitud.ToString()));
            var responseArray = measureReaded.Split(';');
            if (responseArray.Length > 1)
                res = double.Parse(responseArray[1], NumberStyles.Float, new CultureInfo("en-US"));
            else
                throw new InstrumentException(1, "Error no se ha recibido el trozo de trama con el valor de medida del FLUKE");

            return res;
        }
        public double ReadForeGroundData()
        {
            var res = SCPI.FRD.QueryNumber();
            return res.Value;
        }
        public enum freqSource
        {
            AUT,
            FIX_AMP,
            FIX_VOLT,
        }
        public void SetFrecuencySource(freqSource freqSource)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CONFIGURE, true, freqSource.ToString().Replace("_", " ")));
        }
        public void SetSingleHarmonic(byte harmonic)
        {
            if (harmonic > 50)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO("El valor de prog mayor de 50").Throw();

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_HRM, true, harmonic));
        }
        public enum TypeIntegrator
        {
            ENB,
            DIS,
            RUN,
        }
        public void SetIntegrator(TypeIntegrator typeIntegrator)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_INTEGRATOR, true, typeIntegrator.ToString()));
        }
        public enum Type
        {
            VOLTAGE,
            CURRENT,
        }
        public void SetRangeFix(Type typeRange, byte rang)
        {
            if (rang > 8)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONFIGURACION_INSTRUMENTO("El valor de rang mayor de 8").Throw();

            var type = typeRange == Type.VOLTAGE ? "VLT" : "AMP";
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RNG_FIX, true, type, rang));
        }
        public void SetRangeAuto(Type typeRange)
        {
            var type = typeRange == Type.VOLTAGE ? "VLT" : "AMP";
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RNG_AUTO, true, type));
        }
        public void SetScaling(Type typeScaling, byte value)
        {
            var type = typeScaling == Type.VOLTAGE ? "VLT" : "AMP";
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_SCALING, true, type, value));
        }
        public enum SelectFunction
        {
            CLR,
            WAT,
            VAS,
            VAR,
            VLT,
            AMP,
            PWF,
            VPK,
            APK,
            VCF,
            ACF,
            WHR,
            VAH,
            VRH,
            AHR,
            APF,
            VHM,
            AHM,
            WHM,
            VDF,
            ADF,
            FRQ,
            VDC,
            ADC,
            VHA,
            AHA,
            FND,
            SER
        }
        public void SetSelectionFunction(SelectFunction function)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_SELECTION_FUNCTION, true, function.ToString()));
        }
        public enum Shunt
        {
            INT,
            EXT
        }
        public void SetShunt(Shunt shunt)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_SHUNT, true, shunt.ToString()));
        }
        public enum Wiring
        {
            _1P2,
            _1P3,
            _3P3,
            _3P4,
            CH3,
            CH2,
            CH1,
        }
        public void SetWiringConfiguration(Wiring wiring)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_WIRING_CONFIG, true, wiring.ToString().Replace("_", "")));
        }

        #region PM100SCPI
        public void Reset()
        {
            SCPI.Reset.Command();
        }
        public string GetIdentification()
        {
            var read = SCPI.Identification.Query();
            Idn = read.Value;
            return Idn;
        }
        public void SetOPC()
        {
            SCPI.OperationComplete.Command();
        }
        public string QueryOPC()
        {
            var result = SCPI.OperationCompleteQuery.Query();
            return result.Value;
        }
        public void Trigger()
        {
            SCPI.TRIGGER.Command();
        }
        public void Wait()
        {
            SCPI.WAIT.Command();
        }
        public void ResetAverage()
        {
            SCPI.RAV.Command();
        }
        public double QuerySelfTest()
        {
           var result = SCPI.SELF_TEST.Query();
            return result.Value;
        }
        public string QueryESR()
        {
            var result = SCPI.ESR.Query();
            return result.Value;
        }
        public string QuerySRE()
        {
            var result = SCPI.SRE.Query();
            return result.Value;
        }
        public string QuerySTB()
        {
            var result = SCPI.STB.Query();
            return result.Value;
        }

        #endregion

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;
            var privateCommand = (PrivateCommands)command.Commands;
            message = StringMessage.Create(enUS, commandStrings[privateCommand], command.Args);

            switch (command.Commands)
            {
                case (int)PrivateCommands.QUERY_CONFIGURE:
                case (int)PrivateCommands.QUERY_CALIBRATION:
                case (int)PrivateCommands.QUERY_FUNCTION:
                case (int)PrivateCommands.QUERY_FUNDAMENTAL:
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    break;
                case (int)PrivateCommands.SET_AVG_AUTO:
                    message = StringMessage.Create(enUS, commandStrings[PrivateCommands.SET_AVG_AUTO]);
                    message.HasResponse = true;
                    response = transport.SendMessage<StringMessage>(message);
                    break;
                default:
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    response = null;
                    break;
            }

            if (message.HasResponse)
                measureReaded = response.Text;
        }
        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
        public Result<string> WriteCommand(string text)
        {
                var cmd = new SCPIMessage { Text = text };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                return "";
        }
    }

    public class PM100SCPI : SCPIDevice
    {
        internal PM100SCPI(PM100 device)
            : base(device)
        {
        }

        public SCPICommand TRIGGER { get { return new SCPICommand(context, "*TRG"); } }
        public SCPICommand WAIT { get { return new SCPICommand(context, "*WAI"); } }
        public SCPICommand RAV { get { return new SCPICommand(context, "RAV"); } }
        public SCPIQueryCommand<double> SELF_TEST { get { return new SCPIQueryCommand<double>(context, "*TST?"); } }
        public SCPIQueryCommand<string> ESR { get { return new SCPIQueryCommand<string>(context, "*ESR"); } }
        public SCPIQueryCommand<string> SRE { get { return new SCPIQueryCommand<string>(context, "*SRE"); } }
        public SCPIQueryCommand<string> STB { get { return new SCPIQueryCommand<string>(context, "*STB"); } }
        public SCPIQueryCommand<string> DSE { get { return new SCPIQueryCommand<string>(context, ":DSE"); } }
        public SCPIQueryCommand<string> DSR { get { return new SCPIQueryCommand<string>(context, ":DSR"); } }
        public SCPIQueryCommand<string> FRD { get { return new SCPIQueryCommand<string>(context, ":FRD"); } }
    }
}
