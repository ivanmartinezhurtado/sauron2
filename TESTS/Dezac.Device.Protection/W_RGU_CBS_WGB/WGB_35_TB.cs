﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.01)]
    public class WGB_35_TB : DeviceBase
    {
        public ModbusDeviceSerialPort Modbus { get; set; }

        public WGB_35_TB()
        {
        }

        public WGB_35_TB(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 19200, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 19200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.UseFunction06 = true;
            Modbus.PerifericNumber = periferic;
        }


        public double ReadFirmwareVersion()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.FIRMWARE) / 100D;
        }

        public Inputs ReadInputs()
        {
            LogMethod();
            return (Inputs)Modbus.ReadRegister((ushort)Registers.INPUTS);
        }

        public int ReadAdjustOffsetCalculate()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ADJUST_OFFSET);
        }

        public int ReadAdjustCalculateDCPositive()
        {
            LogMethod();
            var high = Modbus.ReadRegister((ushort)Registers.ADJUST_POSITIVE_DC_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.ADJUST_POSITIVE_DC_LOW);
            //Consigna = VALOR_HI * 65536 + VALOR_LO           

            return high * 65536 + low;
        }

        public int ReadAdjustCalculateDCNegative()
        {
            LogMethod();
            var high = Modbus.ReadRegister((ushort)Registers.ADJUST_NEGATIVE_DC_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.ADJUST_NEGATIVE_DC_LOW);
            //Consigna = VALOR_HI * 65536 + VALOR_LO            

            return high * 65536 + low;
        }

        public int ReadAdjustCalculateAC()
        {
            LogMethod();
            var high = Modbus.ReadRegister((ushort)Registers.ADJUST_AC_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.ADJUST_AC_LOW);
            //Consigna = VALOR_HI * 65536 + VALOR_LO           

            return high * 65536 + low;
        }

        public Models ReadModel()
        {
            LogMethod();
            return (Models)Modbus.ReadRegister((ushort)Registers.MODEL);
        }

        public int ReadCurrent()
        {
            LogMethod();

            var high = Modbus.ReadRegister((ushort)Registers.CURRENT_RMS_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.CURRENT_RMS_LOW);

            return high * 65536 + low;
        }

        public int ReadCurrentTrigger()
        {
            LogMethod();

            var high = Modbus.ReadRegister((ushort)Registers.CURRENT_TRIGGER_RMS_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.CURRENT_TRIGGER_RMS_LOW);

            return high * 65536 + low;
        }

        public int ReadSerialNumber()
        {
            LogMethod();
            var high = Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_LOW);

            return high * 65536 + low;
        }

        public int ReadBastidor()
        {
            LogMethod();
            var high = Modbus.ReadRegister((ushort)Registers.BASTIDOR_HIGH);
            var low = Modbus.ReadRegister((ushort)Registers.BASTIDOR_LOW);

            return high * 65536 + low;
        }

        public int ReadErrorCode()
        {
            LogMethod();
            return Modbus.ReadRegister((ushort)Registers.ERROR_CODE);
        }


        public void WriteWorkMode(WorkModes mode)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.WORK_MODE, (ushort)mode);
        }

        public void WriteOutput(Outputs output)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.OUTPUTS, (ushort)output);
        }

        public void WriteModel(Models model)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.MODEL, (ushort)model);
        }

        public void WriteSerialNumber(int sn)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER_HIGH, sn);
        }

        public void WriteBastidor(int bastidor)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.BASTIDOR_HIGH, bastidor);
        }

        public void WriteErrorCode(ushort value)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ERROR_CODE, value);
        }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Enums

        public enum Registers
        {
            FIRMWARE = 0x0000,
            CURRENT_RMS_HIGH = 0x0001,
            CURRENT_RMS_LOW = 0x0002,
            CURRENT_TRIGGER_RMS_HIGH = 0x0003,
            CURRENT_TRIGGER_RMS_LOW = 0x0004,

            WORK_MODE = 0x0010,
            OUTPUTS = 0x0011,
            INPUTS = 0x0012,

            ADJUST_OFFSET = 0x0013,
            ADJUST_TRIGGER_HIGH = 0x0014,
            ADJUST_TRIGGER_LOW = 0x0015,
            ADJUST_POSITIVE_DC_HIGH = 0x0016,
            ADJUST_POSITIVE_DC_LOW = 0x0017,
            ADJUST_NEGATIVE_DC_HIGH = 0x0018,
            ADJUST_NEGATIVE_DC_LOW = 0x0019,
            ADJUST_AC_HIGH = 0x001A,
            ADJUST_AC_LOW = 0x001B,

            MODEL = 0x001C,

            SERIAL_NUMBER_HIGH = 0x01D,
            SERIAL_NUMBER_LOW = 0x01E,

            BASTIDOR_HIGH = 0x001F,
            BASTIDOR_LOW = 0x0020,
            ERROR_CODE = 0x0021
        }

        public enum WorkModes
        {
            RESET,
            NORMAL,
            TEST,
            AJUSTE_OFFSET_DC,
            AJUSTE_DC_POSITIVO,
            AJUSTE_DC_NEGATIVO,
            AJUSTE_AC
        }

        public enum Outputs
        {
            ANY = 0,
            LED_POWER = 1,
            LED_TRIP = 2,
            RELAY = 4
        }

        [Flags]
        public enum Inputs
        {
            ANY = 0,
            ERROR_TRANSFORMADOR = 1,
            RESET = 2,
            TEST = 4
        }

        public enum Models
        {
            CI_30,
            CI_300,
            CS_300,
            CI_30_RELE_STANDAR,
            CI_300_RELE_STANDAR,
            CS_300_RELE_STANDAR,
            NULL = 255
        }

        #endregion
        }
    }
