﻿using Dezac.Device.Bluetooth;
using System;

namespace Dezac.Device.Metering
{
    [DeviceVersion(1.02)]
    public class MyCLAMP : DeviceBase
    {
        private MyClampDevice myClamp;

        private const int DEFAULT_GAIN = 13800;

        public MyCLAMP()
        {

        }

        public override void Dispose()
        {
            if (myClamp != null)
                myClamp.Dispose();
        }

        public void FindDevice()
        {
            _logger.Info("Scanning MyClamp bluetooth LE Device...");

            myClamp = null;

            try
            {
                myClamp = MyClampDevice.Find(5000);

                if (myClamp != null)
                    _logger.Info($"MyClamp Device Found at Address {myClamp.BluetoothAddress}");
                else
                {
                    _logger.Info("No device found!");
                    throw new Exception("Error comunications Bluetooth, no device found");
                }
            }
            catch (Exception ex)
            {
                _logger.Info(ex.Message);
                if (ex.InnerException != null)
                    _logger.Info(ex.InnerException.Message);

                if (myClamp != null)
                    myClamp.Dispose();

                throw new Exception(ex.Message);
            }
        }

        private bool GetInfo(MyClampDevice device)
        {
            // Estandard bluetooth
            _logger.Info("Starting pairing...");

            var result = device.PairAsync(MyClampDevice.DEFAULT_PIN);
            if (result)
            {
                _logger.Info("Pairing Ok");

                var name = device.GetDeviceNameAsync();
                _logger.Info($"Device Model: {name}");

                name = device.GetManufacturerNameAsync();
                _logger.Info($"Manufacturer Name: {name}");

                name = device.GetModelNumberAsync();
                _logger.Info($"Model Number: {name}");

                name = device.GetSerialNumberAsync();
                _logger.Info($"Serial Number: {name}");

                name = device.GetHardwareRevisionAsync();
                _logger.Info($"HW Revision: {name}");

                name = device.GetFirmwareRevisionAsync();
                _logger.Info($"Firmware Revision: {name}");

                var value = device.GetBatteryLevelAsync();
                _logger.Info($"Battery Level: {value}");

                // API
                var svalue = device.ReadInputStatusAsync();
                _logger.Info($"Input Status: {svalue}");

                var stringvalue = device.ReadSerialNumberAsync();
                _logger.Info($"Serial Number API: {stringvalue}");

                var uvalue = device.ReadProductNumberAsync();
                _logger.Info($"Product Number API: {uvalue}");

                uvalue = device.ReadProductStatusAsync();
                _logger.Info($"Product Status API: {uvalue}");

                uvalue = device.ReadIntegrityCRCAsync();
                _logger.Info($"Integrity CRC API: {uvalue}");

                uvalue = device.ReadPointsCurrentAsync();
                _logger.Info($"Points Current API: {uvalue}");

                uvalue = device.ReadGainCurrentAsync();
                _logger.Info($"Gain Current API: {uvalue}");

                uvalue = device.ReadOffsetCurrentAsync();
                _logger.Info($"Offset Current API: {uvalue}");

                // Pairing

                uvalue = device.ReadRMSCurrentAsync();

                _logger.Info($"RMS Current API: {uvalue}");

                _logger.Info("Writing Product Number 1...");
                result = device.WriteProductNumberAsync(1);
                if (result)
                {
                    _logger.Info("Write Product Number OK");
                    _logger.Info("Reading Product Number....");

                    uvalue = device.ReadProductNumberAsync();
                    _logger.Info($"Product Number API: {uvalue}");
                }
                else
                    _logger.Info("Write Product Number ERROR");

                var ok = device.WriteTestOutputAsync(2);
                _logger.Info($"Test Output Write Number API: {ok}");

            }
            else
                _logger.Info("Pairing Error!");

            return true;
        }

        private bool GetServices(MyClampDevice device)
        {
            var services = device.GetServicesAsync();
            if (services == null)
                return false;

            var n = string.Format("{0} ({1:X8})", device.Name, device.BluetoothAddress);

            foreach (var service in services)
            {
                var ns = $"Service: {service.Uuid}";

                foreach (var character in service.Characteristics)
                {
                    var cs = $"Characteristic: {character?.Uuid}";
                }
            }

            return true;
        }

        //**********************************************************************
        //*********************************************************************

        #region Read Functions

        public string ReadHardwareVersion()
        {
            LogMethod();
            var versionHard = myClamp.GetHardwareRevisionAsync();
            return versionHard;
        }

        public string ReadFirmwareVersion()
        {
            LogMethod();
            var versionFirmware = myClamp.GetFirmwareRevisionAsync();
            return versionFirmware;
        }

        public bool ReadStatusPulsador()
        {
            LogMethod();
            var result = myClamp.ReadInputStatusAsync();
            if (result == 1)
                return true;
            return false;
        }

        public double ReadCurrent()
        {
            System.Threading.Thread.Sleep(7000);
            LogMethod();
            var result = myClamp.ReadRMSCurrentAsync();
            return result/10.0;               
        }

        public uint ReadGainCurrent()
        {
            LogMethod();
            var result = myClamp.ReadGainCurrentAsync();
            return result;
        }

        public BateryEnum ReadBateryLevel()
        {
            LogMethod();
            var result = myClamp.GetBatteryLevelAsync();

            if (result == 10)
                return BateryEnum.LOW;
            return BateryEnum.HI;
        }

        public string ReadNumSerie()
        {
            LogMethod();
            var ns = myClamp.ReadSerialNumberAsync();
            return ns;
        }

        public ProdStatus ReadProductStatus()
        {
            LogMethod();
            var result = myClamp.ReadProductStatusAsync();
            return (ProdStatus)result;
        }

        #endregion

        #region Write Functions

        public void WriteNumSerie(string value)
        {
            LogMethod();
            var result = myClamp.WriteSerialNumberAsync(value);
        }

        public void WriteLed(Leds value)
        {
            LogMethod();
            var result = myClamp.WriteTestOutputAsync((ushort)value);
        }

        public void WriteGainCurrent(uint value)
        {
            LogMethod();
            var result = myClamp.WriteGainCurrentAsync(value);
        }

        public void WriteProductStatus(ProdStatus value)
        {
            LogMethod();
            var result = myClamp.WriteProductStatusAsync((ushort)value);
        }

        #endregion

        #region Internal Functions

        private void Pairing()
        {
            LogMethod();
            var result = myClamp.PairAsync(MyClampDevice.DEFAULT_PIN);
            System.Threading.Thread.Sleep(500);
        }

        private void Unpairing()
        {
            LogMethod();
            var result = myClamp.UnpairAsync();
            System.Threading.Thread.Sleep(500);
        }

        public Tuple<bool, uint> CalculateAdjust(int delFirst, int initCount, int samples, int interval, double current, Func<uint, bool> adjustValidation)
        {
            uint gain = ReadGainCurrent();
            WriteGainCurrent(DEFAULT_GAIN);

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, 1000000000,
                () =>
                {
                    double[] readCurrents = { ReadCurrent() };
                    return readCurrents;
                },
                (listValues) =>
                {
                    gain = (uint)((listValues.Average(0) / current) * DEFAULT_GAIN);

                    return adjustValidation(gain);
                });

            return Tuple.Create(adjustResult.Item1, gain);
        }

        #endregion

        #region Enums & Dict

        [Flags]
        public enum Leds
        {
            AZUL = 0x01,
            VERDE = 0x04,
            ROJO = 0x02,
        }

        public enum ProdStatus
        {
            NORMAL = 0,
            TEST = 1234,
            DEFAULT = 9999,
        }

        public enum BateryEnum
        {
            HI,
            LOW,
        }

        #endregion
    }
}
