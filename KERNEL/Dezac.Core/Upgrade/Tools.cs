﻿using System;

namespace Dezac.Core.Upgrade
{
    public static class Tools
    {
        const UInt16 m_P_16 = 0xA001;
        static UInt16[] m_crc_tab16 = new UInt16[256];
        static bool m_bTableCRC;

        public static void Bin2Hex(byte[] array1, ref byte[] array2, int iSize)
        {
            int i;
            int j;

            for (i = 0, j = 0; i < iSize; i++)
            {
                array2[j++] = (byte)WhichHex(array1[i] >> 4);
                array2[j++] = (byte)WhichHex(array1[i] & 0x000F);
            }
        }

        public static void Bin2HexChar(byte[] array1, ref byte[] array2, int iSize)
        {
            int i;
            int j;

            for (i = 0, j = 0; i < iSize; i++)
            {
                array2[j++] = (byte)WhichChar(array1[i] >> 4);
                array2[j++] = (byte)WhichChar(array1[i] & 0x000F);
            }
        }

        public static void Hex2Bin(byte[] array1, ref byte[] array2, int iSize)
        {
            Hex2Bin(array1, ref array2, iSize, 0);
        }

        public static void Hex2Bin(byte[] array1, ref byte[] array2, int iSize, int offset2)
        {
            byte aux;
            int i;
            int j;

            for (i = 0, j = 0; i < iSize; i += 2, j++)
            {
                aux = WhichInt((char)array1[i]);
                aux = (byte)(aux << 4);
                aux = (byte)(aux | (WhichInt((char)array1[i + 1])));
                array2[j + offset2] = aux;
            }
        }

        public static char WhichChar(int iValue)
        {
            switch (iValue)
            {
                case 1: return '1';
                case 2: return '2';
                case 3: return '3';
                case 4: return '4';
                case 5: return '5';
                case 6: return '6';
                case 7: return '7';
                case 8: return '8';
                case 9: return '9';
                case 10: return 'A';
                case 11: return 'B';
                case 12: return 'C';
                case 13: return 'D';
                case 14: return 'E';
                case 15: return 'F';
                default: return '0';
            }
        }

        public static byte WhichHex(int iValue)
        {
            switch (iValue)
            {
                case 1: return 0x01;
                case 2: return 0x02;
                case 3: return 0x03;
                case 4: return 0x04;
                case 5: return 0x05;
                case 6: return 0x06;
                case 7: return 0x07;
                case 8: return 0x08;
                case 9: return 0x09;
                case 10: return 0x0A;
                case 11: return 0x0B;
                case 12: return 0x0C;
                case 13: return 0x0D;
                case 14: return 0x0E;
                case 15: return 0x0F;
                default: return 0x00;
            }
        }

        public static byte WhichInt(char iValue)
        {
            switch (iValue)
            {
                case '1': return 1;
                case '2': return 2;
                case '3': return 3;
                case '4': return 4;
                case '5': return 5;
                case '6': return 6;
                case '7': return 7;
                case '8': return 8;
                case '9': return 9;
                case 'A': return 10;
                case 'B': return 11;
                case 'C': return 12;
                case 'D': return 13;
                case 'E': return 14;
                case 'F': return 15;
                default: return 0;
            }
        }


        public static UInt16 GetCRC(byte[] array, int iSize, bool bBin)
        {
            return GetCRC(array, 0, iSize, bBin);
        }

        public static UInt16 GetCRC(byte[] array, int ini, int iSize, bool bBin)
        {
            char hex_val;
            UInt16 crc = 0xFFFF;
            UInt16 crcOut = 0xFFFF;

            if (bBin)
                for (int i = ini; i < iSize; i++)
                    crc = GetCRCLoop(crc, (char)array[i]);
            else
                for (int i = ini; i < iSize; i++)
                {
                    hex_val = (char)((array[i] & '\x0f') << 4);
                    hex_val |= (char)((array[i + 1] & '\x0f'));
                    crc = GetCRCLoop(crc, hex_val);
                }

            crcOut = (UInt16)(crc & 0x00FF);
            crcOut = (UInt16)(crcOut << 8);
            crcOut = (UInt16)(crcOut | (crc >> 8));
            //return crc;
            return crcOut;
        }

        static UInt16 GetCRCLoop(UInt16 crc, char c)
        {
            UInt16 tmp, short_c;

            short_c = (UInt16)(0x00ff & (UInt16)c);
            if (!m_bTableCRC) CreateCRCTable();
            tmp = (UInt16)(crc ^ short_c);
            crc = (UInt16)((crc >> 8) ^ m_crc_tab16[tmp & 0xff]);

            return crc;
        }

        static void CreateCRCTable()
        {
            int i, j;
            UInt16 crc, c;

            for (i = 0; i < 256; i++)
            {
                crc = 0;
                c = (UInt16)i;
                for (j = 0; j < 8; j++)
                {
                    if (((crc ^ c) & 0x0001) != 0) crc = (UInt16)((crc >> 1) ^ m_P_16);
                    else crc = (UInt16)(crc >> 1);
                    c = (UInt16)(c >> 1);
                }
                m_crc_tab16[i] = crc;
            }
            m_bTableCRC = true;
        }
    }
}
