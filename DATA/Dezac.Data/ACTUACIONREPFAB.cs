namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.ACTUACIONREPFAB")]
    public partial class ACTUACIONREPFAB
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ACTUACIONREPFAB()
        {
            RF_ACTUACION = new HashSet<RF_ACTUACION>();
        }

        [Key]
        [StringLength(10)]
        public string IDACTUACION { get; set; }

        [Required]
        [StringLength(50)]
        public string DESCRIPCION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RF_ACTUACION> RF_ACTUACION { get; set; }
    }
}
