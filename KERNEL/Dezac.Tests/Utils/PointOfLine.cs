﻿namespace Dezac.Tests.Utils
{
    public struct PointOfLine
    {
        public double x { get; set; }
        public double y { get; set; }
    }

    public struct GainOffsetOfLine
    {
        public double gain { get; set; }
        public double offset { get; set; }
    }

}
