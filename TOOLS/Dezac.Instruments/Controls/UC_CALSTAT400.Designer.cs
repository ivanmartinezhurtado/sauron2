﻿namespace Dezac.Instruments.Controls
{
    partial class UC_CALSTAT400
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label potenciaActivaL1Label;
            System.Windows.Forms.Label potenciaActivaL2Label;
            System.Windows.Forms.Label potenciaActivaIIILabel;
            System.Windows.Forms.Label tensionL1Label;
            System.Windows.Forms.Label tensionL2Label;
            System.Windows.Forms.Label corrienteL1Label;
            System.Windows.Forms.Label corrienteL2Label;
            System.Windows.Forms.Label cosenoPhiL1Label;
            System.Windows.Forms.Label cosenoPhiL2Label;
            System.Windows.Forms.Label potenciaAparenteL1Label;
            System.Windows.Forms.Label potenciaAparenteL2Label;
            System.Windows.Forms.Label potenciaAparenteIIILabel;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL1Label;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL2Label;
            System.Windows.Forms.Label potenciaCapacitivaIIILabel;
            System.Windows.Forms.Label frecuenciaLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label factorPotenciaL1Label;
            System.Windows.Forms.Label factorPotenciaL2Label;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label corrienteL3Label;
            System.Windows.Forms.Label cosenoPhiL3Label;
            System.Windows.Forms.Label factorPotenciaL3Label;
            System.Windows.Forms.Label potenciaActivaL3Label;
            System.Windows.Forms.Label potenciaReactivaCapicitivaL3Label;
            System.Windows.Forms.Label potenciaAparenteL3Label;
            System.Windows.Forms.Label tensionL3Label;
            this.cosenoPhiL2TextBox = new System.Windows.Forms.TextBox();
            this.cosenoPhiL1TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL2TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL1TextBox = new System.Windows.Forms.TextBox();
            this.tensionL2TextBox = new System.Windows.Forms.TextBox();
            this.tensionL1TextBox = new System.Windows.Forms.TextBox();
            this.btnOn = new System.Windows.Forms.Button();
            this.cmb_Port = new System.Windows.Forms.ComboBox();
            this.pbStatus = new System.Windows.Forms.ProgressBar();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.potenciaActivaIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL1TextBox = new System.Windows.Forms.TextBox();
            this.potenciaCapacitivaIIITextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL2TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL1TextBox = new System.Windows.Forms.TextBox();
            this.frecuenciaTextBox = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.factorPotenciaL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaAparenteL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaReactivaCapicitivaL3TextBox = new System.Windows.Forms.TextBox();
            this.cosenoPhiL3TextBox = new System.Windows.Forms.TextBox();
            this.potenciaActivaL3TextBox = new System.Windows.Forms.TextBox();
            this.corrienteL3TextBox = new System.Windows.Forms.TextBox();
            this.tensionL3TextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.factorPotenciaL2TextBox = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.factorPotenciaL1TextBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bsAllVars = new System.Windows.Forms.BindingSource(this.components);
            this.btnReset = new System.Windows.Forms.Button();
            potenciaActivaL1Label = new System.Windows.Forms.Label();
            potenciaActivaL2Label = new System.Windows.Forms.Label();
            potenciaActivaIIILabel = new System.Windows.Forms.Label();
            tensionL1Label = new System.Windows.Forms.Label();
            tensionL2Label = new System.Windows.Forms.Label();
            corrienteL1Label = new System.Windows.Forms.Label();
            corrienteL2Label = new System.Windows.Forms.Label();
            cosenoPhiL1Label = new System.Windows.Forms.Label();
            cosenoPhiL2Label = new System.Windows.Forms.Label();
            potenciaAparenteL1Label = new System.Windows.Forms.Label();
            potenciaAparenteL2Label = new System.Windows.Forms.Label();
            potenciaAparenteIIILabel = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL1Label = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL2Label = new System.Windows.Forms.Label();
            potenciaCapacitivaIIILabel = new System.Windows.Forms.Label();
            frecuenciaLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            factorPotenciaL1Label = new System.Windows.Forms.Label();
            factorPotenciaL2Label = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            corrienteL3Label = new System.Windows.Forms.Label();
            cosenoPhiL3Label = new System.Windows.Forms.Label();
            factorPotenciaL3Label = new System.Windows.Forms.Label();
            potenciaActivaL3Label = new System.Windows.Forms.Label();
            potenciaReactivaCapicitivaL3Label = new System.Windows.Forms.Label();
            potenciaAparenteL3Label = new System.Windows.Forms.Label();
            tensionL3Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).BeginInit();
            this.SuspendLayout();
            // 
            // potenciaActivaL1Label
            // 
            potenciaActivaL1Label.AutoSize = true;
            potenciaActivaL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL1Label.Location = new System.Drawing.Point(18, 126);
            potenciaActivaL1Label.Name = "potenciaActivaL1Label";
            potenciaActivaL1Label.Size = new System.Drawing.Size(59, 15);
            potenciaActivaL1Label.TabIndex = 0;
            potenciaActivaL1Label.Text = "Pot. Act:";
            // 
            // potenciaActivaL2Label
            // 
            potenciaActivaL2Label.AutoSize = true;
            potenciaActivaL2Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL2Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL2Label.Location = new System.Drawing.Point(18, 126);
            potenciaActivaL2Label.Name = "potenciaActivaL2Label";
            potenciaActivaL2Label.Size = new System.Drawing.Size(59, 15);
            potenciaActivaL2Label.TabIndex = 2;
            potenciaActivaL2Label.Text = "Pot. Act:";
            // 
            // potenciaActivaIIILabel
            // 
            potenciaActivaIIILabel.AutoSize = true;
            potenciaActivaIIILabel.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaIIILabel.ForeColor = System.Drawing.Color.Black;
            potenciaActivaIIILabel.Location = new System.Drawing.Point(16, 254);
            potenciaActivaIIILabel.Name = "potenciaActivaIIILabel";
            potenciaActivaIIILabel.Size = new System.Drawing.Size(75, 15);
            potenciaActivaIIILabel.TabIndex = 6;
            potenciaActivaIIILabel.Text = "Pot. Act III:";
            // 
            // tensionL1Label
            // 
            tensionL1Label.AutoSize = true;
            tensionL1Label.BackColor = System.Drawing.Color.Transparent;
            tensionL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL1Label.ForeColor = System.Drawing.Color.Black;
            tensionL1Label.Location = new System.Drawing.Point(18, 6);
            tensionL1Label.Name = "tensionL1Label";
            tensionL1Label.Size = new System.Drawing.Size(59, 15);
            tensionL1Label.TabIndex = 0;
            tensionL1Label.Text = "Voltage:";
            // 
            // tensionL2Label
            // 
            tensionL2Label.AutoSize = true;
            tensionL2Label.BackColor = System.Drawing.Color.Transparent;
            tensionL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL2Label.ForeColor = System.Drawing.Color.Black;
            tensionL2Label.Location = new System.Drawing.Point(15, 6);
            tensionL2Label.Name = "tensionL2Label";
            tensionL2Label.Size = new System.Drawing.Size(59, 15);
            tensionL2Label.TabIndex = 2;
            tensionL2Label.Text = "Voltage:";
            // 
            // corrienteL1Label
            // 
            corrienteL1Label.AutoSize = true;
            corrienteL1Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL1Label.ForeColor = System.Drawing.Color.Black;
            corrienteL1Label.Location = new System.Drawing.Point(7, 36);
            corrienteL1Label.Name = "corrienteL1Label";
            corrienteL1Label.Size = new System.Drawing.Size(70, 15);
            corrienteL1Label.TabIndex = 0;
            corrienteL1Label.Text = "Corriente:";
            // 
            // corrienteL2Label
            // 
            corrienteL2Label.AutoSize = true;
            corrienteL2Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL2Label.ForeColor = System.Drawing.Color.Black;
            corrienteL2Label.Location = new System.Drawing.Point(7, 36);
            corrienteL2Label.Name = "corrienteL2Label";
            corrienteL2Label.Size = new System.Drawing.Size(70, 15);
            corrienteL2Label.TabIndex = 2;
            corrienteL2Label.Text = "Corriente:";
            // 
            // cosenoPhiL1Label
            // 
            cosenoPhiL1Label.AutoSize = true;
            cosenoPhiL1Label.BackColor = System.Drawing.Color.Transparent;
            cosenoPhiL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL1Label.ForeColor = System.Drawing.Color.Black;
            cosenoPhiL1Label.Location = new System.Drawing.Point(17, 66);
            cosenoPhiL1Label.Name = "cosenoPhiL1Label";
            cosenoPhiL1Label.Size = new System.Drawing.Size(60, 15);
            cosenoPhiL1Label.TabIndex = 0;
            cosenoPhiL1Label.Text = "Cos Phi:";
            // 
            // cosenoPhiL2Label
            // 
            cosenoPhiL2Label.AutoSize = true;
            cosenoPhiL2Label.BackColor = System.Drawing.Color.Transparent;
            cosenoPhiL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL2Label.ForeColor = System.Drawing.Color.Black;
            cosenoPhiL2Label.Location = new System.Drawing.Point(17, 66);
            cosenoPhiL2Label.Name = "cosenoPhiL2Label";
            cosenoPhiL2Label.Size = new System.Drawing.Size(60, 15);
            cosenoPhiL2Label.TabIndex = 2;
            cosenoPhiL2Label.Text = "Cos Phi:";
            // 
            // potenciaAparenteL1Label
            // 
            potenciaAparenteL1Label.AutoSize = true;
            potenciaAparenteL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaAparenteL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaAparenteL1Label.Location = new System.Drawing.Point(8, 186);
            potenciaAparenteL1Label.Name = "potenciaAparenteL1Label";
            potenciaAparenteL1Label.Size = new System.Drawing.Size(69, 15);
            potenciaAparenteL1Label.TabIndex = 0;
            potenciaAparenteL1Label.Text = "Pot. Apar:";
            // 
            // potenciaAparenteL2Label
            // 
            potenciaAparenteL2Label.AutoSize = true;
            potenciaAparenteL2Label.BackColor = System.Drawing.Color.Transparent;
            potenciaAparenteL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL2Label.ForeColor = System.Drawing.Color.Black;
            potenciaAparenteL2Label.Location = new System.Drawing.Point(8, 186);
            potenciaAparenteL2Label.Name = "potenciaAparenteL2Label";
            potenciaAparenteL2Label.Size = new System.Drawing.Size(69, 15);
            potenciaAparenteL2Label.TabIndex = 2;
            potenciaAparenteL2Label.Text = "Pot. Apar:";
            // 
            // potenciaAparenteIIILabel
            // 
            potenciaAparenteIIILabel.AutoSize = true;
            potenciaAparenteIIILabel.BackColor = System.Drawing.Color.Transparent;
            potenciaAparenteIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteIIILabel.ForeColor = System.Drawing.Color.Black;
            potenciaAparenteIIILabel.Location = new System.Drawing.Point(6, 310);
            potenciaAparenteIIILabel.Name = "potenciaAparenteIIILabel";
            potenciaAparenteIIILabel.Size = new System.Drawing.Size(85, 15);
            potenciaAparenteIIILabel.TabIndex = 6;
            potenciaAparenteIIILabel.Text = "Pot. Apar III:";
            // 
            // potenciaReactivaCapicitivaL1Label
            // 
            potenciaReactivaCapicitivaL1Label.AutoSize = true;
            potenciaReactivaCapicitivaL1Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaCapicitivaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL1Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaCapicitivaL1Label.Location = new System.Drawing.Point(4, 156);
            potenciaReactivaCapicitivaL1Label.Name = "potenciaReactivaCapicitivaL1Label";
            potenciaReactivaCapicitivaL1Label.Size = new System.Drawing.Size(73, 15);
            potenciaReactivaCapicitivaL1Label.TabIndex = 0;
            potenciaReactivaCapicitivaL1Label.Text = "Pot. Reac:";
            // 
            // potenciaReactivaCapicitivaL2Label
            // 
            potenciaReactivaCapicitivaL2Label.AutoSize = true;
            potenciaReactivaCapicitivaL2Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaCapicitivaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL2Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaCapicitivaL2Label.Location = new System.Drawing.Point(4, 156);
            potenciaReactivaCapicitivaL2Label.Name = "potenciaReactivaCapicitivaL2Label";
            potenciaReactivaCapicitivaL2Label.Size = new System.Drawing.Size(73, 15);
            potenciaReactivaCapicitivaL2Label.TabIndex = 2;
            potenciaReactivaCapicitivaL2Label.Text = "Pot. Reac:";
            // 
            // potenciaCapacitivaIIILabel
            // 
            potenciaCapacitivaIIILabel.AutoSize = true;
            potenciaCapacitivaIIILabel.BackColor = System.Drawing.Color.Transparent;
            potenciaCapacitivaIIILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaCapacitivaIIILabel.ForeColor = System.Drawing.Color.Black;
            potenciaCapacitivaIIILabel.Location = new System.Drawing.Point(2, 282);
            potenciaCapacitivaIIILabel.Name = "potenciaCapacitivaIIILabel";
            potenciaCapacitivaIIILabel.Size = new System.Drawing.Size(89, 15);
            potenciaCapacitivaIIILabel.TabIndex = 6;
            potenciaCapacitivaIIILabel.Text = "Pot. Reac III:";
            // 
            // frecuenciaLabel
            // 
            frecuenciaLabel.AutoSize = true;
            frecuenciaLabel.BackColor = System.Drawing.Color.Transparent;
            frecuenciaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            frecuenciaLabel.ForeColor = System.Drawing.Color.Black;
            frecuenciaLabel.Location = new System.Drawing.Point(373, 254);
            frecuenciaLabel.Name = "frecuenciaLabel";
            frecuenciaLabel.Size = new System.Drawing.Size(39, 15);
            frecuenciaLabel.TabIndex = 0;
            frecuenciaLabel.Text = "Frec:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.Black;
            label2.Location = new System.Drawing.Point(19, 97);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(70, 13);
            label2.TabIndex = 54;
            label2.Text = "Serial Port:";
            // 
            // factorPotenciaL1Label
            // 
            factorPotenciaL1Label.AutoSize = true;
            factorPotenciaL1Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL1Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL1Label.Location = new System.Drawing.Point(49, 96);
            factorPotenciaL1Label.Name = "factorPotenciaL1Label";
            factorPotenciaL1Label.Size = new System.Drawing.Size(28, 15);
            factorPotenciaL1Label.TabIndex = 0;
            factorPotenciaL1Label.Text = "PF:";
            // 
            // factorPotenciaL2Label
            // 
            factorPotenciaL2Label.AutoSize = true;
            factorPotenciaL2Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL2Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL2Label.Location = new System.Drawing.Point(49, 96);
            factorPotenciaL2Label.Name = "factorPotenciaL2Label";
            factorPotenciaL2Label.Size = new System.Drawing.Size(28, 15);
            factorPotenciaL2Label.TabIndex = 2;
            factorPotenciaL2Label.Text = "PF:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.Black;
            label1.Location = new System.Drawing.Point(89, 4);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(29, 20);
            label1.TabIndex = 55;
            label1.Text = "L1";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = System.Drawing.Color.Transparent;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.ForeColor = System.Drawing.Color.Black;
            label3.Location = new System.Drawing.Point(250, 4);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(29, 20);
            label3.TabIndex = 56;
            label3.Text = "L2";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = System.Drawing.Color.Transparent;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.ForeColor = System.Drawing.Color.Black;
            label4.Location = new System.Drawing.Point(412, 4);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(29, 20);
            label4.TabIndex = 57;
            label4.Text = "L3";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = System.Drawing.Color.Transparent;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.ForeColor = System.Drawing.Color.Black;
            label5.Location = new System.Drawing.Point(16, 148);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(77, 13);
            label5.TabIndex = 60;
            label5.Text = "Interval (ms)";
            // 
            // corrienteL3Label
            // 
            corrienteL3Label.AutoSize = true;
            corrienteL3Label.BackColor = System.Drawing.Color.Transparent;
            corrienteL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            corrienteL3Label.ForeColor = System.Drawing.Color.Black;
            corrienteL3Label.Location = new System.Drawing.Point(8, 36);
            corrienteL3Label.Name = "corrienteL3Label";
            corrienteL3Label.Size = new System.Drawing.Size(70, 15);
            corrienteL3Label.TabIndex = 4;
            corrienteL3Label.Text = "Corriente:";
            // 
            // cosenoPhiL3Label
            // 
            cosenoPhiL3Label.AutoSize = true;
            cosenoPhiL3Label.BackColor = System.Drawing.Color.Transparent;
            cosenoPhiL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cosenoPhiL3Label.ForeColor = System.Drawing.Color.Black;
            cosenoPhiL3Label.Location = new System.Drawing.Point(18, 66);
            cosenoPhiL3Label.Name = "cosenoPhiL3Label";
            cosenoPhiL3Label.Size = new System.Drawing.Size(60, 15);
            cosenoPhiL3Label.TabIndex = 4;
            cosenoPhiL3Label.Text = "Cos Phi:";
            // 
            // factorPotenciaL3Label
            // 
            factorPotenciaL3Label.AutoSize = true;
            factorPotenciaL3Label.BackColor = System.Drawing.Color.Transparent;
            factorPotenciaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            factorPotenciaL3Label.ForeColor = System.Drawing.Color.Black;
            factorPotenciaL3Label.Location = new System.Drawing.Point(50, 96);
            factorPotenciaL3Label.Name = "factorPotenciaL3Label";
            factorPotenciaL3Label.Size = new System.Drawing.Size(28, 15);
            factorPotenciaL3Label.TabIndex = 4;
            factorPotenciaL3Label.Text = "PF:";
            // 
            // potenciaActivaL3Label
            // 
            potenciaActivaL3Label.AutoSize = true;
            potenciaActivaL3Label.BackColor = System.Drawing.Color.Transparent;
            potenciaActivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaActivaL3Label.ForeColor = System.Drawing.Color.Black;
            potenciaActivaL3Label.Location = new System.Drawing.Point(19, 126);
            potenciaActivaL3Label.Name = "potenciaActivaL3Label";
            potenciaActivaL3Label.Size = new System.Drawing.Size(59, 15);
            potenciaActivaL3Label.TabIndex = 4;
            potenciaActivaL3Label.Text = "Pot. Act:";
            // 
            // potenciaReactivaCapicitivaL3Label
            // 
            potenciaReactivaCapicitivaL3Label.AutoSize = true;
            potenciaReactivaCapicitivaL3Label.BackColor = System.Drawing.Color.Transparent;
            potenciaReactivaCapicitivaL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaReactivaCapicitivaL3Label.ForeColor = System.Drawing.Color.Black;
            potenciaReactivaCapicitivaL3Label.Location = new System.Drawing.Point(5, 156);
            potenciaReactivaCapicitivaL3Label.Name = "potenciaReactivaCapicitivaL3Label";
            potenciaReactivaCapicitivaL3Label.Size = new System.Drawing.Size(73, 15);
            potenciaReactivaCapicitivaL3Label.TabIndex = 4;
            potenciaReactivaCapicitivaL3Label.Text = "Pot. Reac:";
            // 
            // potenciaAparenteL3Label
            // 
            potenciaAparenteL3Label.AutoSize = true;
            potenciaAparenteL3Label.BackColor = System.Drawing.Color.Transparent;
            potenciaAparenteL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            potenciaAparenteL3Label.ForeColor = System.Drawing.Color.Black;
            potenciaAparenteL3Label.Location = new System.Drawing.Point(9, 186);
            potenciaAparenteL3Label.Name = "potenciaAparenteL3Label";
            potenciaAparenteL3Label.Size = new System.Drawing.Size(69, 15);
            potenciaAparenteL3Label.TabIndex = 4;
            potenciaAparenteL3Label.Text = "Pot. Apar:";
            // 
            // tensionL3Label
            // 
            tensionL3Label.AutoSize = true;
            tensionL3Label.BackColor = System.Drawing.Color.Transparent;
            tensionL3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tensionL3Label.ForeColor = System.Drawing.Color.Black;
            tensionL3Label.Location = new System.Drawing.Point(16, 6);
            tensionL3Label.Name = "tensionL3Label";
            tensionL3Label.Size = new System.Drawing.Size(59, 15);
            tensionL3Label.TabIndex = 4;
            tensionL3Label.Text = "Voltage:";
            // 
            // cosenoPhiL2TextBox
            // 
            this.cosenoPhiL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cosenoPhiL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL2", true));
            this.cosenoPhiL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cosenoPhiL2TextBox.Location = new System.Drawing.Point(80, 63);
            this.cosenoPhiL2TextBox.Name = "cosenoPhiL2TextBox";
            this.cosenoPhiL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.cosenoPhiL2TextBox.TabIndex = 3;
            // 
            // cosenoPhiL1TextBox
            // 
            this.cosenoPhiL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cosenoPhiL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL1", true));
            this.cosenoPhiL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cosenoPhiL1TextBox.Location = new System.Drawing.Point(79, 63);
            this.cosenoPhiL1TextBox.Name = "cosenoPhiL1TextBox";
            this.cosenoPhiL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.cosenoPhiL1TextBox.TabIndex = 1;
            // 
            // corrienteL2TextBox
            // 
            this.corrienteL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL2", true));
            this.corrienteL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.corrienteL2TextBox.Location = new System.Drawing.Point(80, 33);
            this.corrienteL2TextBox.Name = "corrienteL2TextBox";
            this.corrienteL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.corrienteL2TextBox.TabIndex = 3;
            // 
            // corrienteL1TextBox
            // 
            this.corrienteL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL1", true));
            this.corrienteL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.corrienteL1TextBox.Location = new System.Drawing.Point(79, 33);
            this.corrienteL1TextBox.Name = "corrienteL1TextBox";
            this.corrienteL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.corrienteL1TextBox.TabIndex = 1;
            // 
            // tensionL2TextBox
            // 
            this.tensionL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL2", true));
            this.tensionL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tensionL2TextBox.Location = new System.Drawing.Point(80, 3);
            this.tensionL2TextBox.Name = "tensionL2TextBox";
            this.tensionL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.tensionL2TextBox.TabIndex = 3;
            // 
            // tensionL1TextBox
            // 
            this.tensionL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL1", true));
            this.tensionL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tensionL1TextBox.Location = new System.Drawing.Point(79, 3);
            this.tensionL1TextBox.Name = "tensionL1TextBox";
            this.tensionL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.tensionL1TextBox.TabIndex = 1;
            // 
            // btnOn
            // 
            this.btnOn.BackColor = System.Drawing.Color.DimGray;
            this.btnOn.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOn.ForeColor = System.Drawing.Color.White;
            this.btnOn.Location = new System.Drawing.Point(9, 257);
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(84, 35);
            this.btnOn.TabIndex = 4;
            this.btnOn.Text = "READ";
            this.btnOn.UseVisualStyleBackColor = false;
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // cmb_Port
            // 
            this.cmb_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Port.FormattingEnabled = true;
            this.cmb_Port.Location = new System.Drawing.Point(13, 113);
            this.cmb_Port.Name = "cmb_Port";
            this.cmb_Port.Size = new System.Drawing.Size(77, 21);
            this.cmb_Port.TabIndex = 0;
            // 
            // pbStatus
            // 
            this.pbStatus.Location = new System.Drawing.Point(103, 363);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(497, 11);
            this.pbStatus.TabIndex = 39;
            this.pbStatus.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // potenciaActivaIIITextBox
            // 
            this.potenciaActivaIIITextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaIII", true));
            this.potenciaActivaIIITextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaActivaIIITextBox.Location = new System.Drawing.Point(93, 251);
            this.potenciaActivaIIITextBox.Name = "potenciaActivaIIITextBox";
            this.potenciaActivaIIITextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaActivaIIITextBox.TabIndex = 7;
            // 
            // potenciaActivaL2TextBox
            // 
            this.potenciaActivaL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL2", true));
            this.potenciaActivaL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaActivaL2TextBox.Location = new System.Drawing.Point(80, 123);
            this.potenciaActivaL2TextBox.Name = "potenciaActivaL2TextBox";
            this.potenciaActivaL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaActivaL2TextBox.TabIndex = 3;
            // 
            // potenciaActivaL1TextBox
            // 
            this.potenciaActivaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL1", true));
            this.potenciaActivaL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaActivaL1TextBox.Location = new System.Drawing.Point(79, 123);
            this.potenciaActivaL1TextBox.Name = "potenciaActivaL1TextBox";
            this.potenciaActivaL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaActivaL1TextBox.TabIndex = 1;
            // 
            // potenciaAparenteIIITextBox
            // 
            this.potenciaAparenteIIITextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaAparenteIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteIII", true));
            this.potenciaAparenteIIITextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaAparenteIIITextBox.Location = new System.Drawing.Point(93, 307);
            this.potenciaAparenteIIITextBox.Name = "potenciaAparenteIIITextBox";
            this.potenciaAparenteIIITextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaAparenteIIITextBox.TabIndex = 7;
            // 
            // potenciaAparenteL2TextBox
            // 
            this.potenciaAparenteL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaAparenteL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL2", true));
            this.potenciaAparenteL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaAparenteL2TextBox.Location = new System.Drawing.Point(80, 183);
            this.potenciaAparenteL2TextBox.Name = "potenciaAparenteL2TextBox";
            this.potenciaAparenteL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaAparenteL2TextBox.TabIndex = 3;
            // 
            // potenciaAparenteL1TextBox
            // 
            this.potenciaAparenteL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaAparenteL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL1", true));
            this.potenciaAparenteL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaAparenteL1TextBox.Location = new System.Drawing.Point(79, 183);
            this.potenciaAparenteL1TextBox.Name = "potenciaAparenteL1TextBox";
            this.potenciaAparenteL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaAparenteL1TextBox.TabIndex = 1;
            // 
            // potenciaCapacitivaIIITextBox
            // 
            this.potenciaCapacitivaIIITextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaCapacitivaIIITextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaCapacitivaIIITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaCapacitivaIII", true));
            this.potenciaCapacitivaIIITextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaCapacitivaIIITextBox.Location = new System.Drawing.Point(93, 279);
            this.potenciaCapacitivaIIITextBox.Name = "potenciaCapacitivaIIITextBox";
            this.potenciaCapacitivaIIITextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaCapacitivaIIITextBox.TabIndex = 7;
            // 
            // potenciaReactivaCapicitivaL2TextBox
            // 
            this.potenciaReactivaCapicitivaL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaCapicitivaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL2", true));
            this.potenciaReactivaCapicitivaL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaReactivaCapicitivaL2TextBox.Location = new System.Drawing.Point(80, 153);
            this.potenciaReactivaCapicitivaL2TextBox.Name = "potenciaReactivaCapicitivaL2TextBox";
            this.potenciaReactivaCapicitivaL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaReactivaCapicitivaL2TextBox.TabIndex = 3;
            // 
            // potenciaReactivaCapicitivaL1TextBox
            // 
            this.potenciaReactivaCapicitivaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaCapicitivaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL1", true));
            this.potenciaReactivaCapicitivaL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaReactivaCapicitivaL1TextBox.Location = new System.Drawing.Point(79, 153);
            this.potenciaReactivaCapicitivaL1TextBox.Name = "potenciaReactivaCapicitivaL1TextBox";
            this.potenciaReactivaCapicitivaL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaReactivaCapicitivaL1TextBox.TabIndex = 1;
            // 
            // frecuenciaTextBox
            // 
            this.frecuenciaTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.frecuenciaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frecuenciaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "Frecuencia", true));
            this.frecuenciaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frecuenciaTextBox.Location = new System.Drawing.Point(417, 251);
            this.frecuenciaTextBox.Name = "frecuenciaTextBox";
            this.frecuenciaTextBox.Size = new System.Drawing.Size(71, 21);
            this.frecuenciaTextBox.TabIndex = 1;
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.BackColor = System.Drawing.Color.DimGray;
            this.checkBox1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.White;
            this.checkBox1.Location = new System.Drawing.Point(9, 206);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(84, 35);
            this.checkBox1.TabIndex = 45;
            this.checkBox1.Text = "LOOP";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtInterval
            // 
            this.txtInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInterval.Location = new System.Drawing.Point(25, 164);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(55, 22);
            this.txtInterval.TabIndex = 46;
            this.txtInterval.Text = "1000";
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(label4);
            this.panel1.Controls.Add(label1);
            this.panel1.Controls.Add(label3);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(potenciaActivaIIILabel);
            this.panel1.Controls.Add(this.potenciaActivaIIITextBox);
            this.panel1.Controls.Add(potenciaCapacitivaIIILabel);
            this.panel1.Controls.Add(potenciaAparenteIIILabel);
            this.panel1.Controls.Add(this.potenciaCapacitivaIIITextBox);
            this.panel1.Controls.Add(this.potenciaAparenteIIITextBox);
            this.panel1.Controls.Add(this.frecuenciaTextBox);
            this.panel1.Controls.Add(frecuenciaLabel);
            this.panel1.Location = new System.Drawing.Point(103, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 343);
            this.panel1.TabIndex = 53;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(tensionL3Label);
            this.panel4.Controls.Add(this.factorPotenciaL3TextBox);
            this.panel4.Controls.Add(this.potenciaAparenteL3TextBox);
            this.panel4.Controls.Add(potenciaAparenteL3Label);
            this.panel4.Controls.Add(this.potenciaReactivaCapicitivaL3TextBox);
            this.panel4.Controls.Add(potenciaReactivaCapicitivaL3Label);
            this.panel4.Controls.Add(potenciaActivaL3Label);
            this.panel4.Controls.Add(factorPotenciaL3Label);
            this.panel4.Controls.Add(this.cosenoPhiL3TextBox);
            this.panel4.Controls.Add(this.potenciaActivaL3TextBox);
            this.panel4.Controls.Add(this.corrienteL3TextBox);
            this.panel4.Controls.Add(cosenoPhiL3Label);
            this.panel4.Controls.Add(this.tensionL3TextBox);
            this.panel4.Controls.Add(corrienteL3Label);
            this.panel4.Location = new System.Drawing.Point(335, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(157, 212);
            this.panel4.TabIndex = 10;
            // 
            // factorPotenciaL3TextBox
            // 
            this.factorPotenciaL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL3", true));
            this.factorPotenciaL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factorPotenciaL3TextBox.Location = new System.Drawing.Point(80, 93);
            this.factorPotenciaL3TextBox.Name = "factorPotenciaL3TextBox";
            this.factorPotenciaL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.factorPotenciaL3TextBox.TabIndex = 5;
            // 
            // potenciaAparenteL3TextBox
            // 
            this.potenciaAparenteL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaAparenteL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaAparenteL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaAparenteL3", true));
            this.potenciaAparenteL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaAparenteL3TextBox.Location = new System.Drawing.Point(80, 183);
            this.potenciaAparenteL3TextBox.Name = "potenciaAparenteL3TextBox";
            this.potenciaAparenteL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaAparenteL3TextBox.TabIndex = 5;
            // 
            // potenciaReactivaCapicitivaL3TextBox
            // 
            this.potenciaReactivaCapicitivaL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaReactivaCapicitivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaReactivaCapicitivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaReactivaCapicitivaL3", true));
            this.potenciaReactivaCapicitivaL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaReactivaCapicitivaL3TextBox.Location = new System.Drawing.Point(80, 153);
            this.potenciaReactivaCapicitivaL3TextBox.Name = "potenciaReactivaCapicitivaL3TextBox";
            this.potenciaReactivaCapicitivaL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaReactivaCapicitivaL3TextBox.TabIndex = 5;
            // 
            // cosenoPhiL3TextBox
            // 
            this.cosenoPhiL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cosenoPhiL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cosenoPhiL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CosenoPhiL3", true));
            this.cosenoPhiL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cosenoPhiL3TextBox.Location = new System.Drawing.Point(80, 63);
            this.cosenoPhiL3TextBox.Name = "cosenoPhiL3TextBox";
            this.cosenoPhiL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.cosenoPhiL3TextBox.TabIndex = 5;
            // 
            // potenciaActivaL3TextBox
            // 
            this.potenciaActivaL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.potenciaActivaL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potenciaActivaL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "PotenciaActivaL3", true));
            this.potenciaActivaL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potenciaActivaL3TextBox.Location = new System.Drawing.Point(80, 123);
            this.potenciaActivaL3TextBox.Name = "potenciaActivaL3TextBox";
            this.potenciaActivaL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.potenciaActivaL3TextBox.TabIndex = 5;
            // 
            // corrienteL3TextBox
            // 
            this.corrienteL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.corrienteL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.corrienteL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "CorrienteL3", true));
            this.corrienteL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.corrienteL3TextBox.Location = new System.Drawing.Point(80, 33);
            this.corrienteL3TextBox.Name = "corrienteL3TextBox";
            this.corrienteL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.corrienteL3TextBox.TabIndex = 5;
            // 
            // tensionL3TextBox
            // 
            this.tensionL3TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tensionL3TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tensionL3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "TensionL3", true));
            this.tensionL3TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tensionL3TextBox.Location = new System.Drawing.Point(80, 3);
            this.tensionL3TextBox.Name = "tensionL3TextBox";
            this.tensionL3TextBox.Size = new System.Drawing.Size(71, 21);
            this.tensionL3TextBox.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(tensionL2Label);
            this.panel3.Controls.Add(this.factorPotenciaL2TextBox);
            this.panel3.Controls.Add(factorPotenciaL2Label);
            this.panel3.Controls.Add(this.potenciaAparenteL2TextBox);
            this.panel3.Controls.Add(potenciaAparenteL2Label);
            this.panel3.Controls.Add(this.potenciaReactivaCapicitivaL2TextBox);
            this.panel3.Controls.Add(potenciaReactivaCapicitivaL2Label);
            this.panel3.Controls.Add(this.cosenoPhiL2TextBox);
            this.panel3.Controls.Add(cosenoPhiL2Label);
            this.panel3.Controls.Add(potenciaActivaL2Label);
            this.panel3.Controls.Add(this.corrienteL2TextBox);
            this.panel3.Controls.Add(corrienteL2Label);
            this.panel3.Controls.Add(this.potenciaActivaL2TextBox);
            this.panel3.Controls.Add(this.tensionL2TextBox);
            this.panel3.Location = new System.Drawing.Point(173, 32);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(157, 212);
            this.panel3.TabIndex = 9;
            // 
            // factorPotenciaL2TextBox
            // 
            this.factorPotenciaL2TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL2", true));
            this.factorPotenciaL2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factorPotenciaL2TextBox.Location = new System.Drawing.Point(80, 93);
            this.factorPotenciaL2TextBox.Name = "factorPotenciaL2TextBox";
            this.factorPotenciaL2TextBox.Size = new System.Drawing.Size(71, 21);
            this.factorPotenciaL2TextBox.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(tensionL1Label);
            this.panel2.Controls.Add(this.factorPotenciaL1TextBox);
            this.panel2.Controls.Add(factorPotenciaL1Label);
            this.panel2.Controls.Add(this.potenciaAparenteL1TextBox);
            this.panel2.Controls.Add(potenciaAparenteL1Label);
            this.panel2.Controls.Add(this.potenciaReactivaCapicitivaL1TextBox);
            this.panel2.Controls.Add(potenciaReactivaCapicitivaL1Label);
            this.panel2.Controls.Add(this.cosenoPhiL1TextBox);
            this.panel2.Controls.Add(cosenoPhiL1Label);
            this.panel2.Controls.Add(this.corrienteL1TextBox);
            this.panel2.Controls.Add(corrienteL1Label);
            this.panel2.Controls.Add(this.tensionL1TextBox);
            this.panel2.Controls.Add(potenciaActivaL1Label);
            this.panel2.Controls.Add(this.potenciaActivaL1TextBox);
            this.panel2.Location = new System.Drawing.Point(13, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(157, 212);
            this.panel2.TabIndex = 8;
            // 
            // factorPotenciaL1TextBox
            // 
            this.factorPotenciaL1TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.factorPotenciaL1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.factorPotenciaL1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsAllVars, "FactorPotenciaL1", true));
            this.factorPotenciaL1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factorPotenciaL1TextBox.Location = new System.Drawing.Point(79, 93);
            this.factorPotenciaL1TextBox.Name = "factorPotenciaL1TextBox";
            this.factorPotenciaL1TextBox.Size = new System.Drawing.Size(71, 21);
            this.factorPotenciaL1TextBox.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Dezac.Instruments.Properties.Resources.HEG;
            this.pictureBox1.Location = new System.Drawing.Point(10, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(84, 72);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 59;
            this.pictureBox1.TabStop = false;
            // 
            // bsAllVars
            // 
            this.bsAllVars.DataSource = typeof(Dezac.Instruments.Controls.UC_MTEWAT.Vars);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.BackgroundImage = global::Dezac.Instruments.Properties.Resources.Actions_exit_icon;
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(25, 326);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(48, 35);
            this.btnReset.TabIndex = 6;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // UC_CALSTAT400
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(label5);
            this.Controls.Add(this.txtInterval);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(label2);
            this.Controls.Add(this.cmb_Port);
            this.Controls.Add(this.btnOn);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnReset);
            this.Name = "UC_CALSTAT400";
            this.Size = new System.Drawing.Size(611, 383);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAllVars)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOn;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ComboBox cmb_Port;
        private System.Windows.Forms.ProgressBar pbStatus;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.BindingSource bsAllVars;
        private System.Windows.Forms.TextBox tensionL2TextBox;
        private System.Windows.Forms.TextBox tensionL1TextBox;
        private System.Windows.Forms.TextBox potenciaActivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaActivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL1TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL2TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL1TextBox;
        private System.Windows.Forms.TextBox corrienteL2TextBox;
        private System.Windows.Forms.TextBox corrienteL1TextBox;
        private System.Windows.Forms.TextBox frecuenciaTextBox;
        private System.Windows.Forms.TextBox potenciaCapacitivaIIITextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL2TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL1TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteIIITextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL2TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL1TextBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox factorPotenciaL2TextBox;
        private System.Windows.Forms.TextBox factorPotenciaL1TextBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox factorPotenciaL3TextBox;
        private System.Windows.Forms.TextBox potenciaAparenteL3TextBox;
        private System.Windows.Forms.TextBox potenciaReactivaCapicitivaL3TextBox;
        private System.Windows.Forms.TextBox cosenoPhiL3TextBox;
        private System.Windows.Forms.TextBox potenciaActivaL3TextBox;
        private System.Windows.Forms.TextBox corrienteL3TextBox;
        private System.Windows.Forms.TextBox tensionL3TextBox;
    }
}
