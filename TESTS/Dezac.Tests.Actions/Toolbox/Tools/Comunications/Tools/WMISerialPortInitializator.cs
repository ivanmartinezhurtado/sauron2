﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Views;
using Dezac.Tests.Extensions;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.08)]
    [DesignerAction(Type = typeof(WMISerialPortInitializeActionDescription))]
    public class WMISerialPortInitialize : ActionBase
    {
        [Description("La descripción del dispositivo PnP buscado")]
        [Category("3. Configuration")]
        [Editor(typeof(UIVariableEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SerialPortToFind { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {       
            Execute(null, null);
        }

        public void Execute(string variableName, string serialPortToFind)
        {
            if (!string.IsNullOrEmpty(variableName))
                DataOutput = variableName;

            if (!string.IsNullOrEmpty(serialPortToFind))
                SerialPortToFind = serialPortToFind;

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TEST_BASE").Throw();

            var SerialPortToFindValue = VariablesTools.ResolveValue(SerialPortToFind);

            var comportInfo = testBase.WMISerialPortInitialize(SerialPortToFindValue.ToString());

            testBase.SetVariable(DataOutput.Key, comportInfo);

            testBase.SetSharedVariable(DataOutput.Key, comportInfo);

            var checkPort = new WMICheckSerialPortAction();
            checkPort.SerialPortToCheck = DataOutput.Value.ToString();
            checkPort.Execute();
        }
    }

    public class WMISerialPortInitializeActionDescription : ActionDescription
    {
        public override string Name { get { return "WMIPortSearcher"; } }
        public override string Category { get { return "Tools/Comunications/Tools"; } }
        public override string Description { get{return "Este action inicia un puerto de serie"; } }
        public override Image Icon { get { return Properties.Resources.Tools_icon; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }

}