﻿using Dezac.Device;
using Dezac.Tests.Model;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ReadRegisterDescription))]
    public class ReadRegister : ActionBase
    {
        [Description("Dirección Modbus en decimal")]
        [Category("3. Configuration")]
        public ushort Address { get; set; }

        [Description("Tipo registro Modbus HoldingRegister (03) InputRegister (04)")]
        [Category("3. Configuration")]
        [DefaultValue(TipoRegisterModbus.InputRegister)]
        public TipoRegisterModbus RegisterType { get; set; }

        [Description("Nombre de la instancia del objecto creado")]
        [Category("3. Configuration")]
        public string DeviceName { get; set; }

        [Description("Nombre de la variable donde guardar el valor")]
        [Category("4. Results")]
        public string DataOutput { get; set; }

        [Category("4. Results")]
        [Description("Unidades con las que trabaja el valor a obtener")]
        public ParamUnidad DataOutputUnits { get; set; }

        public override void Execute()
        {
            string name = DeviceName ?? "ModbusDevice";
            var uut = GetVariable(name) as ModbusDevice;

            var result = RegisterType == TipoRegisterModbus.InputRegister ? uut.ReadRegister(Address) : uut.ReadHoldingRegister(Address);
            var resultado = (uint)result;

            if (string.IsNullOrEmpty(DataOutput))
                DataOutput = Address.ToString();

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });

            if (string.IsNullOrEmpty(DataOutput))
                DataOutput = Address.ToString();

            if (testBase != null)
                testBase.Resultado.Set(DataOutput, result.ToString(), DataOutputUnits);
            else
                Context.ResultList.Add(string.Format("{0}={1}", DataOutput, result));

            Context.Variables.AddOrUpdate(DataOutput, result);
        }
    }

    public enum TipoRegisterModbus
    {
        HoldingRegister,
        InputRegister
    }

    public class ReadRegisterDescription : ActionDescription
    {
        public override string Name { get { return "ReadRegister"; } }
        public override string Description { get { return "Lee un registro de modbus"; } }
        public override string Category { get { return "Tools/Comunications/Modbus"; } }
        public override Image Icon { get { return Properties.Resources.port_icon; } }
        public override string Dependencies { get { return "InitModbusDeviceAction;DisposedModbusDeviceAction:2"; } }
    }


}
