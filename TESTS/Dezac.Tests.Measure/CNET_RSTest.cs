﻿using Dezac.Device.Measure;
using Dezac.Tests.Model;
using Dezac.Tests.Extensions;
using Dezac.Tests.Parameters;
using Dezac.Tests.UserControls;
using Instruments.Router;
using System;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Dezac.Tests.Measure
{
    [TestVersion(1.00)]
    public class CNET_RSTest:TestBase
    {
        private CNET_RS cnet;
        public CNET_RS Cnet
        {
            get
            {
                if (cnet == null)
                    cnet = new CNET_RS(Comunicaciones.SerialPort, Comunicaciones.Periferico, Comunicaciones.BaudRate);

                return cnet;
            }
        }

        public void TestCommunications()
        {
            string version = "";

            this.ShowDialogMessage("CONECTE EL CABLE DE ALIMENTACION", "Conecte el cable de alimentacion para encender el equipo", 100, 200);

            SamplerWithCancel((p) => { version = Cnet.ReadVersion(); return true; }, "Error de comunicaciones", 3, 500, 500, false, false);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version.Trim(), Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware incorrecta, por favor actualice el equipo"), ParamUnidad.SinUnidad);
        }

        public void TestDefaultSetup()
        {
            Assert.AreEqual("FLASH", Cnet.ReadFlash(), Configuracion.GetString("FLASH ID", "0000", ParamUnidad.SinUnidad), Error().UUT.CONFIGURACION.SETUP("No se guardo correctamente la IP"), ParamUnidad.SinUnidad);

            Cnet.WriteFlashFormat();

            Cnet.WriteIP(Comunicaciones.IP); 

            Cnet.WriteSaveConfig();

            this.ShowDialogMessage("DESCONECTE EL CABLE DE ALIMENTACION", "Desconecte el cable de alimentacion para apagar el equipo", 100, 200);

            Delay(2000, "Apagando el equipo");

            this.ShowDialogMessage("CONECTE EL CABLE DE ALIMENTACION", "Conecte el cable de alimentacion para encender el equipo", 100, 200);

            Delay(6000, "Encendiendo el equipo");

            string ip = Cnet.ReadIP();

            Assert.IsTrue("IP", ip.Contains(Comunicaciones.IP), Error().UUT.CONFIGURACION.SETUP("No se guardo correctamente la IP"));
        }

        public void TestEthernetComunications()
        {
            var router = Router.Default();

            string ip = Comunicaciones.IP.Split(' ').FirstOrDefault();

            Assert.IsTrue("COMUNICACIONES ETHERNET", router.PingIP(ip), Error().UUT.COMUNICACIONES.TIME_OUT("Error: El equipo no comunica por ethernet"));

            SetVariable("IPClienTFTPtDevice", ip); // Para hacer la subida del XML

            var imageView = new ImageView("Comprobar que parpadean los LEDs de Ethernet", string.Format("{0}CNET_081\\01\\08101002.jpg", this.PathCameraImages));
            var frm = this.ShowForm(() => imageView, MessageBoxButtons.OKCancel);
            try
            {
                SamplerWithCancel((s) =>
                {
                    if (s == 10)
                    {
                        var diagResult = Shell.MsgBox("¿DESEA CONTINUAR CON EL TEST DE LEDS?", "TEST DE LEDS", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (diagResult == DialogResult.No)
                            throw new Exception("Error: LED ETH no se enciende");
                    }

                    router.PingIP(ip);
                    return frm.DialogResult == DialogResult.OK;
                }, "Error: LED Ethernet no se enciende", 20, 2000);
            }
            finally
            {
                this.CloseForm(frm);
            }

            Resultado.Set("LEDs ETHERNET", "OK", ParamUnidad.SinUnidad);
        }

        public void TestXMLModification()
        {
            var xml = XElement.Load(ConfigurationManager.AppSettings["PathFileBinary"] + "Factory_Data.xml");

            xml.SetAttributeValue("Bastidor", TestInfo.NumBastidor);
            xml.SetAttributeValue("NumSerie", TestInfo.NumSerie);
            xml.SetAttributeValue("CodeError", "0");

            xml.Save(ConfigurationManager.AppSettings["PathFileBinary"] + "Factory_Data.xml");
        }

        public void TestFinalSetup()
        {
            Delay(3000, "Cargando configuracion desde XML");

            var ip = Cnet.ReadIP();

            Assert.IsTrue(ip.Contains(Parametrizacion.GetString("IP_CLIENTE","0.0.0.0", ParamUnidad.SinUnidad) ), Error().UUT.CONFIGURACION.SETUP("No se ha guardado la ip cliente del xml"));

            Resultado.Set("IP_CLIENTE", ip, ParamUnidad.SinUnidad);

            string mac = Cnet.ReadMAC();

            if (mac.Contains("FF-FF-FF-FF-FF-FF"))
            {
                Cnet.WriteMAC(TestInfo.NumMAC);

                mac = Cnet.ReadMAC();

                Assert.IsTrue(mac.Contains(TestInfo.NumMAC),Error().UUT.CONFIGURACION.SETUP("No se ha guardado la MAC"));
            }

            TestInfo.NumMAC = mac;

            Resultado.Set("MAC", mac, ParamUnidad.SinUnidad);

            this.ShowDialogMessage("DESCONECTE EL CABLE DE ALIMENTACION", "Desconecte el cable de alimentacion para apagar el equipo", 100, 200);
        }

        public void TestFinish()
        {
            if (Cnet != null)
                Cnet.Dispose();
        }

    }
}
