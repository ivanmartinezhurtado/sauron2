﻿using Force.DeepCloner;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TaskRunner.Enumerators;
using TaskRunner.Model;
using TaskRunner.Tools;

namespace TaskRunner
{
    public class SequenceRunner
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SequenceRunner));   

        private static object syncObject = new object();
        private bool runOnlyOne = false;
        private TaskScheduler ts;
        private SynchronizationContext sc;
        private List<CancellationTokenSource> cts;
        private ManualResetEvent syncStepEvent = new ManualResetEvent(false);
        private RunningState runningState;

        public event EventHandler<SequenceContext> TestStart;
        public event EventHandler<SequenceContext> TestEnd;
        public event EventHandler<RunnerTestsInfo> AllTestStarting;
        public event EventHandler<RunnerTestsInfo> AllTestEnded;
        public event EventHandler<StepResult> StepStart;
        public event EventHandler<StepResult> StepPaused;
        public event EventHandler<StepResult> StepEnd;
        public event EventHandler<RunningState> RunningStateChanged;

        public RunningState RunningState
        {
            get { return runningState; }
            private set
            {
                if (runningState != value)
                {
                    runningState = value;
                    if (RunningStateChanged != null)
                        sc.Post(_ => { RunningStateChanged(this, runningState); }, null);
                }
            }
        }
        public ScriptService ScriptService { get; }
        public RunnerTestsInfo RunnerInfo { get; set; }

        public SequenceRunner()
        {
            RunningState = RunningState.Pending;
            ScriptService = new ScriptService();
            cts = new List<CancellationTokenSource>();
        }

        public void Play(Func<int, SharedVariablesList, SequenceContext> context = null, int numInstances = 1)
        {
            sc = SynchronizationContext.Current;
            ts = TaskScheduler.FromCurrentSynchronizationContext();

            if (RunningState == RunningState.Running)
                return;

            RunningState = RunningState.Running;

            cts.Clear();
            syncStepEvent.Set();

            RunnerInfo = new RunnerTestsInfo();
            SharedVariablesList sharedVariables = new SharedVariablesList(0);
            sharedVariables.AddOrUpdate("_RUNNING_INSTANCES_", numInstances);

            int numInstancesRemaining = numInstances;
            for (int i = 0; i < numInstances; i++)
            {
                int numInstance = i + 1;
                SequenceContext _seqContext = context(numInstance, sharedVariables);
                _seqContext.SharedVariables = sharedVariables;
                _seqContext.TotalNumInstances = numInstances;
                _seqContext.InitialTaskScheduler = ts;
                cts.Add(_seqContext.CancellationTokenSource);

                RunnerInfo.Contexts.Add(_seqContext);

                Task task = Task.Factory.StartNew<TestResult>(_ => { return InternalPlay(_seqContext); },
                    _seqContext.CancellationTokenSource.Token, TaskCreationOptions.LongRunning)
                    .ContinueWith((t) =>
                    {
                        lock (syncObject)
                        {
                            numInstancesRemaining--;
                            _seqContext.SharedVariables.AddOrUpdate("_RUNNING_INSTANCES_", numInstancesRemaining);
                            _seqContext.SharedVariables.AddOrUpdate("_INSTANCE_RUNNING_" + t.Result.NumInstance.ToString(), false);
                        }
                        if (numInstancesRemaining == 0)
                        {
                            RunningState = RunningState.Completed;
                            RunnerInfo.Variables = _seqContext.Variables;
                            OnAllTestEnded(RunnerInfo);
                        }
                    });
            }
            OnAllTestStarting(RunnerInfo);
        }
        public void Stop()
        {
            cts.ForEach(ct => ct.Cancel());
            Resume();
        }
        public void Pause()
        {
            if (RunningState == RunningState.Running)
            {
                RunningState = RunningState.Paused;
                syncStepEvent.Reset();
            }
        }
        public void Resume()
        {
            if (RunningState == RunningState.Paused)
            {
                RunningState = RunningState.Running;
                syncStepEvent.Set();
            }
        }
        public void PlayAll()
        {
            Resume();
            runOnlyOne = false;
        }
        public void PlayStep()
        {
            Resume();
            runOnlyOne = true;
        }
        public TestResult InternalPlay(SequenceContext seqContext)
        {
            SequenceContext.Current = seqContext;

            seqContext.SharedVariables.AddOrUpdate("_INSTANCE_RUNNING_" + seqContext.NumInstance.ToString(), true);

            OnTestStart(seqContext);

            RunStep(seqContext.TestResult.InitStepResult, null);

            if (seqContext.TestResult.InitStepResult.ExecutionResult == StepExecutionResult.Passed ||
                seqContext.TestResult.InitStepResult.ExecutionResult == StepExecutionResult.Continued ||
                seqContext.TestResult.InitStepResult.ExecutionResult == StepExecutionResult.Omitted ||
                seqContext.TestResult.InitStepResult.ExecutionResult == StepExecutionResult.Disabled)
                RunStep(seqContext.TestResult.MainStepResult, null);

            RunStep(seqContext.TestResult.EndStepResult, null);

            _logger.InfoFormat(" Test Ended! ({0} ms.)", seqContext.TestResult.Duration.TotalMilliseconds);

            OnTestEnd(seqContext);

            SequenceContext.Current = null;

            return seqContext.TestResult;
        }
        private void RunStep(StepResult stepResult, StepResult parentResult)
        {
            LogicalThreadContext.Properties["NumInstance"] = stepResult.NumInstance;

            if (parentResult != null)
                parentResult.AddChildResult(stepResult);

            LogicalThreadContext.Properties["StepPath"] = stepResult.Path;
            _logger.InfoFormat($"Starting Action {stepResult.Name} in RunMode: {SequenceContext.Current.RunMode}");

            if (!CheckRunStep(stepResult))
                return;

            if (stepResult.Step.BreakPoint || runOnlyOne)
            {
                Pause();
                OnStepPaused(stepResult);
            }
            syncStepEvent.WaitOne();

            OnStepStart(stepResult);

            IActionBase action;
            action = stepResult.Step.Action.DeepClone();

            RunPreAction(action);

            bool? repeat = false;
            do
            {
                RunTestParallelismBehaviour(stepResult, action);

                repeat = CheckRepeatStep(stepResult);
                if (!repeat.HasValue)
                    return;

                if (repeat.Value)
                {
                    stepResult.ExecutionResult = StepExecutionResult.Repeated;
                    LogStepResults(stepResult);

                    int loopIndex = stepResult.LoopIndex + 1;
                    int numInstance = stepResult.NumInstance;
                    SectionEnum section = stepResult.Section;
                    Step step = stepResult.Step;
                    stepResult = new StepResult(numInstance, step, Thread.CurrentThread.ManagedThreadId, section, parentResult, loopIndex);
                    OnStepStart(stepResult);

                    if (parentResult != null)
                        parentResult.AddChildResult(stepResult);

                    LogicalThreadContext.Properties["StepPath"] = stepResult.Path;

                    _logger.InfoFormat($"Starting Action {stepResult.Name} in RunMode {SequenceContext.Current.RunMode} Retry: {stepResult.LoopIndex}");

                    Thread.Sleep(stepResult.Step.TimeIntervalLoop);
                }

            } while (repeat.Value);

            RunPostAction(stepResult, action);
            action = null;

            LogStepResults(stepResult);
        }
        private void RunTestParallelismBehaviour(StepResult stepResult, IActionBase action)
        {
            string signalSufix = VariablesResolver.ReplaceForbiddenCharacters($"{stepResult.Name}_{stepResult.LoopIndex}");
            AggregateException exception = null;
            switch (stepResult.Step.TestParallelismBehaviour)
            {
                case TestParallelismBehaviour.WaitAllAndRunOnlyOnce:

                    exception = RunnerHelper.WaitAllTestsAndRunOnlyOnce("_STEP_RUN_ONLY_ONE_" + signalSufix,
                    () =>
                    {
                        InternalRunStep(stepResult, action);
                        if (stepResult.ExecutionResult == StepExecutionResult.Aborted || stepResult.ExecutionResult == StepExecutionResult.Failed)
                            throw new AggregateException(stepResult.GetChildsExceptions());
                    });

                    if (stepResult.ExecutionResult == StepExecutionResult.Pending)
                    {
                        if (exception != null)
                        {
                            stepResult.Exception = new Exception("Ha fallado otra instancia en un semaforo WaitAllTestsAndRunOnlyOnce.", exception);
                            _logger.Error($"Ha fallado otra instancia en un semaforo WaitAllTestsAndRunOnlyOnce: {exception.InnerException.Message}");
                            stepResult.ExecutionResult = StepExecutionResult.Failed;
                        }
                        else
                            stepResult.ExecutionResult = StepExecutionResult.Omitted;
                    }
                    break;
                case TestParallelismBehaviour.WaitAllAndRunSequentially:

                    RunnerHelper.RunTestSequentiallyOrdered("_STEP_RUN_SEQUENTIALLY_" + signalSufix,
                    () => InternalRunStep(stepResult, action));

                    break;
                case TestParallelismBehaviour.RunOnlyOnce:

                    exception = RunnerHelper.RunOnlyOnce("_STEP_RUN_ONLY_ONE_" + signalSufix,
                    () =>
                    {
                        InternalRunStep(stepResult, action);
                        if (stepResult.ExecutionResult == StepExecutionResult.Aborted || stepResult.ExecutionResult == StepExecutionResult.Failed)
                            throw new AggregateException(stepResult.GetChildsExceptions());
                    });

                    if (stepResult.ExecutionResult == StepExecutionResult.Pending)
                    {
                        if (exception != null)
                        {
                            stepResult.Exception = new Exception("Ha fallado otra instancia en un semaforo RunOnlyOnce.", exception);
                            _logger.Error($"Ha fallado otra instancia en un semaforo RunOnlyOnce: {exception.InnerException.Message}");
                            stepResult.ExecutionResult = StepExecutionResult.Failed;
                        }
                        else
                            stepResult.ExecutionResult = StepExecutionResult.Omitted;
                    }
                    break;
                case TestParallelismBehaviour.RunOnlyLastTest:

                    RunnerHelper.RunOnlyLastTest("_STEP_RUN_LAST_TEST_" + signalSufix,
                    () => InternalRunStep(stepResult, action));

                    if (stepResult.ExecutionResult == StepExecutionResult.Pending)
                        stepResult.ExecutionResult = StepExecutionResult.Omitted;

                    break;
                default:
                    InternalRunStep(stepResult, action);
                    break;
            }
        }
        private void InternalRunStep(StepResult stepResult, IActionBase action)
        {
            if ((stepResult.Section != SectionEnum.End) && (stepResult.CheckCancelationRequest() || SequenceContext.Current.CancellationTokenSource.IsCancellationRequested))
            {
                stepResult.ExecutionResult = StepExecutionResult.Aborted;
                return;
            }

            SequenceContext.Current.Step = stepResult.Step;

            if (stepResult.Step.Steps.Count > 0 && stepResult.Step.Action != null && (!stepResult.Step.Action.IsGroupAction))
            {
                stepResult.ExecutionResult = StepExecutionResult.Failed;
                stepResult.Exception = new Exception("Solo los GroupActions pueden tener Hijos");
            }
            else if (stepResult.Step.Steps.Count == 0)
            {
                if (!RunAction(stepResult, action))
                {
                    if (stepResult.Section == SectionEnum.End ||
                        (SequenceContext.Current.RunMode == RunMode.Testing && stepResult.Section == SectionEnum.Main) ||
                        stepResult.Step.FailAction == PostActionStep.Next)
                        stepResult.ExecutionResult = StepExecutionResult.Continued;
                    else
                        stepResult.ExecutionResult = StepExecutionResult.Failed;
                }
                else
                    stepResult.ExecutionResult = StepExecutionResult.Passed;
            }
            else
            {
                if (RunAction(stepResult, action))
                {
                    if (stepResult.Step.RunChildStepsInParallel)
                        RunStepChildsParallel(stepResult);
                    else
                        RunStepChilds(stepResult);
                }
                else
                    stepResult.ExecutionResult = StepExecutionResult.Failed;
            }

            SequenceContext.Current.Step = stepResult.Step;
            return;
        }
        private bool RunAction(StepResult stepResult, IActionBase action)
        {
            if (action == null)
                return true;

            stepResult.Exception = null;

            try
            {
                action.Execute();
                return true;
            }
            catch (Exception ex)
            {
                stepResult.Exception = ex;
                _logger.Error(ex);
                return false;
            }

        }
        private void RunPreAction(IActionBase action)
        {
            if (action == null)
                return;

            try
            {
                action.PreExecute();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (SequenceContext.Current.RunMode == RunMode.Debug)
            {
                //_logger.Debug($"Memoria Privada: {Process.GetCurrentProcess().PrivateMemorySize64 / 1000000} MB");
                //_logger.Debug($"WorkingSet : {Process.GetCurrentProcess().WorkingSet64 / 1000000} MB");
                //_logger.Debug($"Memoria Virtual: {Process.GetCurrentProcess().VirtualMemorySize64 / 1000000} MB");
            }
        }
        private void RunPostAction(StepResult stepResult, IActionBase action)
        {
            if (action == null)
                return;

            try
            {
                action.PostExecute(stepResult);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (SequenceContext.Current.RunMode == RunMode.Debug)
            {
                //_logger.Debug($"Memoria Privada: {Process.GetCurrentProcess().PrivateMemorySize64 / 1000000} MB");
                //_logger.Debug($"WorkingSet : {Process.GetCurrentProcess().WorkingSet64 / 1000000} MB");
                //_logger.Debug($"Memoria Virtual: {Process.GetCurrentProcess().VirtualMemorySize64 / 1000000} MB");
            }
        }
        private void LogStepResults(StepResult stepResult)
        {
            stepResult.EndTime = DateTime.Now;
            LogicalThreadContext.Properties["StepPath"] = stepResult.Path;
            LogicalThreadContext.Properties["Result"] = stepResult.ExecutionResult.ToString("G");
            LogicalThreadContext.Properties["ExecutionTime"] = stepResult.Duration;
            _logger.InfoFormat($"{stepResult.Name} {stepResult.ExecutionResult} ({stepResult.Duration.TotalMilliseconds} ms)");

            LogicalThreadContext.Properties["Result"] = null;
            LogicalThreadContext.Properties["ExecutionTime"] = null;
            OnStepEnd(stepResult);

            return;
        }
        private bool CheckRunStep(StepResult stepResult)
        {
            try
            {
                if (stepResult.Step == null || !stepResult.Step.Enabled ||
                   (stepResult.Step.RunOnNumInstance.HasValue && stepResult.Step.RunOnNumInstance != stepResult.NumInstance) ||
                   !TestExpression(stepResult.Step.RunConditionExpr))
                {
                    stepResult.ExecutionResult = !stepResult.Step.Enabled ? StepExecutionResult.Disabled : StepExecutionResult.Omitted;
                    LogStepResults(stepResult);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                stepResult.ExecutionResult = StepExecutionResult.Failed;
                stepResult.Exception = ex;
                _logger.Error(ex);
                LogStepResults(stepResult);
                return false;
            }
        }
        private bool? CheckRepeatStep(StepResult stepResult)
        {
            if (cts.Where(ct => ct.IsCancellationRequested).Any())
                return false;

            try
            {
                bool repeat = (stepResult.LoopIndex + 1 < TestExpressionNumber(stepResult.Step.LoopMaxIterations) &&
                    (stepResult.Step.LoopType == LoopType.Always ||
                    (stepResult.Step.LoopType == LoopType.OnSuccess && stepResult.ExecutionResult == StepExecutionResult.Passed) ||
                    (stepResult.Step.LoopType == LoopType.OnFail && stepResult.ExecutionResult == StepExecutionResult.Failed) ||
                    (stepResult.Step.LoopType == LoopType.OnFail && stepResult.ExecutionResult == StepExecutionResult.Continued) ||
                    (stepResult.Step.LoopType == LoopType.OnCondition && TestExpression(stepResult.Step.LoopConditionExpr))));

                return repeat;
            }
            catch (Exception ex)
            {
                stepResult.ExecutionResult = StepExecutionResult.Failed;
                _logger.Error(ex);
                LogStepResults(stepResult);
                return null;
            }
        }
        private void RunStepChildsParallel(StepResult stepResult)
        {
            ParallelLoopResult resultParallel = Parallel.ForEach(
                 stepResult.Step.Steps,
                 (child) =>
                 {
                     StepResult childResult = new StepResult(stepResult.NumInstance, child, Thread.CurrentThread.ManagedThreadId, stepResult.Section, stepResult, 0);
                    
                     RunStep(childResult, stepResult);
                
                     if (childResult.ExecutionResult == StepExecutionResult.Failed || childResult.ExecutionResult == StepExecutionResult.Aborted)
                         stepResult.CancelationRequest = true;

                     try
                     {
                         if (!string.IsNullOrEmpty(stepResult.Step.StepOKOnCondition) && ScriptService.Evaluate<bool>(stepResult.Step.StepOKOnCondition))
                             stepResult.CancelationRequest = true;
                     }
                     catch { }

                     try
                     {
                         if (!string.IsNullOrEmpty(stepResult.Step.StepKOOnCondition) && ScriptService.Evaluate<bool>(stepResult.Step.StepKOOnCondition))
                             stepResult.CancelationRequest = true;
                         ;
                     }
                     catch { }
                 }
            );

            if (stepResult.Section != SectionEnum.End && SequenceContext.Current.CancellationTokenSource.IsCancellationRequested)
            {
                stepResult.ExecutionResult = StepExecutionResult.Aborted;
                return;
            }

            try
            {
                if (!string.IsNullOrEmpty(stepResult.Step.StepOKOnCondition) && ScriptService.Evaluate<bool>(stepResult.Step.StepOKOnCondition))
                {
                    stepResult.ExecutionResult = StepExecutionResult.Passed;
                    return;
                }
            }
            catch (Exception ex) { _logger.Warn($"Error evaluando la expresion StepOKOnCondition: {ex}"); }

            try
            {
                if (!string.IsNullOrEmpty(stepResult.Step.StepKOOnCondition) && ScriptService.Evaluate<bool>(stepResult.Step.StepKOOnCondition))
                {
                    stepResult.ExecutionResult = StepExecutionResult.Failed;
                    return;
                }
            }
            catch (Exception ex) { _logger.Warn($"Error evaluando la expresion StepKOOnCondition: {ex}"); }

            if (stepResult.ChildResults.Where((child) => child.ExecutionResult == StepExecutionResult.Aborted || child.ExecutionResult == StepExecutionResult.Failed).Any())
            {
                if (stepResult.Section == SectionEnum.End ||
                    (SequenceContext.Current.RunMode == RunMode.Testing && stepResult.Section == SectionEnum.Main) ||
                    stepResult.Step.FailAction == PostActionStep.Next)
                    stepResult.ExecutionResult = StepExecutionResult.Continued;
                else
                    stepResult.ExecutionResult =
                        stepResult.ChildResults.Where((child) => child.ExecutionResult == StepExecutionResult.Failed).Any() ?
                            StepExecutionResult.Failed : StepExecutionResult.Aborted;
            }
            else
                stepResult.ExecutionResult = StepExecutionResult.Passed;
        }
        private void RunStepChilds(StepResult stepResult)
        {
            foreach (Step childStep in stepResult.Step.Steps)
            {
                StepResult childResult = new StepResult(stepResult.NumInstance, childStep, Thread.CurrentThread.ManagedThreadId, stepResult.Section, stepResult, 0);
                RunStep(childResult, stepResult);

                if (stepResult.Section != SectionEnum.End && SequenceContext.Current.CancellationTokenSource.IsCancellationRequested)
                {
                    stepResult.ExecutionResult = StepExecutionResult.Aborted;
                    break;
                }
                try
                {
                    if (!string.IsNullOrEmpty(stepResult.Step.StepOKOnCondition) && ScriptService.Evaluate<bool>(stepResult.Step.StepOKOnCondition))
                        break;
                }
                catch (Exception ex) { _logger.Warn($"Error evaluando la expresion StepOKOnCondition: {ex}"); }
                try
                {
                    if (!string.IsNullOrEmpty(stepResult.Step.StepKOOnCondition) && ScriptService.Evaluate<bool>(stepResult.Step.StepKOOnCondition))
                    {
                        stepResult.ExecutionResult = StepExecutionResult.Failed;
                        break;
                    }
                }
                catch (Exception ex) { _logger.Warn($"Error evaluando la expresion StepKOOnCondition: {ex}"); }
                if (stepResult.ChildResults.Where((child) => child.ExecutionResult == StepExecutionResult.Aborted || child.ExecutionResult == StepExecutionResult.Failed).Any())
                {
                    if (stepResult.Section == SectionEnum.End ||
                        (SequenceContext.Current.RunMode == RunMode.Testing && stepResult.Section == SectionEnum.Main) ||
                        stepResult.Step.FailAction == PostActionStep.Next)
                        stepResult.ExecutionResult = StepExecutionResult.Continued;
                    else
                        stepResult.ExecutionResult =
                            stepResult.ChildResults.Where((child) => child.ExecutionResult == StepExecutionResult.Failed).Any() ?
                                StepExecutionResult.Failed : StepExecutionResult.Aborted;

                    break;
                }
            }
            if (stepResult.ExecutionResult == StepExecutionResult.Pending)
                stepResult.ExecutionResult = StepExecutionResult.Passed;
        }
        private int TestExpressionNumber(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                return 0;

            if (int.TryParse(expression, out int intValue))
                return intValue;

            return (int)ScriptService.Evaluate<double>(expression);
        }
        private bool TestExpression(string expression)
        {
            if (string.IsNullOrEmpty(expression))
                return true;

            return ScriptService.Evaluate<bool>(expression);
        }

        #region Eventos
        private void OnTestStart(SequenceContext sequenceContext) => TestStart?.Invoke(this, sequenceContext);
        private void OnTestEnd(SequenceContext testResult) => TestEnd?.Invoke(this, testResult);
        private void OnAllTestStarting(RunnerTestsInfo info) => AllTestStarting?.Invoke(this, info);
        private void OnAllTestEnded(RunnerTestsInfo info)
        {
            if (AllTestEnded != null)
                sc.Post(_ => { AllTestEnded(this, info); }, null);
        }
        private void OnStepStart(StepResult stepResult) => StepStart?.Invoke(this, stepResult);
        private void OnStepPaused(StepResult stepResult) => StepPaused?.Invoke(this, stepResult);
        private void OnStepEnd(StepResult stepResult) => StepEnd?.Invoke(this, stepResult);
        #endregion
    }
}
