﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace Dezac.Device.Metering.SOAP
{
    public class CompactDctSoapService
    {
        protected static readonly ILog _logger = LogManager.GetLogger("CompactDctSoapService");

        private CompactDcSoapTransport _soap;
        public string VersionXml;

        public CompactDctSoapService(string version)
        {
            VersionXml = version;
        }

        public CompactDctSoapService()
        {
            VersionXml = "3.1.c";
        }

        protected CompactDcSoapTransport Soap
        {
            get
            {
                if (_soap == null)
                    _soap = new CompactDcSoapTransport();

                return _soap;
            }
        }

        public class configuration
        {
            private DateTime _fecha;

            public string SetFecha
            {
                set
                {
                    int Año = Convert.ToInt32(value.Substring(0, 4));
                    int mes = Convert.ToInt32(value.Substring(4, 2));
                    int dia = Convert.ToInt32(value.Substring(6, 2));
                    int hora = Convert.ToInt32(value.Substring(8, 2));
                    int min = Convert.ToInt32(value.Substring(10, 2));
                    int seg = Convert.ToInt32(value.Substring(12, 2));
                    _fecha = new DateTime(Año, mes, dia, hora, min, seg);
                }
            }

            public DateTime Fecha
            {
                get
                {
                    return _fecha;
                }
            }
            public string Cnc { get; set; }
            public Info info { get; set; }
            public Comunication comunication { get; set; }
            public Stg stg { get; set; }
            public Ftp ftp { get; set; }
            public Meters meters { get; set; }
            public Time time { get; set; }
            public List<Tareas> tareas { get; set; }
            public List<Module> modulos { get; set; }
        }

        public class Info
        {
            public string Mod { get; set; }
            public string Af { get; set; }
            public string Te { get; set; }
            public string Location { get; set; }
            public string Vf { get; set; }
            public string VfComm { get; set; }
            public string Pro { get; set; }
            public string Com { get; set; }
            public string Bat { get; set; }
            public string Macplc { get; set; }
            public string Pse { get; set; }
        }

        public class Comunication
        {
            public string PrimeInyection { get; set; }
            public string ipCom { get; set; }
            public string ipMask { get; set; }
            public string ipGtw { get; set; }
            public string ipDhcp { get; set; }
            public string ipLoc { get; set; }
            public string ipMaskLoc { get; set; }
            public string PrimaryDns { get; set; }
            public string SecondaryDns { get; set; }
            public string DhcpIdentifier { get; set; }
        }

        public class Stg
        {
            public string PortWS { get; set; }
            public string ResetMsg { get; set; }
            public string StgVersion { get; set; }
            public string IPstg { get; set; }
            public string RetryWS { get; set; }
            public string TimeBetwWS { get; set; }
            public string IPstg2 { get; set; }
            public string IPstg3 { get; set; }
            public string ReportFormat { get; set; }
            public string NumMeters { get; set; }
            public string TimeSendReq { get; set; }
        }

        public class Ftp
        {
            public string IPftp { get; set; }
            public string FTPUserReport { get; set; }
            public string DestDirReport { get; set; }
            public string IPftpDCUpg { get; set; }
            public string UserftpDCUpg { get; set; }
            public string IPftpMeterUpg { get; set; }
            public string UserftpMeterUpg { get; set; }
            public string RetryFtp { get; set; }
            public string TimeBetwFtp { get; set; }
            public string IPftp2 { get; set; }
            public string FTPUserReport2 { get; set; }
            public string IPftp3 { get; set; }
            public string FTPUserReport3 { get; set; }
            public string FtpRandomDelay { get; set; }
        }

        public class Meters
        {
            public string TimeOutMeterFwU { get; set; }
            public string TimeDisconMeter { get; set; }
            public string RetryDisconMeter { get; set; }
            public string TimeRetryInterval { get; set; }
            public string RetryDisconMeterOrder { get; set; }
            public string TimeRetryIntervalOrder { get; set; }
            public string MeterRegData { get; set; }
            public string S26Content { get; set; }
            public string ValuesCheckDelay { get; set; }
            public string MaxOrderOutdate { get; set; }
            public string TimeDelayRestart { get; set; }
            public string PLCTimeoutRM { get; set; }
            public string PLCTimeoutF { get; set; }
        }

        public class Time
        {
            public string IPNTP { get; set; }
            public string NTPMaxDeviation { get; set; }
            public string IPNTP2 { get; set; }
            public string NTPSyncInterval { get; set; }
            public string NTPMaxFailure { get; set; }
            public string NTPRecoveryInterval { get; set; }
            public string SyncMeter { get; set; }
            public string TimeDev { get; set; }
            public string TimeDevOrder { get; set; }
            public string TimeZone { get; set; }
        }

        public class Tareas
        {
            public struct tpPro
            {
                public string TpReq;
                public string TpSend;
                public string TpStore;
            }

            public string TpTar { get; set; }
            public string TpPer { get; set; }
            public string TpMet { get; set; }
            public string TpPrio { get; set; }
            public string TpDest { get; set; }
            public string TpHi { get; set; }
            public tpPro TpPro { get; set; }
        }

        public class Module
        {
            public string Id { get; set; }
            public string Mod { get; set; }
            public string Type { get; set; }
            public string Version { get; set; }
            public string Slot { get; set; }
        }
        
        public string ReadConfigurationSaveFile()
        {
            LogMethod();

            var response = Soap.SynchReport("G17");

            if (string.IsNullOrEmpty(response))
                throw new Exception("Error no se ha recibido nada desde el servicio soap al preguntar G17");

            XElement xelement = XElement.Parse(response);

            return xelement.ToString();
        }

        public configuration ReadConfiguration(bool plc1000=false)
        {
            LogMethod();

            var response = Soap.SynchReport("G17");

            if (string.IsNullOrEmpty(response))
                throw new Exception("Error no se ha recibido nada desde el servicio soap al preguntar G17");

            XElement xelement = XElement.Parse(response);

            var fecha = from nm in xelement.Elements("Cnc").Elements("G17")
                        select nm.Attribute("Fh").Value;

            var cnc = from nm in xelement.Elements("Cnc")
                      select nm.Attribute("Id").Value;

            configuration config = new configuration();
            config.SetFecha = fecha.FirstOrDefault();
            config.Cnc = cnc.FirstOrDefault();
            config.info = GetInfoByXML(xelement);
            config.comunication = GetComunicationsByXML(xelement);
            config.ftp = GetFtpByXML(xelement);
            config.meters = GetMetersByXML(xelement);
            config.time = GetTimeByXML(xelement);
            config.stg = GetStgByXML(xelement);

            if(plc1000)
                config.modulos =GetModulePLC1000ByXML(xelement);
            else
                config.modulos = GetModuleByXML(xelement);

            config.tareas = GetTaskByXML(xelement);
            return config;
        }

        public class AlarmInfo
        {
            public int id { get; set; }
            public string spoonEnable { get; set; }
            public string dir { get; set; }
            public string value { get; set; }
            public string name { get; set; }
        }

        public List<AlarmInfo> alarmInfo { get; set; }

        public List<AlarmInfo> ReadAlarms()
        {
            LogMethod();

            var response = Soap.SynchReport("G09");

            if (string.IsNullOrEmpty(response))
                throw new Exception("Error no se ha recibido nada desde el servicio soap al preguntar G09");

            XElement xelement = XElement.Parse(response);

            List<AlarmInfo> alarmas = (from nm in xelement.Elements("Cnc").Elements("G09").Elements("ES")
                                       select new AlarmInfo
                                       {
                                           dir = nm.Attribute("Dir").Value,
                                           id = Convert.ToInt16(nm.Attribute("Id").Value),
                                           name = nm.Attribute("Name").Value,
                                           spoonEnable = nm.Attribute("SponEnable").Value,
                                           value = nm.Attribute("Value").Value
                                       }).ToList();
            return alarmas;
        }

        private Info GetInfoByXML(XElement xelement)
        {
            LogMethod();

            var info = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Info")
                        select new Info
                        {
                            Mod = nm.Attribute("Mod").Value,
                            Af = nm.Attribute("Af").Value,
                            Te = nm.Attribute("Te").Value,
                            Location = nm.Attribute("Location").Value,
                            Vf = nm.Attribute("Vf").Value,
                            VfComm = nm.Attribute("VfComm").Value,
                            Pro = nm.Attribute("Pro").Value,
                            Com = nm.Attribute("Com").Value,
                            Bat = nm.Attribute("Bat").Value,
                            Macplc = nm.Attribute("Macplc").Value,
                            Pse = nm.Attribute("Pse").Value
                        }).ToList();

            if (info.Count == 0)
                throw new Exception("Error, nodo Info del XML incorrecto en la petición G17");

            return info[0];
        }

        private Comunication GetComunicationsByXML(XElement xelement)
        {
            LogMethod();

            var comunications = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Communications")
                                 select new Comunication
                                 {
                                     PrimeInyection = nm.Attribute("PrimeInyection").Value,
                                     ipCom = nm.Attribute("ipCom").Value,
                                     ipMask = nm.Attribute("ipMask").Value,
                                     ipGtw = nm.Attribute("ipGtw").Value,
                                     ipDhcp = nm.Attribute("ipDhcp").Value,
                                     ipLoc = nm.Attribute("ipLoc").Value,
                                     ipMaskLoc = nm.Attribute("ipMaskLoc").Value,
                                     PrimaryDns = nm.Attribute("PrimaryDns").Value,
                                     SecondaryDns = nm.Attribute("SecondaryDns").Value,
                                     DhcpIdentifier = nm.Attribute("DhcpIdentifier").Value
                                 }).ToList();

            if (comunications.Count == 0)
                throw new Exception("Error, nodo Comunications del XML incorrecto en la petición G17");

            return comunications[0];
        }

        private Stg GetStgByXML(XElement xelement)
        {
            LogMethod();

            var stg = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Stg")
                       select new Stg
                       {
                           PortWS = nm.Attribute("PortWS").Value,
                           ResetMsg = nm.Attribute("ResetMsg").Value,
                           StgVersion = nm.Attribute("StgVersion").Value,
                           IPstg = nm.Attribute("IPstg").Value,
                           RetryWS = nm.Attribute("RetryWS").Value,
                           TimeBetwWS = nm.Attribute("TimeBetwWS").Value,
                           IPstg2 = nm.Attribute("IPstg2").Value,
                           IPstg3 = nm.Attribute("IPstg3").Value,
                           ReportFormat = nm.Attribute("ReportFormat").Value,
                           NumMeters = nm.Attribute("NumMeters").Value,
                           TimeSendReq = nm.Attribute("TimeSendReq").Value
                       }).ToList();


            if (stg.Count == 0)
                throw new Exception("Error, nodo stg del XML incorrecto en la petición G17");

            return stg[0];

        }

        private Ftp GetFtpByXML(XElement xelement)
        {
            LogMethod();

            var ftp = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Ftp")
                       select new Ftp
                       {
                           IPftp = nm.Attribute("IPftp").Value,
                           FTPUserReport = nm.Attribute("FTPUserReport").Value,
                           DestDirReport = nm.Attribute("DestDirReport").Value,
                           IPftpDCUpg = nm.Attribute("IPftpDCUpg").Value,
                           UserftpDCUpg = nm.Attribute("UserftpDCUpg").Value,
                           IPftpMeterUpg = nm.Attribute("IPftpMeterUpg").Value,
                           UserftpMeterUpg = nm.Attribute("UserftpMeterUpg").Value,
                           RetryFtp = nm.Attribute("RetryFtp").Value,
                           TimeBetwFtp = nm.Attribute("TimeBetwFtp").Value,
                           FTPUserReport2 = nm.Attribute("FTPUserReport2").Value,
                           IPftp3 = nm.Attribute("IPftp3").Value,
                           FTPUserReport3 = nm.Attribute("FTPUserReport3").Value,
                           FtpRandomDelay = nm.Attribute("FtpRandomDelay").Value,
                       }).ToList();

            if (ftp.Count == 0)
                throw new Exception("Error, nodo Ftp del XML incorrecto en la petición G17");

            return ftp[0];
        }

        private Meters GetMetersByXML(XElement xelement)
        {
            LogMethod();

            var meters = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Meters")
                          select new Meters
                          {
                              TimeOutMeterFwU = nm.Attribute("TimeOutMeterFwU").Value,
                              TimeDisconMeter = nm.Attribute("TimeDisconMeter").Value,
                              RetryDisconMeter = nm.Attribute("RetryDisconMeter").Value,
                              TimeRetryInterval = nm.Attribute("TimeRetryInterval").Value,
                              RetryDisconMeterOrder = nm.Attribute("RetryDisconMeterOrder").Value,
                              TimeRetryIntervalOrder = nm.Attribute("TimeRetryIntervalOrder").Value,
                              MeterRegData = nm.Attribute("MeterRegData").Value,
                              S26Content = nm.Attribute("S26Content").Value,
                              ValuesCheckDelay = nm.Attribute("ValuesCheckDelay").Value,
                              MaxOrderOutdate = nm.Attribute("MaxOrderOutdate").Value,
                              TimeDelayRestart = nm.Attribute("TimeDelayRestart").Value,
                              PLCTimeoutRM = nm.Attribute("PLCTimeoutRM").Value,
                              PLCTimeoutF = nm.Attribute("PLCTimeoutF").Value,
                          }).ToList();

            if (meters.Count == 0)
                throw new Exception("Error, nodo Meters del XML incorrecto en la petición G17");

            return meters[0];
        }

        private Time GetTimeByXML(XElement xelement)
        {
            LogMethod();

            var meters = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Time")
                          select new Time
                          {
                              IPNTP = nm.Attribute("IPNTP").Value,
                              IPNTP2 = nm.Attribute("IPNTP2").Value,
                              NTPMaxDeviation = nm.Attribute("NTPMaxDeviation").Value,
                              NTPMaxFailure = nm.Attribute("NTPMaxFailure").Value,
                              NTPRecoveryInterval = nm.Attribute("NTPRecoveryInterval").Value,
                              NTPSyncInterval = nm.Attribute("NTPSyncInterval").Value,
                              SyncMeter = nm.Attribute("SyncMeter").Value,
                              TimeDev = nm.Attribute("TimeDev").Value,
                              TimeDevOrder = nm.Attribute("TimeDevOver").Value,
                              TimeZone = nm.Attribute("TimeZone").Value,
                          }).ToList();

            if (meters.Count == 0)
                throw new Exception("Error, nodo Meters del XML incorrecto en la petición G17");

            return meters[0];
        }

        private List<Tareas> GetTaskByXML(XElement xelement)
        {
            LogMethod();

            var tareas = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("TP")
                          select new Tareas
                          {
                              TpTar = nm.Attribute("TpTar").Value,
                              TpPer = nm.Attribute("TpPer").Value,
                              TpMet = nm.Attribute("TpMet").Value,
                              TpPrio = nm.Attribute("TpPrio").Value,
                              TpDest = nm.Attribute("TpDest").Value,
                              TpHi = nm.Attribute("TpDest").Value,
                              TpPro = new Tareas.tpPro
                              {
                                  TpReq = nm.Element("TpPro").Attribute("TpReq").Value,
                                  TpSend = nm.Element("TpPro").Attribute("TpSend").Value,
                                  TpStore = nm.Element("TpPro").Attribute("TpStore").Value,
                              }
                          }).ToList();
            return tareas;
        }

        public List<Module> GetModuleByXML(XElement xelement)
        {
            LogMethod();

            var module = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Module")
                          select new Module
                          {
                              Id = nm.Attribute("Id").Value,
                              Mod = nm.Attribute("Mod").Value,
                              Slot = nm.Attribute("Slot").Value,
                              Type = nm.Attribute("Type").Value,
                              Version = nm.Attribute("Version").Value
                          }).ToList();

            return module;
        }

        public List<Module> GetModulePLC1000ByXML(XElement xelement)
        {
            LogMethod();

            var module = (from nm in xelement.Elements("Cnc").Elements("G17").Elements("Module")
                          select new Module
                          {
                              Id = nm.Attribute("Id").Value,
                              Mod = nm.Attribute("Mod").Value,
                              Type = nm.Attribute("Type").Value,
                          }).ToList();

            return module;
        }

        public class ConfigurationPassword
        {
            public string WritePassword { get; set; }
            public string ManagementPassword { get; set; }
            public string UpdatePassword { get; set; }
            public bool SearchPassword { get; set; }

            public Dictionary<string, string> GetDictionayConfiguration
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("CLec", WritePassword);
                    dictionay.Add("CGes", ManagementPassword);
                    dictionay.Add("CAct", UpdatePassword);
                    dictionay.Add("SearchPassword", SearchPassword == true ? "Y" : "N");
                    return dictionay;
                }
            }
        }

        public class ConfigurationInfo
        {
            public string YearManufacture { get; set; }
            public string Cnc { get; set; }
            public string Model { get; set; }
            public string Te { get; set; }

            public Dictionary<string, string> GetDictionayConfiguration
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("Af", YearManufacture);
                    dictionay.Add("Cnc", Cnc);
                    dictionay.Add("Mod", Model);
                    dictionay.Add("Te", Te);
                    return dictionay;
                }
            }
        }

        public class ConfigurationStg
        {
            public string StgVersion { get; set; }

            public Dictionary<string, string> GetDictionayConfiguration
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("StgVersion", StgVersion);
                    return dictionay;
                }
            }
        }

        public class ConfigurationOthers
        {
            public string Client { get; set; }

            public Dictionary<string, string> GetDictionayConfiguration
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("Client", Client);
                    return dictionay;
                }
            }
        }

        public void WriteConfiguration(ConfigurationInfo configuration)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C04", "Info", configuration.GetDictionayConfiguration));
        }

        public void WritePassword(ConfigurationPassword configuration)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C04", "Meters", configuration.GetDictionayConfiguration));
        }

        public void WriteStgVersion(ConfigurationStg configuration)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C04", "Stg", configuration.GetDictionayConfiguration));
        }

        public void WriteOthers(ConfigurationOthers configuration)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C04", "Others", configuration.GetDictionayConfiguration));
        }

        public void WriteDateTimeToTimeZone(string timeZone, DateTime timeZoneLocal)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            var timeZoneDictionaryne = new Dictionary<string, string>();
            timeZoneDictionaryne.Add("TimeZone", timeZone);

            Soap.Order(message.BuildMessageXml("C04", "Time", timeZoneDictionaryne));

            System.Threading.Thread.Sleep(1000);

            var DaylightHour = "W";

            if (timeZoneLocal.IsDaylightSavingTime())
                DaylightHour = "S";

            var dictionay = new Dictionary<string, string>();

            var data = string.Format("{0:00}{1:00}{2:00}{3:00}{4:00}{5:000}00{6}", timeZoneLocal.Year, timeZoneLocal.Month, timeZoneLocal.Day, timeZoneLocal.Hour, timeZoneLocal.Minute, timeZoneLocal.Second, DaylightHour);

            dictionay.Add("date", data);

            message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C06", dictionay));
        }
 
        public void WriteDateTimePCLocal()
        {
            LogMethod();

            var DaylightHour = "W";

            if (DateTime.Today.IsDaylightSavingTime())
                DaylightHour = "S";

            var dictionay = new Dictionary<string, string>();

            var data = string.Format("{0:00}{1:00}{2:00}{3:00}{4:00}{5:000}00{6}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DaylightHour);

            dictionay.Add("date", data);

            var message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C06", dictionay));
        }

        public class ConfigurationModules
        {
            public string Id { get; set; }
            public string Mod { get; set; }
            public string Slot { get; set; }
            public string Type { get; set; }
            //public string Version { get; set; }

            public Dictionary<string, string> GetDictionayModule
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("Id", Id);
                    dictionay.Add("Mod", Mod);
                    dictionay.Add("Slot", Slot);
                    dictionay.Add("Type", Type);
                    // dictionay.Add("Version", Version);
                    return dictionay;
                }
            }
        }

        public void WriteModuleConfiguration(List<ConfigurationModules> modules)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            var modulos = new List<Dictionary<string, string>>();

            foreach (ConfigurationModules mod in modules)
                modulos.Add(mod.GetDictionayModule);
            Soap.Order(message.BuildMessageXml("C04", "Module", modulos));
        }

        public void WriteModuleConfigurationPLC1000(List<ConfigurationModules> modules)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            var modulos = new List<Dictionary<string, string>>();

            foreach (ConfigurationModules mod in modules)
                modulos.Add(mod.GetDictionayModule);

            Soap.Order(message.BuildMessageXml("C04", "Module", modulos));
        }

        public void WriteConfigurationWhitOutInfoAndModuleFromXML(string fileName, bool writeModem3GConfiguration, out bool resetModem)
        {
            LogMethod();

            var dirPath = ConfigurationManager.AppSettings["PathCustomizationFile"];
            var xmlFile = XElement.Load(dirPath + fileName + ".xml");

            var xElement = xmlFile.Element("Cnc").Element("C04");

            if (xElement == null)
                throw new Exception("Error el fichero XML no tiene el formato correcto o no es un fichero de ORDER C04");

            if (xElement.Element("Modem") != null)
            {
                if (!writeModem3GConfiguration)
                    xElement.Element("Modem").Remove();

                resetModem = false;
            }
            else
                resetModem = true;
 
            Soap.Order(xmlFile.ToString());
        }

        public void WriteConfigurationFromXML(string PathfileName)
        {
            LogMethod();

            var xmlFile = XElement.Load(PathfileName);

            var xElement = xmlFile.Element("Cnc").Element("C04");

            if (xElement == null)
                throw new Exception("Error el fichero XML no tiene el formato correcto o no es un fichero de ORDER C04");

            Soap.Order(xmlFile.ToString());
        }

        public void WriteFrameLogMode(FrameLogMode mode)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);
            var frame = new Dictionary<string, string>();
            frame.Add("FrameLogMode",((int)mode).ToString());
            Soap.Order(message.BuildMessageXml("C04", "Frames", frame));
        }

        public enum FrameLogMode
        {
            Disabled,
            Capture,
            Sniffer
        }

        public void WritePasswordAccesAdm(string password)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);
            var frame = new Dictionary<string, string>();
            frame.Add("DCPwdAdm", password);
            Soap.Order(message.BuildMessageXml("C04", "Access", frame));
        }

        public void WriteKeyEncryptationMeters(string serialNumber)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            //< B31 ActDate = "20010101000000000W" CntId = "CIR0140000000" >
            //  < DASec ClientId = "4" Secret = "00000005" >    
            //         < CDTSec KeyId = "5647378" KeyType = "GUnKey" KeyVal = "000102030405060708090A0B0C0D0E0F" />    
            //         < CDTSec KeyId = "64838374" KeyType = "GAuKey" KeyVal = "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF" />      
            //  </ DASec >    
            //</ B31 >

            var modulos = new List<Dictionary<string, string>>();

            var keyncriptationList = new List<KeyEncryptationMeter>();
            var keyEncryptationMeter = new KeyEncryptationMeter()
            {
                KeyId = "5647378",
                KeyType = "GUnKey",
                KeyVal = "000102030405060708090A0B0C0D0E0F",
            };
            keyncriptationList.Add(keyEncryptationMeter);

            var keyEncryptationMeter2 = new KeyEncryptationMeter()
            {
                KeyId = "64838374",
                KeyType = "GAuKey",
                KeyVal = "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF",
            };
            keyncriptationList.Add(keyEncryptationMeter2);

            foreach (KeyEncryptationMeter mod in keyncriptationList)
                modulos.Add(mod.GetKeyEncryptationMeter);

            var attributesParent = new Dictionary<string, string>();
            attributesParent.Add("ActDate", "20010101000000000W");
            attributesParent.Add("CntId", serialNumber);

            var attributesChild = new Dictionary<string, string>();
            attributesChild.Add("ClientId", "4");
            attributesChild.Add("Secret", "00000005");

            Soap.Order(message.BuildMessageXml("B31", attributesParent,"DASec", attributesChild, "CDTSec", modulos));          
        }

        public class KeyEncryptationMeter
        {
            public string KeyId { get; set; }
            public string KeyType { get; set; }
            public string KeyVal { get; set; }

            public Dictionary<string, string> GetKeyEncryptationMeter
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("KeyId", KeyId);
                    dictionay.Add("KeyType", KeyType);
                    dictionay.Add("KeyVal", KeyVal);
                    return dictionay;
                }
            }
        }

        public class AdditionalInfo
        {
            public string Group { get; set; }
            public string Key { get; set; }
            public string Value { get; set; }

            public Dictionary<string, string> GetDictionayModule
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("Group",Group );
                    dictionay.Add("Key", Key);
                    dictionay.Add("Value", Value);   
                    return dictionay;
                }
            }
        }

        public void WriteAdditionalInfo(AdditionalInfo additionalInfo)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);
            Soap.Order(message.BuildMessageXml("C04", "AdditionalInfo", "Parameter", additionalInfo.GetDictionayModule));
        }
        
        public class ConfigurationModem3G
        {
            public string logFrames { get; set; }
            public string pingEnable { get; set; }
            public string pingHost { get; set; }
            public string pingThreshold { get; set; }
            public string pingLength { get; set; }
            public string pingTimeout { get; set; }
            public string pingMeasureInterval { get; set; }
            public string pingInterval { get; set; }
            public string pingAction { get; set; }
            public string pin1 { get; set; }
            public string apn1 { get; set; }
            public string user1 { get; set; }
            public string password1 { get; set; }
            public string authentication1 { get; set; }
            public string pin2 { get; set; }
            public string apn2 { get; set; }
            public string user2 { get; set; }
            public string password2 { get; set; }
            public string authentication2 { get; set; }
            public string sim2mode { get; set; }
            public string sim2interval { get; set; }
            public string sim2retries { get; set; }
            public string externalICMP { get; set; }
            public string externalSSH { get; set; }
            public string ddnsEnable { get; set; }
            public string ddnsHost { get; set; }
            public string ddnsUser { get; set; }
            public string ddnsPassword { get; set; }
            public string dualSim { get; set; }

            public Dictionary<string, string> GetDictionayModem3G
            {
                get
                {
                    var dictionay = new Dictionary<string, string>();
                    dictionay.Add("logFrames", logFrames);
                    dictionay.Add("pingEnable", pingEnable);
                    dictionay.Add("pingHost", pingHost);
                    dictionay.Add("pingThreshold", pingThreshold);
                    dictionay.Add("pingLength", pingLength);
                    dictionay.Add("pingTimeout", pingTimeout);
                    dictionay.Add("pingMeasureInterval", pingMeasureInterval);
                    dictionay.Add("pingInterval", pingInterval);
                    dictionay.Add("pingAction", pingAction);
                    dictionay.Add("pin1", pin1);
                    dictionay.Add("apn1", apn1);
                    dictionay.Add("user1", user1);
                    dictionay.Add("password1", password1);
                    dictionay.Add("authentication1", authentication1);
                    dictionay.Add("pin2", pin2);
                    dictionay.Add("apn2", apn2);
                    dictionay.Add("user2", user2);
                    dictionay.Add("password2", password2);
                    dictionay.Add("authentication2", authentication2);
                    dictionay.Add("sim2mode", sim2mode);
                    dictionay.Add("sim2interval", sim2interval);
                    dictionay.Add("sim2retries", sim2retries);
                    dictionay.Add("externalICMP", externalICMP);
                    dictionay.Add("externalSSH", externalSSH);
                    dictionay.Add("ddnsEnable", ddnsEnable);
                    dictionay.Add("ddnsHost", ddnsHost);
                    dictionay.Add("ddnsUser", ddnsUser);
                    dictionay.Add("ddnsPassword", ddnsPassword);
                    dictionay.Add("dualSim", dualSim);
                    return dictionay;
                }
            }
        }

        public void WriteConfigurationModem3G(ConfigurationModem3G configurationModem3G)
        {
            LogMethod();

            var message = new MessageXmlSoap(VersionXml);

            Soap.Order(message.BuildMessageXml("C04", "Modem", configurationModem3G.GetDictionayModem3G));
        }

        public class devices
        {
            public string MAC { get; set; }
            public bool IsSBT
            {
                get
                {
                    if (MAC == "00:00:00:00:00:00")
                        return true;
                    return false;
                }
            }
            public string MeterID { get; set; }
            public ComStatus ComStatus { get; set; }
        }

        public enum ComStatus
        {
            Conectado = 2,
            Registrado = 1,
        }

        public List<devices> ReadDevice(ComStatus status)
        {
            LogMethod();

            XElement xelement = XElement.Parse(Soap.SynchReport("A02"));

            var devices = (from nm in xelement.Elements("Cnc").Elements("A02").Elements("Meter")
                           select new devices
                           {
                               MeterID = nm.Attribute("MeterId").Value,
                               MAC = nm.Attribute("MAC").Value,
                               ComStatus = (ComStatus)Convert.ToByte(nm.Attribute("ComStatus").Value)
                           }).ToList();

            if (devices.Count == 0)
                SearchSoapXmlError(xelement);

            return devices.Where((p) => p.ComStatus == status).ToList();
        }

        public class Information
        {
            private DateTime _fecha;
            public string SetFecha
            {
                set
                {
                    int Año = Convert.ToInt32(value.Substring(0, 4));
                    int mes = Convert.ToInt32(value.Substring(4, 2));
                    int dia = Convert.ToInt32(value.Substring(6, 2));
                    int hora = Convert.ToInt32(value.Substring(8, 2));
                    int min = Convert.ToInt32(value.Substring(10, 2));
                    int seg = Convert.ToInt32(value.Substring(12, 2));
                    _fecha = new DateTime(Año, mes, dia, hora, min, seg);
                }
            }
            public DateTime Fecha
            {
                get
                {
                    return _fecha;
                }
            }
            public string VL1 { get; set; }
            public string ErrCat { get; set; }
        }

        public class ErrorXML
        {
            public string ErrCode { get; set; }
            public string ErrCat { get; set; }
            public int Code { get { return Convert.ToInt32(ErrCat + ErrCode); } }
            public string FileRequest { get; set; }
        }

        public void TestDeviceComunication(string device)
        {
            LogMethod();

            XElement xelement = XElement.Parse(Soap.SynchReport("S01", device));

            var xElementS01 = xelement.Element("Cnc").Element("Cnt").Elements("S01");

            if (xElementS01 == null)
                throw new Exception("Error comunicaciones al pedir el fichero S01");
        }

        public Information ReadDeviceInformation(string device)
        {
            LogMethod();

            XElement xelement = XElement.Parse(Soap.SynchReport("S01", device));

            var information = (from nm in xelement.Elements("Cnc").Elements("Cnt").Elements("S01")
                               select new Information
                           {
                               SetFecha = nm.Attribute("Fh").Value,
                               VL1 = nm.Attribute("L1v").Value
                           }).ToList();

            if (information.Count == 0)
                SearchSoapXmlError(xelement);

            return information.First();
        }

        public void SearchSoapXmlError(XElement xelement)
        {
            var errorCat = (from nm in xelement.Elements("Cnc").Elements("Cnt")
                            select new ErrorXML
                            {
                                ErrCat = nm.Attribute("ErrCat").Value,
                                ErrCode = nm.Attribute("ErrCode").Value,
                            }).ToList();

            if (errorCat.Count == 1)
                throw new SoapException(errorCat.First().Code);
        }


        //***********************************************
        //******************* IECO **********************
        //************* PLACA SPECTRUM ******************

        public class SpectrumValue
        {
            public string[] MaxString { get; set; }
            public string[] AverageString { get; set; }

            public List<int> Max
            {
                get
                {
                    List<int> max = new List<int>();
                    foreach (string value in MaxString)
                        max.Add(Convert.ToInt32(value));

                    return max;
                }
            }
            public List<int> Average
            {
                get
                {
                    List<int> average = new List<int>();
                    foreach (string value in AverageString)
                        average.Add(Convert.ToInt32(value));

                    return average;
                }
            }
        }

        public SpectrumValue ReadPlacaSpectrum()
        {
            LogMethod();

            var response = Soap.SynchReport("G70");

            if (string.IsNullOrEmpty(response))
                throw new Exception("Error no se ha recibido nada desde el servicio soap al preguntar G70");

            XElement xelement = XElement.Parse(response);

            var Max = from nm in xelement.Elements("Cnc").Elements("G70").Elements("Max")
                        select nm.Attribute("Value").Value;

            var Average = from nm in xelement.Elements("Cnc").Elements("G70").Elements("Average")
                      select nm.Attribute("Value").Value;

            var spectrumValue = new SpectrumValue()
            {
                MaxString = Max.FirstOrDefault().Split(','),
                AverageString = Average.FirstOrDefault().Split(',')
            };

            return spectrumValue;
        }

        protected void LogMethod([CallerMemberName] string name = "")
        {
            _logger.InfoFormat(name);
        }
    }
}
