﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net.Core;
using WinTaskRunner.Utils;
using System.Reflection;
using TaskRunner;
using WinTaskRunner.Services;
using TaskRunner.Model;

namespace WinTaskRunner.Views
{
    public partial class ResultsViewer : UserControl
    {
        protected readonly AppService app;

        private SynchronizationContext sc;

        public ResultsViewer()
        {
            InitializeComponent();
        }

        public ResultsViewer(AppService appService)
            : this()
        {
            this.app = appService;

            this.sc = SynchronizationContext.Current;

            app.Runner.AllTestStarting += (s, e) => { Clear(); };
            app.Runner.TestStart += (s, e) => { 
                SequenceContext.Current.ResultList.ItemAdded += OnResultAdded;                
            };
        }

        public void Clear()
        {
            txtLog.Clear();
        }

        public void AddItem(ResultItem item)
        {
            string text;
            var value = FormatValue(item.Value);

            try
            {
                if (item.NumInstance > 1)
                    text = string.Format("(#{0}) {1} - {2:HH:mm:ss} - {3}{4}", item.NumInstance, item.Step.Name, item.Timestamp, value, Environment.NewLine);
                else
                    text = string.Format("{0} - {1:HH:mm:ss} - {2}{3}", item.Step.Name, item.Timestamp, value, Environment.NewLine);
            }catch(Exception ex)
            {
                text= string.Format("Error, AdItem en ResulViewer por {0}", ex.Message);
            }

            txtLog.AppendText(text);
            txtLog.SelectionStart = txtLog.TextLength;
            txtLog.ScrollToCaret();
        }

        void OnResultAdded(object sender, ResultItem e)
        {
            sc.Post(_ => { this.AddItem(e); }, null);
        }

        private object FormatValue(object value)
        {
            if (value == null)
                return null;

            var type = value.GetType();

            if (!type.IsValueType || type.IsPrimitive)
                return value;

            var text = Environment.NewLine;

            foreach (FieldInfo field in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                var fieldValue = field.GetValue(value);
                if (field.FieldType.IsValueType && !field.FieldType.IsPrimitive)
                    text += string.Format("{0}=[{1}]{2}", field.Name, FormatValue(fieldValue), Environment.NewLine);
                else
                    text += string.Format("{0}={1};", field.Name, fieldValue);
            }

            return text;
        }
    }
}
