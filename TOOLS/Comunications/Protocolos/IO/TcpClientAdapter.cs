using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Comunications.Message
{
    public class TcpClientAdapter : IStreamResource
	{
		private TcpClient _tcpClient;
        private string NewLine = "\r\n";

		public TcpClientAdapter(TcpClient tcpClient, string newLine="\r\n")
		{
			Debug.Assert(tcpClient != null, "Argument tcpClient cannot be null.");

            NewLine = newLine;

			_tcpClient = tcpClient;
		}
      
		public int InfiniteTimeout
		{
			get { return Timeout.Infinite; }
		}

		public int ReadTimeout
		{
			get { return _tcpClient.ReceiveTimeout; }
			set { _tcpClient.ReceiveTimeout = value; }
		}

		public int WriteTimeout
		{
			get { return _tcpClient.SendTimeout; }
			set { _tcpClient.SendTimeout = value; }
		}

		public void Write(byte[] buffer, int offset, int size)
		{
			_tcpClient.GetStream().Write(buffer, offset, size);
		}

		public int Read(byte[] buffer, int offset, int size)
		{
            return _tcpClient.GetStream().Read(buffer, offset, size);
		}

        public string ReadLineString()
        {
            if (!_tcpClient.Connected) return null;
            StringBuilder sb = new StringBuilder();
            bool EndLine = false;
            var endTrama = ASCIIEncoding.ASCII.GetBytes(NewLine);

            while (_tcpClient.Available > 0 && !EndLine)
            {
                int input = _tcpClient.GetStream().ReadByte();
                EndLine = input == (int)endTrama[0] ? true : false;
                sb.Append((char)input);              
            }
            return sb.ToString();
        }

		public void DiscardInBuffer()
		{
			_tcpClient.GetStream().Flush();
		}

        public void HandleMessageException(Exception ex)
        {
            try
            {
                var e = ex.InnerException ?? ex;

                if (e is SocketException)
                {
                    if (((SocketException)e).SocketErrorCode == SocketError.TimedOut)
                        return;

                    var endPoint = (IPEndPoint) _tcpClient.Client.RemoteEndPoint;
                    
                    if (_tcpClient != null)
                        _tcpClient.Close();

                    _tcpClient = new TcpClient(endPoint.Address.ToString(), endPoint.Port);
                }
            } catch
            {

            }
        }

		public void Dispose()
		{
			GC.SuppressFinalize(this);
            if (_tcpClient != null)
                _tcpClient.Close();
		}

        public void WriteLineString(string tx)
        {
            tx = tx + NewLine;
            byte[] byteTx = System.Text.Encoding.ASCII.GetBytes(tx);
            Write(byteTx, 0, byteTx.Length);
        }

        public void WriteLineStringAsBytes(string tx, int byteInterval = 50, bool writeCR = true, bool writeLF = false)
        {
            WriteLineString(tx);
        }

        public string PortName
        {
            get
            {
                return _tcpClient.GetType().FullName;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int ReadBufferSize
        {
            get
            {
                return _tcpClient.ReceiveBufferSize;
            }

            set
            {
                _tcpClient.ReceiveBufferSize = value;
            }
        }
    }
}
