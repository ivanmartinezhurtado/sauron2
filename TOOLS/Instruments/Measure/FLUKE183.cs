﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Threading;

namespace Instruments.Measure
{
    [InstrumentsVersion(1.00)]
    public class FLUKE183 : MeasureBase<FLUKE183>, IDisposable
    {
        public class MeasureData
        {
            public enum RangeVoltage
            {
                _500mV = 0,
                _5V = 1,
                _50V = 2,
                _500V = 3,
                _1000V = 4,
            };

            public enum RangeCurrent
            {
                _500uA = 0,
                _5mA = 1,
                _50mA = 2,
                _500mA = 3,
                _5A = 4,
                _10A = 5, // 'Durante 3 minutos
            };

            public enum RangeResistence
            {
                _50ohm = 0,
                _500ohm = 1,
                _5Kohm = 2,
                _50Kohm = 3,
                _500Kohm = 4,
                _5Mohm = 5,
                _50Mohm = 6,
            };

            public enum RangeCapacitor
            {
                _5nF = 0,
                _50nF = 1,
                _500nF = 2,
                _5uF = 3,
                _50uF = 4,
                _500uF = 5,
                _5mF = 6,
                _50mF = 7,
            };

            public enum RangeFrecuency
            {
                _50Hz = 0,
                _500Hz = 1,
                _5KHz = 2,
            };

            public MeasureData(double t)
            {
                Voltage = t;
                Current = t;
                Freq = 0;
                Resistence = 0;
                Capacitor = 0;
                
            }

            public double Voltage { get;  set; }
            public double Current { get;   set; }
            public double Freq { get; set; }
            public double Resistence { get; set; }
            public double Capacitor { get; set; }

            public RangeVoltage VoltageRange { get; set; }
            public RangeCurrent CurrentRange { get; set; }
            public RangeResistence ResistenceRange {get; set;}
            public RangeCapacitor CapacitorRange { get; set; }
            public RangeFrecuency FrecuencyRange { get; set; }

        }

        internal MeasureData DeviceData { get; set; }

        public double Tolerance { get; set; }

        public enum Variables
        {
            CURRENT,
            VOLTAGE,
            FERQUENCY,
            RESISTENCE,
            CAPACITANCE,
        };

        public enum TypeMeasure
        {
            AC = 0,
            DC = 1,
        };

        public enum PrivateCommands
        {
            SET_VOLTAGE = Commands.USER_DEFINED_COMMANDS + 1,
            SET_VOLTAGE_RANGE = Commands.USER_DEFINED_COMMANDS + 2,
            SET_CURRENT= Commands.USER_DEFINED_COMMANDS + 3,
            SET_CURRENT_RANGE = Commands.USER_DEFINED_COMMANDS + 4,
            SET_FRECUENCY = Commands.USER_DEFINED_COMMANDS + 5,
            SET_FRECUENCY_RANGE = Commands.USER_DEFINED_COMMANDS + 6,
            SET_RESISTENCE = Commands.USER_DEFINED_COMMANDS + 7,
            SET_RESISTENCE_RANGE = Commands.USER_DEFINED_COMMANDS + 8,
            SET_CAPACITANCE = Commands.USER_DEFINED_COMMANDS + 9,
            SET_CAPACITANCE_RANGE = Commands.USER_DEFINED_COMMANDS + 10

        };

        private const string SET_MEASURE_COMMAND = "!M?";
        private const string SET_VOLTAGE_COMMAND = "!K1";
        private const string SET_CURRENT_COMMAND = "!K6";
        private const string SET_RSISTENCE_COMMAND = "!K3";
        private const string SET_FRECUENCY_COMMAND = "!K2";
        private const string SET_CAPACITOR_COMMAND = "!K4";
        private const string SET_VOLTAGE_DC_RANG_COMMAND = "!T03,{0}";
        private const string SET_VOLTAGE_AC_RANG_COMMAND = "!T01,{0}";
        private const string SET_CURRENT_DC_RANG_COMMAND = "!T12,{0}";
        private const string SET_CURRENT_AC_RANG_COMMAND = "!T11,{0}";
        private const string SET_RSISTENCE_RANG_COMMAND = "!T07,{0}";
        private const string SET_FRECUENCY_RANG_COMMAND = "!T19,{0}";
        private const string SET_CAPACITOR_RANG_COMMAND = "!T01,{0}";

        private readonly Dictionary<int, string> messages = new Dictionary<int, string> 
        {
            { (int)Variables.CURRENT, SET_MEASURE_COMMAND },
            { (int)Variables.VOLTAGE, SET_MEASURE_COMMAND },
            { (int)Variables.FERQUENCY, SET_MEASURE_COMMAND },   
            { (int)Variables.RESISTENCE, SET_MEASURE_COMMAND },
            { (int)Variables.CAPACITANCE, SET_MEASURE_COMMAND },
        };

        private Variables lastVariable;

        private SerialPort sp;
        private byte _port = 2;

        public FLUKE183()
        {
            InitializeComponent();

            DeviceData = new MeasureData(0);
            protocol = new FLUKEProtocol();
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), _logger);
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public int WaitTimeMeaure { get; set; }
        
        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.Even;
            sp.DataBits = 7;
            sp.PortName = "COM" + _port.ToString();
        }

        public void ConfigVoltage(TypeMeasure typeMeasure, MeasureData.RangeVoltage rangVoltage, bool? throwException = null)
        {
            DeviceData.VoltageRange = rangVoltage;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE, true));

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_VOLTAGE_RANGE, true, typeMeasure));

            lastVariable = FLUKE183.Variables.VOLTAGE;
        }

        public Result<double> ReadVoltage(TypeMeasure typeMeasure, MeasureData.RangeVoltage rangVoltage, bool? throwException = null)
        {
            ConfigVoltage(typeMeasure, rangVoltage, throwException);

            Thread.Sleep(WaitTimeMeaure);

            return ReadMeasure(throwException);
        }

        public void ConfigCurrent(TypeMeasure typeMeasure, MeasureData.RangeCurrent rangeCurrent, bool? throwException = null)
        {
            DeviceData.CurrentRange = rangeCurrent;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CURRENT, true));

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CURRENT_RANGE, true, typeMeasure));

            lastVariable = FLUKE183.Variables.CURRENT;
        }

        public Result<double> ReadCurrent(TypeMeasure typeMeasure, MeasureData.RangeCurrent rangeCurrent, bool? throwException = null)
        {
            ConfigCurrent(typeMeasure, rangeCurrent, throwException);

            Thread.Sleep(WaitTimeMeaure);

            return ReadMeasure(throwException);
        }

        public void ConfigFrecuency(MeasureData.RangeFrecuency rangeFrecuency, bool? throwException = null)
        {
            DeviceData.FrecuencyRange = rangeFrecuency;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_FRECUENCY, true));

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_FRECUENCY_RANGE, true));

            lastVariable = FLUKE183.Variables.FERQUENCY;
        }

        public Result<double> ReadFrecuency(MeasureData.RangeFrecuency rangeFrecuency, bool? throwException = null)
        {
            ConfigFrecuency(rangeFrecuency, throwException);

            Thread.Sleep(WaitTimeMeaure);

            return ReadMeasure(throwException);
        }

        public void ConfigResistance(MeasureData.RangeResistence rangResistence, bool? throwException = null)
        {
            DeviceData.ResistenceRange = rangResistence;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RESISTENCE, true));

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_RESISTENCE_RANGE, true));

            lastVariable = FLUKE183.Variables.RESISTENCE;
        }

        public Result<double> ReadResistance(MeasureData.RangeResistence rangResistence, bool? throwException = null)
         {
             ConfigResistance(rangResistence, throwException);

             Thread.Sleep(WaitTimeMeaure);

             return ReadMeasure(throwException);
         }

        public void ConfigCapacitance(MeasureData.RangeCapacitor rangCapacitance, bool? throwException = null)
        {
            DeviceData.CapacitorRange = rangCapacitance;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CAPACITANCE, true));

            //SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_CAPACITANCE_RANGE, true));

            lastVariable = FLUKE183.Variables.CAPACITANCE;
        }

        public Result<double> ReadCapacitance(MeasureData.RangeCapacitor rangCapacitance, bool? throwException = null)
        {
            ConfigCapacitance(rangCapacitance, throwException);

            Thread.Sleep(WaitTimeMeaure);

            return ReadMeasure(throwException);
        }

        public double ReadMeasure(bool? throwException = null)
        {
            var  dataValue = Read<double>((int)lastVariable, throwException);

            if (dataValue.Exception != null)
                throw new InstrumentException(1, dataValue.ErrorMessage);

            return dataValue.Value;
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            FLUKEMessage message, response;

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {       
                case (int)Commands.READ_VARIABLE:

                    int variable = (int)command.Args[0];

                    double res = 0;

                    message = new FLUKEMessage { Text = messages[variable] };
                    response = transport.SendMessage<FLUKEMessage>(message);
                    var responseArray = response.Text.Split(';');

                    if (responseArray.Length > 1)
                        res = double.Parse(responseArray[1], System.Globalization.NumberStyles.Float, new CultureInfo("en-US"));
                    else
                        throw new InstrumentException(1, "Error no se ha recibido el trozo de trama con el valor de medida del FLUKE");

                    switch (variable)
                    {
                        case (int)Variables.VOLTAGE:
                            DeviceData.Voltage = res;
                            break;
                        case (int)Variables.CURRENT:
                            DeviceData.Current = res;
                            break;
                        case (int)Variables.FERQUENCY:
                            DeviceData.Freq = res;
                            break;
                        case (int)Variables.RESISTENCE:
                            DeviceData.Resistence = res;
                            break;
                        case (int)Variables.CAPACITANCE:
                            DeviceData.Capacitor = res;
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                    command.Args[1] = res;

                    break;

                case (int)PrivateCommands.SET_CURRENT:
                    message = FLUKEMessage.Create(SET_CURRENT_COMMAND);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_CURRENT_RANGE:
                    var comm = (TypeMeasure)command.Args[0] == TypeMeasure.AC ? SET_CURRENT_AC_RANG_COMMAND : SET_CURRENT_DC_RANG_COMMAND;
                    message = FLUKEMessage.Create(comm, (int)DeviceData.CurrentRange);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_VOLTAGE:
                    message = FLUKEMessage.Create(SET_VOLTAGE_COMMAND);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_VOLTAGE_RANGE:
                    var messageVoltage=  (TypeMeasure)command.Args[0] == TypeMeasure.AC ? SET_VOLTAGE_AC_RANG_COMMAND : SET_VOLTAGE_DC_RANG_COMMAND;
                    message = FLUKEMessage.Create(messageVoltage, (int)DeviceData.VoltageRange);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_FRECUENCY:
                    message = FLUKEMessage.Create(SET_FRECUENCY_COMMAND);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_FRECUENCY_RANGE:
                    message = FLUKEMessage.Create( SET_FRECUENCY_RANG_COMMAND, (int)DeviceData.FrecuencyRange);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_RESISTENCE:
                    message = FLUKEMessage.Create(SET_RSISTENCE_COMMAND);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_RESISTENCE_RANGE:
                    message = FLUKEMessage.Create(SET_RSISTENCE_RANG_COMMAND, (int)DeviceData.ResistenceRange);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_CAPACITANCE:
                    message = FLUKEMessage.Create(SET_CAPACITOR_COMMAND);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;
                case (int)PrivateCommands.SET_CAPACITANCE_RANGE:
                    message = FLUKEMessage.Create(SET_CAPACITOR_RANG_COMMAND, (int)DeviceData.CapacitorRange);
                    transport.SendMessage<FLUKEMessage>(message);
                    break;

                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
    }
}
