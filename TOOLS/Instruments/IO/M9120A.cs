﻿using Agilent.AgMSwitch.Interop;
using Instruments.Utility;
using log4net;
using System;
using System.ComponentModel;

namespace Instruments.IO
{
    [InstrumentsVersion(1.11)]
    public class M9120A : IMux
    {
        protected static readonly ILog _logger = LogManager.GetLogger(typeof(M9120A));

        private AgMSwitch driver = null;

        public AgMSwitch Mux
        {
            get
            {
                if (driver == null)
                    driver = new AgMSwitch();

                return driver;
            }
        }

        public M9120A()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            // Create a pointer to the IviSwtch interface
           // mySwitch = (IIviSwtch)Mux;

            // Edit resource and options as needed.  Resource is ignored if option Simulate=true
            string resourceDesc = "M9120A";

            // If idquery = true, a Model number is required, otherwise, it is not. Note the optional trace feature as well.
            // NOTE:  If you enable the trace, you must have a C:\Temp directory, or change destination
            string initOptions = "Simulate=false"; // DriverSetup=Model=M9120A, Trace=true, TraceName=c:\\Temp\\AgMSwitchTrace";

            bool idquery = false;
            bool reset = false;


            // Initialize the driver.  See driver help topic "Initializing the IVI-COM Driver" for additional information
            Mux.Initialize(resourceDesc, idquery, reset, initOptions);
            _logger.InfoFormat("Driver Initialized");
        }

        public string GetIdentification()
        {
            _logger.DebugFormat("Identifier:  {0}", Mux.Identity.Identifier);
            _logger.DebugFormat("Revision:    {0}", Mux.Identity.Revision);
            _logger.DebugFormat("Vendor:      {0}", Mux.Identity.Vendor);
            _logger.DebugFormat("Description: {0}", Mux.Identity.Description);
            _logger.DebugFormat("Model:       {0}", Mux.Identity.InstrumentModel);
            _logger.DebugFormat("FirmwareRev: {0}", Mux.Identity.InstrumentFirmwareRevision);
            _logger.DebugFormat("Serial #:    {0}", Mux.System.SerialNumber);

            return Mux.Identity.Identifier;
        }

        public bool Connect(InputMuxEnum input, Outputs output)
        {
            return Connect((int)input, output);
        }

        public bool Connect2(InputMuxEnum input, Outputs output, InputMuxEnum input2, Outputs output2)
        {
            return Connect2((int)input, output, (int?)input2, output2);
        }

        [Browsable(false)]
        public bool Connect2(int input, Outputs output, int? input2, IO.Outputs? output2)
        {
            var con1 = Connect(input, output);
            var con2 = true;
            if (input2 != null && output2 != null)
                con2 = Connect((int)input2, (Outputs)output2);
            return (con1 && con2);
        }

        [Browsable(false)]
        public bool Connect(int input, Outputs output)
        {
            try
            {
                if (input > 64)
                    throw new Exception("Error multiplerxora solo tiene 64 salidas");

                var firstChannel = string.Format("m1c{0}", input.ToString());
                var secondChannel = string.Format("m1r{0}", (byte)output + 1);

                var capability = Mux.Path.CanConnect(firstChannel, secondChannel);

                if (capability == AgMSwitchPathCapabilityEnum.AgMSwitchPathAvailable)
                {
                    _logger.InfoFormat("Path is available, connecting CH1{0} -> CH2{1}.", firstChannel, secondChannel);
                    Mux.Path.Connect(secondChannel, firstChannel);

                    string mypath = Mux.Path.GetPath(firstChannel, secondChannel);
                    _logger.InfoFormat("Connect MUX {0}", mypath);

                    // Wait for the switch to settle
                    _logger.InfoFormat("wait time used below 10 ms.");
                    Mux.Path.WaitForDebounce(10);

                }
                else
                {
                    _logger.InfoFormat("Connection not possible between {0} and {1}", firstChannel, secondChannel);
                    switch (capability)
                    {
                        case AgMSwitchPathCapabilityEnum.AgMSwitchPathChannelNotAvailable:
                            _logger.InfoFormat("One of the channels is a configuration channel and is not available for connection.");
                            throw new Exception("One of the channels is a configuration channel and is not available for connection.");
                        case AgMSwitchPathCapabilityEnum.AgMSwitchPathRsrcInUse:
                            _logger.InfoFormat("One of the channels in the path is already in use.");
                            throw new Exception("One of the channels in the path is already in use.");
                        case AgMSwitchPathCapabilityEnum.AgMSwitchPathSourceConflict:
                            _logger.InfoFormat("Both channels are connected to a different source channel.");
                            throw new Exception("Both channels are connected to a different source channel.");
                        case AgMSwitchPathCapabilityEnum.AgMSwitchPathUnsupported:
                            _logger.InfoFormat("Path is not supported.");
                            throw new Exception("Path is not supported.");
                        case AgMSwitchPathCapabilityEnum.AgMSwitchPathExists:
                            _logger.WarnFormat("This path is already connected.");
                            break;
                        default:
                            break;
                    }
            }

        }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
            return true;
        }  

        public void Disconnect(int input, Outputs output)
        {
            var firstChannel = string.Format("m1c{0}", input.ToString());
            var secondChannel = string.Format("m1r{0}", (byte)output + 1);

            var capability = Mux.Path.CanConnect(firstChannel, secondChannel);
            if (capability== AgMSwitchPathCapabilityEnum.AgMSwitchPathExists)
            {
                string mypath = Mux.Path.GetPath(firstChannel, secondChannel);
                _logger.InfoFormat("Disconnect MUX {0}", mypath);
                Mux.Path.Disconnect(firstChannel, secondChannel);

                _logger.InfoFormat("wait time used below 10 ms.");
                Mux.Path.WaitForDebounce(10);
            }
            else
                _logger.InfoFormat("CanConnect did not verify the connection exists.");

        }

        [Browsable(false)]
        public void Disconnect2(int input, Outputs output, int? input2, IO.Outputs? output2)
        {
            Disconnect(input, output);

            if (input2 != null && output2 != null)
                Disconnect((int)input2, (Outputs)output2);
        }

        public void Disconnect2(InputMuxEnum input, Outputs output, InputMuxEnum input2, IO.Outputs output2)
        {
            Disconnect((int)input, output);
            Disconnect((int)input2, output2);
        }

        public void Dispose()
        {
            if (Mux != null && Mux.Initialized)
            {
                // Close the driver
                Mux.Close();
                _logger.InfoFormat("Driver Closed");
            }
        }

        [Browsable(false)]
        public void Reset()
        {
            
        }
    }
}
