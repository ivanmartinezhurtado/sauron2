﻿using Dezac.Tests.Services;
using TaskRunner.Model;

namespace Dezac.Tests
{
    public class TestScriptHost : ScriptHostBase
    {
        private ITestContext TestContext
        {
            get
            {
                return SequenceContext.Current.Services.Get<ITestContext>();
            }
        }

        public string Res(string name)
        {
            return TestContext.Resultados.GetString(name, null, false);
        }
        public string res(string name) => Res(name);

        public string Mar(string name)
        {
            return TestContext.Margenes.GetString(name, null, false);
        }
        public string mar(string name) => Mar(name);

        public string Cons(string name)
        {
            return TestContext.Consignas.GetString(name, null, false);
        }
        public string cons(string name) => Cons(name);

        public string Conf(string name)
        {
            return TestContext.Configuracion.GetString(name, null, false);
        }
        public string conf(string name) => Conf(name);

        public string Param(string name)
        {
            return TestContext.Parametrizacion.GetString(name, null, false);
        }
        public string param(string name) => Param(name);

        public string Vector(string name)
        {
            return TestContext.VectorHardware.GetString(name, null, false);
        }
        public string vector(string name) => Vector(name);

        public string Vision(string name)
        {
            return TestContext.Vision.GetString(name, null, false);
        }
        public string vision(string name) => Vision(name);
    }
}
