﻿using Comunications.Ethernet;
using Comunications.Utility;
using Dezac.Core.Exceptions;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;
using System.Xml.Linq;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.05)]
    public class CVMB1XXModuloDatalogger : DeviceBase
    {   
        public CVMB1XXModuloDatalogger()
        {
        }

        public CVMB1XXModuloDatalogger(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 115200, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public string Host { get; set; }

        public string VersionPowerStudio { get; set; }

        public string IdPowerStudio { get; set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }

        public string ReadIP()
        {
            LogMethod();
            var ipTemp = Modbus.ReadMultipleRegister((ushort)Registers.IP, 4);
            string ip = string.Empty;
            foreach (ushort ipField in ipTemp)
                ip += ipField.ToString() + ".";

            return ip.TrimEnd('.');
        }

        public string ReadKernelVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.KERNEL_VERSION, 4);
        }

        public string ReadPowerStudioVersion()
        {
            LogMethod();
            return Modbus.ReadString((ushort)Registers.POWER_STUDIO_VERSION, 4);
        }

        public long ReadFrameNumber()
        {
            LogMethod();
            return Modbus.Read<long>((ushort)Registers.FRAME_NUMBER);
        }

        public int ReadErrorCode()
        {
            LogMethod();
            return Modbus.Read<int>((ushort)Registers.ERROR_CODE);
        }

        public ushort[] ReadSerialNumber()
        {
            LogMethod();
            return Modbus.ReadMultipleRegister((ushort)Registers.SERIAL_NUMBER, 6);
        }

        public MAC ReadMac()
        {
            LogMethod();
            return Modbus.Read<MAC>();
        }

        public IPConfig ReadIPConfig()
        {
            LogMethod();
            return Modbus.Read<IPConfig>();
        }

        public void WriteFrameNumber(int number)
        {
            LogMethod();
            Modbus.Write<long>((ushort)Registers.FRAME_NUMBER, number);
        }

        public void WriteErrorCode(ushort error)
        {
            LogMethod();
            Modbus.Write<long>((ushort)Registers.ERROR_CODE, error);
        }

        public void WriteFlagTest()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, true);
        }

        public void WriteEnableEmbeddedUART()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.ENABLE_EMBEDDED_UART, (ushort)1);
        }

        public ushort ReadEnableEmbeddedUART()
        {
            LogMethod();
           return Modbus.ReadRegister((ushort)Registers.ENABLE_EMBEDDED_UART);
        }

        public void WriteDateTime(DateTime? date = null)
        {
            LogMethod();
            Modbus.Write<FechaHora>((ushort)Registers.FECHA_HORA, FechaHora.Create(date));
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAC)]
        public struct MAC
        {
            private byte Field1;
            private byte Field2;
            private byte Field3;
            private byte Field4;
            private byte Field5;
            private byte Field6;

            public string MacAddress
            {
                get
                {
                    return Field1 + ":" + Field2 + ":" + Field3 + ":" + Field4 + ":" + Field5 + ":" + Field6;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.IP)]
        public struct IPConfig
        {
            private byte IPField1;
            private byte IPField2;
            private byte IPField3;
            private byte IPField4;
            private byte MaskField1;
            private byte MaskField2;
            private byte MaskField3;
            private byte MaskField4;
            private byte GatewayField1;
            private byte GatewayField2;
            private byte GatewayField3;
            private byte GatewayField4;

            public int Port;
            public string IP
            {
                get
                {
                    return IPField1 + "." + IPField2 + "." + IPField3 + "." + IPField4;
                }
            }
            public string Mask
            {
                get
                {
                    return MaskField1 + "." + MaskField2 + "." + MaskField3 + "." + MaskField4;
                }
            }
            public string Gateway
            {
                get
                {
                    return GatewayField1 + "." + GatewayField2 + "." + GatewayField3 + "." + GatewayField4;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FechaHora
        {
            public UInt16 Year;
            public UInt16 Month;
            public UInt16 Day;
            public UInt16 Hour;
            public UInt16 Minutes;
            public UInt16 Seconds;

            public static FechaHora Create(DateTime? date = null)
            {
                DateTime fecha = date.GetValueOrDefault(DateTime.Now);

                FechaHora value = new FechaHora
                {
                    Year = (ushort)fecha.Year,
                    Month = (ushort)fecha.Month,
                    Day = (ushort)fecha.Day,
                    Hour = (ushort)fecha.Hour,
                    Minutes = (ushort)fecha.Minute,
                    Seconds = (ushort)fecha.Second,
                };

                return value;
            }

            public DateTime ToDateTime()
            {
                return new DateTime(Year, Month, Day, Hour, Minutes, Seconds);
            }
        }

        public enum Registers
        {
            IP = 0xE290, // 2 Registros
            FECHA_HORA = 0x283C,
            HARDWARE_IDENTIFIER = 0x2726,
            FLAG_TEST = 0x2AF8,
            ERROR_CODE = 0x2788,
            FRAME_NUMBER = 0x2792,
            MAC = 0xE2A0,
            SERIAL_NUMBER = 0X2756,
            KERNEL_VERSION = 0xE298,
            POWER_STUDIO_VERSION = 0xE29C, // 4 registros
            ENABLE_EMBEDDED_UART = 0x27D8,
        }

        public void SendHttpLockSession(string IpHost)
        {
            Host = IpHost;
            SendHttpLockSession();
        }

        public void SendHttpLockSession()
        {
            LogMethod();

            try
            {
                var xmlresp = ComunicationHttpClient.HttpPutContent(Host, "services/engine/lock.xml?editorVersion=4.0.12");

                if(string.IsNullOrEmpty(xmlresp))
                    TestException.Create().UUT.COMUNICACIONES.ETHERNET("SendHttpLockSession NO RESPONDE").Throw();

                var xelement = XElement.Parse(xmlresp);

                var idIenum = from nm in xelement.Elements("id")
                              select nm.Value;

                var versionIenum = from nm in xelement.Elements("version")
                                   select nm.Value;
                if (idIenum != null)
                    IdPowerStudio = idIenum.FirstOrDefault();

                if (versionIenum != null)
                    VersionPowerStudio = versionIenum.FirstOrDefault();

                if (string.IsNullOrEmpty(VersionPowerStudio) || string.IsNullOrEmpty(IdPowerStudio))
                    TestException.Create().UUT.COMUNICACIONES.ETHERNET("SendHttpLockSession XML ERROR").Throw();
            }
            catch (AggregateException aex)
            {
                TestException.Create().UUT.COMUNICACIONES.ETHERNET("SendHttpLockSession " + aex.GetBaseException().Message).Throw();
            } catch(Exception ex)
            {
                TestException.Create().UUT.COMUNICACIONES.ETHERNET("SendHttpLockSession " + ex.Message).Throw();
            }
        }

        public void SendHttpXmlConfiguration(string driverName, string deviceName)
        {
            LogMethod();

            var fileConfig = CreateFileConfig(VersionPowerStudio, driverName, deviceName);

            _logger.InfoFormat(fileConfig);

            ComunicationHttpClient.HttpPutContentWithoutResponse(Host, string.Format("services/{0}/commsystem/set.xml", IdPowerStudio), fileConfig);
            Thread.Sleep(1000);
            ComunicationHttpClient.HttpPutContentWithoutResponse(Host, string.Format("services/{0}/devices/setGroups.xml", IdPowerStudio), fileConfig);
        }

        public void SendHttpSynchronize()
        {
            LogMethod();

            ComunicationHttpClient.HttpPutContentWithoutResponse(Host, string.Format("services/{0}/engine/synchronize.xml?setupBaudRate=F", IdPowerStudio));
        }

        public void SendHttpUnLockSession()
        {
           LogMethod();

           var xmlresp = ComunicationHttpClient.HttpPutContent(Host, string.Format("services/{0}/engine/unlock.xml", IdPowerStudio));
        }

        private string CreateFileConfig( string version, string driver, string name)
        {
            List<KeyValuePair<string, string>> informationList = new List<KeyValuePair<string, string>>();

            KeyValuePair<string, string> elementProgram = new KeyValuePair<string, string>("program", "PowerStudio");
            KeyValuePair<string, string> elementversion = new KeyValuePair<string, string>("version", version);
            informationList.Add(elementProgram);
            informationList.Add(elementversion);

            List<KeyValuePair<string, string>> deviceList = new List<KeyValuePair<string, string>>();
            KeyValuePair<string, string> elementId = new KeyValuePair<string, string>("id", IdPowerStudio);
            KeyValuePair<string, string> elementRevision = new KeyValuePair<string, string>("revision", "9EFB3BC8-B98C-4185-B151-AF452FD1E1AF");
            //KeyValuePair<string, string> elementDriver = new KeyValuePair<string, string>("driver", "CVMA1500CONTAINER");
            KeyValuePair<string, string> elementDriver = new KeyValuePair<string, string>("driver", driver);
            KeyValuePair<string, string> elementtype = new KeyValuePair<string, string>("type", "RS");
            //KeyValuePair<string, string> elementname = new KeyValuePair<string, string>("name", "CVM-A1500");
            KeyValuePair<string, string> elementname = new KeyValuePair<string, string>("name", name);
            KeyValuePair<string, string> elementDescription = new KeyValuePair<string, string>("description", "");
            KeyValuePair<string, string> elementstNum = new KeyValuePair<string, string>("stNum", "1");
            KeyValuePair<string, string> elementchildDevices = new KeyValuePair<string, string>("childDevices", "");
            KeyValuePair<string, string> elementsavePeriod = new KeyValuePair<string, string>("savePeriod", "900");
            deviceList.Add(elementId);
            //deviceList.Add(elementRevision);
            deviceList.Add(elementDriver);
            //deviceList.Add(elementtype);
            deviceList.Add(elementname);
            deviceList.Add(elementDescription);
            deviceList.Add(elementstNum);
            //deviceList.Add(elementchildDevices);
            //deviceList.Add(elementsavePeriod);


            List<KeyValuePair<string, string>> informationGroupList = new List<KeyValuePair<string, string>>();

            KeyValuePair<string, string> elementIdGroup = new KeyValuePair<string, string>("id", IdPowerStudio);
            informationGroupList.Add(elementIdGroup);


            List<KeyValuePair<string, string>> idDeviceGroupList = new List<KeyValuePair<string, string>>();

            KeyValuePair<string, string> elementIdDevice = new KeyValuePair<string, string>("id", name);
            idDeviceGroupList.Add(elementIdDevice);



            return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + BuildMessageXml(informationList, deviceList, informationGroupList, idDeviceGroupList);
        }

        public string BuildMessageXml(List<KeyValuePair<string, string>> informationList, List<KeyValuePair<string, string>> commSystemList, List<KeyValuePair<string, string>> informationGroupList, List<KeyValuePair<string, string>> idGroupList)
        {
            var doc = new XmlDocument();

            var main = doc.CreateElement("main");

            var information = doc.CreateElement("information");

            foreach (KeyValuePair<string, string> it in informationList)
            {
                var element = doc.CreateElement(it.Key);
                element.InnerText = it.Value;
                information.AppendChild(element);
            }

            main.AppendChild(information);

            var commSystem = doc.CreateElement("commSystem");
            var elementDevices = doc.CreateElement("devices");
            elementDevices.InnerText = string.Empty;
            var elementConnections = doc.CreateElement("connections");
            elementConnections.InnerText = string.Empty;
            var elementDevice = doc.CreateElement("device");

            commSystem.AppendChild(elementDevices);
            commSystem.AppendChild(elementConnections);
            commSystem.AppendChild(elementDevice);

            foreach (KeyValuePair<string, string> it in commSystemList)
            {
                var element = doc.CreateElement(it.Key);
                element.InnerText = it.Value;
                elementDevice.AppendChild(element);
            }

            main.AppendChild(commSystem);

            var deviceGroups = doc.CreateElement("deviceGroups");
            var devices = doc.CreateElement("devices");
            devices.InnerText = string.Empty;

            var groups = doc.CreateElement("groups");

            deviceGroups.AppendChild(devices);
            deviceGroups.AppendChild(groups);

            var group = doc.CreateElement("group");
            groups.AppendChild(group);

            foreach (KeyValuePair<string, string> it in idGroupList)
            {
                var element = doc.CreateElement(it.Key);
                element.InnerText = it.Value;
                group.AppendChild(element);
            }

            var devicesIntoGroup = doc.CreateElement("devices");

            var deviceIntoGroup = doc.CreateElement("device");
            devicesIntoGroup.AppendChild(deviceIntoGroup);

            foreach (KeyValuePair<string, string> it in informationGroupList)
            {
                var element = doc.CreateElement(it.Key);
                element.InnerText = it.Value;
                deviceIntoGroup.AppendChild(element);
            }

            group.AppendChild(devicesIntoGroup);



            main.AppendChild(deviceGroups);

            //var viewSetups = doc.CreateElement("viewSetups");
            //viewSetups.InnerText = string.Empty;
            //main.AppendChild(viewSetups);
            //var unitSetups = doc.CreateElement("unitSetups");
            //unitSetups.InnerText = string.Empty;
            //main.AppendChild(unitSetups);
            //var preferences = doc.CreateElement("preferences");
            //preferences.InnerText = "";
            //main.AppendChild(preferences);

            doc.AppendChild(main);

            return doc.InnerXml;
        }

    }
}
