﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.00)]
    public class VBTi : DeviceBase
    {
        #region Instanciacion y destrucción de la clase del equipo

        public CirbusDeviceSerialPort Cirbus { get; internal set; }

        public ModbusDeviceTCP Modbus { get; internal set; }

        public VBTi()
        {
        }  

        public VBTi(int port, int baudrate, byte periferic = 2)
        {
            SetPortCirbus(port, baudrate, periferic);
        }

        public VBTi(int port, string hostname, ModbusTCPProtocol protocol)
        {
            SetPortModbusTPC(port, hostname, protocol);
        }

        public void SetPortCirbus(int port, int baudrate, byte periferic)  
        {
            if (Cirbus == null)
                Cirbus = new CirbusDeviceSerialPort(port, baudrate, Parity.None, StopBits.One, _logger);

            Cirbus.PortCom = port;
            Cirbus.DtrEnable = true;
            Cirbus.RtsEnable = true;

            Cirbus.PerifericNumber = periferic;
            Cirbus.TimeOut *= 2;          
        }

        public void SetPortModbusTPC(int port, string hostname, ModbusTCPProtocol protocol )
        {
            if (Modbus == null)
            {
                Modbus = new ModbusDeviceTCP(hostname, port, protocol);           //1004 TCP, 1003 RTU
                Modbus.UseFunction06 = true;
                Modbus.TimeOut = 5000;
                Modbus.PerifericNumber = 2;
            }
        }

        public override void Dispose()
        {
            if (Modbus != null)
            {
                Modbus.Dispose();
                Modbus = null;
            }

            if (Cirbus != null)
            {
                Cirbus.Dispose();
                Cirbus = null;
            }
        }
        #endregion

        #region CIRBUS

        private const string READ_VERSION_FIRMWARE = "VER";
        private const string READ_IP = "IPR";
        private const string READ_MAC = "MAC";
        private const string WRITE_MAC = "MAC WRITE {0}";
        private const string READ_I2C = "I2C";
        private const string WRITE_IP = "IPW";
        private const string SEND_XML = "XML EXP NET";

        private Dictionary<byte, string> listErrorCirbus = new Dictionary<byte, string>()
        {
            { 1, "Error comando Cirbus" },
            { 2, "Error comando Cirbus" },
            { 6, "Error sintaxis del comando" },
            { 5, "Error rechazo de trama o desconocido" },
            { 7, "Error ChekSum del comando" }
        };

        public string ReadFirmware()
        {
            LogMethod();
            return Cirbus.Read(READ_VERSION_FIRMWARE, true);
        }

        public IPStruct ReadIP()
        {
            LogMethod();
            return Cirbus.Read<IPStruct>(READ_IP, true);
        }

        public string ReadI2C()
        {
            LogMethod();
            return Cirbus.Read(READ_I2C, true);
        }

        public string ReadMAC()
        {
            LogMethod();
            return Cirbus.Read(READ_MAC, true);
        }

        public void WriteMAC(string MAC)
        {
            LogMethod();
            var mac = Regex.Replace(MAC, "(.{2})(?!$)", "$0-");
            Cirbus.Write(string.Format(WRITE_MAC, mac), listErrorCirbus);
        }

        public void WriteIP(IPStruct ip)
        {
            LogMethod();
            Cirbus.Write(string.Format("{0} {1}", WRITE_IP, ip.ToFrame()));
        }

        public void SendXML()
        {
            LogMethod();
            Cirbus.Write(SEND_XML);
        }

        #endregion

        #region MODBUS

        public string ReadFirmwareVersionRadio()
        {
            LogMethod();  

            return Modbus.ReadInt32HoldingRegisters((ushort)Registers.VersionFirmwareRadio).ToString("X16").TrimStart('0');
        }

        public uint ReadSerialNumber()
        {
            LogMethod();
            return (uint)Modbus.ReadInt32HoldingRegisters((ushort)Registers.NumeroSerie);
        }

        public int ReadNumeroFusiblesConectado()
        {
            LogMethod();
            return Modbus.ReadHoldingRegister((ushort)Registers.NumeroFusileConect);
        }

        public void WriteCapturaFusiblesRegister()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.NumeroFusileConect, (ushort)0x1111);
        }

        public void WriteReset()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.Reset, (ushort)0x1111);
        }

        public RegisterFusibleIdentification ReadRegisterFusibleIdentification()
        {
            LogMethod();
            return Modbus.ReadHolding<RegisterFusibleIdentification>((ushort)Registers.IdentificationFusible1);
        }

        public void WriteCBTRegister(uint sn)
        {
            LogMethod();
            Modbus.Write((ushort)Registers.SerialNumberFusible1, sn);
        }

        public void WriteSerialNumber(string serialNumber)
        {
            LogMethod();
            Modbus.WriteInt32((ushort)Registers.NumeroSerie, Convert.ToInt32(serialNumber));
        }

        public void WriteToFlash()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.VolcadoFlash, (ushort)0x1234);
            Thread.Sleep(2000);
        }

        public void FlagTest()
        {
            LogMethod();
            Modbus.Write((ushort)Registers.FlagTest, (ushort)1);
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RegisterFusibleIdentification
        {
            public UInt16 IDFusible;
            public UInt16 VersionFrimaware;
            public UInt32 SerialNumber;
        };
        
        public MeasureStuctre ReadMeasureCurrent()
        {
            LogMethod();

            var resp = Modbus.ReadHoldingRegister((ushort)Registers.MesureFusible1);

            var respenum = new MeasureStuctre()
            {
                current = (resp & 0x3FFF)/10.0,
                status = (FuseStatus)(resp >> 14),
            };

            return respenum;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MeasureStuctre
        {
            public FuseStatus status;
            public double current;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct IPStruct
        {
            public string IP;
            public string NetMask;
            public string Gateway;
            public string NetGateway;

            public string ToFrame()
            {
                return string.Format("{0} {1} {2} {3}", IP, NetMask, Gateway, NetGateway);
            }
        };

        public enum Registers
        {
            VersionFirmware = 0x0003,
            NumeroSerie = 0x0004,
            NumeroFusileConect = 0x0006,
            CapturaFusibleRegistrar = 0x0007,
            Reset = 0x0008,
            VersionFirmwareRadio = 0x0009,
            IdentificationFusible1 = 0x0100,
            SerialNumberFusible1 = 0x0102,
            IdentificationFusible2 = 0x0104,          
            MesureFusible1 = 0x1000,
            MesureFusible2 = 0x1001,
            TemperatureFusible1 = 0x2000,
            TemperatureFusible2 = 0x2001,
            VolcadoFlash = 0x001E,
            FlagTest = 0x3000     
        }

        public enum FuseStatus
        {
            OK = 0,
            TIMEOUT = 1,
            NO_REGISTERED = 3,
            FUSED= 2,
        }

        #endregion

    }
}
