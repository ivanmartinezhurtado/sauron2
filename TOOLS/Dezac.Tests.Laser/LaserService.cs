﻿using Dezac.Core.Exceptions;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskRunner;
using TaskRunner.Model;

namespace Dezac.Tests.Laser
{
    public class LaserService : IDisposable
    {
        public const string PARAM_PREDEFINIDO = "20";
        public const string PARAM_CONSTANTE = "21";
        public const string PARAM_RESULTADO = "22";
        public const string PARAM_PARAMETRIZACION = "23";
        public const string PARAM_IDENTIFICACION = "24";
        public const string PARAM_CONSIGNA = "25";

        public const int GRUPO_LASER = 24;
        public const int INITIAL_POSITION = 2;

        private ITestContext testContext { get; set; }
        private string[] formats = new string[] {"yy", "yyyy"};

        public LaserService()
        {
            testContext = SequenceContext.Current.Services.Get<ITestContext>();
        }

        public string BuildLaserFile(string nameFile, string delimiter = ";")
        {
            int numberFields = 0;

            var varsList = GetVariables().OrderBy(p => p.Key);

            numberFields = varsList.Count() == 0 ? 1 : varsList.Max(p => p.Key);

            List<string> content = new List<string>(numberFields);
            content.Add(nameFile + delimiter);

            for (int position = INITIAL_POSITION; position <= numberFields; position++)
                content.Add((varsList.Where(p => p.Key == position) == null
                                            ? string.Empty
                                            : varsList.Where(p => p.Key == position).Select(p => p.Value).FirstOrDefault()) + delimiter);

            return string.Join(string.Empty, content.ToArray());
        }

        private Dictionary<byte, string> GetVariables()
        {
            Dictionary<byte, string> list = new Dictionary<byte, string>();
            byte position;
            string value;

            var varsList = testContext.GetGroupList(GRUPO_LASER);

            foreach (var var in varsList)
            {
                var split = var.Valor.Split(':');
                var name = split[0];
                var format = split.Count() > 1 ? split[1].ToLower() : string.Empty;
      
                if (var.IdCategoria == PARAM_PREDEFINIDO)
                {
                    position = Convert.ToByte(var.Name.Replace("LASER_", ""));
                    value = GetPredefinedValue(name, format);
                }
                else
                {
                    position = Convert.ToByte(var.Name.Replace("LASER_", ""));
                    value = name;
                }

                list.Add(position, value);
            }

            return list;
        }        

        private string GetPredefinedValue(string name, string format)
        {
            var testInfo = testContext.TestInfo;

            switch (name.ToUpper())
            {
                case "PN":
                    return testInfo.Producto;
                case "PV":
                    return testInfo.ProductoVersion;
                case "CODCIR":
                    return testInfo.CodCircutor;
                case "REFCLI_2":
                    return testInfo.CostumerCode2;
                case "COD":
                    return testInfo.DeviceID;
                case "PSW":
                    return testInfo.PSW;
                case "NB":
                    return testInfo.NumBastidor.HasValue ? testInfo.NumBastidor.Value.ToString() : string.Empty;
                case "SN":
                    GetNumSerie();
                    return testInfo.NumSerie;
                case "NS":
                    GetNumSerie();
                    return testInfo.NumSerie;
                case "NUM_SP3":
                    return GetNumSP3();
                case "NUM_BILOGY":
                    return GetNumBilogy();
                case "BARCODE":
                    return "000000000";
                case "DIR1":
                    return testContext.Direcciones.FirstOrDefault();
                case "DIR2":
                    return testContext.Direcciones.Count > 1 ? testContext.Direcciones[1] : null;
                case "DIR3":
                    return testContext.Direcciones.Count > 2 ? testContext.Direcciones[2] : null;
                case "DIR4":
                    return testContext.Direcciones.Count > 3 ? testContext.Direcciones[3] : null;
                case "ORDEN":
                    return testContext.NumOrden.ToString();
                case "YEAR":
                    ValidateFormat(format);
                    return DateTime.Now.ToString(format); 
                case "WEEKYEAR":
                    return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday).ToString("D2") + (DateTime.Now.ToString("yy"));
            }

            if (name.StartsWith("NS"))
            {
                int i = 2;
                foreach (KeyValuePair<string, string> ns2 in testInfo.NumSerieSubconjunto)
                {
                    if (name == "NS" + i)
                        return ns2.Value;
                    i++;
                }
            }

            return null;
        }

        private void ValidateFormat(string format)
        {
            if (formats.Where(p => p == format).Select(p => p).FirstOrDefault() == null)
                TestException.Create().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("FORMATO NO RECONOCIDO").Throw();
        }

        private void GetNumSerie()
        {
            using (DezacService svc = new DezacService())
            {
                if (!testContext.NumOrden.HasValue && testContext.NumProducto != 0)
                    testContext.TestInfo.NumSerie = svc.GetNumSerie(testContext.NumProducto, testContext.Version, 0);
                else                    
                    testContext.TestInfo.NumSerie = svc.GetNumSerie(testContext.NumProducto, testContext.Version, testContext.NumOrden.Value);
            }
        }

        private string GetNumSP3()
        {
            int number = 0;

            if (testContext.NumOrden.HasValue && testContext.NumOrden.Value != 0)
                using (DezacService svc = new DezacService())
                {
                    number = svc.GetNumSP3();
                }
            else
                number = 256;

            return number.ToString("X4");
        }

        private string GetNumBilogy()
        {
            int number = 0;

            if (testContext.NumOrden.HasValue && testContext.NumOrden.Value != 0)
                using (DezacService svc = new DezacService())
                {
                    number = svc.GetNumBilogy();
                }
            else
                number = 256;

            return number.ToString("X4");
        }

        public void Dispose()
        { }
    }
}
