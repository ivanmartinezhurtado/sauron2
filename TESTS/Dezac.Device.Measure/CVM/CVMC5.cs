﻿using Comunications.Utility;
using Dezac.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.02)]
    public class CVMC5 : DeviceBase
    {
        public CVMC5()
        {
        }

        public CVMC5(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudaRate = 9600, int periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, 9600, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = (byte)periferic;
        }

        public ModbusDeviceSerialPort Modbus { get;internal set; }

        #region Escritura de registros

        public void Reset()
        {
            LogMethod();

            Modbus.Retries = 0;
            Modbus.WithTimeOut((m) => { m.WriteSingleCoil((ushort)Registers.RESET, true); });
            Modbus.Retries = 3;
        }

        public void FlagTest(bool state = true)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.FLAG_TEST, state);
        }
        
        public void WriteOutput(State state)
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.OUTPUT_KEYBOARD_INPUTS, state == State.ON ? true : false);
        }

        public void WriteBacklightState(bool backlight1State, bool backlight2State)
        {
            LogMethod();

            Modbus.WriteMultipleCoil((ushort)Registers.BACKLIGHT, new bool[] {!backlight1State, !backlight2State});
        }

        public void WriteClearEnergy()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_ENERGY, true);
        }

        public void WriteInitializeMaximumDemand()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.INITIALIZE_MAX_DEMAND, true);
        }

        public void WriteClearMaximumMinimums()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_MAX_MIN, true);
        }

        public void WriteClearAll()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.CLEAR_ENERGY_MAX_MIN_MD, true);
        }

        public void SendDisplayInfo(DisplayState state, string version)
        {
            if (version.ToUpper().Trim() == "V1" || version.ToUpper().Trim() == "INDIA")
                WriteDisplaySegments(displayDictionary_V1[state]);
            else if (version.ToUpper().Trim() == "V2")
                WriteDisplaySegments(displayDictionary_V2[state]);
            else
                throw new Exception("No se ha configurado bien la version del display (Los valores aceptados son (V1 y V2)");
        }

        public void WriteDisplaySegments(params bool[] segmentsToTurnOn)
        {
            LogMethod();

            Modbus.WriteMultipleCoil((ushort)Registers.DISPLAY_SEGMENTS, segmentsToTurnOn);
        }

        public void WriteHardwareVector(HardwareVector hardwareVector)
        {
            LogMethod();

            Modbus.WriteMultipleCoil((ushort)Registers.HARDWARE_CONFIG, hardwareVector.ToSingleCoil());
        }

        public void WriteCalibrationStart()
        {
            LogMethod();

            Modbus.WriteSingleCoil((ushort)Registers.APPLY_CALIBRATION, true);
        }

        public void WritePasswordInformation(PasswordConfiguration passwordConfiguration)
        {
            LogMethod();
            Modbus.Write<PasswordConfiguration>(passwordConfiguration);
        }

        public void WriteAlarm(Alarms alarm, AlarmConfiguration alarmConfig)
        {
            LogMethod();

            Modbus.Write<AlarmConfiguration>((ushort)alarm, alarmConfig);
        }

        public void WriteMaximumDemandConfig(MaximumDemandConfiguration demandConfig)
        {
            LogMethod();

            Modbus.Write<MaximumDemandConfiguration>((ushort)Registers.MAXIMUM_DEMAND_CONFIGURATION, demandConfig);
        }

        public void WriteInitialMessage(String message)
        {
            LogMethod();

            message = message.PadRight(8, ' ');
            Modbus.WriteString((ushort)Registers.INITIAL_MESSAGE, message);
        }

        public void WriteTariffSelection(Tariff tariff)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.ACTIVE_TARIFF_SELECTION, (ushort)tariff);
        }

        public void WritePresetHardware(ushort preset)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.PRESET_HARDWARE, (ushort)preset);
        }

        public void WriteSerialNumber(UInt32 serialNumber)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER, (int)serialNumber);
        }
        
        public void WriteCompuestasFactors(TesnsionesCompuestas factors)
        {
            LogMethod();
            Modbus.Write<TesnsionesCompuestas>(factors);
        }

        public void WriteCalibrationFactors(CalibrationFactors calibrationFactors)
        {
            LogMethod();
            Modbus.Write<CalibrationFactors>(calibrationFactors);
        }

        public void WritePhaseGapCalibrationFactors(PhaseGapFactors factors)
        {
            LogMethod();

            Modbus.Write<PhaseGapFactors>((ushort)Registers.PHASE_CALIBRATION_FACTORS, factors);
        }
        
        public void WriteEnergies(Energies energies)
        {
            LogMethod();

            Modbus.Write<Energies>(energies);
        }

        public void WriteRecoverCalibrationValuesFromEEPROM()
        {
            LogMethod();
            Modbus.WriteSingleCoil((ushort)Registers.APPLY_CALIBRATION, true);
        }

        public void WriteSetupConfiguration(SetupConfiguration setupConfiguration)
        {
            LogMethod();
            Modbus.Write<SetupConfiguration>(setupConfiguration);
        }

        public void WriteCommunicationConfiguration(byte periferic, int baudRate)
        {
            var commConfig = new CommunicationsConfiguration(periferic, baudRate);
            Modbus.Write<CommunicationsConfiguration>(commConfig);
        }

        public void WriteMaximumDemandConfiguration(MaximumDemandConfiguration maximumDemandConfiguration)
        {
            LogMethod();
            Modbus.Write<MaximumDemandConfiguration>(maximumDemandConfiguration);
        }

        public void WriteAlarmConfiguration(AlarmConfiguration alarmConfiguration)
        {
            LogMethod();
            Modbus.Write<AlarmConfiguration>(alarmConfiguration);
        }

        public void WriteScreenConfiguration(ScreenConfiguration screenConfigurationInfo)
        {
            LogMethod();
            Modbus.WriteMultipleCoil((ushort)Registers.SCREEN_CONFIGURATION, screenConfigurationInfo.ToBoolArray());
        }

        #endregion

        #region Lectura de registros

        public KeyboardKeys ReadKeyboard()
        {
            LogMethod();

            var keyread = Modbus.ReadMultipleCoil((ushort)Registers.OUTPUT_KEYBOARD_INPUTS, 5);

            KeyboardKeys key = (KeyboardKeys)(keyread[0] & 0x1F);
            
            return key;
        }

        public byte[]ReadBacklightState()
        {
            LogMethod();

            return Modbus.ReadMultipleCoil((ushort)Registers.BACKLIGHT, 2);
        }

        public HardwareVector ReadHardwareVector()
        {
            LogMethod();

            var data = Modbus.ReadMultipleCoil((ushort)Registers.HARDWARE_CONFIG, 96);
            var hardwareVector = new HardwareVector(data);
            return hardwareVector;
        }

        public string ReadInitialMessage()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.INITIAL_MESSAGE, 4);
        }
        
        public string ReadFirmwareVersion()
        {
            LogMethod();

            return Modbus.ReadString((ushort)Registers.SOFTWARE_VERSION, (ushort)3);
        }

        public EEPROMErrorCodes ReadEEPROMErrorCodes()
        {
            LogMethod();
            return (EEPROMErrorCodes)Modbus.ReadRegister((ushort)Registers.EEPROM_ERROR_CODE);
        }

        public Tariff ReadActiveTariff()
        {
            LogMethod();

            return (Tariff)Modbus.ReadRegister((ushort)Registers.ACTIVE_TARIFF_SELECTION);
        }

        public CalibrationFactors ReadCalibrationFactors()
        {
            LogMethod();

            return Modbus.Read<CalibrationFactors>((ushort)Registers.CALIBRATION_FACTORS);
        }

        public PhaseGapFactors ReadPhaseGapCalibrationFactors()
        {
            LogMethod();

            return Modbus.Read<PhaseGapFactors>((ushort)Registers.PHASE_CALIBRATION_FACTORS);
        }

        public ConverterPoints ReadConverterPoints()
        {
            LogMethod();

            return Modbus.Read<ConverterPoints>((ushort)Registers.CONVERTER_POINTS);
        }

        public ChannelOffsets ReadChannelOffsets()
        {
            LogMethod();

            return Modbus.Read<ChannelOffsets>((ushort)Registers.CHANNELS_OFFSET);
        }

        public ushort ReadROMCode()
        {
            LogMethod();
            
            return Modbus.ReadRegister((ushort)Registers.CODE_ROM);
        }

        public Energies ReadEnergies()
        {
            LogMethod();

            return Modbus.Read<Energies>();
        }

        public uint ReadSerialNumber()
        {
            LogMethod();

            return (uint)Modbus.ReadInt32((ushort)Registers.SERIAL_NUMBER);
        }

        public AllVariables ReadAllElectricalVariables()
        {
            LogMethod();

            var Fases = Modbus.Read<VariablesInstantaneas>();
            Fases.L1.Tension /= 10;
            Fases.L1.FactorPotencia /= 100;
            Fases.L2.Tension /= 10;
            Fases.L2.FactorPotencia /= 100;
            Fases.L3.Tension /= 10;
            Fases.L3.FactorPotencia /= 100;

            // var All = Read<AllVariables>();
            var Trifasicas = Modbus.Read<VariablesTrifasicas>();
            Trifasicas.CosenoPhiIII /= 100;
            Trifasicas.FactorPotenciaIII /= 100;
            Trifasicas.Frecuencia /= 10;
            Trifasicas.TensionLineaL1L2 /= 10;
            Trifasicas.TensionLineaL2L3 /= 10;
            Trifasicas.TensionLineaL3L1 /= 10;

            AllVariables All = new AllVariables();
            All.Phases = Fases;
            All.Trifasicas = Trifasicas;

            return All;
        }

        public VariablesInstantaneas ReadPhaseVariables()
        {
            LogMethod();
            return Modbus.Read<VariablesInstantaneas>();
        }

        public VariablesTrifasicas ReadThreePhaseVariables()
        {
            LogMethod();
            return Modbus.Read<VariablesTrifasicas>();
        }

        public PasswordConfiguration ReadPasswordInformation()
        {
            LogMethod();
            return Modbus.Read<PasswordConfiguration>();
        }

        public MaximumDemandConfiguration ReadMaximumDemandConfig()
        {
            LogMethod();
            return Modbus.Read<MaximumDemandConfiguration>();
        }

        public SetupConfiguration ReadSetupConfiguration()
        {
            LogMethod();
            return Modbus.Read<SetupConfiguration>();
        }

        public TensionesCompuestas ReadTensionesCompuestas()
        {
            LogMethod();
            return Modbus.Read<TensionesCompuestas>();
        }

        public Frecuencia ReadFrecuencia()
        {
            LogMethod();
            return Modbus.Read<Frecuencia>();
        }


        #endregion

        #region Adjust methods

        public Tuple<bool, CalibrationFactors> CalculateVoltageCurrentCalibrationFactors(int delFirst, int initCount, int samples, int interval, TriLineValue voltage, TriLineValue current, double transformationRelatio, Func<CalibrationFactors, bool> adjustValidation)
        {
            FlagTest(true); // se escribe el flag de test

            var gains = ReadCalibrationFactors();

            current.L1 *= transformationRelatio;
            current.L2 *= transformationRelatio;
            current.L3 *= transformationRelatio;

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, ushort.MaxValue,
                () =>
                {
                    var allVariables = ReadConverterPoints();

                    var varlist = new List<double>(){
                       allVariables.VoltageL1 / allVariables.AmountOfSamples,
                       allVariables.VoltageL2 / allVariables.AmountOfSamples,
                       allVariables.VoltageL3 / allVariables.AmountOfSamples,
                       allVariables.CurrentL1 / allVariables.AmountOfSamples,
                       allVariables.CurrentL2 / allVariables.AmountOfSamples,
                       allVariables.CurrentL3 / allVariables.AmountOfSamples};

                    return varlist.ToArray();
                },
                (listValues) =>
                {
                    gains.VoltageL1 = Convert.ToUInt16(10000 * Math.Pow(voltage.L1, 2) / listValues.Average(0));
                    gains.VoltageL2 = Convert.ToUInt16(10000 * Math.Pow(voltage.L2, 2) / listValues.Average(1));
                    gains.VoltageL3 = Convert.ToUInt16(10000 * Math.Pow(voltage.L3, 2) / listValues.Average(2));
                    gains.CurrentL1 = Convert.ToUInt16(100 * Math.Pow(current.L1 * 1000, 2) / listValues.Average(3));
                    gains.CurrentL2 = Convert.ToUInt16(100 * Math.Pow(current.L2 * 1000, 2) / listValues.Average(4));
                    gains.CurrentL3 = Convert.ToUInt16(100 * Math.Pow(current.L3 * 1000, 2) / listValues.Average(5));

                    return adjustValidation(gains);
                });

            return Tuple.Create(adjustResult.Item1, gains);
        }

        public Tuple<bool, PhaseGapFactors> CalculateGapAdjustFactors(int delFirst, int initCount, int samples, int interval, TriLineValue powerReference, double offsetReference, double transformationRelation, Func<PhaseGapFactors, bool> adjustValidation, Func<TriLineValue, bool> sampleValidation = null)
        {
            double angle = 0.0D;
            double angleError = 0.0D;
            double angleErrorCorrected = 0.0D;
            var result = new PhaseGapFactors();

            var adjustResult = AdjustBase(delFirst, initCount, samples, interval, ushort.MaxValue,
                () =>
                {
                    var electricalVariables = ReadPhaseVariables();

                    return new double[] { (electricalVariables.L1.PotenciaActiva - powerReference.L1) / powerReference.L1,
                    (electricalVariables.L2.PotenciaActiva- powerReference.L2) / powerReference.L2,
                    (electricalVariables.L3.PotenciaActiva- powerReference.L3) / powerReference.L3};
                },
                (listValues) =>
                {
                    angle = (1 - listValues.Average(0)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (760E-9);
                    result.L1 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    angle = (1 - listValues.Average(1)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (760E-9);
                    result.L2 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    angle = (1 - listValues.Average(2)) * Math.Cos(offsetReference * Math.PI / 180);
                    angleError = (Math.Atan(-angle / Math.Sqrt(-angle * angle + 1)) + 2 * Math.Atan(1)) * 180 / Math.PI - offsetReference;
                    angleErrorCorrected = (angleError * 0.02 / 360) / (760E-9);
                    result.L3 = angleErrorCorrected < 0 ? (ushort)0 : (ushort)angleErrorCorrected;
                    return adjustValidation(result);
                });

            return Tuple.Create(adjustResult.Item1, result);
        }


        #endregion

        #region Diccionarios

        public Dictionary<DisplayState, bool[]> displayDictionary_V1 = new Dictionary<DisplayState, bool[]>
        {
            {DisplayState.NO_SEGMENTS, noSegments_V1},
            {DisplayState.HORIZONTAL_SEGMENTS, horizontalDisplaySegments_V1},
            {DisplayState.VERTICAL_SEGMENTS, verticalDisplaySegments_V1},
            {DisplayState.SYMBOL_SEGMENTS, symbolDisplaySegments_V1},
            {DisplayState.EVEN_SEGMENTS, evenDisplaySegments_V1},
            {DisplayState.ODD_SEGMENTS, oddDisplaySegments_V1},
            {DisplayState.ALL_SEGMENTS, allDisplaySegments_V1},
        };

        public Dictionary<DisplayState, bool[]> displayDictionary_V2 = new Dictionary<DisplayState, bool[]>
        {
            {DisplayState.NO_SEGMENTS, noSegments_V2},
            {DisplayState.HORIZONTAL_SEGMENTS, horizontalDisplaySegments_V2},
            {DisplayState.VERTICAL_SEGMENTS, verticalDisplaySegments_V2},
            {DisplayState.SYMBOL_SEGMENTS, symbolDisplaySegments_V2},
            {DisplayState.EVEN_SEGMENTS, evenDisplaySegments_V2},
            {DisplayState.ODD_SEGMENTS, oddDisplaySegments_V2 },
            {DisplayState.ALL_SEGMENTS, allDisplaySegments_V2},
        };

        #endregion

        #region Information Arrays

        public static readonly bool[] noSegments_V2 = new bool[176]
        {
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] horizontalDisplaySegments_V2 = new bool[176]
        {
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] verticalDisplaySegments_V2 = new bool[176]
        {
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] symbolDisplaySegments_V2 = new bool[176]
        {
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
        };

        public static readonly bool[] evenDisplaySegments_V2 = new bool[176]
{
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
};

        public static readonly bool[] oddDisplaySegments_V2 = new bool[176] 
{
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
};

        public static readonly bool[] allDisplaySegments_V2 = new bool[176]
{
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
};

        public static readonly bool[] noSegments_V1 = new bool[160]
{
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
};

        public static readonly bool[] horizontalDisplaySegments_V1 = new bool[160]
        {
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            true,   false,  false,  true,   false,  false,  true,   false,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] verticalDisplaySegments_V1 = new bool[160]
        {
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  true,   true,   false,  false,  true,   false,  true,
            false,  false,  false,  false,  false,  false,  false,  false,
            false,  false,  false,  false,  false,  false,  false,  false,
        };

        public static readonly bool[] symbolDisplaySegments_V1 = new bool[160]
        {
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            false,  false,  false,  false,  true,   false,  false,  false,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
        };
        public static readonly bool[] evenDisplaySegments_V1 = new bool[160]
{
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
            false, false, false, false, false, true, false, true,
};
        public static readonly bool[] oddDisplaySegments_V1 = new bool[160] 
{
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
            true, false, true, false, false, false, false, false,
};
        public static readonly bool[] allDisplaySegments_V1 = new bool[160]
{
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
            true,   true,   true,   true,   true,   true,   true,   true,
};


        #endregion

        #region Vector de Wardware

        public class HardwareVector
        {
            private string powerSupplyString;
            private string inputVoltageString;
            private string inputCurrentString;
            private string modeloString;
            private string vectorHardwareString;
            private string vectorHardwareBoolean;

            public bool Rele { get; set; }
            public bool Rs232 { get; set; }
            public bool Rs485 { get { return !Rs232; } }
            public bool Comunication { get; set; }
            public bool Shunt { get; set; }
            public bool In_Medida { get; set; }
            public bool CalculoHarmonicos { get; set; }

            public string ReleString { get { return Rele == true ? "SI" : "NO"; }  }
            public string Rs232String { get { return Rs232 == true ? "SI" : "NO"; } }
            public string ComunicationString { get { return Comunication == true ? "SI" : "NO"; } }
            public string ShuntString { get { return Shunt == true ? "SI" : "NO"; } }
            public string In_MedidaString { get { return In_Medida == true ? "SI" : "NO"; } }
            public string CalculoHarmonicosString { get { return CalculoHarmonicos == true ? "SI" : "NO"; } }

            public VectorHardwareCurrentInputs InputCurrent { get; set; }
            public VectorHardwareVoltageInputs InputVoltage { get; set; }
            public VectorHardwareSupply PowerSupply { get; set; }
            public VectorHardwareModelo Modelo { get; set; }

            public string PowerSupplyString
            {
                get
                {
                    return powerSupplyString;
                }
                set
                {
                    powerSupplyString = value;
                    if (powerSupplyString.ToUpper().Contains("AC"))
                    {
                        if (powerSupplyString.Contains("230"))
                        {
                            PowerSupply = VectorHardwareSupply._230Vac;
                            return;
                        }
                        else
                        {
                            if (powerSupplyString.Contains("400"))
                            {
                                PowerSupply = VectorHardwareSupply._400Vac;
                                return;
                            }
                            else
                            {
                                if (powerSupplyString.Contains("480"))
                                {
                                    PowerSupply = VectorHardwareSupply._480Vac;
                                    return;
                                }
                                else
                                {
                                    if (powerSupplyString.Contains("85") || powerSupplyString.Contains("265"))
                                    {
                                        PowerSupply = VectorHardwareSupply._85_265Vac;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (powerSupplyString.Contains("20") || powerSupplyString.Contains("21") || powerSupplyString.Contains("90") || powerSupplyString.Contains("120"))
                        {
                            PowerSupply = VectorHardwareSupply._20_120Vdc;
                            return;
                        }
                    }
                    throw new Exception("Error valor de alimentación no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputVoltageString
            {
                get
                {
                    return inputVoltageString;
                }
                set
                {
                    inputVoltageString = value;

                    if (inputVoltageString.Contains("300"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary300V;
                        return;
                    }
                    else if (inputVoltageString.Contains("110"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary110V;
                        return;
                    }
                    else if (inputVoltageString.Contains("500"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary500V;
                        return;
                    }
                    else if (inputVoltageString.Contains("275"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary275V;
                        return;
                    }
                    else if (inputVoltageString.Contains("250"))
                    {
                        InputVoltage = VectorHardwareVoltageInputs.Primary250V;
                        return;
                    }
                    throw new Exception("Error valor de entradas de tensión no permitido en la codificación del Vector de Hardware");
                }
            }
            public string InputCurrentString
            {
                get
                {
                    return inputCurrentString;
                }
                set
                {
                    inputCurrentString = value;

                    if (inputCurrentString.Contains("250:5000"))
                    {
                        InputCurrent = VectorHardwareCurrentInputs.MC;
                        return;
                    }
                    else
                    {
                        if (inputCurrentString.Contains("5000:5000"))
                        {
                            InputCurrent = VectorHardwareCurrentInputs.Primary5A;
                            return;
                        }
                        else
                        {
                            if (inputCurrentString.Contains("2000:2000"))
                            {
                                InputCurrent = VectorHardwareCurrentInputs.Primary2A;
                                return;
                            }
                            else
                            {
                                if (inputCurrentString.Contains("1000:1000"))
                                {
                                    InputCurrent = VectorHardwareCurrentInputs.Primary1A;
                                    return;
                                }
                                else
                                    throw new Exception("Error El formato del valor de la entrada de corriente del vector de HW no es correcto");
                            }
                        }
                    }
                }
            }
            public string ModeloString
            {
                get
                {
                    return modeloString;
                }
                set
                {
                    modeloString = value;

                    if (modeloString.Contains("CVM-C5"))
                    {
                        Modelo = VectorHardwareModelo.CVM_C5;
                        return;
                    }
                    throw new Exception("Error valor de modelo no permitido en la codificación del Vector de Hardware");
                }
            }
            public string VectorHardwareTrama { get { return vectorHardwareString; } }
            public string VectorHardwareBoolean { get { return vectorHardwareBoolean; } }

            public HardwareVector()
            {

            }

            public HardwareVector(byte[] vectorHardware)
            {
                Comunication = (vectorHardware[0] & 0x01) == 0x01;
                Rs232 = (vectorHardware[0] & 0x02) == 0x02;
                Rele = (vectorHardware[0] & 0x10) == 0x10;
                Shunt = (vectorHardware[1] & 0x08) == 0x08;
                InputCurrent = (VectorHardwareCurrentInputs)(vectorHardware[1] & 0x07);
                InputVoltage = (VectorHardwareVoltageInputs)(vectorHardware[1] & 0x70);
                PowerSupply = (VectorHardwareSupply)(vectorHardware[2] & 0x0F);
                In_Medida = (vectorHardware[3] & 0x01) == 0x01;
                CalculoHarmonicos = (vectorHardware[10] & 0x01) == 0x01;
                Modelo = (VectorHardwareModelo)(vectorHardware[11]);

                vectorHardwareString = vectorHardware.BytesToHexString();
            }

            public bool[] ToSingleCoil()
            {
                var vector = new bool[96];

                vector[0] = Comunication;
                vector[1] = Rs232;
                vector[4] = Rele;

                vector[8] = ((int)InputCurrent & 0x01) == 0x01;
                vector[9] = ((int)InputCurrent & 0x02) == 0x02;
                vector[10] = ((int)InputCurrent & 0x04) == 0x04;

                vector[11] = Shunt;

                vector[12] = ((int)InputVoltage & 0x01) == 0x01;
                vector[13] = ((int)InputVoltage & 0x02) == 0x02;
                vector[14] = ((int)InputVoltage & 0x04) == 0x04;

                vector[16] = ((int)PowerSupply & 0x01) == 0x01;
                vector[17] = ((int)PowerSupply & 0x02) == 0x02;
                vector[18] = ((int)PowerSupply & 0x04) == 0x04;
                vector[19] = ((int)PowerSupply & 0x08) == 0x08;

                vector[24] = In_Medida;
                vector[80] = CalculoHarmonicos;

                vector[88] = ((int)Modelo & 0x01) == 1;
                vector[89] = ((int)Modelo & 0x02) == 2;
                vector[90] = ((int)Modelo & 0x04) == 4;
                vector[91] = ((int)Modelo & 0x08) == 8;
                vector[92] = ((int)Modelo & 0x16) == 16;
                vector[93] = ((int)Modelo & 0x32) == 32;
                vector[94] = ((int)Modelo & 0x64) == 64;
                vector[95] = ((int)Modelo & 0x128) == 128;

                vectorHardwareBoolean = "";

                foreach (bool i in vector)
                    vectorHardwareBoolean += Convert.ToByte(i).ToString();

                return vector;
            }
        }

        public enum VectorHardwareCurrentInputs
        {
            Primary1A = 0,
            Primary5A = 1,
            Primary2A = 2,
            MC = 7,
        }

        public enum VectorHardwareVoltageInputs
        {
            Primary300V = 0,
            Primary110V = 1,
            Primary500V = 2,
            Primary275V = 3,
            Primary250V = 4,
        }

        public enum VectorHardwareSupply
        {
            _230Vac = 0,
            _400Vac = 1,
            _480Vac = 2,
            _20_120Vdc = 3,
            _85_265Vac = 4,
        }

        public enum VectorHardwareModelo
        {
            CVM_C5 = 0x0002,
        }

        #endregion

        #region Estructuras y enumerados

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct AllVariables
        {
            public VariablesInstantaneas Phases;
            public VariablesTrifasicas Trifasicas;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesInstantaneas
        {
            public VariablesFase L1;
            public VariablesFase L2;
            public VariablesFase L3;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0000)]
        public struct VariablesFase
        {
            public Int32 Tension;
            public Int32 Corriente;
            public Int32 PotenciaActiva;
            public Int32 PotenciaReactiva;
            public Int32 FactorPotencia;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x002A)]
        public struct TensionesCompuestas
        {
            public Int32 V1V2;
            public Int32 V2V3;
            public Int32 V3V1;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x0028)]
        public struct Frecuencia
        {
            public Int32 frecuencia;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = 0x001E)]
        public struct VariablesTrifasicas
        {
            public Int32 PotenciaActivaIII;
            public Int32 PotenciaInductivaIII;
            public Int32 PotenciaCapacitivaIII;
            public Int32 CosenoPhiIII;
            public Int32 FactorPotenciaIII;
            public Int32 Frecuencia;
            public Int32 TensionLineaL1L2;
            public Int32 TensionLineaL2L3;
            public Int32 TensionLineaL3L1;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)ElectricalVariablesRegisters.THD)]
        public struct TotalHarmonicDistorsion
        {
            private Int32 voltageL1;
            private Int32 voltageL2;
            private Int32 voltageL3;
            private Int32 currentL1;
            private Int32 currentL2;
            private Int32 currentL3;

            public double VoltageL1 { get { return Convert.ToDouble(voltageL1) / 10; } }
            public double VoltageL2 { get { return Convert.ToDouble(voltageL2) / 10; } }
            public double VoltageL3 { get { return Convert.ToDouble(voltageL3) / 10; } }
            public double CurrentL1 { get { return Convert.ToDouble(currentL1) / 10; } }
            public double CurrentL2 { get { return Convert.ToDouble(currentL2) / 10; } }
            public double CurrentL3 { get { return Convert.ToDouble(currentL3) / 10; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.SETUP_CONFIGURATION)]
        public struct SetupConfiguration
        {
            public SetupConfiguration(int voltagePrimary, ushort voltageSecondary, ushort currentPrimary, string voltageType, MainDefaultScreens mainDefaultPage, EnergyDefaultScreens energyDefaultScreen, bool calculateHarmonics, byte backlightShutdownTime)
            {
                if (voltagePrimary > 100000)
                    throw new ArgumentOutOfRangeException("La tensión de primario introducida esta fuera de márgenes");
                if (voltageSecondary > 999)
                    throw new ArgumentOutOfRangeException("La tensión de secundario introducida esta fuera de márgenes");
                if (currentPrimary > 10000)
                    throw new ArgumentOutOfRangeException("La corriente de primario introducida esta fuera de márgenes");
                if (backlightShutdownTime > 99)
                    throw new ArgumentOutOfRangeException("El tiempo de apagado del backlight introducido esta fuera de márgenes");

                VoltagePrimary = voltagePrimary;
                VoltageSecondary = voltageSecondary;
                CurrentPrimary = currentPrimary;
                if (Enum.IsDefined(typeof(VoltageTypes), voltageType))
                {
                    VoltageTypes voltageTypeConversion = VoltageTypes.SIMPLE;

                    var conversionSuccesful = Enum.TryParse<VoltageTypes>(voltageType, true, out voltageTypeConversion);
                    VoltageType = voltageTypeConversion == VoltageTypes.SIMPLE ? (byte)0 : (byte)1;
                }
                else
                    throw new ArgumentException("El tipo de tensión que se intenta introducir no es válido");

                DefaultPage = (byte)((int)energyDefaultScreen * 16 + mainDefaultPage);
                CalculateHarmonics = calculateHarmonics ? (byte)1 : (byte)0;
                BacklightShutdownTime = backlightShutdownTime;
            }

            public int VoltagePrimary;
            public ushort VoltageSecondary;
            public ushort CurrentPrimary;
            public byte VoltageType;
            public byte DefaultPage;
            public byte CalculateHarmonics;
            public byte BacklightShutdownTime;

            public enum VoltageTypes
            {
                SIMPLE,
                COMPOUND
            }

            public enum MainDefaultScreens
            {
                VOLTAGE,
                CURRENT,
                ACTIVE_POWER,
                REACTIVE_POWER,
                POWER_FACTOR,
                VOLTAGE_THD,
                CURRENT_THD,
                ACTIVE_INDUCTIVE_CAPACITIVE_POWER_THREE_PHASE,
                ACTIVE_INDUCTIVE_APPARENT_POWER_THREE_PHASE,
                MAXIMUM_DEMAND_FRECUENCY_POWER_FACTOR_THREE_PHASE,
                MAXIMUM_DEMAND_FRECUENCY_PHI_COSINE_THREE_PHASE,
            }

            public enum EnergyDefaultScreens
            {
                ACTIVE_CONSUMED,
                INDUCTIVE_CONSUMED,
                CAPACITIVE_CONSUMED,
                APPARENT_CONSUMED,
                ACTIVE_GENERATED,
                INDUCTIVE_GENERATED,
                CAPACITIVE_GENERATED,
                APPARENT_GENERATED
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ALARM_CONFIGURATION)]
        public struct AlarmConfiguration
        {
            public int MaximumValue;
            public int MinimumValue;
            public ushort Delay;
            private byte dummyByte;
            public byte VariableNumber;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.MAXIMUM_DEMAND_CONFIGURATION)]
        public struct MaximumDemandConfiguration
        {
            public ushort MaximumDemandVariable;
            public ushort RegisterTime;

            public string VariabletoCalculate
            {
                get
                {
                    return ((MaximumDemandVariables)MaximumDemandVariable).ToString();
                }
                set
                {
                    switch (value)
                    {
                        case "NO_PD":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.NO_PD;
                            break;
                        case "ACTIVE":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.ÀCTIVE;
                            break;
                        case "REACTIVE":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.REACTIVE;
                            break;
                        case "CURRENT_AVERAGE":
                            MaximumDemandVariable = (ushort)MaximumDemandVariables.CURRENT_AVERAGE;
                            break;
                        default:
                            throw new NotImplementedException("Error de parametrización de máxima demanda, la variable a monitorizar selccionada no existe");
                    }
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.PASSWORD_CONFIGURATION)]
        public struct PasswordConfiguration
        {
            public ushort Password;
            public byte DUMMY_BYTE;
            public byte SetupLocking;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.COMMUNICATIONS_CONFIGURATION)]
        public struct CommunicationsConfiguration
        {
            public CommunicationsConfiguration(byte periferic, int baudRate)
            {
                PerifericNumberAndProtocol = (ushort)(0x0000 + periferic);
                BaudsAndParity = BaudrateValues[baudRate];
                StopBytesAndDataLength = 0x0800;
            }

            public ushort PerifericNumberAndProtocol;
            public ushort BaudsAndParity;
            public ushort StopBytesAndDataLength;

            private static readonly Dictionary<int,ushort> BaudrateValues = new Dictionary<int, ushort>
            {
                {1200,0x0000},
                {2400,0x0100},
                {4800,0x0200},
                {9600,0x0300},
                {19200,0x0400},
            };
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CALIBRATION_FACTORS)]
        public struct CalibrationFactors
        {
            public CalibrationFactors(ushort currentFactor, ushort voltageFactor)
            {
                CurrentL1 = CurrentL2 = CurrentL3 = currentFactor;
                VoltageL1 = VoltageL2 = VoltageL3 = voltageFactor;
            }

            public ushort CurrentL1;
            public ushort VoltageL1;
            public ushort CurrentL2;
            public ushort VoltageL2;
            public ushort CurrentL3;
            public ushort VoltageL3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriCalibrationFactors
        {
            public TriCalibrationFactors(ushort factor)
            {
                L1 = L2 = L3 = factor;
            }

            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.PHASE_CALIBRATION_FACTORS)]
        public struct PhaseGapFactors
        {
            public PhaseGapFactors(ushort factor)
            {
                L1 = L2 = L3 = factor;
            }
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.TENSIONES_COMPUESTAS_FACTORS)]
        public struct TesnsionesCompuestas
        {
            public TesnsionesCompuestas(ushort factor)
            {
                Factor = factor;
            }

            public ushort Factor;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CONVERTER_POINTS)]
        public struct ConverterPoints
        {
            public int CurrentL1;
            public int VoltageL1;
            public int CurrentL2;
            public int VoltageL2;
            public int CurrentL3;
            public int VoltageL3;
            public int VoltageL12;
            public int VoltageL23;
            public int VoltageL31;
            public int ActivePowerL1;
            public int ActivePowerL2;
            public int ActivePowerL3;
            public ushort AmountOfSamples;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.CHANNELS_OFFSET)]
        public struct ChannelOffsets
        {
            public ushort CurrentL1;
            public ushort VoltageL1;
            public ushort CurrentL2;
            public ushort VoltageL2;
            public ushort CurrentL3;
            public ushort VoltageL3;
            public ushort VoltageL32; 
            public ushort VoltageL31;
        }

        public struct ScreenConfiguration
        {
            public bool VoltagePhaseNeuter;
            public bool VoltagePhasePhase;
            public bool Current;
            public bool ActivePower;
            public bool ReactivePower;
            public bool PowerFactor;
            public bool VoltageTHD;
            public bool CurrentTHD;
            public bool ThreePhaseActiveInductiveCapacitivePower;
            public bool ThreePhaseActiveReactiveApparentPower;
            public bool PdHzPF;
            public bool PdHzCos;
            public bool PhasesPd;

            public ScreenConfiguration(byte[] screenConfiguration)
            {
                VoltagePhaseNeuter = (screenConfiguration[3] & 0x01) == 0x01;
                VoltagePhasePhase = (screenConfiguration[3] & 0x02) == 0x02;
                Current = (screenConfiguration[3] & 0x04) == 0x04;
                ActivePower = (screenConfiguration[3] & 0x08) == 0x08;
                ReactivePower = (screenConfiguration[3] & 0x10) == 0x10;
                PowerFactor = (screenConfiguration[3] & 0x20) == 0x20;
                VoltageTHD = (screenConfiguration[3] & 0x40) == 0x40;
                CurrentTHD = (screenConfiguration[3] & 0x80) == 0x80;
                ThreePhaseActiveInductiveCapacitivePower = (screenConfiguration[2] & 0x01) == 0x01;
                ThreePhaseActiveReactiveApparentPower = (screenConfiguration[2] & 0x02) == 0x02;
                PdHzPF = (screenConfiguration[2] & 0x04) == 0x04;
                PdHzCos = (screenConfiguration[2] & 0x08) == 0x08;
                PhasesPd = (screenConfiguration[2] & 0x10) == 0x10;
            }

            public bool[] ToBoolArray()
            {
                return new bool[32] { VoltagePhaseNeuter, VoltagePhasePhase, Current, ActivePower, ReactivePower, PowerFactor, VoltageTHD, CurrentTHD,
                    ThreePhaseActiveInductiveCapacitivePower, ThreePhaseActiveReactiveApparentPower, PdHzPF, PdHzCos, PhasesPd, false, false, false,
                    false, false, false, false, false, false, false, false,
                    false, false, false, false, false, false, false, false
                };
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.LOAD_ENERGY)]
        public struct Energies
        {
            public int ActiveEnergy;
            public int InductiveEnergy;
            public int CapacitiveEnergy;

            public bool ComprobarBorrado()
            {
                return ActiveEnergy == 0 && InductiveEnergy == 0 && CapacitiveEnergy == 0;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriUShort
        {
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriInt
        {
            public ushort L1;
            public ushort L2;
            public ushort L3;
        }

        public struct TriDouble
        {
            public double L1;
            public double L2;
            public double L3;
            public double LN;
        }  

        [Flags]
        public enum KeyboardKeys
        {
            NONE = 0x00,
            [Description("<")]
            RESET = 0x02,
            [Description("Central 1")]
            DISPLAY = 0x04,
            [Description("Central 2")]
            MAX = 0x08,
            [Description(">")]
            MIN = 0x10,

            OTRAS = 0xE0
        }

        public enum DisplayState
        {
            NO_SEGMENTS,
            HORIZONTAL_SEGMENTS,
            VERTICAL_SEGMENTS,
            SYMBOL_SEGMENTS,
            EVEN_SEGMENTS,
            ODD_SEGMENTS,
            ALL_SEGMENTS
        }

        public enum State
        {
            ON,
            OFF
        }

        public enum Alarms
        {
            ALARM_1_CONFIGURATION = 1150, // 7 Registros
            ALARM_2_CONFIGURATION = 1155, // 7 Registros
        }

        public enum MaximumDemandCalculusType
        {
            NO = 0,
            KW_TRIPHASE = 16,
            KVA = 34,
            AVERAGE_CURRENT = 36
        }

        public enum Registers
        {
            OUTPUT_KEYBOARD_INPUTS = 0, // 5 Registros, 
            KEYBOARD = 1,
            BACKLIGHT = 8, // 2 registros
            DISPLAY_SEGMENTS = 1000, // 160 Registros
            DISPLAY_SEGMENTS_2 = 1077, // 160 Registros
            RESET = 2000,
            CLEAR_ENERGY = 2100,
            INITIALIZE_MAX_DEMAND = 2104,
            CLEAR_MAX_MIN = 2102,
            CLEAR_ENERGY_MAX_MIN_MD = 2103,
            CLEAR_MAX_DEMAND = 2104,
            HARDWARE_CONFIG = 10000, // 64 Registros
            FLAG_TEST = 11000,
            APPLY_CALIBRATION = 11100,
            ELECTRICAL_VARIABLES = 0,// 500 Registros
            COMMUNICATIONS_CONFIGURATION = 1000,// 3 Registros
            PASSWORD_CONFIGURATION = 1050,// 2 Registros
            SETUP_CONFIGURATION = 1100,// 6 Registros
            MAXIMUM_DEMAND_CONFIGURATION = 1250, // 2 Registros
            INITIAL_MESSAGE = 1300, // 4 Registros
            SOFTWARE_VERSION = 1400, // 6 Registros
            EEPROM_ERROR_CODE = 1500,
            PERIFERIC_BAUD_CONFIGURATION = 3000, // 3 Registros
            ACTIVE_TARIFF_SELECTION = 5000, // 2 Registros
            PRESET_HARDWARE = 5000,
            SERIAL_NUMBER = 10000, // 2 Registros
            CALIBRATION_FACTORS = 11000, // 6 Registros
            PHASE_CALIBRATION_FACTORS = 11050, // 3 Registros
            TENSIONES_COMPUESTAS_FACTORS = 11060,
            CONVERTER_POINTS = 11100, // 53 Registros
            CHANNELS_OFFSET = 11200, // 8 Registros
            LOAD_ENERGY = 20000, // 12 Registros
            CODE_ROM = 21100,
            ALARM_CONFIGURATION = 0x047E,
            SCREEN_CONFIGURATION = 0x0898,

        }

        public enum Tariff
        {
            FARE_1 = 0x0000,
            FARE_2 = 0x0001
        }

        public enum ElectricalVariablesRegisters
        {
            L1 = 0x0000,
            L2 = 0x0010,
            L3 = 0x0020,
            COMPUESTA = 0x001E,
            THD = 0x0030
        }

        public enum MaximumDemandVariables
        {
            NO_PD = 0,
            ÀCTIVE = 16,
            REACTIVE = 34,
            CURRENT_AVERAGE = 36
        }

        [Flags]
        public enum EEPROMErrorCodes
        {
            NO_ERROR = 0x0000,
            HARDWARE_VECTOR_NOT_WRITTEN = 0x0001,
            COMMUNICATIONS_INCORRECT_OR_NOT_WRITTEN = 0x0002,
            PASSWORD_NOT_WRITTEN = 0x0004,
            MAIN_SETUP_PAGE_SELECTION = 0x0008,
            INITIAL_SCREEN_MESSAGE_NOT_WRITTEN = 0x0010,
            ALARM_NOT_INITIALIZED = 0x0020,
            ENERGIES_NOT_WRITTEN_OR_NEGATIVE_VALUES = 0x0080,
            CALIBRATION_FACTORS_NOT_WRITTEN = 0x0100,
            SERIAL_NUMBER_NOT_WRITTEN = 0x0200,
            MAXIMUM_DEMAND_CONFIGURATION_ERROR = 0x0400
        }

        #endregion

        public override void Dispose()
        {
            Modbus.Dispose();
        }
    }
}
