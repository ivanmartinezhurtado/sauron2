﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using TaskRunner.Model;
using WinTaskRunner.Services;

namespace WinTaskRunner.Views
{
    public partial class VariablesView : UserControl
    {
        private const int REFRESHING_PERIOD = 1000;

        protected AppService app;
        private SynchronizationContext sc;
        private System.Timers.Timer timer1;
        private bool refreshingRequested = false;
        private ConcurrentDictionary<string, List<VariableLogEntry>> fillVariablesToGrid;
        private List<string> VariableChek = new List<string>();
        private bool testEnded = false;

        public ReadOnlyDictionary<string, List<VariableLogEntry>> FillVariablesToGrid
        {
            get
            {
                return new ReadOnlyDictionary<string, List<VariableLogEntry>>(fillVariablesToGrid);
            }
        }

        public VariablesView()
        {
            InitializeComponent();

            app = DI.Resolve<AppService>();
            sc = SynchronizationContext.Current;
            Disposed += OnDisposed;
            timer1 = new System.Timers.Timer();

            timer1.Interval = REFRESHING_PERIOD;
            timer1.Elapsed += (s, e) => { timer1_Tick(s, e); };
            timer1.Enabled = false;

            app.Runner.AllTestStarting += OnAllTestStarting;
            app.Runner.TestStart += OnTestStart;
            app.Runner.AllTestEnded += OnAllTestEnding;
        }

        private void OnAllTestStarting(object sender, RunnerTestsInfo e)
        {
            splitContainer1.Panel2Collapsed = true;
            dataGridView1.Rows.Clear();
            dataGridLogPlot.Rows.Clear();
            chart.Series.Clear();

            if (fillVariablesToGrid != null)
            {
                fillVariablesToGrid.Clear();
                fillVariablesToGrid = null;
            }

            fillVariablesToGrid = new ConcurrentDictionary<string, List<VariableLogEntry>>();
            VariableChek.Clear();
            timer1.Enabled = true;
            timer1.Start();
            testEnded = false;

            foreach(var context in e.Contexts)
                context.Variables.EntryAdded += OnVariablesChanged;
        }

        private void OnAllTestEnding(object sender, RunnerTestsInfo e)
        {
            timer1.Stop();
            timer1.Enabled = false;
            testEnded = true;
        }

        private void OnTestStart(object sender, SequenceContext sequenceContext)
        {
            //sequenceContext.Variables.EntryAdded += OnVariablesChanged;
        }

        private void OnDisposed(object sender, EventArgs e)
        {
            app.Runner.AllTestStarting -= OnAllTestStarting;
            app.Runner.TestStart -= OnTestStart;
            app.Runner.AllTestEnded -= OnAllTestEnding;
            timer1.Dispose();
        }

        void OnVariablesChanged(object sender, VariableLogEntry e)
        {
            if (e.Value == null)
                return;
            
            if (fillVariablesToGrid.ContainsKey(e.Variable))
            {
                var ListVariable = fillVariablesToGrid[e.Variable];
                ListVariable.Add(e);
            } else
            {
                var ListVariableLogEntry = new List<VariableLogEntry>();
                ListVariableLogEntry.Add(e);
                fillVariablesToGrid.AddOrUpdate(e.Variable, ListVariableLogEntry, (k, curValue) => ListVariableLogEntry);
            }

            refreshingRequested = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            if (refreshingRequested)
            {
                var dictionaryCopy =  fillVariablesToGrid.ToDictionary(x => x.Key, x => x.Value);
                PostDataGridView(dictionaryCopy);
            }
                        
            refreshingRequested = false;

           timer1.Start();
        }

        private void PostDataGridView(Dictionary<string, List<VariableLogEntry>> variablesToGrid)
        {
            var variableClone = new Dictionary<string, List<VariableLogEntry>>();
            var variablePlot = new Dictionary<string, List<VariableLogEntry>>();

            foreach (var key in variablesToGrid)
            {
                variableClone.Add(key.Key, key.Value);

                if (VariableChek.Count > 0)
                {
                    var rowChek = VariableChek.Find((p) => p == key.Key);
                    if (rowChek != null)
                        variablePlot.Add(key.Key, key.Value);
                }
            }

            sc.Post(_ =>
            {
                AddUpdateRowGrid(variableClone);

                splitContainer1.Panel2Collapsed = VariableChek.Count <= 0;

                if (VariableChek.Count > 0)
                    AddSeries(variablePlot);

            }, null);
        }

        public void AddUpdateRowGrid(Dictionary<string, List<VariableLogEntry>> variableClone)
        {
            foreach (var key in variableClone)
            {
                var valueList = key.Value.LastOrDefault();

                if (dataGridView1.Rows.Count > 0)
                {
                    var row = dataGridView1.Rows
                   .Cast<DataGridViewRow>()
                   .FirstOrDefault(r => r.Cells["Variable"].Value.ToString()==key.Key && r.Cells["Instancia"].Value.ToString() == key.Value.FirstOrDefault().Instancia.ToString());

                    if (row != null)
                    {
                        row.Cells["Valor"].Value = valueList.Value;
                        row.Cells["Tiempo"].Value = valueList.TimeSpan;
                    }
                    else
                        dataGridView1.Rows.Add(valueList.Instancia, valueList.TimeSpan, valueList.Variable, valueList.Value);
                }
                else
                    dataGridView1.Rows.Add(valueList.Instancia, valueList.TimeSpan, valueList.Variable, valueList.Value);
            }

            if (dataGridView1.SortedColumn != null)
            {
                if (dataGridView1.SortOrder == SortOrder.Ascending)
                    dataGridView1.Sort(dataGridView1.SortedColumn, System.ComponentModel.ListSortDirection.Ascending);
                else if (dataGridView1.SortOrder == SortOrder.Descending)
                    dataGridView1.Sort(dataGridView1.SortedColumn, System.ComponentModel.ListSortDirection.Descending);
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0)
                return;

            if (dataGridView1.Columns[e.ColumnIndex].HeaderText == "Plot")
            {
                var itemRow = this.dataGridView1.CurrentRow.Cells["Plot"];
                if (itemRow.Value == null)
                    itemRow.Value = true;
                else if (Convert.ToBoolean(itemRow.Value))
                    itemRow.Value = false;
                else
                    itemRow.Value = true;

                if (Convert.ToBoolean(itemRow.Value))
                {
                    var Rowkey = this.dataGridView1.CurrentRow.Cells["Variable"];
                    var Value = this.dataGridView1.CurrentRow.Cells["Valor"];
                    VariableChek.Add(Rowkey.Value.ToString());
                }
                else
                {
                    var Rowkey = this.dataGridView1.CurrentRow.Cells["Variable"];
                    var instance = this.dataGridView1.CurrentRow.Cells["Instancia"];
                    VariableChek.Remove(Rowkey.Value.ToString());
                    var captionSerie = string.Format("{0}-{1}", instance.Value.ToString(), Rowkey.Value.ToString());
                    sc.Post(_ =>
                    {
                        RemoveSerie(captionSerie);
                        RemoveDataLogGrid(Rowkey.Value.ToString());
                    }, null);

                    splitContainer1.Panel2Collapsed = VariableChek.Count == 0;
                }

                if (testEnded && VariableChek.Count > 0)
                {
                    var dictionaryCopy = fillVariablesToGrid.ToDictionary(x => x.Key, x => x.Value);
                    PostDataGridView(dictionaryCopy);
                }


            }
        }

        private void AddSeries(Dictionary<string, List<VariableLogEntry>> variableClone)
        {
            foreach (var serie in variableClone)
            {
                AddSeriePlotDataGrid(serie.Value.ToArray());
                AddUpdateRowGridToLogger(serie.Value.ToArray());
                Application.DoEvents();
            }
        }

        private void AddSeriePlotDataGrid(VariableLogEntry[] variable)
        {
            Series serie;
            var captionSerie = string.Format("{0}-{1}", variable.FirstOrDefault().Instancia, variable.FirstOrDefault().Variable);
       
            if (chart.Series.IndexOf(captionSerie) < 0)
            {
                serie = new Series();
                serie.ChartArea = "ChartArea1";
                serie.ChartType = SeriesChartType.FastLine; // Spline
                serie.XValueType = ChartValueType.Time;
                serie.Name = captionSerie;
                serie.LegendText = captionSerie;
                chart.Series.Add(serie);
            }
            else
            {
                serie = chart.Series.FindByName(captionSerie);
                serie.Points.Clear();
            }

            for (var i = 0; i< variable.Count(); i++)
                serie.Points.AddXY(variable[i].TimeSpan, variable[i].Value);
        }

        public void AddUpdateRowGridToLogger(VariableLogEntry[] variableClone)
        {
            for (var i = 0; i < variableClone.Count(); i++)
            {
                if (dataGridLogPlot.Rows.Count > 0)
                {
                    var row = dataGridLogPlot.Rows
                        .Cast<DataGridViewRow>()
                        .FirstOrDefault(r => r.Cells["Parameter"].Value.ToString() == variableClone[i].Variable && r.Cells["Time"].Value.ToString() == variableClone[i].TimeSpan && r.Cells["Instance"].Value.ToString() == variableClone[i].Instancia.ToString());

                    if (row == null)
                        dataGridLogPlot.Rows.Add(variableClone[i].Instancia, variableClone[i].TimeSpan, variableClone[i].Variable, variableClone[i].Value);
                }
                else
                    dataGridLogPlot.Rows.Add(variableClone[i].Instancia, variableClone[i].TimeSpan, variableClone[i].Variable, variableClone[i].Value);

                dataGridLogPlot.FirstDisplayedScrollingRowIndex = dataGridLogPlot.Rows.Count - 1;
            }
        }

        private void RemoveSerie(string key)
        {
            var serie = chart.Series.FindByName(key);
            serie.Points.Clear();
            chart.Series.Remove(serie);
        }

        private void RemoveDataLogGrid(string key)
        {
            var rows = dataGridLogPlot.Rows
                   .Cast<DataGridViewRow>().Where(r => r.Cells["Parameter"].Value.ToString().Equals(key)).ToList();

            if (rows != null)
                foreach (var row in rows)
                    dataGridLogPlot.Rows.Remove(row);
        }

    }
}
