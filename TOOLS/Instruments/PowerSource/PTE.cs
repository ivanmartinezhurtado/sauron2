﻿using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Exceptions;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.10)]
    public class PTE : PowerSourceACI, IDisposable
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(PTE));

        public enum PrivateCommands
        {
            SET_INIT_CONFIG = Commands.USER_DEFINED_COMMANDS + 1,
            SET_ACTIVE_BOOSTER = Commands.USER_DEFINED_COMMANDS + 2,
            SET_TIMER_RESET = Commands.USER_DEFINED_COMMANDS + 3,
            SET_TRIP_RESET = Commands.USER_DEFINED_COMMANDS + 4,
            SET_MONITOR_RESET = Commands.USER_DEFINED_COMMANDS + 5,
            SET_MONITOR_CONFIG = Commands.USER_DEFINED_COMMANDS + 6,
            SET_TIMER_START = Commands.USER_DEFINED_COMMANDS + 7,
            SET_TIMER_STOP = Commands.USER_DEFINED_COMMANDS + 8,
            SET_TIMER_START_SOFTWARE = Commands.USER_DEFINED_COMMANDS + 9,
            SET_TIMER_STOP_SOFTWARE = Commands.USER_DEFINED_COMMANDS + 10,
            SET_MODO_EXT=Commands.USER_DEFINED_COMMANDS + 11,
            SET_APPLY_OFF_ALL_COMMAND = Commands.USER_DEFINED_COMMANDS + 12,
            APPLY_CONFIG_AND_START_TIMER = Commands.USER_DEFINED_COMMANDS + 13,
            READ_TIME= Commands.USER_DEFINED_COMMANDS + 14,
            SET_FUNCTION = Commands.USER_DEFINED_COMMANDS + 15,
            SET_FUNCTION_CONFIG = Commands.USER_DEFINED_COMMANDS + 16,
            SET_SECONDARY_SOURCE = Commands.USER_DEFINED_COMMANDS + 17,
            START_FUNCTION = Commands.USER_DEFINED_COMMANDS + 18,
            READ_MONITOR = Commands.USER_DEFINED_COMMANDS + 19,
            SET_SOURCE = Commands.USER_DEFINED_COMMANDS + 20,
        };

        public enum SourceType
        {
            PTE_100V = 1,
            PTE_50_CE = 2,
        }

        public enum FunctionResultEnum
        {
            STEPLEVEL,
            TRIP,
            FAILURE,
            IN_PROCCES,
        }

        public FunctionResultEnum FunctionResult { get; set; }

        private readonly static CultureInfo enUS = new CultureInfo("en-US");

        private SerialPort sp;
        private byte _port = 1;
        private string idn = "";

        private const string RESET_COMMAND = "U;";
        private const string SET_INIT_CONFIG = "REM,CONF1REFM,SYLINE;CONF2SYBUS;";
        private const string SET_MODO_EXT = "REM,CONF2REFS,SYEXT;";
        private const string PRESET_CONFIG_VOLTAGE_COMMAND = "V{0};RA{1};L{2:0.###};";
        private const string PRESET_CONFIG_CURRENT_COMMAND = "C1;X1;RA{0};L{1:0.###};";
        private const string PRESET_RANGE_CURRENT_COMMAND = "C1;X1;RA{0};";
        private const string PRESET_CONFIG_FREQUENCY_COMMAND = "F1;FMN;L{0:0.###};";
        private const string PRESET_CONFIG_DESFASE_COMMAND = "P{0:0.###};";
        private const string PRESET_CONFIG_BOOSTER_COMMAND = "CONF2BOO{0}";

        private const string SET_TIMER_RESET = "T1R;";
        private const string SET_TRIP_RESET = "QRT;";
        private const string SET_MONITOR_RESET = "M0R;";
        private const string SET_TIMER_STOP = "T1STO{0};";
        private const string SET_TIMER_START = "T1S;T1STA{0};";
        private const string SET_TIMER_STOP_SOFTWARE = "T1SOSTO;";
        private const string SET_TIMER_START_SOFTWARE = "T1S;T1SOSTA;";
        private const string SET_SECONDARY_SOURCE = "C1,X{0},RA{1};";
        private const string SET_PRIMARY_SOURCE = "V1;";
        private const string SET_FUNCTION = "FU{0}L;";
        private const string SET_RAMP_FUNCTION = "FDEFI{0},D1{1},L1{2},ST{3};";

        private const string START_FUNCTION = "STA;";
        private const string APPLY_CONFIG_AND_START_TIMER = "ON,T1SOSTA;";
        private const string APPLY_CONFIG_COMMAND = "ON;";
        private const string APPLY_OFF_COMMAND = "{0};OF;";
        private const string APPLY_OFF_ALL_COMMAND = "V1;OF;C1;OF;";
        
        private const string READ_VOLTAGE_COMMAND = "QL;";
        private const string READ_CURRENT_COMMAND = "QL;";
        private const string READ_DESFASE_COMMAND = "QP;";
        private const string READ_STATUS_COMMAND = "QL;";
        private const string READ_TIME = "QT;";
        private const string READ_IDN_COMMAND = "QL;";
        private const string READ_MONITOR = "QM;";
        private string SET_SOURCE = "{0};";

        private double counterTime = 0;
        private string monitor = "";

        public PTE()
            : base(logger)
        {
            InitializeComponent();

            protocol = new ASCIIProtocol() { CaracterFinTx = "\r", CaracterFinRx = "\r" };
            transport = new MessageTransport(protocol, new SerialPortAdapter(sp), logger);

            SelecctionSource = SourceType.PTE_100V;
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public SourceType SelecctionSource { get; set; }

        public class Monitor
        {
            public enum EventTrigger
            {
                AlActivarse = 0,
                AlDesactivarse = 1,
                ActivarseDesactivarse = 2,
            }

            public enum EventType
            {
                TRIP = 0,
                WAIT = 1,
                START_TIMER = 2,
                STOP_TIMER = 3,
                USER = 4,
            }

            public int Number { get; set; }

            public int UserEvent { get; set; }

            public int AntiReboteTime { get; set; }

            public EventTrigger eventTrigger { get; set; }

            public EventType eventType { get; set; }

            public bool ActiveBeep { get; set; }

            public bool ActiveAutoOFF { get; set; }

            public override string ToString()
            {

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("M{0}", Number);

                if (eventType == EventType.START_TIMER)
                    sb.AppendFormat("STA,");
                else if (eventType == EventType.STOP_TIMER)
                    sb.AppendFormat("STO,");
                else if (eventType == EventType.TRIP)
                    sb.AppendFormat("T,");
                else if (eventType == EventType.USER)
                    sb.AppendFormat("US{0},", UserEvent);
                else if (eventType == EventType.WAIT)
                    sb.AppendFormat("W,");

                if (eventTrigger == EventTrigger.AlActivarse)
                    sb.AppendFormat("A,");
                else if (eventTrigger == EventTrigger.AlDesactivarse)
                    sb.AppendFormat("REM,");
                else if (eventTrigger == EventTrigger.ActivarseDesactivarse)
                    sb.AppendFormat("C,");

                sb.AppendFormat("H{0},", ActiveBeep == true ? "E" : "D");

                sb.AppendFormat("AU{0},", ActiveAutoOFF == true ? "E" : "D");

                sb.AppendFormat("D{0}", AntiReboteTime);

                sb.AppendFormat(";");

                return sb.ToString();
            }
        }

        public class Function
        {
            public enum Magnitud
            {
                VOLTAGE,
                CURRENT
            }

            public enum FunctionType
            {
                RAMP,
                PULSE,
                BYNARY,
                FAULT,
                FREQUENCY
            }
            
            public double StartLevel { get; set; }
            public double Increment { get; set; }
            public double Interval { get; set; }
            public double StopLevel { get; set; }

            public static Dictionary<FunctionType, string> RelationFunctionCommand = new Dictionary<FunctionType, string>
            {
                { FunctionType.RAMP, "R" },
                { FunctionType.PULSE, "P" },
                { FunctionType.BYNARY, "B" },
                { FunctionType.FAULT, "F" },
                { FunctionType.FREQUENCY, "FR" }
            };

            public static string GetCommandFunction(FunctionType type)
            {
                string value = string.Empty;
                RelationFunctionCommand.TryGetValue(type, out value);
                return value;
            }
        }        

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            //sets the read and write timeout to 3 seconds
            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.RtsEnable = true;
            sp.DtrEnable = true;
            sp.BaudRate = 9600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();
        }

        private double GetVoltageRange(double Voltage)
        {
            if (Voltage > 500)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE("PTE VOLTAGE FUERA RANGO > 500").Throw();

            if (Voltage <= 6.25)
                 return 6.25; 
            else if (Voltage < 300)
                 return 150;
            else
                return 300;
        }

        private double GetCurrentRange(double Current)
        {
            if (Current > 25)
                TestException.Create().HARDWARE.INSTRUMENTOS.CONSIGNAR_FUENTE("PTE CORRIENTE FUERA RANGO > 25A").Throw();

            if (Current <= 0.33)
                return 0.33;

            if (Current > 8)
                return 25;

            return 8;
        }

        public void InitConfig()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_INIT_CONFIG, true));

            SetRange();
        }

        public void ModoExtConfig()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MODO_EXT, true));
        }

        public void ApplyOffAllAndWaitStabilisation()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_APPLY_OFF_ALL_COMMAND, true));
        }

        public void ActiveBooster(bool Status)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_ACTIVE_BOOSTER, true, Status==true?"ON":"OF"));
        }

        //**********************************************************

        public string GetIdentification()
        {
            SendCommand(new Command<Commands>(Commands.READ_IDN, true));
            return idn;
        }

        public double ReadTime()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.READ_TIME, true));
            return counterTime;
        }

        public string ReadMonitor()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.READ_MONITOR, true));
            return monitor;
        }

        //*****************************************************

        public void ResetTimerTripMonitor()
        {
            ResetTimer();
            ResetTrip();
            ResetMonitor();
        }

        public void ResetTimer()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_TIMER_RESET, true));
        }

        public void ResetTrip()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_TRIP_RESET, true));
        }

        public void ResetMonitor()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MONITOR_RESET, true));
        }

        public void ApplyConfigMonitor(int number, Monitor.EventType TipoEvento, Monitor.EventTrigger TipoDisparo, bool activaBeep=true, bool activaAutoOFF=false, int antireboteTime=0)
        {
            Monitor  monitor = new Monitor
            {
                Number = number,            
                eventType=TipoEvento,
                eventTrigger=TipoDisparo,
                ActiveBeep=activaBeep,
                ActiveAutoOFF = activaAutoOFF,
                AntiReboteTime = antireboteTime,
            };
            ApplyConfigMonitor(monitor);
        }

        public void ApplyConfigMonitor(Monitor monitor)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_MONITOR_CONFIG, true, monitor));
            Thread.Sleep(100);
        }

        public void ActivaTimer(int NumTimer)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_TIMER_START, true, NumTimer));
            Thread.Sleep(100);
        }      

        public void DesActivaTimer(int NumTimer)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_TIMER_STOP, true, NumTimer));
            Thread.Sleep(100);
        }

        public void ActivaTimerSoftware()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_TIMER_START_SOFTWARE, true));
            Thread.Sleep(100);
        }

        public void DesActivaTimerSoftware()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_TIMER_STOP_SOFTWARE, true));
            Thread.Sleep(100);
        }

        public void ApplyCurrentAndStartTimer(double current)
        {
            this.Presets.Current = current;
            SendCommand( new Command<Commands>(Commands.SET_CURRENT, true, true));
            System.Threading.Thread.Sleep(500);
            SendCommand(new Command<PrivateCommands>(PrivateCommands.APPLY_CONFIG_AND_START_TIMER, true));  
        }

        public void SetSource(SourceType source)
        {
            SelecctionSource = source;    
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_SOURCE, true));
        }

        private void SetRange()
        {
            SendCommand(new Command<Commands>(Commands.SET_CURRENT_RANGE, true));
            System.Threading.Thread.Sleep(500);
        }

        public Tuple<double,FunctionResultEnum> StartFunctionAndWaitReturnLevel(int TimeOut)
        {
            var timeOutDefault = transport.ReadTimeout;
            transport.ReadTimeout = TimeOut;

            SendCommand(new Command<PrivateCommands>(PrivateCommands.START_FUNCTION, true));
            Thread.Sleep(100);

            transport.ReadTimeout = timeOutDefault;

            var level = ReadCurrent();

            return new Tuple<double, FunctionResultEnum> (level, FunctionResult);
        }

        public void ConfigFunction(Function.Magnitud magnitud, Function.FunctionType functionType, Function function)
        {
            if (magnitud == Function.Magnitud.CURRENT)
            {
                this.Presets.Current = function.StartLevel;
                SendCommand(new Command<Commands>(Commands.SET_CURRENT, true));
                Thread.Sleep(100);
            }
            else
            {
                this.Presets.Voltage = function.StartLevel;
                SendCommand(new Command<Commands>(Commands.SET_VOLTAGE, true));
                Thread.Sleep(100);
            }

            SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
            Thread.Sleep(100);

            SetFunction(functionType);

            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_FUNCTION_CONFIG, true, function.Increment, function.Interval, function.StartLevel, function.StopLevel));
            Thread.Sleep(100);
        }    

        private void SetFunction(Function.FunctionType functionType)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.SET_FUNCTION, true, Function.GetCommandFunction(functionType)));
            Thread.Sleep(100);
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            PTEMessage message, response;

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)Commands.SET_VOLTAGE:
                    message = PTEMessage.Create(PRESET_CONFIG_VOLTAGE_COMMAND, (int)SelecctionSource, GetVoltageRange(Presets.Voltage), (Presets.Voltage / 2));
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Voltage = Presets.Voltage;
                    Thread.Sleep(500);
                    SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
                    break;

                case (int)Commands.SET_CURRENT:
                    message = PTEMessage.Create(PRESET_CONFIG_CURRENT_COMMAND, GetCurrentRange(Presets.Current), Presets.Current);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    Thread.Sleep(500);
                    if (command.Args.Length == 0 || !(bool)command.Args[0])
                        SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
                    break;
                case (int)Commands.SET_CURRENT_RANGE:
                    message = PTEMessage.Create(PRESET_RANGE_CURRENT_COMMAND, GetCurrentRange(Presets.Current));
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
                    break;

                case (int)Commands.SET_FREQ:
                    message = PTEMessage.Create(PRESET_CONFIG_FREQUENCY_COMMAND, Presets.Freq);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Current = Presets.Current;
                    Thread.Sleep(500);
                    SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
                    break;

                case (int)Commands.SET_PF:
                    message = PTEMessage.Create(PRESET_CONFIG_DESFASE_COMMAND, Presets.PF);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.PF = Presets.PF;
                    Thread.Sleep(500);
                    SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
                    break;

                case (int)PrivateCommands.SET_SOURCE:
                    string args = SelecctionSource == SourceType.PTE_100V ? "V1" : "C1";
                    message = PTEMessage.Create(SET_SOURCE, args);
                    message.HasResponse = false;                    
                    transport.SendMessage<PTEMessage>(message);
                    Thread.Sleep(500);
                    break;

                case (int)Commands.APPLY_OFF:
                    message = new PTEMessage { Text = APPLY_OFF_COMMAND };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Voltage = 0;
                    DevicePreset.Current = 0;
                    break;

                case (int)PrivateCommands.SET_APPLY_OFF_ALL_COMMAND:
                    message = new PTEMessage { Text = APPLY_OFF_ALL_COMMAND };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Voltage = 0;
                    DevicePreset.Current = 0;
                    DevicePreset.Desfase = 0;
                    break;

                case (int)Commands.APPLY_CONFIG:
                    message = new PTEMessage { Text = APPLY_CONFIG_COMMAND };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

                case (int)PrivateCommands.APPLY_CONFIG_AND_START_TIMER:
                    message = new PTEMessage { Text = APPLY_CONFIG_AND_START_TIMER };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

                case (int)Commands.RESET:
                    message = new PTEMessage { Text = RESET_COMMAND };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    DevicePreset.Voltage = 0;
                    DevicePreset.Current = 0;
                    DevicePreset.Desfase = 0;
                    break;

                case (int)PrivateCommands.SET_INIT_CONFIG:
                    message = new PTEMessage { Text = SET_INIT_CONFIG };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_MODO_EXT:
                    message = new PTEMessage { Text = SET_MODO_EXT };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_ACTIVE_BOOSTER:
                    message = PTEMessage.Create(PRESET_CONFIG_BOOSTER_COMMAND, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_TIMER_RESET:
                    message = new PTEMessage { Text = SET_TIMER_RESET };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_MONITOR_RESET:
                    message = new PTEMessage { Text = SET_MONITOR_RESET };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_TRIP_RESET:
                    message = new PTEMessage { Text = SET_TRIP_RESET };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_MONITOR_CONFIG:
                    message = PTEMessage.Create(command.Args[0].ToString());
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;


               case (int)PrivateCommands.SET_TIMER_START:
                    message = PTEMessage.Create(SET_TIMER_START, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_TIMER_STOP:
                    message = PTEMessage.Create(SET_TIMER_STOP, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;


               case (int)PrivateCommands.SET_TIMER_START_SOFTWARE:
                    message = new PTEMessage { Text = SET_TIMER_START_SOFTWARE };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_TIMER_STOP_SOFTWARE:
                    message = new PTEMessage { Text = SET_TIMER_STOP_SOFTWARE };
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_FUNCTION:
                    message = PTEMessage.Create(SET_FUNCTION, command.Args[0]);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

                case (int)PrivateCommands.SET_SECONDARY_SOURCE:
                    message = PTEMessage.Create(SET_SECONDARY_SOURCE, command.Args[0], command.Args[1]);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    break;

               case (int)PrivateCommands.SET_FUNCTION_CONFIG:
                    message = PTEMessage.Create(SET_RAMP_FUNCTION, command.Args[0], command.Args[1], command.Args[2], command.Args[3]);
                    message.HasResponse = false;
                    transport.SendMessage<PTEMessage>(message);
                    FunctionResult = FunctionResultEnum.IN_PROCCES;
                    break;

                case (int)PrivateCommands.START_FUNCTION:
                    message = PTEMessage.Create(START_FUNCTION);
                    message.HasResponse = true;
                    message.Validator = (m) => { return (m.Text.Contains("(R01)") || m.Text.Contains("(R02)")); };                  
                    response = transport.SendMessage<PTEMessage>(message);
                    if (response.Text.Contains("(R01)"))
                        FunctionResult = FunctionResultEnum.TRIP;
                    if (response.Text.Contains("(R02)"))
                        FunctionResult = FunctionResultEnum.STEPLEVEL;
                    break;
                     
               case (int)Commands.READ_VOLTAGE:
               case (int)Commands.READ_CURRENT:
               case (int)Commands.READ_STATUS:

                    message = new PTEMessage { Text = READ_STATUS_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("LEVEL ="); };
                    response = transport.SendMessage<PTEMessage>(message);

                    string[] tokens = response.Text.Split('=');

                    bool status =false;

                    if (tokens[0].Contains("V 1"))
                    {
                        DeviceData.Voltage = Convert.ToDouble(tokens[1], enUS) * 2;
                        status = DeviceData.Voltage >= Presets.Voltage;
                    }
                    else
                    {
                        DeviceData.Current = Convert.ToDouble(tokens[1], enUS);
                        status = (DeviceData.Current) >= Presets.Current;
                    }

                    if ((int)command.Commands == (int)Commands.READ_STATUS)
                        status = true;

                    Status.IsBusy = false;
                    Status.HasError = !status;
                    Status.HasWarning = false;

                    if (Status.HasError)
                        throw new InstrumentException(1);                 
                    break;

               case (int)Commands.READ_POWERFACTOR:
               case (int)Commands.READ_DESFASE:
                    message = new PTEMessage { Text = READ_DESFASE_COMMAND };
                    message.Validator = (m) => { return m.Text.Contains("PHASE ="); };
                    response = transport.SendMessage<PTEMessage>(message);

                     string[] token = response.Text.Split('=');

                     DeviceData.Desfase = Convert.ToDouble(token[1], enUS);
         
                    break;

               case (int)PrivateCommands.READ_TIME:
                    message = new PTEMessage { Text = READ_TIME };
                    message.Validator = (m) => { return m.Text.Contains("TIME 1="); };
                    response = transport.SendMessage<PTEMessage>(message);

                    string[] time = response.Text.Split('=');

                    counterTime = Convert.ToDouble(time[1], enUS);

                    break;

                case (int)Commands.READ_IDN:
                    message = new PTEMessage { Text = READ_IDN_COMMAND };
                    response = transport.SendMessage<PTEMessage>(message);
                    idn = response.Text;

                    break;

                case (int)PrivateCommands.READ_MONITOR:
                    message = new PTEMessage { Text = READ_MONITOR };
                    response = transport.SendMessage<PTEMessage>(message);
                    monitor = response.Text;

                    break;

                default:
                    throw new NotSupportedException();
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }

        protected override bool ValidateEstabilization(double tolerance)
        {
            return true;
        }
    }
}
