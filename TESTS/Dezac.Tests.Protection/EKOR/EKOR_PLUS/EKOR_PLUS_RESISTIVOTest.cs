﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.0)]
    public class EKOR_PLUS_RESISTIVOTest : EKOR_PLUSTest
    {
        #region Constants

        internal const int IN_DETECT_UUT = 22;
        internal const int OUT_PISTON_FIJACION = 5;
        internal const int OUT_PISTON_DOT_TEST = 6;

        internal const int OUT_ACTIVA_INPUTS_PAR = 41;
        internal const int OUT_ACTIVA_INPUTS_IMPAR = 42;

        internal const int OUT_ACTIVA_OUT_VOLTAGE_IN = 22;
        internal const int OUT_ACTIVA_INPUTS_VOLTAGE_6 = 40;
        internal const int OUT_ACTIVA_INPUTS_VOLTAGE_7 = 0;
        internal const int OUT_ACTIVA_INPUTS_VOLTAGE_8 = 1;

        internal const int IN_OUT1_VOLTAGE = 16;
        internal const int IN_OUT2_VOLTAGE = 17;
        internal const int IN_OUT3_VOLTAGE = 9;

        internal const int OUT_ACTIVA_LECTURA_OUT_4 = 43;
        internal const int IN_OUT4_VOLTAGE = 15;

        internal const int IN_LED = 11;

        private const int PASATAPAS_L1 = 18;
        private const int PASATAPAS_L2 = 19;
        private const int PASATAPAS_L3 = 21;

        private const int OUT_SUPPLY_USB = 17;

        #endregion

        private EKOR_PLUS.EkorPlusFamilyCalibration model;
        public EKOR_PLUS.EkorPlusFamilyCalibration ModularModel
        {
            get
            {
                
                switch (Identificacion.MODELO)
                {
                    case "RESISTIVO":
                        model = EKOR_PLUS.EkorPlusFamilyCalibration.RESISTIVO;
                        break;
                    case "CAPACITIVO":
                        model = EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO;
                        break;
                    default:
                        Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("MODELO").Throw();
                        break;
                }
                
                return model;
            }
        }

        public override void TestInitialization()
        {
            EkorPlus.Cirbus.PerifericNumber = Comunicaciones.Periferico;
            EkorPlus.FactorTransformationCurrent = 300 * 1000; //unidad mA
            EkorPlus.FactorTransformationVoltage = 10000;
            EkorPlus.Cirbus.TimeOut = 4000;

            Tower.Active24VDC();

            if (ModularModel == EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO)
                Tower.ActiveVoltageCircuit(); 
        }

        public void TestConsumptionBattery(string portMTX)
        {
            Tower.Metrix3293.PortCom = Convert.ToByte(GetVariable<string>(portMTX, portMTX).Replace("COM", ""));
            Tower.Metrix3293.SetAutoRange();

            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(3, 0.1);

            Delay(1000, "Tiempo de espera de lectura del tester");

            var voltageJPvacio = new AdjustValueDef(Params.I.Null.EnVacio.Name, 0, Params.I.Null.EnVacio.Min(), Params.I.Null.EnVacio.Max(), 0, 0, ParamUnidad.A);

            TestMeasureBase(voltageJPvacio, (step) =>
            {
                return Tower.Metrix3293.ReadCurrent(MTX3293.measureTypes.DC);
            }, 1, 4, 500);
        }

        public void TestPositionDevice()
        {
            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[IN_DETECT_UUT])
                    return false;

                return true;

            }, "Error No se ha detectado el equipo o el equipo no lleva el ancla puesta", 5, 500, 1500, false, false);

            Tower.ActiveElectroValvule();

            Tower.IO.DO.OnWait(500, OUT_PISTON_FIJACION);

            Tower.IO.DO.OnWait(500, OUT_PISTON_DOT_TEST);
        }

        public override void TestComunicationsRS485(int porEkorttL5)
        {
            if (porEkorttL5 == 0)
                porEkorttL5 = Comunicaciones.SerialPort;

            EkorPlus.Cirbus.PortCom = porEkorttL5;
            EkorPlus.Cirbus.BaudRate = 9600;
            EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.Even;
            EkorPlus.Cirbus.PerifericNumber = 0;
            var retries = EkorPlus.Cirbus.Retries;
            EkorPlus.Cirbus.Retries = 1;

            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[IN_LED];
            }, string.Format("Error. no se detectado el LED activado"), 15, 100, 200);

            Resultado.Set("TEST_LED", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                if (p == 1)
                {
                    logger.InfoFormat("Comprobamos configuracion del equipo que esta siendo repetido (4800, n, 0)");
                    EkorPlus.Cirbus.BaudRate = 4800;
                    EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.None;
                    EkorPlus.Cirbus.PerifericNumber = 0;
                }

                EkorPlus.ReadDateTime();
                logger.InfoFormat("Puerto de comunicacion RS485 asignado en  COM {0}", porEkorttL5.ToString());
                return true;

            }, "Error. No comunica el equipo en RS485", 3, 500, 1000, false, false);

            EkorPlus.Cirbus.Retries = retries;

            Resultado.Set("TEST_RS485", "OK", ParamUnidad.s);
        }

        public void TestConsumoUSB()
        {
            var param = new AdjustValueDef(Params.I_DC.Null.TestPoint("USB"), ParamUnidad.A);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.IN3, MagnitudsMultimeter.VoltDC) / 1.3D;
            }, 1, 4, 2000);
        }

        public override void TestDefaultSetup()
        {
            EkorPlus.WriteDate(DateTime.Now);

            EkorPlus.WriteFrameNumber(TestInfo.NumBastidor.ToString());

            EkorPlus.WriteAngleCalibrationInitialization();

            EkorPlus.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_DEFAULT", "04b0 012c 003c 003c 003c 003c 00 24 0f 0f 00 00 0000 0001 0258 00 ff 00", ParamUnidad.SinUnidad));

            Delay(2000, "");
            EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_DEFAULT", "0102 270f 1A 00 00 00 1964 06 0006 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));

            Delay(2000, "");

            EkorPlus.Cirbus.BaudRate = 4800;
            EkorPlus.Cirbus.Parity = System.IO.Ports.Parity.None;
            EkorPlus.Cirbus.PerifericNumber = 0;

            EkorPlus.WritePasatapas(65536, 65536, 65536);  

            string developerVersion = "";
            string firmwareVersion = "";

            SamplerWithCancel((p) =>
            {
                developerVersion = EkorPlus.ReadVersion();
                logger.InfoFormat("Puerto de comunicacion TTL asignado en  COM {0}", EkorPlus.Cirbus.PortCom.ToString());
                return true;
            }, "Error. No comunica el equipo", 3, 500, 1000, false, false);

            Assert.AreEqual("VERSION_DEVELOPER", developerVersion.Trim(), Identificacion.VERSION_DEVELOPER.Trim(),
            Error().SOFTWARE.SECUENCIA_TEST.VERSION_INCORRECTA("Error en la programación del equipo, la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                firmwareVersion = EkorPlus.ReadFirmware();
                logger.InfoFormat("Puerto de comunicacion TTL asignado en  COM {0}", EkorPlus.Cirbus.PortCom.ToString());
                return true;
            }, "Error. No comunica el equipo", 3, 500, 1000, false, false);

            Assert.AreEqual("VERSION_FIRMWARE_COM_CIRBUS", firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(),
            Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version de firmware es incorrecta"), ParamUnidad.SinUnidad);     
        }

        public override void TestVUSB(string nameDevicePortVirtual)
        {           
            SamplerWithCancel((p) =>
            {
                if (p > 0 && p % 10 == 0)
                    MessageBox.Show("Desconecte el USB del Ekor, espere unos segundos y vuelva a conectarlo", "TEST USB", MessageBoxButtons.OK);

                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKORPLUS", ParamUnidad.SinUnidad).Trim())
                        return true;
                return false;
            }, "Error No se ha encontrado el dispositivo USB del EKOR", 25, 2500, 4000);

            Resultado.Set("TEST_USB_DRIVE", "OK", ParamUnidad.SinUnidad);

            if (Configuracion.GetString("TEST_USB_COMUNICATION", "NO", ParamUnidad.SinUnidad).Trim() == "SI")
            {
                var portEkor = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));

                var portSerial = EkorPlus.Cirbus.PortCom;
                EkorPlus.Cirbus.ClosePort();
                EkorPlus.Cirbus.PortCom = portEkor;

                SamplerWithCancel((p) =>
                {
                    if (p == 2)
                    {
                        Tower.Chroma.ApplyOff();
                        Tower.IO.DO.Off(OUT_SUPPLY_USB);
                        Delay(5000, "Desconectando USB");
                        Tower.Chroma.ApplyAndWaitStabilisation();
                        Tower.IO.DO.On(OUT_SUPPLY_USB);
                        Delay(10000, "Conectando USB");
                    }
                    EkorPlus.ReadFirmware();
                    logger.InfoFormat("Puerto de comunicacion USB virtual asignado en  COM {0}", portEkor.ToString());
                    return true;
                }, "Error. No comunica el equipo", 8, 500, 1000, false, false);

                EkorPlus.Cirbus.ClosePort();
                EkorPlus.Cirbus.PortCom = portSerial;

                Resultado.Set("TEST_USB_SERIALPORT", "OK", ParamUnidad.SinUnidad);
            }
        }

        //******************************************************************************

        public override void TestInputs()
        {
            var inputsValue = new List<KeyValuePair<byte, EKOR_PLUS.enumInputs>>();

            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_ACTIVA_INPUTS_IMPAR, EKOR_PLUS.enumInputs.IN1_3_5));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_ACTIVA_INPUTS_PAR, EKOR_PLUS.enumInputs.IN2_4));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_ACTIVA_INPUTS_VOLTAGE_6, EKOR_PLUS.enumInputs.IN6));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_ACTIVA_INPUTS_VOLTAGE_7, EKOR_PLUS.enumInputs.IN7));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_ACTIVA_INPUTS_VOLTAGE_8, EKOR_PLUS.enumInputs.IN8));

            foreach (KeyValuePair<byte, EKOR_PLUS.enumInputs> input in inputsValue)
            {
                this.Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                if (input.Value != EKOR_PLUS.enumInputs.IN1_3_5 && input.Value != EKOR_PLUS.enumInputs.IN2_4)
                    Tower.IO.DO.On(OUT_ACTIVA_OUT_VOLTAGE_IN);

                Tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    var value = (EKOR_PLUS.enumInputs)EkorPlus.ReadInputs();

                    if (input.Value != (EKOR_PLUS.enumInputs)value)
                    {
                        var result = (EKOR_PLUS.enumInputs)((input.Value ^ value) ^ EKOR_PLUS.enumInputs.NOTHING);
                        throw new Exception(string.Format("Error. no se detecta la entrada {0} activada", result.ToString()));
                    }

                    return true;

                }, "", 3, 500, 200, false, false);

                Tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    var value = (EKOR_PLUS.enumInputs)EkorPlus.ReadInputs();

                    if (EKOR_PLUS.enumInputs.NOTHING != value)
                    {
                        var result = (EKOR_PLUS.enumInputs)(value ^ EKOR_PLUS.enumInputs.NOTHING);
                        throw new Exception(string.Format("Error. no se detecta la entrada {0} desactivada", result.ToString()));
                    }

                    return true;

                }, "", 3, 500, 200, false, false);

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);

                if (input.Value != EKOR_PLUS.enumInputs.IN1_3_5 && input.Value != EKOR_PLUS.enumInputs.IN2_4)
                    Tower.IO.DO.Off(OUT_ACTIVA_OUT_VOLTAGE_IN);
            }
        }

        public override void TestOutputs()
        {
            var outputValue = new List<KeyValuePair<byte, EKOR_PLUS.enumOutputs>>();
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_OUT1_VOLTAGE, EKOR_PLUS.enumOutputs.OUT1));
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_OUT2_VOLTAGE, EKOR_PLUS.enumOutputs.OUT2));
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_OUT3_VOLTAGE, EKOR_PLUS.enumOutputs.OUT3));
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_OUT4_VOLTAGE, EKOR_PLUS.enumOutputs.OUT4));

            foreach (KeyValuePair<byte, EKOR_PLUS.enumOutputs> output in outputValue)
            {
                this.Logger.InfoFormat("{0}", output.Value.ToString());

                if (output.Value == EKOR_PLUS.enumOutputs.OUT4)
                    Tower.IO.DO.On(OUT_ACTIVA_LECTURA_OUT_4);

                EkorPlus.WriteOutput(output.Value);

                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[output.Key];
                }, string.Format("Error. no se detectado la salida {0} activada", output.Value.ToString()), 10, 100, 200);

                EkorPlus.WriteOutput(EKOR_PLUS.enumOutputs.NOTHING);

                SamplerWithCancel((p) =>
                {
                    return !Tower.IO.DI[output.Key];
                }, string.Format("Error. se ha detectado la salida {0} desactivada", output.Value.ToString()), 10, 100, 200);

                if (output.Value == EKOR_PLUS.enumOutputs.OUT4)
                    Tower.IO.DO.Off(OUT_ACTIVA_LECTURA_OUT_4);

                Resultado.Set(string.Format("{0}", output.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public override void TestTrigger()
        {
            SamplerWithCancel((x) =>
            {
                Logger.InfoFormat(string.Format("Reintento : {0}", x));

                Tower.IO.DO.On(OUT_SUPPLY_USB);
                SamplerWithCancel((p) =>
                {
                    if (p == 4)
                    {
                        Tower.IO.DO.PulseOff(2000, OUT_SUPPLY_USB);
                        Delay(10000, "Espera detección unidad lógica");
                    }
                    SendConfigUserXML();
                    return true;
                }, string.Empty, 6, 15000, 10000, false, false);
                Tower.IO.DO.Off(OUT_SUPPLY_USB);

                var currentTrigger = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 2, ParamUnidad.A);
              
                Tower.ActiveCurrentCircuit(true, true, true);

                this.InParellel(
                () =>
                {
                    Tower.MUX.Reset();
                    Tower.MUX.Connect2((int)InputMuxEnum.IN9, Instruments.IO.Outputs.FreCH1, (int)InputMuxEnum.IN4, Instruments.IO.Outputs.FreCH2);

                    Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1);

                    Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 0.5);
                },
                () =>
                {
                    Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(currentTrigger, 0, 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
                });

                var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

                var adjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    try
                    {
                        var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 10000, HP53131A.Channels.CH1, () =>
                        {        
                           Delay(500, "");
                          Tower.IO.DO.Off(28);
                        });

                        return (timeResult.Value);
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Format("Error, el equipo no ha disparado"));
                    }
                }, 0, 1, 1000);

                Tower.ActiveCurrentCircuit(true, true, true);

                var inyectedCurrent = new AdjustValueDef(Params.I.Null.TRIGGER.Name, 0, 0, 0, Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 2, ParamUnidad.A), Params.I.Null.TRIGGER.Tol(), ParamUnidad.A);
                TestMeasureBase(inyectedCurrent, (s) =>
                {
                    var currrentTower = Tower.PowerSourceIII.ReadCurrent().L1;
                    return currrentTower;
                }, 0, 2, 500);

                Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();           

                SamplerWithCancel((p) =>
                {
                    EkorPlus.ReadValidateTrigger();
                    return true;
                }, "Error, no se ha podido validar el disparo");


                var currentTriggerVerif = new AdjustValueDef(Params.I.Other("TRIGGER").Verificacion.Name, 0, 0, 0, 0, Params.I.Null.TRIGGER.Tol(), ParamUnidad.s);

                TestCalibracionBase(currentTriggerVerif,
                   () =>
                   {
                       var historicTrigger = EkorPlus.ReadInfoTriggers().currentTriggerHomopolar;
                       return historicTrigger; ;

                   },
                   () =>
                   {
                       return inyectedCurrent.Value;
                   }, 1, 1, 500, "Error en la verificación de la corriente de disparo");


                var timeTriggerVerif = new AdjustValueDef(Params.TIME.Other("TRIGGER").Verificacion.Name, 0, 0, 0, 0, 5, ParamUnidad.s);

                TestCalibracionBase(timeTriggerVerif,
                   () =>
                   {
                       return EkorPlus.ReadInfoTriggers().timeTriggerHomopolar;
                   },
                   () =>
                   {
                       return adjustResult.Value;
                   }, 1, 1, 500, "Error en la verifiación del tiempo de disparo");                      

                return true;
            }, string.Empty, 2, 1000, 0, false, false);
        }

        //******************************************************************************

        public override void TestCalibrationVoltageCurrents([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            if (ModularModel == EKOR_PLUS.EkorPlusFamilyCalibration.RESISTIVO)
                TestCalibrationVoltageCurrentsResistivo(scale, consigna_A);
            else if (ModularModel == EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO)
                TestCalibrationVoltageCurrentsCapacitivo(scale, consigna_A);
            else
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("PARAM MODELO INCORRECTO, DEBE SER RESISTIVO O CAPACITIVO").Throw();
        }

        public override void TestCalibrationCurrentHomopolar([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            if (ModularModel == EKOR_PLUS.EkorPlusFamilyCalibration.RESISTIVO)
                TestCalibrationCurrentHomopolarResistivo(scale, consigna_A);
            else if (ModularModel == EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO)
                base.TestCalibrationCurrentHomopolar(scale, consigna_A);
            else
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE("PARAM MODELO INCORRECTO, DEBE SER RESISTIVO O CAPACITIVO").Throw();

            
        }

        public void TestCalibrationVoltageCurrentsResistivo([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {            
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();
            
            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc).Name, consigna_A, ParamUnidad.A);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            var model = EKOR_PLUS.EkorPlusFamilyCalibration.RESISTIVO;
            var mesuraHP = 0.000;

            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, current, frecuency, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.TestPoint(scaleDesc).Name, Params.GAIN_I.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_I.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false));

            if (scale == EKOR_PLUS.Scale.Gruesa)
                defs.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.TestPoint(scaleDesc).Name, Params.GAIN_V.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_V.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false));

            TestMeasureBase(defs,
            (step) =>
            {
                var rmsMeasure = EkorPlus.CalculateFactorCalibrationByScala(model, scale, current, voltage, false, 1, numMuestras, numMuestrasTotal, 1100, mesuraHP);
                return rmsMeasure.ToArray();
            }, 0, 2, 1500);

            EkorPlus.WriteCurrentFactorCalibration();

            if (scale == EKOR_PLUS.Scale.Gruesa)
                EkorPlus.WriteVoltageFactorCalibration();

            Delay(3000, "Espera grbacion factores");

            var factorCurrent = EkorPlus.ReadCurrentFactorCalibration();
            Assert.AreEqual(factorCurrent.TramaEFC, EkorPlus.FactoresCalibracionCorriente.TramaEFC, Error().UUT.CONSUMO.NO_CORRESPONDE_CON_ENVIO("Error los factores de corriente grabados no corresponde a los leidos"));

            var factorVoltage = EkorPlus.ReadVoltageFactorCalibration();
            Assert.AreEqual(factorVoltage.TramaEFV, EkorPlus.FactoresCalibracionVoltage.TramaEFV, Error().UUT.CONSUMO.NO_CORRESPONDE_CON_ENVIO("Error los factores de voltage grabados no corresponde a los leidos"));

            //******************************************************
            //******************************************************

            var defsVerif = new List<TriAdjustValueDef>();
            defsVerif.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, current, Params.I.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.A, false, false));
            defsVerif.Add(new TriAdjustValueDef(Params.ANGLE_GAP.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, gap, Params.ANGLE_GAP.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.Grados, false, false));

            if (scale == EKOR_PLUS.Scale.Gruesa)
                defsVerif.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, voltage, Params.V.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.V, false, false));

            TestCalibracionBase(defsVerif,
            () =>
            {
                var vars = EkorPlus.ReadMeasurementsCurrentDisplay();
                logger.DebugFormat("LPU Current L1: {0} L2: {1} L3: {2} LH: {3}", vars.L1, vars.L2, vars.L3, vars.LH);

                var varsGap = EkorPlus.ReadMeasurementsAnglePhase();
                logger.DebugFormat("GFx Current L1: {0} L2: {1} L3: {2} ", varsGap.L1.Current, varsGap.L2.Current, varsGap.L3.Current);
                logger.DebugFormat("GFx Voltage L1: {0} L2: {1} L3: {2} ", varsGap.L1.Voltage, varsGap.L2.Voltage, varsGap.L3.Voltage);
                logger.DebugFormat("GFx Angulo L1: {0} L2: {1} L3: {2} ", varsGap.L1.Angle, varsGap.L2.Angle, varsGap.L3.Angle);

                var result = new List<double>() { vars.L1, vars.L2, vars.L3, varsGap.L1.Angle, varsGap.L2.Angle, varsGap.L3.Angle };

                if (scale == EKOR_PLUS.Scale.Gruesa)
                {
                    var vars2 = EkorPlus.ReadMeasurementsVolageDisplay();
                    logger.DebugFormat("LEV Voltage L1: {0} L2: {1} L3: {2} LH: {3}", vars2.L1, vars2.L2, vars2.L3, vars2.LH);

                    result.Add(vars2.L1);
                    result.Add(vars2.L2);
                    result.Add(vars2.L3);
                }

                return result.ToArray();
            },
            () =>
            {
                var result = new List<double>() { current, current, current, gap, gap, gap };
                if (scale == EKOR_PLUS.Scale.Gruesa)
                {
                    result.Add(voltage);
                    result.Add(voltage);
                    result.Add(voltage);
                }

                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);
        }

        public void TestCalibrationCurrentHomopolarResistivo([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc + "_HOMOPOLAR").Name, consigna_A, ParamUnidad.A);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_HOMOPOLAR", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            var model = EKOR_PLUS.EkorPlusFamilyCalibration.RESISTIVO;
            var mesuraHP = 0.000;

            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(voltage, new TriLineValue { L1 = current, L2 = 0, L3 = 0 }, frecuency, gap, 0, true);

            var defs = new AdjustValueDef(Params.GAIN_I.Other("LH").Ajuste.TestPoint(scaleDesc).Name, 0, Params.GAIN_I.Other("LH").Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_I.Other("LH").Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos);

            TestMeasureBase(defs,
            (step) =>
            {
                var rmsMeasure = EkorPlus.CalculateFactorCalibrationByScala(model, scale, current, voltage, true, 1, numMuestras, numMuestrasTotal, 1100, mesuraHP);
                return rmsMeasure.FirstOrDefault(); ;
            }, 0, 2, 1500);

            EkorPlus.WriteCurrentFactorCalibration();

            Delay(3000, "Espera grbacion factores");

            var defsVerif = new List<AdjustValueDef>();
            defsVerif.Add(new AdjustValueDef(Params.I.Other("LH").Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, current, Params.I.Other("LH").Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.A));
            defsVerif.Add(new AdjustValueDef(Params.ANGLE_GAP.Other("LH").Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, gap, Params.ANGLE_GAP.Other("LH").Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.Grados));

           TestCalibracionBase(defsVerif,
           () =>
           {
               var vars = EkorPlus.ReadMeasurementsCurrentDisplay();
               logger.DebugFormat("LPU Current L1: {0} L2: {1} L3: {2} LH: {3}", vars.L1, vars.L2, vars.L3, vars.LH);

               var varsGap = EkorPlus.ReadMeasurementsAnglePhase();
               logger.DebugFormat("GFx Current L1: {0} L2: {1} L3: {2} ", varsGap.L1.Current, varsGap.L2.Current, varsGap.L3.Current);
               logger.DebugFormat("GFx Voltage L1: {0} L2: {1} L3: {2} ", varsGap.L1.Voltage, varsGap.L2.Voltage, varsGap.L3.Voltage);
               logger.DebugFormat("GFx Angulo L1: {0} L2: {1} L3: {2} ", varsGap.L1.Angle, varsGap.L2.Angle, varsGap.L3.Angle);

               var varsGapHomo = EkorPlus.ReadMeasurementsAnglePhaseHomopolar();
               logger.DebugFormat("GFI Angulo LH: {0}", varsGapHomo.Angle);

               var result = new List<double>() { vars.LH, varsGapHomo.Angle };
               return result.ToArray();
           },
           () =>
           {
               var result = new List<double>() { current, gap };
               return result.ToArray();
           }, numMuestras, numMuestrasTotal, 1100);

        }

        public void TestCalibrationVoltageCurrentsCapacitivo([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            Tower.Metrix3293.PortCom = Convert.ToByte(GetVariable<string>("portMetrix").Replace("COM", ""));
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc).Name, consigna_A, ParamUnidad.A);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            var model = EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO;    // CAMBIO MARC: Dos parametros necesarios para llamar a la función EkorPlus.CalculateFactorCalibrationByScala (mesuraHP no se usa en el resistivo)
            var mesuraHP = TriLineValue.Create(0.000);

            Tower.FLUKE.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(voltage), TriLineValue.Create(current), frecuency, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.TestPoint(scaleDesc).Name, Params.GAIN_I.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_I.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false));

            if (scale == EKOR_PLUS.Scale.Gruesa)
            {
                defs.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.TestPoint(scaleDesc).Name, Params.GAIN_V.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_V.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false));

                foreach (var line in new string[] { "L1", "L2", "L3" })
                {
                    if (line == "L1")
                        Tower.IO.DO.On(PASATAPAS_L1);
                    if (line == "L2")
                        Tower.IO.DO.On(PASATAPAS_L2);
                    if (line == "L3")
                        Tower.IO.DO.On(PASATAPAS_L3);

                    Delay(5000, "Esperando estabilización medida pasatapas");

                    var valueTester = new AdjustValueDef(Params.V.Other(line).TESTER.Name, 0, Params.V.Null.TESTER.Min(), Params.V.Null.TESTER.Max(), 0, 0, ParamUnidad.V);
                    var measure = TestMeasureBase(valueTester, (step) =>
                   {
                       var testerMeas = Tower.Metrix3293.ReadVoltage(MTX3293.measureTypes.AC);
                       logger.InfoFormat("Valor Tester Metrix:", testerMeas);
                       return testerMeas;
                   }, 1, 4, 500, true);

                    if (line == "L1")
                    {
                        mesuraHP.L1 = measure.Value * 10D;
                        Tower.IO.DO.OffWait(2000, PASATAPAS_L1);
                    }
                    if (line == "L2")
                    {
                        mesuraHP.L2 = measure.Value * 10D;
                        Tower.IO.DO.OffWait(2000, PASATAPAS_L2);
                    }
                    if (line == "L3")
                    {
                        mesuraHP.L3 = measure.Value * 10D;
                        Tower.IO.DO.OffWait(2000, PASATAPAS_L3);
                    }
                }
            }

            TestMeasureBase(defs,
            (step) =>
            {
                var rmsMeasure = EkorPlus.CalculateFactorCalibrationByScala(model, scale, current, voltage, false, 1, numMuestras, numMuestrasTotal, 1100, mesuraHP);
                return rmsMeasure.ToArray();
            }, 0, 2, 1500);

            EkorPlus.WriteCurrentFactorCalibration();

            if (scale == EKOR_PLUS.Scale.Gruesa)
                EkorPlus.WriteVoltageFactorCalibration();

            Delay(3000, "Espera grbacion factores");

            var factorCurrent = EkorPlus.ReadCurrentFactorCalibration();
            Assert.AreEqual(factorCurrent.TramaEFC, EkorPlus.FactoresCalibracionCorriente.TramaEFC, Error().UUT.CONSUMO.NO_CORRESPONDE_CON_ENVIO("Error los factores de corriente grabados no corresponde a los leidos"));

            var factorVoltage = EkorPlus.ReadVoltageFactorCalibration();
            Assert.AreEqual(factorVoltage.TramaEFV, EkorPlus.FactoresCalibracionVoltage.TramaEFV, Error().UUT.CONSUMO.NO_CORRESPONDE_CON_ENVIO("Error los factores de voltage grabados no corresponde a los leidos"));

            //******************************************************
            //******************************************************

            var defsVerif = new List<TriAdjustValueDef>();
            defsVerif.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, current, Params.I.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.A, false, false));
            defsVerif.Add(new TriAdjustValueDef(Params.ANGLE_GAP.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, gap, Params.ANGLE_GAP.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.Grados, false, false));

            if (scale == EKOR_PLUS.Scale.Gruesa)
                defsVerif.Add(new TriAdjustValueDef(Params.V.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, voltage, Params.V.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.V, false, false));

            TestCalibracionBase(defsVerif,
            () =>
            {
                var vars = EkorPlus.ReadMeasurementsCurrentDisplay();
                logger.DebugFormat("LPU Current L1: {0} L2: {1} L3: {2} LH: {3}", vars.L1, vars.L2, vars.L3, vars.LH);

                var varsGap = EkorPlus.ReadMeasurementsAnglePhase();
                logger.DebugFormat("GFx Current L1: {0} L2: {1} L3: {2} ", varsGap.L1.Current, varsGap.L2.Current, varsGap.L3.Current);
                logger.DebugFormat("GFx Voltage L1: {0} L2: {1} L3: {2} ", varsGap.L1.Voltage, varsGap.L2.Voltage, varsGap.L3.Voltage);
                logger.DebugFormat("GFx Angulo L1: {0} L2: {1} L3: {2} ", varsGap.L1.Angle, varsGap.L2.Angle, varsGap.L3.Angle);

                var result = new List<double>() { vars.L1, vars.L2, vars.L3, varsGap.L1.Angle, varsGap.L2.Angle, varsGap.L3.Angle };

                if (scale == EKOR_PLUS.Scale.Gruesa)
                {
                    var vars2 = EkorPlus.ReadMeasurementsVoltageDisplayInVolts();
                    logger.DebugFormat("LEV Voltage L1: {0} L2: {1} L3: {2}", vars2.L1, vars2.L2, vars2.L3);

                    result.Add(vars2.L1);
                    result.Add(vars2.L2);
                    result.Add(vars2.L3);
                }

                return result.ToArray();
            },
            () =>
            {
                var result = new List<double>() { current, current, current, gap, gap, gap };
                if (scale == EKOR_PLUS.Scale.Gruesa)
                {
                    result.Add(mesuraHP.L1);
                    result.Add(mesuraHP.L2);
                    result.Add(mesuraHP.L3);
                }

                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);
        }

        //******************************************************************************

        public override void TestCostumize()
        {
            var factoresProcome = EkorPlus.ReadFactoresProcome();
            var tramaFactoresProcome = "25000 00003 12500 00003 25000 00001 12500 00001 30000 00001";
            if (factoresProcome.TramaLFP != tramaFactoresProcome)
                EkorPlus.WriteFactoresProcome(tramaFactoresProcome);

            EkorPlus.WriteSerialNumber(TestInfo.NumSerie);

            Delay(2000, "");

            Tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(10000, "Espera Reset");

            Tower.LAMBDA.ApplyAndWaitStabilisation();

            var serialNumber = EkorPlus.ReadSerialNumber();
            Assert.AreEqual(TestInfo.NumSerie, serialNumber, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error el numero de serie leido no corresponde con el grabado"));

            TestDesviationClock();
            
            EkorPlus.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_FAB","04b0 012c 003c 003c 003c 003c 00 24 0f 0f 00 00 0000 0001 0258 00 ff 00", ParamUnidad.SinUnidad));

            Delay(1000, "");

            EkorPlus.WriteInstallerSettings("0102 270f 19 00 00 00 1964 06 0006 0096 0003 ffa6 00 00 00");

            Delay(1000, "");

            factoresProcome = EkorPlus.ReadFactoresProcome();
            tramaFactoresProcome = "25000 00003 12500 00003 25000 00001 12500 00001 30000 00001";
            if (factoresProcome.TramaLFP != tramaFactoresProcome)
                EkorPlus.WriteFactoresProcome(tramaFactoresProcome);

            factoresProcome = EkorPlus.ReadFactoresProcome();
            Assert.AreEqual("TRAMA_FACTORES_PROCOME", factoresProcome.TramaLFP, tramaFactoresProcome, Error().UUT.CONFIGURACION.NO_GRABADO("Error los factores Procome no estan grabados correctamente"), ParamUnidad.SinUnidad);

            Delay(1000, "");

            EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_FAB","0843 270f 19 00 00 00 1964 06 0006 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));

            Delay(2000, "");
        }

        public override void TestFinish()
        {           
            if (Tower != null)
            {
                Tower.ShutdownSources();
                Tower.IO.DO.Off(OUT_PISTON_DOT_TEST, OUT_PISTON_FIJACION, OUT_SUPPLY_USB); //17
                Tower.IO.DO.Off(14); //20
                Tower.Dispose();
            }

            if (EkorPlus != null)
                EkorPlus.Dispose();
        }
    }

}