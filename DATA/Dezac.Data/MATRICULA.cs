namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.MATRICULA")]
    public partial class MATRICULA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MATRICULA()
        {
            ASISTENCIATECNICA = new HashSet<ASISTENCIATECNICA>();
            ESTRUCTURAMATRICULA = new HashSet<ESTRUCTURAMATRICULA>();
            ORDENPRODUCTO = new HashSet<ORDENPRODUCTO>();
            REPROCESOFAB = new HashSet<REPROCESOFAB>();
            T_LOG = new HashSet<T_LOG>();
            T_TEST = new HashSet<T_TEST>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMMATRICULA { get; set; }

        public DateTime FECHAALTA { get; set; }

        [Required]
        [StringLength(30)]
        public string USUARIO { get; set; }

        public DateTime? FECHAIMPRESION { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASISTENCIATECNICA> ASISTENCIATECNICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ESTRUCTURAMATRICULA> ESTRUCTURAMATRICULA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDENPRODUCTO> ORDENPRODUCTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPROCESOFAB> REPROCESOFAB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_LOG> T_LOG { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_TEST> T_TEST { get; set; }
    }
}
