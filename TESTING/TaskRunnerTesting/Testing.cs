﻿using System;
using System.Windows.Forms;
using TaskRunner;
using TaskRunner.Model;

namespace TaskRunnerTesting
{
    public partial class Testing : Form
    {
        public Testing()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PlayExecuteTestParalel();
        }

        public void PlayExecuteTestParalel()
        {
            var sequence = SequenceModel.LoadFile(@"C:\SAURON_SYSTEM\SEQUENCES\Pruebas\TestParalelismo.seq");
            var runner = new SequenceRunner();
            runner.Play((numInstance, sharedVariables) =>
            {
                var context = new SequenceContext(sequence, numInstance);
                context.RunMode = TaskRunner.Enumerators.RunMode.Debug;
                return context;
            },2);

            while (runner.RunningState != TaskRunner.Enumerators.RunningState.Completed)
            {
                Application.DoEvents();
            }

            var result = runner.RunnerInfo;
            //var time = Convert.ToDouble(result.Variables["DiffTime"]);
            //Assert.True(time < 5);
        }
    }
}
