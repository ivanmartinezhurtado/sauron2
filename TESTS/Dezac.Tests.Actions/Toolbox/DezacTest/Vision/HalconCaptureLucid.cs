﻿using Dezac.Tests.Extensions;
using Instruments.Cameras.DTO;
using System;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(HalconCaptureLucidDescription))]
    public class HalconCaptureLucid : ActionBase
    {
        private CameraLucidSettings cameraSettings = new CameraLucidSettings();

        [Category("3. Configuration")]
        [Description("Nombre del test de visión artificial")]
        public ActionVariable TestName { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Nombre del equipo de test")]
        public ActionVariable DeviceName { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Número de capturas (reintentos)")]
        public ActionVariable NumberOfCaptures { get; set; } = new ActionVariable();

        [Category("3. Configuration")]
        [Description("Función para ser ejecutada antes de cada captura")]
        public RunMethodAction PreCaptureFunction { get; set; }

        [Category("3. Configuration")]
        [Description("Tipo del procedimiento de visión artificial que el action debe transformar")]
        [DefaultValue(TestHalconExtension.TypeProcedure.FindLeds)]
        public TestHalconExtension.TypeProcedure ProcedureType { get; set; }

        [Category("3.1 Camera")]
        [Description("Nombre de la cámara (InstanceContext)")]
        public ActionVariable CameraName { get; set; } = new ActionVariable();

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                throw new Exception("No se ha instanciado el método de inicialización del TestBase");

            testBase.TestHalconCaptureLucid(
                ProcedureType, 
                CameraName.Value.ToString(),
                DeviceName.Value.ToString(),
                TestName.Value.ToString(),
                Convert.ToInt32(NumberOfCaptures.Value.ToString().Replace(".", ",")),
                (s) => PreCaptureFunction?.Execute());
        }
    }

    public class HalconCaptureLucidDescription : ActionDescription
    {
        public override string Name { get { return "HalconCaptureLucid"; } }
        public override string Category { get { return "DezacTest/Vision/Halcon"; } }
        public override string Description { get { return "Este action sirve para validar variables en la fase de impresión"; } }
        public override Image Icon { get { return Properties.Resources.mvtec_halcon; } }
        public override string Dependencies { get { return "InitTestBase"; } }

    }
}
