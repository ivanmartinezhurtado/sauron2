﻿namespace WinTestPlayer.Views
{
    partial class LoginView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.rbLaser = new Telerik.WinControls.UI.RadRadioButton();
            this.printLabel = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.rdTIC = new Telerik.WinControls.UI.RadRadioButton();
            this.rbAutotest = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.rbSAT = new Telerik.WinControls.UI.RadRadioButton();
            this.rbPostVenta = new Telerik.WinControls.UI.RadRadioButton();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtPassword = new Telerik.WinControls.UI.RadTextBox();
            this.txtUserName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbLaser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdTIC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbAutotest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPostVenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.AliceBlue;
            this.radPanel1.Controls.Add(this.rbLaser);
            this.radPanel1.Controls.Add(this.printLabel);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.rdTIC);
            this.radPanel1.Controls.Add(this.rbAutotest);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.rbSAT);
            this.radPanel1.Controls.Add(this.rbPostVenta);
            this.radPanel1.Controls.Add(this.lblError);
            this.radPanel1.Controls.Add(this.pictureBox1);
            this.radPanel1.Controls.Add(this.txtPassword);
            this.radPanel1.Controls.Add(this.txtUserName);
            this.radPanel1.Controls.Add(this.radLabel13);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.radPanel1.Name = "radPanel1";
            // 
            // 
            // 
            this.radPanel1.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 250, 125);
            this.radPanel1.Size = new System.Drawing.Size(557, 425);
            this.radPanel1.TabIndex = 6;
            // 
            // rbLaser
            // 
            this.rbLaser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.rbLaser.Location = new System.Drawing.Point(367, 318);
            this.rbLaser.Margin = new System.Windows.Forms.Padding(4);
            this.rbLaser.Name = "rbLaser";
            this.rbLaser.Size = new System.Drawing.Size(64, 22);
            this.rbLaser.TabIndex = 5;
            this.rbLaser.TabStop = false;
            this.rbLaser.Text = "Laser";
            // 
            // printLabel
            // 
            this.printLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.printLabel.Location = new System.Drawing.Point(367, 229);
            this.printLabel.Margin = new System.Windows.Forms.Padding(4);
            this.printLabel.Name = "printLabel";
            this.printLabel.Size = new System.Drawing.Size(102, 22);
            this.printLabel.TabIndex = 23;
            this.printLabel.TabStop = false;
            this.printLabel.Text = "Print Label";
            // 
            // radLabel2
            // 
            this.radLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel2.Location = new System.Drawing.Point(301, 229);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            // 
            // 
            // 
            this.radLabel2.RootElement.ControlBounds = new System.Drawing.Rectangle(301, 229, 125, 22);
            this.radLabel2.Size = new System.Drawing.Size(54, 22);
            this.radLabel2.TabIndex = 21;
            this.radLabel2.Text = "Tools:";
            // 
            // rdTIC
            // 
            this.rdTIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.rdTIC.Location = new System.Drawing.Point(117, 273);
            this.rdTIC.Margin = new System.Windows.Forms.Padding(4);
            this.rdTIC.Name = "rdTIC";
            this.rdTIC.Size = new System.Drawing.Size(49, 22);
            this.rdTIC.TabIndex = 22;
            this.rdTIC.TabStop = false;
            this.rdTIC.Text = "TIC";
            // 
            // rbAutotest
            // 
            this.rbAutotest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.rbAutotest.Location = new System.Drawing.Point(367, 273);
            this.rbAutotest.Margin = new System.Windows.Forms.Padding(4);
            this.rbAutotest.Name = "rbAutotest";
            this.rbAutotest.Size = new System.Drawing.Size(93, 22);
            this.rbAutotest.TabIndex = 21;
            this.rbAutotest.TabStop = false;
            this.rbAutotest.Text = "Auto Test";
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel1.Location = new System.Drawing.Point(51, 229);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            // 
            // 
            // 
            this.radLabel1.RootElement.ControlBounds = new System.Drawing.Rectangle(51, 229, 125, 22);
            this.radLabel1.Size = new System.Drawing.Size(54, 22);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "Modo:";
            // 
            // rbSAT
            // 
            this.rbSAT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbSAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.rbSAT.Location = new System.Drawing.Point(117, 229);
            this.rbSAT.Margin = new System.Windows.Forms.Padding(4);
            this.rbSAT.Name = "rbSAT";
            this.rbSAT.Size = new System.Drawing.Size(54, 22);
            this.rbSAT.TabIndex = 3;
            this.rbSAT.Text = "SAT";
            this.rbSAT.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // rbPostVenta
            // 
            this.rbPostVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.rbPostVenta.Location = new System.Drawing.Point(117, 318);
            this.rbPostVenta.Margin = new System.Windows.Forms.Padding(4);
            this.rbPostVenta.Name = "rbPostVenta";
            this.rbPostVenta.Size = new System.Drawing.Size(123, 22);
            this.rbPostVenta.TabIndex = 4;
            this.rbPostVenta.TabStop = false;
            this.rbPostVenta.Text = "POSTVENTA";
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(0, 399);
            this.lblError.Margin = new System.Windows.Forms.Padding(4);
            this.lblError.Name = "lblError";
            // 
            // 
            // 
            this.lblError.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 399, 125, 22);
            this.lblError.Size = new System.Drawing.Size(557, 26);
            this.lblError.TabIndex = 19;
            this.lblError.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WinTestPlayer.Properties.Resources.dezac;
            this.pictureBox1.Location = new System.Drawing.Point(13, 11);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(196, 53);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(235, 159);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            // 
            // 
            // 
            this.txtPassword.RootElement.ControlBounds = new System.Drawing.Rectangle(235, 159, 125, 25);
            this.txtPassword.RootElement.StretchVertically = true;
            this.txtPassword.Size = new System.Drawing.Size(157, 25);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(235, 103);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserName.Name = "txtUserName";
            // 
            // 
            // 
            this.txtUserName.RootElement.ControlBounds = new System.Drawing.Rectangle(235, 103, 125, 25);
            this.txtUserName.RootElement.StretchVertically = true;
            this.txtUserName.Size = new System.Drawing.Size(157, 25);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel13
            // 
            this.radLabel13.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel13.Location = new System.Drawing.Point(123, 159);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            // 
            // 
            // 
            this.radLabel13.RootElement.ControlBounds = new System.Drawing.Rectangle(123, 159, 125, 22);
            this.radLabel13.Size = new System.Drawing.Size(99, 22);
            this.radLabel13.TabIndex = 14;
            this.radLabel13.Text = "Contraseña:";
            // 
            // radLabel3
            // 
            this.radLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.radLabel3.Location = new System.Drawing.Point(153, 101);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            // 
            // 
            // 
            this.radLabel3.RootElement.ControlBounds = new System.Drawing.Rectangle(153, 101, 125, 22);
            this.radLabel3.Size = new System.Drawing.Size(70, 22);
            this.radLabel3.TabIndex = 7;
            this.radLabel3.Text = "Usuario:";
            // 
            // LoginView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LoginView";
            this.Size = new System.Drawing.Size(557, 425);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbLaser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdTIC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbAutotest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPostVenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadTextBox txtPassword;
        private Telerik.WinControls.UI.RadTextBox txtUserName;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadRadioButton rbSAT;
        private Telerik.WinControls.UI.RadRadioButton rbPostVenta;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadRadioButton rbAutotest;
        private Telerik.WinControls.UI.RadRadioButton rdTIC;
        private Telerik.WinControls.UI.RadRadioButton printLabel;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadRadioButton rbLaser;
    }
}
