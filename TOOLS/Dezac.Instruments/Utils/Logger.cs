﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net.Appender;
using log4net.Core;
using log4net;

namespace Dezac.Instruments.Utils
{
    public static class Logger 
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Logger));

        public static void Error(string msg, Exception ex)
        {
            _logger.Error(msg, ex);
        }
    }
}
