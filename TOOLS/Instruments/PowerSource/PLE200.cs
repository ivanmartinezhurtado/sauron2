﻿using Comunications;
using Comunications.Message;
using Comunications.Protocolo;
using Dezac.Core.Utility;
using Instruments.SCPI;
using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;

namespace Instruments.PowerSource
{
    [InstrumentsVersion(1.00)]
    public class PLE200 : PowerSourceACIII, IDisposable, IConsoleDevice
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(PLE200));
        private SerialPort sp;
        private byte _port = 5;
        private string deviceTest;
        private readonly static CultureInfo enUS = new CultureInfo("en-US");
        public PLE200SCPI SCPI { get { return new PLE200SCPI(this); } }

        public int WaitTimeMeasure { get; set; }

        public enum OnOffEnum
        {
            UI = 1,
            U = 2,
            I = 3
        }

        public enum Phase
        {
            L1 = 1,
            L2 = 2,
            L3 = 3
        }

        public enum PrivateCommands
        {
            DEVICE_TEST = Commands.USER_DEFINED_COMMANDS + 1,
            STAT_PRESS = Commands.USER_DEFINED_COMMANDS + 2,
            OPER_STOP = Commands.USER_DEFINED_COMMANDS + 3,
            OPER_RUN = Commands.USER_DEFINED_COMMANDS + 4,
            COND = Commands.USER_DEFINED_COMMANDS + 5,
            PLE_MEN = Commands.USER_DEFINED_COMMANDS + 6,
            PLE_FREQ = Commands.USER_DEFINED_COMMANDS + 7,
            PLE_VOLT = Commands.USER_DEFINED_COMMANDS + 8,
            PLE_CURR = Commands.USER_DEFINED_COMMANDS + 9,
            PLE_PHASE = Commands.USER_DEFINED_COMMANDS + 10,
            OPER_ROT = Commands.USER_DEFINED_COMMANDS + 11,
            DIG = Commands.USER_DEFINED_COMMANDS + 12,
            SOURCE_MEN = Commands.USER_DEFINED_COMMANDS + 13,
            SOURCE_VOLT = Commands.USER_DEFINED_COMMANDS + 14,
            SOURCE_CURR = Commands.USER_DEFINED_COMMANDS + 15,
            SOURCE_PHASE = Commands.USER_DEFINED_COMMANDS + 16,
            SOURCE_FREQ = Commands.USER_DEFINED_COMMANDS + 17,
            STAT_OPER = Commands.USER_DEFINED_COMMANDS + 18,
            OPER_STAT_QUERY = Commands.USER_DEFINED_COMMANDS + 19,
        };

        public Dictionary<PrivateCommands, string> commandStrings = new Dictionary<PrivateCommands, string>()
        {
            {PrivateCommands.DEVICE_TEST, "*TST?"},
            {PrivateCommands.STAT_PRESS, ":STAT:PRES"},
            {PrivateCommands.OPER_STOP, ":OPER:STOP"},
            {PrivateCommands.OPER_RUN, ":OPER:RUN"},
            {PrivateCommands.OPER_STAT_QUERY, ":OPER:STAT?"},
            {PrivateCommands.OPER_ROT, ":OPER:ROT {0}"},
            {PrivateCommands.COND, ":COND {0}"},
            {PrivateCommands.PLE_MEN, "PLE:MEN"},
            {PrivateCommands.PLE_FREQ, ":PLE:FREQ {0}"},
            {PrivateCommands.PLE_VOLT, ":PLE:VOLT {0}"},
            {PrivateCommands.PLE_CURR, ":PLE:CURR {0}"},
            {PrivateCommands.PLE_PHASE, ":PLE:PHASE {0}"},
            {PrivateCommands.DIG, ":DIG #H{0:00}"},
            {PrivateCommands.SOURCE_MEN, ":SOUR:SOUR:MEN"},
            {PrivateCommands.SOURCE_VOLT, ":SOUR:SOUR:VOLT{0} {1}"},
            {PrivateCommands.SOURCE_CURR, ":SOUR:SOUR:CURR{0} {1}"},
            {PrivateCommands.SOURCE_PHASE, ":SOUR:SOUR:PHASE {0}"},
            {PrivateCommands.SOURCE_FREQ, ":SOUR:SOUR:FREQ {0}"},
            {PrivateCommands.STAT_OPER, ":STAT:OPER:ENAB #H{0}"}
        };
       
        private const string READ_VOLTAGE_COMMAND = ":SOUR:SOUR:VOLT{0}?";
        private const string READ_CURRENT_COMMAND = ":SOUR:SOUR:CURR{0}?";
        private const string READ_POWERFACTOR_COMMAND = ":SOUR:SOUR:PHASE?";


        public PLE200()
            : base(logger)
        {
            InitializeComponent();

            protocol = new StringProtocol() { CaracterFinTx = "\n\r", CaracterFinRx = "\r\n" };
            var spAdapter = new SerialPortAdapter(sp, "\r\n");
            transport = new MessageTransport(protocol, spAdapter, _logger);
        }

        private void InitializeComponent()
        {
            if (sp == null)
                sp = new SerialPort();

            sp.WriteTimeout = sp.ReadTimeout = 3000;
            sp.BaudRate = 57600;
            sp.StopBits = StopBits.One;
            sp.Parity = Parity.None;
            sp.DataBits = 8;
            sp.PortName = "COM" + _port.ToString();

            WaitTimeMeasure = 1000;
        }

        public byte PortCom
        {
            get { return _port; }
            set
            {
                if (_port == value)
                    return;

                _port = value;
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.PortName = "COM" + _port;
                    sp.Open();
                }
                else 
                    sp.PortName = "COM" + _port;
            }
        }

        public void SetPort(string port)
        {         
            var portTmp = port.Replace("COM", "").Trim();
            byte result = 0;
            if (byte.TryParse(portTmp, out result))
                PortCom = result;
            else
                throw new ArithmeticException("No se puede convertir el valor del argumrento port a byte");
        }

        public string GetIdentification()
        {
            var read = SCPI.Identification.Query();
            return read.Value;
        }

        public void Clear()
        {
            SCPI.Clear.Command();
        }

        public string GetOperationComplete()
        {
            var result = SCPI.OperationCompleteQuery.Query();
            return result.Value;
        }

        public string GetStatusByte()
        {
            var read = SCPI.StatusByte.Query();
            return read.Value;
        }

        public string DeviceTest()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.DEVICE_TEST, true));
            return deviceTest;
        }

        public void ConectionRotationPhase(bool L123 = true)
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.OPER_ROT, true, L123 ? 0 : 1));
        }

        public void InitConfiguration()
        {
            SendCommand(new Command<PrivateCommands>(PrivateCommands.STAT_PRESS, true));
            Clear();
            SendCommand(new Command<PrivateCommands>(PrivateCommands.OPER_STOP, true));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.COND, true, 4));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.PLE_MEN, true));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.PLE_FREQ, true, 0));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.PLE_VOLT, true, 0));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.PLE_CURR, true, 0));
            ConectionRotationPhase();
            SendCommand(new Command<PrivateCommands>(PrivateCommands.PLE_PHASE, true, 0));
            SendCommand(new Command<PrivateCommands>(PrivateCommands.DIG, true, 0));
        }

        public void SetCurrentOutput()
        {            
            if (Presets.Current.L1 >= 10 || Presets.Current.L2 >= 10 || Presets.Current.L3 >= 10)
                SendCommand(new Command<PrivateCommands>(PrivateCommands.DIG, true, 2));
            else
                SendCommand(new Command<PrivateCommands>(PrivateCommands.DIG, true, 0));
        }

        public void ApplyConfig()
        {
            SendCommand(new Command<Commands>(Commands.APPLY_CONFIG, true));
        }

        protected override void InternalSendCommand<T>(Command<T> command)
        {
            StringMessage message, response;

            if (!sp.IsOpen)
                sp.Open();

            switch (command.Commands)
            {
                case (int)PrivateCommands.STAT_PRESS:
                case (int)PrivateCommands.COND:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)PrivateCommands.DEVICE_TEST:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands]);
                    response = transport.SendMessage<StringMessage>(message);
                    deviceTest = response.Text;
                    break;
                case (int)PrivateCommands.DIG:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands)command.Commands], command.Args);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    break;
                case (int)Commands.READ_STATUS:
                    var result = SCPI.OperationCompleteQuery.Query();
                    Status.IsBusy = result.Value != "1";
                    var resp = SCPI.StatusByte.Query();
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.OPER_STAT_QUERY)]);
                    response = transport.SendMessage<StringMessage>(message);
                    var stat = response.Text;            
                    Status.HasError = resp.Value != "0" || stat.Contains("ERROR");
                    Status.HasWarning = false;
                    break;
                case (int)Commands.APPLY_CONFIG:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.OPER_RUN)]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(1000);
                    break;
                case (int)Commands.APPLY_OFF:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.OPER_STOP)]);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(1000);
                    break;
                case (int)Commands.SET_VOLTAGE:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_VOLT)], 1, Presets.Voltage.L1);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);

                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_VOLT)], 2, Presets.Voltage.L2);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);

                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_VOLT)], 3, Presets.Voltage.L3);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);

                    DevicePreset.Voltage = Presets.Voltage;
                    break;
                case (int)Commands.SET_CURRENT:
                    SetCurrentOutput();
                    
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_CURR)], 1, Presets.Current.L1);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(1000);

                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_CURR)], 2, Presets.Current.L2);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(1000);

                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_CURR)], 3, Presets.Current.L3);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    System.Threading.Thread.Sleep(1000);

                    DevicePreset.Current = Presets.Current;
                    break;
                case (int)Commands.SET_FREQ:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_FREQ)], Presets.Freq);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.Freq = Presets.Freq;
                    break;
                case (int)Commands.SET_PF:
                    message = StringMessage.Create(enUS, commandStrings[(PrivateCommands.SOURCE_PHASE)], Presets.PF.L1);
                    message.HasResponse = false;
                    transport.SendMessage<StringMessage>(message);
                    DevicePreset.PF = Presets.PF;
                    break;
                case (int)Commands.READ_DESFASE:
                case (int)Commands.SET_DESFASE:
                    DevicePreset.Desfase = Presets.Desfase;
                    DeviceData.Desfase = DevicePreset.Desfase;
                    break;          
                case (int)Commands.RESET:
                    SCPI.Reset.Command();
                    ResetPresets();
                    break;
                case (int)Commands.READ_VOLTAGE:
                    message = StringMessage.Create(enUS, READ_VOLTAGE_COMMAND, 1);
                    response = transport.SendMessage<StringMessage>(message);
                    var vl1 = Convert.ToDouble(response.Text, enUS);
                    message = StringMessage.Create(enUS, READ_VOLTAGE_COMMAND, 2);
                    response = transport.SendMessage<StringMessage>(message);
                    var vl2 = Convert.ToDouble(response.Text, enUS);
                    message = StringMessage.Create(enUS, READ_VOLTAGE_COMMAND, 3);
                    response = transport.SendMessage<StringMessage>(message);
                    var vl3 = Convert.ToDouble(response.Text, enUS);
                    DeviceData.Voltage = TriLineValue.Create(vl1, vl2, vl3);
                    break;
                case (int)Commands.READ_CURRENT:
                    message = StringMessage.Create(enUS, READ_CURRENT_COMMAND, 1);
                    response = transport.SendMessage<StringMessage>(message);
                    var currentl1 = Convert.ToDouble(response.Text, enUS);
                    message = StringMessage.Create(enUS, READ_CURRENT_COMMAND, 2);
                    response = transport.SendMessage<StringMessage>(message);
                    var currentl2 = Convert.ToDouble(response.Text, enUS);
                    message = StringMessage.Create(enUS, READ_CURRENT_COMMAND, 3);
                    response = transport.SendMessage<StringMessage>(message);
                    var currentl3 = Convert.ToDouble(response.Text, enUS);
                    DeviceData.Current = TriLineValue.Create(currentl1, currentl2, currentl3);
                    break;
                case (int)Commands.READ_POWERFACTOR:
                    message = new StringMessage { Text = READ_POWERFACTOR_COMMAND };
                    response = transport.SendMessage<StringMessage>(message);
                    var pf = Convert.ToDouble(response.Text, enUS);
                    DeviceData.PF = TriLineValue.Create(pf, pf, pf);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }


  //*************************************************************************************************

        protected override bool ValidateEstabilization(double tolerance)
        {
            var list = new StatisticalList("VoltageL1", "VoltageL2", "VoltageL3", "CurrentL1", "CurrentL2", "CurrentL3","PF_L1L2", "PF_L2L3", "PF_L3L1");
            var NumeroMuestras = 2;
            var maxRepeticiones = NumeroMuestras * 3;

            var sampler = Sampler.Run(
               () => new SamplerConfig { Interval = 1200, InitialDelay = 0, NumIterations = maxRepeticiones, CancelOnException = true, ThrowException = true},
               (step) =>
               {
                   var voltage = ReadVoltage();
                   var current = ReadCurrent();
                   var pf = ReadPowerFactor();

                   if (voltage.InToleranceFrom(DevicePreset.Voltage, tolerance) && current.InToleranceFrom(DevicePreset.Current, tolerance)
                       && pf.InToleranceFrom(DevicePreset.PF, tolerance))
                   {
                       var valores = new List<double>();
                       valores.AddRange(voltage.ToArray());
                       valores.AddRange(current.ToArray());
                       valores.AddRange(pf.ToArray());
                       list.Add(valores.ToArray());
                   }
                   else
                   {
                       list.Clear();
                       return;
                   }

                   if (list.Count(0) >= NumeroMuestras)
                       step.Cancel = true;
               });

            return sampler.Canceled;
        }

        protected virtual void OnMessageException(object sender, MessageExceptionHandler e)
        {
            logger.WarnFormat("PowerSource PLE200 OnMessageExceptionr by {0}", e.Exception.Message);
            if (e.Exception is ApplicationException)
                if (!e.Throw)
                {
                    SendCommand(new Command<Commands>(Commands.RESET, true));
                    System.Threading.Thread.Sleep(1000);
                }
        }

        public Result<string> WriteCommand(string text)
        {
            try
            {
                if (!sp.IsOpen)
                    sp.Open();

                var cmd = new SCPIMessage { Text = text  };
                var response = transport.SendMessage<SCPIMessage>(cmd);
                if (response != null)
                    return response.Text;

                response = transport.SendMessage<SCPIMessage>(new SCPIMessage { Text = "SYST:ERR?" });
                var tokens = response.Text.Split(',');
                int result = Convert.ToInt32(tokens[0]);
                if (result != 0)
                    throw new Exception(string.Format("PLE200 error: {0}", response.Text));

                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            if (sp != null)
                sp.Dispose();
        }
    }

    public class PLE200SCPI : SCPIDevice
    {
        internal PLE200SCPI(PLE200 device)
            : base(device)
        {
        }
    }
}
