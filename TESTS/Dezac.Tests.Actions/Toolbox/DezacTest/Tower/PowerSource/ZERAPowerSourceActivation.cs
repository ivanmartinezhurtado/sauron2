﻿using Dezac.Core.Utility;
using Dezac.Tests.Services;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.00)]
    [DesignerAction(Type = typeof(ZERAPowerSourceActivationActionDescription))]
    [Browsable(false)]
    public class ZERAPowerSourceActivation : ActionBase
    {
        [CategoryAttribute("Comunicaciones")]
        [DefaultValue(null)]
        [Description("Puerto Serie")]
        public string SerialPort { get; set; }

        [CategoryAttribute("1.- Voltage")]
        [DefaultValue(230)]
        [Description("Voltage L1")]
        public double V_L1 { get; set; }

        [CategoryAttribute("1.- Voltage")]
        [DefaultValue(230)]
        [Description("Voltage L2")]
        public double V_L2 { get; set; }

        [CategoryAttribute("1.- Voltage")]
        [Description("Voltage L3")]
        [DefaultValue(230)]
        public double V_L3 { get; set; }

        [CategoryAttribute("2.- Corriente")]
        [DefaultValue(0)]
        [Description("Corriente L1")]
        public double I_L1 { get; set; }

        [CategoryAttribute("2.- Corriente")]
        [DefaultValue(0)]
        [Description("Corriente L2")]
        public double I_L2 { get; set; }

        [CategoryAttribute("2.- Corriente")]
        [DefaultValue(0)]
        [Description("Corriente L3")]
        public double I_L3 { get; set; }

        [CategoryAttribute("3.- Angulo factor potencia")]
        [DefaultValue(0)]
        [Description("Angulo Factor de potencia L1")]
        public double PF_L1 { get; set; }

        [CategoryAttribute("3.- Angulo factor potencia")]
        [DefaultValue(0)]
        [Description("Angulo Factor de potencia L2")]
        public double PF_L2 { get; set; }

        [CategoryAttribute("3.- Angulo factor potencia")]
        [DefaultValue(0)]
        [Description("Angulo Factor de potencia L3")]
        public double PF_L3 { get; set; }

        [CategoryAttribute("4.- Desfase entre lineas")]
        [DefaultValue(0)]
        [Description("Angulo L1")]
        public double Gap_L1 { get; set; }

        [CategoryAttribute("4.- Desfase entre lineas")]
        [DefaultValue(120)]
        [Description("Angulo L2")]
        public double Gap_L2 { get; set; }

        [CategoryAttribute("4.- Desfase entre lineas")]
        [DefaultValue(240)]
        [Description("Angulo L3")]
        public double Gap_L3 { get; set; }

        [CategoryAttribute("5.- Frecuencia")]
        [DefaultValue(50)]
        [Description("Frecuencia")]
        public double Frequency { get; set; }

        [CategoryAttribute("6.- Configuracion fuente")]
        [DefaultValue(true)]
        [Description("Estabilizacion Interna")]
        public bool InternalStabilization { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TOWER").Throw();

            if (!string.IsNullOrEmpty(SerialPort))
            {
                var portDevice = Convert.ToByte(GetVariable<string>(SerialPort, SerialPort).Replace("COM", ""));
                tower.PortZera = portDevice;
                tower.ZERA.PortCom = portDevice;
            }

            var voltage = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var current = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };
            var powerFactor = new TriLineValue { L1 = PF_L1, L2 = PF_L2, L3 = PF_L3 };
            var desfase = new TriLineValue { L1 = Gap_L1, L2 = Gap_L2, L3 = Gap_L3 };

            if (InternalStabilization)
                tower.ZERA.ApplyPresetsAndWaitStabilisation(voltage, current, Frequency, powerFactor, desfase);
            else
                tower.ZERA.ApplyPresets(voltage, current, Frequency, powerFactor, desfase);
        }
    }
    
    public class ZERAPowerSourceActivationActionDescription : ActionDescription
    {
        public override string Name { get { return "ActivateZERA"; } }
        public override string Description { get { return "Control de la fuente ZERA"; } }
        public override string Category { get { return "Hardware/Tower/PowerSource"; } }
        public override Image Icon { get { return Properties.Resources.battery_charge_icon; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;PowerSourcesShutdown:2;TowerDispose:2"; } }
    }
}