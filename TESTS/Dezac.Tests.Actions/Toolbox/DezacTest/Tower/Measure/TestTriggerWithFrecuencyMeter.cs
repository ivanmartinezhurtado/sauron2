﻿using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(TestTriggerWithFrecuencyMeterDescription))]
    public class TestTriggerWithFrecuencyMeter : ActionBase
    {
        private HP53131A.Impedancia impedanceStart = new HP53131A.Impedancia() { Channel = 1, Filtro = HP53131A.Impedancia.FilterEnum.Filt_ON, LevelImpedancia = HP53131A.Impedancia.ImpedanciaEnum.OHM_1M };
        private HP53131A.Trigger triggerStart = new HP53131A.Trigger() { Channel = 1, Level = HP53131A.Trigger.LevelEnum.NUM_V_OFF, Hysterisis = HP53131A.Trigger.SensibilitiEnum.HI };

        private HP53131A.Impedancia impedanceStop = new HP53131A.Impedancia() { Channel = 2, Filtro = HP53131A.Impedancia.FilterEnum.Filt_ON, LevelImpedancia = HP53131A.Impedancia.ImpedanciaEnum.OHM_1M };
        private HP53131A.Trigger triggerStop = new HP53131A.Trigger() { Channel = 2, Level = HP53131A.Trigger.LevelEnum.NUM_V_OFF, Hysterisis = HP53131A.Trigger.SensibilitiEnum.HI };


        [Category("3.1 Configuration MUX")]
        [Description("Entrada MUX al canal de inicio")]
        public InputMuxEnum InputMuxToChannelStart { get; set; }
        
        [Category("3.1 Configuration MUX")]
        [Description("Entrada MUX al canal para parar")]
        public InputMuxEnum InputMuxToChannelStop { get; set; }

        [Category("3.2 Times")]
        [Description("Medida Tiempo de espera")]
        public int TimeOutMeasure { get; set; }
        
        [Category("3.2 Times")]
        [Description("Señal tiempo de espera ")]
        public int DelayStartSignal { get; set; }

        [Category("3.3 Configuration Channel Start")]
        [Description("Impedancia de entrada")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public HP53131A.Impedancia ImpedanceStart { get { return impedanceStart; } set { impedanceStart = value; } }

        [Category("3.3 Configuration Channel Start")]
        [Description("Disparador de entrada")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public HP53131A.Trigger TriggerStart { get { return triggerStart; } set { triggerStart = value; } }

        [Category("3.3 Configuration Channel Start")]
        [Description("Señal de inicio de salida")]
        public byte? OutputSignalStart { get; set; }

        [Category("3.4 Configuration Channel Stop")]
        [Description("Impedancia de parada")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public HP53131A.Impedancia ImpedanceStop { get { return impedanceStop; } set { impedanceStop = value; } }

        [Category("3.4 Configuration Channel Stop")]
        [Description("Disparador de parada")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public HP53131A.Trigger TriggerStop { get { return triggerStop; } set { triggerStop = value; } }

        [Category("3.5 TestPoint")]
        [Description("Nombre del TestPoint")]
        public string TestPoint { get; set; }

        public override void Execute()
        {
            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            tower.ActiveCurrentCircuit(true, true, true);

            tower.IO.DO.On(OutputSignalStart.Value);

            tower.MUX.Reset();
            tower.MUX.Connect2((int)InputMuxToChannelStart, Outputs.FreCH1, (int)InputMuxToChannelStop, Outputs.FreCH2);

            tower.HP53131A.PresetConfigImpendance((HP53131A.Channels)ImpedanceStart.Channel, ImpedanceStart.Acoplo, ImpedanceStart.Atenuacion, ImpedanceStart.Filtro, ImpedanceStart.LevelImpedancia);
            tower.HP53131A.PresetConfigTrigger((HP53131A.Channels)TriggerStart.Channel, TriggerStart.Level, TriggerStart.Slope, TriggerStart.Hysterisis, TriggerStart.LevelValue);

            tower.HP53131A.PresetConfigImpendance((HP53131A.Channels)ImpedanceStop.Channel, ImpedanceStop.Acoplo, ImpedanceStop.Atenuacion, ImpedanceStop.Filtro, ImpedanceStop.LevelImpedancia);
            tower.HP53131A.PresetConfigTrigger((HP53131A.Channels)TriggerStop.Channel, TriggerStop.Level, TriggerStop.Slope, TriggerStop.Hysterisis, TriggerStop.LevelValue);


            var adjustValue = new AdjustValueDef(testBase.Params.TIME.Null.TRIGGER.TestPoint(TestPoint), ParamUnidad.s);

            var adjustResult = testBase.TestMeasureBase(adjustValue, (s) =>
            {
                try
                {
                    var timeResult = tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, TimeOutMeasure, HP53131A.Channels.CH1, () =>
                    {
                        Thread.Sleep(DelayStartSignal);

                        if(OutputSignalStart.HasValue)
                            tower.IO.DO.Off(OutputSignalStart.Value);
                    });

                    return (timeResult.Value);
                }
                catch (Exception)
                {
                    throw Error().UUT.DISPARO.NO_DISPARA();
                }
            }, 0, 1, 1000);

            tower.ActiveCurrentCircuit(true, true, true);

        }

    }


    class TestTriggerWithFrecuencyMeterDescription : ActionDescription
    {
        public override string Name { get { return "TestTriggerWithFrecuencyMeter"; } }
        public override string Description { get { return "Action que sirve para medir la siguiente unidad con los criterios definidos en los \"Parameters\" haciendo medidas de la unidad"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.LineChart; } }
        public override string Dependencies { get { return "InitTestBase;TowerInitialization;"; } }
    }
}
