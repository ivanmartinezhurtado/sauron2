﻿using Dezac.Device.Protection;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.Utils;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;

namespace Dezac.Tests.Protection
{
    [TestVersion(1.03)]
    public class Fusible_CBT_TestPlacas : TestBase
    {
        #region MapaUtil

        public const byte IN_DEVICE_PRESENCE = 21;
        public const byte IN_SECURITY_BLOQ = 24;

        public const byte OUT_CLOSE_UNION_FUSE = 20;
        public const byte OUT_OPEN_INT1 = 21;
        public const byte OUT_RESET_GND = 22;

        public const InputMuxEnum INPUT_TP_VDD1 = InputMuxEnum.IN11;

        #endregion

        private CBT cbt;
        private TowerBoard tower;

        public void TestInitialization(int port = 0, int baudrate = 0)
        {
            port = port == 0 ? Comunicaciones.SerialPort : port;
            baudrate = baudrate == 0 ? Comunicaciones.BaudRate: baudrate;
            cbt = new CBT(port, baudrate);
            cbt.Modbus.PerifericNumber = Comunicaciones.Periferico;
           
            var cacheSvc = this.GetService<ICacheService>();
            if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
            {
                tower = AddOrGetSharedVar<TowerBoard>("TOWER", () => { return new TowerBoard(); });
                if (cacheSvc != null)
                    cacheSvc.Add("TOWER", tower);
            }

            tower.MUX.Reset();
            tower.Active24VDC();
            tower.ActiveVoltageCircuit();
            tower.ActiveCurrentCircuit(false, false, false);
            tower.Chroma.ApplyOff();

            SamplerWithCancel((p) =>
            {
                if (!tower.IO.DI[IN_DEVICE_PRESENCE])
                    return false;
                return true;

            }, "Error No se ha detectado el equipo o util abierto", 30, 500, 1500, false, false);

            tower.IO.DO.On(OUT_RESET_GND);
        }

        public void TestCommunications()
        {
            var version = "";
            this.SamplerWithCancel((p) => { version = cbt.ReadFirmwareVersion(); return true; },"Error: El equipo no comuncia");
            Assert.AreEqual("VERSION_FIRMWARE", version, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error la version de firmware del equipo no coincide con la de la BBDD"), ParamUnidad.SinUnidad);
        }

        public void TestPoints()
        {
            var VcpuBBDD = new AdjustValueDef(Params.V.Null.TestPoint("CPU").Name, 0, Params.V.Null.TestPoint("CPU").Min(), Params.V.Null.TestPoint("CPU").Max(), 0, 0, ParamUnidad.V);
            var Vcpu = 0.0;

            TestMeasureBase(VcpuBBDD, (step) =>
            {
                Vcpu = cbt.ReadVcpu();
                return Vcpu;
            }, 0, 3, 1000);

            var VradioBBDD = new AdjustValueDef(Params.V.Null.TestPoint("RADIO").Name, 0, 0, 0, Vcpu - 0.02, 5, ParamUnidad.V);
            var Vradio = 0.0;

            TestMeasureBase(VradioBBDD, (step) =>
            {
                Vradio = tower.MeasureMultimeter(INPUT_TP_VDD1, new MagnitudToMeasure(MagnitudsMultimeter.VoltDC, 4), 1000);
                return Vradio;
            }, 0, 3, 1000);

            var VhBBDD = new AdjustValueDef(Params.V.Null.TestPoint("DIODOS").Name, 0, Params.V.Null.TestPoint("DIODOS").Min(), Params.V.Null.TestPoint("DIODOS").Max(), 0, 0, ParamUnidad.V);
            var Vh = 0.0;

            TestMeasureBase(VhBBDD, (step) =>
            {
                Vh = (cbt.ReadVh() * Vcpu) / 1024.0;
                return Vh;
            }, 0, 3, 1000);

            var ItoroBBDD = new AdjustValueDef(Params.I.Null.TestPoint("TOROIDAL").Name, 0, Params.I.Null.TestPoint("TOROIDAL").Min(), Params.I.Null.TestPoint("TOROIDAL").Max(), 0, 0, ParamUnidad.A);
            var Itoro = 0.0;

            TestMeasureBase(ItoroBBDD, (step) =>
            {
                Itoro = cbt.ReadCurrent();
                return Itoro;
            }, 0, 3, 1000);

            tower.IO.DO.On(OUT_CLOSE_UNION_FUSE);
            tower.IO.DO.Off(OUT_OPEN_INT1);
            Delay(200, "Conmutacion Salidas torre");

            var VfuseCloseBBDD = new AdjustValueDef(Params.V.Null.TestPoint("FUSIBLE_CERRADO").Name, 0, Params.V.Null.TestPoint("FUSIBLE_CERRADO").Min(), Params.V.Null.TestPoint("FUSIBLE_CERRADO").Max(), 0, 0, ParamUnidad.V);
            var VfuseClose = 0.0;

            TestMeasureBase(VfuseCloseBBDD, (step) =>
            {
                VfuseClose = (Math.Sqrt(cbt.ReadVfuse()) * Vcpu) / 1024.0;
                return VfuseClose;
            }, 0, 3, 1000);

            var VfuseOpenBBDD = new AdjustValueDef(Params.V.Null.TestPoint("FUSIBLE_ABIERTO").Name, 0, Params.V.Null.TestPoint("FUSIBLE_ABIERTO").Min(), Params.V.Null.TestPoint("FUSIBLE_ABIERTO").Max(), 0, 0, ParamUnidad.V);
            var VfuseOpen = 0.0;

            tower.IO.DO.On(OUT_OPEN_INT1);
            Delay(200, "Conmutacion Salidas torre");

            TestMeasureBase(VfuseOpenBBDD, (step) =>
            {
                VfuseOpen = (Math.Sqrt(cbt.ReadVfuse()) * Vcpu) / 1024.0;
                return VfuseOpen;
            }, 0, 3, 1000);

            tower.IO.DO.Off(OUT_OPEN_INT1, OUT_CLOSE_UNION_FUSE);
            Delay(200, "Conmutacion Salidas torre");

        }

        public void TestCalibrationClock()
        {
            tower.HP53131A
            .PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X1, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M)
            .PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1.6D)
            .PresetConfigGate(HP53131A.Gate.FuncionGateEnum.FREQ, HP53131A.Gate.ModoGateStartEnum.AutoStart, HP53131A.Gate.SlopeEnum.POS, HP53131A.Gate.ModoGateStopEnum.Digits, HP53131A.Gate.SlopeEnum.NEG, 9);

            int i = 0;
            ushort clockGain = 7;
            while (i < 7)
            {
                cbt.WriteClockConstant(clockGain);  

                Delay(500, "Grabacion ppm en el micro");

                ResetPowerSource();

                Assert.AreEqual(cbt.ReadClockConstant(), clockGain, Error().UUT.MEDIDA.NO_GRABADO("Error: No se ha podido grabar la cte. de clock"));

                var frec = tower.MeasureWithFrecuency(InputMuxEnum.NOTHING, 20000, HP53131A.Variables.FREQ, HP53131A.Channels.CH1);       

                try
                {
                    Assert.IsValidMaxMin(Params.FREQ.Null.RTC.Name, frec, ParamUnidad.Hz, Params.FREQ.Null.RTC.Min(), Params.FREQ.Null.RTC.Max(), Error().UUT.MEDIDA.MARGENES("RTC"));
                    break;
                }
                catch(AssertFailedException e)
                {
                    i++;
                    if (i >= 7)
                        throw e;

                    if ((Params.FREQ.Null.RTC.Min() + Params.FREQ.Null.RTC.Max()) / 2 < frec)
                        clockGain++;
                    else
                        clockGain--;
                }
            }

        }

        public void TestCostumization()
        {
            cbt.WriteNumBastidor((uint)TestInfo.NumBastidor);

            Delay(1000, "");

            cbt.WriteNumSerie((uint)UInt32.Parse(TestInfo.NumSerie));

            Delay(1000, "");

            ResetPowerSource();

            Assert.AreEqual("NUM_BASTIDOR", cbt.ReadNumBastidor(), (uint)TestInfo.NumBastidor, Error().UUT.NUMERO_DE_BASTIDOR.PARAMETROS_NO_GRABADOS("Error: El numero de bastidor no se ha guardado correctamente"), ParamUnidad.SinUnidad);

            Delay(1000, "");

            Assert.AreEqual("NUM_SERIE", cbt.ReadNumSerie().ToString(), TestInfo.NumSerie, Error().UUT.NUMERO_DE_BASTIDOR.PARAMETROS_NO_GRABADOS("Error: El numero de serie no se ha guardado correctamente"), ParamUnidad.SinUnidad);

            Delay(1000, "");

            Assert.AreEqual("ALARMAS", cbt.ReadAlarms(), 0, Error().UUT.CONFIGURACION.ALARMA("Error: El equipo tiene alguna alarma"), ParamUnidad.SinUnidad); 
        }
    
        public void TestFinish()
        {
            if (cbt != null)
                cbt.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                tower.Dispose();
            }
        }

        private void ResetPowerSource()
        {
            tower.Chroma.ApplyOffAndWaitStabilisation();
            Delay(2000, "Apagado del equipo");
            var voltage = Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 230, ParamUnidad.V); 
            tower.Chroma.ApplyPresetsAndWaitStabilisation(voltage, 0, 50);
            Delay(4000, "Esperando encendido del equipo");
        }

    }
}
