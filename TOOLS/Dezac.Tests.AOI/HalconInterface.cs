﻿using Comunications.Ethernet;
using Dezac.Core.Model;
using Dezac.Services;
using Dezac.Tests.AOI.Models;
using Dezac.Tests.Extensions;
using HalconDotNet;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Dezac.Tests.AOI
{
    public class HalconInterface
    {
        private static ILog logger = LogManager.GetLogger("TEST_UUT");
        private static bool isInitialized;
        private static readonly object _syncObject = new object();

        public static void InitEngine()
        {
            lock (_syncObject)
            {
                if (!isInitialized)
                {
                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    logger.DebugFormat("Initializing HalconEngine");

                    HalconEngine.Init();

                    isInitialized = true;

                    stopwatch.Stop();
                    logger.DebugFormat("HalconEngine Initialized in {0} ms", stopwatch.ElapsedMilliseconds);
                }
            }
        }

        /// <summary>
        /// Procedures to grab and save an image.
        /// </summary>
        public static void AOIGrabAndSaveImage(string fileName)
        {
            Stopwatch stopwatch = new Stopwatch();

            InitEngine();

            logger.DebugFormat("Start lineal image grabbing");
            stopwatch.Start();

            HObject image = HalconFrameGrabber.GrabImage();
                
            stopwatch.Stop();
            logger.DebugFormat("Image grab success in {0} ms", stopwatch.ElapsedMilliseconds);

            SaveImage(image, fileName);
        }

        /// <summary>
        /// Extract and save the features from a list of welding images
        /// </summary>
        public static void AOIExtractFeatures(string weldingsFolder, string inFeaturesFile, string outFeaturesFile)
        {
            InitEngine();

            HObject weldings = null;
            HTuple weldingsFolderAux = null;

            HOperatorSet.ListFiles((HTuple)weldingsFolder, new HTuple("files", "follow_links"), out weldingsFolderAux);

            HOperatorSet.TupleRegexpSelect(weldingsFolderAux, new HTuple("\\.(tif|tiff|gif|bmp|jpg|jpeg|jp2|png|pcx|pgm|ppm|pbm|xwd|ima|hobj)$", "ignore_case"), out weldingsFolderAux);

            HOperatorSet.ReadImage(out weldings, weldingsFolderAux);

            var result = HalconEngine.CallProcedure(HalconProcedureName.ExtractGeometricFeatures, weldings);
            if (!result.Success)
                throw result.Exception;

            HTupleVector features = result.VectorResults["Features"];

            object[] inputs = new object[] { features, inFeaturesFile, outFeaturesFile };
            result = HalconEngine.CallProcedure(HalconProcedureName.AddSamplesToSVM, inputs);
            if (!result.Success)
                throw result.Exception;
        }

        /// <summary>
        /// Perform welding classification grabbing an image at the same moment.
        /// </summary>
        public static HalconInspectionResults AOIClassification(AOIClassficationInfo info, string imageSaveName)
        {
            Stopwatch stopwatch = new Stopwatch();

            InitEngine();

            logger.DebugFormat("Start lineal image grabbing");
            stopwatch.Start();

            HObject image = HalconFrameGrabber.GrabImage();
            //HOperatorSet.ReadImage(out image, "W:\\_AOI\\IMAGES\\116091\\04052018_103738.png");

            stopwatch.Stop();
            logger.DebugFormat("Image grab success in {0} ms", stopwatch.ElapsedMilliseconds );

            var result = new HalconInspectionResults();
            try
            {
                result = InternalAOIClassification(image, info);
                result.GrabImageTime = stopwatch.ElapsedMilliseconds;
            }
            finally
            {
                stopwatch.Restart();
                if (!String.IsNullOrEmpty(imageSaveName))
                {
                    if (result.Barcodes != null)
                        Task.Factory.StartNew(() => SaveImage(image, imageSaveName.Insert(imageSaveName.IndexOf('.'), "_" + string.Join<string>("_", result.Barcodes))));
                    else
                        Task.Factory.StartNew(() => SaveImage(image, imageSaveName));
                }
                stopwatch.Stop();

                result.SaveImageTime = stopwatch.ElapsedMilliseconds;
            }

            return result;
        }

        /// <summary>
        /// Perform welding classification in a specified image "imageFile".
        /// </summary>
        public static HalconInspectionResults AOIClassification(string imageFile, AOIClassficationInfo info)
        {
            InitEngine();

            HObject image = null;
            HOperatorSet.ReadImage(out image, imageFile);

            return InternalAOIClassification(image, info);
        }

        private static HalconInspectionResults InternalAOIClassification(HObject image, AOIClassficationInfo info)
        {
            logger.Debug("AOIClassification");

            object[] inputs = new object[] { image, (HTuple)info.matchingFileName, (HTuple)info.searchFileName,
                (HTuple)info.ROIFileName, (HTuple)info.boardRegionFileName, (HTuple)info.numRows,
                (HTuple)info.numColumns, (HTuple)info.rowDistance, (HTuple)info.columnDistance, (HTuple)"false" };

            var extractWeldignResult = HalconEngine.CallProcedure(HalconProcedureName.ExtractWeldignV3, inputs);
            if (!extractWeldignResult.Success)
                throw extractWeldignResult.Exception;
            logger.DebugFormat("CallProcedure {0} Success", HalconProcedureName.ExtractWeldignV3);

            var ROIWeldings = extractWeldignResult.IconicResults["ROI_MeasureAtNewPosition"];
            var ROIBoard = extractWeldignResult.IconicResults["ROI_Board"];

            inputs = new object[] { image, ROIBoard };

            var readBarcodesResults = HalconEngine.CallProcedure(HalconProcedureName.ReadBarcodes, inputs);
            if (!readBarcodesResults.Success)
                throw readBarcodesResults.Exception;
            logger.DebugFormat("CallProcedure {0} Success", HalconProcedureName.ReadBarcodes);

            if (readBarcodesResults.TupleResults["Results"].LArr.ToList().Any((x) => x == -1) && info.barcodeRead)
            {
                var index = readBarcodesResults.TupleResults["Results"].LArr.ToList().
                    Select((b, i) => b == -1 ? i : -1).Where(i => i != -1).ToList();
                throw new Exception("Error: Imposible to read barcode of board #" + string.Join<string>(", #", index.ConvertAll<string>((x) => x.ToString())));
            }

            HObject weldings = extractWeldignResult.IconicResults["Weldings"];

            var extractFeaturesResult = HalconEngine.CallProcedure(HalconProcedureName.ExtractSpecularity2Features, weldings);
            if (!extractFeaturesResult.Success)
                throw extractFeaturesResult.Exception;
            logger.DebugFormat("CallProcedure {0} Success", HalconProcedureName.ExtractSpecularity2Features);

            HTupleVector features = extractFeaturesResult.VectorResults["Features"];

            HTuple SVMHandle = new HTuple() ;
            HTuple SVMHandleAux = null;

            foreach (var file in info.SVMFile)
            {
                HOperatorSet.ReadClassSvm(file, out SVMHandleAux);
                SVMHandle.Append(SVMHandleAux);
            }

            HTuple selector = new HTuple();
            if (File.Exists(info.weldingSVM))
                HOperatorSet.ReadTuple(info.weldingSVM, out selector);

            inputs = new object[] { SVMHandle, features, selector };
            var classificationResult = HalconEngine.CallProcedure(HalconProcedureName.Classification, inputs);
            if (!classificationResult.Success)
                throw classificationResult.Exception;
            logger.DebugFormat("CallProcedure {0} Success", HalconProcedureName.Classification);
            HOperatorSet.ClearClassSvm(SVMHandle);


            if (!info.barcodeRead)
                using (DezacService svc = new DezacService())
                {
                    readBarcodesResults.TupleResults["Barcodes"] = svc.GetNewMatricula(info.numColumns * info.numRows);
                }

            var inspectionResults = new HalconInspectionResults(readBarcodesResults.TupleResults["Barcodes"].SArr, classificationResult.TupleResults["Results"].ToDArr(), selector.ToIArr());
            inspectionResults.ExtractWeldingsTime = extractWeldignResult.ExecutionTime;
            inspectionResults.ReadBarcodesTime = readBarcodesResults.ExecutionTime;
            inspectionResults.ExtractFeaturesTime = extractFeaturesResult.ExecutionTime;
            inspectionResults.ClassificationTime = classificationResult.ExecutionTime;

            SaveIconicResults(image, ROIWeldings, ROIBoard, inspectionResults.Barcodes, info.orderPath);

            return inspectionResults;
        }

        private static void SaveImage(HObject image, string imageFileName)
        {
            HOperatorSet.Compose3(image, image, image, out HObject imageNew);
            HOperatorSet.InterleaveChannels(imageNew, out imageNew, "argb", "match", 255);
            HOperatorSet.GetImagePointer1(imageNew, out HTuple pointer, out HTuple type, out HTuple width, out HTuple height);

            using (FTPClient client = new FTPClient(ConfigurationManager.AppSettings["VAFTPDirection"], ConfigurationManager.AppSettings["VAFTPUser"], ConfigurationManager.AppSettings["VAFTPPassword"]))
            using (Bitmap bitmap = new Bitmap(width/4, height, width, PixelFormat.Format32bppArgb, pointer))
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Png);
                client.CreateDirectory(Path.GetDirectoryName(imageFileName));
                client.UploadOverwrite(stream, imageFileName, 3);
            }
            
            logger.DebugFormat("Save Image Success. Filename: {0}", imageFileName);
        }

        private static void SaveIconicResults(HObject image, HObject ROIWeldings, HObject ROIBoards, string[] barcodes, string orderPath)
        {
            string jointBarcodes = string.Join("_", barcodes);

            if (!Directory.Exists(orderPath))
                Directory.CreateDirectory(orderPath);

            if (!Directory.Exists(orderPath + "Iconic\\"))
                Directory.CreateDirectory(orderPath + "Iconic\\");

            HOperatorSet.WriteObject(image, orderPath + "Iconic\\Image_" + jointBarcodes + ".hobj");
            HOperatorSet.WriteObject(ROIWeldings, orderPath + "Iconic\\Weldings_" + jointBarcodes + ".hobj");
            HOperatorSet.WriteObject(ROIBoards, orderPath + "Iconic\\WBoards_" + jointBarcodes + ".hobj");
        }
    }
}
