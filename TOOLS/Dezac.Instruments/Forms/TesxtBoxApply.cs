﻿using Instruments.PowerSource;
using System;
using System.Windows.Forms;

namespace Dezac.Instruments.Forms
{
    public partial class TesxtBoxApply : Form
    {
        public TesxtBoxApply()
        {
            InitializeComponent();
        }

        public MTEPPS.Phase Phase
        {
            get
            {
                MTEPPS.Phase phase = MTEPPS.Phase.L1;

                if (!string.IsNullOrEmpty(cmbPhase.SelectedItem.ToString()))
                    Enum.TryParse<MTEPPS.Phase>(cmbPhase.SelectedItem.ToString(), out phase);     
               
                return phase;
            }
        } 

        public byte Harmonic
        {
            get
            {
                byte harmonic = 0;
                byte.TryParse(txtHarmonic.Text, out harmonic);
                return harmonic;
            }
        }

        public bool IsHarmonicVoltage
        {
            get
            {
                return rbVoltage.Checked;
            }
        }

        public int Amplitud
        {
            get
            {
                int amplitud = 0;
                int.TryParse(txtAmplitud.Text, out amplitud);
                return amplitud;
            }
        }
    }
}
