﻿using Dezac.Tests.Extensions;
using Dezac.Tests.Services;
using Instruments.Towers;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.01)]
    [DesignerAction(Type = typeof(TestMeasureWithAnalizerDescription))]
    public class TestMeasureWithAnalizer : ActionBase
    {
        [Category("3. Configuration")]
        [Description("Selección del analizador")]
        [DefaultValue(TestMeasureWithAnalizerExtension.TowerAnalizer.CVMB100Low)]
        public TestMeasureWithAnalizerExtension.TowerAnalizer AnalizerSelected { get; set; }

        [Category("3. Configuration")]
        [Description("Selección de la magnitud")]
        [DefaultValue(TestMeasureWithAnalizerExtension.Magnitud.Voltage)]
        public TestMeasureWithAnalizerExtension.Magnitud MagnitudSelected { get; set; }

        [Category("3. Configuration")]
        [Description("Tiempo de intervalo")]
        [DefaultValue(1000)]
        public int IntervalTime { get; set; }

        [Category("3. Configuration")]
        [Description("Iteraciones")]
        [DefaultValue(2)]
        public int Restries { get; set; }

        [Category("3. Configuration")]
        [Description("Lectura de medida del tiempo de espera")]
        [DefaultValue(500)]
        public int WaitTimeReadMeasure { get; set; }

        [Category("4. Results")]
        [Description("Nombre del resultado")]
        public string DataOutput { get; set; }


        public override void Execute()
        {

            var testBase = GetVariable<TestBase>("TEST_UUT", () => { return null; });
            if (testBase == null)
                Error().SOFTWARE.SECUENCIA_TEST.FALTA_TEST_BASE("TestBase").Throw();

            var cacheSvc = testBase.GetService<ICacheService>();
            var tower = cacheSvc.Get<ITower>("TOWER");
            if (tower == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("TORRE").Throw();

            if (DataOutput == null)
                DataOutput = "";

            var tespoint = new TestMeasureWithAnalizerExtension.TestPointConfiguration()
            {
                AnalizerSelected = AnalizerSelected,
                magnitudSelected = MagnitudSelected,
                ResultName = DataOutput,
                Iteraciones = Restries,
                TiempoIntervalo = IntervalTime,
                WaitTimeReadMeasure = WaitTimeReadMeasure
            };

            testBase.TestMeasureWithAnalizer(tower, tespoint);
        }
    }

    public class TestMeasureWithAnalizerDescription : ActionDescription
    {
        public override string Name { get { return "TestMeasureWithAnalizer"; } }
        public override string Description { get { return "Acción genérica para medir con el analizador de la torre"; } }
        public override string Category { get { return "DezacTest/Tower/Measure"; } }
        public override Image Icon { get { return Properties.Resources.Line_Chart_icon__1_; } }
        public override string Dependencies { get { return "InitTestBase"; } }
    }
}
