namespace Dezac.Entities
{
    using System;
    using System.Collections.Generic;
    
    
    public partial class ORDENPRODUCTO
    {
        public ORDENPRODUCTO()
        {
            this.ORDENPRODUCTOERROR = new HashSet<ORDENPRODUCTOERROR>();
            this.T_TEST = new HashSet<T_TEST>();
        }
    
        public int NUMFABRICACION { get; set; }
        public int NUMORDEN { get; set; }
        public int NUMPRODUCTO { get; set; }
        public System.DateTime FECHA { get; set; }
        public Nullable<int> NUMMATRICULA { get; set; }
        public string NROSERIE { get; set; }
        public string IDOPERARIO { get; set; }
        public string PRODUCTOVALIDO { get; set; }
        public Nullable<System.DateTime> FECHAMAT { get; set; }
        public string SITUACION { get; set; }
        public Nullable<System.DateTime> FECHASIT { get; set; }
        public Nullable<int> NUMCAJA { get; set; }
    
        public virtual MATRICULA MATRICULA { get; set; }
        public virtual ORDEN ORDEN { get; set; }
        public virtual ICollection<ORDENPRODUCTOERROR> ORDENPRODUCTOERROR { get; set; }
        public virtual ICollection<T_TEST> T_TEST { get; set; }
    }
}
