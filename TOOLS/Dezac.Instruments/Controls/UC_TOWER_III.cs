﻿using Dezac.Core.Utility;
using Instruments.Towers;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(Tower3Description))]
    public partial class UC_TOWER3 : UserControl
    {
        private Tower3 tower;

        public Type InstrumentType
        {
            get
            {
                return typeof(Tower3);
            }
        }

        public Tower3 Instrument
        {
            get
            {
                if (tower == null)
                    tower = new Tower3();

                return tower;
            }
            set
            {
                if (tower != null)
                {
                    tower.Dispose();
                    tower = null;
                }
                tower = value;
            }
        }

        public UC_TOWER3()
        {
            InitializeComponent();          
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if(Instrument != null)
                Instrument.Dispose();

            base.Dispose(disposing);
        }
    }
    public class Tower3Description : ControlDescription
    {
        public override string Name { get { return "Tower_III"; } }
        public override string Category { get { return "Tower"; } }
        public override Image Icon { get { return Properties.Resources.Tower; } }
        public override Type SubcomponentType { get { return typeof(Tower3); } }
    }
}
