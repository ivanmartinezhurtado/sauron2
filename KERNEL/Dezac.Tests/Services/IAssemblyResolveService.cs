﻿using System.Collections.Generic;
using System.Reflection;

namespace Dezac.Tests.Services
{
    public interface IAssemblyResolveService
    {
        string DefaultPathFiles { get; set; }
        Assembly Resolve(string name);
        Assembly Resolve(string path, string name);
    }
}
