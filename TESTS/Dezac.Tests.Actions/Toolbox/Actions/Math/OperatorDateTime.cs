﻿using Dezac.Core.Exceptions;
using Dezac.Tests.Actions.Kernel;
using Dezac.Tests.Actions.Views;
using System;
using System.ComponentModel;
using System.Drawing;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.03)]
    [DesignerAction(Type = typeof(DiffToDateTimeDescriprion))]
    public class DiffToDateTime : ActionBase
    {
        //TODO: UNIFICAR en un action generico para fechas

        [Description("Nombre del dato de tipo DateTime correspondiente al operador 1")]
        [Category("3. Configuration")]
        public ActionVariable DataInput1 { get; set; } = new ActionVariable();

        [Description("Nombre del dato de tipo DateTime correspondiente al operador 2")]
        [Category("3. Configuration")]
        public ActionVariable DataInput2 { get; set; } = new ActionVariable();

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {

            if (DataInput1.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DateTime1").Throw();

            if (DataInput2.IsKeyNullOrEmpty)
                TestException.Create().PROCESO.ACTION_EXECUTE.FALTA_VALOR_PARAMETRO("DateTime2").Throw();

            var opr1 = Convert.ToDateTime(DataInput1.Value.ToString());
            var opr2 = Convert.ToDateTime(DataInput2.Value.ToString());
            Logger.InfoFormat("Operador1={0}  Substract  Operador2={1}", opr1, opr2);
            var result2 = opr1.Subtract(opr2);
            Logger.InfoFormat("Resultado ={0}", result2);
            Context.ResultList.Add(string.Format("Operador1={0} Substract  Operador2={1}  Resultado={2}", opr1, opr2, result2));
            DataOutput.Value = result2;
        }
    }

    public class DiffToDateTimeDescriprion : ActionDescription
    {
        public override string Name { get { return "DiffToDateTime"; } }
        public override string Description { get { return "Realiza operaciones con fecha y tiempos"; } }
        public override string Category { get { return "Actions/Math"; } }
        public override Image Icon { get { return Properties.Resources.Math; } }
    }
}
