﻿using FluentFTP;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Comunications.Ethernet
{
    public struct FileINFO
    {
        public string Name;
        public string FullName;
        public ulong ModDate;
    }

    public class FTPClient : IDisposable
    {
        private static readonly ILog logger = LogManager.GetLogger("FTPClient");

        private FtpClient client;

        public FTPClient()
        {
            client = new FtpClient();
        }

        public FTPClient(string ip)
        {
            // if you don't specify login credentials, we use the "anonymous" user account
            client = new FtpClient(ip);
            client.Connect();
        }

        public FTPClient(string ip, string user, string password)
        {
            client = new FtpClient(ip);
            client.OnLogEvent += OnFTPLogEvent;
            client.Credentials = new NetworkCredential(user, password);
            client.Connect();
        }

        private static void OnFTPLogEvent(FtpTraceLevel ftpTraceLevel, string logMessage)
        {
            switch (ftpTraceLevel)
            {
                case FtpTraceLevel.Error:
                    logger.Error(logMessage);
                    break;
                case FtpTraceLevel.Verbose:
                    logger.Debug(logMessage);
                    break;
                case FtpTraceLevel.Warn:
                    logger.Warn(logMessage);
                    break;
                case FtpTraceLevel.Info:
                default:
                    logger.Info(logMessage);
                    break;
            }
        }

        public bool UploadOverwrite(string localPath, string remotePath, int retry)
        {
            var auxstring = remotePath.Split('/');
            var directory = "";
            for (int i = 0; i < auxstring.Length - 1; i++)
                directory += auxstring[i] + '/';

            CreateDirectory(directory);

            client.RetryAttempts = retry;
            var status =  client.UploadFile(localPath, remotePath, FtpRemoteExists.Overwrite, false, FtpVerify.Retry);
            return status != FtpStatus.Failed;
        }

        public bool UploadOverwrite(Stream stream, string remotePath, int retry)
        {
            var auxstring = remotePath.Split('/');
            var directory = "";
            for (int i = 0; i < auxstring.Length - 1; i++)
                directory += auxstring[i] + '/';

            CreateDirectory(directory);

            client.RetryAttempts = retry;
            var status = client.Upload(stream, remotePath, FtpRemoteExists.Overwrite, false);
            return status != FtpStatus.Failed;
        }

        public bool UploadAppend(string localPath, string remotePath, int retry = 3)
        {
            var auxstring = remotePath.Split('/');
            var directory = "";
            for (int i = 0; i < auxstring.Length - 1; i++)
                directory += auxstring[i] + '/';

            CreateDirectory(directory);

            client.RetryAttempts = retry;
            var status = client.UploadFile(localPath, remotePath, FtpRemoteExists.Append, false, FtpVerify.Retry);
            return status != FtpStatus.Failed;
        }

        public bool UploadSkip(string localPath, string remotePath, int retry = 3)
        {
            var auxstring = remotePath.Split('/');
            var directory = "";
            for (int i = 0; i < auxstring.Length - 1; i++)
                directory += auxstring[i] + '/';

            CreateDirectory(directory);

            client.RetryAttempts = retry;
            var status = client.UploadFile(localPath, remotePath, FtpRemoteExists.Skip, false, FtpVerify.Retry);
            return status != FtpStatus.Failed;
        }

        public void DownloadFile(string localPath, string remotePath, bool overwrite = true)
        {
            client.DownloadFile(localPath, remotePath, overwrite == true ?  FtpLocalExists.Overwrite : FtpLocalExists.Append);
        }

        public void Download(Stream outStream, string clientPath)
        {
            client.Download(outStream, clientPath);
        }

        public int NumberFiles(string directory)
        {
            int count = 0;
            foreach (FtpListItem item in client.GetListing(directory))
                count = count + 1;

            return count;
        }

        public void ReadFilesDirectory(string directory)
        {
            // get a list of files and directories in the "/htdocs" folder
            foreach (FtpListItem item in client.GetListing(directory))
            {
                // if this is a file
                if (item.Type == FtpFileSystemObjectType.File)
                {
                    // get the file size
                    long size = client.GetFileSize(item.FullName);
                }
                // get modified date/time of the file or folder
                DateTime time = client.GetModifiedTime(item.FullName);
                // calculate a hash for the file on the server side (default algorithm)
                FtpHash hash = client.GetHash(item.FullName);
            }
        }

        public List<FileINFO> ListFilesDirectory(string directory)
        {
            //Gets in the FTP directory and return a list with all the file names sorted by  uploaded date
            List<FileINFO> Dire = new List<FileINFO>();

            FileINFO Data = new FileINFO();
            string a;

            foreach (FtpListItem item in client.GetListing(directory))
            // if this is a file
                if (item.Type == FtpFileSystemObjectType.File)
                {
                    Data.Name = item.Name;
                    a = item.Modified.Year.ToString().PadLeft(4, '0') + item.Modified.Month.ToString().PadLeft(2, '0') + item.Modified.Day.ToString().PadLeft(2, '0') + item.Modified.Hour.ToString().PadLeft(2, '0') + item.Modified.Minute.ToString().PadLeft(2, '0') + item.Modified.Second.ToString().PadLeft(2, '0');
                    ulong.TryParse(a, out ulong fecha);
                    Data.ModDate = fecha;
                    Data.FullName = item.FullName;

                    Dire.Add(Data);
                }

            Dire.Sort((s1, s2) => s1.ModDate.CompareTo(s2.ModDate));

            return Dire;
        }

        public void CreateDirectory(string directory)
        {
            if (!client.DirectoryExists(directory))
                client.CreateDirectory(directory);
        }

        public void RenemaFile(string file, string rename)
        {
            client.Rename(file, rename);
        }

        public void DeleteFile(string file)
        {
            if (client.FileExists(file))
                client.DeleteFile(file);

        }

        public void DeleteDirectory(string directory)
        {
            if (client.DirectoryExists(directory))
                client.DeleteDirectory(directory);
        }

        public bool FileExist(string path)
        {
            return client.FileExists(path);
        }

        public bool DirectoryExists(string path)
        {
            return client.DirectoryExists(path);
        }

        public void Disconnect()
        {
            client.Disconnect();
            client.OnLogEvent -= OnFTPLogEvent;
            client.Dispose();
            client = null;
        }

        public void Dispose()
        {
            Disconnect();
        }
    } 
}
