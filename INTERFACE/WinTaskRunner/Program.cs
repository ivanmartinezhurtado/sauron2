﻿using log4net;
using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using WinTaskRunner.Services;
using WinTaskRunner.Utils;

namespace WinTaskRunner
{
    static class Program
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            InitializeLogging();
            DI.Build();

            Application.ThreadException += Application_ThreadException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(DI.Resolve<MainForm>());
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Exception: " + e.ExceptionObject.ToString(), "Exception CurrentDomain", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show("Exception: " + e.Exception.Message, "Exception Application", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static void InitializeLogging()
        {
            Thread.CurrentThread.Name = "UI";
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Path.Combine(AppProfile.ProgramPath, "Log4net.config")));
            _logger.Info("Starting WinTaskRunner...");
        }
    }
}
