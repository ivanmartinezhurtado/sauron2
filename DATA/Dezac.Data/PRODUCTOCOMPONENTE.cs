namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.PRODUCTOCOMPONENTE")]
    public partial class PRODUCTOCOMPONENTE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMPRODUCTO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VERSIONPRODUCTO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NUMCOMPONENTE { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VERSIONCOMPONENTE { get; set; }

        public decimal CANTIDAD { get; set; }

        [StringLength(50)]
        public string ORDENMONTAJE { get; set; }

        public virtual ARTICULO ARTICULO { get; set; }

        public virtual PRODUCTO PRODUCTO { get; set; }
    }
}
