﻿using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Actions;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.0)]
    public class EKOR_PLUSTest : TestBase
    {        
        private EKOR_PLUS ekorPlus;
        private Tower3 tower;
        private TriLineValue voltageMeasurePasTapas = TriLineValue.Create(1, 2, 4);

        public EKOR_PLUS EkorPlus
        {
            get
            {
                if (ekorPlus == null)
                    ekorPlus = new EKOR_PLUS(Comunicaciones.SerialPort);

                return ekorPlus;
            }
        }

        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        public string CAMERA_IDS { get; set; }
        public string CAMERA_IDS_LED { get; set; }
        public bool utilNuevo { get; set; }

        protected USB5856 usbIO;

        //#region Constants

        //internal const int _24V = 14;
        //// UTIL VIEJO (AJUSTE)
        internal const int PISTONS_REAR = 5;
        internal const int PISTONS_FRONT = 6;
        internal const int IN_LED = 11;

        //// UTIL MODULAR (AJUSTE/VERIFICACION)
        internal const int OUT_PISTON_DOT_TEST = 5;
        internal const int OUT_PISTON_FIJACION = 6;

        //// NUEVO UTIL (VERIFICACION)
        internal const int ANY_BUTTON = 9999;
        internal const int VALVULA_GENERAL_UTIL = 9;
        internal const int VALVULA_DOMO_KEYBOARD = 5;
        internal const int OUT_SET_BUTTON_ACTUATOR = 7;
        internal const int OUT_ESC_BUTTON_ACTUATOR = 8;
        internal const int OUT_DOWN_BUTTON_ACTUATOR = 12;
        internal const int OUT_LEFT_BUTTON_ACTUATOR = 10;
        internal const int OUT_RIGHT_BUTTON_ACTUATOR = 11;
        internal const int OUT_UP_BUTTON_ACTUATOR = 9; //ADVANTECH

        internal const int DETECTION_DOMO_DOWN = 11;
        internal const int DETECTION_DOMO_UP = 14;

        internal const int OUT_FAN = 15;                      // Activacion 230Vac??
        internal const int IN_EKORPLUS_LED = 16;
        internal const int IN_DETECTION_FIJACION_EQUIPO = 21;
        internal const int IN_DETECTION_TRIGGER = 10;
        internal const int OUT_CHANNEL_1 = 28;
        internal const int IN_CHANNEL_1 = 8;
        internal const int OUT_CHANNEL_2 = 29;
        internal const int IN_CHANNEL_2 = 9;
        internal const int OUT_CHANNEL_3 = 30;
        internal const int IN_CHANNEL_3 = 10;

        // SALIDAS DIGITALES NUEVO UTIL
        internal const int IN_DO_1 = 9;
        internal const int OUT_DETECTION_DO_2 = 43;    // Activado para la deteccion relé 2
        internal const int IN_DO_2 = 15;
        internal const int IN_DO_3 = 16;
        internal const int IN_DO_4 = 17;
        internal const int IN_DO_5 = 18;
        internal const int IN_DO_6 = 19;
        internal const int IN_DO_7 = 20;
        internal const int BYPASS = 5;
        internal const int SUPPLY_232_EXT = 0;
        internal const int ACTIVE_VEXT232_TO_IN2_MUX = 1;
        // ENTRADA DIGITALES NUEVO UTIL
        internal const int OUT_DI_1 = 17;
        internal const int OUT_DI_2 = 18;
        internal const int OUT_DI_3 = 19;
        internal const int OUT_DI_4 = 41;
        internal const int OUT_DI_5 = 42;
        internal const int OUT_COMUN_DI = 22;   // Para las entradas de la 6 a la 10, deberá estar activado
        internal const int OUT_DI_6 = 40;
        internal const int OUT_DI_7 = 0;
        internal const int OUT_DI_8 = 1;
        internal const int OUT_DI_9 = 2;
        internal const int OUT_DI_10 = 3;

        internal enum Pistones
        {
            PISTON_PUNTAS = 6,
            PISTON_DB9 = 50,
            PISTON_USB_RPA = 51,
            PISTON_ETH1_RTU = 52,
            PISTON_ETH2_RTU = 53,
            PISTON_SP0_SP3_RS485 = 54,
            PISTON_TRIGGER_PASARELA = 55,
            PISTON_ETH_DTC2 = 56,
            PISTON_RS485_DTC2_EKORPLUS = 57,
            PISTON_PUNTAS_DISPARO_DTC2_EKORPLUS = 58,
            PISTON_FIJACION_EQUIPO = 59,
            PISTON_GENERAL_COM_FRONTAL = 48,
            PISTON_GENERAL_POSTERIOR = 49
        }

        //#endregion

        public virtual void TestInitialization()
        {
            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            Tower.PowerSourceIII.Tolerance = Configuracion.GetDouble("TOLERANCE_MTE", 0.1, ParamUnidad.PorCentage);            

            Tower.ActiveVoltageCircuit();
            Tower.Active24VDC();

            var wmi = new WMISerialPortInitialize();
            wmi.Execute("MetrixPortVirtual", "Silicon Labs");

            EkorPlus.FactorTransformationVoltage = 1;
            logger.InfoFormat("Relacion transfomacio Voltage en el EKOR_PLUS = 1");

            utilNuevo = Model.NumUtilBien == 6534 ? false : true;

            if (utilNuevo)
            {
                usbIO = new USB5856();
                TestConectionDevice();
                usbIO.DO.On(BYPASS);
            }
            else
            {
                Tower.IO.DO.On(4);
                Tower.IO.DO.OnWait(500, PISTONS_REAR);

            }
        }

        public void TestConsumptionBattery()
        {
            var voltage = Consignas.GetDouble(Params.V.Null.TestPoint("PILA").Vnom.Name, 3.6, ParamUnidad.V);
            var limitCurrent = Consignas.GetDouble(Params.I.Null.TestPoint("PILA").Vnom.Name, 0.5, ParamUnidad.A);

            Tower.IO.DO.Off((ushort)Pistones.PISTON_DB9);
            Delay(500, "Esperando extracción DB9");

            Tower.IO.DO.On(20, 21);

            Delay(2000, "Esperando a que el equipo entre en modo Standbye");

            Tower.LAMBDA.ApplyOffAndWaitStabilisation();

            var param = new AdjustValueDef(Params.I_DC.Null.EnVacio.TestPoint("PILA"), ParamUnidad.A);
            TestMeasureBase(param,
            (step) =>
            {
                return Tower.MeasureMultimeter(InputMuxEnum.NOTHING, MagnitudsMultimeter.AmpDC);
            }, 2, 15, 2500);

            Tower.IO.DO.Off(20, 21);

            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V), 0.5);
        }

        public void AssemblyBattery()
        {
            if (utilNuevo)
            {
                TestDeviceDisconnection();

                Shell.MsgBox("Saque el equipo del útil e introduzca la pila y vuelva a introducir el equipo al útil", "ENSAMBLE LA PILA");

                TestConectionDevice();
            }
            else
                Shell.MsgBox("Ensamble la pila al equipo", "ENSAMBLE LA PILA");
        }

        public void TestComunicationsRS232(int porEkorfront9)
        {           
            if (porEkorfront9 == 0)
                porEkorfront9 = Comunicaciones.SerialPort;

            EkorPlus.Cirbus.PortCom = porEkorfront9;
            EkorPlus.Cirbus.BaudRate = 38400;
            EkorPlus.Cirbus.PerifericNumber = 0;

            SamplerWithCancel((p) =>   // Necesario para la gama SMART (no influye en la RCI): el binario se modifica segun las fases del test, por lo tanto, siempre se tendrá que poner en modo DEFECTO cuando se inicie el test antes de comprobar su VER
            {
                EkorPlus.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_DEFAULT", "04b0 012c 003c 003c 003c 003c 00 24 0f 0f 00 00 0000 0001 0258 00 ff 00", ParamUnidad.SinUnidad));
                Delay(500, "");
                EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_DEFAULT", "0105 270f 1A 00 00 00 1964 06 0006 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));
                Delay(500, "");
                return true;
            }, "Error. No se han podido escribir los ajustes de usuario / instalador por defecto en el equipo", 3, 1000, 0, false, false);

            string firmwareVersion = "";
            string developerVersion = "";

            SamplerWithCancel((p) =>
            {
                firmwareVersion = EkorPlus.ReadFirmware();
                developerVersion = EkorPlus.ReadVersion();
                logger.InfoFormat("Puerto de comunicacion RS232 asignado en  COM {0}", porEkorfront9.ToString());
                return true;
            }, "Error. No comunica el equipo en RS232", 3, 500, 1000, false, false);

            Resultado.Set("TEST_RS232", "OK", ParamUnidad.s);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, firmwareVersion.Trim(), Identificacion.VERSION_FIRMWARE.Trim(), Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error en la programación del equipo, la version del binario es incorrecta"), ParamUnidad.SinUnidad);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_DEVELOPER, developerVersion.Trim(), Identificacion.VERSION_DEVELOPER.Trim(), Error().UUT.CONFIGURACION.NO_COINCIDE("Error en la programación del equipo, la version de desarollo es incorrecta"), ParamUnidad.SinUnidad);            
        }

        public void TestConsumptionRS232()
        {
            if (utilNuevo)
            {
                Tower.IO.DO.On((int)Pistones.PISTON_DB9);
                SamplerWithCancel((p) => { return Tower.IO.DI[28]; }, "Error, no se ha detectado el piston DB9 entrar al equipo correctamente", 3, 500, 1000);

                usbIO.DO.OnWait(250, SUPPLY_232_EXT);
                usbIO.DO.On(ACTIVE_VEXT232_TO_IN2_MUX);

                var param = new AdjustValueDef(Params.V_DC.Null.TestPoint("EXT232"), ParamUnidad.V);
                TestMeasureBase(param,
                (step) =>
                {
                    return Tower.MeasureMultimeter(InputMuxEnum.IN2, MagnitudsMultimeter.VoltDC);
                }, 1, 4, 2000);

                usbIO.DO.OffWait(250, SUPPLY_232_EXT);
                usbIO.DO.Off(ACTIVE_VEXT232_TO_IN2_MUX);
            }        
        }

        public virtual void TestComunicationsRS485(int port)
        {
            if (port == 0)
                port = Comunicaciones.SerialPort2;

            EkorPlus.Cirbus.PortCom = port;
            EkorPlus.Cirbus.BaudRate = 38400;
            EkorPlus.Cirbus.PerifericNumber = 0;


            SamplerWithCancel((p) =>
            {
                EkorPlus.ReadFirmware();
                logger.InfoFormat("Puerto de comunicacion RS485 asignado en  COM {0}", port.ToString());
                return true;
            }, "Error. No comunica el equipo en RS485", 3, 500, 1000, false, false);

            Resultado.Set("TEST_RS485", "OK", ParamUnidad.s);

        }

        //***********************************************************

        public virtual void TestDefaultSetup()
        {
            if (utilNuevo)
            {
                Tower.IO.DO.OnWait(1000, (byte)Pistones.PISTON_DB9);
                SamplerWithCancel((p) => { return Tower.IO.DI[28]; }, "Error, no se ha detectado el piston DB9 entrar al equipo correctamente", 3, 500, 1000);
            }
            else
                Tower.IO.DO.OnWait(200, PISTONS_FRONT);

            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;
            bool isSmart = Configuracion.GetString("VERSION_SMART", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            if (hasAdjust)
            {
                EkorPlus.WriteDate(DateTime.Now);

                EkorPlus.WriteFrameNumber(TestInfo.NumBastidor.ToString());

                EkorPlus.WriteRemoveCounter();

                EkorPlus.WriteAngleCalibrationInitialization();

                // Implementar Factores de calibracion por defecto, en lugar de resetearlos!!

                EkorPlus.WriteVoltageFactorCalibration();             // Reseteamos factores calibración de voltaje, ya que en caso de pasar una segunda vez un equipo que ha llegado a la fase de calibracion, estos se quedan grabados, y en las comprobaciones nos daria error

                EkorPlus.WriteCurrentFactorCalibration();             // Reseteamos factores calibración de intensidad

                Delay(2000, "Espera");

                SamplerWithCancel((p) =>
                {
                    var info = EkorPlus.ReadFLASHInformation();

                    if (info.Contains("ID"))
                        return true;
                    return false;

                }, "Error. No se ha podido formatear la unidad logica FLASH", 10, 1000, 0);

                Resultado.Set("FLASH_FORMAT", "OK", ParamUnidad.SinUnidad);

                EkorPlus.WritePasatapas(1099930, 1099930, 1099930);
                Delay(200, "Escritura factor Pasatapas en modo CAL");
            }

            if (isSmart)
            {
                SamplerWithCancel((p) =>
                {
                    if (p == 3)
                    {
                        Tower.IO.DO.PulseOff(3000, (ushort)Pistones.PISTON_DB9);
                        Delay(3000, "");
                    }
                    SendConfigUserXML();
                    return true;
                }, "Error enviando el fichero XML de configuración de usuario", 6, 10000, 5000, false, false);

                //Se le vuelven a escribir los ajustes de defecto, para cambiar el modo de funcionamiento del equipo (/300 = 1A). En este momento, el equipo cambia su versión de binario VER
                //No le escribimos los ajustes de usuario debido a que entraria en conflicto con el fichero XML anterior
                EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("AJUSTES_DEFECTO_SMART", "0105 270f 1A 00 00 00 1964 06 0006 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));
                Delay(500, "Escribiendo ajustes de calibración gama smart");
                //Comprobamos nueva version del VER
                var versionVERSmart = EkorPlus.ReadFirmware();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_2, versionVERSmart.Trim(), Identificacion.VERSION_FIRMWARE_2.Trim(),
                Error().PROCESO.PARAMETROS_ERROR.BINARIOS("Error en la programación del equipo, la version del binario de la version SMART es incorrecta"), ParamUnidad.SinUnidad);
            }
        }

        //***********************************************************

        public void TestKeyboard()
        {
            foreach (KeyValuePair<int, EKOR_PLUS.InputReadings> keyToDeactivate in keyboardActuatorsDictionary) //resetear todas teclas
                if (keyToDeactivate.Key != ANY_BUTTON)
                {
                    if (keyToDeactivate.Key == OUT_UP_BUTTON_ACTUATOR)
                        usbIO.DO.Off((int)keyToDeactivate.Key);
                    else
                        tower.IO.DO.Off((int)keyToDeactivate.Key);
                }

            Delay(100, "Esperando retirada de los pistones");

            tower.IO.DO.On(VALVULA_DOMO_KEYBOARD);
            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[DETECTION_DOMO_UP];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            foreach (KeyValuePair<int, EKOR_PLUS.InputReadings> keyToRead in keyboardActuatorsDictionary) //Se comprueva cada una de las teclas
            {
                EkorPlus.WriteClearKeyboard();

                Delay(100, "Espera limpiado operación lectura teclado");

                if (keyToRead.Key != ANY_BUTTON)
                {
                    if (keyToRead.Key == OUT_UP_BUTTON_ACTUATOR)
                        usbIO.DO.OnWait(500, (int)keyToRead.Key);
                    else
                        tower.IO.DO.OnWait(500, (int)keyToRead.Key);
                }

                SamplerWithCancel((s) =>
                {
                    return EkorPlus.ReadKeyboard() == keyToRead.Value;

                }, string.Format("Error al verificar el estado del teclado, no se detecta la {0}", keyToRead.Value.GetDescription()), 70, 100);

                Resultado.Set(string.Format("{0}", keyToRead.Value.ToString()), "OK", ParamUnidad.SinUnidad);


                if (keyToRead.Key != ANY_BUTTON)
                {
                    if (keyToRead.Key == OUT_UP_BUTTON_ACTUATOR)
                        usbIO.DO.Off((int)keyToRead.Key);
                    else
                        tower.IO.DO.Off((int)keyToRead.Key);
                }
            }
         
            tower.IO.DO.Off(VALVULA_DOMO_KEYBOARD);
            SamplerWithCancel((p) =>
            {
                return Tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());
        }

        public void TestDisplay()
        {
            if (utilNuevo)
            {
                if (Model.IdFase == "120")
                    this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "EKOR_PLUS_DISPLAY_ALL", "DISPLAY_ALL", "EKOR_PLUS", 3, (s) => { EkorPlus.ReadyDisplay(); Delay(500, "Enviando Trama"); });
            }
            else
            {
                EkorPlus.ReadyDisplay();
                Delay(1500, "Esperando display");

                var frm = this.ShowForm(() => new ImageView("TEST DISPLAY", this.PathCameraImages + "\\EKORPLUS_55\\TEST_DISPLAY.jpg"), MessageBoxButtons.OKCancel);
                try
                {
                    SamplerWithCancel((p) =>
                    {
                        EkorPlus.ReadyDisplay();
                        return frm.DialogResult == DialogResult.OK;
                    }, "Error display incorrecto", 50, 1000, 100);
                }
                finally
                {
                    this.CloseForm(frm);
                }

                Resultado.Set("DISPLAY", "OK", ParamUnidad.SinUnidad);

                SamplerWithCancel((p) =>
                {
                    return Tower.IO.DI[IN_LED];
                }, "Error, no se ha detectado el LED encendido", 5, 500, 500);

                Resultado.Set("LED", "OK", ParamUnidad.SinUnidad);
            }
        }

        public void TestLeds()
        {
            CAMERA_IDS_LED = GetVariable<string>("CAMARA_IDS_LED", CAMERA_IDS_LED);
            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS_LED, "EKOR_PLUS_LED", "LED", "EKOR_PLUS");
        }

        public void TestCover()
        {
            var caratula = Identificacion.CARATULA;
            if (caratula != "NO")
                this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "EKOR_PLUS_CARATULA_" + caratula, "CARATULA_" + caratula, "EKOR_PLUS");
        }

        public virtual void TestInputs()
        {
            var numEntradas = (int)Configuracion.GetDouble("NUMERO_ENTRADAS", 8, ParamUnidad.SinUnidad);

            var inputsValue = new List<KeyValuePair<byte, EKOR_PLUS.enumInputs>>();
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_1, EKOR_PLUS.enumInputs.IN1));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_2, EKOR_PLUS.enumInputs.IN2));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_3, EKOR_PLUS.enumInputs.IN3));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_4, EKOR_PLUS.enumInputs.IN4));
            inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_5, EKOR_PLUS.enumInputs.IN5));

            switch (numEntradas)
            {
                case 8:
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_6, EKOR_PLUS.enumInputs.IN6));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_7, EKOR_PLUS.enumInputs.IN7));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_8, EKOR_PLUS.enumInputs.IN8));
                    break;
                case 10:
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_6, EKOR_PLUS.enumInputs.IN6));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_7, EKOR_PLUS.enumInputs.IN7));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_8, EKOR_PLUS.enumInputs.IN8));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_9, EKOR_PLUS.enumInputs.IN9));
                    inputsValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumInputs>(OUT_DI_10, EKOR_PLUS.enumInputs.IN10));
                    break;
            }
            
            SamplerWithCancel((p) =>
            {
                var defaultValue = (EKOR_PLUS.enumInputs)EkorPlus.ReadInputs();

                if (defaultValue == EKOR_PLUS.enumInputs.NOTHING)
                    return true;
                else
                    throw new Exception(string.Format("Error. Se ha detectado la entrada {0} activada cuando debería estar desactivada", defaultValue.ToString()));

            }, string.Empty, 5, 500, 200, false, false);

            foreach (KeyValuePair<byte, EKOR_PLUS.enumInputs> input in inputsValue)
            {
                this.Logger.InfoFormat("Test entrada {0}", input.Value.ToString());

                if (input.Key == OUT_DI_6 || input.Key == OUT_DI_7 || input.Key == OUT_DI_8 || input.Key == OUT_DI_9 || input.Key == OUT_DI_10)
                    tower.IO.DO.On(OUT_COMUN_DI);

                tower.IO.DO.On(input.Key);

                SamplerWithCancel((p) =>
                {
                    var value = (EKOR_PLUS.enumInputs)EkorPlus.ReadInputs();

                    return value == input.Value;

                }, string.Format("Error. No se detecta la {0} activada", input.Value.GetDescription(), 3, 500, 200, false, false));

                tower.IO.DO.Off(input.Key);

                SamplerWithCancel((p) =>
                {
                    var value = (EKOR_PLUS.enumInputs)EkorPlus.ReadInputs();

                    return value == EKOR_PLUS.enumInputs.NOTHING;

                }, string.Format("Error. Se ha detectado la entrada {0} activada cuando debería estar desactivada", input.Value.ToString()), 3, 500, 200, false, false);

                Resultado.Set(string.Format("{0}", input.Value.ToString()), "OK", ParamUnidad.SinUnidad);

                if (input.Key == OUT_DI_6 || input.Key == OUT_DI_7 || input.Key == OUT_DI_8 || input.Key == OUT_DI_9 || input.Key == OUT_DI_10)
                    tower.IO.DO.Off(OUT_COMUN_DI);
            }
        }

        public virtual void TestOutputs()
        {
            var numSalidas = Convert.ToInt32(Configuracion.GetString("NUMERO_SALIDAS", "4", ParamUnidad.SinUnidad));
            
            var outputValue = new List<KeyValuePair<byte, EKOR_PLUS.enumOutputs>>();
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_1, EKOR_PLUS.enumOutputs.OUT1));
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_2, EKOR_PLUS.enumOutputs.OUT2));
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_3, EKOR_PLUS.enumOutputs.OUT3));
            outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_4, EKOR_PLUS.enumOutputs.OUT4));

            if (numSalidas == 7)
            {
                outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_5, EKOR_PLUS.enumOutputs.OUT5));
                outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_6, EKOR_PLUS.enumOutputs.OUT6));
                outputValue.Add(new KeyValuePair<byte, EKOR_PLUS.enumOutputs>(IN_DO_7, EKOR_PLUS.enumOutputs.OUT7));
            }

            foreach (KeyValuePair<byte, EKOR_PLUS.enumOutputs> output in outputValue)
            {
                this.Logger.InfoFormat("{0}", output.Value.ToString());

                bool readOutInverted = false;
                if (output.Value == EKOR_PLUS.enumOutputs.OUT2)
                {
                    Tower.IO.DO.On(OUT_DETECTION_DO_2);
                    readOutInverted = Configuracion.GetString("OUTPUT_2_INVERTED", "NO", ParamUnidad.SinUnidad) == "SI" ? true : false;
                }

                EkorPlus.WriteOutput(output.Value);

                SamplerWithCancel((p) =>
                {
                    if (output.Value == EKOR_PLUS.enumOutputs.OUT2)
                        if (readOutInverted)
                            return !Tower.IO.DI[output.Key];
                        else
                            return Tower.IO.DI[output.Key];
                    else
                        return Tower.IO.DI[output.Key];

                }, string.Format("Error. No se detectado la salida {0} activada", output.Value.ToString()), 10, 100, 200);

                EkorPlus.WriteOutput(EKOR_PLUS.enumOutputs.NOTHING);

                SamplerWithCancel((p) =>
                {
                    if (output.Value == EKOR_PLUS.enumOutputs.OUT2)
                        if (readOutInverted)
                            return Tower.IO.DI[output.Key];
                        else
                            return !Tower.IO.DI[output.Key];
                    else
                        return !Tower.IO.DI[output.Key];

                }, string.Format("Error. Se ha detectado la salida {0} activada cuando debería estar desactivada", output.Value.ToString()), 10, 100, 200);

                if (output.Value == EKOR_PLUS.enumOutputs.OUT2)
                    Tower.IO.DO.Off(OUT_DETECTION_DO_2);

                Resultado.Set(string.Format("{0}", output.Value.ToString()), "OK", ParamUnidad.SinUnidad);
            }
        }

        public virtual void TestVUSB(string nameDevicePortVirtual)
        {
            if (utilNuevo)
            {
                Tower.IO.DO.On((int)Pistones.PISTON_DB9);
                SamplerWithCancel((p) => { return Tower.IO.DI[28]; }, "Error, no se ha detectado el piston DB9 entrar al equipo correctamente", 3, 500, 1000);
            }
            else
                Tower.IO.DO.OnWait(200, PISTONS_FRONT);

            SamplerWithCancel((p) =>
            {
                if (p > 0 && p % 10 == 0)
                    MessageBox.Show("Desconecte el USB del Ekor, espere unos segundos y vuelva a conectarlo", "TEST USB", MessageBoxButtons.OK);

                foreach (DriveInfo drive in DriveInfo.GetDrives().Where((t) => t.IsReady == true && t.DriveType == DriveType.Removable))
                    if (drive.VolumeLabel.ToUpper() == Configuracion.GetString("USB_DISK_NAME", "EKORPLUS", ParamUnidad.SinUnidad).Trim())
                        return true;
                return false;
            }, "Error No se ha encontrado el dispositivo USB del EKOR", 25, 2500, 4000);

            Resultado.Set("TEST_USB_DRIVE", "OK", ParamUnidad.SinUnidad);

            if (Configuracion.GetString("TEST_USB_COMUNICATION", "NO", ParamUnidad.SinUnidad).Trim() == "SI")
            {
                var portEkor = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));

                var portSerial = EkorPlus.Cirbus.PortCom;
                EkorPlus.Cirbus.ClosePort();
                EkorPlus.Cirbus.PortCom = portEkor;

                SamplerWithCancel((p) =>
                {
                    EkorPlus.ReadFirmware();
                    logger.InfoFormat("Puerto de comunicacion USB virtual asignado en  COM {0}", portEkor.ToString());
                    return true;
                }, "Error. No comunica el equipo", 3, 500, 1000, false, false);           

                EkorPlus.Cirbus.ClosePort();
                EkorPlus.Cirbus.PortCom = portSerial;

                Resultado.Set("TEST_USB_SERIALPORT", "OK", ParamUnidad.SinUnidad);
            }
        }

        //*****************************************************

        public void TestVacio()
        {
            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

            TriAdjustValueDef lineMeasurements = new TriAdjustValueDef(Params.V.L1.EnVacio.Name, Params.V.Null.EnVacio.Min(), Params.V.Null.EnVacio.Max(), 0, 0, ParamUnidad.V);
            TestMeasureBase(lineMeasurements,
                (step) =>
                {
                    var result = EkorPlus.ReadMeasurementsVolageDisplay();
                    var VoltageList = new List<double>();
                    VoltageList.Add(result.L1);
                    VoltageList.Add(result.L2);
                    VoltageList.Add(result.L3);

                    return VoltageList.ToArray();
                }, 2, 5, 500);

            AdjustValueDef lineNeutro = new AdjustValueDef(Params.V.LN.EnVacio.Name, 0,  Params.V.LN.EnVacio.Min(), Params.V.LN.EnVacio.Max(), 0, 0, ParamUnidad.V);
            TestMeasureBase(lineNeutro,
                (step) =>
                {
                    var result = EkorPlus.ReadMeasurementsVolageDisplay();
                    return result.LH;
                }, 2, 5, 500);

            TriAdjustValueDef lineMeasurements2 = new TriAdjustValueDef(Params.I_AC.L1.EnVacio.Name, Params.I_AC.Null.EnVacio.Min(), Params.I_AC.Null.EnVacio.Max(), 0, 0, ParamUnidad.A, true);

            TestMeasureBase(lineMeasurements2,
                (step) =>
                {
                    var result = EkorPlus.ReadMeasurementsCurrentDisplay();
                    var CurrentList = new List<double>();
                    CurrentList.Add(result.L1);
                    CurrentList.Add(result.L2);
                    CurrentList.Add(result.L3);
                    CurrentList.Add(result.LH);
                    return CurrentList.ToArray();

                }, 2, 5, 500);
        }

        public void TestShortCircuit()
        {
            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            if (hasAdjust)
            {
                var factorCalibrationCurrent = new EKOR_PLUS.FactorCalibrationCurrent { FCL1E1 = 6175, FCL2E1 = 6177, FCL3E1 = 6178, FCL1E2 = 143600, FCL2E2 = 143650, FCL3E2 = 143660, FCL1E3 = 3908786, FCL2E3 = 3910990, FCL3E3 = 3907217 };
                var factorCalibrationVoltage = new EKOR_PLUS.CalibrationVoltageStruct { FCL1E1 = 11000, FCL2E1 = 11165, FCL3E1 = 11023, FCL1E2 = 555218, FCL2E2 = 563877, FCL3E2 = 556692 };
                EkorPlus.WriteCurrentFactorCalibration();
                EkorPlus.WriteVoltageFactorCalibration();
            }

            var V_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 5, ParamUnidad.V);
            var V_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 10, ParamUnidad.V);
            var V_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 15, ParamUnidad.V);

            var I_L1 = Consignas.GetDouble(Params.I.L1.CrucePistas.Name, 1, ParamUnidad.A);
            var I_L2 = Consignas.GetDouble(Params.I.L2.CrucePistas.Name, 1.5, ParamUnidad.A);
            var I_L3 = Consignas.GetDouble(Params.I.L3.CrucePistas.Name, 2, ParamUnidad.A);

            var ConsignasV = new TriLineValue { L1 = V_L1, L2 = V_L2, L3 = V_L3 };
            var ConsignasI = new TriLineValue { L1 = I_L1, L2 = I_L2, L3 = I_L3 };

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(ConsignasV, ConsignasI, 50, 0, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 });

            TriAdjustValueDef lineMeasurements = new TriAdjustValueDef(Params.V.L1.CrucePistas.Name, Params.V.Null.CrucePistas.Min(), Params.V.Null.CrucePistas.Max(), 0, 0, ParamUnidad.V, true);
            TestMeasureBase(lineMeasurements,
                (step) =>
                {
                    //var result = EkorPlus.ReadMeasurementsVolageDisplay();
                    var result = EkorPlus.ReadMeasurementsVoltageDisplayInVolts();
                    var VoltageList = new List<double>();
                    VoltageList.Add(result.L1);
                    VoltageList.Add(result.L2);
                    VoltageList.Add(result.L3);

                    return VoltageList.ToArray();
                }, 2, 5, 500);

            TriAdjustValueDef lineMeasurements2 = new TriAdjustValueDef(Params.I_AC.L1.CrucePistas.Name, Params.I_AC.Null.CrucePistas.Min(), Params.I_AC.Null.CrucePistas.Max(), 0, 0, ParamUnidad.A, true);

            TestMeasureBase(lineMeasurements2,
                (step) =>
                {
                    var result = EkorPlus.ReadMeasurementsCurrentDisplay();
                    var CurrentList = new List<double>();
                    CurrentList.Add(result.L1);
                    CurrentList.Add(result.L2);
                    CurrentList.Add(result.L3);
                    CurrentList.Add(result.LH);
                    return CurrentList.ToArray();

                }, 2, 5, 500);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        //******************************************************************************

        public virtual void TestCalibrationVoltageCurrents([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            // Calibración de V e I
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();
            var model = EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO;

            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc).Name, consigna_A, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            var gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, frecuency, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            var defs = new List<TriAdjustValueDef>();
            defs.Add(new TriAdjustValueDef(Params.GAIN_I.L1.Ajuste.TestPoint(scaleDesc).Name, Params.GAIN_I.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_I.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false));
            if (scale == EKOR_PLUS.Scale.Gruesa)
            {

                voltageMeasurePasTapas = MeasureMetrix();   
                defs.Add(new TriAdjustValueDef(Params.GAIN_V.L1.Ajuste.TestPoint(scaleDesc).Name, Params.GAIN_V.Null.Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_V.Null.Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos, false, false));
            }
                
            TestMeasureBase(defs,
            (step) =>
            {
                var rmsMeasure = EkorPlus.CalculateFactorCalibrationByScala(model, scale, current, voltage, false, 1, numMuestras, numMuestrasTotal, 1100, voltageMeasurePasTapas);
                return rmsMeasure.ToArray();
            }, 0, 2, 1500);

            EkorPlus.WriteCurrentFactorCalibration();

            if (scale == EKOR_PLUS.Scale.Gruesa)
                EkorPlus.WriteVoltageFactorCalibration();

            Delay(3000, "Espera grabacion factores");

            var factorCurrent = EkorPlus.ReadCurrentFactorCalibration();
            Assert.AreEqual(factorCurrent.TramaEFC, EkorPlus.FactoresCalibracionCorriente.TramaEFC, Error().UUT.MEDIDA.NO_COINCIDE("Error los factores de corriente grabados no corresponden con los leidos"));

            var factorVoltage = EkorPlus.ReadVoltageFactorCalibration();
            Assert.AreEqual(factorVoltage.TramaEFV, EkorPlus.FactoresCalibracionVoltage.TramaEFV, Error().UUT.MEDIDA.NO_COINCIDE("Error los factores de voltage grabados no corresponden con los leidos"));

            // Verificacion de la calibración de V e I y Desfase
            //****************************************************
            //****************************************************
            TestVerificationVoltageCurrents(scale);
        }

        public void TestVerificationVoltageCurrents([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            var voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc).Name, consigna_A, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            var gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            bool isSmart = Configuracion.GetString("VERSION_SMART", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;
            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            if (!hasAdjust)
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, current, frecuency, gap, new TriLineValue { L1 = 0, L2 = 120, L3 = 240 }, true);

            var defsVerif = new List<TriAdjustValueDef>();
            defsVerif.Add(new TriAdjustValueDef(Params.I.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, current, Params.I.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.A, false, false));
            defsVerif.Add(new TriAdjustValueDef(Params.ANGLE_GAP.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, gap, Params.ANGLE_GAP.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.Grados, false, false));

            TestCalibracionBase(defsVerif,
            () =>
            {
                var vars = EkorPlus.ReadMeasurementsCurrentDisplay();
                logger.DebugFormat("LPU Current L1: {0} L2: {1} L3: {2} LH: {3}", vars.L1, vars.L2, vars.L3, vars.LH);
                var result = new List<double>() { vars.L1, vars.L2, vars.L3 };

                var varsGap = EkorPlus.ReadMeasurementsAnglePhase();
                var gapL1 = isSmart ? varsGap.L1.Angle : varsGap.L1.Angle_EkorPlus;
                var gapL2 = isSmart ? varsGap.L2.Angle : varsGap.L2.Angle_EkorPlus;
                var gapL3 = isSmart ? varsGap.L3.Angle : varsGap.L3.Angle_EkorPlus;
                logger.DebugFormat("GFx Angulo L1: {0} L2: {1} L3: {2} ", gapL1, gapL2, gapL3);

                result.Add(gapL1, gapL2, gapL3);

                return result.ToArray();
            },
            () =>
            {
                var result = new List<double>() { current, current, current };
                result.Add(gap);
                result.Add(gap);
                result.Add(gap);
                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);

            if (scale == EKOR_PLUS.Scale.Gruesa)
            {
                var defsVL1 = new AdjustValueDef(Params.V.L1.Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, voltageMeasurePasTapas.L1, Params.V.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.V);
                var defsVL2 = new AdjustValueDef(Params.V.L2.Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, voltageMeasurePasTapas.L2, Params.V.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.V);
                var defsVL3 = new AdjustValueDef(Params.V.L3.Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, voltageMeasurePasTapas.L3, Params.V.Null.Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.V);

                

                using (var mtx = new MTX3293())
                {
                    string nameDevicePortVirtual = "MetrixPortVirtual";
                    var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
                    mtx.PortCom = portMetrix;

                    usbIO.DO.On(4);

                    TestCalibracionBase(defsVL1,
                    () =>
                    {
                        CheckCancellationRequested();
                        var vars2 = EkorPlus.ReadMeasurementsVoltageDisplayInVolts();
                        logger.DebugFormat("RMS Voltage L1: {0}", vars2.L1);
                        var varsVoltagePoints = EkorPlus.ReadMeasurementsVolageDisplay();
                        logger.DebugFormat("Points Voltage L1: {0}", varsVoltagePoints.L1);
                        return vars2.L1;
                    },
                    () =>
                    {
                        var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC) * 10;
                        logger.InfoFormat("METRIX -> VL1 = {0}", vl);
                        return vl;
                    }, numMuestras, numMuestrasTotal, 1100);

                    usbIO.DO.Off(4);

                    usbIO.DO.On(3);

                    TestCalibracionBase(defsVL2,
                      () =>
                      {
                          CheckCancellationRequested();
                          var vars2 = EkorPlus.ReadMeasurementsVoltageDisplayInVolts();
                          logger.DebugFormat("RMS Voltage L2: {0}", vars2.L2);
                          var varsVoltagePoints = EkorPlus.ReadMeasurementsVolageDisplay();
                          logger.DebugFormat("Points Voltage L2: {0}", varsVoltagePoints.L2);
                          return vars2.L2;
                      },
                      () =>
                      {
                          var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC) * 10;
                          logger.InfoFormat("METRIX -> VL2 = {0}", vl);
                          return vl;
                      }, numMuestras, numMuestrasTotal, 1100);

                    usbIO.DO.Off(3);

                    usbIO.DO.On(2);
                    TestCalibracionBase(defsVL3,
                    () =>
                    {
                        CheckCancellationRequested();
                        var vars2 = EkorPlus.ReadMeasurementsVoltageDisplayInVolts();
                        logger.DebugFormat("RMS Voltage L3: {0}", vars2.L3);
                        var varsVoltagePoints = EkorPlus.ReadMeasurementsVolageDisplay();
                        logger.DebugFormat("Points Voltage L3: {0}", varsVoltagePoints.L3);
                        return vars2.L3;
                    },
                    () =>
                    {
                        var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC) * 10;
                        logger.InfoFormat("METRIX -> VL3 = {0}", vl);
                        return vl;
                    }, numMuestras, numMuestrasTotal, 1100);

                    usbIO.DO.Off(2);
                }
            }
        }

        public virtual void TestCalibrationCurrentHomopolar([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            // Calibración de Ih
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();

            var measureMetrix = 0.000;
            var model = EKOR_PLUS.EkorPlusFamilyCalibration.CAPACITIVO;

            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc + "_HOMOPOLAR").Name, consigna_A, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);

            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_HOMOPOLAR", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;
           
            tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, new TriLineValue { L1 = current, L2 = 0, L3 = 0 }, frecuency, gap, 0, true);

            var defs = new AdjustValueDef(Params.GAIN_I.Other("LH").Ajuste.TestPoint(scaleDesc).Name, 0, Params.GAIN_I.Other("LH").Ajuste.TestPoint(scaleDesc).Min(), Params.GAIN_I.Other("LH").Ajuste.TestPoint(scaleDesc).Max(), 0, 0, ParamUnidad.Puntos);
            TestMeasureBase(defs,
            (step) =>
            {
                var rmsMeasure = EkorPlus.CalculateFactorCalibrationByScala(model, scale, current, voltage, true, 1, numMuestras, numMuestrasTotal, 1100, measureMetrix);
                return rmsMeasure.FirstOrDefault(); ;
            }, 0, 2, 1500);

            EkorPlus.WriteCurrentFactorCalibration();

            Delay(3000, "Espera grabacion factores");

            // Verificacion de la calibración de V e I            
            TestVerificationCurrentHomopolar(scale);
        }

        public void TestVerificationCurrentHomopolar([Description("Fina=0, Gruesa=1, Cortocircuito=2")]EKOR_PLUS.Scale scale, double consigna_A = 0)
        {
            string scaleDesc = "ESC_" + scale.ToString().ToUpper();
      
            double voltage = Consignas.GetDouble(Params.V.Null.Ajuste.Name, 1, ParamUnidad.V);
            double current = Consignas.GetDouble(Params.I.Null.Ajuste.TestPoint(scaleDesc + "_HOMOPOLAR").Name, consigna_A, ParamUnidad.A);
            var frecuency = Consignas.GetDouble(Params.FREQ.Null.Ajuste.Name, 0, ParamUnidad.Hz);
            double gap = Consignas.GetDouble(Params.ANGLE_GAP.Null.Ajuste.Name, 45, ParamUnidad.Grados);
            int numMuestras = (int)Configuracion.GetDouble("NUM_MUESTRAS_PHASES", 5, ParamUnidad.SinUnidad);
            var numMuestrasTotal = numMuestras + 3;

            var hasAdjust = Configuracion.GetString("HAS_ADJUST", "SI", ParamUnidad.SinUnidad) == "SI" ? true : false;

            if (!hasAdjust)
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(voltage, new TriLineValue { L1 = current, L2 = 0, L3 = 0 }, frecuency, gap, 0, true);

            var defsVerif = new List<AdjustValueDef>();
            defsVerif.Add(new AdjustValueDef(Params.I.Other("LH").Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, current, Params.I.Other("LH").Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.A));
            defsVerif.Add(new AdjustValueDef(Params.ANGLE_GAP.Other("LH").Verificacion.TestPoint(scaleDesc).Name, 0, 0, 0, gap, Params.ANGLE_GAP.Other("LH").Verificacion.TestPoint(scaleDesc).Tol(), ParamUnidad.Grados));

            TestCalibracionBase(defsVerif,
            () =>
            {
                var vars = EkorPlus.ReadMeasurementsCurrentDisplay();
                logger.DebugFormat("LPU Current L1: {0} L2: {1} L3: {2} LH: {3}", vars.L1, vars.L2, vars.L3, vars.LH);

                var varsGap = EkorPlus.ReadMeasurementsAnglePhase();
                logger.DebugFormat("GFx Angulo L1: {0} L2: {1} L3: {2} ", varsGap.L1.Angle, varsGap.L2.Angle, varsGap.L3.Angle);

                var varsGapHomo = EkorPlus.ReadMeasurementsAnglePhaseHomopolar();
                logger.DebugFormat("GFI Angulo LH: {0}", varsGapHomo.Angle);

                var result = new List<double>() { vars.LH, varsGapHomo.Angle };
                return result.ToArray();
            },
            () =>
            {
                var result = new List<double>() { current, gap };
                return result.ToArray();
            }, numMuestras, numMuestrasTotal, 1100);
        }

        public void TestDireccionalidad()
        {                                                                                                        
            EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_DIRECCIONALIDAD", "0105 270f 1A 00 00 00 1964 06 0004 0096 0003 ffa6 00 01 00", ParamUnidad.SinUnidad));

            var voltage = Consignas.GetDouble(Params.V.Null.TestPoint("DIRECCIONALIDAD").Name, 100, ParamUnidad.V);
            var current = Consignas.GetDouble(Params.I.Null.TestPoint("DIRECCIONALIDAD").Name, 1, ParamUnidad.A);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(voltage, 0, 0), TriLineValue.Create(current, 0, 0 ), 50, TriLineValue.Create(0, 0, 0), TriLineValue.Create(0, 0, 0), true);
            Tower.IO.DO.On(27);
                        
            SamplerWithCancel(
                (p) =>
                {
                    var direccPhase = EkorPlus.ReadDirectionalPhase();
                    Assert.AreBetween("DIRECCIONALIDAD_L1", direccPhase.angleLH, Params.PHASE.Null.Offset.Min(), Params.PHASE.Null.Offset.Max(), Error().UUT.MEDIDA.MARGENES("Error. La medida del Angulo direccional de la linea 1 fuera de margenes"), ParamUnidad.Grados);                    
                    return true;
                }, string.Empty, 3, 500, 1000, false, false);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(0, voltage, 0), TriLineValue.Create(current, 0, 0), 50, TriLineValue.Create(0, 0, 0), TriLineValue.Create(0, 0, 0), true);

            SamplerWithCancel(
                (p) =>
                {
                    var direccPhase = EkorPlus.ReadDirectionalPhase();
                    Assert.AreBetween("DIRECCIONALIDAD_L2", direccPhase.angleLH, Params.PHASE.Null.Offset.Min(), Params.PHASE.Null.Offset.Max(), Error().UUT.MEDIDA.MARGENES("Error. La medida del Angulo direccional de la linea 1 fuera de margenes"), ParamUnidad.Grados);
                    return true;
                }, string.Empty, 3, 500, 1000, false, false);

            Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(TriLineValue.Create(0, 0, voltage), TriLineValue.Create(current, 0, 0), 50, TriLineValue.Create(0, 0, 0), TriLineValue.Create(0, 0, 0), true);

            SamplerWithCancel(
                (p) =>
                {
                    var direccPhase = EkorPlus.ReadDirectionalPhase();
                    Assert.AreBetween("DIRECCIONALIDAD_L3", direccPhase.angleLH, Params.PHASE.Null.Offset.Min(), Params.PHASE.Null.Offset.Max(), Error().UUT.MEDIDA.MARGENES("Error. La medida del Angulo direccional de la linea 1 fuera de margenes"), ParamUnidad.Grados);
                    return true;
                }, string.Empty, 3, 500, 1000, false, false);

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

        }

        public TriLineValue MeasureMetrix()
        {
            var defsL1 = new AdjustValueDef(Params.V.Other("L1").TESTER.Name, 0, Params.V.Other("L1").TESTER.Min(), Params.V.Other("L1").TESTER.Max(), 0, 0, ParamUnidad.V);
            var defsL2 = new AdjustValueDef(Params.V.Other("L2").TESTER.Name, 0, Params.V.Other("L2").TESTER.Min(), Params.V.Other("L2").TESTER.Max(), 0, 0, ParamUnidad.V);
            var defsL3 = new AdjustValueDef(Params.V.Other("L3").TESTER.Name, 0, Params.V.Other("L3").TESTER.Min(), Params.V.Other("L3").TESTER.Max(), 0, 0, ParamUnidad.V);

            using (var mtx = new MTX3293())
            {
                string nameDevicePortVirtual = "MetrixPortVirtual";
                var portMetrix = Convert.ToByte(GetVariable<string>(nameDevicePortVirtual, nameDevicePortVirtual).Replace("COM", ""));
                mtx.PortCom = portMetrix;

                usbIO.DO.On(4);

                TestMeasureBase(defsL1,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC);
                    logger.InfoFormat("METRIX -> VL1 = {0}", vl);
                    return vl;
                }, 1, 3, 1000, true);

                usbIO.DO.Off(4);
                logger.InfoFormat("Resultado Medida METRIX L1 {0}", defsL1.ToString());
                usbIO.DO.On(3);

                TestMeasureBase(defsL2,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC);
                    logger.InfoFormat("METRIX -> VL2 = {0}", vl);
                    return vl;
                }, 1, 3, 1000, true);

                usbIO.DO.Off(3);
                logger.InfoFormat("Resultado Medida METRIX L2 {0}", defsL2.ToString());
                usbIO.DO.On(2);

                TestMeasureBase(defsL3,
                (step) =>
                {
                    CheckCancellationRequested();
                    var vl = mtx.ReadVoltage(MTX3293.measureTypes.AC);
                    logger.InfoFormat("METRIX -> VL3 = {0}", vl);
                    return vl;
                }, 1, 3, 1000, true);

                usbIO.DO.Off(2);
                logger.InfoFormat("Resultado Medida METRIX L3 {0}", defsL3.ToString());

               return TriLineValue.Create(defsL1.Value * 10, defsL2.Value * 10, defsL3.Value *10);
            }
        }

    
        //************************************************************************************

        public void TestSerialPortPPP()
        {
            var periferic = EkorPlus.Cirbus.PerifericNumber;
            EkorPlus.Cirbus.PerifericNumber = 99;
            SamplerWithCancel((p) =>
            {
                EkorPlus.ReadIPPasarelaPPP();
                return true;
            }, string.Empty, 25, 1000, 0, false, false);

            EkorPlus.Cirbus.PerifericNumber = periferic;
            Resultado.Set("SERIAL_PORT_PPP", "OK", ParamUnidad.SinUnidad);
        }

        public void TestCalibrationClock()
        {
            var defs = new List<AdjustValueDef>();

            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_50Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_ARR_60Hz_TM1", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Err25", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Err25", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_RTC_PRS", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });
            defs.Add(new AdjustValueDef() { Name = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Name, Min = Params.PARAMETER("CLK_Po", ParamUnidad.SinUnidad).Null.Ajuste.Min(), Max = Params.PARAMETER("CLK_Po", ParamUnidad.us).Null.Ajuste.Max(), Unidad = ParamUnidad.SinUnidad });

            TestMeasureBase(defs,
            (step) =>
            {
                logger.InfoFormat("Configurando la tolerancia del Error en ppm");

                var calibrationClk = EkorPlus.ReadCalibrationClock();

                return new double[] { calibrationClk.Arr_50Hz_TM1, calibrationClk.Arr_60Hz_TM1,
                                        calibrationClk.Err25, calibrationClk.RTC_PRS, calibrationClk.Po };
            }, 0, 2, 1000);

        }

        public virtual void TestTrigger()
        {
            SamplerWithCancel((x) =>
            {
                Logger.InfoFormat(string.Format("Reintento : {0}", x));

                if (utilNuevo)
                    tower.IO.DO.OnWait(200, (byte)Pistones.PISTON_DB9);

                else
                    tower.IO.DO.OnWait(200, PISTONS_FRONT);

                SamplerWithCancel((p) =>
                {
                    EkorPlus.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_TRIGGER", "0064 04ec 0000 003c 003c 003c 00 00 00 01 00 11 0000 0001 0258 00 ff 00", ParamUnidad.SinUnidad));
                    Delay(1500, "");
                    EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_TRIGGER", "0103 270f 1a 00 00 00 1964 06 0004 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));
                    Delay(1000, "");
                    return true;
                }, "Error. No se han podido escribir los ajustes de usuario / instalador en el equipo", 3, 1000, 0, false, false);

                var currentTrigger = Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 2, ParamUnidad.A);

                if (utilNuevo)
                    tower.IO.DO.OffWait(200, (byte)Pistones.PISTON_DB9);

                else
                    tower.IO.DO.OffWait(200, PISTONS_FRONT);

                Tower.ActiveCurrentCircuit(true, true, true);

                this.InParellel(
                () =>
                {                   
                    Tower.MUX.Reset();
                    Tower.MUX.Connect2((int)InputMuxEnum.IN9, Outputs.FreCH1, (int)InputMuxEnum.IN4, Outputs.FreCH2);

                    Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 1);

                    Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 0.5);
                },
                () =>
                {
                    Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(currentTrigger, 0, 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
                });

                var adjustValue = new AdjustValueDef(Params.TIME.Null.TRIGGER, ParamUnidad.s);

                var adjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    try
                    {
                        var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 10000, HP53131A.Channels.CH1, () =>
                        {
                            Delay(500, "");
                            Tower.IO.DO.Off(28);
                        });

                        return (timeResult.Value);
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Format("Error, el equipo no ha disparado"));
                    }
                }, 0, 1, 1000);

                Tower.ActiveCurrentCircuit(true, true, true);

                var inyectedCurrent = new AdjustValueDef(Params.I.Null.TRIGGER.Name, 0, 0, 0, Consignas.GetDouble(Params.I.Null.TRIGGER.Name, 2, ParamUnidad.A), Params.I.Null.TRIGGER.Tol(), ParamUnidad.A);
                TestMeasureBase(inyectedCurrent, (s) =>
                {
                    var currrentTower = Tower.PowerSourceIII.ReadCurrent().L1;
                    return currrentTower;
                }, 0, 2, 500);

                Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();

                if (utilNuevo)
                    tower.IO.DO.OnWait(200, (byte)Pistones.PISTON_DB9);
                else
                    tower.IO.DO.OnWait(200, PISTONS_FRONT);

                SamplerWithCancel((p) =>
                {
                    EkorPlus.ReadValidateTrigger();
                    return true;
                }, "Error, no se ha podido validar el disparo");


                var currentTriggerVerif = new AdjustValueDef(Params.I.Other("TRIGGER").Verificacion.Name, 0, 0, 0, 0, Params.I.Null.TRIGGER.Tol(), ParamUnidad.s);

                TestCalibracionBase(currentTriggerVerif,
                   () =>
                   {
                       var historicTrigger = EkorPlus.ReadInfoTriggers().currentTriggerHomopolar;
                       return historicTrigger; ;

                   },
                   () =>
                   {
                       return inyectedCurrent.Value;
                   }, 1, 1, 500, "Error en la verificación de la corriente de disparo");


                var timeTriggerVerif = new AdjustValueDef(Params.TIME.Other("TRIGGER").Verificacion.Name, 0, 0, 0, 0, 5, ParamUnidad.s);

                TestCalibracionBase(timeTriggerVerif,
                   () =>
                   {
                       return EkorPlus.ReadInfoTriggers().timeTriggerHomopolar;
                   },
                   () =>
                   {
                       return adjustResult.Value;
                   }, 1, 1, 500, "Error en la verifiación del tiempo de disparo");

                return true;
            }, string.Empty, 2, 1000, 0, false, false);
        }

        public virtual void TestCostumize()
        {
            bool isSmart = Configuracion.GetString("VERSION_SMART", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            EkorPlus.WriteFrameNumber(TestInfo.NumBastidor.ToString());
            Delay(500, "Escribiendo nº de bastidor en el equipo");

            if (isSmart)
            {
                EkorPlus.WriteSerialNumber(TestInfo.NumSerie);
                Delay(2000, "Escribiendo nº de serie en el equipo");
            }

            var powerFactors = Configuracion.GetString("FACTORES_DE_POTENCIA", "25000 00003 12500 00003 25000 00001 12500 00001 30000 00001", ParamUnidad.SinUnidad);
            EkorPlus.WriteFactoresProcome(powerFactors);

            var ptL1 = (int)Parametrizacion.GetDouble("PASATAPAS_FINAL_L1", 1099930, ParamUnidad.Puntos);
            var ptL2 = (int)Parametrizacion.GetDouble("PASATAPAS_FINAL_L2", 1114640, ParamUnidad.Puntos);
            var ptL3 = (int)Parametrizacion.GetDouble("PASATAPAS_FINAL_L3", 1068170, ParamUnidad.Puntos);

            EkorPlus.WritePasatapas(ptL1, ptL2, ptL3);

            Delay(1000, "Escritura factor Pasatapas en modo TRIPOLAR");

            Tower.LAMBDA.ApplyOffAndWaitStabilisation();

            Delay(2000, "Espera Reset de hardware");

            Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(Consignas.GetDouble(Params.V.Null.EnVacio.Vnom.Name, 24, ParamUnidad.V), 0.5);

            var frameNumber = string.Empty;
            SamplerWithCancel((p) =>
            {
                frameNumber = EkorPlus.ReadFrameNumber();
                return true;
            }, "Error. No se ha podido comunicar con el equipo después del reset", 10, 500, 1000);           
            Assert.AreEqual(TestInfo.NumBastidor.ToString(), frameNumber, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. El número de bastidor grabado en el equipo no corresponde con el leido"));

            if (isSmart)
            {
                var serialNumber = EkorPlus.ReadSerialNumber();
                Assert.AreEqual(TestInfo.NumSerie, serialNumber, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El numero de serie leido en el equipo no corresponde con el grabado"));
            }

            TestDesviationClock();

            if (isSmart)
            {
                var procomeFactorsReaded = EkorPlus.ReadFactoresProcome();
                Resultado.Set("PROCOME_FACTORS", procomeFactorsReaded.TramaLFP, ParamUnidad.SinUnidad);
                var pasatapasReaded = EkorPlus.ReadPasaTapas();
                Assert.AreEqual("PASATAPAS_L1", pasatapasReaded.L1.ToString(), ptL1.ToString(), Error().PROCESO.PARAMETROS_ERROR.FALTA_TAPA("Error, el pasatapas L1 no coincide con el escrito anteriormente"), ParamUnidad.SinUnidad);
                Assert.AreEqual("PASATAPAS_L2", pasatapasReaded.L2.ToString(), ptL2.ToString(), Error().PROCESO.PARAMETROS_ERROR.FALTA_TAPA("Error, el pasatapas L2 no coincide con el escrito anteriormente"), ParamUnidad.SinUnidad);
                Assert.AreEqual("PASATAPAS_L3", pasatapasReaded.L3.ToString(), ptL3.ToString(), Error().PROCESO.PARAMETROS_ERROR.FALTA_TAPA("Error, el pasatapas L3 no coincide con el escrito anteriormente"), ParamUnidad.SinUnidad);
            }

            EkorPlus.WriteRemoveCounter();

            EkorPlus.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_FAB", "04b0 012c 003c 003c 003c 003c 00 24 00 00 00 00 0000 0001 0258 00 ff 00", ParamUnidad.SinUnidad));
            Delay(500, "Esperando grabacion factores usuario cliente");

            if (isSmart)
                SamplerWithCancel((p) =>
                {
                    if (p == 4)
                    {
                        Tower.IO.DO.PulseOff(1500, (byte)Pistones.PISTON_DB9);
                        Delay(10000, "Espera detección unidad lógica");
                    }
                    SendConfigUserXML();
                    return true;
                }, string.Empty, 6, 15000, 10000, false, false);

            EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_FAB", "0a43 270f 1A 00 00 00 1964 06 0004 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));
            Delay(500, "Esperando grabacion factores instalador cliente");
        }

        public virtual void TestFinish()
        {
            if (EkorPlus != null)
                EkorPlus.Dispose();

            if (tower != null)
            {
                tower.ShutdownSources();
                if (utilNuevo)
                {
                    tower.IO.DO.OffWait(2000, OUT_ESC_BUTTON_ACTUATOR, OUT_LEFT_BUTTON_ACTUATOR, OUT_RIGHT_BUTTON_ACTUATOR, OUT_SET_BUTTON_ACTUATOR, OUT_DOWN_BUTTON_ACTUATOR);
                    usbIO.DO.Off(OUT_UP_BUTTON_ACTUATOR);
                    TestDeviceDisconnection();
                    tower.IO.DO.OffWait(500, VALVULA_GENERAL_UTIL);
                }
                else
                    Tower.IO.DO.OffWait(500, PISTONS_REAR, PISTONS_FRONT);

                tower.IO.DO.Off(14);
                tower.IO.DO.Off(4);
                tower.Dispose();
            }
        }

        internal void TestDesviationClock()
        {
            var timeEkor = EkorPlus.ReadDateTime();

            logger.InfoFormat("EKOR_PLUS FECHA:{0} HORA:{1}", timeEkor.ToShortDateString(), timeEkor.ToLongTimeString());
            logger.InfoFormat("PC FECHA:{0} HORA:{1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());

            double timeDiff = (DateTime.Now.Subtract(timeEkor)).TotalSeconds;

            logger.InfoFormat("DERIVA RELOJ:{0}", timeDiff);

            Assert.AreBetween(Params.TIME.Null.Offset.Name, timeDiff, Params.TIME.Null.Offset.Min(), Params.TIME.Null.Offset.Max(), Error().UUT.CONFIGURACION.RELOJ("Error. El equipo tiene una deriva de reloj fuera de margenes"), ParamUnidad.s);

            EkorPlus.WriteDate(DateTime.Now);
        }

        //******************************************************************************* 
        //*******************************************************************************

        internal Dictionary<int, EKOR_PLUS.InputReadings> keyboardActuatorsDictionary = new Dictionary<int, EKOR_PLUS.InputReadings>()
        {
            {ANY_BUTTON, EKOR_PLUS.InputReadings.NO_BUTTON_OR_INPUT},
            {OUT_SET_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.SET_BUTTON},
            {OUT_ESC_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.ESC_BUTTON},
            {OUT_DOWN_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.DOWN_BUTTON},
            {OUT_LEFT_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.LEFT_BUTTON},
            {OUT_RIGHT_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.RIGHT_BUTTON},
            {OUT_UP_BUTTON_ACTUATOR, EKOR_PLUS.InputReadings.UP_BUTTON}
        };


        internal void SendConfigUserXML()
        {
            Logger.InfoFormat("Enviando fichero XML de configuracion de usuario");
            string pathFile;

            pathFile = ConfigurationManager.AppSettings["PathFileBinary"] + "CFG_USR_TEST.XML";

            if(!File.Exists(pathFile))
                Error().SOFTWARE.BBDD.FICHERO_NO_ENCONTRADO("No se ha encontrado el fichero XML de configuración").Throw();
           
            if (File.Exists("D:\\CFG_USR.XML"))
                File.Delete("D:\\CFG_USR.XML");

            Delay(2000, "Eliminando fichero XML");

            Logger.InfoFormat("Se ha eliminado el fichero de la unidad logica D:");

            File.Copy(pathFile, "D:\\CFG_USR.XML");

            Delay(2000, "Copiando fichero XML en la unidad lógica");

            Logger.InfoFormat("Fichero nuevo copiado en la unidad logica D:");

            EkorPlus.ImportXML(EKOR_PLUS.FileTypeExportImport.USR);
            Delay(500, "Ejecutando comando import XML del equipo");

            Logger.InfoFormat("El fichero se ha importado al equipo correctamente");

            //Cada vez que importamos fichero XML, el equipo pierde las configuraciones de hora y fecha
            EkorPlus.WriteDate(DateTime.Now);
            Delay(500, "Esperando grabacion correcta de la hora y fecha");
        }

        public void TestCustom()
        {
            var port = Convert.ToInt32(GetVariable<string>("ekorPort").Replace("COM", ""));

            EkorPlus.Cirbus.PortCom = port;
            EkorPlus.Cirbus.BaudRate = 38400;
            EkorPlus.Cirbus.PerifericNumber = 0;

            bool isSmart = Configuracion.GetString("VERSION_SMART", "NO", ParamUnidad.SinUnidad).ToUpper() == "SI" ? true : false;

            EkorPlus.WriteFrameNumber(TestInfo.NumBastidor.ToString());
            Delay(500, "Escribiendo nº de bastidor en el equipo");

            if (isSmart)
            {
                EkorPlus.WriteSerialNumber(TestInfo.NumSerie);
                Delay(2000, "Escribiendo nº de serie en el equipo");
            }

            var powerFactors = Configuracion.GetString("FACTORES_DE_POTENCIA", "25000 00003 12500 00003 25000 00001 12500 00001 30000 00001", ParamUnidad.SinUnidad);
            EkorPlus.WriteFactoresProcome(powerFactors);

            var ptL1 = (int)Parametrizacion.GetDouble("PASATAPAS_FINAL_L1", 1099930, ParamUnidad.Puntos);
            var ptL2 = (int)Parametrizacion.GetDouble("PASATAPAS_FINAL_L2", 1114640, ParamUnidad.Puntos);
            var ptL3 = (int)Parametrizacion.GetDouble("PASATAPAS_FINAL_L3", 1068170, ParamUnidad.Puntos);

            EkorPlus.WritePasatapas(ptL1, ptL2, ptL3);

            Delay(1000, "Escritura factor Pasatapas en modo TRIPOLAR");


            using (var io = new USB4761())
            {
                io.DO.PulseOff(3000, 0, 1);
            }

            var frameNumber = string.Empty;
            SamplerWithCancel((p) =>
            {
                frameNumber = EkorPlus.ReadFrameNumber();
                return true;
            }, "Error. No se ha podido comunicar con el equipo después del reset", 10, 500, 1000);
            Assert.AreEqual(TestInfo.NumBastidor.ToString(), frameNumber, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error. El número de bastidor grabado en el equipo no corresponde con el leido"));

            if (isSmart)
            {
                var serialNumber = EkorPlus.ReadSerialNumber();
                Assert.AreEqual(TestInfo.NumSerie, serialNumber, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error. El numero de serie leido en el equipo no corresponde con el grabado"));
            }

            TestDesviationClock();

            if (isSmart)
            {
                var procomeFactorsReaded = EkorPlus.ReadFactoresProcome();
                Resultado.Set("PROCOME_FACTORS", procomeFactorsReaded.TramaLFP, ParamUnidad.SinUnidad);
                var pasatapasReaded = EkorPlus.ReadPasaTapas();
                Assert.AreEqual("PASATAPAS_L1", pasatapasReaded.L1.ToString(), ptL1.ToString(), Error().PROCESO.PARAMETROS_ERROR.FALTA_TAPA("Error, el pasatapas L1 no coincide con el escrito anteriormente"), ParamUnidad.SinUnidad);
                Assert.AreEqual("PASATAPAS_L2", pasatapasReaded.L2.ToString(), ptL2.ToString(), Error().PROCESO.PARAMETROS_ERROR.FALTA_TAPA("Error, el pasatapas L2 no coincide con el escrito anteriormente"), ParamUnidad.SinUnidad);
                Assert.AreEqual("PASATAPAS_L3", pasatapasReaded.L3.ToString(), ptL3.ToString(), Error().PROCESO.PARAMETROS_ERROR.FALTA_TAPA("Error, el pasatapas L3 no coincide con el escrito anteriormente"), ParamUnidad.SinUnidad);
            }

            EkorPlus.WriteRemoveCounter();

            EkorPlus.WriteUserSettings(Parametrizacion.GetString("ADJUST_USER_FAB", "04b0 012c 003c 003c 003c 003c 00 24 00 00 00 00 0000 0001 0258 00 ff 00", ParamUnidad.SinUnidad));
            Delay(500, "Esperando grabacion factores usuario cliente");

            EkorPlus.WriteInstallerSettings(Parametrizacion.GetString("ADJUST_INSTALLER_FAB", "0a43 270f 1A 00 00 00 1964 06 0004 0096 0003 ffa6 00 00 00", ParamUnidad.SinUnidad));
            Delay(500, "Esperando grabacion factores instalador cliente");

        }
        //***************************** DESACTIVACION PISTONES UTILLAJE NUEVO ********************************

        internal void TestConectionDevice()
        {
            tower.Active24VDC();
            tower.ActiveElectroValvule();

            var timeWait = 1000;
            tower.IO.DO.OnWait(timeWait, VALVULA_GENERAL_UTIL);

            tower.IO.DO.Off(VALVULA_DOMO_KEYBOARD);
            SamplerWithCancel((p) =>
            {
                return tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());
            var inputsOuputsEkor = new Dictionary<Pistones, int[]>()
            {
                {Pistones.PISTON_FIJACION_EQUIPO, new int[]{21, 22}},    // deteccion piston carro
                {Pistones.PISTON_PUNTAS, new int[]{25,26}},              // deteccion pistones puntas test
            };

            string modelo = Identificacion.MODELO;
            bool hasPasarela = Identificacion.PASARELA == "NO" ? false : true;

            if(hasPasarela)
            {
                tower.IO.DO.OffWait(1000, (int)Pistones.PISTON_GENERAL_POSTERIOR);
                inputsOuputsEkor.Add(Pistones.PISTON_ETH1_RTU, new int[] { 24 });    // Detección pistones ETH1 RTU
                inputsOuputsEkor.Add(Pistones.PISTON_ETH2_RTU, new int[] { 29 });    // Detección pistones ETH2 RTU
                inputsOuputsEkor.Add(Pistones.PISTON_SP0_SP3_RS485, new int[] { 30, 31 });   // Detección pistones SP0/SP3/RS485 
                inputsOuputsEkor.Add(Pistones.PISTON_TRIGGER_PASARELA, new int[] { 32 });
            }
            else
            {
                tower.IO.DO.OnWait(2000, (int)Pistones.PISTON_GENERAL_POSTERIOR);
                inputsOuputsEkor.Add(Pistones.PISTON_RS485_DTC2_EKORPLUS, new int[] { 34 });
                inputsOuputsEkor.Add(Pistones.PISTON_PUNTAS_DISPARO_DTC2_EKORPLUS, new int[] { 35 });
            }

            if (modelo == "RPA")
            {
                tower.IO.DO.OnWait(2000, (int)Pistones.PISTON_GENERAL_COM_FRONTAL);
                inputsOuputsEkor.Add(Pistones.PISTON_USB_RPA, new int[] { 27 });
            }
            else if (modelo == "PLUS")
            {
                tower.IO.DO.OffWait(2000, (int)Pistones.PISTON_GENERAL_COM_FRONTAL);
                inputsOuputsEkor.Add(Pistones.PISTON_DB9, new int[] { 28 });
            }
            else
                Error().PROCESO.PARAMETROS_ERROR.PARAMETRO_INCOHERENTE().Throw();

            foreach (KeyValuePair<Pistones, int[]> inOut in inputsOuputsEkor)
                SamplerWithCancel((p) =>
                {
                    tower.IO.DO.OnWait(timeWait, (int)inOut.Key);

                    foreach (int input in inOut.Value)
                        if (!tower.IO.DI[input])
                            throw new Exception(string.Format("Error no se ha activado el piston {0} de la IN{1} ", inOut.Key.ToString(), input));

                    return true;
                }, "", cancelOnException: false, throwException: false);
        }

        internal void TestDeviceDisconnection()
        {
            var timeWait = 800;
            var inputsOuputs = new Dictionary<Pistones, int[]>()
            {               
                {Pistones.PISTON_PUNTAS, new int[]{25,26}},   // Detección pistones puntas
                {Pistones.PISTON_DB9, new int[]{28}},   // Detección pistones DB9
                {Pistones.PISTON_USB_RPA, new int[]{27}},    // Detección pistones USB RPA
                {Pistones.PISTON_ETH1_RTU, new int[]{24}},     // Detección pistones ETH1 RTU
                {Pistones.PISTON_ETH2_RTU, new int[]{29}},     // Detección pistones ETH2 RTU
                {Pistones.PISTON_SP0_SP3_RS485, new int[]{30,31}},     // Detección pistones SP0/SP3/RS485
                {Pistones.PISTON_TRIGGER_PASARELA, new int[]{32}},      // Detección pistones TRIGGER DTC"
                {Pistones.PISTON_ETH_DTC2, new int[]{33}},         // Detección pistones ethernet DTC2
                {Pistones.PISTON_RS485_DTC2_EKORPLUS, new int[]{34}},       // Detección pistones RS485 DTC2/EKORPLUS
                {Pistones.PISTON_PUNTAS_DISPARO_DTC2_EKORPLUS, new int[]{35}},     // Detección puntas de disparo DTC2 / EKORPLUS
                {Pistones.PISTON_FIJACION_EQUIPO, new int[]{21}},     // Detección puntas de disparo DTC2 / EKORPLUS
            };

            tower.IO.DO.Off(VALVULA_DOMO_KEYBOARD);
            SamplerWithCancelWhitOutCancelToken(
            () =>
            {
                return Tower.IO.DI[DETECTION_DOMO_DOWN];
            }, string.Empty, 10, 500, 1000, true, true, (e) => Error().HARDWARE.UTILLAJE.ENTRADAS_DIGITALES("No se ha detectado el Domo en la posición inferior").Throw());

            foreach (KeyValuePair<Pistones, int[]> inOut in inputsOuputs)
                SamplerWithCancelWhitOutCancelToken(
                    () =>
                    {
                        tower.IO.DO.OffWait(timeWait, (int)inOut.Key);

                        foreach (int input in inOut.Value)
                            if (tower.IO.DI[input])
                                throw new Exception(string.Format("Error no se han desactivado el piston {0} de la IN{1} ", inOut.Key.ToString(), input));

                        return true;
                    }, "", initialDelay: 200, cancelOnException: false, throwException: false);

            tower.IO.DO.OffWait(timeWait, (int)Pistones.PISTON_GENERAL_COM_FRONTAL, (int)Pistones.PISTON_GENERAL_POSTERIOR);
        }
    }
}

