﻿using Comunications.Ethernet;
using Dezac.Core.Exceptions;
using System.ComponentModel;
using System.Drawing;
using Dezac.Tests.Actions.Kernel;

namespace Dezac.Tests.Actions
{
    [ActionVersion(1.04)]
    [DesignerAction(Type = typeof(SSHRunCommandActionDescription))]
    public class SSHRunCommand : ActionBase
    {
        [Editor(typeof(System.ComponentModel.Design.MultilineStringEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Linea de Comando que se enviara por SSH")]
        [Category("3. Configuation")]
        public string TextToSend { get; set; }

        [DefaultValue("SSH_Device")]
        [Category("3.1 Instance")]
        [Description("Nombre de la instancia del objeto SSH")]
        public string DeviceName { get; set; }

        [DefaultValue(false)]
        [Description("Indica que NO se guardará cada linea de respuesta en la variable DataOutput + linea")]
        [Category("4. Results")]
        public bool NotSaveSaveByLine { get; set; }

        [Description("Nombre del dato con el resultado")]
        [Category("4. Results")]
        public ActionVariable DataOutput { get; set; } = new ActionVariable();

        public override void Execute()
        {
            if (DataOutput == null)
                DataOutput = new ActionVariable();

            var ssh = GetVariable(DeviceName.Replace('-', '_')) as SSHClient;
            if (ssh == null)
                Error().SOFTWARE.SECUENCIA_TEST.INSTANCIA_CLASE("SSHClient").Throw();          

            var send = VariablesTools.ResolveText(TextToSend.Replace("\r\n", ""));
            Logger.InfoFormat("SSH Run -> {0}", send);

            var result = ssh.Run(send);

            Logger.InfoFormat("SSH Response -> {0}", result);

            if (string.IsNullOrEmpty(result))
                return;

            if(!DataOutput.IsKeyNullOrEmpty)
                DataOutput.Value = result.Replace("\n", "");                            
        }

        public class SSHRunCommandActionDescription : ActionDescription
        {
            public override string Name { get { return "SSHCommand"; } }
            public override string Description { get { return "Envía y espera a recibir la trama especificada por SSH"; } }
            public override string Category { get { return "Tools/Ethernet/SSH"; } }
            public override Image Icon { get { return Properties.Resources.terminal_network_icon; } }
            public override string Dependencies { get { return "SSHInitialization;SSHFinish:2"; } }
        }
    }
}
