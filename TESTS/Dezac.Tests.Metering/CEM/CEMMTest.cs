﻿using Dezac.Core.Utility;
using Dezac.Device;
using Dezac.Device.Metering;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WinDHCP.Library;

namespace Dezac.Tests.Metering
{
    [TestVersion(1.18)]
    public class CEMMTest : TestBase
    {
        private CEMM cemm; 
        private string PathImage = @"\\Sfserver01\idp\Idp Público\IMAGESOURCE\CEM_AUX_153\";

        private string HostName = "";

        public void TestInitialization()
        {
            var viewForm = Shell.ShowDialog("INICIO TEST", () => new
             ImageView("COLOQUE EL EQUIPO Y ENCIENDA EL INTERRUPTOR", PathImage + "interruptor.jpg"),
             MessageBoxButtons.OK);
        }

        public void InitTCP()
        {

            SamplerWithCancel((p) =>
            {
                cemm = new CEMM(GetVariable("IP").ToString(), 502, ModbusTCPProtocol.TCP);
                cemm.Modbus.PerifericNumber = 255;
                return true;
            }, "Error no se ha podido esteblecer conexion con el CEMM por Ethrnet");
        }

        public void ComunicationAndLeds()
        {
            string version = "";
            string versionBD = Identificacion.VERSION_FIRMWARE;

            SamplerWithCancel((p) =>
            {
                version = cemm.ReadSoftwareVersion();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, versionBD, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Versión de Firmware del equipo diferente a la de la BBDD"), ParamUnidad.SinUnidad);
                return true;
            }, "",5,2000);

            cemm.Modbus.PerifericNumber = 1;

            var Energy = cemm.ReadEnergyCEMC10();

            cemm.WriteBastidorCEMC10(123456789);

            Delay(1000, "");

            var BastidorCEMC10Read = cemm.ReadBastidorCEMC10();

            Assert.IsTrue(BastidorCEMC10Read == 123456789, Error().UUT.COMUNICACIONES.NO_COINCIDE("error comunicaciones con el CEM-MC10 no se ha leido el Bastidor esperado"));

            cemm.Modbus.PerifericNumber = 255;

            cemm.WriteFlagTest(true);

            cemm.WriteLedGreen(false);

            cemm.WriteLedRed(true);

            Shell.MsgBox("Comprobar que el led Rojo esta encendido", "TEST LEDS", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Question);

            cemm.WriteLedGreen(true);

            Shell.MsgBox("Comprobar que el led Verde esta encendido", "TEST LEDS", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Question);

        }

        public void Costumize()
        {
            cemm.Modbus.PerifericNumber = 255;

            cemm.WriteBastidor((int)TestInfo.NumBastidor);

            cemm.WriteNumSerie(TestInfo.NumSerie.Trim());

            string numMAC = TestInfo.NumMAC.Replace("-", "").Replace(":", "");

            cemm.WriteMAC(numMAC);

            cemm.Dispose();

            cemm = null;

            var DHCPserver = GetSharedVariable("DHCPServer") as DhcpServer;
            if (DHCPserver == null)
                Error().SOFTWARE.FALLO_RED.CARGA_SERVICIO("DHCPServer no existente").Throw();

            DHCPserver.Stop();

            ResetManualHardwareDevice();

            DHCPserver.Start();

            var resultClients = new List<ClientEntry>();
            var waitTime = 2000;
            HostName = null;

            SamplerWithCancel((p) =>
            {
                logger.InfoFormat("Intento {0} de {1} Leemos equipos conectados al Router sin contar el PC", p, 50);
                var clients = DHCPserver.ClientsActive;
                foreach (var cli in clients)
                {
                    resultClients.Add(new ClientEntry() { Name = cli.Value.HostName, MAC = cli.Value.Owner.MAC, IP = cli.Value.Address.ToString(), Expire = cli.Value.Expiration.ToShortTimeString() });
                    Logger.InfoFormat(string.Format("--> CLIENT  -- NAME: {0}  -- MAC: {1}  -- IP: {2}  -- Expired: {3}", cli.Value.HostName, cli.Value.Owner.MAC, cli.Value.Address.ToString(), cli.Value.Expiration));
                    var nameHost = string.IsNullOrEmpty(cli.Value.HostName) ? "UNKNOW" : cli.Value.HostName;
                }

                logger.InfoFormat("Equipos conectados: {0}", clients.Count);
                if (clients.Count.Equals(0))
                    return false;

                logger.InfoFormat("Buscamos la IP del equipo conectado al Router con la MAC {0}", TestInfo.NumMAC.Replace("-", ":"));
                var deviceFound = resultClients.Where((d) => d.MAC == TestInfo.NumMAC.Replace("-", ":")).FirstOrDefault();
                if (deviceFound == null)
                {
                    logger.InfoFormat("Equipos NO encontrado con la MAC {0}", TestInfo.NumMAC.Replace("-", ":"));
                    return false;
                }

                HostName = deviceFound.IP;
                logger.InfoFormat("Equipos encontrado con la MAC: {0} y la IP: {1}", deviceFound.MAC, deviceFound.IP);
                return true;

            }, "No se ha encontrado el equipo con la nueva MAC en al router", 30, 2000, waitTime, false);

            SamplerWithCancel((p) =>
            {
                cemm = new CEMM(HostName, 502, ModbusTCPProtocol.TCP);
                cemm.Modbus.PerifericNumber = 255;
                return true;
            }, "Error no se ha podido esteblecer conexion con el CEMM por Ethrnet", 10, 500);

            SamplerWithCancel((p) =>
            {
                var NumSerieRead = cemm.ReadNumSerie();

                Resultado.Set("NumeroSerie", NumSerieRead.ToString(), ParamUnidad.Numero, TestInfo.NumSerie.ToString());

                Assert.AreEqual(NumSerieRead, Convert.ToInt32(TestInfo.NumSerie), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error,en la de grabación del Numero de Serie incorrecto"));

                return true;

            }, "Error comunicaciones, no hemos recibido respuesta del equipo", 10, 500);

            var Bastidor = cemm.ReadBastidor();

            Resultado.Set("Bastidor", Convert.ToDouble(Bastidor), ParamUnidad.Numero);

            Assert.AreEqual(Bastidor, (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, de grabación del numero de Bastidor incorrecto"));

            var deviceMAC = cemm.ReadMAC();

            var regex = "(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})";
            var replace = "$1-$2-$3-$4-$5-$6";
            TestInfo.NumMAC = Regex.Replace(deviceMAC, regex, replace);

            Resultado.Set("MAC", TestInfo.NumMAC, ParamUnidad.SinUnidad);

            Assert.AreEqual(numMAC, deviceMAC, Error().UUT.HARDWARE.NO_COINCIDE("Error, de grabación de la MAC incorrecto"));
        }

        #region MODELO CEMM-RS485

        public void InitCOM(string serialPort, byte perifericNumber = 1)
        {
            if (perifericNumber == 0)
                perifericNumber = Comunicaciones.Periferico;

            cemm = new CEMM(serialPort);

            cemm.Modbus.PerifericNumber = perifericNumber;

            ((ModbusDeviceSerialPort)cemm.Modbus).RtsEnable = true;
        }

        public void TestLeds()
        {
            if (Shell.ShowDialog("INICIO TEST", () => new ImageView("Encienda el interruptor y OBSERVE LOS LEDS:\n\nLED POWER->VERDE\nLED LINK->PASA DE ROJO A VERDE\n", PathImage + "interruptor.jpg"), MessageBoxButtons.OKCancel) != DialogResult.OK)
                throw new Exception("El usuario ha detectado un error en la prueba de LEDS");

            Resultado.Set("LED_POWER", "OK", ParamUnidad.SinUnidad);
            Resultado.Set("LED_LINK", "OK", ParamUnidad.SinUnidad);
        }

        public void TestComsContinuo()
        {
            SetVariable("USER_END", false);        

            do
            {
                SamplerWithCancel((p) =>
                {
                    cemm.ReadSoftwareVersion485();
                    return true;
                }, "No se ha podido comunicar con el CEM-AUX al mandarle una trama para que se encienda el led de comunicaciones", 5, 100, 0, false, false);

                Delay(150, "");
                
            }
            while (!GetVariable<bool>("USER_END"));
        }

        public void TestLedCOMS()
        {
            SetVariable("USER_END", false);

            if (Shell.MsgBox("Parpadea el led COMS de color naranja?", "TEST LED COMS", MessageBoxButtons.OKCancel) == DialogResult.OK)
                SetVariable("USER_END", true);
            else
            throw new Exception("Error, led COMS incorrecto");

            Resultado.Set("LED_COMS", "OK", ParamUnidad.SinUnidad);
        }         

        public void TestComunicationsRS485()
        {
            string version = "";
            string versionBD = Identificacion.VERSION_FIRMWARE;

            SamplerWithCancel((p) =>
            {
                version = cemm.ReadSoftwareVersion485();
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, version, versionBD, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Versión de Firmware del equipo diferente a la de la BBDD"), ParamUnidad.SinUnidad);
                return true;
            }, "", 5, 2000);

            cemm.Modbus.PerifericNumber = 255;

            var SNPatron = cemm.ReadNumSerie485();
            var SNExpected = Identificacion.SN_PATRON;
            Assert.AreEqual("SN_PATRON", SNPatron.ToString(), SNExpected, Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error, el numero de serie del patrón CEM-C10 es incorrecto"), ParamUnidad.SinUnidad);

            cemm.WriteBastidorCEMC10(123456789);

            Delay(1000, "");

            var BastidorCEMC10Read = cemm.ReadBastidorCEMC10();

            Assert.IsTrue(BastidorCEMC10Read == 123456789, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("error comunicaciones con el CEM-C10 no se ha leido el Bastidor esperado"));

            cemm.Modbus.PerifericNumber = 1;

            var Energy = cemm.ReadEnergyCEMC10();
            
            Resultado.Set("COMUNICATIONS", "OK", ParamUnidad.SinUnidad);

        }

        public void TestCostumize()
        {
            cemm.WriteBastidor485((int)TestInfo.NumBastidor);

            cemm.WriteNumSerie485(TestInfo.NumSerie.Trim());

            cemm.WriteErrorCodeCEMC10(0);

            Shell.MsgBox("Apague y vuelva a encender el equipo", "TEST MEMORIA", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Question);

            Delay(2000, "Esperamos reinicialización del equipo por cambio de MAC");           

            SamplerWithCancel((p) =>
            {
                var NumSerieRead = cemm.ReadNumSerie485();

                Resultado.Set("NumeroSerie", NumSerieRead.ToString(), ParamUnidad.Numero, TestInfo.NumSerie.ToString());

                Assert.AreEqual(NumSerieRead, Convert.ToInt32(TestInfo.NumSerie), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error,en la de grabación del Numero de Serie incorrecto"));

                return true;

            }, "Error comunicaciones, no hemos recibido respuesta del equipo");

            var Bastidor = cemm.ReadBastidor485();

            Resultado.Set("Bastidor", Convert.ToDouble(Bastidor), ParamUnidad.Numero);

            Assert.AreEqual(Bastidor, (int)TestInfo.NumBastidor.Value, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error, de grabación del numero de Bastidor incorrecto"));

        }

        #endregion

        public void TestFinish()
        {
            var viewForm = Shell.ShowDialog("FIN TEST", () => new
             ImageView("APAGE EL EQUIPO", PathImage + "interruptor.jpg"),
             MessageBoxButtons.OK);

            if (cemm != null)
                cemm.Dispose();
        }

        private void ResetManualHardwareDevice()
        {
            Shell.MsgBox("APAGUE EL EQUIPO", "REINICIO EQUIPO", MessageBoxButtons.OK, MessageBoxIcon.Question);

            Delay(2000, "Esperamos reinicialización del equipo por cambio de MAC");

            Shell.MsgBox("ENCIENDA EL EQUIPO", "REINICIO EQUIPO", MessageBoxButtons.OK, MessageBoxIcon.Question);

            Delay(5000, "Esperamos reinicialización del equipo por cambio de MAC");
        }
    }
}
