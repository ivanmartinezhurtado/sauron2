﻿using Instruments.Utility;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace Instruments.IO
{
    [InstrumentsVersion(1.05)]
    public class IOMux: IMux
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(IOMux));

        public bool ThrowException { get; set; }

        public int D1 { get; set; }
        public int D2 { get; set; }
        public int D3 { get; set; }
        public int D4 { get; set; }
        public int CHA { get; set; }
        public int CHB { get; set; }
        public int CLK1 { get; set; }
        public int CLK2 { get; set; }

        protected IIOOutput IODevice;

        private Dictionary<string, int[]> outputMap = new Dictionary<string, int[]>
        {
            { "A-", new int[] {0, 1, 0, 0}},
            { "-A", new int[] {0, 0, 0, 1}},
            { "AA", new int[] {0, 1, 0, 1}},
            { "B-", new int[] {1, 0, 0, 0}},
            { "-B", new int[] {0, 0, 1, 0}},
            { "BB", new int[] {1, 0, 1, 0}},
            { "AB", new int[] {0, 1, 1, 0}},
            { "BA", new int[] {1, 0, 0, 1}},
        };

        public IOMux()
        {
        }

        public IOMux(IIOOutput Device)
        {
            IODevice = Device;
            Config(31, 32, 33, 34, 37, 38, 35, 36);
        }

        public string GetInstrumentVersion()
        {
            CultureInfo enUS = new CultureInfo("en-US");
            var attr = this.GetType().GetCustomAttributes(typeof(InstrumentsVersionAttribute), true).FirstOrDefault() as InstrumentsVersionAttribute;
            if (attr != null)
                return string.Format(enUS, "{0:0.00}", attr.Version);
            else
                return "SIN VERSIONAR";
        }

        [Browsable(false)]
        public void Config(int d1, int d2, int d3, int d4, int chA, int chB, int clk1, int clk2)
        {
            D1 = d1;
            D2 = d2;
            D3 = d3;
            D4 = d4;
            CHA = chA;
            CHB = chB;
            CLK1 = clk1;
            CLK2 = clk2;
        }

        public void Reset()
        {
            IODevice.PulseOn(100, CLK1, CLK2);
        }

        public bool Connect(InputMuxEnum input, Outputs output)
        {
            return Connect((int)input, output);
        }

        [Browsable(false)]
        public bool Connect(int input, Outputs output)
        {
            return Connect2(input, output, null, null);
        }

        public bool Connect2(InputMuxEnum input, Outputs output, InputMuxEnum input2, Outputs output2)
        {
            return Connect2((int)input, output, (int?)input2, output2);
        }

        [Browsable(false)]
        public bool Connect2(int input, Outputs output, int? input2, Outputs? output2)
        {
            try
            {
                Reset();

                if (input <= 0 || input > 16)
                    throw new IOException(IOException.Codes.CANNOT_CONFIG, string.Format("Entrada {0} fuera de rango", input));

                if (input2.HasValue && (input2 <= 0 || input2 > 16))
                    throw new IOException(IOException.Codes.CANNOT_CONFIG, string.Format("Entrada {0} fuera de rango", input2));

                int[] sel1 = GetSelectInputs(input);
                int[] sel2 = input2.HasValue ? GetSelectInputs(input2.Value) : null;

                _logger.InfoFormat("Selecting CHA for Input {0} => {1} - IO Outs: {2}", input, output, Array2String(sel1));

                IODevice.On(sel1);
                IODevice.PulseOn(150, CHA);
                
                //TODO Poner una espera si no no lo detecta - OK
                Thread.Sleep(200);

                IODevice.Off(sel1);

                if (sel2 != null)
                {
                    _logger.InfoFormat("Selecting CHB for Input {0} => {1} - IO Outs: {2}", input2, output2, Array2String(sel2));

                    IODevice.On(sel2);
                    IODevice.PulseOn(150, CHB);
                    
                    Thread.Sleep(150);

                    IODevice.Off(sel2);
                }
          
                int[] selOut1 = GetSelectOutputsA(output, output2);
                if (selOut1 != null)
                {
                    _logger.InfoFormat("Selecting outputs CLKA -> IO Outs: {0}", Array2String(selOut1));

                    IODevice.On(selOut1);

                    Thread.Sleep(150);

                    IODevice.PulseOn(100, CLK1);

                    Thread.Sleep(150);

                    IODevice.Off(selOut1);
                }

                int[] selOut2 = GetSelectOutputsB(output, output2);
                if (selOut2 != null)
                {
                    _logger.InfoFormat("Selecting outputs CLKB -> IO Outs: {0}", Array2String(selOut2));

                    IODevice.On(selOut2);

                    Thread.Sleep(150);

                    IODevice.PulseOn(100, CLK2);

                    Thread.Sleep(150);

                    IODevice.Off(selOut2);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        [Browsable(false)]
        public string Array2String(int[] data)
        {
            StringBuilder sb = new StringBuilder(4);

            for (int i = 0; i < data.Length; i++)
                sb.AppendFormat("{0} ", data[i]);

            return sb.ToString();
        }

        [Browsable(false)]
        public int[] GetSelectInputs(int input)
        {
            List<int> dataSelec = new List<int>();

            input = ~(input - 1);

            if ((input & 0x01) > 0)
                dataSelec.Add(D1);
            if ((input & 0x02) > 0)
                dataSelec.Add(D2);
            if ((input & 0x04) > 0)
                dataSelec.Add(D3);
            if ((input & 0x08) > 0)
                dataSelec.Add(D4);

            return dataSelec.ToArray();
        }

        [Browsable(false)]
        public int[] GetSelectOutputsA(Outputs outputA, Outputs? outputB)
        {
            string t = outputA == Outputs.Multimetro ? "A" : outputB == Outputs.Multimetro ? "B" : "-";
            string f1 = outputA == Outputs.FreCH1 ? "A" : outputB == Outputs.FreCH1 ? "B" : "-";

            string key = t + f1;

            int[] result;

            if (!outputMap.TryGetValue(key, out result))
                return null;

            return GetDOOutputs(result);
        }

        [Browsable(false)]
        public int[] GetSelectOutputsB(Outputs outputA, Outputs? outputB)
        {
            string t = outputA == Outputs.FreCH2 ? "A" : outputB == Outputs.FreCH2 ? "B" : "-";
            string f1 = outputA == Outputs.Aux ? "A" : outputB == Outputs.Aux ? "B" : "-";

            string key = t + f1;

            int[] result;

            if (!outputMap.TryGetValue(key, out result))
                return null;

            return GetDOOutputs(result);
        }

        private int[] GetDOOutputs(int[] result)
        {
            List<int> dataSelec = new List<int>();

            if (result[0] != 0)
                dataSelec.Add(D1);
            if (result[1] != 0)
                dataSelec.Add(D2);
            if (result[2] != 0)
                dataSelec.Add(D3);
            if (result[3] != 0)
                dataSelec.Add(D4);

            return dataSelec.ToArray();
        }

        public void Dispose()
        {
        }

        [Browsable(false)]
        public void Disconnect(int input, Outputs output)
        {
            Reset();
        }

        [Browsable(false)]
        public void Disconnect2(int input, Outputs output, int? input2, Outputs? output2)
        {
            Reset();
        }

        [Browsable(false)]
        public void Disconnect2(InputMuxEnum input, Outputs output, InputMuxEnum input2, Outputs output2)
        {
            Reset();
        }
    }
}
