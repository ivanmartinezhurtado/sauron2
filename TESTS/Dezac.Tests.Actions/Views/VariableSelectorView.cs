﻿using Dezac.Core.Model;
using Dezac.Data;
using Dezac.Services;
using Dezac.Tests.Model;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TaskRunner.Model;
using Telerik.WinControls.UI;
using WinTaskRunner.Design;

namespace Dezac.Tests.Actions.Views
{
    public partial class VariableSelectorView : Form
    {
        private static FamiliaProductsFase producto;

        private int? NumProducto { get; set; }
        private string Descripcion { get; set; }
        private int? Version { get; set; }
        private string IdFase { get; set; }
        private string Fase { get; set; }
        private int? IdFamilia { get; set; }
        private string Familia { get; set; }

        public static SharedVariablesList VariablesList { get; set; }

        private IShell Shell
        {
            get { return ServiceContainer.Default.Resolve<IShell>(); }
        }

        private static Dictionary<string, string> history = new Dictionary<string, string>();

        public string Value { get; set; }

        public VariableSelectorView()
        {
            InitializeComponent();

            this.Icon = Application.OpenForms[0].Icon;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            list.LoadDevices(true);
            list.LoadInstruments(true);

            testContext.AddType(null, typeof(ITestContext), "testcontext");
            testInfo.AddType(null, typeof(TestInfo), "testinfo");

            UpdateVariables();

            if (producto != null)
            {
                Descripcion = producto.DESCRIPCION;
                Familia = producto.FAMILIA;
                IdFase = producto.IDFASE;
                Fase = producto.FASE;
                NumProducto = producto.NUMPRODUCTO;
                Version = producto.VERSION;
                IdFamilia = producto.NUMFAMILIA;

                UpdateUI();

                LoadConfigFile();
            }

            if (!string.IsNullOrEmpty(Value) && Value.StartsWith("$"))
            {
                var value = Value.Substring(1);
                var prefix = value.Split('.')[0];

                RadTreeNode node = null;

                if (prefix == "testinfo")
                {
                    txtPrefix.Text = "";
                    cards.SelectTab("Info");// = 1;
                    node = testInfo.FindNodeRecursive(p => p.Tag != null && (p.Tag.ToString() == value || string.Format("{0}.{1}", prefix, p.Tag) == value));
                }
                else if (prefix == "testcontext")
                {
                    txtPrefix.Text = "";
                    cards.SelectTab("Context"); // cards.SelectedIndex = 2;
                    node = testContext.FindNodeRecursive(p => p.Tag != null && (p.Tag.ToString() == value || string.Format("{0}.{1}", prefix, p.Tag) == value));
                }
                else
                {
                    if (value.Contains("-"))
                        txtPrefix.Text = value.Substring(0, value.IndexOf("-"));

                    string path = null;
                    if (history.TryGetValue(Value, out path))
                        node = list.FindNodeRecursive(p => p.FullPath == path);
                }

                if (node != null)
                {
                    node.Selected = true;
                    node.EnsureVisible();
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (cards.SelectedTab.Name == "Device") // (cards.SelectedIndex == 0)
                SelectNodo(list.SelectedNode);
            else if (cards.SelectedTab.Name == "Info")  //(cards.SelectedIndex == 1)
                SelectNodo(testInfo.SelectedNode);
            else if (cards.SelectedTab.Name == "Context")  //(cards.SelectedIndex == 2)
                SelectNodo(testContext.SelectedNode);
            else if (cards.SelectedTab.Name == "Parametrizacion") //(cards.SelectedIndex == 3)
                SelectParam();
            else if (cards.SelectedTab.Name == "Variables") 
                Value = "$" + SelectedVariables.Key;
            else if (cards.SelectedTab.Name == "ConfigurationFiles") 
                SelectConfigFile();

            NumProducto = NumProducto;
            Version = Version;
            IdFase = IdFase;
        }

        private void SelectNodo(RadTreeNode node)
        {
            if (node == null || !(node.Tag is string))
            {
                MessageBox.Show("Debe seleccionar un nodo de una variable de retorno válido!");
                return;
            }

            var name = node.Tag.ToString();

            if (string.IsNullOrEmpty(txtPrefix.Text))
                Value = "$" + name;
            else
                Value = "$" + txtPrefix.Text + "-" + name;

            if (history.ContainsKey(Value))
                history[Value] = node.FullPath;
            else
                history.Add(Value, node.FullPath);
        }

        private void SelectParam()
        {
            var value = paramsView.SelectedParam;
            if (value == null)
            {
                MessageBox.Show("Debe seleccionar un parámetro!");
                return;
            }

            Value = "$" + value.Grupo.Replace("ó", "o").Replace("á", "a").Replace("é", "e") + "." + value.Name;
        }

        private void btnParams_Click(object sender, EventArgs e)
        {
            if (!IdFamilia.HasValue)
                return;

            if (!NumProducto.HasValue)
                return;

            if (!Version.HasValue)
                return;

            if (string.IsNullOrEmpty(IdFase))
                return;

            this.Cursor = Cursors.WaitCursor;

            paramsView.SetProducto(NumProducto.Value, Version.Value, IdFase);

            if (producto == null)
                producto = new FamiliaProductsFase();

            producto.DESCRIPCION = Descripcion;
            producto.FAMILIA = Familia;
            producto.IDFASE = IdFase;
            producto.FASE = Fase;
            producto.NUMPRODUCTO = NumProducto.Value;
            producto.VERSION = Version.Value;
            producto.NUMFAMILIA = IdFamilia.Value;

            this.Cursor = Cursors.Default;
        }

        private void btnAddParam_Click(object sender, EventArgs e)
        {
            if (!IdFamilia.HasValue)
                return;

            if (!NumProducto.HasValue)
                return;

            if (!Version.HasValue)
                return;

            if (string.IsNullOrEmpty(IdFase))
                return;

            this.Cursor = Cursors.WaitCursor;

            using (var editor = new UploadParamsView(IdFamilia, NumProducto, Version, IdFase))
            {
                editor.ShowDialog();
                paramsView.SetProducto(NumProducto.Value, Version.Value, IdFase);
            }

            if (producto == null)
                producto = new FamiliaProductsFase();

            producto.DESCRIPCION = Descripcion;
            producto.FAMILIA = Familia;
            producto.IDFASE = IdFase;
            producto.FASE = Fase;
            producto.NUMPRODUCTO = NumProducto.Value;
            producto.VERSION = Version.Value;
            producto.NUMFAMILIA = IdFamilia.Value;

            this.Cursor = Cursors.Default;
        }

        private void btnFamilia_Click(object sender, EventArgs e)
        {
            List<T_FAMILIA> fams = null;

            using (DezacService svc = new DezacService())
            {
                fams = svc.GetFamilia();
            }

            var item = Shell.ShowTableDialog<T_FAMILIA>("FAMILIAS", new TableGridViewConfig { Data = fams, Fields = "NUMFAMILIA;FAMILIA" });
            if (item == null || item.NUMFAMILIA == IdFamilia)
                return;

            IdFamilia = item.NUMFAMILIA;
            Familia = item.FAMILIA;
            NumProducto = null;
            Version = 0;
            Descripcion = null;
            Fase = null;
            IdFase = null;

            UpdateUI();
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            List<ProductsViewModel> fams = null;

            if (IdFamilia == null)
                return;

            using (DezacService svc = new DezacService())
            {
                fams = svc.GetProductoByFamilia(IdFamilia);
            }

            var item = Shell.ShowTableDialog<ProductsViewModel>("PRODUCTOS", new TableGridViewConfig { Data = fams, Fields = "NUMPRODUCTO;VERSION;DESCRIPCION;ACTIVO" });
            if (item == null)
                return;

            NumProducto = item.NUMPRODUCTO;
            Version = item.VERSION;
            Descripcion = item.DESCRIPCION;

            UpdateUI();
        }

        private void btnFase_Click(object sender, EventArgs e)
        {
            List<ProductFaseViewModel> fases;

            if (!NumProducto.HasValue)
                return;

            if (!Version.HasValue)
                return;

            using (DezacService svc = new DezacService())
            {
                fases = svc.GetProductoFaseByNumProductoVersion(NumProducto.Value, Version.Value);
            }

            var fase = Shell.ShowTableDialog<ProductFaseViewModel>("FASE", new TableGridViewConfig { Data = fases, Fields = "IDFASE;FASE" });  
            if (fase == null)
                return;

            IdFase = fase.IDFASE;
            Fase = fase.FASE;

            UpdateUI();
        }

        private void btnFaseFiles_Click(object sender, EventArgs e)
        {
            List<ProductFaseViewModel> fases;

            if (!NumProducto.HasValue || !Version.HasValue)
                return;

            using (DezacService svc = new DezacService())
            {
                fases = svc.GetProductoFaseByNumProductoVersion(NumProducto.Value, Version.Value);
            }

            var fase = Shell.ShowTableDialog<ProductFaseViewModel>("FASE", new TableGridViewConfig { Data = fases, Fields = "IDFASE;FASE" });
            if (fase == null)
                return;

            IdFase = fase.IDFASE;
            Fase = fase.FASE;

            UpdateUI();

            if (producto == null)
                producto = new FamiliaProductsFase();

            producto.DESCRIPCION = Descripcion;
            producto.FAMILIA = Familia;
            producto.IDFASE = IdFase;
            producto.FASE = Fase;
            producto.NUMPRODUCTO = NumProducto.Value;
            producto.VERSION = Version.Value;
            producto.NUMFAMILIA = IdFamilia.Value;

            LoadConfigFile();
        }

        private void LoadConfigFile()
        {
            this.Cursor = Cursors.WaitCursor;

            List<ConfigurationFile> configurationFilesList = new List<ConfigurationFile>();
            var configFileList = new List<T_TESTCONFIGFILEVERSION>();
            using (DezacService svc = new DezacService())
            {
                configFileList = svc.GetAllProductTestConfigFiles(producto.NUMPRODUCTO, producto.VERSION);
            }
            foreach (var file in configFileList)
                configurationFilesList.Add(new ConfigurationFile(file));

            viewConfigFiles.DataSource = configurationFilesList;

            this.Cursor = Cursors.Default;
        }

        private void SelectConfigFile()
        {
            var value = viewConfigFiles.SelectedRows.FirstOrDefault()?.DataBoundItem as ConfigurationFile;
            if (value == null)
            {
                MessageBox.Show("Debe seleccionar un parámetro!");
                return;
            }

            Value = $"$ConfigurationFiles.{value.Clave}";
        }

        private void UpdateUI()
        {
            if (IdFamilia.HasValue)
            {
                lblFamilia.Text = $"{IdFamilia}-{Familia}";
                lblFamiliaFiles.Text = $"{IdFamilia}-{Familia}";
            }
            else
            {
                lblFamilia.Text = null;
                lblFamiliaFiles.Text = null;
            }

            if (NumProducto.HasValue)
            {
                lblNumProducto.Text = $"{NumProducto}/{Version}-{Descripcion}";
                lblNumProductoFiles.Text = $"{NumProducto}/{Version}-{Descripcion}";
            }
            else
            {
                lblNumProducto.Text = null;
                lblNumProductoFiles.Text = null;
            }

            if (!string.IsNullOrEmpty(IdFase))
            {
                lblIdFase.Text = $"{IdFase}-{Fase}";
                lblIdFaseFiles.Text = $"{IdFase}-{Fase}";
            }
            else
            {
                lblIdFase.Text = null;
                lblIdFaseFiles.Text = null;
            }
        }

        private void UpdateVariables()
        {
            bsList.Clear();

            SharedVariablesList vars = VariablesList;

            var context = SequenceContext.Current;
            if (context != null)
                vars = context.Variables;

            if (vars == null)
                return;

            var dict = vars.VariableLogger.VariablesEntries;
            var items = dict.Select(p => new VariableItemView
            {
                Key = p.Key,
                Value = p.Value.LastOrDefault().Value
            }).ToList();

            bsList.DataSource = items;
        }

        private VariableItemView SelectedVariables
        {
            get { return bsList.Current as VariableItemView; }
        }

        private class FamiliaProductsFase
        {
            public int NUMPRODUCTO { get; set; }
            public int VERSION { get; set; }
            public string DESCRIPCION { get; set; }
            public int NUMFAMILIA { get; set; }
            public string FAMILIA { get; set; }
            public string IDFASE { get; set; }
            public string FASE { get; set; }
        }

        private class VariableItemView
        {
            public string Key { get; set; }

            public object Value { get; set; }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            var configFile = viewConfigFiles.SelectedRows.FirstOrDefault()?.DataBoundItem as ConfigurationFile;
            if (configFile == null)
            {
                MessageBox.Show("Debe seleccionar un parámetro!");
                return;
            }

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = "C:\\SAURON_SYSTEM\\BinaryTemp\\";
                saveFileDialog.FileName = configFile.FileName;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    using (MemoryStream ms = new MemoryStream())
                    {
                        File.WriteAllBytes(saveFileDialog.FileName, configFile.File);
                    }
            } 
        }

    }
}
