namespace Dezac.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APP.VW_EMPLEADO")]
    public partial class VW_EMPLEADO
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string IDEMPLEADO { get; set; }

        [StringLength(101)]
        public string NOMBRE { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string IDPUESTOTRABAJO { get; set; }

        [StringLength(10)]
        public string IDOPERARIO { get; set; }

        public byte[] FOTOGRAFIA { get; set; }

        [StringLength(50)]
        public string EMAIL { get; set; }

        [StringLength(30)]
        public string USERNAME { get; set; }

        [StringLength(10)]
        public string IDCALENDARIO { get; set; }

        public long? NUMTARJETA { get; set; }

        [StringLength(15)]
        public string EXTTLFNINTERNA { get; set; }

        [StringLength(50)]
        public string DEPARTAMENTO { get; set; }

        public DateTime? FECHABAJA { get; set; }

        [StringLength(50)]
        public string DEPT { get; set; }
    }
}
