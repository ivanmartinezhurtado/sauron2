﻿using Comunications.Utility;
using System;
using System.Runtime.InteropServices;

namespace Dezac.Device.Measure
{
    [DeviceVersion(1.03)]
    public class SEM_THREE : DeviceBase
    {
        public SEM_THREE()
        {
        }

        public SEM_THREE(int port)
        {
            SetPort(port);
        }

        public void SetPort(int port, byte periferic = 72, int baudRate = 9600)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
        }

        public ModbusDeviceSerialPort Modbus { get; internal set; }

     
        public void WriteIDManufature(int idManufacture)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.ID_MANUFACTURE, idManufacture);
        }

        public void WriteIDProductoCode(int idProductoCode)
        {
            LogMethod();

            Modbus.WriteInt32((ushort)Registers.ID_PRODUCT_CODE, idProductoCode);
        }

        public void WriteIDVerify(ushort idVerify)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.ID_VERIFY, idVerify);
        }

        public void WriteHardwareVersion(ushort HWversion)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.HARDWARE_VERSION, HWversion);
        }

        public void WriteSoftwareVersion(ushort SFversion)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.SOFTWARE_VERSION, SFversion);
        }

        public void WriteSerialNumber(int modelSerie)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.MODEL_SERIE_HIGH, modelSerie);
        }

        public void WriteModelProduction(int modelProduction)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.MODEL_PRODUCTION, modelProduction);
        }

        public int ReadTransfomationRelation()
        {
            LogMethod();

           return Modbus.ReadHoldingRegister((ushort)Registers.TRANSFORMATION_CURRENT);
        }

        public void WriteTransfomationRelation(ushort relationCurrent)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.TRANSFORMATION_CURRENT, relationCurrent);
        }

        public void ResetCounters()
        {
            LogMethod();

            Modbus.Write((ushort)Registers.COUNTERS, 0x0001);
        }

        //******************************************************************************

        public Variables ReadVariables(SEM_THREE.Lines line)
        {
            LogMethod();

            var addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.VOLTAGE_L1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.VOLTAGE_L2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.VOLTAGE_L3;
                    break;
            }

            return Modbus.Read<Variables>((ushort)addres);
        }       

        public Energies ReadEnergies()
        {
            LogMethod();

            return Modbus.Read<Energies>();
        }      

        public MoreVariables ReadCosPhiAndFrecuency(SEM_THREE.Lines line)
        {
            LogMethod();

            var addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.COS_PHI_L1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.COS_PHI_L2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.COS_PHI_L3;
                    break;
            }

            return Modbus.Read<MoreVariables>((ushort)addres);

        }


        public ushort ReadIDManufature()
        {
            LogMethod();

            return Modbus.ReadHoldingRegister((ushort)Registers.ID_MANUFACTURE);
        }

        public ushort ReadIDProductoCode()
        {
            LogMethod();

            return Modbus.ReadHoldingRegister((ushort)Registers.ID_PRODUCT_CODE);
        }

        public ushort ReadIDVerify()
        {
            LogMethod();

            return Modbus.ReadHoldingRegister((ushort)Registers.ID_VERIFY);
        }

        public ushort ReadHardwareversion()
        {
            LogMethod();

            return Modbus.ReadHoldingRegister((ushort)Registers.HARDWARE_VERSION);
        }

        public ushort ReadSoftwareVersion()
        {
            LogMethod();

            return Modbus.ReadHoldingRegister((ushort)Registers.SOFTWARE_VERSION);
        }

        public int ReadSerialNumber()
        {
            LogMethod();

            return (int)((Modbus.ReadHoldingRegister((ushort)Registers.MODEL_SERIE_HIGH) << 16) + Modbus.ReadHoldingRegister((ushort)Registers.MODEL_SERIE_LOW));           
        }

        public int ReadModelProduction()
        {
            LogMethod();

            return Modbus.ReadInt32((ushort)Registers.MODEL_PRODUCTION);
        }

        public ushort ReadKey()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.KEY);
        }

        public ushort ReadState()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.STATE);
        }


        public ushort ReadAlarmsInstant()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.ALARM_INSANT);
        }

        public Gains ReadGains(Lines line)
        {
            LogMethod();

            var addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.GAINSL1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.GAINSL2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.GAINSL3;
                    break;
            }

            return Modbus.ReadHolding<Gains>((ushort)addres);
        }
       
        public Offsets ReadOffsets(Lines line)
        {
            LogMethod();

            var addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.OFFSETSL1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.OFFSETSL2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.OFFSETSL3;
                    break;
            }

            return Modbus.ReadHolding<Offsets>((ushort)addres);
        }               

        //******************************************************************************

        public void WritePasswords(Passwords passwords, Lines line)
        {
            LogMethod();

            ushort addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.PASSWORDS_L1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.PASSWORDS_L2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.PASSWORDS_L3;
                    break;
            }
            Modbus.Write<Passwords>(addres, passwords);
            System.Threading.Thread.Sleep(200);

        }

        public void WriteGains(Gains gains, Lines line)
        {
            LogMethod();

            ushort addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.GAINSL1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.GAINSL2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.GAINSL3;
                    break;
            }
            Modbus.Write<Gains>(addres, gains);
            System.Threading.Thread.Sleep(200);
        }

        public void WriteAutoCalibrationAllLines(ushort autoCalibration)
        {
            LogMethod();

            WriteAutoCalibration(autoCalibration, Lines.L1);
            WriteAutoCalibration(autoCalibration, Lines.L2);
            WriteAutoCalibration(autoCalibration, Lines.L3);
        }

        public void WriteAutoCalibration(ushort autoCalibration, Lines line)
        {
            LogMethod();

            ushort addres = 0x0000;

            switch(line)
            {
                case Lines.L1: addres = (ushort)Registers.AUTO_CALIBRATION_L1;
                    break;
                case Lines.L2: addres = (ushort)Registers.AUTO_CALIBRATION_L2;
                    break;
                case Lines.L3: addres = (ushort)Registers.AUTO_CALIBRATION_L3;
                    break;
            }

            Modbus.Write(addres, autoCalibration);
            System.Threading.Thread.Sleep(200);
        }

        public void WriteAutoCalibrationPerido(ushort autoCalibrationPeriod)
        {
            LogMethod();

            Modbus.Write((ushort)Registers.AUTO_CALIBRATION_PERIOD, autoCalibrationPeriod);
        }
      

        public void WiteFactorsCalibration(AutoCalibrations calib)
        {
            LogMethod();

            Modbus.Write<AutoCalibrations>((ushort)Registers.AUTO_CALIBRATION_L1, calib);
            System.Threading.Thread.Sleep(200);

        }


        public ushort ReadAutoCalibration(Lines line)
        {
            LogMethod();

            ushort addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.AUTO_CALIBRATION_L1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.AUTO_CALIBRATION_L2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.AUTO_CALIBRATION_L3;
                    break;
            }
            System.Threading.Thread.Sleep(200);

            var vars = ReadVariables(line);
            _logger.InfoFormat(string.Format("Current Line {0} --> {1}", line.ToString(), vars.Current));         
            _logger.InfoFormat(string.Format("Pot.Active Line {0} --> {1}", line.ToString(), vars.ActivePower));
            _logger.InfoFormat(string.Format("Pot.Reactive Line {0} --> {1}", line.ToString(), vars.ReactivePower));
            _logger.InfoFormat(string.Format("Pot.Aparent Line {0} --> {1}", line.ToString(), vars.AparentPower));
            _logger.InfoFormat(string.Format("Power Factor Line {0} --> {1}", line.ToString(), vars.PowerFactor));

            var gainPhase = ReadGains(line).Phase;
            _logger.InfoFormat(string.Format("GAIN ANGLE {0} --> {1}", line.ToString(), gainPhase));

            return Modbus.ReadHoldingRegister(addres);
        }

        public int ReadPhase(Lines line)
        {
            LogMethod();

            ushort addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.PHASE_L1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.PHASE_L2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.PHASE_L3;
                    break;
            }

            return Modbus.ReadHoldingRegister((ushort)addres);
        }

        public void WritePhase(Lines line, ushort value)
        {
            LogMethod();

            ushort addres = 0x0000;

            switch (line)
            {
                case Lines.L1:
                    addres = (ushort)Registers.PHASE_L1;
                    break;
                case Lines.L2:
                    addres = (ushort)Registers.PHASE_L2;
                    break;
                case Lines.L3:
                    addres = (ushort)Registers.PHASE_L3;
                    break;
            }

            Modbus.Write(addres, value);

        }


        public Int32 ReadAutoCalibrationPerido()
        {
            LogMethod();

            return Modbus.ReadRegister((ushort)Registers.AUTO_CALIBRATION_PERIOD);
        }

        //******************************************************************************


        #region Enums

        public enum Registers
        {
           ID_MANUFACTURE = 2,  
           ID_PRODUCT_CODE = 4,
           ID_VERIFY = 6,
           HARDWARE_VERSION = 7,
           SOFTWARE_VERSION = 8,
           MODEL_SERIE_HIGH = 9,
            MODEL_SERIE_LOW = 10,
            MODEL_PRODUCTION = 11,
           TRANSFORMATION_CURRENT = 0x032,
           STATE = 0,
           ALARM_INSANT = 1,
           VOLTAGE_L1 = 2,
           COS_PHI_L1 = 38,
           COS_PHI_L2 = 138,
           COS_PHI_L3 = 238,
            ENERGIES = 60,
           PASSWORDS_L1 = 100,
            PASSWORDS_L2 = 300,
            PASSWORDS_L3 = 500,
            AUTO_CALIBRATION_L1 = 106,
           AUTO_CALIBRATION_L2 = 306,
           AUTO_CALIBRATION_L3 = 506,
           AUTO_CALIBRATION_PERIOD = 112,
           PHASE_L1 = 114,
            PHASE_L2 = 314,
            PHASE_L3 = 514,
            COUNTERS = 111,
           KEY = 101,
           GAINSL1 = 112,
           GAINSL2 = 312,
           GAINSL3 = 512,
           OFFSETSL1 = 125,
           OFFSETSL2 = 325,
           OFFSETSL3 = 525,

           VOLTAGE_L2 = 102,
           VOLTAGE_L3 = 202
        }


        public enum Lines
        {
            L1,
            L2,
            L3,
        }
        #endregion


        public override void Dispose()
        {
            Modbus.Dispose();
        }

        #region Structs                     

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Variables
        {
            private Int32 V;
            private Int32 I;
            private Int32 KW;
            private Int32 KVAR;
            private Int32 VA;
            private Int32 powerFactor;

            public double Voltage { get { return V / (double)10; } }
            public double Current { get { return I / (double)1000; } }
            public double ActivePower { get { return KW; } }
            public double ReactivePower { get { return KVAR; } }
            public double AparentPower { get { return VA; } }
            public double PowerFactor { get { return powerFactor / (double)1000; } }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MoreVariables
        {
            public Int32 CosPhi;
            private Int32 frecuency;

            public double Frecuency { get { return frecuency / (double)100; } }
        }       

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.ENERGIES)]
        public struct Energies
        {
            public Int32 EnergieActive;
            public Int32 EnergieReactiveInductive;
            public Int32 EnergieReactiveCapacitive;
            public Int32 EnergieAparent;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.AUTO_CALIBRATION_L1)]
        public struct AutoCalibrations
        {
            public ushort autoCalibration;
            public ushort AutoVoltage;
            public ushort AutoCurrent;
            public ushort AutoFrecuency;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.AUTO_CALIBRATION_L2)]
        public struct AutoCalibrationsL2
        {
            public ushort autoCalibration;
            public ushort AutoVoltage;
            public ushort AutoCurrent;
            public ushort AutoFrecuency;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.AUTO_CALIBRATION_L3)]
        public struct AutoCalibrationsL3
        {
            public ushort autoCalibration;
            public ushort AutoVoltage;
            public ushort AutoCurrent;
            public ushort AutoFrecuency;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.PASSWORDS_L1)]
        public struct Passwords
        {
            public ushort Password1;
            public ushort Password2;
            public ushort Password3;
            public ushort Password4;
            public ushort Password5;
            public ushort Password6;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.GAINSL1)]
        public struct GainsL1
        {
            public Int32 Voltage;
            public Int32 Current;
            public Int32 ActivePower;
            public Int32 ReactivePower;
            public Int32 AparentPower;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.GAINSL2)]
        public struct GainsL2
        {
            public Int32 Voltage;
            public Int32 Current;
            public Int32 ActivePower;
            public Int32 ReactivePower;
            public Int32 AparentPower;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        [ModbusLayout(Address = (ushort)Registers.GAINSL3)]
        public struct GainsL3
        {
            public Int32 Voltage;
            public Int32 Current;
            public Int32 ActivePower;
            public Int32 ReactivePower;
            public Int32 AparentPower;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Gains
        {
            public Int32 Frecuency;
            public ushort Phase; 
            public Int32 Voltage;
            public Int32 Current;
            public Int32 ActivePower;
            public Int32 ReactivePower;
            public Int32 AparentPower;           

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Offsets
        {
            public Int32 Voltage;
            public Int32 Current;
            public Int32 ActivePower;
            public Int32 ReactivePower;
            public Int32 AparentPower;
        }          
        #endregion
    }
}
