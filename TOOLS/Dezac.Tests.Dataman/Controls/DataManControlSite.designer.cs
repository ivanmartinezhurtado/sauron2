﻿namespace Dezac.Tests.Dataman.Controls
{
    partial class DataManControlSite
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataManControlSite));
            this.LedPanel = new System.Windows.Forms.GroupBox();
            this.lblOpResult = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblChecksum = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.lblPrgState = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblOp = new System.Windows.Forms.Label();
            this.lblSite = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Image1 = new System.Windows.Forms.PictureBox();
            this.lblDevice = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.goodLed = new Dezac.Tests.Dataman.Controls.LedIndicator();
            this.errorLed = new Dezac.Tests.Dataman.Controls.LedIndicator();
            this.busyLed = new Dezac.Tests.Dataman.Controls.LedIndicator();
            this.powerLed = new Dezac.Tests.Dataman.Controls.LedIndicator();
            this.LedPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            this.SuspendLayout();
            // 
            // LedPanel
            // 
            this.LedPanel.BackColor = System.Drawing.SystemColors.Control;
            this.LedPanel.Controls.Add(this.lblOpResult);
            this.LedPanel.Controls.Add(this.label3);
            this.LedPanel.Controls.Add(this.lblChecksum);
            this.LedPanel.Controls.Add(this.goodLed);
            this.LedPanel.Controls.Add(this.Label5);
            this.LedPanel.Controls.Add(this.errorLed);
            this.LedPanel.Controls.Add(this.lblPrgState);
            this.LedPanel.Controls.Add(this.busyLed);
            this.LedPanel.Controls.Add(this.Label9);
            this.LedPanel.Controls.Add(this.powerLed);
            this.LedPanel.Controls.Add(this.progressBar);
            this.LedPanel.Controls.Add(this.lblOp);
            this.LedPanel.Controls.Add(this.lblSite);
            this.LedPanel.Controls.Add(this.Label11);
            this.LedPanel.Controls.Add(this.Image1);
            this.LedPanel.Controls.Add(this.lblDevice);
            this.LedPanel.Controls.Add(this.Label7);
            this.LedPanel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LedPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LedPanel.Location = new System.Drawing.Point(3, 3);
            this.LedPanel.Name = "LedPanel";
            this.LedPanel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LedPanel.Size = new System.Drawing.Size(154, 393);
            this.LedPanel.TabIndex = 3;
            this.LedPanel.TabStop = false;
            // 
            // lblOpResult
            // 
            this.lblOpResult.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblOpResult.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOpResult.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOpResult.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOpResult.Location = new System.Drawing.Point(8, 366);
            this.lblOpResult.Name = "lblOpResult";
            this.lblOpResult.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblOpResult.Size = new System.Drawing.Size(140, 17);
            this.lblOpResult.TabIndex = 80;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(6, 349);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 79;
            this.label3.Text = "Op result:";
            // 
            // lblChecksum
            // 
            this.lblChecksum.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblChecksum.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblChecksum.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblChecksum.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblChecksum.Location = new System.Drawing.Point(8, 289);
            this.lblChecksum.Name = "lblChecksum";
            this.lblChecksum.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblChecksum.Size = new System.Drawing.Size(140, 17);
            this.lblChecksum.TabIndex = 78;
            // 
            // Label5
            // 
            this.Label5.BackColor = System.Drawing.SystemColors.Control;
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label5.Location = new System.Drawing.Point(7, 272);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(114, 17);
            this.Label5.TabIndex = 77;
            this.Label5.Text = "Checksum:";
            // 
            // lblPrgState
            // 
            this.lblPrgState.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblPrgState.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblPrgState.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPrgState.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPrgState.Location = new System.Drawing.Point(8, 255);
            this.lblPrgState.Name = "lblPrgState";
            this.lblPrgState.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblPrgState.Size = new System.Drawing.Size(140, 17);
            this.lblPrgState.TabIndex = 76;
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.SystemColors.Control;
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label9.Location = new System.Drawing.Point(8, 238);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(140, 17);
            this.Label9.TabIndex = 75;
            this.Label9.Text = "State:";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(8, 184);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(140, 16);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 10;
            this.progressBar.Visible = false;
            // 
            // lblOp
            // 
            this.lblOp.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblOp.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOp.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOp.Location = new System.Drawing.Point(8, 327);
            this.lblOp.Name = "lblOp";
            this.lblOp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblOp.Size = new System.Drawing.Size(140, 17);
            this.lblOp.TabIndex = 52;
            // 
            // lblSite
            // 
            this.lblSite.BackColor = System.Drawing.SystemColors.Control;
            this.lblSite.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblSite.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSite.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSite.Location = new System.Drawing.Point(8, 8);
            this.lblSite.Name = "lblSite";
            this.lblSite.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblSite.Size = new System.Drawing.Size(129, 17);
            this.lblSite.TabIndex = 4;
            this.lblSite.Text = "Site #1";
            this.lblSite.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label11
            // 
            this.Label11.BackColor = System.Drawing.SystemColors.Control;
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label11.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label11.Location = new System.Drawing.Point(6, 310);
            this.Label11.Name = "Label11";
            this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label11.Size = new System.Drawing.Size(88, 17);
            this.Label11.TabIndex = 51;
            this.Label11.Text = "Operation:";
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(8, 24);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(56, 159);
            this.Image1.TabIndex = 5;
            this.Image1.TabStop = false;
            // 
            // lblDevice
            // 
            this.lblDevice.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblDevice.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDevice.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDevice.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDevice.Location = new System.Drawing.Point(8, 220);
            this.lblDevice.Name = "lblDevice";
            this.lblDevice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDevice.Size = new System.Drawing.Size(140, 17);
            this.lblDevice.TabIndex = 50;
            // 
            // Label7
            // 
            this.Label7.BackColor = System.Drawing.SystemColors.Control;
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label7.Location = new System.Drawing.Point(6, 203);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(49, 17);
            this.Label7.TabIndex = 22;
            this.Label7.Text = "Device:";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // goodLed
            // 
            this.goodLed.BackColor = System.Drawing.Color.Gray;
            this.goodLed.CaptionText = "GOOD";
            this.goodLed.ColorOff = System.Drawing.Color.DarkGreen;
            this.goodLed.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.goodLed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goodLed.ForeColor = System.Drawing.SystemColors.Control;
            this.goodLed.IsOn = false;
            this.goodLed.Location = new System.Drawing.Point(71, 97);
            this.goodLed.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.goodLed.Name = "goodLed";
            this.goodLed.Size = new System.Drawing.Size(73, 17);
            this.goodLed.TabIndex = 14;
            // 
            // errorLed
            // 
            this.errorLed.BackColor = System.Drawing.Color.Gray;
            this.errorLed.CaptionText = "ERROR";
            this.errorLed.ColorOff = System.Drawing.Color.Olive;
            this.errorLed.ColorOn = System.Drawing.Color.Yellow;
            this.errorLed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorLed.ForeColor = System.Drawing.SystemColors.Control;
            this.errorLed.IsOn = false;
            this.errorLed.Location = new System.Drawing.Point(71, 120);
            this.errorLed.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.errorLed.Name = "errorLed";
            this.errorLed.Size = new System.Drawing.Size(73, 17);
            this.errorLed.TabIndex = 13;
            // 
            // busyLed
            // 
            this.busyLed.BackColor = System.Drawing.Color.Gray;
            this.busyLed.CaptionText = "BUSY";
            this.busyLed.ColorOff = System.Drawing.Color.Maroon;
            this.busyLed.ColorOn = System.Drawing.Color.Red;
            this.busyLed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.busyLed.ForeColor = System.Drawing.SystemColors.Control;
            this.busyLed.IsOn = false;
            this.busyLed.Location = new System.Drawing.Point(71, 74);
            this.busyLed.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.busyLed.Name = "busyLed";
            this.busyLed.Size = new System.Drawing.Size(73, 17);
            this.busyLed.TabIndex = 12;
            // 
            // powerLed
            // 
            this.powerLed.BackColor = System.Drawing.Color.Gray;
            this.powerLed.CaptionText = "POWER";
            this.powerLed.ColorOff = System.Drawing.Color.DarkGreen;
            this.powerLed.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.powerLed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.powerLed.ForeColor = System.Drawing.SystemColors.Control;
            this.powerLed.IsOn = false;
            this.powerLed.Location = new System.Drawing.Point(71, 29);
            this.powerLed.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.powerLed.Name = "powerLed";
            this.powerLed.Size = new System.Drawing.Size(73, 17);
            this.powerLed.TabIndex = 11;
            // 
            // DataManControlSite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LedPanel);
            this.Name = "DataManControlSite";
            this.Size = new System.Drawing.Size(160, 400);
            this.LedPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox LedPanel;
        internal System.Windows.Forms.ProgressBar progressBar;
        public System.Windows.Forms.Label lblSite;
        public System.Windows.Forms.PictureBox Image1;
        private LedIndicator powerLed;
        private LedIndicator errorLed;
        private LedIndicator busyLed;
        private LedIndicator goodLed;
        public System.Windows.Forms.Label Label7;
        public System.Windows.Forms.Label lblDevice;
        public System.Windows.Forms.Label lblOp;
        public System.Windows.Forms.Label Label11;
        public System.Windows.Forms.Label lblPrgState;
        public System.Windows.Forms.Label Label9;
        public System.Windows.Forms.Label lblChecksum;
        public System.Windows.Forms.Label Label5;
        public System.Windows.Forms.Label lblOpResult;
        public System.Windows.Forms.Label label3;
    }
}
