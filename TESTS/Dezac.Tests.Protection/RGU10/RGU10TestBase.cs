﻿using Dezac.Core.Enumerate;
using Dezac.Core.Utility;
using Dezac.Device.Protection;
using Dezac.Tests.Extensions;
using Dezac.Tests.Model;
using Dezac.Tests.Parameters;
using Dezac.Tests.Services;
using Dezac.Tests.UserControls;
using Dezac.Tests.Utils;
using Instruments.IO;
using Instruments.Measure;
using Instruments.Towers;
using Instruments.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace Dezac.Tests.Protection
{
    [TestVersion(2.18)]
    public class RGU10TestBase : TestBase
    {
        #region Variables y clases auxiliares

        // Salidas de la manguera
        protected const byte OUT_SWITCH_COM5_TO_COM4 = 15;//
        protected const byte OUT_EXTERNAL_TRIGGER = 19; //
        protected const byte OUT_CORRIENTE_DC_1ESPIRA = 20;
        protected const byte OUT_CORRIENTE_DC_5ESPIRA = 21;
        protected const byte OUT_CIRCUITO_DC = 23;//
        protected const byte OUT_CORRIENTE_L1 = 28;//
        protected const byte OUT_CORRIENTE_L2 = 29;//
        protected const byte OUT_ILUMINACION = 59;//

        // Salidas teclado
        protected const byte OUT_TECLA_TD = 6; //
        protected const byte OUT_TECLA_ID = 7;//
        protected const byte OUT_TECLA_STD = 8;//
        protected const byte OUT_TECLA_SET = 9;//
        protected const byte OUT_TECLA_PROG = 10;//
        protected const byte OUT_TECLA_TEST = 11;//
        protected const byte OUT_TECLA_RESET = 12;//

        // Salidas conexión de puntas al equipo 
        protected const byte OUT_ACTIVACION_ELECTROVALVULAS = 5;//
        protected const byte OUT_BORNERA_IZQUIERDA = 48;//
        protected const byte OUT_BORNERA_DERECHA = 49;//
        protected const byte OUT_PUNTAS_COM_SERIE = 50;//
        protected const byte OUT_PUNTAS_TTL = 51;//
        protected const byte OUT_PUNTAS_TTL_BORNES_17 = 52;//
        protected const byte OUT_PUNTAS_TTL_BORNES_18 = 53;//
        protected const byte OUT_PUNTAS_TTL_BORNES_19 = 54;//
        protected const byte OUT_PUNTAS_TTL_BORNES_20 = 55;//
        protected const byte OUT_PUNTAS_TTL_BORNES_21 = 56;//

        protected const byte OUT_FIJACION_CARRO = 57;//

        // Salidas USB Advantech
        protected const byte OUT_ADVANTECH_MODELO_MT = 2;//
        protected const byte OUT_ADVANTECH_TORO_500 = 3;//
        protected const byte OUT_ADVANTECH_TORO_B = 4;//
        protected const byte OUT_ADVANTECH_TORO_ABIERTO = 5;//
        protected const byte OUT_ADVANTECH_TORO_CRUZADO = 6;//
        protected const byte OUT_ADVANTECH_MODELO_B = 0;//

        // Entradas de la mangera
        protected const byte INPUT_NC_PREALARM_RELAY = 9;//
        protected const byte INPUT_NO_PREALARM_RELAY = 10;//
        protected const byte INPUT_NO_TRIGGER_RELAY = 11;//
        protected const byte INPUT_NC_TRIGGER_RELAY = 12;//
        protected const byte INPUT_NO_PREALARM_MT_RELAY = 10;//
        protected const byte INPUT_NC_PREALARM_MT_RELAY = 11;//
        protected const byte INPUT_OFF_MOTOR_MT_RELAY = 12;//
        protected const byte INPUT_ON_MOTOR_MT_RELAY = 13;//
        protected const byte INPUT_NC_TRIGGER_MT_RELAY = 9;//

        //Entradas presencia equipo 
        protected const byte BLOQ_SECURITY = 23;//
        protected const byte IN_UTIL_CERRADO = 25;//
        protected const byte IN_DETECT_UUT = 26;//
        protected const byte IN_DETECT_COVER = 24;//

        //Entradas Estado Pisto
        protected const byte IN_ESTADO_PUNTAS_TTL = 30;//
        protected const byte IN_ESTADO_PUNTAS_COM_SERIE = 31;//
        protected const byte IN_ESTADO_PUNTAS_DERECHA = 32;//
        protected const byte IN_ESTADO_PUNTAS_IZQUIERDA = 33;//

        // Entradas Mux
        protected const InputMuxEnum TOROIDAL_COMMON = InputMuxEnum.IN9; // Salida corriente
        protected const InputMuxEnum TOROIDAL_CHANNEL_1 = InputMuxEnum.IN4; //

        #endregion

        private Tower3 tower;
        private RGU10 rgu;
        private string CAMERA_IDS;
        private USB4761 usbIO;

        public USB4761 UsbIO
        {
            get
            {
                if (usbIO == null)
                    usbIO = new USB4761();

                return usbIO;
            }
        }
        public RGU10 Rgu
        {
            get
            {
                if (rgu == null)
                    rgu = new RGU10(Comunicaciones.SerialPort);

                return rgu;
            }
        }
        public Tower3 Tower
        {
            get
            {
                var cacheSvc = this.GetService<ICacheService>();
                if (cacheSvc == null || !cacheSvc.TryGet("TOWER", out tower))
                {
                    tower = AddOrGetSharedVar<Tower3>("TOWER", () => { return new Tower3(); });
                    if (cacheSvc != null)
                        cacheSvc.Add("TOWER", tower);
                }
                return tower;
            }
        }

        public byte CurrentPosition { get; set; }

        public byte TimePosition { get; set; }

        public void TestInitialization()
        {
            Rgu.Modbus.BaudRate = Comunicaciones.BaudRate;
            Rgu.Modbus.PerifericNumber = Comunicaciones.Periferico;
            Rgu.Modbus.UseFunction06 = true;
            Rgu.ValorFinTabla = (ushort)Parametrizacion.GetDouble("VALOR_FIN_TABLA", 0, ParamUnidad.SinUnidad);

            Rgu.TimeWaitCom =(int)Configuracion.GetDouble("TIEMPO_ESPERA_COM", 100, ParamUnidad.ms);

            Tower.MUX.Reset();
            Tower.Active24VDC();
            Tower.ActiveElectroValvule();
            Tower.ActiveVoltageCircuit();
            Tower.ActiveCurrentCircuit(false, false, false);
            Tower.IO.DO.On(OUT_ACTIVACION_ELECTROVALVULAS);

            SamplerWithCancel((p) =>
            {
                if (!Tower.IO.DI[BLOQ_SECURITY])
                    return false;
                return true;
            }, "Error: No se ha cerrado el utillaje.", 40, 500, 2500, false, false);

            Tower.IO.DO.On(OUT_FIJACION_CARRO);

            this.InParellel(() =>
            {
                Tower.PowerSourceIII.ApplyOff();
            },
            () =>
            {
                Tower.Chroma.ApplyOff();
            },
            () =>
            {
                Tower.LAMBDA.ApplyOff();
            });

            UsbIO.DO.OffRange(0, 6);

            if (Identificacion.MODELO == "B")
            {
                UsbIO.DO.On(OUT_ADVANTECH_MODELO_B);
                UsbIO.DO.On(OUT_ADVANTECH_TORO_B);
                Delay(500, "");
            }
            else
            {
                if ((int)VectorHardware.GetDouble("NUMERO_RELES", 2, ParamUnidad.SinUnidad) == 4)
                {
                    UsbIO.DO.On(OUT_ADVANTECH_MODELO_MT);
                    Delay(500, "");

                    if (UsbIO.DI.Read(OUT_ADVANTECH_MODELO_MT).Value == false)
                        throw new Exception("No se ha activado el modulo USB que commuta a modelo RAL/MT");
                }
            }

            Tower.IO.DO.On(OUT_BORNERA_IZQUIERDA, OUT_BORNERA_DERECHA);
            Tower.IO.DI.WaitAllOn(2000, IN_ESTADO_PUNTAS_DERECHA, IN_ESTADO_PUNTAS_IZQUIERDA);

            switch (Comunicaciones.SerialPort)
            {
                case 5:
                case 4:
                    Tower.IO.DO.On(OUT_PUNTAS_TTL);
                    Tower.IO.DI.WaitAllOn(2000, IN_ESTADO_PUNTAS_TTL);
                    if (Identificacion.MODELO == "B")
                        Tower.IO.DO.On(OUT_PUNTAS_TTL_BORNES_17, OUT_PUNTAS_TTL_BORNES_18, OUT_PUNTAS_TTL_BORNES_19);
                    else
                        Tower.IO.DO.On(OUT_PUNTAS_TTL_BORNES_17, OUT_PUNTAS_TTL_BORNES_19, OUT_PUNTAS_TTL_BORNES_21);
                    
                    if (Comunicaciones.SerialPort == 5)
                        Tower.IO.DO.Off(OUT_SWITCH_COM5_TO_COM4);
                    else
                        Tower.IO.DO.On(OUT_SWITCH_COM5_TO_COM4);
                    break;
                case 7:
                    Tower.IO.DO.On(OUT_PUNTAS_COM_SERIE);
                    Tower.IO.DI.WaitOn(IN_ESTADO_PUNTAS_COM_SERIE, 2000);
                    break;
            }

            Assert.IsTrue(Tower.IO.DI[IN_DETECT_UUT], Error().UUT.HARDWARE.NO_COMUNICA("Error No se ha detectado el equipo o el equipo no lleva el ancla puesta"));

            Assert.IsTrue(Tower.IO.DI[IN_DETECT_COVER], Error().UUT.HARDWARE.SETUP("Error El equipo no lleva la tapa puesta"));
        }

        public void TestConsumption([Description("Tiempo espera estabilización consumo")]int timeWait = 1000)
        {
            var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);

            var configuration = new TestPointConfiguration()
            {
                Current = 0,
                Frecuency = 50,
                Iteraciones = 5,
                ResultName = "",
                TiempoIntervalo = 100,
                typePowerSource = typeSupply == "AC" ? TypePowerSource.CHROMA : TypePowerSource.LAMBDA,
                typeTestExecute = typeSupply == "AC" ? TypeTestExecute.ActiveAndReactive : TypeTestExecute.OnlyActive,
                typeTestPoint = TypeTestPoint.EnVacio,
                typeTestVoltage = TypeTestVoltage.VoltageNominal,
                Voltage = 0,
                WaitTimeReadMeasure = timeWait
            };

            if (configuration.typePowerSource == TypePowerSource.LAMBDA)
                tower.IO.DO.On(OUT_CIRCUITO_DC);

            this.TestConsumo(Tower, configuration);
        }

        public void TestCommunications()
        {
            bool parallelReset = Parametrizacion.GetString("PARALLEL_RESET", "NO", ParamUnidad.SinUnidad) == "SI";

            PowerSourceResetAndApplyPrsetes(parallelReset);

            SamplerWithCancel((p) =>
            {
                Rgu.WriteFlagTest();
                return true;
            }, "Error de comunicaciones, el equipo no arranca o no comunica", 10, 500, 500, false, false);

            //Rgu.WriteCuttingType(RGU10.CuttingTypes.MAG);

            CultureInfo enUS = new CultureInfo("en-US");

            Delay(1000,"Espera para leer version, equipos RS-232 lee 0.00");

            double firmwareVersion = Rgu.ReadFirmwareVersion();
            TestInfo.FirmwareVersion = (firmwareVersion / 10).ToString("0.0", enUS);
            TestInfo.ProductoVersion = string.Format("{0}/{1}", Model.NumProducto, Model.Version);

            Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE, TestInfo.FirmwareVersion, Identificacion.VERSION_FIRMWARE, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware incorrecta"), ParamUnidad.SinUnidad);

            if (Comunicaciones.SerialPort == 7)
            {
                double firmwareVersionPlacaLateral = Rgu.ReadCommunicationsBoardVersion();
                var stringfirmwareVersionPlacaLateral = (firmwareVersionPlacaLateral / 10).ToString("0.0", enUS);
                Assert.AreEqual(ConstantsParameters.Identification.VERSION_FIRMWARE_PLACA_COM, stringfirmwareVersionPlacaLateral, Identificacion.VERSION_FIRMWARE_PLACA_COM, Error().SOFTWARE.FIRMWARE.VERSION_INCORRECTA("Error, version de firmware placa comunicaciones incorrecta"), ParamUnidad.SinUnidad);
            }
        }

        public virtual void TestSetupCustomer()
        {
            WriteCostumerTableCurrentTime();

            WriteCustomizeRelaySecurity();

            WriteCustomizeFrequency();
        }

        public void TestLeds()
        {
            var timeWait = 1000;

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);
             
            Rgu.WriteFlagTest();

            Rgu.WriteDisplay(RGU10.DisplayOperations.TURN_OFF_ALL_DIGITS);

            Delay(500, "Espera condicionamiento de los LEDS");

            Rgu.WriteDisplay(RGU10.DisplayOperations.INVERT_SEGMENTS);

            Delay(500, "Espera condicionamiento de los LEDS");

            Rgu.WriteLeds(RGU10.Leds.TURN_OFF_BACKLIGHT_POWER_PREALARM_LEDS);

            Delay(timeWait, "Espera condicionamiento de los LEDS");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "RGU10_LED_VERDE", "LED_VERDE", "RGU10");

            Rgu.WriteDisplay(RGU10.DisplayOperations.TURN_OFF_ALL_DIGITS);

            Delay(500, "Espera condicionamiento de los LEDS");

            Rgu.WriteLeds(RGU10.Leds.ORANGE_RED_LED_RED_BACKLIGHT);

            Delay(timeWait, "Espera condicionamiento de los LEDS");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.FindLeds, CAMERA_IDS, "RGU10_LED_ROJO_NARANJA", "LED_ROJO_NARANJA", "RGU10");

            Rgu.WriteDisplay(RGU10.DisplayOperations.TURN_OFF_ALL_DIGITS);
        }

        public void TestDisplay()
        {
            var timeWait = 1500;

            CAMERA_IDS = GetVariable<string>("CAMARA_IDS", CAMERA_IDS);

            Rgu.WriteFlagTest();

            Rgu.WriteLeds(RGU10.Leds.TURN_OFF_BACKLIGHT_POWER_PREALARM_LEDS);

            Rgu.WriteDisplay(RGU10.DisplayOperations.TURN_OFF_ALL_DIGITS);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, "RGU10_OFF", "OFF", "RGU10");

            Rgu.WriteDisplay(RGU10.DisplayOperations.TURN_ON_ALL_DIGITS);

            Delay(timeWait, "Espera condicionamiento del display");

            var procedure = (Identificacion.MODELO == "STD") || Identificacion.MODELO == "RAL" ? "" : "CC_";

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, string.Format("RGU10_{0}SYMBOL_SEGMENTS", procedure), Identificacion.MODELO + "_SYMBOL_SEGMENTS", "RGU10");

            Rgu.WriteDisplay(RGU10.DisplayOperations.INVERT_SEGMENTS);

            Delay(timeWait, "Espera condicionamiento del display");

            this.TestHalconCaptureAsync(TestHalconExtension.TypeProcedure.PaternMatching, CAMERA_IDS, string.Format("RGU10_{0}NUMERIC_SEGMENTS", procedure), Identificacion.MODELO + "_NUMERIC_SEGMENTS", "RGU10");

            Rgu.WriteDisplay(RGU10.DisplayOperations.TURN_OFF_ALL_DIGITS);
        }

        public void TestKeyboard()
        {
            var keyboardTowerOutputsDictionary = new Dictionary<RGU10.Keyboard, int?>()
            {
                {RGU10.Keyboard.AUTO_KEY, OUT_TECLA_SET},
                {RGU10.Keyboard.CURRENT_SELECTION_KEY, OUT_TECLA_ID},
                {RGU10.Keyboard.POLARITY_SELECTION_KEY, OUT_TECLA_STD},
                {RGU10.Keyboard.PROG_KEY, OUT_TECLA_PROG},
                {RGU10.Keyboard.RESET_KEY, OUT_TECLA_RESET},
                {RGU10.Keyboard.TEST_KEY, OUT_TECLA_TEST},
                {RGU10.Keyboard.TIME_SELECTION_KEY, OUT_TECLA_TD},
            };

            Tower.IO.DO.Off(OUT_TECLA_SET, OUT_TECLA_ID, OUT_TECLA_PROG, OUT_TECLA_RESET, OUT_TECLA_STD, OUT_TECLA_TD, OUT_TECLA_TEST);

            Rgu.ResetTrigger();

            Rgu.WriteFlagTest();

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

            foreach (KeyValuePair<RGU10.Keyboard, int?> keyboard in keyboardTowerOutputsDictionary)
            {
                Rgu.ResetTrigger();

                SamplerWithCancel((p) =>
                {
                    if (keyboard.Value.HasValue)
                        Tower.IO.DO.OnWait(150, keyboard.Value.Value);

                    var result = Rgu.ReadKeyboard().HasFlag(keyboard.Key);

                    if (keyboard.Value.HasValue)
                        Tower.IO.DO.OffWait(150, keyboard.Value.Value);

                    var result2 = !Rgu.ReadKeyboard().HasFlag(keyboard.Key);

                    Resultado.Set(keyboard.Key.ToString(), result == true ? "OK" : "ERROR", ParamUnidad.SinUnidad);

                    return result && result2;

                }, string.Format("Error tecla {0} no detectada al activarla o encallada al desactivarla", keyboard.Key.GetDescription()));
            }
        }

        public virtual void TestExternalInput()
        {
            Rgu.WriteFlagTest();

            Tower.IO.DO.On(OUT_EXTERNAL_TRIGGER);

            SamplerWithCancel((p) =>
            {
                if (Identificacion.MODELO == "B")
                {
                    var inputsB = ((int)Rgu.ReadInputs() & 0x0080);
                    return inputsB == (int)RGU10.Inputs_B.EXTERNAL_INPUT;
                }
                var inputs = ((int)Rgu.ReadInputs() & 0x0F00);
                return inputs == (int)RGU10.Inputs.EXTERNAL_INPUT;

            }, "Error. No se detecta correctamente la señal externa.", 3, 1000, 1000, false, false);

            Tower.IO.DO.Off(OUT_EXTERNAL_TRIGGER);

            SamplerWithCancel((p) =>
            {
                if (Identificacion.MODELO == "B")
                {
                    var inputsB = ((int)Rgu.ReadInputs() & 0x0080);
                    return inputsB == (int)RGU10.Inputs_B.NO_INPUT;
                }
                var inputs = ((int)Rgu.ReadInputs() & 0x0F00);
                return inputs == (int)RGU10.Inputs.NO_INPUT;

            }, "Error. la señal externa sigue activada", 3, 1000, 1000, false, false);

            Resultado.Set("EXTERNA_INPUT", "OK", ParamUnidad.SinUnidad);
        }

        public virtual void TestRelays()
        {
            Rgu.WriteFlagTest();

            SamplerWithCancel((p) =>
            {
                bool result;

                Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

                Delay(150, "Espera activación rele");

                if (Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                    result = !Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;
                else
                    result = Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && !Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;

                if (result)
                {
                    Rgu.WriteRelays(RGU10.Relays.PREALARM_RELAY);

                    Delay(150, "Espera activación rele");

                    if (Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                        result = Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && !Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;
                    else
                        result = !Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;

                    if (result)
                    {
                        Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

                        Delay(150, "Espera activación rele");

                        if (Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                            result = !Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;
                        else
                            result = Tower.IO.DI.Read(INPUT_NO_PREALARM_RELAY).Value && !Tower.IO.DI.Read(INPUT_NC_PREALARM_RELAY).Value;
                    }
                }

                return result;

            }, "Error, No se detecta la des/activación del relé de prealarma", 3, 500, 0);

            Resultado.Set("PREALARM_RELAY", "OK", ParamUnidad.SinUnidad);

            SamplerWithCancel((p) =>
            {
                bool result;

                Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

                Delay(150, "Espera activación rele");

                if (Parametrizacion.GetString("SEGURIDAD_RELE_TRIP_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                    result = !Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];
                else
                    result = Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && !Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];

                if (result)
                {
                    Rgu.WriteRelays(RGU10.Relays.TRIGGER_RELAY);

                    Delay(150, "Espera activación rele");

                    if (Parametrizacion.GetString("SEGURIDAD_RELE_TRIP_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                        result = Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && !Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];
                    else
                        result = !Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];

                    if (result)
                    {
                        Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);

                        Delay(150, "Espera activación rele");

                        if (Parametrizacion.GetString("SEGURIDAD_RELE_TRIP_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                            result = !Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];
                        else
                            result = Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && !Tower.IO.DI[INPUT_NC_TRIGGER_RELAY];
                    }
                }

                return result;
            }, "Error, No se detecta la activación del relé de disparo", 3, 500, 0);

            Resultado.Set("TRIGGER_RELAY", "OK", ParamUnidad.SinUnidad);

            Rgu.WriteRelays(RGU10.Relays.TURN_OFF_RELAYS);
        }

        public void TestToroidal()
        {
            Rgu.WriteNormalMode();
            Delay(1000, "");
            Rgu.WriteNormalMode();

            var testsToroiddal = new Dictionary<int, RGU10.Toroidal>()
            {
                {0, RGU10.Toroidal.TOROIDAL_OK},
                {OUT_ADVANTECH_TORO_ABIERTO, RGU10.Toroidal.TOROIDAL_ERROR},
            };

            if (Identificacion.MODELO.Trim().ToUpper() == "CC")
                testsToroiddal.Add(OUT_ADVANTECH_TORO_CRUZADO, RGU10.Toroidal.TOROIDAL_OK);

            foreach (KeyValuePair<int, RGU10.Toroidal> testToroiddal in testsToroiddal)
            {
                var modoTest = string.Format("TOROIDAL_{0}", testToroiddal.Key == OUT_ADVANTECH_TORO_CRUZADO ? "CORTOCIRCUITADO" : "CIRCUITO_ABIERTO");

                UsbIO.DO.Off(OUT_ADVANTECH_TORO_ABIERTO, OUT_ADVANTECH_TORO_CRUZADO);

                Delay(1000, "Espera test toroidal");

                if (testToroiddal.Value == RGU10.Toroidal.TOROIDAL_ERROR)
                    UsbIO.DO.On(testToroiddal.Key);
                Delay(6000, "Espera test toroidal");
                SamplerWithCancel((s) =>
                {
                    if (s % 3 == 0)
                    {
                        Rgu.ResetTrigger();
                        Delay(5000, "Espera triger");
                    }
                    
                    var toroidalReading = Rgu.ReadToroidalStatus();

                    return toroidalReading == testToroiddal.Value;

                }, string.Format("Error en la detección {0}", modoTest), 7, 1500, 0);

                if (testToroiddal.Value == RGU10.Toroidal.TOROIDAL_ERROR)
                {
                    SamplerWithCancel((p) =>
                    {
                        if ((Identificacion.SUBMODELO.Trim().ToUpper() == "SIEMENS") && (Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")) 
                            Assert.IsTrue(Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && !Tower.IO.DI[INPUT_NC_TRIGGER_RELAY], Error().UUT.DISPARO.NO_DISPARA("Error el equipo no ha disparado en la prueba de toroidal"));
                        if ((Identificacion.SUBMODELO.Trim().ToUpper() == "SIEMENS") && (Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "POSITIVA"))
                            Assert.IsTrue(!Tower.IO.DI[INPUT_NO_TRIGGER_RELAY] && Tower.IO.DI[INPUT_NC_TRIGGER_RELAY], Error().UUT.DISPARO.NO_DISPARA("Error el equipo no ha disparado en la prueba de toroidal"));
                        return true;
                    }, "", 3, 500, 500,false,false);

                    Resultado.Set(modoTest, "OK", ParamUnidad.SinUnidad);
                }
            }

            UsbIO.DO.Off(OUT_ADVANTECH_TORO_ABIERTO, OUT_ADVANTECH_TORO_CRUZADO, OUT_ADVANTECH_TORO_500);
        }

        public void TestAdjust()
        {
            var currentScala1 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_1.Name, 0.6, ParamUnidad.A), 0 };
            var currentScala2 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_2.Name, 3, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.Ajuste.esc_2.Name, 0, ParamUnidad.A) };
            var currentScala3 = new double[] { Consignas.GetDouble(Params.I.L1.Ajuste.esc_3.Name, 0, ParamUnidad.A), Consignas.GetDouble(Params.I.L2.Ajuste.esc_3.Name, 1.5, ParamUnidad.A) };
            var currentScala4 = new double[] { 0, Consignas.GetDouble(Params.I.L2.Ajuste.esc_4.Name, 10, ParamUnidad.A) };

            var currentDictionaryTemporal = new Dictionary<RGU10.ScaleCalibration, double[]>()
            {
                {RGU10.ScaleCalibration.SCALE_1,currentScala1},
                {RGU10.ScaleCalibration.SCALE_2,currentScala2},
                {RGU10.ScaleCalibration.SCALE_3,currentScala3},
            };

            var sinEscala4 = Identificacion.MODELO.Trim().ToUpper() == "CC" || Identificacion.MODELO.Trim().ToUpper() == "B";
            if (!sinEscala4)
                currentDictionaryTemporal.Add(RGU10.ScaleCalibration.SCALE_4, currentScala4);

            InternalAdjust(currentDictionaryTemporal);
        }
        
        public virtual void TestVerification([Description("Posicion de la corriente que se seleccionar para probar el disparo")]byte positionCurrent = 0, [Description("Posicion del tiempo que se seleccionar para probar el disparo")]byte positionTime = 2)
        {
            Tower.IO.DO.On(OUT_CORRIENTE_L1);

            var currentTrigger = new double[] { Consignas.GetDouble(Params.I.L1.TRIGGER.esc_1.Name, 0.06, ParamUnidad.mA), Consignas.GetDouble(Params.I.L2.TRIGGER.esc_1.Name, 0, ParamUnidad.mA) };

            this.InParellel(() =>
            {
                bool parallelReset = Parametrizacion.GetString("PARALLEL_RESET", "NO", ParamUnidad.SinUnidad) == "SI";

                PowerSourceResetAndApplyPrsetes(parallelReset);

                Rgu.WriteNormalMode();
                Rgu.WriteAllowParametersWriting();
                Rgu.WriteTriggerCurrentSelection(CurrentPosition);
                Rgu.WriteTriggerTimeSelection(TimePosition);
                Rgu.WriteSetupWriting();
            },
            () =>
            {
                Tower.MUX.Reset();
                Tower.MUX.Connect2((int)TOROIDAL_COMMON, Outputs.FreCH1, (int)TOROIDAL_CHANNEL_1, Outputs.FreCH2);

                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH1, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);
                Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH1, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.2);

                Tower.HP53131A.PresetConfigImpendance(HP53131A.Channels.CH2, HP53131A.Impedancia.CoupEnum.DC, HP53131A.Impedancia.AttenEnum.X10, HP53131A.Impedancia.FilterEnum.Filt_ON, HP53131A.Impedancia.ImpedanciaEnum.OHM_1M);

                if (Parametrizacion.GetString("SEGURIDAD_RELE_TRIP_HW", "ESTANDAR", ParamUnidad.SinUnidad) == "ESTANDAR")
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.POS, HP53131A.Trigger.SensibilitiEnum.HI, 1.2);
                else
                    Tower.HP53131A.PresetConfigTrigger(HP53131A.Channels.CH2, HP53131A.Trigger.LevelEnum.NUM_V_OFF, HP53131A.Trigger.SlopeEnum.NEG, HP53131A.Trigger.SensibilitiEnum.HI, 0.1);
            },
            () =>
            {
                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, TriLineValue.Create(currentTrigger[0], currentTrigger[1], 0), 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });
            });

            Tower.MUX.Connect2((int)TOROIDAL_COMMON, Outputs.FreCH1, (int)InputMuxEnum.IN2, Outputs.FreCH2);

            var adjustValue = new AdjustValueDef(Params.TIME.Other("TRIGGER").Verificacion.esc_1, ParamUnidad.s);
            var adjustResult = TestMeasureBase(adjustValue, (Func<int, double>)((s) =>
            {
                var timeResult = Tower.HP53131A.RunMeasure(HP53131A.Variables.TINT, 3000, HP53131A.Channels.CH1, (Action)(() =>
                {
                    Delay(500, "Espera disparo");
                    Tower.IO.DO.Off(OUT_CORRIENTE_L1);
                }));
                return (timeResult.Value * 1000);
            }), 0, 1, 1000);

            Delay(1000, "Espera");

            var adjustValueCurrent = new AdjustValueDef(Params.I.Other("TRIGGER").Verificacion.esc_1, ParamUnidad.mA);
            adjustResult = TestMeasureBase(adjustValueCurrent, (s) =>
            {
                var currentReading = Rgu.ReadCurrent();
                return currentReading;
            }, 0, 8, 500);

            Tower.IO.DO.On(OUT_CORRIENTE_L1);
        }

        public virtual void TestCustomization()
        {
            Rgu.WriteFlagTest();

            rgu.WriteFrameNumber((int)TestInfo.NumBastidor.Value);

            if (Identificacion.MODELO != "B")
                Rgu.WriteSerialNumber(Convert.ToUInt32(TestInfo.NumSerie));

            SamplerWithCancel((p) =>
            {
                Rgu.WriteErrorCode(0);

                Delay(500, "");

                return Rgu.ReadErrorCode() == 0;

            }, "Error no ha grabado el code error 0 en el equipo depues de escribirlo 3 veces");

            //El equipo solo se puede dejar con configuración de defecto pulsando la tecla DEFAULT, no hay ninguna funcion por modbus que lo haga.

            var rele2 = Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM", "ESTANDAR", ParamUnidad.SinUnidad);
            var releSecurity2 = RGU10.Logic.ESTANDAR;
            Enum.TryParse<RGU10.Logic>(rele2, out releSecurity2);

            if ((releSecurity2 == RGU10.Logic.POSITIVA) && (Identificacion.MODELO.ToUpper().Trim() == "CC"))
            {
                Rgu.WriteNormalMode();

                Tower.IO.DO.PulseOn(3000, OUT_TECLA_STD);

                SamplerWithCancel((p) =>
                {
                    var status = Rgu.ReadReleSecurityTime();
                    return status == 2;
                }, "No se ha podido poner la configuración de defecto al equipo", 3, 1000);

            }

            bool parallelReset = Parametrizacion.GetString("PARALLEL_RESET", "NO", ParamUnidad.SinUnidad) == "SI";

            PowerSourceResetAndApplyPrsetes(parallelReset);

            SamplerWithCancel((p) => { Rgu.WriteFlagTest(); return true; }, "Error de comunicaciones, al leer numero de serie después de reiniciar el equipo", 3, 1500, 1000);

            uint serialNumber = 0;

            SamplerWithCancel((p) => { serialNumber = Rgu.ReadSerialNumber(); return true; }, "Error de comunicaciones, al leer numero de serie después de reiniciar el equipo", 3, 1500, 1000);

            if (Identificacion.MODELO != "B")
                Assert.IsTrue(serialNumber.ToString().Trim() == TestInfo.NumSerie.Trim(), Error().UUT.NUMERO_DE_SERIE.NO_COINCIDE("Error en la grabación del número de serie, el número grabado no coincide con el original"));

            var valorTime = Parametrizacion.GetDouble("TIEMPO_RETARDO_PREALARMA", 0, ParamUnidad.SinUnidad);
            if (valorTime != 0)
                rgu.WriteReleSecurityTimeInterval((ushort)valorTime);

            if (Identificacion.MODELO.ToUpper().Trim() == "CC" || Identificacion.MODELO.ToUpper().Trim() == "B")
            {
                var frameNumber = Rgu.ReadFrameNumber();
                Assert.AreEqual(frameNumber, (int)TestInfo.NumBastidor, Error().UUT.NUMERO_DE_BASTIDOR.NO_COINCIDE("Error en la grabación del numero de bastidor, el número grabado no coincide con el original"));
            }

            var errorCode2 = Rgu.ReadErrorCode();
            Resultado.Set("CODE_ERROR", errorCode2, ParamUnidad.SinUnidad);
            Assert.IsTrue(errorCode2 == 0, Error().UUT.CONFIGURACION.NO_GRABADO("Error no ha grabado el code error a 0"));
        }

        public void TestCodigoEmbalageCliente()
        {
            var labelBox = Parametrizacion.GetString("ETIQUETA_EMABALJE", "NO", ParamUnidad.SinUnidad);
            if (labelBox != "NO" && Model.modoPlayer != ModoTest.PostVenta)
            {
                string barcode = "";
                SamplerWithCancel((p) =>
                {
                    var barcodeInput = new InputKeyBoard("Leer código de barras de la etiqueta del embalaje");
                    Shell.ShowDialog("INICIO TEST", () => barcodeInput, MessageBoxButtons.OKCancel);

                    if (barcodeInput.TextValue.ToUpper().Trim() != labelBox.ToUpper().Trim())
                    {
                        Shell.MsgBox("Error. código de barras del embalaje leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DE EMBALAJE INCORRECTO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }

                    barcode = barcodeInput.TextValue;
                    return true;

                }, "Error no se ha leido el código de barras del embalaje", 2);

                if (barcode.ToUpper().Trim() != labelBox.ToUpper().Trim())
                    //throw new Exception("Error. código de barras del embalaje leido no coincide con el de la BBDD");
                    Error().SOFTWARE.BBDD.ERROR_GRABACION("Error. código de barras del embalaje leido no coincide con el de la BBDD").Throw();
            }
        }

        public void TestCodigoLaser()
        {
            string barcode = "";
            var barcodeInputSemana = 99;
            SamplerWithCancel((p) =>
            {
                var barcodeInput = new InputKeyBoard("Leer código de barras del laser del equipo");
                Shell.ShowDialog("COMPROBACION LASER", () => barcodeInput, MessageBoxButtons.OKCancel);

                if (barcodeInput.TextValue.Length != 6)
                {
                    Shell.MsgBox("Error. código de barras del laser leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DEL MARCADO LASER", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                barcodeInputSemana = Int32.Parse(string.Format("{0}{1}", barcodeInput.TextValue[2], barcodeInput.TextValue[3]));

                if (!barcodeInput.TextValue.ToUpper().Trim().StartsWith("ZR") || barcodeInputSemana > 53)
                {
                    Shell.MsgBox("Error. código de barras del laser leido no coincide con el de la BBDD", "LECTURA DEL CODIGO DE BARRAS DEL MARCADO LASER", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                barcode = barcodeInput.TextValue;
                return true;

            }, "Error no se ha leido el código de barras del laser", 2);

            Resultado.Set("CODIGO_ZR", barcode, ParamUnidad.SinUnidad);

            if (!barcode.ToUpper().Trim().StartsWith("ZR") || barcode.Length != 6 || barcodeInputSemana > 53)
                //throw new Exception("Error. código de barras del laser leido no coincide con el de la BBDD");
                Error().SOFTWARE.BBDD.ERROR_GRABACION("Error. código de barras del laser leido no coincide con el de la BBDD").Throw();
        }

        public void TestFinish()
        {
            if (rgu != null)
                rgu.Dispose();

            if (Tower != null)
            {
                Tower.ShutdownSources();

                if (VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad) != "AC")
                    tower.IO.DO.Off(OUT_CIRCUITO_DC);

                Tower.IO.DO.Off(OUT_TECLA_ID, OUT_TECLA_PROG, OUT_TECLA_RESET, OUT_TECLA_SET, OUT_TECLA_STD, OUT_TECLA_TD, OUT_TECLA_TEST);

                Tower.IO.DO.OffWait(300, OUT_PUNTAS_COM_SERIE, OUT_PUNTAS_TTL, OUT_PUNTAS_TTL_BORNES_17, OUT_PUNTAS_TTL_BORNES_18, OUT_PUNTAS_TTL_BORNES_19, OUT_PUNTAS_TTL_BORNES_20, OUT_PUNTAS_TTL_BORNES_21);

                Tower.IO.DO.OffWait(300, OUT_BORNERA_IZQUIERDA, OUT_BORNERA_DERECHA);

                Tower.IO.DO.Off(OUT_SWITCH_COM5_TO_COM4);

                Tower.IO.DO.OffWait(300, OUT_EXTERNAL_TRIGGER);

                Tower.IO.DO.OffWait(300, OUT_FIJACION_CARRO);

                Tower.Dispose();
            }
        }

        #region Auxiliar Functions

        protected void ReadAndSaveResultsCurrentTimeTables()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad);

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad);

            var times = GetTimeScaleTable(timeTableElements);

            Delay(10000, "");

            var resultCurrents = Rgu.ReadScaleTable(RGU10.TableKind.CURRENT);

            Resultado.Set("TABLA_CORRIENTES", resultCurrents, ParamUnidad.SinUnidad, currents.listElementsString.Trim());

            if (resultCurrents.Trim() != currents.listElementsString.Trim())
                //throw new Exception("Error la tabla de corrientes leida no es igual a la BBDD");
                Error().SOFTWARE.BBDD.ERROR_GRABACION("Error la tabla de corrientes leida no es igual a la BBDD").Throw();

           var resultTime = Rgu.ReadScaleTable(RGU10.TableKind.TIME);

            Resultado.Set("TABLA_TIEMPOS", resultTime, ParamUnidad.SinUnidad, times.listElementsString.Trim());

            if (resultTime.Trim() != times.listElementsString.Trim())
                //throw new Exception("Error la tabla de tiempos leida no es igual a la BBDD");
                Error().SOFTWARE.BBDD.ERROR_GRABACION("Error la tabla de tiempos leida no es igual a la BBDD").Throw();
        }

        protected void ReadAndSaveResultsLimits()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad);

            if (currentScaleElements.Trim().ToUpper() == "NO")
                return;

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad);

            if (timeTableElements.Trim().ToUpper() == "NO")
                return;

            var times = GetTimeScaleTable(timeTableElements);

            var currentLimit = Parametrizacion.GetDouble("LIMITE_I_DISPARO", 80, ParamUnidad.mA);

            var timeLimit = Parametrizacion.GetDouble("LIMITE_T_RETARDO", 9, ParamUnidad.ms);

            var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)currentLimit);

            var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)timeLimit);

            var read = Rgu.ReadScaleLimit();

            var tiemporead = (read & 0x00F0) >> 4;

            var currentread = read & 0x000F;

            Resultado.Set("POSICION_LIMITE_T_RETARDO", tiemporead.ToString(), ParamUnidad.SinUnidad);

            Assert.AreEqual((byte)tiemporead, posTime.FirstOrDefault().Position, Error().SOFTWARE.BBDD.ERROR_GRABACION("Error el limite de tiempo no es igual a la BBDD"));

            Resultado.Set("POSICION_LIMITE_I_DISPARO", currentread.ToString(), ParamUnidad.SinUnidad);

            Assert.AreEqual((byte)currentread, posCurrent.FirstOrDefault().Position, Error().SOFTWARE.BBDD.ERROR_GRABACION("Error el limite de corriente no es igual a la BBDD"));
        }

        protected void ReadAndSaveResultsRegisterLocking()
        {
            bool bloqueoTeclaTiempo = Parametrizacion.GetString("LOCK_00_BLOQUEO_TECLA_TIEMPO", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoTeclaIntensidad = Parametrizacion.GetString("LOCK_01_BLOQUEO_TECLA_INTENSIDAD", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoTeclaRele = Parametrizacion.GetString("LOCK_02_BLOQUEO_TECLA_RELE", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoTeclaREC = Parametrizacion.GetString("LOCK_03_BLOQUEO_TECLA_REC", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoMenuSetupComunicaciones = Parametrizacion.GetString("LOCK_04_MOSTRAR_MENU_SETUP_COM", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupFrequencia = Parametrizacion.GetString("LOCK_05_MOSTRAR_MENU_SETUP_FREC", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupLimites = Parametrizacion.GetString("LOCK_06_MOSTRAR_MENU_SETUP_LIM", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupSect = Parametrizacion.GetString("LOCK_07_MOSTRAR_MENU_SETUP_SECT", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupTrigger = Parametrizacion.GetString("LOCK_08_MOSTRAR_MENU_SETUP_TRIP", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupRETC = Parametrizacion.GetString("LOCK_09_MOSTRAR_MENU_SETUP_RSTC", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoModbus = Parametrizacion.GetString("LOCK_10_BLOQUEO_ESCRITURA_MODBUS", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoSRFija = Parametrizacion.GetString("LOCK_11_SR_FIJA", "NO", ParamUnidad.SinUnidad) == "SI";
            bool bloqueoEntradaExterna = Parametrizacion.GetString("LOCK_12_ACTIVACION_ENTRADA_EXTERNA", "NIVEL", ParamUnidad.SinUnidad) == "PULSOS";  ///???????
            bool bloqueoSeleccion2Reles = Parametrizacion.GetString("LOCK_13_BLOQUEO_SELECCION_2_RELES", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";

            BitArray locking = new BitArray(new bool[14] {bloqueoSeleccion2Reles, bloqueoEntradaExterna, bloqueoSRFija, bloqueoModbus,
                bloqueoMenuSetupRETC, bloqueoMenuSetupTrigger, bloqueoMenuSetupSect, bloqueoMenuSetupLimites,bloqueoMenuSetupFrequencia,
                bloqueoMenuSetupComunicaciones,bloqueoTeclaREC ,bloqueoTeclaRele,bloqueoTeclaIntensidad,bloqueoTeclaTiempo});

            ushort registerLock = 0;
            foreach (bool bit in locking)
            {
                var value = bit ? 1 : 0;
                registerLock += (ushort)value;
                registerLock = (ushort)(registerLock << 1);
            }

            registerLock = (ushort)(registerLock >> 1);

            ushort read = (ushort)Rgu.ReadRegisterLocking();

            Resultado.Set("VECTOR_LOCK", read.ToString(), ParamUnidad.SinUnidad);

            Assert.AreEqual(read, registerLock, Error().SOFTWARE.BBDD.ERROR_GRABACION("Error le registro Lock leido no es igual a la BBDD"));
        }

        protected void ReadAndSaveResultsReconectionTables()
        {

            var srdcPosition = Parametrizacion.GetDouble("SRDC_DEFECTO", 10, ParamUnidad.SinUnidad);
            var srmcPosition = Parametrizacion.GetDouble("SRMC_DEFECTO", 5, ParamUnidad.SinUnidad);

            var srdcPositionRead = Rgu.ReadSRDSelection();
            var srmcPositionRead = Rgu.ReadSRMSelection();

            Resultado.Set("SRDC_DEFECTO", srdcPositionRead, ParamUnidad.SinUnidad);
            Resultado.Set("SRMC_DEFECTO", srmcPositionRead, ParamUnidad.SinUnidad);

            Assert.AreEqual(srdcPositionRead, (ushort)srdcPosition, Error().SOFTWARE.BBDD.ERROR_GRABACION("Error: SRDC por defecto leida no es igual a BBDD"));
            Assert.AreEqual(srmcPositionRead, (ushort)srmcPosition, Error().SOFTWARE.BBDD.ERROR_GRABACION("Error: SRMC por defecto leida no es igual a BBDD"));

            var reconexionesSRDC = Parametrizacion.GetString("SRDC_COSTUM", "NO", ParamUnidad.SinUnidad);
            var reconexionesSRMC = Parametrizacion.GetString("SRMC_COSTUM", "NO", ParamUnidad.SinUnidad);

            if (reconexionesSRDC != "NO")
            {
                Delay(1000, "");

                var resultSRDC = Rgu.ReadReconectionTable(RGU10.TableKindReconection.SRDT);

                Resultado.Set("TABLA_SRDC_COSTUM", resultSRDC, ParamUnidad.SinUnidad);

                if (resultSRDC.Trim() != (reconexionesSRDC.Trim() + ";"))
                    //throw new Exception("Error la tabla SRDC leida no es igual a la BBDD");
                    Error().SOFTWARE.BBDD.ERROR_GRABACION("Error la tabla SRDC leida no es igual a la BBDD").Throw();

                Resultado.Set("TABLA_SRDC_RECONEXIONES_TOTALES", Rgu.ReadSRDTableElement(1), ParamUnidad.SinUnidad);

                Resultado.Set("TABLA_SRDC_TIEMPO_RESET", Rgu.ReadSRDTableElement(2), ParamUnidad.SinUnidad);
            }

            if (reconexionesSRMC != "NO")
            {
                var resultSRMC = Rgu.ReadReconectionTable(RGU10.TableKindReconection.SRMT);

                Resultado.Set("TABLA_SRMC_COSTUM", resultSRMC, ParamUnidad.SinUnidad);

                if (resultSRMC.Trim() != reconexionesSRMC.Trim() + ";")
                    //throw new Exception("Error la tabla SRMC leida no es igual a la BBDD");
                    Error().SOFTWARE.BBDD.ERROR_GRABACION("Error la tabla SRMC leida no es igual a la BBDD").Throw();

                Resultado.Set("TABLA_SRMC_RECONEXIONES_TOTALES", Rgu.ReadSRMTableElement(1), ParamUnidad.SinUnidad);

                Resultado.Set("TABLA_SRMC_TIEMPO_RESET", Rgu.ReadSRMTableElement(2), ParamUnidad.SinUnidad);
            }
        }

        protected void ReadAndSaveResultsInitialMesage()
        {
            var initialMessage = Parametrizacion.GetString("MENSAJE_INICIAL", "NO", ParamUnidad.SinUnidad);

            if (initialMessage.ToUpper().Trim() == "NO")
                return;

            var retrievedInitialMessage = "";

            
            if (Identificacion.MODELO == "CC")
                retrievedInitialMessage = Rgu.ReadInitialMessageCC();
            else
                retrievedInitialMessage = Rgu.ReadInitialMessage();

            Resultado.Set("MENSAJE_INICIAL", retrievedInitialMessage, ParamUnidad.SinUnidad);

            Assert.AreEqual(retrievedInitialMessage, initialMessage, Error().UUT.COMUNICACIONES.NO_CORRESPONDE_CON_ENVIO("Error. El mensaje leido no corresponde con el grabado"));
        }

        private void PowerSourceResetAndApplyPrsetesParallel()
        {
            this.InParellel(() =>
            {
                var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);
                var paramV = typeSupply == "AC" ? Params.V.Null.EnVacio.Vnom.Name : Params.V_DC.Null.EnVacio.Vmin.Name;
                var paramI = Params.I.Null.EnVacio.Vnom.Name;
                var voltage = Consignas.GetDouble(paramV, 230, ParamUnidad.V);
                var current = Consignas.GetDouble(paramI, 0, ParamUnidad.A);
                if (typeSupply == "AC")
                {
                    Tower.Chroma.ApplyOff();
                    Delay(1500, "Esperando apagado del equipo");

                    if (voltage > 300)
                    {
                        voltage /= 2;
                        tower.IO.DO.On(13);
                    }
                    Tower.Chroma.ApplyPresetsAndWaitStabilisation(voltage, 0, 50);
                }
                else
                {
                    Tower.LAMBDA.ApplyOff();
                    Delay(1500, "Esperando apagado del equipo");
                    tower.IO.DO.On(OUT_CIRCUITO_DC);
                    Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltage, current);
                    Delay(1500, "Esperando apagado del equipo");
                }
            }, () =>
            {
                SamplerWithCancel((p) =>
                {
                    Rgu.WriteFlagTest();
                    return true;
                }, "Error de comunicaciones, el equipo no arranca o no comunica", 10, 500, 500);
            });
        }

        protected void PowerSourceResetAndApplyPrsetes(bool parallel)
        {
            Delay(2000, "");

            if (!parallel)
            {
                var typeSupply = VectorHardware.GetString("TYPE_SUPPLY", "AC", ParamUnidad.SinUnidad);
                var paramV = typeSupply == "AC" ? Params.V.Null.EnVacio.Vnom.Name : Params.V_DC.Null.EnVacio.Vmin.Name;
                var paramI = Params.I.Null.EnVacio.Vnom.Name;
                var voltage = Consignas.GetDouble(paramV, 230, ParamUnidad.V);
                var current = Consignas.GetDouble(paramI, 0, ParamUnidad.A);
                if (typeSupply == "AC")
                {
                    Tower.Chroma.ApplyOff();
                    Delay(1500, "Esperando apagado del equipo");

                    if (voltage > 300)
                    {
                        voltage /= 2;
                        tower.IO.DO.On(13);
                    }
                    Tower.Chroma.ApplyPresetsAndWaitStabilisation(voltage, 0, 50);
                }
                else
                {
                    Tower.LAMBDA.ApplyOff();
                    Delay(1500, "Esperando apagado del equipo");
                    tower.IO.DO.On(OUT_CIRCUITO_DC);
                    Tower.LAMBDA.ApplyPresetsAndWaitStabilisation(voltage, current);
                    Delay(1500, "Esperando apagado del equipo");
                }
            }
            else
                PowerSourceResetAndApplyPrsetesParallel();
        }

        protected void InternalAdjust(Dictionary<RGU10.ScaleCalibration, double[]> currentDictionaryTemporal)
        {
            Rgu.WriteFlagTest();

            foreach (KeyValuePair<RGU10.ScaleCalibration, double[]> scale in currentDictionaryTemporal)
            {
                Tower.IO.DO.On(OUT_CORRIENTE_L1, OUT_CORRIENTE_L2);

                Tower.PowerSourceIII.ApplyPresetsAndWaitStabilisation(0, new TriLineValue { L1 = scale.Value[0], L2 = scale.Value[1], L3 = 0 }, 50, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 }, new TriLineValue { L1 = 0, L2 = 0, L3 = 0 });

                Tower.IO.DO.OffWait(150, OUT_CORRIENTE_L1, OUT_CORRIENTE_L2);

                var adjustValue = new AdjustValueDef(Params.GAIN_I.Null.Ajuste.TestPoint(scale.Key.ToString()), ParamUnidad.Puntos);

                var resadjustResult = TestMeasureBase(adjustValue, (s) =>
                {
                    Rgu.WriteStartCalibration(scale.Key);

                    Delay(1500, "Esperando a la finalización de la calibración.");

                    var resultadoCalibracion = Rgu.ReadCalibrationResult();

                    if (Identificacion.MODELO.Trim().ToUpper() == "CC")
                        return Math.Sqrt(resultadoCalibracion);
                    else
                        return resultadoCalibracion / 4096;

                }, 0, 4, 500);

                Rgu.WriteScaleConstant(Rgu.calibrationScaleDictionary[scale.Key], (ushort)resadjustResult.Value);

                var lecturaConstante = Rgu.ReadScaleConstant(Rgu.calibrationScaleDictionary[scale.Key]);

                Assert.AreEqual(lecturaConstante, (ushort)resadjustResult.Value, Error().UUT.CONFIGURACION.NO_GRABADO("Error. No se ha grabado correctamente la constante de la " + scale.Key.ToString()));
            }

            Tower.PowerSourceIII.ApplyOffAndWaitStabilisation();
        }

        //**********************************************************************
        //      PARAMETRIZACION
        //**********************************************************************

        protected RGU10.ScaleTableConfiguration GetCurrentScaleTable(string currentScaleElements)
        {
            var scaleElementsList = currentScaleElements.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return new RGU10.ScaleTableConfiguration(scaleElementsList, "corriente");
        }

        protected RGU10.ScaleTableConfiguration GetTimeScaleTable(string timeScaleElements)
        {
            var scaleElementsArray = timeScaleElements.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            for (int i = 0; i < scaleElementsArray.Count(); i++)
            {
                if (scaleElementsArray[i].Trim().ToUpper() == "INS")
                    scaleElementsArray[i] = "1";

                if (scaleElementsArray[i].Trim().ToUpper() == "SEL")
                    scaleElementsArray[i] = "2";
            };

            return new RGU10.ScaleTableConfiguration(scaleElementsArray, "tiempo");
        }

        protected RGU10.ReconnectionTableConfiguration GetSRDCTable(string tablaCostum)
        {
            var numeroReconexiones = (ushort)Parametrizacion.GetDouble("TABLA_SRDC_NUMERO_ETAPAS", 6, ParamUnidad.s);
            
            var tiempoReset =  (ushort)Parametrizacion.GetDouble("TABLA_SRDC_TIEMPO_RESET", 15, ParamUnidad.s);

            RGU10.ReconnectionTableConfiguration srdcTable = new RGU10.ReconnectionTableConfiguration(numeroReconexiones, tiempoReset);

            var TablaCostumElements = tablaCostum.Split(';');

            if ((TablaCostumElements.Length % 2) != 0)
                throw new Exception("Error: La tabla SRDC tiene que tener numero de argumentos pares");
            if (TablaCostumElements.Length > 30)
                throw new Exception("Error: La tabla SRDC tiene mas de 15 etapas");

            for (byte i = 0; i < TablaCostumElements.Length-1; i += 2)
                srdcTable.listStages.Add(new RGU10.ReconnectionTableConfiguration.ReconnectionStage(TablaCostumElements[i], TablaCostumElements[i+1]));

            return srdcTable;
        }

        protected RGU10.ReconnectionTableConfiguration GetSRMCTable(string tablaCostum)
        {
            var numeroReconexiones = (ushort)Parametrizacion.GetDouble("TABLA_SRMC_NUMERO_ETAPAS", 6, ParamUnidad.s);

            var tiempoReset = (ushort)Parametrizacion.GetDouble("TABLA_SRMC_TIEMPO_RESET", 15, ParamUnidad.s);

            RGU10.ReconnectionTableConfiguration srmcTable = new RGU10.ReconnectionTableConfiguration(numeroReconexiones, tiempoReset);

            var TablaCostumElements = tablaCostum.Split(';');

            if ((TablaCostumElements.Length % 2) != 0)
                throw new Exception("Error: La tabla SRDC tiene que tener numero de argumentos pares");
            if (TablaCostumElements.Length > 30)
                throw new Exception("Error: La tabla SRDC tiene mas de 15 etapas");

            for (byte i = 0; i < TablaCostumElements.Length - 1; i += 2)
                srmcTable.listStages.Add(new RGU10.ReconnectionTableConfiguration.ReconnectionStage(TablaCostumElements[i], TablaCostumElements[i + 1]));

            return srmcTable;
        }

        protected void WriteTypeCutt()
        {
            var configuracionTipoCorte = Parametrizacion.GetString("ELEMENTO_CORTE", "MAG", ParamUnidad.SinUnidad);
            switch (configuracionTipoCorte.ToUpper().Trim())
            {
                case "CONT":
                    Rgu.WriteCuttingType(RGU10.CuttingTypes.CONT);
                    break;
                case "MAG":
                    Rgu.WriteCuttingType(RGU10.CuttingTypes.MAG);
                    break;
                case "MAGBOB":
                    Rgu.WriteCuttingType(RGU10.CuttingTypes.MAGBOB);
                    break;
                default:
                    break;
            }

            Resultado.Set("ELEMENTO_CORTE", configuracionTipoCorte, ParamUnidad.SinUnidad);

        }

        protected virtual void WriteCostumerTableCurrentTime()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad);

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad);

            var times = GetTimeScaleTable(timeTableElements);

            var serialNumber = Rgu.ReadSerialNumber();
            if (serialNumber != 0xffffffff)
            {
                Rgu.WriteFlagTest();

                Rgu.ClearCurrentTimeTables();

                Delay(1000, "Espera para el borrado de las tablas");

                Rgu.WriteScaleTable(currents, RGU10.TableKind.CURRENT);

                Rgu.WriteScaleTable(times, RGU10.TableKind.TIME);
            }

            var resultCurrents = Rgu.ReadScaleTable(RGU10.TableKind.CURRENT);

            Resultado.Set("TABLA_CORRIENTES", resultCurrents, ParamUnidad.SinUnidad, currents.listElementsString.Trim());

            if (resultCurrents.Trim() != currents.listElementsString.Trim())
                throw new Exception("Error la tabla de corrientes leida no es igual a la BBDD");

            var resultTime = Rgu.ReadScaleTable(RGU10.TableKind.TIME);

            Resultado.Set("TABLA_TIEMPOS", resultTime, ParamUnidad.SinUnidad, times.listElementsString.Trim());

            if (resultTime.Trim() != times.listElementsString.Trim())
                throw new Exception("Error la tabla de tiempos leida no es igual a la BBDD");

            //*******************************************************************

            Rgu.WriteAllowParametersWriting();

            var current = Parametrizacion.GetDouble("I_DISPARO_DEFECTO", 20, ParamUnidad.mA);

            var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)current);

            if (posCurrent.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la I_DISPARO_DEFECTO no existe en la tabla de corrientes");

            CurrentPosition = posCurrent.FirstOrDefault().Position;

            Resultado.Set("POSICION_I_DISPARO_DEFECTO", CurrentPosition.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteTriggerCurrentSelection(posCurrent.FirstOrDefault().Position);

            var tiempo = Parametrizacion.GetDouble("T_RETARDO_DEFECTO", 100, ParamUnidad.ms);

            var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)tiempo);

            if (posTime.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la T_RETARDO_DEFECTO no existe en la tabla de tiempos");

            TimePosition = posTime.FirstOrDefault().Position;

            Resultado.Set("POSICION_T_RETARDO_DEFECTO", TimePosition.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteTriggerTimeSelection(posTime.FirstOrDefault().Position);

            Rgu.WriteSetupWriting();
        }

        protected void WriteCustomizeReconectionTable()
        {
            var reconexionesSRDC = Parametrizacion.GetString("SRDC_COSTUM", "NO", ParamUnidad.SinUnidad);
            var reconexionesSRMC = Parametrizacion.GetString("SRMC_COSTUM", "NO", ParamUnidad.SinUnidad);

            Rgu.WriteAllowParametersWriting();

            Rgu.ClearReconnectionTables();

            if (reconexionesSRDC != "NO")
            {
                var srdcTable = GetSRDCTable(reconexionesSRDC);

                Rgu.WriteSRDTable(srdcTable);
            }

            if (reconexionesSRMC != "NO")
            {
                var srdcTable = GetSRMCTable(reconexionesSRMC);

                Rgu.WriteSRMTable(srdcTable);
            }

            var srdcPosition = Parametrizacion.GetDouble("SRDC_DEFECTO", 10, ParamUnidad.SinUnidad);
            var srmcPosition = Parametrizacion.GetDouble("SRMC_DEFECTO", 5, ParamUnidad.SinUnidad);

            Rgu.WriteSRDSelection((byte)srdcPosition);

            Rgu.WriteSRMSelection((byte)srmcPosition);

            Rgu.WriteSetupWriting();
        }

        protected void WriteMessageInitial()
        {
            var initialMessage = Parametrizacion.GetString("MENSAJE_INICIAL", "NO", ParamUnidad.SinUnidad);

            if (initialMessage.ToUpper().Trim() == "NO")
                return;

            if (Identificacion.MODELO == "CC")
                Rgu.WriteInitialMessageCC(initialMessage);
            else
                Rgu.WriteInitialMessage(initialMessage);
        }

        protected void WriteCustomizeRelaySecurity()
        {
            //Rgu.WriteNormalMode();
            //Rgu.WriteAllowParametersWriting();
            var releTrip = Parametrizacion.GetString("SEGURIDAD_RELE_TRIP", "ESTANDAR", ParamUnidad.SinUnidad);
            var rele2 = Parametrizacion.GetString("SEGURIDAD_RELE_PREALARM", "ESTANDAR", ParamUnidad.SinUnidad);
            var releSecurityTrip = RGU10.Logic.ESTANDAR;
            Enum.TryParse<RGU10.Logic>(releTrip, out releSecurityTrip);
            var releSecurity2 = RGU10.Logic.ESTANDAR;
            Enum.TryParse<RGU10.Logic>(rele2, out releSecurity2);
            Rgu.WriteReleSecurity(releSecurityTrip, releSecurity2);
            //Rgu.WriteSetupWriting();

            Resultado.Set("SEGURIDAD_RELE_TRIP", releSecurityTrip.ToString(), ParamUnidad.SinUnidad);
            Resultado.Set("SEGURIDAD_RELE_PREALARM", releSecurity2.ToString(), ParamUnidad.SinUnidad);

            if ((releSecurity2 == RGU10.Logic.POSITIVA) && (Identificacion.MODELO.ToUpper().Trim() == "CC"))
            {
                Rgu.WriteFlagTest();
                Rgu.WriteReleSecurityStatus(1);
                Rgu.WriteReleSecurityTimeInterval(0);
            }
       }

        protected void WriteCustomizeLockingConfiguration()
        {
            bool bloqueoTeclaTiempo = Parametrizacion.GetString("LOCK_00_BLOQUEO_TECLA_TIEMPO", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoTeclaIntensidad = Parametrizacion.GetString("LOCK_01_BLOQUEO_TECLA_INTENSIDAD", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoTeclaRele = Parametrizacion.GetString("LOCK_02_BLOQUEO_TECLA_RELE", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoTeclaREC = Parametrizacion.GetString("LOCK_03_BLOQUEO_TECLA_REC", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoMenuSetupComunicaciones = Parametrizacion.GetString("LOCK_04_MOSTRAR_MENU_SETUP_COM", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupFrequencia = Parametrizacion.GetString("LOCK_05_MOSTRAR_MENU_SETUP_FREC", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupLimites = Parametrizacion.GetString("LOCK_06_MOSTRAR_MENU_SETUP_LIM", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupSect = Parametrizacion.GetString("LOCK_07_MOSTRAR_MENU_SETUP_SECT", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupTrigger = Parametrizacion.GetString("LOCK_08_MOSTRAR_MENU_SETUP_TRIP", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoMenuSetupRETC = Parametrizacion.GetString("LOCK_09_MOSTRAR_MENU_SETUP_RSTC", "NO", ParamUnidad.SinUnidad) == "INHIBIR";
            bool bloqueoModbus = Parametrizacion.GetString("LOCK_10_BLOQUEO_ESCRITURA_MODBUS", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";
            bool bloqueoSRFija = Parametrizacion.GetString("LOCK_11_SR_FIJA", "NO", ParamUnidad.SinUnidad) == "SI";
            bool bloqueoEntradaExterna = Parametrizacion.GetString("LOCK_12_ACTIVACION_ENTRADA_EXTERNA", "NIVEL", ParamUnidad.SinUnidad) == "PULSOS";  ///???????
            bool bloqueoSeleccion2Reles = Parametrizacion.GetString("LOCK_13_BLOQUEO_SELECCION_2_RELES", "NO", ParamUnidad.SinUnidad) == "BLOQUEAR";

            BitArray locking = new BitArray(new bool[14] {bloqueoSeleccion2Reles, bloqueoEntradaExterna, bloqueoSRFija, bloqueoModbus,
                bloqueoMenuSetupRETC, bloqueoMenuSetupTrigger, bloqueoMenuSetupSect, bloqueoMenuSetupLimites,bloqueoMenuSetupFrequencia,
                bloqueoMenuSetupComunicaciones,bloqueoTeclaREC ,bloqueoTeclaRele,bloqueoTeclaIntensidad,bloqueoTeclaTiempo});

            ushort registerLock = 0;
            foreach (bool bit in locking)
            {
                var value = bit ? 1 : 0;
                registerLock += (ushort)value;
                registerLock = (ushort)(registerLock << 1);
            }

            registerLock = (ushort)(registerLock >> 1);

            //Resultado.Set("CONFIG_BLOQUEO_MASK", registerLock.ToString(), ParamUnidad.SinUnidad);

            Rgu.WriteRegisterLocking(registerLock);
        }

        protected void WriteCustomizeLimits()
        {
            var currentScaleElements = Parametrizacion.GetString("TABLA_I_DISPARO", "30;100;300;500;1000;3000;5000;10000;30000", ParamUnidad.SinUnidad);

            if (currentScaleElements.Trim().ToUpper() == "NO")
                return;

            var currents = GetCurrentScaleTable(currentScaleElements);

            var timeTableElements = Parametrizacion.GetString("TABLA_T_RETARDO", "INS;SEL;20;100;200;300;400;500;750;1000;3000;5000;10000", ParamUnidad.SinUnidad);

            if (timeTableElements.Trim().ToUpper() == "NO")
                return;

            var times = GetTimeScaleTable(timeTableElements);

            var currentLimit = Parametrizacion.GetDouble("LIMITE_I_DISPARO", 80, ParamUnidad.mA);

            var timeLimit = Parametrizacion.GetDouble("LIMITE_T_RETARDO", 9, ParamUnidad.ms);

            var posCurrent = currents.ListElemnts.Where((p) => p.GetElementValue == (ushort)currentLimit);

            if (posCurrent.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la LIMITE_I_DISPARO no existe en la tabla de corrientes");

            var posTime = times.ListElemnts.Where((p) => p.GetElementValue == (ushort)timeLimit);

            if (posTime.Count() == 0)
                throw new Exception("Error parametrización incorrecta, la LIMITE_T_RETARDO no existe en la tabla de corrientes");

            RGU10.Limits limiteEstado = RGU10.Limits.LOW;
            var Limite = Parametrizacion.GetString("LIMITE", "LOW", ParamUnidad.SinUnidad);
            Enum.TryParse<RGU10.Limits>(Limite, out limiteEstado);

            Rgu.WriteScaleLimit(limiteEstado, posCurrent.FirstOrDefault().Position, posTime.FirstOrDefault().Position);

            Delay(2000, "");
        }

        protected void WriteCustomizeFrequency()
        {
            Rgu.WriteAllowParametersWriting();
            var workingFrequency = TypeMeterConection.frequency._50Hz;
            var frecuencia = Parametrizacion.GetString("FRECUENCIA", "50", ParamUnidad.Hz);
            Enum.TryParse<TypeMeterConection.frequency>(frecuencia, out workingFrequency);
            Rgu.WriteFrequencySelection(workingFrequency);
            Rgu.WriteSetupWriting();

            Resultado.Set("FRECUENCIA", frecuencia, ParamUnidad.Hz);
        }

        protected void WriteCustomizePerifericNumber()
        {
            var perifericNumber = Parametrizacion.GetDouble("PERIFERICO", 0, ParamUnidad.SinUnidad);
            if (perifericNumber == 0)
                return;

            //Rgu.WriteAllowParametersWriting();
            Rgu.WritePeriphericNumber((ushort)perifericNumber);
            //Rgu.WriteSetupWriting();

            Resultado.Set("PERIFERICO", perifericNumber, ParamUnidad.SinUnidad);

            Rgu.Modbus.PerifericNumber = (byte)perifericNumber;
        }

        //**********************************************************************

        //**********************************************************************
        //       VERIFICACION
        //**********************************************************************

        protected void VerificationTableSRDC(RGU10.ReconnectionTableConfiguration tablaSRDC)
        {
            byte index = 0;
            var tableSize = Rgu.ReadSRDTableElement(index);
            Resultado.Set("TAMAÑO_TABLA_SRDC", tableSize, ParamUnidad.SinUnidad);
            index++;
            var totalAmountOfReconnections = Rgu.ReadSRDTableElement(index);
            Resultado.Set("TOTAL_RECONEXIONES_SRDC", totalAmountOfReconnections, ParamUnidad.SinUnidad);
            index++;
            var resetTime = Rgu.ReadSRDTableElement(index);
            Resultado.Set("TIEMPO_RESET_SRDC", totalAmountOfReconnections, ParamUnidad.SinUnidad);
            index++;
            foreach (var srdcTableElement in tablaSRDC.listStages)
            {
                var indexAux = (byte)tablaSRDC.listStages.FindIndex(x => x.Equals(srdcTableElement));

                var srdcElementReading = Rgu.ReadSRDTableElement(index);
                index++;
                Resultado.Set(string.Format("TABLA_SRDC_RECONEXIONES_ETAPA_{0}", indexAux), srdcElementReading, ParamUnidad.SinUnidad);
                //Assert.AreEqual(srdcElementReading, srdcTableElement.Value.AmountOfReconnections, string.Format("Error en la grabación de la tabla de reconexion SRD, el numero de reconexiones del elemento {0} de la tabla con valor {1} no se ha grabado correctamente, valor recuperado {2}", index, srdcTableElement, srdcElementReading));

                srdcElementReading = Rgu.ReadSRDTableElement(index);
                index++;
                Resultado.Set(string.Format("TABLA_SRDC_TIEMPO_RECONEXION_ETAPA_{0}", indexAux), srdcElementReading, ParamUnidad.SinUnidad);
                //Assert.AreEqual(srdcElementReading, srdcTableElement.Value.ReconnectionTime, string.Format("Error en la grabación de la tabla de reconexion SRD, el tiempo de reconexión del elemento {0} de la tabla con valor {1} no se ha grabado correctamente, valor recuperado {2}", index - 1, srdcTableElement, srdcElementReading));
            }
        }

        protected void VerificationTableSRMC(RGU10.ReconnectionTableConfiguration tablaSRMC)
        {
            byte index = 0;
            var tableSize = Rgu.ReadSRMTableElement(index);
            Resultado.Set("TAMAÑO_TABLA_SRMC", tableSize, ParamUnidad.SinUnidad);
            index++;
            var totalAmountOfReconnections = Rgu.ReadSRMTableElement(index);
            Resultado.Set("TOTAL_RECONEXIONES_SRMC", totalAmountOfReconnections, ParamUnidad.SinUnidad);
            index++;
            var resetTime = Rgu.ReadSRMTableElement(index);
            Resultado.Set("TIEMPO_RESET_SRMC", totalAmountOfReconnections, ParamUnidad.SinUnidad);
            index++;

            foreach (var srmcTableElement in tablaSRMC.listStages)
            {
                var indexAux = (byte)tablaSRMC.listStages.FindIndex(x => x.Equals(srmcTableElement));

                var srmcElementReading = Rgu.ReadSRMTableElement(index);
                index++;
                Resultado.Set(string.Format("TABLA_SRMC_RECONEXIONES_ETAPA_{0}", indexAux), srmcElementReading, ParamUnidad.SinUnidad);
                //Assert.AreEqual(srmcElementReading, srmcTableElement.Value.AmountOfReconnections, string.Format("Error en la grabación de la tabla de reconexion SRM, el numero de reconexiones del elemento {0} de la tabla con valor {1} no se ha grabado correctamente, valor recuperado {2}", index, srmcElementReading, srmcTableElement));
                srmcElementReading = Rgu.ReadSRMTableElement(index);
                index++;
                Resultado.Set(string.Format("TABLA_SRMC_TIEMPO_RECONEXION_ETAPA_{0}", indexAux), srmcElementReading, ParamUnidad.SinUnidad);
                //Assert.AreEqual(srmcElementReading, srmcTableElement.Value.ReconnectionTime, string.Format("Error en la grabación de la tabla de reconexion SRM, el tiempo de reconexión del elemento {0} de la tabla con valor {1} no se ha grabado correctamente, valor recuperado {2}", index, srmcElementReading, srmcTableElement));
            }

        }

        #endregion
    }
}
