﻿using Dezac.Core.Utility;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WinDHCP.Library;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(UC_DHCPServerDescription))]
    public partial class UC_DHCPServer : UserControl
    {
        private DhcpServer server;

        public Type InstrumentType
        {
            get
            {
                return typeof(DhcpServer);
            }
        }

        public DhcpServer Instrument
        {
            get
            {
                if (server == null)
                    server = new DhcpServer();

                return server;
            }
            set
            {
                if (server != null)
                {
                    server.Stop();
                    server = null;
                }
                server = value;
            }
        }

        public UC_DHCPServer()
        {
            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();

            if (server != null)
                server.Stop();

            base.Dispose(disposing);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                txtTerminal.Text = "";
                btnStart.Enabled = false;
                button1.Enabled = true;

                StartServer();
            }
            catch (Exception ex)
            {
                btnStart.Enabled = true;
                button1.Enabled = false;
                txtTerminal.Text = ex.Message;
            }
        }

        private void StartServer()
        {
            var startAddress = txtStartAddress.Text.Trim();
            NetUtils.ConfigIP(startAddress, startAddress, "255.255.255.0", ChkBox_isAdmin.Checked);
            var miIp = NetUtils.GetMyIP();
            Instrument.ConfigIP(startAddress,  "255.255.255.0", miIp);
            Instrument.EventLogg += Server_EventLogg;
            Instrument.Start();
        }

        private void StopServer()
        {
            if (server != null)
            {
                server.Stop();
                server.EventLogg -= Server_EventLogg;
                server = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            StopServer();
            btnStart.Enabled = true;
        }

        private void Server_EventLogg(object sender, MyEventArgs e)
        {
            try
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    // Here I want to add my string from the worker-thread to the textbox!
                    txtTerminal.Text += e.MyEventString + Environment.NewLine;
                    txtTerminal.SelectionStart = txtTerminal.Text.Length;
                    txtTerminal.ScrollToCaret();
                });
            }
            catch (Exception ex)
            {
                txtTerminal.Text += ex.Message + Environment.NewLine;
                txtTerminal.SelectionStart = txtTerminal.Text.Length;
                txtTerminal.ScrollToCaret();
            }
        }

        private void btReadClientsDHCP_Click(object sender, EventArgs e)
        {
            if (server != null)
            {
                txtTerminal.Text += string.Format("------------------------------------------------------------") + Environment.NewLine;
                txtTerminal.Text += string.Format("--------  LIST DHCP CLIENTS -------------") + Environment.NewLine;
                txtTerminal.Text += string.Format("------------------------------------------------------------") + Environment.NewLine;

                var clients = server.ClientsActive;
                foreach(var cli in clients)
                {
                    txtTerminal.Text += string.Format("--> CLIENT  -- NAME: {0}  -- MAC: {1}  -- IP: {2}  -- Expired: {3}", cli.Value.HostName, cli.Value.Owner.MAC, cli.Value.Address, cli.Value.Expiration) + Environment.NewLine;
                    txtTerminal.SelectionStart = txtTerminal.Text.Length;
                    txtTerminal.ScrollToCaret();
                }

                txtTerminal.Text += string.Format("---------------------------------------------------------------") + Environment.NewLine;
                txtTerminal.SelectionStart = txtTerminal.Text.Length;
                txtTerminal.ScrollToCaret();
            }
        }

    }


    public class UC_DHCPServerDescription : ControlDescription
    {
        public override string Name { get { return "DHCPServer"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.Apps_esd_icon; } }
        public override Image IconBig { get { return Properties.Resources.DHCPServer; } }
        public override Type SubcomponentType { get { return typeof(DhcpServer); } }
    }
}
