﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Dezac.Tests.Extensions
{
    public static class TestLoginExtension
    {
        public static void FileCopy(string sourcePath, string destinationPath, string userName, string password)
        {
            using (new Login(userName, "dezac.net", password))
            {
                if (File.Exists(sourcePath))
                    File.Copy(sourcePath, destinationPath, true);
            }
        }

        public static void SaveImage(Image image, System.Drawing.Imaging.ImageFormat format, string destinationPath, string userName, string password)
        {
            using (new Login(userName, "dezac.net", password))
            {
                image.Save(destinationPath, format);
            }
        }

        public static void MoveFile(string sourcePath, string destinationPath, string userName, string password)
        {
            using (new Login(userName, "dezac.net", password))
            {
                if (File.Exists(sourcePath))
                    File.Move(sourcePath, destinationPath);
            }
        }

        public static void DeleteFile(string filePath, string userName, string password)
        {
            using (new Login(userName, "dezac.net", password))
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
        }

        public static void CreateDirectory(string path, string userName, string password)
        {
            using (new Login(userName, "dezac.net", password))
            {
                Directory.CreateDirectory(path);
            }
        }

        private class Login : IDisposable
        {
            [DllImport("advapi32.dll", SetLastError = true)]
            private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, out IntPtr phToken);
            [DllImport("kernel32", SetLastError = true)]
            private static extern bool CloseHandle(IntPtr hObject);

            private IntPtr userHandle = IntPtr.Zero;
            private WindowsImpersonationContext impersonationContext;


            public Login(string user, string domain, string password)
            {
                try
                {
                    OSversion version = (OSversion)Environment.OSVersion.Version.Minor;
                    int typeCredential = version == OSversion.Windows_7 ? 9 : 2;

                    if (!string.IsNullOrEmpty(user))
                    {
                        // Call LogonUser to get a token for the user 
                        bool loggedOn = LogonUser(user, domain, password,
                         typeCredential /*(int)LogonType.LOGON32_LOGON_NEW_CREDENTIALS*/,
                         3 /*(int)LogonProvider.LOGON32_PROVIDER_WINNT50*/,
                        out userHandle);
                        if (!loggedOn)
                            throw new Win32Exception(Marshal.GetLastWin32Error());
                        // Begin impersonating the user 
                        impersonationContext = WindowsIdentity.Impersonate(userHandle);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
            public void Dispose()
            {
                try
                {
                    if (userHandle != IntPtr.Zero)
                        CloseHandle(userHandle);
                    if (impersonationContext != null)
                        impersonationContext.Undo();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
        }

        private enum OSversion
        {
            Windows_7 = 1,
            Windows_10 = 2,
        }
    }
}
