using Comunications.Protocolo;
using log4net;
using System;
using System.Linq;
using System.Text;

namespace Comunications.Message
{
    public class MidabusTransport : MessageTransport
	{
        public MidabusTransport(IStreamResource streamResource)
            : base(new ASCIIProtocol() { CaracterFinRx = ";", CaracterFinTx=";" }, streamResource)
        {
            this.ReadTimeout = 2000;
            LogRawMessage = true;
            LogMessage = false;
        }

        public MidabusTransport(IStreamResource streamResource, ILog logger)
            : base(new ASCIIProtocol() { CaracterFinRx = ";", CaracterFinTx = ";" }, streamResource, logger)
        {
            this.ReadTimeout = 2000;
            LogRawMessage = true;
            LogMessage = false;
        }

        protected override string GetLogFrameString(byte[] frame)
        {
            return string.Format("{0}", ASCIIEncoding.ASCII.GetString(frame)); 
        }

        protected override void ValidateResponse(ITransportMessage request, ITransportMessage response)
		{
            if (!request.HasResponse)
                return;

            var typedRequest = request as MidabusMessage;
            typedRequest.ValidateResponse(response);
		}
  
        public byte[] Read(byte[] slaveAddress, string message, string response)
        {

            var data = new MidabusMessage(slaveAddress, message);
            data.Validator = (m) =>
            {
                var resp = ASCIIEncoding.ASCII.GetString(m.Data);
                var adress = ASCIIEncoding.ASCII.GetString(data.SlaveAddress);
                if (!resp.StartsWith(adress + response))
                    throw new Exception(string.Format("Error no se ha recibido la respuesta de la trama {0}", message));

                return true;

            };
            var result = SendMessage<MidabusMessage>(data);
            return result.Data.Skip<byte>(2).ToArray();
        }

        public void Write(byte[] slaveAddress, string message, string response)
        {
            var data = new MidabusMessage(slaveAddress, message);
            data.Validator = (m) =>
            {
                var resp = ASCIIEncoding.ASCII.GetString(m.Data);
                var adress = ASCIIEncoding.ASCII.GetString(data.SlaveAddress);
                if (!resp.StartsWith(adress + response))
                    throw new Exception(string.Format("Error no se ha recibido la respuesta de la trama {0}", message));

                return true;

            };
            var result = SendMessage<MidabusMessage>(data);
        }

        public void Write(byte[] slaveAddress, string message)
        {
            var data = new MidabusMessage(slaveAddress, message);
            data.HasResponse = false;          
            SendMessage<MidabusMessage>(data);
        }

        public void WriteWithoutResponse(byte[] slaveAddress, string message)
        { 
            var data = new MidabusMessage(slaveAddress, message);
            data.HasResponse = false;
            SendMessage<MidabusMessage>(data);
        }
    }
}
