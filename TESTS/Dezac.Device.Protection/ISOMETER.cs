﻿using System;
using System.Threading;

namespace Dezac.Device.Protection
{
    [DeviceVersion(1.00)]
    public class ISOMETER : DeviceBase
    {
        public ISOMETER()
        {
        }

        public ISOMETER(int port = 5)
        {
            SetPort(port);
        }

        public void SetPort(int port, int baudRate = 9600, byte periferic = 1)
        {
            if (Modbus == null)
                Modbus = new ModbusDeviceSerialPort(port, baudRate, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One, _logger);
            else
                Modbus.PortCom = port;

            Modbus.PerifericNumber = periferic;
            Modbus.UseFunction06 = true;
        }

        

        public ModbusDeviceSerialPort Modbus { get; internal set; }

        public override void Dispose()
        {
            Modbus.Dispose();
        }
     
        #region Write operations
        
        public void WriteDisplaySegments(ValuesDisplay value)
        {
            Modbus.Write((ushort)Registers.DISPLAY, (ushort)value);
            //Thread.Sleep(1000);
        }

        public void WriteClearRegisterDigitalInput()
        {
            Modbus.Write((ushort)Registers.DIGITAL_INPUTS, (ushort)0);
            Thread.Sleep(150);
        }

        public void WriteDigitalOutput(DigitalOuputs value)
        {
            Modbus.Write((ushort)Registers.DIGITAL_OUTPUTS, (ushort)value);
        }

        public void WriteOperatingMode(OperatingModeValues value)
        {
            Modbus.Write((ushort)Registers.DEVICE_OPERATION_MODE, (ushort)value);
            Thread.Sleep(500);
        }

        public void FlagTest()
        {
            Modbus.Write((ushort)Registers.DEVICE_OPERATION_MODE, (ushort)OperatingModeValues.TEST);
            Thread.Sleep(500);
        }

        public void ClearCteKHigh()
        {
            Modbus.WriteInt32((ushort)Registers.CTE_K_HI, (int)0x0001);
        }

        public void ClearCteKLow()
        {
            Modbus.Write((ushort)Registers.CTE_K_LO, 0x0001);
        }

        public void ClearCteRiHigh()
        {
            Modbus.Write((ushort)Registers.CTE_Ri_HI, 0x0000);
        }

        public void ClearCteRiLow()
        {
            Modbus.Write((ushort)Registers.CTE_Ri_LO, 0x0000);
        }

        public void ClearParameterization(FlashPages value)
        {
            Modbus.Write((ushort)Registers.CLEAR_FLASH, (ushort) value);
        }
        
        public void WriteAutoTest(NoYesValues value)
        {
            Modbus.Write((ushort)Registers.AUTOTEST, (ushort) value);
        }

        public void WriteClearError()
        {
             Modbus.Write((ushort)Registers.ERROR_REGISTER, (ushort)0);
        }

        public void WriteSerialNumber(uint serialNumber)
        {
            Modbus.WriteInt32((ushort)Registers.SERIAL_NUMBER_HIGH, (int)serialNumber);
        }

        public void WriteNumBastidor(int bastidorNumber)
        {
            Modbus.WriteInt32((ushort)Registers.BASTIDOR_NUMBER_HIGH, bastidorNumber);
        }

        public void WriteCteK(Int32 value)
        {
            Modbus.WriteInt32((ushort)Registers.CTE_K_HI, value);
        }

        public void WriteCteRi(Int32 value)
        {
            Modbus.WriteInt32((ushort)Registers.CTE_Ri_HI, value);
        }

        public void WriteOrderReadCalibration(OrderReadCalibrationValues value)
        {
            Modbus.Write((ushort)Registers.ORDER_READ_CALIBATRION, (ushort) value);
        }

        //public void Reset()
        //{
        //    Modbus.Write((ushort)Registers.EXTERNAL_SIGNAL, (ushort)ExternalSignalValues.RESET);
        //    Thread.Sleep(1000);
        //}

        public void WriteModel(ushort value)
        {
            Modbus.Write((ushort)Registers.MODEL, value);
        }

        public void WriteSubModel(string value)
        {
            Modbus.WriteString((ushort)Registers.SUBMODEL_1, value);
        }

        public void WriteCRC(ushort value)
        {
            Modbus.Write((ushort)Registers.CRC, value);
        }
        
        //*** PARAMETRIZACION

        public void WriteResistanceAlarmAL1TableParameter(ushort positionTableRAN)
        {
            Modbus.Write((ushort)Registers.RAN_AL1, positionTableRAN);
        }

        public void WriteResistanceAlarmAL2TableParameter(ushort positionTableRAN)
        {
            Modbus.Write((ushort)Registers.RAN_AL2, positionTableRAN);
        }

        public void WriteStartDelayTime(ushort value)
        {
            Modbus.Write((ushort)Registers.TIME_DELAY_START, value);
        }

        public void WriteDelayResponse(ushort value)
        {
            Modbus.Write((ushort)Registers.TIME_DELAY_RESPONSE, value);
        }

        public void WriteRegisterMemoryErrors(NoYesValues value)
        {
            Modbus.Write((ushort)Registers.ACTIVATION_MEMORY_ERRORS, (ushort)value);
        }

        public void WritePolarityRelayAL1(PolarityRelayValues value)
        {
            Modbus.Write((ushort)Registers.POLARITY_RELAY_AL1, (ushort)value);
        }

        public void WritePolarityRelayAL2(PolarityRelayValues value)
        {
            Modbus.Write((ushort)Registers.POLARITY_RELAY_AL2, (ushort)value);
        }

        public void WritePasswordLockActivation(NoYesValues value)
        {
            Modbus.Write((ushort)Registers.ACTIVATION_PASSWORD_LOCK, (ushort)value);
        }

        public void WritePasswordNumber(ushort value)
        {
            Modbus.Write((ushort)Registers.PASSWORD_NUMBER, (ushort)value);
        }

        
        #endregion

        #region Read Operations

        public ErrorMessages ReadError()
        {                       
            return (ErrorMessages)Modbus.ReadRegister((ushort)Registers.ERROR_REGISTER);
        }

        public DigitalInputs ReadDigitalInputs()
        {
            return (DigitalInputs) Modbus.ReadRegister((ushort)Registers.DIGITAL_INPUTS);
        }

        public ushort ReadModel()
        {
            return Modbus.ReadRegister((ushort)Registers.MODEL);
        }

        public string ReadSubModel()
        {
            return Modbus.ReadString((ushort)Registers.SUBMODEL_1, 1) + Modbus.ReadString((ushort)Registers.SUBMODEL_2, 1);
        }

        public ushort ReadFirmwareVersion()
        {
            return Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION);
        }

        public ushort ReadFirmwareVersionDisplay()
        {
            return Modbus.ReadRegister((ushort)Registers.FIRMWARE_VERSION_DISPLAY);
        }

        public int ReadExactMeasureRF()
        {
            return (int)((Modbus.ReadRegister((ushort)Registers.MEASURE_RF_HIGH)) << 16) + Modbus.ReadRegister((ushort)Registers.MEASURE_RF_LOW);
        }

        public ushort ReadMeasureRF()
        {
            return Modbus.ReadRegister((ushort)Registers.MEASURE_RF_DISPLAY);
        }

        public double ReadTensionsVCPU()
        {
            return Modbus.ReadRegister((ushort)Registers.TENSION_VCPU) / (double)100;
        }

         public double ReadTensionsVH()
        {
            return Modbus.ReadRegister((ushort)Registers.TENSION_VH) / (double)100;
        }

         public double ReadTensionsVL()
        {
            return Modbus.ReadRegister((ushort)Registers.TENSION_VL) / (double)100;
        }

        public Int32 ReadCteK()
         {
             return Modbus.ReadInt32((ushort)Registers.CTE_K_HI);
         }

        public Int32 ReadCteRi()
        {
            return Modbus.ReadInt32((ushort)Registers.CTE_Ri_HI);
        }

        public Int32 ReadValueCteCalibration()
        {
            return Modbus.ReadInt32((ushort)Registers.READ_VALUE_CALIBRATION);
        }

        public int ReadNumBastidor()
        {
            return (int)((Modbus.ReadRegister((ushort)Registers.BASTIDOR_NUMBER_HIGH)) << 16) + Modbus.ReadRegister((ushort)Registers.BASTIDOR_NUMBER_LOW);
        }

        public uint ReadSerialNumber()
        {
            return (uint)((Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_HIGH) << 16) + Modbus.ReadRegister((ushort)Registers.SERIAL_NUMBER_LOW));
        }

        public ushort ReadResistanceAlarmAL1TableParameter()
        {
            return Modbus.ReadRegister((ushort)Registers.RAN_AL1);
        }

        public ushort ReadResistanceAlarmAL2TableParameter()
        {
            return Modbus.ReadRegister((ushort)Registers.RAN_AL2);
        }

        #endregion


        #region  Enums Parameters Value

        [Flags]
        public enum DigitalInputs
        {
            ALL_INPUTS_OFF = 0X000,
            KEY_R = 0X0002,
            KEY_T = 0X0004,
            KEY_PROG = 0X0008,
            INPUT_T_R = 0X0001,
            INPUT_PE = 0X0010
        }

        [Flags]
        public enum DigitalOuputs
        {
            ALL_OUTPUTS_OFF = 0X000,
            ACTIVATION_RELAY_A1 = 0X0001,
            ACTIVATION_RELAY_A2 = 0X0002,
            ACTIVATION_DISPLAY = 0X0004
        }

        [Flags]
        public enum ValuesDisplay
        {
            ODD_SEGMENTS = 0X0000,
            PAIRS_SEGMENTS = 0X0001,
            ODD_SEGMENTS_AND_COMMON_PAIRS = 0X0002,
            PAIRS_SEGMENTS_AND_COMMON_ODDS = 0X0003,
            ALL_SEGMENTS_OFF = 0X0004,
            ALL_SEGMENTS_ON = 0X0005
        }

        [Flags]
        public enum ErrorMessages
        {
            NO_ERROR = 0X0000,
            CONNECTION_EARTH_WIRE = 0X0001,
            INCORRECT_MEASURE_RA = 0X0002,
            VCPU_OUT_RANG = 0X0004,
            VL_OUT_RANG = 0X0008,
            VH_OUT_RANG = 0X0010,
            COMUNICATIONS_DISPLAY = 0X0020,
            ERROR_MEMORY_REGISTERS_SETUP = 0X0040,
            ERROR_MEMORY_REGISTERS_PARAMETERIZATION = 0X0080,
            ERROR_MEMORY_REGISTERS_IDP = 0X0100,
        } 
     
        [Flags]
        public enum PolarityRelayValues
        {
            STANDARD = 0,
            POSITIVE = 1
        }

        [Flags]
        public enum NoYesValues
        {
            NO = 0,
            YES = 1
        }

        public enum OperatingModeValues
        {
            NORMAL = 1,
            TEST = 2
        }

        public enum ExternalSignalValues
        {
            TEST = 0X0001,
            RESET = 0X0002
        }

        public enum FlashPages
        {
            PARAMETERIZATION = 0X1111,
            SETUP = 0X2222,
            IDP = 0X3333
        }

        public enum OrderReadCalibrationValues
        {
            K = 0X1111,
            Ri = 0X2222
        }

        #endregion
        

        #region  Enums Adress Registers

        [Flags]
        public enum Registers
        {
            ERROR_REGISTER = 0X0081,
            DISPLAY = 0X010B,
            DIGITAL_INPUTS = 0X0102,
            DIGITAL_OUTPUTS = 0X0101,
            TENSION_VH = 0X0105,
            TENSION_VL = 0x0106,
            TENSION_VCPU = 0X0109,
            FIRMWARE_VERSION = 0X080,
            FIRMWARE_VERSION_DISPLAY = 0X010C,
            MODEL = 0X0060,
            SUBMODEL_1 = 0X0061,
            SUBMODEL_2 = 0X0062,
            CRC = 0X0063,
            DEVICE_OPERATION_MODE = 0X00C0,
            CLEAR_FLASH = 0X0100,
            RAN_AL1 = 0X0040,
            RAN_AL2 = 0X0041,
            TIME_DELAY_START = 0X0042,
            TIME_DELAY_RESPONSE = 0X0043,
            ACTIVATION_MEMORY_ERRORS = 0X0044,
            POLARITY_RELAY_AL1 = 0X0045,
            POLARITY_RELAY_AL2 = 0X0046,
            ACTIVATION_PASSWORD_LOCK = 0X0047,
            PASSWORD_NUMBER = 0X0048,
            MEASURE_RF_HIGH = 0X010D,
            MEASURE_RF_LOW = 0X010E,
            MEASURE_RF_DISPLAY = 0X0085,
            SETUP_PAGE = 0X0100,
            BASTIDOR_NUMBER_HIGH = 0X0002,
            BASTIDOR_NUMBER_LOW = 0X0003,
            SERIAL_NUMBER_HIGH = 0X0000,
            SERIAL_NUMBER_LOW = 0X0001,
            EXTERNAL_SIGNAL = 0X00C1,
            AUTOTEST = 0X010F,
            CTE_K_HI = 0X0007,
            CTE_K_LO = 0X0008,
            CTE_Ri_HI = 0x0009,
            CTE_Ri_LO = 0X000A,
            ORDER_READ_CALIBATRION = 0X0110,
            READ_VALUE_CALIBRATION = 0X0111,
        }
        #endregion
    }
}
