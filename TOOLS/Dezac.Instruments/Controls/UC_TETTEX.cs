﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dezac.Core.Utility;
using Instruments.Switch;
using Instruments.Auxiliars;

namespace Dezac.Instruments.Controls
{
    [ToolboxItem(true)]
    [DesignerControl(Type = typeof(TETTEX_Description))]
    public partial class UC_TETTEX : UserControl
    {
        private TETTEX tettex;

        public Type InstrumentType
        {
            get
            {
                return typeof(TETTEX);
            }
        }

        public TETTEX Instrument
        {
            get
            {
                if (tettex == null)
                    tettex = new TETTEX();

                return tettex;
            }
            set
            {
                tettex = value;
            }
        }

        public UC_TETTEX()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }

    public class TETTEX_Description : ControlDescription
    {
        public override string Name { get { return "TETTEX"; } }
        public override string Category { get { return "Instruments"; } }
        public override Image Icon { get { return Properties.Resources.battery_icon; } }
        public override Image IconBig { get { return Properties.Resources.battery_icon; } }
        public override Type SubcomponentType { get { return typeof(TETTEX); } }
        public override bool Visible => false;
    }
}