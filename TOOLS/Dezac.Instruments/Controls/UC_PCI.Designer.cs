﻿namespace Dezac.Instruments.Controls
{
    partial class UC_PCI
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.layoutO = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.panelOutputs = new System.Windows.Forms.FlowLayoutPanel();
            this.layoutI = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.InputPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panelMux = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutO)).BeginInit();
            this.layoutO.Panel1.SuspendLayout();
            this.layoutO.Panel2.SuspendLayout();
            this.layoutO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutI)).BeginInit();
            this.layoutI.Panel1.SuspendLayout();
            this.layoutI.Panel2.SuspendLayout();
            this.layoutI.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Window;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.layoutO);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.layoutI);
            this.splitContainer1.Size = new System.Drawing.Size(815, 309);
            this.splitContainer1.SplitterDistance = 460;
            this.splitContainer1.SplitterWidth = 13;
            this.splitContainer1.TabIndex = 0;
            // 
            // layoutO
            // 
            this.layoutO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutO.IsSplitterFixed = true;
            this.layoutO.Location = new System.Drawing.Point(0, 0);
            this.layoutO.Margin = new System.Windows.Forms.Padding(4);
            this.layoutO.Name = "layoutO";
            this.layoutO.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // layoutO.Panel1
            // 
            this.layoutO.Panel1.Controls.Add(this.label1);
            // 
            // layoutO.Panel2
            // 
            this.layoutO.Panel2.BackColor = System.Drawing.Color.LightGray;
            this.layoutO.Panel2.Controls.Add(this.panelOutputs);
            this.layoutO.Size = new System.Drawing.Size(460, 309);
            this.layoutO.SplitterDistance = 25;
            this.layoutO.SplitterWidth = 5;
            this.layoutO.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(460, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "OUTPUTS";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelOutputs
            // 
            this.panelOutputs.AutoScroll = true;
            this.panelOutputs.AutoSize = true;
            this.panelOutputs.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelOutputs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOutputs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOutputs.Location = new System.Drawing.Point(0, 0);
            this.panelOutputs.Margin = new System.Windows.Forms.Padding(4);
            this.panelOutputs.Name = "panelOutputs";
            this.panelOutputs.Size = new System.Drawing.Size(460, 279);
            this.panelOutputs.TabIndex = 0;
            // 
            // layoutI
            // 
            this.layoutI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutI.IsSplitterFixed = true;
            this.layoutI.Location = new System.Drawing.Point(0, 0);
            this.layoutI.Margin = new System.Windows.Forms.Padding(4);
            this.layoutI.Name = "layoutI";
            this.layoutI.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // layoutI.Panel1
            // 
            this.layoutI.Panel1.Controls.Add(this.label3);
            this.layoutI.Panel1.Controls.Add(this.label2);
            // 
            // layoutI.Panel2
            // 
            this.layoutI.Panel2.BackColor = System.Drawing.Color.LightGray;
            this.layoutI.Panel2.Controls.Add(this.InputPanel);
            this.layoutI.Size = new System.Drawing.Size(342, 309);
            this.layoutI.SplitterDistance = 25;
            this.layoutI.SplitterWidth = 5;
            this.layoutI.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(342, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "INPUTS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // InputPanel
            // 
            this.InputPanel.AutoScroll = true;
            this.InputPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.InputPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InputPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InputPanel.Location = new System.Drawing.Point(0, 0);
            this.InputPanel.Margin = new System.Windows.Forms.Padding(4);
            this.InputPanel.Name = "InputPanel";
            this.InputPanel.Size = new System.Drawing.Size(342, 279);
            this.InputPanel.TabIndex = 0;
            // 
            // panelMux
            // 
            this.panelMux.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelMux.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMux.Location = new System.Drawing.Point(0, 348);
            this.panelMux.Name = "panelMux";
            this.panelMux.Size = new System.Drawing.Size(815, 220);
            this.panelMux.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 317);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(815, 25);
            this.label4.TabIndex = 1;
            this.label4.Text = "MULTIPLEXORA";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PCI_7296
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panelMux);
            this.Controls.Add(this.splitContainer1);
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(820, 698);
            this.MinimumSize = new System.Drawing.Size(800, 246);
            this.Name = "PCI_7296";
            this.Size = new System.Drawing.Size(815, 568);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.layoutO.Panel1.ResumeLayout(false);
            this.layoutO.Panel2.ResumeLayout(false);
            this.layoutO.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutO)).EndInit();
            this.layoutO.ResumeLayout(false);
            this.layoutI.Panel1.ResumeLayout(false);
            this.layoutI.Panel1.PerformLayout();
            this.layoutI.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutI)).EndInit();
            this.layoutI.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer layoutO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer layoutI;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel panelOutputs;
        private System.Windows.Forms.FlowLayoutPanel InputPanel;
        private System.Windows.Forms.Panel panelMux;
        private System.Windows.Forms.Label label4;
    }
}
