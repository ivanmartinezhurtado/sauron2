﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.IO;
using System.Windows.Forms;
using TaskRunner;
using WinTaskRunner.Utils;

namespace WinTaskRunner.Reports
{
    public partial class ImprimirReport : Form
    {
        private ReportDocument report = null;

        private string reportName;
        private object data;

        public ImprimirReport()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (report != null)
                viewer.ReportSource = report;

            Cursor.Current = Cursors.Default;
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);

            try
            {
                if (report != null)
                    report.Dispose();
            }
            catch
            {
            }
        }

        public bool LoadReport(string reportName, TestResult testResult, ITestReportAdapter adapter = null)
        {
            if (report != null)
                report.Dispose();

            this.reportName = reportName;

            try
            {
                if (adapter == null)
                    adapter = new TestReportAdapter();

                data = adapter.GetReport(testResult);

                report = new TestReport();
                //report.Load(Path.Combine(AppProfile.ReportsPath, reportName));
                report.ReportOptions.EnableSaveDataWithReport = false;
                report.SetDataSource(data);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
